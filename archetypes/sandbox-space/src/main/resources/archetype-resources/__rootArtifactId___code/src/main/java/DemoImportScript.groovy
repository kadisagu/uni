import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType
import ru.tandemservice.uni.dao.core.CoreDao
import ru.tandemservice.uni.dao.core.ICoreDao

// класс для чтения и записи данных
final ICoreDao dao = IUniBaseDao.instance.get();

// головное подразделение
TopOrgUnit academy = TopOrgUnit.getInstance();

OrgUnitType ouType = new OrgUnitType();
ouType.setTitle("Кафедра"); ouType.setCode("caf");
ouType.setPlural("Кафедры"); ouType.setGenitive("Кафедры");
ouType.setAllowEmployee(true); ouType.setAllowArchival(true);
ouType.setPriority(10);

dao.save(ouType);

final String title = "Тестовая кафедра"
final String shortTitle = "ТК"

OrgUnit testOu = new OrgUnit();
testOu.setOrgUnitType(ouType);
testOu.setParent(academy);

testOu.setTitle(title);
testOu.setFullTitle(title);
testOu.setTerritorialTitle(title);
testOu.setTerritorialFullTitle(title);

testOu.setShortTitle(shortTitle);
testOu.setTerritorialShortTitle(title);

dao.save(testOu);