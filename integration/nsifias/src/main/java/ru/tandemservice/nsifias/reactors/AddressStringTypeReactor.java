/* $Id$ */
package ru.tandemservice.nsifias.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.fias.base.entity.AddressString;
import ru.tandemservice.nsiclient.datagram.AddressStringType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ConditionalDatagramArrayList;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 25.09.2015
 */
public class AddressStringTypeReactor extends BaseEntityReactor<AddressString, AddressStringType>
{
    private static final String ADDRESS = "AddressID";

    @Override
    public Class<AddressString> getEntityClass()
    {
        return AddressString.class;
    }

    @Override
    public AddressStringType createDatagramObject(String guid)
    {
        AddressStringType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressStringType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AddressString entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AddressStringType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressStringType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAddress(entity.getAddress());

        return new ResultListWrapper(null, result);
    }

    @Override
    public ChangedWrapper<AddressString> processDatagramObject(AddressStringType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        String address = StringUtils.trimToNull(datagramObject.getAddress());

        boolean isNew = false;
        boolean isChanged = false;
        AddressString entity = null;
        CoreCollectionUtils.Pair<AddressString, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null)
        {
            entity = new AddressString();
            isNew = true;
            isChanged = true;
        }

        if (address != null && !address.equals(entity.getAddress()))
        {
            isChanged = true;
            entity.setAddress(address);
        }

        if(null == entity.getAddress())
        {
            isChanged = true;
            entity.setAddress("-");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, null);
    }
}