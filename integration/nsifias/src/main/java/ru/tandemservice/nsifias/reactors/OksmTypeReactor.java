/* $Id: OksmTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsifias.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.OksmType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class OksmTypeReactor extends BaseEntityReactor<AddressCountry, OksmType>
{
    @Override
    public Class<AddressCountry> getEntityClass() {
        return AddressCountry.class;
    }

    @Override
    public OksmType createDatagramObject(String guid)
    {
        OksmType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createOksmType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AddressCountry entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        OksmType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createOksmType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setOksmID(Integer.toString(entity.getDigitalCode()));
        datagramObject.setOksmNameFull(entity.getFullTitle());
        datagramObject.setOksmNameShort(entity.getShortTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected AddressCountry findAddressCountry(Integer codeInt, String shortName, String fullName)
    {
        if(codeInt != null) {
            List<AddressCountry> list = findEntity(eq(property(ENTITY_ALIAS, AddressCountry.digitalCode()), value(codeInt)));
            if (list.size() == 1) return list.get(0);
        }
        if(fullName != null)
        {
            List<AddressCountry> list = findEntity(eq(property(ENTITY_ALIAS, AddressCountry.fullTitle()), value(fullName)));
            if (list.size() == 1) return list.get(0);
        }
        if(shortName != null)
        {
            List<AddressCountry> list = findEntity(eq(property(ENTITY_ALIAS, AddressCountry.title()), value(shortName)));
            if (list.size() == 1) return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<AddressCountry> processDatagramObject(OksmType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // OksmID init
        String code = StringUtils.trimToNull(datagramObject.getOksmID());
        Integer codeInt = null;
        if(code != null)
        {
            try {
                codeInt = Integer.parseInt(code);
            } catch (NumberFormatException e) {
                throw new ProcessingDatagramObjectException("OksmID is not number: " + code + '!');
            }
        }

        // OksmNameFull init
        String fullName = StringUtils.trimToNull(datagramObject.getOksmNameFull());
        // OksmNameShort init
        String shortName = StringUtils.trimToNull(datagramObject.getOksmNameShort());

        boolean isNew = false;
        boolean isChanged = false;
        AddressCountry entity = null;
        CoreCollectionUtils.Pair<AddressCountry, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findAddressCountry(codeInt, shortName, fullName);
        if(entity == null)
        {
            entity = new AddressCountry();
            isNew = true;
            isChanged = true;
        }

        // OksmNameFull set
        if(fullName != null && !fullName.equals(entity.getFullTitle()))
        {
            isChanged = true;
            entity.setFullTitle(fullName);
        }

        // OksmNameShort set
        if (null == shortName && isNew)
        {
            shortName = fullName;
            if(shortName == null)
                throw new ProcessingDatagramObjectException("OksmType object without OksmNameShort and OksmNameFull!");
        }
        if(shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }

        StringBuilder warning = new StringBuilder();
        String title = StringUtils.isEmpty(entity.getFullTitle()) ? entity.getShortTitle() : entity.getFullTitle();
        if(StringUtils.isEmpty(entity.getTitle()))
        {
            entity.setTitle(title);
            if(!DataAccessServices.dao().isUnique(AddressCountry.class, entity, AddressCountry.P_TITLE))
                throw new ProcessingDatagramObjectException("OksmName is not unique!");
            isChanged = true;
        }

        // OksmID set
        // Поле code immutable, поэтому оставляем как есть
        if(isNew)
        {
            if (codeInt == null)
                throw new ProcessingDatagramObjectException("OksmType object without OksmID!");
            entity.setCode(codeInt);
            entity.setDigitalCode(codeInt);
            if (!DataAccessServices.dao().isUnique(AddressCountry.class, entity, AddressCountry.P_CODE))
                throw new ProcessingDatagramObjectException("OksmID is not unique!");
        } else if(codeInt != null && codeInt != entity.getCode())
            warning.append("[OksmID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<IDatagramObject> createTestObjects() {
        OksmType entity = new OksmType();
        entity.setID("cc5d937a-3202-3c23-8a11-oksmTypexxxX");
        entity.setOksmNameShort("shortNameTest");
        entity.setOksmNameFull("fullNameTest");
        entity.setOksmID("99999");

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        return datagrams;
    }

    @Override
    public OksmType getEmbedded(boolean onlyGuid)
    {
        OksmType entity = new OksmType();
        entity.setID("cc5d937a-3202-3c23-8a11-oksmEmbeddxX");

        if (!onlyGuid)
        {
            entity.setOksmID("99999");
            entity.setOksmNameShort("shortNameEmbeddedTest");
            entity.setOksmNameFull("fullNameEmbeddedTest");
        }
        return entity;
    }

    @Override
    public boolean isCorrect(AddressCountry entity, OksmType datagramObject, List<IDatagramObject> retrievedDatagram) {
        return datagramObject.getOksmNameShort().equals(entity.getShortTitle())
                && datagramObject.getOksmNameFull().equals(entity.getFullTitle())
                && datagramObject.getOksmNameFull().equals(entity.getTitle());
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(AddressCountry.P_ALPHA2);
        fields.add(AddressCountry.P_ALPHA3);
        fields.add(AddressCountry.P_DIGITAL_CODE);
        fields.add(AddressCountry.L_COUNTRY_TYPE);
        fields.add(AddressCountry.P_TITLE);
        return  fields;
    }
}