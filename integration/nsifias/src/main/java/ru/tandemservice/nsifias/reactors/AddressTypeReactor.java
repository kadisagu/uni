/* $Id$ */
package ru.tandemservice.nsifias.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AddressType;
import ru.tandemservice.nsiclient.datagram.AddressTypeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.OksmType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.08.2015
 */
public class AddressTypeReactor extends BaseEntityReactor<AddressItem, AddressType>
{
    private static final String PARENT_ADDRESS = "parentAddressID";
    private static final String OKSM = "oksmID";
    private static final String ADDRESS_TYPE = "AddressTypeID";

    @Override
    public Class<AddressItem> getEntityClass()
    {
        return AddressItem.class;
    }

    @Override
    public void collectUnknownDatagrams(AddressType datagramObject, IUnknownDatagramsCollector collector) {
        AddressType.ParentAddressID parentAddressID = datagramObject.getParentAddressID();
        AddressType addressType = parentAddressID == null ? null : parentAddressID.getAddress();
        if (addressType != null) collector.check(addressType.getID(), AddressType.class);

        AddressType.AddressTypeID addressTypeID = datagramObject.getAddressTypeID();
        AddressTypeType addressTypeType = addressTypeID == null ? null : addressTypeID.getAddressType();
        if (addressTypeType != null) collector.check(addressTypeType.getID(), AddressTypeType.class);

        AddressType.OksmID oksmID = datagramObject.getOksmID();
        OksmType oksmType = oksmID == null ? null : oksmID.getOksm();
        if (oksmType != null) collector.check(oksmType.getID(), OksmType.class);
    }

    protected AddressItem findAddressItem(String code)
    {
        if (code != null)
        {
            List<AddressItem> list = findEntity(eq(property(ENTITY_ALIAS, AddressItem.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public AddressType createDatagramObject(String guid)
    {
        AddressType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AddressItem entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AddressType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAddressID(entity.getCode());
        datagramObject.setAddressName(entity.getTitle());
        datagramObject.setAddressPostcode(entity.getPostcode());
        datagramObject.setAddressOkato(entity.getCodeOKATO());

        if (entity.getParent()!=null)
        {
            ProcessedDatagramObject<AddressType> parentAddressType = createDatagramObject(AddressType.class, entity.getParent(), commonCache, warning);
            AddressType.ParentAddressID parentAddressID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressTypeParentAddressID();
            parentAddressID.setAddress(parentAddressType.getObject());
            datagramObject.setParentAddressID(parentAddressID);
            if (parentAddressType.getList() != null)
                result.addAll(parentAddressType.getList());
        }
        ProcessedDatagramObject<OksmType> oksmType = createDatagramObject(OksmType.class, entity.getCountry(), commonCache, warning);
        AddressType.OksmID oksmID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressTypeOksmID();
        oksmID.setOksm(oksmType.getObject());
        datagramObject.setOksmID(oksmID);
        if (oksmType.getList() != null)
            result.addAll(oksmType.getList());

        ProcessedDatagramObject<AddressTypeType> addressType = createDatagramObject(AddressTypeType.class, entity.getAddressType(), commonCache, warning);
        AddressType.AddressTypeID addressTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressTypeAddressTypeID();
        addressTypeID.setAddressType(addressType.getObject());
        datagramObject.setAddressTypeID(addressTypeID);
        if (addressType.getList() != null)
            result.addAll(addressType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }


    @Override
    public ChangedWrapper<AddressItem> processDatagramObject(AddressType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();


        //Address init
        AddressType.ParentAddressID parentAddressID = datagramObject.getParentAddressID();
        AddressType parentAddressType = parentAddressID!=null?parentAddressID.getAddress():null;
        AddressItem addressItem = getOrCreate(AddressType.class, params, commonCache, parentAddressType, PARENT_ADDRESS, null, warning);

        AddressType.OksmID oksmID = datagramObject.getOksmID();
        OksmType oksmType = oksmID!=null?oksmID.getOksm():null;
        AddressCountry addressCountry = getOrCreate(OksmType.class, params, commonCache, oksmType, OKSM, null, warning);

        AddressType.AddressTypeID addressTypeID = datagramObject.getAddressTypeID();
        AddressTypeType addressTypeType = addressTypeID == null ? null : addressTypeID.getAddressType();
        org.tandemframework.shared.fias.base.entity.AddressType addressType = getOrCreate(AddressTypeType.class, params, commonCache, addressTypeType, ADDRESS_TYPE, null, warning);

        String code = StringUtils.trimToNull(datagramObject.getAddressID());
        String title = StringUtils.trimToNull(datagramObject.getAddressName());
        String postCode = StringUtils.trimToNull(datagramObject.getAddressPostcode());
        String okato = StringUtils.trimToNull(datagramObject.getAddressOkato());

        boolean isNew = false;
        boolean isChanged = false;
        AddressItem entity = null;

        CoreCollectionUtils.Pair<AddressItem, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findAddressItem(code);
        if (entity == null)
        {
            entity = new AddressItem();
            isNew = true;
            isChanged = true;
        }

        // AddressType set
        if(addressType != null && !addressType.equals(entity.getAddressType())) {
            isChanged = true;
            entity.setAddressType(addressType);
        }
        if (entity.getAddressType() == null)
            throw new ProcessingDatagramObjectException("AddressType object without addressTypeID!");
        if (okato != null && !okato.equals(entity.getCodeOKATO()))
        {
            isChanged = true;
            entity.setCodeOKATO(okato);
        }

        if (addressCountry != null && !addressCountry.equals(entity.getCountry()))
        {
            isChanged = true;
            entity.setCountry(addressCountry);
        }
        if (entity.getCountry() == null)
            throw new ProcessingDatagramObjectException("AddressType object without OksmID");

        if (postCode != null && !postCode.equals(entity.getPostcode()))
        {
            isChanged = true;
            entity.setPostcode(postCode);
        }

        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            entity.setTitleLatin(CoreStringUtils.transliterate(title));
        }
        if (entity.getTitle() == null)
            throw new ProcessingDatagramObjectException("AddressType object without AddressName");

        if (addressItem != null && !addressItem.equals(entity.getParent()))
        {
            isChanged = true;
            entity.setParent(addressItem);
        }

        if (entity.getParent() != null && entity.getParent().getId().equals(entity.getId()))
            throw new ProcessingDatagramObjectException("AddressType and it's parent have the same ID");

        if (isNew)
        {
            if (code == null)
                throw new ProcessingDatagramObjectException("AddressType object without AddressID!");
            entity.setCode(code);
            if (!DataAccessServices.dao().isUnique(AddressItem.class, entity, AddressItem.P_CODE))
                throw new ProcessingDatagramObjectException("AddressID is not unique!");
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[AddressTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<AddressItem> w, AddressType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(AddressTypeType.class, session, params, ADDRESS_TYPE, nsiPackage);
            saveChildEntity(AddressType.class, session, params, PARENT_ADDRESS, nsiPackage);
            saveChildEntity(OksmType.class, session, params, OKSM, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(AddressItem.P_OPTIONAL_CODE1);
        fields.add(AddressItem.P_OPTIONAL_CODE2);
        fields.add(AddressItem.P_STATUS);
        fields.add(AddressItem.P_TITLE_LATIN);
        fields.add(AddressItem.P_DIRECT_TITLE);
        fields.add(AddressItem.P_INHERITED_REGION_CODE);
        fields.add(AddressItem.P_INHERITED_POST_CODE);
        return  fields;
    }
}
