/* $Id$ */
package ru.tandemservice.nsifias.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.*;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AddressRUType;
import ru.tandemservice.nsiclient.datagram.AddressType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.09.2015
 */
public class AddressRUTypeReactor extends BaseEntityReactor<AddressRu, AddressRUType>
{
    private static final String ADDRESS = "AddressID";

    @Override
    public Class<AddressRu> getEntityClass()
    {
        return AddressRu.class;
    }

    @Override
    public void collectUnknownDatagrams(AddressRUType datagramObject, IUnknownDatagramsCollector collector) {
        AddressRUType.AddressID addressID = datagramObject.getAddressID();
        AddressType addressType = addressID == null ? null : addressID.getAddress();
        if (addressType != null) collector.check(addressType.getID(), AddressType.class);
    }

    @Override
    public AddressRUType createDatagramObject(String guid)
    {
        AddressRUType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressRUType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AddressRu entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AddressRUType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressRUType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAddressRUDistrict(entity.getDistrict() != null ? entity.getDistrict().getTitle() : null);
        datagramObject.setAddressRUStreet(entity.getStreet() != null ? entity.getStreet().getTitle() : null);
        datagramObject.setAddressRUHouseUnitNumber(entity.getHouseUnitNumber());
        datagramObject.setAddressRUHouseNumber(entity.getHouseNumber());
        datagramObject.setAddressRUFlatNumber(entity.getFlatNumber());

        if (entity.getSettlement()!=null)
        {
            ProcessedDatagramObject<AddressType> addressType = createDatagramObject(AddressType.class, entity.getSettlement(), commonCache, warning);
            AddressRUType.AddressID addressID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressRUTypeAddressID();
            addressID.setAddress(addressType.getObject());
            datagramObject.setAddressID(addressID);
            if (addressType.getList() != null)
                result.addAll(addressType.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected AddressRu findAddressRu(AddressItem addressItem, String district, String street, String houseNumber, String houseUnitNumber, String flat) {
        List<IExpression> expressions = new ArrayList<>();
        if(addressItem != null)
            expressions.add(eq(property(ENTITY_ALIAS, AddressRu.settlement().id()), value(addressItem.getId())));
        if(district != null)
            expressions.add(eq(property(ENTITY_ALIAS, AddressRu.district().title()), value(district)));
        if(street != null)
            expressions.add(eq(property(ENTITY_ALIAS, AddressRu.street().title()), value(street)));
        if(houseNumber != null)
            expressions.add(eq(property(ENTITY_ALIAS, AddressRu.houseNumber()), value(houseNumber)));
        if(houseUnitNumber != null)
            expressions.add(eq(property(ENTITY_ALIAS, AddressRu.houseUnitNumber()), value(houseUnitNumber)));
        if(flat != null)
            expressions.add(eq(property(ENTITY_ALIAS, AddressRu.flatNumber()), value(flat)));
        if (!expressions.isEmpty())
        {
            List<AddressRu> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    protected AddressDistrict findAddressDistrict(String title) {
        if (title != null)
        {
            List<AddressDistrict> list = findEntity(AddressDistrict.class, eq(property(ENTITY_ALIAS, AddressDistrict.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    protected AddressStreet findAddressStreet(String title) {
        if (title != null)
        {
            List<AddressStreet> list = findEntity(AddressStreet.class, eq(property(ENTITY_ALIAS, AddressStreet.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<AddressRu> processDatagramObject(AddressRUType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();
        String district = datagramObject.getAddressRUDistrict();
        String street = datagramObject.getAddressRUStreet();
        String houseNumber = datagramObject.getAddressRUHouseNumber();
        String houseUnitNumber = datagramObject.getAddressRUHouseUnitNumber();
        String flat = datagramObject.getAddressRUFlatNumber();

        AddressRUType.AddressID addressID = datagramObject.getAddressID();
        AddressType addressType = addressID == null ? null : addressID.getAddress();
        AddressItem addressItem = getOrCreate(AddressType.class, params, commonCache, addressType, ADDRESS, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        AddressRu entity = null;
        CoreCollectionUtils.Pair<AddressRu, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findAddressRu(addressItem, district, street, houseNumber, houseUnitNumber, flat);
        if(entity == null)
        {
            entity = new AddressRu();
            isNew = true;
            isChanged = true;
        }

        if(district != null && (entity.getDistrict() == null || !district.equals(entity.getDistrict().getTitle())))
        {
            AddressDistrict addressDistrict = findAddressDistrict(district);
            if(addressDistrict == null)
                warning.append("[District with title '").append(street).append("' not found]");
            else {
                isChanged = true;
                entity.setDistrict(addressDistrict);
            }
        }

        if(street != null && (entity.getStreet() == null || !street.equals(entity.getStreet().getTitle())))
        {
            AddressStreet addressStreet = findAddressStreet(street);
            if(addressStreet == null)
                warning.append("[Street with title '").append(street).append("' not found]");
            else {
                isChanged = true;
                entity.setStreet(addressStreet);
            }
        }

        if(houseNumber != null && !houseNumber.equals(entity.getHouseNumber()))
        {
            isChanged = true;
            entity.setHouseNumber(houseNumber);
        }

        if(houseUnitNumber != null && !houseUnitNumber.equals(entity.getHouseUnitNumber()))
        {
            isChanged = true;
            entity.setHouseUnitNumber(houseUnitNumber);
        }

        if(flat != null && !flat.equals(entity.getFlatNumber()))
        {
            isChanged = true;
            entity.setFlatNumber(flat);
        }

        if(addressItem != null && !addressItem.equals(entity.getSettlement()))
        {
            isChanged = true;
            entity.setSettlement(addressItem);
        }
        if (null == entity.getCountry()) {
            if (entity.getSettlement() != null)
                entity.setCountry(entity.getSettlement().getCountry());
            if (null == entity.getCountry())
                entity.setCountry(DataAccessServices.dao().get(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE));
        }

        // addressItemCheck check
        if(entity.getStreet() != null && (entity.getSettlement() == null || !entity.getStreet().getParent().getId().equals(entity.getSettlement().getId())))
            throw new ProcessingDatagramObjectException("Если указана улица, то населенный пункт должен быть актуальным");
        // addressCountryCheck check
        if(entity.getSettlement() != null && (entity.getCountry() == null || !entity.getSettlement().getCountry().getId().equals(entity.getCountry().getId())))
            throw new ProcessingDatagramObjectException("Если указан населенный пункт, то страна должна быть актуальной");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<AddressRu> w, AddressRUType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(AddressType.class, session, params, ADDRESS, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(AddressRu.P_INHERITED_POST_CODE);
        return  fields;
    }
}
