/* $Id$ */
package ru.tandemservice.nsifias.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<Boolean> orderableExtPoint()
    {
        return itemListExtension(_catalogManager.orderableExtPoint())
                .add(AddressDetailed.ENTITY_NAME, false)
                .add(AddressBase.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> filterableExtPoint()
    {
        return itemListExtension(_catalogManager.filterableExtPoint())
                .add(AddressDetailed.ENTITY_NAME, false)
                .add(AddressBase.ENTITY_NAME, false)
                .create();
    }
}
