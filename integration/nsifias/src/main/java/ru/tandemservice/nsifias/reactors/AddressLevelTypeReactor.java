/* $Id$ */
package ru.tandemservice.nsifias.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.base.entity.AddressLevel;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.AddressLevelType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 28.08.2015
 */
public class AddressLevelTypeReactor extends BaseEntityReactor<AddressLevel, AddressLevelType>
{
    @Override
    public Class<AddressLevel> getEntityClass()
    {
        return AddressLevel.class;
    }

    protected AddressLevel findAddressLevel(Integer codeInt, String title)
    {
        if (codeInt != null)
        {
            List<AddressLevel> list = findEntity(eq(property(ENTITY_ALIAS, AddressLevel.code()), value(codeInt)));
            if (list.size() == 1) return list.get(0);
        }
        if (title != null)
        {
            List<AddressLevel> list = findEntity(eq(property(ENTITY_ALIAS, AddressLevel.title()), value(title)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public AddressLevelType createDatagramObject(String guid)
    {
        AddressLevelType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressLevelType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AddressLevel entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AddressLevelType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressLevelType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAddressLevelID(Integer.toString(entity.getCode()));
        datagramObject.setAddressLevelName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    @Override
    public ChangedWrapper<AddressLevel> processDatagramObject(AddressLevelType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // AddressLevel init
        String code = StringUtils.trimToNull(datagramObject.getAddressLevelID());
        Integer codeInt = null;
        if (code != null)
        {
            try
            {
                codeInt = Integer.parseInt(code);
            }
            catch (NumberFormatException e)
            {
                throw new ProcessingDatagramObjectException("AddressLevelID is not number: " + code + '!');
            }
        }
        String title = StringUtils.trimToNull(datagramObject.getAddressLevelName());

        boolean isNew = false;
        boolean isChanged = false;
        AddressLevel entity = null;

        CoreCollectionUtils.Pair<AddressLevel, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findAddressLevel(codeInt, title);
        if (entity == null)
        {
            entity = new AddressLevel();
            isNew = true;
            isChanged = true;
        }

        // AddressLevel set
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(AddressLevel.class, AddressLevel.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("AddressLevelType title is not unique");
            }
        }
        /*if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("AddressLevelType object without title");
        }*/

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            if (codeInt == null)
                throw new ProcessingDatagramObjectException("AddressLevelType object without AddressLevelID!");
            entity.setCode(codeInt);
            if (!DataAccessServices.dao().isUnique(AddressLevel.class, entity, AddressLevel.P_CODE))
                throw new ProcessingDatagramObjectException("AddressLevelID is not unique!");
        }
        else if (codeInt != null && codeInt != entity.getCode())
            warning.append("[AddressLevelID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
