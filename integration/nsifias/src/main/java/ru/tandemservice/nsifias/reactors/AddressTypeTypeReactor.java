/* $Id$ */
package ru.tandemservice.nsifias.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.fias.base.entity.AddressLevel;
import org.tandemframework.shared.fias.base.entity.AddressType;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AddressLevelType;
import ru.tandemservice.nsiclient.datagram.AddressTypeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 28.08.2015
 */
public class AddressTypeTypeReactor extends BaseEntityReactor<AddressType, AddressTypeType>
{
    private static final String ADDRESS_LEVEL = "addressLevelID";

    @Override
    public Class<AddressType> getEntityClass()
    {
        return AddressType.class;
    }

    @Override
    public void collectUnknownDatagrams(AddressTypeType datagramObject, IUnknownDatagramsCollector collector)
    {
        AddressTypeType.AddressLevelID addressLevelID = datagramObject.getAddressLevelID();
        AddressLevelType addressLevelType = addressLevelID == null ? null : addressLevelID.getAddressLevel();
        if (addressLevelType != null) collector.check(addressLevelType.getID(), AddressLevelType.class);
    }

    protected AddressType findAddressType(Integer code, AddressLevel addressLevel, String fullName, String shortName)
    {
        if (code != null)
        {
            List<AddressType> list = findEntity(eq(property(ENTITY_ALIAS, AddressType.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }

        if (addressLevel != null && fullName != null)
        {
            List<AddressType> list = findEntity(and(eq(property(ENTITY_ALIAS, AddressType.addressLevel()), value(addressLevel)),
                                                    eq(property(ENTITY_ALIAS, AddressType.title()), value(fullName))));
            if (list.size() == 1) return list.get(0);
        }

        if (addressLevel != null && shortName != null)
        {
            List<AddressType> list = findEntity(and(eq(property(ENTITY_ALIAS, AddressType.addressLevel()), value(addressLevel)),
                                                    eq(property(ENTITY_ALIAS, AddressType.shortTitle()), value(shortName))));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public AddressTypeType createDatagramObject(String guid)
    {
        AddressTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressTypeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AddressType entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AddressTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressTypeType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAddressTypeCountryside(NsiUtils.formatBoolean(entity.isCountryside()));
        datagramObject.setAddressTypeName(entity.getTitle());
        datagramObject.setAddressTypeNameShort(entity.getShortTitle());
        datagramObject.setAddressTypeStatus(Integer.toString(entity.getStatus()));
        datagramObject.setAddressTypeID(Integer.toString(entity.getCode()));

        ProcessedDatagramObject<AddressLevelType> addressLevelType = createDatagramObject(AddressLevelType.class, entity.getAddressLevel(), commonCache, warning);
        AddressTypeType.AddressLevelID addressLevelID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAddressTypeTypeAddressLevelID();
        addressLevelID.setAddressLevel(addressLevelType.getObject());
        datagramObject.setAddressLevelID(addressLevelID);
        if (addressLevelType.getList() != null)
            result.addAll(addressLevelType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<AddressType> processDatagramObject(AddressTypeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {

        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();


        //AddressType init
        AddressTypeType.AddressLevelID addressLevelID = datagramObject.getAddressLevelID();
        AddressLevelType addressLevelType = addressLevelID == null ? null : addressLevelID.getAddressLevel();
        AddressLevel addressLevel = getOrCreate(AddressLevelType.class, params, commonCache, addressLevelType, ADDRESS_LEVEL, null, warning);

        String code = StringUtils.trimToNull(datagramObject.getAddressTypeID());
        Integer codeInt = null;
        if (code != null)
        {
            try
            {
                codeInt = Integer.parseInt(code);
            }
            catch (NumberFormatException e)
            {
                throw new ProcessingDatagramObjectException("AddressTypeID is not number: " + code + '!');
            }
        }
        Boolean countrySide = NsiUtils.parseBoolean(datagramObject.getAddressTypeCountryside());
        String fullName = StringUtils.trimToNull(datagramObject.getAddressTypeName());
        String shortName = StringUtils.trimToNull(datagramObject.getAddressTypeNameShort());
        Integer status = Integer.parseInt(StringUtils.trimToNull(datagramObject.getAddressTypeStatus()));

        boolean isNew = false;
        boolean isChanged = false;
        AddressType entity = null;

        CoreCollectionUtils.Pair<AddressType, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findAddressType(codeInt, addressLevel, fullName, shortName);
        if (entity == null)
        {
            entity = new AddressType();
            isNew = true;
            isChanged = true;
        }

        // AddressType set
        if (countrySide != null && !countrySide.equals(entity.isCountryside()))
        {
            isChanged = true;
            entity.setCountryside(countrySide);
        }
        if (fullName != null && !fullName.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(fullName);
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("AddressTypeType object without fullName!");
        }
        if (shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("AddressTypeType object without shortName!");
        }

        if (status != entity.getStatus())
        {
            isChanged = true;
            entity.setStatus(status);
        }

        if (addressLevel != null && !addressLevel.equals(entity.getAddressLevel()))
        {
            isChanged = true;
            entity.setAddressLevel(addressLevel);
        }

        if (entity.getAddressLevel() == null)
        {
            throw new ProcessingDatagramObjectException("AddressTypeType object without AddressLevelID!");
        }

        if (!NsiSyncManager.instance().reactorDAO().isPropertiesUnique(AddressType.class, entity,  AddressType.L_ADDRESS_LEVEL, AddressType.P_TITLE))
            throw new ProcessingDatagramObjectException("AddressTypeType addressLevel/fullName combination is not unique! ");
        if (!NsiSyncManager.instance().reactorDAO().isPropertiesUnique(AddressType.class, entity,  AddressType.L_ADDRESS_LEVEL, AddressType.P_SHORT_TITLE))
            throw new ProcessingDatagramObjectException("AddressTypeType addressLevel/shortName combination is not unique! ");

        if (isNew)
        {
            if (codeInt == null)
                throw new ProcessingDatagramObjectException("AddressTypeType object without AddressTypeID!");
            entity.setCode(codeInt);
            if (!DataAccessServices.dao().isUnique(AddressType.class, entity, AddressType.P_CODE))
                throw new ProcessingDatagramObjectException("AddressTypeID is not unique!");

        }
        else if (codeInt != null && codeInt != entity.getCode())
            warning.append("[AddressTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<AddressType> w, AddressTypeType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(AddressLevelType.class, session, params, ADDRESS_LEVEL, nsiPackage);
        }
        super.save(session, w, datagramObject, nsiPackage);
    }
}
