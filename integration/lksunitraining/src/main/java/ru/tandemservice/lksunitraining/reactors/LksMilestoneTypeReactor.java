/* $Id:$ */
package ru.tandemservice.lksunitraining.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.lksunitraining.entity.LksMilestone;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksMilestoneType;
import ru.tandemservice.nsiclient.datagram.LksSessionResultType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 17.08.2016
 */
public class LksMilestoneTypeReactor extends BaseEntityReactor<LksMilestone, LksMilestoneType>
{
    private static final String LKS_SESSION_RESULT = "LksSessionResultID";

    @Override
    public Class<LksMilestone> getEntityClass()
    {
        return LksMilestone.class;
    }

    @Override
    public void collectUnknownDatagrams(LksMilestoneType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksMilestoneType.LksSessionResultID sessionResultID = datagramObject.getLksSessionResultID();
        LksSessionResultType sessionResultType = sessionResultID == null ? null : sessionResultID.getLksSessionResult();
        if (sessionResultType != null) collector.check(sessionResultType.getID(), LksSessionResultType.class);
    }

    @Override
    public LksMilestoneType createDatagramObject(String guid)
    {
        LksMilestoneType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksMilestoneType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksMilestone entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksMilestoneType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksMilestoneType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksMilestoneID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksMilestoneName(entity.getTitle());
        datagramObject.setLksMilestoneValue(entity.getValue());


        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<LksSessionResultType> sessionResultType = createDatagramObject(LksSessionResultType.class, entity.getResult(), commonCache, warning);
        LksMilestoneType.LksSessionResultID sessionResultID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksMilestoneTypeLksSessionResultID();
        sessionResultID.setLksSessionResult(sessionResultType.getObject());
        datagramObject.setLksSessionResultID(sessionResultID);
        if (sessionResultType.getList() != null) result.addAll(sessionResultType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksMilestone findLksMilestone(String code)
    {
        if (code != null)
        {
            Long entityId = null;
            try
            {
                entityId = Long.parseLong(code);
            } catch (NumberFormatException ex)
            {
                // Не судьба
            }
            List<LksMilestone> list = findEntity(eq(property(ENTITY_ALIAS, LksMilestone.entityId()), value(entityId)));
            if (list.size() == 1) return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<LksMilestone> processDatagramObject(LksMilestoneType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksMilestoneID init
        String code = StringUtils.trimToNull(datagramObject.getLksMilestoneID());
        // LksMilestoneName init
        String name = StringUtils.trimToNull(datagramObject.getLksMilestoneName());
        // LksMilestoneValue init
        String value = StringUtils.trimToNull(datagramObject.getLksMilestoneValue());

        // LksSessionResultID init
        LksMilestoneType.LksSessionResultID sessionResultID = datagramObject.getLksSessionResultID();
        LksSessionResultType sessionResultType = sessionResultID == null ? null : sessionResultID.getLksSessionResult();
        LksSessionResult sessionResult = getOrCreate(LksSessionResultType.class, params, commonCache, sessionResultType, LKS_SESSION_RESULT, null, warning);


        boolean isNew = false;
        boolean isChanged = false;
        LksMilestone entity = null;
        CoreCollectionUtils.Pair<LksMilestone, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksMilestone(code);
        if (entity == null)
        {
            entity = new LksMilestone();
        }

        // LksMilestoneName set
        if (null != name && (null == entity.getTitle() || !name.equals(entity.getTitle())))
        {
            isChanged = true;
            entity.setTitle(name);
        }
        // LksMilestoneValue set
        if (null != value && (null == entity.getValue() || !value.equals(entity.getValue())))
        {
            isChanged = true;
            entity.setValue(value);
        }

        // LksSessionResultID set
        if (null != sessionResult && (null == entity.getResult() || !sessionResult.equals(entity.getResult())))
        {
            isChanged = true;
            entity.setResult(sessionResult);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksMilestone> w, LksMilestoneType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(LksSessionResultType.class, session, params, LKS_SESSION_RESULT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}