/* $Id:$ */
package ru.tandemservice.lksunitraining.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.LksTrainingSystemManager;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.DataDownloader;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.DownloadingMethod;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Dmitry Seleznev
 * @since 08.11.2016
 */
public class LksUnitrainingSystemActionPubAddon extends UIAddon
{
    public LksUnitrainingSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickDeleteLksMarks()
    {
        LksTrainingSystemManager.instance().dao().deleteLksMarks();
    }

    public void onClickDownloadMarksAndEduPlanData()
    {
        downloadAnything("Выгрузка данных об оценках и строках УП завершена",
                DataDownloader.INSTANCE::downloadMarksAndEduPlanData);
    }

    public void onClickDownloadPortfolioData()
    {
        downloadAnything("Выгрузка данных о портфолио студентов завершена",
                state -> DataDownloader.INSTANCE.downloadPertfolioData(state));
    }

    public void onClickDownloadEduPlanData()
    {
        downloadAnything("Выгрузка данных об учебных планах завершена",
                DataDownloader.INSTANCE::downloadEduPlanData);
    }

    public void onClickDownloadEduPlanBlockData()
    {
        downloadAnything("Выгрузка данных о блоках учебных планов завершена",
                DataDownloader.INSTANCE::downloadEduPlanBlockData);
    }

    public void onClickDownloadLksSubjectData()
    {
        downloadAnything("Выгрузка данных о дисциплинах реестра завершена",
                DataDownloader.INSTANCE::downloadLksSubjectData);
    }

    public void onClickDownloadLksSubjectPartData()
    {
        downloadAnything("Выгрузка данных о частях дисциплин реестра завершена",
                DataDownloader.INSTANCE::downloadLksSubjectPartData);
    }

    public void onClickDownloadStudent2EduPlanBlockData()
    {
        downloadAnything("Выгрузка данных об учебных планах студентов завершена",
                DataDownloader.INSTANCE::downloadStudent2EduPlanBlockData);
    }

    public void onClickDownloadEppRegistryElement()
    {
        downloadAnything("Выгрузка данных об элементах реестра завершена",
                DataDownloader.INSTANCE::downloadEppRegistryElementData);
    }

    public void onClickDownloadLksStudent2SubjectPartData()
    {
        downloadAnything("Выгрузка данных о связи студента с частью дисциплины реестра",
                DataDownloader.INSTANCE::downloadLksStudent2SubjectPartData);
    }

    public void onClickDownloadPortfolioFilesData()
    {
        downloadAnything("Выгрузка вложений в портфолио студентов завершена",
                state -> DataDownloader.INSTANCE.downloadPertfolioFileData(state));
    }

    public void onClickDownloadWorkProgramFilesData()
    {
        downloadAnything("Выгрузка вложений в РПД завершена",
                DataDownloader.INSTANCE::downloadWorkProgramFileData);
    }

    public void onClickDownloadEduPlanFilesData()
    {
        downloadAnything("Выгрузка файлов учебных планов завершена",
                DataDownloader.INSTANCE::downloadEduPlanFileData);
    }


    public void downloadAnything(String operationDescription, DownloadingMethod method)
    {
        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                StringBuilder message = new StringBuilder();

                try
                {
                    message.append(operationDescription);
                    method.downloadData(state);
                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    message.append("Ошибка выгрузки: " + e.getMessage());
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    DataDownloader.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = DataDownloader.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Выгрузка данных для ЛКС", process, ProcessDisplayMode.custom);
        DataDownloader.INSTANCE.runProcess(thread);
    }

    public void onClickDownloadPreparedData()
    {
        if (null == DataDownloader.INSTANCE.getResult())
            throw new ApplicationException("В памяти нет подготовленного файла выгрузки.");

        String fileName = DataDownloader.INSTANCE.getFileName();
        if (null == fileName) fileName = "Downloaded.zip";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().zip().fileName(fileName).document(DataDownloader.INSTANCE.getResult()), true);
    }
}