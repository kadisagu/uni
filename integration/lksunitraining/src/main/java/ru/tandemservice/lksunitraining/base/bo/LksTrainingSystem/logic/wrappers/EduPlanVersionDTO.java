/* $Id$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import java.util.Date;

/**
 * @author Stanislav Shibarshin
 * @since 04.02.2017
 */
public class EduPlanVersionDTO
{

    private Long _id;
    private String _guid;
    private String _number;
    private Date _confirmDate;
    private String _gosTitle;
    private String _title;
    private String _baseLevelTitle;
    private String _programSubjectTitle;
    private String _developConditionTitle;
    private String _programFormTitle;
    private String _standardProgramDurationTitle;
    private String _developPeriodTitle;
    private String _programQualificationTitle;
    private String _yearsStr;

    public EduPlanVersionDTO(Object[] item)
    {
        EppEduPlanVersion version = (EppEduPlanVersion)item[1];
        EppStateEduStandard stateEduStandard = (EppStateEduStandard)item[2];
        _guid = (String) item[0];
        if (stateEduStandard != null)
            _gosTitle = stateEduStandard.getTitle();
        _id = version.getId();
        _number = version.getNumber();
        _confirmDate = version.getConfirmDate();
        _title = version.getFullTitle();
        _developConditionTitle = version.getEduPlan().getDevelopCondition().getTitle();
        _programFormTitle = version.getEduPlan().getProgramForm().getTitle();
        _developPeriodTitle = version.getEduPlan().getDevelopGrid().getTitle();
        _yearsStr = version.getEduPlan().getYearsString();

        if (version.getEduPlan() instanceof EppEduPlanProf)
        {
            EppEduPlanProf eduPlan = (EppEduPlanProf) version.getEduPlan();
            _baseLevelTitle = eduPlan.getBaseLevel().getTitle();
            _programSubjectTitle = eduPlan.getProgramSubject().getTitleWithCode();
            _standardProgramDurationTitle = eduPlan.getStandardProgramDurationStr();
            _programQualificationTitle = eduPlan.getProgramQualification().getTitle();

        }

    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _number);
        LksWrapUtil.appendString(result, NsiUtils.formatDate(_confirmDate));
        LksWrapUtil.appendString(result, _gosTitle);
        LksWrapUtil.appendString(result, _title);
        LksWrapUtil.appendString(result, _baseLevelTitle);
        LksWrapUtil.appendString(result, _programSubjectTitle);
        LksWrapUtil.appendString(result, _developConditionTitle);
        LksWrapUtil.appendString(result, _programFormTitle);
        LksWrapUtil.appendString(result, _standardProgramDurationTitle);
        LksWrapUtil.appendString(result, _developPeriodTitle);
        LksWrapUtil.appendString(result, _programQualificationTitle);
        LksWrapUtil.appendString(result, _yearsStr);
        return result.toString();
    }

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        this._number = number;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        this._guid = guid;
    }

    public String getGosTitle()
    {
        return _gosTitle;
    }

    public void setGosTitle(String gosTitle)
    {
        this._gosTitle = gosTitle;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        this._title = title;
    }

    public String getBaseLevelTitle()
    {
        return _baseLevelTitle;
    }

    public void setBaseLevelTitle(String baseLevelTitle)
    {
        this._baseLevelTitle = baseLevelTitle;
    }

    public String getProgramSubjectTitle()
    {
        return _programSubjectTitle;
    }

    public void setProgramSubjectTitle(String programSubjectTitle)
    {
        this._programSubjectTitle = programSubjectTitle;
    }

    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    public void setDevelopConditionTitle(String developConditionTitle)
    {
        this._developConditionTitle = developConditionTitle;
    }

    public String getProgramFormTitle()
    {
        return _programFormTitle;
    }

    public void setProgramFormTitle(String programFormTitle)
    {
        this._programFormTitle = programFormTitle;
    }

    public String getStandardProgramDurationTitle()
    {
        return _standardProgramDurationTitle;
    }

    public void setStandardProgramDurationTitle(String standardProgramDurationTitle)
    {
        this._standardProgramDurationTitle = standardProgramDurationTitle;
    }

    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        this._developPeriodTitle = developPeriodTitle;
    }

    public String getProgramQualificationTitle()
    {
        return _programQualificationTitle;
    }

    public void setProgramQualificationTitle(String programQualificationTitle)
    {
        this._programQualificationTitle = programQualificationTitle;
    }

    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    public void setConfirmDate(Date confirmDate)
    {
        this._confirmDate = confirmDate;
    }

    public String getYearsStr()
    {
        return _yearsStr;
    }

    public void setYearsStr(String yearsStr)
    {
        this._yearsStr = yearsStr;
    }
}
