package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

/**
 * @author Stanislav Shibarshin
 * @since 04.02.2017
 */
public class EduPlanBlockDTO
{

    private Long _id;
    private String _guid;
    private String _common;
    private String _profileTitle;
    private String _eduPlanGuid;


    public EduPlanBlockDTO(Object[] item)
    {
        _guid = (String) item[0];
        EppEduPlanVersionRootBlock rootBlock = (EppEduPlanVersionRootBlock)item[1];
        EppEduPlanVersionSpecializationBlock specializationBlock = (EppEduPlanVersionSpecializationBlock) item[2];
        if (rootBlock != null)
        {
            _common = NsiUtils.formatBoolean(Boolean.TRUE);
            _id = rootBlock.getId();
        } else if (specializationBlock != null)
        {
            _profileTitle = (specializationBlock.getTitle());
            _id = specializationBlock.getId();
        }
        _eduPlanGuid = (String)item[3];
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _common);
        LksWrapUtil.appendString(result, _profileTitle);
        LksWrapUtil.appendString(result, _eduPlanGuid);
        return result.toString();
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        this._guid = guid;
    }

    public String getCommon()
    {
        return _common;
    }

    public void setCommon(String common)
    {
        this._common = common;
    }

    public String getProfileTitle()
    {
        return _profileTitle;
    }

    public void setProfileTitle(String profileTitle)
    {
        this._profileTitle = profileTitle;
    }

    public String getEduPlanGuid()
    {
        return _eduPlanGuid;
    }

    public void setEduPlanGuid(String eduPlanGuid)
    {
        this._eduPlanGuid = eduPlanGuid;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }
}
