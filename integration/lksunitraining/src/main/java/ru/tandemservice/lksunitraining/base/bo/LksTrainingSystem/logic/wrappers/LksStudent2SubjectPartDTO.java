/* $Id$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Igor Belanov
 * @since 05.02.2017
 */
public class LksStudent2SubjectPartDTO
{
    private Long _id;
    private String _guid;
    private String _number;
    private String _studentGuid;
    private String _subjectPartGuid;
    private String _eduYearGuid;

    public LksStudent2SubjectPartDTO(Object[] item)
    {
        _id = (Long)item[1];
        _guid = (String)item[0];
        _number = (String)item[2];
        _studentGuid = (String)item[3];
        _subjectPartGuid = (String)item[4];
        _eduYearGuid = (String)item[5];
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _number);
        LksWrapUtil.appendString(result, _studentGuid);
        LksWrapUtil.appendString(result, _subjectPartGuid);
        LksWrapUtil.appendString(result, _eduYearGuid);
        return result.toString();
    }

    // getters & setters
    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }

    public String getStudentGuid()
    {
        return _studentGuid;
    }

    public void setStudentGuid(String studentGuid)
    {
        _studentGuid = studentGuid;
    }

    public String getSubjectPartGuid()
    {
        return _subjectPartGuid;
    }

    public void setSubjectPartGuid(String subjectPartGuid)
    {
        _subjectPartGuid = subjectPartGuid;
    }

    public String getEduYearGuid()
    {
        return _eduYearGuid;
    }

    public void setEduYearGuid(String eduYearGuid)
    {
        _eduYearGuid = eduYearGuid;
    }
}
