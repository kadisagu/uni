package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Stanislav Shibarshin
 * @since 04.02.2017
 */
public class LksSubjectPartDTO
{

    private Long _id;
    private String _guid;
    private String _subjectGuid;
    private String _num;
    private String _seminars;
    private String _labs;
    private String _practices;
    private String _lectures;
    private String _credit;
    private String _diffCredit;
    private String _exam;
    private String _examAccum;
    private String _courseWork;
    private String _courseProject;
    private String _controlWork;

    public LksSubjectPartDTO(Object[] item)
    {
        _id = (Long)item[2];
        _guid = (String)item[0];
        _subjectGuid = (String)item[1];
        _num = (String)item[3];
        _seminars = (String)item[4];
        _labs = (String)item[5];
        _practices = (String)item[6];
        _lectures = (String)item[7];
        _credit = (String)item[8];
        _diffCredit = (String)item[9];
        _exam = (String)item[10];
        _examAccum = (String)item[11];
        _courseWork = (String)item[12];
        _courseProject = (String)item[13];
        _controlWork = (String)item[14];
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _num);
        LksWrapUtil.appendString(result, _seminars);
        LksWrapUtil.appendString(result, _labs);
        LksWrapUtil.appendString(result, _practices);
        LksWrapUtil.appendString(result, _lectures);
        LksWrapUtil.appendString(result, _credit);
        LksWrapUtil.appendString(result, _diffCredit);
        LksWrapUtil.appendString(result, _exam);
        LksWrapUtil.appendString(result, _examAccum);
        LksWrapUtil.appendString(result, _courseWork);
        LksWrapUtil.appendString(result, _courseProject);
        LksWrapUtil.appendString(result, _controlWork);
        LksWrapUtil.appendString(result, _subjectGuid);
        return result.toString();
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        this._guid = guid;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public String getSubjectGuid()
    {

        return _subjectGuid;
    }

    public void setSubjectGuid(String subjectGuid)
    {
        this._subjectGuid = subjectGuid;
    }

    public String getNum()
    {
        return _num;
    }

    public void setNum(String num)
    {
        this._num = num;
    }

    public String getSeminars()
    {
        return _seminars;
    }

    public void setSeminars(String seminars)
    {
        this._seminars = seminars;
    }

    public String getLabs()
    {
        return _labs;
    }

    public void setLabs(String labs)
    {
        this._labs = labs;
    }

    public String getPractices()
    {
        return _practices;
    }

    public void setPractices(String practices)
    {
        this._practices = practices;
    }

    public String getLectures()
    {
        return _lectures;
    }

    public void setLectures(String lectures)
    {
        this._lectures = lectures;
    }

    public String getCredit()
    {
        return _credit;
    }

    public void setCredit(String credit)
    {
        this._credit = credit;
    }

    public String getDiffCredit()
    {
        return _diffCredit;
    }

    public void setDiffCredit(String diffCredit)
    {
        this._diffCredit = diffCredit;
    }

    public String getExam()
    {
        return _exam;
    }

    public void setExam(String exam)
    {
        this._exam = exam;
    }

    public String getExamAccum()
    {
        return _examAccum;
    }

    public void setExamAccum(String examAccum)
    {
        this._examAccum = examAccum;
    }

    public String getCourseWork()
    {
        return _courseWork;
    }

    public void setCourseWork(String courseWork)
    {
        this._courseWork = courseWork;
    }

    public String getCourseProject()
    {
        return _courseProject;
    }

    public void setCourseProject(String courseProject)
    {
        this._courseProject = courseProject;
    }

    public String getControlWork()
    {
        return _controlWork;
    }

    public void setControlWork(String controlWork)
    {
        this._controlWork = controlWork;
    }
}
