/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLInsertValuesBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.LksDao;
import ru.tandemservice.lksbase.entity.LksChangedEntity;
import ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksuniepp.entity.LksSubjectPart;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers.*;
import ru.tandemservice.lksunitraining.entity.LksMilestone;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import ru.tandemservice.lksunitraining.entity.LksWorkResult;
import ru.tandemservice.lksunitraining.utils.LksUnitrainingListenerEntityListProvider;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksTrainingDao extends LksDao implements ILksTrainingDao
{
    public List<Long> getEntityToUpdateIdList(Long entityId)
    {
        IEntity entity = get(entityId);
        List<Long> resultList = new ArrayList<>();
        if (entity instanceof TrEduGroupEvent)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "s")
                    .column(property(TrEduGroupEvent.group().fromAlias("s")))
                    .where(eq(property(TrEduGroupEvent.id().fromAlias("s")), value(entityId)));

            List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                    .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")))
                    .where(in(property(TrJournalGroupStudent.group().group().fromAlias("e")), subBuilder.buildQuery()))
                    .createStatement(getSession()).list();

            resultList.addAll(tjGrStudentIds);
        } else if (entity instanceof TrEduGroupEventStudent)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "s")
                    .column(property(TrEduGroupEventStudent.event().group().fromAlias("s")))
                    .where(eq(property(TrEduGroupEventStudent.id().fromAlias("s")), value(entityId)));

            List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                    .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")))
                    .where(in(property(TrJournalGroupStudent.group().group().fromAlias("e")), subBuilder.buildQuery()))
                    .createStatement(getSession()).list();

            resultList.addAll(tjGrStudentIds);
        } else if (entity instanceof TrJournal)
        {
            List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                    .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")))
                    .where(eq(property(TrJournalGroupStudent.group().journal().id().fromAlias("e")), value(entityId)))
                    .createStatement(getSession()).list();

            resultList.addAll(tjGrStudentIds);
        } else if (entity instanceof TrJournalModule)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrJournalModule.class, "s")
                    .column(property(TrJournalModule.journal().fromAlias("s")))
                    .where(eq(property(TrJournalModule.id().fromAlias("s")), value(entityId)));

            List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                    .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")))
                    .where(in(property(TrJournalGroupStudent.group().journal().fromAlias("e")), subBuilder.buildQuery()))
                    .createStatement(getSession()).list();

            resultList.addAll(tjGrStudentIds);
        } else if (entity instanceof TrJournalEvent)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "s")
                    .column(property(TrJournalEvent.journalModule().journal().fromAlias("s")))
                    .where(eq(property(TrEduGroupEventStudent.id().fromAlias("s")), value(entityId)));

            List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                    .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")))
                    .where(in(property(TrJournalGroupStudent.group().journal().fromAlias("e")), subBuilder.buildQuery()))
                    .createStatement(getSession()).list();

            resultList.addAll(tjGrStudentIds);
        } else if (entity instanceof TrJournalGroup)
        {
            List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                    .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")))
                    .where(eq(property(TrJournalGroupStudent.group().id().fromAlias("e")), value(entityId)))
                    .createStatement(getSession()).list();

            resultList.addAll(tjGrStudentIds);
        } else if (entity instanceof SessionMark)
        {
            List<Long> sessMarkIds = new DQLSelectBuilder().fromEntity(SessionMark.class, "e").distinct()
                    .joinEntity("e", DQLJoinType.inner, SessionDocumentSlot.class, "s", eq(property("e", SessionMark.slot().id()), property("s", SessionDocumentSlot.id())))
                    .column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().id().fromAlias("s")))
                    .where(eq(property(SessionMark.id().fromAlias("e")), value(entityId)))
                    .createStatement(getSession()).list();

            resultList.addAll(sessMarkIds);
        }

        return resultList;
    }

    @Override
    public void doInintFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        if (null != entityType && !LksUnitrainingListenerEntityListProvider.RESULT_MOBILE.equals(entityType))
            return;

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();

        List<Long> fullEntityIdList = new ArrayList<>();

        /*TODO не забыть раскомментарить когда закончим с МИРЭА*/
        DQLSelectBuilder journalBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
                .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e"))).where(isNull(property("s")))
                //.where(eq(property(TrJournalGroupStudent.group().journal().yearPart().year().educationYear().current().fromAlias("e")), value(Boolean.TRUE)))
                .where(eq(property(TrJournalGroupStudent.studentWpe().student().archival().fromAlias("e")), value(Boolean.FALSE)))
                .order(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")));

        if (null != studentIds && !studentIds.isEmpty())
        {
            journalBuilder.where(in(property(TrJournalGroupStudent.studentWpe().student().id().fromAlias("e")), studentIds));
        }

        fullEntityIdList.addAll(journalBuilder.createStatement(getSession()).list());

        DQLSelectBuilder sessionBuilder = new DQLSelectBuilder().fromEntity(SessionMark.class, "e")
                .joinEntity("e", DQLJoinType.inner, SessionDocumentSlot.class, "s", eq(property("e", SessionMark.slot().id()), property("s", SessionDocumentSlot.id())))
                .joinEntity("s", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "d", eq(property("s", SessionDocumentSlot.document().id()), property("d", SessionStudentGradeBookDocument.id())))
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("s"), "wpeCa")
                .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "ce", eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().id().fromAlias("s")), property(LksChangedEntity.entityId().fromAlias("ce"))))
                .column(property(EppStudentWpeCAction.studentWpe().id().fromAlias("wpeCa")))
                .where(in(property(EppStudentWpeCAction.studentWpe().student().archival().fromAlias("wpeCa")), value(Boolean.FALSE)))
                .where(eq(property(SessionDocumentSlot.inSession().fromAlias("s")), value(Boolean.FALSE)))
                .where(isNull(property(EppStudentWpeCAction.studentWpe().removalDate().fromAlias("wpeCa"))))
                .order(property(EppStudentWpeCAction.studentWpe().student().id().fromAlias("wpeCa")));

        if (null != studentIds && !studentIds.isEmpty())
        {
            sessionBuilder.where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().student().id().fromAlias("s")), studentIds));
        }

        fullEntityIdList.addAll(sessionBuilder.createStatement(getSession()).<Long>list());

        System.out.println("Подготовка данных о сессии заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        int updCnt = 0;
        Set<Long> processedIdSet = new HashSet<>();
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);

        for (Long id : fullEntityIdList)
        {
            if (!processedIdSet.contains(id))
            {
                insertBuilder.value(LksChangedEntity.entityId().s(), id);
                insertBuilder.value(LksChangedEntity.deleted().s(), Boolean.FALSE);
                insertBuilder.value(LksChangedEntity.entityType().s(), LksUnitrainingListenerEntityListProvider.RESULT_MOBILE);
                insertBuilder.addBatch();
                processedIdSet.add(id);
                updCnt++;
            }
            if (updCnt == 300)
            {
                createStatement(insertBuilder).execute();
                insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);
                updCnt = 0;
            }
        }

        if (updCnt > 0) createStatement(insertBuilder).execute();
        System.out.println("Запись строк в БД заняла " + (System.currentTimeMillis() - time) + " мс.");
        System.out.println("Всего " + (System.currentTimeMillis() - startTime) + " мс.");
    }

    @Override
    public int doSyncEntityPortion(int elementsToProcessAmount)
    {
        List<LksChangedEntity> entityPortion = getLksEntityPortion(elementsToProcessAmount, LksUnitrainingListenerEntityListProvider.RESULT_MOBILE);
        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> resultIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            entityPortion.stream().forEach(e -> {
                resultIds.add(e.getEntityId());
                changedEntityIdsList.add(e.getId());
            });

            //Подготоавливаем мап сохраненных ранее объектов
            List<LksSessionResult> savedSessResultList = new DQLSelectBuilder().fromEntity(LksSessionResult.class, "e").column(property("e"))
                    .where(in(property(LksSessionResult.entityId().fromAlias("e")), resultIds))
                    .createStatement(getSession()).list();

            Map<Long, LksSessionResult> sessResultMap = new HashMap<>();
            for (LksSessionResult sessResult : savedSessResultList)
                sessResultMap.put(sessResult.getEntityId(), sessResult);

            List<LksSubject> savedSubjList = new DQLSelectBuilder().fromEntity(LksSubject.class, "e").column(property("e"))
                    .createStatement(getSession()).list();

            Map<Long, LksSubject> subjMap = new HashMap<>();
            for (LksSubject subject : savedSubjList) subjMap.put(subject.getEntityId(), subject);


            // Получаем список всех студентов УГС, включенных в журнал (в реализацию дисциплины)
            //List<TrJournalGroupStudent> studentSlotsList = new ArrayList<>();

            List<TrJournalGroupStudent> studentSlotList = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
                    .fetchPath(DQLJoinType.inner, TrJournalGroupStudent.group().journal().fromAlias("e"))
                    .where(eq(property(TrJournalGroupStudent.studentWpe().student().archival().fromAlias("e")), value(Boolean.FALSE)))
                    .where(in(property(TrJournalGroupStudent.studentWpe().id().fromAlias("e")), resultIds))
                    .createStatement(getSession()).list();

            //TODO: убрать обнуление списка, когда закончится эпопея с МИРЭА
            //studentSlotList.clear(); //TODO delete

            Set<Long> journalIdsSet = new HashSet<>();
            for (TrJournalGroupStudent stud : studentSlotList)
                journalIdsSet.add(stud.getGroup().getJournal().getId());

            // Подготавливаем Мап событий студента. Ключ - EppStudentEpvSlot.id, событие студента
            Map<Long, List<TrEduGroupEventStudent>> studentToEventsMap = new HashMap<>();

            List<TrEduGroupEventStudent> events = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "event").column("event")
                    .where(in(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().id().fromAlias("event")), journalIdsSet))
                    .createStatement(getSession()).list();

            for (TrEduGroupEventStudent event : events)
            {
                Long studentSlotId = event.getStudentWpe().getId();
                List<TrEduGroupEventStudent> studEventsList = studentToEventsMap.get(studentSlotId);
                if (null == studEventsList) studEventsList = new ArrayList<>();
                studEventsList.add(event);
                studentToEventsMap.put(studentSlotId, studEventsList);
            }

            // Подготавливаем Мап оценок. Ключ - пара(EppStudentEpvSlot.id, TrEduGroupEvent.id), значение - пара(оценка, дата оценки)
            // Дата оценки берется из истории оценок, если дата мероприятия не указана! - из последней (по дате) записи в истории.
            Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> markActualMap = new HashMap<>();

            DQLSelectBuilder histDQL = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "m").column(property("m"), "stud_id")
                    .where(in(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().id().fromAlias("m")), journalIdsSet));

            DQLSelectColumnNumerator histDQLNumerator = new DQLSelectColumnNumerator(histDQL);

            int eventColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.event().id()));
            int slotColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.studentWpe().id()));
            int markColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.P_GRADE_AS_LONG));
            int markDateColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.event().deadlineDate()));

            Iterable<Object[]> rows = scrollRows(histDQLNumerator.getDql().createStatement(getSession()));
            for (Object[] item : rows)
            {
                Long eventId = (Long) item[eventColumnIdx];
                Long slotId = (Long) item[slotColumnIdx];
                Long markAsLong = (Long) item[markColumnIdx];
                Double mark = (markAsLong != null) ? (markAsLong * 0.01d) : null;
                Date markDate = (Date) item[markDateColumnIdx];

                CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slotId, eventId);
                CoreCollectionUtils.Pair<Double, Date> value = new CoreCollectionUtils.Pair<>(mark, markDate);

                markActualMap.put(key, value);
            }

            // Подготавливаем Мап коэффициентов БРС для КМ. Ключ - TrJournalEvent.id, значение - список коэффициентов БРС для КМ журнала
            Map<Long, List<TrBrsCoefficientValue>> brsCoeffsMap = new HashMap<>();

            List<TrBrsCoefficientValue> brsCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
                    .joinEntity("e", DQLJoinType.inner, TrJournalEvent.class, "ev", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournalEvent.id().fromAlias("ev"))))
                    .where(in(property(TrJournalEvent.journalModule().journal().id().fromAlias("ev")), journalIdsSet))
                    //.where(eq(property(TrJournalEvent.journalModule().journal().yearPart().year().educationYear().current().fromAlias("ev")), value(Boolean.TRUE)))
                    .createStatement(getSession()).list();

            for (TrBrsCoefficientValue brsCoefficientValue : brsCoeffsList)
            {
                Long ownerId = brsCoefficientValue.getOwner().getId();
                List<TrBrsCoefficientValue> ownerBrsCoeffsList = brsCoeffsMap.get(ownerId);
                if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
                ownerBrsCoeffsList.add(brsCoefficientValue);
                brsCoeffsMap.put(ownerId, ownerBrsCoeffsList);
            }

            //Подготавливаем Мап коэффициентов БРС для журнала. Ключ - TrJournal.id, значение - список коэффициентов БРС для журнала
            Map<Long, List<TrBrsCoefficientValue>> journalCoeffsMap = new HashMap<>();

            List<TrBrsCoefficientValue> journalCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
                    .joinEntity("e", DQLJoinType.inner, TrJournal.class, "j", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournal.id().fromAlias("j"))))
                    .where(in(property(TrJournal.id().fromAlias("j")), journalIdsSet))
                    //.where(eq(property(TrJournal.yearPart().year().educationYear().current().fromAlias("j")), value(Boolean.TRUE)))
                    .createStatement(getSession()).list();

            for (TrBrsCoefficientValue brsCoefficientValue : journalCoeffsList)
            {
                Long ownerId = brsCoefficientValue.getOwner().getId();
                List<TrBrsCoefficientValue> ownerBrsCoeffsList = journalCoeffsMap.get(ownerId);
                if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
                ownerBrsCoeffsList.add(brsCoefficientValue);
                journalCoeffsMap.put(ownerId, ownerBrsCoeffsList);
            }


            // Кэш для рейтингов. Ключ - TrJournal.id, значение - рейтинг студентов по журналу
            Map<Long, IBrsDao.ICurrentRatingCalc> ratingMap = new HashMap<>();

            // Кэш дополнительных атрибутов журнала. Ключ - TrJournal.id, значение - мап (ключ - код параметра) всех дополнительных атрибутов журнала
            Map<Long, Map<String, ISessionBrsDao.IRatingAdditParamDef>> additParamDefMap = new HashMap<>();

            // Мап для соотнесения результата с конкретным слотом студента в рабочем плане (нужно для вставки итоговых оценок в сессию)
            Map<String, Long> resultIdToWorkplanSlotIdMap = new HashMap<>();
            Set<Long> workPlanSlotIdsSet = new HashSet<>();

            Map<Long, LksWorkResult> workResultMap = new HashMap<>();
            for (LksWorkResult workResult : getList(LksWorkResult.class))
                workResultMap.put(workResult.getEntityId(), workResult);

            List<LksWorkResult> savedWorkResultList = getList(LksWorkResult.class, LksWorkResult.result(), savedSessResultList);

            Map<Long, LksMilestone> milestoneMap = new HashMap<>();
            for (LksMilestone milestone : getList(LksMilestone.class))
                milestoneMap.put(milestone.getEntityId(), milestone);

            List<LksMilestone> savedMilestone = getList(LksMilestone.class, LksMilestone.result(), savedSessResultList);

            Set<Long> regElIdSet = new HashSet<>();
            Map<Long, LksSessionResult> resultsMap = new HashMap<>();
            Set<LksMilestone> milestoneSet = new HashSet<>();
            Set<LksWorkResult> workResultsSet = new HashSet<>();
            Set<Long> lostLksSubjectIds = new HashSet<>();
            for (TrJournalGroupStudent journalStudent : studentSlotList)
            {
                EppStudentWorkPlanElement slot = journalStudent.getStudentWpe();
                Student student = slot.getStudent();
                TrJournal journal = journalStudent.getGroup().getJournal();
                regElIdSet.add(slot.getRegistryElementPart().getRegistryElement().getId());
                // Получаем закэшированный набор дополнительных атрибутов журнала, либо поднимаем его из базы и кэшируем
                Map<String, ISessionBrsDao.IRatingAdditParamDef> journalAdditParamDefMap = additParamDefMap.get(journal.getId());
                if (null == journalAdditParamDefMap)
                {
                    try
                    {
                        journalAdditParamDefMap = TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(journal);
                        additParamDefMap.put(journal.getId(), journalAdditParamDefMap);
                    } catch (Exception e)
                    {
                        e.printStackTrace(System.err);
                    }
                }

                LksSessionResult result = sessResultMap.get(slot.getId());
                if (null == result)
                {
                    result = new LksSessionResult();
                    result.setEntityId(slot.getId());
                }

                result.setStudent(student);
                result.setEducationYear(slot.getYear().getEducationYear());
                result.setSemesterNumber(String.valueOf(slot.getTerm().getIntValue()));
                //result.setSemester(String.valueOf(slot.getRegistryElementPart().getNumber())); //TODO реальный семестр по РП заменили на номер дисциплино-части, чтобы связать с semester

                LksSubject subject = subjMap.get(slot.getRegistryElementPart().getRegistryElement().getId());
                if (null != subject) result.setSubject(subject);
                else
                {
                    lostLksSubjectIds.add(slot.getRegistryElementPart().getRegistryElement().getId());
                    continue;
                }

                resultIdToWorkplanSlotIdMap.put(String.valueOf(result.getEntityId()), slot.getId());
                workPlanSlotIdsSet.add(slot.getId());

                //String.valueOf(slot.getYear().getEducationYear().getTitle() + " / "  + slot.getSourceRow().getWorkPlan().getTerm().getIntValue() + " - " + slot.getGridTerm().getPartNumber() + " (" + slot.getGridTerm().getPartAmount() + ")");
                if (null != journalCoeffsMap.get(journal.getId()))
                {
                    List<TrBrsCoefficientValue> coefficientValueList = journalCoeffsMap.get(journal.getId());
                    if (null != coefficientValueList)
                    {
                        for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
                        {
                            if (!"0".equals(brsCoefficientValue.getValueStr()))
                            {
                                Long milestoneId = brsCoefficientValue.getId() | journalStudent.getId();
                                LksMilestone milestone = milestoneMap.get(milestoneId);
                                if (null == milestone)
                                {
                                    milestone = new LksMilestone();
                                    milestone.setEntityId(milestoneId);
                                }

                                milestone.setResult(result);
                                milestone.setValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(brsCoefficientValue.getValueAsDouble()));

                                String brsDefTitle = brsCoefficientValue.getDefinition().getShortTitle();
                                if (brsDefTitle.startsWith("01.")) milestone.setTitle("3");
                                else if (brsDefTitle.startsWith("02.")) milestone.setTitle("4");
                                else if (brsDefTitle.startsWith("03.")) milestone.setTitle("5");
                                else milestone.setTitle(brsDefTitle.replaceAll(">=", "").replaceAll("<=", "").trim());

                                milestoneSet.add(milestone);
                            }
                        }
                    }
                }


                // Получаем закэшированный рейтинг студентов по журналу, либо поднимаем его из базы и кэшируем
                IBrsDao.ICurrentRatingCalc rating = ratingMap.get(journal.getId());
                if (null == rating)
                {
                    try
                    {
                        rating = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journal);
                    } catch (Exception e)
                    {
                        e.printStackTrace(System.err);
                    }
                }

                ratingMap.put(journal.getId(), rating);

                if (null != rating)
                {
                    IBrsDao.IStudentCurrentRatingData studentRating = rating.getCurrentRating(slot);
                    ISessionBrsDao.IRatingValue ratValue = studentRating.getRatingValue();
                    if (null != ratValue)
                    {
                        result.setResult(null != ratValue.getMessage() ? ratValue.getMessage() : BrsIRatingValueFormatter.instance.format(ratValue));
                        result.setMaxResult("100");
                    }
                }


                if (studentToEventsMap.containsKey(slot.getId()))
                {
                    for (TrEduGroupEventStudent eventStud : studentToEventsMap.get(slot.getId()))
                    {
                        Long milestoneId = ((eventStud.getId() >> 20) << 20) | ((journalStudent.getStudentWpe().getId() >> 20) & 1048575);

                        LksWorkResult workResult = workResultMap.get(milestoneId);
                        if (null == workResult)
                        {
                            workResult = new LksWorkResult();
                            workResult.setEntityId(milestoneId);
                        }

                        workResult.setResult(result);
                        workResult.setTitle(eventStud.getEvent().getJournalEvent().getTitle());
                        workResult.setAbsent((eventStud.getAbsent() != null && eventStud.getAbsent()) ? "н" : "");
                        if (null != eventStud.getEvent().getScheduleEvent())
                            workResult.setDate(eventStud.getEvent().getScheduleEvent().getDurationBegin());
                        else
                            workResult.setDate(eventStud.getEvent().getDeadlineDate());

                        List<TrBrsCoefficientValue> coefficientValueList = brsCoeffsMap.get(eventStud.getEvent().getJournalEvent().getId());
                        if (null != coefficientValueList)
                        {
                            for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
                            {
                                if ("max_points".equals(brsCoefficientValue.getDefinition().getUserCode()))
                                    workResult.setMaxValue(brsCoefficientValue.getValueStr());
                            }
                        }

                        CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slot.getId(), eventStud.getEvent().getId());
                        CoreCollectionUtils.Pair<Double, Date> actualMark = markActualMap.get(key);

                        if (actualMark != null && actualMark.getX() != null)
                            workResult.setValue(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(actualMark.getX()));

                        workResultsSet.add(workResult);
                        workResultMap.put(milestoneId, workResult);
                    }
                }
                resultsMap.put(result.getEntityId(), result);
                sessResultMap.put(result.getEntityId(), result);
            }
            if (!lostLksSubjectIds.isEmpty())
            {
                LksSystemManager.instance().dao().doRegisterChangedEntities(new ArrayList<>(lostLksSubjectIds), false);
                return 0;
            }

            // Поднимаем состав экзаменационной/зачетной комиссии
            final Map<Long, List<String>> tutorMap = new HashMap<>();
            final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                    .fromEntity(SessionComissionPps.class, "rel")
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                    .joinEntity("comm", DQLJoinType.inner, SessionMark.class, "slot", eq(property("comm.id"), property(SessionMark.commission().id().fromAlias("slot"))))
                    .joinEntity("slot", DQLJoinType.inner, SessionDocumentSlot.class, "s", eq(property("slot", SessionMark.slot().id()), property("s", SessionDocumentSlot.id())))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("s"), "wpeCa")
                    .column(property(SessionMark.id().fromAlias("slot")))
                    .where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().id().fromAlias("s")), resultIds))
                    .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
            for (final Object[] row : tutorDQL.createStatement(this.getSession()).<Object[]>list())
            {
                SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add(((PpsEntry) row[0]).getTitle());
            }

            // Добавляем оценки, не вошедшие в результаты журналов (оценка в сессию есть, а журнала нет)
            List<Object[]> markResults = new DQLSelectBuilder().fromEntity(SessionMark.class, "m")
                    .joinEntity("m", DQLJoinType.inner, SessionDocumentSlot.class, "s", eq(property("m", SessionMark.slot().id()), property("s", SessionDocumentSlot.id())))
                    .joinEntity("s", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "d", eq(property("s", SessionDocumentSlot.document().id()), property("d", SessionStudentGradeBookDocument.id())))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("s"), "wpeCa")
                    .joinEntity("wpeCa", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpeCa", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
                    .column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().id())) // Видимо, самое разумное по МСРП завязываться между сессией и журналами
                    .column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().id()))
                    .column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().student()))
                    .column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().registryElementPart().registryElement().id()))
                    .column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear()))
                    .column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().term().intValue()))
                    //.column(property(EppStudentWpeCAction.studentWpe().registryElementPart().number().fromAlias("wpeCa"))) //TODO реальный семестр по РП заменили на номер дисциплино-части, чтобы связать с semester
                    .column(property("ca", EppFControlActionType.code()))
                    .column(property("m", SessionMark.cachedMarkValue().title()))
                    .column(property("m", SessionMark.performDate()))
                    .column(property("m", SessionMark.id()))
                    .where(eq(property(SessionDocumentSlot.inSession().fromAlias("s")), value(Boolean.FALSE)))
                    .where(in(property(SessionDocumentSlot.studentWpeCAction().studentWpe().id().fromAlias("s")), resultIds))
                    .where(isNull(property(EppStudentWpeCAction.studentWpe().removalDate().fromAlias("wpeCa"))))
                    .order(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear()))
                    .order(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().term().intValue()))
                    .order(property("m", SessionMark.performDate()))
                    .createStatement(getSession()).list();

            for (Object[] item : markResults)
            {
                Long slotId = (Long) item[1];
                if (!resultIdToWorkplanSlotIdMap.values().contains(slotId))
                {
                    LksSessionResult result = sessResultMap.get(item[0]);
                    if (null == result)
                    {
                        result = new LksSessionResult();
                        result.setEntityId((Long) item[0]);
                    }

                    result.setStudent((Student) item[2]);
                    result.setEducationYear((EducationYear) item[4]);
                    result.setSemesterNumber(String.valueOf(item[5]));

                    LksSubject subject = subjMap.get(item[3]);
                    if (null != subject) result.setSubject(subject);
                    else
                    {
                        lostLksSubjectIds.add((Long) item[3]);
                        continue;
                    }

                    result.setDate((Date) item[8]);

                    Long markId = (Long) item[9];
                    if (null != markId && null != tutorMap.get(markId))
                    {
                        String commission = StringUtils.join(tutorMap.get(markId), ", ");
                        result.setCommission(commission.length() > 255 ? commission.substring(0, 254) : commission);
                    }

                    String actionCode = (String) item[6];

                    if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF.equals(actionCode))
                        result.setCredit((String) item[7]);
                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF.equals(actionCode))
                        result.setDiffCredit((String) item[7]);
                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM.equals(actionCode))
                        result.setExam((String) item[7]);
                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM.equals(actionCode))
                        result.setExamAccum((String) item[7]);
                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT.equals(actionCode))
                        result.setCourseProject((String) item[7]);
                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK.equals(actionCode))
                        result.setCourseWork((String) item[7]);
                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_CONTROL_WORK.equals(actionCode))
                        result.setControlWork((String) item[7]);

                    resultsMap.put(result.getEntityId(), result);
                    sessResultMap.put((Long) item[0], result);
                }
            }
            if (!lostLksSubjectIds.isEmpty())
            {
                LksSystemManager.instance().dao().doRegisterChangedEntities(new ArrayList<>(lostLksSubjectIds), false);
                return 0;
            }


            for (LksSessionResult result : resultsMap.values()) saveOrUpdate(result);
//            for (LksMilestone milestone : milestoneList) saveOrUpdate(milestone);
//            for (LksWorkResult workResult : workResultsList) saveOrUpdate(workResult);

            new MergeAction.SessionMergeAction<Long, LksMilestone>()
            {
                @Override
                protected Long key(final LksMilestone source)
                {
                    return source.getEntityId();
                }

                @Override
                protected void fill(final LksMilestone target, final LksMilestone source)
                {
                    target.update(source, false);
                }

                @Override
                protected LksMilestone buildRow(final LksMilestone source)
                {
                    return source;
                }
            }.merge(savedMilestone, milestoneSet);

            new MergeAction.SessionMergeAction<Long, LksWorkResult>()
            {
                @Override
                protected Long key(final LksWorkResult source)
                {
                    return source.getEntityId();
                }

                @Override
                protected void fill(final LksWorkResult target, final LksWorkResult source)
                {
                    target.update(source, false);
                }

                @Override
                protected LksWorkResult buildRow(final LksWorkResult source)
                {
                    return source;
                }
            }.merge(savedWorkResultList, workResultsSet);

            new MergeAction.SessionMergeAction<Long, LksSessionResult>()
            {
                @Override
                protected Long key(final LksSessionResult source)
                {
                    return source.getEntityId();
                }

                @Override
                protected void fill(final LksSessionResult target, final LksSessionResult source)
                {
                    target.update(source, false);
                }

                @Override
                protected LksSessionResult buildRow(final LksSessionResult source)
                {
                    return source;
                }
            }.merge(savedSessResultList, resultsMap.values());

            System.out.println("!!!! Created Result:" + resultsMap.size() + ", Milestone: " + milestoneSet.size() + ", WorkResult: " + workResultsSet.size());

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return resultsMap.size() + milestoneSet.size() + workResultsSet.size();
        }

        return 0;
    }

    @Override
    public String getPortfolioElementMarks(StudentPortfolioElement portfolioElement)
    {
        if (null == portfolioElement.getWpeCAction()) return null;

        List<SessionMark> marks = new DQLSelectBuilder().fromEntity(SessionMark.class, "e").column(property("e"))
                .where(in(property("e", SessionMark.slot().studentWpeCAction().id()), portfolioElement.getWpeCAction().getId()))
                .where(eq(property("e", SessionMark.slot().studentWpeCAction().studentWpe().student()), value(portfolioElement.getStudent())))
                .createStatement(getSession()).<SessionMark>list().stream()
                .filter(mark -> (!mark.getSlot().isInSession()) && mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument)
                .collect(Collectors.toList());

        if (marks.size() > 0)
        {
            SessionMark mark = marks.get(0);
            return mark.getValueTitle();
        }
        return null;
    }

    @Override
    public void deleteLksMarks()
    {
        List<String> entityTypes = new ArrayList<>(3);
        entityTypes.add(LksWorkResult.ENTITY_CLASS);
        entityTypes.add(LksMilestone.ENTITY_CLASS);
        entityTypes.add(LksSessionResult.ENTITY_CLASS);

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e").column(property(NsiEntityLog.entity().id().fromAlias("e")))
                .where(in(property(NsiEntityLog.entity().entityType().fromAlias("e")), entityTypes));

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(NsiEntityLog.class);
        deleteBuilder.where(in(property(NsiEntityLog.entity()), subBuilder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
        getSession().flush();

        DQLDeleteBuilder deleteBuilder1 = new DQLDeleteBuilder(NsiEntity.class);
        deleteBuilder1.where(or(eq(property(NsiEntity.entityType()), value(LksWorkResult.ENTITY_CLASS)), eq(property(NsiEntity.entityType()), value(LksMilestone.ENTITY_CLASS))));
        deleteBuilder1.createStatement(getSession()).execute();
        getSession().flush();

        DQLDeleteBuilder deleteBuilder2 = new DQLDeleteBuilder(NsiEntity.class);
        deleteBuilder2.where(eq(property(NsiEntity.entityType()), value(LksSessionResult.ENTITY_CLASS)));
        deleteBuilder2.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteBuilder3 = new DQLDeleteBuilder(LksMilestone.class);
        deleteBuilder3.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteBuilder4 = new DQLDeleteBuilder(LksWorkResult.class);
        deleteBuilder4.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteBuilder5 = new DQLDeleteBuilder(LksSessionResult.class);
        deleteBuilder5.createStatement(getSession()).execute();
    }

    @Override
    public String doDownloadSessionResults(ProcessState state)
    {
        state.setMessage("Получение данных об оценках");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$LksSessionResult\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LksSessionResult.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", LksSessionResult.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "eyid", eq(property("e", LksSessionResult.educationYear().id()), property("eyid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "sid", eq(property("e", LksSessionResult.subject().id()), property("sid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "stid", eq(property("e", LksSessionResult.student().id()), property("stid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", LksSessionResult.id()))
                .column(property("e", LksSessionResult.semesterNumber()))
                .column(property("e", LksSessionResult.date()))
                .column(property("e", LksSessionResult.result()))
                .column(property("e", LksSessionResult.maxResult()))
                .column(property("e", LksSessionResult.credit()))
                .column(property("e", LksSessionResult.diffCredit()))
                .column(property("e", LksSessionResult.exam()))
                .column(property("e", LksSessionResult.examAccum()))
                .column(property("e", LksSessionResult.courseProject()))
                .column(property("e", LksSessionResult.courseWork()))
                .column(property("e", LksSessionResult.controlWork()))
                .column(property("e", LksSessionResult.commission()))
                .column(property("eyid", NsiEntity.guid()))
                .column(property("sid", NsiEntity.guid()))
                .column(property("stid", NsiEntity.guid()))
                .order(property("e", LksSessionResult.student().id()))
                .order(property("e", LksSessionResult.educationYear().id()))
                .order(property("e", LksSessionResult.subject().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        logEvent(resultBuilder, "---- Получение данных об оценках из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных об оценках");
        List<LksSessionResultDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();

        for (Object[] item : items)
        {
            LksSessionResultDTO dto = new LksSessionResultDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, LksSessionResult.ENTITY_CLASS, LksSessionResultType.class.getSimpleName());

        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        state.setMaxValue(items.size());
        int progressCounter = 0;
        for (LksSessionResultDTO dto : result)
        {
            state.setCurrentValue(++progressCounter);
            state.setMessage("Выгрузка данных об оценках: " + progressCounter + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " оценок. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка оценок для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEppEpvRows(ProcessState state)
    {
        state.setMessage("Получение данных о строках УП");
        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEpvRow\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRow.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEpvRow.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "blid", eq(property("e", EppEpvRow.owner().id()), property("blid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.left, EppEpvRegistryRow.class, "re", eq(property("e", EppEpvRow.id()), property("re", EppEpvRegistryRow.id())))
                .joinEntity("re", DQLJoinType.left, NsiEntity.class, "reid", eq(property("re", EppEpvRegistryRow.registryElement().id()), property("reid", NsiEntity.entityId())))
                .column(property("e")).column(property("eid", NsiEntity.guid())).column(property("blid", NsiEntity.guid())).column(property("reid", NsiEntity.guid()))
                .order(property("e", EppEpvRow.owner().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        logEvent(resultBuilder, "---- Получение данных о строках УП из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных о строках УП.");
        Set<Long> regElIdSet = new HashSet<>();
        Map<Long, String> rowIdToGuidMap = new HashMap<>();
        List<EppEpvRowDTO> preResult = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EppEpvRowDTO dto = new EppEpvRowDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            if (null != dto.getRegDeisciplineId()) regElIdSet.add(dto.getRegDeisciplineId());
            rowIdToGuidMap.put(dto.getId(), dto.getGuid());
            preResult.add(dto);
        }

        List<Object[]> regElItems = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e")
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "ids", eq(property("e", EppRegistryElement.id()), property("ids", NsiEntity.id())))
                .column(property("e", EppRegistryElement.id())).column(property("ids", NsiEntity.guid()))
                .where(in(property("e", EppRegistryElement.id()), regElIdSet))
                .createStatement(getSession()).list();

        Map<Long, String> regElIdToGuidMap = new HashMap<>();
        for (Object[] item : regElItems) regElIdToGuidMap.put((Long) item[0], (String) item[1]);

        // Дозаполняем ссылочные поля
        int progressCounter = 0;
        state.setMaxValue(preResult.size());
        Map<String, List<EppEpvRowDTO>> childsMap = new HashMap<>();
        for (EppEpvRowDTO dto : preResult)
        {
            if (null != dto.getParentId() && null != rowIdToGuidMap.get(dto.getParentId()))
                dto.setParentGUID(rowIdToGuidMap.get(dto.getParentId()));

            if (null != dto.getRegDeisciplineId() && null != regElIdToGuidMap.get(dto.getRegDeisciplineId()))
                dto.setRegDeisciplineGUID(regElIdToGuidMap.get(dto.getRegDeisciplineId()));

            String parentGuid = dto.getParentGUID();
            List<EppEpvRowDTO> childsList = childsMap.get(parentGuid);
            if (null == childsList) childsList = new ArrayList<>();
            childsList.add(dto);
            childsMap.put(parentGuid, childsList);
            //result.add(dto);
            state.setCurrentValue(++progressCounter);
            state.setMessage("Подготовка данных о строках УП: " + progressCounter + " из " + preResult.size());
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEpvRow.ENTITY_CLASS, EduPlanRowType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        List<EppEpvRowDTO> result = new ArrayList<>(items.size());
        fillTreeRecursive(result, childsMap, null);
        logEvent(resultBuilder, "---- Пересортировка дерева заняла " + (System.currentTimeMillis() - time) + " мс.");

        progressCounter = 0;
        state.setMaxValue(result.size());
        for (EppEpvRowDTO dto : result)
        {
            resultBuilder.append(dto.print()).append("\n");
            state.setCurrentValue(++progressCounter);
            state.setMessage("Выгрузка данных о строках УП: " + progressCounter + " из " + preResult.size());
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " строк УП. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка строк УП для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    private void fillTreeRecursive(List<EppEpvRowDTO> result, Map<String, List<EppEpvRowDTO>> childsMap, EppEpvRowDTO parent)
    {
        if (null != parent) result.add(parent);
        List<EppEpvRowDTO> childsList = childsMap.get(null == parent ? null : parent.getGuid());
        if (null != childsList)
        {
            for (EppEpvRowDTO dto : childsList) fillTreeRecursive(result, childsMap, dto);
        }
    }

    @Override
    public String doDownloadEppEpvRowLoads(ProcessState state)
    {
        state.setMessage("Получение данных о нагрузках строк УП");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEpvRowLoad\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRowLoad.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEpvRowLoad.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "rowid", eq(property("e", EppEpvRowLoad.row().id()), property("rowid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", EppEpvRowLoad.id()))
                .column(property("e", EppEpvRowLoad.loadType().code()))
                .column(property("e", EppEpvRowLoad.loadType().title()))
                .column(property("e", EppEpvRowLoad.loadType().shortTitle()))
                .column(property("e", EppEpvRowLoad.loadType().abbreviation()))
                .column(property("e", EppEpvRowLoad.hours()))
                .column(property("e", EppEpvRowLoad.hoursE()))
                .column(property("e", EppEpvRowLoad.hoursI()))
                .column(property("rowid", NsiEntity.guid()))
                .order(property("e", EppEpvRowLoad.row().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        logEvent(resultBuilder, "---- Получение данных о нагрузках строк УП из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных о нагрузках строк УП");

        List<EppEpvRowLoadDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EppEpvRowLoadDTO dto = new EppEpvRowLoadDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEpvRowLoad.ENTITY_CLASS, EduPlanRowLoadType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        state.setMaxValue(result.size());
        int progressCounter = 0;
        for (EppEpvRowLoadDTO dto : result)
        {
            state.setCurrentValue(++progressCounter);
            state.setMessage("Выгрузка данных о нагрузках строк УП: " + progressCounter + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " нагрузок строк УП. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка нагрузок строк УП для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEppEpvRowTerms(ProcessState state)
    {

        state.setMessage("Получение данных о семестрах в строках УП");
        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEpvRowTerm\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEpvRowTerm.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "rowid", eq(property("e", EppEpvRowTerm.row().id()), property("rowid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", EppEpvRowTerm.id()))
                .column(property("e", EppEpvRowTerm.term().title()))
                .column(property("e", EppEpvRowTerm.hoursTotal()))
                .column(property("e", EppEpvRowTerm.labor()))
                .column(property("e", EppEpvRowTerm.weeks()))
                .column(property("e", EppEpvRowTerm.hoursAudit()))
                .column(property("e", EppEpvRowTerm.hoursSelfwork()))
                .column(property("e", EppEpvRowTerm.hoursControl()))
                .column(property("e", EppEpvRowTerm.hoursControlE()))
                .column(property("rowid", NsiEntity.guid()))
                .order(property("e", EppEpvRowLoad.row().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        logEvent(resultBuilder, "---- Получение данных о семестрах в строках УП из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных о семестрах в строках УП к выгрузке");
        List<EppEpvRowTermDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EppEpvRowTermDTO dto = new EppEpvRowTermDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEpvRowTerm.ENTITY_CLASS, EduPlanRowTermType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        state.setMaxValue(result.size());
        int progressCounter = 0;
        for (EppEpvRowTermDTO dto : result)
        {
            state.setCurrentValue(++progressCounter);
            state.setMessage("Выгрузка данных о семестрах в строках УП: " + progressCounter + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " семестров в строках УП. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка семестров в строках УП для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEppEpvRowTermLoads(ProcessState state)
    {
        state.setMessage("Получение данных о нагрузках в семестрах строк УП");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEpvRowTermLoad\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRowTermLoad.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEpvRowTermLoad.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "rtid", eq(property("e", EppEpvRowTermLoad.rowTerm().id()), property("rtid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", EppEpvRowTermLoad.id()))
                .column(property("e", EppEpvRowTermLoad.loadType().code()))
                .column(property("e", EppEpvRowTermLoad.loadType().title()))
                .column(property("e", EppEpvRowTermLoad.loadType().shortTitle()))
                .column(property("e", EppEpvRowTermLoad.loadType().abbreviation()))
                .column(property("e", EppEpvRowTermLoad.hours()))
                .column(property("e", EppEpvRowTermLoad.hoursE()))
                .column(property("e", EppEpvRowTermLoad.hoursI()))
                .column(property("rtid", NsiEntity.guid()))
                .order(property("e", EppEpvRowTermLoad.rowTerm().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        logEvent(resultBuilder, "---- Получение данных о нагрузках в семестрах строк УП из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных о нагрузках в семестрах строк УП к выгрузке");
        List<EppEpvRowTermLoadDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EppEpvRowTermLoadDTO dto = new EppEpvRowTermLoadDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEpvRowTermLoad.ENTITY_CLASS, EduPlanRowTermLoadType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        state.setMaxValue(result.size());
        int progressCounter = 0;
        for (EppEpvRowTermLoadDTO dto : result)
        {
            state.setCurrentValue(++progressCounter);
            state.setMessage("Выгрузка данных о нагрузках в семестрах строк УП: " + progressCounter + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " нагрузок в семестрах строк УП. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка нагрузок в семестрах строк УП для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEppEpvRowTermActions(ProcessState state)
    {
        state.setMessage("Получение данных о формах итогового контроля в семестрах строк УП");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEpvRowTermAction\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRowTermAction.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEpvRowTermAction.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "rtid", eq(property("e", EppEpvRowTermAction.rowTerm().id()), property("rtid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", EppEpvRowTermAction.id()))
                .column(property("e", EppEpvRowTermAction.controlActionType().code()))
                .column(property("e", EppEpvRowTermAction.controlActionType().title()))
                .column(property("e", EppEpvRowTermAction.controlActionType().shortTitle()))
                .column(property("e", EppEpvRowTermAction.controlActionType().abbreviation()))
                .column(property("rtid", NsiEntity.guid()))
                .order(property("e", EppEpvRowTermAction.rowTerm().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        logEvent(resultBuilder, "---- Получение данных о формах итогового контроля в семестрах строк УП из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных о формах итогового контроля в семестрах строк УП к выгрузке");
        List<EppEpvRowTermActionDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EppEpvRowTermActionDTO dto = new EppEpvRowTermActionDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEpvRowTermAction.ENTITY_CLASS, EduPlanRowTermActionType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int progressCounter = 0;
        for (EppEpvRowTermActionDTO dto : result)
        {
            state.setCurrentValue(++progressCounter);
            state.setMessage("Выгрузка данных о формах итогового контроля в семестрах строк УП: " + progressCounter + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " форм итогового контроля в семестрах строк УП. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка форм итогового контроля в семестрах строк УП для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadPortfolio(ProcessState state)
    {
        state.setMessage("Получение данных о портфолио из базы");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$StudentPortfolioElement\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentPortfolioElement.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", StudentPortfolioElement.id()), property("eid", NsiEntity.entityId())))
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "stid", eq(property("e", StudentPortfolioElement.student().id()), property("stid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, StudentPortfolioElement.wpeCAction().fromAlias("e"), "wpe")
                .joinPath(DQLJoinType.left, EppStudentWpeCAction.type().fromAlias("wpe"), "wpet")
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", StudentPortfolioElement.id()))
                .column(property("e", StudentPortfolioElement.title()))
                .column(property("wpet", EppGroupType.title()))
                .column(property("e", StudentPortfolioElement.file()))
                .column(property("stid", NsiEntity.guid()))
                .column(property("e", StudentPortfolioElement.student().id()))
                .column(property("wpe", EppStudentWpeCAction.id()))
                .order(property("e", StudentPortfolioElement.student().id()));

        List<Object[]> items = builder.createStatement(getSession()).list();

        state.setMaxValue(items.size());

        Set<Long> studentIds = new HashSet<>();
        for (Object[] item : items)
        {
            studentIds.add((Long) item[6]);
        }

        state.setMessage("Получение данных об оценках в портфолио из базы");
        logEvent(resultBuilder, "---- Получение данных об элементах портфолио студентов из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        Map<CoreCollectionUtils.Pair<Long, Long>, String> marksMap = new HashMap<>();
        BatchUtils.execute(studentIds, 256, elements ->
        {
            List<SessionMark> marks = new DQLSelectBuilder().fromEntity(SessionMark.class, "e").column(property("e"))
                    .where(in(property("e", SessionMark.slot().studentWpeCAction().studentWpe().student()), studentIds))
                    .createStatement(getSession()).<SessionMark>list().stream()
                    .filter(mark -> (!mark.getSlot().isInSession()) && mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument)
                    .collect(Collectors.toList());

            for (SessionMark mark : marks)
            {
                CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(
                        mark.getSlot().getStudentWpeCAction().getStudentWpe().getStudent().getId(),
                        mark.getSlot().getStudentWpeCAction().getId());

                if (!marksMap.containsKey(key)) marksMap.put(key, mark.getValueTitle());
            }
        });

        logEvent(resultBuilder, "---- Получение данных об оценках для элементов портфолио студентов из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        state.setMessage("Подготовка данных к выгрузке");
        List<StudentPortfolioDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            StudentPortfolioDTO dto = new StudentPortfolioDTO(item);
            CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(dto.getStudentId(), dto.getControlActionId());
            if (marksMap.containsKey(key)) dto.setMarks(marksMap.get(key));

            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEpvRowTermAction.ENTITY_CLASS, EduPlanRowTermActionType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (StudentPortfolioDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " элементов портфолио студентов. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование списка форм итогового контроля в семестрах строк УП для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEduPlan(ProcessState state)
    {
        state.setMessage("Получение данных об учебных планах из базы");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEduPlanVersion\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEduPlanVersion.id()), property("eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, EppEduPlanVersion.eduPlan().parent().fromAlias("e"), "gos")
                .column(property("eid", NsiEntity.guid()))
                .column(property("e"))
                .column(property("gos"));


        List<Object[]> items = builder.createStatement(getSession()).list();

        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных об учбных планов из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<EduPlanVersionDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EduPlanVersionDTO dto = new EduPlanVersionDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEduPlanVersion.ENTITY_CLASS, EduPlanVersionType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (EduPlanVersionDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " учбеных планов. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEduPlanBlock(ProcessState state)
    {
        state.setMessage("Получение данных о блоках учебных планов из базы");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppEduPlanVersionBlock\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlock.class, "e")
                .joinEntity("e", DQLJoinType.left, EppEduPlanVersionRootBlock.class, "rb", eq(property(EppEduPlanVersionBlock.id().fromAlias("e")), property(EppEduPlanVersionRootBlock.id().fromAlias("rb"))))
                .joinEntity("e", DQLJoinType.left, EppEduPlanVersionSpecializationBlock.class, "sb", eq(property(EppEduPlanVersionBlock.id().fromAlias("e")), property(EppEduPlanVersionSpecializationBlock.id().fromAlias("sb"))))
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEduPlanVersionBlock.id()), property("eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("e"), "ver")
                .joinEntity("ver", DQLJoinType.left, NsiEntity.class, "ver_eid", eq(property("ver", EppEduPlanVersion.id()), property("ver_eid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("rb"))
                .column(property("sb"))
                .column(property("ver_eid", NsiEntity.guid()));


        List<Object[]> items = builder.createStatement(getSession()).list();

        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных о блоках учебных планов из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<EduPlanBlockDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            EduPlanBlockDTO dto = new EduPlanBlockDTO(item);

            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppEduPlanVersionBlock.ENTITY_CLASS, EduPlanBlockType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (EduPlanBlockDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " блоков учбеных планов. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadLksSubject(ProcessState state)
    {
        state.setMessage("Получение данных о дисциплинах реестра");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$LksSubject\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LksSubject.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", LksSubject.id()), property("eid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", LksSubject.id()))
                .column(property("e", LksSubject.title()))
                .column(property("e", LksSubject.shortTitle()));


        List<Object[]> items = builder.createStatement(getSession()).list();

        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных о дисциплинах реестра из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<LksSubjectDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            LksSubjectDTO dto = new LksSubjectDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, LksSubject.ENTITY_CLASS, LksSubjectType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (LksSubjectDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " дисцпилин реестра. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadLksSubjectPart(ProcessState state)
    {
        state.setMessage("Получение данных о частях дисциплин реестра");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$LksSubjectPart\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LksSubjectPart.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", LksSubjectPart.id()), property("eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, LksSubjectPart.subject().fromAlias("e"), "sub")
                .joinEntity("sub", DQLJoinType.left, NsiEntity.class, "sub_eid", eq(property("sub", LksSubject.id()), property("sub_eid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("sub_eid", NsiEntity.guid()))
                .column(property("e", LksSubjectPart.id()))
                .column(property("e", LksSubjectPart.number()))
                .column(property("e", LksSubjectPart.seminars()))
                .column(property("e", LksSubjectPart.labs()))
                .column(property("e", LksSubjectPart.practices()))
                .column(property("e", LksSubjectPart.lectures()))
                .column(property("e", LksSubjectPart.credit()))
                .column(property("e", LksSubjectPart.diffCredit()))
                .column(property("e", LksSubjectPart.exam()))
                .column(property("e", LksSubjectPart.examAccum()))
                .column(property("e", LksSubjectPart.courseWork()))
                .column(property("e", LksSubjectPart.courseProject()))
                .column(property("e", LksSubjectPart.controlWork()));


        List<Object[]> items = builder.createStatement(getSession()).list();

        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных о частях дисциплин реестра из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<LksSubjectPartDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            LksSubjectPartDTO dto = new LksSubjectPartDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, LksSubjectPart.ENTITY_CLASS, LksSubjectPartType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (LksSubjectPartDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " частей дисцпилин реестра. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadStudent2EduPlanBlock(ProcessState state)
    {
        state.setMessage("Получение данных об учебных планах студентов");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$Student2EduPlanBlock\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppStudent2EduPlanVersion.id()), property("eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, EppStudent2EduPlanVersion.student().fromAlias("e"), "st")
                .joinEntity("st", DQLJoinType.left, NsiEntity.class, "st_eid", eq(property("st", Student.id()), property("st_eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, EppStudent2EduPlanVersion.block().fromAlias("e"), "bl")
                .joinEntity("bl", DQLJoinType.left, NsiEntity.class, "bl_eid", eq(property("bl", EppEduPlanVersionBlock.id()), property("bl_eid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("st_eid", NsiEntity.guid()))
                .column(property("bl_eid", NsiEntity.guid()))
                .column(property("e", EppStudent2EduPlanVersion.id()))
                .column(property("e", EppStudent2EduPlanVersion.confirmDate()))
                .column(property("e", EppStudent2EduPlanVersion.removalDate()));


        List<Object[]> items = builder.createStatement(getSession()).list();

        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных об учебных планах студентов из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<Student2EduPlanBlockDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            Student2EduPlanBlockDTO dto = new Student2EduPlanBlockDTO(item);
            if (dto.getStudentGuid() == null) continue;
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppStudent2EduPlanVersion.ENTITY_CLASS, Student2EduPlanBlockType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (Student2EduPlanBlockDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " учебных планов студентов. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadEppRegistryElement(ProcessState state)
    {
        state.setMessage("Получение данных об элементах реестра");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$EppRegistryElement\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", EppEduPlanVersion.id()), property("eid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", EppRegistryElement.id()))
                .column(property("e", EppRegistryElement.number()))
                .column(property("e", EppRegistryElement.title()))
                .column(property("e", EppRegistryElement.workProgramFile()))
                .column(property("e", EppRegistryElement.workProgramAnnotation()));

        List<Object[]> items = builder.createStatement(getSession()).list();
        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных об элементе реестра из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<EppRegistryElementDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();

        for (Object[] item : items)
        {
            EppRegistryElementDTO dto = new EppRegistryElementDTO(item);
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, EppRegistryElement.ENTITY_CLASS, RegistryDisciplineType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (EppRegistryElementDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " элементов реестра. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование элементов реестра для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public String doDownloadLksStudent2SubjectPart(ProcessState state)
    {
        state.setMessage("Получение данных о связи студента с частью дисциплины реестра");

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        StringBuilder resultBuilder = new StringBuilder("\n$LksStudent2SubjectPart\n");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LksStudent2SubjectPart.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiEntity.class, "eid", eq(property("e", LksStudent2SubjectPart.id()), property("eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, LksStudent2SubjectPart.student().fromAlias("e"), "st")
                .joinEntity("st", DQLJoinType.left, NsiEntity.class, "st_eid", eq(property("st", Student.id()), property("st_eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, LksStudent2SubjectPart.subjectPart().fromAlias("e"), "sp")
                .joinEntity("sp", DQLJoinType.left, NsiEntity.class, "sp_eid", eq(property("sp", LksSubjectPart.id()), property("sp_eid", NsiEntity.entityId())))
                .joinPath(DQLJoinType.left, LksStudent2SubjectPart.educationYear().fromAlias("e"), "ey")
                .joinEntity("ey", DQLJoinType.left, NsiEntity.class, "ey_eid", eq(property("ey", EducationYear.id()), property("ey_eid", NsiEntity.entityId())))
                .column(property("eid", NsiEntity.guid()))
                .column(property("e", LksStudent2SubjectPart.id()))
                .column(property("e", LksStudent2SubjectPart.semesterNumber()))
                .column(property("st_eid", NsiEntity.guid()))
                .column(property("sp_eid", NsiEntity.guid()))
                .column(property("ey_eid", NsiEntity.guid()));

        List<Object[]> items = builder.createStatement(getSession()).list();
        state.setMaxValue(items.size());

        logEvent(resultBuilder, "---- Получение данных о связи студента с частью дисциплины реестра из базы заняло " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();


        state.setMessage("Подготовка данных к выгрузке");
        List<LksStudent2SubjectPartDTO> result = new ArrayList<>(items.size());
        List<CoreCollectionUtils.Pair<Long, String>> guidsToSave = new ArrayList<>();
        for (Object[] item : items)
        {
            LksStudent2SubjectPartDTO dto = new LksStudent2SubjectPartDTO(item);
            if (dto.getStudentGuid() == null) continue;
            if (null == dto.getGuid())
            {
                dto.setGuid(NsiUtils.generateGUID(dto.getId()));
                guidsToSave.add(new CoreCollectionUtils.Pair<>(dto.getId(), dto.getGuid()));
            }

            result.add(dto);
        }

        logEvent(resultBuilder, "---- Подготовка данных к выгрузке заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        saveNsiEntityGuids(guidsToSave, LksStudent2SubjectPart.ENTITY_CLASS, LksStudent2SubjectPartType.class.getSimpleName());
        logEvent(resultBuilder, "---- Сохранение GUID'ов заняло " + (System.currentTimeMillis() - time) + " мс.");

        int i = 0;
        for (LksStudent2SubjectPartDTO dto : result)
        {
            state.setCurrentValue(++i);
            state.setMessage("Выгрузка: " + i + " из " + result.size());
            resultBuilder.append(dto.print()).append("\n");
        }

        logEvent(resultBuilder, "---- Выгружено " + result.size() + " связей студента с частью дисциплины реестра. Сгенерировано " + guidsToSave.size() + " идентификаторов НСИ.");
        logEvent(resultBuilder, "---- ИТОГО. Формирование связей студента с частью дисциплины реестра для выгрузки в csv заняло " + (System.currentTimeMillis() - startTime) + " мс.");

        return resultBuilder.toString();
    }

    @Override
    public List<Object[]> getPortfoliFilesIdSet()
    {
        return new DQLSelectBuilder().fromEntity(StudentPortfolioElement.class, "e")
                .column(property("e", StudentPortfolioElement.id()))
                .column(property("e", StudentPortfolioElement.file()))
                .where(isNotNull(property("e", StudentPortfolioElement.file())))
                .order(property("e", StudentPortfolioElement.student().id()))
                .createStatement(getSession()).list();
    }

    @Override
    public List<Long> getWorkProgramFilesIdSet()
    {
        return new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e")
                .column(property("e", EppRegistryElement.workProgramFile()))
                .where(isNotNull(property("e", EppRegistryElement.workProgramFile())))
                .createStatement(getSession()).list();
    }

    @Override
    public List<Object[]> getEduPlanIdToNsiEntityGuid()
    {
        return new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "e")
                .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "ne", eq(property(NsiEntity.entityId().fromAlias("ne")), property(EppEduPlanVersion.id().fromAlias("e"))))
                .column(property("e", EppEduPlanVersion.id()))
                .column(property("ne", NsiEntity.guid()))
                .createStatement(getSession()).list();
    }


    private void logEvent(StringBuilder builder, String msg)
    {
        builder.append("#").append(msg).append("\n");
        System.out.println(msg);
    }

    private void saveNsiEntityGuids(List<CoreCollectionUtils.Pair<Long, String>> guidsToSave, String entityType, String type)
    {
        int updCnt = 0;
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(NsiEntity.class);

        for (CoreCollectionUtils.Pair<Long, String> pair : guidsToSave)
        {
            insertBuilder.value(NsiEntity.guid().s(), pair.getY());
            insertBuilder.value(NsiEntity.entityId().s(), pair.getX());
            insertBuilder.value(NsiEntity.deleted().s(), Boolean.FALSE);
            insertBuilder.value(NsiEntity.entityType().s(), entityType);
            insertBuilder.value(NsiEntity.createDate().s(), new Date());
            insertBuilder.value(NsiEntity.updateDate().s(), new Date());
            insertBuilder.value(NsiEntity.sync().s(), Boolean.TRUE);
            insertBuilder.value(NsiEntity.type().s(), type);
            insertBuilder.addBatch();
            updCnt++;

            if (updCnt == 300)
            {
                createStatement(insertBuilder).execute();
                insertBuilder = new DQLInsertValuesBuilder(NsiEntity.class);
                updCnt = 0;
            }
        }

        if (updCnt > 0) createStatement(insertBuilder).execute();
    }
}