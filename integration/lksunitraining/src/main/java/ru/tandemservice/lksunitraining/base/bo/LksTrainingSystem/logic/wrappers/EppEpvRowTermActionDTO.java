/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2016
 */
public class EppEpvRowTermActionDTO
{
    private Long _id;
    private String _guid;
    private String _controlActionTypeCode;
    private String _controlActionTypeTitle;
    private String _controlActionTypeShortTitle;
    private String _controlActionTypeAbbreviation;
    private String _rowTermGUID;

    public EppEpvRowTermActionDTO(Object[] item)
    {
        _id = (Long) item[1];
        _guid = (String) item[0];
        _controlActionTypeCode = (String) item[2];
        _controlActionTypeTitle = (String) item[3];
        _controlActionTypeShortTitle = (String) item[4];
        _controlActionTypeAbbreviation = (String) item[5];
        _rowTermGUID = (String) item[6];
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _controlActionTypeCode);
        LksWrapUtil.appendString(result, _controlActionTypeTitle);
        LksWrapUtil.appendString(result, _controlActionTypeShortTitle);
        LksWrapUtil.appendString(result, _controlActionTypeAbbreviation);
        LksWrapUtil.appendString(result, _rowTermGUID);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getControlActionTypeCode()
    {
        return _controlActionTypeCode;
    }

    public String getControlActionTypeTitle()
    {
        return _controlActionTypeTitle;
    }

    public String getControlActionTypeShortTitle()
    {
        return _controlActionTypeShortTitle;
    }

    public String getControlActionTypeAbbreviation()
    {
        return _controlActionTypeAbbreviation;
    }

    public String getRowTermGUID()
    {
        return _rowTermGUID;
    }
}