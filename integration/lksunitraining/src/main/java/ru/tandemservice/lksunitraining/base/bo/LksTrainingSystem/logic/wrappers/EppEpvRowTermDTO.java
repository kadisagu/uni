/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2016
 */
public class EppEpvRowTermDTO
{
    private Long _id;
    private String _guid;
    private String _termTitle;
    private String _hoursTotal;
    private String _labor;
    private String _weeks;
    private String _hoursAudit;
    private String _hoursSelfwork;
    private String _hoursControl;
    private String _hoursControlE;
    private String _rowGUID;

    public EppEpvRowTermDTO(Object[] item)
    {
        _id = (Long) item[1];
        _guid = (String) item[0];
        _termTitle = (String) item[2];
        _hoursTotal = String.valueOf(item[3]);
        _labor = String.valueOf(item[4]);
        _weeks = String.valueOf(item[5]);
        _hoursAudit = String.valueOf(item[6]);
        _hoursSelfwork = String.valueOf(item[7]);
        _hoursControl = String.valueOf(item[8]);
        _hoursControlE = String.valueOf(item[9]);
        _rowGUID = (String) item[10];

        if ("-1".equals(_hoursTotal)) _hoursTotal = "";
        if ("-1".equals(_labor)) _labor = "";
        if ("-1".equals(_weeks)) _weeks = "";
        if ("-1".equals(_hoursAudit)) _hoursAudit = "";
        if ("-1".equals(_hoursSelfwork)) _hoursSelfwork = "";
        if ("-1".equals(_hoursControl)) _hoursControl = "";
        if ("-1".equals(_hoursControlE)) _hoursControlE = "";
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _termTitle);
        LksWrapUtil.appendString(result, _hoursTotal);
        LksWrapUtil.appendString(result, _labor);
        LksWrapUtil.appendString(result, _weeks);
        LksWrapUtil.appendString(result, _hoursAudit);
        LksWrapUtil.appendString(result, _hoursSelfwork);
        LksWrapUtil.appendString(result, _hoursControl);
        LksWrapUtil.appendString(result, _hoursControlE);
        LksWrapUtil.appendString(result, _rowGUID);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getTermTitle()
    {
        return _termTitle;
    }

    public String getHoursTotal()
    {
        return _hoursTotal;
    }

    public String getLabor()
    {
        return _labor;
    }

    public String getWeeks()
    {
        return _weeks;
    }

    public String getHoursAudit()
    {
        return _hoursAudit;
    }

    public String getHoursSelfwork()
    {
        return _hoursSelfwork;
    }

    public String getHoursControl()
    {
        return _hoursControl;
    }

    public String getHoursControlE()
    {
        return _hoursControlE;
    }

    public String getRowGUID()
    {
        return _rowGUID;
    }
}