/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.BackgroundProcessThread;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.tapsupport.TapSupportUtils;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.LksTrainingSystemManager;
import ru.tandemservice.nsiclient.reactor.support.ProcessingException;
import ru.tandemservice.uniepp.catalog.entity.EppScriptItem;
import ru.tandemservice.uniepp.catalog.entity.codes.EppScriptItemCodes;
import ru.tandemservice.uniepp.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.StudentPortfolioManager;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Dmitry Seleznev
 * @since 04.02.2017
 */
public class DataDownloader
{
    public static final DataDownloader INSTANCE = new DataDownloader();

    private boolean _cancelled;
    private Long _initiator;
    private BackgroundProcessThread _thread;
    private String _message;
    private byte[] _result;
    private String _fileName;

    public String getMessage()
    {
        return _message;
    }

    public BackgroundProcessThread getThread()
    {
        return _thread;
    }

    public boolean cancel()
    {
        if (UserContext.getInstance().isLoggedIn() && UserContext.getInstance().getPrincipalId().equals(_initiator))
        {
            _cancelled = true;
            return true;
        }
        return false;
    }

    public void finish()
    {
        if (_thread != null)
        {
            synchronized (_thread)
            {
                _cancelled = false;
                _initiator = null;
                _thread = null;
            }
        }
    }

    private void init() throws ProcessingException
    {
        if (!UserContext.getInstance().isLoggedIn())
            throw new ProcessingException("Выгрузка невозможна.");

        _initiator = UserContext.getInstance().getPrincipalId();
        _cancelled = false;
        _message = null;
    }

    public synchronized void runProcess(BackgroundProcessThread backgroundProcessThread)
    {
        if (_thread != null)
            TapSupportUtils.addInitScript(cycle -> "alert(\"Запуск выгрузки невозможен, текущая выгрузка не завершена\")");
        else
        {
            _thread = backgroundProcessThread;
            _thread.getState().setRefreshTime(1000);
            BusinessComponentUtils.runProcess(_thread);
        }
    }

    public String downloadMarksAndEduPlanData(ProcessState state)
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadSessionResults(state));
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEppEpvRows(state));
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEppEpvRowLoads(state));
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEppEpvRowTerms(state));
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEppEpvRowTermLoads(state));
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEppEpvRowTermActions(state));

        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("EduData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

        _result = zipStream.toByteArray();
        _fileName = "EduData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных о портфолио
     */
    public String downloadPertfolioData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadPortfolio(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("PortfolioData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "PortfolioData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных об учебных планах
     */
    public String downloadEduPlanData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEduPlan(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("EduPlanData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "EduPlanData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных о блоках учебных планво
     */
    public String downloadEduPlanBlockData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEduPlanBlock(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("EduPlanBlockData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "EduPlanBlockData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных о дисциплинах реестра
     */
    public String downloadLksSubjectData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadLksSubject(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("LksSubjectData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "LksSubjectData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных о частах дисциплин реестра.
     */
    public String downloadLksSubjectPartData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadLksSubjectPart(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("LksSubjectPartData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "LksSubjectPartData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных об учебных планах студентов.
     */
    public String downloadStudent2EduPlanBlockData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadStudent2EduPlanBlock(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("Student2EduPlanBlockData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "Student2EduPlanBlockData.zip";
        return "OK";
    }

    /**
     * Выгрузка файлов, вложенных в портфолио
     */
    public String downloadPertfolioFileData(ProcessState state) throws ProcessingException
    {
        String path = ApplicationRuntime.getAppInstallPath();
        final File dir = new File(path, "data");

        if (!dir.exists())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: не найдена папка data.");
        }
        if (!dir.canRead())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: Проверьте доступ к папке data на сервере приложения.");
        }
        if (!dir.canWrite())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: Проверьте доступ к папке data на сервере приложения.");
        }

        List<Object[]> idSet = LksTrainingSystemManager.instance().dao().getPortfoliFilesIdSet();
        state.setMaxValue(idSet.size());

        int i = 0;
        for(Object[] item : idSet)
        {
            if(_cancelled)
            {
                throw new ApplicationException("Прервано пользователем. Выгружено " + i + " из " + idSet.size() + " файлов.");
            }

            Long objId = (Long) item[0];
            Long fileId = (Long) item[1];
            final RemoteDocumentDTO dto = StudentPortfolioManager.instance().dao().getAchievementAttachement(objId);
            if (null != dto)
            {
                try
                {
                    File file = new File(dir.getAbsolutePath() + "/" + fileId + "_" + dto.getFileName());
                    FileOutputStream stream = new FileOutputStream(file);
                    stream.write(dto.getContent());
                    stream.close();
                }catch (FileNotFoundException fnf)
                {
                    fnf.printStackTrace();
                }
                catch (IOException io)
                {
                    io.printStackTrace();
                }
                catch (ApplicationException ex)
                {
                }
            }

            state.setMessage("Выгружено " + ++i + " из " + idSet.size() + " файлов.");
            state.setCurrentValue(i);
        }

        return "OK";
    }

    /**
     * Выгрузка файлов РПД
     */
    public String downloadWorkProgramFileData(ProcessState state) throws ProcessingException
    {
        String path = ApplicationRuntime.getAppInstallPath();
        final File dir = new File(path, "data");

        if (!dir.exists())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: не найдена папка data.");
        }
        if (!dir.canRead())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: Проверьте доступ к папке data на сервере приложения.");
        }
        if (!dir.canWrite())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: Проверьте доступ к папке data на сервере приложения.");
        }

        List<Long> idSet = LksTrainingSystemManager.instance().dao().getWorkProgramFilesIdSet();
        state.setMaxValue(idSet.size());

        int i = 0;
        for(Long fileId : idSet)
        {
            if(_cancelled)
            {
                throw new ApplicationException("Прервано пользователем. Выгружено " + i + " из " + idSet.size() + " файлов.");
            }

            try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME))
            {
                final RemoteDocumentDTO dto = service.get(fileId);
                if (null != dto)
                {
                    File file = new File(dir.getAbsolutePath() + "/" + fileId + "_" + dto.getFileName());
                    FileOutputStream stream = new FileOutputStream(file);
                    stream.write(dto.getContent());
                    stream.close();
                }
            } catch (IOException io)
            {
                io.printStackTrace();
            }
            catch (ApplicationException ex)
            {
            }
            state.setMessage("Выгружено " + ++i + " из " + idSet.size() + " файлов.");
            state.setCurrentValue(i);
        }

        return "OK";
    }

    /**
     * Выгрузка файлов учебных планов
     */
    public String downloadEduPlanFileData(ProcessState state) throws ProcessingException
    {
        String path = ApplicationRuntime.getAppInstallPath();
        final File dir = new File(path, "data");

        if (!dir.exists())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: не найдена папка data.");
        }
        if (!dir.canRead())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: Проверьте доступ к папке data на сервере приложения.");
        }
        if (!dir.canWrite())
        {
            throw new ApplicationException("Выгрузка файлов невозможна: Проверьте доступ к папке data на сервере приложения.");
        }

        List<Object[]> idSet = LksTrainingSystemManager.instance().dao().getEduPlanIdToNsiEntityGuid();
        state.setMaxValue(idSet.size());

        int i = 0;
        for (Object[] item : idSet)
        {
            if (_cancelled)
            {
                throw new ApplicationException("Прервано пользователем. Выгружено " + i + " из " + idSet.size() + " файлов.");
            }

            Long eduPlanId = (Long) item[0];
            String guid = (String) item[1];

            try
            {
                IScriptItem scriptItem = DataAccessServices.dao().getByCode(EppScriptItem.class, EppScriptItemCodes.UNIEPP_EDU_PLAN_VERSION);
                Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                        scriptItem,
                        IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                        "epvIds", Collections.singletonList(eduPlanId)
                );

                byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
                if (content.length > 1)
                {
                    try
                    {
                        File file = new File(dir.getAbsolutePath() + "/" + guid + ".xls");
                        FileOutputStream stream = new FileOutputStream(file);
                        stream.write(content);
                        stream.close();
                    } catch (FileNotFoundException fnf)
                    {
                        fnf.printStackTrace();
                    } catch (IOException io)
                    {
                        io.printStackTrace();
                    }
                }

                state.setMessage("Выгружено " + ++i + " из " + idSet.size() + " файлов.");
                state.setCurrentValue(i);
            } catch (Exception e)
            {
            }
        }

        return "OK";
    }

    /**
     * Выгрузка данных об учебных планах
     */
    public String downloadEppRegistryElementData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadEppRegistryElement(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("EppRegistryElementData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "EppRegistryElementData.zip";
        return "OK";
    }

    /**
     * Выгрузка данных о связи студента с частью дисциплины реестра
     */
    public String downloadLksStudent2SubjectPartData(ProcessState state) throws ProcessingException
    {
        _result = null;
        StringBuilder builder = new StringBuilder();
        builder.append(LksTrainingSystemManager.instance().dao().doDownloadLksStudent2SubjectPart(state));
        ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
        BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

        ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
        zipOut.setLevel(Deflater.BEST_COMPRESSION);
        try
        {
            zipOut.putNextEntry(new ZipEntry("LksStudent2SubjectPartData.txt"));
            zipOut.write(builder.toString().getBytes());
            zipOut.closeEntry();
            zipOut.close();
            zipBuffer.close();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        _result = zipStream.toByteArray();
        _fileName = "LksStudent2SubjectPartData.zip";
        return "OK";
    }

    public byte[] getResult()
    {
        return _result;
    }

    public String getFileName()
    {
        return _fileName;
    }
}
