/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.ILksTrainingDao;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.LksTrainingDao;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
@Configuration
public class LksTrainingSystemManager extends BusinessObjectManager
{
    public static LksTrainingSystemManager instance()
    {
        return instance(LksTrainingSystemManager.class);
    }

    @Bean
    public ILksTrainingDao dao()
    {
        return new LksTrainingDao();
    }
}