/* $Id:$ */
package ru.tandemservice.lksunitraining.utils;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.lksbase.utils.ILksListenerEntityListProvider;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.LksTrainingSystemManager;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksUnitrainingListenerEntityListProvider implements ILksListenerEntityListProvider
{
    public static final String RESULT_MOBILE = "result";

    public static final List<Class> CLASSES_TO_LISTEN = new ArrayList<>();
    public static final Set<String> ORIGINAL_ENTITY_NAME_SET = new HashSet<>();
    public static final Map<String, String> ENTITY_NAME_TO_MOBILE_ENTITY_NAME = new HashMap<>();

    static
    {
        CLASSES_TO_LISTEN.add(TrEduGroupEvent.class);
        CLASSES_TO_LISTEN.add(TrEduGroupEventStudent.class);
        CLASSES_TO_LISTEN.add(TrJournal.class);
        CLASSES_TO_LISTEN.add(TrJournalModule.class);

        CLASSES_TO_LISTEN.add(TrJournalEvent.class);
        CLASSES_TO_LISTEN.add(TrEventAction.class);
        CLASSES_TO_LISTEN.add(TrEventAddon.class);
        CLASSES_TO_LISTEN.add(TrEventLoad.class);

        CLASSES_TO_LISTEN.add(TrJournalGroup.class);
        CLASSES_TO_LISTEN.add(TrJournalGroupStudent.class);

        CLASSES_TO_LISTEN.add(SessionMark.class);
        CLASSES_TO_LISTEN.add(SessionSlotLinkMark.class);
        CLASSES_TO_LISTEN.add(SessionSlotMarkGradeValue.class);
        CLASSES_TO_LISTEN.add(SessionSlotMarkState.class);

        ORIGINAL_ENTITY_NAME_SET.add(TrJournalGroupStudent.ENTITY_NAME);

        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEduGroupEvent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEduGroupEventStudent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournal.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalModule.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalEvent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEventAction.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEventAddon.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEventLoad.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalGroup.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalGroupStudent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SessionMark.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SessionSlotLinkMark.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SessionSlotMarkGradeValue.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SessionSlotMarkState.ENTITY_NAME, RESULT_MOBILE);

    }

    @Override
    public int getPriority()
    {
        return 2;
    }

    @Override
    public List<Class> getClassesToListen()
    {
        return CLASSES_TO_LISTEN;
    }

    @Override
    public List<CoreCollectionUtils.Pair<Long, String>> getIdToEntityTypePairList(Long entityId, boolean delete)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entityId);
        if (null == meta) return new ArrayList<>();

        if (!CLASSES_TO_LISTEN.contains(meta.getEntityClass())) return new ArrayList<>();

        List<CoreCollectionUtils.Pair<Long, String>> resultIdList = new ArrayList<>();

        if (ORIGINAL_ENTITY_NAME_SET.contains(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(entityId, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName())));
        } else
        {
            List<Long> ids = LksTrainingSystemManager.instance().dao().getEntityToUpdateIdList(entityId);
            ids.stream().map(e-> new CoreCollectionUtils.Pair<>(e, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName()))).forEach(resultIdList::add);
        }
        return resultIdList;
    }

    @Override
    public void initFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        LksTrainingSystemManager.instance().dao().doInintFullLksEntitySync(studentIds, entityType);
    }

    @Override
    public int syncLksEntityPortion(int elementsToProcessAmount)
    {
        return LksTrainingSystemManager.instance().dao().doSyncEntityPortion(elementsToProcessAmount);
    }
}