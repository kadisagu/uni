package ru.tandemservice.lksunitraining.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_lksunitraining_2x10x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность lksWorkResult

		//  свойство value стало необязательным
		if(tool.tableExists("lksworkresult_t"))
		{
			// сделать колонку NULL
			tool.setColumnNullable("lksworkresult_t", "value_p", true);

		}

		//  свойство maxValue стало необязательным
		if(tool.tableExists("lksworkresult_t"))
		{
			// сделать колонку NULL
			tool.setColumnNullable("lksworkresult_t", "maxvalue_p", true);

		}


    }
}