/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Dmitry Seleznev
 * @since 03.02.2017
 */
public class StudentPortfolioDTO
{
    private Long _id;
    private String _guid;
    private String _title;
    private String _controlActionTypeTitle;
    private String _marks;
    private Long _fileId;
    private String _studentGUID;
    private Long _studentId;
    private Long _controlActionId;

    public StudentPortfolioDTO(Object[] item)
    {
        _id = (Long) item[1];
        _guid = (String) item[0];
        _title = (String) item[2];
        _controlActionTypeTitle = (String) item[3];
        _fileId = (Long) item[4];
        _studentGUID = (String) item[5];
        _studentId = (Long) item[6];
        _controlActionId = (Long) item[7];
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _title);
        LksWrapUtil.appendString(result, _controlActionTypeTitle);
        LksWrapUtil.appendString(result, _marks);
        LksWrapUtil.appendString(result, _fileId == null ? "" : String.valueOf(_fileId));
        LksWrapUtil.appendString(result, _studentGUID);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public String getControlActionTypeTitle()
    {
        return _controlActionTypeTitle;
    }

    public void setControlActionTypeTitle(String controlActionTypeTitle)
    {
        _controlActionTypeTitle = controlActionTypeTitle;
    }

    public String getMarks()
    {
        return _marks;
    }

    public void setMarks(String marks)
    {
        _marks = marks;
    }

    public Long getFileId()
    {
        return _fileId;
    }

    public void setFileId(Long fileId)
    {
        _fileId = fileId;
    }

    public String getStudentGUID()
    {
        return _studentGUID;
    }

    public void setStudentGUID(String studentGUID)
    {
        _studentGUID = studentGUID;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Long getControlActionId()
    {
        return _controlActionId;
    }

    public void setControlActionId(Long controlActionId)
    {
        _controlActionId = controlActionId;
    }
}