/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2016
 */
public class EppEpvRowDTO
{
    private Long _id;
    private String _guid;
    private String _title;
    private String _storedIndex;
    private String _userIndex;
    private String _rowNumber;
    private String _totalLabor;
    private String _hoursTotal;
    private String _hoursAudit;
    private String _hoursSelfwork;
    private String _hoursControl;
    private String _hoursControlE;
    private String _iga;
    private String _blockGUID;
    private Long _parentId;
    private String _parentGUID;
    private Long _regDeisciplineId;
    private String _regDeisciplineGUID;

    public EppEpvRowDTO(Object[] item)
    {
        EppEpvRow row = (EppEpvRow) item[0];
        _id = row.getId();
        _guid = (String) item[1];
        _blockGUID = (String) item[2];
        _regDeisciplineGUID = (String) item[3];
        _title = row.getTitle();
        _storedIndex = row.getStoredIndex();
        _userIndex = row.getUserIndex();

        if (row instanceof EppEpvGroupReRow) _rowNumber = ((EppEpvGroupReRow) row).getNumber();

        EppRegistryElement regElement = null;

        if (row instanceof EppEpvTermDistributedRow)
        {
            EppEpvTermDistributedRow castedRow = (EppEpvTermDistributedRow) row;
            _rowNumber = castedRow.getNumber();
            _totalLabor = String.valueOf(castedRow.getTotalLabor());
            _hoursTotal = String.valueOf(castedRow.getHoursTotal());
            _hoursAudit = String.valueOf(castedRow.getHoursAudit());
            _hoursSelfwork = String.valueOf(castedRow.getHoursSelfwork());
            _hoursControl = String.valueOf(castedRow.getHoursControl());
            _hoursControlE = String.valueOf(castedRow.getHoursControlE());

            if (row instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow regElRow = (EppEpvRegistryRow) row;
                regElement = regElRow.getRegistryElement();

                if (!regElRow.getType().isDisciplineElement())
                    _iga = NsiUtils.formatBoolean(Boolean.TRUE);
            }
        }

        EppEpvRow parentRow = row.getHierarhyParent();
        if (null != parentRow) _parentId = parentRow.getId();
        if (null != regElement) _regDeisciplineId = regElement.getId();
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _title);
        LksWrapUtil.appendString(result, _storedIndex);
        LksWrapUtil.appendString(result, _userIndex);
        LksWrapUtil.appendString(result, _rowNumber);
        LksWrapUtil.appendString(result, _totalLabor);
        LksWrapUtil.appendString(result, _hoursTotal);
        LksWrapUtil.appendString(result, _hoursAudit);
        LksWrapUtil.appendString(result, _hoursSelfwork);
        LksWrapUtil.appendString(result, _hoursControl);
        LksWrapUtil.appendString(result, _hoursControlE);
        LksWrapUtil.appendString(result, _iga);
        LksWrapUtil.appendString(result, _blockGUID);
        LksWrapUtil.appendString(result, _parentGUID);
        LksWrapUtil.appendString(result, _regDeisciplineGUID);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getTitle()
    {
        return _title;
    }

    public String getStoredIndex()
    {
        return _storedIndex;
    }

    public String getUserIndex()
    {
        return _userIndex;
    }

    public String getRowNumber()
    {
        return _rowNumber;
    }

    public String getTotalLabor()
    {
        return _totalLabor;
    }

    public String getHoursTotal()
    {
        return _hoursTotal;
    }

    public String getHoursAudit()
    {
        return _hoursAudit;
    }

    public String getHoursSelfwork()
    {
        return _hoursSelfwork;
    }

    public String getHoursControl()
    {
        return _hoursControl;
    }

    public String getHoursControlE()
    {
        return _hoursControlE;
    }

    public String getIga()
    {
        return _iga;
    }

    public String getBlockGUID()
    {
        return _blockGUID;
    }

    public Long getParentId()
    {
        return _parentId;
    }

    public String getParentGUID()
    {
        return _parentGUID;
    }

    public void setParentGUID(String parentGUID)
    {
        _parentGUID = parentGUID;
    }

    public Long getRegDeisciplineId()
    {
        return _regDeisciplineId;
    }

    public String getRegDeisciplineGUID()
    {
        return _regDeisciplineGUID;
    }

    public void setRegDeisciplineGUID(String regDeisciplineGUID)
    {
        _regDeisciplineGUID = regDeisciplineGUID;
    }
}