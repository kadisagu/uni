/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic;

import org.tandemframework.core.process.ProcessState;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.ILksDao;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;

import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public interface ILksTrainingDao extends ILksDao
{
    void doInintFullLksEntitySync(Collection<Long> studentIds, String entityType);

    List<Long> getEntityToUpdateIdList(Long entityId);

    int doSyncEntityPortion(int elementsToProcessAmount);

    String getPortfolioElementMarks(StudentPortfolioElement portfolioElement);

    void deleteLksMarks();

    String doDownloadSessionResults(ProcessState state);

    String doDownloadEppEpvRows(ProcessState state);

    String doDownloadEppEpvRowLoads(ProcessState state);

    String doDownloadEppEpvRowTerms(ProcessState state);

    String doDownloadEppEpvRowTermLoads(ProcessState state);

    String doDownloadEppEpvRowTermActions(ProcessState state);

    String doDownloadPortfolio(ProcessState state);

    String doDownloadLksSubjectPart(ProcessState state);

    String doDownloadStudent2EduPlanBlock(ProcessState state);

    String doDownloadEppRegistryElement(ProcessState state);

    String doDownloadEduPlan(ProcessState state);

    String doDownloadEduPlanBlock(ProcessState state);

    String doDownloadLksSubject(ProcessState state);

    String doDownloadLksStudent2SubjectPart(ProcessState state);

    List<Object[]> getPortfoliFilesIdSet();

    List<Long> getWorkProgramFilesIdSet();

    List<Object[]> getEduPlanIdToNsiEntityGuid();
}