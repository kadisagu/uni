package ru.tandemservice.lksunitraining.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_lksunitraining_2x10x7_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.7"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность lksSessionResult

		// создано свойство date
		if (tool.tableExists("lkssessionresult_t") && !tool.columnExists("lkssessionresult_t", "date_p"))
		{
			// создать колонку
			tool.createColumn("lkssessionresult_t", new DBColumn("date_p", DBType.DATE));

		}

		// создано свойство commission
		if (tool.tableExists("lkssessionresult_t") && !tool.columnExists("lkssessionresult_t", "commission_p"))
		{
			// создать колонку
			tool.createColumn("lkssessionresult_t", new DBColumn("commission_p", DBType.createVarchar(255)));

		}


    }
}