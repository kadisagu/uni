/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

import org.tandemframework.core.CoreStringUtils;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2016
 */
public class LksSessionResultDTO
{
    private Long _id;
    private String _guid;
    private String _semesterNumber;
    private String _date;
    private String _result;
    private String _maxResult;
    private String _credit;
    private String _diffCredit;
    private String _exam;
    private String _examAccum;
    private String _courseProject;
    private String _courseWork;
    private String _controlWork;
    private String _commission;
    private String _educationYearGUID;
    private String _subjectGUID;
    private String _studentGUID;

    public LksSessionResultDTO(Object[] item)
    {
        _id = (Long) item[1];
        _guid = (String) item[0];
        _semesterNumber = (String) item[2];
        _result = (String) item[4];
        _maxResult = (String) item[5];
        _credit = (String) item[6];
        _diffCredit = (String) item[7];
        _exam = (String) item[8];
        _examAccum = (String) item[9];
        _courseProject = (String) item[10];
        _courseWork = (String) item[11];
        _controlWork = (String) item[12];
        _commission = (String) item[13];
        _educationYearGUID = (String) item[14];
        _subjectGUID = (String) item[15];
        _studentGUID = (String) item[16];

        Date date = (Date) item[3];
        _date = NsiUtils.formatDate(date);
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _semesterNumber);
        LksWrapUtil.appendString(result, _result);
        LksWrapUtil.appendString(result, _maxResult);
        LksWrapUtil.appendString(result, _credit);
        LksWrapUtil.appendString(result, _diffCredit);
        LksWrapUtil.appendString(result, _exam);
        LksWrapUtil.appendString(result, _examAccum);
        LksWrapUtil.appendString(result, _courseProject);
        LksWrapUtil.appendString(result, _courseWork);
        LksWrapUtil.appendString(result, _commission);
        LksWrapUtil.appendString(result, _educationYearGUID);
        LksWrapUtil.appendString(result, _subjectGUID);
        LksWrapUtil.appendString(result, _studentGUID);
        LksWrapUtil.appendString(result, _date);
        LksWrapUtil.appendString(result, _controlWork);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getSemesterNumber()
    {
        return _semesterNumber;
    }

    public String getDate()
    {
        return _date;
    }

    public String getResult()
    {
        return _result;
    }

    public String getMaxResult()
    {
        return _maxResult;
    }

    public String getCredit()
    {
        return _credit;
    }

    public String getDiffCredit()
    {
        return _diffCredit;
    }

    public String getExam()
    {
        return _exam;
    }

    public String getExamAccum()
    {
        return _examAccum;
    }

    public String getCourseWork()
    {
        return _courseWork;
    }

    public String getCourseProject()
    {
        return _courseProject;
    }

    public String getControlWork()
    {
        return _controlWork;
    }

    public String getCommission()
    {
        return _commission;
    }

    public String getEducationYearGUID()
    {
        return _educationYearGUID;
    }

    public String getSubjectGUID()
    {
        return _subjectGUID;
    }

    public String getStudentGUID()
    {
        return _studentGUID;
    }
}