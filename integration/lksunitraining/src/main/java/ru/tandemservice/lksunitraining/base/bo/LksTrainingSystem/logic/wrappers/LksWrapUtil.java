/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2016
 */
public class LksWrapUtil
{
    private static final String SPLITTER = ";";

    public static void appendString(StringBuilder builder, String str)
    {
        builder.append(null != str ? str.replaceAll("\n", "").replaceAll("\r", "").replaceAll(";", ",") : "").append(SPLITTER);
    }
}