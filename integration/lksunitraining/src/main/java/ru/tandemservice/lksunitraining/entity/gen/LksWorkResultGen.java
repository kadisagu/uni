package ru.tandemservice.lksunitraining.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import ru.tandemservice.lksunitraining.entity.LksWorkResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Промежуточная аттестация для личного кабинета студента (WorkResult)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksWorkResultGen extends EntityBase
 implements INaturalIdentifiable<LksWorkResultGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksunitraining.entity.LksWorkResult";
    public static final String ENTITY_NAME = "lksWorkResult";
    public static final int VERSION_HASH = 532028930;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_DATE = "date";
    public static final String P_TITLE = "title";
    public static final String P_ABSENT = "absent";
    public static final String P_VALUE = "value";
    public static final String P_MAX_VALUE = "maxValue";
    public static final String L_RESULT = "result";

    private long _entityId;     // Идентификатор связанного объекта
    private Date _date;     // Дата проведения контрольного мероприятия
    private String _title;     // Наименование события по журналу
    private String _absent;     // Отсутствовал
    private String _value;     // Оценка за мероприятие
    private String _maxValue;     // Максимальный балл за контрольное мероприятие
    private LksSessionResult _result;     // Оценка в сессию для личного кабинета студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Дата проведения контрольного мероприятия.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата проведения контрольного мероприятия.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Наименование события по журналу. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование события по журналу. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Отсутствовал.
     */
    @Length(max=255)
    public String getAbsent()
    {
        return _absent;
    }

    /**
     * @param absent Отсутствовал.
     */
    public void setAbsent(String absent)
    {
        dirty(_absent, absent);
        _absent = absent;
    }

    /**
     * @return Оценка за мероприятие.
     */
    @Length(max=255)
    public String getValue()
    {
        return _value;
    }

    /**
     * @param value Оценка за мероприятие.
     */
    public void setValue(String value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Максимальный балл за контрольное мероприятие.
     */
    @Length(max=255)
    public String getMaxValue()
    {
        return _maxValue;
    }

    /**
     * @param maxValue Максимальный балл за контрольное мероприятие.
     */
    public void setMaxValue(String maxValue)
    {
        dirty(_maxValue, maxValue);
        _maxValue = maxValue;
    }

    /**
     * @return Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     */
    @NotNull
    public LksSessionResult getResult()
    {
        return _result;
    }

    /**
     * @param result Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     */
    public void setResult(LksSessionResult result)
    {
        dirty(_result, result);
        _result = result;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksWorkResultGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksWorkResult)another).getEntityId());
            }
            setDate(((LksWorkResult)another).getDate());
            setTitle(((LksWorkResult)another).getTitle());
            setAbsent(((LksWorkResult)another).getAbsent());
            setValue(((LksWorkResult)another).getValue());
            setMaxValue(((LksWorkResult)another).getMaxValue());
            setResult(((LksWorkResult)another).getResult());
        }
    }

    public INaturalId<LksWorkResultGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksWorkResultGen>
    {
        private static final String PROXY_NAME = "LksWorkResultNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksWorkResultGen.NaturalId) ) return false;

            LksWorkResultGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksWorkResultGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksWorkResult.class;
        }

        public T newInstance()
        {
            return (T) new LksWorkResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "date":
                    return obj.getDate();
                case "title":
                    return obj.getTitle();
                case "absent":
                    return obj.getAbsent();
                case "value":
                    return obj.getValue();
                case "maxValue":
                    return obj.getMaxValue();
                case "result":
                    return obj.getResult();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "absent":
                    obj.setAbsent((String) value);
                    return;
                case "value":
                    obj.setValue((String) value);
                    return;
                case "maxValue":
                    obj.setMaxValue((String) value);
                    return;
                case "result":
                    obj.setResult((LksSessionResult) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "date":
                        return true;
                case "title":
                        return true;
                case "absent":
                        return true;
                case "value":
                        return true;
                case "maxValue":
                        return true;
                case "result":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "date":
                    return true;
                case "title":
                    return true;
                case "absent":
                    return true;
                case "value":
                    return true;
                case "maxValue":
                    return true;
                case "result":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "date":
                    return Date.class;
                case "title":
                    return String.class;
                case "absent":
                    return String.class;
                case "value":
                    return String.class;
                case "maxValue":
                    return String.class;
                case "result":
                    return LksSessionResult.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksWorkResult> _dslPath = new Path<LksWorkResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksWorkResult");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Дата проведения контрольного мероприятия.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Наименование события по журналу. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Отсутствовал.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getAbsent()
     */
    public static PropertyPath<String> absent()
    {
        return _dslPath.absent();
    }

    /**
     * @return Оценка за мероприятие.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getValue()
     */
    public static PropertyPath<String> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Максимальный балл за контрольное мероприятие.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getMaxValue()
     */
    public static PropertyPath<String> maxValue()
    {
        return _dslPath.maxValue();
    }

    /**
     * @return Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getResult()
     */
    public static LksSessionResult.Path<LksSessionResult> result()
    {
        return _dslPath.result();
    }

    public static class Path<E extends LksWorkResult> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _title;
        private PropertyPath<String> _absent;
        private PropertyPath<String> _value;
        private PropertyPath<String> _maxValue;
        private LksSessionResult.Path<LksSessionResult> _result;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksWorkResultGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Дата проведения контрольного мероприятия.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(LksWorkResultGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Наименование события по журналу. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LksWorkResultGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Отсутствовал.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getAbsent()
     */
        public PropertyPath<String> absent()
        {
            if(_absent == null )
                _absent = new PropertyPath<String>(LksWorkResultGen.P_ABSENT, this);
            return _absent;
        }

    /**
     * @return Оценка за мероприятие.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getValue()
     */
        public PropertyPath<String> value()
        {
            if(_value == null )
                _value = new PropertyPath<String>(LksWorkResultGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Максимальный балл за контрольное мероприятие.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getMaxValue()
     */
        public PropertyPath<String> maxValue()
        {
            if(_maxValue == null )
                _maxValue = new PropertyPath<String>(LksWorkResultGen.P_MAX_VALUE, this);
            return _maxValue;
        }

    /**
     * @return Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksWorkResult#getResult()
     */
        public LksSessionResult.Path<LksSessionResult> result()
        {
            if(_result == null )
                _result = new LksSessionResult.Path<LksSessionResult>(L_RESULT, this);
            return _result;
        }

        public Class getEntityClass()
        {
            return LksWorkResult.class;
        }

        public String getEntityName()
        {
            return "lksWorkResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
