package ru.tandemservice.lksunitraining.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка в сессию для личного кабинета студента (Result)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksSessionResultGen extends EntityBase
 implements INaturalIdentifiable<LksSessionResultGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksunitraining.entity.LksSessionResult";
    public static final String ENTITY_NAME = "lksSessionResult";
    public static final int VERSION_HASH = -901085280;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_SEMESTER_NUMBER = "semesterNumber";
    public static final String P_DATE = "date";
    public static final String P_RESULT = "result";
    public static final String P_MAX_RESULT = "maxResult";
    public static final String P_CREDIT = "credit";
    public static final String P_DIFF_CREDIT = "diffCredit";
    public static final String P_EXAM = "exam";
    public static final String P_EXAM_ACCUM = "examAccum";
    public static final String P_COURSE_WORK = "courseWork";
    public static final String P_COURSE_PROJECT = "courseProject";
    public static final String P_CONTROL_WORK = "controlWork";
    public static final String P_COMMISSION = "commission";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_SUBJECT = "subject";
    public static final String L_STUDENT = "student";

    private long _entityId;     // Идентификатор связанного объекта
    private String _semesterNumber;     // Номер семестра
    private Date _date;     // Дата проведения контрольного мероприятия
    private String _result;     // Оценка
    private String _maxResult;     // Максимальный балл за дисциплину
    private String _credit;     // Признак наличия зачета, или оценка за него
    private String _diffCredit;     // Признак наличия дифференцированного зачета, или оценка за него
    private String _exam;     // Признак наличия экзамена, или оценка за него
    private String _examAccum;     // Признак наличия накопительного экзамена, или оценка за него
    private String _courseWork;     // Признак наличия кусовой работы, или оценка за неё
    private String _courseProject;     // Признак наличия кусового проекта, или оценка за него
    private String _controlWork;     // Признак наличия контрольной работы, или оценка за неё
    private String _commission;     // Состав коммиссии
    private EducationYear _educationYear;     // Учебный год
    private LksSubject _subject;     // Дисциплина реестра для личного кабинета студента
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Номер семестра. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSemesterNumber()
    {
        return _semesterNumber;
    }

    /**
     * @param semesterNumber Номер семестра. Свойство не может быть null.
     */
    public void setSemesterNumber(String semesterNumber)
    {
        dirty(_semesterNumber, semesterNumber);
        _semesterNumber = semesterNumber;
    }

    /**
     * @return Дата проведения контрольного мероприятия.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата проведения контрольного мероприятия.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Оценка.
     */
    @Length(max=255)
    public String getResult()
    {
        return _result;
    }

    /**
     * @param result Оценка.
     */
    public void setResult(String result)
    {
        dirty(_result, result);
        _result = result;
    }

    /**
     * @return Максимальный балл за дисциплину.
     */
    @Length(max=255)
    public String getMaxResult()
    {
        return _maxResult;
    }

    /**
     * @param maxResult Максимальный балл за дисциплину.
     */
    public void setMaxResult(String maxResult)
    {
        dirty(_maxResult, maxResult);
        _maxResult = maxResult;
    }

    /**
     * @return Признак наличия зачета, или оценка за него.
     */
    @Length(max=255)
    public String getCredit()
    {
        return _credit;
    }

    /**
     * @param credit Признак наличия зачета, или оценка за него.
     */
    public void setCredit(String credit)
    {
        dirty(_credit, credit);
        _credit = credit;
    }

    /**
     * @return Признак наличия дифференцированного зачета, или оценка за него.
     */
    @Length(max=255)
    public String getDiffCredit()
    {
        return _diffCredit;
    }

    /**
     * @param diffCredit Признак наличия дифференцированного зачета, или оценка за него.
     */
    public void setDiffCredit(String diffCredit)
    {
        dirty(_diffCredit, diffCredit);
        _diffCredit = diffCredit;
    }

    /**
     * @return Признак наличия экзамена, или оценка за него.
     */
    @Length(max=255)
    public String getExam()
    {
        return _exam;
    }

    /**
     * @param exam Признак наличия экзамена, или оценка за него.
     */
    public void setExam(String exam)
    {
        dirty(_exam, exam);
        _exam = exam;
    }

    /**
     * @return Признак наличия накопительного экзамена, или оценка за него.
     */
    @Length(max=255)
    public String getExamAccum()
    {
        return _examAccum;
    }

    /**
     * @param examAccum Признак наличия накопительного экзамена, или оценка за него.
     */
    public void setExamAccum(String examAccum)
    {
        dirty(_examAccum, examAccum);
        _examAccum = examAccum;
    }

    /**
     * @return Признак наличия кусовой работы, или оценка за неё.
     */
    @Length(max=255)
    public String getCourseWork()
    {
        return _courseWork;
    }

    /**
     * @param courseWork Признак наличия кусовой работы, или оценка за неё.
     */
    public void setCourseWork(String courseWork)
    {
        dirty(_courseWork, courseWork);
        _courseWork = courseWork;
    }

    /**
     * @return Признак наличия кусового проекта, или оценка за него.
     */
    @Length(max=255)
    public String getCourseProject()
    {
        return _courseProject;
    }

    /**
     * @param courseProject Признак наличия кусового проекта, или оценка за него.
     */
    public void setCourseProject(String courseProject)
    {
        dirty(_courseProject, courseProject);
        _courseProject = courseProject;
    }

    /**
     * @return Признак наличия контрольной работы, или оценка за неё.
     */
    @Length(max=255)
    public String getControlWork()
    {
        return _controlWork;
    }

    /**
     * @param controlWork Признак наличия контрольной работы, или оценка за неё.
     */
    public void setControlWork(String controlWork)
    {
        dirty(_controlWork, controlWork);
        _controlWork = controlWork;
    }

    /**
     * @return Состав коммиссии.
     */
    @Length(max=255)
    public String getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Состав коммиссии.
     */
    public void setCommission(String commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Дисциплина реестра для личного кабинета студента. Свойство не может быть null.
     */
    @NotNull
    public LksSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Дисциплина реестра для личного кабинета студента. Свойство не может быть null.
     */
    public void setSubject(LksSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksSessionResultGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksSessionResult)another).getEntityId());
            }
            setSemesterNumber(((LksSessionResult)another).getSemesterNumber());
            setDate(((LksSessionResult)another).getDate());
            setResult(((LksSessionResult)another).getResult());
            setMaxResult(((LksSessionResult)another).getMaxResult());
            setCredit(((LksSessionResult)another).getCredit());
            setDiffCredit(((LksSessionResult)another).getDiffCredit());
            setExam(((LksSessionResult)another).getExam());
            setExamAccum(((LksSessionResult)another).getExamAccum());
            setCourseWork(((LksSessionResult)another).getCourseWork());
            setCourseProject(((LksSessionResult)another).getCourseProject());
            setControlWork(((LksSessionResult)another).getControlWork());
            setCommission(((LksSessionResult)another).getCommission());
            setEducationYear(((LksSessionResult)another).getEducationYear());
            setSubject(((LksSessionResult)another).getSubject());
            setStudent(((LksSessionResult)another).getStudent());
        }
    }

    public INaturalId<LksSessionResultGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksSessionResultGen>
    {
        private static final String PROXY_NAME = "LksSessionResultNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksSessionResultGen.NaturalId) ) return false;

            LksSessionResultGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksSessionResultGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksSessionResult.class;
        }

        public T newInstance()
        {
            return (T) new LksSessionResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "semesterNumber":
                    return obj.getSemesterNumber();
                case "date":
                    return obj.getDate();
                case "result":
                    return obj.getResult();
                case "maxResult":
                    return obj.getMaxResult();
                case "credit":
                    return obj.getCredit();
                case "diffCredit":
                    return obj.getDiffCredit();
                case "exam":
                    return obj.getExam();
                case "examAccum":
                    return obj.getExamAccum();
                case "courseWork":
                    return obj.getCourseWork();
                case "courseProject":
                    return obj.getCourseProject();
                case "controlWork":
                    return obj.getControlWork();
                case "commission":
                    return obj.getCommission();
                case "educationYear":
                    return obj.getEducationYear();
                case "subject":
                    return obj.getSubject();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "semesterNumber":
                    obj.setSemesterNumber((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "result":
                    obj.setResult((String) value);
                    return;
                case "maxResult":
                    obj.setMaxResult((String) value);
                    return;
                case "credit":
                    obj.setCredit((String) value);
                    return;
                case "diffCredit":
                    obj.setDiffCredit((String) value);
                    return;
                case "exam":
                    obj.setExam((String) value);
                    return;
                case "examAccum":
                    obj.setExamAccum((String) value);
                    return;
                case "courseWork":
                    obj.setCourseWork((String) value);
                    return;
                case "courseProject":
                    obj.setCourseProject((String) value);
                    return;
                case "controlWork":
                    obj.setControlWork((String) value);
                    return;
                case "commission":
                    obj.setCommission((String) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "subject":
                    obj.setSubject((LksSubject) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "semesterNumber":
                        return true;
                case "date":
                        return true;
                case "result":
                        return true;
                case "maxResult":
                        return true;
                case "credit":
                        return true;
                case "diffCredit":
                        return true;
                case "exam":
                        return true;
                case "examAccum":
                        return true;
                case "courseWork":
                        return true;
                case "courseProject":
                        return true;
                case "controlWork":
                        return true;
                case "commission":
                        return true;
                case "educationYear":
                        return true;
                case "subject":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "semesterNumber":
                    return true;
                case "date":
                    return true;
                case "result":
                    return true;
                case "maxResult":
                    return true;
                case "credit":
                    return true;
                case "diffCredit":
                    return true;
                case "exam":
                    return true;
                case "examAccum":
                    return true;
                case "courseWork":
                    return true;
                case "courseProject":
                    return true;
                case "controlWork":
                    return true;
                case "commission":
                    return true;
                case "educationYear":
                    return true;
                case "subject":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "semesterNumber":
                    return String.class;
                case "date":
                    return Date.class;
                case "result":
                    return String.class;
                case "maxResult":
                    return String.class;
                case "credit":
                    return String.class;
                case "diffCredit":
                    return String.class;
                case "exam":
                    return String.class;
                case "examAccum":
                    return String.class;
                case "courseWork":
                    return String.class;
                case "courseProject":
                    return String.class;
                case "controlWork":
                    return String.class;
                case "commission":
                    return String.class;
                case "educationYear":
                    return EducationYear.class;
                case "subject":
                    return LksSubject.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksSessionResult> _dslPath = new Path<LksSessionResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksSessionResult");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Номер семестра. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getSemesterNumber()
     */
    public static PropertyPath<String> semesterNumber()
    {
        return _dslPath.semesterNumber();
    }

    /**
     * @return Дата проведения контрольного мероприятия.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Оценка.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getResult()
     */
    public static PropertyPath<String> result()
    {
        return _dslPath.result();
    }

    /**
     * @return Максимальный балл за дисциплину.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getMaxResult()
     */
    public static PropertyPath<String> maxResult()
    {
        return _dslPath.maxResult();
    }

    /**
     * @return Признак наличия зачета, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCredit()
     */
    public static PropertyPath<String> credit()
    {
        return _dslPath.credit();
    }

    /**
     * @return Признак наличия дифференцированного зачета, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getDiffCredit()
     */
    public static PropertyPath<String> diffCredit()
    {
        return _dslPath.diffCredit();
    }

    /**
     * @return Признак наличия экзамена, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getExam()
     */
    public static PropertyPath<String> exam()
    {
        return _dslPath.exam();
    }

    /**
     * @return Признак наличия накопительного экзамена, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getExamAccum()
     */
    public static PropertyPath<String> examAccum()
    {
        return _dslPath.examAccum();
    }

    /**
     * @return Признак наличия кусовой работы, или оценка за неё.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCourseWork()
     */
    public static PropertyPath<String> courseWork()
    {
        return _dslPath.courseWork();
    }

    /**
     * @return Признак наличия кусового проекта, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCourseProject()
     */
    public static PropertyPath<String> courseProject()
    {
        return _dslPath.courseProject();
    }

    /**
     * @return Признак наличия контрольной работы, или оценка за неё.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getControlWork()
     */
    public static PropertyPath<String> controlWork()
    {
        return _dslPath.controlWork();
    }

    /**
     * @return Состав коммиссии.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCommission()
     */
    public static PropertyPath<String> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Дисциплина реестра для личного кабинета студента. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getSubject()
     */
    public static LksSubject.Path<LksSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends LksSessionResult> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _semesterNumber;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _result;
        private PropertyPath<String> _maxResult;
        private PropertyPath<String> _credit;
        private PropertyPath<String> _diffCredit;
        private PropertyPath<String> _exam;
        private PropertyPath<String> _examAccum;
        private PropertyPath<String> _courseWork;
        private PropertyPath<String> _courseProject;
        private PropertyPath<String> _controlWork;
        private PropertyPath<String> _commission;
        private EducationYear.Path<EducationYear> _educationYear;
        private LksSubject.Path<LksSubject> _subject;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksSessionResultGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Номер семестра. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getSemesterNumber()
     */
        public PropertyPath<String> semesterNumber()
        {
            if(_semesterNumber == null )
                _semesterNumber = new PropertyPath<String>(LksSessionResultGen.P_SEMESTER_NUMBER, this);
            return _semesterNumber;
        }

    /**
     * @return Дата проведения контрольного мероприятия.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(LksSessionResultGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Оценка.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getResult()
     */
        public PropertyPath<String> result()
        {
            if(_result == null )
                _result = new PropertyPath<String>(LksSessionResultGen.P_RESULT, this);
            return _result;
        }

    /**
     * @return Максимальный балл за дисциплину.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getMaxResult()
     */
        public PropertyPath<String> maxResult()
        {
            if(_maxResult == null )
                _maxResult = new PropertyPath<String>(LksSessionResultGen.P_MAX_RESULT, this);
            return _maxResult;
        }

    /**
     * @return Признак наличия зачета, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCredit()
     */
        public PropertyPath<String> credit()
        {
            if(_credit == null )
                _credit = new PropertyPath<String>(LksSessionResultGen.P_CREDIT, this);
            return _credit;
        }

    /**
     * @return Признак наличия дифференцированного зачета, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getDiffCredit()
     */
        public PropertyPath<String> diffCredit()
        {
            if(_diffCredit == null )
                _diffCredit = new PropertyPath<String>(LksSessionResultGen.P_DIFF_CREDIT, this);
            return _diffCredit;
        }

    /**
     * @return Признак наличия экзамена, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getExam()
     */
        public PropertyPath<String> exam()
        {
            if(_exam == null )
                _exam = new PropertyPath<String>(LksSessionResultGen.P_EXAM, this);
            return _exam;
        }

    /**
     * @return Признак наличия накопительного экзамена, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getExamAccum()
     */
        public PropertyPath<String> examAccum()
        {
            if(_examAccum == null )
                _examAccum = new PropertyPath<String>(LksSessionResultGen.P_EXAM_ACCUM, this);
            return _examAccum;
        }

    /**
     * @return Признак наличия кусовой работы, или оценка за неё.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCourseWork()
     */
        public PropertyPath<String> courseWork()
        {
            if(_courseWork == null )
                _courseWork = new PropertyPath<String>(LksSessionResultGen.P_COURSE_WORK, this);
            return _courseWork;
        }

    /**
     * @return Признак наличия кусового проекта, или оценка за него.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCourseProject()
     */
        public PropertyPath<String> courseProject()
        {
            if(_courseProject == null )
                _courseProject = new PropertyPath<String>(LksSessionResultGen.P_COURSE_PROJECT, this);
            return _courseProject;
        }

    /**
     * @return Признак наличия контрольной работы, или оценка за неё.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getControlWork()
     */
        public PropertyPath<String> controlWork()
        {
            if(_controlWork == null )
                _controlWork = new PropertyPath<String>(LksSessionResultGen.P_CONTROL_WORK, this);
            return _controlWork;
        }

    /**
     * @return Состав коммиссии.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getCommission()
     */
        public PropertyPath<String> commission()
        {
            if(_commission == null )
                _commission = new PropertyPath<String>(LksSessionResultGen.P_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Дисциплина реестра для личного кабинета студента. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getSubject()
     */
        public LksSubject.Path<LksSubject> subject()
        {
            if(_subject == null )
                _subject = new LksSubject.Path<LksSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksSessionResult#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return LksSessionResult.class;
        }

        public String getEntityName()
        {
            return "lksSessionResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
