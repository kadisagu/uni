package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.Date;

/**
 * @author Stanislav Shibarshin
 * @since 04.02.2017
 */
public class Student2EduPlanBlockDTO
{

    private Long _id;
    private String _guid;
    private String _studentGuid;
    private String _blockGuid;
    private String _confirmDate;
    private String _removalDate;

    public Student2EduPlanBlockDTO(Object[] item)
    {
        _id = (Long)item[3];
        _guid = (String)item[0];
        _studentGuid = (String)item[1];
        _blockGuid = (String)item[2];
        _confirmDate = NsiUtils.formatDate((Date)item[4]);
        _removalDate = NsiUtils.formatDate((Date)item[5]);
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _studentGuid);
        LksWrapUtil.appendString(result, _blockGuid);
        LksWrapUtil.appendString(result, _confirmDate);
        LksWrapUtil.appendString(result, _removalDate);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        this._guid = guid;
    }

    public String getStudentGuid() {
        return _studentGuid;
    }

    public void setStudentGuid(String studentGuid) {
        this._studentGuid = studentGuid;
    }

    public String getBlockGuid() {
        return _blockGuid;
    }

    public void setBlockGuid(String blockGuid) {
        this._blockGuid = blockGuid;
    }

    public String getConfirmDate() {
        return _confirmDate;
    }

    public void setConfirmDate(String confirmDate) {
        this._confirmDate = confirmDate;
    }

    public String getRemovalDate() {
        return _removalDate;
    }

    public void setRemovalDate(String removalDate) {
        this._removalDate = removalDate;
    }
}
