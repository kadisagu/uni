/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2016
 */
public class EppEpvRowTermLoadDTO
{
    private Long _id;
    private String _guid;
    private String _loadTypeCode;
    private String _loadTypeTitle;
    private String _loadTypeShortTitle;
    private String _loadTypeAbbreviation;
    private String _hours;
    private String _hoursE;
    private String _hoursI;
    private String _rowTermGUID;

    public EppEpvRowTermLoadDTO(Object[] item)
    {
        _id = (Long) item[1];
        _guid = (String) item[0];
        _loadTypeCode = (String) item[2];
        _loadTypeTitle = (String) item[3];
        _loadTypeShortTitle = (String) item[4];
        _loadTypeAbbreviation = (String) item[5];
        _hours = String.valueOf(item[6]);
        _hoursE = String.valueOf(item[7]);
        _hoursI = String.valueOf(item[8]);
        _rowTermGUID = (String) item[9];

        if ("-1".equals(_hours)) _hours = "";
        if ("-1".equals(_hoursE)) _hoursE = "";
        if ("-1".equals(_hoursI)) _hoursI = "";
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _loadTypeCode);
        LksWrapUtil.appendString(result, _loadTypeTitle);
        LksWrapUtil.appendString(result, _loadTypeShortTitle);
        LksWrapUtil.appendString(result, _loadTypeAbbreviation);
        LksWrapUtil.appendString(result, _hours);
        LksWrapUtil.appendString(result, _hoursE);
        LksWrapUtil.appendString(result, _hoursI);
        LksWrapUtil.appendString(result, _rowTermGUID);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getLoadTypeCode()
    {
        return _loadTypeCode;
    }

    public String getLoadTypeTitle()
    {
        return _loadTypeTitle;
    }

    public String getLoadTypeShortTitle()
    {
        return _loadTypeShortTitle;
    }

    public String getLoadTypeAbbreviation()
    {
        return _loadTypeAbbreviation;
    }

    public String getHours()
    {
        return _hours;
    }

    public String getHoursE()
    {
        return _hoursE;
    }

    public String getHoursI()
    {
        return _hoursI;
    }

    public String getRowTermGUID()
    {
        return _rowTermGUID;
    }
}