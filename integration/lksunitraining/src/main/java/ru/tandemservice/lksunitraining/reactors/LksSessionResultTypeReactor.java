/* $Id:$ */
package ru.tandemservice.lksunitraining.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 17.08.2016
 */
public class LksSessionResultTypeReactor extends BaseEntityReactor<LksSessionResult, LksSessionResultType>
{
    private static final String EDUCATION_YEAR = "EducationYearID";
    private static final String LKS_SUBJECT = "LksSubjectID";
    private static final String STUDENT = "StudentID";

    @Override
    public Class<LksSessionResult> getEntityClass()
    {
        return LksSessionResult.class;
    }

    @Override
    public void collectUnknownDatagrams(LksSessionResultType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksSessionResultType.EducationYearID educationYearID = datagramObject.getEducationYearID();
        EducationYearType educationYearType = educationYearID == null ? null : educationYearID.getEducationYear();
        if (educationYearType != null) collector.check(educationYearType.getID(), EducationYearType.class);

        LksSessionResultType.LksSubjectID lksSubjectID = datagramObject.getLksSubjectID();
        LksSubjectType lksSubjectType = lksSubjectID == null ? null : lksSubjectID.getLksSubject();
        if (lksSubjectType != null) collector.check(lksSubjectType.getID(), LksSubjectType.class);

        LksSessionResultType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        if (studentType != null) collector.check(studentType.getID(), StudentType.class);
    }

    @Override
    public LksSessionResultType createDatagramObject(String guid)
    {
        LksSessionResultType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSessionResultType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksSessionResult entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksSessionResultType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSessionResultType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksSessionResultID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksSessionResultDate(NsiUtils.formatDate(entity.getDate()));
        datagramObject.setLksSessionResultSemesterNumber(entity.getSemesterNumber());
        datagramObject.setLksSessionResultResult(entity.getResult());
        datagramObject.setLksSessionResultMaxResult(entity.getMaxResult());
        datagramObject.setLksSessionResultCredit(entity.getCredit());
        datagramObject.setLksSessionResultDiffCredit(entity.getDiffCredit());
        datagramObject.setLksSessionResultExam(entity.getExam());
        datagramObject.setLksSessionResultExamAccum(entity.getExamAccum());
        datagramObject.setLksSessionResultCourseProject(entity.getCourseProject());
        datagramObject.setLksSessionResultCourseWork(entity.getCourseWork());
        datagramObject.setLksSessionResultControlWork(entity.getControlWork());
        datagramObject.setLksSessionResultCommission(entity.getCommission());

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<EducationYearType> educationYearType = createDatagramObject(EducationYearType.class, entity.getEducationYear(), commonCache, warning);
        LksSessionResultType.EducationYearID educationYearID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSessionResultTypeEducationYearID();
        educationYearID.setEducationYear(educationYearType.getObject());
        datagramObject.setEducationYearID(educationYearID);
        if (educationYearType.getList() != null) result.addAll(educationYearType.getList());

        ProcessedDatagramObject<LksSubjectType> lksSubjectType = createDatagramObject(LksSubjectType.class, entity.getSubject(), commonCache, warning);
        LksSessionResultType.LksSubjectID lksSubjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSessionResultTypeLksSubjectID();
        lksSubjectID.setLksSubject(lksSubjectType.getObject());
        datagramObject.setLksSubjectID(lksSubjectID);
        if (lksSubjectType.getList() != null) result.addAll(lksSubjectType.getList());

        ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, entity.getStudent(), commonCache, warning);
        LksSessionResultType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSessionResultTypeStudentID();
        studentID.setStudent(studentType.getObject());
        datagramObject.setStudentID(studentID);
        if (studentType.getList() != null) result.addAll(studentType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksSessionResult findLksSessionResult(String code)
    {
        if (code != null)
        {
            Long entityId = null;
            try
            {
                entityId = Long.parseLong(code);
            } catch (NumberFormatException ex)
            {
                // Не судьба
            }
            List<LksSessionResult> list = findEntity(eq(property(ENTITY_ALIAS, LksSessionResult.entityId()), value(entityId)));
            if (list.size() == 1) return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<LksSessionResult> processDatagramObject(LksSessionResultType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksSessionResultID init
        String code = StringUtils.trimToNull(datagramObject.getLksSessionResultID());
        // LksSessionResultDate init
        Date date = NsiUtils.parseDate(datagramObject.getLksSessionResultDate(), "LksSessionResultDate");
        // LksSessionResultSemesterNumber init
        String semesterNumber = StringUtils.trimToNull(datagramObject.getLksSessionResultSemesterNumber());
        // LksSessionResultResult init
        String result = StringUtils.trimToNull(datagramObject.getLksSessionResultResult());
        // LksSessionResultMaxResult init
        String maxResult = StringUtils.trimToNull(datagramObject.getLksSessionResultMaxResult());
        // LksSessionResultCredit init
        String credit = StringUtils.trimToNull(datagramObject.getLksSessionResultCredit());
        // LksSessionResultDiffCredit init
        String diffCredit = StringUtils.trimToNull(datagramObject.getLksSessionResultDiffCredit());
        // LksSessionResultExam init
        String exam = StringUtils.trimToNull(datagramObject.getLksSessionResultExam());
        // LksSessionResultExamAccum init
        String examAccum = StringUtils.trimToNull(datagramObject.getLksSessionResultExamAccum());
        // LksSessionResultCourseProject init
        String courseProject = StringUtils.trimToNull(datagramObject.getLksSessionResultCourseProject());
        // LksSessionResultCourseWork init
        String courseWork = StringUtils.trimToNull(datagramObject.getLksSessionResultCourseWork());
        // LksSessionResultControlWork init
        String controlWork = StringUtils.trimToNull(datagramObject.getLksSessionResultControlWork());
        // LksSessionResultCommission init
        String commission = StringUtils.trimToNull(datagramObject.getLksSessionResultCommission());

        // EducationYearID init
        LksSessionResultType.EducationYearID educationYearID = datagramObject.getEducationYearID();
        EducationYearType educationYearType = educationYearID == null ? null : educationYearID.getEducationYear();
        EducationYear educationYear = getOrCreate(EducationYearType.class, params, commonCache, educationYearType, EDUCATION_YEAR, null, warning);

        // LksSubjectID init
        LksSessionResultType.LksSubjectID subjectID = datagramObject.getLksSubjectID();
        LksSubjectType lksSubjectType = subjectID == null ? null : subjectID.getLksSubject();
        LksSubject lksSubject = getOrCreate(LksSubjectType.class, params, commonCache, lksSubjectType, LKS_SUBJECT, null, warning);

        // StudentID init
        LksSessionResultType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        Student student = getOrCreate(StudentType.class, params, commonCache, studentType, STUDENT, null, warning);


        boolean isNew = false;
        boolean isChanged = false;
        LksSessionResult entity = null;
        CoreCollectionUtils.Pair<LksSessionResult, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksSessionResult(code);
        if (entity == null)
        {
            entity = new LksSessionResult();
        }

        // LksSessionResultSemesterNumber set
        if (null != semesterNumber && (null == entity.getSemesterNumber() || !semesterNumber.equals(entity.getSemesterNumber())))
        {
            isChanged = true;
            entity.setSemesterNumber(semesterNumber);
        }
        // LksSessionResultDate
        if (null != date && (null == entity.getDate() || !date.equals(entity.getDate())))
        {
            isChanged = true;
            entity.setDate(date);
        }
        // LksSessionResultResult set
        if (null != result && (null == entity.getResult() || !result.equals(entity.getResult())))
        {
            isChanged = true;
            entity.setResult(result);
        }
        // LksSessionResultMaxResult set
        if (null != maxResult && (null == entity.getMaxResult() || !maxResult.equals(entity.getMaxResult())))
        {
            isChanged = true;
            entity.setMaxResult(maxResult);
        }
        // LksSessionResultCredit set
        if (null != credit && (null == entity.getCredit() || !credit.equals(entity.getCredit())))
        {
            isChanged = true;
            entity.setCredit(credit);
        }
        // LksSessionResultDiffCredit set
        if (null != diffCredit && (null == entity.getDiffCredit() || !diffCredit.equals(entity.getDiffCredit())))
        {
            isChanged = true;
            entity.setDiffCredit(diffCredit);
        }
        // LksSessionResultExam set
        if (null != exam && (null == entity.getExam() || !exam.equals(entity.getExam())))
        {
            isChanged = true;
            entity.setExam(exam);
        }
        // LksSessionResultExamAccum set
        if (null != examAccum && (null == entity.getExamAccum() || !examAccum.equals(entity.getExamAccum())))
        {
            isChanged = true;
            entity.setExamAccum(examAccum);
        }
        // LksSessionResultCourseProject set
        if (null != courseProject && (null == entity.getCourseProject() || !courseProject.equals(entity.getCourseProject())))
        {
            isChanged = true;
            entity.setCourseProject(courseProject);
        }
        // LksSessionResultCourseWork set
        if (null != courseWork && (null == entity.getCourseWork() || !courseWork.equals(entity.getCourseWork())))
        {
            isChanged = true;
            entity.setCourseWork(courseWork);
        }
        // LksSessionResultControlWork set
        if (null != controlWork && (null == entity.getControlWork() || !controlWork.equals(entity.getControlWork())))
        {
            isChanged = true;
            entity.setControlWork(controlWork);
        }
        // LksSessionResultCommission set
        if (null != commission && (null == entity.getCommission() || !commission.equals(entity.getCommission())))
        {
            isChanged = true;
            entity.setCommission(commission);
        }

        // EducationYearID set
        if (null != educationYear && (null == entity.getEducationYear() || !educationYear.equals(entity.getEducationYear())))
        {
            isChanged = true;
            entity.setEducationYear(educationYear);
        }

        // LksSubjectID set
        if (null != lksSubject && (null == entity.getSubject() || !lksSubject.equals(entity.getSubject())))
        {
            isChanged = true;
            entity.setSubject(lksSubject);
        }

        // StudentID set
        if (null != student && (null == entity.getStudent() || !student.equals(entity.getStudent())))
        {
            isChanged = true;
            entity.setStudent(student);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksSessionResult> w, LksSessionResultType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EducationYearType.class, session, params, EDUCATION_YEAR, nsiPackage);
            saveChildEntity(LksSubjectType.class, session, params, LKS_SUBJECT, nsiPackage);
            saveChildEntity(StudentType.class, session, params, STUDENT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}