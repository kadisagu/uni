/* $Id:$ */
package ru.tandemservice.lksunitraining.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import ru.tandemservice.lksunitraining.entity.LksWorkResult;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksSessionResultType;
import ru.tandemservice.nsiclient.datagram.LksWorkResultType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2016
 */
public class LksWorkResultTypeReactor extends BaseEntityReactor<LksWorkResult, LksWorkResultType>
{
    private static final String LKS_SESSION_RESULT = "LksSessionResultID";

    @Override
    public Class<LksWorkResult> getEntityClass()
    {
        return LksWorkResult.class;
    }

    @Override
    public void collectUnknownDatagrams(LksWorkResultType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksWorkResultType.LksSessionResultID sessionResultID = datagramObject.getLksSessionResultID();
        LksSessionResultType sessionResultType = sessionResultID == null ? null : sessionResultID.getLksSessionResult();
        if (sessionResultType != null) collector.check(sessionResultType.getID(), LksSessionResultType.class);
    }

    @Override
    public LksWorkResultType createDatagramObject(String guid)
    {
        LksWorkResultType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksWorkResultType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksWorkResult entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksWorkResultType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksWorkResultType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksWorkResultID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksWorkResultDate(NsiUtils.formatDate(entity.getDate()));
        datagramObject.setLksWorkResultName(entity.getTitle());
        datagramObject.setLksWorkResultAbsent(entity.getAbsent());
        datagramObject.setLksWorkResultValue(entity.getValue());
        datagramObject.setLksWorkResultMaxValue(entity.getMaxValue());


        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<LksSessionResultType> sessionResultType = createDatagramObject(LksSessionResultType.class, entity.getResult(), commonCache, warning);
        LksWorkResultType.LksSessionResultID sessionResultID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksWorkResultTypeLksSessionResultID();
        sessionResultID.setLksSessionResult(sessionResultType.getObject());
        datagramObject.setLksSessionResultID(sessionResultID);
        if (sessionResultType.getList() != null) result.addAll(sessionResultType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksWorkResult findLksWorkResult(String code)
    {
        if (code != null)
        {
            Long entityId = null;
            try
            {
                entityId = Long.parseLong(code);
            } catch (NumberFormatException ex)
            {
                // Не судьба
            }
            List<LksWorkResult> list = findEntity(eq(property(ENTITY_ALIAS, LksWorkResult.entityId()), value(entityId)));
            if (list.size() == 1) return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<LksWorkResult> processDatagramObject(LksWorkResultType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksWorkResultID init
        String code = StringUtils.trimToNull(datagramObject.getLksWorkResultID());
        // LksWorkResultDate init
        Date date = NsiUtils.parseDate(datagramObject.getLksWorkResultDate(), "LksWorkResultDate");
        // LksWorkResultName init
        String name = StringUtils.trimToNull(datagramObject.getLksWorkResultName());
        // LksWorkResultAbsent init
        String absent = StringUtils.trimToNull(datagramObject.getLksWorkResultAbsent());
        // LksWorkResultValue init
        String value = StringUtils.trimToNull(datagramObject.getLksWorkResultValue());
        // LksWorkResultMaxValue init
        String maxValue = StringUtils.trimToNull(datagramObject.getLksWorkResultMaxValue());

        // LksSessionResultID init
        LksWorkResultType.LksSessionResultID sessionResultID = datagramObject.getLksSessionResultID();
        LksSessionResultType sessionResultType = sessionResultID == null ? null : sessionResultID.getLksSessionResult();
        LksSessionResult sessionResult = getOrCreate(LksSessionResultType.class, params, commonCache, sessionResultType, LKS_SESSION_RESULT, null, warning);


        boolean isNew = false;
        boolean isChanged = false;
        LksWorkResult entity = null;
        CoreCollectionUtils.Pair<LksWorkResult, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksWorkResult(code);
        if (entity == null)
        {
            entity = new LksWorkResult();
        }

        // LksWorkResultDate
        if (null != date && (null == entity.getDate() || !date.equals(entity.getDate())))
        {
            isChanged = true;
            entity.setDate(date);
        }
        // LksWorkResultName set
        if (null != name && (null == entity.getTitle() || !name.equals(entity.getTitle())))
        {
            isChanged = true;
            entity.setTitle(name);
        }
        // LksWorkResultAbsent set
        if (null != absent && (null == entity.getAbsent() || !absent.equals(entity.getAbsent())))
        {
            isChanged = true;
            entity.setAbsent(absent);
        }
        // LksWorkResultValue set
        if (null != value && (null == entity.getValue() || !value.equals(entity.getValue())))
        {
            isChanged = true;
            entity.setValue(value);
        }
        // LksWorkResultMaxValue set
        if (null != maxValue && (null == entity.getMaxValue() || !maxValue.equals(entity.getMaxValue())))
        {
            isChanged = true;
            entity.setMaxValue(maxValue);
        }

        // LksSessionResultID set
        if (null != sessionResult && (null == entity.getResult() || !sessionResult.equals(entity.getResult())))
        {
            isChanged = true;
            entity.setResult(sessionResult);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksWorkResult> w, LksWorkResultType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(LksSessionResultType.class, session, params, LKS_SESSION_RESULT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}