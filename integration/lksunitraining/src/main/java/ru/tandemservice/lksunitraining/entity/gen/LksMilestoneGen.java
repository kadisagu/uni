package ru.tandemservice.lksunitraining.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksunitraining.entity.LksMilestone;
import ru.tandemservice.lksunitraining.entity.LksSessionResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шкала для вычисления итогового балла в сеесию для личного кабинета студента (Milestone)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksMilestoneGen extends EntityBase
 implements INaturalIdentifiable<LksMilestoneGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksunitraining.entity.LksMilestone";
    public static final String ENTITY_NAME = "lksMilestone";
    public static final int VERSION_HASH = 1116790101;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_TITLE = "title";
    public static final String P_VALUE = "value";
    public static final String L_RESULT = "result";

    private long _entityId;     // Идентификатор связанного объекта
    private String _title;     // Определение границ балла для получения оценки
    private String _value;     // Оценка
    private LksSessionResult _result;     // Оценка в сессию для личного кабинета студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Определение границ балла для получения оценки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Определение границ балла для получения оценки. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Оценка. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getValue()
    {
        return _value;
    }

    /**
     * @param value Оценка. Свойство не может быть null.
     */
    public void setValue(String value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     */
    @NotNull
    public LksSessionResult getResult()
    {
        return _result;
    }

    /**
     * @param result Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     */
    public void setResult(LksSessionResult result)
    {
        dirty(_result, result);
        _result = result;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksMilestoneGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksMilestone)another).getEntityId());
            }
            setTitle(((LksMilestone)another).getTitle());
            setValue(((LksMilestone)another).getValue());
            setResult(((LksMilestone)another).getResult());
        }
    }

    public INaturalId<LksMilestoneGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksMilestoneGen>
    {
        private static final String PROXY_NAME = "LksMilestoneNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksMilestoneGen.NaturalId) ) return false;

            LksMilestoneGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksMilestoneGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksMilestone.class;
        }

        public T newInstance()
        {
            return (T) new LksMilestone();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "title":
                    return obj.getTitle();
                case "value":
                    return obj.getValue();
                case "result":
                    return obj.getResult();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "value":
                    obj.setValue((String) value);
                    return;
                case "result":
                    obj.setResult((LksSessionResult) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "title":
                        return true;
                case "value":
                        return true;
                case "result":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "title":
                    return true;
                case "value":
                    return true;
                case "result":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "title":
                    return String.class;
                case "value":
                    return String.class;
                case "result":
                    return LksSessionResult.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksMilestone> _dslPath = new Path<LksMilestone>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksMilestone");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Определение границ балла для получения оценки. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getValue()
     */
    public static PropertyPath<String> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getResult()
     */
    public static LksSessionResult.Path<LksSessionResult> result()
    {
        return _dslPath.result();
    }

    public static class Path<E extends LksMilestone> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _title;
        private PropertyPath<String> _value;
        private LksSessionResult.Path<LksSessionResult> _result;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksMilestoneGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Определение границ балла для получения оценки. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LksMilestoneGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getValue()
     */
        public PropertyPath<String> value()
        {
            if(_value == null )
                _value = new PropertyPath<String>(LksMilestoneGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Оценка в сессию для личного кабинета студента. Свойство не может быть null.
     * @see ru.tandemservice.lksunitraining.entity.LksMilestone#getResult()
     */
        public LksSessionResult.Path<LksSessionResult> result()
        {
            if(_result == null )
                _result = new LksSessionResult.Path<LksSessionResult>(L_RESULT, this);
            return _result;
        }

        public Class getEntityClass()
        {
            return LksMilestone.class;
        }

        public String getEntityName()
        {
            return "lksMilestone";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
