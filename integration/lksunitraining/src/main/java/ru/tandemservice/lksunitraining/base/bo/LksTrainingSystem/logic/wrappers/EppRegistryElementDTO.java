/* $Id$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Igor Belanov
 * @since 04.02.2017
 */
public class EppRegistryElementDTO
{
    private Long _id;
    private String _guid;
    private String _number;
//    private String _number; // (не нужен в ЛКС)
//    private String _labour; // (не нужен в ЛКС)
//    private String _size; // (не нужен в ЛКС)
    private String _title;
//    private String _fullTitle; // (не нужен в ЛКС)
//    private String _shortTitle; // (не нужен в ЛКС)
//    private String _fileName;
    private Long _fileId;
//    private String _annotationFileName;
    private Long _annotationFileId;

    public EppRegistryElementDTO(Object[] item)
    {
        _id = (Long)item[1];
        _guid = (String)item[0];
        _number = (String)item[2];
        _title = (String)item[3];
        _fileId = (Long)item[4];
        _annotationFileId = (Long)item[5];
    }


    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _number);
        LksWrapUtil.appendString(result, _title);
        LksWrapUtil.appendString(result, _fileId == null ? "" : String.valueOf(_fileId));
        LksWrapUtil.appendString(result, _annotationFileId == null ? "" : String.valueOf(_annotationFileId));
        return result.toString();
    }

    // getters & setters
    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public Long getFileId()
    {
        return _fileId;
    }

    public void setFileId(Long fileId)
    {
        _fileId = fileId;
    }

    public Long getAnnotationFileId()
    {
        return _annotationFileId;
    }

    public void setAnnotationFileId(Long annotationFileId)
    {
        _annotationFileId = annotationFileId;
    }
}
