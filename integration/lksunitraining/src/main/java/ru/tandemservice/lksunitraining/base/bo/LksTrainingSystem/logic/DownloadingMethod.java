/* $Id:$ */
package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic;

import org.tandemframework.core.process.ProcessState;

/**
 * @author Dmitry Seleznev
 * @since 04.02.2017
 */
public interface DownloadingMethod
{
    void downloadData(ProcessState state) throws Throwable;
}