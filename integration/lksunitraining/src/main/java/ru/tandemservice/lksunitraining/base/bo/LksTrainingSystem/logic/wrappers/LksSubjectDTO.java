package ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.logic.wrappers;

/**
 * @author Stanislav Shibarshin
 * @since 04.02.2017
 */
public class LksSubjectDTO
{

    private Long _id;
    private String _guid;
    private String _title;
    private String _shortTitle;

    public LksSubjectDTO(Object[] item)
    {
        _guid = (String)item[0];
        _id = (Long)item[1];
        _title = (String)item[2];
        _shortTitle = (String)item[3];
    }

    public String print()
    {
        StringBuilder result = new StringBuilder();
        LksWrapUtil.appendString(result, _guid);
        LksWrapUtil.appendString(result, _title);
        LksWrapUtil.appendString(result, _shortTitle);
        return result.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        this._guid = guid;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        this._title = title;
    }

    public String getShortTitle()
    {
        return _shortTitle;
    }

    public void setShortTitle(String shortTitle)
    {
        this._shortTitle = shortTitle;
    }
}
