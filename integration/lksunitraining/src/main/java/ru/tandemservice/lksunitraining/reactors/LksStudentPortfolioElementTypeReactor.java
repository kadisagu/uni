/* $Id:$ */
package ru.tandemservice.lksunitraining.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import ru.tandemservice.lksunitraining.base.bo.LksTrainingSystem.LksTrainingSystemManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksStudentPortfolioElementType;
import ru.tandemservice.nsiclient.datagram.StudentType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiDatagramUtils;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.StudentPortfolio.StudentPortfolioManager;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 31.08.2016
 */
public class LksStudentPortfolioElementTypeReactor extends BaseEntityReactor<StudentPortfolioElement, LksStudentPortfolioElementType>
{
    private static final String STUDENT = "StudentID";

    @Override
    public Class<StudentPortfolioElement> getEntityClass()
    {
        return StudentPortfolioElement.class;
    }

    @Override
    public void collectUnknownDatagrams(LksStudentPortfolioElementType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksStudentPortfolioElementType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        if (studentType != null) collector.check(studentType.getID(), StudentType.class);
    }

    @Override
    public LksStudentPortfolioElementType createDatagramObject(String guid)
    {
        LksStudentPortfolioElementType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudentPortfolioElementType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(StudentPortfolioElement entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksStudentPortfolioElementType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudentPortfolioElementType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksStudentPortfolioElementName(entity.getTitle());
        if (null != entity.getWpeCAction() && null != entity.getWpeCAction().getType() && null != entity.getWpeCAction().getType().getTitle())
            datagramObject.setLksStudentPortfolioElementConActionType(entity.getWpeCAction().getType().getTitle());
        datagramObject.setLksStudentPortfolioElementMarks(LksTrainingSystemManager.instance().dao().getPortfolioElementMarks(entity));

        if (!NsiDatagramUtils.isFilesShouldNotBeIncludedInDatagram())
        {
            if (null != entity.getFile())
            {
                final RemoteDocumentDTO dto = StudentPortfolioManager.instance().dao().getAchievementAttachement(entity.getId());
                if (null != dto)
                {
                    datagramObject.setLksStudentPortfolioElementContent(dto.getContent());
                    datagramObject.setLksStudentPortfolioElementFileName(dto.getFileName());
                }
            }
        }

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, entity.getStudent(), commonCache, warning);
        LksStudentPortfolioElementType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudentPortfolioElementTypeStudentID();
        studentID.setStudent(studentType.getObject());
        datagramObject.setStudentID(studentID);
        if (studentType.getList() != null) result.addAll(studentType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<StudentPortfolioElement> processDatagramObject(LksStudentPortfolioElementType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksStudentPortfolioElementName init
        String title = StringUtils.trimToNull(datagramObject.getLksStudentPortfolioElementName());

        // StudentID init
        LksStudentPortfolioElementType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        Student student = getOrCreate(StudentType.class, params, commonCache, studentType, STUDENT, null, warning);


        boolean isNew = false;
        boolean isChanged = false;
        StudentPortfolioElement entity = null;
        CoreCollectionUtils.Pair<StudentPortfolioElement, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null)
        {
            entity = new StudentPortfolioElement();
        }

        // StudentPortfolioElementSemesterNumber set
        if (null != title && (null == entity.getTitle() || !title.equals(entity.getTitle())))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        // StudentID set
        if (null == student)
            throw new ProcessingDatagramObjectException("LksStudentPortfolioElementType object without StudentID!");
        else if (null == entity.getStudent() || !student.equals(entity.getStudent()))
        {
            isChanged = true;
            entity.setStudent(student);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<StudentPortfolioElement> w, LksStudentPortfolioElementType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(StudentType.class, session, params, STUDENT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}