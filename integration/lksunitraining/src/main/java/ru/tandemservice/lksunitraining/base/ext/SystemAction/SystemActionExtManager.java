/* $Id:$ */
package ru.tandemservice.lksunitraining.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.lksunitraining.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Dmitry Seleznev
 * @since 08.11.2016
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("lks_deleteLksMarks", new SystemActionDefinition("lksbase", "deleteLksMarks", "onClickDeleteLksMarks", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadMarksAndEduPlanData", new SystemActionDefinition("lksbase", "downloadMarksAndEduPlanData", "onClickDownloadMarksAndEduPlanData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadPortfolioData", new SystemActionDefinition("lksbase", "downloadPortfolioData", "onClickDownloadPortfolioData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadPortfolioFilesData", new SystemActionDefinition("lksbase", "downloadPortfolioFilesData", "onClickDownloadPortfolioFilesData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadWorkProgramFilesData", new SystemActionDefinition("lksbase", "downloadWorkProgramFilesData", "onClickDownloadWorkProgramFilesData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadEduPlanFilesData", new SystemActionDefinition("lksbase", "downloadEduPlanFilesData", "onClickDownloadEduPlanFilesData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadPreparedData", new SystemActionDefinition("lksbase", "downloadPreparedData", "onClickDownloadPreparedData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadEduPlanData", new SystemActionDefinition("lksbase", "downloadEduPlanData", "onClickDownloadEduPlanData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadEduPlanBlockData", new SystemActionDefinition("lksbase", "downloadEduPlanBlockData", "onClickDownloadEduPlanBlockData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadLksSubjectData", new SystemActionDefinition("lksbase", "downloadLksSubjectData", "onClickDownloadLksSubjectData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadEppRegistryElementData", new SystemActionDefinition("lksbase", "downloadEppRegistryElementData", "onClickDownloadEppRegistryElement", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadLksSubjectPartData", new SystemActionDefinition("lksbase", "downloadLksSubjectPartData", "onClickDownloadLksSubjectPartData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadStudent2EduPlanBlockData", new SystemActionDefinition("lksbase", "downloadStudent2EduPlanBlockData", "onClickDownloadStudent2EduPlanBlockData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_downloadLksStudent2SubjectPartData", new SystemActionDefinition("lksbase", "downloadLksStudent2SubjectPartData", "onClickDownloadLksStudent2SubjectPartData", SystemActionPubExt.LKS_UNITRAINING_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}