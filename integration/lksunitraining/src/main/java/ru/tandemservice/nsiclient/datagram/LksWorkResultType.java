//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.17 at 12:01:17 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника Промежуточная аттестация для личного кабинета студента (WorkResult)
 * 
 * <p>Java class for LksWorkResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LksWorkResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="LksWorkResultID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LksWorkResultDate" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlDate" minOccurs="0"/>
 *         &lt;element name="LksWorkResultName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LksWorkResultAbsent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LksWorkResultValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LksWorkResultMaxValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LksSessionResultID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="LksSessionResult" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}LksSessionResultType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LksWorkResultType", propOrder = {

})
public class LksWorkResultType implements IDatagramObject {

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "LksWorkResultID")
    protected String lksWorkResultID;
    @XmlElement(name = "LksWorkResultDate")
    @XmlSchemaType(name = "anySimpleType")
    protected String lksWorkResultDate;
    @XmlElement(name = "LksWorkResultName")
    protected String lksWorkResultName;
    @XmlElement(name = "LksWorkResultAbsent")
    protected String lksWorkResultAbsent;
    @XmlElement(name = "LksWorkResultValue")
    protected String lksWorkResultValue;
    @XmlElement(name = "LksWorkResultMaxValue")
    protected String lksWorkResultMaxValue;
    @XmlElement(name = "LksSessionResultID")
    protected LksWorkResultType.LksSessionResultID lksSessionResultID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the lksWorkResultID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLksWorkResultID() {
        return lksWorkResultID;
    }

    /**
     * Sets the value of the lksWorkResultID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLksWorkResultID(String value) {
        this.lksWorkResultID = value;
    }

    /**
     * Gets the value of the lksWorkResultDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLksWorkResultDate() {
        return lksWorkResultDate;
    }

    /**
     * Sets the value of the lksWorkResultDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLksWorkResultDate(String value) {
        this.lksWorkResultDate = value;
    }

    /**
     * Gets the value of the lksWorkResultName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLksWorkResultName() {
        return lksWorkResultName;
    }

    /**
     * Sets the value of the lksWorkResultName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLksWorkResultName(String value) {
        this.lksWorkResultName = value;
    }

    /**
     * Gets the value of the lksWorkResultAbsent property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLksWorkResultAbsent() {
        return lksWorkResultAbsent;
    }

    /**
     * Sets the value of the lksWorkResultAbsent property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLksWorkResultAbsent(String value) {
        this.lksWorkResultAbsent = value;
    }

    /**
     * Gets the value of the lksWorkResultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLksWorkResultValue() {
        return lksWorkResultValue;
    }

    /**
     * Sets the value of the lksWorkResultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLksWorkResultValue(String value) {
        this.lksWorkResultValue = value;
    }

    /**
     * Gets the value of the lksWorkResultMaxValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLksWorkResultMaxValue() {
        return lksWorkResultMaxValue;
    }

    /**
     * Sets the value of the lksWorkResultMaxValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLksWorkResultMaxValue(String value) {
        this.lksWorkResultMaxValue = value;
    }

    /**
     * Gets the value of the lksSessionResultID property.
     * 
     * @return
     *     possible object is
     *     {@link LksWorkResultType.LksSessionResultID }
     *     
     */
    public LksWorkResultType.LksSessionResultID getLksSessionResultID() {
        return lksSessionResultID;
    }

    /**
     * Sets the value of the lksSessionResultID property.
     * 
     * @param value
     *     allowed object is
     *     {@link LksWorkResultType.LksSessionResultID }
     *     
     */
    public void setLksSessionResultID(LksWorkResultType.LksSessionResultID value) {
        this.lksSessionResultID = value;
    }

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="LksSessionResult" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}LksSessionResultType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class LksSessionResultID {

        @XmlElement(name = "LksSessionResult")
        protected LksSessionResultType lksSessionResult;

        /**
         * Gets the value of the lksSessionResult property.
         * 
         * @return
         *     possible object is
         *     {@link LksSessionResultType }
         *     
         */
        public LksSessionResultType getLksSessionResult() {
            return lksSessionResult;
        }

        /**
         * Sets the value of the lksSessionResult property.
         * 
         * @param value
         *     allowed object is
         *     {@link LksSessionResultType }
         *     
         */
        public void setLksSessionResult(LksSessionResultType value) {
            this.lksSessionResult = value;
        }

    }

}
