/* $Id$ */
package ru.tandemservice.nsibpms.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmRegOrgUnit.BpmRegOrgUnitManager;
import org.tandemframework.bpms.base.entity.BpmGroupMember;
import org.tandemframework.bpms.base.entity.BpmPersonHelper;
import org.tandemframework.bpms.base.entity.BpmRegOrgUnit;
import org.tandemframework.bpms.base.entity.BpmRegOrgUnitMember;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<String[]> hidedExtPoint()
    {
        return itemListExtension(_catalogManager.hidedExtPoint())
                .add(BpmRegOrgUnit.ENTITY_NAME, new String[]{BpmRegOrgUnit.L_HEAD_ACL_CONTEXT})
                .add(BpmPersonHelper.ENTITY_NAME, new String[]{BpmPersonHelper.L_OWNER_PART, BpmPersonHelper.L_HELPER_PART, BpmPersonHelper.L_HELPER_GROUP})
                .add(BpmRegOrgUnitMember.ENTITY_NAME, new String[]{BpmRegOrgUnitMember.L_PERSON_PART})
                .create();
    }


    @Bean
    public ItemListExtension<IDefaultComboDataSourceHandler> handlerExtPoint()
    {
        return itemListExtension(_catalogManager.handlerExtPoint())
                .add(BpmRegOrgUnit.ENTITY_NAME, BpmRegOrgUnitManager.instance().regOrgUnitComboDSHandler())
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> orderableExtPoint()
    {
        return itemListExtension(_catalogManager.orderableExtPoint())
                .add(BpmRegOrgUnit.ENTITY_NAME, false)
                .add(BpmGroupMember.ENTITY_NAME, false)
                .add(BpmRegOrgUnitMember.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> filterableExtPoint()
    {
        return itemListExtension(_catalogManager.filterableExtPoint())
                .add(BpmGroupMember.ENTITY_NAME, false)
                .add(BpmRegOrgUnitMember.ENTITY_NAME, false)
                .create();
    }
}