/* $Id: BpmRegDepartmentTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsibpms.reactors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.hibernate.Session;
import org.tandemframework.bpms.base.entity.BpmRegOrgUnit;
import org.tandemframework.bpms.base.entity.BpmRegOrgUnitHead;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.BpmRegDepartmentType;
import ru.tandemservice.nsiclient.datagram.DepartmentType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.IOperationReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.05.2015
 */
public class BpmRegDepartmentTypeReactor extends BaseEntityReactor<BpmRegOrgUnit, BpmRegDepartmentType>
{
    private static final String DEPARTMENT = "DepartmentID";
    private static final String HEAD = "head";

    private IOperationReactor _deleteOperationReactor;

    @Override
    public IOperationReactor getDeleteOperationReactor()
    {
        if(_deleteOperationReactor == null)
            _deleteOperationReactor = new IOperationReactor() {
                @Override
                public List<IDatagramObject> process(List<IDatagramObject> items, NsiPackage nsiPackage, Map<String, Object> commonCache, Map<String, Object> reactorCache, StringBuilder warnLog, StringBuilder errorLog)
                {
                    Class<BpmRegOrgUnit> c = getEntityClass();
                    String className = c.getSimpleName();
                    ILogger logger = NsiRequestHandlerService.instance().getLogger();

                    // Подготавливаем сет идентификаторов ОБ для инициализации мапов
                    List<Long> entityIds = NsiSyncManager.instance().dao().getEntityIdsByDatagramObjects(items);

                    // Не указан ни один идентификатор сущности НСИ на удаление, либо запрос предполагает тотальное удаление всех элементов из ОБ
                    if (null == entityIds)
                    {
                        logger.log(Level.ERROR, "---------- There are no any GUID specified for the delete operation. Whole entity set could not be deleted ----------");
                        if(errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append("Could not delete whole entity set. Please, specify entity ids list to delete.");
                        return null;
                    }

                    // Инициализируем мапы объектов и связанных с ними GUID'ов
                    Map<String, CoreCollectionUtils.Pair<BpmRegOrgUnit, NsiEntity>> idToEntityMap = NsiSyncManager.instance().dao().getEntityWithNsiIdsMap(c, entityIds);
                    logger.log(Level.INFO, "---------- Whole elements list (" + idToEntityMap.values().size() + " items) for " + className + " catalog was received from database ----------");

                    // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов)
                    List<String> nonDeletableGuids = NsiSyncManager.instance().dao().getNonDeletableGuids(c, entityIds);
                    if(!nonDeletableGuids.isEmpty()) {
                        if(warnLog.length() > 0)
                            warnLog.append('\n');
                        warnLog.append("One, or more entities were set inactive: ").append(CommonBaseStringUtil.joinNotEmpty(nonDeletableGuids, ", "));
                    }

                    // Подготавливаем список объектов реально обработанных и GUID'ов, не найденных в системе
                    List<String> strangeGuids = new ArrayList<>();
                    List<IDatagramObject> deletedObjectsList = new ArrayList<>();
                    for (IDatagramObject datagramObject : items)
                    {
                        String guid = StringUtils.trimToNull(datagramObject.getID());
                        if(nonDeletableGuids.contains(guid)) {
                            CoreCollectionUtils.Pair<BpmRegOrgUnit, NsiEntity> pair = idToEntityMap.get(guid);
                            if (null != pair) {
                                deletedObjectsList.add(createDatagramObject(pair.getY().getGuid()));
                                NsiSyncManager.instance().dao().createEntityLog(pair.getY(), getCatalogType(), nsiPackage, null);
                                pair.getX().setActive(false);
                                DataAccessServices.dao().saveOrUpdate(pair.getX());
                            } else
                                strangeGuids.add(datagramObject.getID());
                        }
                        else
                        {
                            CoreCollectionUtils.Pair<BpmRegOrgUnit, NsiEntity> pair = idToEntityMap.get(guid);
                            if (null != pair) {
                                deletedObjectsList.add(createDatagramObject(pair.getY().getGuid()));
                                NsiSyncManager.instance().dao().createEntityLog(pair.getY(), getCatalogType(), nsiPackage, null);
                                DataAccessServices.dao().delete(pair.getX());
                            } else
                                strangeGuids.add(datagramObject.getID());
                        }
                    }

                    // Выдаём предупреждение, если среди переданных GUID'ов имеются не найденные в ОБ. Удалять нечего.
                    if (!strangeGuids.isEmpty())
                    {
                        logger.log(Level.WARN, "---------- Deleting requested list (" + deletedObjectsList.size() + " items) for " + className + " catalog was executed with warnings ----------");
                        if(warnLog.length() > 0)
                            warnLog.append('\n');
                        warnLog.append("One, or more entities were not found at OB by given GUID's, so they were ignored: ").append(CommonBaseStringUtil.joinNotEmpty(strangeGuids, ", "));
                    }

                    logger.log(Level.INFO, "---------- Deleting requested elements list (" + deletedObjectsList.size() + " items) for " + className + " catalog was executed successfully ----------");

                    return deletedObjectsList;
                }
            };
        return _deleteOperationReactor;
    }

    @Override
    public Class<BpmRegOrgUnit> getEntityClass()
    {
        return BpmRegOrgUnit.class;
    }

    @Override
    public BpmRegDepartmentType createDatagramObject(String guid)
    {
        BpmRegDepartmentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(BpmRegOrgUnit entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        BpmRegDepartmentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setBpmRegDepartmentHead(NsiUtils.formatBoolean(entity.isHead()));
        datagramObject.setBpmRegDepartmentActive(NsiUtils.formatBoolean(entity.isActive()));

        StringBuilder warning = new StringBuilder();
        if(entity.getOrgUnit() != null) {
            ProcessedDatagramObject<DepartmentType> r = createDatagramObject(DepartmentType.class, entity.getOrgUnit(), commonCache, warning);
            BpmRegDepartmentType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentTypeDepartmentID();
            departmentID.setDepartment(r.getObject());
            datagramObject.setDepartmentID(departmentID);
            if(r.getList() != null)
                result.addAll(r.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(BpmRegDepartmentType datagramObject, IUnknownDatagramsCollector collector) {
        BpmRegDepartmentType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if(departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);
    }

    protected BpmRegOrgUnit findBpmRegOrgUnit(Long orgUnitId)
    {
        if (orgUnitId == null)
            return null;
        List<BpmRegOrgUnit> list = findEntity(eq(property(ENTITY_ALIAS, BpmRegOrgUnit.orgUnit().id()), value(orgUnitId)));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<BpmRegOrgUnit> processDatagramObject(BpmRegDepartmentType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // BpmRegDepartmentHead init
        Boolean head = NsiUtils.parseBoolean(datagramObject.getBpmRegDepartmentHead());
        // BpmRegDepartmentActive init
        Boolean active = NsiUtils.parseBoolean(datagramObject.getBpmRegDepartmentActive());
        // DepartmentID init
        BpmRegDepartmentType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit orgUnit = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        BpmRegOrgUnit entity = null;
        CoreCollectionUtils.Pair<BpmRegOrgUnit, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findBpmRegOrgUnit(orgUnit == null ? null : orgUnit.getId());
        if (entity == null)
        {
            entity = new BpmRegOrgUnit();
            isNew = true;
            isChanged = true;
            params.put(HEAD, true);
        }

        // BpmRegDepartmentHead set
        if (head != null && head != entity.isHead())
        {
            isChanged = true;
            entity.setHead(head);
        }
        // BpmRegDepartmentActive set
        if (active != null && active != entity.isActive())
        {
            isChanged = true;
            entity.setActive(active);
        }
        // DepartmentID set
        if (null == orgUnit && isNew)
            throw new ProcessingDatagramObjectException("BpmRegDepartmentType object without DepartmentID!");
        if (orgUnit != null && !orgUnit.equals(entity.getOrgUnit()))
        {
            isChanged = true;
            entity.setOrgUnit(orgUnit);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<BpmRegOrgUnit> w, BpmRegDepartmentType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        BpmRegOrgUnit entity = w.getResult();
        if(params != null) {
            saveChildEntity(DepartmentType.class, session, params, DEPARTMENT, nsiPackage);

            if (params.containsKey(HEAD)) {
                BpmRegOrgUnitHead head = new BpmRegOrgUnitHead();
                session.saveOrUpdate(head);
                entity.setHeadAclContext(head);
            }
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    public List<IDatagramObject> createTestObjects() {
        BpmRegDepartmentType entity = new BpmRegDepartmentType();
        entity.setID("cc5d937a-3202-3c23-8a11-regDepartxxX");
        entity.setDepartmentID(new BpmRegDepartmentType.DepartmentID());
        entity.setBpmRegDepartmentHead("1");
        entity.setBpmRegDepartmentActive("1");
        INsiEntityReactor<OrgUnit, DepartmentType> reactor = (INsiEntityReactor<OrgUnit, DepartmentType>) ReactorHolder.getReactorMap().get(DepartmentType.class.getSimpleName());
        entity.getDepartmentID().setDepartment(reactor.getEmbedded(true));

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        datagrams.add(reactor.getEmbedded(false));
        return datagrams;
    }

    public static List<IDatagramObject> getBpmRegDepartmentTypeForEmbedded(boolean onlyGuid)
    {
        BpmRegDepartmentType entity = new BpmRegDepartmentType();
        entity.setID("cc5d937a-3202-3c23-8a11-regDepartxxX");
        List<IDatagramObject> datagrams = new ArrayList<>();

        datagrams.add(entity);

        if (!onlyGuid)
        {
            entity.setDepartmentID(new BpmRegDepartmentType.DepartmentID());
            entity.setBpmRegDepartmentHead("1");
            entity.setBpmRegDepartmentActive("1");
            INsiEntityReactor<OrgUnit, DepartmentType> reactor = (INsiEntityReactor<OrgUnit, DepartmentType>) ReactorHolder.getReactorMap().get(DepartmentType.class.getSimpleName());
            entity.getDepartmentID().setDepartment(reactor.getEmbedded(true));

            datagrams.add(reactor.getEmbedded(true));
        }
        return datagrams;
    }

    @Override
    public boolean isCorrect(BpmRegOrgUnit entity, BpmRegDepartmentType datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException {
        return entity.getOrgUnit().isTop();
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(BpmRegOrgUnit.L_HEAD_ACL_CONTEXT);
        return  fields;
    }
}
