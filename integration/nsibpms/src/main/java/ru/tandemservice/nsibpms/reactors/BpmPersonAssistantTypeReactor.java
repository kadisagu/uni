/* $Id: BpmPersonAssistantTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsibpms.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.bpms.base.bo.BpmHelper.BpmHelperManager;
import org.tandemframework.bpms.base.entity.BpmPersonHelper;
import org.tandemframework.bpms.base.entity.BpmPersonHelpersGroup;
import org.tandemframework.bpms.catalog.entity.BpmHelperType;
import org.tandemframework.bpms.catalog.entity.codes.BpmHelperTypeCodes;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.BpmPersonAssistantType;
import ru.tandemservice.nsiclient.datagram.EmployeeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.05.2015
 */
public class BpmPersonAssistantTypeReactor extends BaseEntityReactor<BpmPersonHelper, BpmPersonAssistantType>
{
    private static final String OWNER = "OwnerID";
    private static final String ASSISTANT = "AssistantID";

    @Override
    public Class<BpmPersonHelper> getEntityClass()
    {
        return BpmPersonHelper.class;
    }

    @Override
    public BpmPersonAssistantType createDatagramObject(String guid)
    {
        BpmPersonAssistantType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmPersonAssistantType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(BpmPersonHelper entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        BpmPersonAssistantType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmPersonAssistantType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        StringBuilder warning = new StringBuilder();

        datagramObject.setBpmPersonAssistantResponsible(NsiUtils.formatBoolean(entity.isResponse()));
        datagramObject.setBpmPersonAssistantDateFrom(NsiUtils.formatDate(entity.getHelpDateFrom()));
        datagramObject.setBpmPersonAssistantDateTo(NsiUtils.formatDate(entity.getHelpDateTo()));

        if (entity.getHelperEmpl() != null)
        {
            ProcessedDatagramObject<EmployeeType> r = createDatagramObject(EmployeeType.class, entity.getHelperEmpl(), commonCache, warning);
            BpmPersonAssistantType.AssistantID assistantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmPersonAssistantTypeAssistantID();
            assistantID.setEmployee(r.getObject());
            datagramObject.setAssistantID(assistantID);
            if (r.getList() != null)
                result.addAll(r.getList());
        }
        ProcessedDatagramObject<EmployeeType> r = createDatagramObject(EmployeeType.class, entity.getOwnerEmpl(), commonCache, warning);
        BpmPersonAssistantType.OwnerID ownerID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmPersonAssistantTypeOwnerID();
        ownerID.setEmployee(r.getObject());
        datagramObject.setOwnerID(ownerID);
        if(r.getList() != null)
            result.addAll(r.getList());

        datagramObject.setBpmPersonAssistantReason(entity.getTitle());
        datagramObject.setAssistanceType(BpmHelperTypeCodes.HELPER.equals(entity.getHelperType().getCode()) ? NsiUtils.TYPE_ASSISTANT_HELPER : NsiUtils.TYPE_ASSISTANT_VICE);
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(BpmPersonAssistantType datagramObject, IUnknownDatagramsCollector collector) {
        BpmPersonAssistantType.AssistantID assistantID = datagramObject.getAssistantID();
        EmployeeType assistant = assistantID == null ? null : assistantID.getEmployee();
        if(assistant != null)
            collector.check(assistant.getID(), EmployeeType.class);
    }

    protected BpmPersonHelper findBpmPersonHelper(String reason, Long ownerId, Long helperId, BpmHelperType helperType, Date dateFrom, Date dateTo)
    {
        if(ownerId == null || helperId == null)
            return null;
        List<IExpression> expressions = new ArrayList<>();
        expressions.add(eq(property(ENTITY_ALIAS, BpmPersonHelper.ownerEmpl().id()), value(ownerId)));
        expressions.add(eq(property(ENTITY_ALIAS, BpmPersonHelper.helperEmpl().id()), value(helperId)));
        if(dateFrom != null)
            expressions.add(eq(property(ENTITY_ALIAS, BpmPersonHelper.helpDateFrom()), commonValue(dateFrom)));
        if(dateTo != null)
            expressions.add(eq(property(ENTITY_ALIAS, BpmPersonHelper.helpDateTo()), commonValue(dateTo)));
        if(reason != null)
            expressions.add(eq(property(ENTITY_ALIAS, BpmPersonHelper.title()), commonValue(reason)));
        if(helperType != null)
            expressions.add(eq(property(ENTITY_ALIAS, BpmPersonHelper.helperType()), commonValue(helperType)));
        List<BpmPersonHelper> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if(list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<BpmPersonHelper> processDatagramObject(BpmPersonAssistantType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // BpmPersonAssistantResponsible init
        Boolean responsible = NsiUtils.parseBoolean(datagramObject.getBpmPersonAssistantResponsible());
        // BpmPersonAssistantDateFrom init
        Date dateFrom = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getBpmPersonAssistantDateFrom()), "bpmPersonAssistantDateFrom");
        // BpmPersonAssistantDateTo init
        Date dateTo = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getBpmPersonAssistantDateTo()), "bpmPersonAssistantDateTo");
        // AssistantID init
        BpmPersonAssistantType.AssistantID assistantID = datagramObject.getAssistantID();
        EmployeeType assistant = assistantID == null ? null : assistantID.getEmployee();
        EmployeePost assistantPost = getOrCreate(EmployeeType.class, params, commonCache, assistant, ASSISTANT, null, warning);
        // OwnerID init
        BpmPersonAssistantType.OwnerID ownerID = datagramObject.getOwnerID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        EmployeePost ownerPost = getOrCreate(EmployeeType.class, params, commonCache, owner, OWNER, null, warning);
        // BpmPersonAssistantReason init
        String reason = StringUtils.trimToNull(datagramObject.getBpmPersonAssistantReason());
        // AssistanceType init
        String assistantType = StringUtils.trimToNull(datagramObject.getAssistanceType());
        BpmHelperType helperType = null;
        if(assistantType != null)
        {
            if(assistantType.equalsIgnoreCase(NsiUtils.TYPE_ASSISTANT_VICE))
                helperType = DataAccessServices.dao().get(BpmHelperType.class, BpmHelperType.code(), BpmHelperTypeCodes.VICE);
            if(assistantType.equalsIgnoreCase(NsiUtils.TYPE_ASSISTANT_HELPER))
                helperType = DataAccessServices.dao().get(BpmHelperType.class, BpmHelperType.code(), BpmHelperTypeCodes.HELPER);
        }

        boolean isNew = false;
        boolean isChanged = false;
        BpmPersonHelper entity = null;
        CoreCollectionUtils.Pair<BpmPersonHelper, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findBpmPersonHelper(reason, ownerPost == null ? null : ownerPost.getId(), assistantPost == null ? null : assistantPost.getId(), helperType, dateFrom, dateTo);
        if(entity == null)
        {
            entity = new BpmPersonHelper();
            isNew = true;
            isChanged = true;
        }

        // BpmPersonAssistantResponsible set
        if(isNew && responsible == null)
            responsible = false;
        if(responsible != null && responsible != entity.isResponse())
        {
            isChanged = true;
            entity.setResponse(responsible);
        }
        // BpmPersonAssistantDateFrom set
        if(dateFrom != null && !dateFrom.equals(entity.getHelpDateFrom()))
        {
            isChanged = true;
            entity.setHelpDateFrom(dateFrom);
        }
        // BpmPersonAssistantDateTo set
        if(dateTo != null && !dateTo.equals(entity.getHelpDateTo()))
        {
            isChanged = true;
            entity.setHelpDateTo(dateTo);
        }
        // AssistantID set
        if(assistantPost != null && !assistantPost.equals(entity.getHelperEmpl()))
        {
            isChanged = true;
            entity.setHelperEmpl(assistantPost);
        }
        // OwnerID set
        if(ownerPost == null && isNew)
            throw new ProcessingDatagramObjectException("BpmPersonAssistantType object without OwnerID!");
        if(ownerPost != null && !ownerPost.equals(entity.getOwnerEmpl()))
        {
            isChanged = true;
            entity.setOwnerEmpl(ownerPost);
        }
        // BpmPersonAssistantReason set
        if(reason != null && !reason.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(reason);
        }
        // AssistanceType set
        if(helperType == null && isNew)
            throw new ProcessingDatagramObjectException("BpmPersonAssistantType object without AssistanceType!");
        if(helperType != null && !helperType.equals(entity.getHelperType()))
        {
            isChanged = true;
            entity.setHelperType(helperType);
        }

        String message = BpmHelperManager.instance().modifyDAO().validateBpmPersonHelper(entity, entity.getHelpDateFrom(), false, entity.getHelpDateTo(), null);
        if(message != null)
            throw new ProcessingDatagramObjectException(message);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<BpmPersonHelper> w, BpmPersonAssistantType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        BpmPersonHelper entity = w.getResult();
        if(params != null) {
            saveChildEntity(EmployeeType.class, session, params, OWNER, nsiPackage);
            saveChildEntity(EmployeeType.class, session, params, ASSISTANT, nsiPackage);

            BpmPersonHelpersGroup group = findHelpersGroupByOwner(session, entity.getOwnerEmpl());
            session.saveOrUpdate(group);

            entity.setHelperGroup(group);
            entity.setUpdateDate(new Date());
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(BpmPersonHelper.L_UPDATED_BY_EMPL);
        return  fields;
    }

    public BpmPersonHelpersGroup findHelpersGroupByOwner(Session session, EmployeePost employeePost)
    {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(BpmPersonHelpersGroup.class, "k")
                    .top(1)
                    .where(eq(property("k", BpmPersonHelpersGroup.ownerEmpl().id()), value(employeePost.getId())));
            BpmPersonHelpersGroup group = builder.createStatement(session).uniqueResult();
            if(group == null)
            {
                group = new BpmPersonHelpersGroup();
                group.setOwnerEmpl(employeePost);
            }
            return group;
    }
}
