/* $Id: BpmGroupMemberTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsibpms.reactors;

import org.hibernate.Session;
import org.tandemframework.bpms.base.bo.BpmGroup.BpmGroupManager;
import org.tandemframework.bpms.base.entity.BpmGroup;
import org.tandemframework.bpms.base.entity.BpmGroupMember;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.BpmGroupMemberType;
import ru.tandemservice.nsiclient.datagram.BpmGroupType;
import ru.tandemservice.nsiclient.datagram.EmployeeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiemployeebase.reactors.EmployeeTypeReactor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.05.2015
 */
public class BpmGroupMemberTypeReactor extends BaseEntityReactor<BpmGroupMember, BpmGroupMemberType>
{
    private static final String EMPLOYEE = "EmployeeID";
    private static final String BPM_GROUP = "BpmGroupID";

    @Override
    public Class<BpmGroupMember> getEntityClass()
    {
        return BpmGroupMember.class;
    }

    @Override
    public BpmGroupMemberType createDatagramObject(String guid)
    {
        BpmGroupMemberType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupMemberType();
        if (null != guid)
            datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(BpmGroupMember entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        BpmGroupMemberType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupMemberType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<EmployeeType> r = createDatagramObject(EmployeeType.class, entity.getEmployeePost(), commonCache, warning);
        BpmGroupMemberType.EmployeeID employeeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupMemberTypeEmployeeID();
        employeeID.setEmployee(r.getObject());
        datagramObject.setEmployeeID(employeeID);
        if(r.getList() != null)
            result.addAll(r.getList());

        ProcessedDatagramObject<BpmGroupType> bpmGroupTypeResultWrapper = createDatagramObject(BpmGroupType.class, entity.getBpmGroup(), commonCache, warning);
        BpmGroupMemberType.BpmGroupID groupID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupMemberTypeBpmGroupID();
        groupID.setBpmGroup(bpmGroupTypeResultWrapper.getObject());
        datagramObject.setBpmGroupID(groupID);
        if(bpmGroupTypeResultWrapper.getList() != null)
            result.addAll(bpmGroupTypeResultWrapper.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(BpmGroupMemberType datagramObject, IUnknownDatagramsCollector collector) {
        BpmGroupMemberType.EmployeeID ownerID = datagramObject.getEmployeeID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        if(owner != null)
            collector.check(owner.getID(), EmployeeType.class);

        BpmGroupMemberType.BpmGroupID groupID = datagramObject.getBpmGroupID();
        BpmGroupType groupType = groupID == null ? null : groupID.getBpmGroup();
        if(groupType != null)
            collector.check(groupType.getID(), BpmGroupType.class);
    }

    protected BpmGroupMember findBpmGroupMember(Long groupId, Long employeePostId)
    {
        if(groupId == null || employeePostId == null)
            return null;
        List<BpmGroupMember> list = findEntity(and(
                eq(property(ENTITY_ALIAS, BpmGroupMember.bpmGroup().id()), value(groupId)),
                eq(property(ENTITY_ALIAS, BpmGroupMember.employeePost().id()), value(employeePostId))
        ));
        if(list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<BpmGroupMember> processDatagramObject(BpmGroupMemberType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        // EmployeeID init
        BpmGroupMemberType.EmployeeID ownerID = datagramObject.getEmployeeID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        EmployeePost post = getOrCreate(EmployeeType.class, params, commonCache, owner, EMPLOYEE, null, warning);

        // BpmGroupID init
        BpmGroupMemberType.BpmGroupID groupID = datagramObject.getBpmGroupID();
        BpmGroupType groupType = groupID == null ? null : groupID.getBpmGroup();
        BpmGroup group = getOrCreate(BpmGroupType.class, params, commonCache, groupType, BPM_GROUP, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        BpmGroupMember entity = null;
        CoreCollectionUtils.Pair<BpmGroupMember, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findBpmGroupMember(group == null ? null : group.getId(), post == null ? null : post.getId());
        if(entity == null)
        {
            entity = new BpmGroupMember();
            isNew = true;
            isChanged = true;
        }

        // EmployeeID set
        if (null == post && isNew)
            throw new ProcessingDatagramObjectException("BpmGroupMemberType object without EmployeeID!");
        if(post != null && !post.equals(entity.getEmployeePost()))
        {
            entity.setEmployeePost(post);
            isChanged = true;
        }

        // BpmGroupID set
        if (null == group && isNew)
            throw new ProcessingDatagramObjectException("BpmGroupMemberType object without BpmGroupID!");
        if(group != null && !group.equals(entity.getBpmGroup()))
        {
            isChanged = true;
            entity.setBpmGroup(group);
        }

        if(isNew)
            entity.setPriority(BpmGroupManager.instance().modifyDao().findBpmGroupMaxShowIndex(group) + 1000);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<BpmGroupMember> w, BpmGroupMemberType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(EmployeeType.class, session, params, EMPLOYEE, nsiPackage);
            saveChildEntity(BpmGroupType.class, session, params, BPM_GROUP, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(BpmGroupMember.P_PRIORITY);
        return  fields;
    }
}
