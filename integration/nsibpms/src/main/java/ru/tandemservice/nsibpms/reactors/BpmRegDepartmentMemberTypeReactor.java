/* $Id: BpmRegDepartmentMemberReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsibpms.reactors;

import org.hibernate.Session;
import org.tandemframework.bpms.base.entity.BpmRegOrgUnit;
import org.tandemframework.bpms.base.entity.BpmRegOrgUnitMember;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.BpmRegDepartmentMemberType;
import ru.tandemservice.nsiclient.datagram.BpmRegDepartmentType;
import ru.tandemservice.nsiclient.datagram.EmployeeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiemployeebase.reactors.EmployeeTypeReactor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.05.2015
 */
public class BpmRegDepartmentMemberTypeReactor extends BaseEntityReactor<BpmRegOrgUnitMember, BpmRegDepartmentMemberType>
{
    private static final String EMPLOYEE = "EmployeeID";
    private static final String BPM_REG_DEPARTMENT = "BpmRegDepartmentID";

    @Override
    public Class<BpmRegOrgUnitMember> getEntityClass()
    {
        return BpmRegOrgUnitMember.class;
    }

    @Override
    public BpmRegDepartmentMemberType createDatagramObject(String guid)
    {
        BpmRegDepartmentMemberType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentMemberType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(BpmRegOrgUnitMember entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        BpmRegDepartmentMemberType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentMemberType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        StringBuilder warning = new StringBuilder();

        datagramObject.setBpmRegDepartmentMemberHead(NsiUtils.formatBoolean(entity.isHeader()));
        datagramObject.setBpmRegDepartmentMemberClerk(NsiUtils.formatBoolean(entity.isClerk()));
        datagramObject.setBpmRegDepartmentMemberAdmin(NsiUtils.formatBoolean(entity.isAdmin()));

        ProcessedDatagramObject<EmployeeType> employeeType = createDatagramObject(EmployeeType.class, entity.getPersonEmpl(), commonCache, warning);
        BpmRegDepartmentMemberType.EmployeeID employeeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentMemberTypeEmployeeID();
        employeeID.setEmployee(employeeType.getObject());
        datagramObject.setEmployeeID(employeeID);
        if(employeeType.getList() != null)
            result.addAll(employeeType.getList());

        ProcessedDatagramObject<BpmRegDepartmentType> bpmRegDepartmentType= createDatagramObject(BpmRegDepartmentType.class, entity.getBpmRegOrgUnit(), commonCache, warning);
        BpmRegDepartmentMemberType.BpmRegDepartmentID regDepartmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmRegDepartmentMemberTypeBpmRegDepartmentID();
        regDepartmentID.setBpmRegDepartment(bpmRegDepartmentType.getObject());
        datagramObject.setBpmRegDepartmentID(regDepartmentID);
        if(bpmRegDepartmentType.getList() != null)
            result.addAll(bpmRegDepartmentType.getList());


        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(BpmRegDepartmentMemberType datagramObject, IUnknownDatagramsCollector collector) {
        BpmRegDepartmentMemberType.EmployeeID ownerID = datagramObject.getEmployeeID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        if(owner != null)
            collector.check(owner.getID(), EmployeeType.class);

        BpmRegDepartmentMemberType.BpmRegDepartmentID regDepartmentID = datagramObject.getBpmRegDepartmentID();
        BpmRegDepartmentType regDepartment = regDepartmentID == null ? null : regDepartmentID.getBpmRegDepartment();
        if(regDepartment != null)
            collector.check(regDepartment.getID(), BpmRegDepartmentType.class);
    }

    protected BpmRegOrgUnitMember findBpmRegOrgUnitMember(Long bpmRegOrgUnitId, Long employeePostId)
    {
        if(bpmRegOrgUnitId == null || employeePostId == null)
            return null;
        List<BpmRegOrgUnitMember> list = findEntity(and(
                eq(property(ENTITY_ALIAS, BpmRegOrgUnitMember.bpmRegOrgUnit().id()), value(bpmRegOrgUnitId)),
                eq(property(ENTITY_ALIAS, BpmRegOrgUnitMember.personEmpl().id()), value(employeePostId))
        ));
        if(list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<BpmRegOrgUnitMember> processDatagramObject(BpmRegDepartmentMemberType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // BpmRegDepartmentMemberHead init
        Boolean head = NsiUtils.parseBoolean(datagramObject.getBpmRegDepartmentMemberHead());
        // BpmRegDepartmentMemberClerk init
        Boolean clerk = NsiUtils.parseBoolean(datagramObject.getBpmRegDepartmentMemberClerk());
        // BpmRegDepartmentMemberAdmin init
        Boolean admin = NsiUtils.parseBoolean(datagramObject.getBpmRegDepartmentMemberAdmin());
        // EmployeeID init
        BpmRegDepartmentMemberType.EmployeeID ownerID = datagramObject.getEmployeeID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        EmployeePost post = getOrCreate(EmployeeType.class, params, commonCache, owner, EMPLOYEE, null, warning);

        // BpmRegDepartmentID init
        BpmRegDepartmentMemberType.BpmRegDepartmentID regDepartmentID = datagramObject.getBpmRegDepartmentID();
        BpmRegDepartmentType regDepartment = regDepartmentID == null ? null : regDepartmentID.getBpmRegDepartment();
        BpmRegOrgUnit regOrgUnit = getOrCreate(BpmRegDepartmentType.class, params, commonCache, regDepartment, BPM_REG_DEPARTMENT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        BpmRegOrgUnitMember entity = null;
        CoreCollectionUtils.Pair<BpmRegOrgUnitMember, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findBpmRegOrgUnitMember(regOrgUnit == null ? null : regOrgUnit.getId(), post == null ? null : post.getId());
        if(entity == null)
        {
            entity = new BpmRegOrgUnitMember();
            isNew = true;
            isChanged = true;
        }

        // BpmRegDepartmentMemberHead set
        if(head != null && head != entity.isHeader())
        {
            isChanged = true;
            entity.setHeader(head);
        }
        // BpmRegDepartmentMemberClerk set
        if(clerk != null && clerk != entity.isClerk())
        {
            isChanged = true;
            entity.setClerk(clerk);
        }
        // BpmRegDepartmentMemberAdmin set
        if(admin != null && admin != entity.isAdmin())
        {
            isChanged = true;
            entity.setAdmin(admin);
        }

        // EmployeeID set
        if (null == post && isNew)
            throw new ProcessingDatagramObjectException("BpmRegDepartmentMemberType object without EmployeeID!");
        if(post != null && !post.equals(entity.getPersonEmpl()))
        {
            isChanged = true;
            entity.setPersonEmpl(post);
        }

        // BpmRegDepartmentID set
        if (null == regOrgUnit && isNew)
            throw new ProcessingDatagramObjectException("BpmRegDepartmentMemberType object without BpmRegDepartmentID!");
        if(regOrgUnit != null && !regOrgUnit.equals(entity.getBpmRegOrgUnit()))
        {
            isChanged = true;
            entity.setBpmRegOrgUnit(regOrgUnit);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<BpmRegOrgUnitMember> w, BpmRegDepartmentMemberType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null) {
            saveChildEntity(EmployeeType.class, session, params, EMPLOYEE, nsiPackage);
            saveChildEntity(BpmRegDepartmentType.class, session, params, BPM_REG_DEPARTMENT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
