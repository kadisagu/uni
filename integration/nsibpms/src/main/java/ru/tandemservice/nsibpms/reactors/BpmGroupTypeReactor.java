/* $Id: BpmGroupTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsibpms.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.bpms.base.entity.BpmGroup;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.BpmGroupType;
import ru.tandemservice.nsiclient.datagram.EmployeeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.05.2015
 */
public class BpmGroupTypeReactor extends BaseEntityReactor<BpmGroup, BpmGroupType>
{
    private static final String OWNER = "OwnerID";

    @Override
    public Class<BpmGroup> getEntityClass()
    {
        return BpmGroup.class;
    }

    @Override
    public BpmGroupType createDatagramObject(String guid)
    {
        BpmGroupType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(BpmGroup entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        BpmGroupType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setBpmGroupName(entity.getTitle());
        datagramObject.setBpmGroupDescription(entity.getDesc());

        StringBuilder warning = new StringBuilder();
        if (entity.getEmployeePost() != null)
        {
            ProcessedDatagramObject<EmployeeType> r = createDatagramObject(EmployeeType.class, entity.getEmployeePost(), commonCache, warning);
            EmployeeType employeeType = r.getObject();
            BpmGroupType.OwnerID ownerID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createBpmGroupTypeOwnerID();
            ownerID.setEmployee(employeeType);
            datagramObject.setOwnerID(ownerID);

            if (r.getList() != null)
                result.addAll(r.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(BpmGroupType datagramObject, IUnknownDatagramsCollector collector)
    {
        BpmGroupType.OwnerID ownerID = datagramObject.getOwnerID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        if (owner != null)
            collector.check(owner.getID(), EmployeeType.class);
    }

    protected BpmGroup findBpmGroup(String title, Long ownerId)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (title != null)
            expressions.add(eq(property(ENTITY_ALIAS, BpmGroup.title()), value(title)));
        if (ownerId != null)
            expressions.add(eq(property(ENTITY_ALIAS, BpmGroup.employeePost().id()), value(ownerId)));
        List<BpmGroup> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<BpmGroup> processDatagramObject(BpmGroupType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // BpmGroupDescription init
        String desc = StringUtils.trimToNull(datagramObject.getBpmGroupDescription());
        // BpmGroupName init
        String name = StringUtils.trimToNull(datagramObject.getBpmGroupName());
        // OwnerID init
        BpmGroupType.OwnerID ownerID = datagramObject.getOwnerID();
        EmployeeType owner = ownerID == null ? null : ownerID.getEmployee();
        EmployeePost post = getOrCreate(EmployeeType.class, params, commonCache, owner, OWNER, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        BpmGroup entity = null;
        CoreCollectionUtils.Pair<BpmGroup, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null && (post == null || post.getId() != null))
            entity = findBpmGroup(name, post == null ? null : post.getId());
        if (entity == null)
        {
            entity = new BpmGroup();
            isNew = true;
            isChanged = true;
        }

        // BpmGroupDescription set
        if (desc != null && !desc.equals(entity.getDesc()))
        {
            isChanged = true;
            entity.setDesc(desc);
        }

        // BpmGroupName set
        if (null == name && isNew)
            throw new ProcessingDatagramObjectException("BpmGroupType object without BpmGroupName!");
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }

        // OwnerID set
        if (post != null && !post.equals(entity.getEmployeePost()))
        {
            isChanged = true;
            entity.setEmployeePost(post);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<BpmGroup> w, BpmGroupType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EmployeeType.class, session, params, OWNER, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

}
