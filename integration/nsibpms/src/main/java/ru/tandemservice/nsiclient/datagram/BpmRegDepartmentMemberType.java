//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.07.14 at 03:43:31 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


/**
 * Элемент справочника Участник регистрирующего подразделения
 * 
 * <p>Java class for BpmRegDepartmentMemberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BpmRegDepartmentMemberType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="BpmRegDepartmentMemberHead" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *         &lt;element name="BpmRegDepartmentMemberClerk" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *         &lt;element name="BpmRegDepartmentMemberAdmin" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *         &lt;element name="BpmRegDepartmentID">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="BpmRegDepartment" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}BpmRegDepartmentType"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EmployeeID">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Employee" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EmployeeType"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BpmRegDepartmentMemberType", propOrder = {

})
public class BpmRegDepartmentMemberType implements IDatagramObject
{

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "BpmRegDepartmentMemberHead")
    protected String bpmRegDepartmentMemberHead;
    @XmlElement(name = "BpmRegDepartmentMemberClerk")
    protected String bpmRegDepartmentMemberClerk;
    @XmlElement(name = "BpmRegDepartmentMemberAdmin")
    protected String bpmRegDepartmentMemberAdmin;
    @XmlElement(name = "BpmRegDepartmentID", required = true)
    protected BpmRegDepartmentID bpmRegDepartmentID;
    @XmlElement(name = "EmployeeID", required = true)
    protected EmployeeID employeeID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the bpmRegDepartmentMemberHead property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBpmRegDepartmentMemberHead() {
        return bpmRegDepartmentMemberHead;
    }

    /**
     * Sets the value of the bpmRegDepartmentMemberHead property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBpmRegDepartmentMemberHead(String value) {
        this.bpmRegDepartmentMemberHead = value;
    }

    /**
     * Gets the value of the bpmRegDepartmentMemberClerk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBpmRegDepartmentMemberClerk() {
        return bpmRegDepartmentMemberClerk;
    }

    /**
     * Sets the value of the bpmRegDepartmentMemberClerk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBpmRegDepartmentMemberClerk(String value) {
        this.bpmRegDepartmentMemberClerk = value;
    }

    /**
     * Gets the value of the bpmRegDepartmentMemberAdmin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBpmRegDepartmentMemberAdmin() {
        return bpmRegDepartmentMemberAdmin;
    }

    /**
     * Sets the value of the bpmRegDepartmentMemberAdmin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBpmRegDepartmentMemberAdmin(String value) {
        this.bpmRegDepartmentMemberAdmin = value;
    }

    /**
     * Gets the value of the bpmRegDepartmentID property.
     * 
     * @return
     *     possible object is
     *     {@link BpmRegDepartmentID }
     *     
     */
    public BpmRegDepartmentID getBpmRegDepartmentID() {
        return bpmRegDepartmentID;
    }

    /**
     * Sets the value of the bpmRegDepartmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BpmRegDepartmentID }
     *     
     */
    public void setBpmRegDepartmentID(BpmRegDepartmentID value) {
        this.bpmRegDepartmentID = value;
    }

    /**
     * Gets the value of the employeeID property.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeID }
     *     
     */
    public EmployeeID getEmployeeID() {
        return employeeID;
    }

    /**
     * Sets the value of the employeeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeID }
     *     
     */
    public void setEmployeeID(EmployeeID value) {
        this.employeeID = value;
    }

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="BpmRegDepartment" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}BpmRegDepartmentType"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class BpmRegDepartmentID {

        @XmlElement(name = "BpmRegDepartment", required = true)
        protected BpmRegDepartmentType bpmRegDepartment;

        /**
         * Gets the value of the bpmRegDepartment property.
         * 
         * @return
         *     possible object is
         *     {@link BpmRegDepartmentType }
         *     
         */
        public BpmRegDepartmentType getBpmRegDepartment() {
            return bpmRegDepartment;
        }

        /**
         * Sets the value of the bpmRegDepartment property.
         * 
         * @param value
         *     allowed object is
         *     {@link BpmRegDepartmentType }
         *     
         */
        public void setBpmRegDepartment(BpmRegDepartmentType value) {
            this.bpmRegDepartment = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Employee" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EmployeeType"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EmployeeID {

        @XmlElement(name = "Employee", required = true)
        protected EmployeeType employee;

        /**
         * Gets the value of the employee property.
         * 
         * @return
         *     possible object is
         *     {@link EmployeeType }
         *     
         */
        public EmployeeType getEmployee() {
            return employee;
        }

        /**
         * Sets the value of the employee property.
         * 
         * @param value
         *     allowed object is
         *     {@link EmployeeType }
         *     
         */
        public void setEmployee(EmployeeType value) {
            this.employee = value;
        }

    }

}
