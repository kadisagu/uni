//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.09.25 at 05:41:01 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


/**
 * Элемент справочника Набор ОП для приема
 * 
 * <p>Java class for EnrProgramSetBaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnrProgramSetBaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EnrProgramSetBaseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnrEnrollmentCampaignID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EnrEnrollmentCampaign" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EnrEnrollmentCampaignType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EduProgramSubjectID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EduProgramSubject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramSubjectType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EduProgramFormID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EduProgramForm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramFormType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnrProgramSetBaseType", propOrder = {

})
public class EnrProgramSetBaseType implements IDatagramObject {

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EnrProgramSetBaseName")
    protected String enrProgramSetBaseName;
    @XmlElement(name = "EnrEnrollmentCampaignID")
    protected EnrProgramSetBaseType.EnrEnrollmentCampaignID enrEnrollmentCampaignID;
    @XmlElement(name = "EduProgramSubjectID")
    protected EnrProgramSetBaseType.EduProgramSubjectID eduProgramSubjectID;
    @XmlElement(name = "EduProgramFormID")
    protected EnrProgramSetBaseType.EduProgramFormID eduProgramFormID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the enrProgramSetBaseName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEnrProgramSetBaseName() {
        return enrProgramSetBaseName;
    }

    /**
     * Sets the value of the enrProgramSetBaseName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEnrProgramSetBaseName(String value) {
        this.enrProgramSetBaseName = value;
    }

    /**
     * Gets the value of the enrEnrollmentCampaignID property.
     *
     * @return
     *     possible object is
     *     {@link EnrProgramSetBaseType.EnrEnrollmentCampaignID }
     *
     */
    public EnrProgramSetBaseType.EnrEnrollmentCampaignID getEnrEnrollmentCampaignID() {
        return enrEnrollmentCampaignID;
    }

    /**
     * Sets the value of the enrEnrollmentCampaignID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrProgramSetBaseType.EnrEnrollmentCampaignID }
     *
     */
    public void setEnrEnrollmentCampaignID(EnrProgramSetBaseType.EnrEnrollmentCampaignID value) {
        this.enrEnrollmentCampaignID = value;
    }

    /**
     * Gets the value of the eduProgramSubjectID property.
     *
     * @return
     *     possible object is
     *     {@link EnrProgramSetBaseType.EduProgramSubjectID }
     *
     */
    public EnrProgramSetBaseType.EduProgramSubjectID getEduProgramSubjectID() {
        return eduProgramSubjectID;
    }

    /**
     * Sets the value of the eduProgramSubjectID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrProgramSetBaseType.EduProgramSubjectID }
     *
     */
    public void setEduProgramSubjectID(EnrProgramSetBaseType.EduProgramSubjectID value) {
        this.eduProgramSubjectID = value;
    }

    /**
     * Gets the value of the eduProgramFormID property.
     *
     * @return
     *     possible object is
     *     {@link EnrProgramSetBaseType.EduProgramFormID }
     *
     */
    public EnrProgramSetBaseType.EduProgramFormID getEduProgramFormID() {
        return eduProgramFormID;
    }

    /**
     * Sets the value of the eduProgramFormID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrProgramSetBaseType.EduProgramFormID }
     *
     */
    public void setEduProgramFormID(EnrProgramSetBaseType.EduProgramFormID value) {
        this.eduProgramFormID = value;
    }

    /**
     * Gets the value of the oid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     *
     * @return
     *     possible object is
     *     {@link java.math.BigInteger }
     *
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     *
     * @param value
     *     allowed object is
     *     {@link java.math.BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EduProgramForm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramFormType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EduProgramFormID {

        @XmlElement(name = "EduProgramForm")
        protected EduProgramFormType eduProgramForm;

        /**
         * Gets the value of the eduProgramForm property.
         * 
         * @return
         *     possible object is
         *     {@link EduProgramFormType }
         *     
         */
        public EduProgramFormType getEduProgramForm() {
            return eduProgramForm;
        }

        /**
         * Sets the value of the eduProgramForm property.
         * 
         * @param value
         *     allowed object is
         *     {@link EduProgramFormType }
         *     
         */
        public void setEduProgramForm(EduProgramFormType value) {
            this.eduProgramForm = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EduProgramSubject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramSubjectType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EduProgramSubjectID {

        @XmlElement(name = "EduProgramSubject")
        protected EduProgramSubjectType eduProgramSubject;

        /**
         * Gets the value of the eduProgramSubject property.
         * 
         * @return
         *     possible object is
         *     {@link EduProgramSubjectType }
         *     
         */
        public EduProgramSubjectType getEduProgramSubject() {
            return eduProgramSubject;
        }

        /**
         * Sets the value of the eduProgramSubject property.
         * 
         * @param value
         *     allowed object is
         *     {@link EduProgramSubjectType }
         *     
         */
        public void setEduProgramSubject(EduProgramSubjectType value) {
            this.eduProgramSubject = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EnrEnrollmentCampaign" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EnrEnrollmentCampaignType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EnrEnrollmentCampaignID {

        @XmlElement(name = "EnrEnrollmentCampaign")
        protected EnrEnrollmentCampaignType enrEnrollmentCampaign;

        /**
         * Gets the value of the enrEnrollmentCampaign property.
         * 
         * @return
         *     possible object is
         *     {@link EnrEnrollmentCampaignType }
         *     
         */
        public EnrEnrollmentCampaignType getEnrEnrollmentCampaign() {
            return enrEnrollmentCampaign;
        }

        /**
         * Sets the value of the enrEnrollmentCampaign property.
         * 
         * @param value
         *     allowed object is
         *     {@link EnrEnrollmentCampaignType }
         *     
         */
        public void setEnrEnrollmentCampaign(EnrEnrollmentCampaignType value) {
            this.enrEnrollmentCampaign = value;
        }

    }

}
