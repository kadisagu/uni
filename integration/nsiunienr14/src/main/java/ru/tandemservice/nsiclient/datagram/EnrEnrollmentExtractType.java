//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.09.25 at 06:23:56 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


/**
 * Элемент справочника Выписка приказа о зачислении абитуриентов
 * 
 * <p>Java class for EnrEnrollmentExtractType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnrEnrollmentExtractType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EduProgramSubjectID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EduProgramSubject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramSubjectType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EduProgramFormID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EduProgramForm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramFormType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EnrOrderID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EnrOrder" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EnrOrderType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EnrEnrollmentExtractNumber" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlInteger" minOccurs="0"/>
 *         &lt;element name="EnrRequestedCompetitionID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EnrRequestedCompetition" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EnrRequestedCompetitionType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EnrEnrollmentExtractCancelled" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnrEnrollmentExtractType", propOrder = {

})
public class EnrEnrollmentExtractType implements IDatagramObject {

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EduProgramSubjectID")
    protected EnrEnrollmentExtractType.EduProgramSubjectID eduProgramSubjectID;
    @XmlElement(name = "EduProgramFormID")
    protected EnrEnrollmentExtractType.EduProgramFormID eduProgramFormID;
    @XmlElement(name = "EnrOrderID")
    protected EnrEnrollmentExtractType.EnrOrderID enrOrderID;
    @XmlElement(name = "EnrEnrollmentExtractNumber")
    @XmlSchemaType(name = "anySimpleType")
    protected String enrEnrollmentExtractNumber;
    @XmlElement(name = "EnrRequestedCompetitionID")
    protected EnrEnrollmentExtractType.EnrRequestedCompetitionID enrRequestedCompetitionID;
    @XmlElement(name = "EnrEnrollmentExtractCancelled")
    @XmlSchemaType(name = "anySimpleType")
    protected String enrEnrollmentExtractCancelled;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the eduProgramSubjectID property.
     *
     * @return
     *     possible object is
     *     {@link EnrEnrollmentExtractType.EduProgramSubjectID }
     *
     */
    public EnrEnrollmentExtractType.EduProgramSubjectID getEduProgramSubjectID() {
        return eduProgramSubjectID;
    }

    /**
     * Sets the value of the eduProgramSubjectID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrEnrollmentExtractType.EduProgramSubjectID }
     *
     */
    public void setEduProgramSubjectID(EnrEnrollmentExtractType.EduProgramSubjectID value) {
        this.eduProgramSubjectID = value;
    }

    /**
     * Gets the value of the eduProgramFormID property.
     *
     * @return
     *     possible object is
     *     {@link EnrEnrollmentExtractType.EduProgramFormID }
     *
     */
    public EnrEnrollmentExtractType.EduProgramFormID getEduProgramFormID() {
        return eduProgramFormID;
    }

    /**
     * Sets the value of the eduProgramFormID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrEnrollmentExtractType.EduProgramFormID }
     *
     */
    public void setEduProgramFormID(EnrEnrollmentExtractType.EduProgramFormID value) {
        this.eduProgramFormID = value;
    }

    /**
     * Gets the value of the enrOrderID property.
     *
     * @return
     *     possible object is
     *     {@link EnrEnrollmentExtractType.EnrOrderID }
     *
     */
    public EnrEnrollmentExtractType.EnrOrderID getEnrOrderID() {
        return enrOrderID;
    }

    /**
     * Sets the value of the enrOrderID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrEnrollmentExtractType.EnrOrderID }
     *
     */
    public void setEnrOrderID(EnrEnrollmentExtractType.EnrOrderID value) {
        this.enrOrderID = value;
    }

    /**
     * Gets the value of the enrEnrollmentExtractNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEnrEnrollmentExtractNumber() {
        return enrEnrollmentExtractNumber;
    }

    /**
     * Sets the value of the enrEnrollmentExtractNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEnrEnrollmentExtractNumber(String value) {
        this.enrEnrollmentExtractNumber = value;
    }

    /**
     * Gets the value of the enrRequestedCompetitionID property.
     *
     * @return
     *     possible object is
     *     {@link EnrEnrollmentExtractType.EnrRequestedCompetitionID }
     *
     */
    public EnrEnrollmentExtractType.EnrRequestedCompetitionID getEnrRequestedCompetitionID() {
        return enrRequestedCompetitionID;
    }

    /**
     * Sets the value of the enrRequestedCompetitionID property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrEnrollmentExtractType.EnrRequestedCompetitionID }
     *
     */
    public void setEnrRequestedCompetitionID(EnrEnrollmentExtractType.EnrRequestedCompetitionID value) {
        this.enrRequestedCompetitionID = value;
    }

    /**
     * Gets the value of the enrEnrollmentExtractCancelled property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEnrEnrollmentExtractCancelled() {
        return enrEnrollmentExtractCancelled;
    }

    /**
     * Sets the value of the enrEnrollmentExtractCancelled property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEnrEnrollmentExtractCancelled(String value) {
        this.enrEnrollmentExtractCancelled = value;
    }

    /**
     * Gets the value of the oid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     *
     * @return
     *     possible object is
     *     {@link java.math.BigInteger }
     *
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     *
     * @param value
     *     allowed object is
     *     {@link java.math.BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EduProgramForm" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramFormType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EduProgramFormID {

        @XmlElement(name = "EduProgramForm")
        protected EduProgramFormType eduProgramForm;

        /**
         * Gets the value of the eduProgramForm property.
         * 
         * @return
         *     possible object is
         *     {@link EduProgramFormType }
         *     
         */
        public EduProgramFormType getEduProgramForm() {
            return eduProgramForm;
        }

        /**
         * Sets the value of the eduProgramForm property.
         * 
         * @param value
         *     allowed object is
         *     {@link EduProgramFormType }
         *     
         */
        public void setEduProgramForm(EduProgramFormType value) {
            this.eduProgramForm = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EduProgramSubject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduProgramSubjectType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EduProgramSubjectID {

        @XmlElement(name = "EduProgramSubject")
        protected EduProgramSubjectType eduProgramSubject;

        /**
         * Gets the value of the eduProgramSubject property.
         * 
         * @return
         *     possible object is
         *     {@link EduProgramSubjectType }
         *     
         */
        public EduProgramSubjectType getEduProgramSubject() {
            return eduProgramSubject;
        }

        /**
         * Sets the value of the eduProgramSubject property.
         * 
         * @param value
         *     allowed object is
         *     {@link EduProgramSubjectType }
         *     
         */
        public void setEduProgramSubject(EduProgramSubjectType value) {
            this.eduProgramSubject = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EnrOrder" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EnrOrderType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EnrOrderID {

        @XmlElement(name = "EnrOrder")
        protected EnrOrderType enrOrder;

        /**
         * Gets the value of the enrOrder property.
         * 
         * @return
         *     possible object is
         *     {@link EnrOrderType }
         *     
         */
        public EnrOrderType getEnrOrder() {
            return enrOrder;
        }

        /**
         * Sets the value of the enrOrder property.
         * 
         * @param value
         *     allowed object is
         *     {@link EnrOrderType }
         *     
         */
        public void setEnrOrder(EnrOrderType value) {
            this.enrOrder = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EnrRequestedCompetition" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EnrRequestedCompetitionType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EnrRequestedCompetitionID {

        @XmlElement(name = "EnrRequestedCompetition")
        protected EnrRequestedCompetitionType enrRequestedCompetition;

        /**
         * Gets the value of the enrRequestedCompetition property.
         * 
         * @return
         *     possible object is
         *     {@link EnrRequestedCompetitionType }
         *     
         */
        public EnrRequestedCompetitionType getEnrRequestedCompetition() {
            return enrRequestedCompetition;
        }

        /**
         * Sets the value of the enrRequestedCompetition property.
         * 
         * @param value
         *     allowed object is
         *     {@link EnrRequestedCompetitionType }
         *     
         */
        public void setEnrRequestedCompetition(EnrRequestedCompetitionType value) {
            this.enrRequestedCompetition = value;
        }

    }

}
