/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrBenefitTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 16.09.2015
 */
public class EnrEntrantRequestTypeReactor extends BaseEntityReactor<EnrEntrantRequest, EnrEntrantRequestType>
{
    private static final String ENTRANT = "entrantID";
    private static final String IDENTITY_CARD = "identityCardID";
    private static final String PERSON_EDU_DOCUMENT = "personEduDocumentID";

    @Override
    public Class<EnrEntrantRequest> getEntityClass()
    {
        return EnrEntrantRequest.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrantRequestType datagramObject, IUnknownDatagramsCollector collector)
    {
        EnrEntrantRequestType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        if (entrantType != null)
            collector.check(entrantType.getID(), EnrEntrantType.class);

        EnrEntrantRequestType.IdentityCardID identityCardID = datagramObject.getIdentityCardID();
        IdentityCardType identityCardType = identityCardID == null ? null : identityCardID.getIdentityCard();
        if (identityCardType != null)
            collector.check(identityCardType.getID(), IdentityCardType.class);

        EnrEntrantRequestType.PersonEduDocumentID personEduDocumentID = datagramObject.getPersonEduDocumentID();
        PersonEduDocumentType personEduDocumentType = personEduDocumentID == null ? null : personEduDocumentID.getPersonEduDocument();
        if (personEduDocumentType != null)
            collector.check(personEduDocumentType.getID(), PersonEduDocumentType.class);
    }

    @Override
    public EnrEntrantRequestType createDatagramObject(String guid)
    {
        EnrEntrantRequestType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantRequestType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantRequest entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrEntrantRequestType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantRequestType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEntrantRequestBenefitCategory(entity.getBenefitCategory() != null ? StringUtils.trimToNull(entity.getBenefitCategory().getCode()) : null);
        datagramObject.setEnrEntrantRequestEnrollmentCommission(entity.getEnrollmentCommission() != null ? StringUtils.trimToNull(entity.getEnrollmentCommission().getTitle()) : null);
        datagramObject.setEnrEntrantRequestEntrantAchievementMarkSum(String.valueOf(entity.getEntrantAchievementMarkSum()));
        datagramObject.setEnrEntrantRequestFiledByTrustee(NsiUtils.formatBoolean(entity.isFiledByTrustee()));
        datagramObject.setEnrEntrantRequestGeneralEduSchool(NsiUtils.formatBoolean(entity.isGeneralEduSchool()));
        datagramObject.setEnrEntrantRequestInternalExamReason(entity.getInternalExamReason() != null ? StringUtils.trimToNull(entity.getInternalExamReason().getTitle()) : null);
        datagramObject.setEnrEntrantRequestInternationalTreatyContractor(NsiUtils.formatBoolean(entity.isInternationalTreatyContractor()));
        datagramObject.setEnrEntrantRequestOriginalReturnWay(StringUtils.trimToNull(entity.getOriginalReturnWay().getTitle()));
        datagramObject.setEnrEntrantRequestOriginalSubmissionWay(StringUtils.trimToNull(entity.getOriginalSubmissionWay().getTitle()));
        datagramObject.setEnrEntrantRequestReceiveEduLevelFirst(NsiUtils.formatBoolean(entity.isReceiveEduLevelFirst()));
        datagramObject.setEnrEntrantRequestRegDate(NsiUtils.formatDate(entity.getRegDate()));
        datagramObject.setEnrEntrantRequestRegNumber(entity.getRegNumber());
        datagramObject.setEnrEntrantRequestType(entity.getType().getTitle());
        datagramObject.setEnrEntrantRequestTakeAwayDocumentDate(NsiUtils.formatDate(entity.getTakeAwayDocumentDate()));
        datagramObject.setEnrEntrantRequestEduInstDocOriginalRef(NsiUtils.formatBoolean(entity.isEduInstDocOriginalHandedIn()));

        ProcessedDatagramObject<EnrEntrantType> entrantType = createDatagramObject(EnrEntrantType.class, entity.getEntrant(), commonCache, warning);
        EnrEntrantRequestType.EnrEntrantID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantRequestTypeEnrEntrantID();
        entrantID.setEnrEntrant(entrantType.getObject());
        datagramObject.setEnrEntrantID(entrantID);
        if (entrantType.getList() != null)
            result.addAll(entrantType.getList());

        ProcessedDatagramObject<PersonEduDocumentType> personEduDocumentType = createDatagramObject(PersonEduDocumentType.class, entity.getEduDocument(), commonCache, warning);
        EnrEntrantRequestType.PersonEduDocumentID personEduDocumentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantRequestTypePersonEduDocumentID();
        personEduDocumentID.setPersonEduDocument(personEduDocumentType.getObject());
        datagramObject.setPersonEduDocumentID(personEduDocumentID);
        if (personEduDocumentType.getList() != null)
            result.addAll(personEduDocumentType.getList());

        ProcessedDatagramObject<IdentityCardType> identityCardType = createDatagramObject(IdentityCardType.class, entity.getIdentityCard(), commonCache, warning);
        EnrEntrantRequestType.IdentityCardID identityCardID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantRequestTypeIdentityCardID();
        identityCardID.setIdentityCard(identityCardType.getObject());
        datagramObject.setIdentityCardID(identityCardID);
        if (identityCardType.getList() != null)
            result.addAll(identityCardType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private EnrEntrantRequest findEnrEntrantRequest(EnrEntrant entrant, String reqNumber)
    {
        if (entrant != null && StringUtils.trimToNull(reqNumber) != null)
        {
            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantRequest.entrant()), value(entrant)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantRequest.regNumber()), value(reqNumber)));
            List<EnrEntrantRequest> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrantRequest> processDatagramObject(EnrEntrantRequestType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // PersonForeignLanguage init
        EnrEntrantRequestType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        EnrEntrant entrant = getOrCreate(EnrEntrantType.class, params, commonCache, entrantType, ENTRANT, null, warning);

        EnrEntrantRequestType.IdentityCardID identityCardID = datagramObject.getIdentityCardID();
        IdentityCardType identityCardType = identityCardID == null ? null : identityCardID.getIdentityCard();
        IdentityCard identityCard = getOrCreate(IdentityCardType.class, params, commonCache, identityCardType, IDENTITY_CARD, null, warning);

        EnrEntrantRequestType.PersonEduDocumentID personEduDocumentID = datagramObject.getPersonEduDocumentID();
        PersonEduDocumentType personEduDocumentType = personEduDocumentID == null ? null : personEduDocumentID.getPersonEduDocument();
        PersonEduDocument personEduDocument = getOrCreate(PersonEduDocumentType.class, params, commonCache, personEduDocumentType, PERSON_EDU_DOCUMENT, null, warning);

        String enrollmentComissionTitle = datagramObject.getEnrEntrantRequestEnrollmentCommission();
        EnrEnrollmentCommission enrollmentCommission = getEnrEnrollmentCommission(enrollmentComissionTitle);
        String benefitCategoryCode = datagramObject.getEnrEntrantRequestBenefitCategory();
        EnrBenefitCategory benefitCategory = getEnrBenefitCategory(benefitCategoryCode);
        Boolean filledByTrustee = NsiUtils.parseBoolean(datagramObject.getEnrEntrantRequestFiledByTrustee());
        Boolean generalEduSchool = NsiUtils.parseBoolean(datagramObject.getEnrEntrantRequestGeneralEduSchool());
        String internalExamReasonTitle = datagramObject.getEnrEntrantRequestInternalExamReason();
        EnrInternalExamReason internalExamReason = getEnrInternalExamReason(internalExamReasonTitle);
        Boolean internationalTreatyContractor = NsiUtils.parseBoolean(datagramObject.getEnrEntrantRequestInternationalTreatyContractor());
        String originalReturnWayTitle = datagramObject.getEnrEntrantRequestOriginalReturnWay();
        EnrOriginalSubmissionAndReturnWay originalReturnWay = getEnrOriginalSubmissionAndReturnWay(originalReturnWayTitle);
        String originalSubmissionWayTitle = datagramObject.getEnrEntrantRequestOriginalSubmissionWay();
        EnrOriginalSubmissionAndReturnWay originalSubmissionWay = getEnrOriginalSubmissionAndReturnWay(originalSubmissionWayTitle);
        Boolean receiveEduLevelFirst = NsiUtils.parseBoolean(datagramObject.getEnrEntrantRequestReceiveEduLevelFirst());
        Date regDate = NsiUtils.parseDate(datagramObject.getEnrEntrantRequestRegDate(), "EnrEntrantRequestRegDate");
        String regNumber = datagramObject.getEnrEntrantRequestRegNumber();
        Date takeAwayDocumentDate = NsiUtils.parseDate(datagramObject.getEnrEntrantRequestTakeAwayDocumentDate(), "EnrEntrantRequestTakeAwayDocumentDate");
        String requestTypeTitle = datagramObject.getEnrEntrantRequestType();
        EnrRequestType entrantRequestType = getEnrRequestType(requestTypeTitle);


        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantRequest entity = null;

        CoreCollectionUtils.Pair<EnrEntrantRequest, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrantRequest(entrant, regNumber);
        if (entity == null)
        {
            entity = new EnrEntrantRequest();
            isNew = true;
            isChanged = true;
        }

        // EnrEntrantRequestType set
        if (benefitCategory != null && !benefitCategory.equals(entity.getBenefitCategory()))
        {
            isChanged = true;
            entity.setBenefitCategory(benefitCategory);
        }
        if (personEduDocument != null && !personEduDocument.equals(entity.getEduDocument()))
        {
            isChanged = true;
            entity.setEduDocument(personEduDocument);
        }
        if (entity.getEduDocument() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without PersonEduDocumentID");
        }
        if (enrollmentCommission != null && !enrollmentCommission.equals(entity.getEnrollmentCommission()))
        {
            isChanged = true;
            entity.setEnrollmentCommission(enrollmentCommission);
        }
        if (entrant != null && !entrant.equals(entity.getEntrant()))
        {
            isChanged = true;
            entity.setEntrant(entrant);
        }
        if (entity.getEntrant() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without EnrEntrantID");
        }
        if (filledByTrustee != null && !filledByTrustee.equals(entity.isFiledByTrustee()))
        {
            isChanged = true;
            entity.setFiledByTrustee(filledByTrustee);
        }
        if (generalEduSchool != null && !generalEduSchool.equals(entity.isGeneralEduSchool()))
        {
            isChanged = true;
            entity.setGeneralEduSchool(generalEduSchool);
        }
        if (identityCard != null && !identityCard.equals(entity.getIdentityCard()))
        {
            isChanged = true;
            entity.setIdentityCard(identityCard);
        }
        if (entity.getIdentityCard() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without IdentityCardID");
        }
        if (internalExamReason != null && !internalExamReason.equals(entity.getInternalExamReason()))
        {
            isChanged = true;
            entity.setInternalExamReason(internalExamReason);
        }
        if (internationalTreatyContractor != null && !internationalTreatyContractor.equals(entity.isInternationalTreatyContractor()))
        {
            isChanged = true;
            entity.setInternationalTreatyContractor(internationalTreatyContractor);
        }
        if (originalReturnWay != null && !originalReturnWay.equals(entity.getOriginalReturnWay()))
        {
            isChanged = true;
            entity.setOriginalReturnWay(originalReturnWay);
        }
        if (entity.getOriginalReturnWay() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without EnrEntrantRequestOriginalReturnWay");
        }
        if (originalSubmissionWay != null && !originalSubmissionWay.equals(entity.getOriginalSubmissionWay()))
        {
            isChanged = true;
            entity.setOriginalSubmissionWay(originalSubmissionWay);
        }
        if (entity.getOriginalSubmissionWay() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without EnrEntrantRequestOriginalSubmissionWay");
        }

        if (receiveEduLevelFirst != null && !receiveEduLevelFirst.equals(entity.isReceiveEduLevelFirst()))
        {
            isChanged = true;
            entity.setReceiveEduLevelFirst(receiveEduLevelFirst);
        }

        if (regDate != null && !regDate.equals(entity.getRegDate()))
        {
            isChanged = true;
            entity.setRegDate(regDate);
        }
        if (entity.getRegDate() == null)
        {
            entity.setRegDate(new Date());
        }
        if (regNumber != null && !regNumber.equals(entity.getRegNumber()))
        {
            isChanged = true;
            entity.setRegNumber(regNumber);
        }
        if (entity.getRegNumber() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without EnrEntrantRequestRegNumber");
        }
        if (takeAwayDocumentDate != null && !takeAwayDocumentDate.equals(entity.getTakeAwayDocumentDate()))
        {
            isChanged = true;
            entity.setTakeAwayDocumentDate(takeAwayDocumentDate);
        }
        if (entrantRequestType != null && !entrantRequestType.equals(entity.getType()))
        {
            isChanged = true;
            entity.setType(entrantRequestType);
        }
        if (entity.getRegNumber() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType object without EnrEntrantRequestRegNumber");
        }

        if (entity.getEntrant() != null && entity.getRegNumber() != null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantRequest.class, entity, EnrEntrantRequest.L_ENTRANT, EnrEntrantRequest.P_REG_NUMBER))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType Entrant/EnrEntrantRequestRegNumber combination is not unique");
        }
        if (entity.getBenefitCategory() != null && !entity.getBenefitCategory().getBenefitType().getCode().equals(EnrBenefitTypeCodes.PREFERENCE))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType EnrEntrantRequestBenefitCategory type must be preference");
        }
        if (entity.getEntrant() != null && entity.getRegNumber() != null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantRequest.class, entity, EnrEntrantRequest.entrant().enrollmentCampaign().s(), EnrEntrantRequest.P_REG_NUMBER))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantRequestType Entrant/EnrEntrantRequestRegNumber combination is not unique");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    private static EnrEnrollmentCommission getEnrEnrollmentCommission(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrEnrollmentCommission.class, EnrEnrollmentCommission.title(), trimmedTitle);
        }
        return null;
    }

    private static EnrBenefitCategory getEnrBenefitCategory(String code)
    {
        String trimmedCode = StringUtils.trimToNull(code);
        if (!trimmedCode.isEmpty())
        {
            return DataAccessServices.dao().get(EnrBenefitCategory.class, EnrBenefitCategory.code(), trimmedCode);
        }
        return null;
    }

    private static EnrBenefitType getEnrBenefitType(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrBenefitType.class, EnrBenefitType.title(), trimmedTitle);
        }
        return null;
    }

    private static EnrInternalExamReason getEnrInternalExamReason(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrInternalExamReason.class, EnrInternalExamReason.title(), trimmedTitle);
        }
        return null;
    }

    private static EnrOriginalSubmissionAndReturnWay getEnrOriginalSubmissionAndReturnWay(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrOriginalSubmissionAndReturnWay.class, EnrOriginalSubmissionAndReturnWay.title(), trimmedTitle);
        }
        return null;
    }

    private static EnrRequestType getEnrRequestType(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrRequestType.class, EnrRequestType.title(), trimmedTitle);
        }
        return null;
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrEntrantRequest> w, EnrEntrantRequestType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEntrantType.class, session, params, ENTRANT, nsiPackage);
            saveChildEntity(IdentityCardType.class, session, params, IDENTITY_CARD, nsiPackage);
            saveChildEntity(PersonEduDocumentType.class, session, params, PERSON_EDU_DOCUMENT, nsiPackage);
        }
        super.save(session, w, datagramObject, nsiPackage);
    }
}
