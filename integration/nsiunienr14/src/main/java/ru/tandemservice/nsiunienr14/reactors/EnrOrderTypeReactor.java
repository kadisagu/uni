/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrEnrollmentCampaignType;
import ru.tandemservice.nsiclient.datagram.EnrOrderType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 15.09.2015
 */
public class EnrOrderTypeReactor extends BaseEntityReactor<EnrOrder, EnrOrderType> {
    private static final String ENR_ENROLLMENT_CAMPAIGN_ID = "EnrEnrollmentCampaignID";

    @Override
    public Class<EnrOrder> getEntityClass() {
        return EnrOrder.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrOrderType datagramObject, IUnknownDatagramsCollector collector) {
        EnrOrderType.EnrEnrollmentCampaignID enrEntrantID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrEnrollmentCampaign();
        if (entrantType != null) collector.check(entrantType.getID(), EnrEnrollmentCampaignType.class);
    }

    @Override
    public EnrOrderType createDatagramObject(String guid) {
        EnrOrderType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrOrderType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrOrder entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrOrderType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrOrderType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrOrderNumber(entity.getNumber());
        datagramObject.setEnrOrderCommitDate(NsiUtils.formatDate(entity.getCommitDate()));
        datagramObject.setEnrOrderActionDate(NsiUtils.formatDate(entity.getActionDate()));

        ProcessedDatagramObject<EnrEnrollmentCampaignType> ct = createDatagramObject(EnrEnrollmentCampaignType.class, entity.getEnrollmentCampaign(), commonCache, warning);
        EnrOrderType.EnrEnrollmentCampaignID competitionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrOrderTypeEnrEnrollmentCampaignID();
        competitionID.setEnrEnrollmentCampaign(ct.getObject());
        datagramObject.setEnrEnrollmentCampaignID(competitionID);
        if (ct.getList() != null)
            result.addAll(ct.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrOrder findEnrOrder(String number)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (number != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrOrder.number()), value(number)));
        List<EnrOrder> list = findEntity(EnrOrder.class, and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrOrder> processDatagramObject(EnrOrderType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        Date commitDate = NsiUtils.parseDate(datagramObject.getEnrOrderCommitDate(), "EnrOrderCommitDate");
        Date actionDate = NsiUtils.parseDate(datagramObject.getEnrOrderActionDate(), "EnrOrderActionDate");
        String number = StringUtils.trimToNull(datagramObject.getEnrOrderNumber());

        EnrOrderType.EnrEnrollmentCampaignID enrEntrantID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrEnrollmentCampaign();
        EnrEnrollmentCampaign campaign = getOrCreate(EnrEnrollmentCampaignType.class, params, commonCache, entrantType, ENR_ENROLLMENT_CAMPAIGN_ID, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrOrder entity = null;

        CoreCollectionUtils.Pair<EnrOrder, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrOrder(number);
        if (entity == null)
        {
            entity = new EnrOrder();
            entity.setCreateDate(new Date());
            entity.setState(DataAccessServices.dao().get(OrderStates.class, OrderStates.code(), OrderStatesCodes.FORMING));
            entity.setCompensationType(DataAccessServices.dao().get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
            entity.setEnrollmentCampaign(campaign);
            entity.setPrintFormType(DataAccessServices.dao().get(EnrOrderPrintFormType.class, EnrOrderPrintFormType.code(), EnrOrderPrintFormTypeCodes.BASE_ENROLLMENT));
            entity.setRequestType(DataAccessServices.dao().get(EnrRequestType.class, EnrRequestType.code(), EnrRequestTypeCodes.BS));
            entity.setType(DataAccessServices.dao().get(ru.tandemservice.unienr14.catalog.entity.EnrOrderType.class, ru.tandemservice.unienr14.catalog.entity.EnrOrderType.code(), EnrOrderTypeCodes.ENROLLMENT));

            isNew = true;
            isChanged = true;
        }

        if (commitDate != null && !commitDate.equals(entity.getCommitDate()))
        {
            isChanged = true;
            entity.setCommitDate(commitDate);
        }
        if (actionDate != null && !actionDate.equals(entity.getActionDate()))
        {
            isChanged = true;
            entity.setActionDate(actionDate);
        }
        if (campaign != null && !campaign.equals(entity.getEnrollmentCampaign()))
        {
            isChanged = true;
            entity.setEnrollmentCampaign(campaign);
        }
        if (null == entity.getEnrollmentCampaign())
            throw new ProcessingDatagramObjectException("EnrOrder object without EnrEnrollmentCampaignID!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrOrder> w, EnrOrderType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEnrollmentCampaignType.class, session, params, ENR_ENROLLMENT_CAMPAIGN_ID, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
