/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrEntrantBenefitProofType;
import ru.tandemservice.nsiclient.datagram.EnrEntrantType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 15.09.2015
 */
public class EnrEntrantBenefitProofTypeReactor extends BaseEntityReactor<EnrEntrantBenefitProof, EnrEntrantBenefitProofType> {
    private static final String ENTRANT = "EnrEntrantID";

    @Override
    public Class<EnrEntrantBenefitProof> getEntityClass() {
        return EnrEntrantBenefitProof.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrantBenefitProofType datagramObject, IUnknownDatagramsCollector collector) {
        EnrEntrantBenefitProofType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        if (entrantType != null) collector.check(entrantType.getID(), EnrEntrantType.class);
    }

    @Override
    public EnrEntrantBenefitProofType createDatagramObject(String guid) {
        EnrEntrantBenefitProofType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantBenefitProofType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantBenefitProof entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrEntrantBenefitProofType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantBenefitProofType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        if(entity.getBenefitStatement().getBenefitCategory() != null)
            datagramObject.setEnrEntrantBenefitProofBenefitCategory(entity.getBenefitStatement().getBenefitCategory().getTitleWithType());
        if(entity.getDocument().getDocumentType() != null)
            datagramObject.setEnrEntrantBenefitProofDocumentType(entity.getDocument().getDocumentType().getTitle());
        datagramObject.setEnrEntrantBenefitProofNumber(entity.getDocument().getNumber());
        datagramObject.setEnrEntrantBenefitProofSeria(entity.getDocument().getSeria());
        datagramObject.setEnrEntrantBenefitProofIssuancePlace(entity.getDocument().getIssuancePlace());
        datagramObject.setEnrEntrantBenefitProofIssuanceDate(NsiUtils.formatDate(entity.getDocument().getIssuanceDate()));

        if(entity.getBenefitStatement().getEntrant() != null) {
            ProcessedDatagramObject<EnrEntrantType> entrantType = createDatagramObject(EnrEntrantType.class, entity.getBenefitStatement().getEntrant(), commonCache, warning);
            EnrEntrantBenefitProofType.EnrEntrantID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantBenefitProofTypeEnrEntrantID();
            entrantID.setEnrEntrant(entrantType.getObject());
            datagramObject.setEnrEntrantID(entrantID);
            if (entrantType.getList() != null)
                result.addAll(entrantType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrEntrantBenefitProof findEnrEntrantBenefitProof(EnrEntrant entrant, String seria, String number, String documentType)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (entrant != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantBenefitProof.benefitStatement().entrant().id()), value(entrant.getId())));
        if (seria != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantBenefitProof.document().seria()), value(seria)));
        if (number != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantBenefitProof.document().number()), value(number)));
        if (documentType != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantBenefitProof.document().documentType().title()), value(documentType)));
        List<EnrEntrantBenefitProof> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrantBenefitProof> processDatagramObject(EnrEntrantBenefitProofType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        Date issuanceDate = NsiUtils.parseDate(datagramObject.getEnrEntrantBenefitProofIssuanceDate(), "EnrEntrantBenefitProofIssuanceDate");
        String issuancePlace = datagramObject.getEnrEntrantBenefitProofIssuancePlace();
        String benefitCategory = datagramObject.getEnrEntrantBenefitProofBenefitCategory();
        String seria = datagramObject.getEnrEntrantBenefitProofSeria();
        String number = datagramObject.getEnrEntrantBenefitProofNumber();
        String documentType = datagramObject.getEnrEntrantBenefitProofDocumentType();

        EnrEntrantBenefitProofType.EnrEntrantID enrEntrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrEntrant();
        EnrEntrant entrant = getOrCreate(EnrEntrantType.class, params, commonCache, entrantType, ENTRANT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantBenefitProof entity = null;

        CoreCollectionUtils.Pair<EnrEntrantBenefitProof, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrantBenefitProof(entrant, seria, number, documentType);
        if (entity == null)
        {
            throw new ProcessingDatagramObjectException("Creation object for type EnrEntrantBenefitProofType is unsupported");
        }

        if (issuanceDate != null && !issuanceDate.equals(entity.getDocument().getIssuanceDate())) {
            // todo доделать заполнение сущности
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }
}
