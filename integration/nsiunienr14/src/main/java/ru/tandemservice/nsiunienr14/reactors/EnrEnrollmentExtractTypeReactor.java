/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 17.09.2015
 */
public class EnrEnrollmentExtractTypeReactor extends BaseEntityReactor<EnrEnrollmentExtract, EnrEnrollmentExtractType> {
    private static final String PROGRAM_FORM = "EduProgramFormID";
    private static final String PROGRAM_SUBJECT = "EduProgramSubjectID";
    private static final String ORDER = "EnrOrderID";
    private static final String COMPETITION = "EnrRequestedCompetitionID";
    private static final String EDU_INST_ORG_UNIT = "EduInstitutionOrgUnit";
    private static final String ENR_ORG_UNIT = "enrOrgUnit";
    private static final String PARAGRAPH = "para";

    @Override
    public Class<EnrEnrollmentExtract> getEntityClass() {
        return EnrEnrollmentExtract.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEnrollmentExtractType datagramObject, IUnknownDatagramsCollector collector) {
        EnrEnrollmentExtractType.EduProgramFormID programFormID = datagramObject.getEduProgramFormID();
        EduProgramFormType programFormType = programFormID == null ? null : programFormID.getEduProgramForm();
        if (programFormType != null)
            collector.check(programFormType.getID(), EduProgramFormType.class);

        EnrEnrollmentExtractType.EduProgramSubjectID programSubjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType programSubjectType = programSubjectID == null ? null : programSubjectID.getEduProgramSubject();
        if (programSubjectType != null)
            collector.check(programSubjectType.getID(), EduProgramSubjectType.class);

        EnrEnrollmentExtractType.EnrOrderID enrOrderID = datagramObject.getEnrOrderID();
        EnrOrderType orderType = enrOrderID == null ? null : enrOrderID.getEnrOrder();
        if (orderType != null)
            collector.check(orderType.getID(), EnrOrderType.class);

        EnrEnrollmentExtractType.EnrRequestedCompetitionID enrEntrantID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrRequestedCompetition();
        if (entrantType != null)
            collector.check(entrantType.getID(), EnrRequestedCompetitionType.class);
    }

    @Override
    public EnrEnrollmentExtractType createDatagramObject(String guid) {
        EnrEnrollmentExtractType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentExtractType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEnrollmentExtract entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrEnrollmentExtractType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentExtractType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEnrollmentExtractNumber(Integer.toString(entity.getParagraph().getNumber()));
        datagramObject.setEnrEnrollmentExtractCancelled(NsiUtils.formatBoolean(entity.isCancelled()));

        if(entity.getParagraph() != null) {
            ProcessedDatagramObject<EnrOrderType> campaignType = createDatagramObject(EnrOrderType.class, entity.getParagraph().getOrder(), commonCache, warning);
            EnrEnrollmentExtractType.EnrOrderID orderID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentExtractTypeEnrOrderID();
            orderID.setEnrOrder(campaignType.getObject());
            datagramObject.setEnrOrderID(orderID);
            if (campaignType.getList() != null)
                result.addAll(campaignType.getList());
        }

        if(entity.getParagraph() != null && entity.getParagraph() instanceof EnrEnrollmentParagraph) {
            ProcessedDatagramObject<EduProgramFormType> formType = createDatagramObject(EduProgramFormType.class, ((EnrEnrollmentParagraph) entity.getParagraph()).getProgramForm(), commonCache, warning);
            EnrEnrollmentExtractType.EduProgramFormID programFormID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentExtractTypeEduProgramFormID();
            programFormID.setEduProgramForm(formType.getObject());
            datagramObject.setEduProgramFormID(programFormID);
            if (formType.getList() != null)
                result.addAll(formType.getList());

            ProcessedDatagramObject<EduProgramSubjectType> subjectType = createDatagramObject(EduProgramSubjectType.class, ((EnrEnrollmentParagraph) entity.getParagraph()).getProgramSubject(), commonCache, warning);
            EnrEnrollmentExtractType.EduProgramSubjectID programSubjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentExtractTypeEduProgramSubjectID();
            programSubjectID.setEduProgramSubject(subjectType.getObject());
            datagramObject.setEduProgramSubjectID(programSubjectID);
            if (subjectType.getList() != null)
                result.addAll(subjectType.getList());
        }

        if(entity.getRequestedCompetition() != null) {
            ProcessedDatagramObject<EnrRequestedCompetitionType> competitionType = createDatagramObject(EnrRequestedCompetitionType.class, entity.getRequestedCompetition(), commonCache, warning);
            EnrEnrollmentExtractType.EnrRequestedCompetitionID competitionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentExtractTypeEnrRequestedCompetitionID();
            competitionID.setEnrRequestedCompetition(competitionType.getObject());
            datagramObject.setEnrRequestedCompetitionID(competitionID);
            if (competitionType.getList() != null)
                result.addAll(competitionType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrEnrollmentExtract findEnrEnrollmentExtract(EnrOrder order, Integer number) {
        List<IExpression> expressions = new ArrayList<>();
        if (order != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEnrollmentExtract.entity().id()), value(order.getId())));
        if (number != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEnrollmentExtract.number()), value(number)));
        List<EnrEnrollmentExtract> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrEnrollmentExtract> processDatagramObject(EnrEnrollmentExtractType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        Integer number = NsiUtils.parseInteger(datagramObject.getEnrEnrollmentExtractNumber(), "EnrEnrollmentExtractNumber");
        Boolean cancelled = NsiUtils.parseBoolean(datagramObject.getEnrEnrollmentExtractCancelled());
        EnrEnrollmentExtractType.EduProgramFormID programFormID = datagramObject.getEduProgramFormID();
        EduProgramFormType programFormType = programFormID == null ? null : programFormID.getEduProgramForm();
        EduProgramForm programForm = getOrCreate(EduProgramFormType.class, params, commonCache, programFormType, PROGRAM_FORM, null, warning);

        EnrEnrollmentExtractType.EduProgramSubjectID programSubjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType programSubjectType = programSubjectID == null ? null : programSubjectID.getEduProgramSubject();
        EduProgramSubject programSubject = getOrCreate(EduProgramSubjectType.class, params, commonCache, programSubjectType, PROGRAM_SUBJECT, null, warning);

        EnrEnrollmentExtractType.EnrOrderID enrOrderID = datagramObject.getEnrOrderID();
        EnrOrderType orderType = enrOrderID == null ? null : enrOrderID.getEnrOrder();
        EnrOrder order = getOrCreate(EnrOrderType.class, params, commonCache, orderType, ORDER, null, warning);

        EnrEnrollmentExtractType.EnrRequestedCompetitionID enrEntrantID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrRequestedCompetition();
        EnrRequestedCompetition competition = getOrCreate(EnrRequestedCompetitionType.class, params, commonCache, entrantType, COMPETITION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        boolean isParagraphChanged = false;
        EnrEnrollmentExtract entity = null;
        EnrAbstractParagraph abstractParagraph;

        CoreCollectionUtils.Pair<EnrEnrollmentExtract, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEnrollmentExtract(order, number);
        if (entity == null)
        {
            entity = new EnrEnrollmentExtract();
            entity.setCreateDate(new Date());
            entity.setState(DataAccessServices.dao().get(ExtractStates.class, ExtractStates.code(), ExtractStatesCodes.FORMING));

            EnrEnrollmentParagraph paragraph = new EnrEnrollmentParagraph();
            paragraph.setNumber(0);
            paragraph.setOrder(order);
            OrgUnit orgUnit = TopOrgUnit.getInstance();
            EduInstitutionOrgUnit ou = DataAccessServices.dao().get(EduInstitutionOrgUnit.class, EduInstitutionOrgUnit.id(), orgUnit.getId());
            EnrOrgUnit enrOrgUnit;
            if(ou == null) {
                ou = new EduInstitutionOrgUnit();
                ou.setOrgUnit(orgUnit);
                params.put(EDU_INST_ORG_UNIT, ou);

                enrOrgUnit = new EnrOrgUnit();
                enrOrgUnit.setEnrollmentCampaign(order.getEnrollmentCampaign());
                enrOrgUnit.setInstitutionOrgUnit(ou);
                params.put(ENR_ORG_UNIT, enrOrgUnit);
            } else {
                enrOrgUnit = DataAccessServices.dao().get(EnrOrgUnit.class, EnrOrgUnit.institutionOrgUnit().id(), ou.getId());
                if(enrOrgUnit == null)
                {
                    enrOrgUnit = new EnrOrgUnit();
                    enrOrgUnit.setEnrollmentCampaign(order.getEnrollmentCampaign());
                    enrOrgUnit.setInstitutionOrgUnit(ou);
                    params.put(ENR_ORG_UNIT, enrOrgUnit);
                }
            }
            paragraph.setEnrOrgUnit(enrOrgUnit);
            paragraph.setFormativeOrgUnit(orgUnit);
            paragraph.setProgramForm(programForm);
            paragraph.setProgramSubject(programSubject);
            if(competition != null)
                paragraph.setCompetitionType(competition.getCompetition().getType());
            params.put(PARAGRAPH, paragraph);
            abstractParagraph = paragraph;

            entity.setParagraph(paragraph);
            isNew = true;
            isChanged = true;
            isParagraphChanged = true;
        } else
            abstractParagraph = entity.getParagraph();

        if (competition != null && !competition.equals(entity.getEntity())) {
            isChanged = true;
            entity.setEntity(competition);
        }
        if (null == entity.getEntity())
            throw new ProcessingDatagramObjectException("EnrEnrollmentExtractType object without EnrRequestedCompetitionID!");
        entity.setNumber(number);

        if(abstractParagraph instanceof EnrEnrollmentParagraph) {
            EnrEnrollmentParagraph p = (EnrEnrollmentParagraph) abstractParagraph;
            if (programForm != null && !programForm.equals(p.getProgramForm())) {
                isParagraphChanged = true;
                p.setProgramForm(programForm);
            }
            if (null == p.getProgramForm())
                throw new ProcessingDatagramObjectException("EnrEnrollmentExtractType object without EduProgramFormID!");

            if (programSubject != null && !programSubject.equals(p.getProgramSubject())) {
                isParagraphChanged = true;
                p.setProgramSubject(programSubject);
            }
            if (null == p.getProgramSubject())
                throw new ProcessingDatagramObjectException("EnrEnrollmentExtractType object without EduProgramSubjectID!");

            if (competition != null && !competition.getCompetition().getType().equals(p.getCompetitionType())) {
                isParagraphChanged = true;
                p.setCompetitionType(competition.getCompetition().getType());
            }
            if (null == p.getCompetitionType())
                throw new ProcessingDatagramObjectException("EnrEnrollmentExtractType object without EnrRequestedCompetitionID!");
        }

        if (order != null && !order.equals(abstractParagraph.getOrder())) {
            isParagraphChanged = true;
            abstractParagraph.setOrder(order);
        }
        if (null == abstractParagraph.getOrder())
            throw new ProcessingDatagramObjectException("EnrEnrollmentExtractType object without EnrOrderID!");

        if (cancelled != null && !cancelled.equals(entity.isCancelled())) {
            isChanged = true;
            entity.setCancelled(cancelled);
        }

        if(isParagraphChanged)
            params.put(PARAGRAPH, abstractParagraph);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrEnrollmentExtract> w, EnrEnrollmentExtractType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            EduInstitutionOrgUnit ou = (EduInstitutionOrgUnit) params.get(EDU_INST_ORG_UNIT);
            if(ou != null)
                session.saveOrUpdate(ou);

            EnrOrgUnit enrOrgUnit = (EnrOrgUnit) params.get(ENR_ORG_UNIT);
            if(enrOrgUnit != null)
                session.saveOrUpdate(ou);

            EnrAbstractParagraph paragraph = (EnrAbstractParagraph) params.get(PARAGRAPH);
            if(paragraph != null)
                session.saveOrUpdate(paragraph);

            saveChildEntity(EduProgramFormType.class, session, params, PROGRAM_FORM, nsiPackage);
            saveChildEntity(EduProgramSubjectType.class, session, params, PROGRAM_SUBJECT, nsiPackage);
            saveChildEntity(EnrOrderType.class, session, params, ORDER, nsiPackage);
            saveChildEntity(EnrRequestedCompetitionType.class, session, params, COMPETITION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
