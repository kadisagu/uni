/* $Id$ */
package ru.tandemservice.nsiunienr14.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<Boolean> orderableExtPoint()
    {
        return itemListExtension(_catalogManager.orderableExtPoint())
                .add(EnrCampaignDiscipline.ENTITY_NAME, false)
                .add(EnrEnrollmentExtract.ENTITY_NAME, false)
                .add(EnrEntrantRequest.ENTITY_NAME, false)
                .add(EnrEntrantStateExamResult.ENTITY_NAME, false)
                .add(EnrEntrant.ENTITY_NAME, false)
                .add(EnrEntrantRequestAttachment.ENTITY_NAME, false)
                .add(EnrExamPassDiscipline.ENTITY_NAME, false)
                .add(EnrOrder.ENTITY_NAME, false)
                .add(EnrProgramSetItem.ENTITY_NAME, false)
                .add(EnrProgramSetOrgUnit.ENTITY_NAME, false)
                .add(EnrRatingItem.ENTITY_NAME, false)
                .add(EnrRequestedCompetition.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> filterableExtPoint()
    {
        return itemListExtension(_catalogManager.filterableExtPoint())
                .add(EnrCampaignDiscipline.ENTITY_NAME, false)
                .add(EnrEnrollmentExtract.ENTITY_NAME, false)
                .add(EnrEntrantRequest.ENTITY_NAME, false)
                .add(EnrEntrantStateExamResult.ENTITY_NAME, false)
                .add(EnrEntrant.ENTITY_NAME, false)
                .add(EnrEntrantRequestAttachment.ENTITY_NAME, false)
                .add(EnrExamPassDiscipline.ENTITY_NAME, false)
                .add(EnrOrder.ENTITY_NAME, false)
                .add(EnrProgramSetItem.ENTITY_NAME, false)
                .add(EnrProgramSetOrgUnit.ENTITY_NAME, false)
                .add(EnrRatingItem.ENTITY_NAME, false)
                .add(EnrRequestedCompetition.ENTITY_NAME, false)
                .create();
    }
}
