/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrEntrantType;
import ru.tandemservice.nsiclient.datagram.EnrRatingType;
import ru.tandemservice.nsiclient.datagram.EnrRequestedCompetitionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 15.09.2015
 */
public class EnrRatingTypeReactor extends BaseEntityReactor<EnrRatingItem, EnrRatingType> {
    private static final String COMPETITION = "EnrRequestedCompetitionID";
    private static final String ENTRANT = "EnrEntrantID";

    @Override
    public Class<EnrRatingItem> getEntityClass() {
        return EnrRatingItem.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrRatingType datagramObject, IUnknownDatagramsCollector collector) {
        EnrRatingType.EnrEntrantID enrEntrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrEntrant();
        if (entrantType != null)
            collector.check(entrantType.getID(), EnrEntrantType.class);

        EnrRatingType.EnrRequestedCompetitionID competitionID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType competitionType = competitionID == null ? null : competitionID.getEnrRequestedCompetition();
        if (competitionType != null)
            collector.check(competitionType.getID(), EnrRequestedCompetitionType.class);
    }

    @Override
    public EnrRatingType createDatagramObject(String guid) {
        EnrRatingType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRatingType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrRatingItem entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrRatingType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRatingType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrRatingTotalMarkAsLong(String.valueOf(entity.getTotalMarkAsLong()));

        ProcessedDatagramObject<EnrEntrantType> entrant = createDatagramObject(EnrEntrantType.class, entity.getEntrant(), commonCache, warning);
        EnrRatingType.EnrEntrantID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRatingTypeEnrEntrantID();
        entrantID.setEnrEntrant(entrant.getObject());
        datagramObject.setEnrEntrantID(entrantID);
        if (entrant.getList() != null)
            result.addAll(entrant.getList());

        ProcessedDatagramObject<EnrRequestedCompetitionType> competitionType = createDatagramObject(EnrRequestedCompetitionType.class, entity.getRequestedCompetition(), commonCache, warning);
        EnrRatingType.EnrRequestedCompetitionID competitionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRatingTypeEnrRequestedCompetitionID();
        competitionID.setEnrRequestedCompetition(competitionType.getObject());
        datagramObject.setEnrRequestedCompetitionID(competitionID);
        if (competitionType.getList() != null)
            result.addAll(competitionType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrRatingItem findEnrRatingItem(EnrEntrant entrant, EnrRequestedCompetition competition)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (entrant != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrRatingItem.entrant().id()), value(entrant.getId())));
        if (competition != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrRatingItem.requestedCompetition().id()), value(competition.getId())));
        List<EnrRatingItem> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrRatingItem> processDatagramObject(EnrRatingType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        // EnrRatingTotalMarkAsLong init
        Long totalMarkAsLong = NsiUtils.parseLong(datagramObject.getEnrRatingTotalMarkAsLong(), "EnrRatingTotalMarkAsLong");

        EnrRatingType.EnrEntrantID enrEntrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrEntrant();
        EnrEntrant entrant = getOrCreate(EnrEntrantType.class, params, commonCache, entrantType, ENTRANT, null, warning);

        EnrRatingType.EnrRequestedCompetitionID competitionID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType competitionType = competitionID == null ? null : competitionID.getEnrRequestedCompetition();
        EnrRequestedCompetition competition = getOrCreate(EnrRequestedCompetitionType.class, params, commonCache, competitionType, COMPETITION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrRatingItem entity = null;

        CoreCollectionUtils.Pair<EnrRatingItem, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrRatingItem(entrant, competition);
        if (entity == null)
        {
            entity = new EnrRatingItem();
            isNew = true;
            isChanged = true;
        }

        // EnrEntrant set
        if (totalMarkAsLong != null && !totalMarkAsLong.equals(entity.getTotalMarkAsLong()))
        {
            isChanged = true;
            entity.setTotalMarkAsLong(totalMarkAsLong);
        }

        if (isNew) {
            if (entrant != null && !entrant.equals(entity.getEntrant())) {
                isChanged = true;
                entity.setEntrant(entrant);
            }
        } else if (entrant != null && !entrant.equals(entity.getEntrant()))
            warning.append("[EnrEntrantID is immutable!]");
        if (null == entity.getEntrant())
            throw new ProcessingDatagramObjectException("EnrRatingType object without EnrEntrantID!");

        if (competition != null && !competition.equals(entity.getRequestedCompetition()))
        {
            isChanged = true;
            entity.setRequestedCompetition(competition);
            if(isNew)
                entity.setCompetition(competition.getCompetition());
        }
        if (null == entity.getRequestedCompetition())
            throw new ProcessingDatagramObjectException("EnrRatingType object without EnrRequestedCompetitionID!");
        if (!NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrRatingItem.class, entity, EnrRatingItem.L_REQUESTED_COMPETITION))
            throw new ProcessingDatagramObjectException("EnrRatingType EnrRequestedCompetitionID is not unique!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrRatingItem> w, EnrRatingType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEntrantType.class, session, params, ENTRANT, nsiPackage);
            saveChildEntity(EnrRequestedCompetitionType.class, session, params, COMPETITION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
