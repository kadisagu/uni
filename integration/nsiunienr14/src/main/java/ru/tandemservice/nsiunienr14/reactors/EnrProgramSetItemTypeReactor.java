/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EduProgramHigherProfType;
import ru.tandemservice.nsiclient.datagram.EnrProgramSetBaseType;
import ru.tandemservice.nsiclient.datagram.EnrProgramSetItemType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 18.09.2015
 */
public class EnrProgramSetItemTypeReactor extends BaseEntityReactor<EnrProgramSetItem, EnrProgramSetItemType> {
    private static final String PROGRAM_SET_BASE = "EnrProgramSetBaseID";
    private static final String PROGRAM_HIGHER_PROF = "EduProgramHigherProfID";
    @Override
    public Class<EnrProgramSetItem> getEntityClass() {
        return EnrProgramSetItem.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrProgramSetItemType datagramObject, IUnknownDatagramsCollector collector) {
        EnrProgramSetItemType.EduProgramHigherProfID programHigherProfID = datagramObject.getEduProgramHigherProfID();
        EduProgramHigherProfType higherProfType = programHigherProfID == null ? null : programHigherProfID.getEduProgramHigherProf();
        if (higherProfType != null)
            collector.check(higherProfType.getID(), EduProgramHigherProfType.class);

        EnrProgramSetItemType.EnrProgramSetBaseID enrEntrantID = datagramObject.getEnrProgramSetBaseID();
        EnrProgramSetBaseType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrProgramSetBase();
        if (entrantType != null)
            collector.check(entrantType.getID(), EnrProgramSetBaseType.class);
    }

    @Override
    public EnrProgramSetItemType createDatagramObject(String guid) {
        EnrProgramSetItemType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetItemType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrProgramSetItem entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrProgramSetItemType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetItemType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(1);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        ProcessedDatagramObject<EduProgramHigherProfType> programHigherProf = createDatagramObject(EduProgramHigherProfType.class, entity.getProgram(), commonCache, warning);
        EnrProgramSetItemType.EduProgramHigherProfID programHigherProfID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetItemTypeEduProgramHigherProfID();
        programHigherProfID.setEduProgramHigherProf(programHigherProf.getObject());
        datagramObject.setEduProgramHigherProfID(programHigherProfID);
        if (programHigherProf.getList() != null)
            result.addAll(programHigherProf.getList());

        ProcessedDatagramObject<EnrProgramSetBaseType> entrant = createDatagramObject(EnrProgramSetBaseType.class, entity.getProgramSet(), commonCache, warning);
        EnrProgramSetItemType.EnrProgramSetBaseID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetItemTypeEnrProgramSetBaseID();
        entrantID.setEnrProgramSetBase(entrant.getObject());
        datagramObject.setEnrProgramSetBaseID(entrantID);
        if (entrant.getList() != null)
            result.addAll(entrant.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrProgramSetItem findEnrProgramSetItem(EnrProgramSetBase programSetBase, EduProgramHigherProf programHigherProf)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (programSetBase != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrProgramSetItem.programSet().id()), value(programSetBase.getId())));
        if (programHigherProf != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrProgramSetItem.program().id()), value(programHigherProf.getId())));
        List<EnrProgramSetItem> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrProgramSetItem> processDatagramObject(EnrProgramSetItemType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        EnrProgramSetItemType.EduProgramHigherProfID programHigherProfID = datagramObject.getEduProgramHigherProfID();
        EduProgramHigherProfType higherProfType = programHigherProfID == null ? null : programHigherProfID.getEduProgramHigherProf();
        EduProgramHigherProf programHigherProf = getOrCreate(EduProgramHigherProfType.class, params, commonCache, higherProfType, PROGRAM_HIGHER_PROF, null, warning);

        EnrProgramSetItemType.EnrProgramSetBaseID enrEntrantID = datagramObject.getEnrProgramSetBaseID();
        EnrProgramSetBaseType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrProgramSetBase();
        EnrProgramSetBase programSetBase = getOrCreate(EnrProgramSetBaseType.class, params, commonCache, entrantType, PROGRAM_SET_BASE, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrProgramSetItem entity = null;

        CoreCollectionUtils.Pair<EnrProgramSetItem, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrProgramSetItem(programSetBase, programHigherProf);
        if (entity == null)
        {
            entity = new EnrProgramSetItem();
            isNew = true;
            isChanged = true;
        }

        if (programSetBase != null && !programSetBase.equals(entity.getProgramSet()))
        {
            isChanged = true;
            entity.setProgramSet(programSetBase);
        }
        if (null == entity.getProgramSet())
            throw new ProcessingDatagramObjectException("EnrProgramSetItemType object without EnrProgramSetBaseID!");

        if (programHigherProf != null && !programHigherProf.equals(entity.getProgram()))
        {
            isChanged = true;
            entity.setProgram(programHigherProf);
        }
        if (null == entity.getProgram())
            throw new ProcessingDatagramObjectException("EnrProgramSetItemType object without EduProgramHigherProfID!");
        if (!entity.getProgram().getYear().equals(entity.getProgramSet().getEnrollmentCampaign().getEducationYear()))
            throw new ProcessingDatagramObjectException("Год начала обучения по образовательной программе для приема должен совпадать с годом приемной кампании.");
        if (!entity.getProgram().getForm().equals(entity.getProgramSet().getProgramForm()))
            throw new ProcessingDatagramObjectException("Форма обучения по образовательной программе для приема должна совпадать с формой обучения, установленной для набора ОП для приема.");
        if (!entity.getProgram().getProgramSubject().equals(entity.getProgramSet().getProgramSubject()))
            throw new ProcessingDatagramObjectException("Направление (специальность) образовательной программы для приема должно совпадать с направлением (специальностью), установленным для набора ОП для приема.");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrProgramSetItem> w, EnrProgramSetItemType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EduProgramHigherProfType.class, session, params, PROGRAM_HIGHER_PROF, nsiPackage);
            saveChildEntity(EnrProgramSetBaseType.class, session, params, PROGRAM_SET_BASE, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
