/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrEntrReqAttachmentType;
import ru.tandemservice.nsiclient.datagram.EnrEntrantRequestType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantRequestAttachable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 18.09.2015
 */
public class EnrEntrReqAttachmentReactor extends BaseEntityReactor<EnrEntrantRequestAttachment, EnrEntrReqAttachmentType>
{
    private static final String REQUEST = "requestID";

    @Override
    public Class<EnrEntrantRequestAttachment> getEntityClass()
    {
        return EnrEntrantRequestAttachment.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrReqAttachmentType datagramObject, IUnknownDatagramsCollector collector)
    {
        EnrEntrReqAttachmentType.EnrEntrantRequestID requestID = datagramObject.getEnrEntrantRequestID();
        EnrEntrantRequestType requestType = requestID == null ? null : requestID.getEnrEntrantRequest();
        if (requestType != null) collector.check(requestType.getID(), EnrEntrantRequestType.class);
    }

    @Override
    public EnrEntrReqAttachmentType createDatagramObject(String guid)
    {
        EnrEntrReqAttachmentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrReqAttachmentType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantRequestAttachment entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrEntrReqAttachmentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrReqAttachmentType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEntrReqAttachmentOriginalHandedIn(NsiUtils.formatBoolean(entity.isOriginalHandedIn()));
        datagramObject.setEnrEntrReqAttachmentDocumentType(entity.getDocument().getDocumentType().getTitle());

        List<EnrEntrantBenefitProof> enrEntrantBenefitProofList = DataAccessServices.dao().getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.L_DOCUMENT, entity.getDocument());
        if (!enrEntrantBenefitProofList.isEmpty())
        {
            datagramObject.setEnrEntrReqAttachmentBenefitStatement(NsiUtils.TRUE);
        }
        else
        {
            datagramObject.setEnrEntrReqAttachmentBenefitStatement(NsiUtils.FALSE);
        }

        ProcessedDatagramObject<EnrEntrantRequestType> requestType = createDatagramObject(EnrEntrantRequestType.class, entity.getEntrantRequest(), commonCache, warning);
        EnrEntrReqAttachmentType.EnrEntrantRequestID requestID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrReqAttachmentTypeEnrEntrantRequestID();
        requestID.setEnrEntrantRequest(requestType.getObject());
        datagramObject.setEnrEntrantRequestID(requestID);
        if (requestType.getList() != null)
            result.addAll(requestType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrEntrantRequestAttachment findEnrEntrantRequestAttachment(IEnrEntrantRequestAttachable document, EnrEntrantRequest entrantRequest)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (document != null && entrantRequest != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantRequestAttachment.document()), value(document)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantRequestAttachment.entrantRequest()), value(entrantRequest)));
            List<EnrEntrantRequestAttachment> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrantRequestAttachment> processDatagramObject(EnrEntrReqAttachmentType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EnrEntrantRequestAttachment init
        EnrEntrReqAttachmentType.EnrEntrantRequestID entrantRequestID = datagramObject.getEnrEntrantRequestID();
        EnrEntrantRequestType entrantRequestType = entrantRequestID == null ? null : entrantRequestID.getEnrEntrantRequest();
        EnrEntrantRequest entrantRequest = getOrCreate(EnrEntrantRequestType.class, params, commonCache, entrantRequestType, REQUEST, null, warning);

        Boolean originalHandedIn = NsiUtils.parseBoolean(datagramObject.getEnrEntrReqAttachmentOriginalHandedIn());
        IEnrEntrantRequestAttachable document = null;
        if (entrantRequest != null && datagramObject.getEnrEntrReqAttachmentDocumentType() != null)
        {
            document = getDocument(entrantRequest.getEntrant(), datagramObject.getEnrEntrReqAttachmentDocumentType());
        }

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantRequestAttachment entity = null;

        CoreCollectionUtils.Pair<EnrEntrantRequestAttachment, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrantRequestAttachment(document, entrantRequest);
        if (entity == null)
        {
            entity = new EnrEntrantRequestAttachment();
            isNew = true;
            isChanged = true;
        }

        // EnrEntrantRequestAttachment set
        if (entrantRequest != null && !entrantRequest.equals(entity.getEntrantRequest()))
        {
            isChanged = true;
            entity.setEntrantRequest(entrantRequest);
        }
        if (entity.getEntrantRequest() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrReqAttachmentType object without EnrEntrantRequestID");
        }

        if (document != null && !document.equals(entity.getDocument()))
        {
            isChanged = true;
            entity.setDocument(document);
        }
        if (entity.getDocument() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrReqAttachmentType object without EnrEntrReqAttachmentDocumentType");
        }

        if (originalHandedIn != null && !originalHandedIn.equals(entity.isOriginalHandedIn()))
        {
            isChanged = true;
            entity.setOriginalHandedIn(originalHandedIn);
        }

        if (entity.getDocument() != null && entity.getEntrantRequest() != null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantRequestAttachment.class, entity,
                                                                              EnrEntrantRequestAttachment.L_DOCUMENT, EnrEntrantRequestAttachment.L_DOCUMENT))
        {
            throw new ProcessingDatagramObjectException("EnrEntrReqAttachmentType EnrEntrReqAttachmentDocumentType/EnrEntrantRequestID combination is not unique");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }


    @Override
    public void save(Session session, ChangedWrapper<EnrEntrantRequestAttachment> w, EnrEntrReqAttachmentType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEntrantRequestType.class, session, params, REQUEST, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }


    private static IEnrEntrantRequestAttachable getDocument(EnrEntrant entrant, String documentTypeTitle)
    {
        String trimmedTitle = StringUtils.trimToNull(documentTypeTitle);

        DQLSelectBuilder baseDql = new DQLSelectBuilder()
                .fromEntity(EnrEntrantBaseDocument.class, "e")
                .column(property("e"))
                .where(eqValue(property("e", EnrEntrantBaseDocument.entrant()), entrant))
                .where(eqValue(property("e", EnrEntrantBaseDocument.documentType().title()), trimmedTitle));

        List<EnrEntrantBaseDocument> baseDocumentList = DataAccessServices.dao().getList(baseDql);

        if (!baseDocumentList.isEmpty())
        {
            return baseDocumentList.get(0);
        }
        return null;
    }
}
