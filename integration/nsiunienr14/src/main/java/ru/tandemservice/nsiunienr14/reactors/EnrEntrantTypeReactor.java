/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 09.09.2015
 */
public class EnrEntrantTypeReactor extends BaseEntityReactor<EnrEntrant, EnrEntrantType>
{
    private static final String HUMAN = "humanID";
    private static final String CAMPAIGN = "enrollmentCampaignID";
    private static final String ENTRANT_STATE = "entrantStateID";

    @Override
    public Class<EnrEntrant> getEntityClass()
    {
        return EnrEntrant.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrantType datagramObject, IUnknownDatagramsCollector collector)
    {
        EnrEntrantType.EnrEnrollmentCampaignID enrollmentCampaignID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType enrollmentCampaignType = enrollmentCampaignID == null ? null : enrollmentCampaignID.getEnrEnrollmentCampaign();
        if (enrollmentCampaignType != null)
            collector.check(enrollmentCampaignType.getID(), EnrEnrollmentCampaignType.class);

        EnrEntrantType.EnrEntrantStateID entrantStateID = datagramObject.getEnrEntrantStateID();
        EnrEntrantStateType enrEntrantStateType = entrantStateID == null ? null : entrantStateID.getEnrEntrantState();
        if (enrEntrantStateType != null)
            collector.check(enrEntrantStateType.getID(), EnrEntrantStateType.class);
    }

    @Override
    public EnrEntrantType createDatagramObject(String guid)
    {
        EnrEntrantType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrant entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {

        EnrEntrantType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEntrantArchival(NsiUtils.formatBoolean(entity.isArchival()));
        datagramObject.setEnrEntrantRegistrationDate(NsiUtils.formatDate(entity.getRegistrationDate()));
        datagramObject.setEnrEntrantPersonalNumber(entity.getPersonalNumber());

        ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        EnrEntrantType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantTypeHumanID();
        humanID.setHuman(humanType.getObject());
        datagramObject.setHumanID(humanID);
        if (humanType.getList() != null)
            result.addAll(humanType.getList());

        ProcessedDatagramObject<EnrEnrollmentCampaignType> enrEnrollmentCampaignIType = createDatagramObject(EnrEnrollmentCampaignType.class, entity.getEnrollmentCampaign(), commonCache, warning);
        EnrEntrantType.EnrEnrollmentCampaignID campaignID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantTypeEnrEnrollmentCampaignID();
        campaignID.setEnrEnrollmentCampaign(enrEnrollmentCampaignIType.getObject());
        datagramObject.setEnrEnrollmentCampaignID(campaignID);
        if (enrEnrollmentCampaignIType.getList() != null)
            result.addAll(enrEnrollmentCampaignIType.getList());

        if(entity.getState() != null) {
            ProcessedDatagramObject<EnrEntrantStateType> entrantStateType = createDatagramObject(EnrEntrantStateType.class, entity.getState(), commonCache, warning);
            EnrEntrantType.EnrEntrantStateID stateID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantTypeEnrEntrantStateID();
            stateID.setEnrEntrantState(entrantStateType.getObject());
            datagramObject.setEnrEntrantStateID(stateID);
            if (entrantStateType.getList() != null)
                result.addAll(entrantStateType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrEntrant findEnrEntrant(EnrEnrollmentCampaign enrEnrollmentCampaign, String personalNumber, Person person)
    {
        List<IExpression> expressions = new ArrayList<>();
        List<EnrEntrant> list = new ArrayList<>();
        if (enrEnrollmentCampaign != null && personalNumber != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrant.personalNumber()), value(personalNumber)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrant.enrollmentCampaign()), value(enrEnrollmentCampaign)));
            list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        }
        else if (enrEnrollmentCampaign != null && person != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrant.person()), value(person)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrant.enrollmentCampaign()), value(enrEnrollmentCampaign)));
            list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        }
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrant> processDatagramObject(EnrEntrantType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EnrEntrant init
        String personalNumber = StringUtils.trimToNull(datagramObject.getEnrEntrantPersonalNumber());
        Boolean archival = NsiUtils.parseBoolean(datagramObject.getEnrEntrantArchival());
        Date registrationDate = NsiUtils.parseDate(datagramObject.getEnrEntrantRegistrationDate(), "EnrEntrantRegistrationDate");

        EnrEntrantType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);

        EnrEntrantType.EnrEnrollmentCampaignID enrollmentCampaignID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType enrollmentCampaignType = enrollmentCampaignID == null ? null : enrollmentCampaignID.getEnrEnrollmentCampaign();
        EnrEnrollmentCampaign enrEnrollmentCampaign = getOrCreate(EnrEnrollmentCampaignType.class, params, commonCache, enrollmentCampaignType, CAMPAIGN, null, warning);

        EnrEntrantType.EnrEntrantStateID entrantStateID = datagramObject.getEnrEntrantStateID();
        EnrEntrantStateType stateType = entrantStateID == null ? null : entrantStateID.getEnrEntrantState();
        EnrEntrantState entrantState = getOrCreate(EnrEntrantStateType.class, params, commonCache, stateType, ENTRANT_STATE, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrant entity = null;

        CoreCollectionUtils.Pair<EnrEntrant, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrant(enrEnrollmentCampaign, personalNumber, person);
        if (entity == null)
        {
            entity = new EnrEntrant();
            isNew = true;
            isChanged = true;
        }

        // EnrEntrant set
        if (personalNumber != null && !personalNumber.equals(entity.getPersonalNumber()))
        {
            isChanged = true;
            entity.setPersonalNumber(personalNumber);
        }
        if (null == entity.getPersonalNumber())
        {
            throw new ProcessingDatagramObjectException("EnrEntrantType object without EnrEntrantPersonalNumber!");
        }

        if (archival != null && !archival.equals(entity.isArchival()))
        {
            isChanged = true;
            entity.setArchival(archival);
        }

        if (registrationDate != null && !registrationDate.equals(entity.getRegistrationDate()))
        {
            isChanged = true;
            entity.setRegistrationDate(registrationDate);
        }
        if (null == entity.getRegistrationDate())
        {
            throw new ProcessingDatagramObjectException("EnrEntrantType object without EnrEntrantRegistrationDate!");
        }

        if (person != null && !person.equals(entity.getPerson()))
        {
            isChanged = true;
            entity.setPerson(person);
        }
        if (null == entity.getPerson())
        {
            throw new ProcessingDatagramObjectException("EnrEntrantType object without HumanID!");
        }
        if (enrEnrollmentCampaign != null && !enrEnrollmentCampaign.equals(entity.getEnrollmentCampaign()))
        {
            isChanged = true;
            entity.setEnrollmentCampaign(enrEnrollmentCampaign);
        }
        if (null == entity.getEnrollmentCampaign())
        {
            throw new ProcessingDatagramObjectException("EnrEntrantType object without EnrEnrollmentCampaignID!");
        }

        if (entrantState != null && !entrantState.equals(entity.getState()))
        {
            isChanged = true;
            entity.setState(entrantState);
        }

        if (!NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrant.class, entity, EnrEntrant.L_ENROLLMENT_CAMPAIGN, EnrEntrant.P_PERSONAL_NUMBER))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantType EnrEntrantPersonalNumber/EnrEnrollmentCampaignID combination is not unique");
        }

        if (!NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrant.class, entity, EnrEntrant.L_ENROLLMENT_CAMPAIGN, EnrEntrant.L_PERSON))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantType HumanID/EnrEnrollmentCampaignID combination is not unique");
        }

        Principal principal = PersonManager.instance().dao().getPrincipal(person);

        if (principal != null && !principal.equals(entity.getPrincipal()))
        {
            isChanged = true;
            entity.setPrincipal(principal);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }


    @Override
    public void save(Session session, ChangedWrapper<EnrEntrant> w, EnrEntrantType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(EnrEnrollmentCampaignType.class, session, params, CAMPAIGN, nsiPackage);
            saveChildEntity(EnrEntrantStateType.class, session, params, ENTRANT_STATE, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }


}
