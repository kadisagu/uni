/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrCampaignDisciplineType;
import ru.tandemservice.nsiclient.datagram.EnrEnrollmentCampaignType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrDiscipline;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 18.09.2015
 */
public class EnrCampaignDisciplineTypeReactor extends BaseEntityReactor<EnrCampaignDiscipline, EnrCampaignDisciplineType>
{
    private static final String CAMPAIGN = "campaignID";

    @Override
    public Class<EnrCampaignDiscipline> getEntityClass()
    {
        return EnrCampaignDiscipline.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrCampaignDisciplineType datagramObject, IUnknownDatagramsCollector collector)
    {
        EnrCampaignDisciplineType.EnrEnrollmentCampaignID campaignID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType campaignType = campaignID == null ? null : campaignID.getEnrEnrollmentCampaign();
        if (campaignType != null) collector.check(campaignType.getID(), EnrEnrollmentCampaignType.class);
    }

    @Override
    public EnrCampaignDisciplineType createDatagramObject(String guid)
    {
        EnrCampaignDisciplineType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrCampaignDisciplineType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrCampaignDiscipline entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrCampaignDisciplineType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrCampaignDisciplineType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrCampaignDisciplineDefaultPassMarkAsLong(String.valueOf(entity.getDefaultPassMarkAsLong()));
        datagramObject.setEnrCampaignDisciplineDiscipline(entity.getDiscipline().getTitle());
        datagramObject.setEnrCampaignDisciplineStateExamSubject(entity.getStateExamSubject() != null ? entity.getStateExamSubject().getTitle() : null);

        ProcessedDatagramObject<EnrEnrollmentCampaignType> campaignType = createDatagramObject(EnrEnrollmentCampaignType.class, entity.getEnrollmentCampaign(), commonCache, warning);
        EnrCampaignDisciplineType.EnrEnrollmentCampaignID campaignID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrCampaignDisciplineTypeEnrEnrollmentCampaignID();
        campaignID.setEnrEnrollmentCampaign(campaignType.getObject());
        datagramObject.setEnrEnrollmentCampaignID(campaignID);
        if (campaignType.getList() != null)
            result.addAll(campaignType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrCampaignDiscipline findEnrCampaignDiscipline(EnrDiscipline discipline, EnrEnrollmentCampaign enrollmentCampaign)
    {

        List<IExpression> expressions = new ArrayList<>();
        if (discipline != null && enrollmentCampaign != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EnrCampaignDiscipline.discipline()), value(discipline)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrCampaignDiscipline.enrollmentCampaign()), value(enrollmentCampaign)));
            List<EnrCampaignDiscipline> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrCampaignDiscipline> processDatagramObject(EnrCampaignDisciplineType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EnrExamPassDiscipline init
        EnrCampaignDisciplineType.EnrEnrollmentCampaignID campaignID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType campaignType = campaignID == null ? null : campaignID.getEnrEnrollmentCampaign();
        EnrEnrollmentCampaign enrollmentCampaign = getOrCreate(EnrEnrollmentCampaignType.class, params, commonCache, campaignType, CAMPAIGN, null, warning);

        EnrDiscipline discipline = getEnrDiscipline(datagramObject.getEnrCampaignDisciplineDiscipline());
        EnrStateExamSubject subject = getEnrStateExamSubject(datagramObject.getEnrCampaignDisciplineStateExamSubject());

        boolean isNew = false;
        boolean isChanged = false;
        EnrCampaignDiscipline entity = null;

        CoreCollectionUtils.Pair<EnrCampaignDiscipline, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrCampaignDiscipline(discipline, enrollmentCampaign);
        if (entity == null)
        {
            entity = new EnrCampaignDiscipline();
            isNew = true;
            isChanged = true;
        }
        // EnrCampaignDiscipline set
        if (discipline != null && !discipline.equals(entity.getDiscipline()))
        {
            isChanged = true;
            entity.setDiscipline(discipline);
        }
        if (entity.getDiscipline() == null)
        {
            throw new ProcessingDatagramObjectException("EnrCampaignDisciplineType object without EnrCampaignDisciplineDiscipline");
        }

        if (enrollmentCampaign != null && !enrollmentCampaign.equals(entity.getEnrollmentCampaign()))
        {
            isChanged = true;
            entity.setEnrollmentCampaign(enrollmentCampaign);
        }
        if (entity.getEnrollmentCampaign() == null)
        {
            throw new ProcessingDatagramObjectException("EnrCampaignDisciplineType object without EnrEnrollmentCampaignID");
        }

        if (subject != null && !subject.equals(entity.getStateExamSubject()))
        {
            isChanged = true;
            entity.setStateExamSubject(subject);
        }

        if (entity.getDiscipline() != null && entity.getEnrollmentCampaign() != null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrCampaignDiscipline.class, entity,
                                                                              EnrCampaignDiscipline.L_DISCIPLINE, EnrCampaignDiscipline.L_ENROLLMENT_CAMPAIGN))
        {
            throw new ProcessingDatagramObjectException("EnrCampaignDisciplineType EnrCampaignDisciplineDiscipline/EnrEnrollmentCampaignID combination is not unique");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrCampaignDiscipline> w, EnrCampaignDisciplineType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEnrollmentCampaignType.class, session, params, CAMPAIGN, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    private static EnrDiscipline getEnrDiscipline(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrDiscipline.class, EnrDiscipline.title(), trimmedTitle);
        }
        return null;
    }

    private static EnrStateExamSubject getEnrStateExamSubject(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrStateExamSubject.class, EnrStateExamSubject.title(), trimmedTitle);
        }
        return null;
    }
}
