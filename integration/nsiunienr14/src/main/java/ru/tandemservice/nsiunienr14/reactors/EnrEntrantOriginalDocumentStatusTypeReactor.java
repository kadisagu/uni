/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrEntrantOriginalDocumentStatusType;
import ru.tandemservice.nsiclient.datagram.EnrEntrantType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.PersonEduDocumentType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 29.09.2015
 */
public class EnrEntrantOriginalDocumentStatusTypeReactor extends BaseEntityReactor<EnrEntrantOriginalDocumentStatus, EnrEntrantOriginalDocumentStatusType> {
    private static final String ENTRANT = "EnrEntrantID";
    private static final String DOCUMENT = "PersonEduDocumentID";

    @Override
    public Class<EnrEntrantOriginalDocumentStatus> getEntityClass() {
        return EnrEntrantOriginalDocumentStatus.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrantOriginalDocumentStatusType datagramObject, IUnknownDatagramsCollector collector) {
        EnrEntrantOriginalDocumentStatusType.PersonEduDocumentID personEduDocumentID = datagramObject.getPersonEduDocumentID();
        PersonEduDocumentType personEduDocumentType = personEduDocumentID == null ? null : personEduDocumentID.getPersonEduDocument();
        if (personEduDocumentType != null)
            collector.check(personEduDocumentType.getID(), PersonEduDocumentType.class);

        EnrEntrantOriginalDocumentStatusType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        if (entrantType != null)
            collector.check(entrantType.getID(), EnrEntrantType.class);
    }

    @Override
    public EnrEntrantOriginalDocumentStatusType createDatagramObject(String guid) {
        EnrEntrantOriginalDocumentStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantOriginalDocumentStatusType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantOriginalDocumentStatus entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrEntrantOriginalDocumentStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantOriginalDocumentStatusType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEntrantOriginalDocumentStatusRegistrationDate(NsiUtils.formatDate(entity.getRegistrationDate()));

        ProcessedDatagramObject<PersonEduDocumentType> personType = createDatagramObject(PersonEduDocumentType.class, entity.getEduDocument(), commonCache, warning);
        EnrEntrantOriginalDocumentStatusType.PersonEduDocumentID personEduDocumentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantOriginalDocumentStatusTypePersonEduDocumentID();
        personEduDocumentID.setPersonEduDocument(personType.getObject());
        datagramObject.setPersonEduDocumentID(personEduDocumentID);
        if (personType.getList() != null)
            result.addAll(personType.getList());

        ProcessedDatagramObject<EnrEntrantType> entrantType = createDatagramObject(EnrEntrantType.class, entity.getEntrant(), commonCache, warning);
        EnrEntrantOriginalDocumentStatusType.EnrEntrantID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantOriginalDocumentStatusTypeEnrEntrantID();
        entrantID.setEnrEntrant(entrantType.getObject());
        datagramObject.setEnrEntrantID(entrantID);
        if (entrantType.getList() != null)
            result.addAll(entrantType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrEntrantOriginalDocumentStatus findEnrEntrantOriginalDocumentStatus(EnrEntrant entrant, PersonEduDocument document)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (entrant != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantOriginalDocumentStatus.entrant().id()), value(entrant.getId())));
        if (document != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantOriginalDocumentStatus.eduDocument().id()), value(document.getId())));
        List<EnrEntrantOriginalDocumentStatus> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrantOriginalDocumentStatus> processDatagramObject(EnrEntrantOriginalDocumentStatusType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        Date registrationDate = NsiUtils.parseDate(datagramObject.getEnrEntrantOriginalDocumentStatusRegistrationDate(), "EnrEntrantOriginalDocumentStatusRegistrationDate");

        EnrEntrantOriginalDocumentStatusType.PersonEduDocumentID personEduDocumentID = datagramObject.getPersonEduDocumentID();
        PersonEduDocumentType personEduDocumentType = personEduDocumentID == null ? null : personEduDocumentID.getPersonEduDocument();
        PersonEduDocument document = getOrCreate(PersonEduDocumentType.class, params, commonCache, personEduDocumentType, DOCUMENT, null, warning);

        EnrEntrantOriginalDocumentStatusType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        EnrEntrant entrant = getOrCreate(EnrEntrantType.class, params, commonCache, entrantType, ENTRANT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantOriginalDocumentStatus entity = null;

        CoreCollectionUtils.Pair<EnrEntrantOriginalDocumentStatus, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrantOriginalDocumentStatus(entrant, document);
        if (entity == null)
        {
            entity = new EnrEntrantOriginalDocumentStatus();
            isNew = true;
            isChanged = true;
        }

        if (registrationDate != null && !registrationDate.equals(entity.getRegistrationDate()))
        {
            isChanged = true;
            entity.setRegistrationDate(registrationDate);
        }
        if (null == entity.getEntrant())
            throw new ProcessingDatagramObjectException("EnrEntrantOriginalDocumentStatusType object without EnrEntrantOriginalDocumentStatusRegistrationDate!");

        if (entrant != null && !entrant.equals(entity.getEntrant()))
        {
            isChanged = true;
            entity.setEntrant(entrant);
        }
        if (null == entity.getEntrant())
            throw new ProcessingDatagramObjectException("EnrEntrantOriginalDocumentStatusType object without EnrEntrantID!");

        if (document != null && !document.equals(entity.getEduDocument()))
        {
            isChanged = true;
            entity.setEduDocument(document);
        }
        if (null == entity.getEntityMeta())
            throw new ProcessingDatagramObjectException("EnrEntrantOriginalDocumentStatusType object without PersonEduDocumentID!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrEntrantOriginalDocumentStatus> w, EnrEntrantOriginalDocumentStatusType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(PersonEduDocumentType.class, session, params, DOCUMENT, nsiPackage);
            saveChildEntity(EnrEntrantType.class, session, params, ENTRANT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
