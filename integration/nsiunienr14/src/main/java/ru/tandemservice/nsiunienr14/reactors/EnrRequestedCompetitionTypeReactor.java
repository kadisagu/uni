/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.EnrTargetAdmissionKind;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrCompetitionTypeGen;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrEduLevelRequirementGen;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignTargetAdmissionKind;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 15.09.2015
 */
public class EnrRequestedCompetitionTypeReactor extends BaseEntityReactor<EnrRequestedCompetition, EnrRequestedCompetitionType> {
    private static final String REQUEST = "EnrEntrantRequestID";
    private static final String SET_ITEM = "EnrProgramSetItemID";
    private static final String SET_ORG_UNIT = "EnrProgramSetOrgUnitID";
    private static final String CAMPAIGN_TARGET_ADMISSION_KIND = "EnrCampaignTargetAdmissionKind";
    private static final String EXT_ORG_UNIT = "externalOrgUnit";
    private static final String COMPETITION = "competition";
    private static final String COMPENSATION = "CompensationTypeID";

    @Override
    public Class<EnrRequestedCompetition> getEntityClass() {
        return EnrRequestedCompetition.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrRequestedCompetitionType datagramObject, IUnknownDatagramsCollector collector) {
        EnrRequestedCompetitionType.EnrEntrantRequestID requestID = datagramObject.getEnrEntrantRequestID();
        EnrEntrantRequestType requestType = requestID == null ? null : requestID.getEnrEntrantRequest();
        if (requestType != null)
            collector.check(requestType.getID(), EnrEntrantRequestType.class);

        EnrRequestedCompetitionType.EnrProgramSetItemID setItemID = datagramObject.getEnrProgramSetItemID();
        EnrProgramSetItemType setItemType = setItemID == null ? null : setItemID.getEnrProgramSetItem();
        if (setItemType != null)
            collector.check(setItemType.getID(), EnrProgramSetItemType.class);

        EnrRequestedCompetitionType.EnrProgramSetOrgUnitID orgUnitID = datagramObject.getEnrProgramSetOrgUnitID();
        EnrProgramSetOrgUnitType orgUnitType = orgUnitID == null ? null : orgUnitID.getEnrProgramSetOrgUnit();
        if (orgUnitType != null)
            collector.check(orgUnitType.getID(), EnrProgramSetOrgUnitType.class);

        EnrRequestedCompetitionType.CompensationTypeID compensationTypeID = datagramObject.getCompensationTypeID();
        CompensationTypeType compensationTypeType = compensationTypeID == null ? null : compensationTypeID.getCompensationType();
        if (compensationTypeType != null)
            collector.check(compensationTypeType.getID(), CompensationTypeType.class);
    }

    @Override
    public EnrRequestedCompetitionType createDatagramObject(String guid) {
        EnrRequestedCompetitionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRequestedCompetitionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrRequestedCompetition entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrRequestedCompetitionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRequestedCompetitionType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        EnrCompetitionType competitionType = entity.getCompetition().getType();
        datagramObject.setEnrRequestedCompetitionType(competitionType.getShortTitle());
        if(entity.getState() != null)
            datagramObject.setEnrRequestedCompetitionState(entity.getState().getCode());

        if(entity instanceof EnrRequestedCompetitionTA) {
            EnrRequestedCompetitionTA ta = (EnrRequestedCompetitionTA) entity;
            datagramObject.setEnrRequestedCompetitionTargetAdmissionKind(ta.getTargetAdmissionKind().getTitle());
            datagramObject.setEnrRequestedCompetitionTargetAdmissionKindOrgUnit(ta.getTargetAdmissionOrgUnit().getTitle());
            datagramObject.setEnrRequestedCompetitionContractNumber(ta.getContractNumber());
        }

        if(entity instanceof EnrRequestedCompetitionNoExams) {
            ProcessedDatagramObject<EnrProgramSetItemType> entrant = createDatagramObject(EnrProgramSetItemType.class, ((EnrRequestedCompetitionNoExams) entity).getProgramSetItem(), commonCache, warning);
            EnrRequestedCompetitionType.EnrProgramSetItemID setItemID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRequestedCompetitionTypeEnrProgramSetItemID();
            setItemID.setEnrProgramSetItem(entrant.getObject());
            datagramObject.setEnrProgramSetItemID(setItemID);
            if (entrant.getList() != null)
                result.addAll(entrant.getList());
        }

        ProcessedDatagramObject<CompensationTypeType> compensationType = createDatagramObject(CompensationTypeType.class, competitionType.getCompensationType(), commonCache, warning);
        EnrRequestedCompetitionType.CompensationTypeID compensationTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRequestedCompetitionTypeCompensationTypeID();
        compensationTypeID.setCompensationType(compensationType.getObject());
        datagramObject.setCompensationTypeID(compensationTypeID);
        if (compensationType.getList() != null)
            result.addAll(compensationType.getList());

        ProcessedDatagramObject<EnrProgramSetOrgUnitType> orgUnitType = createDatagramObject(EnrProgramSetOrgUnitType.class, entity.getCompetition().getProgramSetOrgUnit(), commonCache, warning);
        EnrRequestedCompetitionType.EnrProgramSetOrgUnitID orgUnitID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRequestedCompetitionTypeEnrProgramSetOrgUnitID();
        orgUnitID.setEnrProgramSetOrgUnit(orgUnitType.getObject());
        datagramObject.setEnrProgramSetOrgUnitID(orgUnitID);
        if (orgUnitType.getList() != null)
            result.addAll(orgUnitType.getList());

        ProcessedDatagramObject<EnrEntrantRequestType> requestType = createDatagramObject(EnrEntrantRequestType.class, entity.getRequest(), commonCache, warning);
        EnrRequestedCompetitionType.EnrEntrantRequestID requestID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrRequestedCompetitionTypeEnrEntrantRequestID();
        requestID.setEnrEntrantRequest(requestType.getObject());
        datagramObject.setEnrEntrantRequestID(requestID);
        if (requestType.getList() != null)
            result.addAll(requestType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrRequestedCompetition findEnrRequestedCompetition(EnrEntrantRequest request, EnrProgramSetOrgUnit setOrgUnit) {
        List<IExpression> expressions = new ArrayList<>();
        if (request != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrRequestedCompetition.request().id()), value(request.getId())));
        if (setOrgUnit != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrRequestedCompetition.competition().programSetOrgUnit().id()), value(setOrgUnit.getId())));
        List<EnrRequestedCompetition> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    protected EnrCompetitionType findEnrCompetitionType(String title, CompensationType compensation) {
        List<IExpression> expressions = new ArrayList<>();
        if (title != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrCompetitionType.title()), value(title)));
        if (compensation != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrCompetitionType.compensationType().id()), value(compensation.getId())));
        List<EnrCompetitionType> list = findEntity(EnrCompetitionType.class, and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    private ExternalOrgUnit findExternalOrgUnit(String title)
    {
        if(title != null)
        {
            List<ExternalOrgUnit> units = findEntity(ExternalOrgUnit.class, eq(property(ENTITY_ALIAS, ExternalOrgUnit.title()), value(title)));
            if(units.size() == 1)
                return units.get(0);
        }
        return null;
    }

    private EnrCampaignTargetAdmissionKind findEnrCampaignTargetAdmissionKind(String title)
    {
        if(title != null)
        {
            List<EnrCampaignTargetAdmissionKind> units = findEntity(EnrCampaignTargetAdmissionKind.class, eq(property(ENTITY_ALIAS, EnrCampaignTargetAdmissionKind.targetAdmissionKind().title()), value(title)));
            if(units.size() == 1)
                return units.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrRequestedCompetition> processDatagramObject(EnrRequestedCompetitionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        // EnrRequestedCompetitionType init
        String competitionType = datagramObject.getEnrRequestedCompetitionType();
        // EnrRequestedCompetitionState init
        String competitionState = datagramObject.getEnrRequestedCompetitionState();
        // EnrRequestedCompetitionTargetAdmissionKind init
        String kind = datagramObject.getEnrRequestedCompetitionTargetAdmissionKind();
        String contractNumber = datagramObject.getEnrRequestedCompetitionContractNumber();
        // EnrRequestedCompetitionTargetAdmissionKindOrgUnit init
        String kindOrgUnit = datagramObject.getEnrRequestedCompetitionTargetAdmissionKindOrgUnit();
        // CompensationTypeID init
        EnrRequestedCompetitionType.CompensationTypeID compensationTypeID = datagramObject.getCompensationTypeID();
        CompensationTypeType compensationTypeType = compensationTypeID == null ? null : compensationTypeID.getCompensationType();
        CompensationType compensation = getOrCreate(CompensationTypeType.class, params, commonCache, compensationTypeType, COMPENSATION, null, warning);

        // EnrEntrantRequestID init
        EnrRequestedCompetitionType.EnrEntrantRequestID requestID = datagramObject.getEnrEntrantRequestID();
        EnrEntrantRequestType requestType = requestID == null ? null : requestID.getEnrEntrantRequest();
        EnrEntrantRequest request = getOrCreate(EnrEntrantRequestType.class, params, commonCache, requestType, REQUEST, null, warning);

        // EnrProgramSetItemID init
        EnrRequestedCompetitionType.EnrProgramSetItemID setItemID = datagramObject.getEnrProgramSetItemID();
        EnrProgramSetItemType setItemType = setItemID == null ? null : setItemID.getEnrProgramSetItem();
        EnrProgramSetItem setItem = getOrCreate(EnrProgramSetItemType.class, params, commonCache, setItemType, SET_ITEM, null, warning);

        // EnrProgramSetOrgUnitID init
        EnrRequestedCompetitionType.EnrProgramSetOrgUnitID orgUnitID = datagramObject.getEnrProgramSetOrgUnitID();
        EnrProgramSetOrgUnitType orgUnitType = setItemID == null ? null : orgUnitID.getEnrProgramSetOrgUnit();
        EnrProgramSetOrgUnit setOrgUnit = getOrCreate(EnrProgramSetOrgUnitType.class, params, commonCache, orgUnitType, SET_ORG_UNIT, null, warning);

        boolean isTa = kind != null && kindOrgUnit != null;
        boolean isNew = false;
        boolean isChanged = false;
        EnrCompetition enrCompetition = null;
        EnrRequestedCompetition entity = null;

        CoreCollectionUtils.Pair<EnrRequestedCompetition, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrRequestedCompetition(request, setOrgUnit);
        if (entity == null)
        {
            if(setItem != null)
                entity = new EnrRequestedCompetitionNoExams();
            else if(isTa)
                entity = new EnrRequestedCompetitionTA();
            else
                entity = new EnrRequestedCompetition();
            isNew = true;
            isChanged = true;
            if (null == setOrgUnit)
                throw new ProcessingDatagramObjectException("EnrRequestedCompetitionType object without EnrProgramSetOrgUnitID!");

            enrCompetition = new EnrCompetition();
            enrCompetition.setProgramSetOrgUnit(setOrgUnit);
            enrCompetition.setRequestType(setOrgUnit.getProgramSet().getRequestType());
            enrCompetition.setType(DataAccessServices.dao().<EnrCompetitionType>getByNaturalId(new EnrCompetitionTypeGen.NaturalId(EnrCompetitionTypeCodes.CONTRACT)));
            enrCompetition.setEduLevelRequirement(DataAccessServices.dao().<EnrEduLevelRequirement>getByNaturalId(new EnrEduLevelRequirementGen.NaturalId(EnrEduLevelRequirementCodes.VO)));

            EnrExamSetVariant examSetVariant = null;
            if (setOrgUnit.getProgramSet() instanceof EnrProgramSetBase.ISingleExamSetOwner) {
                examSetVariant = ((EnrProgramSetBase.ISingleExamSetOwner) setOrgUnit.getProgramSet()).getExamSetVariant();
            }
            enrCompetition.setExamSetVariant(examSetVariant);
            enrCompetition.setPlan(0);
            params.put(COMPETITION, enrCompetition);
        } else
            enrCompetition = entity.getCompetition();

        if (isNew && null == competitionType)
            throw new ProcessingDatagramObjectException("EnrRequestedCompetitionType object without EnrRequestedCompetitionType!");
        if (isNew && null == compensation)
            throw new ProcessingDatagramObjectException("EnrRequestedCompetitionType object without CompensationTypeID!");

        if ((competitionType != null && !competitionType.equals(enrCompetition.getType().getTitle())) ||
                (competitionType != null && !competitionType.equals(enrCompetition.getType().getCompensationType().getTitle()))) {
            String newTitle = competitionType == null ? enrCompetition.getType().getShortTitle() : competitionType;
            EnrCompetitionType type = findEnrCompetitionType(newTitle, compensation);
            if(type == null) {
                type = new EnrCompetitionType();
                type.setShortTitle(newTitle);
                type.setCompensationType(compensation);
            }
        }

        // EnrRequestedCompetitionState set
        if (competitionState != null && !competitionState.equals(entity.getState().getTitle()))
        {
            isChanged = true;
            EnrEntrantState state = DataAccessServices.dao().get(EnrEntrantState.class, EnrEntrantState.code(), competitionState);
            if(state == null)
                throw new ProcessingDatagramObjectException("EnrRequestedCompetitionType object with wrong EnrRequestedCompetitionState!");
            entity.setState(state);
        }
        if(entity instanceof EnrRequestedCompetitionTA) {
            EnrRequestedCompetitionTA ta = (EnrRequestedCompetitionTA) entity;
            if(isNew) {
                EnrCampaignTargetAdmissionKind targetAdmissionKind = createEnrCampaignTargetAdmissionKind(kind, params);
                ta.setTargetAdmissionKind(targetAdmissionKind);
            }
            if (kind != null && !kind.equals(ta.getTargetAdmissionKind().getTitle()))
            {
                isChanged = true;
                EnrCampaignTargetAdmissionKind targetAdmissionKind = createEnrCampaignTargetAdmissionKind(kind, params);
                ta.setTargetAdmissionKind(targetAdmissionKind);
            }

            if(isNew) {
                ExternalOrgUnit externalOrgUnit = createExternalOrgUnit(kindOrgUnit, params);
                ta.setTargetAdmissionOrgUnit(externalOrgUnit);
            }
            if (kindOrgUnit != null && !kindOrgUnit.equals(ta.getTargetAdmissionOrgUnit().getTitle()))
            {
                isChanged = true;
                ExternalOrgUnit externalOrgUnit = createExternalOrgUnit(kindOrgUnit, params);
                ta.setTargetAdmissionOrgUnit(externalOrgUnit);
            }

            if (contractNumber != null && !contractNumber.equals(ta.getContractNumber()))
            {
                isChanged = true;
                ta.setContractNumber(contractNumber);
            }
        }

        if (isNew) {
            if (request != null && !request.equals(entity.getRequest())) {
                isChanged = true;
                entity.setRequest(request);
            }
        } else if (request != null && !request.equals(entity.getRequest()))
            warning.append("[EnrEntrantRequestID is immutable!]");
        if (null == entity.getRequest())
            throw new ProcessingDatagramObjectException("EnrRequestedCompetitionType object without EnrEntrantRequestID!");

        if(entity instanceof EnrRequestedCompetitionNoExams) {
            EnrRequestedCompetitionNoExams exams = (EnrRequestedCompetitionNoExams) entity;
            if (setItem != null && !setItem.equals(exams.getProgramSetItem()))
            {
                isChanged = true;
                exams.setProgramSetItem(setItem);
            }
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    private EnrCampaignTargetAdmissionKind createEnrCampaignTargetAdmissionKind(String kind, Map<String, Object> params) {
        EnrCampaignTargetAdmissionKind targetAdmissionKind = findEnrCampaignTargetAdmissionKind(kind);
        if(targetAdmissionKind == null) {
            targetAdmissionKind = new EnrCampaignTargetAdmissionKind();
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentCampaign.class, "e").top(1);
                List<EnrEnrollmentCampaign> l = DataAccessServices.dao().getList(builder);
                if (!l.isEmpty())
                    targetAdmissionKind.setEnrollmentCampaign(l.get(0));
            }
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrTargetAdmissionKind.class, "e").top(1);
                List<EnrTargetAdmissionKind> l = DataAccessServices.dao().getList(builder);
                if (!l.isEmpty())
                    targetAdmissionKind.setTargetAdmissionKind(l.get(0));
            }
            params.put(CAMPAIGN_TARGET_ADMISSION_KIND, targetAdmissionKind);
        }
        return targetAdmissionKind;
    }

    private ExternalOrgUnit createExternalOrgUnit(String kindOrgUnit, Map<String, Object> params) {
        ExternalOrgUnit externalOrgUnit = findExternalOrgUnit(kindOrgUnit);
        if(externalOrgUnit == null) {
            externalOrgUnit = new ExternalOrgUnit();
            externalOrgUnit.setTitle(kindOrgUnit);
            externalOrgUnit.setShortTitle(kindOrgUnit);
            externalOrgUnit.setCreateDate(new Date());
            LegalForm defaultLegalForm = DataAccessServices.dao().get(LegalForm.class, LegalForm.code(), Integer.toString(14));
            externalOrgUnit.setLegalForm(defaultLegalForm);
            params.put(EXT_ORG_UNIT, externalOrgUnit);
        }
        return externalOrgUnit;
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrRequestedCompetition> w, EnrRequestedCompetitionType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(CompensationTypeType.class, session, params, COMPENSATION, nsiPackage);

            EnrCompetition enrCompetition = (EnrCompetition) params.get(COMPETITION);
            if(enrCompetition != null)
                session.saveOrUpdate(enrCompetition);

            saveChildEntity(EnrEntrantRequestType.class, session, params, REQUEST, nsiPackage);
            saveChildEntity(EnrProgramSetOrgUnitType.class, session, params, SET_ORG_UNIT, nsiPackage);
            saveChildEntity(EnrProgramSetItemType.class, session, params, SET_ITEM, nsiPackage);

            EnrCampaignTargetAdmissionKind targetAdmissionKind = (EnrCampaignTargetAdmissionKind) params.get(CAMPAIGN_TARGET_ADMISSION_KIND);
            if(targetAdmissionKind != null)
                session.saveOrUpdate(targetAdmissionKind);

            ExternalOrgUnit externalOrgUnit = (ExternalOrgUnit) params.get(EXT_ORG_UNIT);
            if(externalOrgUnit != null)
                session.saveOrUpdate(externalOrgUnit);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
