/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EnrEnrollmentCampaignType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 09.09.2015
 */
public class EnrEnrollmentCampaignTypeReactor extends BaseEntityReactor<EnrEnrollmentCampaign, EnrEnrollmentCampaignType>
{
    private static final String CAMPAIGN_SETTINGS = "campaignSettings";

    @Override
    public Class<EnrEnrollmentCampaign> getEntityClass()
    {
        return EnrEnrollmentCampaign.class;
    }

    @Override
    public EnrEnrollmentCampaignType createDatagramObject(String guid)
    {
        EnrEnrollmentCampaignType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentCampaignType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEnrollmentCampaign entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrEnrollmentCampaignType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEnrollmentCampaignType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEnrollmentCampaignName(entity.getTitle());
        datagramObject.setEnrEnrollmentCampaignEducationYear(String.valueOf(entity.getEducationYear()));
        datagramObject.setEnrEnrollmentCampaignDateFrom(NsiUtils.formatDate(entity.getDateFrom()));
        datagramObject.setEnrEnrollmentCampaignDateTo(NsiUtils.formatDate(entity.getDateTo()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EnrEnrollmentCampaign findEnrEnrollmentCampaign(String name)
    {
        if (name != null)
        {
            List<EnrEnrollmentCampaign> list = findEntity(eq(property(ENTITY_ALIAS, EnrEnrollmentCampaign.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrEnrollmentCampaign> processDatagramObject(EnrEnrollmentCampaignType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();

        // EnrEnrollmentCampaign init
        String name = StringUtils.trimToNull(datagramObject.getEnrEnrollmentCampaignName());
        Integer educationYearValue = NsiUtils.parseInteger(datagramObject.getEnrEnrollmentCampaignEducationYear(), "EnrEnrollmentCampaignEducationYear");
        EducationYear educationYear = findEducationYear(educationYearValue);
        Date dateFrom = NsiUtils.parseDate(datagramObject.getEnrEnrollmentCampaignDateFrom(), "EnrEnrollmentCampaignDateFrom");
        Date dateTo = NsiUtils.parseDate(datagramObject.getEnrEnrollmentCampaignDateTo(), "EnrEnrollmentCampaignDateTo");

        boolean isNew = false;
        boolean isChanged = false;
        EnrEnrollmentCampaign entity = null;

        CoreCollectionUtils.Pair<EnrEnrollmentCampaign, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEnrollmentCampaign(name);
        if (entity == null)
        {
            entity = new EnrEnrollmentCampaign();
            isNew = true;
            isChanged = true;
        }

        // EnrEnrollmentCampaign set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEnrollmentCampaign.class, entity, EnrEnrollmentCampaign.P_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EnrEnrollmentCampaignType EnrEnrollmentCampaignName is not unique");
            }
        }

        if (null == entity.getTitle())
        {
            throw new ProcessingDatagramObjectException("EnrEnrollmentCampaignType object without EnrEnrollmentCampaignName!");
        }

        if (dateFrom != null && !dateFrom.equals(entity.getDateFrom()))
        {
            isChanged = true;
            entity.setDateFrom(dateFrom);
        }

        if (null == entity.getDateFrom())
        {
            throw new ProcessingDatagramObjectException("EnrEnrollmentCampaignType object without EnrEnrollmentCampaignDateFrom!");
        }

        if (dateTo != null && !dateTo.equals(entity.getDateTo()))
        {
            isChanged = true;
            entity.setDateTo(dateTo);
        }

        if (null == entity.getDateTo())
        {
            throw new ProcessingDatagramObjectException("EnrEnrollmentCampaignType object without EnrEnrollmentCampaignDateTo!");
        }

        if (educationYear != null && !educationYear.equals(entity.getEducationYear()))
        {
            isChanged = true;
            entity.setEducationYear(educationYear);
        }

        if (null == entity.getEducationYear())
        {
            throw new ProcessingDatagramObjectException("EnrEnrollmentCampaignType object without EnrEnrollmentCampaignEducationYear!");
        }

        if (null == entity.getSettings())
        {
            EnrEnrollmentCampaignSettings settings = findSettings();
            if (settings != null)
            {
                entity.setSettings(settings);
                params.put(CAMPAIGN_SETTINGS, settings);
            }
            else
            {
                throw new ProcessingDatagramObjectException("EnrEnrollmentCampaignType object without EnrEnrollmentCampaignSettings!");
            }
        }
        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrEnrollmentCampaign> w, EnrEnrollmentCampaignType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            session.saveOrUpdate(params.get(CAMPAIGN_SETTINGS));
        }
        super.save(session, w, datagramObject, nsiPackage);
    }

    private static EducationYear findEducationYear(Integer year)
    {
        if (year != null)
        {
            return DataAccessServices.dao().get(EducationYear.class, EducationYear.intValue(), year);
        }
        return null;
    }

    private static EnrEnrollmentCampaignSettings findSettings()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().top(1)
                .fromEntity(EnrEnrollmentCampaign.class, "e")
                .column("e")
                .order(EnrEnrollmentCampaign.educationYear().intValue().fromAlias("e").s(), OrderDirection.desc);

        List<EnrEnrollmentCampaign> campaignList = DataAccessServices.dao().getList(dql);
        if (campaignList.size() == 1)
        {
            EnrEnrollmentCampaignSettings settings = campaignList.get(0).getSettings();
            EnrEnrollmentCampaignSettings settingsDup = new EnrEnrollmentCampaignSettings();
            settingsDup.update(settings);
            return settingsDup;
        }
        return null;
    }
}
