/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrMethodDivCompetitions;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrMethodDivCompetitionsCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.gen.EnrRequestTypeGen;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBS;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 15.09.2015
 */
public class EnrProgramSetBaseTypeReactor extends BaseEntityReactor<EnrProgramSetBase, EnrProgramSetBaseType> {
    private static final String PROGRAM_FORM = "EduProgramFormID";
    private static final String PROGRAM_SUBJECT = "EduProgramSubjectID";
    private static final String CAMPAIGN = "EnrEnrollmentCampaignID";

    @Override
    public Class<EnrProgramSetBase> getEntityClass() {
        return EnrProgramSetBase.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrProgramSetBaseType datagramObject, IUnknownDatagramsCollector collector) {
        EnrProgramSetBaseType.EduProgramFormID programFormID = datagramObject.getEduProgramFormID();
        EduProgramFormType programFormType = programFormID == null ? null : programFormID.getEduProgramForm();
        if (programFormType != null)
            collector.check(programFormType.getID(), EduProgramFormType.class);

        EnrProgramSetBaseType.EduProgramSubjectID programSubjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType programSubjectType = programSubjectID == null ? null : programSubjectID.getEduProgramSubject();
        if (programSubjectType != null)
            collector.check(programSubjectType.getID(), EduProgramSubjectType.class);

        EnrProgramSetBaseType.EnrEnrollmentCampaignID campaignID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType campaignType = campaignID == null ? null : campaignID.getEnrEnrollmentCampaign();
        if (campaignType != null)
            collector.check(campaignType.getID(), EnrEnrollmentCampaignType.class);
    }

    @Override
    public EnrProgramSetBaseType createDatagramObject(String guid) {
        EnrProgramSetBaseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetBaseType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrProgramSetBase entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrProgramSetBaseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetBaseType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrProgramSetBaseName(entity.getTitle());

        ProcessedDatagramObject<EduProgramFormType> formType = createDatagramObject(EduProgramFormType.class, entity.getProgramForm(), commonCache, warning);
        EnrProgramSetBaseType.EduProgramFormID programFormID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetBaseTypeEduProgramFormID();
        programFormID.setEduProgramForm(formType.getObject());
        datagramObject.setEduProgramFormID(programFormID);
        if (formType.getList() != null)
            result.addAll(formType.getList());

        ProcessedDatagramObject<EduProgramSubjectType> subjectType = createDatagramObject(EduProgramSubjectType.class, entity.getProgramSubject(), commonCache, warning);
        EnrProgramSetBaseType.EduProgramSubjectID programSubjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetBaseTypeEduProgramSubjectID();
        programSubjectID.setEduProgramSubject(subjectType.getObject());
        datagramObject.setEduProgramSubjectID(programSubjectID);
        if (subjectType.getList() != null)
            result.addAll(subjectType.getList());

        ProcessedDatagramObject<EnrEnrollmentCampaignType> campaignType = createDatagramObject(EnrEnrollmentCampaignType.class, entity.getEnrollmentCampaign(), commonCache, warning);
        EnrProgramSetBaseType.EnrEnrollmentCampaignID campaignID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetBaseTypeEnrEnrollmentCampaignID();
        campaignID.setEnrEnrollmentCampaign(campaignType.getObject());
        datagramObject.setEnrEnrollmentCampaignID(campaignID);
        if (campaignType.getList() != null)
            result.addAll(campaignType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrProgramSetBase findEnrProgramSetBase(EduProgramForm programForm, EduProgramSubject programSubject, EnrEnrollmentCampaign campaign) {
        List<IExpression> expressions = new ArrayList<>();
        if (programForm != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrProgramSetBase.programForm().id()), value(programForm.getId())));
        if (programSubject != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrProgramSetBase.programSubject()), value(programSubject.getId())));
        if (campaign != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrProgramSetBase.enrollmentCampaign()), value(campaign.getId())));
        List<EnrProgramSetBase> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrProgramSetBase> processDatagramObject(EnrProgramSetBaseType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        EnrProgramSetBaseType.EduProgramFormID programFormID = datagramObject.getEduProgramFormID();
        EduProgramFormType programFormType = programFormID == null ? null : programFormID.getEduProgramForm();
        EduProgramForm programForm = getOrCreate(EduProgramFormType.class, params, commonCache, programFormType, PROGRAM_FORM, null, warning);

        EnrProgramSetBaseType.EduProgramSubjectID programSubjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType programSubjectType = programSubjectID == null ? null : programSubjectID.getEduProgramSubject();
        EduProgramSubject programSubject = getOrCreate(EduProgramSubjectType.class, params, commonCache, programSubjectType, PROGRAM_SUBJECT, null, warning);

        EnrProgramSetBaseType.EnrEnrollmentCampaignID campaignID = datagramObject.getEnrEnrollmentCampaignID();
        EnrEnrollmentCampaignType campaignType = campaignID == null ? null : campaignID.getEnrEnrollmentCampaign();
        EnrEnrollmentCampaign campaign = getOrCreate(EnrEnrollmentCampaignType.class, params, commonCache, campaignType, CAMPAIGN, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrProgramSetBase entity = null;

        CoreCollectionUtils.Pair<EnrProgramSetBase, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrProgramSetBase(programForm, programSubject, campaign);
        if (entity == null)
        {
            entity = new EnrProgramSetBS();
            entity.setMethodDivCompetitions(IUniBaseDao.instance.get().getCatalogItem(EnrMethodDivCompetitions.class, EnrMethodDivCompetitionsCodes.SEC_NO_DIV));
            entity.setRequestType(DataAccessServices.dao().<EnrRequestType>getByNaturalId(new EnrRequestTypeGen.NaturalId(EnrRequestTypeCodes.BS)));
            entity.setTitle(datagramObject.getEnrProgramSetBaseName());
            isNew = true;
            isChanged = true;
        }

        if (programForm != null && !programForm.equals(entity.getProgramForm()))
        {
            isChanged = true;
            entity.setProgramForm(programForm);
        }
        if (null == entity.getProgramForm())
            throw new ProcessingDatagramObjectException("EnrProgramSetBaseType object without EduProgramFormID!");

        if (programSubject != null && !programSubject.equals(entity.getProgramSubject()))
        {
            isChanged = true;
            entity.setProgramSubject(programSubject);
        }
        if (null == entity.getProgramSubject())
            throw new ProcessingDatagramObjectException("EnrProgramSetBaseType object without EduProgramSubjectID!");

        if (campaign != null && !campaign.equals(entity.getEnrollmentCampaign()))
        {
            isChanged = true;
            entity.setEnrollmentCampaign(campaign);
        }
        if (null == entity.getEnrollmentCampaign())
            throw new ProcessingDatagramObjectException("EnrProgramSetBaseType object without EnrEnrollmentCampaignID!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrProgramSetBase> w, EnrProgramSetBaseType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EduProgramFormType.class, session, params, PROGRAM_FORM, nsiPackage);
            saveChildEntity(EduProgramSubjectType.class, session, params, PROGRAM_SUBJECT, nsiPackage);
            saveChildEntity(EnrEnrollmentCampaignType.class, session, params, CAMPAIGN, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
