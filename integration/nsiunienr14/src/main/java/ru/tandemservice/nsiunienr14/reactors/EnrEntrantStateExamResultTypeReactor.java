/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrEntrantStateExamResultType;
import ru.tandemservice.nsiclient.datagram.EnrEntrantType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrStateExamSubject;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 17.09.2015
 */
public class EnrEntrantStateExamResultTypeReactor extends BaseEntityReactor<EnrEntrantStateExamResult, EnrEntrantStateExamResultType>
{
    private static final String ENTRANT = "entrantID";

    @Override
    public Class<EnrEntrantStateExamResult> getEntityClass()
    {
        return EnrEntrantStateExamResult.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrantStateExamResultType datagramObject, IUnknownDatagramsCollector collector)
    {
        EnrEntrantStateExamResultType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        if (entrantType != null) collector.check(entrantType.getID(), EnrEntrantType.class);
    }

    @Override
    public EnrEntrantStateExamResultType createDatagramObject(String guid)
    {
        EnrEntrantStateExamResultType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantStateExamResultType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    protected EnrEntrantStateExamResult findEnrEntrantStateExamResult(EnrEntrant entrant, EnrStateExamSubject subject, Integer year)
    {

        List<IExpression> expressions = new ArrayList<>();
        if (entrant != null && subject != null && year != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantStateExamResult.entrant()), value(entrant)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantStateExamResult.subject()), value(subject)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantStateExamResult.year()), value(year)));
            List<EnrEntrantStateExamResult> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantStateExamResult entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrEntrantStateExamResultType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantStateExamResultType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEntrantStateExamResultAccepted(NsiUtils.formatBoolean(entity.isAccepted()));
        datagramObject.setEnrEntrantStateExamResultDocumentIssuanceDate(NsiUtils.formatDate(entity.getDocumentIssuanceDate()));
        datagramObject.setEnrEntrantStateExamResultDocumentNumber(StringUtils.trimToNull(entity.getDocumentNumber()));
        datagramObject.setEnrEntrantStateExamResultMarkAsLong(entity.getMarkAsLong() != null ? String.valueOf(entity.getMarkAsLong()) : null);
        datagramObject.setEnrEntrantStateExamResultSecondWave(NsiUtils.formatBoolean(entity.isSecondWave()));
        datagramObject.setEnrEntrantStateExamResultStateCheckStatus(StringUtils.trimToNull(entity.getStateCheckStatus()));
        datagramObject.setEnrEntrantStateExamResultSubject(entity.getSubject().getTitle());
        datagramObject.setEnrEntrantStateExamResultVerifiedByUser(NsiUtils.formatBoolean(entity.isVerifiedByUser()));
        datagramObject.setEnrEntrantStateExamResultYear(String.valueOf(entity.getYear()));


        ProcessedDatagramObject<EnrEntrantType> entrantType = createDatagramObject(EnrEntrantType.class, entity.getEntrant(), commonCache, warning);
        EnrEntrantStateExamResultType.EnrEntrantID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantStateExamResultTypeEnrEntrantID();
        entrantID.setEnrEntrant(entrantType.getObject());
        datagramObject.setEnrEntrantID(entrantID);
        if (entrantType.getList() != null)
            result.addAll(entrantType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EnrEntrantStateExamResult> processDatagramObject(EnrEntrantStateExamResultType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EnrEntrantStateExamResult init
        EnrEntrantStateExamResultType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        EnrEntrant entrant = getOrCreate(EnrEntrantType.class, params, commonCache, entrantType, ENTRANT, null, warning);

        Boolean accepted = NsiUtils.parseBoolean(datagramObject.getEnrEntrantStateExamResultAccepted());
        Date issuanceDate = NsiUtils.parseDate(datagramObject.getEnrEntrantStateExamResultDocumentIssuanceDate(), "EnrEntrantStateExamResultDocumentIssuanceDate");
        String docNumber = StringUtils.trimToNull(datagramObject.getEnrEntrantStateExamResultDocumentNumber());
        Long resultMark = NsiUtils.parseLong(datagramObject.getEnrEntrantStateExamResultMarkAsLong(), "EnrEntrantStateExamResultMarkAsLong");
        Boolean secondWave = NsiUtils.parseBoolean(datagramObject.getEnrEntrantStateExamResultSecondWave());
        String checkStatus = StringUtils.trimToNull(datagramObject.getEnrEntrantStateExamResultStateCheckStatus());
        EnrStateExamSubject stateExamSubject = getEnrStateExamSubject(datagramObject.getEnrEntrantStateExamResultSubject());
        Boolean verifiedByUser = NsiUtils.parseBoolean(datagramObject.getEnrEntrantStateExamResultVerifiedByUser());
        Integer resultYear = NsiUtils.parseInteger(datagramObject.getEnrEntrantStateExamResultYear(), "EnrEntrantStateExamResultYear");

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantStateExamResult entity = null;

        CoreCollectionUtils.Pair<EnrEntrantStateExamResult, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrantStateExamResult(entrant, stateExamSubject, resultYear);
        if (entity == null)
        {
            entity = new EnrEntrantStateExamResult();
            isNew = true;
            isChanged = true;
        }

        // EnrEntrantStateExamResultType set
        if (entrant != null && !entrant.equals(entity.getEntrant()))
        {
            isChanged = true;
            entity.setEntrant(entrant);
        }
        if (entity.getEntrant() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateExamResultType object without EnrEntrantID");
        }

        if (stateExamSubject != null && !stateExamSubject.equals(entity.getSubject()))
        {
            isChanged = true;
            entity.setSubject(stateExamSubject);
        }
        if (entity.getEntrant() == null)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateExamResultType object without EnrEntrantStateExamResultSubject");
        }

        if (resultYear != null && !resultYear.equals(entity.getYear()))
        {
            isChanged = true;
            entity.setYear(resultYear);
        }
        if (entity.getYear() == 0)
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateExamResultType object without EnrEntrantStateExamResultYear");
        }

        if (accepted != null && !accepted.equals(entity.isAccepted()))
        {
            isChanged = true;
            entity.setAccepted(accepted);
        }

        if (issuanceDate != null && !issuanceDate.equals(entity.getDocumentIssuanceDate()))
        {
            isChanged = true;
            entity.setDocumentIssuanceDate(issuanceDate);
        }
        if (docNumber != null && !docNumber.equals(entity.getDocumentNumber()))
        {
            isChanged = true;
            entity.setDocumentNumber(docNumber);
        }
        if (resultMark != null && !resultMark.equals(entity.getMarkAsLong()))
        {
            isChanged = true;
            entity.setMarkAsLong(resultMark);
        }

        if (entity.getRegistrationDate() == null && isNew)
        {
            entity.setRegistrationDate(new Date());
        }

        if (secondWave != null && !secondWave.equals(entity.isSecondWave()))
        {
            isChanged = true;
            entity.setSecondWave(secondWave);
        }

        if (checkStatus != null && !checkStatus.equals(entity.getStateCheckStatus()))
        {
            isChanged = true;
            entity.setStateCheckStatus(checkStatus);
        }

        if (verifiedByUser != null && !verifiedByUser.equals(entity.isVerifiedByUser()))
        {
            isChanged = true;
            entity.setVerifiedByUser(verifiedByUser);
        }

        if (isChanged || isNew)
        {
            entity.setModificationDate(new Date());
        }

        if (entity.isSecondWave() && entity.getMarkAsLong() == null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantStateExamResult.class, entity, EnrEntrantStateExamResult.L_SUBJECT, EnrEntrantStateExamResult.L_ENTRANT))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateExamResult EnrEntrantID/EnrEntrantStateExamResultSubject combination is not unique for second wave and EnrEntrantStateExamResultMarkAsLong is a null");
        }

        if (entity.getSubject() != null && entity.getEntrant() != null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantStateExamResult.class, entity, EnrEntrantStateExamResult.L_SUBJECT, EnrEntrantStateExamResult.L_ENTRANT, EnrEntrantStateExamResult.P_YEAR))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateExamResult EnrEntrantID/EnrEntrantStateExamResultSubject/EnrEntrantStateExamResultYear combination is not unique");
        }

        if ((entity.isSecondWave() && entity.getSecondWaveExamPlace() == null) || (!entity.isSecondWave() && entity.getSecondWaveExamPlace() != null))
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateExamResult object must have (EnrEntrantStateExamResultSecondWave and SecondWaveExamPlace) or must have not (EnrEntrantStateExamResultSecondWave and SecondWaveExamPlace)");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrEntrantStateExamResult> w, EnrEntrantStateExamResultType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEntrantType.class, session, params, ENTRANT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }


    private static EnrStateExamSubject getEnrStateExamSubject(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrStateExamSubject.class, EnrStateExamSubject.title(), trimmedTitle);
        }
        return null;
    }
}
