/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EnrEntrantStateType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 09.09.2015
 */
public class EnrEntrantStateTypeReactor extends BaseEntityReactor<EnrEntrantState, EnrEntrantStateType>
{
    @Override
    public Class<EnrEntrantState> getEntityClass()
    {
        return EnrEntrantState.class;
    }

    @Override
    public EnrEntrantStateType createDatagramObject(String guid)
    {
        EnrEntrantStateType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantStateType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantState entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrEntrantStateType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantStateType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrEntrantStateID(entity.getCode());
        datagramObject.setEnrEntrantStateName(entity.getTitle());
        datagramObject.setEnrEntrantStateNameShort(entity.getShortTitle());
        datagramObject.setEnrEntrantStatePriority(String.valueOf(entity.getPriority()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EnrEntrantState findEnrEntrantState(String code, String name, String nameShort, Integer priority)
    {
        if (code != null)
        {
            List<EnrEntrantState> list = findEntity(eq(property(ENTITY_ALIAS, EnrEntrantState.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (name != null)
        {
            List<EnrEntrantState> list = findEntity(eq(property(ENTITY_ALIAS, EnrEntrantState.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }

        if (nameShort != null)
        {
            List<EnrEntrantState> list = findEntity(eq(property(ENTITY_ALIAS, EnrEntrantState.shortTitle()), value(nameShort)));
            if (list.size() == 1)
                return list.get(0);
        }

        if (priority != null)
        {
            List<EnrEntrantState> list = findEntity(eq(property(ENTITY_ALIAS, EnrEntrantState.priority()), value(priority)));
            if (list.size() == 1)
                return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrantState> processDatagramObject(EnrEntrantStateType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // EnrEntrantState init
        String code = StringUtils.trimToNull(datagramObject.getEnrEntrantStateID());
        String name = StringUtils.trimToNull(datagramObject.getEnrEntrantStateName());
        String nameShort = StringUtils.trimToNull(datagramObject.getEnrEntrantStateNameShort());
        Integer statePriority = NsiUtils.parseInteger(datagramObject.getEnrEntrantStatePriority(), "EnrEntrantStatePriority");

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantState entity = null;

        CoreCollectionUtils.Pair<EnrEntrantState, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrEntrantState(code, name, nameShort, statePriority);
        if (entity == null)
        {
            entity = new EnrEntrantState();
            isNew = true;
            isChanged = true;
        }

        // EnrEntrantState set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantState.class, entity, EnrEntrantState.P_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EnrEntrantStateType EnrEntrantStateName is not unique");
            }
        }

        if (nameShort != null && !nameShort.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(nameShort);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantState.class, entity, EnrEntrantState.P_SHORT_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EnrEntrantStateType EnrEntrantStateNameShort is not unique");
            }
        }
        if (null == entity.getShortTitle())
        {
            throw new ProcessingDatagramObjectException("EnrEntrantStateType object without EnrEntrantStateNameShort!");
        }

        if (statePriority != null && !statePriority.equals(entity.getPriority()))
        {
            isChanged = true;
            entity.setPriority(statePriority);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrEntrantState.class, entity, EnrEntrantState.P_PRIORITY);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EnrEntrantStateType EnrEntrantStatePriority is not unique");
            }
        }


        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EnrEntrantState.class, entity, EnrEntrantState.P_CODE))
            {
                if (code == null)
                    warning.append("[EnrEntrantStateType object without EnrEntrantStateTypeID!");
                else
                    warning.append("EnrEntrantStateTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EnrEntrantState.class);
                warning.append(" EnrEntrantStateTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EnrEntrantStateTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
