/* $Id$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrCampaignDisciplineType;
import ru.tandemservice.nsiclient.datagram.EnrEntrantType;
import ru.tandemservice.nsiclient.datagram.EnrExamPassDisciplineType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrAbsenceNoteCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 17.09.2015
 */
public class EnrExamPassDisciplineTypeReactor extends BaseEntityReactor<EnrExamPassDiscipline, EnrExamPassDisciplineType>
{
    private static final String ENTRANT = "entrantID";
    private static final String CAMPAIGN_DISCIPLINE = "campaignDisciplineID";

    @Override
    public Class<EnrExamPassDiscipline> getEntityClass()
    {
        return EnrExamPassDiscipline.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrExamPassDisciplineType datagramObject, IUnknownDatagramsCollector collector)
    {
        EnrExamPassDisciplineType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        if (entrantType != null)
            collector.check(entrantType.getID(), EnrEntrantType.class);

        EnrExamPassDisciplineType.EnrCampaignDisciplineID disciplineID = datagramObject.getEnrCampaignDisciplineID();
        EnrCampaignDisciplineType disciplineType = disciplineID == null ? null : disciplineID.getEnrCampaignDiscipline();
        if (disciplineType != null)
            collector.check(disciplineType.getID(), EnrCampaignDisciplineType.class);
    }

    @Override
    public EnrExamPassDisciplineType createDatagramObject(String guid)
    {
        EnrExamPassDisciplineType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrExamPassDisciplineType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrExamPassDiscipline entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrExamPassDisciplineType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrExamPassDisciplineType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrExamPassDisciplineAbsenceNote(entity.getAbsenceNote().getTitle());
        datagramObject.setEnrExamPassDisciplineMarkAsLong(entity.getMarkAsLong() != null ? String.valueOf(entity.getMarkAsLong()) : null);
        datagramObject.setEnrExamPassDisciplineMarkDate(NsiUtils.formatDate(entity.getMarkDate()));
        datagramObject.setEnrExamPassDisciplinePassForm(entity.getPassForm().getTitle());
        datagramObject.setEnrExamPassDisciplineRetake(NsiUtils.formatBoolean(entity.isRetake()));

        ProcessedDatagramObject<EnrEntrantType> entrantType = createDatagramObject(EnrEntrantType.class, entity.getEntrant(), commonCache, warning);
        EnrExamPassDisciplineType.EnrEntrantID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrExamPassDisciplineTypeEnrEntrantID();
        entrantID.setEnrEntrant(entrantType.getObject());
        datagramObject.setEnrEntrantID(entrantID);
        if (entrantType.getList() != null)
            result.addAll(entrantType.getList());

        ProcessedDatagramObject<EnrCampaignDisciplineType> disciplineType = createDatagramObject(EnrCampaignDisciplineType.class, entity.getDiscipline(), commonCache, warning);
        EnrExamPassDisciplineType.EnrCampaignDisciplineID disciplineID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrExamPassDisciplineTypeEnrCampaignDisciplineID();
        disciplineID.setEnrCampaignDiscipline(disciplineType.getObject());
        datagramObject.setEnrCampaignDisciplineID(disciplineID);
        if (disciplineType.getList() != null)
            result.addAll(disciplineType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrExamPassDiscipline findEnrExamPassDiscipline(EnrCampaignDiscipline campaignDiscipline, EnrEntrant entrant, EnrExamPassForm passForm, Boolean retake)
    {

        List<IExpression> expressions = new ArrayList<>();
        if (campaignDiscipline != null && entrant != null && passForm != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EnrExamPassDiscipline.discipline()), value(campaignDiscipline)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrExamPassDiscipline.entrant()), value(entrant)));
            expressions.add(eq(property(ENTITY_ALIAS, EnrExamPassDiscipline.passForm()), value(passForm)));
            if (retake.equals(Boolean.TRUE))
            {
                expressions.add(eq(property(ENTITY_ALIAS, EnrExamPassDiscipline.retake()), value(retake)));
            }
            else
            {
                expressions.add(eq(property(ENTITY_ALIAS, EnrExamPassDiscipline.retake()), value(Boolean.FALSE)));
            }
            List<EnrExamPassDiscipline> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrExamPassDiscipline> processDatagramObject(EnrExamPassDisciplineType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EnrExamPassDiscipline init
        EnrExamPassDisciplineType.EnrEntrantID entrantID = datagramObject.getEnrEntrantID();
        EnrEntrantType entrantType = entrantID == null ? null : entrantID.getEnrEntrant();
        EnrEntrant entrant = getOrCreate(EnrEntrantType.class, params, commonCache, entrantType, ENTRANT, null, warning);

        EnrExamPassDisciplineType.EnrCampaignDisciplineID disciplineID = datagramObject.getEnrCampaignDisciplineID();
        EnrCampaignDisciplineType disciplineType = disciplineID == null ? null : disciplineID.getEnrCampaignDiscipline();
        EnrCampaignDiscipline campaignDiscipline = getOrCreate(EnrCampaignDisciplineType.class, params, commonCache, disciplineType, CAMPAIGN_DISCIPLINE, null, warning);

        Boolean retake = NsiUtils.parseBoolean(datagramObject.getEnrExamPassDisciplineRetake());
        EnrAbsenceNote absenceNote = getEnrAbsenceNote(datagramObject.getEnrExamPassDisciplineAbsenceNote());
        Long disciplineMark = NsiUtils.parseLong(datagramObject.getEnrExamPassDisciplineMarkAsLong(), "EnrExamPassDisciplineMarkAsLong");
        Date markDate = NsiUtils.parseDate(datagramObject.getEnrExamPassDisciplineMarkDate(), "EnrExamPassDisciplineMarkDate");
        EnrExamPassForm passForm = getEnrExamPassForm(datagramObject.getEnrExamPassDisciplinePassForm());


        boolean isNew = false;
        boolean isChanged = false;
        EnrExamPassDiscipline entity = null;

        CoreCollectionUtils.Pair<EnrExamPassDiscipline, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrExamPassDiscipline(campaignDiscipline, entrant, passForm, retake);
        if (entity == null)
        {
            entity = new EnrExamPassDiscipline();
            isNew = true;
            isChanged = true;
        }

        // EnrExamPassDiscipline set
        if (campaignDiscipline != null && !campaignDiscipline.equals(entity.getDiscipline()))
        {
            isChanged = true;
            entity.setDiscipline(campaignDiscipline);
        }
        if (entity.getDiscipline() == null)
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object without EnrExamPassDisciplineDiscipline");
        }

        if (entrant != null && !entrant.equals(entity.getEntrant()))
        {
            isChanged = true;
            entity.setEntrant(entrant);
        }
        if (entity.getEntrant() == null)
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object without EnrEntrantID");
        }

        if (passForm != null && !passForm.equals(entity.getPassForm()))
        {
            isChanged = true;
            entity.setPassForm(passForm);
        }
        if (entity.getPassForm() == null)
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object without EnrExamPassDisciplinePassForm");
        }

        if (retake != null && !retake.equals(entity.isRetake()))
        {
            isChanged = true;
            entity.setRetake(retake);
        }

        if (absenceNote != null && !absenceNote.equals(entity.getAbsenceNote()))
        {
            isChanged = true;
            entity.setAbsenceNote(absenceNote);
        }

        if (entity.getAbsenceNote() != null)
        {
            if (absenceNote.getCode().equals(EnrAbsenceNoteCodes.VALID) && disciplineMark != null)
            {
                throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object with absenceNode with a good cause must have empty EnrExamPassDisciplineMarkAsLong");
            }
            else if (absenceNote.getCode().equals(EnrAbsenceNoteCodes.VALID) && disciplineMark == null && entity.getMarkAsLong() != null)
            {
                isChanged = true;
                entity.setMarkAsLong(null);
            }
            else if (absenceNote.getCode().equals(EnrAbsenceNoteCodes.UNKNOWN)
                    && (disciplineMark == null || disciplineMark != 0))
            {
                throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object with absenceNode with a good cause must have zero EnrExamPassDisciplineMarkAsLong");
            }
            else if (absenceNote.getCode().equals(EnrAbsenceNoteCodes.UNKNOWN)
                    && (disciplineMark != null && disciplineMark == 0)
                    && entity.getMarkAsLong() != 0)
            {
                isChanged = true;
                entity.setMarkAsLong(0L);
            }
        }
        else if (disciplineMark != null && !disciplineMark.equals(entity.getMarkAsLong()))
        {
            isChanged = true;
            entity.setMarkAsLong(disciplineMark);
        }


        if (markDate != null && !markDate.equals(entity.getMarkDate()))
        {
            isChanged = true;
            entity.setMarkDate(markDate);
        }

        if (entity.getDiscipline() != null && entity.getEntrant() != null && entity.getPassForm() != null
                && !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EnrExamPassDiscipline.class, entity,
                                                                              EnrExamPassDiscipline.L_DISCIPLINE, EnrExamPassDiscipline.L_ENTRANT, EnrExamPassDiscipline.L_PASS_FORM, EnrExamPassDiscipline.P_RETAKE))
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType EnrExamPassDisciplineDiscipline/EnrEntrantID/EnrExamPassDisciplinePassForm/EnrExamPassDisciplineRetake combination is not unique");
        }

        if (entity.getExamGroup() != null && (!entity.getExamGroup().getPassForm().equals(entity.getPassForm()) || !entity.getExamGroup().getDiscipline().equals(entity.getDiscipline())))
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType and ExamGroup must have the same EnrExamPassDisciplineDiscipline and EnrExamPassDisciplinePassForm");
        }

        if ((entity.getMarkAsLong() != null && entity.getMarkDate() == null)
                || (entity.getMarkAsLong() == null && entity.getMarkDate() != null))
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object must have (EnrExamPassDisciplineMarkAsLong and EnrExamPassDisciplineMarkDate) or must have not (EnrExamPassDisciplineMarkAsLong and EnrExamPassDisciplineMarkDate)");
        }

        if (entity.getPassForm() != null && !entity.getPassForm().isInternal())
        {
            throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object must have internal EnrExamPassDisciplinePassForm");
        }

        if (entity.isRetake())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "e")
                    .column(property("e"));
            if (entity.getEntrant() != null)
                builder.where(eq(property("e", EnrExamPassDiscipline.entrant()), commonValue(entity.getEntrant())));
            if (entity.getPassForm() != null)
                builder.where(eq(property("e", EnrExamPassDiscipline.passForm()), commonValue(entity.getPassForm())));
            if (entity.getDiscipline() != null)
                builder.where(eq(property("e", EnrExamPassDiscipline.discipline()), commonValue(entity.getDiscipline())));

            builder.where(isNotNull(property("e", EnrExamPassDiscipline.absenceNote())));
            builder.where(eq(property("e", EnrExamPassDiscipline.absenceNote().code()), commonValue(EnrAbsenceNoteCodes.VALID)));

            if (entity.getId() != null)
                builder.where(ne(property("e", IEntity.P_ID), value(entity.getId())));

            List<EnrExamPassDiscipline> passDisciplines = DataAccessServices.dao().getList(builder);
            if (!passDisciplines.isEmpty())
            {
                throw new ProcessingDatagramObjectException("EnrExamPassDisciplineType object retake inforamation must to comply with previous attempt");
            }
        }

        if (entity.getTerritorialOrgUnit() == null)
        {
            EnrEnrollmentCampaign enrollmentCampaign = entity.getDiscipline() != null ? entity.getDiscipline().getEnrollmentCampaign() : null;
            if (enrollmentCampaign != null)
            {
             List<EnrOrgUnit> enrOrgUnitList = DataAccessServices.dao().getList(EnrOrgUnit.class, EnrOrgUnit.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign);

                if(!enrOrgUnitList.isEmpty() && enrOrgUnitList.get(0)!=null)
                {
                    OrgUnit orgUnit = enrOrgUnitList.get(0).getInstitutionOrgUnit().getOrgUnit();
                    entity.setTerritorialOrgUnit(orgUnit);
                }
            }
        }


        if (isChanged || isNew)
        {
            entity.setModificationDate(new Date());
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }


    @Override
    public void save(Session session, ChangedWrapper<EnrExamPassDiscipline> w, EnrExamPassDisciplineType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EnrEntrantType.class, session, params, ENTRANT, nsiPackage);
            saveChildEntity(EnrCampaignDisciplineType.class, session, params, CAMPAIGN_DISCIPLINE, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    private static EnrAbsenceNote getEnrAbsenceNote(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrAbsenceNote.class, EnrAbsenceNote.title(), trimmedTitle);
        }
        return null;
    }

    private static EnrExamPassForm getEnrExamPassForm(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (!trimmedTitle.isEmpty())
        {
            return DataAccessServices.dao().get(EnrExamPassForm.class, EnrExamPassForm.title(), trimmedTitle);
        }
        return null;
    }
}
