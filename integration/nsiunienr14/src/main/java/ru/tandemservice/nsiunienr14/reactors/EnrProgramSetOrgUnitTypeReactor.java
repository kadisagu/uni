/* $Id:$ */
package ru.tandemservice.nsiunienr14.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EnrProgramSetBaseType;
import ru.tandemservice.nsiclient.datagram.EnrProgramSetOrgUnitType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 18.09.2015
 */
public class EnrProgramSetOrgUnitTypeReactor extends BaseEntityReactor<EnrProgramSetOrgUnit, EnrProgramSetOrgUnitType> {
    private static final String PROGRAM_SET_BASE = "EnrProgramSetBaseID";
    private static final String ENR_ORG_UNIT = "enrOrgUnit";

    @Override
    public Class<EnrProgramSetOrgUnit> getEntityClass() {
        return EnrProgramSetOrgUnit.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrProgramSetOrgUnitType datagramObject, IUnknownDatagramsCollector collector) {
        EnrProgramSetOrgUnitType.EnrProgramSetBaseID enrEntrantID = datagramObject.getEnrProgramSetBaseID();
        EnrProgramSetBaseType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrProgramSetBase();
        if (entrantType != null) collector.check(entrantType.getID(), EnrProgramSetBaseType.class);
    }

    @Override
    public EnrProgramSetOrgUnitType createDatagramObject(String guid) {
        EnrProgramSetOrgUnitType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetOrgUnitType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrProgramSetOrgUnit entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EnrProgramSetOrgUnitType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetOrgUnitType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(1);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEnrProgramSetOrgUnitDepartmentCode(entity.getOrgUnit().getInstitutionOrgUnit().getCode());
        datagramObject.setEnrProgramSetOrgUnitFormativeDepartmentShortName(entity.getFormativeOrgUnit().getShortTitle());

        ProcessedDatagramObject<EnrProgramSetBaseType> entrant = createDatagramObject(EnrProgramSetBaseType.class, entity.getProgramSet(), commonCache, warning);
        EnrProgramSetOrgUnitType.EnrProgramSetBaseID entrantID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrProgramSetOrgUnitTypeEnrProgramSetBaseID();
        entrantID.setEnrProgramSetBase(entrant.getObject());
        datagramObject.setEnrProgramSetBaseID(entrantID);
        if (entrant.getList() != null) result.addAll(entrant.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EnrProgramSetOrgUnit findEnrProgramSetOrgUnit(EnrProgramSetBase programSetBase)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (programSetBase != null)
            expressions.add(eq(property(ENTITY_ALIAS, EnrProgramSetOrgUnit.programSet().id()), value(programSetBase.getId())));
        List<EnrProgramSetOrgUnit> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EnrProgramSetOrgUnit> processDatagramObject(EnrProgramSetOrgUnitType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        String institutionOrgUnitCode = StringUtils.trimToNull(datagramObject.getEnrProgramSetOrgUnitDepartmentCode());
        String formativeOrgUnitTitle = StringUtils.trimToNull(datagramObject.getEnrProgramSetOrgUnitFormativeDepartmentShortName());
        EnrProgramSetOrgUnitType.EnrProgramSetBaseID enrEntrantID = datagramObject.getEnrProgramSetBaseID();
        EnrProgramSetBaseType entrantType = enrEntrantID == null ? null : enrEntrantID.getEnrProgramSetBase();
        EnrProgramSetBase programSetBase = getOrCreate(EnrProgramSetBaseType.class, params, commonCache, entrantType, PROGRAM_SET_BASE, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrProgramSetOrgUnit entity = null;

        CoreCollectionUtils.Pair<EnrProgramSetOrgUnit, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEnrProgramSetOrgUnit(programSetBase);
        if (entity == null)
        {
            entity = new EnrProgramSetOrgUnit();
            isNew = true;
            isChanged = true;
        }

        if (programSetBase != null && !programSetBase.equals(entity.getProgramSet()))
        {
            isChanged = true;
            entity.setProgramSet(programSetBase);
        }
        if (null == entity.getProgramSet())
            throw new ProcessingDatagramObjectException("EnrProgramSetOrgUnitType object without EnrProgramSetBaseID!");

        if(institutionOrgUnitCode == null && isNew)
            throw new ProcessingDatagramObjectException("EnrProgramSetOrgUnitType object without EnrProgramSetOrgUnitDepartmentCode!");

        if(institutionOrgUnitCode != null && (entity.getOrgUnit() == null || !institutionOrgUnitCode.equals(entity.getOrgUnit().getInstitutionOrgUnit().getCode()))) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduInstitutionOrgUnit.class, "e").top(1).where(eq(property("e", EduInstitutionOrgUnit.code()), value(institutionOrgUnitCode)));
            List<EduInstitutionOrgUnit> list = DataAccessServices.dao().getList(builder);
            if(list.isEmpty())
                throw new ProcessingDatagramObjectException("OrgUnit with code '" + institutionOrgUnitCode + "' not found");
            EnrOrgUnit enrOrgUnit = DataAccessServices.dao().get(EnrOrgUnit.class, EnrOrgUnit.institutionOrgUnit().id(), list.get(0).getId());
            if(enrOrgUnit == null)
            {
                enrOrgUnit = new EnrOrgUnit();
                if (null == programSetBase)
                    throw new ProcessingDatagramObjectException("EnrProgramSetOrgUnitType object without EnrProgramSetBaseID!");
                enrOrgUnit.setEnrollmentCampaign(programSetBase.getEnrollmentCampaign());
                enrOrgUnit.setInstitutionOrgUnit(list.get(0));
                params.put(ENR_ORG_UNIT, enrOrgUnit);
            }

            entity.setOrgUnit(enrOrgUnit);
        }

        if(formativeOrgUnitTitle == null && isNew)
            throw new ProcessingDatagramObjectException("EnrProgramSetOrgUnitType object without EnrProgramSetOrgUnitFormativeDepartmentShortName!");

        if(formativeOrgUnitTitle != null && (entity.getFormativeOrgUnit() == null || !formativeOrgUnitTitle.equals(entity.getFormativeOrgUnit().getShortTitle()))) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "e").top(1).where(eq(property("e", OrgUnit.shortTitle()), value(formativeOrgUnitTitle)));
            List<OrgUnit> list = DataAccessServices.dao().getList(builder);
            if(list.isEmpty())
                throw new ProcessingDatagramObjectException("OrgUnit with title '" + formativeOrgUnitTitle + "' not found");
            entity.setFormativeOrgUnit(list.get(0));
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrProgramSetOrgUnit> w, EnrProgramSetOrgUnitType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            EnrOrgUnit enrOrgUnit = (EnrOrgUnit) params.get(ENR_ORG_UNIT);
            if(enrOrgUnit != null)
                session.saveOrUpdate(enrOrgUnit);
            saveChildEntity(EnrProgramSetBaseType.class, session, params, PROGRAM_SET_BASE, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
