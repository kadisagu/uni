/* $Id: OrganizationTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiorganization.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.logic.IExternalOrgUnitDao;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.OrganizationType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 29.04.2015
 */
public class OrganizationTypeReactor extends BaseEntityReactor<ExternalOrgUnit, OrganizationType>
{
    private static final String ORGANIZATION_UPPER = "OrganizationUpperID";
    private static final String CHILD_MAP = "ChildMap";

    @Override
    public Class<ExternalOrgUnit> getEntityClass()
    {
        return ExternalOrgUnit.class;
    }

    @Override
    public OrganizationType createDatagramObject(String guid)
    {
        OrganizationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createOrganizationType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(ExternalOrgUnit entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        OrganizationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createOrganizationType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setOrganizationID(entity.getUserCode());
        datagramObject.setOrganizationName(entity.getTitle());
        datagramObject.setOrganizationPrefix(null);
        datagramObject.setOrganizationDateReg(NsiUtils.formatDate(entity.getCreateDate()));
        datagramObject.setOrganizationINN(entity.getInn());
        datagramObject.setOrganizationOPF(null);
        datagramObject.setOrganizationKFS(null);
        datagramObject.setOrganizationOKATO(entity.getOkato());
        datagramObject.setOrganizationOKVED(null);
        datagramObject.setOrganizationOKPO(entity.getOkpo());
        datagramObject.setOrganizationOKONH(entity.getOkonh());
        datagramObject.setOrganizationKPP(entity.getKpp());
        datagramObject.setOrganizationOKOPF(null);
        datagramObject.setOrganizationOGRN(entity.getOgrn());
        datagramObject.setOrganizationSubscriber(null);
        datagramObject.setOrganizationIFNS(null);
        datagramObject.setOrganizationPFR(null);
        datagramObject.setOrganizationIMNS(null);
        datagramObject.setOrganizationNameKFS(null);
        datagramObject.setOrganizationNameOKVED(null);
        datagramObject.setOrganizationNamePFR(null);
        datagramObject.setOrganizationNameFSS(null);
        datagramObject.setOrganizationNameFull(entity.getTitle());
        datagramObject.setOrganizationNameShort(entity.getShortTitle());
        datagramObject.setOrganizationRegPFR(null);
        datagramObject.setOrganizationRegFSS(null);

        StringBuilder warning = new StringBuilder();
        ExternalOrgUnit parent = entity.getParent();
        if (parent != null)
        {
            ProcessedDatagramObject<OrganizationType> r = createDatagramObject(null, parent, commonCache, warning);
            OrganizationType organizationType = r.getObject();
            OrganizationType.OrganizationUpperID organizationUpperID = new OrganizationType.OrganizationUpperID();
            organizationUpperID.setOrganization(organizationType);
            datagramObject.setOrganizationUpperID(organizationUpperID);

            if (r.getList() != null)
                result.addAll(r.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(OrganizationType datagramObject, IUnknownDatagramsCollector collector)
    {
        OrganizationType.OrganizationUpperID organizationUpperID = datagramObject.getOrganizationUpperID();
        OrganizationType organizationType = organizationUpperID == null ? null : organizationUpperID.getOrganization();
        if (organizationType != null)
            collector.check(organizationType.getID(), OrganizationType.class);
    }

    private ExternalOrgUnit findExternalOrgUnit(String title)
    {
        if (title != null)
        {
            List<ExternalOrgUnit> units = findEntity(eq(property(ENTITY_ALIAS, ExternalOrgUnit.title()), value(title)));
            if (units.size() == 1)
                return units.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<ExternalOrgUnit> processDatagramObject(OrganizationType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();

        StringBuilder warning = new StringBuilder();
        // OrganizationID init
        String code = StringUtils.trimToNull(datagramObject.getOrganizationID());
        // OrganizationName init
        String name = StringUtils.trimToNull(datagramObject.getOrganizationName());
        // OrganizationNameShort init
        String shortName = StringUtils.trimToNull(datagramObject.getOrganizationNameShort());
        // OrganizationNameFull init
        String fullName = StringUtils.trimToNull(datagramObject.getOrganizationNameFull());
        // OrganizationINN init
        String inn = StringUtils.trimToNull(datagramObject.getOrganizationINN());
        // OrganizationOPF init
        String opf = StringUtils.trimToNull(datagramObject.getOrganizationOPF());
        // OrganizationKFS init
        String kfs = StringUtils.trimToNull(datagramObject.getOrganizationKFS());
        // OrganizationOKATO init
        String okato = StringUtils.trimToNull(datagramObject.getOrganizationOKATO());
        // OrganizationOKVED init
        String okved = StringUtils.trimToNull(datagramObject.getOrganizationOKVED());
        // OrganizationOKPO init
        String okpo = StringUtils.trimToNull(datagramObject.getOrganizationOKPO());
        // OrganizationOKONH init
        String okonh = StringUtils.trimToNull(datagramObject.getOrganizationOKONH());
        // OrganizationKPP init
        String kpp = StringUtils.trimToNull(datagramObject.getOrganizationKPP());
        // OrganizationOGRN init
        String ogrn = StringUtils.trimToNull(datagramObject.getOrganizationOGRN());
        // OrganizationUpperID init
        OrganizationType.OrganizationUpperID organizationUpperID = datagramObject.getOrganizationUpperID();
        OrganizationType organizationType = organizationUpperID == null ? null : organizationUpperID.getOrganization();
        ExternalOrgUnit externalOrgUnit = null;
        if(organizationType != null) {
            if(StringUtils.trimToNull(organizationType.getID()) == null)
                warning.append("[").append(ORGANIZATION_UPPER).append(" without GUID!]");
                // нельзя ссылаться на саму себя
            else if(organizationType.getID().equals(datagramObject.getID()))
                warning.append("[").append(ORGANIZATION_UPPER).append(" GUID equals child GUID]");
            else
                externalOrgUnit = getOrCreate(OrganizationType.class, params, commonCache, organizationType, ORGANIZATION_UPPER, null, warning);
        }
        // OrganizationDateReg init
        Date regDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getOrganizationDateReg()), "organizationDateReg");

        boolean isNew = false;
        boolean isChanged = false;
        ExternalOrgUnit entity = null;
        CoreCollectionUtils.Pair<ExternalOrgUnit, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findExternalOrgUnit(name);
        if (entity == null)
        {
            entity = new ExternalOrgUnit();
            isNew = true;
            isChanged = true;
            LegalForm defaultLegalForm = DataAccessServices.dao().get(LegalForm.class, LegalForm.code(), Integer.toString(14));
            entity.setLegalForm(defaultLegalForm);
        }

        // OrganizationID set
        if (code != null && !code.equals(entity.getUserCode()))
        {
            isChanged = true;
            entity.setUserCode(code);
        }

        // OrganizationName set
        if (null == name && isNew)
        {
            if (name == null)
                name = shortName;
            if (name == null)
                name = fullName;
            if (name == null)
                throw new ProcessingDatagramObjectException("OrganizationType object without OrganizationName, OrganizationNameShort, OrganizationNameFull!");
        }
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }

        // OrganizationNameShort set
        if (null == shortName && isNew)
        {
            if (shortName == null)
                shortName = name;
            if (shortName == null)
                shortName = fullName;
            if (shortName == null)
                throw new ProcessingDatagramObjectException("OrganizationType object without OrganizationName, OrganizationNameShort, OrganizationNameFull!");
        }
        if (shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }

        // OrganizationINN set
        if (inn != null && !inn.equals(entity.getInn()))
        {
            isChanged = true;
            entity.setInn(inn);
        }
        // OrganizationOKATO set
        if (okato != null && !okato.equals(entity.getOkato()))
        {
            isChanged = true;
            entity.setOkato(okato);
        }
        // OrganizationOKPO set
        if (okpo != null && !okpo.equals(entity.getOkpo()))
        {
            isChanged = true;
            entity.setOkpo(okpo);
        }
        // OrganizationOKONH set
        if (okonh != null && !okonh.equals(entity.getOkonh()))
        {
            isChanged = true;
            entity.setOkonh(okonh);
        }
        // OrganizationKPP set
        if (kpp != null && !kpp.equals(entity.getKpp()))
        {
            isChanged = true;
            entity.setKpp(kpp);
        }
        // OrganizationOGRN set
        if (ogrn != null && !ogrn.equals(entity.getOgrn()))
        {
            isChanged = true;
            entity.setOgrn(ogrn);
        }

        if (externalOrgUnit != null && !externalOrgUnit.equals(entity.getParent()))
        {
            isChanged = true;
            entity.setParent(externalOrgUnit);
        }
//        if(organizationType != null)
//        {
//            ChangedWrapper<ExternalOrgUnit>> wrapper = processDatagramObject(organizationType, commonCache, reactorCache);
//            if (wrapper.getResult().isChanged()) {
//                params.put("OrganizationUpperID", organizationType);
//                params.put("OrganizationUpperIDParams", wrapper.getResult().getParams());
//            }
//            if (StringUtils.isNotEmpty(wrapper.getWarning()))
//                warning.append("Warning with object with ID " + organizationType.getID() + ":" + wrapper.getWarning());
//            entity.setParent(wrapper.getResult().getResult());
//        }

        if (regDate == null && isNew)
        {
            regDate = new Date();
            isChanged = true;
            warning.append("[Warning with object with ID ").append(datagramObject.getID()).append(": OrganizationDateReg is null]");
        }
        if (regDate != null && !regDate.equals(entity.getCreateDate()))
        {
            isChanged = true;
            entity.setCreateDate(regDate);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<ExternalOrgUnit> w, OrganizationType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            @SuppressWarnings("unchecked")
            ChangedWrapper<ExternalOrgUnit> eoucw = (ChangedWrapper<ExternalOrgUnit>) params.get(ORGANIZATION_UPPER);
            if (eoucw != null)
                save(session, eoucw, (OrganizationType) params.get(ORGANIZATION_UPPER + PARAM), nsiPackage);
        }

        NamedSyncInTransactionCheckLocker.register(session, IExternalOrgUnitDao.EXTERNAL_ORG_UNIT_HIERARCHY_LOCK);
        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<IDatagramObject> createTestObjects()
    {
        OrganizationType parent = createParentTestObject();
        OrganizationType entity = createChildTestObject(parent);

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        datagrams.add(parent);
        return datagrams;
    }


    public OrganizationType createParentTestObject()
    {
        OrganizationType root = new OrganizationType();
        root.setID("cc5d937a-3202-3c23-8a12-orgTypexxx0X");

        root.setOrganizationID("99999");
        root.setOrganizationName("organizationNameTestRoot");
        root.setOrganizationDateReg("2014-02-03");
        root.setOrganizationName("organizationNameTestRoot");
        root.setOrganizationNameFull("organizationNameFullTestRoot");
        root.setOrganizationNameShort("organizationNameShortTestRoot");
        root.setOrganizationINN("111111112");
        root.setOrganizationOKATO("2222222");
        root.setOrganizationOKPO("33333332");
        root.setOrganizationOKONH("44444442");
        root.setOrganizationKPP("55555552");
        root.setOrganizationOGRN("666666662");
        return root;
    }

    public OrganizationType createChildTestObject(OrganizationType parent)
    {

        OrganizationType entity = new OrganizationType();
        entity.setID("cc5d937a-3202-3c23-8a12-orgTypexxx1X");

        entity.setOrganizationID("99998");
        entity.setOrganizationName("organizationNameTest");
        entity.setOrganizationDateReg("2014-02-03");
        entity.setOrganizationName("organizationNameTest");
        entity.setOrganizationNameFull("organizationNameFullTest");
        entity.setOrganizationNameShort("organizationNameShortTest");
        entity.setOrganizationINN("11111111");
        entity.setOrganizationOKATO("222222");
        entity.setOrganizationOKPO("3333333");
        entity.setOrganizationOKONH("4444444");
        entity.setOrganizationKPP("5555555");
        entity.setOrganizationOGRN("66666666");

        OrganizationType.OrganizationUpperID x = new OrganizationType.OrganizationUpperID();
        x.setOrganization(parent);
        entity.setOrganizationUpperID(x);

        return entity;
    }

    @Override
    public boolean isCorrect(ExternalOrgUnit entity, OrganizationType datagramObject, List<IDatagramObject> retrievedDatagram)
    {
        OrganizationType organizationType = createChildTestObject(createParentTestObject());
        return organizationType.getOrganizationName().equals(entity.getTitle())
                && organizationType.getOrganizationNameShort().equals(entity.getShortTitle())
                && organizationType.getOrganizationINN().equals(entity.getInn())
                && organizationType.getOrganizationOKATO().equals(entity.getOkato())
                && organizationType.getOrganizationOKPO().equals(entity.getOkpo())
                && organizationType.getOrganizationOKONH().equals(entity.getOkonh())
                && organizationType.getOrganizationKPP().equals(entity.getKpp())
                && organizationType.getOrganizationOGRN().equals(entity.getOgrn())
                && organizationType.getOrganizationDateReg().equals(NsiUtils.DATE_FORMAT.format(entity.getCreateDate()));
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {

        List<String> fields = super.getNotValidatedFieldList();
        fields.add(ExternalOrgUnit.P_CODE);
        fields.add(ExternalOrgUnit.L_LEGAL_ADDRESS);
        fields.add(ExternalOrgUnit.P_DIVISION);
        fields.add(ExternalOrgUnit.P_PAYEE);
        fields.add(ExternalOrgUnit.P_BANK);
        fields.add(ExternalOrgUnit.P_ARCHIVAL_DATE);
        fields.add(ExternalOrgUnit.L_SCOPE_OF_ACTIVITY);
        fields.add(ExternalOrgUnit.P_WEBPAGE);
        fields.add(ExternalOrgUnit.P_FAX);
        fields.add(ExternalOrgUnit.P_BASIC_ACTS);
        fields.add(ExternalOrgUnit.P_PER_ACCOUNT);
        fields.add(ExternalOrgUnit.P_COR_ACCOUNT);
        fields.add(ExternalOrgUnit.P_BANK_BRANCH);
        fields.add(ExternalOrgUnit.P_ACTIVE);
        fields.add(ExternalOrgUnit.L_FACT_ADDRESS);
        fields.add(ExternalOrgUnit.L_LEGAL_FORM);
        fields.add(ExternalOrgUnit.P_CUR_ACCOUNT);
        fields.add(ExternalOrgUnit.P_REGISTRY_NUMBER);
        fields.add(ExternalOrgUnit.P_PHONE);
        fields.add(ExternalOrgUnit.P_TOTAL_STAFF);
        fields.add(ExternalOrgUnit.P_BIC);
        fields.add(ExternalOrgUnit.P_GAIN_CODE);
        fields.add(ExternalOrgUnit.P_COMMENT);
        fields.add(ExternalOrgUnit.P_EMAIL_LIST);
        fields.add(ExternalOrgUnit.P_MISCELLANEOUS_DETAILS);

        return  fields;
    }
}
