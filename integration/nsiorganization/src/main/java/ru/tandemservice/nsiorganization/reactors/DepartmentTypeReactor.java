/* $Id: DepartmentTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiorganization.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AddressRUType;
import ru.tandemservice.nsiclient.datagram.DepartmentType;
import ru.tandemservice.nsiclient.datagram.DepartmentTypeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class DepartmentTypeReactor extends BaseEntityReactor<OrgUnit, DepartmentType>
{
    private static final String DEPARTMENT_TYPE = "DepartmentTypeID";
    private static final String DEPARTMENT_UPPER = "DepartmentUpperID";
    private static final String POSTAL_ADDRESS = "DepartmentPostalAddreessID";
    private static final String LEGAL_ADDRESS = "DepartmentLegalAddreessID";
    private static final String FACT_ADDRESS = "DepartmentFactAddreessID";

    @Override
    public Class<OrgUnit> getEntityClass()
    {
        return OrgUnit.class;
    }

    @Override
    public DepartmentType createDatagramObject(String guid)
    {
        DepartmentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(OrgUnit entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        DepartmentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setDepartmentID(entity.getDivisionCode());
        datagramObject.setDepartmentName(entity.getPrintTitle());
        datagramObject.setIsArchive(NsiUtils.formatBoolean(entity.isArchival()));
        datagramObject.setOrganizationID(null);

        if (entity instanceof TopOrgUnit)
        {
            TopOrgUnit top = (TopOrgUnit) entity;
            datagramObject.setDepartmentOKATOCode(top.getOkato());
            datagramObject.setDepartmentKPP(top.getKpp());
        }

        OrgUnitType type = entity.getOrgUnitType();
        StringBuilder warning = new StringBuilder();
        if (type != null)
        {
            ProcessedDatagramObject<DepartmentTypeType> r = createDatagramObject(DepartmentTypeType.class, type, commonCache, warning);
            DepartmentType.DepartmentTypeID departmentTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypeDepartmentTypeID();
            departmentTypeID.setDepartmentType(r.getObject());
            datagramObject.setDepartmentTypeID(departmentTypeID);
            if (r.getList() != null) result.addAll(r.getList());
        }

        OrgUnit parent = entity.getParent();
        if (parent != null)
        {
            ProcessedDatagramObject<DepartmentType> r = createDatagramObject(null, parent, commonCache, warning);
            DepartmentType.DepartmentUpperID departmentUpperID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypeDepartmentUpperID();
            departmentUpperID.setDepartment(r.getObject());
            datagramObject.setDepartmentUpperID(departmentUpperID);

            if (r.getList() != null)
                result.addAll(r.getList());
        }

        AddressDetailed postalAddr = entity.getPostalAddress();
        if (postalAddr != null)
        {
            ProcessedDatagramObject<AddressRUType> r = createDatagramObject(AddressRUType.class, postalAddr, commonCache, warning);
            DepartmentType.DepartmentPostalAddressID postalAddrID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypePostalAddressID();
            postalAddrID.setAddressRu(r.getObject());
            datagramObject.setDepartmentPostalAddressID(postalAddrID);

            if (r.getList() != null) result.addAll(r.getList());
        }

        AddressDetailed legalAddr = entity.getLegalAddress();
        if (legalAddr != null)
        {
            ProcessedDatagramObject<AddressRUType> r = createDatagramObject(AddressRUType.class, legalAddr, commonCache, warning);
            DepartmentType.DepartmentLegalAddressID legalAddrID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypeLegalAddressID();
            legalAddrID.setAddressRu(r.getObject());
            datagramObject.setDepartmentLegalAddressID(legalAddrID);

            if (r.getList() != null) result.addAll(r.getList());
        }

        AddressDetailed factAddr = entity.getAddress();
        if (factAddr != null)
        {
            ProcessedDatagramObject<AddressRUType> r = createDatagramObject(AddressRUType.class, factAddr, commonCache, warning);
            DepartmentType.DepartmentFactAddressID factAddrID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypeFactAddressID();
            factAddrID.setAddressRu(r.getObject());
            datagramObject.setDepartmentFactAddressID(factAddrID);

            if (r.getList() != null) result.addAll(r.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(DepartmentType datagramObject, IUnknownDatagramsCollector collector)
    {
        DepartmentType.DepartmentTypeID departmentTypeID = datagramObject.getDepartmentTypeID();
        DepartmentTypeType departmentTypeType = departmentTypeID == null ? null : departmentTypeID.getDepartmentType();
        if (departmentTypeType != null) collector.check(departmentTypeType.getID(), DepartmentTypeType.class);

        DepartmentType.DepartmentUpperID departmentUpperID = datagramObject.getDepartmentUpperID();
        DepartmentType departmentType = departmentUpperID == null ? null : departmentUpperID.getDepartment();
        if (departmentType != null) collector.check(departmentType.getID(), DepartmentType.class);

        DepartmentType.DepartmentPostalAddressID postalAddrID = datagramObject.getDepartmentPostalAddressID();
        AddressRUType postalAddr = postalAddrID == null ? null : postalAddrID.getAddressRu();
        if (postalAddr != null) collector.check(postalAddr.getID(), AddressRUType.class);

        DepartmentType.DepartmentLegalAddressID legalAddrID = datagramObject.getDepartmentLegalAddressID();
        AddressRUType legalAddr = legalAddrID == null ? null : legalAddrID.getAddressRu();
        if (legalAddr != null) collector.check(legalAddr.getID(), AddressRUType.class);

        DepartmentType.DepartmentFactAddressID factAddrID = datagramObject.getDepartmentFactAddressID();
        AddressRUType factAddr = factAddrID == null ? null : factAddrID.getAddressRu();
        if (factAddr != null) collector.check(factAddr.getID(), AddressRUType.class);
    }

    private OrgUnit findOrgUnit(String title)
    {
        if (title != null)
        {
            List<OrgUnit> units = findEntity(eq(property(ENTITY_ALIAS, OrgUnit.title()), value(title)));
            if (units.size() == 1)
                return units.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<OrgUnit> processDatagramObject(DepartmentType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // DepartmentID init
        String code = StringUtils.trimToNull(datagramObject.getDepartmentID());
        // DepartmentName init
        String name = StringUtils.trimToNull(datagramObject.getDepartmentName());
        // DepartmentOKATOCode init
        String okato = StringUtils.trimToNull(datagramObject.getDepartmentOKATOCode());
        // DepartmentKPP init
        String kpp = StringUtils.trimToNull(datagramObject.getDepartmentKPP());
        // IsArchive init
        Boolean isArchive = NsiUtils.parseBoolean(datagramObject.getIsArchive());

        // DepartmentTypeID init
        DepartmentType.DepartmentTypeID departmentTypeID = datagramObject.getDepartmentTypeID();
        DepartmentTypeType departmentTypeType = departmentTypeID == null ? null : departmentTypeID.getDepartmentType();
        OrgUnitType type = null;
        // нельзя ссылаться на саму себя
        if (departmentTypeType != null)
        {
            type = getOrCreate(DepartmentTypeType.class, params, commonCache, departmentTypeType, DEPARTMENT_TYPE, null, warning);
        }

        // DepartmentUpperID init
        DepartmentType.DepartmentUpperID departmentUpperID = datagramObject.getDepartmentUpperID();
        DepartmentType departmentType = departmentUpperID == null ? null : departmentUpperID.getDepartment();
        OrgUnit parent = null;
        // нельзя ссылаться на саму себя
        if (departmentType != null)
        {
            if (StringUtils.trimToNull(departmentType.getID()) == null)
                warning.append("[").append(DEPARTMENT_UPPER).append(" without GUID!]");
            else if (!departmentType.getID().equals(datagramObject.getID()))
                parent = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT_UPPER, null, warning);
        }

        // DepartmentPostalAddressID init
        DepartmentType.DepartmentPostalAddressID postalAddrID = datagramObject.getDepartmentPostalAddressID();
        AddressRUType postalAddrType = postalAddrID == null ? null : postalAddrID.getAddressRu();
        AddressDetailed postalAddr = null;
        if (postalAddrType != null)
        {
            if (StringUtils.trimToNull(postalAddrType.getID()) == null)
                warning.append("[").append(POSTAL_ADDRESS).append(" without GUID!]");

            postalAddr = getOrCreate(AddressRUType.class, params, commonCache, postalAddrType, POSTAL_ADDRESS, null, warning);
        }

        // DepartmentLegalAddressID init
        DepartmentType.DepartmentLegalAddressID legalAddrID = datagramObject.getDepartmentLegalAddressID();
        AddressRUType legalAddrType = legalAddrID == null ? null : legalAddrID.getAddressRu();
        AddressDetailed legalAddr = null;
        if (legalAddrType != null)
        {
            if (StringUtils.trimToNull(legalAddrType.getID()) == null)
                warning.append("[").append(LEGAL_ADDRESS).append(" without GUID!]");

            legalAddr = getOrCreate(AddressRUType.class, params, commonCache, legalAddrType, LEGAL_ADDRESS, null, warning);
        }

        // DepartmentFactAddressID init
        DepartmentType.DepartmentFactAddressID factAddrID = datagramObject.getDepartmentFactAddressID();
        AddressRUType factAddrType = factAddrID == null ? null : factAddrID.getAddressRu();
        AddressDetailed factAddr = null;
        if (factAddrType != null)
        {
            if (StringUtils.trimToNull(factAddrType.getID()) == null)
                warning.append("[").append(FACT_ADDRESS).append(" without GUID!]");

            postalAddr = getOrCreate(AddressRUType.class, params, commonCache, factAddrType, FACT_ADDRESS, null, warning);
        }

        boolean isNew = false;
        boolean isChanged = false;
        OrgUnit entity = null;
        CoreCollectionUtils.Pair<OrgUnit, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findOrgUnit(name);
        if (entity == null)
        {
            if (parent == null)
                throw new ProcessingDatagramObjectException("DepartmentType object without DepartmentUpperID!");
            else {
                entity = new OrgUnit();
            }

            isNew = true;
            isChanged = true;

            entity.setOrgUnitType(SharedBaseDao.instance.get().getCatalogItem(OrgUnitType.class, "division"));
        }

        if (null != type)
        {
            if(null == entity.getOrgUnitType() || (null != entity.getOrgUnitType() && !entity.getOrgUnitType().equals(type)))
                isChanged = true;

            entity.setOrgUnitType(type);
        }

        if (null != postalAddr) entity.setPostalAddress(postalAddr);
        if (null != legalAddr) entity.setLegalAddress(legalAddr);
        if (null != factAddr) entity.setAddress(factAddr);

        // OrganizationID set
        if (code != null && !code.equals(entity.getDivisionCode()))
        {
            isChanged = true;
            entity.setDivisionCode(code);
        }

        // DepartmentName set
        if (null == name && isNew)
            throw new ProcessingDatagramObjectException("DepartmentType object without DepartmentName!");
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            if (entity.getTerritorialTitle() == null)
            {
                isChanged = true;
                entity.setTerritorialTitle(name);
            }
            if (entity.getShortTitle() == null)
            {
                isChanged = true;
                entity.setShortTitle(name);
            }
            if (entity.getTerritorialShortTitle() == null)
            {
                isChanged = true;
                entity.setTerritorialShortTitle(name);
            }
            if (entity.getFullTitle() == null)
            {
                isChanged = true;
                entity.setFullTitle(name);
            }
            if (entity.getTerritorialFullTitle() == null)
            {
                isChanged = true;
                entity.setTerritorialFullTitle(name);
            }
        }
        // IsArchive set
        if (isArchive != null && isArchive != entity.isArchival())
        {
            isChanged = true;
            entity.setArchival(isArchive);
        }

        if (entity instanceof TopOrgUnit)
        {
            TopOrgUnit topOrgUnit = (TopOrgUnit) entity;
            // DepartmentOKATOCode set
            if (okato != null && !okato.equals(topOrgUnit.getOkato()))
            {
                isChanged = true;
                topOrgUnit.setOkato(okato);
            }
            // DepartmentKPP set
            if (kpp != null && !kpp.equals(topOrgUnit.getKpp()))
            {
                isChanged = true;
                topOrgUnit.setKpp(kpp);
            }
        }
        else
        {
            // DepartmentUpperID set
            if (parent == null && isNew)
                throw new ProcessingDatagramObjectException("DepartmentType object without DepartmentUpperID!");
            if (parent != null && !parent.equals(entity.getParent()))
            {
                isChanged = true;
                entity.setParent(parent);
            }
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<OrgUnit> w, DepartmentType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            @SuppressWarnings("unchecked")
            ChangedWrapper<OrgUnit> oucw = (ChangedWrapper<OrgUnit>) params.get(DEPARTMENT_UPPER);
            if (oucw != null) save(session, oucw, (DepartmentType) params.get(DEPARTMENT_UPPER + PARAM), nsiPackage);

            saveChildEntity(DepartmentTypeType.class, session, params, DEPARTMENT_TYPE, nsiPackage);
            saveChildEntity(AddressRUType.class, session, params, POSTAL_ADDRESS, nsiPackage);
            saveChildEntity(AddressRUType.class, session, params, LEGAL_ADDRESS, nsiPackage);
            saveChildEntity(AddressRUType.class, session, params, FACT_ADDRESS, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<IDatagramObject> createTestObjects()
    {
        DepartmentType entity = new DepartmentType();
        entity.setID("cc5d937a-3202-3c23-8a12-departmentxX");
        entity.setDepartmentName("departmentNameTest");
        entity.setIsArchive("1");
        entity.setDepartmentID("99999");
        entity.setDepartmentUpperID(new DepartmentType.DepartmentUpperID());
        entity.getDepartmentUpperID().setDepartment(getEmbedded(true));

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        datagrams.add(getEmbedded(false));
        return datagrams;
    }

    @Override
    public DepartmentType getEmbedded(boolean onlyGuid)
    {
        DepartmentType entity = new DepartmentType();
        entity.setID("cc5d937a-3202-3c23-8a12-department2X");
        if (!onlyGuid)
        {
            OrgUnit topOrgUnit = TopOrgUnit.getInstance();
            entity.setDepartmentName(topOrgUnit.getTitle());
        }
        return entity;
    }

    @Override
    public boolean isCorrect(OrgUnit entity, DepartmentType datagramObject, List<IDatagramObject> retrievedDatagram)
    {
        return !(datagramObject.getDepartmentName().equals(entity.getTitle())
                && (datagramObject.getIsArchive() != null ? datagramObject.getIsArchive() : "0").equals(entity.isArchival() ? "1" : "0")
                && (datagramObject.getDepartmentUpperID() != null || entity.getParent() != null))
                    || ((DepartmentType) findDatagramObject(retrievedDatagram, datagramObject.getDepartmentUpperID() == null ? null : datagramObject.getDepartmentUpperID().getDepartment().getID())).getDepartmentName().equals(entity.getParent().getTitle());
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(OrgUnit.P_CORPS);
        fields.add(OrgUnit.L_POSTAL_ADDRESS);
        fields.add(OrgUnit.P_PHONE);
        fields.add(OrgUnit.P_INTERNAL_PHONE);
        fields.add(OrgUnit.P_ACCUSATIVE_CASE_TITLE);
        fields.add(OrgUnit.P_DATIVE_CASE_TITLE);
        fields.add(OrgUnit.P_GENITIVE_CASE_TITLE);
        fields.add(OrgUnit.P_NOMINATIVE_CASE_TITLE);
        fields.add(OrgUnit.P_INSTRUMENTAL_CASE_TITLE);
        fields.add(OrgUnit.P_PREPOSITIONAL_CASE_TITLE);
        fields.add(OrgUnit.P_DESCRIPTION);
        fields.add(OrgUnit.P_SHORT_TITLE);
        fields.add(OrgUnit.L_LEGAL_ADDRESS);
        fields.add(OrgUnit.P_RANK);
        fields.add(OrgUnit.P_WEBPAGE);
        fields.add(OrgUnit.P_TAG);
        fields.add(OrgUnit.P_FAX);
        fields.add(OrgUnit.P_TERRITORIAL_FULL_TITLE);
        fields.add(OrgUnit.P_TERRITORIAL_TITLE);
        fields.add(OrgUnit.P_TERRITORIAL_SHORT_TITLE);
        fields.add(OrgUnit.P_EMAIL);
        fields.add(OrgUnit.P_ARCHIVING_DATE);
        fields.add(OrgUnit.P_PERSONNEL_DEPARTMENT);
        fields.add(OrgUnit.P_FULL_TITLE);
        fields.add(OrgUnit.P_AUDIENCE);
        fields.add(OrgUnit.L_ADDRESS);
        fields.add(OrgUnit.P_ACCOUNTING_CODE);
        fields.add(OrgUnit.L_ORG_UNIT_TYPE);

        return  fields;
    }
}
