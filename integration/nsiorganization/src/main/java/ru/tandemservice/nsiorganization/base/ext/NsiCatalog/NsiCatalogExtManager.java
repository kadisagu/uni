/* $Id$ */
package ru.tandemservice.nsiorganization.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<String[]> hidedExtPoint()
    {
        return itemListExtension(_catalogManager.hidedExtPoint())
                .add(OrgUnit.ENTITY_NAME, new String[]{OrgUnit.P_LOCAL_ROLE_CONTEXT_TITLE})
                .create();
    }
}
