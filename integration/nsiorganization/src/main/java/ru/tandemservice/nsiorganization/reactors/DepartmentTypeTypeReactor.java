/* $Id$ */
package ru.tandemservice.nsiorganization.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.DepartmentTypeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 28.07.2016
 */
public class DepartmentTypeTypeReactor extends BaseEntityReactor<OrgUnitType, DepartmentTypeType>
{
    private static int maxOccupiedPriority = -1;

    @Override
    public Class<OrgUnitType> getEntityClass()
    {
        return OrgUnitType.class;
    }

    @Override
    public DepartmentTypeType createDatagramObject(String guid)
    {
        DepartmentTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(OrgUnitType entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        DepartmentTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDepartmentTypeType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setDepartmentTypeID(entity.getCode());
        datagramObject.setDepartmentTypeName(entity.getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected OrgUnitType findOrgUnitType(String code, String name)
    {
        if (code != null)
        {
            List<OrgUnitType> list = findEntity(eq(property(ENTITY_ALIAS, OrgUnitType.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (name != null)
        {
            List<OrgUnitType> list = findEntity(eq(property(ENTITY_ALIAS, OrgUnitType.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<OrgUnitType> processDatagramObject(DepartmentTypeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // OrgUnitType init
        String code = StringUtils.trimToNull(datagramObject.getDepartmentTypeID());
        String name = StringUtils.trimToNull(datagramObject.getDepartmentTypeName());

        boolean isNew = false;
        boolean isChanged = false;
        OrgUnitType entity = null;

        CoreCollectionUtils.Pair<OrgUnitType, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findOrgUnitType(code, name);
        if (entity == null)
        {
            entity = new OrgUnitType();
            isNew = true;
            isChanged = true;
        }

        // OrgUnitType set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(OrgUnitType.class, entity, OrgUnitType.P_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("DepartmentTypeType OrgUnitTypeName is not unique");
            }
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(OrgUnitType.class, entity, OrgUnitType.P_CODE))
            {
                if (code == null)
                    warning.append("[DepartmentTypeType object without DepartmentTypeTypeID!");
                else
                    warning.append("DepartmentTypeTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(OrgUnitType.class);
                warning.append(" DepartmentTypeTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }

            entity.setAllowArchival(true);
            entity.setAllowEmployee(true);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[DepartmentTypeTypeID is immutable!]");

        if (null == entity.getDative()) entity.setDative(entity.getTitle());
        if (null == entity.getGenitive()) entity.setGenitive(entity.getTitle());
        if (null == entity.getPlural()) entity.setPlural(entity.getTitle());
        if (null == entity.getDativePlural()) entity.setDativePlural(entity.getTitle());

        if(maxOccupiedPriority < 0) maxOccupiedPriority = DataAccessServices.dao().getCount(OrgUnitType.class) + 1;
        if (isNew && 0 == entity.getPriority()) entity.setPriority(++maxOccupiedPriority);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
