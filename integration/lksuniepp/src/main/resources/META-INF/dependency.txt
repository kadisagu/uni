org.tandemframework core 1.6.18-SNAPSHOT 
org.tandemframework db-support 1.6.18-SNAPSHOT 
org.tandemframework hib-support 1.6.18-SNAPSHOT 
org.tandemframework rtf 1.6.18-SNAPSHOT 
org.tandemframework caf 1.6.18-SNAPSHOT 
org.tandemframework tap-support 1.6.18-SNAPSHOT 
org.tandemframework common 1.6.18-SNAPSHOT 
org.tandemframework sec 1.6.18-SNAPSHOT 
org.tandemframework.shared sandbox 1.11.4-SH-SNAPSHOT Sandbox
org.tandemframework.shared commonbase 1.11.4-SH-SNAPSHOT Общие прикладные функции
org.tandemframework.shared fias 1.11.4-SH-SNAPSHOT Реестр адресов
org.tandemframework.shared person 1.11.4-SH-SNAPSHOT Персоны
org.tandemframework.shared cxfws 1.11.4-SH-SNAPSHOT Веб-сервисы CXF
ru.tandemservice.nsiclient nsiclient 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (базовые механизмы для обмена с НСИ)
ru.tandemservice.nsiclient lksbase 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Личный кабинет студента. Базовые модули)
org.tandemframework __classreplace 1.6.18-SNAPSHOT 
org.tandemframework.shared organization 1.11.4-SH-SNAPSHOT Оргструктура
org.tandemframework.shared image 1.11.4-SH-SNAPSHOT Работа с изображениями
org.tandemframework.shared archive 1.11.4-SH-SNAPSHOT Архив
org.tandemframework ldap 1.6.18-SNAPSHOT 
ru.tandemservice.uni.product unibase 2.11.4-UNI-SNAPSHOT Базовый модуль
ru.tandemservice.uni.product uniedu 2.11.4-UNI-SNAPSHOT Образовательные программы
org.tandemframework.shared employeebase 1.11.4-SH-SNAPSHOT Кадровый реестр
org.tandemframework.shared ctr 1.11.4-SH-SNAPSHOT Базовые договоры
org.tandemframework.shared survey 1.11.4-SH-SNAPSHOT Анкетирование
ru.tandemservice.uni.product uni 2.11.4-UNI-SNAPSHOT Студенты
ru.tandemservice.nsiclient nsiuni 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Базовые модули УНИ)
ru.tandemservice.nsiclient nsifias 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Реестр адресов)
ru.tandemservice.nsiclient nsiperson 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Персоны)
ru.tandemservice.nsiclient nsiorganization 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Оргструктура)
ru.tandemservice.nsiclient nsiemployeebase 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Кадровый реестр)
ru.tandemservice.nsiclient nsictr 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Базовые договоры)
ru.tandemservice.uni.product unieductr 2.11.4-UNI-SNAPSHOT Договоры на обучение
ru.tandemservice.nsiclient nsiuniedu 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Образовательные программы)
ru.tandemservice.uni.product unictr 2.11.4-UNI-SNAPSHOT Контрагенты (старый)
ru.tandemservice.uni.product uniepp 2.11.4-UNI-SNAPSHOT Учебный процесс
ru.tandemservice.nsiclient nsiuniepp 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Учебный процесс)
ru.tandemservice.nsiclient lksuniepp 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Личный кабинет студента. Учебный процесс)
