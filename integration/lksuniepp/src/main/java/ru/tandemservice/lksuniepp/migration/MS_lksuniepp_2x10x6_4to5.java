package ru.tandemservice.lksuniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_lksuniepp_2x10x6_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность lksEduPlanVersion

		// создана новая сущность
		if (!tool.tableExists("lkseduplanversion_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("lkseduplanversion_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_lkseduplanversion"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entityid_p", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("text_p", DBType.BLOB), 
				new DBColumn("planversion_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("lksEduPlanVersion");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность lksEduPlanWeekPeriod

		// создана новая сущность
		if (!tool.tableExists("lkseduplanweekperiod_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("lkseduplanweekperiod_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_lkseduplanweekperiod"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("eduplanversion_id", DBType.LONG).setNullable(false), 
				new DBColumn("course_id", DBType.LONG).setNullable(false), 
				new DBColumn("term_id", DBType.LONG).setNullable(false), 
				new DBColumn("weektype_id", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("monthnumberstart_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("monthnumberend_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("daynumberstart_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("daynumberend_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("lksEduPlanWeekPeriod");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность lksStudent2EduPlanVersion

		// создана новая сущность
		if (!tool.tableExists("lksstudent2eduplanversion_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("lksstudent2eduplanversion_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_lksstudent2eduplanversion"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entityid_p", DBType.LONG).setNullable(false), 
				new DBColumn("student_id", DBType.LONG), 
				new DBColumn("planversion_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("lksStudent2EduPlanVersion");

		}


    }
}