/* $Id:$ */
package ru.tandemservice.lksuniepp.base.bo.LksEppSystem.logic;

import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.ILksDao;

import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public interface ILksEppDao extends ILksDao
{
    void doInintFullLksEntitySync(Collection<Long> studentIdCollection, String entityType);

    List<Long> getEntityToUpdateIdList(Long entityId);

    int doSyncEntityPortion(int elementsToProcessAmount);
}