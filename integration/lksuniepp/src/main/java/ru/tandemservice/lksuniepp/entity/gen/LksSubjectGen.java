package ru.tandemservice.lksuniepp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина реестра для личного кабинета студента (Subject)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksSubjectGen extends EntityBase
 implements INaturalIdentifiable<LksSubjectGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksuniepp.entity.LksSubject";
    public static final String ENTITY_NAME = "lksSubject";
    public static final int VERSION_HASH = -1901992412;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String L_REQ_ELEMENT = "reqElement";

    private long _entityId;     // Идентификатор связанного объекта
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private String _fullTitle;     // Полное название
    private EppRegistryElement _reqElement;     // Дисциплина реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Полное название.
     */
    @Length(max=255)
    public String getFullTitle()
    {
        return _fullTitle;
    }

    /**
     * @param fullTitle Полное название.
     */
    public void setFullTitle(String fullTitle)
    {
        dirty(_fullTitle, fullTitle);
        _fullTitle = fullTitle;
    }

    /**
     * @return Дисциплина реестра.
     */
    public EppRegistryElement getReqElement()
    {
        return _reqElement;
    }

    /**
     * @param reqElement Дисциплина реестра.
     */
    public void setReqElement(EppRegistryElement reqElement)
    {
        dirty(_reqElement, reqElement);
        _reqElement = reqElement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksSubjectGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksSubject)another).getEntityId());
            }
            setTitle(((LksSubject)another).getTitle());
            setShortTitle(((LksSubject)another).getShortTitle());
            setFullTitle(((LksSubject)another).getFullTitle());
            setReqElement(((LksSubject)another).getReqElement());
        }
    }

    public INaturalId<LksSubjectGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksSubjectGen>
    {
        private static final String PROXY_NAME = "LksSubjectNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksSubjectGen.NaturalId) ) return false;

            LksSubjectGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksSubjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksSubject.class;
        }

        public T newInstance()
        {
            return (T) new LksSubject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "fullTitle":
                    return obj.getFullTitle();
                case "reqElement":
                    return obj.getReqElement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "fullTitle":
                    obj.setFullTitle((String) value);
                    return;
                case "reqElement":
                    obj.setReqElement((EppRegistryElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "fullTitle":
                        return true;
                case "reqElement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "fullTitle":
                    return true;
                case "reqElement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "fullTitle":
                    return String.class;
                case "reqElement":
                    return EppRegistryElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksSubject> _dslPath = new Path<LksSubject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksSubject");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Полное название.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getFullTitle()
     */
    public static PropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @return Дисциплина реестра.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getReqElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> reqElement()
    {
        return _dslPath.reqElement();
    }

    public static class Path<E extends LksSubject> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _fullTitle;
        private EppRegistryElement.Path<EppRegistryElement> _reqElement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksSubjectGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LksSubjectGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(LksSubjectGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Полное название.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getFullTitle()
     */
        public PropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new PropertyPath<String>(LksSubjectGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @return Дисциплина реестра.
     * @see ru.tandemservice.lksuniepp.entity.LksSubject#getReqElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> reqElement()
        {
            if(_reqElement == null )
                _reqElement = new EppRegistryElement.Path<EppRegistryElement>(L_REQ_ELEMENT, this);
            return _reqElement;
        }

        public Class getEntityClass()
        {
            return LksSubject.class;
        }

        public String getEntityName()
        {
            return "lksSubject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
