package ru.tandemservice.lksuniepp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Период учебного графика для личного кабинета студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksEduPlanWeekPeriodGen extends EntityBase
 implements INaturalIdentifiable<LksEduPlanWeekPeriodGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod";
    public static final String ENTITY_NAME = "lksEduPlanWeekPeriod";
    public static final int VERSION_HASH = -2136538005;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String L_WEEK_TYPE = "weekType";
    public static final String P_TITLE = "title";
    public static final String P_MONTH_NUMBER_START = "monthNumberStart";
    public static final String P_MONTH_NUMBER_END = "monthNumberEnd";
    public static final String P_DAY_NUMBER_START = "dayNumberStart";
    public static final String P_DAY_NUMBER_END = "dayNumberEnd";

    private EppEduPlanVersion _eduPlanVersion;     // Версия учебного плана
    private Course _course;     // Курс
    private Term _term;     // Семестр
    private EppWeekType _weekType;     // Тип деятельности в учебном графике
    private String _title;     // Наименование периода
    private int _monthNumberStart;     // Номер месяца начала периода
    private int _monthNumberEnd;     // Номер месяца окончания периода
    private int _dayNumberStart;     // Номер дня начала периода
    private int _dayNumberEnd;     // Номер дня окончания периода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Тип деятельности в учебном графике. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип деятельности в учебном графике. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    /**
     * @return Наименование периода. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование периода. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер месяца начала периода. Свойство не может быть null.
     */
    @NotNull
    public int getMonthNumberStart()
    {
        return _monthNumberStart;
    }

    /**
     * @param monthNumberStart Номер месяца начала периода. Свойство не может быть null.
     */
    public void setMonthNumberStart(int monthNumberStart)
    {
        dirty(_monthNumberStart, monthNumberStart);
        _monthNumberStart = monthNumberStart;
    }

    /**
     * @return Номер месяца окончания периода. Свойство не может быть null.
     */
    @NotNull
    public int getMonthNumberEnd()
    {
        return _monthNumberEnd;
    }

    /**
     * @param monthNumberEnd Номер месяца окончания периода. Свойство не может быть null.
     */
    public void setMonthNumberEnd(int monthNumberEnd)
    {
        dirty(_monthNumberEnd, monthNumberEnd);
        _monthNumberEnd = monthNumberEnd;
    }

    /**
     * @return Номер дня начала периода. Свойство не может быть null.
     */
    @NotNull
    public int getDayNumberStart()
    {
        return _dayNumberStart;
    }

    /**
     * @param dayNumberStart Номер дня начала периода. Свойство не может быть null.
     */
    public void setDayNumberStart(int dayNumberStart)
    {
        dirty(_dayNumberStart, dayNumberStart);
        _dayNumberStart = dayNumberStart;
    }

    /**
     * @return Номер дня окончания периода. Свойство не может быть null.
     */
    @NotNull
    public int getDayNumberEnd()
    {
        return _dayNumberEnd;
    }

    /**
     * @param dayNumberEnd Номер дня окончания периода. Свойство не может быть null.
     */
    public void setDayNumberEnd(int dayNumberEnd)
    {
        dirty(_dayNumberEnd, dayNumberEnd);
        _dayNumberEnd = dayNumberEnd;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksEduPlanWeekPeriodGen)
        {
            if (withNaturalIdProperties)
            {
                setEduPlanVersion(((LksEduPlanWeekPeriod)another).getEduPlanVersion());
                setCourse(((LksEduPlanWeekPeriod)another).getCourse());
                setTerm(((LksEduPlanWeekPeriod)another).getTerm());
                setWeekType(((LksEduPlanWeekPeriod)another).getWeekType());
            }
            setTitle(((LksEduPlanWeekPeriod)another).getTitle());
            setMonthNumberStart(((LksEduPlanWeekPeriod)another).getMonthNumberStart());
            setMonthNumberEnd(((LksEduPlanWeekPeriod)another).getMonthNumberEnd());
            setDayNumberStart(((LksEduPlanWeekPeriod)another).getDayNumberStart());
            setDayNumberEnd(((LksEduPlanWeekPeriod)another).getDayNumberEnd());
        }
    }

    public INaturalId<LksEduPlanWeekPeriodGen> getNaturalId()
    {
        return new NaturalId(getEduPlanVersion(), getCourse(), getTerm(), getWeekType());
    }

    public static class NaturalId extends NaturalIdBase<LksEduPlanWeekPeriodGen>
    {
        private static final String PROXY_NAME = "LksEduPlanWeekPeriodNaturalProxy";

        private Long _eduPlanVersion;
        private Long _course;
        private Long _term;
        private Long _weekType;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersion eduPlanVersion, Course course, Term term, EppWeekType weekType)
        {
            _eduPlanVersion = ((IEntity) eduPlanVersion).getId();
            _course = ((IEntity) course).getId();
            _term = ((IEntity) term).getId();
            _weekType = ((IEntity) weekType).getId();
        }

        public Long getEduPlanVersion()
        {
            return _eduPlanVersion;
        }

        public void setEduPlanVersion(Long eduPlanVersion)
        {
            _eduPlanVersion = eduPlanVersion;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public Long getTerm()
        {
            return _term;
        }

        public void setTerm(Long term)
        {
            _term = term;
        }

        public Long getWeekType()
        {
            return _weekType;
        }

        public void setWeekType(Long weekType)
        {
            _weekType = weekType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksEduPlanWeekPeriodGen.NaturalId) ) return false;

            LksEduPlanWeekPeriodGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduPlanVersion(), that.getEduPlanVersion()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            if( !equals(getTerm(), that.getTerm()) ) return false;
            if( !equals(getWeekType(), that.getWeekType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduPlanVersion());
            result = hashCode(result, getCourse());
            result = hashCode(result, getTerm());
            result = hashCode(result, getWeekType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduPlanVersion());
            sb.append("/");
            sb.append(getCourse());
            sb.append("/");
            sb.append(getTerm());
            sb.append("/");
            sb.append(getWeekType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksEduPlanWeekPeriodGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksEduPlanWeekPeriod.class;
        }

        public T newInstance()
        {
            return (T) new LksEduPlanWeekPeriod();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "weekType":
                    return obj.getWeekType();
                case "title":
                    return obj.getTitle();
                case "monthNumberStart":
                    return obj.getMonthNumberStart();
                case "monthNumberEnd":
                    return obj.getMonthNumberEnd();
                case "dayNumberStart":
                    return obj.getDayNumberStart();
                case "dayNumberEnd":
                    return obj.getDayNumberEnd();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "monthNumberStart":
                    obj.setMonthNumberStart((Integer) value);
                    return;
                case "monthNumberEnd":
                    obj.setMonthNumberEnd((Integer) value);
                    return;
                case "dayNumberStart":
                    obj.setDayNumberStart((Integer) value);
                    return;
                case "dayNumberEnd":
                    obj.setDayNumberEnd((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "course":
                        return true;
                case "term":
                        return true;
                case "weekType":
                        return true;
                case "title":
                        return true;
                case "monthNumberStart":
                        return true;
                case "monthNumberEnd":
                        return true;
                case "dayNumberStart":
                        return true;
                case "dayNumberEnd":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "course":
                    return true;
                case "term":
                    return true;
                case "weekType":
                    return true;
                case "title":
                    return true;
                case "monthNumberStart":
                    return true;
                case "monthNumberEnd":
                    return true;
                case "dayNumberStart":
                    return true;
                case "dayNumberEnd":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "course":
                    return Course.class;
                case "term":
                    return Term.class;
                case "weekType":
                    return EppWeekType.class;
                case "title":
                    return String.class;
                case "monthNumberStart":
                    return Integer.class;
                case "monthNumberEnd":
                    return Integer.class;
                case "dayNumberStart":
                    return Integer.class;
                case "dayNumberEnd":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksEduPlanWeekPeriod> _dslPath = new Path<LksEduPlanWeekPeriod>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksEduPlanWeekPeriod");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Тип деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    /**
     * @return Наименование периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер месяца начала периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getMonthNumberStart()
     */
    public static PropertyPath<Integer> monthNumberStart()
    {
        return _dslPath.monthNumberStart();
    }

    /**
     * @return Номер месяца окончания периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getMonthNumberEnd()
     */
    public static PropertyPath<Integer> monthNumberEnd()
    {
        return _dslPath.monthNumberEnd();
    }

    /**
     * @return Номер дня начала периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getDayNumberStart()
     */
    public static PropertyPath<Integer> dayNumberStart()
    {
        return _dslPath.dayNumberStart();
    }

    /**
     * @return Номер дня окончания периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getDayNumberEnd()
     */
    public static PropertyPath<Integer> dayNumberEnd()
    {
        return _dslPath.dayNumberEnd();
    }

    public static class Path<E extends LksEduPlanWeekPeriod> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private Course.Path<Course> _course;
        private Term.Path<Term> _term;
        private EppWeekType.Path<EppWeekType> _weekType;
        private PropertyPath<String> _title;
        private PropertyPath<Integer> _monthNumberStart;
        private PropertyPath<Integer> _monthNumberEnd;
        private PropertyPath<Integer> _dayNumberStart;
        private PropertyPath<Integer> _dayNumberEnd;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Тип деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

    /**
     * @return Наименование периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LksEduPlanWeekPeriodGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер месяца начала периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getMonthNumberStart()
     */
        public PropertyPath<Integer> monthNumberStart()
        {
            if(_monthNumberStart == null )
                _monthNumberStart = new PropertyPath<Integer>(LksEduPlanWeekPeriodGen.P_MONTH_NUMBER_START, this);
            return _monthNumberStart;
        }

    /**
     * @return Номер месяца окончания периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getMonthNumberEnd()
     */
        public PropertyPath<Integer> monthNumberEnd()
        {
            if(_monthNumberEnd == null )
                _monthNumberEnd = new PropertyPath<Integer>(LksEduPlanWeekPeriodGen.P_MONTH_NUMBER_END, this);
            return _monthNumberEnd;
        }

    /**
     * @return Номер дня начала периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getDayNumberStart()
     */
        public PropertyPath<Integer> dayNumberStart()
        {
            if(_dayNumberStart == null )
                _dayNumberStart = new PropertyPath<Integer>(LksEduPlanWeekPeriodGen.P_DAY_NUMBER_START, this);
            return _dayNumberStart;
        }

    /**
     * @return Номер дня окончания периода. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod#getDayNumberEnd()
     */
        public PropertyPath<Integer> dayNumberEnd()
        {
            if(_dayNumberEnd == null )
                _dayNumberEnd = new PropertyPath<Integer>(LksEduPlanWeekPeriodGen.P_DAY_NUMBER_END, this);
            return _dayNumberEnd;
        }

        public Class getEntityClass()
        {
            return LksEduPlanWeekPeriod.class;
        }

        public String getEntityName()
        {
            return "lksEduPlanWeekPeriod";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
