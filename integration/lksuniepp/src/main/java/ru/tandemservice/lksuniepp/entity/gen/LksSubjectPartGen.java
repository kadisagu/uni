package ru.tandemservice.lksuniepp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksuniepp.entity.LksSubjectPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть дисциплины реестра для личного кабинета студента (Semester)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksSubjectPartGen extends EntityBase
 implements INaturalIdentifiable<LksSubjectPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksuniepp.entity.LksSubjectPart";
    public static final String ENTITY_NAME = "lksSubjectPart";
    public static final int VERSION_HASH = 1657547788;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_NUMBER = "number";
    public static final String P_LECTURES = "lectures";
    public static final String P_PRACTICES = "practices";
    public static final String P_LABS = "labs";
    public static final String P_SEMINARS = "seminars";
    public static final String P_CREDIT = "credit";
    public static final String P_DIFF_CREDIT = "diffCredit";
    public static final String P_EXAM = "exam";
    public static final String P_EXAM_ACCUM = "examAccum";
    public static final String P_COURSE_WORK = "courseWork";
    public static final String P_COURSE_PROJECT = "courseProject";
    public static final String P_CONTROL_WORK = "controlWork";
    public static final String L_SUBJECT = "subject";

    private long _entityId;     // Идентификатор связанного объекта
    private String _number;     // Номер части
    private String _lectures;     // Аудиторная нагрузка. Лекции
    private String _practices;     // Аудиторная нагрузка. Практические занятия
    private String _labs;     // Аудиторная нагрузка. Лабораторные работы
    private String _seminars;     // Аудиторная нагрузка. Семинары
    private String _credit;     // Формы итогового контроля. Зачет
    private String _diffCredit;     // Формы итогового контроля. Дифференцированный зачет
    private String _exam;     // Формы итогового контроля. Экзамен
    private String _examAccum;     // Формы итогового контроля. Накопительный экзамен
    private String _courseWork;     // Формы итогового контроля. Кусовая работа
    private String _courseProject;     // Формы итогового контроля. Кусовой проект
    private String _controlWork;     // Формы итогового контроля. Контрольная работа
    private LksSubject _subject;     // Дисциплина реестра для личного кабинета студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Номер части. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер части. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Аудиторная нагрузка. Лекции.
     */
    @Length(max=255)
    public String getLectures()
    {
        return _lectures;
    }

    /**
     * @param lectures Аудиторная нагрузка. Лекции.
     */
    public void setLectures(String lectures)
    {
        dirty(_lectures, lectures);
        _lectures = lectures;
    }

    /**
     * @return Аудиторная нагрузка. Практические занятия.
     */
    @Length(max=255)
    public String getPractices()
    {
        return _practices;
    }

    /**
     * @param practices Аудиторная нагрузка. Практические занятия.
     */
    public void setPractices(String practices)
    {
        dirty(_practices, practices);
        _practices = practices;
    }

    /**
     * @return Аудиторная нагрузка. Лабораторные работы.
     */
    @Length(max=255)
    public String getLabs()
    {
        return _labs;
    }

    /**
     * @param labs Аудиторная нагрузка. Лабораторные работы.
     */
    public void setLabs(String labs)
    {
        dirty(_labs, labs);
        _labs = labs;
    }

    /**
     * @return Аудиторная нагрузка. Семинары.
     */
    @Length(max=255)
    public String getSeminars()
    {
        return _seminars;
    }

    /**
     * @param seminars Аудиторная нагрузка. Семинары.
     */
    public void setSeminars(String seminars)
    {
        dirty(_seminars, seminars);
        _seminars = seminars;
    }

    /**
     * @return Формы итогового контроля. Зачет.
     */
    @Length(max=255)
    public String getCredit()
    {
        return _credit;
    }

    /**
     * @param credit Формы итогового контроля. Зачет.
     */
    public void setCredit(String credit)
    {
        dirty(_credit, credit);
        _credit = credit;
    }

    /**
     * @return Формы итогового контроля. Дифференцированный зачет.
     */
    @Length(max=255)
    public String getDiffCredit()
    {
        return _diffCredit;
    }

    /**
     * @param diffCredit Формы итогового контроля. Дифференцированный зачет.
     */
    public void setDiffCredit(String diffCredit)
    {
        dirty(_diffCredit, diffCredit);
        _diffCredit = diffCredit;
    }

    /**
     * @return Формы итогового контроля. Экзамен.
     */
    @Length(max=255)
    public String getExam()
    {
        return _exam;
    }

    /**
     * @param exam Формы итогового контроля. Экзамен.
     */
    public void setExam(String exam)
    {
        dirty(_exam, exam);
        _exam = exam;
    }

    /**
     * @return Формы итогового контроля. Накопительный экзамен.
     */
    @Length(max=255)
    public String getExamAccum()
    {
        return _examAccum;
    }

    /**
     * @param examAccum Формы итогового контроля. Накопительный экзамен.
     */
    public void setExamAccum(String examAccum)
    {
        dirty(_examAccum, examAccum);
        _examAccum = examAccum;
    }

    /**
     * @return Формы итогового контроля. Кусовая работа.
     */
    @Length(max=255)
    public String getCourseWork()
    {
        return _courseWork;
    }

    /**
     * @param courseWork Формы итогового контроля. Кусовая работа.
     */
    public void setCourseWork(String courseWork)
    {
        dirty(_courseWork, courseWork);
        _courseWork = courseWork;
    }

    /**
     * @return Формы итогового контроля. Кусовой проект.
     */
    @Length(max=255)
    public String getCourseProject()
    {
        return _courseProject;
    }

    /**
     * @param courseProject Формы итогового контроля. Кусовой проект.
     */
    public void setCourseProject(String courseProject)
    {
        dirty(_courseProject, courseProject);
        _courseProject = courseProject;
    }

    /**
     * @return Формы итогового контроля. Контрольная работа.
     */
    @Length(max=255)
    public String getControlWork()
    {
        return _controlWork;
    }

    /**
     * @param controlWork Формы итогового контроля. Контрольная работа.
     */
    public void setControlWork(String controlWork)
    {
        dirty(_controlWork, controlWork);
        _controlWork = controlWork;
    }

    /**
     * @return Дисциплина реестра для личного кабинета студента.
     */
    public LksSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Дисциплина реестра для личного кабинета студента.
     */
    public void setSubject(LksSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksSubjectPartGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksSubjectPart)another).getEntityId());
            }
            setNumber(((LksSubjectPart)another).getNumber());
            setLectures(((LksSubjectPart)another).getLectures());
            setPractices(((LksSubjectPart)another).getPractices());
            setLabs(((LksSubjectPart)another).getLabs());
            setSeminars(((LksSubjectPart)another).getSeminars());
            setCredit(((LksSubjectPart)another).getCredit());
            setDiffCredit(((LksSubjectPart)another).getDiffCredit());
            setExam(((LksSubjectPart)another).getExam());
            setExamAccum(((LksSubjectPart)another).getExamAccum());
            setCourseWork(((LksSubjectPart)another).getCourseWork());
            setCourseProject(((LksSubjectPart)another).getCourseProject());
            setControlWork(((LksSubjectPart)another).getControlWork());
            setSubject(((LksSubjectPart)another).getSubject());
        }
    }

    public INaturalId<LksSubjectPartGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksSubjectPartGen>
    {
        private static final String PROXY_NAME = "LksSubjectPartNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksSubjectPartGen.NaturalId) ) return false;

            LksSubjectPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksSubjectPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksSubjectPart.class;
        }

        public T newInstance()
        {
            return (T) new LksSubjectPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "number":
                    return obj.getNumber();
                case "lectures":
                    return obj.getLectures();
                case "practices":
                    return obj.getPractices();
                case "labs":
                    return obj.getLabs();
                case "seminars":
                    return obj.getSeminars();
                case "credit":
                    return obj.getCredit();
                case "diffCredit":
                    return obj.getDiffCredit();
                case "exam":
                    return obj.getExam();
                case "examAccum":
                    return obj.getExamAccum();
                case "courseWork":
                    return obj.getCourseWork();
                case "courseProject":
                    return obj.getCourseProject();
                case "controlWork":
                    return obj.getControlWork();
                case "subject":
                    return obj.getSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "lectures":
                    obj.setLectures((String) value);
                    return;
                case "practices":
                    obj.setPractices((String) value);
                    return;
                case "labs":
                    obj.setLabs((String) value);
                    return;
                case "seminars":
                    obj.setSeminars((String) value);
                    return;
                case "credit":
                    obj.setCredit((String) value);
                    return;
                case "diffCredit":
                    obj.setDiffCredit((String) value);
                    return;
                case "exam":
                    obj.setExam((String) value);
                    return;
                case "examAccum":
                    obj.setExamAccum((String) value);
                    return;
                case "courseWork":
                    obj.setCourseWork((String) value);
                    return;
                case "courseProject":
                    obj.setCourseProject((String) value);
                    return;
                case "controlWork":
                    obj.setControlWork((String) value);
                    return;
                case "subject":
                    obj.setSubject((LksSubject) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "number":
                        return true;
                case "lectures":
                        return true;
                case "practices":
                        return true;
                case "labs":
                        return true;
                case "seminars":
                        return true;
                case "credit":
                        return true;
                case "diffCredit":
                        return true;
                case "exam":
                        return true;
                case "examAccum":
                        return true;
                case "courseWork":
                        return true;
                case "courseProject":
                        return true;
                case "controlWork":
                        return true;
                case "subject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "number":
                    return true;
                case "lectures":
                    return true;
                case "practices":
                    return true;
                case "labs":
                    return true;
                case "seminars":
                    return true;
                case "credit":
                    return true;
                case "diffCredit":
                    return true;
                case "exam":
                    return true;
                case "examAccum":
                    return true;
                case "courseWork":
                    return true;
                case "courseProject":
                    return true;
                case "controlWork":
                    return true;
                case "subject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "number":
                    return String.class;
                case "lectures":
                    return String.class;
                case "practices":
                    return String.class;
                case "labs":
                    return String.class;
                case "seminars":
                    return String.class;
                case "credit":
                    return String.class;
                case "diffCredit":
                    return String.class;
                case "exam":
                    return String.class;
                case "examAccum":
                    return String.class;
                case "courseWork":
                    return String.class;
                case "courseProject":
                    return String.class;
                case "controlWork":
                    return String.class;
                case "subject":
                    return LksSubject.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksSubjectPart> _dslPath = new Path<LksSubjectPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksSubjectPart");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Номер части. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Аудиторная нагрузка. Лекции.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getLectures()
     */
    public static PropertyPath<String> lectures()
    {
        return _dslPath.lectures();
    }

    /**
     * @return Аудиторная нагрузка. Практические занятия.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getPractices()
     */
    public static PropertyPath<String> practices()
    {
        return _dslPath.practices();
    }

    /**
     * @return Аудиторная нагрузка. Лабораторные работы.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getLabs()
     */
    public static PropertyPath<String> labs()
    {
        return _dslPath.labs();
    }

    /**
     * @return Аудиторная нагрузка. Семинары.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getSeminars()
     */
    public static PropertyPath<String> seminars()
    {
        return _dslPath.seminars();
    }

    /**
     * @return Формы итогового контроля. Зачет.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getCredit()
     */
    public static PropertyPath<String> credit()
    {
        return _dslPath.credit();
    }

    /**
     * @return Формы итогового контроля. Дифференцированный зачет.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getDiffCredit()
     */
    public static PropertyPath<String> diffCredit()
    {
        return _dslPath.diffCredit();
    }

    /**
     * @return Формы итогового контроля. Экзамен.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getExam()
     */
    public static PropertyPath<String> exam()
    {
        return _dslPath.exam();
    }

    /**
     * @return Формы итогового контроля. Накопительный экзамен.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getExamAccum()
     */
    public static PropertyPath<String> examAccum()
    {
        return _dslPath.examAccum();
    }

    /**
     * @return Формы итогового контроля. Кусовая работа.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getCourseWork()
     */
    public static PropertyPath<String> courseWork()
    {
        return _dslPath.courseWork();
    }

    /**
     * @return Формы итогового контроля. Кусовой проект.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getCourseProject()
     */
    public static PropertyPath<String> courseProject()
    {
        return _dslPath.courseProject();
    }

    /**
     * @return Формы итогового контроля. Контрольная работа.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getControlWork()
     */
    public static PropertyPath<String> controlWork()
    {
        return _dslPath.controlWork();
    }

    /**
     * @return Дисциплина реестра для личного кабинета студента.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getSubject()
     */
    public static LksSubject.Path<LksSubject> subject()
    {
        return _dslPath.subject();
    }

    public static class Path<E extends LksSubjectPart> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _number;
        private PropertyPath<String> _lectures;
        private PropertyPath<String> _practices;
        private PropertyPath<String> _labs;
        private PropertyPath<String> _seminars;
        private PropertyPath<String> _credit;
        private PropertyPath<String> _diffCredit;
        private PropertyPath<String> _exam;
        private PropertyPath<String> _examAccum;
        private PropertyPath<String> _courseWork;
        private PropertyPath<String> _courseProject;
        private PropertyPath<String> _controlWork;
        private LksSubject.Path<LksSubject> _subject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksSubjectPartGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Номер части. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(LksSubjectPartGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Аудиторная нагрузка. Лекции.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getLectures()
     */
        public PropertyPath<String> lectures()
        {
            if(_lectures == null )
                _lectures = new PropertyPath<String>(LksSubjectPartGen.P_LECTURES, this);
            return _lectures;
        }

    /**
     * @return Аудиторная нагрузка. Практические занятия.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getPractices()
     */
        public PropertyPath<String> practices()
        {
            if(_practices == null )
                _practices = new PropertyPath<String>(LksSubjectPartGen.P_PRACTICES, this);
            return _practices;
        }

    /**
     * @return Аудиторная нагрузка. Лабораторные работы.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getLabs()
     */
        public PropertyPath<String> labs()
        {
            if(_labs == null )
                _labs = new PropertyPath<String>(LksSubjectPartGen.P_LABS, this);
            return _labs;
        }

    /**
     * @return Аудиторная нагрузка. Семинары.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getSeminars()
     */
        public PropertyPath<String> seminars()
        {
            if(_seminars == null )
                _seminars = new PropertyPath<String>(LksSubjectPartGen.P_SEMINARS, this);
            return _seminars;
        }

    /**
     * @return Формы итогового контроля. Зачет.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getCredit()
     */
        public PropertyPath<String> credit()
        {
            if(_credit == null )
                _credit = new PropertyPath<String>(LksSubjectPartGen.P_CREDIT, this);
            return _credit;
        }

    /**
     * @return Формы итогового контроля. Дифференцированный зачет.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getDiffCredit()
     */
        public PropertyPath<String> diffCredit()
        {
            if(_diffCredit == null )
                _diffCredit = new PropertyPath<String>(LksSubjectPartGen.P_DIFF_CREDIT, this);
            return _diffCredit;
        }

    /**
     * @return Формы итогового контроля. Экзамен.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getExam()
     */
        public PropertyPath<String> exam()
        {
            if(_exam == null )
                _exam = new PropertyPath<String>(LksSubjectPartGen.P_EXAM, this);
            return _exam;
        }

    /**
     * @return Формы итогового контроля. Накопительный экзамен.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getExamAccum()
     */
        public PropertyPath<String> examAccum()
        {
            if(_examAccum == null )
                _examAccum = new PropertyPath<String>(LksSubjectPartGen.P_EXAM_ACCUM, this);
            return _examAccum;
        }

    /**
     * @return Формы итогового контроля. Кусовая работа.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getCourseWork()
     */
        public PropertyPath<String> courseWork()
        {
            if(_courseWork == null )
                _courseWork = new PropertyPath<String>(LksSubjectPartGen.P_COURSE_WORK, this);
            return _courseWork;
        }

    /**
     * @return Формы итогового контроля. Кусовой проект.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getCourseProject()
     */
        public PropertyPath<String> courseProject()
        {
            if(_courseProject == null )
                _courseProject = new PropertyPath<String>(LksSubjectPartGen.P_COURSE_PROJECT, this);
            return _courseProject;
        }

    /**
     * @return Формы итогового контроля. Контрольная работа.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getControlWork()
     */
        public PropertyPath<String> controlWork()
        {
            if(_controlWork == null )
                _controlWork = new PropertyPath<String>(LksSubjectPartGen.P_CONTROL_WORK, this);
            return _controlWork;
        }

    /**
     * @return Дисциплина реестра для личного кабинета студента.
     * @see ru.tandemservice.lksuniepp.entity.LksSubjectPart#getSubject()
     */
        public LksSubject.Path<LksSubject> subject()
        {
            if(_subject == null )
                _subject = new LksSubject.Path<LksSubject>(L_SUBJECT, this);
            return _subject;
        }

        public Class getEntityClass()
        {
            return LksSubjectPart.class;
        }

        public String getEntityName()
        {
            return "lksSubjectPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
