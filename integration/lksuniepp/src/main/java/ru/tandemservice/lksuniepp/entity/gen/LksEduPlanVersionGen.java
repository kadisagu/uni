package ru.tandemservice.lksuniepp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Версия учебного плана для личного кабинета студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksEduPlanVersionGen extends EntityBase
 implements INaturalIdentifiable<LksEduPlanVersionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksuniepp.entity.LksEduPlanVersion";
    public static final String ENTITY_NAME = "lksEduPlanVersion";
    public static final int VERSION_HASH = 1611682615;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_TITLE = "title";
    public static final String P_TEXT = "text";
    public static final String L_PLAN_VERSION = "planVersion";

    private long _entityId;     // Идентификатор связанного объекта
    private String _title;     // Название
    private byte[] _text;     // Сохраненная печатная форма
    private EppEduPlanVersion _planVersion;     // Версия учебного плана

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сохраненная печатная форма.
     */
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Сохраненная печатная форма.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Версия учебного плана.
     */
    public EppEduPlanVersion getPlanVersion()
    {
        return _planVersion;
    }

    /**
     * @param planVersion Версия учебного плана.
     */
    public void setPlanVersion(EppEduPlanVersion planVersion)
    {
        dirty(_planVersion, planVersion);
        _planVersion = planVersion;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksEduPlanVersionGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksEduPlanVersion)another).getEntityId());
            }
            setTitle(((LksEduPlanVersion)another).getTitle());
            setText(((LksEduPlanVersion)another).getText());
            setPlanVersion(((LksEduPlanVersion)another).getPlanVersion());
        }
    }

    public INaturalId<LksEduPlanVersionGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksEduPlanVersionGen>
    {
        private static final String PROXY_NAME = "LksEduPlanVersionNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksEduPlanVersionGen.NaturalId) ) return false;

            LksEduPlanVersionGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksEduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksEduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new LksEduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "title":
                    return obj.getTitle();
                case "text":
                    return obj.getText();
                case "planVersion":
                    return obj.getPlanVersion();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
                case "planVersion":
                    obj.setPlanVersion((EppEduPlanVersion) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "title":
                        return true;
                case "text":
                        return true;
                case "planVersion":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "title":
                    return true;
                case "text":
                    return true;
                case "planVersion":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "title":
                    return String.class;
                case "text":
                    return byte[].class;
                case "planVersion":
                    return EppEduPlanVersion.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksEduPlanVersion> _dslPath = new Path<LksEduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksEduPlanVersion");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Версия учебного плана.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> planVersion()
    {
        return _dslPath.planVersion();
    }

    public static class Path<E extends LksEduPlanVersion> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _title;
        private PropertyPath<byte[]> _text;
        private EppEduPlanVersion.Path<EppEduPlanVersion> _planVersion;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksEduPlanVersionGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(LksEduPlanVersionGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(LksEduPlanVersionGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Версия учебного плана.
     * @see ru.tandemservice.lksuniepp.entity.LksEduPlanVersion#getPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> planVersion()
        {
            if(_planVersion == null )
                _planVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_PLAN_VERSION, this);
            return _planVersion;
        }

        public Class getEntityClass()
        {
            return LksEduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "lksEduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
