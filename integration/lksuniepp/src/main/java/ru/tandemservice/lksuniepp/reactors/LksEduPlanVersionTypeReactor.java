/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.lksuniepp.entity.LksEduPlanVersion;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksEduPlanVersionType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ConditionalDatagramArrayList;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2016
 */
public class LksEduPlanVersionTypeReactor extends BaseEntityReactor<LksEduPlanVersion, LksEduPlanVersionType>
{
    private static final String LKS_SUBJECT = "LksSubjectID";

    @Override
    public Class<LksEduPlanVersion> getEntityClass()
    {
        return LksEduPlanVersion.class;
    }

    @Override
    public LksEduPlanVersionType createDatagramObject(String guid)
    {
        LksEduPlanVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksEduPlanVersionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksEduPlanVersion entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksEduPlanVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksEduPlanVersionType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksEduPlanVersionID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksEduPlanVersionName(entity.getTitle());
        datagramObject.setLksEduPlanVersionContent(entity.getText());

        return new ResultListWrapper(null, result);
    }

    private LksEduPlanVersion findLksEduPlanVersion(String code)
    {
        if (code != null)
        {
            Long entityId = null;
            try
            {
                entityId = Long.parseLong(code);
            } catch (NumberFormatException ex)
            {
                // Не судьба
            }
            List<LksEduPlanVersion> list = findEntity(eq(property(ENTITY_ALIAS, LksEduPlanVersion.entityId()), value(entityId)));
            if (list.size() == 1) return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<LksEduPlanVersion> processDatagramObject(LksEduPlanVersionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksEduPlanVersionID init
        String code = StringUtils.trimToNull(datagramObject.getLksEduPlanVersionID());
        // LksEduPlanVersionName init
        String title = StringUtils.trimToNull(datagramObject.getLksEduPlanVersionName());
        // LksEduPlanVersionContent init
        byte[] text = datagramObject.getLksEduPlanVersionContent();

        boolean isNew = false;
        boolean isChanged = false;
        LksEduPlanVersion entity = null;
        CoreCollectionUtils.Pair<LksEduPlanVersion, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksEduPlanVersion(code);
        if (entity == null)
        {
            entity = new LksEduPlanVersion();
        }

        // LksEduPlanVersionName set
        if (null != title && (null == entity.getTitle() || !title.equals(entity.getTitle())))
        {
            isChanged = true;
            entity.setTitle(title);
        }
        // LksEduPlanVersionContent set
        if (null != text && text.length > 0)
        {
            isChanged = true;
            entity.setText(text);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }
}