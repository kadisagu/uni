/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2016
 */
public class DatagramObjectFactoryHolder
{
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
}
