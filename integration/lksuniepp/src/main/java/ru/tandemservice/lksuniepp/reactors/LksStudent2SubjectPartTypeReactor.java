/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart;
import ru.tandemservice.lksuniepp.entity.LksSubjectPart;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2016
 */
public class LksStudent2SubjectPartTypeReactor extends BaseEntityReactor<LksStudent2SubjectPart, LksStudent2SubjectPartType>
{
    private static final String STUDENT = "StudentID";
    private static final String EDUCATION_YEAR = "EducationYearID";
    private static final String LKS_SUBJECT_PART = "LksSubjectPartID";


    @Override
    public Class<LksStudent2SubjectPart> getEntityClass()
    {
        return LksStudent2SubjectPart.class;
    }

    @Override
    public void collectUnknownDatagrams(LksStudent2SubjectPartType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksStudent2SubjectPartType.EducationYearID educationYearID = datagramObject.getEducationYearID();
        EducationYearType educationYearType = educationYearID == null ? null : educationYearID.getEducationYear();
        if (educationYearType != null) collector.check(educationYearType.getID(), EducationYearType.class);

        LksStudent2SubjectPartType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        if (studentType != null) collector.check(studentType.getID(), StudentType.class);

        LksStudent2SubjectPartType.LksSubjectPartID lksSubjectPartID = datagramObject.getLksSubjectPartID();
        LksSubjectPartType lksSubjectPartType = lksSubjectPartID == null ? null : lksSubjectPartID.getLksSubjectPart();
        if (lksSubjectPartType != null) collector.check(lksSubjectPartType.getID(), LksSubjectPartType.class);
    }

    @Override
    public LksStudent2SubjectPartType createDatagramObject(String guid)
    {
        LksStudent2SubjectPartType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2SubjectPartType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksStudent2SubjectPart entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksStudent2SubjectPartType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2SubjectPartType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksStudent2SubjectPartSemesterNumber(entity.getSemesterNumber());

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<EducationYearType> educationYear = createDatagramObject(EducationYearType.class, entity.getEducationYear(), commonCache, warning);
        LksStudent2SubjectPartType.EducationYearID educationYearID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2SubjectPartTypeEducationYearID();
        educationYearID.setEducationYear(educationYear.getObject());
        datagramObject.setEducationYearID(educationYearID);
        if (educationYear.getList() != null) result.addAll(educationYear.getList());

        ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, entity.getStudent(), commonCache, warning);
        LksStudent2SubjectPartType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2SubjectPartTypeStudentID();
        studentID.setStudent(studentType.getObject());
        datagramObject.setStudentID(studentID);
        if (studentType.getList() != null) result.addAll(studentType.getList());

        ProcessedDatagramObject<LksSubjectPartType> lksSubjectType = createDatagramObject(LksSubjectPartType.class, entity.getSubjectPart(), commonCache, warning);
        LksStudent2SubjectPartType.LksSubjectPartID lksSubjectPartID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2SubjectPartTypeLksSubjectPartID();
        lksSubjectPartID.setLksSubjectPart(lksSubjectType.getObject());
        datagramObject.setLksSubjectPartID(lksSubjectPartID);
        if (lksSubjectType.getList() != null) result.addAll(lksSubjectType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksStudent2SubjectPart findLksStudent2SubjectPart(Student student, EducationYear eduYear, LksSubjectPart subjectPart)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (student != null)
            expressions.add(eq(property(ENTITY_ALIAS, LksStudent2SubjectPart.student()), value(student)));
        if (eduYear != null)
            expressions.add(eq(property(ENTITY_ALIAS, LksStudent2SubjectPart.educationYear()), value(eduYear)));
        if (subjectPart != null)
            expressions.add(eq(property(ENTITY_ALIAS, LksStudent2SubjectPart.subjectPart()), value(subjectPart)));

        if (expressions.isEmpty()) return null;

        List<LksStudent2SubjectPart> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<LksStudent2SubjectPart> processDatagramObject(LksStudent2SubjectPartType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksStudent2SubjectPartSemesterNumber init
        String number = StringUtils.trimToNull(datagramObject.getLksStudent2SubjectPartSemesterNumber());

        // EducationYearID init
        LksStudent2SubjectPartType.EducationYearID educationYearID = datagramObject.getEducationYearID();
        EducationYearType educationYearType = educationYearID == null ? null : educationYearID.getEducationYear();
        EducationYear educationYear = getOrCreate(EducationYearType.class, params, commonCache, educationYearType, EDUCATION_YEAR, null, warning);

        // StudentID init
        LksStudent2SubjectPartType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        Student student = getOrCreate(StudentType.class, params, commonCache, studentType, STUDENT, null, warning);

        // LksSubjectPartID init
        LksStudent2SubjectPartType.LksSubjectPartID lksSubjectPartID = datagramObject.getLksSubjectPartID();
        LksSubjectPartType lksSubjectPartType = lksSubjectPartID == null ? null : lksSubjectPartID.getLksSubjectPart();
        LksSubjectPart lksSubjectPart = getOrCreate(LksSubjectPartType.class, params, commonCache, lksSubjectPartType, LKS_SUBJECT_PART, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        LksStudent2SubjectPart entity = null;
        CoreCollectionUtils.Pair<LksStudent2SubjectPart, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksStudent2SubjectPart(student, educationYear, lksSubjectPart);
        if (entity == null)
        {
            entity = new LksStudent2SubjectPart();
        }

        // LksStudent2SubjectPartSemesterNumber set
        if (null != number && (null == entity.getSemesterNumber() || !number.equals(entity.getSemesterNumber())))
        {
            isChanged = true;
            entity.setSemesterNumber(number);
        }

        // EducationYearID set
        if (educationYear == null)
            throw new ProcessingDatagramObjectException("LksStudent2SubjectPartType object without EducationYearID!");
        else if (null == entity.getEducationYear() || !educationYear.equals(entity.getEducationYear()))
        {
            isChanged = true;
            entity.setEducationYear(educationYear);
        }

        // StudentID set
        if (student == null)
            throw new ProcessingDatagramObjectException("LksStudent2SubjectPartType object without StudentID!");
        else if (null == entity.getStudent() || !student.equals(entity.getStudent()))
        {
            isChanged = true;
            entity.setStudent(student);
        }

        // LksSubjectPartID set
        if (lksSubjectPart == null)
            throw new ProcessingDatagramObjectException("LksStudent2SubjectPartType object without LksSubjectPartID!");
        else if (null == entity.getSubjectPart() || !lksSubjectPart.equals(entity.getSubjectPart()))
        {
            isChanged = true;
            entity.setSubjectPart(lksSubjectPart);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksStudent2SubjectPart> w, LksStudent2SubjectPartType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EducationYearType.class, session, params, EDUCATION_YEAR, nsiPackage);
            saveChildEntity(StudentType.class, session, params, STUDENT, nsiPackage);
            saveChildEntity(LksSubjectPartType.class, session, params, LKS_SUBJECT_PART, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}