/* $Id:$ */
package ru.tandemservice.lksuniepp.base.bo.LksEppSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.lksuniepp.base.bo.LksEppSystem.logic.ILksEppDao;
import ru.tandemservice.lksuniepp.base.bo.LksEppSystem.logic.LksEppDao;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
@Configuration
public class LksEppSystemManager extends BusinessObjectManager
{
    public static LksEppSystemManager instance()
    {
        return instance(LksEppSystemManager.class);
    }

    @Bean
    public ILksEppDao dao()
    {
        return new LksEppDao();
    }
}