package ru.tandemservice.lksuniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_lksuniepp_2x10x8_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность lksSubjectPart

		// создано свойство examAccum
		{
			// создать колонку
			tool.createColumn("lkssubjectpart_t", new DBColumn("examaccum_p", DBType.createVarchar(255)));

		}

		// создано свойство courseWork
		{
			// создать колонку
			tool.createColumn("lkssubjectpart_t", new DBColumn("coursework_p", DBType.createVarchar(255)));

		}

		// создано свойство courseProject
		{
			// создать колонку
			tool.createColumn("lkssubjectpart_t", new DBColumn("courseproject_p", DBType.createVarchar(255)));

		}

		// создано свойство controlWork
		{
			// создать колонку
			tool.createColumn("lkssubjectpart_t", new DBColumn("controlwork_p", DBType.createVarchar(255)));

		}


    }
}