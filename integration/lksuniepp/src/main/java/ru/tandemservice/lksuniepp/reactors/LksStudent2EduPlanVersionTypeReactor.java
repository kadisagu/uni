/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.lksuniepp.entity.LksEduPlanVersion;
import ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksEduPlanVersionType;
import ru.tandemservice.nsiclient.datagram.LksStudent2EduPlanVersionType;
import ru.tandemservice.nsiclient.datagram.StudentType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2016
 */
public class LksStudent2EduPlanVersionTypeReactor extends BaseEntityReactor<LksStudent2EduPlanVersion, LksStudent2EduPlanVersionType>
{
    private static final String STUDENT = "StudentID";
    private static final String LKS_EDU_PLAN_VERSION = "LksEduPlanVersionID";

    @Override
    public Class<LksStudent2EduPlanVersion> getEntityClass()
    {
        return LksStudent2EduPlanVersion.class;
    }

    @Override
    public void collectUnknownDatagrams(LksStudent2EduPlanVersionType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksStudent2EduPlanVersionType.LksEduPlanVersionID versionID = datagramObject.getLksEduPlanVersionID();
        LksEduPlanVersionType versionType = versionID == null ? null : versionID.getLksEduPlanVersion();
        if (versionType != null) collector.check(versionType.getID(), LksEduPlanVersionType.class);

        LksStudent2EduPlanVersionType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        if (studentType != null) collector.check(studentType.getID(), StudentType.class);
    }

    @Override
    public LksStudent2EduPlanVersionType createDatagramObject(String guid)
    {
        LksStudent2EduPlanVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2EduPlanVersionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksStudent2EduPlanVersion entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksStudent2EduPlanVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2EduPlanVersionType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<LksEduPlanVersionType> eduPlanVersionType = createDatagramObject(LksEduPlanVersionType.class, entity.getPlanVersion(), commonCache, warning);
        LksStudent2EduPlanVersionType.LksEduPlanVersionID versionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2EduPlanVersionTypeLksEduPlanVersionID();
        versionID.setLksEduPlanVersion(eduPlanVersionType.getObject());
        datagramObject.setLksEduPlanVersionID(versionID);
        if (eduPlanVersionType.getList() != null) result.addAll(eduPlanVersionType.getList());

        ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, entity.getStudent(), commonCache, warning);
        LksStudent2EduPlanVersionType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksStudent2EduPlanVersionTypeStudentID();
        studentID.setStudent(studentType.getObject());
        datagramObject.setStudentID(studentID);
        if (studentType.getList() != null) result.addAll(studentType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksStudent2EduPlanVersion findLksStudent2EduPlanVersion(Student student, LksEduPlanVersion planVersion)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (student != null)
            expressions.add(eq(property(ENTITY_ALIAS, LksStudent2EduPlanVersion.student()), value(student)));
        if (planVersion != null)
            expressions.add(eq(property(ENTITY_ALIAS, LksStudent2EduPlanVersion.planVersion()), value(planVersion)));

        if (expressions.isEmpty()) return null;

        List<LksStudent2EduPlanVersion> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<LksStudent2EduPlanVersion> processDatagramObject(LksStudent2EduPlanVersionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // StudentID init
        LksStudent2EduPlanVersionType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        Student student = getOrCreate(StudentType.class, params, commonCache, studentType, STUDENT, null, warning);

        // LksEduPlanVersionID init
        LksStudent2EduPlanVersionType.LksEduPlanVersionID versionID = datagramObject.getLksEduPlanVersionID();
        LksEduPlanVersionType versionType = versionID == null ? null : versionID.getLksEduPlanVersion();
        LksEduPlanVersion version = getOrCreate(LksEduPlanVersionType.class, params, commonCache, versionType, LKS_EDU_PLAN_VERSION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        LksStudent2EduPlanVersion entity = null;
        CoreCollectionUtils.Pair<LksStudent2EduPlanVersion, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksStudent2EduPlanVersion(student, version);
        if (entity == null)
        {
            entity = new LksStudent2EduPlanVersion();
        }

        // StudentID set
        if (student == null)
            throw new ProcessingDatagramObjectException("LksStudent2EduPlanVersionType object without StudentID!");
        else if (null == entity.getStudent() || !student.equals(entity.getStudent()))
        {
            isChanged = true;
            entity.setStudent(student);
        }

        // LksEduPlanVersionID set
        if (version == null)
            throw new ProcessingDatagramObjectException("LksStudent2EduPlanVersionType object without LksEduPlanVersionID!");
        else if (null == entity.getPlanVersion() || !version.equals(entity.getPlanVersion()))
        {
            isChanged = true;
            entity.setPlanVersion(version);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksStudent2EduPlanVersion> w, LksStudent2EduPlanVersionType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(StudentType.class, session, params, STUDENT, nsiPackage);
            saveChildEntity(LksEduPlanVersionType.class, session, params, LKS_EDU_PLAN_VERSION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}