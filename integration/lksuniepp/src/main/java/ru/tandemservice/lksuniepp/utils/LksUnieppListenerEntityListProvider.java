/* $Id:$ */
package ru.tandemservice.lksuniepp.utils;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.lksbase.utils.ILksListenerEntityListProvider;
import ru.tandemservice.lksuniepp.base.bo.LksEppSystem.LksEppSystemManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksUnieppListenerEntityListProvider implements ILksListenerEntityListProvider
{
    public static final String SUBJECT_MOBILE = "subject";
    public static final String STUDENT_TO_SEMESTER_MOBILE = "student2Semester";
    public static final String EDUPLAN_WEEK_PERIOD = "eduPlanWeekPeriod";
    //public static final String EDU_PLAN_VERSION = "eduPlanVersion";
    //public static final String STUDENT_TO_EDU_PLAN = "student2EduPlan";

    public static final List<Class> CLASSES_TO_LISTEN = new ArrayList<>();
    public static final Set<String> ORIGINAL_ENTITY_NAME_SET = new HashSet<>();
    public static final Map<String, String> ENTITY_NAME_TO_MOBILE_ENTITY_NAME = new HashMap<>();

    static
    {
        CLASSES_TO_LISTEN.add(EppRegistryElement.class);
        CLASSES_TO_LISTEN.add(EppRegistryAttestation.class);
        CLASSES_TO_LISTEN.add(EppRegistryPractice.class);
        CLASSES_TO_LISTEN.add(EppRegistryDiscipline.class);

        CLASSES_TO_LISTEN.add(EppRegistryElementPart.class);
        CLASSES_TO_LISTEN.add(EppRegistryElementPartModule.class);
        CLASSES_TO_LISTEN.add(EppRegistryElementPartFControlAction.class);
        CLASSES_TO_LISTEN.add(EppRegistryElementLoad.class);
        CLASSES_TO_LISTEN.add(EppRegistryModuleALoad.class);
        CLASSES_TO_LISTEN.add(EppStudentWorkPlanElement.class);
        CLASSES_TO_LISTEN.add(EppStudentWpeALoad.class);
        CLASSES_TO_LISTEN.add(EppStudentWpeCAction.class);
        //CLASSES_TO_LISTEN.add(EppStudent2EduPlanVersion.class);
        CLASSES_TO_LISTEN.add(EppEduPlanVersionWeekType.class);

        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryElement.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppStudentWorkPlanElement.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryAction.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryAttestation.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryDiscipline.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryPractice.ENTITY_NAME);
        //ORIGINAL_ENTITY_NAME_SET.add(EppStudent2EduPlanVersion.ENTITY_NAME);

        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElement.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryAction.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryAttestation.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryDiscipline.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryPractice.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementPart.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementPartModule.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementPartFControlAction.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementLoad.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryModuleALoad.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudentWorkPlanElement.ENTITY_NAME, STUDENT_TO_SEMESTER_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudentWpeALoad.ENTITY_NAME, STUDENT_TO_SEMESTER_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudentWpeCAction.ENTITY_NAME, STUDENT_TO_SEMESTER_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppEduPlanVersionWeekType.ENTITY_NAME, EDUPLAN_WEEK_PERIOD);

        //TODO всё, что относится к учебным планам

        //ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudent2EduPlanVersion.ENTITY_NAME, STUDENT_TO_EDU_PLAN);
    }

    @Override
    public int getPriority()
    {
        return 0;
    }

    @Override
    public List<Class> getClassesToListen()
    {
        return CLASSES_TO_LISTEN;
    }

    @Override
    public List<CoreCollectionUtils.Pair<Long, String>> getIdToEntityTypePairList(Long entityId, boolean delete)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entityId);
        if (null == meta) return new ArrayList<>();

        if (!CLASSES_TO_LISTEN.contains(meta.getEntityClass())) return new ArrayList<>();

        List<CoreCollectionUtils.Pair<Long, String>> resultIdList = new ArrayList<>();

        if (ORIGINAL_ENTITY_NAME_SET.contains(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(entityId, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName())));
        } else
        {
            List<Long> ids = LksEppSystemManager.instance().dao().getEntityToUpdateIdList(entityId);
            ids.stream().map(e-> new CoreCollectionUtils.Pair<>(e, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName()))).forEach(resultIdList::add);
        }
        return resultIdList;
    }

    @Override
    public void initFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        LksEppSystemManager.instance().dao().doInintFullLksEntitySync(studentIds, entityType);
    }

    @Override
    public int syncLksEntityPortion(int elementsToProcessAmount)
    {
        return LksEppSystemManager.instance().dao().doSyncEntityPortion(elementsToProcessAmount);
    }
}