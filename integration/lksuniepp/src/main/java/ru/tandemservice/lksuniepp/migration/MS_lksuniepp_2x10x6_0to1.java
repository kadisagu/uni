package ru.tandemservice.lksuniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_lksuniepp_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность lksStudent2SubjectPart

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("lksstudent2subjectpart_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_lksstudent2subjectpart"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entityid_p", DBType.LONG).setNullable(false), 
				new DBColumn("semesternumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("student_id", DBType.LONG), 
				new DBColumn("subjectpart_id", DBType.LONG), 
				new DBColumn("educationyear_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("lksStudent2SubjectPart");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность lksSubject

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("lkssubject_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_lkssubject"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entityid_p", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)), 
				new DBColumn("fulltitle_p", DBType.createVarchar(255)), 
				new DBColumn("reqelement_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("lksSubject");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность lksSubjectPart

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("lkssubjectpart_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_lkssubjectpart"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entityid_p", DBType.LONG).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("lectures_p", DBType.createVarchar(255)), 
				new DBColumn("practices_p", DBType.createVarchar(255)), 
				new DBColumn("labs_p", DBType.createVarchar(255)), 
				new DBColumn("seminars_p", DBType.createVarchar(255)), 
				new DBColumn("credit_p", DBType.createVarchar(255)), 
				new DBColumn("diffcredit_p", DBType.createVarchar(255)), 
				new DBColumn("exam_p", DBType.createVarchar(255)), 
				new DBColumn("subject_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("lksSubjectPart");

		}


    }
}