/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

import org.hibernate.Session;
import ru.tandemservice.lksuniepp.entity.LksEduPlanWeekPeriod;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EduPlanVersionType;
import ru.tandemservice.nsiclient.datagram.EduPlanWeekPeriodType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 20.09.2016
 */
public class EduPlanWeekPeriodTypeReactor extends BaseEntityReactor<LksEduPlanWeekPeriod, EduPlanWeekPeriodType>
{
    private static final String EDU_PLAN_VERSION = "EduPlanVersionID";

    @Override
    public Class<LksEduPlanWeekPeriod> getEntityClass()
    {
        return LksEduPlanWeekPeriod.class;
    }

    @Override
    public void collectUnknownDatagrams(EduPlanWeekPeriodType datagramObject, IUnknownDatagramsCollector collector)
    {
        EduPlanWeekPeriodType.EduPlanVersionID versionID = datagramObject.getEduPlanVersionID();
        EduPlanVersionType versionType = versionID == null ? null : versionID.getEduPlanVersion();
        if (versionType != null) collector.check(versionType.getID(), EduPlanVersionType.class);
    }

    @Override
    public EduPlanWeekPeriodType createDatagramObject(String guid)
    {
        EduPlanWeekPeriodType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanWeekPeriodType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksEduPlanWeekPeriod entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanWeekPeriodType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanWeekPeriodType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanWeekPeriodName(entity.getTitle());
        datagramObject.setEduPlanWeekPeriodCourseName(entity.getCourse().getTitle());
        datagramObject.setEduPlanWeekPeriodTermName(entity.getTerm().getTitle());
        datagramObject.setEduPlanWeekPeriodMonthNumberStart(String.valueOf(entity.getMonthNumberStart()));
        datagramObject.setEduPlanWeekPeriodDayNumberStart(String.valueOf(entity.getDayNumberStart()));
        datagramObject.setEduPlanWeekPeriodMonthNumberEnd(String.valueOf(entity.getMonthNumberEnd()));
        datagramObject.setEduPlanWeekPeriodDayNumberEnd(String.valueOf(entity.getDayNumberEnd()));
        datagramObject.setEduPlanWeekPeriodWeekTypeName(entity.getWeekType().getTitle());
        datagramObject.setEduPlanWeekPeriodWeekTypeShortName(entity.getWeekType().getShortTitle());

        ProcessedDatagramObject<EduPlanVersionType> eduPlanVersionType = createDatagramObjectShort(EduPlanVersionType.class, entity.getEduPlanVersion(), commonCache);
        EduPlanWeekPeriodType.EduPlanVersionID versionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanWeekPeriodTypeEduPlanVersionID();
        versionID.setEduPlanVersion(eduPlanVersionType.getObject());
        datagramObject.setEduPlanVersionID(versionID);
        if (eduPlanVersionType.getList() != null) result.addAll(eduPlanVersionType.getList());

        return new ResultListWrapper( null, result);
    }

    @Override
    public ChangedWrapper<LksEduPlanWeekPeriod> processDatagramObject(EduPlanWeekPeriodType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type LksEduPlanWeekPeriod is unsupported");
    }

    @Override
    public void save(Session session, ChangedWrapper<LksEduPlanWeekPeriod> w, EduPlanWeekPeriodType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EduPlanVersionType.class, session, params, EDU_PLAN_VERSION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}