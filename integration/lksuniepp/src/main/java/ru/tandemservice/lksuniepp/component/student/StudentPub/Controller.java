/* $Id:$ */
package ru.tandemservice.lksuniepp.component.student.StudentPub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;

import java.util.Collections;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2016
 */
public class Controller extends ru.tandemservice.uni.component.student.StudentPub.Controller
{
    public void onClickSyncWithPortal(IBusinessComponent component)
    {
        LksSystemManager.instance().dao().doInitFullLksEntitySync(Collections.singletonList(getModel(component).getStudent().getId()), null);
    }
}