/* $Id:$ */
package ru.tandemservice.lksuniepp.base.bo.LksEppSystem.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLInsertValuesBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.LksDao;
import ru.tandemservice.lksbase.entity.LksChangedEntity;
import ru.tandemservice.lksuniepp.entity.*;
import ru.tandemservice.lksuniepp.utils.LksUnieppListenerEntityListProvider;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksEppDao extends LksDao implements ILksEppDao
{
    public List<Long> getEntityToUpdateIdList(Long entityId)
    {
        IEntity entity = get(entityId);
        List<Long> resultList = new ArrayList<>();
        if (null == entity) return resultList;

        if (entity instanceof EppRegistryElementPart)
        {
            EppRegistryElementPart elementPart = (EppRegistryElementPart) entity;
            resultList.add(elementPart.getRegistryElement().getId());
        } else if (entity instanceof EppRegistryElementPartModule)
        {
            EppRegistryElementPartModule eppRegistryElementPartModule = (EppRegistryElementPartModule) entity;
            resultList.add(eppRegistryElementPartModule.getHierarhyParent().getRegistryElement().getId());
        } else if (entity instanceof EppRegistryElementPartFControlAction)
        {
            EppRegistryElementPartFControlAction action = (EppRegistryElementPartFControlAction) entity;
            resultList.add(action.getPart().getRegistryElement().getId());
        } else if (entity instanceof EppRegistryElementLoad)
        {
            EppRegistryElementLoad load = (EppRegistryElementLoad) entity;
            resultList.add(load.getRegistryElement().getId());
        } else if (entity instanceof EppRegistryModuleALoad)
        {
            EppRegistryModuleALoad load = (EppRegistryModuleALoad) entity;
            List<Long> regElIds = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "e").distinct()
                    .column(property(EppRegistryElementPartModule.part().registryElement().id().fromAlias("e")))
                    .where(eq(property(EppRegistryElementPartModule.module().fromAlias("e")), value(load.getModule())))
                    .createStatement(getSession()).list();

            resultList.addAll(regElIds);

        } else if (entity instanceof EppStudentWpeALoad)
        {
            EppStudentWpeALoad loadSlot = (EppStudentWpeALoad) entity;
            resultList.add(loadSlot.getStudentWpe().getId());
        } else if (entity instanceof EppStudentWpeCAction)
        {
            EppStudentWpeCAction actionSlot = (EppStudentWpeCAction) entity;
            resultList.add(actionSlot.getStudentWpe().getId());
        }
// TODO всё, что относится к учебному плану
        /*else if (entity instanceof EppStudent2EduPlanVersion)
        {
            EppStudent2EduPlanVersion stud2EduPlan = (EppStudent2EduPlanVersion) entity;
            resultList.add(stud2EduPlan.getId());
        }*/
        else if (entity instanceof EppEduPlanVersionWeekType)
        {
            EppEduPlanVersionWeekType weekType = (EppEduPlanVersionWeekType) entity;
            resultList.add(weekType.getId());
        }

        return resultList;
    }

    @Override
    public void doInintFullLksEntitySync(Collection<Long> studentIdCollection, String entityType)
    {
        if(null != entityType
                && !LksUnieppListenerEntityListProvider.SUBJECT_MOBILE.equals(entityType)
                && !LksUnieppListenerEntityListProvider.STUDENT_TO_SEMESTER_MOBILE.equals(entityType)
                && !LksUnieppListenerEntityListProvider.EDUPLAN_WEEK_PERIOD.equals(entityType))
            return;

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        List<Long> fullEntityIdList = new ArrayList<>();

        List<Long> subjectIdsList = new ArrayList<>();
        if(null == entityType || LksUnieppListenerEntityListProvider.SUBJECT_MOBILE.equals(entityType))
        {
            // TODO учитывать уже добавленные, сначала поднять список из БД, чтобы не плодить дублей
            DQLSelectBuilder subjIdsBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e").distinct()
                    .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(EppStudentWorkPlanElement.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                    .column(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("e")))
                /*.where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("e"))))*/.where(isNull(property("s")))
                    .order(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("e")));

            if (null != studentIdCollection && !studentIdCollection.isEmpty())
            {
                subjIdsBuilder.where(in(property(EppStudentWorkPlanElement.student().id().fromAlias("e")), studentIdCollection));
            }

            subjectIdsList = subjIdsBuilder.createStatement(getSession()).list();
            fullEntityIdList.addAll(subjectIdsList);

            System.out.println("Подготовка данных о дисицплинах реестра заняла " + (System.currentTimeMillis() - time) + " мс.");
            time = System.currentTimeMillis();
        }
        /*TODO List<Long> journalExploitedSubjectIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                .column(property(TrJournalGroupStudent.group().journal().registryElementPart().registryElement().id().fromAlias("e")))
                .order(property(TrJournalGroupStudent.group().journal().registryElementPart().registryElement().id().fromAlias("e")))
                .createStatement(getSession()).list();

        journalExploitedSubjectIds.removeAll(subjectIdsList);
        subjectIdsList.addAll(journalExploitedSubjectIds);*/

        List<Long> student2SemesterIdsList = new ArrayList<>();
        if(null == entityType || LksUnieppListenerEntityListProvider.STUDENT_TO_SEMESTER_MOBILE.equals(entityType))
        {
            DQLSelectBuilder student2SemesterIdsBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e").distinct()
                    .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(EppStudentWorkPlanElement.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                    .column(property(EppStudentWorkPlanElement.id().fromAlias("e"))).where(isNull(property("s")))
                /*.where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("e"))))*/
                    .order(property(EppStudentWorkPlanElement.id().fromAlias("e")));

            if (null != studentIdCollection && !studentIdCollection.isEmpty())
            {
                student2SemesterIdsBuilder.where(in(property(EppStudentWorkPlanElement.student().id().fromAlias("e")), studentIdCollection));
            }

            student2SemesterIdsList = student2SemesterIdsBuilder.createStatement(getSession()).list();
            fullEntityIdList.addAll(student2SemesterIdsList);

            System.out.println("Подготовка данных о связях студентов с дисциплиночастью заняла " + (System.currentTimeMillis() - time) + " мс.");
            time = System.currentTimeMillis();
        }

        /*DQLSelectBuilder eduPlanIdsBuilder = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "e").distinct()
                .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(EppEduPlanVersion.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                .column(property(EppEduPlanVersion.id().fromAlias("e"))).where(isNull(property("s")))
                .order(property(EppEduPlanVersion.id().fromAlias("e")));

        if(null != studentIdCollection && !studentIdCollection.isEmpty())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s")
                    .column(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s")))
                    .where(in(property(EppStudent2EduPlanVersion.student().id().fromAlias("s")), studentIdCollection));
            eduPlanIdsBuilder.where(in(property(EppEduPlanVersion.id().fromAlias("e")), subBuilder.buildQuery()));
        }

        List<Long> eduPlanIdsList = eduPlanIdsBuilder.createStatement(getSession()).list();

        System.out.println("Подготовка данных об учебных планах заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        DQLSelectBuilder stud2EduPlanIdsBuilder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "e").distinct()
                .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(EppStudent2EduPlanVersion.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                .column(property(EppStudent2EduPlanVersion.id().fromAlias("e"))).where(isNull(property("s")))
                .order(property(EppStudent2EduPlanVersion.id().fromAlias("e")));

        if(null != studentIdCollection && !studentIdCollection.isEmpty())
        {
            stud2EduPlanIdsBuilder.where(in(property(EppStudent2EduPlanVersion.student().id().fromAlias("e")), studentIdCollection));
        }

        List<Long> stud2EduPlanIdsList = stud2EduPlanIdsBuilder.createStatement(getSession()).list();

        System.out.println("Подготовка данных о связях студентов с учебными планами заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();*/

        List<Long> eppEduPlanWeekTypeIdsList = new ArrayList<>();
        if(null == entityType || LksUnieppListenerEntityListProvider.EDUPLAN_WEEK_PERIOD.equals(entityType))
        {
            DQLSelectBuilder eppEduPlanWeekTypeIdsBuilder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e").distinct()
                    .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(EppEduPlanVersionWeekType.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                    .column(property(EppEduPlanVersionWeekType.id().fromAlias("e"))).where(isNull(property("s")))
                    .order(property(EppEduPlanVersionWeekType.id().fromAlias("e")));

            if (null != studentIdCollection && !studentIdCollection.isEmpty())
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s")
                        .column(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s")))
                        .where(in(property(EppStudent2EduPlanVersion.student().id().fromAlias("s")), studentIdCollection));
                eppEduPlanWeekTypeIdsBuilder.where(in(property(EppEduPlanVersionWeekType.eduPlanVersion().id().fromAlias("e")), subBuilder.buildQuery()));
            }

            eppEduPlanWeekTypeIdsList = eppEduPlanWeekTypeIdsBuilder.createStatement(getSession()).list();
            fullEntityIdList.addAll(eppEduPlanWeekTypeIdsList);

            System.out.println("Подготовка данных о неделях строки курса в учебном графике заняла " + (System.currentTimeMillis() - time) + " мс.");
            time = System.currentTimeMillis();
        }

        /*fullEntityIdList.addAll(eduPlanIdsList);
        fullEntityIdList.addAll(stud2EduPlanIdsList);*/

        int updCnt = 0;
        Set<Long> processedIdSet = new HashSet<>();
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);

        for (Long id : fullEntityIdList)
        {
            if (!processedIdSet.contains(id))
            {
                String entityTypeToProcess = subjectIdsList.contains(id) ? LksUnieppListenerEntityListProvider.SUBJECT_MOBILE : null;
                if (null == entityTypeToProcess && student2SemesterIdsList.contains(id))
                    entityTypeToProcess = LksUnieppListenerEntityListProvider.STUDENT_TO_SEMESTER_MOBILE;
/*                if (null == entityTypeToProcess && eduPlanIdsList.contains(id))
                    entityTypeToProcess = LksUnieppListenerEntityListProvider.EDU_PLAN_VERSION;
                if (null == entityTypeToProcess && stud2EduPlanIdsList.contains(id))
                    entityTypeToProcess = LksUnieppListenerEntityListProvider.STUDENT_TO_EDU_PLAN;*/
                if (null == entityTypeToProcess && eppEduPlanWeekTypeIdsList.contains(id))
                    entityTypeToProcess = LksUnieppListenerEntityListProvider.EDUPLAN_WEEK_PERIOD;

                insertBuilder.value(LksChangedEntity.entityId().s(), id);
                insertBuilder.value(LksChangedEntity.deleted().s(), Boolean.FALSE);
                insertBuilder.value(LksChangedEntity.entityType().s(), entityTypeToProcess);
                insertBuilder.addBatch();
                processedIdSet.add(id);
                updCnt++;
            }
            if (updCnt == 300)
            {
                createStatement(insertBuilder).execute();
                insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);
                updCnt = 0;
            }
        }

        if (updCnt > 0) createStatement(insertBuilder).execute();
        System.out.println("Запись строк в БД заняла " + (System.currentTimeMillis() - time) + " мс.");
        System.out.println("Всего " + (System.currentTimeMillis() - startTime) + " мс.");
    }

    @Override
    public int doSyncEntityPortion(int elementsToProcessAmount)
    {
        List<LksChangedEntity> entityPortion = getLksEntityPortion(elementsToProcessAmount, LksUnieppListenerEntityListProvider.SUBJECT_MOBILE);
        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> subjectIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            entityPortion.stream().forEach(e-> { subjectIds.add(e.getEntityId()); changedEntityIdsList.add(e.getId());});

            //Подготоавливаем мап сохраненных ранее объектов
            List<LksSubject> savedSubjList = new DQLSelectBuilder().fromEntity(LksSubject.class, "e").column(property("e"))
                    .where(in(property(LksSubject.entityId().fromAlias("e")), subjectIds))
                    .createStatement(getSession()).list();

            Map<Long, LksSubject> subjMap = new HashMap<>();
            for (LksSubject subject : savedSubjList) subjMap.put(subject.getEntityId(), subject);

            List<LksSubjectPart> savedSubjPartList = new DQLSelectBuilder().fromEntity(LksSubjectPart.class, "e").column(property("e"))
                    .where(in(property(LksSubjectPart.subject().entityId().fromAlias("e")), subjectIds))
                    .createStatement(getSession()).list();

            Map<Long, LksSubjectPart> subjPartMap = new HashMap<>();
            for (LksSubjectPart part : savedSubjPartList) subjPartMap.put(part.getEntityId(), part);


            List<LksSubject> subjectList = new ArrayList<>();
            List<LksSubjectPart> semesterList = new ArrayList<>();
            Map<Long, IEppRegElWrapper> regElMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(subjectIds);
            EppELoadType selfWorkELoadType = getCatalogItem(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

            for (Map.Entry<Long, IEppRegElWrapper> regElEntry : regElMap.entrySet())
            {
                IEppRegElWrapper regElementWrapper = regElMap.get(regElEntry.getKey());

                if (null != regElementWrapper)
                {
                    List<EppALoadType> loadTypesList = getList(EppALoadType.class);
                    List<EppFControlActionType> controlActionTypes = getList(EppFControlActionType.class);

                    LksSubject subject = subjMap.get(regElementWrapper.getId());
                    if (null == subject)
                    {
                        subject = new LksSubject();
                        subject.setEntityId(regElementWrapper.getId());
                    }

                    subject.setTitle(regElementWrapper.getItem().getTitle());
                    subject.setFullTitle(null != regElementWrapper.getItem().getFullTitle() ? regElementWrapper.getItem().getFullTitle() : regElementWrapper.getItem().getTitle());
                    subject.setShortTitle(null != regElementWrapper.getItem().getShortTitle() ? regElementWrapper.getItem().getShortTitle() : regElementWrapper.getItem().getTitle());

                    for (Map.Entry<Integer, IEppRegElPartWrapper> partEntry : regElementWrapper.getPartMap().entrySet())
                    {
                        IEppRegElPartWrapper loadWrapper = partEntry.getValue();
                        if (null != loadWrapper)
                        {
                            LksSubjectPart semester = subjPartMap.get(loadWrapper.getId());
                            if (null == semester)
                            {
                                semester = new LksSubjectPart();
                                semester.setEntityId(loadWrapper.getId());
                                semester.setSubject(subject);
                            }

                            semester.setNumber(String.valueOf(loadWrapper.getNumber()));

                            //TODO milestones - шкала для отображения рейтинга.

                            // В поле seminars (одному Богу изветно почему так названное) помещаем данные о часах на самостоятельную работу
                            semester.setSeminars(String.valueOf(Double.valueOf(loadWrapper.getLoadAsDouble(selfWorkELoadType.getFullCode())).intValue()));

                            // Предварительно зануляем все поля, на случай, если объект взят из базы,
                            // а какой-то из видов нагрузки был убран из дисциплины
                            semester.setLabs(null);
                            semester.setLectures(null);
                            semester.setPractices(null);

                            for (EppALoadType loadType : loadTypesList)
                            {
                                String load = String.valueOf(Double.valueOf(loadWrapper.getLoadAsDouble(loadType.getFullCode())).intValue());
                                if (EppALoadTypeCodes.TYPE_LABS.equals(loadType.getCode())) semester.setLabs(load);
                                else if (EppALoadTypeCodes.TYPE_LECTURES.equals(loadType.getCode()))
                                    semester.setLectures(load);
                                else if (EppALoadTypeCodes.TYPE_PRACTICE.equals(loadType.getCode()))
                                    semester.setPractices(load);
                            }

                            // Предварительно зануляем все поля, на случай, если объект взят из базы,
                            // а какая-то из форм итогового контроля была убрана из дисциплины
                            semester.setCredit(null);
                            semester.setDiffCredit(null);
                            semester.setExam(null);
                            semester.setExamAccum(null);
                            semester.setCourseProject(null);
                            semester.setCourseWork(null);
                            semester.setControlWork(null);

                            // Проставляем реальные значения, в зависимости от наличия соответствующих форм итогового контроля в дисциплине
                            for (EppFControlActionType ctrlActionType : controlActionTypes)
                            {
                                if (loadWrapper.hasActions4ControlActionType(ctrlActionType))
                                {
                                    if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF.equals(ctrlActionType.getCode()))
                                        semester.setCredit(String.valueOf(Boolean.TRUE));
                                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF.equals(ctrlActionType.getCode()))
                                        semester.setDiffCredit(String.valueOf(Boolean.TRUE));
                                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM.equals(ctrlActionType.getCode()))
                                        semester.setExam(String.valueOf(Boolean.TRUE));
                                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM.equals(ctrlActionType.getCode()))
                                        semester.setExamAccum(String.valueOf(Boolean.TRUE));
                                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT.equals(ctrlActionType.getCode()))
                                        semester.setCourseProject(String.valueOf(Boolean.TRUE));
                                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK.equals(ctrlActionType.getCode()))
                                        semester.setCourseWork(String.valueOf(Boolean.TRUE));
                                    else if (EppFControlActionTypeCodes.CONTROL_ACTION_CONTROL_WORK.equals(ctrlActionType.getCode()))
                                        semester.setControlWork(String.valueOf(Boolean.TRUE));
                                }
                            }
                            semesterList.add(semester);
                        }
                    }
                    subjectList.add(subject);
                }
            }

            new MergeAction.SessionMergeAction<Long, LksSubject>()
            {
                @Override protected Long key(final LksSubject source){ return source.getEntityId();}
                @Override protected void fill(final LksSubject target, final LksSubject source) { target.update(source, false);}
                @Override protected LksSubject buildRow(final LksSubject source) { return source;}
            }.merge(savedSubjList, subjectList);

            savedSubjPartList = new DQLSelectBuilder().fromEntity(LksSubjectPart.class, "e").column(property("e"))
                    .where(in(property(LksSubjectPart.subject().entityId().fromAlias("e")), subjectIds))
                    .createStatement(getSession()).list();

            new MergeAction.SessionMergeAction<Long, LksSubjectPart>()
            {
                @Override protected Long key(final LksSubjectPart source){ return source.getEntityId();}
                @Override protected void fill(final LksSubjectPart target, final LksSubjectPart source) { target.update(source, false);}
                @Override protected LksSubjectPart buildRow(final LksSubjectPart source) { return source;}
            }.merge(savedSubjPartList, semesterList);

//            for (LksSubject subject : subjectList) saveOrUpdate(subject);
//            for (LksSubjectPart semester : semesterList) saveOrUpdate(semester);

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return subjectList.size() + semesterList.size();
        }

        entityPortion = getLksEntityPortion(elementsToProcessAmount, LksUnieppListenerEntityListProvider.STUDENT_TO_SEMESTER_MOBILE);
        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> stud2SubjPartIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            entityPortion.stream().forEach(e-> { stud2SubjPartIds.add(e.getEntityId()); changedEntityIdsList.add(e.getId());});


            //Подготоавливаем мап сохраненных ранее объектов
            List<LksStudent2SubjectPart> savedStud2SubjPartList = new DQLSelectBuilder().fromEntity(LksStudent2SubjectPart.class, "e").column(property("e"))
                    .where(in(property(LksStudent2SubjectPart.entityId().fromAlias("e")), stud2SubjPartIds))
                    .createStatement(getSession()).list();

            Map<Long, LksStudent2SubjectPart> studToSubjPartMap = new HashMap<>();
            for (LksStudent2SubjectPart stud2SubjPart : savedStud2SubjPartList) studToSubjPartMap.put(stud2SubjPart.getEntityId(), stud2SubjPart);

            List<Object[]> items = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e")
                    .column(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                    .column(property(EppStudentWorkPlanElement.student().fromAlias("e")))
                    .column(property("s"))
                    //.column(property(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("e")))
                    .column(property(EppStudentWorkPlanElement.year().educationYear().fromAlias("e")))
                    //.column(property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("e"))) // Удобнее сравнивать по номеру семестра в РП
                    .column(property(EppStudentWorkPlanElement.term().intValue().fromAlias("e")))
                    .joinEntity("e", DQLJoinType.inner, LksSubjectPart.class, "s", eq(property(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("e")), property(LksSubjectPart.entityId().fromAlias("s"))))
                    //.where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("e"))))
                    .where(in(property(EppStudentWorkPlanElement.id().fromAlias("e")), stud2SubjPartIds))
                    .order(property(EppStudentWorkPlanElement.student().id().fromAlias("e")))
                    .order(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("e")))
                    .order(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                    .createStatement(getSession()).list();

            List<LksStudent2SubjectPart> student2SemesterList = new ArrayList<>();

            for (Object[] item : items)
            {
                LksStudent2SubjectPart stud2SubjPart = studToSubjPartMap.get(item[0]);
                if(null == stud2SubjPart)
                {
                    stud2SubjPart = new LksStudent2SubjectPart();
                    stud2SubjPart.setEntityId((Long)item[0]);
                }

                stud2SubjPart.setStudent((Student)item[1]);
                stud2SubjPart.setSubjectPart((LksSubjectPart) item[2]);
                stud2SubjPart.setEducationYear((EducationYear) item[3]);
                stud2SubjPart.setSemesterNumber(String.valueOf(item[4]));
                student2SemesterList.add(stud2SubjPart);
            }

            new MergeAction.SessionMergeAction<Long, LksStudent2SubjectPart>()
            {
                @Override protected Long key(final LksStudent2SubjectPart source){ return source.getEntityId();}
                @Override protected void fill(final LksStudent2SubjectPart target, final LksStudent2SubjectPart source) { target.update(source, false);}
                @Override protected LksStudent2SubjectPart buildRow(final LksStudent2SubjectPart source) { return source;}
            }.merge(savedStud2SubjPartList, student2SemesterList);

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return student2SemesterList.size();
        }

//        entityPortion = getLksEntityPortion(10/*TODO elementsToProcessAmount*/, LksUnieppListenerEntityListProvider.EDU_PLAN_VERSION);
/*        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> eduPlanVersionIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            for (LksChangedEntity entity : entityPortion)
            {
                eduPlanVersionIds.add(entity.getEntityId());
                changedEntityIdsList.add(entity.getId());
            }

            //Подготоавливаем мап сохраненных ранее объектов
            List<LksEduPlanVersion> savedEduPlanVersionList = new DQLSelectBuilder().fromEntity(LksEduPlanVersion.class, "e").column(property("e"))
                    .where(in(property(LksEduPlanVersion.entityId().fromAlias("e")), eduPlanVersionIds))
                    .createStatement(getSession()).list();

            Map<Long, LksEduPlanVersion> eduPlanVersionsMap = new HashMap<>();
            for (LksEduPlanVersion eduPlanVersion : savedEduPlanVersionList) eduPlanVersionsMap.put(eduPlanVersion.getEntityId(), eduPlanVersion);

            List<EppEduPlanVersion> items = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "e").column(property("e"))
                    .where(in(property(EppEduPlanVersion.id().fromAlias("e")), eduPlanVersionIds))
                    .order(property(EppEduPlanVersion.id().fromAlias("e")))
                    .createStatement(getSession()).list();

            List<LksEduPlanVersion> eduPlanVersionList = new ArrayList<>();

            for (EppEduPlanVersion planVersion : items)
            {
                LksEduPlanVersion lksEduPlan = eduPlanVersionsMap.get(planVersion.getId());
                if(null == lksEduPlan)
                {
                    lksEduPlan = new LksEduPlanVersion();
                    lksEduPlan.setEntityId(planVersion.getId());
                }

                IScriptItem scriptItem = getByCode(EppScriptItem.class, EppScriptItemCodes.UNIEPP_EDU_PLAN_VERSION);
                Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                        scriptItem,
                        IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                        "epvIds", Collections.singletonList(planVersion.getId())
                );

                lksEduPlan.setTitle(planVersion.getTitle());
                lksEduPlan.setPlanVersion(planVersion);
                lksEduPlan.setText((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
                eduPlanVersionList.add(lksEduPlan);
            }

            for (LksEduPlanVersion lksEduPlanVersion : eduPlanVersionList) saveOrUpdate(lksEduPlanVersion);

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return eduPlanVersionList.size();
        }

        entityPortion = getLksEntityPortion(elementsToProcessAmount, LksUnieppListenerEntityListProvider.STUDENT_TO_EDU_PLAN);
        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> stud2EduPlanIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            for (LksChangedEntity entity : entityPortion)
            {
                stud2EduPlanIds.add(entity.getEntityId());
                changedEntityIdsList.add(entity.getId());
            }

            //Подготоавливаем мап сохраненных ранее объектов
            List<LksStudent2EduPlanVersion> savedStud2EduPlanList = new DQLSelectBuilder().fromEntity(LksStudent2EduPlanVersion.class, "e").column(property("e"))
                    .where(in(property(LksStudent2EduPlanVersion.entityId().fromAlias("e")), stud2EduPlanIds))
                    .createStatement(getSession()).list();

            Map<Long, LksStudent2EduPlanVersion> stud2EduPlansMap = new HashMap<>();
            for (LksStudent2EduPlanVersion stud2EduPlan : savedStud2EduPlanList) stud2EduPlansMap.put(stud2EduPlan.getEntityId(), stud2EduPlan);

            List<Object[]> items = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "e")
                    .column(property(EppStudent2EduPlanVersion.id().fromAlias("e"))).column(property("s"))
                    .column(property(EppStudent2EduPlanVersion.student().fromAlias("e")))
                    .joinEntity("e", DQLJoinType.inner, LksEduPlanVersion.class, "s", eq(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("e")), property(LksEduPlanVersion.entityId().fromAlias("s"))))
                    .where(in(property(EppStudent2EduPlanVersion.id().fromAlias("e")), stud2EduPlanIds))
                    .order(property(EppStudent2EduPlanVersion.id().fromAlias("e")))
                    .createStatement(getSession()).list();

            List<LksStudent2EduPlanVersion> stud2EduPlanList = new ArrayList<>();

            for (Object[] item : items)
            {
                Long stud2EduPlanId = (Long) item[0];
                LksEduPlanVersion lskEduPlan = (LksEduPlanVersion) item[1];
                LksStudent2EduPlanVersion lksStud2EduPlan = stud2EduPlansMap.get(stud2EduPlanId);
                if(null == lksStud2EduPlan)
                {
                    lksStud2EduPlan = new LksStudent2EduPlanVersion();
                    lksStud2EduPlan.setEntityId(stud2EduPlanId);
                }

                lksStud2EduPlan.setPlanVersion(lskEduPlan);
                lksStud2EduPlan.setStudent((Student) item[2]);
                stud2EduPlanList.add(lksStud2EduPlan);
            }

            for (LksStudent2EduPlanVersion lksStud2EduPlan : stud2EduPlanList) saveOrUpdate(lksStud2EduPlan);

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return stud2EduPlanList.size();
        }*/

        entityPortion = getLksEntityPortion(elementsToProcessAmount, LksUnieppListenerEntityListProvider.EDUPLAN_WEEK_PERIOD);
        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> eduPlanWeekTypeIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            entityPortion.stream().forEach(e-> { eduPlanWeekTypeIds.add(e.getEntityId()); changedEntityIdsList.add(e.getId());});

            //Подготоавливаем мап сохраненных ранее объектов
            List<LksEduPlanWeekPeriod> savedEduPlanPeriodList = new DQLSelectBuilder().fromEntity(LksEduPlanWeekPeriod.class, "e").column(property("e"))
                    //.where(in(property(LksStudent2EduPlanVersion.entityId().fromAlias("e")), eduPlanWeekTypeIds))
                    .createStatement(getSession()).list();

            Map<String, LksEduPlanWeekPeriod> eduPlanWeekPeriodsMap = new HashMap<>();
            for (LksEduPlanWeekPeriod eduPlanWeekPeriod : savedEduPlanPeriodList)
            {
                String key = String.valueOf(eduPlanWeekPeriod.getEduPlanVersion().getId()) + "|" + eduPlanWeekPeriod.getCourse().getTitle() + "|" + eduPlanWeekPeriod.getTerm().getTitle() + "|" + eduPlanWeekPeriod.getWeekType().getCode();
                eduPlanWeekPeriodsMap.put(key, eduPlanWeekPeriod);
            }

            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e").distinct()
                    .column(property(EppEduPlanVersionWeekType.eduPlanVersion().id().fromAlias("e")))
                    .where(in(property(EppEduPlanVersionWeekType.id().fromAlias("e")), eduPlanWeekTypeIds));

            List<EppEduPlanVersionWeekType> items = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e").column("e")
                    .where(in(property(EppEduPlanVersionWeekType.eduPlanVersion().id().fromAlias("e")), subBuilder.buildQuery()))
                    .createStatement(getSession()).list();

            Map<String, List<EppEduPlanVersionWeekType>> weekTypesMap = new HashMap<>();
            for (EppEduPlanVersionWeekType item : items)
            {
                String key = String.valueOf(item.getEduPlanVersion().getId()) + "|" + item.getCourse().getTitle() + "|" + item.getTerm().getTitle() + "|" + item.getWeekType().getCode();
                List<EppEduPlanVersionWeekType> weekTypesList = weekTypesMap.get(key);
                if (null == weekTypesList) weekTypesList = new ArrayList<>();
                weekTypesList.add(item);
                weekTypesMap.put(key, weekTypesList);
            }
            ///TODO


            List<LksEduPlanWeekPeriod> weekPeriodsList = new ArrayList<>();
            Set<String> processedWeekTypeKeys = new HashSet<>();

            for (EppEduPlanVersionWeekType item : items)
            {
                String key = String.valueOf(item.getEduPlanVersion().getId()) + "|" + item.getCourse().getTitle() + "|" + item.getTerm().getTitle() + "|" + item.getWeekType().getCode();
                if (!processedWeekTypeKeys.contains(key))
                {
                    LksEduPlanWeekPeriod savedWeekPeriod = eduPlanWeekPeriodsMap.get(key);
                    if (null == savedWeekPeriod)
                    {
                        savedWeekPeriod = new LksEduPlanWeekPeriod();
                        savedWeekPeriod.setEduPlanVersion(item.getEduPlanVersion());
                        savedWeekPeriod.setCourse(item.getCourse());
                        savedWeekPeriod.setTerm(item.getTerm());
                        savedWeekPeriod.setWeekType(item.getWeekType());
                    }

                    List<EppEduPlanVersionWeekType> fullTypedWeekPeriodsList = weekTypesMap.get(key);
                    if (null != fullTypedWeekPeriodsList && !fullTypedWeekPeriodsList.isEmpty())
                    {
                        EppWeek minWeek = null;
                        EppWeek maxWeek = null;
                        for (EppEduPlanVersionWeekType weekType : fullTypedWeekPeriodsList)
                        {
                            if (null == minWeek || weekType.getWeek().getNumber() < minWeek.getNumber())
                                minWeek = weekType.getWeek();
                            if (null == maxWeek || weekType.getWeek().getNumber() > maxWeek.getNumber())
                                maxWeek = weekType.getWeek();
                        }

                        String startMonth = "";
                        if (null != minWeek)
                        {
                            String[] periodParts = minWeek.getTitle().split("-");
                            if (periodParts.length > 1 && periodParts[0].trim().length() > 3)
                            {
                                String[] startPeriodParts = periodParts[0].trim().split(" ");
                                if (startPeriodParts.length > 1 && startPeriodParts[1].trim().length() > 2)
                                    startMonth = startPeriodParts[1].trim();
                            } else if (periodParts.length > 1 && periodParts[1].trim().length() > 3)
                            {
                                String[] endPeriodParts = periodParts[1].trim().split(" ");
                                if (endPeriodParts.length > 1 && endPeriodParts[1].trim().length() > 2)
                                    startMonth = endPeriodParts[1].trim();
                            }
                            savedWeekPeriod.setMonthNumberStart(minWeek.getMonth());
                            savedWeekPeriod.setDayNumberStart(Integer.parseInt(minWeek.getTitle().substring(0, 2).trim()));
                        }

                        String endMonth = "";
                        if (null != maxWeek)
                        {
                            String[] periodParts = maxWeek.getTitle().split("-");
                            if (periodParts.length > 1 && periodParts[1].trim().length() > 3)
                            {
                                String[] endPeriodParts = periodParts[1].trim().split(" ");
                                if (endPeriodParts.length > 1 && endPeriodParts[1].trim().length() > 2)
                                    endMonth = endPeriodParts[1].trim();
                            } else if (periodParts.length > 1 && periodParts[0].trim().length() > 3)
                            {
                                String[] startPeriodParts = periodParts[0].trim().split(" ");
                                if (startPeriodParts.length > 1 && startPeriodParts[1].trim().length() > 2)
                                    endMonth = startPeriodParts[1].trim();
                            }
                            if(null != startMonth && null != endMonth && !startMonth.equals(endMonth))
                                savedWeekPeriod.setMonthNumberEnd(maxWeek.getMonth() + 1);
                            else  savedWeekPeriod.setMonthNumberEnd(maxWeek.getMonth());

                            savedWeekPeriod.setDayNumberEnd(Integer.parseInt(periodParts[1].trim().substring(0, 2).trim()));
                        }

                        if (null != startMonth || null != endMonth)
                        {
                            String title = String.valueOf(savedWeekPeriod.getDayNumberStart());
                            if (null != startMonth && (null == endMonth || !startMonth.equals(endMonth)))
                                title += " " + startMonth;
                            title += " - " + savedWeekPeriod.getDayNumberEnd();
                            if (null != endMonth && (null == startMonth || !endMonth.equals(startMonth)))
                                title += " " + endMonth;
                            else if (null != startMonth) title += " " + startMonth;
                            savedWeekPeriod.setTitle(title);
                        }

                    }
                    if(savedWeekPeriod.getMonthNumberEnd() > 12) savedWeekPeriod.setMonthNumberEnd(1);
                    if(null == savedWeekPeriod.getTitle()) savedWeekPeriod.setTitle("-");

                    processedWeekTypeKeys.add(key);
                    weekPeriodsList.add(savedWeekPeriod);
                }
            }

            for (LksEduPlanWeekPeriod weekPeriod : weekPeriodsList) saveOrUpdate(weekPeriod);

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return weekPeriodsList.size();
        }
        
        return 0;
    }
}