package ru.tandemservice.lksuniepp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart;
import ru.tandemservice.lksuniepp.entity.LksSubjectPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь студента с частью дисциплины реестра для личного кабинета студента (Student2Semester)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksStudent2SubjectPartGen extends EntityBase
 implements INaturalIdentifiable<LksStudent2SubjectPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart";
    public static final String ENTITY_NAME = "lksStudent2SubjectPart";
    public static final int VERSION_HASH = 704588309;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_SEMESTER_NUMBER = "semesterNumber";
    public static final String L_STUDENT = "student";
    public static final String L_SUBJECT_PART = "subjectPart";
    public static final String L_EDUCATION_YEAR = "educationYear";

    private long _entityId;     // Идентификатор связанного объекта
    private String _semesterNumber;     // Номер семестра
    private Student _student;     // Студент
    private LksSubjectPart _subjectPart;     // Часть дисциплины реестра для личного кабинета студента
    private EducationYear _educationYear;     // Учебный год

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Номер семестра. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSemesterNumber()
    {
        return _semesterNumber;
    }

    /**
     * @param semesterNumber Номер семестра. Свойство не может быть null.
     */
    public void setSemesterNumber(String semesterNumber)
    {
        dirty(_semesterNumber, semesterNumber);
        _semesterNumber = semesterNumber;
    }

    /**
     * @return Студент.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Часть дисциплины реестра для личного кабинета студента.
     */
    public LksSubjectPart getSubjectPart()
    {
        return _subjectPart;
    }

    /**
     * @param subjectPart Часть дисциплины реестра для личного кабинета студента.
     */
    public void setSubjectPart(LksSubjectPart subjectPart)
    {
        dirty(_subjectPart, subjectPart);
        _subjectPart = subjectPart;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksStudent2SubjectPartGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksStudent2SubjectPart)another).getEntityId());
            }
            setSemesterNumber(((LksStudent2SubjectPart)another).getSemesterNumber());
            setStudent(((LksStudent2SubjectPart)another).getStudent());
            setSubjectPart(((LksStudent2SubjectPart)another).getSubjectPart());
            setEducationYear(((LksStudent2SubjectPart)another).getEducationYear());
        }
    }

    public INaturalId<LksStudent2SubjectPartGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksStudent2SubjectPartGen>
    {
        private static final String PROXY_NAME = "LksStudent2SubjectPartNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksStudent2SubjectPartGen.NaturalId) ) return false;

            LksStudent2SubjectPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksStudent2SubjectPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksStudent2SubjectPart.class;
        }

        public T newInstance()
        {
            return (T) new LksStudent2SubjectPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "semesterNumber":
                    return obj.getSemesterNumber();
                case "student":
                    return obj.getStudent();
                case "subjectPart":
                    return obj.getSubjectPart();
                case "educationYear":
                    return obj.getEducationYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "semesterNumber":
                    obj.setSemesterNumber((String) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "subjectPart":
                    obj.setSubjectPart((LksSubjectPart) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "semesterNumber":
                        return true;
                case "student":
                        return true;
                case "subjectPart":
                        return true;
                case "educationYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "semesterNumber":
                    return true;
                case "student":
                    return true;
                case "subjectPart":
                    return true;
                case "educationYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "semesterNumber":
                    return String.class;
                case "student":
                    return Student.class;
                case "subjectPart":
                    return LksSubjectPart.class;
                case "educationYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksStudent2SubjectPart> _dslPath = new Path<LksStudent2SubjectPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksStudent2SubjectPart");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Номер семестра. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getSemesterNumber()
     */
    public static PropertyPath<String> semesterNumber()
    {
        return _dslPath.semesterNumber();
    }

    /**
     * @return Студент.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Часть дисциплины реестра для личного кабинета студента.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getSubjectPart()
     */
    public static LksSubjectPart.Path<LksSubjectPart> subjectPart()
    {
        return _dslPath.subjectPart();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    public static class Path<E extends LksStudent2SubjectPart> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _semesterNumber;
        private Student.Path<Student> _student;
        private LksSubjectPart.Path<LksSubjectPart> _subjectPart;
        private EducationYear.Path<EducationYear> _educationYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksStudent2SubjectPartGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Номер семестра. Свойство не может быть null.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getSemesterNumber()
     */
        public PropertyPath<String> semesterNumber()
        {
            if(_semesterNumber == null )
                _semesterNumber = new PropertyPath<String>(LksStudent2SubjectPartGen.P_SEMESTER_NUMBER, this);
            return _semesterNumber;
        }

    /**
     * @return Студент.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Часть дисциплины реестра для личного кабинета студента.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getSubjectPart()
     */
        public LksSubjectPart.Path<LksSubjectPart> subjectPart()
        {
            if(_subjectPart == null )
                _subjectPart = new LksSubjectPart.Path<LksSubjectPart>(L_SUBJECT_PART, this);
            return _subjectPart;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2SubjectPart#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

        public Class getEntityClass()
        {
            return LksStudent2SubjectPart.class;
        }

        public String getEntityName()
        {
            return "lksStudent2SubjectPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
