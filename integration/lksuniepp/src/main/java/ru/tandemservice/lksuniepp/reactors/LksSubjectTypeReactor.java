/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksSubjectType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2016
 */
public class LksSubjectTypeReactor extends BaseEntityReactor<LksSubject, LksSubjectType>
{
    @Override
    public Class<LksSubject> getEntityClass()
    {
        return LksSubject.class;
    }

    @Override
    public LksSubjectType createDatagramObject(String guid)
    {
        LksSubjectType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSubjectType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksSubject entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksSubjectType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSubjectType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksSubjectID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksSubjectName(entity.getTitle());
        datagramObject.setLksSubjectFullName(entity.getFullTitle());
        datagramObject.setLksSubjectShortName(entity.getShortTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected LksSubject findLksSubject(String code, String title)
    {
        if (code != null)
        {
            List<LksSubject> list = findEntity(eq(property(ENTITY_ALIAS, String.valueOf(LksSubject.entityId())), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (title != null)
        {
            List<LksSubject> list = findEntity(eq(property(ENTITY_ALIAS, LksSubject.title()), value(title)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<LksSubject> processDatagramObject(LksSubjectType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getLksSubjectID());
        String title = StringUtils.trimToNull(datagramObject.getLksSubjectName());
        String fullTitle = StringUtils.trimToNull(datagramObject.getLksSubjectName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getLksSubjectName());

        boolean isNew = false;
        boolean isChanged = false;
        LksSubject entity = null;

        CoreCollectionUtils.Pair<LksSubject, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksSubject(code, title);
        if (entity == null)
        {
            entity = new LksSubject();
            isNew = true;
            isChanged = true;
        }

//        //code
//        if (code != null && !code.equals(String.valueOf(entity.getEntityId())))
//        {
//            isChanged = true;
//            try
//            {
//                entity.setEntityId(Long.parseLong(code));
//            } catch (NumberFormatException ex)
//            {
//                // не судьба
//            }
//        }
        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        //fullTitle
        if (fullTitle != null && (null == entity.getFullTitle() || !fullTitle.equals(entity.getFullTitle())))
        {
            isChanged = true;
            entity.setFullTitle(fullTitle);
        }

        //shortTitle
        if (shortTitle != null && (null == entity.getShortTitle() || !shortTitle.equals(entity.getShortTitle())))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, null);
    }
}