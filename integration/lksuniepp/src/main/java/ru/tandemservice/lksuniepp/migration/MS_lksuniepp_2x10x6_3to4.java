package ru.tandemservice.lksuniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_lksuniepp_2x10x6_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность lksEduPlanWeekPeriod

		// создано обязательное свойство title
		if (tool.tableExists("lkseduplanweekperiod_t") && !tool.columnExists("lkseduplanweekperiod_t", "title_p"))
		{
			// создать колонку
			tool.createColumn("lkseduplanweekperiod_t", new DBColumn("title_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
			java.lang.String defaultTitle = "-";
			tool.executeUpdate("update lkseduplanweekperiod_t set title_p=? where title_p is null", defaultTitle);

			// сделать колонку NOT NULL
			tool.setColumnNullable("lkseduplanweekperiod_t", "title_p", false);

		}


    }
}