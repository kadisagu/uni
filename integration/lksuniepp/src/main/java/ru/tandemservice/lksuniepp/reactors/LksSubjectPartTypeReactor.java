/* $Id:$ */
package ru.tandemservice.lksuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksuniepp.entity.LksSubjectPart;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksSubjectPartType;
import ru.tandemservice.nsiclient.datagram.LksSubjectType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2016
 */
public class LksSubjectPartTypeReactor extends BaseEntityReactor<LksSubjectPart, LksSubjectPartType>
{
    private static final String LKS_SUBJECT = "LksSubjectID";

    @Override
    public Class<LksSubjectPart> getEntityClass()
    {
        return LksSubjectPart.class;
    }

    @Override
    public void collectUnknownDatagrams(LksSubjectPartType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksSubjectPartType.LksSubjectID lksSubjectID = datagramObject.getLksSubjectID();
        LksSubjectType lksSubjectType = lksSubjectID == null ? null : lksSubjectID.getLksSubject();
        if (lksSubjectType != null) collector.check(lksSubjectType.getID(), LksSubjectType.class);
    }

    @Override
    public LksSubjectPartType createDatagramObject(String guid)
    {
        LksSubjectPartType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSubjectPartType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksSubjectPart entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksSubjectPartType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSubjectPartType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksSubjectPartID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksSubjectPartNumber(entity.getNumber());
        datagramObject.setLksSubjectPartLectures(entity.getLectures());
        datagramObject.setLksSubjectPartPractices(entity.getPractices());
        datagramObject.setLksSubjectPartLabs(entity.getLabs());
        datagramObject.setLksSubjectPartSeminars(entity.getSeminars());
        datagramObject.setLksSubjectPartCredit(entity.getCredit());
        datagramObject.setLksSubjectPartDiffCredit(entity.getDiffCredit());
        datagramObject.setLksSubjectPartExam(entity.getExam());
        datagramObject.setLksSubjectPartExamAccum(entity.getExamAccum());
        datagramObject.setLksSubjectPartCourseProject(entity.getCourseProject());
        datagramObject.setLksSubjectPartCourseWork(entity.getCourseWork());
        datagramObject.setLksSubjectPartControlWork(entity.getControlWork());

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<LksSubjectType> lksSubjectType = createDatagramObject(LksSubjectType.class, entity.getSubject(), commonCache, warning);
        LksSubjectPartType.LksSubjectID lksSubjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSubjectPartTypeLksSubjectID();
        lksSubjectID.setLksSubject(lksSubjectType.getObject());
        datagramObject.setLksSubjectID(lksSubjectID);
        if (lksSubjectType.getList() != null) result.addAll(lksSubjectType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksSubjectPart findLksSubjectPart(String code, LksSubject subject)
    {
        if (code != null)
        {
            Long entityId = null;
            try
            {
                entityId = Long.parseLong(code);
            } catch (NumberFormatException ex)
            {
                // Не судьба
            }
            List<LksSubjectPart> list = findEntity(eq(property(ENTITY_ALIAS, LksSubjectPart.entityId()), value(entityId)));
            if (list.size() == 1) return list.get(0);
        }

        List<IExpression> expressions = new ArrayList<>();
        if (subject != null) expressions.add(eq(property(ENTITY_ALIAS, LksSubjectPart.subject()), value(subject)));

        if (expressions.isEmpty()) return null;

        List<LksSubjectPart> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<LksSubjectPart> processDatagramObject(LksSubjectPartType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksSubjectPartID init
        String code = StringUtils.trimToNull(datagramObject.getLksSubjectPartID());
        // LksSubjectPartNumber init
        String number = StringUtils.trimToNull(datagramObject.getLksSubjectPartNumber());
        // LksSubjectPartLectures init
        String lectures = StringUtils.trimToNull(datagramObject.getLksSubjectPartLectures());
        // LksSubjectPartPractices init
        String practices = StringUtils.trimToNull(datagramObject.getLksSubjectPartPractices());
        // LksSubjectPartLabs init
        String labs = StringUtils.trimToNull(datagramObject.getLksSubjectPartLabs());
        // LksSubjectPartSeminars init
        String seminars = StringUtils.trimToNull(datagramObject.getLksSubjectPartSeminars());
        // LksSubjectPartCredit init
        String credit = StringUtils.trimToNull(datagramObject.getLksSubjectPartCredit());
        // LksSubjectPartDiffCredit init
        String diffCredit = StringUtils.trimToNull(datagramObject.getLksSubjectPartDiffCredit());
        // LksSubjectPartExam init
        String exam = StringUtils.trimToNull(datagramObject.getLksSubjectPartExam());
        // LksSubjectPartExamAccum init
        String examAccum = StringUtils.trimToNull(datagramObject.getLksSubjectPartExamAccum());
        // LksSubjectPartCourseProject init
        String courseProject = StringUtils.trimToNull(datagramObject.getLksSubjectPartCourseProject());
        // LksSubjectPartCourseWork init
        String courseWork = StringUtils.trimToNull(datagramObject.getLksSubjectPartCourseWork());
        // LksSubjectPartControlWork init
        String controlWork = StringUtils.trimToNull(datagramObject.getLksSubjectPartControlWork());

        // LksSubjectID init
        LksSubjectPartType.LksSubjectID subjectID = datagramObject.getLksSubjectID();
        LksSubjectType lksSubjectType = subjectID == null ? null : subjectID.getLksSubject();
        LksSubject lksSubject = getOrCreate(LksSubjectType.class, params, commonCache, lksSubjectType, LKS_SUBJECT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        LksSubjectPart entity = null;
        CoreCollectionUtils.Pair<LksSubjectPart, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksSubjectPart(code, lksSubject);
        if (entity == null)
        {
            entity = new LksSubjectPart();
        }

        // LksSubjectPartNumber set
        if (null != number && (null == entity.getNumber() || !number.equals(entity.getNumber())))
        {
            isChanged = true;
            entity.setNumber(number);
        }
        // LksSubjectPartLectures set
        if (null != lectures && (null == entity.getLectures() || !lectures.equals(entity.getLectures())))
        {
            isChanged = true;
            entity.setLectures(lectures);
        }
        // LksSubjectPartPractices set
        if (null != practices && (null == entity.getPractices() || !practices.equals(entity.getPractices())))
        {
            isChanged = true;
            entity.setPractices(practices);
        }
        // LksSubjectPartLabs set
        if (null != labs && (null == entity.getLabs() || !labs.equals(entity.getLabs())))
        {
            isChanged = true;
            entity.setLabs(labs);
        }
        // LksSubjectPartSeminars set
        if (null != seminars && (null == entity.getSeminars() || !seminars.equals(entity.getSeminars())))
        {
            isChanged = true;
            entity.setSeminars(seminars);
        }
        // LksSubjectPartCredit set
        if (null != credit && (null == entity.getCredit() || !credit.equals(entity.getCredit())))
        {
            isChanged = true;
            entity.setCredit(credit);
        }
        // LksSubjectPartDiffCredit set
        if (null != diffCredit && (null == entity.getDiffCredit() || !diffCredit.equals(entity.getDiffCredit())))
        {
            isChanged = true;
            entity.setDiffCredit(diffCredit);
        }
        // LksSubjectPartExam set
        if (null != exam && (null == entity.getExam() || !exam.equals(entity.getExam())))
        {
            isChanged = true;
            entity.setExam(exam);
        }
        // LksSubjectPartExamAccum set
        if (null != examAccum && (null == entity.getExamAccum() || !examAccum.equals(entity.getExamAccum())))
        {
            isChanged = true;
            entity.setExamAccum(examAccum);
        }
        // LksSubjectPartCourseProject set
        if (null != courseProject && (null == entity.getCourseProject() || !courseProject.equals(entity.getCourseProject())))
        {
            isChanged = true;
            entity.setCourseProject(courseProject);
        }
        // LksSubjectPartCourseWork set
        if (null != courseWork && (null == entity.getCourseWork() || !courseWork.equals(entity.getCourseWork())))
        {
            isChanged = true;
            entity.setCourseWork(courseWork);
        }
        // LksSubjectPartControlWork set
        if (null != controlWork && (null == entity.getControlWork() || !controlWork.equals(entity.getControlWork())))
        {
            isChanged = true;
            entity.setControlWork(controlWork);
        }

        // LksSubjectID set
        if (lksSubject == null)
            throw new ProcessingDatagramObjectException("LksSubjectPartType object without LksSubjectID!");
        else if (null == entity.getSubject() || !lksSubject.equals(entity.getSubject()))
        {
            isChanged = true;
            entity.setSubject(lksSubject);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksSubjectPart> w, LksSubjectPartType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(LksSubjectType.class, session, params, LKS_SUBJECT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}