package ru.tandemservice.lksuniepp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksEduPlanVersion;
import ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь студента с версией учебного плана для личного кабинета студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksStudent2EduPlanVersionGen extends EntityBase
 implements INaturalIdentifiable<LksStudent2EduPlanVersionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion";
    public static final String ENTITY_NAME = "lksStudent2EduPlanVersion";
    public static final int VERSION_HASH = -1222261446;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String L_STUDENT = "student";
    public static final String L_PLAN_VERSION = "planVersion";

    private long _entityId;     // Идентификатор связанного объекта
    private Student _student;     // Студент
    private LksEduPlanVersion _planVersion;     // Версия учебного плана

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Студент.
     */
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Версия учебного плана.
     */
    public LksEduPlanVersion getPlanVersion()
    {
        return _planVersion;
    }

    /**
     * @param planVersion Версия учебного плана.
     */
    public void setPlanVersion(LksEduPlanVersion planVersion)
    {
        dirty(_planVersion, planVersion);
        _planVersion = planVersion;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksStudent2EduPlanVersionGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksStudent2EduPlanVersion)another).getEntityId());
            }
            setStudent(((LksStudent2EduPlanVersion)another).getStudent());
            setPlanVersion(((LksStudent2EduPlanVersion)another).getPlanVersion());
        }
    }

    public INaturalId<LksStudent2EduPlanVersionGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksStudent2EduPlanVersionGen>
    {
        private static final String PROXY_NAME = "LksStudent2EduPlanVersionNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksStudent2EduPlanVersionGen.NaturalId) ) return false;

            LksStudent2EduPlanVersionGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksStudent2EduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksStudent2EduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new LksStudent2EduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "student":
                    return obj.getStudent();
                case "planVersion":
                    return obj.getPlanVersion();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "planVersion":
                    obj.setPlanVersion((LksEduPlanVersion) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "student":
                        return true;
                case "planVersion":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "student":
                    return true;
                case "planVersion":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "student":
                    return Student.class;
                case "planVersion":
                    return LksEduPlanVersion.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksStudent2EduPlanVersion> _dslPath = new Path<LksStudent2EduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksStudent2EduPlanVersion");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Студент.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Версия учебного плана.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion#getPlanVersion()
     */
    public static LksEduPlanVersion.Path<LksEduPlanVersion> planVersion()
    {
        return _dslPath.planVersion();
    }

    public static class Path<E extends LksStudent2EduPlanVersion> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private Student.Path<Student> _student;
        private LksEduPlanVersion.Path<LksEduPlanVersion> _planVersion;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksStudent2EduPlanVersionGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Студент.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Версия учебного плана.
     * @see ru.tandemservice.lksuniepp.entity.LksStudent2EduPlanVersion#getPlanVersion()
     */
        public LksEduPlanVersion.Path<LksEduPlanVersion> planVersion()
        {
            if(_planVersion == null )
                _planVersion = new LksEduPlanVersion.Path<LksEduPlanVersion>(L_PLAN_VERSION, this);
            return _planVersion;
        }

        public Class getEntityClass()
        {
            return LksStudent2EduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "lksStudent2EduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
