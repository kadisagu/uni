/* $Id$ */
package ru.tandemservice.nsictr.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentResult;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContactPersonType;
import ru.tandemservice.nsiclient.datagram.ContractObjectType;
import ru.tandemservice.nsiclient.datagram.ContractPaymentType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 27.01.2016
 */
public class ContractPaymentReactor extends BaseEntityReactor<CtrPaymentResult, ContractPaymentType>
{
    private static final String CONTRACT = "contractID";
    private static final String PAYEE = "payeeID";
    private static final String PAYER = "payerID";

    @Override
    public Class<CtrPaymentResult> getEntityClass()
    {
        return CtrPaymentResult.class;
    }

    @Override
    public void collectUnknownDatagrams(ContractPaymentType datagramObject, IUnknownDatagramsCollector collector)
    {

        ContractPaymentType.ContractObjectID contractObjectID = datagramObject.getContractObjectID();
        ContractObjectType contractObjectType = contractObjectID == null ? null : contractObjectID.getContractObject();
        if (contractObjectType != null)
            collector.check(contractObjectType.getID(), ContractObjectType.class);

        ContractPaymentType.PayeeID payeeID = datagramObject.getPayeeID();
        ContactPersonType payeeType = payeeID == null ? null : payeeID.getContactPerson();
        if (payeeType != null)
            collector.check(payeeType.getID(), ContactPersonType.class);

        ContractPaymentType.PayerID payerID = datagramObject.getPayerID();
        ContactPersonType payerType = payerID == null ? null : payerID.getContactPerson();
        if (payerType != null)
            collector.check(payerType.getID(), ContactPersonType.class);

    }

    @Override
    public ContractPaymentType createDatagramObject(String guid)
    {
        ContractPaymentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractPaymentType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }


    @Override
    public ResultListWrapper createDatagramObject(CtrPaymentResult entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ContractPaymentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractPaymentType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setContractPaymentCost(String.valueOf(entity.getCostAsLong()));
        datagramObject.setContractPaymentPaymentDocNumber(entity.getPaymentDocumentNumber());
        datagramObject.setContractPaymentPaymentDate(NsiUtils.formatDate(entity.getTimestamp()));

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);
        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<ContractObjectType> contractObjectType = createDatagramObject(ContractObjectType.class, entity.getContract(), commonCache, warning);
        ContractPaymentType.ContractObjectID contractObjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractPaymentTypeContractObjectID();
        contractObjectID.setContractObject(contractObjectType.getObject());
        datagramObject.setContractObjectID(contractObjectID);
        if (contractObjectType.getList() != null)
            result.addAll(contractObjectType.getList());

        ProcessedDatagramObject<ContactPersonType> payeeType = createDatagramObject(ContactPersonType.class, entity.getDst(), commonCache, warning);
        ContractPaymentType.PayeeID payeeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractPaymentTypePayeeID();
        payeeID.setContactPerson(payeeType.getObject());
        datagramObject.setPayeeID(payeeID);
        if (payeeType.getList() != null)
            result.addAll(payeeType.getList());

        ProcessedDatagramObject<ContactPersonType> payerType = createDatagramObject(ContactPersonType.class, entity.getSrc(), commonCache, warning);
        ContractPaymentType.PayerID payerID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractPaymentTypePayerID();
        payerID.setContactPerson(payerType.getObject());
        datagramObject.setPayerID(payerID);
        if (payerType.getList() != null)
            result.addAll(payerType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<CtrPaymentResult> processDatagramObject(@NotNull ContractPaymentType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        Long cost = NsiUtils.parseLong(datagramObject.getContractPaymentCost(), "ContractPaymentCost");
        Date date = NsiUtils.parseDate(datagramObject.getContractPaymentPaymentDate(), "ContractPaymentPaymentDate");
        String docNumber = datagramObject.getContractPaymentPaymentDocNumber();

        ContractPaymentType.ContractObjectID contractObjectID = datagramObject.getContractObjectID();
        ContractObjectType contractObjectType = contractObjectID == null ? null : contractObjectID.getContractObject();
        CtrContractObject contractObject = getOrCreate(ContractObjectType.class, params, commonCache, contractObjectType, CONTRACT, null, warning);

        ContractPaymentType.PayeeID payeeID = datagramObject.getPayeeID();
        ContactPersonType payeeType = payeeID == null ? null : payeeID.getContactPerson();
        ContactorPerson payee = getOrCreate(ContactPersonType.class, params, commonCache, payeeType, PAYEE, null, warning);

        ContractPaymentType.PayerID payerID = datagramObject.getPayerID();
        ContactPersonType payerType = payerID == null ? null : payerID.getContactPerson();
        ContactorPerson payer = getOrCreate(ContactPersonType.class, params, commonCache, payerType, PAYEE, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        CtrPaymentResult entity = null;
        CoreCollectionUtils.Pair<CtrPaymentResult, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
        {
            entity = new CtrPaymentResult();
            Currency currency = DataAccessServices.dao().get(Currency.class, Currency.P_CODE, CurrencyCodes.RUB);
            entity.setCurrency(currency);
            isNew = true;
            isChanged = true;
        }

        if(contractObject != null && !contractObject.equals(entity.getContract()))
        {
            isChanged = true;
            entity.setContract(contractObject);
        }
        if (null == entity.getContract())
            throw new ProcessingDatagramObjectException("ContractPaymentType object without ContractObjectID!");

        if(cost != null && !cost.equals(entity.getCostAsLong()))
        {
            isChanged = true;
            entity.setCostAsLong(cost);
        }

        if(docNumber != null && !docNumber.equals(entity.getPaymentDocumentNumber()))
        {
            isChanged = true;
            entity.setPaymentDocumentNumber(docNumber);
        }
        if (null == entity.getPaymentDocumentNumber())
            throw new ProcessingDatagramObjectException("ContractPaymentType object without document number!");

        if(date != null && !date.equals(entity.getTimestamp()))
        {
            isChanged = true;
            entity.setTimestamp(date);
        }
        if (null == entity.getTimestamp())
            throw new ProcessingDatagramObjectException("ContractPaymentType object without payment date!");

        if(payee != null && !payee.equals(entity.getDst()))
        {
            isChanged = true;
            entity.setDst(payee);
        }
        if (payer != null && !payer.equals(entity.getSrc()))
        {
            isChanged = true;
            entity.setSrc(payer);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<CtrPaymentResult> w, ContractPaymentType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(ContractObjectType.class, session, params, CONTRACT, nsiPackage);
            saveChildEntity(ContactPersonType.class, session, params, PAYEE, nsiPackage);
            saveChildEntity(ContactPersonType.class, session, params, PAYER, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(CtrPaymentResult.P_DST_COMMENT);
        fields.add(CtrPaymentResult.P_SRC_COMMENT);
        fields.add(CtrPaymentResult.L_PAYMENT_DOCUMENT_TYPE);
        fields.add(CtrPaymentResult.P_SOURCE_DOCUMENT_NUMBER);
        fields.add(CtrPaymentResult.P_COMMENT);
        fields.add(CtrPaymentResult.L_CURRENCY);
        return  fields;
    }
}
