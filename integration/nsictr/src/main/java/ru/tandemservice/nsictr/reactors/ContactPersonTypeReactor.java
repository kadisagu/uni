package ru.tandemservice.nsictr.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.ctr.base.entity.contactor.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ContactPersonTypeReactor extends BaseEntityReactor<ContactorPerson, ContactPersonType>
{
    private static final String EMPLOYEE = "EmployeeID";
    private static final String ORGANIZATION = "OrganizationID";
    private static final String HUMAN = "HumanID";
    private static final String PRINCIPAL_LOGIN = "principalLogin";

    @Override
    public Class<ContactorPerson> getEntityClass()
    {
        return ContactorPerson.class;
    }

    @Override
    public void collectUnknownDatagrams(ContactPersonType datagramObject, IUnknownDatagramsCollector collector) {
        ContactPersonType.EmployeeID employeeID = datagramObject.getEmployeeID();
        EmployeeType employeeType = employeeID == null ? null : employeeID.getEmployee();
        if(employeeType != null)
            collector.check(employeeType.getID(), EmployeeType.class);

        ContactPersonType.OrganizationID organizationID = datagramObject.getOrganizationID();
        OrganizationType organizationType = organizationID == null ? null : organizationID.getOrganization();
        if(organizationType != null)
            collector.check(organizationType.getID(), OrganizationType.class);

        ContactPersonType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if(humanType != null)
            collector.check(humanType.getID(), HumanType.class);
    }

    @Override
    public ContactPersonType createDatagramObject(String guid)
    {
        ContactPersonType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContactPersonType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(ContactorPerson entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ContactPersonType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContactPersonType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setContactPersonEmail(entity.getEmail());
        datagramObject.setContactPersonLogin(entity.getPrincipal().getLogin());
        datagramObject.setContactPersonPhone(entity.getPhone());

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);
        StringBuilder warning = new StringBuilder();

        if(entity instanceof EmployeePostContactor) {
            EmployeePost post = ((EmployeePostContactor) entity).getEmployeePost();
            ProcessedDatagramObject<EmployeeType> r = createDatagramObject(EmployeeType.class, post, commonCache, warning);
            ContactPersonType.EmployeeID employeeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContactPersonTypeEmployeeID();
            employeeID.setEmployee(r.getObject());
            datagramObject.setEmployeeID(employeeID);
            if (r.getList() != null)
                result.addAll(r.getList());
        }

        if(entity instanceof JuridicalContactor) {
            ProcessedDatagramObject<OrganizationType> r = createDatagramObject(OrganizationType.class, ((JuridicalContactor) entity).getExternalOrgUnit(), commonCache, warning);
            ContactPersonType.OrganizationID organizationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContactPersonTypeOrganizationID();
            organizationID.setOrganization(r.getObject());
            datagramObject.setOrganizationID(organizationID);
            if (r.getList() != null)
                result.addAll(r.getList());
            datagramObject.setContactPersonPost(((JuridicalContactor) entity).getPost());
        }

        ProcessedDatagramObject<HumanType> r = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        ContactPersonType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContactPersonTypeHumanID();
        humanID.setHuman(r.getObject());
        datagramObject.setHumanID(humanID);
        if (r.getList() != null)
            result.addAll(r.getList());
        return new ResultListWrapper(null, result);
    }

    protected ContactorPerson findEmployeePostContactor(EmployeePost post, Person person)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (post != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EmployeePostContactor.employeePost().id()), value(post.getId())));
        }
        if (person != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, EmployeePostContactor.person().id()), value(person.getId())));
        }
        List<EmployeePostContactor> list = findEntity(EmployeePostContactor.class, and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    protected ContactorPerson findJuridicalContactor(ExternalOrgUnit externalOrgUnit, Person person)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (externalOrgUnit != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, JuridicalContactor.externalOrgUnit().id()), value(externalOrgUnit.getId())));
        }
        if (person != null)
        {
            expressions.add(eq(property(ENTITY_ALIAS, JuridicalContactor.person().id()), value(person.getId())));
        }
        List<JuridicalContactor> list = findEntity(JuridicalContactor.class, and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    protected ContactorPerson findContactor(Person person)
    {
        if (person != null)
        {
            List<ContactorPerson> list = findEntity(eq(property(ENTITY_ALIAS, ContactorPerson.person().id()), value(person.getId())));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<ContactorPerson> processDatagramObject(ContactPersonType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String phone = StringUtils.trimToNull(datagramObject.getContactPersonPhone());
        String email = StringUtils.trimToNull(datagramObject.getContactPersonEmail());
        String postString = StringUtils.trimToNull(datagramObject.getContactPersonPost());
        String login = StringUtils.trimToNull(datagramObject.getContactPersonLogin());
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        // EmployeeID init
        ContactPersonType.EmployeeID employeeID = datagramObject.getEmployeeID();
        EmployeeType employeeType = employeeID == null ? null : employeeID.getEmployee();
        EmployeePost post = getOrCreate(EmployeeType.class, params, commonCache, employeeType, EMPLOYEE, null, warning);

        // OrganizationID init
        ContactPersonType.OrganizationID organizationID = datagramObject.getOrganizationID();
        OrganizationType organizationType = organizationID == null ? null : organizationID.getOrganization();
        ExternalOrgUnit externalOrgUnit = getOrCreate(OrganizationType.class, params, commonCache, organizationType, ORGANIZATION, null, warning);

        // HumanID init
        ContactPersonType.HumanID humanID = datagramObject.getHumanID();
        HumanType owner = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, owner, HUMAN, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        boolean isEmployeePostContactor = post != null;
        boolean isJuridicalContactor = externalOrgUnit != null;

        ContactorPerson entity = null;
        CoreCollectionUtils.Pair<ContactorPerson, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();


        if (entity == null) {
            if(isEmployeePostContactor) {
                entity = findEmployeePostContactor(post, person);
            } else if(isJuridicalContactor) {
                entity = findJuridicalContactor(externalOrgUnit, person);
            }
            if (entity == null)
                entity = findContactor(person);
        }
        if (entity == null)
        {
            if(isEmployeePostContactor)
                entity = new EmployeePostContactor();
            else if(isJuridicalContactor)
                entity = new JuridicalContactor();
            else
                entity = new PhysicalContactor();
            isNew = true;
            isChanged = true;
            entity.setActive(true);
        }

        // phone
        if (phone != null && !phone.equals(entity.getPhone()))
        {
            isChanged = true;
            entity.setPhone(phone);
        }

        // email
        if (email != null && !email.equals(entity.getEmail()))
        {
            isChanged = true;
            entity.setEmail(email);
        }

        // HumanID set
        if(person != null && isNew)
        {
            isChanged = true;
            entity.setPerson(person);
        }
        if (null == entity.getPerson())
            throw new ProcessingDatagramObjectException("ContactPersonType object without HumanID!");

        Principal principal = entity.getPrincipal();
        if (login != null && !login.equals(principal.getLogin()))
        {
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(Principal.class, Principal.P_LOGIN, entity.getId(), login);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("ContactPersonType login is not unique");
            }
            else
            {
                isChanged = true;
                principal.setLogin(login);
                params.put(PRINCIPAL_LOGIN, principal);
            }
        }

        if(!isEmployeePostContactor && post != null)
            warning.append("This ContactPersonType object must not have EmployeeID.");
        else if(post != null && !post.equals(((EmployeePostContactor) entity).getEmployeePost()))
        {
            isChanged = true;
            ((EmployeePostContactor) entity).setEmployeePost(post);
        }

        if(!isJuridicalContactor && externalOrgUnit != null)
            warning.append("This ContactPersonType object must not have OrganizationID.");
        else if(externalOrgUnit != null && !externalOrgUnit.equals(((JuridicalContactor) entity).getExternalOrgUnit()))
        {
            isChanged = true;
            ((JuridicalContactor) entity).setExternalOrgUnit(externalOrgUnit);
        }

        if(!isJuridicalContactor && postString != null)
            warning.append("This ContactPersonType object must not have ContactPersonPost.");
        else if(postString != null && !postString.equals(((JuridicalContactor) entity).getPost()))
        {
            isChanged = true;
            ((JuridicalContactor) entity).setPost(postString);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<ContactorPerson> w, ContactPersonType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            if (params.containsKey(PRINCIPAL_LOGIN) || params.get(PRINCIPAL_LOGIN) != null)
                session.update(params.get(PRINCIPAL_LOGIN));

            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(EmployeeType.class, session, params, EMPLOYEE, nsiPackage);
            saveChildEntity(OrganizationType.class, session, params, ORGANIZATION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(ContactorPerson.P_ARCHIVAL_DATE);
        fields.add(ContactorPerson.P_SIMPLE_SEARCH_INDEX);
        fields.add(ContactorPerson.P_SEARCH_INDEX);
        fields.add(ContactorPerson.P_ACTIVE);
        return  fields;
    }
}