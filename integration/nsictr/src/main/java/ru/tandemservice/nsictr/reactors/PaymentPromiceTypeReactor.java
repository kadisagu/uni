/* $Id$ */
package ru.tandemservice.nsictr.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContractVersionContractorType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.PaymentPromiceType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.09.2015
 */
public class PaymentPromiceTypeReactor extends BaseEntityReactor<CtrPaymentPromice, PaymentPromiceType>
{
    private static final String SOURCE = "ContractVersionContractorID";
    private static final String DESTINATION = "DestinationID";

    @Override
    public Class<CtrPaymentPromice> getEntityClass()
    {
        return CtrPaymentPromice.class;
    }

    @Override
    public void collectUnknownDatagrams(PaymentPromiceType datagramObject, IUnknownDatagramsCollector collector) {
        PaymentPromiceType.ContractVersionContractorID contractVersionContractorID = datagramObject.getContractVersionContractorID();
        ContractVersionContractorType contractVersionContractorType = contractVersionContractorID == null ? null : contractVersionContractorID.getContractVersionContractor();
        if(contractVersionContractorType != null)
            collector.check(contractVersionContractorType.getID(), ContractVersionContractorType.class);

        PaymentPromiceType.DestinationID destinationID = datagramObject.getDestinationID();
        ContractVersionContractorType destinationType = destinationID == null ? null : destinationID.getContractVersionContractor();
        if(destinationType != null)
            collector.check(destinationType.getID(), ContractVersionContractorType.class);
    }

    @Override
    public PaymentPromiceType createDatagramObject(String guid)
    {
        PaymentPromiceType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPaymentPromiceType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(CtrPaymentPromice entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        PaymentPromiceType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPaymentPromiceType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setPaymentPromiceCost(String.valueOf(entity.getCostAsLong()));

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);
        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<ContractVersionContractorType> contractVersionType = createDatagramObject(ContractVersionContractorType.class, entity.getSrc(), commonCache, warning);
        PaymentPromiceType.ContractVersionContractorID contractVersionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPaymentPromiceTypeContractVersionContractorID();
        contractVersionID.setContractVersionContractor(contractVersionType.getObject());
        datagramObject.setContractVersionContractorID(contractVersionID);
        if (contractVersionType.getList() != null)
            result.addAll(contractVersionType.getList());

        ProcessedDatagramObject<ContractVersionContractorType> destinationType = createDatagramObject(ContractVersionContractorType.class, entity.getDst(), commonCache, warning);
        PaymentPromiceType.DestinationID destinationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPaymentPromiceTypeDestinationID();
        destinationID.setContractVersionContractor(destinationType.getObject());
        datagramObject.setDestinationID(destinationID);
        if (destinationType.getList() != null)
            result.addAll(destinationType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }


    @Override
    public ChangedWrapper<CtrPaymentPromice> processDatagramObject(PaymentPromiceType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        Long cost = NsiUtils.parseLong(datagramObject.getPaymentPromiceCost(), "PaymentPromiceCost");

        PaymentPromiceType.ContractVersionContractorID contractVersionContractorID = datagramObject.getContractVersionContractorID();
        ContractVersionContractorType contractVersionContractorType = contractVersionContractorID == null ? null : contractVersionContractorID.getContractVersionContractor();
        CtrContractVersionContractor source = getOrCreate(ContractVersionContractorType.class, params, commonCache, contractVersionContractorType, SOURCE, null, warning);

        PaymentPromiceType.DestinationID destinationID = datagramObject.getDestinationID();
        ContractVersionContractorType destinationType = destinationID == null ? null : destinationID.getContractVersionContractor();
        CtrContractVersionContractor destination = getOrCreate(ContractVersionContractorType.class, params, commonCache, destinationType, DESTINATION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        CtrPaymentPromice entity = null;
        CoreCollectionUtils.Pair<CtrPaymentPromice, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
        {
            entity = new CtrPaymentPromice();
            entity.setDeadlineDate(new Date());
            entity.setCurrency(DataAccessServices.dao().getByCode(org.tandemframework.shared.commonbase.catalog.entity.Currency.class, CurrencyCodes.RUB));
            isNew = true;
            isChanged = true;
        }

        if(source != null && !source.equals(entity.getSrc()))
        {
            isChanged = true;
            entity.setSrc(source);
        }
        if (null == entity.getSrc())
            throw new ProcessingDatagramObjectException("PaymentPromiceType object without ContractVersionContractorID!");

        if(destination != null && !destination.equals(entity.getDst()))
        {
            isChanged = true;
            entity.setDst(destination);
        }
        if (null == entity.getDst())
            throw new ProcessingDatagramObjectException("PaymentPromiceType object without DestinationID!");

        if(cost != null && !cost.equals(entity.getCostAsLong()))
        {
            isChanged = true;
            entity.setCostAsLong(cost);
        }
        if (null == cost && isNew)
            throw new ProcessingDatagramObjectException("PaymentPromiceType object without PaymentPromiceCost!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<CtrPaymentPromice> w, PaymentPromiceType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(ContractVersionContractorType.class, session, params, SOURCE, nsiPackage);
            saveChildEntity(ContractVersionContractorType.class, session, params, DESTINATION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(CtrPaymentPromice.P_STAGE);
        fields.add(CtrPaymentPromice.P_DEADLINE_DATE);
        fields.add(CtrPaymentPromice.L_SOURCE_STAGE);
        fields.add(CtrPaymentPromice.P_COMMENT);
        fields.add(CtrPaymentPromice.L_CURRENCY);
        return  fields;
    }
}
