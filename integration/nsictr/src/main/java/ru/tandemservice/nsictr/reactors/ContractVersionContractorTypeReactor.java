/* $Id$ */
package ru.tandemservice.nsictr.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContactPersonType;
import ru.tandemservice.nsiclient.datagram.ContractVersionContractorType;
import ru.tandemservice.nsiclient.datagram.ContractVersionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.09.2015
 */
public class ContractVersionContractorTypeReactor extends BaseEntityReactor<CtrContractVersionContractor, ContractVersionContractorType>
{
    private static final String VERSION = "ContractVersionID";
    private static final String PERSON = "ContactPersonID";

    @Override
    public Class<CtrContractVersionContractor> getEntityClass()
    {
        return CtrContractVersionContractor.class;
    }

    @Override
    public void collectUnknownDatagrams(ContractVersionContractorType datagramObject, IUnknownDatagramsCollector collector) {
        ContractVersionContractorType.ContactPersonID contactPersonID = datagramObject.getContactPersonID();
        ContactPersonType contactPersonType = contactPersonID == null ? null : contactPersonID.getContactPerson();
        if(contactPersonType != null)
            collector.check(contactPersonType.getID(), ContactPersonType.class);

        ContractVersionContractorType.ContractVersionID contractVersionID = datagramObject.getContractVersionID();
        ContractVersionType contractVersionType = contractVersionID == null ? null : contractVersionID.getContractVersion();
        if(contractVersionType != null)
            collector.check(contractVersionType.getID(), ContractVersionType.class);
    }

    @Override
    public ContractVersionContractorType createDatagramObject(String guid)
    {
        ContractVersionContractorType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionContractorType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(CtrContractVersionContractor entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ContractVersionContractorType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionContractorType();
        datagramObject.setID(nsiEntity.getGuid());

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);
        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<ContractVersionType> contractVersionType = createDatagramObject(ContractVersionType.class, entity.getOwner(), commonCache, warning);
        ContractVersionContractorType.ContractVersionID contractVersionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionContractorTypeContractVersionID();
        contractVersionID.setContractVersion(contractVersionType.getObject());
        datagramObject.setContractVersionID(contractVersionID);
        if (contractVersionType.getList() != null)
            result.addAll(contractVersionType.getList());

        ProcessedDatagramObject<ContactPersonType> contactPersonType = createDatagramObject(ContactPersonType.class, entity.getContactor(), commonCache, warning);
        ContractVersionContractorType.ContactPersonID contactPersonID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionContractorTypeContactPersonID();
        contactPersonID.setContactPerson(contactPersonType.getObject());
        datagramObject.setContactPersonID(contactPersonID);
        if (contactPersonType.getList() != null)
            result.addAll(contactPersonType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected CtrContractVersionContractor findContractVersionContractor(ContactorPerson contactorPerson, CtrContractVersion contractVersion) {
        if (contactorPerson != null && contractVersion != null)
        {
            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, CtrContractVersionContractor.contactor().id()), value(contactorPerson.getId())));
            expressions.add(eq(property(ENTITY_ALIAS, CtrContractVersionContractor.owner().id()), value(contractVersion.getId())));
            List<CtrContractVersionContractor> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<CtrContractVersionContractor> processDatagramObject(ContractVersionContractorType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();
        ContractVersionContractorType.ContactPersonID contactPersonID = datagramObject.getContactPersonID();
        ContactPersonType contactPersonType = contactPersonID == null ? null : contactPersonID.getContactPerson();
        ContactorPerson contactorPerson = getOrCreate(ContactPersonType.class, params, commonCache, contactPersonType, PERSON, null, warning);

        ContractVersionContractorType.ContractVersionID contractVersionID = datagramObject.getContractVersionID();
        ContractVersionType contractVersionType = contractVersionID == null ? null : contractVersionID.getContractVersion();
        CtrContractVersion contractVersion = getOrCreate(ContractVersionType.class, params, commonCache, contractVersionType, VERSION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        CtrContractVersionContractor entity = null;
        CoreCollectionUtils.Pair<CtrContractVersionContractor, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findContractVersionContractor(contactorPerson, contractVersion);
        if(entity == null)
        {
            entity = new CtrContractVersionContractor();
            isNew = true;
            isChanged = true;
        }

        if(contactorPerson != null && !contactorPerson.equals(entity.getContactor()))
        {
            isChanged = true;
            entity.setContactor(contactorPerson);
        }
        if (null == entity.getContactor())
            throw new ProcessingDatagramObjectException("ContractVersionContractorType object without ContactPersonID!");

        if(contractVersion != null && !contractVersion.equals(entity.getOwner()))
        {
            isChanged = true;
            entity.setOwner(contractVersion);
        }
        if (null == entity.getContactor())
            throw new ProcessingDatagramObjectException("ContractVersionContractorType object without ContractVersionID!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<CtrContractVersionContractor> w, ContractVersionContractorType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(ContactPersonType.class, session, params, PERSON, nsiPackage);
            saveChildEntity(ContractVersionType.class, session, params, VERSION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(CtrContractVersionContractor.P_REQUIRE_SIGNATURE);
        fields.add(CtrContractVersionContractor.P_COMMENT);
        fields.add(CtrContractVersionContractor.P_SIGNATURE_TIMESTAMP);
        return  fields;
    }
}
