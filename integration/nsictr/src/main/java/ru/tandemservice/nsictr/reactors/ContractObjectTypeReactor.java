/* $Id$ */
package ru.tandemservice.nsictr.reactors;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.nsiclient.datagram.ContractObjectType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.09.2015
 */
public class ContractObjectTypeReactor extends BaseEntityReactor<CtrContractObject, ContractObjectType>
{
    @Override
    public Class<CtrContractObject> getEntityClass()
    {
        return CtrContractObject.class;
    }

    @Override
    public ContractObjectType createDatagramObject(String guid)
    {
        ContractObjectType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractObjectType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(CtrContractObject entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ContractObjectType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractObjectType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setContractObjectNumber(entity.getNumber());
        datagramObject.setContractObjectType(entity.getType().getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected CtrContractObject findContractObject(String contractObjectNumber, String contractObjectType) {
        List<IExpression> expressions = new ArrayList<>();
        if(contractObjectNumber != null)
            expressions.add(eq(property(ENTITY_ALIAS, CtrContractObject.number()), value(contractObjectNumber)));
        if(contractObjectType != null)
            expressions.add(eq(property(ENTITY_ALIAS, CtrContractObject.type().title()), value(contractObjectType)));
        List<CtrContractObject> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    protected CtrContractType findType(String title) {
        if (title != null)
        {
            List<CtrContractType> list = findEntity(CtrContractType.class, eq(property(ENTITY_ALIAS, CtrContractType.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<CtrContractObject> processDatagramObject(ContractObjectType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();
        String contractObjectNumber = datagramObject.getContractObjectNumber();
        String contractObjectType = datagramObject.getContractObjectType();

        boolean isNew = false;
        boolean isChanged = false;
        CtrContractObject entity = null;
        CoreCollectionUtils.Pair<CtrContractObject, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findContractObject(contractObjectNumber, contractObjectType);
        if(entity == null)
        {
            entity = new CtrContractObject();
            entity.setOrgUnit(TopOrgUnit.getInstance());
            isNew = true;
            isChanged = true;
        }

        if(contractObjectNumber != null && !contractObjectNumber.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(contractObjectNumber);
        }

        if(contractObjectType != null && (entity.getType() == null || !contractObjectType.equals(entity.getType().getTitle())))
        {
            CtrContractType type = findType(contractObjectType);

            if(type == null)
                warning.append("[District with title '").append(contractObjectType).append("' not found]");
            else {
                isChanged = true;
                entity.setType(type);
            }
        }
        if (null == entity.getType())
            throw new ProcessingDatagramObjectException("ContractObjectType object without ContractObjectType!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(CtrContractObject.P_COMMENT);
        fields.add(CtrContractObject.L_ORG_UNIT);
        return  fields;
    }
}
