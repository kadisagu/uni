/* $Id$ */
package ru.tandemservice.nsictr.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContractObjectType;
import ru.tandemservice.nsiclient.datagram.ContractVersionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.09.2015
 */
public class ContractVersionTypeReactor extends BaseEntityReactor<CtrContractVersion, ContractVersionType>
{
    private static final String CONTRACT = "ContractObjectID";

    @Override
    public Class<CtrContractVersion> getEntityClass()
    {
        return CtrContractVersion.class;
    }

    @Override
    public void collectUnknownDatagrams(ContractVersionType datagramObject, IUnknownDatagramsCollector collector) {
        ContractVersionType.ContractObjectID contractObjectID = datagramObject.getContractObjectID();
        ContractObjectType contractObjectType = contractObjectID == null ? null : contractObjectID.getContractObject();
        if(contractObjectType != null) collector.check(contractObjectType.getID(), ContractObjectType.class);
    }

    @Override
    public ContractVersionType createDatagramObject(String guid)
    {
        ContractVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(CtrContractVersion entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ContractVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setContractVersionDurationBeginDate(NsiUtils.formatDate(entity.getDurationBeginDate()));
        datagramObject.setContractVersionNumber(entity.getNumber());

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);
        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<ContractObjectType> contractObjectType = createDatagramObject(ContractObjectType.class, entity.getContract(), commonCache, warning);
        ContractVersionType.ContractObjectID contractObjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createContractVersionTypeContractObjectID();
        contractObjectID.setContractObject(contractObjectType.getObject());
        datagramObject.setContractObjectID(contractObjectID);
        if (contractObjectType.getList() != null)
            result.addAll(contractObjectType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected CtrContractVersion findCtrContractVersion(CtrContractObject contractObject, String versionNumber) {
        List<IExpression> expressions = new ArrayList<>();
        if(contractObject != null)
            expressions.add(eq(property(ENTITY_ALIAS, CtrContractVersion.contract().id()), value(contractObject.getId())));
        if(versionNumber != null)
            expressions.add(eq(property(ENTITY_ALIAS, CtrContractVersion.number()), value(versionNumber)));
        List<CtrContractVersion> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<CtrContractVersion> processDatagramObject(ContractVersionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        String versionNumber = datagramObject.getContractVersionNumber();
        Date durationBeginDate = NsiUtils.parseDate(datagramObject.getContractVersionDurationBeginDate(), "ContractVersionDurationBeginDate");

        ContractVersionType.ContractObjectID contractObjectID = datagramObject.getContractObjectID();
        ContractObjectType contractObjectType = contractObjectID == null ? null : contractObjectID.getContractObject();
        CtrContractObject contractObject = getOrCreate(ContractObjectType.class, params, commonCache, contractObjectType, CONTRACT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        CtrContractVersion entity = null;
        CoreCollectionUtils.Pair<CtrContractVersion, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findCtrContractVersion(contractObject, versionNumber);
        if(entity == null)
        {
            entity = new CtrContractVersion();
            entity.setCreationDate(new Date());
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Admin.class, "e").top(1);
            List<Admin> admins = DataAccessServices.dao().getList(builder);
            if(!admins.isEmpty())
                entity.setCreator(admins.get(0));
            isNew = true;
            isChanged = true;
        }

        if(contractObject != null && !contractObject.equals(entity.getContract()))
        {
            isChanged = true;
            entity.setContract(contractObject);
        }
        if (null == entity.getContract())
            throw new ProcessingDatagramObjectException("ContractVersionType object without ContractObjectID!");

        if(versionNumber != null && !versionNumber.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(versionNumber);
        }
        if (null == entity.getNumber())
            throw new ProcessingDatagramObjectException("ContractVersionType object without ContractVersionNumber!");

        if(durationBeginDate != null && !durationBeginDate.equals(entity.getDurationBeginDate()))
        {
            isChanged = true;
            entity.setDurationBeginDate(durationBeginDate);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<CtrContractVersion> w, ContractVersionType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(ContractObjectType.class, session, params, CONTRACT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(CtrContractVersion.P_CONTRACT_REGISTRATION_DATE);
        fields.add(CtrContractVersion.L_CREATOR);
        fields.add(CtrContractVersion.P_DURATION_END_DATE);
        fields.add(CtrContractVersion.P_COMMENT);
        fields.add(CtrContractVersion.P_CREATION_DATE);

        return  fields;
    }
}
