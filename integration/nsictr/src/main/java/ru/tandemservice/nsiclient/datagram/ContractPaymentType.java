/* $Id$ */
package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника Платеж по договору
 *
 * <p>Java class for ContractPaymentType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ContractPaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="ContractObjectID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ContractObject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContractObjectType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PayerID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ContactPerson" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContactPersonType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PayeeID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ContactPerson" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContactPersonType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ContractPaymentCost" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlLong" minOccurs="0"/>
 *         &lt;element name="ContractPaymentPaymentDate" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlDate" minOccurs="0"/>
 *         &lt;element name="ContractPaymentPaymentDocNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContractPaymentDeadlineDate" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlDate" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractPaymentType", propOrder = {

})
public class ContractPaymentType implements IDatagramObject {

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "ContractObjectID")
    protected ContractPaymentType.ContractObjectID contractObjectID;
    @XmlElement(name = "PayerID")
    protected ContractPaymentType.PayerID payerID;
    @XmlElement(name = "PayeeID")
    protected ContractPaymentType.PayeeID payeeID;
    @XmlElement(name = "ContractPaymentCost")
    @XmlSchemaType(name = "anySimpleType")
    protected String contractPaymentCost;
    @XmlElement(name = "ContractPaymentPaymentDate")
    @XmlSchemaType(name = "anySimpleType")
    protected String contractPaymentPaymentDate;
    @XmlElement(name = "ContractPaymentPaymentDocNumber")
    protected String contractPaymentPaymentDocNumber;
    @XmlElement(name = "ContractPaymentDeadlineDate")
    @XmlSchemaType(name = "anySimpleType")
    protected String contractPaymentDeadlineDate;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the contractObjectID property.
     *
     * @return
     *     possible object is
     *     {@link ContractPaymentType.ContractObjectID }
     *
     */
    public ContractPaymentType.ContractObjectID getContractObjectID() {
        return contractObjectID;
    }

    /**
     * Sets the value of the contractObjectID property.
     *
     * @param value
     *     allowed object is
     *     {@link ContractPaymentType.ContractObjectID }
     *
     */
    public void setContractObjectID(ContractPaymentType.ContractObjectID value) {
        this.contractObjectID = value;
    }

    /**
     * Gets the value of the contactPersonID property.
     *
     * @return
     *     possible object is
     *     {@link ru.tandemservice.nsiclient.datagram.ContractPaymentType.PayerID }
     *
     */
    public ContractPaymentType.PayerID getPayerID() {
        return this.payerID;
    }

    /**
     * Sets the value of the contactPersonID property.
     *
     * @param value
     *     allowed object is
     *     {@link ru.tandemservice.nsiclient.datagram.ContractPaymentType.PayerID }
     *
     */
    public void setPayerID(ContractPaymentType.PayerID value) {
        this.payerID = value;
    }

    /**
     * Gets the value of the payeeID property.
     *
     * @return
     *     possible object is
     *     {@link ContractPaymentType.PayeeID }
     *
     */
    public ContractPaymentType.PayeeID getPayeeID() {
        return payeeID;
    }

    /**
     * Sets the value of the payeeID property.
     *
     * @param value
     *     allowed object is
     *     {@link ContractPaymentType.PayeeID }
     *
     */
    public void setPayeeID(ContractPaymentType.PayeeID value) {
        this.payeeID = value;
    }

    /**
     * Gets the value of the contractPaymentCost property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getContractPaymentCost() {
        return contractPaymentCost;
    }

    /**
     * Sets the value of the contractPaymentCost property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setContractPaymentCost(String value) {
        this.contractPaymentCost = value;
    }

    /**
     * Gets the value of the contractPaymentPaymentDate property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getContractPaymentPaymentDate() {
        return contractPaymentPaymentDate;
    }

    /**
     * Sets the value of the contractPaymentPaymentDate property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setContractPaymentPaymentDate(String value) {
        this.contractPaymentPaymentDate = value;
    }

    /**
     * Gets the value of the contractPaymentPaymentDocNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getContractPaymentPaymentDocNumber() {
        return contractPaymentPaymentDocNumber;
    }

    /**
     * Sets the value of the contractPaymentPaymentDocNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setContractPaymentPaymentDocNumber(String value) {
        this.contractPaymentPaymentDocNumber = value;
    }

    /**
     * Gets the value of the contractPaymentDeadlineDate property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getContractPaymentDeadlineDate() {
        return contractPaymentDeadlineDate;
    }

    /**
     * Sets the value of the contractPaymentDeadlineDate property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setContractPaymentDeadlineDate(String value) {
        this.contractPaymentDeadlineDate = value;
    }

    /**
     * Gets the value of the oid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     *
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     *
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     *
     *
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class PayerID {

        @XmlElement(name = "ContactPerson")
        protected ContactPersonType contactPerson;

        /**
         * Gets the value of the contactPerson property.
         *
         * @return
         *     possible object is
         *     {@link ContactPersonType }
         *
         */
        public ContactPersonType getContactPerson() {
            return contactPerson;
        }

        /**
         * Sets the value of the contactPerson property.
         *
         * @param value
         *     allowed object is
         *     {@link ContactPersonType }
         *
         */
        public void setContactPerson(ContactPersonType value) {
            this.contactPerson = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ContractObject" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContractObjectType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class ContractObjectID {

        @XmlElement(name = "ContractObject")
        protected ContractObjectType contractObject;

        /**
         * Gets the value of the contractObject property.
         *
         * @return
         *     possible object is
         *     {@link ContractObjectType }
         *
         */
        public ContractObjectType getContractObject() {
            return contractObject;
        }

        /**
         * Sets the value of the contractObject property.
         *
         * @param value
         *     allowed object is
         *     {@link ContractObjectType }
         *
         */
        public void setContractObject(ContractObjectType value) {
            this.contractObject = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ContactPerson" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ContactPersonType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class PayeeID {

        @XmlElement(name = "ContactPerson")
        protected ContactPersonType contactPerson;

        /**
         * Gets the value of the contactPerson property.
         *
         * @return
         *     possible object is
         *     {@link ContactPersonType }
         *
         */
        public ContactPersonType getContactPerson() {
            return contactPerson;
        }

        /**
         * Sets the value of the contactPerson property.
         *
         * @param value
         *     allowed object is
         *     {@link ContactPersonType }
         *
         */
        public void setContactPerson(ContactPersonType value) {
            this.contactPerson = value;
        }

    }

}
