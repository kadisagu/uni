/* $Id:$ */
package ru.tandemservice.lksunispp.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksunispp.entity.LksSchedulePeriod;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AcademicGroupType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.LksSchedulePeriodType;
import ru.tandemservice.nsiclient.datagram.LksSubjectType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2016
 */
public class LksSchedulePeriodTypeReactor extends BaseEntityReactor<LksSchedulePeriod, LksSchedulePeriodType>
{
    private static final String LKS_SUBJECT = "LksSubjectID";
    private static final String ACADEMIC_GROUP = "AcademicGroupID";

    @Override
    public Class<LksSchedulePeriod> getEntityClass()
    {
        return LksSchedulePeriod.class;
    }

    @Override
    public void collectUnknownDatagrams(LksSchedulePeriodType datagramObject, IUnknownDatagramsCollector collector)
    {
        LksSchedulePeriodType.LksSubjectID lksSubjectID = datagramObject.getLksSubjectID();
        LksSubjectType lksSubjectType = lksSubjectID == null ? null : lksSubjectID.getLksSubject();
        if (lksSubjectType != null) collector.check(lksSubjectType.getID(), LksSubjectType.class);

        LksSchedulePeriodType.AcademicGroupID academicGroupID = datagramObject.getAcademicGroupID();
        AcademicGroupType academicGroupType = academicGroupID == null ? null : academicGroupID.getAcademicGroup();
        if (academicGroupType != null) collector.check(academicGroupType.getID(), AcademicGroupType.class);
    }

    @Override
    public LksSchedulePeriodType createDatagramObject(String guid)
    {
        LksSchedulePeriodType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSchedulePeriodType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(LksSchedulePeriod entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        LksSchedulePeriodType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSchedulePeriodType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setLksSchedulePeriodID(String.valueOf(entity.getEntityId()));
        datagramObject.setLksSchedulePeriodSubjectName(entity.getSubjectTitle());
        datagramObject.setLksSchedulePeriodSubjectShortName(entity.getSubjectShortTitle());
        datagramObject.setLksSchedulePeriodTypeCode(entity.getTypeCode());
        datagramObject.setLksSchedulePeriodDayOfWeek(String.valueOf(entity.getDayOfWeek()));
        datagramObject.setLksSchedulePeriodWeekType(String.valueOf(entity.getWeekType()));
        datagramObject.setLksSchedulePeriodPosition(String.valueOf(entity.getPosition()));
        datagramObject.setLksSchedulePeriodPlace(entity.getPlace());
        datagramObject.setLksSchedulePeriodLecturer(entity.getLecturer());
        datagramObject.setLksSchedulePeriodDate(NsiUtils.formatDate(entity.getDate()));
        datagramObject.setLksSchedulePeriodDateStart(NsiUtils.formatDate(entity.getDateStart()));
        datagramObject.setLksSchedulePeriodDateEnd(NsiUtils.formatDate(entity.getDateEnd()));
        datagramObject.setLksSchedulePeriodTimeStart(entity.getTimeStart());
        datagramObject.setLksSchedulePeriodTimeEnd(entity.getTimeEnd());

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<LksSubjectType> lksSubjectType = createDatagramObject(LksSubjectType.class, entity.getSubject(), commonCache, warning);
        LksSchedulePeriodType.LksSubjectID lksSubjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSchedulePeriodTypeLksSubjectID();
        lksSubjectID.setLksSubject(lksSubjectType.getObject());
        datagramObject.setLksSubjectID(lksSubjectID);
        if (lksSubjectType.getList() != null) result.addAll(lksSubjectType.getList());

        ProcessedDatagramObject<AcademicGroupType> academicGroupType = createDatagramObject(AcademicGroupType.class, entity.getGroup(), commonCache, warning);
        LksSchedulePeriodType.AcademicGroupID academicGroupID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createLksSchedulePeriodTypeAcademicGroupID();
        academicGroupID.setAcademicGroup(academicGroupType.getObject());
        datagramObject.setAcademicGroupID(academicGroupID);
        if (academicGroupType.getList() != null) result.addAll(academicGroupType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private LksSchedulePeriod findLksSchedulePeriod(String code)
    {
        if (code != null)
        {
            Long entityId = null;
            try
            {
                entityId = Long.parseLong(code);
            } catch (NumberFormatException ex)
            {
                // Не судьба
            }
            List<LksSchedulePeriod> list = findEntity(eq(property(ENTITY_ALIAS, LksSchedulePeriod.entityId()), value(entityId)));
            if (list.size() == 1) return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<LksSchedulePeriod> processDatagramObject(LksSchedulePeriodType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // LksSchedulePeriodID init
        String code = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodID());
        // LksSchedulePeriodSubjectName init
        String subjectName = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodSubjectName());
        // LksSchedulePeriodSubjectShortName init
        String subjectShortName = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodSubjectShortName());
        // LksSchedulePeriodTypeCode init
        String typeCode = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodTypeCode());
        // LksSchedulePeriodDayOfWeek init
        Integer dayOfWeek = null;
        try
        {
            dayOfWeek = Integer.parseInt(StringUtils.trimToNull(datagramObject.getLksSchedulePeriodDayOfWeek()));
        } catch (NumberFormatException ex)
        {
        }
        // LksSchedulePeriodWeekType init
        Integer weekType = null;
        try
        {
            weekType = Integer.parseInt(StringUtils.trimToNull(datagramObject.getLksSchedulePeriodWeekType()));
        } catch (NumberFormatException ex)
        {
        }
        // LksSchedulePeriodPosition init
        Integer position = null;
        try
        {
            position = Integer.parseInt(StringUtils.trimToNull(datagramObject.getLksSchedulePeriodPosition()));
        } catch (NumberFormatException ex)
        {
        }
        // LksSchedulePeriodPlace init
        String place = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodPlace());
        // LksSchedulePeriodLecturer init
        String lecturer = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodLecturer());
        // LksSchedulePeriodDate init
        Date date = NsiUtils.parseDate(datagramObject.getLksSchedulePeriodDate(), "LksSchedulePeriodDate");
        // LksSchedulePeriodDateStart init
        Date dateStart = NsiUtils.parseDate(datagramObject.getLksSchedulePeriodDateStart(), "LksSchedulePeriodDateStart");
        // LksSchedulePeriodDateEnd init
        Date dateEnd = NsiUtils.parseDate(datagramObject.getLksSchedulePeriodDateEnd(), "LksSchedulePeriodDateEnd");
        // LksSchedulePeriodTimeStart init
        String timeStart = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodTimeStart());
        // LksSchedulePeriodTimeEnd init
        String timeEnd = StringUtils.trimToNull(datagramObject.getLksSchedulePeriodTimeEnd());

        // LksSubjectID init
        LksSchedulePeriodType.LksSubjectID subjectID = datagramObject.getLksSubjectID();
        LksSubjectType lksSubjectType = subjectID == null ? null : subjectID.getLksSubject();
        LksSubject lksSubject = getOrCreate(LksSubjectType.class, params, commonCache, lksSubjectType, LKS_SUBJECT, null, warning);

        // AcademicGroupID init
        LksSchedulePeriodType.AcademicGroupID groupID = datagramObject.getAcademicGroupID();
        AcademicGroupType academicGroupType = groupID == null ? null : groupID.getAcademicGroup();
        Group group = getOrCreate(AcademicGroupType.class, params, commonCache, academicGroupType, ACADEMIC_GROUP, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        LksSchedulePeriod entity = null;
        CoreCollectionUtils.Pair<LksSchedulePeriod, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findLksSchedulePeriod(code);
        if (entity == null)
        {
            entity = new LksSchedulePeriod();
        }

        // LksSchedulePeriodSubjectName set
        if (null != subjectName && (null == entity.getSubjectTitle() || !subjectName.equals(entity.getSubjectTitle())))
        {
            isChanged = true;
            entity.setSubjectTitle(subjectName);
        }
        // LksSchedulePeriodSubjectShortName set
        if (null != subjectShortName && (null == entity.getSubjectShortTitle() || !subjectShortName.equals(entity.getSubjectShortTitle())))
        {
            isChanged = true;
            entity.setSubjectShortTitle(subjectShortName);
        }
        // LksSchedulePeriodTypeCode set
        if (null != typeCode && (null == entity.getTypeCode() || !typeCode.equals(entity.getTypeCode())))
        {
            isChanged = true;
            entity.setTypeCode(typeCode);
        }
        // LksSchedulePeriodDayOfWeek set
        if (null != dayOfWeek && (null == entity.getDayOfWeek() || !dayOfWeek.equals(entity.getDayOfWeek())))
        {
            isChanged = true;
            entity.setDayOfWeek(dayOfWeek);
        }
        // LksSchedulePeriodWeekType set
        if (null != weekType && (null == entity.getWeekType() || !weekType.equals(entity.getWeekType())))
        {
            isChanged = true;
            entity.setWeekType(weekType);
        }
        // LksSchedulePeriodPosition set
        if (null != position && (null == entity.getPosition() || !position.equals(entity.getPosition())))
        {
            isChanged = true;
            entity.setPosition(position);
        }
        // LksSchedulePeriodPlace set
        if (null != place && (null == entity.getPlace() || !place.equals(entity.getPlace())))
        {
            isChanged = true;
            entity.setPlace(place);
        }
        // LksSchedulePeriodLecturer set
        if (null != lecturer && (null == entity.getLecturer() || !lecturer.equals(entity.getLecturer())))
        {
            isChanged = true;
            entity.setLecturer(lecturer);
        }
        // LksSchedulePeriodDate set
        if (null != date && (null == entity.getDate() || !date.equals(entity.getDate())))
        {
            isChanged = true;
            entity.setDate(date);
        }
        // LksSchedulePeriodDateStart set
        if (null != dateStart && (null == entity.getDateStart() || !dateStart.equals(entity.getDateStart())))
        {
            isChanged = true;
            entity.setDateStart(dateStart);
        }
        // LksSchedulePeriodDateEnd set
        if (null != dateEnd && (null == entity.getDateEnd() || !dateEnd.equals(entity.getDateEnd())))
        {
            isChanged = true;
            entity.setDateEnd(dateEnd);
        }
        // LksSchedulePeriodTimeStart set
        if (null != timeStart && (null == entity.getTimeStart() || !timeStart.equals(entity.getTimeStart())))
        {
            isChanged = true;
            entity.setTimeStart(timeStart);
        }
        // LksSchedulePeriodTimeEnd set
        if (null != timeEnd && (null == entity.getTimeEnd() || !timeEnd.equals(entity.getTimeEnd())))
        {
            isChanged = true;
            entity.setTimeEnd(timeEnd);
        }

        // LksSubjectID set
        if (null != lksSubject && (null == entity.getSubject() || !lksSubject.equals(entity.getSubject())))
        {
            isChanged = true;
            entity.setSubject(lksSubject);
        }

        // AcademicGroupID set
        if (null != group && (null == entity.getGroup() || !group.equals(entity.getGroup())))
        {
            isChanged = true;
            entity.setGroup(group);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<LksSchedulePeriod> w, LksSchedulePeriodType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(LksSubjectType.class, session, params, LKS_SUBJECT, nsiPackage);
            saveChildEntity(AcademicGroupType.class, session, params, ACADEMIC_GROUP, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}