package ru.tandemservice.lksunispp.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksunispp.entity.LksSchedulePeriod;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка учебного расписания для личного кабинета студента (Period)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksSchedulePeriodGen extends EntityBase
 implements INaturalIdentifiable<LksSchedulePeriodGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksunispp.entity.LksSchedulePeriod";
    public static final String ENTITY_NAME = "lksSchedulePeriod";
    public static final int VERSION_HASH = 1115157911;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_SUBJECT_TITLE = "subjectTitle";
    public static final String P_SUBJECT_SHORT_TITLE = "subjectShortTitle";
    public static final String P_TYPE_CODE = "typeCode";
    public static final String P_DAY_OF_WEEK = "dayOfWeek";
    public static final String P_WEEK_TYPE = "weekType";
    public static final String P_POSITION = "position";
    public static final String P_PLACE = "place";
    public static final String P_LECTURER = "lecturer";
    public static final String P_DATE = "date";
    public static final String P_DATE_START = "dateStart";
    public static final String P_DATE_END = "dateEnd";
    public static final String P_TIME_START = "timeStart";
    public static final String P_TIME_END = "timeEnd";
    public static final String L_SUBJECT = "subject";
    public static final String L_GROUP = "group";

    private long _entityId;     // Идентификатор связанного объекта
    private String _subjectTitle;     // Наименование дисциплины
    private String _subjectShortTitle;     // Сокращенное наименование дисциплины
    private String _typeCode;     // Код вида аудиторной нагрузки
    private Integer _dayOfWeek;     // День недели
    private Integer _weekType;     // Тип сменности по неделям (четная(2)/нечетная(1)/любая(0)
    private Integer _position;     // Позиция в сетке расписания (номер по счету от начала дня)
    private String _place;     // Место проведения (номер аудитории)
    private String _lecturer;     // Преподаватель
    private Date _date;     // Дата переносимой пары (для переносов)
    private Date _dateStart;     // Дата начала действия расписания
    private Date _dateEnd;     // Дата окончания действия расписания
    private String _timeStart;     // Время начала пары
    private String _timeEnd;     // Время окончания пары
    private LksSubject _subject;     // Дисциплина реестра для личного кабинета студента
    private Group _group;     // Академическая группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSubjectTitle()
    {
        return _subjectTitle;
    }

    /**
     * @param subjectTitle Наименование дисциплины. Свойство не может быть null.
     */
    public void setSubjectTitle(String subjectTitle)
    {
        dirty(_subjectTitle, subjectTitle);
        _subjectTitle = subjectTitle;
    }

    /**
     * @return Сокращенное наименование дисциплины.
     */
    @Length(max=255)
    public String getSubjectShortTitle()
    {
        return _subjectShortTitle;
    }

    /**
     * @param subjectShortTitle Сокращенное наименование дисциплины.
     */
    public void setSubjectShortTitle(String subjectShortTitle)
    {
        dirty(_subjectShortTitle, subjectShortTitle);
        _subjectShortTitle = subjectShortTitle;
    }

    /**
     * @return Код вида аудиторной нагрузки.
     */
    @Length(max=255)
    public String getTypeCode()
    {
        return _typeCode;
    }

    /**
     * @param typeCode Код вида аудиторной нагрузки.
     */
    public void setTypeCode(String typeCode)
    {
        dirty(_typeCode, typeCode);
        _typeCode = typeCode;
    }

    /**
     * @return День недели.
     */
    public Integer getDayOfWeek()
    {
        return _dayOfWeek;
    }

    /**
     * @param dayOfWeek День недели.
     */
    public void setDayOfWeek(Integer dayOfWeek)
    {
        dirty(_dayOfWeek, dayOfWeek);
        _dayOfWeek = dayOfWeek;
    }

    /**
     * @return Тип сменности по неделям (четная(2)/нечетная(1)/любая(0).
     */
    public Integer getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип сменности по неделям (четная(2)/нечетная(1)/любая(0).
     */
    public void setWeekType(Integer weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    /**
     * @return Позиция в сетке расписания (номер по счету от начала дня).
     */
    public Integer getPosition()
    {
        return _position;
    }

    /**
     * @param position Позиция в сетке расписания (номер по счету от начала дня).
     */
    public void setPosition(Integer position)
    {
        dirty(_position, position);
        _position = position;
    }

    /**
     * @return Место проведения (номер аудитории).
     */
    @Length(max=255)
    public String getPlace()
    {
        return _place;
    }

    /**
     * @param place Место проведения (номер аудитории).
     */
    public void setPlace(String place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Преподаватель.
     */
    @Length(max=255)
    public String getLecturer()
    {
        return _lecturer;
    }

    /**
     * @param lecturer Преподаватель.
     */
    public void setLecturer(String lecturer)
    {
        dirty(_lecturer, lecturer);
        _lecturer = lecturer;
    }

    /**
     * @return Дата переносимой пары (для переносов).
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата переносимой пары (для переносов).
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Дата начала действия расписания.
     */
    public Date getDateStart()
    {
        return _dateStart;
    }

    /**
     * @param dateStart Дата начала действия расписания.
     */
    public void setDateStart(Date dateStart)
    {
        dirty(_dateStart, dateStart);
        _dateStart = dateStart;
    }

    /**
     * @return Дата окончания действия расписания.
     */
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Дата окончания действия расписания.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Время начала пары.
     */
    @Length(max=255)
    public String getTimeStart()
    {
        return _timeStart;
    }

    /**
     * @param timeStart Время начала пары.
     */
    public void setTimeStart(String timeStart)
    {
        dirty(_timeStart, timeStart);
        _timeStart = timeStart;
    }

    /**
     * @return Время окончания пары.
     */
    @Length(max=255)
    public String getTimeEnd()
    {
        return _timeEnd;
    }

    /**
     * @param timeEnd Время окончания пары.
     */
    public void setTimeEnd(String timeEnd)
    {
        dirty(_timeEnd, timeEnd);
        _timeEnd = timeEnd;
    }

    /**
     * @return Дисциплина реестра для личного кабинета студента.
     */
    public LksSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Дисциплина реестра для личного кабинета студента.
     */
    public void setSubject(LksSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Академическая группа.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Академическая группа.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksSchedulePeriodGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksSchedulePeriod)another).getEntityId());
            }
            setSubjectTitle(((LksSchedulePeriod)another).getSubjectTitle());
            setSubjectShortTitle(((LksSchedulePeriod)another).getSubjectShortTitle());
            setTypeCode(((LksSchedulePeriod)another).getTypeCode());
            setDayOfWeek(((LksSchedulePeriod)another).getDayOfWeek());
            setWeekType(((LksSchedulePeriod)another).getWeekType());
            setPosition(((LksSchedulePeriod)another).getPosition());
            setPlace(((LksSchedulePeriod)another).getPlace());
            setLecturer(((LksSchedulePeriod)another).getLecturer());
            setDate(((LksSchedulePeriod)another).getDate());
            setDateStart(((LksSchedulePeriod)another).getDateStart());
            setDateEnd(((LksSchedulePeriod)another).getDateEnd());
            setTimeStart(((LksSchedulePeriod)another).getTimeStart());
            setTimeEnd(((LksSchedulePeriod)another).getTimeEnd());
            setSubject(((LksSchedulePeriod)another).getSubject());
            setGroup(((LksSchedulePeriod)another).getGroup());
        }
    }

    public INaturalId<LksSchedulePeriodGen> getNaturalId()
    {
        return new NaturalId(getEntityId());
    }

    public static class NaturalId extends NaturalIdBase<LksSchedulePeriodGen>
    {
        private static final String PROXY_NAME = "LksSchedulePeriodNaturalProxy";

        private long _entityId;

        public NaturalId()
        {}

        public NaturalId(long entityId)
        {
            _entityId = entityId;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksSchedulePeriodGen.NaturalId) ) return false;

            LksSchedulePeriodGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksSchedulePeriodGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksSchedulePeriod.class;
        }

        public T newInstance()
        {
            return (T) new LksSchedulePeriod();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "subjectTitle":
                    return obj.getSubjectTitle();
                case "subjectShortTitle":
                    return obj.getSubjectShortTitle();
                case "typeCode":
                    return obj.getTypeCode();
                case "dayOfWeek":
                    return obj.getDayOfWeek();
                case "weekType":
                    return obj.getWeekType();
                case "position":
                    return obj.getPosition();
                case "place":
                    return obj.getPlace();
                case "lecturer":
                    return obj.getLecturer();
                case "date":
                    return obj.getDate();
                case "dateStart":
                    return obj.getDateStart();
                case "dateEnd":
                    return obj.getDateEnd();
                case "timeStart":
                    return obj.getTimeStart();
                case "timeEnd":
                    return obj.getTimeEnd();
                case "subject":
                    return obj.getSubject();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "subjectTitle":
                    obj.setSubjectTitle((String) value);
                    return;
                case "subjectShortTitle":
                    obj.setSubjectShortTitle((String) value);
                    return;
                case "typeCode":
                    obj.setTypeCode((String) value);
                    return;
                case "dayOfWeek":
                    obj.setDayOfWeek((Integer) value);
                    return;
                case "weekType":
                    obj.setWeekType((Integer) value);
                    return;
                case "position":
                    obj.setPosition((Integer) value);
                    return;
                case "place":
                    obj.setPlace((String) value);
                    return;
                case "lecturer":
                    obj.setLecturer((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "dateStart":
                    obj.setDateStart((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "timeStart":
                    obj.setTimeStart((String) value);
                    return;
                case "timeEnd":
                    obj.setTimeEnd((String) value);
                    return;
                case "subject":
                    obj.setSubject((LksSubject) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "subjectTitle":
                        return true;
                case "subjectShortTitle":
                        return true;
                case "typeCode":
                        return true;
                case "dayOfWeek":
                        return true;
                case "weekType":
                        return true;
                case "position":
                        return true;
                case "place":
                        return true;
                case "lecturer":
                        return true;
                case "date":
                        return true;
                case "dateStart":
                        return true;
                case "dateEnd":
                        return true;
                case "timeStart":
                        return true;
                case "timeEnd":
                        return true;
                case "subject":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "subjectTitle":
                    return true;
                case "subjectShortTitle":
                    return true;
                case "typeCode":
                    return true;
                case "dayOfWeek":
                    return true;
                case "weekType":
                    return true;
                case "position":
                    return true;
                case "place":
                    return true;
                case "lecturer":
                    return true;
                case "date":
                    return true;
                case "dateStart":
                    return true;
                case "dateEnd":
                    return true;
                case "timeStart":
                    return true;
                case "timeEnd":
                    return true;
                case "subject":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "subjectTitle":
                    return String.class;
                case "subjectShortTitle":
                    return String.class;
                case "typeCode":
                    return String.class;
                case "dayOfWeek":
                    return Integer.class;
                case "weekType":
                    return Integer.class;
                case "position":
                    return Integer.class;
                case "place":
                    return String.class;
                case "lecturer":
                    return String.class;
                case "date":
                    return Date.class;
                case "dateStart":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "timeStart":
                    return String.class;
                case "timeEnd":
                    return String.class;
                case "subject":
                    return LksSubject.class;
                case "group":
                    return Group.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksSchedulePeriod> _dslPath = new Path<LksSchedulePeriod>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksSchedulePeriod");
    }
            

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getSubjectTitle()
     */
    public static PropertyPath<String> subjectTitle()
    {
        return _dslPath.subjectTitle();
    }

    /**
     * @return Сокращенное наименование дисциплины.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getSubjectShortTitle()
     */
    public static PropertyPath<String> subjectShortTitle()
    {
        return _dslPath.subjectShortTitle();
    }

    /**
     * @return Код вида аудиторной нагрузки.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getTypeCode()
     */
    public static PropertyPath<String> typeCode()
    {
        return _dslPath.typeCode();
    }

    /**
     * @return День недели.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDayOfWeek()
     */
    public static PropertyPath<Integer> dayOfWeek()
    {
        return _dslPath.dayOfWeek();
    }

    /**
     * @return Тип сменности по неделям (четная(2)/нечетная(1)/любая(0).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getWeekType()
     */
    public static PropertyPath<Integer> weekType()
    {
        return _dslPath.weekType();
    }

    /**
     * @return Позиция в сетке расписания (номер по счету от начала дня).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getPosition()
     */
    public static PropertyPath<Integer> position()
    {
        return _dslPath.position();
    }

    /**
     * @return Место проведения (номер аудитории).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getPlace()
     */
    public static PropertyPath<String> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getLecturer()
     */
    public static PropertyPath<String> lecturer()
    {
        return _dslPath.lecturer();
    }

    /**
     * @return Дата переносимой пары (для переносов).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Дата начала действия расписания.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDateStart()
     */
    public static PropertyPath<Date> dateStart()
    {
        return _dslPath.dateStart();
    }

    /**
     * @return Дата окончания действия расписания.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Время начала пары.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getTimeStart()
     */
    public static PropertyPath<String> timeStart()
    {
        return _dslPath.timeStart();
    }

    /**
     * @return Время окончания пары.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getTimeEnd()
     */
    public static PropertyPath<String> timeEnd()
    {
        return _dslPath.timeEnd();
    }

    /**
     * @return Дисциплина реестра для личного кабинета студента.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getSubject()
     */
    public static LksSubject.Path<LksSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Академическая группа.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends LksSchedulePeriod> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _subjectTitle;
        private PropertyPath<String> _subjectShortTitle;
        private PropertyPath<String> _typeCode;
        private PropertyPath<Integer> _dayOfWeek;
        private PropertyPath<Integer> _weekType;
        private PropertyPath<Integer> _position;
        private PropertyPath<String> _place;
        private PropertyPath<String> _lecturer;
        private PropertyPath<Date> _date;
        private PropertyPath<Date> _dateStart;
        private PropertyPath<Date> _dateEnd;
        private PropertyPath<String> _timeStart;
        private PropertyPath<String> _timeEnd;
        private LksSubject.Path<LksSubject> _subject;
        private Group.Path<Group> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор связанного объекта. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksSchedulePeriodGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getSubjectTitle()
     */
        public PropertyPath<String> subjectTitle()
        {
            if(_subjectTitle == null )
                _subjectTitle = new PropertyPath<String>(LksSchedulePeriodGen.P_SUBJECT_TITLE, this);
            return _subjectTitle;
        }

    /**
     * @return Сокращенное наименование дисциплины.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getSubjectShortTitle()
     */
        public PropertyPath<String> subjectShortTitle()
        {
            if(_subjectShortTitle == null )
                _subjectShortTitle = new PropertyPath<String>(LksSchedulePeriodGen.P_SUBJECT_SHORT_TITLE, this);
            return _subjectShortTitle;
        }

    /**
     * @return Код вида аудиторной нагрузки.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getTypeCode()
     */
        public PropertyPath<String> typeCode()
        {
            if(_typeCode == null )
                _typeCode = new PropertyPath<String>(LksSchedulePeriodGen.P_TYPE_CODE, this);
            return _typeCode;
        }

    /**
     * @return День недели.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDayOfWeek()
     */
        public PropertyPath<Integer> dayOfWeek()
        {
            if(_dayOfWeek == null )
                _dayOfWeek = new PropertyPath<Integer>(LksSchedulePeriodGen.P_DAY_OF_WEEK, this);
            return _dayOfWeek;
        }

    /**
     * @return Тип сменности по неделям (четная(2)/нечетная(1)/любая(0).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getWeekType()
     */
        public PropertyPath<Integer> weekType()
        {
            if(_weekType == null )
                _weekType = new PropertyPath<Integer>(LksSchedulePeriodGen.P_WEEK_TYPE, this);
            return _weekType;
        }

    /**
     * @return Позиция в сетке расписания (номер по счету от начала дня).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getPosition()
     */
        public PropertyPath<Integer> position()
        {
            if(_position == null )
                _position = new PropertyPath<Integer>(LksSchedulePeriodGen.P_POSITION, this);
            return _position;
        }

    /**
     * @return Место проведения (номер аудитории).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getPlace()
     */
        public PropertyPath<String> place()
        {
            if(_place == null )
                _place = new PropertyPath<String>(LksSchedulePeriodGen.P_PLACE, this);
            return _place;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getLecturer()
     */
        public PropertyPath<String> lecturer()
        {
            if(_lecturer == null )
                _lecturer = new PropertyPath<String>(LksSchedulePeriodGen.P_LECTURER, this);
            return _lecturer;
        }

    /**
     * @return Дата переносимой пары (для переносов).
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(LksSchedulePeriodGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Дата начала действия расписания.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDateStart()
     */
        public PropertyPath<Date> dateStart()
        {
            if(_dateStart == null )
                _dateStart = new PropertyPath<Date>(LksSchedulePeriodGen.P_DATE_START, this);
            return _dateStart;
        }

    /**
     * @return Дата окончания действия расписания.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(LksSchedulePeriodGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Время начала пары.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getTimeStart()
     */
        public PropertyPath<String> timeStart()
        {
            if(_timeStart == null )
                _timeStart = new PropertyPath<String>(LksSchedulePeriodGen.P_TIME_START, this);
            return _timeStart;
        }

    /**
     * @return Время окончания пары.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getTimeEnd()
     */
        public PropertyPath<String> timeEnd()
        {
            if(_timeEnd == null )
                _timeEnd = new PropertyPath<String>(LksSchedulePeriodGen.P_TIME_END, this);
            return _timeEnd;
        }

    /**
     * @return Дисциплина реестра для личного кабинета студента.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getSubject()
     */
        public LksSubject.Path<LksSubject> subject()
        {
            if(_subject == null )
                _subject = new LksSubject.Path<LksSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Академическая группа.
     * @see ru.tandemservice.lksunispp.entity.LksSchedulePeriod#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return LksSchedulePeriod.class;
        }

        public String getEntityName()
        {
            return "lksSchedulePeriod";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
