/* $Id:$ */
package ru.tandemservice.lksunispp.base.bo.LksSppSystem.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLInsertValuesBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.LksDao;
import ru.tandemservice.lksbase.entity.LksChangedEntity;
import ru.tandemservice.lksuniepp.entity.LksSubject;
import ru.tandemservice.lksunispp.entity.LksSchedulePeriod;
import ru.tandemservice.lksunispp.utils.LksUnisppListenerEntityListProvider;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.unispp.base.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksSppDao extends LksDao implements ILksSppDao
{
    public List<Long> getEntityToUpdateIdList(Long entityId)
    {
        IEntity entity = get(entityId);
        List<Long> resultList = new ArrayList<>();
        if (null == entity) return resultList;

        if (entity instanceof SppSchedule)
        {
            List<Long> periodIds = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").distinct()
                    .column(property(SppSchedulePeriod.id().fromAlias("e")))
                    .where(eq(property(SppSchedulePeriod.weekRow().schedule().id().fromAlias("e")), value(entityId)))
                    .createStatement(getSession()).list();

            for (Long periodId : periodIds) resultList.add(periodId);
        } else if (entity instanceof SppScheduleWeekRow)
        {
            List<Long> periodIds = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").distinct()
                    .column(property(SppSchedulePeriod.id().fromAlias("e")))
                    .where(eq(property(SppSchedulePeriod.weekRow().id().fromAlias("e")), value(entityId)))
                    .createStatement(getSession()).list();

            for (Long periodId : periodIds) resultList.add(periodId);
        } else if (entity instanceof SppSchedulePeriodFix)
        {
            //TODO что делать с фиксами? PERIOD_MOBILE);
        } else if (entity instanceof SppScheduleSeason)
        {
            List<Long> periodIds = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").distinct()
                    .column(property(SppSchedulePeriod.id().fromAlias("e")))
                    .where(eq(property(SppSchedulePeriod.weekRow().schedule().season().id().fromAlias("e")), value(entityId)))
                    .createStatement(getSession()).list();

            for (Long periodId : periodIds) resultList.add(periodId);
        }

        return resultList;
    }

    @Override
    public void doInintFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        if(null != entityType && !LksUnisppListenerEntityListProvider.PERIOD_MOBILE.equals(entityType))
            return;

        long time = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();

        List<Long> fullEntityIdList = new ArrayList<>();

        DQLSelectBuilder schedPeriodBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(SppSchedulePeriod.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                .column(property(SppSchedulePeriod.id().fromAlias("e"))).where(isNull(property("s")))
                .where(ge(property(SppSchedulePeriod.weekRow().schedule().season().endDate().fromAlias("e")), valueDate(new Date())))
                .where(eq(property(SppSchedulePeriod.weekRow().schedule().approved().fromAlias("e")), value(Boolean.TRUE)))
                .where(eq(property(SppSchedulePeriod.empty().fromAlias("e")), value(Boolean.FALSE)));

        if(null != studentIds && !studentIds.isEmpty())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                    .column(property(Student.group().id().fromAlias("s")))
                    .where(in(property(Student.id().fromAlias("s")), studentIds));
            schedPeriodBuilder.where(in(property(SppSchedulePeriod.weekRow().schedule().group().id().fromAlias("e")), subBuilder.buildQuery()));
        }

        fullEntityIdList.addAll(schedPeriodBuilder.createStatement(getSession()).<Long>list());


        DQLSelectBuilder fixBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "e")
                .joinEntity("e", DQLJoinType.left, LksChangedEntity.class, "s", eq(property(SppSchedulePeriodFix.id().fromAlias("e")), property(LksChangedEntity.entityId().fromAlias("s"))))
                .column(property(SppSchedulePeriodFix.id().fromAlias("e"))).where(isNull(property("s")))
                .where(ge(property(SppSchedulePeriodFix.sppSchedule().season().endDate().fromAlias("e")), valueDate(new Date())))
                .where(eq(property(SppSchedulePeriodFix.sppSchedule().approved().fromAlias("e")), value(Boolean.TRUE)));

        if(null != studentIds && !studentIds.isEmpty())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                    .column(property(Student.group().id().fromAlias("s")))
                    .where(in(property(Student.id().fromAlias("s")), studentIds));
            fixBuilder.where(in(property(SppSchedulePeriodFix.sppSchedule().group().id().fromAlias("e")), subBuilder.buildQuery()));
        }

        fullEntityIdList.addAll(fixBuilder.createStatement(getSession()).<Long>list());

        System.out.println("Подготовка данных о строках расписания заняла " + (System.currentTimeMillis() - time) + " мс.");
        time = System.currentTimeMillis();

        int updCnt = 0;
        Set<Long> processedIdSet = new HashSet<>();
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);

        for (Long id : fullEntityIdList)
        {
            if (!processedIdSet.contains(id))
            {
                insertBuilder.value(LksChangedEntity.entityId().s(), id);
                insertBuilder.value(LksChangedEntity.deleted().s(), Boolean.FALSE);
                insertBuilder.value(LksChangedEntity.entityType().s(), LksUnisppListenerEntityListProvider.PERIOD_MOBILE);
                insertBuilder.addBatch();
                processedIdSet.add(id);
                updCnt++;
            }
            if (updCnt == 300)
            {
                createStatement(insertBuilder).execute();
                insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);
                updCnt = 0;
            }
        }

        if (updCnt > 0) createStatement(insertBuilder).execute();
        System.out.println("Запись строк в БД заняла " + (System.currentTimeMillis() - time) + " мс.");
        System.out.println("Всего " + (System.currentTimeMillis() - startTime) + " мс.");
    }

    @Override
    public int doSyncEntityPortion(int elementsToProcessAmount)
    {
        List<LksChangedEntity> entityPortion = getLksEntityPortion(elementsToProcessAmount, LksUnisppListenerEntityListProvider.PERIOD_MOBILE);
        if (null != entityPortion && !entityPortion.isEmpty())
        {
            List<Long> periodIds = new ArrayList<>(entityPortion.size());
            List<Long> changedEntityIdsList = new ArrayList<>(entityPortion.size());
            entityPortion.stream().forEach(e-> { periodIds.add(e.getEntityId()); changedEntityIdsList.add(e.getId());});

            //Подготоавливаем мап сохраненных ранее объектов
            List<LksSchedulePeriod> savedPeriodList = new DQLSelectBuilder().fromEntity(LksSchedulePeriod.class, "e").column(property("e"))
                    .where(in(property(LksSchedulePeriod.entityId().fromAlias("e")), periodIds))
                    .createStatement(getSession()).list();

            Map<Long, LksSchedulePeriod> periodMap = new HashMap<>();
            for (LksSchedulePeriod period : savedPeriodList) periodMap.put(period.getEntityId(), period);


            List<LksSchedulePeriod> schedulePeriodList = new ArrayList<>();

            List<Object[]> sppPeriodsList = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").column("e").column("s")
                    .joinEntity("e", DQLJoinType.left, LksSubject.class, "s", eq(property(SppSchedulePeriod.subjectVal().id().fromAlias("e")), property(LksSubject.entityId().fromAlias("s"))))
                    .where(eq(property(SppSchedulePeriod.empty().fromAlias("e")), value(Boolean.FALSE)))
                    .where(in(property(SppSchedulePeriod.id().fromAlias("e")), periodIds))
                    .createStatement(getSession()).list();


            for (Object[] item : sppPeriodsList)
            {
                SppSchedulePeriod sppPeriod = (SppSchedulePeriod) item[0];
                LksSubject subject = (LksSubject) item[1];

                LksSchedulePeriod period = periodMap.get(sppPeriod.getId());

                if(null == period)
                {
                    period = new LksSchedulePeriod();
                    period.setEntityId(sppPeriod.getId());
                }

                if (null != sppPeriod.getSubjectVal())
                {
                    period.setSubject(subject);
                    period.setSubjectTitle(sppPeriod.getSubjectVal().getTitle());
                    period.setSubjectShortTitle(sppPeriod.getSubjectVal().getShortTitle());
                    if (null == period.getSubjectShortTitle())
                        period.setSubjectShortTitle(sppPeriod.getSubjectVal().getTitle());
                } else
                {
                    period.setSubjectTitle(sppPeriod.getSubject());
                    period.setSubjectShortTitle(sppPeriod.getSubject());
                }

                if (sppPeriod.getWeekRow().getSchedule().isDiffEven())
                    period.setWeekType(sppPeriod.getWeekRow().isEven() ? 2 : 1);
                else period.setWeekType(0);

                period.setDayOfWeek(sppPeriod.getDayOfWeek() - 1);
                period.setGroup(sppPeriod.getWeekRow().getSchedule().getGroup());
                period.setTypeCode(null != sppPeriod.getSubjectLoadType() ? sppPeriod.getSubjectLoadType().getCode() : EppALoadTypeCodes.TYPE_LECTURES);
                period.setDateStart(sppPeriod.getWeekRow().getSchedule().getSeason().getStartDate());
                period.setDateEnd(sppPeriod.getWeekRow().getSchedule().getSeason().getEndDate());
                period.setPosition(sppPeriod.getPeriodNum());
                period.setTimeStart(String.valueOf(sppPeriod.getWeekRow().getBell().getStartHour() * 60 + sppPeriod.getWeekRow().getBell().getStartMin()));
                period.setTimeEnd(String.valueOf(sppPeriod.getWeekRow().getBell().getEndHour() * 60 + sppPeriod.getWeekRow().getBell().getEndMin()));
                period.setPlace(sppPeriod.getLectureRoom());
                period.setLecturer(sppPeriod.getTeacher());

                schedulePeriodList.add(period);
            }

            List<Object[]> fixPeriodsList = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "e").column("e").column("s")
                    .joinEntity("e", DQLJoinType.left, LksSubject.class, "s", eq(property(SppSchedulePeriod.subjectVal().id().fromAlias("e")), property(LksSubject.entityId().fromAlias("s"))))
                    .where(in(property(SppSchedulePeriodFix.id().fromAlias("e")), periodIds))
                    .createStatement(getSession()).list();

            for (Object[] item : fixPeriodsList)
            {
                SppSchedulePeriodFix sppPeriod = (SppSchedulePeriodFix) item[0];
                LksSubject subject = (LksSubject) item[1];

                LksSchedulePeriod period = new LksSchedulePeriod();
                period.setId(sppPeriod.getId());
                if (null != sppPeriod.getSubjectVal())
                {
                    period.setSubject(subject);
                    period.setSubjectTitle(sppPeriod.getSubjectVal().getTitle());
                    period.setSubjectShortTitle(sppPeriod.getSubjectVal().getShortTitle());
                    if (null == period.getSubjectShortTitle())
                        period.setSubjectShortTitle(sppPeriod.getSubjectVal().getTitle());
                } else
                {
                    period.setSubjectTitle(sppPeriod.getSubject());
                    period.setSubjectShortTitle(sppPeriod.getSubject());
                }

                period.setGroup(sppPeriod.getSppSchedule().getGroup());
                period.setTypeCode(null != sppPeriod.getSubjectLoadType() ? sppPeriod.getSubjectLoadType().getCode() : EppALoadTypeCodes.TYPE_LECTURES);
                period.setDateStart(sppPeriod.getSppSchedule().getSeason().getStartDate());
                period.setDateEnd(sppPeriod.getSppSchedule().getSeason().getEndDate());
                period.setPosition(sppPeriod.getPeriodNum());
                period.setTimeStart(String.valueOf(sppPeriod.getBell().getStartHour() * 60 + sppPeriod.getBell().getStartMin()));
                period.setTimeEnd(String.valueOf(sppPeriod.getBell().getEndHour() * 60 + sppPeriod.getBell().getEndMin()));
                period.setDate(sppPeriod.getDate());
                period.setPlace(sppPeriod.getLectureRoom());
                period.setLecturer(sppPeriod.getTeacher());

                schedulePeriodList.add(period);
            }

//            for (LksSchedulePeriod period : schedulePeriodList) saveOrUpdate(period);
            new MergeAction.SessionMergeAction<Long, LksSchedulePeriod>()
            {
                @Override protected Long key(final LksSchedulePeriod source){ return source.getEntityId();}
                @Override protected void fill(final LksSchedulePeriod target, final LksSchedulePeriod source) { target.update(source, false);}
                @Override protected LksSchedulePeriod buildRow(final LksSchedulePeriod source) { return source;}
            }.merge(savedPeriodList, schedulePeriodList);

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(LksChangedEntity.class);
            deleteBuilder.where(in(property(LksChangedEntity.id()), changedEntityIdsList));
            deleteBuilder.createStatement(getSession()).execute();

            return schedulePeriodList.size();
        }

        return 0;
    }
}