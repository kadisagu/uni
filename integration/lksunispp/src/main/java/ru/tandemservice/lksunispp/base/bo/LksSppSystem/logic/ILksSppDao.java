/* $Id:$ */
package ru.tandemservice.lksunispp.base.bo.LksSppSystem.logic;

import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.ILksDao;

import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public interface ILksSppDao extends ILksDao
{
    void doInintFullLksEntitySync(Collection<Long> studentIds, String entityType);

    List<Long> getEntityToUpdateIdList(Long entityId);

    int doSyncEntityPortion(int elementsToProcessAmount);
}