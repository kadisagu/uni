/* $Id:$ */
package ru.tandemservice.lksunispp.base.bo.LksSppSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.lksunispp.base.bo.LksSppSystem.logic.ILksSppDao;
import ru.tandemservice.lksunispp.base.bo.LksSppSystem.logic.LksSppDao;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
@Configuration
public class LksSppSystemManager extends BusinessObjectManager
{
    public static LksSppSystemManager instance()
    {
        return instance(LksSppSystemManager.class);
    }

    @Bean
    public ILksSppDao dao()
    {
        return new LksSppDao();
    }
}