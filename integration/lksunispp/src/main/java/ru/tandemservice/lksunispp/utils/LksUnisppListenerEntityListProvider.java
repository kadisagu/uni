/* $Id:$ */
package ru.tandemservice.lksunispp.utils;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.lksbase.utils.ILksListenerEntityListProvider;
import ru.tandemservice.lksunispp.base.bo.LksSppSystem.LksSppSystemManager;
import ru.tandemservice.unispp.base.entity.*;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksUnisppListenerEntityListProvider implements ILksListenerEntityListProvider
{
    public static final String PERIOD_MOBILE = "period";

    public static final List<Class> CLASSES_TO_LISTEN = new ArrayList<>();
    public static final Set<String> ORIGINAL_ENTITY_NAME_SET = new HashSet<>();
    public static final Map<String, String> ENTITY_NAME_TO_MOBILE_ENTITY_NAME = new HashMap<>();

    static
    {
        CLASSES_TO_LISTEN.add(SppSchedule.class);
        CLASSES_TO_LISTEN.add(SppScheduleWeekRow.class);
        CLASSES_TO_LISTEN.add(SppSchedulePeriod.class);
        CLASSES_TO_LISTEN.add(SppSchedulePeriodFix.class);
        CLASSES_TO_LISTEN.add(SppScheduleSeason.class);

        ORIGINAL_ENTITY_NAME_SET.add(SppSchedulePeriod.ENTITY_NAME);

        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppSchedule.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppScheduleWeekRow.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppSchedulePeriod.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppSchedulePeriodFix.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppScheduleSeason.ENTITY_NAME, PERIOD_MOBILE);
    }

    @Override
    public int getPriority()
    {
        return 1;
    }

    @Override
    public List<Class> getClassesToListen()
    {
        return CLASSES_TO_LISTEN;
    }

    @Override
    public List<CoreCollectionUtils.Pair<Long, String>> getIdToEntityTypePairList(Long entityId, boolean delete)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entityId);
        if (null == meta) return new ArrayList<>();

        if (!CLASSES_TO_LISTEN.contains(meta.getEntityClass())) return new ArrayList<>();

        List<CoreCollectionUtils.Pair<Long, String>> resultIdList = new ArrayList<>();

        if (ORIGINAL_ENTITY_NAME_SET.contains(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(entityId, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName())));
        } else
        {
            for (Long id : LksSppSystemManager.instance().dao().getEntityToUpdateIdList(entityId))
                resultIdList.add(new CoreCollectionUtils.Pair<>(id, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName())));
        }
        return resultIdList;
    }

    @Override
    public void initFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        LksSppSystemManager.instance().dao().doInintFullLksEntitySync(studentIds, entityType);
    }

    @Override
    public int syncLksEntityPortion(int elementsToProcessAmount)
    {
        return LksSppSystemManager.instance().dao().doSyncEntityPortion(elementsToProcessAmount);
    }
}