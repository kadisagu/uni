//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.07.02 at 01:39:16 PM YEKT 
//


package ru.tandemservice.nsiuni.reactors;

import ru.tandemservice.nsiclient.datagram.*;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.nsi.datagram package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.nsi.datagram
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CompensationTypeType }
     *
     */
    public CompensationTypeType createCompensationTypeType() {
        return new CompensationTypeType();
    }

    /**
     * Create an instance of {@link CourseType }
     *
     */
    public CourseType createCourseType() {
        return new CourseType();
    }

    /**
     * Create an instance of {@link DevelopConditionType }
     *
     */
    public DevelopConditionType createDevelopConditionType() {
        return new DevelopConditionType();
    }

    /**
     * Create an instance of {@link DevelopFormType }
     *
     */
    public DevelopFormType createDevelopFormType() {
        return new DevelopFormType();
    }

    /**
     * Create an instance of {@link DevelopPeriodType }
     *
     */
    public DevelopPeriodType createDevelopPeriodType() {
        return new DevelopPeriodType();
    }

    /**
     * Create an instance of {@link DevelopTechType }
     *
     */
    public DevelopTechType createDevelopTechType() {
        return new DevelopTechType();
    }

    /**
     * Create an instance of {@link StudentCategoryType }
     *
     */
    public StudentCategoryType createStudentCategoryType() {
        return new StudentCategoryType();
    }

    /**
     * Create an instance of {@link StudentStatusType }
     * 
     */
    public StudentStatusType createStudentStatusType() {
        return new StudentStatusType();
    }

    /**
     * Create an instance of {@link QualificationType }
     *
     */
    public QualificationType createQualificationType() {
        return new QualificationType();
    }
}
