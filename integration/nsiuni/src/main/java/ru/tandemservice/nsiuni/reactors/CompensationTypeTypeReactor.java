/* $Id: CompensationTypeTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.CompensationTypeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uni.entity.catalog.CompensationType;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class CompensationTypeTypeReactor extends BaseEntityReactor<CompensationType, CompensationTypeType>
{
    @Override
    public Class<CompensationType> getEntityClass()
    {
        return CompensationType.class;
    }

    @Override
    public CompensationTypeType createDatagramObject(String guid)
    {
        CompensationTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createCompensationTypeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(CompensationType entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        CompensationTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createCompensationTypeType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setCompensationTypeID(entity.getCode());
        datagramObject.setCompensationTypeName(entity.getTitle());
        datagramObject.setCompensationTypeNameShort(entity.getShortTitle());
        datagramObject.setCompensationTypeGroup(entity.getGroup());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected CompensationType findCompensationType(String code, String title, String shortTitle)
    {
        if (code != null)
        {
            List<CompensationType> list = findEntity(eq(property(ENTITY_ALIAS, CompensationType.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<CompensationType> list = findEntity(eq(property(ENTITY_ALIAS, CompensationType.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortTitle != null)
        {
            List<CompensationType> list = findEntity(eq(property(ENTITY_ALIAS, CompensationType.shortTitle()), value(shortTitle)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<CompensationType> processDatagramObject(CompensationTypeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getCompensationTypeID());
        String title = StringUtils.trimToNull(datagramObject.getCompensationTypeName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getCompensationTypeNameShort());
        String group = StringUtils.trimToNull(datagramObject.getCompensationTypeGroup());

        boolean isNew = false;
        boolean isChanged = false;
        CompensationType entity = null;

        CoreCollectionUtils.Pair<CompensationType, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findCompensationType(code, title, shortTitle);
        if (entity == null)
        {
            entity = new CompensationType();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        //group
        if (group != null && !group.equals(entity.getGroup()))
        {
            isChanged = true;
            entity.setGroup(group);
        }

        //short title
        if (shortTitle!=null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }

        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("CompensationTypeType object without short title");
        }


        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(CompensationType.class, entity, CompensationType.P_CODE))
            {
                if (code == null)
                    warning.append("[CompensationTypeType object without CompensationTypeTypeID!");
                else
                    warning.append("CompensationTypeTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(CompensationType.class);
                warning.append(" CompensationTypeTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[CompensationTypeTypeID is immutable!]");


        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
