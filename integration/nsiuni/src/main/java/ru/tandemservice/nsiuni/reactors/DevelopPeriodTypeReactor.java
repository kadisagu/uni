/* $Id: DevelopPeriodTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.DevelopPeriodType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class DevelopPeriodTypeReactor extends BaseEntityReactor<DevelopPeriod, DevelopPeriodType>
{
    @Override
    public Class<DevelopPeriod> getEntityClass()
    {
        return DevelopPeriod.class;
    }

    @Override
    public DevelopPeriodType createDatagramObject(String guid)
    {
        DevelopPeriodType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopPeriodType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(DevelopPeriod entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        DevelopPeriodType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopPeriodType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setDevelopPeriodID(entity.getCode());
        datagramObject.setDevelopPeriodName(entity.getTitle());
        datagramObject.setDevelopPeriodLastCourse(String.valueOf(entity.getLastCourse()));
        datagramObject.setDevelopPeriodPriority(String.valueOf(entity.getPriority()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected DevelopPeriod findDevelopPeriod(String code, String title)
    {
        if (code != null)
        {
            List<DevelopPeriod> list = findEntity(eq(property(ENTITY_ALIAS, DevelopPeriod.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<DevelopPeriod> list = findEntity(eq(property(ENTITY_ALIAS, DevelopPeriod.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<DevelopPeriod> processDatagramObject(DevelopPeriodType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {

        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getDevelopPeriodID());
        String title = StringUtils.trimToNull(datagramObject.getDevelopPeriodName());
        Integer priority = NsiUtils.parseInteger(datagramObject.getDevelopPeriodPriority(), "DevelopPeriodPriority");
        Integer lastCourse = NsiUtils.parseInteger(datagramObject.getDevelopPeriodLastCourse(), "DevelopPeriodLastCourse");

        boolean isNew = false;
        boolean isChanged = false;
        DevelopPeriod entity = null;

        CoreCollectionUtils.Pair<DevelopPeriod, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findDevelopPeriod(code, title);
        if (entity == null)
        {
            entity = new DevelopPeriod();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(DevelopPeriod.class, DevelopPeriod.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("DevelopPeriodType title is not unique");
            }
        }
        if (entity.getTitle()==null)
        {
            throw new ProcessingDatagramObjectException("DevelopPeriodType object without title");
        }

        //priority
        if (priority != null && priority != entity.getPriority())
        {
            isChanged = true;
            entity.setPriority(priority);
        }
        if (priority == null && isNew)
        {
            throw new ProcessingDatagramObjectException("DevelopPeriodType object without priority");
        }

        //last Course
        if (lastCourse != null && lastCourse != entity.getLastCourse())
        {
            isChanged = true;
            entity.setLastCourse(lastCourse);
        }
        if (lastCourse == null && isNew)
        {
            throw new ProcessingDatagramObjectException("DevelopPeriodType object without lastCourse");
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(DevelopPeriod.class, entity, DevelopPeriod.P_CODE))
            {
                if (code == null)
                    warning.append("[DevelopPeriodType object without DevelopPeriodTypeID!");
                else
                    warning.append("DevelopPeriodTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopPeriod.class);
                warning.append(" DevelopPeriodTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(DevelopPeriod.class, DevelopPeriod.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[DevelopPeriodTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(DevelopPeriod.L_EDU_PROGRAM_DURATION);
        return  fields;
    }
}
