/* $Id: DevelopFormTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.DevelopFormType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class DevelopFormTypeReactor extends BaseEntityReactor<DevelopForm, DevelopFormType>
{
    @Override
    public Class<DevelopForm> getEntityClass()
    {
        return DevelopForm.class;
    }

    @Override
    public DevelopFormType createDatagramObject(String guid)
    {
        DevelopFormType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopFormType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(DevelopForm entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        DevelopFormType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopFormType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setDevelopFormID(entity.getCode());
        datagramObject.setDevelopFormName(entity.getTitle());
        datagramObject.setDevelopFormNameShort(entity.getShortTitle());
        datagramObject.setDevelopFormGroup(entity.getGroup());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected DevelopForm findDevelopForm(String code, String title, String shortTitle)
    {
        if (code != null)
        {
            List<DevelopForm> list = findEntity(eq(property(ENTITY_ALIAS, DevelopForm.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<DevelopForm> list = findEntity(eq(property(ENTITY_ALIAS, DevelopForm.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);

            list = findEntity(eq(property(ENTITY_ALIAS, DevelopForm.defaultTitle()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortTitle != null)
        {
            List<DevelopForm> list = findEntity(eq(property(ENTITY_ALIAS, DevelopForm.shortTitle()), value(shortTitle)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<DevelopForm> processDatagramObject(DevelopFormType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getDevelopFormID());
        String title = StringUtils.trimToNull(datagramObject.getDevelopFormName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getDevelopFormNameShort());
        String group = StringUtils.trimToNull(datagramObject.getDevelopFormGroup());

        boolean isNew = false;
        boolean isChanged = false;
        DevelopForm entity = null;

        CoreCollectionUtils.Pair<DevelopForm, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findDevelopForm(code, title, shortTitle);
        if (entity == null)
        {
            entity = new DevelopForm();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(DevelopForm.class, DevelopForm.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("DevelopFormType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("DevelopFormType object without title");
        }

        //short title
        if (shortTitle != null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("DevelopFormType object without short title");
        }

        //group
        if (group != null && !group.equals(entity.getGroup()))
        {
            isChanged = true;
            entity.setGroup(group);
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setDefaultTitle(title);
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(DevelopForm.class, entity, DevelopForm.P_CODE))
            {
                if (code == null)
                    warning.append("[DevelopFormType object without DevelopFormTypeID!");
                else
                    warning.append("DevelopFormTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopForm.class);
                warning.append(" DevelopFormTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }

            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(DevelopForm.class, DevelopForm.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[DevelopFormTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(DevelopForm.P_DEFAULT_TITLE);
        fields.add(DevelopForm.L_PROGRAM_FORM);
        return  fields;
    }
}
