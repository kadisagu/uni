/* $Id: CourseTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.CourseType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class CourseTypeReactor extends BaseEntityReactor<Course, CourseType>
{
    @Override
    public Class<Course> getEntityClass()
    {
        return Course.class;
    }

    @Override
    public CourseType createDatagramObject(String guid)
    {
        CourseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createCourseType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(Course entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        CourseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createCourseType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setCourseID(entity.getCode());
        datagramObject.setCourseName(entity.getTitle());
        datagramObject.setCourseNumber(String.valueOf(entity.getIntValue()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected Course findCourse(String code, String title, Integer courseNumber)
    {
        if (code != null)
        {
            List<Course> list = findEntity(eq(property(ENTITY_ALIAS, Course.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<Course> list = findEntity(eq(property(ENTITY_ALIAS, Course.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (courseNumber != null)
        {
            List<Course> list = findEntity(eq(property(ENTITY_ALIAS, Course.intValue()), value(courseNumber)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<Course> processDatagramObject(CourseType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getCourseID());
        Integer courseNumber = NsiUtils.parseInteger(datagramObject.getCourseNumber(), "CourseNumber");
        String title = StringUtils.trimToNull(datagramObject.getCourseName());

        boolean isNew = false;
        boolean isChanged = false;
        Course entity = null;

        CoreCollectionUtils.Pair<Course, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findCourse(code, title, courseNumber);
        if (entity == null)
        {
            entity = new Course();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        //course number
        if (courseNumber != null && courseNumber != entity.getIntValue())
        {
            isChanged = true;
            entity.setIntValue(courseNumber);
        }
        if (isNew && courseNumber == null)
        {
            throw new ProcessingDatagramObjectException("CourseType object without courseNumber");
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(Course.class, entity, Course.P_CODE))
            {
                if (code == null)
                    warning.append("[CourseType object without CourseTypeID!");
                else
                    warning.append("CourseTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(Course.class);
                warning.append(" CourseTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[CourseTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
