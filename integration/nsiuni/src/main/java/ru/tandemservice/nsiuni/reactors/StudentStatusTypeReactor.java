/* $Id: StudentStatusTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.StudentStatusType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class StudentStatusTypeReactor extends BaseEntityReactor<StudentStatus, StudentStatusType>
{
    @Override
    public Class<StudentStatus> getEntityClass()
    {
        return StudentStatus.class;
    }

    @Override
    public StudentStatusType createDatagramObject(String guid)
    {
        StudentStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentStatusType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(StudentStatus entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        StudentStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentStatusType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setStudentStatusID(entity.getCode());
        datagramObject.setStudentStatusName(entity.getTitle());
        datagramObject.setStudentStatusNameShort(entity.getShortTitle());
        datagramObject.setStudentStatusActive(NsiUtils.formatBoolean(entity.isActive()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected StudentStatus findStudentStatus(String code, String title, String shortTitle)
    {
        if (code != null)
        {
            List<StudentStatus> list = findEntity(eq(property(ENTITY_ALIAS, StudentStatus.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<StudentStatus> list = findEntity(eq(property(ENTITY_ALIAS, StudentStatus.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortTitle != null)
        {
            List<StudentStatus> list = findEntity(eq(property(ENTITY_ALIAS, StudentStatus.shortTitle()), value(shortTitle)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<StudentStatus> processDatagramObject(StudentStatusType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getStudentStatusID());
        String title = StringUtils.trimToNull(datagramObject.getStudentStatusName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getStudentStatusNameShort());
        Boolean studentStatusActive = NsiUtils.parseBoolean(datagramObject.getStudentStatusActive());

        boolean isNew = false;
        boolean isChanged = false;
        StudentStatus entity = null;

        CoreCollectionUtils.Pair<StudentStatus, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findStudentStatus(code, title, shortTitle);
        if (entity == null)
        {
            entity = new StudentStatus();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        //short title
        if (shortTitle!=null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("StudentStatusType object without short title");
        }

        //studentStatusActive
        if (studentStatusActive != null && !studentStatusActive.equals(entity.isActive()))
        {
            isChanged = true;
            entity.setActive(studentStatusActive);
        }
        if (isNew && studentStatusActive == null)
        {
            throw new ProcessingDatagramObjectException("StudentStatusType object without studentStatusActive");
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setUsedInSystem(true);

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentStatus.class, "s")
                    .column(max(property("s", StudentStatus.priority())));
            List<Integer> max = DataAccessServices.dao().getList(builder);
            int priority = max.isEmpty() ? 1 : max.get(0) + 1;
            entity.setPriority(priority);
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(StudentStatus.class, entity, StudentStatus.P_CODE))
            {
                if (code == null)
                    warning.append("[StudentStatusType object without StudentStatusID!");
                else
                    warning.append("StudentStatusID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(StudentStatus.class);
                warning.append(" StudentStatusID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[StudentStatusID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(StudentStatus.P_USED_IN_SYSTEM);
        return  fields;
    }
}
