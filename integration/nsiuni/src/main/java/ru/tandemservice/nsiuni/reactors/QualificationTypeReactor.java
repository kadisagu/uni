/* $Id: QualificationTypeReactor.java 43966 2015-07-10 05:13:06Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.QualificationType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class QualificationTypeReactor extends BaseEntityReactor<Qualifications, QualificationType>
{

    @Override
    public Class<Qualifications> getEntityClass()
    {
        return Qualifications.class;
    }

    @Override
    public QualificationType createDatagramObject(String guid)
    {
        QualificationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createQualificationType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(Qualifications entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        QualificationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createQualificationType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setQualificationID(entity.getCode());
        datagramObject.setQualificationName(entity.getTitle());
        datagramObject.setQualificationNameShort(entity.getShortTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected Qualifications findQualifications(String code)
    {
        if (code != null)
        {
            List<Qualifications> list = findEntity(eq(property(ENTITY_ALIAS, Qualifications.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<Qualifications> processDatagramObject(QualificationType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // EduProgramForm init
        String code = StringUtils.trimToNull(datagramObject.getQualificationID());
        String name = StringUtils.trimToNull(datagramObject.getQualificationName());
        String nameShort = StringUtils.trimToNull(datagramObject.getQualificationNameShort());

        boolean isNew = false;
        boolean isChanged = false;
        Qualifications entity = null;

        CoreCollectionUtils.Pair<Qualifications, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findQualifications(code);
        if (entity == null)
        {
            entity = new Qualifications();
            isNew = true;
            isChanged = true;
        }

        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }

        if (nameShort != null && !nameShort.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(nameShort);
        }


        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(Qualifications.class, entity, Qualifications.P_CODE))
            {
                if (code == null)
                    warning.append("[QualificationType object without QualificationTypeID!");
                else
                    warning.append("QualificationTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(Qualifications.class);
                warning.append(" QualificationTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[QualificationTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(Qualifications.P_ORDER);

        return  fields;
    }
}
