/* $Id: DevelopTechTypeReactor.java 43860 2015-07-07 06:17:59Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.DevelopTechType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uni.entity.catalog.DevelopTech;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class DevelopTechTypeReactor extends BaseEntityReactor<DevelopTech, DevelopTechType>
{
    @Override
    public Class<DevelopTech> getEntityClass()
    {
        return DevelopTech.class;
    }

    @Override
    public DevelopTechType createDatagramObject(String guid)
    {
        DevelopTechType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopTechType();
        if (null != guid)
            datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(DevelopTech entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        DevelopTechType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopTechType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setDevelopTechID(entity.getCode());
        datagramObject.setDevelopTechName(entity.getTitle());
        datagramObject.setDevelopTechNameShort(entity.getShortTitle());
        datagramObject.setDevelopTechGroup(entity.getGroup());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected DevelopTech findDevelopTech(String code, String title, String shortTitle)
    {
        if (code != null)
        {
            List<DevelopTech> list = findEntity(eq(property(ENTITY_ALIAS, DevelopTech.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<DevelopTech> list = findEntity(eq(property(ENTITY_ALIAS, DevelopTech.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortTitle != null)
        {
            List<DevelopTech> list = findEntity(eq(property(ENTITY_ALIAS, DevelopTech.shortTitle()), value(shortTitle)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<DevelopTech> processDatagramObject(DevelopTechType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getDevelopTechID());
        String title = StringUtils.trimToNull(datagramObject.getDevelopTechName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getDevelopTechNameShort());
        String group = StringUtils.trimToNull(datagramObject.getDevelopTechGroup());

        boolean isNew = false;
        boolean isChanged = false;
        DevelopTech entity = null;

        CoreCollectionUtils.Pair<DevelopTech, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findDevelopTech(code, title, shortTitle);
        if (entity == null)
        {
            entity = new DevelopTech();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(DevelopTech.class, DevelopTech.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("DevelopTechType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("DevelopTechType object without title");
        }

        //short title
        if (shortTitle != null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }

        //group
        if (group != null && !group.equals(entity.getGroup()))
        {
            isChanged = true;
            entity.setGroup(group);
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(DevelopTech.class, entity, DevelopTech.P_CODE))
            {
                if (code == null)
                    warning.append("[DevelopTechType object without DevelopTechTypeID!");
                else
                    warning.append("DevelopTechTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopTech.class);
                warning.append(" DevelopTechTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(DevelopTech.class, DevelopTech.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[DevelopTechTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(DevelopTech.L_PROGRAM_TRAIT);
        return  fields;
    }
}
