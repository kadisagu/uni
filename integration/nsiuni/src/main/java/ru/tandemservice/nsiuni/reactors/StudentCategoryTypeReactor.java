/* $Id: StudentCategoryTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.StudentCategoryType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uni.entity.catalog.StudentCategory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class StudentCategoryTypeReactor extends BaseEntityReactor<StudentCategory, StudentCategoryType>
{

    @Override
    public Class<StudentCategory> getEntityClass()
    {
        return StudentCategory.class;
    }

    @Override
    public StudentCategoryType createDatagramObject(String guid)
    {
        StudentCategoryType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentCategoryType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(StudentCategory entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        StudentCategoryType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentCategoryType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setStudentCategoryID(entity.getCode());
        datagramObject.setStudentCategoryName(entity.getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected StudentCategory findStudentCategory(String code, String title)
    {
        if (code != null)
        {
            List<StudentCategory> list = findEntity(eq(property(ENTITY_ALIAS, StudentCategory.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<StudentCategory> list = findEntity(eq(property(ENTITY_ALIAS, StudentCategory.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<StudentCategory> processDatagramObject(StudentCategoryType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getStudentCategoryID());
        String title = StringUtils.trimToNull(datagramObject.getStudentCategoryName());

        boolean isNew = false;
        boolean isChanged = false;
        StudentCategory entity = null;

        CoreCollectionUtils.Pair<StudentCategory, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findStudentCategory(code, title);
        if (entity == null)
        {
            entity = new StudentCategory();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(StudentCategory.class, entity, StudentCategory.P_CODE))
            {
                if (code == null)
                    warning.append("[StudentCategoryType object without StudentCategoryID!");
                else
                    warning.append("StudentCategoryID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(StudentCategory.class);
                warning.append(" StudentCategoryID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[StudentCategoryID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
