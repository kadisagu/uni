/* $Id: DevelopConditionTypeReactor.java 43857 2015-07-07 05:48:28Z aavetisov $ */
package ru.tandemservice.nsiuni.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.DevelopConditionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class DevelopConditionTypeReactor extends BaseEntityReactor<DevelopCondition, DevelopConditionType>
{
    @Override
    public Class<DevelopCondition> getEntityClass()
    {
        return DevelopCondition.class;
    }

    @Override
    public DevelopConditionType createDatagramObject(String guid)
    {
        DevelopConditionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopConditionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(DevelopCondition entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        DevelopConditionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createDevelopConditionType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setDevelopConditionID(entity.getCode());
        datagramObject.setDevelopConditionName(entity.getTitle());
        datagramObject.setDevelopConditionNameShort(entity.getShortTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected DevelopCondition findDevelopCondition(String code, String title, String shortTitle)
    {
        if (code != null)
        {
            List<DevelopCondition> list = findEntity(eq(property(ENTITY_ALIAS, DevelopCondition.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<DevelopCondition> list = findEntity(eq(property(ENTITY_ALIAS, DevelopCondition.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortTitle != null)
        {
            List<DevelopCondition> list = findEntity(eq(property(ENTITY_ALIAS, DevelopCondition.shortTitle()), value(shortTitle)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<DevelopCondition> processDatagramObject(DevelopConditionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getDevelopConditionID());
        String title = StringUtils.trimToNull(datagramObject.getDevelopConditionName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getDevelopConditionNameShort());

        boolean isNew = false;
        boolean isChanged = false;
        DevelopCondition entity = null;

        CoreCollectionUtils.Pair<DevelopCondition, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findDevelopCondition(code, title, shortTitle);
        if (entity == null)
        {
            entity = new DevelopCondition();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(DevelopCondition.class, DevelopCondition.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("DevelopConditionType title is not unique");
            }
        }
        if (entity.getTitle()==null)
        {
            throw new ProcessingDatagramObjectException("DevelopConditionType object without title");
        }


        //short title
        if (shortTitle != null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(DevelopCondition.class, entity, DevelopCondition.P_CODE))
            {
                if (code == null)
                    warning.append("[DevelopConditionType object without DevelopConditionTypeID!");
                else
                    warning.append("DevelopConditionTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopCondition.class);
                warning.append(" DevelopConditionTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }

            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(DevelopCondition.class, DevelopCondition.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[DevelopConditionTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(DevelopCondition.P_GROUP);
        return  fields;
    }
}
