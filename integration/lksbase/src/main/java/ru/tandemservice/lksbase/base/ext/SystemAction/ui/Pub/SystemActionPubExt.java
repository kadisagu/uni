/* $Id:$ */
package ru.tandemservice.lksbase.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Dmitry Seleznev
 * @since 13.08.2016
 */
@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String LKS_SYSTEM_ACTION_PUB_ADDON_NAME = "lksSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(LKS_SYSTEM_ACTION_PUB_ADDON_NAME, LksSystemActionPubAddon.class))
                .create();
    }
}