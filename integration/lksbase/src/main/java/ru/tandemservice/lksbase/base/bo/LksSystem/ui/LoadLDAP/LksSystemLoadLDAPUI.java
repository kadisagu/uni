package ru.tandemservice.lksbase.base.bo.LksSystem.ui.LoadLDAP;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;

import java.io.IOException;

/**
 * Created by nsvetlov on 01.10.2016.
 */
public class LksSystemLoadLDAPUI extends UIPresenter
{
    private IUploadFile _uploadFile;
    private String _log;

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public String getLog() {
        return _log;
    }

    public void setLog(String log) {
        _log = log;
    }

    public void onClickApply() throws IOException
    {
        setLog(LksSystemManager.instance().dao().doInitLoadLDapData(getUploadFile().getStream()));
    }

}
