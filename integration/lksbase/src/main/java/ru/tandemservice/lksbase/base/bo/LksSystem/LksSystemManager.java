/* $Id:$ */
package ru.tandemservice.lksbase.base.bo.LksSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.ILksDao;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.ILksTrimLoginDao;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.LksDao;
import ru.tandemservice.lksbase.base.bo.LksSystem.logic.LksTrimLoginDao;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
@Configuration
public class LksSystemManager extends BusinessObjectManager
{
    public static LksSystemManager instance()
    {
        return instance(LksSystemManager.class);
    }

    @Bean
    public ILksDao dao()
    {
        return new LksDao();
    }

    @Bean
    public ILksTrimLoginDao trimDao()
    {
        return new LksTrimLoginDao();
    }
}