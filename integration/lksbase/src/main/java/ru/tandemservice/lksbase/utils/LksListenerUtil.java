/* $Id:$ */
package ru.tandemservice.lksbase.utils;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksListenerUtil
{
    public static final int PORTION_AMOUNT = 500;
    public static List<ILksListenerEntityListProvider> PROVIDERS;

    public static List<Class> getEntityToListenList()
    {
        List<Class> result = new ArrayList<>();
        List<ILksListenerEntityListProvider> providers = getProvidersSortedByPriority();
        for (ILksListenerEntityListProvider provider : providers)
        {
            result.addAll(provider.getClassesToListen());
        }
        return result;
    }

    public static List<CoreCollectionUtils.Pair<Long, String>> getActualTypedEntityList(Long entityId, boolean delete)
    {
        final List<CoreCollectionUtils.Pair<Long, String>> result = new ArrayList<>();
        List<ILksListenerEntityListProvider> providers = getProvidersSortedByPriority();
        for (ILksListenerEntityListProvider provider : providers)
        {
            result.addAll(provider.getIdToEntityTypePairList(entityId, delete));
        }
        return result;
    }

    public static void initFullLksEntitySync(String entityType)
    {
        final List<ILksListenerEntityListProvider> providers = getProvidersSortedByPriority();
        for (ILksListenerEntityListProvider provider : providers)
        {
            provider.initFullLksEntitySync(null, entityType);
        }
    }

    public static void initFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        final List<ILksListenerEntityListProvider> providers = getProvidersSortedByPriority();
        for (ILksListenerEntityListProvider provider : providers)
        {
            provider.initFullLksEntitySync(studentIds, entityType);
        }
    }

    public static int syncLksEntityPortion()
    {
        final List<ILksListenerEntityListProvider> providers = getProvidersSortedByPriority();
        for (ILksListenerEntityListProvider provider : providers)
        {
            int processedAmount = provider.syncLksEntityPortion(PORTION_AMOUNT);
            if (500 < processedAmount) return processedAmount;
        }
        return 0;
    }

    private static List<ILksListenerEntityListProvider> getProvidersSortedByPriority()
    {
        if(null == PROVIDERS)
        {
            PROVIDERS = (List<ILksListenerEntityListProvider>) ApplicationRuntime.getBean(ILksListenerEntityListProvider.LKS_LISTENED_ENTITY_LIST_EXTENSIONS_BEAN_NAME);
            PROVIDERS.sort((ILksListenerEntityListProvider o1, ILksListenerEntityListProvider o2) -> o1.getPriority() - o2.getPriority());
        }
        return PROVIDERS;
    }

//    public static Map<String, String> getActualEntityTypesMap()
//    {
//        Map<String, String> result = new HashMap<>();
//        for (ILksListenerEntityListProvider provider : (List<ILksListenerEntityListProvider>) ApplicationRuntime.getBean(ILksListenerEntityListProvider.LKS_LISTENED_ENTITY_LIST_EXTENSIONS_BEAN_NAME))
//        {
//            result.putAll(provider.getActualEntityTypesMap());
//        }
//        return result;
//    }
}