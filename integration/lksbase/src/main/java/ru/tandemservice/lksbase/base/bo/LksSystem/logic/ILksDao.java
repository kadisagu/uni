/* $Id:$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.lksbase.entity.LksChangedEntity;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public interface ILksDao extends ISharedBaseDao
{
    void doInitFullLksEntitySync(Collection<Long> studentIds, String entityType);

    void doRegisterChangedEntities(List<Long> entityIds, boolean delete);

    List<LksChangedEntity> getLksEntityPortion(int elementsToProcessAmount, String entityType);

    String doInitLoadLDapData(InputStream fileName);

    List<Long> getPotentiallyLargePackagesIds();

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void doRemoveLargePackages(List<Long> ids);

}