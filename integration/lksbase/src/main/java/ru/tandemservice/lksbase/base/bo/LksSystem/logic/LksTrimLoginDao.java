/* $Id$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.logic;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Специальный Dao для трима логинов
 *
 * @author Igor Belanov
 * @since 23.11.2016
 */
public class LksTrimLoginDao extends SharedBaseDao implements ILksTrimLoginDao
{
    @Override
    public Map<String, String> getChanges(boolean withUpdate)
    {
        // изменения
        Map<String, String> changes = Maps.newHashMap();

        // все логины
        Set<String> logins = new HashSet<>(new DQLSelectBuilder().fromEntity(Principal.class, "pri")
                .column(property("pri", Principal.login())).createStatement(getSession()).list());

        // логины на изменение
        Set<String> loginsForChange = logins.stream().filter(login -> !StringUtils.trim(login).equals(login)).collect(Collectors.toSet());
        Set<String> nextLoginBatch;

        int i = 0;
        while (!loginsForChange.isEmpty())
        {
            final String num = (i > 0) ? Integer.toString(i) : "";

            nextLoginBatch = loginsForChange.stream().filter(login -> !logins.contains(StringUtils.trim(login) + num)).collect(Collectors.toSet());
            for(String login : nextLoginBatch)
            {
                String newLogin = StringUtils.trim(login) + num;
                changes.put(login, newLogin);
                loginsForChange.remove(login);
                logins.remove(login);
                logins.add(newLogin);
            }

            // TODO: 25.11.16 refactor нужно таки вынести это и возвращать мапу (num -> лист принципалов (или ids))
            if (withUpdate)
            {
                DQLSelectBuilder sBuilder = new DQLSelectBuilder().fromEntity(Principal.class, "pri").column(property("pri", Principal.login()));
                DQLUpdateBuilder uBuilder = new DQLUpdateBuilder(Principal.class);
                uBuilder.set(Principal.P_LOGIN, DQLFunctions.concat(DQLFunctions.trim(property(Principal.P_LOGIN)), value(num)));
                uBuilder.where(ne(property(Principal.P_LOGIN), DQLFunctions.trim(property(Principal.P_LOGIN))));
                uBuilder.where(notIn(DQLFunctions.concat(DQLFunctions.trim(property(Principal.P_LOGIN)), value(num)), sBuilder.buildQuery()));
                int affectedRows = CommonDAO.executeAndClear(uBuilder, getSession());
                assert affectedRows == nextLoginBatch.size(); // (wtf?)
                getSession().flush();
            }

            ++i;
        }
        return changes;
    }

    @Override
    public void doTrimLogin(PrintStream logTarget)
    {
        final Map<String, String> changes = getChanges(true);
        for (String login : changes.keySet())
            logTarget.println("\"" + login + "\" -> \"" + changes.get(login) + "\"");
        logTarget.println("Logins trimming complete. " + changes.size() + " rows affected.");
    }
}

