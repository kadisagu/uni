/* $Id:$ */
package ru.tandemservice.lksbase.utils;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 11.08.2016
 */
public interface ILksListenerEntityListProvider
{
    String LKS_LISTENED_ENTITY_LIST_EXTENSIONS_BEAN_NAME = "lksListenedEntityList";

    int getPriority();

    List<Class> getClassesToListen();

    void initFullLksEntitySync(Collection<Long> studentIds, String entityType);

    int syncLksEntityPortion(int elementsToProcessAmount);

    List<CoreCollectionUtils.Pair<Long, String>> getIdToEntityTypePairList(Long entityId, boolean delete);
}