/* $Id:$ */
package ru.tandemservice.lksbase.events;

import org.hibernate.Session;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;
import ru.tandemservice.lksbase.utils.LksListenerUtil;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksEntityInsertListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        for (Class clazz : LksListenerUtil.getEntityToListenList())
        {
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, clazz, this);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, clazz, this);
        }
    }

    @Override
    public Boolean beforeCompletion(Session session, Collection<Long> params)
    {
        //if (!FefuMobileDao.AUTO_SYNC_ENABLED) return true;
        LksSystemManager.instance().dao().doRegisterChangedEntities(new ArrayList(params), false);
        return true;
    }
}