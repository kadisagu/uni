/* $Id:$ */
package ru.tandemservice.lksbase.events;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateDeleteListener;
import org.tandemframework.hibsupport.event.single.type.HibernateDeleteEvent;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;
import ru.tandemservice.lksbase.utils.LksListenerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksEntityDeleteListener extends FilteredSingleEntityEventListener<HibernateDeleteEvent> implements IHibernateDeleteListener
{
    @Override
    public void onFilteredEvent(HibernateDeleteEvent event)
    {
        IEntityMeta meta = EntityRuntime.getMeta(event.getEntity());
        List<Class> entityTypeSet = LksListenerUtil.getEntityToListenList();
        if (null != meta && entityTypeSet.contains(meta.getEntityClass()))
        {
            System.out.println(event.getEntity().getId());
            List<Long> deletedEntityList = new ArrayList<>();
            deletedEntityList.add(event.getEntity().getId());
            LksSystemManager.instance().dao().doRegisterChangedEntities(deletedEntityList, true);
        }
    }
}