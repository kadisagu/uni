/* $Id$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.logic;

import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;

import java.io.PrintStream;
import java.util.Map;

/**
 * Специальный Dao для трима логинов
 *
 * @author Igor Belanov
 * @since 23.11.2016
 */
public interface ILksTrimLoginDao extends ISharedBaseDao
{
    Map<String, String> getChanges(boolean withUpdate);

    void doTrimLogin(PrintStream logTarget);
}

