/* $Id:$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollableResults;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.lksbase.entity.LksChangedEntity;
import ru.tandemservice.lksbase.utils.LksListenerUtil;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;

import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.08.2016
 */
public class LksDao extends SharedBaseDao implements ILksDao
{
    private Map<String, Set<Long>> PREPROCESSED_ENTITY_MAP = new HashMap<>();

    @Override
    public void doInitFullLksEntitySync(Collection<Long> studentIds, String entityType)
    {
        LksListenerUtil.initFullLksEntitySync(studentIds, entityType);
    }

    @Override
    public void doRegisterChangedEntities(List<Long> entityIds, boolean delete)
    {
        /*
        // Проверяем наличие записей об изменениях в базе. Если сущность уже изменилась и есть среди записей, то нужно её убрать из этого списка.
        List<Long> existEntityId = new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e")
                .column(property(FefuChangedMobileEntity.entityId().fromAlias("e")))
                .where(in(property(FefuChangedMobileEntity.entityId().fromAlias("e")), entityIds))
                .createStatement(getSession()).list();

        entityIds.removeAll(existEntityId); */

        Map<String, Set<Long>> localEntityMap = new HashMap<>();
        Set<Long> correctedEntityIds = new HashSet<>();

        for (Long id : entityIds)
        {
            List<CoreCollectionUtils.Pair<Long, String>> actualIdToTypePairsList = LksListenerUtil.getActualTypedEntityList(id, delete);

            if (null != actualIdToTypePairsList)
            {
                for (CoreCollectionUtils.Pair<Long, String> pair : actualIdToTypePairsList)
                {
                    //if (delete || !PREPROCESSED_ENTITY_MAP.containsKey(pair.getY()) || !PREPROCESSED_ENTITY_MAP.get(pair.getY()).contains(pair.getX()))
                    if (null != pair.getY())
                    {
                        Set<Long> typedEntityIdsList = localEntityMap.get(pair.getY());
                        if (null == typedEntityIdsList) typedEntityIdsList = new HashSet<>();
                        typedEntityIdsList.add(pair.getX());
                        localEntityMap.put(pair.getY(), typedEntityIdsList);
                        correctedEntityIds.add(pair.getX());
                    }
                }
            }
        }

        // Проверяем наличие записей об изменениях в базе, с учетом связанных объектов. Если сущность уже изменилась и есть среди записей, то нужно её убрать из этого списка.
        DQLSelectBuilder deletedExistEntityBuilder = new DQLSelectBuilder().fromEntity(LksChangedEntity.class, "e")
                .column(property(LksChangedEntity.entityId().fromAlias("e")))
                .where(in(property(LksChangedEntity.entityId().fromAlias("e")), correctedEntityIds));

        if (delete)
            deletedExistEntityBuilder.where(eq(property(LksChangedEntity.deleted().fromAlias("e")), value(Boolean.TRUE)));

        List<Long> existEntityId = deletedExistEntityBuilder.createStatement(getSession()).list();

        if (delete)
        {
            List<LksChangedEntity> updatedEntityList = new DQLSelectBuilder().fromEntity(LksChangedEntity.class, "e").column(property("e"))
                    .where(in(property(LksChangedEntity.entityId().fromAlias("e")), correctedEntityIds))
                    .where(eq(property(LksChangedEntity.deleted().fromAlias("e")), value(Boolean.FALSE)))
                    .createStatement(getSession()).list();

            for (LksChangedEntity changedMobileEntity : updatedEntityList)
            {
                changedMobileEntity.setDeleted(true);
                existEntityId.add(changedMobileEntity.getEntityId());
            }
        }

        if (localEntityMap.entrySet().size() > 0)
        {
            int updCnt = 0;
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);
            for (Map.Entry<String, Set<Long>> entry : localEntityMap.entrySet())
            {
                Set<Long> globalSet = PREPROCESSED_ENTITY_MAP.get(entry.getKey());
                if (null == globalSet) globalSet = new HashSet<>();

                for (Long id : entry.getValue())
                {
                    if (!existEntityId.contains(id) && null != entry.getKey())
                    {
                        insertBuilder.value(LksChangedEntity.entityId().s(), id);
                        insertBuilder.value(LksChangedEntity.entityType().s(), entry.getKey());
                        insertBuilder.value(LksChangedEntity.deleted().s(), delete);
                        insertBuilder.addBatch();
                        globalSet.add(id);
                        updCnt++;
                    }
                    if (updCnt == 300)
                    {
                        createStatement(insertBuilder).execute();
                        insertBuilder = new DQLInsertValuesBuilder(LksChangedEntity.class);
                        updCnt = 0;
                    }
                }
                PREPROCESSED_ENTITY_MAP.put(entry.getKey(), globalSet);
            }

            if (updCnt > 0) createStatement(insertBuilder).execute();
        }
    }

    @Override
    public List<LksChangedEntity> getLksEntityPortion(int elementsToProcessAmount, String entityType)
    {
        return new DQLSelectBuilder().fromEntity(LksChangedEntity.class, "e").column(property("e"))
                .where(eq(property(LksChangedEntity.entityType().fromAlias("e")), value(entityType)))
                .top(elementsToProcessAmount).createStatement(getSession()).list();
    }

    @Override
    public String doInitLoadLDapData(InputStream file)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            SQLQuery sqlQuery = getSession().createSQLQuery("select pri.id, ic.lastname_p, ic.firstname_p, ic.middlename_p, g.title_p, per.contactdata_id " +
                    "FROM principal_t pri, " +
                    "personrole_t pr, " +
                    "student_t s, " +
                    "person_t per, " +
                    "identityCard_t ic, " +
                    "group_t g " +
                    "WHERE pr.principal_id=pri.id " +
                    "AND s.id=pr.id " +
                    "AND per.id=pr.person_id " +
                    "AND ic.id=per.identitycard_id " +
                    "AND g.id=s.group_id");

            ScrollableResults resultSet = sqlQuery.scroll();
            List<PrincipalInfo> principalInfoList = new ArrayList<>();
            while (resultSet.next())
            {
                Object[] row = resultSet.get();
                Long principalId = ((BigInteger)row[0]).longValue();
                String lastName = StringUtils.trimToNull((String)row[1]);
                String firstName = StringUtils.trimToNull((String)row[2]);
                String middleName = StringUtils.trimToNull((String)row[3]);
                String groupTitle = StringUtils.trimToNull((String)row[4]);
                Long contactDataId = ((BigInteger)row[5]).longValue();;
                principalInfoList.add(new PrincipalInfo(principalId, lastName, firstName, middleName, groupTitle, contactDataId));
            }

            String csvSplitSym = ",";

            BufferedReader bufferedReader;
            // прочитаем прямо из файла данные, для импорта данных в базу
            bufferedReader = new BufferedReader(new InputStreamReader(file));

            log(sb, "============= импорт логинов и email'ов из LDAP ============= ");

            int count = 0;
            int totalCount = 0;
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                try
                {
                    // возьмём значения из csv (убирая кавычки по бокам (если они есть))
                    String[] splittedLine = line.split(csvSplitSym);
                    String lastName     = StringUtils.trimToNull(splittedLine[3].trim().replaceAll("(^\")|(\"$)", ""));
                    String firstname    = StringUtils.trimToNull(splittedLine[2].trim().replaceAll("(^\")|(\"$)", ""));
                    String middleName   = StringUtils.trimToNull(splittedLine[4].trim().replaceAll("(^\")|(\"$)", ""));
                    String groupTitle   = StringUtils.trimToNull(splittedLine[6].trim().replaceAll("(^\")|(\"$)", ""));
                    String email        = StringUtils.trimToNull(splittedLine[8].trim().replaceAll("(^\")|(\"$)", ""));
                    String username     = StringUtils.trimToNull(splittedLine[9].trim().replaceAll("(^\")|(\"$)", ""));
                    String password     = StringUtils.trimToNull(splittedLine[13].trim().replaceAll("(^\")|(\"$)", ""));
                    String salt         = StringUtils.trimToNull(splittedLine[14]);
                    if ("\"\"".equals(salt)) {
                        salt = null;
                    } else {
                        salt = salt.replaceAll("(^\")|(\"$)", "");
                    }


                    List<PrincipalInfo> result = principalInfoList.stream()
                            .filter(item -> isEqualStrings(item.getLastName(), lastName) &&
                                    isEqualStrings(item.getFirstName(), firstname) &&
                                    isEqualStrings(item.getMiddleName(), middleName)
                            )
                            .collect(Collectors.toList());

                    if (result.isEmpty())
                    {
                        log(sb, "Студент \"" +
                                lastName + " " + firstname + " " + middleName + " (группа: " + groupTitle + ")\" не найден в базе");
                    }
                    // если мы нашли более одной записи с разными принципалами, то попробуем отфильтровать по группам
                    else if (result.size() > 1 && result.stream().collect(Collectors.groupingBy(PrincipalInfo::getPrincipalId)).size() > 1)
                    {
                        List<PrincipalInfo> withGroups = result.stream()
                                .filter(item -> isEqualGroupStrings(item.getGroupVariants(), item.getVariants(groupTitle)))
                                .collect(Collectors.toList());

                        // либо мы нашли одну запись, либо нашли больше, но принципал теперь один
                        if (withGroups.size() == 1 || (withGroups.size() > 1 &&
                                withGroups.stream().collect(Collectors.groupingBy(PrincipalInfo::getPrincipalId)).size() == 1))
                        {
                            count = updatePrincipal(count, email, username, password, salt, withGroups);
                        }
                        else
                        {
                            String groupList = result.stream().map(PrincipalInfo::getGroupTitle).collect(Collectors.joining(", "));
                            log(sb, "В базе найдено несколько (" + result.size() + ") студентов \"" +
                                    lastName + " " + firstname + " " + middleName + "\" в группах " + groupList);
                        }
                    }
                    else
                    {
                        count = updatePrincipal(count, email, username, password, salt, result);
                    }
                } catch (Exception ex)
                {
                    log(sb, "Строка обработана с ошибкой: \"" + line + "\"");
                    logError(sb, ex);
                }
                totalCount++;
            }

//            principalUsernameUpdater.executeUpdate(dbTool);
//            contactDataEmailUpdater.executeUpdate(dbTool);

            log(sb, "Обновлены данные студентов: " + count + " из " + totalCount);
            log(sb, "========================== Успех ==========================");
        } catch (Exception ex) {
            logError(sb, ex);
        }
        return sb.toString();
    }

    @Override
    public List<Long> getPotentiallyLargePackagesIds()
    {
        return new DQLSelectBuilder().fromEntity(NsiQueue.class, "q")
                .joinPath(DQLJoinType.inner, NsiQueue.log().fromAlias("q"), "log")
                .joinPath(DQLJoinType.inner, NsiSubSystemLog.pack().fromAlias("log"), "pack")
                .where(exists
                        (new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e_log")
                                .where(eq(property(NsiEntityLog.pack().id().fromAlias("e_log")), property(NsiPackage.id().fromAlias("pack"))))
                                .where(or(
                                        eq(property(NsiEntityLog.catalogType().fromAlias("e_log")), value("RegistryDisciplineType")),
                                        eq(property(NsiEntityLog.catalogType().fromAlias("e_log")), value("LksStudentPortfolioElementType"))
                                ))
                                .buildQuery()
                        )
                )
                .order(property(NsiQueue.createDate().fromAlias("q")))
                .column(property(NsiQueue.id().fromAlias("q")))
                .createStatement(getSession()).list();
    }

    @Override
    public void doRemoveLargePackages(List<Long> ids)
    {
        DQLSelectBuilder selectPackages = new DQLSelectBuilder().fromEntity(NsiQueue.class, "q")
                .joinPath(DQLJoinType.inner, NsiQueue.log().fromAlias("q"), "log")
                .joinPath(DQLJoinType.inner, NsiSubSystemLog.pack().fromAlias("log"), "pack")
                .where(exists
                        (new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e_log")
                                .where(eq(property(NsiEntityLog.pack().id().fromAlias("e_log")), property(NsiPackage.id().fromAlias("pack"))))
                                .where(or(
                                        eq(property(NsiEntityLog.catalogType().fromAlias("e_log")), value("RegistryDisciplineType")),
                                        eq(property(NsiEntityLog.catalogType().fromAlias("e_log")), value("LksStudentPortfolioElementType"))
                                ))
                                .buildQuery()
                        )
                )
                .column(property(NsiQueue.id().fromAlias("q")))
                .column(property(NsiPackage.body().fromAlias("pack")))
                .where(in(property(NsiQueue.id().fromAlias("q")), ids));

        List<Object[]> rows = selectPackages.createStatement(getSession()).list();
        List<Long> idsToDelete = new ArrayList<>();
        for (Object[] row : rows)
        {
            String body = (String)row[1];
            if (body.getBytes().length > 5 * 1024 * 1024)
                idsToDelete.add((Long)row[0]);
        }

        new DQLDeleteBuilder(NsiQueue.class)
                .where(in(property(NsiQueue.id()), idsToDelete))
                .createStatement(getSession())
                .execute();
    }

    private int updatePrincipal(int count, String email, String username, String password, String salt, List<PrincipalInfo> result)
    {
        PrincipalInfo principalInfo = result.get(0);

        // обновим логин
        Principal principal = get(Principal.class, principalInfo.getPrincipalId());
        principal.setLogin(username);
        principal.setPasswordHash(password);
        principal.setPasswordSalt(salt);
        getSession().saveOrUpdate(principal);
        getSession().flush();


        // contactDataId - not null, поэтому можно сразу обновлять email
        PersonContactData contactData = get(PersonContactData.class, principalInfo.getContactDataId());
        contactData.setEmail(email);
        getSession().saveOrUpdate(contactData);
        getSession().flush();

        return ++count;
    }

    private void logError(StringBuilder sb, Exception ex)
    {
        ex.printStackTrace();
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        PrintStream print = new PrintStream(buff);
        ex.printStackTrace(print);
        print.flush();
        sb.append(buff);
    }

    private void log(StringBuilder sb, String msg)
    {
        System.out.println(msg);
        sb.append(msg + "\n");
    }

    private boolean isEqualStrings(String val1, String val2)
    {
        String val1Trimmed = StringUtils.trimToNull(val1);
        String val2Trimmed = StringUtils.trimToNull(val2);

        if (null == val1Trimmed && null == val2Trimmed) return true;
        if (null != val1Trimmed && null != val2Trimmed && val1Trimmed.equalsIgnoreCase(val2Trimmed)) return true;
        return false;
    }

    private boolean isEqualGroupStrings(List<String> val1, List<String> val2)
    {
        if ((null == val1 || val1.isEmpty()) && (null == val2 || val2.isEmpty())) return true;
        if ((null == val1 || val1.isEmpty()) || (null == val2 || val2.isEmpty())) return false;
        for (String val : val1) if (val2.contains(val)) return true;
        return false;
    }
    /**
     * имя, фамилия, отчество и группа принципала
     */
    private class PrincipalInfo
    {
        private final Long _principalId;
        private final String _lastName;
        private final String _firstName;
        private final String _middleName;
        private final String _groupTitle;
        private final Long _contactDataId;
        private List<String> _groupVariants;

        public PrincipalInfo(Long principalId, String lastName, String firstName, String middleName, String groupTitle, Long contactDataId)
        {
            this._principalId = principalId;
            this._lastName = lastName;
            this._firstName = firstName;
            this._middleName = middleName;
            this._groupTitle = groupTitle;
            this._contactDataId = contactDataId;

            _groupVariants = getVariants(_groupTitle);
        }

        public List<String> getVariants(String groupTitle)
        {
            List<String> groupVariants = new ArrayList<>();
            if (null != groupTitle)
            {
                String[] splittedGroupTitle = groupTitle.split("-");

                for (String segment : splittedGroupTitle)
                {
                    try
                    {
                        int intVal = Integer.parseInt(segment);
                        List<String> toAppend = new ArrayList<>();
                        for (int i = 0; i < groupVariants.size(); i++)
                        {
                            String part = groupVariants.get(i) + "-";
                            groupVariants.set(i, part + intVal);
                            toAppend.add(part + "0" + intVal);
                        }
                        groupVariants.addAll(toAppend);

                    } catch (NumberFormatException ex)
                    {
                        if (groupVariants.isEmpty()) groupVariants.add(segment);
                        else
                        {
                            for (int i = 0; i < groupVariants.size(); i++)
                            {
                                groupVariants.set(i, groupVariants.get(i) + "-" + segment);
                            }
                        }
                    }
                }
            }
            return groupVariants;
        }

        public Long getPrincipalId()
        {
            return _principalId;
        }

        public String getLastName()
        {
            return _lastName;
        }

        public String getFirstName()
        {
            return _firstName;
        }

        public String getMiddleName()
        {
            return _middleName;
        }

        public String getGroupTitle()
        {
            return _groupTitle;
        }

        public Long getContactDataId()
        {
            return _contactDataId;
        }

        public List<String> getGroupVariants()
        {
            return _groupVariants;
        }

        public void setGroupVariants(List<String> groupVariants)
        {
            _groupVariants = groupVariants;
        }
    }
}
