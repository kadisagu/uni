/* $Id:$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.logic;

import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;

/**
 * @author Dmitry Seleznev
 * @since 15.08.2016
 */
public interface ILksDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = ILksDaemonDao.class.getName() + ".global-lock";
}