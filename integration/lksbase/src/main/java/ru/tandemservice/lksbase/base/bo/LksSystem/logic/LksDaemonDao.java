/* $Id:$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.logic;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.*;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.lksbase.utils.LksListenerUtil;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 15.08.2016
 */
public class LksDaemonDao extends SharedBaseDao implements ILksDaemonDao
{
    public static final int DAEMON_ITERATION_TIME = 1;  //TODO
    private static boolean sync_locked = false;

    protected static final Logger log4j_logger = Logger.getLogger(LksDaemonDao.class);

    public static void logEvent(Level loggingLevel, String message)
    {
        // проверяем, есть ли уже appender
        Appender appender = log4j_logger.getAppender("LksSyncAppender");

        if (null != appender) log4j_logger.log(loggingLevel, message);

        if (null == appender)
        {
            try
            {
                // добавляем, если нет
                final String path = ApplicationRuntime.getAppInstallPath();
                Calendar cal = CoreDateUtils.createCalendar(new Date());
                String logFileName = "LksSync_" + cal.get(Calendar.YEAR) + "_" + (cal.get(Calendar.MONTH) + 1) + "_" + cal.get(Calendar.DAY_OF_MONTH) + ".log";
                try
                {
                    appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
                } catch (IOException ex)
                {

                }

                appender.setName("LksSyncAppender");
                ((FileAppender) appender).setThreshold(Level.INFO);
                log4j_logger.addAppender(appender);
                log4j_logger.setLevel(Level.INFO);

                if (null != appender)
                {
                    log4j_logger.log(loggingLevel, message);
                }

            } finally
            {
                // и отцепляем, если добавляли
                if (null != appender)
                {
                    log4j_logger.removeAppender(appender);
                }
            }
        }
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(LksDaemonDao.class.getName(), DAEMON_ITERATION_TIME, ILksDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked) return;
            sync_locked = true;

            try
            {
                int lastIterationProcessedAmount = -1;
                long startTime = System.currentTimeMillis();
                while (System.currentTimeMillis() - startTime < DAEMON_ITERATION_TIME * 60000 && 0 != lastIterationProcessedAmount)
                {
                    lastIterationProcessedAmount = LksListenerUtil.syncLksEntityPortion();
                    Thread.sleep(2000);
                }

            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
                t.printStackTrace();
            }

            sync_locked = false;
        }
    };
}