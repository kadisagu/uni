/* $Id$ */
package ru.tandemservice.lksbase.base.bo.LksSystem.ui.TrimLogin;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Igor Belanov
 * @since 24.11.2016
 */
public class LksSystemTrimLoginUI extends UIPresenter
{
    private String _log;
    private boolean _isNothingToChange = false;

    @Override
    public void onComponentRefresh()
    {
        Map<String, String> changes = LksSystemManager.instance().trimDao().getChanges(false);
        if (changes.isEmpty()) setNothingToChange(true);
        StringBuilder sb = new StringBuilder();
        Iterator<String> iter = changes.keySet().stream().sorted().iterator();
        while(iter.hasNext())
        {
            String login = iter.next();
            sb.append("Логин \"").append(login).append("\" будет изменён на \"").append(changes.get(login)).append("\"");
            if (iter.hasNext()) sb.append("\n");
        }
        setLog(sb.toString());
    }

    public void onClickApply() throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        LksSystemManager.instance().trimDao().doTrimLogin(ps);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().txt().fileName("changedLogins.txt").document(baos), true);
        baos.close();
        ps.close();

        deactivate();
    }

    // getters & setters
    public String getLog()
    {
        return _log;
    }

    public void setLog(String log)
    {
        _log = log;
    }

    public boolean isNothingToChange()
    {
        return _isNothingToChange;
    }

    public void setNothingToChange(boolean nothingToChange)
    {
        _isNothingToChange = nothingToChange;
    }
}

