package ru.tandemservice.lksbase.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.lksbase.entity.LksChangedEntity;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Буфер для накопления ссылок на измененные объекты для последующей отправки в ЛКС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LksChangedEntityGen extends EntityBase
 implements INaturalIdentifiable<LksChangedEntityGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.lksbase.entity.LksChangedEntity";
    public static final String ENTITY_NAME = "lksChangedEntity";
    public static final int VERSION_HASH = 148607374;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_ENTITY_TYPE = "entityType";
    public static final String P_DELETED = "deleted";

    private long _entityId;     // Идентификатор добавленной/измененной сущности
    private String _entityType;     // Тип сущности УНИ
    private boolean _deleted;     // Удалён

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор добавленной/измененной сущности. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор добавленной/измененной сущности. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип сущности УНИ. Свойство не может быть null.
     */
    public void setEntityType(String entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Удалён. Свойство не может быть null.
     */
    @NotNull
    public boolean isDeleted()
    {
        return _deleted;
    }

    /**
     * @param deleted Удалён. Свойство не может быть null.
     */
    public void setDeleted(boolean deleted)
    {
        dirty(_deleted, deleted);
        _deleted = deleted;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LksChangedEntityGen)
        {
            if (withNaturalIdProperties)
            {
                setEntityId(((LksChangedEntity)another).getEntityId());
                setEntityType(((LksChangedEntity)another).getEntityType());
            }
            setDeleted(((LksChangedEntity)another).isDeleted());
        }
    }

    public INaturalId<LksChangedEntityGen> getNaturalId()
    {
        return new NaturalId(getEntityId(), getEntityType());
    }

    public static class NaturalId extends NaturalIdBase<LksChangedEntityGen>
    {
        private static final String PROXY_NAME = "LksChangedEntityNaturalProxy";

        private long _entityId;
        private String _entityType;

        public NaturalId()
        {}

        public NaturalId(long entityId, String entityType)
        {
            _entityId = entityId;
            _entityType = entityType;
        }

        public long getEntityId()
        {
            return _entityId;
        }

        public void setEntityId(long entityId)
        {
            _entityId = entityId;
        }

        public String getEntityType()
        {
            return _entityType;
        }

        public void setEntityType(String entityType)
        {
            _entityType = entityType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof LksChangedEntityGen.NaturalId) ) return false;

            LksChangedEntityGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntityId(), that.getEntityId()) ) return false;
            if( !equals(getEntityType(), that.getEntityType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntityId());
            result = hashCode(result, getEntityType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntityId());
            sb.append("/");
            sb.append(getEntityType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LksChangedEntityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LksChangedEntity.class;
        }

        public T newInstance()
        {
            return (T) new LksChangedEntity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "entityType":
                    return obj.getEntityType();
                case "deleted":
                    return obj.isDeleted();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "entityType":
                    obj.setEntityType((String) value);
                    return;
                case "deleted":
                    obj.setDeleted((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "entityType":
                        return true;
                case "deleted":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "entityType":
                    return true;
                case "deleted":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "entityType":
                    return String.class;
                case "deleted":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LksChangedEntity> _dslPath = new Path<LksChangedEntity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LksChangedEntity");
    }
            

    /**
     * @return Идентификатор добавленной/измененной сущности. Свойство не может быть null.
     * @see ru.tandemservice.lksbase.entity.LksChangedEntity#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.lksbase.entity.LksChangedEntity#getEntityType()
     */
    public static PropertyPath<String> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Удалён. Свойство не может быть null.
     * @see ru.tandemservice.lksbase.entity.LksChangedEntity#isDeleted()
     */
    public static PropertyPath<Boolean> deleted()
    {
        return _dslPath.deleted();
    }

    public static class Path<E extends LksChangedEntity> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _entityType;
        private PropertyPath<Boolean> _deleted;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор добавленной/измененной сущности. Свойство не может быть null.
     * @see ru.tandemservice.lksbase.entity.LksChangedEntity#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(LksChangedEntityGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.lksbase.entity.LksChangedEntity#getEntityType()
     */
        public PropertyPath<String> entityType()
        {
            if(_entityType == null )
                _entityType = new PropertyPath<String>(LksChangedEntityGen.P_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Удалён. Свойство не может быть null.
     * @see ru.tandemservice.lksbase.entity.LksChangedEntity#isDeleted()
     */
        public PropertyPath<Boolean> deleted()
        {
            if(_deleted == null )
                _deleted = new PropertyPath<Boolean>(LksChangedEntityGen.P_DELETED, this);
            return _deleted;
        }

        public Class getEntityClass()
        {
            return LksChangedEntity.class;
        }

        public String getEntityName()
        {
            return "lksChangedEntity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
