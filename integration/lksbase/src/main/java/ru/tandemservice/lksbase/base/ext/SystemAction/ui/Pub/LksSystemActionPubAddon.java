/* $Id:$ */
package ru.tandemservice.lksbase.base.ext.SystemAction.ui.Pub;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.lksbase.base.bo.LksSystem.LksSystemManager;
import ru.tandemservice.lksbase.base.bo.LksSystem.ui.LoadLDAP.LksSystemLoadLDAP;
import ru.tandemservice.lksbase.base.bo.LksSystem.ui.TrimLogin.LksSystemTrimLogin;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 13.08.2016
 */
public class LksSystemActionPubAddon extends UIAddon
{
    public LksSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickInitFullSync()
    {
        LksSystemManager.instance().dao().doInitFullLksEntitySync(null, null);
    }

    public void onClickInitEduPlanWeekPeriodsSync()
    {
        LksSystemManager.instance().dao().doInitFullLksEntitySync(null, "eduPlanWeekPeriod");
    }

    public void onClickInitSessionResultSync()
    {
        LksSystemManager.instance().dao().doInitFullLksEntitySync(null, "result");
    }

    public void onClickInitSubjectSync()
    {
        LksSystemManager.instance().dao().doInitFullLksEntitySync(null, "subject");
    }

    public void onClickInitLoadLDapData()
    {
        this.getActivationBuilder().asRegionDialog(LksSystemLoadLDAP.class)
                .activate();
    }

    public void onClickTrimLogin()
    {
        getActivationBuilder().asRegionDialog(LksSystemTrimLogin.class)
                .activate();
    }

    public void onClickRemoveLargePackages()
    {
        List<Long> ids = LksSystemManager.instance().dao().getPotentiallyLargePackagesIds();
        for (List<Long> part : Lists.partition(ids, 10))
        {
            LksSystemManager.instance().dao().doRemoveLargePackages(part);
        }
    }
}