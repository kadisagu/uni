/* $Id:$ */
package ru.tandemservice.lksbase.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.lksbase.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Dmitry Seleznev
 * @since 13.08.2016
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("lks_removeLargePackagesFromQueue", new SystemActionDefinition("lksbase", "removeLargePackagesFromQueue", "onClickRemoveLargePackages", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_initFullSync", new SystemActionDefinition("lksbase", "initFullSync", "onClickInitFullSync", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_initEduPlanWeekPeriodsSync", new SystemActionDefinition("lksbase", "initEduPlanWeekPeriodsSync", "onClickInitEduPlanWeekPeriodsSync", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_initSessionResultSync", new SystemActionDefinition("lksbase", "initSessionResultSync", "onClickInitSessionResultSync", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_initSubjectSync", new SystemActionDefinition("lksbase", "initSubjectSync", "onClickInitSubjectSync", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_initLoadLDapData", new SystemActionDefinition("lksbase", "initLoadLDapData", "onClickInitLoadLDapData", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("lks_doTrimLogin", new SystemActionDefinition("lksbase", "doTrimLogin", "onClickTrimLogin", SystemActionPubExt.LKS_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}