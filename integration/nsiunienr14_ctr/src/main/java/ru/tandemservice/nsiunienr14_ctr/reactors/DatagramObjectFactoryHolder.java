/* $Id: DatagramObjectFactoryHolder.java 259 2015-08-20 15:45:44Z anikonov $ */
package ru.tandemservice.nsiunienr14_ctr.reactors;

/**
 * @author Dmitry Seleznev
 * @since 14.07.2015
 */
public class DatagramObjectFactoryHolder
{
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
}