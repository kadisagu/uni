package ru.tandemservice.nsiunienr14_ctr.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.codes.CtrContractTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContactPersonType;
import ru.tandemservice.nsiclient.datagram.EduCtrContractVersionTemplateDataType;
import ru.tandemservice.nsiclient.datagram.EnrRequestedCompetitionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;

import java.util.*;

public class EduCtrContractVersionTemplateDataTypeReactor extends BaseEntityReactor<CtrContractVersionTemplateData, EduCtrContractVersionTemplateDataType>
{
    private static final String CONTRACTOR = "ContractorID";
    private static final String COMPETITION = "EnrRequestedCompetitionID";
    private static final String VERSION = "version";
    private static final String VERSION_CONTRACTOR = "versionContractor";
    private static final String COST = "cost";

    @Override
    public Class<CtrContractVersionTemplateData> getEntityClass() {
        return CtrContractVersionTemplateData.class;
    }

    @Override
    public EduCtrContractVersionTemplateDataType createDatagramObject(String guid)
    {
        EduCtrContractVersionTemplateDataType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrContractVersionTemplateDataType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(CtrContractVersionTemplateData entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduCtrContractVersionTemplateDataType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrContractVersionTemplateDataType();
        datagramObject.setID(nsiEntity.getGuid());
        if(entity instanceof EduCtrContractVersionTemplateData) {
            CtrPriceElementCost cost = ((EduCtrContractVersionTemplateData) entity).getCost();
            if (cost != null) datagramObject.setEduCtrContractVersionTemplateDataCost(cost.getCostAsCurrencyString());
        }
        datagramObject.setEduCtrContractVersionTemplateDataDurationBeginDate(NsiUtils.formatDate(entity.getOwner().getDurationBeginDate()));
        datagramObject.setEduCtrContractVersionTemplateDataDurationEndDate(NsiUtils.formatDate(entity.getOwner().getDurationEndDate()));
        datagramObject.setEduCtrContractVersionTemplateDataContractRegistrationDate(NsiUtils.formatDate(entity.getOwner().getContractRegistrationDate()));
        datagramObject.setEduCtrContractVersionTemplateDataNumber(entity.getOwner().getNumber());

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);
        StringBuilder warning = new StringBuilder();

        List<CtrContractVersionContractor> contractors = CtrContractVersionManager.instance().dao().getContractors(entity.getOwner());
        if(contractors != null && !contractors.isEmpty()) {
            ProcessedDatagramObject<ContactPersonType> r = createDatagramObject(ContactPersonType.class, contractors.get(0).getContactor(), commonCache, warning);
            EduCtrContractVersionTemplateDataType.ContactPersonID contractorID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrContractVersionTemplateDataTypeContactorPersonID();
            contractorID.setContactPerson(r.getObject());
            datagramObject.setContactPersonID(contractorID);
            if (r.getList() != null)
                result.addAll(r.getList());
        }

        if(entity instanceof EnrContractTemplateData) {
            ProcessedDatagramObject<EnrRequestedCompetitionType> r2 = createDatagramObject(EnrRequestedCompetitionType.class, ((EnrContractTemplateData) entity).getRequestedCompetition(), commonCache, warning);
            EduCtrContractVersionTemplateDataType.EnrRequestedCompetitionID competitionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrContractVersionTemplateDataTypeEnrRequestedCompetitionID();
            competitionID.setEnrRequestedCompetition(r2.getObject());
            datagramObject.setEnrRequestedCompetitionID(competitionID);
            if (r2.getList() != null)
                result.addAll(r2.getList());
        }

        return new ResultListWrapper(null, result);
    }

    @Override
    public void collectUnknownDatagrams(EduCtrContractVersionTemplateDataType datagramObject, IUnknownDatagramsCollector collector) {
        EduCtrContractVersionTemplateDataType.ContactPersonID contractorID = datagramObject.getContactPersonID();
        ContactPersonType contractor = contractorID == null ? null : contractorID.getContactPerson();
        if(contractor != null)
            collector.check(contractor.getID(), ContactPersonType.class);

        EduCtrContractVersionTemplateDataType.EnrRequestedCompetitionID competitionID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType competitionType = competitionID == null ? null : competitionID.getEnrRequestedCompetition();
        if(competitionType != null)
            collector.check(competitionType.getID(), EnrRequestedCompetitionType.class);
    }

    private boolean hasContactor(CtrContractVersion version, ContactorPerson contactor) {
        List<CtrContractVersionContractor> contractors = CtrContractVersionManager.instance().dao().getContractors(version);
        for(CtrContractVersionContractor c: contractors)
        if(c.getContactor().equals(contactor))
            return true;
        return false;
    }

    protected CtrContractVersionTemplateData findEnrContractTemplateData(String number, ContactorPerson contactor, EnrRequestedCompetition competition) {
        return null;
    }

    @Override
    public ChangedWrapper<CtrContractVersionTemplateData> processDatagramObject(EduCtrContractVersionTemplateDataType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        // EduCtrContractVersionTemplateDataCost init
        String cost = StringUtils.trimToNull(datagramObject.getEduCtrContractVersionTemplateDataCost());
        // EduCtrContractVersionTemplateDataDurationBeginDate init
        Date beginDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getEduCtrContractVersionTemplateDataDurationBeginDate()), "eduCtrContractVersionTemplateDataDurationBeginDate");
        // EduCtrContractVersionTemplateDataDurationEndDate init
        Date endDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getEduCtrContractVersionTemplateDataDurationEndDate()), "eduCtrContractVersionTemplateDataDurationEndDate");
        // EduCtrContractVersionTemplateDataContractRegistrationDate init
        Date registrationDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getEduCtrContractVersionTemplateDataContractRegistrationDate()), "eduCtrContractVersionTemplateDataContractRegistrationDate");
        // EduCtrContractVersionTemplateDataNumber init
        String number = StringUtils.trimToNull(datagramObject.getEduCtrContractVersionTemplateDataNumber());

        // ContactorPersonID init
        EduCtrContractVersionTemplateDataType.ContactPersonID contractorID = datagramObject.getContactPersonID();
        ContactPersonType contractorType = contractorID == null ? null : contractorID.getContactPerson();
        ContactorPerson contactor = getOrCreate(ContactPersonType.class, params, commonCache, contractorType, CONTRACTOR, null, warning);

        // EnrRequestedCompetitionID init
        EduCtrContractVersionTemplateDataType.EnrRequestedCompetitionID competitionID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType competitionType = competitionID == null ? null : competitionID.getEnrRequestedCompetition();
        EnrRequestedCompetition competition = getOrCreate(EnrRequestedCompetitionType.class, params, commonCache, competitionType, COMPETITION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        boolean isCostChanged = false;
        boolean isVersionChanged = false;
        CtrContractVersionTemplateData entity = null;
        CtrPriceElementCost elementCost = null;
        CtrContractVersion version;
        CoreCollectionUtils.Pair<CtrContractVersionTemplateData, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findEnrContractTemplateData(number, contactor, competition);
        if(entity == null)
        {
            entity = new EnrContractTemplateDataSimple();
            ((EnrContractTemplateDataSimple) entity).setEducationYear(EducationYearManager.instance().dao().getCurrent());

            version = new CtrContractVersion();
            String numberStr = "nsi";

            CtrContractObject contractObject = new CtrContractObject();
            contractObject.setType(DataAccessServices.dao().getByCode(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE));
            contractObject.setOrgUnit(TopOrgUnit.getInstance());
            contractObject.setNumber(numberStr);

            version.setDurationBeginDate(new Date());
            version.setContract(contractObject);
            version.setCreationDate(new Date());
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Admin.class, "e").top(1);
            List<Admin> admins = DataAccessServices.dao().getList(builder);
            if(!admins.isEmpty())
                version.setCreator(admins.get(0));
            version.setNumber(numberStr);
            version.setDurationBeginDate(new Date());
            entity.setOwner(version);


//            CtrPriceElementPeriod period = new CtrPriceElementPeriod();
//            period.setDurationBegin(new Date());
//            ICtrPriceElement priceElement = null;
//            period.setElement(priceElement);
//            elementCost.setPeriod(period);
//
//            CtrPricePaymentGrid paymentGrid = DataAccessServices.dao().getByCode(CtrPricePaymentGrid.class, CtrPricePaymentGridCodes.EPP_PERIOD);
//            elementCost.setPaymentGrid(paymentGrid);
//            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CtrPriceCategory.class, "e").top(1);
//            List<CtrPriceCategory> categories = DataAccessServices.dao().getList(builder);
//            if(!categories.isEmpty())
//                elementCost.setCategory(categories.get(0));
//            Currency currency = DataAccessServices.dao().getByCode(Currency.class, CurrencyCodes.RUB);
//            elementCost.setCurrency(currency);
//            elementCost.setTotalCostAsLong(0l);
//            ((EnrContractTemplateDataSimple) entity).setCost(elementCost);
            isNew = true;
            isChanged = true;
            isVersionChanged = true;
            isCostChanged = true;
        } else {
            version = entity.getOwner();
            if (entity instanceof EnrContractTemplateDataSimple) {
                elementCost = ((EnrContractTemplateDataSimple) entity).getCost();
            }
        }

        // EduCtrContractVersionTemplateDataCost set
//        if(cost != null && elementCost == null)
//            warning.append("This ContactPersonType object must not have EduCtrContractVersionTemplateDataCost.");
//        else if(cost != null && !cost.equals(elementCost.getCostTitle()))
//        {
//            isCostChanged = true;
//            elementCost.setTotalCostAsLong(null);
//        }
        // EduCtrContractVersionTemplateDataDurationBeginDate set
        if(beginDate != null && !beginDate.equals(version.getDurationBeginDate()))
        {
            isVersionChanged = true;
            version.setDurationBeginDate(beginDate);
        }
        // EduCtrContractVersionTemplateDataDurationEndDate set
        if(endDate != null && !endDate.equals(version.getDurationEndDate()))
        {
            isVersionChanged = true;
            version.setDurationEndDate(endDate);
        }
        // EduCtrContractVersionTemplateDataContractRegistrationDate set
        if(registrationDate != null && !registrationDate.equals(version.getContractRegistrationDate()))
        {
            isVersionChanged = true;
            version.setContractRegistrationDate(registrationDate);
        }
        // EduCtrContractVersionTemplateDataNumber set
         if(number != null && !number.equals(version.getNumber()))
        {
            isVersionChanged = true;
            version.setNumber(number);
        }

        // ContactorPersonID set
        if(!hasContactor(version, contactor)) {
            params.put(VERSION_CONTRACTOR, contactor);
        }

        // EnrRequestedCompetitionID set
        if (null == competition && isNew)
            throw new ProcessingDatagramObjectException("EduCtrContractVersionTemplateDataType object without EnrRequestedCompetitionID!");
        if(entity instanceof EnrContractTemplateData && competition != null && !competition.equals(((EnrContractTemplateData) entity).getRequestedCompetition()))
        {
            isChanged = true;
            ((EnrContractTemplateData) entity).setRequestedCompetition(competition);
        }
        if(isCostChanged)
            params.put(COST, elementCost);
        if(isVersionChanged)
            params.put(VERSION, version);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<CtrContractVersionTemplateData> w, EduCtrContractVersionTemplateDataType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(ContactPersonType.class, session, params, CONTRACTOR, nsiPackage);
            saveChildEntity(EnrRequestedCompetitionType.class, session, params, COMPETITION, nsiPackage);
        }

        CtrContractVersion version = (CtrContractVersion) params.get(VERSION);
        if(version != null)
            session.saveOrUpdate(version);
        CtrPriceElementCost cost = (CtrPriceElementCost) params.get(COST);
        if(cost != null)
            session.saveOrUpdate(version);

        super.save(session, w, datagramObject, nsiPackage);

        ContactorPerson versionContractor = (ContactorPerson) params.get(VERSION_CONTRACTOR);
        if(versionContractor != null) {
            CtrContractVersionContractor contractVersionContractorEmployee = new CtrContractVersionContractor();
            contractVersionContractorEmployee.setContactor(versionContractor);
            contractVersionContractorEmployee.setOwner(w.getResult().getOwner());
            contractVersionContractorEmployee.setRequireSignature(false);
            List<CtrContractVersionContractor> contractorList = new ArrayList<>();
            contractorList.add(contractVersionContractorEmployee);
            CtrContractVersionManager.instance().dao().doSaveOrUpdateContractVersion(w.getResult().getOwner(), contractorList, null);
        }
    }
}