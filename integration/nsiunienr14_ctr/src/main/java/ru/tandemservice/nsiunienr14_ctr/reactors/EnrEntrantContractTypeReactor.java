/* $Id:$ */
package ru.tandemservice.nsiunienr14_ctr.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContractObjectType;
import ru.tandemservice.nsiclient.datagram.EnrEntrantContractType;
import ru.tandemservice.nsiclient.datagram.EnrRequestedCompetitionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 25.09.2015
 */
public class EnrEntrantContractTypeReactor extends BaseEntityReactor<EnrEntrantContract, EnrEntrantContractType> {
    private static final String CONTRACT = "ContractObjectID";
    private static final String COMPETITION = "EnrRequestedCompetitionID";

    @Override
    public Class<EnrEntrantContract> getEntityClass() {
        return EnrEntrantContract.class;
    }

    @Override
    public void collectUnknownDatagrams(EnrEntrantContractType datagramObject, IUnknownDatagramsCollector collector) {
        EnrEntrantContractType.ContractObjectID contractorID = datagramObject.getContractObjectID();
        ContractObjectType contractor = contractorID == null ? null : contractorID.getContractObject();
        if(contractor != null) collector.check(contractor.getID(), ContractObjectType.class);

        EnrEntrantContractType.EnrRequestedCompetitionID competitionID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType competitionType = competitionID == null ? null : competitionID.getEnrRequestedCompetition();
        if(competitionType != null) collector.check(competitionType.getID(), EnrRequestedCompetitionType.class);
    }

    @Override
    public EnrEntrantContractType createDatagramObject(String guid) {
        EnrEntrantContractType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantContractType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EnrEntrantContract entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EnrEntrantContractType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantContractType();
        datagramObject.setID(nsiEntity.getGuid());
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.add(datagramObject);

        ProcessedDatagramObject<ContractObjectType> r = createDatagramObject(ContractObjectType.class, entity.getContractObject(), commonCache, warning);
        EnrEntrantContractType.ContractObjectID contractObjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantContractTypeContractObjectID();
        contractObjectID.setContractObject(r.getObject());
        datagramObject.setContractObjectID(contractObjectID);
        if (r.getList() != null) result.addAll(r.getList());

        ProcessedDatagramObject<EnrRequestedCompetitionType> r2 = createDatagramObject(EnrRequestedCompetitionType.class, entity.getRequestedCompetition(), commonCache, warning);
        EnrEntrantContractType.EnrRequestedCompetitionID competitionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEnrEntrantContractTypeEnrRequestedCompetitionID();
        competitionID.setEnrRequestedCompetition(r2.getObject());
        datagramObject.setEnrRequestedCompetitionID(competitionID);
        if (r2.getList() != null) result.addAll(r2.getList());

        return new ResultListWrapper(null, result);
    }

    protected EnrEntrantContract findEnrEntrantContract(CtrContractObject contactor, EnrRequestedCompetition competition) {
        if (contactor != null && competition != null)
        {
            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantContract.contractObject().id()), value(contactor.getId())));
            expressions.add(eq(property(ENTITY_ALIAS, EnrEntrantContract.requestedCompetition().id()), value(competition.getId())));
            List<EnrEntrantContract> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EnrEntrantContract> processDatagramObject(EnrEntrantContractType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        // ContactorPersonID init
        EnrEntrantContractType.ContractObjectID contractID = datagramObject.getContractObjectID();
        ContractObjectType contractObjectType = contractID == null ? null : contractID.getContractObject();
        CtrContractObject contactor = getOrCreate(ContractObjectType.class, params, commonCache, contractObjectType, CONTRACT, null, warning);

        // EnrRequestedCompetitionID init
        EnrEntrantContractType.EnrRequestedCompetitionID competitionID = datagramObject.getEnrRequestedCompetitionID();
        EnrRequestedCompetitionType competitionType = competitionID == null ? null : competitionID.getEnrRequestedCompetition();
        EnrRequestedCompetition competition = getOrCreate(EnrRequestedCompetitionType.class, params, commonCache, competitionType, COMPETITION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EnrEntrantContract entity = null;
        CoreCollectionUtils.Pair<EnrEntrantContract, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findEnrEntrantContract(contactor, competition);
        if(entity == null)
        {
            entity = new EnrEntrantContract();
            isNew = true;
            isChanged = true;
        }

        if(contactor != null && !contactor.equals(entity.getContractObject()))
        {
            isChanged = true;
            entity.setContractObject(contactor);
        }
        if (null == entity.getContractObject())
            throw new ProcessingDatagramObjectException("EnrEntrantContractType object without ContractObjectID!");

        if(competition != null && !competition.equals(entity.getRequestedCompetition()))
        {
            isChanged = true;
            entity.setRequestedCompetition(competition);
        }
        if (null == entity.getRequestedCompetition())
            throw new ProcessingDatagramObjectException("EnrEntrantContractType object without EnrRequestedCompetitionID!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EnrEntrantContract> w, EnrEntrantContractType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(ContractObjectType.class, session, params, CONTRACT, nsiPackage);
            saveChildEntity(EnrRequestedCompetitionType.class, session, params, COMPETITION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
