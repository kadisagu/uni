//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.07.02 at 01:39:16 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника Дисциплины учебной программы
 * 
 * <p>Java class for EduProgramDisciplineInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EduProgramDisciplineInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineInformationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineInformationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineInformationChairShortTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineInformationComponentLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineInformationCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegistryDisciplineID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="RegistryDiscipline" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}RegistryDisciplineType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DepartmentID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Department" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DepartmentType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EduProgramDisciplineInformationType", propOrder = {

})
public class EduProgramDisciplineInformationType implements IDatagramObject
{

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EduProgramDisciplineInformationID")
    protected String eduProgramDisciplineInformationID;
    @XmlElement(name = "EduProgramDisciplineInformationName")
    protected String eduProgramDisciplineInformationName;
    @XmlElement(name = "EduProgramDisciplineInformationChairShortTitle")
    protected String eduProgramDisciplineInformationChairShortTitle;
    @XmlElement(name = "EduProgramDisciplineInformationComponentLevel")
    protected String eduProgramDisciplineInformationComponentLevel;
    @XmlElement(name = "EduProgramDisciplineInformationCycle")
    protected String eduProgramDisciplineInformationCycle;
    @XmlElement(name = "RegistryDisciplineID")
    protected RegistryDisciplineID registryDisciplineID;
    @XmlElement(name = "DepartmentID")
    protected DepartmentID departmentID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineInformationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduProgramDisciplineInformationID() {
        return eduProgramDisciplineInformationID;
    }

    /**
     * Sets the value of the eduProgramDisciplineInformationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduProgramDisciplineInformationID(String value) {
        this.eduProgramDisciplineInformationID = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineInformationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduProgramDisciplineInformationName() {
        return eduProgramDisciplineInformationName;
    }

    /**
     * Sets the value of the eduProgramDisciplineInformationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduProgramDisciplineInformationName(String value) {
        this.eduProgramDisciplineInformationName = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineInformationChairShortTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduProgramDisciplineInformationChairShortTitle() {
        return eduProgramDisciplineInformationChairShortTitle;
    }

    /**
     * Sets the value of the eduProgramDisciplineInformationChairShortTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduProgramDisciplineInformationChairShortTitle(String value) {
        this.eduProgramDisciplineInformationChairShortTitle = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineInformationComponentLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduProgramDisciplineInformationComponentLevel() {
        return eduProgramDisciplineInformationComponentLevel;
    }

    /**
     * Sets the value of the eduProgramDisciplineInformationComponentLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduProgramDisciplineInformationComponentLevel(String value) {
        this.eduProgramDisciplineInformationComponentLevel = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineInformationCycle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduProgramDisciplineInformationCycle() {
        return eduProgramDisciplineInformationCycle;
    }

    /**
     * Sets the value of the eduProgramDisciplineInformationCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduProgramDisciplineInformationCycle(String value) {
        this.eduProgramDisciplineInformationCycle = value;
    }

    /**
     * Gets the value of the registryDisciplineID property.
     * 
     * @return
     *     possible object is
     *     {@link RegistryDisciplineID }
     *     
     */
    public RegistryDisciplineID getRegistryDisciplineID() {
        return registryDisciplineID;
    }

    /**
     * Sets the value of the registryDisciplineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistryDisciplineID }
     *     
     */
    public void setRegistryDisciplineID(RegistryDisciplineID value) {
        this.registryDisciplineID = value;
    }

    /**
     * Gets the value of the departmentID property.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentID }
     *     
     */
    public DepartmentID getDepartmentID() {
        return departmentID;
    }

    /**
     * Sets the value of the departmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentID }
     *     
     */
    public void setDepartmentID(DepartmentID value) {
        this.departmentID = value;
    }

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Department" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DepartmentType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class DepartmentID {

        @XmlElement(name = "Department")
        protected DepartmentType department;

        /**
         * Gets the value of the department property.
         * 
         * @return
         *     possible object is
         *     {@link DepartmentType }
         *     
         */
        public DepartmentType getDepartment() {
            return department;
        }

        /**
         * Sets the value of the department property.
         * 
         * @param value
         *     allowed object is
         *     {@link DepartmentType }
         *     
         */
        public void setDepartment(DepartmentType value) {
            this.department = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="RegistryDiscipline" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}RegistryDisciplineType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class RegistryDisciplineID {

        @XmlElement(name = "RegistryDiscipline")
        protected RegistryDisciplineType registryDiscipline;

        /**
         * Gets the value of the registryDiscipline property.
         * 
         * @return
         *     possible object is
         *     {@link RegistryDisciplineType }
         *     
         */
        public RegistryDisciplineType getRegistryDiscipline() {
            return registryDiscipline;
        }

        /**
         * Sets the value of the registryDiscipline property.
         * 
         * @param value
         *     allowed object is
         *     {@link RegistryDisciplineType }
         *     
         */
        public void setRegistryDiscipline(RegistryDisciplineType value) {
            this.registryDiscipline = value;
        }

    }

}
