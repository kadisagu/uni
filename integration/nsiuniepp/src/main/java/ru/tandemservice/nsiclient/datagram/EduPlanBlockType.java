//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.09.15 at 12:33:12 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника Блок учебного плана (версии УП)
 * 
 * <p>Java class for EduPlanBlockType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EduPlanBlockType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EduPlanBlockCommon" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *         &lt;element name="EduPlanBlockProfileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduPlanVersionID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EduPlanVersion" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduPlanVersionType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EduPlanBlockType", propOrder = {

})
public class EduPlanBlockType implements IDatagramObject
{
    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EduPlanBlockCommon")
    @XmlSchemaType(name = "anySimpleType")
    protected String eduPlanBlockCommon;
    @XmlElement(name = "EduPlanBlockProfileName")
    protected String eduPlanBlockProfileName;
    @XmlElement(name = "EduPlanVersionID")
    protected EduPlanBlockType.EduPlanVersionID eduPlanVersionID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the eduPlanBlockCommon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduPlanBlockCommon() {
        return eduPlanBlockCommon;
    }

    /**
     * Sets the value of the eduPlanBlockCommon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduPlanBlockCommon(String value) {
        this.eduPlanBlockCommon = value;
    }

    /**
     * Gets the value of the eduPlanBlockProfileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEduPlanBlockProfileName() {
        return eduPlanBlockProfileName;
    }

    /**
     * Sets the value of the eduPlanBlockProfileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEduPlanBlockProfileName(String value) {
        this.eduPlanBlockProfileName = value;
    }

    /**
     * Gets the value of the eduPlanVersionID property.
     * 
     * @return
     *     possible object is
     *     {@link EduPlanBlockType.EduPlanVersionID }
     *     
     */
    public EduPlanBlockType.EduPlanVersionID getEduPlanVersionID() {
        return eduPlanVersionID;
    }

    /**
     * Sets the value of the eduPlanVersionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link EduPlanBlockType.EduPlanVersionID }
     *     
     */
    public void setEduPlanVersionID(EduPlanBlockType.EduPlanVersionID value) {
        this.eduPlanVersionID = value;
    }

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EduPlanVersion" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EduPlanVersionType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EduPlanVersionID {

        @XmlElement(name = "EduPlanVersion")
        protected EduPlanVersionType eduPlanVersion;

        /**
         * Gets the value of the eduPlanVersion property.
         * 
         * @return
         *     possible object is
         *     {@link EduPlanVersionType }
         *     
         */
        public EduPlanVersionType getEduPlanVersion() {
            return eduPlanVersion;
        }

        /**
         * Sets the value of the eduPlanVersion property.
         * 
         * @param value
         *     allowed object is
         *     {@link EduPlanVersionType }
         *     
         */
        public void setEduPlanVersion(EduPlanVersionType value) {
            this.eduPlanVersion = value;
        }

    }

}
