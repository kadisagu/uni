/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import ru.tandemservice.nsiclient.datagram.EduPlanRowTermActionType;
import ru.tandemservice.nsiclient.datagram.EduPlanRowTermType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanRowTermActionTypeReactor extends BaseEntityReactor<EppEpvRowTermAction, EduPlanRowTermActionType>
{
    @Override
    public Class<EppEpvRowTermAction> getEntityClass()
    {
        return EppEpvRowTermAction.class;
    }

    @Override
    public EduPlanRowTermActionType createDatagramObject(String guid)
    {
        EduPlanRowTermActionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermActionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEpvRowTermAction entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanRowTermActionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermActionType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanRowTermActionPriority(entity.getControlActionType().getCode());
        datagramObject.setEduPlanRowTermActionActionTypeName(entity.getControlActionType().getTitle());
        datagramObject.setEduPlanRowTermActionActionTypeShortName(entity.getControlActionType().getShortTitle());
        datagramObject.setEduPlanRowTermActionActionTypeAbbreviation(entity.getControlActionType().getAbbreviation());

        StringBuilder warning = new StringBuilder();
        ProcessedDatagramObject<EduPlanRowTermType> rowTermType = createDatagramObject(EduPlanRowTermType.class, entity.getRowTerm(), commonCache, warning);
        EduPlanRowTermActionType.EduPlanRowTermID rowTermID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermActionTypeEduPlanRowTermID();
        rowTermID.setEduPlanRowTerm(rowTermType.getObject());
        datagramObject.setEduPlanRowTermID(rowTermID);
        if (rowTermType.getList() != null) result.addAll(rowTermType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppEpvRowTermAction> processDatagramObject(EduPlanRowTermActionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanRowTermActionType is unsupported");
    }
}