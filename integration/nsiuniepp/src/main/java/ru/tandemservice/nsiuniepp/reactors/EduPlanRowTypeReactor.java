/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.datagram.EduPlanBlockType;
import ru.tandemservice.nsiclient.datagram.EduPlanRowType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.RegistryDisciplineType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiHierarchyReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanRowTypeReactor extends BaseEntityReactor<EppEpvRow, EduPlanRowType> implements INsiHierarchyReactor
{
    @Override
    public Class<EppEpvRow> getEntityClass()
    {
        return EppEpvRow.class;
    }

    @Override
    public EduPlanRowType createDatagramObject(String guid)
    {
        EduPlanRowType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEpvRow entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanRowType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanRowName(entity.getTitle());
        datagramObject.setEduPlanRowStoredIndex(entity.getStoredIndex());
        datagramObject.setEduPlanRowUserIndex(entity.getUserIndex());

        if (entity instanceof EppEpvGroupReRow)
        {
            EppEpvGroupReRow castedRow = (EppEpvGroupReRow) entity;
            datagramObject.setEduPlanRowNumber(castedRow.getNumber());
        }

        EppRegistryElement regElement = null;

        if (entity instanceof EppEpvTermDistributedRow)
        {
            EppEpvTermDistributedRow castedRow = (EppEpvTermDistributedRow) entity;
            datagramObject.setEduPlanRowNumber(castedRow.getNumber());
            datagramObject.setEduPlanRowTotalLabor(String.valueOf(castedRow.getTotalLabor()));
            datagramObject.setEduPlanRowHoursTotal(String.valueOf(castedRow.getHoursTotal()));
            datagramObject.setEduPlanRowHoursAudit(String.valueOf(castedRow.getHoursAudit()));
            datagramObject.setEduPlanRowHoursSelfwork(String.valueOf(castedRow.getHoursSelfwork()));
            datagramObject.setEduPlanRowHoursControl(String.valueOf(castedRow.getHoursControl()));
            datagramObject.setEduPlanRowHoursControlE(String.valueOf(castedRow.getHoursControlE()));

            if (entity instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow regElRow = (EppEpvRegistryRow) entity;
                regElement = regElRow.getRegistryElement();

                if (!regElRow.getType().isDisciplineElement())
                    datagramObject.setEduPlanRowIga(NsiUtils.formatBoolean(Boolean.TRUE));
            }
        }


        // TODO datagramObject.setEduPlanRowLineNumber();

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<EduPlanBlockType> eduPlanBlockType = createDatagramObject(EduPlanBlockType.class, entity.getOwner(), commonCache, warning);
        EduPlanRowType.EduPlanBlockID blockID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTypeEduPlanBlockID();
        blockID.setEduPlanBlock(eduPlanBlockType.getObject());
        datagramObject.setEduPlanBlockID(blockID);
        if (eduPlanBlockType.getList() != null) result.addAll(eduPlanBlockType.getList());

        EppEpvRow parentRow = entity.getHierarhyParent();
        if (null != parentRow)
        {
            ProcessedDatagramObject<EduPlanRowType> parentRowType = createDatagramObject(EduPlanRowType.class, parentRow, commonCache, warning);
            EduPlanRowType.ParentEduPlanRowID parentRowID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTypeParentEduPlanRowID();
            parentRowID.setEduPlanRow(parentRowType.getObject());
            datagramObject.setParentEduPlanRowID(parentRowID);
            if (parentRowType.getList() != null) result.addAll(parentRowType.getList());
        }

        if (null != regElement)
        {
            ProcessedDatagramObject<RegistryDisciplineType> regElementType = createDatagramObject(RegistryDisciplineType.class, regElement, commonCache, warning);
            EduPlanRowType.RegistryDisciplineID regElementID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTypeRegistryDisciplineID();
            regElementID.setRegistryDiscipline(regElementType.getObject());
            datagramObject.setRegistryDisciplineID(regElementID);
            if (regElementType.getList() != null) result.addAll(regElementType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppEpvRow> processDatagramObject(EduPlanRowType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanRowType is unsupported");
    }

    @Override
    public List<EppEpvRow> getHierarchyList(int packageSize)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                .column(property("nsi", NsiEntity.entityId()))
                .where(eq(property("nsi", NsiEntity.type()), value(getCatalogType())))
                .where(eq(property("nsi", NsiEntity.entityId()), property("e", IEntity.P_ID)));

        List<EppEpvRow> insertList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(getEntityClass(), "e")
                .top(packageSize).column(property("e")).where(notExists(builder.buildQuery())));

        List<EppEpvRow> preResultList = new ArrayList<>();
        for (EppEpvRow row : insertList)
        {
            if (!preResultList.contains(row))
            {
                preResultList.add(row);
                EppEpvRow parentRow = row.getHierarhyParent();
                while (null != parentRow)
                {
                    if (!preResultList.contains(parentRow))
                    {
                        preResultList.add(0, parentRow);
                        parentRow = parentRow.getHierarhyParent();
                    } else parentRow = null;
                }
                if (preResultList.size() > packageSize) break;
            }
        }

        DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                .column(property("nsi", NsiEntity.entityId()))
                .where(eq(property("nsi", NsiEntity.type()), value(getCatalogType())))
                .where(eq(property("nsi", NsiEntity.entityId()), property("e", IEntity.P_ID)));

        List<EppEpvRow> insertList1 = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(getEntityClass(), "e")
                .top(packageSize).column(property("e"))
                .where(in(property("e", IEntity.P_ID), preResultList))
                .where(notExists(builder1.buildQuery())));

        List<EppEpvRow> resultList = preResultList.stream().filter(row->insertList1.contains(row)).collect(Collectors.toList());

        return resultList;
    }
}