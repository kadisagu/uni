/* $Id:$ */
package ru.tandemservice.nsiuniepp.base.bo.EducationalProgram.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

/**
 * @author Dmitry Seleznev
 * @since 28.07.2016
 */
public interface IEducationalProgramDAO
{
    EducationLevelsHighSchool doGetOrCreateEduLevelHS(String title, String okso, OrgUnit responsibleOrgUnit, EduProgramQualification qualification, String gosGeneration, String code)
            throws ProcessingDatagramObjectException;
}