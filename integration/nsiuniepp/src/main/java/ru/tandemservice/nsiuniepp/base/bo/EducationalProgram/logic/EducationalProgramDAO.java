/* $Id:$ */
package ru.tandemservice.nsiuniepp.base.bo.EducationalProgram.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 28.07.2016
 */
public class EducationalProgramDAO extends SharedBaseDao implements IEducationalProgramDAO
{
    @Override
    public EducationLevelsHighSchool doGetOrCreateEduLevelHS(String title, String okso, OrgUnit responsibleOrgUnit, EduProgramQualification qualification, String gosGeneration, String code)
            throws ProcessingDatagramObjectException
    {
        if (null == StringUtils.trimToNull(title))
            throw new ProcessingDatagramObjectException("EducationalProgram name is not specified.");
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "elhs").column(property("elhs"));
        builder.where(eq(property(EducationLevelsHighSchool.title().fromAlias("elhs")), value(title)));
        if (null != responsibleOrgUnit) builder.where(eq(property(EducationLevelsHighSchool.orgUnit().fromAlias("elhs")), value(responsibleOrgUnit)));
        if (null != qualification) builder.where(eq(property(EducationLevelsHighSchool.assignedQualification().fromAlias("elhs")), value(qualification)));
        //if(null != gosGeneration) builder.where(likeUpper(property(EducationLevelsHighSchool.educationLevel().eduProgramSubject().)))
        List<EducationLevelsHighSchool> possibleLevels = builder.createStatement(getSession()).list();

        if (possibleLevels.size() == 1) return possibleLevels.get(0);
        else if (possibleLevels.size() > 1)
        {
            Map<String, List<EducationLevelsHighSchool>> oksoEduLvlHSMap = new HashMap<>();
            Map<String, List<EducationLevelsHighSchool>> gosGenerationEduLvlHSMap = new HashMap<>();
            for (EducationLevelsHighSchool elhs : possibleLevels)
            {
                if (null != StringUtils.trimToNull(gosGeneration))
                {
                    String generation = null != elhs.getEducationLevel().getEduProgramSubject()
                            ? IEppEduStdDAO.instance.get().getGeneration(elhs.getEducationLevel().getEduProgramSubject()).getTitle()
                            : EppGeneration.GENERATION_3.getTitle();

                    List<EducationLevelsHighSchool> gosGenElhsList = gosGenerationEduLvlHSMap.get(generation);
                    if (null == gosGenElhsList) gosGenElhsList = new ArrayList<>();
                    gosGenElhsList.add(elhs);
                    gosGenerationEduLvlHSMap.put(generation, gosGenElhsList);
                }

                if (null != StringUtils.trimToNull(okso) && null != elhs.getAssignedQualification())
                {
                    List<EducationLevelsHighSchool> oksoElhsList = oksoEduLvlHSMap.get(elhs.getEducationLevel().getTitleCodePrefix());
                    if (null == oksoElhsList) oksoElhsList = new ArrayList<>();
                    oksoElhsList.add(elhs);
                    oksoEduLvlHSMap.put(elhs.getEducationLevel().getTitleCodePrefix(), oksoElhsList);
                }
            }

            for (List<EducationLevelsHighSchool> items : gosGenerationEduLvlHSMap.values())
                if (items.size() == 1) return items.get(0);

            for (List<EducationLevelsHighSchool> items : oksoEduLvlHSMap.values())
                if (items.size() == 1) return items.get(0);
        }

        if (null == responsibleOrgUnit)
            throw new ProcessingDatagramObjectException("Could not create EducationLevelHighSchool: EducationalProgram responsibleOrgUnit required, but is not specified.");

        EduProgramQualification eduQual = null;
        /*if(null != StringUtils.trimToNull(qualification))

        EducationLevelsHighSchool elhs = new EducationLevelsHighSchool();
        elhs.setCode(null != code ? code : String.valueOf(System.currentTimeMillis()));
        elhs.setAllowStudents(true);
        elhs.set*/

        throw new ProcessingDatagramObjectException("Could not create EducationLevelHighSchool: Could not find EducationLevelHighSchool.");
    }
}