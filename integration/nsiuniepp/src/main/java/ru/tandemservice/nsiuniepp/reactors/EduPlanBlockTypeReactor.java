/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import ru.tandemservice.nsiclient.datagram.EduPlanBlockType;
import ru.tandemservice.nsiclient.datagram.EduPlanVersionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanBlockTypeReactor extends BaseEntityReactor<EppEduPlanVersionBlock, EduPlanBlockType>
{
    @Override
    public Class<EppEduPlanVersionBlock> getEntityClass()
    {
        return EppEduPlanVersionBlock.class;
    }

    @Override
    public EduPlanBlockType createDatagramObject(String guid)
    {
        EduPlanBlockType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanBlockType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEduPlanVersionBlock entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanBlockType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanBlockType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        if (entity instanceof EppEduPlanVersionRootBlock)
        {
            datagramObject.setEduPlanBlockCommon(NsiUtils.formatBoolean(Boolean.TRUE));
        } else if (entity instanceof EppEduPlanVersionSpecializationBlock)
        {
            EppEduPlanVersionSpecializationBlock specBlock = (EppEduPlanVersionSpecializationBlock) entity;
            datagramObject.setEduPlanBlockProfileName(specBlock.getTitle());
        }

        StringBuilder warning = new StringBuilder();
        ProcessedDatagramObject<EduPlanVersionType> eplVerType = createDatagramObjectShort(EduPlanVersionType.class, entity.getEduPlanVersion(), commonCache);
        EduPlanBlockType.EduPlanVersionID eplVersionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanBlockTypeEduPlanVersionID();
        eplVersionID.setEduPlanVersion(eplVerType.getObject());
        datagramObject.setEduPlanVersionID(eplVersionID);
        if (eplVerType.getList() != null) result.addAll(eplVerType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppEduPlanVersionBlock> processDatagramObject(EduPlanBlockType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanBlockType is unsupported");
    }
}