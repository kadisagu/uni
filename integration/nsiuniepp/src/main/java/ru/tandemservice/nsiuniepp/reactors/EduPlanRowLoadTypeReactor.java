/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import ru.tandemservice.nsiclient.datagram.EduPlanRowLoadType;
import ru.tandemservice.nsiclient.datagram.EduPlanRowType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanRowLoadTypeReactor extends BaseEntityReactor<EppEpvRowLoad, EduPlanRowLoadType>
{
    @Override
    public Class<EppEpvRowLoad> getEntityClass()
    {
        return EppEpvRowLoad.class;
    }

    @Override
    public EduPlanRowLoadType createDatagramObject(String guid)
    {
        EduPlanRowLoadType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowLoadType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEpvRowLoad entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanRowLoadType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowLoadType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanRowLoadPriority(entity.getLoadType().getCode());
        datagramObject.setEduPlanRowLoadLoadTypeName(entity.getLoadType().getTitle());
        datagramObject.setEduPlanRowLoadLoadTypeShortName(entity.getLoadType().getShortTitle());
        datagramObject.setEduPlanRowLoadLoadTypeAbbreviation(entity.getLoadType().getAbbreviation());
        datagramObject.setEduPlanRowLoadHours(String.valueOf(entity.getHours()));
        datagramObject.setEduPlanRowLoadHoursE(String.valueOf(entity.getHoursE()));
        datagramObject.setEduPlanRowLoadHoursI(String.valueOf(entity.getHoursI()));

        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<EduPlanRowType> rowType = createDatagramObject(EduPlanRowType.class, entity.getRow(), commonCache, warning);
        EduPlanRowLoadType.EduPlanRowID rowID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowLoadTypeEduPlanRowID();
        rowID.setEduPlanRow(rowType.getObject());
        datagramObject.setEduPlanRowID(rowID);
        if (rowType.getList() != null) result.addAll(rowType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppEpvRowLoad> processDatagramObject(EduPlanRowLoadType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanRowLoadType is unsupported");
    }
}