/* $Id: EduProgramDisciplineInformationTypeReactor.java 43966 2015-07-10 05:13:06Z aavetisov $ */
package ru.tandemservice.nsiuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.DepartmentType;
import ru.tandemservice.nsiclient.datagram.EduProgramDisciplineInformationType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.RegistryDisciplineType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 08.07.2015
 */
public class EduProgramDisciplineInformationTypeReactor extends BaseEntityReactor<EppWorkPlanRegistryElementRow, EduProgramDisciplineInformationType>
{
    private static final String DEPARTMENT = "DepartmentID";
    private static final String DISCIPLINE = "RegistryDisciplineID";
    private static final String REGISTRY_ELEMENT = "RegistryElement";

    @Override
    public Class<EppWorkPlanRegistryElementRow> getEntityClass()
    {
        return EppWorkPlanRegistryElementRow.class;
    }

    @Override
    public void collectUnknownDatagrams(EduProgramDisciplineInformationType datagramObject, IUnknownDatagramsCollector collector)
    {
        EduProgramDisciplineInformationType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if (departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);

        EduProgramDisciplineInformationType.RegistryDisciplineID registryDisciplineID = datagramObject.getRegistryDisciplineID();
        RegistryDisciplineType registryDisciplineType = registryDisciplineID == null ? null : registryDisciplineID.getRegistryDiscipline();
        if (registryDisciplineType != null)
            collector.check(registryDisciplineType.getID(), RegistryDisciplineType.class);
    }

    @Override
    public EduProgramDisciplineInformationType createDatagramObject(String guid)
    {
        EduProgramDisciplineInformationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineInformationType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppWorkPlanRegistryElementRow entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramDisciplineInformationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineInformationType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        if(entity.getOwner() != null) {
            ProcessedDatagramObject<DepartmentType> departmentType = createDatagramObject(DepartmentType.class, entity.getOwner(), commonCache, warning);
            EduProgramDisciplineInformationType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineInformationTypeDepartmentID();
            departmentID.setDepartment(departmentType.getObject());
            datagramObject.setDepartmentID(departmentID);
            if (departmentType.getList() != null)
                result.addAll(departmentType.getList());
        }

        if(entity.getRegistryElement() != null && entity.getRegistryElement() instanceof EppRegistryDiscipline) {
            ProcessedDatagramObject<RegistryDisciplineType> registryDisciplineType = createDatagramObject(RegistryDisciplineType.class, entity.getRegistryElement(), commonCache, warning);
            EduProgramDisciplineInformationType.RegistryDisciplineID registryDisciplineID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineInformationTypeRegistryDisciplineID();
            registryDisciplineID.setRegistryDiscipline(registryDisciplineType.getObject());
            datagramObject.setRegistryDisciplineID(registryDisciplineID);
            if (registryDisciplineType.getList() != null)
                result.addAll(registryDisciplineType.getList());
        }

        datagramObject.setEduProgramDisciplineInformationID(entity.getRegistryElementPart().getRegistryElement().getNumber());
        datagramObject.setEduProgramDisciplineInformationName(entity.getRegistryElement().getTitle());
        datagramObject.setEduProgramDisciplineInformationChairShortTitle(entity.getRegistryElement().getOwner().getShortTitle());
        findAndSetInDatagramCycleAndPartTitles(entity, datagramObject);
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppWorkPlanRegistryElementRow> processDatagramObject(EduProgramDisciplineInformationType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        EduProgramDisciplineInformationType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit orgUnit = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT, null, warning);

        EduProgramDisciplineInformationType.RegistryDisciplineID registryDisciplineID = datagramObject.getRegistryDisciplineID();
        RegistryDisciplineType registryDisciplineType = registryDisciplineID == null ? null : registryDisciplineID.getRegistryDiscipline();
        EppRegistryElement eppRegistryElement = getOrCreate(RegistryDisciplineType.class, params, commonCache, registryDisciplineType, DISCIPLINE, null, warning);

        String number = StringUtils.trimToNull(datagramObject.getEduProgramDisciplineInformationID());
        String title = StringUtils.trimToNull(datagramObject.getEduProgramDisciplineInformationName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getEduProgramDisciplineInformationChairShortTitle());

        EppWorkPlanRegistryElementRow entity = null;
        boolean isRegistryElementChanged = false;

        boolean isChanged = false;
        CoreCollectionUtils.Pair<EppWorkPlanRegistryElementRow, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();

        if(entity == null)
            throw new ProcessingDatagramObjectException("Creation object for type EduProgramDisciplineInformationType is unsupported");

//        if(number != null && !number.equals(entity.getRegistryElement().getNumber())) {
//            isRegistryElementChanged = true;
//            entity.getRegistryElement().setNumber(number);
//        }
//
//        if(title != null && !title.equals(entity.getRegistryElement().getTitle())) {
//            isRegistryElementChanged = true;
//            entity.getRegistryElement().setTitle(title);
//        }
//
//        if(shortTitle != null && !shortTitle.equals(entity.getRegistryElement().getShortTitle())) {
//            isRegistryElementChanged = true;
//            entity.getRegistryElement().setShortTitle(shortTitle);
//        }
//
//        if(orgUnit != null && !orgUnit.equals(entity.getOwner())) {
//            isRegistryElementChanged = true;
//            entity.getRegistryElement().setOwner(orgUnit);
//        }
//
//        if(eppRegistryElement != null && !eppRegistryElement.equals(entity.getRegistryElement())) {
//            isRegistryElementChanged = true;
//            entity.setRegistryElementPart(eppRegistryElement);
//        }
//
//        if(isRegistryElementChanged)
//            additionalParams.put(REGISTRY_ELEMENT, entity.getRegistryElement());

        return new ChangedWrapper<>(false, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }


    //support methods
    private static EppEpvRegistryRow getEpvRow(EppWorkPlanRegistryElementRow entity)
    {
        EppEduPlanVersionBlock block = entity.getWorkPlan().getBlock();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "r").column("r")
                .where(DQLExpressions.eqValue(DQLExpressions.property("r", EppEpvRegistryRow.L_OWNER), block))
                .where(DQLExpressions.eqValue(DQLExpressions.property("r", EppEpvRegistryRow.L_REGISTRY_ELEMENT), entity.getRegistryElement()));
        List<EppEpvRegistryRow> rows = DataAccessServices.dao().getList(dql);
        if (rows.size() == 1)
        {
            return rows.get(0);
        }
        return null;
    }

    private static void findAndSetInDatagramCycleAndPartTitles(EppWorkPlanRegistryElementRow entity, EduProgramDisciplineInformationType datagramObject)
    {
        EppEpvRow row = getEpvRow(entity);
        while (row != null)
        {
            row = row.getHierarhyParent();
            if (row instanceof EppEpvStructureRow)
            {
                EppPlanStructure planStructure = ((EppEpvStructureRow) row).getValue();
                if (isPart(planStructure))
                {
                    datagramObject.setEduProgramDisciplineInformationComponentLevel(getPlanStructureShortTitle(planStructure));
                }
                else if (isCycle(planStructure))
                {
                    datagramObject.setEduProgramDisciplineInformationCycle(getPlanStructureShortTitle(planStructure));
                }
            }
        }

        if (datagramObject.getEduProgramDisciplineInformationComponentLevel() == null)
        {
            //Если ничего не нашли, то компонент базовый. На случай изменения кодов, делаем запрос к базе:
            EppPlanStructure planStructure = DataAccessServices.dao().getByCode(EppPlanStructure.class, EppPlanStructureCodes.FGOS_2013_PARTS_BASE);
            datagramObject.setEduProgramDisciplineInformationComponentLevel(getPlanStructureShortTitle(planStructure));
        }
    }

    private static boolean isPart(EppPlanStructure planStructure)
    {
        return planStructure.getCode().contains(EppPlanStructureCodes.GOS_COMPONENTS)
                || planStructure.getCode().contains(EppPlanStructureCodes.FGOS_2013_PARTS)
                || planStructure.getCode().contains(EppPlanStructureCodes.FGOS_PARTS);
    }


    private static boolean isCycle(EppPlanStructure planStructure)
    {
        return planStructure.getCode().contains(EppPlanStructureCodes.GOS_CYCLES)
                || planStructure.getCode().contains(EppPlanStructureCodes.FGOS_CYCLES)
                || planStructure.getCode().contains(EppPlanStructureCodes.FGOS_2013_BLOCKS);
    }


    private static String getPlanStructureShortTitle(EppPlanStructure planStructure)
    {
        String shortTitle = planStructure.getShortTitle();
        return shortTitle != null ? shortTitle : planStructure.getTitle();
    }


}
