/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import ru.tandemservice.nsiclient.datagram.EduPlanRowTermType;
import ru.tandemservice.nsiclient.datagram.EduPlanRowType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanRowTermTypeReactor extends BaseEntityReactor<EppEpvRowTerm, EduPlanRowTermType>
{
    @Override
    public Class<EppEpvRowTerm> getEntityClass()
    {
        return EppEpvRowTerm.class;
    }

    @Override
    public EduPlanRowTermType createDatagramObject(String guid)
    {
        EduPlanRowTermType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEpvRowTerm entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanRowTermType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanRowTermTermName(entity.getTerm().getTitle());
        datagramObject.setEduPlanRowTermHoursTotal(String.valueOf(entity.getHoursTotal()));
        datagramObject.setEduPlanRowTermLabor(String.valueOf(entity.getLabor()));
        datagramObject.setEduPlanRowTermWeeks(String.valueOf(entity.getWeeks()));
        datagramObject.setEduPlanRowTermHoursAudit(String.valueOf(entity.getHoursAudit()));
        datagramObject.setEduPlanRowTermHoursSelfwork(String.valueOf(entity.getHoursSelfwork()));
        datagramObject.setEduPlanRowTermHoursControl(String.valueOf(entity.getHoursControl()));
        datagramObject.setEduPlanRowTermHoursControl(String.valueOf(entity.getHoursControlE()));

        StringBuilder warning = new StringBuilder();
        ProcessedDatagramObject<EduPlanRowType> rowType = createDatagramObject(EduPlanRowType.class, entity.getRow(), commonCache, warning);
        EduPlanRowTermType.EduPlanRowID rowID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermTypeEduPlanRowID();
        rowID.setEduPlanRow(rowType.getObject());
        datagramObject.setEduPlanRowID(rowID);
        if (rowType.getList() != null) result.addAll(rowType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppEpvRowTerm> processDatagramObject(EduPlanRowTermType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanRowTermType is unsupported");
    }
}