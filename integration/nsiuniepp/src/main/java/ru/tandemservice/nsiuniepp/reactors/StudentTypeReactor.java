/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.07.2015
 */
public class StudentTypeReactor extends BaseEntityReactor<Student, StudentType>
{
    private static final String HUMAN = "HumanID";
    private static final String EDUCATIONAL_PROGRAM = "EducationalProgramID";
    private static final String ACADEMIC_GROUP = "AcademicGroupID";
    private static final String COMPENSATION_TYPE = "CompensationTypeID";
    private static final String STUDENT_STATUS = "StudentStatusID";
    private static final String COURSE = "CourseID";
    private static final String STUDENT_CATEGORY = "StudentCategoryID";
    private static final String PROGRAM_QUALIFICATION = "ProgramQualificationID";
    private static final String FOREIGN_LANGUAGE = "ForeignLanguageID";
    private static final String ORDER_DATA = "orderData";
    private static final String HIGH_SCHOOL = "highSchool";

    @Override
    public Class<Student> getEntityClass()
    {
        return Student.class;
    }

    @Override
    public void collectUnknownDatagrams(StudentType datagramObject, IUnknownDatagramsCollector collector)
    {
        StudentType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null) collector.check(humanType.getID(), HumanType.class);

        StudentType.AcademicGroupID academicGroupID = datagramObject.getAcademicGroupID();
        AcademicGroupType academicGroupType = academicGroupID == null ? null : academicGroupID.getAcademicGroup();
        if (academicGroupType != null) collector.check(academicGroupType.getID(), AcademicGroupType.class);

        StudentType.EducationalProgramID educationalProgramID = datagramObject.getEducationalProgramID();
        EducationalProgramType educationalProgramType = educationalProgramID == null ? null : educationalProgramID.getEducationalProgram();
        if (educationalProgramType != null) collector.check(educationalProgramType.getID(), EducationalProgramType.class);

        StudentType.CompensationTypeID compensationTypeID = datagramObject.getCompensationTypeID();
        CompensationTypeType compensationType = compensationTypeID == null ? null : compensationTypeID.getCompensationType();
        if (compensationType != null) collector.check(compensationType.getID(), CompensationTypeType.class);

        StudentType.StudentStatusID studentStatusID = datagramObject.getStudentStatusID();
        StudentStatusType studentStatusType = studentStatusID == null ? null : studentStatusID.getStudentStatus();
        if (studentStatusType != null) collector.check(studentStatusType.getID(), StudentStatusType.class);

        StudentType.CourseID courseID = datagramObject.getCourseID();
        CourseType courseType = courseID == null ? null : courseID.getCourse();
        if (courseType != null) collector.check(courseType.getID(), CourseType.class);

        StudentType.StudentCategoryID studentCategoryID = datagramObject.getStudentCategoryID();
        StudentCategoryType studentCategoryType = studentCategoryID == null ? null : studentCategoryID.getStudentCategory();
        if (studentCategoryType != null) collector.check(studentCategoryType.getID(), StudentCategoryType.class);

        StudentType.ProgramQualificationID qualificationID = datagramObject.getProgramQualificationID();
        EduProgramQualificationType programQualification = qualificationID == null ? null : qualificationID.getProgramQualification();
        if (programQualification != null) collector.check(programQualification.getID(), EduProgramQualificationType.class);

        StudentType.ForeignLanguageID foreignLanguageID = datagramObject.getForeignLanguageID();
        ForeignLanguageType foreignLanguageType = foreignLanguageID == null ? null : foreignLanguageID.getForeignLanguage();
        if (foreignLanguageType != null) collector.check(foreignLanguageType.getID(), ForeignLanguageType.class);
    }

    @Override
    public StudentType createDatagramObject(String guid)
    {
        StudentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(Student entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        StudentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        datagramObject.setStudentCourseNumber(Integer.toString(entity.getCourse().getIntValue()));
        EppStudent2EduPlanVersion s2epv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(entity.getId());
        if (s2epv != null)
            datagramObject.setStudentTermNumber(Integer.toString(s2epv.getEduPlanVersion().getDevelopGridTerm().getTermNumber()));
        datagramObject.setStudentPersonalNumber(entity.getPersonalNumber());
        datagramObject.setStudentBookNumber(entity.getBookNumber());
        datagramObject.setStudentEntranceYear(Integer.toString(entity.getEntranceYear()));
        datagramObject.setStudentAnnotation(entity.getComment());
        datagramObject.setStudentIndividualTraining(NsiUtils.formatBoolean(entity.isPersonalEducation()));
        datagramObject.setStudentTarget(NsiUtils.formatBoolean(entity.isTargetAdmission()));
        datagramObject.setStudentEconomicGroup(NsiUtils.formatBoolean(entity.isThriftyGroup()));
        if (entity.getFinishYear() != null)
            datagramObject.setStudentYearEnd(Integer.toString(entity.getFinishYear()));
        datagramObject.setStudentPrivacyNumber(entity.getPersonalFileNumber());
        datagramObject.setStudentArchivePrivacyNumber(entity.getPersonalArchivalFileNumber());
        datagramObject.setStudentArchival(NsiUtils.formatBoolean(entity.isArchival()));
        datagramObject.setStudentArchiveDate(NsiUtils.formatDate(entity.getArchivingDate()));
        datagramObject.setAccountExpirationDate(NsiUtils.formatDate(entity.getArchivingDate()));
        datagramObject.setStudentFinalQualifiyngWorkTheme(entity.getFinalQualifyingWorkTheme());
        datagramObject.setStudentFinalQualifiyngWorkAdvisor(entity.getFinalQualifyingWorkAdvisor());
        datagramObject.setStudentPracticePlace(entity.getPracticePlace());
        datagramObject.setStudentEmployment(entity.getEmployment());

        OrderData orderData = DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, entity);
        if (orderData != null)
        {
            datagramObject.setStudentEnrollmentOrderDate(NsiUtils.formatDate(orderData.getEduEnrollmentOrderDate()));
            datagramObject.setStudentEnrollmentOrderEnrDate(NsiUtils.formatDate(orderData.getEduEnrollmentOrderEnrDate()));
            datagramObject.setStudentEnrollmentOrderNumber(orderData.getEduEnrollmentOrderNumber());
            datagramObject.setStudentDischargeOrderDate(NsiUtils.formatDate(orderData.getExcludeOrderDate()));
            datagramObject.setStudentDischargeOrderNumber(orderData.getExcludeOrderNumber());

            if (datagramObject.getAccountExpirationDate() == null)
            {
                Date weekendDateFrom = orderData.getWeekendDateFrom();
                Date excludedDate = orderData.getExcludeOrderDate();

                if (weekendDateFrom != null && excludedDate != null)
                {
                    if (weekendDateFrom.after(excludedDate))
                        datagramObject.setAccountExpirationDate(NsiUtils.formatDate(weekendDateFrom));
                    else
                        datagramObject.setAccountExpirationDate(NsiUtils.formatDate(excludedDate));
                }
                else if (weekendDateFrom != null)
                {
                    datagramObject.setAccountExpirationDate(NsiUtils.formatDate(weekendDateFrom));
                }
                else
                {
                    datagramObject.setAccountExpirationDate(NsiUtils.formatDate(excludedDate));
                }
            }
        }
        datagramObject.setStudentComment(entity.getDescription());

        StringBuilder warning = new StringBuilder();
        ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        StudentType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeHumanID();
        humanID.setHuman(humanType.getObject());
        datagramObject.setHumanID(humanID);
        if (humanType.getList() != null)
            result.addAll(humanType.getList());

        Group group = entity.getGroup();
        if (group != null)
        {
            ProcessedDatagramObject<AcademicGroupType> academicGroupType = createDatagramObject(AcademicGroupType.class, group, commonCache, warning);
            StudentType.AcademicGroupID academicGroupID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeAcademicGroupID();
            academicGroupID.setAcademicGroup(academicGroupType.getObject());
            datagramObject.setAcademicGroupID(academicGroupID);
            if (academicGroupType.getList() != null)
                result.addAll(academicGroupType.getList());
        }

        ProcessedDatagramObject<EducationalProgramType> educationalProgramType = createDatagramObject(EducationalProgramType.class, entity.getEducationOrgUnit(), commonCache, warning);
        StudentType.EducationalProgramID educationalProgramID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeEducationalProgramID();
        educationalProgramID.setEducationalProgram(educationalProgramType.getObject());
        datagramObject.setEducationalProgramID(educationalProgramID);
        if (educationalProgramType.getList() != null)
            result.addAll(educationalProgramType.getList());

        ProcessedDatagramObject<CompensationTypeType> compensationType = createDatagramObject(CompensationTypeType.class, entity.getCompensationType(), commonCache, warning);
        StudentType.CompensationTypeID compensationTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeCompensationTypeID();
        compensationTypeID.setCompensationType(compensationType.getObject());
        datagramObject.setCompensationTypeID(compensationTypeID);
        if (compensationType.getList() != null)
            result.addAll(compensationType.getList());

        ProcessedDatagramObject<StudentStatusType> studentStatusType = createDatagramObject(StudentStatusType.class, entity.getStatus(), commonCache, warning);
        StudentType.StudentStatusID studentStatusID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeStudentStatusID();
        studentStatusID.setStudentStatus(studentStatusType.getObject());
        datagramObject.setStudentStatusID(studentStatusID);
        if (studentStatusType.getList() != null)
            result.addAll(studentStatusType.getList());

        ProcessedDatagramObject<CourseType> courseType = createDatagramObject(CourseType.class, entity.getCourse(), commonCache, warning);
        StudentType.CourseID courseID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeCourseID();
        courseID.setCourse(courseType.getObject());
        datagramObject.setCourseID(courseID);
        if (courseType.getList() != null)
            result.addAll(courseType.getList());

        ProcessedDatagramObject<StudentCategoryType> studentCategoryType = createDatagramObject(StudentCategoryType.class, entity.getStudentCategory(), commonCache, warning);
        StudentType.StudentCategoryID studentCategoryID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeStudentCategoryID();
        studentCategoryID.setStudentCategory(studentCategoryType.getObject());
        datagramObject.setStudentCategoryID(studentCategoryID);
        if (studentCategoryType.getList() != null)
            result.addAll(studentCategoryType.getList());

        EduProgramQualification qualification = entity.getEducationOrgUnit().getEducationLevelHighSchool().getAssignedQualification();
        if (qualification != null)
        {
            ProcessedDatagramObject<EduProgramQualificationType> qualificationType = createDatagramObject(EduProgramQualificationType.class, qualification, commonCache, warning);
            StudentType.ProgramQualificationID qualificationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeProgramQualificationID();
            qualificationID.setProgramQualification(qualificationType.getObject());
            datagramObject.setProgramQualificationID(qualificationID);
            if (qualificationType.getList() != null)
                result.addAll(qualificationType.getList());
        }

        PersonForeignLanguage language = null;
        List<PersonForeignLanguage> languages = DataAccessServices.dao().getList(PersonForeignLanguage.class, PersonForeignLanguage.L_PERSON, entity.getPerson());
        if (!languages.isEmpty())
            language = languages.get(0);
        if (language != null)
        {
            ProcessedDatagramObject<ForeignLanguageType> foreignLanguageType = createDatagramObject(ForeignLanguageType.class, language.getLanguage(), commonCache, warning);
            StudentType.ForeignLanguageID foreignLanguageID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentTypeForeignLanguageID();
            foreignLanguageID.setForeignLanguage(foreignLanguageType.getObject());
            datagramObject.setForeignLanguageID(foreignLanguageID);
            if (foreignLanguageType.getList() != null)
                result.addAll(foreignLanguageType.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private Student findStudent(Person person, EducationOrgUnit educationOrgUnit, Course course, CompensationType compensationType, StudentStatus status, String personalNumber, String entranceYear)
    {
        if (person != null && educationOrgUnit != null)
        {
            if (null != personalNumber)
            {
                List<IExpression> expressions = new ArrayList<>();
                expressions.add(eq(property(ENTITY_ALIAS, Student.person()), value(person)));
                expressions.add(eq(property(ENTITY_ALIAS, Student.educationOrgUnit()), value(educationOrgUnit)));
                expressions.add(eq(property(ENTITY_ALIAS, Student.personalNumber()), value(personalNumber)));
                List<Student> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
                if (list.size() == 1) return list.get(0);
            }

            if (null != personalNumber)
            {
                List<IExpression> expressions = new ArrayList<>();
                expressions.add(eq(property(ENTITY_ALIAS, Student.personalNumber()), value(personalNumber)));
                List<Student> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
                if (list.size() == 1) return list.get(0);
            }

            if (null != compensationType || null != course || null != entranceYear || null != status)
            {
                List<IExpression> expressions = new ArrayList<>();
                expressions.add(eq(property(ENTITY_ALIAS, Student.person()), value(person)));
                expressions.add(eq(property(ENTITY_ALIAS, Student.educationOrgUnit()), value(educationOrgUnit)));
                if (null != compensationType) expressions.add(eq(property(ENTITY_ALIAS, Student.compensationType()), value(compensationType)));
                if (null != entranceYear) expressions.add(eq(property(ENTITY_ALIAS, Student.entranceYear()), value(entranceYear)));
                if (null != status) expressions.add(eq(property(ENTITY_ALIAS, Student.status()), value(status)));
                if (null != course) expressions.add(eq(property(ENTITY_ALIAS, Student.course()), value(course)));
                List<Student> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
                if (list.size() == 1) return list.get(0);
            }

            if (null != compensationType || null != course || null != entranceYear)
            {
                List<IExpression> expressions = new ArrayList<>();
                expressions.add(eq(property(ENTITY_ALIAS, Student.person()), value(person)));
                expressions.add(eq(property(ENTITY_ALIAS, Student.educationOrgUnit()), value(educationOrgUnit)));
                if (null != compensationType) expressions.add(eq(property(ENTITY_ALIAS, Student.compensationType()), value(compensationType)));
                if (null != entranceYear) expressions.add(eq(property(ENTITY_ALIAS, Student.entranceYear()), value(entranceYear)));
                if (null != course) expressions.add(eq(property(ENTITY_ALIAS, Student.course()), value(course)));
                List<Student> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
                if (list.size() == 1) return list.get(0);
            }

            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, Student.person()), value(person)));
            expressions.add(eq(property(ENTITY_ALIAS, Student.educationOrgUnit()), value(educationOrgUnit)));
            List<Student> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            return list.isEmpty()? null : list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<Student> processDatagramObject(StudentType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // StudentCourseNumber init
        String courseNumber = StringUtils.trimToNull(datagramObject.getStudentCourseNumber());
        // StudentTermNumber init
        String termNumber = StringUtils.trimToNull(datagramObject.getStudentTermNumber());
        // StudentPersonalNumber init
        String personalNumber = StringUtils.trimToNull(datagramObject.getStudentPersonalNumber());
        // StudentBookNumber init
        String bookNumber = StringUtils.trimToNull(datagramObject.getStudentBookNumber());
        // StudentEntranceYear init
        String entranceYearStr = StringUtils.trimToNull(datagramObject.getStudentEntranceYear());
        Integer entranceYear = null;
        if (entranceYearStr != null)
        {
            try
            {
                entranceYear = Integer.parseInt(entranceYearStr);
            }
            catch (NumberFormatException e)
            {
                throw new ProcessingDatagramObjectException("StudentEntranceYear is not number: " + entranceYearStr + '!');
            }
        }
        // StudentAnnotation init
        String annotation = StringUtils.trimToNull(datagramObject.getStudentAnnotation());
        // StudentIndividualTraining init
        Boolean individualTraining = NsiUtils.parseBoolean(datagramObject.getStudentIndividualTraining());
        // StudentTarget init
        Boolean target = NsiUtils.parseBoolean(datagramObject.getStudentTarget());
        // StudentEconomicGroup init
        Boolean economicGroup = NsiUtils.parseBoolean(datagramObject.getStudentEconomicGroup());
        // StudentYearEnd init
        String yearEndStr = StringUtils.trimToNull(datagramObject.getStudentYearEnd());
        Integer yearEnd = null;
        if (yearEndStr != null)
        {
            try
            {
                yearEnd = Integer.parseInt(yearEndStr);
            }
            catch (NumberFormatException e)
            {
                throw new ProcessingDatagramObjectException("StudentYearEnd is not number: " + yearEndStr + '!');
            }
        }
        // StudentPrivacyNumber init
        String privacyNumber = StringUtils.trimToNull(datagramObject.getStudentPrivacyNumber());
        // StudentArchivePrivacyNumber init
        String archivePrivacyNumber = StringUtils.trimToNull(datagramObject.getStudentArchivePrivacyNumber());
        // StudentArchival init
        Boolean isArchival = NsiUtils.parseBoolean(datagramObject.getStudentArchival());
        // StudentArchiveDate init
        Date archiveDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getStudentArchiveDate()), "StudentArchiveDate");
        // StudentFinalQualifiyngWorkTheme init
        String finalWorkTheme = StringUtils.trimToNull(datagramObject.getStudentFinalQualifiyngWorkTheme());
        // StudentFinalQualifiyngWorkAdvisor init
        String finalWorkAdvisor = StringUtils.trimToNull(datagramObject.getStudentFinalQualifiyngWorkAdvisor());
        // StudentPracticePlace init
        String practicePlace = StringUtils.trimToNull(datagramObject.getStudentPracticePlace());
        // StudentEmployment init
        String employment = StringUtils.trimToNull(datagramObject.getStudentEmployment());
        // StudentEnrollmentOrderDate init
        Date enrollmentOrderDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getStudentEnrollmentOrderDate()), "StudentEnrollmentOrderDate");
        // StudentEnrollmentOrderEnrDate init
        Date enrollmentOrderEnrDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getStudentEnrollmentOrderEnrDate()), "StudentEnrollmentOrderEnrDate");
        // StudentEnrollmentOrderNumber init
        String enrollmentOrderNumber = StringUtils.trimToNull(datagramObject.getStudentEnrollmentOrderNumber());
        // StudentDischargeOrderDate init
        Date dischargeOrderDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getStudentDischargeOrderDate()), "StudentDischargeOrderDate");
        // StudentDischargeOrderDischargeDate init
        //Date dischargeOrderDischargeDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getStudentDischargeOrderDischargeDate()), "StudentDischargeOrderDischargeDate");
        // StudentDischargeOrderNumber init
        String dischargeOrderNumber = StringUtils.trimToNull(datagramObject.getStudentDischargeOrderNumber());
        // StudentComment init
        String comment = StringUtils.trimToNull(datagramObject.getStudentComment());
        // HumanID init
        StudentType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);
        // AcademicGroupID init
        StudentType.AcademicGroupID academicGroupID = datagramObject.getAcademicGroupID();
        AcademicGroupType academicGroupType = academicGroupID == null ? null : academicGroupID.getAcademicGroup();
        Group group = getOrCreate(AcademicGroupType.class, params, commonCache, academicGroupType, ACADEMIC_GROUP, null, warning);
        // EducationalProgramID init
        StudentType.EducationalProgramID educationalProgramID = datagramObject.getEducationalProgramID();
        EducationalProgramType educationalProgramType = educationalProgramID == null ? null : educationalProgramID.getEducationalProgram();
        EducationOrgUnit educationOrgUnit = getOrCreate(EducationalProgramType.class, params, commonCache, educationalProgramType, EDUCATIONAL_PROGRAM, null, warning);
        // CompensationTypeID init
        StudentType.CompensationTypeID compensationTypeID = datagramObject.getCompensationTypeID();
        CompensationTypeType compensationType = compensationTypeID == null ? null : compensationTypeID.getCompensationType();
        CompensationType compensation = getOrCreate(CompensationTypeType.class, params, commonCache, compensationType, COMPENSATION_TYPE, null, warning);
        // StudentStatusID init
        StudentType.StudentStatusID studentStatusID = datagramObject.getStudentStatusID();
        StudentStatusType studentStatusType = studentStatusID == null ? null : studentStatusID.getStudentStatus();
        StudentStatus status = getOrCreate(StudentStatusType.class, params, commonCache, studentStatusType, STUDENT_STATUS, null, warning);
        // CourseID init
        StudentType.CourseID courseID = datagramObject.getCourseID();
        CourseType courseType = courseID == null ? null : courseID.getCourse();
        Course course = getOrCreate(CourseType.class, params, commonCache, courseType, COURSE, null, warning);
        // StudentCategoryID init
        StudentType.StudentCategoryID studentCategoryID = datagramObject.getStudentCategoryID();
        StudentCategoryType studentCategoryType = studentCategoryID == null ? null : studentCategoryID.getStudentCategory();
        StudentCategory category = getOrCreate(StudentCategoryType.class, params, commonCache, studentCategoryType, STUDENT_CATEGORY, null, warning);
        // QualificationID init
        StudentType.ProgramQualificationID qualificationID = datagramObject.getProgramQualificationID();
        EduProgramQualificationType qualificationType = qualificationID == null ? null : qualificationID.getProgramQualification();
        EduProgramQualification qualification = getOrCreate(EduProgramQualificationType.class, params, commonCache, qualificationType, PROGRAM_QUALIFICATION, null, warning);
        // ForeignLanguageID init
        StudentType.ForeignLanguageID foreignLanguageID = datagramObject.getForeignLanguageID();
        ForeignLanguageType foreignLanguageType = foreignLanguageID == null ? null : foreignLanguageID.getForeignLanguage();
        ForeignLanguage foreignLanguage = getOrCreate(ForeignLanguageType.class, params, commonCache, foreignLanguageType, FOREIGN_LANGUAGE, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        boolean isOrderDataChanged = false;
        Student entity = null;
        OrderData orderData = null;
        CoreCollectionUtils.Pair<Student, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findStudent(person, educationOrgUnit, course, compensation, status, personalNumber, entranceYearStr);
        if (entity == null)
        {
            entity = new Student();
            isNew = true;
            isChanged = true;
        }
        else
            orderData = DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, entity);

        if (orderData == null)
        {
            orderData = new OrderData();
            orderData.setStudent(entity);
            isOrderDataChanged = true;
        }

        boolean justChangeStatus = !isNew && null != status && null != entity.getStatus() && !status.equals(entity.getStatus());

        // StudentPersonalNumber set
        if (personalNumber == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without StudentPersonalNumber!");
        if (personalNumber != null && !personalNumber.equals(entity.getPersonalNumber()))
        {
            boolean perNumUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(Student.class, Student.P_PERSONAL_NUMBER, entity.getId(), personalNumber);
            if (!perNumUnique && !justChangeStatus)
                throw new ProcessingDatagramObjectException("StudentPersonalNumber is not unique!");

            if(perNumUnique)
            {
                isChanged = true;
                entity.setPersonalNumber(personalNumber);
            }
        }
        // StudentBookNumber set
        if (bookNumber != null && !bookNumber.equals(entity.getBookNumber()))
        {
            isChanged = true;
            entity.setBookNumber(bookNumber);
        }
        // StudentEntranceYear set
        if (entranceYear != null && entranceYear != entity.getEntranceYear())
        {
            isChanged = true;
            entity.setEntranceYear(entranceYear);
        }
        // StudentAnnotation set
        if (annotation != null && !annotation.equals(entity.getComment()))
        {
            isChanged = true;
            entity.setComment(annotation);
        }
        // StudentIndividualTraining set
        if (individualTraining != null && individualTraining != entity.isPersonalEducation())
        {
            isChanged = true;
            entity.setPersonalEducation(individualTraining);
        }
        // StudentTarget set
        if (target != null && target != entity.isTargetAdmission())
        {
            isChanged = true;
            entity.setTargetAdmission(target);
        }
        // StudentEconomicGroup set
        if (economicGroup != null && economicGroup != entity.isThriftyGroup())
        {
            isChanged = true;
            entity.setThriftyGroup(economicGroup);
        }
        // StudentYearEnd set
        if (yearEnd != null && !yearEnd.equals(entity.getFinishYear()))
        {
            isChanged = true;
            entity.setFinishYear(yearEnd);
        }
        // StudentPrivacyNumber set
        if (privacyNumber != null && !privacyNumber.equals(entity.getPersonalFileNumber()))
        {
            isChanged = true;
            entity.setPersonalFileNumber(privacyNumber);
        }
        // StudentArchivePrivacyNumber set
        if (archivePrivacyNumber != null && !archivePrivacyNumber.equals(entity.getPersonalArchivalFileNumber()))
        {
            isChanged = true;
            entity.setPersonalArchivalFileNumber(archivePrivacyNumber);
        }
        // StudentArchival set
        if (isArchival != null && isArchival != entity.isArchival())
        {
            isChanged = true;
            entity.setArchival(isArchival);
        }
        // StudentArchiveDate set
        if (archiveDate != null && !archiveDate.equals(entity.getArchivingDate()))
        {
            isChanged = true;
            entity.setArchivingDate(archiveDate);
        }
        // StudentFinalQualifiyngWorkTheme set
        if (finalWorkTheme != null && !finalWorkTheme.equals(entity.getFinalQualifyingWorkTheme()))
        {
            isChanged = true;
            entity.setFinalQualifyingWorkTheme(finalWorkTheme);
        }
        // StudentFinalQualifiyngWorkAdvisor set
        if (finalWorkAdvisor != null && !finalWorkAdvisor.equals(entity.getFinalQualifyingWorkAdvisor()))
        {
            isChanged = true;
            entity.setFinalQualifyingWorkAdvisor(finalWorkAdvisor);
        }
        // StudentPracticePlace set
        if (practicePlace != null && !practicePlace.equals(entity.getPracticePlace()))
        {
            isChanged = true;
            entity.setPracticePlace(practicePlace);
        }
        // StudentEmployment set
        if (employment != null && !employment.equals(entity.getEmployment()))
        {
            isChanged = true;
            entity.setEmployment(employment);
        }
        // StudentEnrollmentOrderDate set
        if (enrollmentOrderDate != null && !enrollmentOrderDate.equals(orderData.getEduEnrollmentOrderDate()))
        {
            isOrderDataChanged = true;
            orderData.setEduEnrollmentOrderDate(enrollmentOrderDate);
        }
        // StudentEnrollmentOrderEnrDate set
        if (enrollmentOrderEnrDate != null && !enrollmentOrderEnrDate.equals(orderData.getEduEnrollmentOrderEnrDate()))
        {
            isOrderDataChanged = true;
            orderData.setEduEnrollmentOrderEnrDate(enrollmentOrderEnrDate);
        }
        // StudentEnrollmentOrderNumber set
        if (enrollmentOrderNumber != null && !enrollmentOrderNumber.equals(orderData.getEduEnrollmentOrderNumber()))
        {
            isOrderDataChanged = true;
            orderData.setEduEnrollmentOrderNumber(enrollmentOrderNumber);
        }
        // StudentDischargeOrderDate set
        if (dischargeOrderDate != null && !dischargeOrderDate.equals(orderData.getExcludeOrderDate()))
        {
            isOrderDataChanged = true;
            orderData.setExcludeOrderDate(dischargeOrderDate);
        }
        // StudentDischargeOrderNumber set
        if (dischargeOrderNumber != null && !dischargeOrderNumber.equals(orderData.getExcludeOrderNumber()))
        {
            isOrderDataChanged = true;
            orderData.setExcludeOrderNumber(dischargeOrderNumber);
        }
        // StudentComment set
        if (comment != null && !comment.equals(entity.getDescription()))
        {
            isChanged = true;
            entity.setDescription(comment);
        }

        // HumanID set
        if (person == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without HumanID!");
        if (person != null && !person.equals(entity.getPerson()))
        {
            isChanged = true;
            entity.setPerson(person);
        }
        // AcademicGroupID set
        if (group != null && !group.equals(entity.getGroup()))
        {
            isChanged = true;
            entity.setGroup(group);
        }
        // EducationalProgramID set
        if (educationOrgUnit == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without EducationalProgramID!");
        if (educationOrgUnit != null && !educationOrgUnit.equals(entity.getEducationOrgUnit()))
        {
            isChanged = true;
            entity.setEducationOrgUnit(educationOrgUnit);
            entity.setDevelopPeriodAuto(educationOrgUnit.getDevelopPeriod());
        }
        // CompensationTypeID set
        if (compensation == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without CompensationTypeID!");
        if (compensation != null && !compensation.equals(entity.getCompensationType()))
        {
            isChanged = true;
            entity.setCompensationType(compensation);
        }
        // StudentStatusID set
        if (status == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without StudentStatusID!");
        if (status != null && !status.equals(entity.getStatus()))
        {
            isChanged = true;
            entity.setStatus(status);
        }
        // CourseID set
        if (course == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without CourseID!");
        if (course != null && !course.equals(entity.getCourse()))
        {
            isChanged = true;
            entity.setCourse(course);
        }
        // StudentCategoryID set
        if (category == null && isNew)
            throw new ProcessingDatagramObjectException("StudentType object without StudentCategoryID!");
        if (category != null && !category.equals(entity.getStudentCategory()))
        {
            isChanged = true;
            entity.setStudentCategory(category);
        }
        // QualificationID set
        EducationLevelsHighSchool highSchool = entity.getEducationOrgUnit().getEducationLevelHighSchool();
        if (qualification != null && !qualification.equals(highSchool.getAssignedQualification()))
        {
            highSchool.setAssignedQualification(qualification);
            params.put(HIGH_SCHOOL, highSchool);
        }
        // ForeignLanguageID set

        if (isOrderDataChanged)
            params.put(ORDER_DATA, orderData);
        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<Student> w, StudentType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(EducationalProgramType.class, session, params, EDUCATIONAL_PROGRAM, nsiPackage);
            saveChildEntity(AcademicGroupType.class, session, params, ACADEMIC_GROUP, nsiPackage);
            saveChildEntity(CompensationTypeType.class, session, params, COMPENSATION_TYPE, nsiPackage);
            saveChildEntity(StudentStatusType.class, session, params, STUDENT_STATUS, nsiPackage);
            saveChildEntity(CourseType.class, session, params, COURSE, nsiPackage);
            saveChildEntity(StudentCategoryType.class, session, params, STUDENT_CATEGORY, nsiPackage);
            saveChildEntity(EduProgramQualificationType.class, session, params, PROGRAM_QUALIFICATION, nsiPackage);
            saveChildEntity(ForeignLanguageType.class, session, params, FOREIGN_LANGUAGE, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);

        if (params != null)
        {
            OrderData orderData = (OrderData) params.get(ORDER_DATA);
            if (orderData != null)
                session.saveOrUpdate(orderData);

            EducationLevelsHighSchool highSchool = (EducationLevelsHighSchool) params.get(HIGH_SCHOOL);
            if (highSchool != null)
                session.saveOrUpdate(highSchool);
        }
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(Student.L_PRINCIPAL);
        fields.add(Student.P_CONTRACT_NUMBER);
        fields.add(Student.P_CONTRACT_DATE);
        fields.add(Student.P_FINAL_QUALIFYING_WORK_THEME);
        fields.add(Student.L_EDU_DOCUMENT);
        fields.add(Student.P_EDU_DOCUMENT_ORIGINAL_HANDED_IN);
        fields.add(Student.P_PRACTICE_PLACE);
        fields.add(Student.L_TARGET_ADMISSION_ORG_UNIT);
        fields.add(Student.L_OFFICE_POSTAL_ADDRESS);
        fields.add(Student.P_EMPLOYMENT);
        return  fields;
    }
}
