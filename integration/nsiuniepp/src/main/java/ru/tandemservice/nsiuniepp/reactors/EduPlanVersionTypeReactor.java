/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.nsiclient.datagram.EduPlanVersionType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiDatagramUtils;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.catalog.entity.EppScriptItem;
import ru.tandemservice.uniepp.catalog.entity.codes.EppScriptItemCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.Collections;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanVersionTypeReactor extends BaseEntityReactor<EppEduPlanVersion, EduPlanVersionType>
{
    @Override
    public Class<EppEduPlanVersion> getEntityClass()
    {
        return EppEduPlanVersion.class;
    }

    @Override
    public EduPlanVersionType createDatagramObject(String guid)
    {
        EduPlanVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanVersionType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEduPlanVersion entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanVersionType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanVersionType();

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanVersionNumber(entity.getNumber());
        datagramObject.setEduPlanVersionName(entity.getFullTitle());
        datagramObject.setEduPlanVersionConfirmDate(NsiUtils.formatDate(entity.getConfirmDate()));
        datagramObject.setEduPlanVersionDevelopConditionName(entity.getEduPlan().getDevelopCondition().getTitle());
        datagramObject.setEduPlanVersionProgramFormName(entity.getEduPlan().getProgramForm().getTitle());
        datagramObject.setEduPlanVersionDevelopPeriodName(entity.getEduPlan().getDevelopGrid().getTitle());
        datagramObject.setEduPlanVersionYearsStr(entity.getEduPlan().getYearsString());

        if (null != entity.getEduPlan().getParent())
            datagramObject.setEduPlanVersionGosName(entity.getEduPlan().getParent().getTitle());

        if (entity.getEduPlan() instanceof EppEduPlanProf)
        {
            EppEduPlanProf eduPlan = (EppEduPlanProf) entity.getEduPlan();
            datagramObject.setEduPlanVersionBaseLevelName(eduPlan.getBaseLevel().getTitle());
            datagramObject.setEduPlanVersionProgramSubjectName(eduPlan.getProgramSubject().getTitleWithCode());
            datagramObject.setEduPlanVersionStandardProgramDurationName(eduPlan.getStandardProgramDurationStr());
            datagramObject.setEduPlanVersionProgramQualificationName(eduPlan.getProgramQualification().getTitle());
        }

        if (!NsiDatagramUtils.isFilesShouldNotBeIncludedInDatagram())
        {
            IScriptItem scriptItem = DataAccessServices.dao().getByCode(EppScriptItem.class, EppScriptItemCodes.UNIEPP_EDU_PLAN_VERSION);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                    scriptItem,
                    IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                    "epvIds", Collections.singletonList(entity.getId())
            );

            datagramObject.setEduPlanVersionFileName((String) scriptResult.get(IScriptExecutor.FILE_NAME));
            datagramObject.setEduPlanVersionFile((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        }

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    @Override
    public ChangedWrapper<EppEduPlanVersion> processDatagramObject(EduPlanVersionType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanVersionType is unsupported");
    }
}