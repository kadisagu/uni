/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 07.07.2015
 */
public class AcademicGroupTypeReactor extends BaseEntityReactor<Group, AcademicGroupType>
{
    private static final String COURSE = "CourseID";
    private static final String EDUCATIONAL_PROGRAM = "EducationalProgramID";
    private static final String DEPARTMENT = "DepartmentID";
    private static final String EMPLOYEE = "EmployeeID";
    private static final String CAPTAIN = "CaptainStudent";

    @Override
    public Class<Group> getEntityClass()
    {
        return Group.class;
    }

    @Override
    public void collectUnknownDatagrams(AcademicGroupType datagramObject, IUnknownDatagramsCollector collector)
    {
        AcademicGroupType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if(departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);

        AcademicGroupType.EducationalProgramID educationalProgramID = datagramObject.getEducationalProgramID();
        EducationalProgramType educationalProgram = educationalProgramID == null ? null : educationalProgramID.getEducationalProgram();
        if(educationalProgram != null)
            collector.check(educationalProgram.getID(), EducationalProgramType.class);

        AcademicGroupType.CourseID courseID = datagramObject.getCourseID();
        CourseType courseType = courseID == null ? null : courseID.getCourse();
        if(courseType != null)
            collector.check(courseType.getID(), CourseType.class);

        AcademicGroupType.EmployeeID employeeID = datagramObject.getEmployeeID();
        EmployeeType employeeType = employeeID == null ? null : employeeID.getEmployee();
        if(employeeType != null)
            collector.check(employeeType.getID(), EmployeeType.class);

        AcademicGroupType.CaptainStudent captainStudentID = datagramObject.getCaptainStudent();
        StudentType studentType = captainStudentID == null ? null : captainStudentID.getStudent();
        if(studentType != null)
            collector.check(studentType.getID(), StudentType.class);
    }

    @Override
    public AcademicGroupType createDatagramObject(String guid)
    {
        AcademicGroupType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(Group entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AcademicGroupType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAcademicGroupID(entity.getCode());
        datagramObject.setAcademicGroupArchival(NsiUtils.formatBoolean(entity.isArchival()));
        datagramObject.setAcademicGroupArchiveDate(NsiUtils.formatDate(entity.getArchivingDate()));
        datagramObject.setAcademicGroupCreationDate(NsiUtils.formatDate(entity.getCreationDate()));
        datagramObject.setAcademicGroupDescription(entity.getDescription());
        datagramObject.setAcademicGroupName(entity.getTitle());
        datagramObject.setAcademicGroupNumber(Integer.toString(entity.getNumber()));
        datagramObject.setAcademicGroupYearBegin(Integer.toString(entity.getStartEducationYear().getIntValue()));
        if(entity.getEndingYear() != null)
            datagramObject.setAcademicGroupYearEnd(Integer.toString(entity.getEndingYear().getIntValue()));

        StringBuilder warning = new StringBuilder();
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(entity);
        if(captainList != null && !captainList.isEmpty()) {
            if(captainList.size() > 1)
                warning.append("Group has more than one captains.");
            ProcessedDatagramObject<StudentType> captainStudentType = createDatagramObject(StudentType.class, captainList.get(0), commonCache, warning);
            AcademicGroupType.CaptainStudent captainStudent = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupTypeCaptainStudent();
            captainStudent.setStudent(captainStudentType.getObject());
            datagramObject.setCaptainStudent(captainStudent);
            if (captainStudentType.getList() != null)
                result.addAll(captainStudentType.getList());
        }

        ProcessedDatagramObject<CourseType> courseType = createDatagramObject(CourseType.class, entity.getCourse(), commonCache, warning);
        AcademicGroupType.CourseID courseID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupTypeCourseID();
        courseID.setCourse(courseType.getObject());
        datagramObject.setCourseID(courseID);
        if (courseType.getList() != null)
            result.addAll(courseType.getList());

        if(entity.getParent() != null) {
            ProcessedDatagramObject<DepartmentType> departmentType = createDatagramObject(DepartmentType.class, entity.getParent(), commonCache, warning);
            AcademicGroupType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupTypeDepartmentID();
            departmentID.setDepartment(departmentType.getObject());
            datagramObject.setDepartmentID(departmentID);
            if (departmentType.getList() != null)
                result.addAll(departmentType.getList());
        }

        if(entity.getCurator() != null) {
            ProcessedDatagramObject<EmployeeType> employeeType = createDatagramObject(EmployeeType.class, entity.getCurator(), commonCache, warning);
            AcademicGroupType.EmployeeID employeeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupTypeEmployeeID();
            employeeID.setEmployee(employeeType.getObject());
            datagramObject.setEmployeeID(employeeID);
            if (employeeType.getList() != null)
                result.addAll(employeeType.getList());
        }

        if(entity.getEducationOrgUnit() != null) {
            ProcessedDatagramObject<EducationalProgramType> employeeType = createDatagramObject(EducationalProgramType.class, entity.getEducationOrgUnit(), commonCache, warning);
            AcademicGroupType.EducationalProgramID educationalProgramID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicGroupTypeEducationalProgramID();
            educationalProgramID.setEducationalProgram(employeeType.getObject());
            datagramObject.setEducationalProgramID(educationalProgramID);
            if (employeeType.getList() != null)
                result.addAll(employeeType.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private Group findGroup(Integer number, Course course)
    {
        List<IExpression> expressions = new ArrayList<>();
        if(number != null)
            expressions.add(eq(property(ENTITY_ALIAS, Group.number()), value(number)));
        if(course != null)
            expressions.add(eq(property(ENTITY_ALIAS, Group.course().id()), value(course.getId())));
        if(expressions.isEmpty())
            return null;
        List<Group> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if(list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<Group> processDatagramObject(AcademicGroupType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        String code = StringUtils.trimToNull(datagramObject.getAcademicGroupID());
        Boolean archival = NsiUtils.parseBoolean(datagramObject.getAcademicGroupArchival());
        Date archiveDate = NsiUtils.parseDate(datagramObject.getAcademicGroupArchiveDate(), "AcademicGroupArchiveDate");
        Date creationDate = NsiUtils.parseDate(datagramObject.getAcademicGroupCreationDate(), "AcademicGroupCreationDate");
        String description = StringUtils.trimToNull(datagramObject.getAcademicGroupDescription());
        String title = StringUtils.trimToNull(datagramObject.getAcademicGroupName());
        Integer number = NsiUtils.parseInteger(datagramObject.getAcademicGroupNumber(), "AcademicGroupNumber");
        EducationYear yearBegin = getEducationYear(NsiUtils.parseInteger(datagramObject.getAcademicGroupYearBegin(), "AcademicGroupYearBegin"));
        EducationYear yearEnd = getEducationYear(NsiUtils.parseInteger(datagramObject.getAcademicGroupYearEnd(), "AcademicGroupYearEnd"));

        AcademicGroupType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit parent = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT, null, warning);

        AcademicGroupType.EducationalProgramID educationalProgramID = datagramObject.getEducationalProgramID();
        EducationalProgramType educationalProgram = educationalProgramID == null ? null : educationalProgramID.getEducationalProgram();
        EducationOrgUnit educationOrgUnit = getOrCreate(EducationalProgramType.class, params, commonCache, educationalProgram, EDUCATIONAL_PROGRAM, null, warning);

        AcademicGroupType.CourseID courseID = datagramObject.getCourseID();
        CourseType courseType = courseID == null ? null : courseID.getCourse();
        Course course = getOrCreate(CourseType.class, params, commonCache, courseType, COURSE, null, warning);

        AcademicGroupType.EmployeeID employeeID = datagramObject.getEmployeeID();
        EmployeeType employeeType = employeeID == null ? null : employeeID.getEmployee();
        EmployeePost curator = getOrCreate(EmployeeType.class, params, commonCache, employeeType, EMPLOYEE, null, warning);

        AcademicGroupType.CaptainStudent captainStudentID = datagramObject.getCaptainStudent();
        StudentType studentType = captainStudentID == null ? null : captainStudentID.getStudent();
        Student captain = getOrCreate(StudentType.class, params, commonCache, studentType, CAPTAIN, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        Group entity = null;
        CoreCollectionUtils.Pair<Group, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findGroup(number, course);
        if(entity == null) {
            entity = new Group();
            isNew = true;
            isChanged = true;
        }

        if(code != null && !code.equals(entity.getCode()))
        {
            isChanged = true;
            entity.setCode(code);
        }

        if(archival != null && archival != entity.isArchival())
        {
            isChanged = true;
            entity.setArchival(archival);
        }

        if(archiveDate != null && !archiveDate.equals(entity.getArchivingDate()))
        {
            isChanged = true;
            entity.setArchivingDate(archiveDate);
        }

        if(creationDate != null && !creationDate.equals(entity.getCreationDate()))
        {
            isChanged = true;
            entity.setCreationDate(creationDate);
        }
        if(entity.getCreationDate() == null)
            throw new ProcessingDatagramObjectException("AcademicGroupType object without AcademicGroupArchiveDate!");

        if(description != null && !description.equals(entity.getDescription()))
        {
            isChanged = true;
            entity.setDescription(description);
        }

        if(title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        if(isNew && yearBegin == null)
            throw new ProcessingDatagramObjectException("AcademicGroupType object without AcademicGroupYearBegin!");
        if(yearBegin != null && !yearBegin.equals(entity.getStartEducationYear()))
        {
            isChanged = true;
            entity.setStartEducationYear(yearBegin);
        }

        if(yearEnd != null && !yearEnd.equals(entity.getEndingYear()))
        {
            isChanged = true;
            entity.setEndingYear(yearEnd);
        }

        if(parent != null && !parent.equals(entity.getParent()))
        {
            isChanged = true;
            entity.setParent(parent);
        }

        if(educationOrgUnit != null && !educationOrgUnit.equals(entity.getEducationOrgUnit()))
        {
            isChanged = true;
            entity.setEducationOrgUnit(educationOrgUnit);
        }

        if(course != null && !course.equals(entity.getCourse()))
        {
            isChanged = true;
            entity.setCourse(course);
        }
        if(entity.getCourse() == null)
            throw new ProcessingDatagramObjectException("AcademicGroupType object without CourseID!");

        if(curator != null && !curator.equals(entity.getCurator()))
        {
            isChanged = true;
            entity.setCurator(curator);
        }

        if(!isNew && captain != null) {
            UniDaoFacade.getGroupDao().addCaptainStudent(entity, captain);
        }
        UniDaoFacade.getGroupDao().isCaptainStudent(entity, captain);
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(entity);
        if(captain != null && (CollectionUtils.isEmpty(captainList) || !captainList.contains(captain)))
        {
            isChanged = true;
            UniDaoFacade.getGroupDao().addCaptainStudent(entity, captain);
        }

        if(isNew && number == null)
            throw new ProcessingDatagramObjectException("AcademicGroupType object without AcademicGroupID!");
        if(number != null && number != entity.getNumber())
        {
            isChanged = true;
            entity.setNumber(number);
        }
        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    private static EducationYear getEducationYear(Integer yearInt)
    {
        if (yearInt != null)
            return DataAccessServices.dao().get(EducationYear.class, EducationYear.intValue(), yearInt);
        return null;
    }

    @Override
    public void save(Session session, ChangedWrapper<Group> w, AcademicGroupType datagramObject, NsiPackage nsiPackage)
    {
        Student captain = null;
        Map<String, Object> params = w.getParams();
        if(params != null)
        {
            saveChildEntity(CourseType.class, session, params, COURSE, nsiPackage);
            saveChildEntity(EducationalProgramType.class, session, params, EDUCATIONAL_PROGRAM, nsiPackage);
            saveChildEntity(EmployeeType.class, session, params, EMPLOYEE, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, DEPARTMENT, nsiPackage);

            ChangedWrapper<Student> wrapper = (ChangedWrapper<Student>) params.get(CAPTAIN);
            if(wrapper != null) {
                @SuppressWarnings("unchecked")
                INsiEntityReactor<Student, StudentType> reactor = (INsiEntityReactor<Student, StudentType>) ReactorHolder.getReactorMap().get(StudentType.class.getSimpleName());
                reactor.save(session, wrapper, (StudentType) params.get(CAPTAIN + PARAM), nsiPackage);

                captain = wrapper.getResult();
            }
        }

        super.save(session, w, datagramObject, nsiPackage);

        Group group = w.getResult();
        if(captain != null && !UniDaoFacade.getGroupDao().isCaptainStudent(group, captain)) {
            UniDaoFacade.getGroupDao().deleteCaptainStudentList(group);
            UniDaoFacade.getGroupDao().addCaptainStudent(group, captain);
        }
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(Group.P_SHORT_TITLE);

        return  fields;
    }
}
