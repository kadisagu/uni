/* $Id: RegistryDisciplineTypeReactor.java 43915 2015-07-08 08:26:58Z aavetisov $ */
package ru.tandemservice.nsiuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.RegistryDisciplineType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiDatagramUtils;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import ru.tandemservice.uniepp.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class RegistryDisciplineTypeReactor extends BaseEntityReactor<EppRegistryElement, RegistryDisciplineType>
{
    @Override
    public Class<EppRegistryElement> getEntityClass()
    {
        return EppRegistryElement.class;
    }

    @Override
    public RegistryDisciplineType createDatagramObject(String guid)
    {
        RegistryDisciplineType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createRegistryDisciplineType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppRegistryElement entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        RegistryDisciplineType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createRegistryDisciplineType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setRegistryDisciplineID(entity.getNumber());
        setLoad(datagramObject, entity);
        datagramObject.setRegistryDisciplineLabour(String.valueOf(entity.getLabor()));
        datagramObject.setRegistryDisciplineLoadTotal(String.valueOf(entity.getSize()));
        datagramObject.setRegistryDisciplineName(entity.getTitle());
        datagramObject.setRegistryDisciplineNameFull(entity.getFullTitle());
        datagramObject.setRegistryDisciplineNameShort(entity.getShortTitle());

        if (!NsiDatagramUtils.isFilesShouldNotBeIncludedInDatagram())
        {
            long workProgramFileSize = 0;
            if (null != entity.getWorkProgramFile())
            {
                try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME))
                {
                    final RemoteDocumentDTO dto = service.get(entity.getWorkProgramFile());
                    if (null != dto)
                    {
                        workProgramFileSize = dto.getContent().length;
                        datagramObject.setRegistryDisciplineEduProgramFileName(dto.getFileName());
                        datagramObject.setRegistryDisciplineEduProgramFile(dto.getContent());
                    }
                }
            }

            long workProgramAnnotationFileSize = 0;
            if (null != entity.getWorkProgramAnnotation())
            {
                try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME))
                {
                    final RemoteDocumentDTO dto = service.get(entity.getWorkProgramAnnotation());
                    if (null != dto)
                    {
                        workProgramAnnotationFileSize = dto.getContent().length;
                        datagramObject.setRegistryDisciplineEduProgramAnnotationFileName(dto.getFileName());
                        datagramObject.setRegistryDisciplineEduProgramAnnotationFile(dto.getContent());
                    }
                }
            }

            //TODO: Удалить, когда сделаем нормальные ограничения по загрузке файлов
            if ((workProgramAnnotationFileSize + workProgramFileSize) > (5 * 1024 * 1024))
            {
                datagramObject.setRegistryDisciplineEduProgramAnnotationFileName(null);
                datagramObject.setRegistryDisciplineEduProgramAnnotationFile(null);

                datagramObject.setRegistryDisciplineEduProgramFileName(null);
                datagramObject.setRegistryDisciplineEduProgramFile(null);
            }
        }

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    private RegistryDisciplineType setLoad(RegistryDisciplineType datagramObject, EppRegistryElement entity)
    {
        List <EppRegistryElementLoad> loadList = DataAccessServices.dao().getList(EppRegistryElementLoad.class, EppRegistryElementLoad.L_REGISTRY_ELEMENT, entity);

        int loadTeaching = 0;     // Аудиторная нагрузка (часов)
        int loadSelfEducation = 0;     // Самостоятельная нагрузка (часов)
        int loadLectures = 0;     // Лекционная нагрузка (часов)
        int loadPractice = 0;     // Практические занятия (часов)
        int loadLab = 0;     // Лабораторные занятия (часов)

        for (EppRegistryElementLoad load : loadList)
        {
            if (load.getLoadType() instanceof EppALoadType)
            {
                switch (load.getLoadType().getCode())
                {
                    case EppALoadTypeCodes.TYPE_LABS: loadLab+=load.getLoad(); break;
                    case EppALoadTypeCodes.TYPE_LECTURES: loadLectures+=load.getLoad(); break;
                    case EppALoadTypeCodes.TYPE_PRACTICE: loadPractice+=load.getLoad(); break;
                }

            }
            else if (load.getLoadType() instanceof EppELoadType)
            {
                switch (load.getLoadType().getCode())
                {
                    case EppELoadTypeCodes.TYPE_TOTAL_AUDIT: loadTeaching+=load.getLoad(); break;
                    case EppELoadTypeCodes.TYPE_TOTAL_SELFWORK: loadSelfEducation+=load.getLoad(); break;
                }
            }
        }

        datagramObject.setRegistryDisciplineLoadLab(String.valueOf(loadLab));
        datagramObject.setRegistryDisciplineLoadLectures(String.valueOf(loadLectures));
        datagramObject.setRegistryDisciplineLoadPractice(String.valueOf(loadPractice));
        datagramObject.setRegistryDisciplineLoadSelfEducation(String.valueOf(loadSelfEducation));
        datagramObject.setRegistryDisciplineLoadTeaching(String.valueOf(loadTeaching));

        return datagramObject;
    }

    @Override
    public ChangedWrapper<EppRegistryElement> processDatagramObject(RegistryDisciplineType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        String number = StringUtils.trimToNull(datagramObject.getRegistryDisciplineID());
        Long labour = NsiUtils.parseLong(datagramObject.getRegistryDisciplineLabour(), "RegistryDisciplineLabour");
        Long size = NsiUtils.parseLong(datagramObject.getRegistryDisciplineLoadTotal(), "RegistryDisciplineLoadTotal");
        String title = StringUtils.trimToNull(datagramObject.getRegistryDisciplineName());
        String fullTitle = StringUtils.trimToNull(datagramObject.getRegistryDisciplineNameFull());
        String shortTitle = StringUtils.trimToNull(datagramObject.getRegistryDisciplineNameShort());

        EppRegistryElement entity = null;
        boolean isChanged = false;
        CoreCollectionUtils.Pair<EppRegistryElement, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();

        if (entity == null) throw new ProcessingDatagramObjectException("Creation object for type RegistryDisciplineType is unsupported");

        if (number != null && !number.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(number);
        }

        if (labour != null && !labour.equals(entity.getLabor()))
        {
            isChanged = true;
            entity.setLabor(labour);
        }

        if (size != null && !size.equals(entity.getSize()))
        {
            isChanged = true;
            entity.setSize(size);
        }

        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        if (fullTitle != null && !fullTitle.equals(entity.getFullTitle()))
        {
            isChanged = true;
            entity.setFullTitle(fullTitle);
        }

        if (shortTitle != null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }
        return new ChangedWrapper<>(false, isChanged, pair.getY(), entity, null, null);
    }
}
