/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import ru.tandemservice.nsiclient.datagram.EduPlanRowTermLoadType;
import ru.tandemservice.nsiclient.datagram.EduPlanRowTermType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class EduPlanRowTermLoadTypeReactor extends BaseEntityReactor<EppEpvRowTermLoad, EduPlanRowTermLoadType>
{
    @Override
    public Class<EppEpvRowTermLoad> getEntityClass()
    {
        return EppEpvRowTermLoad.class;
    }

    @Override
    public EduPlanRowTermLoadType createDatagramObject(String guid)
    {
        EduPlanRowTermLoadType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermLoadType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppEpvRowTermLoad entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduPlanRowTermLoadType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermLoadType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduPlanRowTermLoadPriority(entity.getLoadType().getCode());
        datagramObject.setEduPlanRowTermLoadLoadTypeName(entity.getLoadType().getTitle());
        datagramObject.setEduPlanRowTermLoadLoadTypeShortName(entity.getLoadType().getShortTitle());
        datagramObject.setEduPlanRowTermLoadLoadTypeAbbreviation(entity.getLoadType().getAbbreviation());
        datagramObject.setEduPlanRowTermLoadHours(String.valueOf(entity.getHours()));
        datagramObject.setEduPlanRowTermLoadHoursE(String.valueOf(entity.getHoursE()));
        datagramObject.setEduPlanRowTermLoadHoursI(String.valueOf(entity.getHoursI()));

        StringBuilder warning = new StringBuilder();
        ProcessedDatagramObject<EduPlanRowTermType> rowTermType = createDatagramObject(EduPlanRowTermType.class, entity.getRowTerm(), commonCache, warning);
        EduPlanRowTermLoadType.EduPlanRowTermID rowTermID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduPlanRowTermLoadTypeEduPlanRowTermID();
        rowTermID.setEduPlanRowTerm(rowTermType.getObject());
        datagramObject.setEduPlanRowTermID(rowTermID);
        if (rowTermType.getList() != null) result.addAll(rowTermType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppEpvRowTermLoad> processDatagramObject(EduPlanRowTermLoadType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type EduPlanRowTermLoadType is unsupported");
    }
}