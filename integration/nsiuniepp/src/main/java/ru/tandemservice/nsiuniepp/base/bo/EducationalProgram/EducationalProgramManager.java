/* $Id:$ */
package ru.tandemservice.nsiuniepp.base.bo.EducationalProgram;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.nsiuniepp.base.bo.EducationalProgram.logic.EducationalProgramDAO;
import ru.tandemservice.nsiuniepp.base.bo.EducationalProgram.logic.IEducationalProgramDAO;

/**
 * @author Dmitry Seleznev
 * @since 28.07.2016
 */
@Configuration
public class EducationalProgramManager extends BusinessObjectManager
{
    public static EducationalProgramManager instance()
    {
        return instance(EducationalProgramManager.class);
    }

    @Bean
    public IEducationalProgramDAO dao()
    {
        return new EducationalProgramDAO();
    }
}