/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.datagram.EduPlanBlockType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.Student2EduPlanBlockType;
import ru.tandemservice.nsiclient.datagram.StudentType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.09.2016
 */
public class Student2EduPlanBlockTypeReactor extends BaseEntityReactor<EppStudent2EduPlanVersion, Student2EduPlanBlockType>
{
    @Override
    public Class<EppStudent2EduPlanVersion> getEntityClass()
    {
        return EppStudent2EduPlanVersion.class;
    }

    @Override
    public Student2EduPlanBlockType createDatagramObject(String guid)
    {
        Student2EduPlanBlockType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudent2EduPlanBlockType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EppStudent2EduPlanVersion entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        Student2EduPlanBlockType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudent2EduPlanBlockType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        StringBuilder warning = new StringBuilder();

        EppEduPlanVersionBlock block = entity.getBlock();
        if (null == block)
        {
            List<EppEduPlanVersionRootBlock> blockList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "e")
                    .column(property("e")).where(eq(property(EppEduPlanVersionRootBlock.eduPlanVersion().fromAlias("e")), value(entity.getEduPlanVersion()))));
            if (!blockList.isEmpty())
            {
                block = blockList.get(0);
            }
        }

        if (null != block)
        {
            ProcessedDatagramObject<EduPlanBlockType> eduPlanBlockType = createDatagramObject(EduPlanBlockType.class, block, commonCache, warning);
            Student2EduPlanBlockType.EduPlanBlockID blockID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudent2EduPlanBlockTypeEduPlanBlockID();
            blockID.setEduPlanBlock(eduPlanBlockType.getObject());
            datagramObject.setEduPlanBlockID(blockID);
            if (eduPlanBlockType.getList() != null) result.addAll(eduPlanBlockType.getList());
        }

        ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, entity.getStudent(), commonCache, warning);
        Student2EduPlanBlockType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudent2EduPlanBlockTypeStudentID();
        studentID.setStudent(studentType.getObject());
        datagramObject.setStudentID(studentID);
        if (studentType.getList() != null) result.addAll(studentType.getList());

        if (entity.getConfirmDate() == null) {
            datagramObject.setStudent2EduPlanBlockConfirmDate("");
        } else {
            datagramObject.setStudent2EduPlanBlockConfirmDate(NsiUtils.formatDate(entity.getConfirmDate()));
        }

        if (entity.getRemovalDate() == null) {
            datagramObject.setStudent2EduPlanBlockRemovalDate("");
        } else {
            datagramObject.setStudent2EduPlanBlockRemovalDate(NsiUtils.formatDate(entity.getRemovalDate()));
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EppStudent2EduPlanVersion> processDatagramObject(Student2EduPlanBlockType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        throw new ProcessingDatagramObjectException("Creation and update object for type Student2EduPlanBlockType is unsupported");
    }
}