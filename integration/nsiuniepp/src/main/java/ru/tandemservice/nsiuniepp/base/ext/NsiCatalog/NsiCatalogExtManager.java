/* $Id$ */
package ru.tandemservice.nsiuniepp.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<Boolean> orderableExtPoint()
    {
        return itemListExtension(_catalogManager.orderableExtPoint())
                .add(EducationOrgUnit.ENTITY_NAME, false)
                .add(EppWorkPlanRegistryElementRow.ENTITY_NAME, false)
                .add(Student.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> filterableExtPoint()
    {
        return itemListExtension(_catalogManager.filterableExtPoint())
                .add(EducationOrgUnit.ENTITY_NAME, false)
                .add(EppWorkPlanRegistryElementRow.ENTITY_NAME, false)
                .add(Student.ENTITY_NAME, false)
                .create();
    }
}
