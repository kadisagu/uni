/* $Id:$ */
package ru.tandemservice.nsiuniepp.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiuniepp.base.bo.EducationalProgram.EducationalProgramManager;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 07.07.2015
 */
public class EducationalProgramTypeReactor extends BaseEntityReactor<EducationOrgUnit, EducationalProgramType>
{
    private static final String DEVELOP_FORM = "DevelopFormID";
    private static final String DEVELOP_PERIOD = "DevelopPeriodID";
    private static final String DEVELOP_CONDITION = "DevelopConditionID";
    private static final String DEVELOP_TECH = "DevelopTechID";
    private static final String DEPARTMENT = "DepartmentID";
    private static final String TERRITORIAL_DEPARTMENT = "TerritorialDepartmentID";
    private static final String DEAN_OFFICE = "DeansOfficeID";
    private static final String RESPONSIBLE_DEPARTMENT = "ResponsibleDepartmentID";
    private static final String PRODUCTIVE_DEPARTMENT = "ProductiveDepartmentID";
    private static final String QUALIFICATION = "EduProgramQualificationID";

    @Override
    public Class<EducationOrgUnit> getEntityClass()
    {
        return EducationOrgUnit.class;
    }

    @Override
    public void collectUnknownDatagrams(EducationalProgramType datagramObject, IUnknownDatagramsCollector collector)
    {
        EducationalProgramType.DevelopFormID developFormID = datagramObject.getDevelopFormID();
        DevelopFormType developFormType = developFormID == null ? null : developFormID.getDevelopForm();
        if (developFormType != null)
            collector.check(developFormType.getID(), DevelopFormType.class);

        EducationalProgramType.DevelopPeriodID developPeriodID = datagramObject.getDevelopPeriodID();
        DevelopPeriodType developPeriodType = developPeriodID == null ? null : developPeriodID.getDevelopPeriod();
        if (developPeriodType != null)
            collector.check(developPeriodType.getID(), DevelopPeriodType.class);

        EducationalProgramType.DevelopConditionID developConditionID = datagramObject.getDevelopConditionID();
        DevelopConditionType developConditionType = developConditionID == null ? null : developConditionID.getDevelopCondition();
        if (developConditionType != null)
            collector.check(developConditionType.getID(), DevelopConditionType.class);

        EducationalProgramType.DevelopTechID developTechID = datagramObject.getDevelopTechID();
        DevelopTechType developTechType = developTechID == null ? null : developTechID.getDevelopTech();
        if (developTechType != null)
            collector.check(developTechType.getID(), DevelopTechType.class);

        EducationalProgramType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if (departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);

        EducationalProgramType.TerritorialDepartmentID territorialDepartmentID = datagramObject.getTerritorialDepartmentID();
        DepartmentType territorialDepartmentType = territorialDepartmentID == null ? null : territorialDepartmentID.getDepartment();
        if (territorialDepartmentType != null)
            collector.check(territorialDepartmentType.getID(), DepartmentType.class);

        EducationalProgramType.DeansOfficeID deansOfficeID = datagramObject.getDeansOfficeID();
        DepartmentType deansDepartmentType = deansOfficeID == null ? null : deansOfficeID.getDepartment();
        if (deansDepartmentType != null)
            collector.check(deansDepartmentType.getID(), DepartmentType.class);

        EducationalProgramType.ResponsibleDepartmentID responsibleDepartmentID = datagramObject.getResponsibleDepartmentID();
        DepartmentType responsibleDepartmentType = responsibleDepartmentID == null ? null : responsibleDepartmentID.getDepartment();
        if (responsibleDepartmentType != null)
            collector.check(responsibleDepartmentType.getID(), DepartmentType.class);

        EducationalProgramType.ProductiveDepartmentID productiveDepartmentID = datagramObject.getProductiveDepartmentID();
        DepartmentType productiveDepartmentType = productiveDepartmentID == null ? null : productiveDepartmentID.getDepartment();
        if (productiveDepartmentType != null)
            collector.check(productiveDepartmentType.getID(), DepartmentType.class);

        EducationalProgramType.EduProgramQualificationID qualificationID = datagramObject.getEduProgramQualificationID();
        EduProgramQualificationType qualificationType = qualificationID == null ? null : qualificationID.getQualification();
        if (qualificationType != null)
            collector.check(qualificationType.getID(), EduProgramQualificationType.class);
    }

    @Override
    public EducationalProgramType createDatagramObject(String guid)
    {
        EducationalProgramType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EducationOrgUnit entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EducationalProgramType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramType();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(8);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEducationalProgramID(Integer.toString(entity.getCode()));
        datagramObject.setEducationalProgramName(entity.getEducationLevelHighSchool().getTitle());

        if (null != entity.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject())
        {
            EduProgramKind kind = entity.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind();
            datagramObject.setEducationalProgramProgramBachelorDegree(NsiUtils.formatBoolean(kind.isProgramBachelorDegree()));
            datagramObject.setEducationalProgramProgramSpecialistDegree(NsiUtils.formatBoolean(kind.isProgramSpecialistDegree()));
            datagramObject.setEducationalProgramProgramMasterDegree(NsiUtils.formatBoolean(kind.isProgramMasterDegree()));
            datagramObject.setEducationalProgramProgramPostgraduate(NsiUtils.formatBoolean(kind.isProgramPostgraduate()));
            datagramObject.setEducationalProgramProgramTraineeship(NsiUtils.formatBoolean(kind.isProgramTraineeship()));
            datagramObject.setEducationalProgramProgramInternship(NsiUtils.formatBoolean(kind.isProgramInternship()));
            datagramObject.setEducationalProgramProgramBasic(NsiUtils.formatBoolean(kind.isProgramBasic()));
            datagramObject.setEducationalProgramProgramProf(NsiUtils.formatBoolean(kind.isProgramProf()));
            datagramObject.setEducationalProgramProgramSecondaryProf(NsiUtils.formatBoolean(kind.isProgramSecondaryProf()));
        }

        EducationLevels educationLevels = entity.getEducationLevelHighSchool().getEducationLevel();
        if (educationLevels.isProfileOrSpecialization())
        {
            datagramObject.setEducationalProgramProfileOKSO(educationLevels.getInheritedOkso());
            datagramObject.setEducationalProgramOKSO(educationLevels.getParentLevel() != null ? educationLevels.getParentLevel().getInheritedOkso() : null);
        }
        else
            datagramObject.setEducationalProgramOKSO(educationLevels.getInheritedOkso());
        EduProgramSubject eduProgramSubject = educationLevels.getEduProgramSubject();
        if (eduProgramSubject != null)
        {
            EppGeneration generation = IEppEduStdDAO.instance.get().getGeneration(entity.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject());
            if (generation != null) datagramObject.setGenerationGos(generation.getTitle());
        }

        StringBuilder warning = new StringBuilder();
        ProcessedDatagramObject<DevelopFormType> developFormType = createDatagramObject(DevelopFormType.class, entity.getDevelopForm(), commonCache, warning);
        EducationalProgramType.DevelopFormID developFormID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeDevelopFormID();
        developFormID.setDevelopForm(developFormType.getObject());
        datagramObject.setDevelopFormID(developFormID);
        if (developFormType.getList() != null)
            result.addAll(developFormType.getList());

        ProcessedDatagramObject<DevelopPeriodType> developPeriodType = createDatagramObject(DevelopPeriodType.class, entity.getDevelopPeriod(), commonCache, warning);
        EducationalProgramType.DevelopPeriodID developPeriodID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeDevelopPeriodID();
        developPeriodID.setDevelopPeriod(developPeriodType.getObject());
        datagramObject.setDevelopPeriodID(developPeriodID);
        if (developPeriodType.getList() != null)
            result.addAll(developPeriodType.getList());

        ProcessedDatagramObject<DevelopConditionType> developConditionType = createDatagramObject(DevelopConditionType.class, entity.getDevelopCondition(), commonCache, warning);
        EducationalProgramType.DevelopConditionID developConditionID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeDevelopConditionID();
        developConditionID.setDevelopCondition(developConditionType.getObject());
        datagramObject.setDevelopConditionID(developConditionID);
        if (developConditionType.getList() != null)
            result.addAll(developConditionType.getList());

        ProcessedDatagramObject<DevelopTechType> developTechType = createDatagramObject(DevelopTechType.class, entity.getDevelopTech(), commonCache, warning);
        EducationalProgramType.DevelopTechID developTechID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeDevelopTechID();
        developTechID.setDevelopTech(developTechType.getObject());
        datagramObject.setDevelopTechID(developTechID);
        if (developTechType.getList() != null)
            result.addAll(developTechType.getList());

        ProcessedDatagramObject<DepartmentType> departmentType = createDatagramObject(DepartmentType.class, entity.getFormativeOrgUnit(), commonCache, warning);
        EducationalProgramType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeDepartmentID();
        departmentID.setDepartment(departmentType.getObject());
        datagramObject.setDepartmentID(departmentID);
        if (departmentType.getList() != null)
            result.addAll(departmentType.getList());

        ProcessedDatagramObject<DepartmentType> territorialDepartmentType = createDatagramObject(DepartmentType.class, entity.getTerritorialOrgUnit(), commonCache, warning);
        EducationalProgramType.TerritorialDepartmentID territorialDepartmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeTerritorialDepartmentID();
        territorialDepartmentID.setDepartment(territorialDepartmentType.getObject());
        datagramObject.setTerritorialDepartmentID(territorialDepartmentID);
        if (territorialDepartmentType.getList() != null)
            result.addAll(territorialDepartmentType.getList());

        OrgUnit groupOrgUnit = entity.getGroupOrgUnit();
        if (groupOrgUnit != null)
        {
            ProcessedDatagramObject<DepartmentType> deansDepartmentType = createDatagramObject(DepartmentType.class, groupOrgUnit, commonCache, warning);
            EducationalProgramType.DeansOfficeID deansOfficeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeDeansOfficeID();
            deansOfficeID.setDepartment(deansDepartmentType.getObject());
            datagramObject.setDeansOfficeID(deansOfficeID);
            if (deansDepartmentType.getList() != null)
                result.addAll(deansDepartmentType.getList());
        }

        OrgUnit operationOrgUnit = entity.getOperationOrgUnit();
        if (operationOrgUnit != null)
        {
            ProcessedDatagramObject<DepartmentType> operationDepartmentType = createDatagramObject(DepartmentType.class, operationOrgUnit, commonCache, warning);
            EducationalProgramType.ResponsibleDepartmentID responsibleDepartmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeResponsibleDepartmentID();
            responsibleDepartmentID.setDepartment(operationDepartmentType.getObject());
            datagramObject.setResponsibleDepartmentID(responsibleDepartmentID);
            if (operationDepartmentType.getList() != null)
                result.addAll(operationDepartmentType.getList());
        }

        OrgUnit productiveOrgUnit = entity.getEducationLevelHighSchool().getOrgUnit();
        if (productiveOrgUnit != null)
        {
            ProcessedDatagramObject<DepartmentType> productiveDepartmentType = createDatagramObject(DepartmentType.class, productiveOrgUnit, commonCache, warning);
            EducationalProgramType.ProductiveDepartmentID productiveDepartmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeProductiveDepartmentID();
            productiveDepartmentID.setDepartment(productiveDepartmentType.getObject());
            datagramObject.setProductiveDepartmentID(productiveDepartmentID);
            if (productiveDepartmentType.getList() != null)
                result.addAll(productiveDepartmentType.getList());
        }

        EduProgramQualification qualification = entity.getEducationLevelHighSchool().getAssignedQualification();
        if (qualification != null)
        {
            ProcessedDatagramObject<EduProgramQualificationType> qualificationType = createDatagramObject(EduProgramQualificationType.class, qualification, commonCache, warning);
            EducationalProgramType.EduProgramQualificationID qualificationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationalProgramTypeEduProgramQualificationID();
            qualificationID.setQualification(qualificationType.getObject());
            datagramObject.setEduProgramQualificationID(qualificationID);
            if (qualificationType.getList() != null)
                result.addAll(qualificationType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private EducationOrgUnit findEducationOrgUnit(Integer code, String title, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit, EducationLevelsHighSchool educationLevelHighSchool, DevelopForm developForm, DevelopTech developTech, DevelopCondition developCondition, DevelopPeriod developPeriod)
    {
        if (code != null)
        {
            List<EducationOrgUnit> list = findEntity(eq(property(ENTITY_ALIAS, EducationOrgUnit.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<EducationOrgUnit> list = findEntity(eq(property(ENTITY_ALIAS, EducationOrgUnit.shortTitle()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        List<IExpression> expressions = new ArrayList<>();
        if (formativeOrgUnit != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.formativeOrgUnit().id()), value(formativeOrgUnit)));
        if (territorialOrgUnit != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.territorialOrgUnit().id()), value(territorialOrgUnit)));
        if (educationLevelHighSchool != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.educationLevelHighSchool().id()), value(educationLevelHighSchool)));
        if (developForm != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.developForm().id()), value(developForm)));
        if (developTech != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.developTech().id()), value(developTech)));
        if (developCondition != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.developCondition().id()), value(developCondition)));
        if (developPeriod != null)
            expressions.add(eq(property(ENTITY_ALIAS, EducationOrgUnit.developPeriod().id()), value(developPeriod)));
        if (expressions.isEmpty())
            return null;
        List<EducationOrgUnit> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EducationOrgUnit> processDatagramObject(EducationalProgramType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EducationalProgramID init
        String code = StringUtils.trimToNull(datagramObject.getEducationalProgramID());
        Integer numberInt = null;
        if (code != null)
        {
            try
            {
                numberInt = Integer.parseInt(code);
            }
            catch (NumberFormatException e)
            {
                throw new ProcessingDatagramObjectException("EducationalProgramID is not number: " + code + '!');
            }
        }
        // EducationalProgramName init
        String name = StringUtils.trimToNull(datagramObject.getEducationalProgramName());
        // EducationalProgramOKSO init
        String okso = StringUtils.trimToNull(datagramObject.getEducationalProgramOKSO());
        // EducationalProgramProfileOKSO init
        String profileOkso = StringUtils.trimToNull(datagramObject.getEducationalProgramProfileOKSO());
        // GenerationGos init
        String gos = StringUtils.trimToNull(datagramObject.getGenerationGos());
        // DevelopFormID init
        EducationalProgramType.DevelopFormID developFormID = datagramObject.getDevelopFormID();
        DevelopFormType developFormType = developFormID == null ? null : developFormID.getDevelopForm();
        DevelopForm developForm = getOrCreate(DevelopFormType.class, params, commonCache, developFormType, DEVELOP_FORM, null, warning);
        // DevelopPeriodID init
        EducationalProgramType.DevelopPeriodID developPeriodID = datagramObject.getDevelopPeriodID();
        DevelopPeriodType developPeriodType = developPeriodID == null ? null : developPeriodID.getDevelopPeriod();
        DevelopPeriod developPeriod = getOrCreate(DevelopPeriodType.class, params, commonCache, developPeriodType, DEVELOP_PERIOD, null, warning);
        // DevelopConditionID init
        EducationalProgramType.DevelopConditionID developConditionID = datagramObject.getDevelopConditionID();
        DevelopConditionType developConditionType = developConditionID == null ? null : developConditionID.getDevelopCondition();
        DevelopCondition condition = getOrCreate(DevelopConditionType.class, params, commonCache, developConditionType, DEVELOP_CONDITION, null, warning);
        // DevelopTechID init
        EducationalProgramType.DevelopTechID developTechID = datagramObject.getDevelopTechID();
        DevelopTechType developTechType = developTechID == null ? null : developTechID.getDevelopTech();
        DevelopTech tech = getOrCreate(DevelopTechType.class, params, commonCache, developTechType, DEVELOP_TECH, null, warning);
        // DepartmentID init
        EducationalProgramType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit formativeOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT, null, warning);
        // TerritorialDepartmentID init
        EducationalProgramType.TerritorialDepartmentID territorialDepartmentID = datagramObject.getTerritorialDepartmentID();
        DepartmentType territorialDepartmentType = territorialDepartmentID == null ? null : territorialDepartmentID.getDepartment();
        OrgUnit territorialOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, territorialDepartmentType, TERRITORIAL_DEPARTMENT, null, warning);
        // DeansOfficeID init
        EducationalProgramType.DeansOfficeID deansOfficeID = datagramObject.getDeansOfficeID();
        DepartmentType deansOfficeDepartmentType = deansOfficeID == null ? null : deansOfficeID.getDepartment();
        OrgUnit groupOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, deansOfficeDepartmentType, DEAN_OFFICE, null, warning);
        // ResponsibleDepartmentID init
        EducationalProgramType.ResponsibleDepartmentID responsibleDepartmentID = datagramObject.getResponsibleDepartmentID();
        DepartmentType responsibleDepartmentType = responsibleDepartmentID == null ? null : responsibleDepartmentID.getDepartment();
        OrgUnit operationOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, responsibleDepartmentType, RESPONSIBLE_DEPARTMENT, null, warning);
        // ProductiveDepartmentID init
        EducationalProgramType.ProductiveDepartmentID productiveDepartmentID = datagramObject.getProductiveDepartmentID();
        DepartmentType productiveDepartmentType = productiveDepartmentID == null ? null : productiveDepartmentID.getDepartment();
        OrgUnit productiveOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, productiveDepartmentType, PRODUCTIVE_DEPARTMENT, null, warning);
        // QualificationID init
        EducationalProgramType.EduProgramQualificationID qualificationID = datagramObject.getEduProgramQualificationID();
        EduProgramQualificationType qualificationType = qualificationID == null ? null : qualificationID.getQualification();
        EduProgramQualification qualification = getOrCreate(EduProgramQualificationType.class, params, commonCache, qualificationType, QUALIFICATION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EducationOrgUnit entity = null;
        CoreCollectionUtils.Pair<EducationOrgUnit, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEducationOrgUnit(numberInt, name, formativeOrgUnit, territorialOrgUnit, null, developForm, tech, condition, developPeriod);
        if (entity == null)
        {
            entity = new EducationOrgUnit();
            //throw new ProcessingDatagramObjectException("Creation object for type EducationalProgramType is unsupported");
        }

        // EducationalProgramID set
        if (numberInt == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without EducationalProgramID!");
        if (numberInt != entity.getCode())
        {
            isChanged = true;
            entity.setCode(numberInt);
        }
        // EducationalProgramName set
        if (name == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without EducationalProgramName!");
        if (!name.equals(entity.getEducationLevelHighSchool().getTitle()))
        {
            isChanged = true;
            entity.getEducationLevelHighSchool().setTitle(name);
        }
        // EducationalProgramOKSO set
        // EducationalProgramProfileOKSO set
        // GenerationGos set
        // DevelopFormID set
        if (developForm == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without DevelopFormID!");
        else if (!developForm.equals(entity.getDevelopForm()))
        {
            isChanged = true;
            entity.setDevelopForm(developForm);
        }
        // DevelopPeriodID set
        if (developPeriod == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without DevelopPeriodID!");
        else if (!developPeriod.equals(entity.getDevelopPeriod()))
        {
            isChanged = true;
            entity.setDevelopPeriod(developPeriod);
        }
        // DevelopConditionID set
        if (condition == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without DevelopConditionID!");
        else if (!condition.equals(entity.getDevelopCondition()))
        {
            isChanged = true;
            entity.setDevelopCondition(condition);
        }
        // DevelopTechID set
        if (tech == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without DevelopTechID!");
        else if (!tech.equals(entity.getDevelopTech()))
        {
            isChanged = true;
            entity.setDevelopTech(tech);
        }
        // DepartmentID set
        if (formativeOrgUnit == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without DepartmentID!");
        else if (!formativeOrgUnit.equals(entity.getFormativeOrgUnit()))
        {
            isChanged = true;
            entity.setFormativeOrgUnit(formativeOrgUnit);
        }
        // TerritorialDepartmentID set
        if (territorialOrgUnit == null)
            throw new ProcessingDatagramObjectException("EducationalProgramType object without TerritorialDepartmentID!");
        else if (!territorialOrgUnit.equals(entity.getTerritorialOrgUnit()))
        {
            isChanged = true;
            entity.setTerritorialOrgUnit(territorialOrgUnit);
        }
        // DeansOfficeID set
        if (groupOrgUnit != null && !groupOrgUnit.equals(entity.getGroupOrgUnit()))
        {
            isChanged = true;
            entity.setGroupOrgUnit(groupOrgUnit);
        }
        // ResponsibleDepartmentID set
        if (operationOrgUnit != null && !operationOrgUnit.equals(entity.getOperationOrgUnit()))
        {
            isChanged = true;
            entity.setOperationOrgUnit(operationOrgUnit);
        }
        // todo ?????????
        //entity.setEducationLevelHighSchool(null);

        EducationLevelsHighSchool elhs = EducationalProgramManager.instance().dao().doGetOrCreateEduLevelHS(name, okso, formativeOrgUnit, null, gos, null);
        entity.setEducationLevelHighSchool(elhs);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EducationOrgUnit> w, EducationalProgramType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(DevelopFormType.class, session, params, DEVELOP_FORM, nsiPackage);
            saveChildEntity(DevelopPeriodType.class, session, params, DEVELOP_PERIOD, nsiPackage);
            saveChildEntity(DevelopConditionType.class, session, params, DEVELOP_CONDITION, nsiPackage);
            saveChildEntity(DevelopTechType.class, session, params, DEVELOP_TECH, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, DEPARTMENT, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, TERRITORIAL_DEPARTMENT, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, DEAN_OFFICE, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, RESPONSIBLE_DEPARTMENT, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, PRODUCTIVE_DEPARTMENT, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, QUALIFICATION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
