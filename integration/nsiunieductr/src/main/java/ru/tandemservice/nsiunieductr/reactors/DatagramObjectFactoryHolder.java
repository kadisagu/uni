/* $Id$ */
package ru.tandemservice.nsiunieductr.reactors;

/**
 * @author Andrey Avetisov
 * @since 25.01.2016
 */
public class DatagramObjectFactoryHolder
{
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
}
