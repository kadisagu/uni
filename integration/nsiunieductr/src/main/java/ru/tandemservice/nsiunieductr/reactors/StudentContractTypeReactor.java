/* $Id$ */
package ru.tandemservice.nsiunieductr.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContractObjectType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.StudentContractType;
import ru.tandemservice.nsiclient.datagram.StudentType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 25.01.2016
 */
public class StudentContractTypeReactor extends BaseEntityReactor<EduCtrStudentContract, StudentContractType>
{
    private static final String CONTRACT = "ContractObjectID";
    private static final String STUDENT = "StudentID";

    @Override
    public Class<EduCtrStudentContract> getEntityClass()
    {
        return EduCtrStudentContract.class;
    }

    @Override
    public StudentContractType createDatagramObject(String guid)
    {
        StudentContractType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentContractType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public void collectUnknownDatagrams(StudentContractType datagramObject, IUnknownDatagramsCollector collector)
    {
        StudentContractType.ContractObjectID contractorID = datagramObject.getContractObjectID();
        ContractObjectType contractor = contractorID == null ? null : contractorID.getContractObject();
        if (contractor != null)
            collector.check(contractor.getID(), ContractObjectType.class);

        StudentContractType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        if (studentType != null)
            collector.check(studentType.getID(), StudentType.class);
    }

    @Override
    public ResultListWrapper createDatagramObject(EduCtrStudentContract entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        StudentContractType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentContractType();
        datagramObject.setID(nsiEntity.getGuid());
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        ProcessedDatagramObject<ContractObjectType> contractObjectType = createDatagramObject(ContractObjectType.class, entity.getContractObject(), commonCache, warning);
        StudentContractType.ContractObjectID contractObjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentContractTypeContractObjectID();
        contractObjectID.setContractObject(contractObjectType.getObject());
        datagramObject.setContractObjectID(contractObjectID);
        if (contractObjectType.getList() != null)
            result.addAll(contractObjectType.getList());

        ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, entity.getStudent(), commonCache, warning);
        StudentContractType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentContractTypeStudentID();
        studentID.setStudent(studentType.getObject());
        datagramObject.setStudentID(studentID);
        if (studentType.getList() != null)
            result.addAll(studentType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EduCtrStudentContract findEduCtrStudentContract(CtrContractObject contractObject, Student student)
    {
        if (contractObject != null && student != null)
        {
            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, EduCtrStudentContract.contractObject().id()), value(contractObject.getId())));
            expressions.add(eq(property(ENTITY_ALIAS, EduCtrStudentContract.student().id()), value(student.getId())));
            List<EduCtrStudentContract> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduCtrStudentContract> processDatagramObject(@NotNull StudentContractType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        assert datagramObject.getID() != null;
        StringBuilder warning = new StringBuilder();

        // ContactorPersonID init
        StudentContractType.ContractObjectID contractID = datagramObject.getContractObjectID();
        ContractObjectType contractObjectType = contractID == null ? null : contractID.getContractObject();
        CtrContractObject contract = getOrCreate(ContractObjectType.class, params, commonCache, contractObjectType, CONTRACT, null, warning);

        // EnrRequestedCompetitionID init
        StudentContractType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        Student student = getOrCreate(StudentType.class, params, commonCache, studentType, STUDENT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EduCtrStudentContract entity = null;
        CoreCollectionUtils.Pair<EduCtrStudentContract, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);

        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduCtrStudentContract(contract, student);
        if (entity == null)
        {
            entity = new EduCtrStudentContract();
            isNew = true;
            isChanged = true;
        }

        if (contract != null && !contract.equals(entity.getContractObject()))
        {
            isChanged = true;
            entity.setContractObject(contract);
        }
        if (null == entity.getContractObject())
            throw new ProcessingDatagramObjectException("StudentContractType object without ContractObjectID!");

        if (student != null && !student.equals(entity.getStudent()))
        {
            isChanged = true;
            entity.setStudent(student);
        }
        if (null == entity.getStudent())
            throw new ProcessingDatagramObjectException("StudentContractType object without StudentID!");

        boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EduCtrStudentContract.class, entity, EduCtrStudentContract.L_CONTRACT_OBJECT, EduCtrStudentContract.L_STUDENT);
        if (!isUnique)
        {
            throw new ProcessingDatagramObjectException("StudentContractType object's student/contract combination is not unique");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EduCtrStudentContract> w, StudentContractType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(ContractObjectType.class, session, params, CONTRACT, nsiPackage);
            saveChildEntity(StudentType.class, session, params, STUDENT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
