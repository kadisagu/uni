/* $Id$ */
package ru.tandemservice.nsiunieductr.reactors;

import ru.tandemservice.nsiclient.datagram.*;

import javax.xml.bind.annotation.XmlRegistry;
/**
 * @author Andrey Avetisov
 * @since 25.01.2016
 */

/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.tandemservice.nsiclient.datagram package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory
{
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.nsiclient.datagram
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonForeignLanguageType }
     */
    public PersonForeignLanguageType createPersonForeignLanguageType()
    {
        return new PersonForeignLanguageType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType }
     */
    public PersonEduDocumentType createPersonEduDocumentType()
    {
        return new PersonEduDocumentType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.OrganizationType }
     */
    public OrganizationType createOrganizationType()
    {
        return new OrganizationType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.IdentityCardType }
     */
    public IdentityCardType createIdentityCardType()
    {
        return new IdentityCardType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.HumanType }
     */
    public HumanType createHumanType()
    {
        return new HumanType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.FamilyStructureType }
     */
    public FamilyStructureType createFamilyStructureType()
    {
        return new FamilyStructureType();
    }

    /**
     * Create an instance of {@link StudentType }
     */
    public StudentType createStudentType()
    {
        return new StudentType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EmployeeType }
     */
    public EmployeeType createEmployeeType()
    {
        return new EmployeeType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduSpecializationBaseType }
     */
    public EduSpecializationBaseType createEduSpecializationBaseType()
    {
        return new EduSpecializationBaseType();
    }


    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramHigherProfType }
     */
    public EduProgramHigherProfType createEduProgramHigherProfType()
    {
        return new EduProgramHigherProfType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduLevelType }
     */
    public EduLevelType createEduLevelType()
    {
        return new EduLevelType();
    }

//    /**
//     * Create an instance of {@link EduCtrContractVersionTemplateDataType }
//     *
//     */
//    public EduCtrContractVersionTemplateDataType createEduCtrContractVersionTemplateDataType() {
//        return new EduCtrContractVersionTemplateDataType();
//    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.DepartmentType }
     */
    public DepartmentType createDepartmentType()
    {
        return new DepartmentType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.AddressType }
     */
    public AddressType createAddressType()
    {
        return new AddressType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduDocumentKindType }
     */
    public EduDocumentKindType createEduDocumentKindType()
    {
        return new EduDocumentKindType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramDurationType }
     */
    public EduProgramDurationType createEduProgramDurationType()
    {
        return new EduProgramDurationType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramFormType }
     */
    public EduProgramFormType createEduProgramFormType()
    {
        return new EduProgramFormType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramQualificationType }
     */
    public EduProgramQualificationType createEduProgramQualificationType()
    {
        return new EduProgramQualificationType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramSubjectType }
     */
    public EduProgramSubjectType createEduProgramSubjectType()
    {
        return new EduProgramSubjectType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramTraitType }
     */
    public EduProgramTraitType createEduProgramTraitType()
    {
        return new EduProgramTraitType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.ForeignLanguageType }
     */
    public ForeignLanguageType createForeignLanguageType()
    {
        return new ForeignLanguageType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.ForeignLanguageSkillType }
     */
    public ForeignLanguageSkillType createForeignLanguageSkillType()
    {
        return new ForeignLanguageSkillType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.GradeType }
     */
    public GradeType createGradeType()
    {
        return new GradeType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.GraduationHonourType }
     */
    public GraduationHonourType createGraduationHonourType()
    {
        return new GraduationHonourType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.IdentityCardKindType }
     */
    public IdentityCardKindType createIdentityCardKindType()
    {
        return new IdentityCardKindType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.OksmType }
     */
    public OksmType createOksmType()
    {
        return new OksmType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.QualificationType }
     */
    public QualificationType createQualificationType()
    {
        return new QualificationType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PostType }
     */
    public PostType createPostType()
    {
        return new PostType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.RelDegreeType }
     */
    public RelDegreeType createRelDegreeType()
    {
        return new RelDegreeType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonForeignLanguageType.HumanID }
     */
    public PersonForeignLanguageType.HumanID createPersonForeignLanguageTypeHumanID()
    {
        return new PersonForeignLanguageType.HumanID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonForeignLanguageType.ForeignLanguageID }
     */
    public PersonForeignLanguageType.ForeignLanguageID createPersonForeignLanguageTypeForeignLanguageID()
    {
        return new PersonForeignLanguageType.ForeignLanguageID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonForeignLanguageType.ForeignLanguageSkillID }
     */
    public PersonForeignLanguageType.ForeignLanguageSkillID createPersonForeignLanguageTypeForeignLanguageSkillID()
    {
        return new PersonForeignLanguageType.ForeignLanguageSkillID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType.HumanID }
     */
    public PersonEduDocumentType.HumanID createPersonEduDocumentTypeHumanID()
    {
        return new PersonEduDocumentType.HumanID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType.EduDocumentKindID }
     */
    public PersonEduDocumentType.EduDocumentKindID createPersonEduDocumentTypeEduDocumentKindID()
    {
        return new PersonEduDocumentType.EduDocumentKindID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType.AddressID }
     */
    public PersonEduDocumentType.AddressID createPersonEduDocumentTypeAddressID()
    {
        return new PersonEduDocumentType.AddressID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType.EduLevelID }
     */
    public PersonEduDocumentType.EduLevelID createPersonEduDocumentTypeEduLevelID()
    {
        return new PersonEduDocumentType.EduLevelID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType.GraduationHonourID }
     */
    public PersonEduDocumentType.GraduationHonourID createPersonEduDocumentTypeGraduationHonourID()
    {
        return new PersonEduDocumentType.GraduationHonourID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.OrganizationType.OrganizationUpperID }
     */
    public OrganizationType.OrganizationUpperID createOrganizationTypeOrganizationUpperID()
    {
        return new OrganizationType.OrganizationUpperID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.IdentityCardType.HumanID }
     */
    public IdentityCardType.HumanID createIdentityCardTypeHumanID()
    {
        return new IdentityCardType.HumanID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.IdentityCardType.IdentityCardKindID }
     */
    public IdentityCardType.IdentityCardKindID createIdentityCardTypeIdentityCardKindID()
    {
        return new IdentityCardType.IdentityCardKindID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.HumanType.HumanCitizenshipID }
     */
    public HumanType.HumanCitizenshipID createHumanTypeHumanCitizenshipID()
    {
        return new HumanType.HumanCitizenshipID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.FamilyStructureType.FamilyStructureRelDegreeID }
     */
    public FamilyStructureType.FamilyStructureRelDegreeID createFamilyStructureTypeFamilyStructureRelDegreeID()
    {
        return new FamilyStructureType.FamilyStructureRelDegreeID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.FamilyStructureType.HumanID }
     */
    public FamilyStructureType.HumanID createFamilyStructureTypeHumanID()
    {
        return new FamilyStructureType.HumanID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EmployeeType.OrganizationID }
     */
    public EmployeeType.OrganizationID createEmployeeTypeOrganizationID()
    {
        return new EmployeeType.OrganizationID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EmployeeType.HumanID }
     */
    public EmployeeType.HumanID createEmployeeTypeHumanID()
    {
        return new EmployeeType.HumanID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EmployeeType.EmployeeGradeID }
     */
    public EmployeeType.EmployeeGradeID createEmployeeTypeEmployeeGradeID()
    {
        return new EmployeeType.EmployeeGradeID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EmployeeType.EmployeeDepartmentID }
     */
    public EmployeeType.EmployeeDepartmentID createEmployeeTypeEmployeeDepartmentID()
    {
        return new EmployeeType.EmployeeDepartmentID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EmployeeType.PostID }
     */
    public EmployeeType.PostID createEmployeeTypePostID()
    {
        return new EmployeeType.PostID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduSpecializationBaseType.EduProgramSubjectID }
     */
    public EduSpecializationBaseType.EduProgramSubjectID createEduSpecializationBaseTypeEduProgramSubjectID()
    {
        return new EduSpecializationBaseType.EduProgramSubjectID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramHigherProfType.EduSpecializationBaseID }
     */
    public EduProgramHigherProfType.EduSpecializationBaseID createEduProgramHigherProfTypeEduSpecializationBaseID()
    {
        return new EduProgramHigherProfType.EduSpecializationBaseID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduProgramHigherProfType.EduProgramQualificationID }
     */
    public EduProgramHigherProfType.EduProgramQualificationID createEduProgramHigherProfTypeEduProgramQualificationID()
    {
        return new EduProgramHigherProfType.EduProgramQualificationID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.EduLevelType.ParentEduLevelID }
     */
    public EduLevelType.ParentEduLevelID createEduLevelTypeParentEduLevelID()
    {
        return new EduLevelType.ParentEduLevelID();
    }

//    /**
//     * Create an instance of {@link EduCtrContractVersionTemplateDataType.EnrRequestedCompetitionID }
//     *
//     */
//    public EduCtrContractVersionTemplateDataType.EnrRequestedCompetitionID createEduCtrContractVersionTemplateDataTypeEnrRequestedCompetitionID() {
//        return new EduCtrContractVersionTemplateDataType.EnrRequestedCompetitionID();
//    }
//
//    /**
//     * Create an instance of {@link EduCtrContractVersionTemplateDataType.ContactPersonID }
//     *
//     */
//    public EduCtrContractVersionTemplateDataType.ContactPersonID createEduCtrContractVersionTemplateDataTypeContactPersonID() {
//        return new EduCtrContractVersionTemplateDataType.ContactPersonID();
//    }
//

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.DepartmentType.OrganizationID }
     */
    public DepartmentType.OrganizationID createDepartmentTypeOrganizationID()
    {
        return new DepartmentType.OrganizationID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.DepartmentType.DepartmentUpperID }
     */
    public DepartmentType.DepartmentUpperID createDepartmentTypeDepartmentUpperID()
    {
        return new DepartmentType.DepartmentUpperID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.AddressType.OksmID }
     */
    public AddressType.OksmID createAddressTypeOksmID()
    {
        return new AddressType.OksmID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.AddressType.ParentAddressID }
     */
    public AddressType.ParentAddressID createAddressTypeParentAddressID()
    {
        return new AddressType.ParentAddressID();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.ContactPersonType }
     */
    public ContactPersonType createContractorType()
    {
        return new ContactPersonType();
    }

    /**
     * Create an instance of {@link StudentContractType}
     */
    public StudentContractType createStudentContractType()
    {
        return new StudentContractType();
    }

    /**
     * Create an instance of {@link StudentContractType.ContractObjectID }
     */
    public StudentContractType.ContractObjectID createStudentContractTypeContractObjectID()
    {
        return new StudentContractType.ContractObjectID();
    }

    /**
     * Create an instance of {@link StudentContractType.StudentID }
     */
    public StudentContractType.StudentID createStudentContractTypeStudentID()
    {
        return new StudentContractType.StudentID();
    }
}
