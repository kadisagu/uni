/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguageSkill;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 26.08.2015
 */
public class PersonForeignLanguageTypeReactor extends BaseEntityReactor<PersonForeignLanguage, PersonForeignLanguageType>
{
    private static final String LANGUAGE_SKILL = "foreignLanguageSkillID";
    private static final String FOREING_LAGUAGE = "foreignLanguageID";
    private static final String HUMAN = "humanID";

    @Override
    public Class<PersonForeignLanguage> getEntityClass()
    {
        return PersonForeignLanguage.class;
    }

    @Override
    public void collectUnknownDatagrams(PersonForeignLanguageType datagramObject, IUnknownDatagramsCollector collector)
    {
        PersonForeignLanguageType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null)
            collector.check(humanType.getID(), HumanType.class);

        PersonForeignLanguageType.ForeignLanguageID foreignLanguageID = datagramObject.getForeignLanguageID();
        ForeignLanguageType foreignLanguageType = foreignLanguageID == null ? null : foreignLanguageID.getForeignLanguage();
        if (foreignLanguageType != null)
            collector.check(foreignLanguageType.getID(), ForeignLanguageType.class);

        PersonForeignLanguageType.ForeignLanguageSkillID foreignLanguageSkillID = datagramObject.getForeignLanguageSkillID();
        ForeignLanguageSkillType foreignLanguageSkillType = foreignLanguageSkillID == null ? null : foreignLanguageSkillID.getForeignLanguageSkill();
        if (foreignLanguageSkillType != null)
            collector.check(foreignLanguageSkillType.getID(), ForeignLanguageSkillType.class);
    }

    protected PersonForeignLanguage findPersonForeignLanguage(Person person, ForeignLanguage foreignLanguage)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (person != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonForeignLanguage.person()), value(person)));
        if (foreignLanguage != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonForeignLanguage.language()), value(foreignLanguage)));
        List<PersonForeignLanguage> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public PersonForeignLanguageType createDatagramObject(String guid)
    {
        PersonForeignLanguageType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonForeignLanguageType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PersonForeignLanguage entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        PersonForeignLanguageType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonForeignLanguageType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setPersonForeignLanguageMain(NsiUtils.formatBoolean(entity.isMain()));

        ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        PersonForeignLanguageType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonForeignLanguageTypeHumanID();
        humanID.setHuman(humanType.getObject());
        datagramObject.setHumanID(humanID);
        if (humanType.getList() != null)
            result.addAll(humanType.getList());

        ProcessedDatagramObject<ForeignLanguageType> foreignLanguageType = createDatagramObject(ForeignLanguageType.class, entity.getLanguage(), commonCache, warning);
        PersonForeignLanguageType.ForeignLanguageID foreignLanguageID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonForeignLanguageTypeForeignLanguageID();
        foreignLanguageID.setForeignLanguage(foreignLanguageType.getObject());
        datagramObject.setForeignLanguageID(foreignLanguageID);
        if (foreignLanguageType.getList() != null)
            result.addAll(foreignLanguageType.getList());

        if(entity.getSkill() != null) {
            ProcessedDatagramObject<ForeignLanguageSkillType> foreignLanguageSkillType = createDatagramObject(ForeignLanguageSkillType.class, entity.getSkill(), commonCache, warning);
            PersonForeignLanguageType.ForeignLanguageSkillID foreignLanguageSkillID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonForeignLanguageTypeForeignLanguageSkillID();
            foreignLanguageSkillID.setForeignLanguageSkill(foreignLanguageSkillType.getObject());
            datagramObject.setForeignLanguageSkillID(foreignLanguageSkillID);
            if (foreignLanguageSkillType.getList() != null)
                result.addAll(foreignLanguageSkillType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<PersonForeignLanguage> processDatagramObject(PersonForeignLanguageType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {

        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // PersonForeignLanguage init
        PersonForeignLanguageType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);

        PersonForeignLanguageType.ForeignLanguageID foreignLanguageID = datagramObject.getForeignLanguageID();
        ForeignLanguageType foreignLanguageType = foreignLanguageID == null ? null : foreignLanguageID.getForeignLanguage();
        ForeignLanguage foreignLanguage = getOrCreate(ForeignLanguageType.class, params, commonCache, foreignLanguageType, FOREING_LAGUAGE, null, warning);

        PersonForeignLanguageType.ForeignLanguageSkillID foreignLanguageSkillID = datagramObject.getForeignLanguageSkillID();
        ForeignLanguageSkillType foreignLanguageSkillType = foreignLanguageSkillID == null ? null : foreignLanguageSkillID.getForeignLanguageSkill();
        ForeignLanguageSkill foreignLanguageSkill = getOrCreate(ForeignLanguageSkillType.class, params, commonCache, foreignLanguageSkillType, LANGUAGE_SKILL, null, warning);

        Boolean languageMain = NsiUtils.parseBoolean(datagramObject.getPersonForeignLanguageMain());

        boolean isNew = false;
        boolean isChanged = false;
        PersonForeignLanguage entity = null;

        CoreCollectionUtils.Pair<PersonForeignLanguage, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findPersonForeignLanguage(person, foreignLanguage);
        if (entity == null)
        {
            entity = new PersonForeignLanguage();
            isNew = true;
            isChanged = true;
        }

        // PersonForeignLanguageType set
        if (person != null && !person.equals(entity.getPerson()))
        {
            isChanged = true;
            entity.setPerson(person);
        }
        if (entity.getPerson() == null)
        {
            throw new ProcessingDatagramObjectException("PersonForeignLanguageType object without HumanID");
        }
        if (foreignLanguage != null && !foreignLanguage.equals(entity.getLanguage()))
        {
            isChanged = true;
            entity.setLanguage(foreignLanguage);
        }
        if (entity.getLanguage() == null)
        {
            throw new ProcessingDatagramObjectException("PersonForeignLanguageType object without ForeignLanguageID");
        }
        if (languageMain != null && !languageMain.equals(entity.isMain()))
        {
            isChanged = true;
            entity.setMain(languageMain);
        }
        if (foreignLanguageSkill != null && !foreignLanguageSkill.equals(entity.getSkill()))
        {
            isChanged = true;
            entity.setSkill(foreignLanguageSkill);
        }
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "f")
                .column(property("f"))
                .where(eqValue(property("f", PersonForeignLanguage.L_PERSON), entity.getPerson()))
                .where(eqValue(property("f", PersonForeignLanguage.L_LANGUAGE), entity.getLanguage()));

        List<PersonForeignLanguage> eqPersonForeignLanguageList = DataAccessServices.dao().getList(dql);
        PersonForeignLanguage eqPersonForeignLanguage = eqPersonForeignLanguageList.size()>0? eqPersonForeignLanguageList.get(0):null;
        if (eqPersonForeignLanguage!=null && !eqPersonForeignLanguage.getId().equals(entity.getId()))
        {
            throw new ProcessingDatagramObjectException("PersonForeignLanguageType person/language combination is not unique");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<PersonForeignLanguage> w, PersonForeignLanguageType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(ForeignLanguageType.class, session, params, FOREING_LAGUAGE, nsiPackage);
            saveChildEntity(ForeignLanguageSkillType.class, session, params, LANGUAGE_SKILL, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
