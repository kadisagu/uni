/* $Id: RelDegreeTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.RelDegreeType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class RelDegreeTypeReactor extends BaseEntityReactor<RelationDegree, RelDegreeType>
{
    @Override
    public Class<RelationDegree> getEntityClass()
    {
        return RelationDegree.class;
    }

    @Override
    public RelDegreeType createDatagramObject(String guid)
    {
        RelDegreeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createRelDegreeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(RelationDegree entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        RelDegreeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createRelDegreeType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setRelDegreeID(getUserCodeForNSIChecked(entity.getUserCode()));
        datagramObject.setRelDegreeName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected RelationDegree findRelationDegree(String userCode, String name)
    {
        if(userCode != null) {
            List<RelationDegree> list = findEntity(eq(property(ENTITY_ALIAS, RelationDegree.userCode()), value(userCode)));
            if (list.size() == 1)
                return list.get(0);
        }
        if(name != null) {
            List<RelationDegree> list = findEntity(eq(property(ENTITY_ALIAS, RelationDegree.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<RelationDegree> processDatagramObject(RelDegreeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // RelDegreeID init
        String code = StringUtils.trimToNull(datagramObject.getRelDegreeID());
        // RelDegreeName init
        String name = StringUtils.trimToNull(datagramObject.getRelDegreeName());

        boolean isNew = false;
        boolean isChanged = false;
        RelationDegree entity = null;
        CoreCollectionUtils.Pair<RelationDegree, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findRelationDegree(code, name);
        if(entity == null)
        {
            entity = new RelationDegree();
            isNew = true;
            isChanged = true;
            entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(RelationDegree.class));
        }

        // RelDegreeID set
        if(code != null && !code.equals(entity.getUserCode()))
        {
            isChanged = true;
            entity.setUserCode(code);
        }

        // RelDegreeName set
        if (null == name && isNew)
        {
            throw new ProcessingDatagramObjectException("RelDegreeType object without RelDegreeName!");
        }
        if(name != null)
        {
            isChanged = true;
            entity.setTitle(name);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, null);
    }

    @Override
    public List<IDatagramObject> createTestObjects() {
        RelDegreeType entity = new RelDegreeType();
        entity.setID("cc5d937a-3202-3c23-8a11-relDegreexxX");
        entity.setRelDegreeName("Степень родства (тест)");
        entity.setRelDegreeID("99999");
        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        return datagrams;
    }

    @Override
    public boolean isCorrect(RelationDegree entity, RelDegreeType datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException {
        return datagramObject.getRelDegreeName().equals(entity.getTitle());
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(RelationDegree.P_CODE);
        fields.add(RelationDegree.L_SEX);
        fields.add(RelationDegree.P_QUANTITY);
        return  fields;
    }
}
