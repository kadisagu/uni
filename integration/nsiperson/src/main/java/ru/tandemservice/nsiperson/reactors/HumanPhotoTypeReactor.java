/* $Id:$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.HumanPhotoType;
import ru.tandemservice.nsiclient.datagram.HumanType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 06.09.2016
 */
public class HumanPhotoTypeReactor extends BaseEntityReactor<DatabaseFile, HumanPhotoType>
{
    private static final String HUMAN = "HumanID";
    private static final String IDENTITY_CARD = "identityCard";

    @Override
    public Class<DatabaseFile> getEntityClass()
    {
        return DatabaseFile.class;
    }

    @Override
    protected List<Long> getFilteredEntityIdList(List<Long> srcListEntityIdList)
    {
        return DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(IdentityCard.class, "e").column(property("e", IdentityCard.photo().id()))
                .where(in(property("e", IdentityCard.photo().id()), srcListEntityIdList)));
    }

    private Person getPersonByDatabaseFile(DatabaseFile file)
    {
        if (null == file || null == file.getId()) return null;
        List<Person> persons = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Person.class, "e").column(property("e"))
                .where(eq(property("e", Person.identityCard().photo().id()), value(file.getId()))));
        return persons.size() > 0 ? persons.get(0) : null;
    }

    @Override
    public void collectUnknownDatagrams(HumanPhotoType datagramObject, IUnknownDatagramsCollector collector)
    {
        HumanPhotoType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null) collector.check(humanType.getID(), HumanType.class);
    }

    @Override
    public HumanPhotoType createDatagramObject(String guid)
    {
        HumanPhotoType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanPhotoType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(DatabaseFile entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        HumanPhotoType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanPhotoType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        Person person = getPersonByDatabaseFile(entity);
        if (person != null)
        {
            ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, person, commonCache, warning);
            HumanPhotoType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanPhotoTypeHumanID();
            humanID.setHuman(humanType.getObject());
            datagramObject.setHumanID(humanID);
            if (humanType.getList() != null)
                result.addAll(humanType.getList());
        }
        else warning.append("Can not find person for photo ID=" + entity.getId());

        datagramObject.setHumanPhotoFileName(entity.getFilename());
        datagramObject.setHumanPhotoContent(Base64.encodeBase64(entity.getContent()));

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<DatabaseFile> processDatagramObject(HumanPhotoType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();

        StringBuilder warning = new StringBuilder();

        HumanPhotoType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);
        String fileName = StringUtils.trimToNull(datagramObject.getHumanPhotoFileName());
        byte[] content = null != datagramObject.getHumanPhotoContent() ? Base64.decodeBase64(datagramObject.getHumanPhotoContent()) : null;

        boolean isNew = false;
        boolean isChanged = false;
        DatabaseFile entity = null;

        CoreCollectionUtils.Pair<DatabaseFile, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        //if (entity == null) entity = findDatabaseFile(person);
        if (entity == null)
        {
            entity = new DatabaseFile();
            isNew = true;
            isChanged = true;
        }

        if (fileName != null && !fileName.equals(entity.getFilename()))
        {
            isChanged = true;
            entity.setFilename(entity.getFilename());
        }

        if (content != null && content.length > 0)
        {
            isChanged = true;
            entity.setContent(content);
        }

        if (isNew && person == null) throw new ProcessingDatagramObjectException("HumanPhotoType object without HumanID!");

        if (person != null && null != person.getIdentityCard() && (null == person.getIdentityCard().getPhoto() || null == person.getIdentityCard().getPhoto().getContent()))
        {
            isChanged = true;
            person.getIdentityCard().setPhoto(entity);
            params.put(IDENTITY_CARD, person.getIdentityCard());
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<DatabaseFile> w, HumanPhotoType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            IdentityCard identityCard = (IdentityCard) params.get(IDENTITY_CARD);
            if (identityCard != null) session.saveOrUpdate(identityCard);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}
