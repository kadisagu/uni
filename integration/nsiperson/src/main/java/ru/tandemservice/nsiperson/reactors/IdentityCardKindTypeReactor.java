/* $Id: IdentityCardKindTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IdentityCardKindType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class IdentityCardKindTypeReactor extends BaseEntityReactor<IdentityCardType, IdentityCardKindType>
{
    @Override
    public Class<IdentityCardType> getEntityClass()
    {
        return IdentityCardType.class;
    }

    @Override
    public IdentityCardKindType createDatagramObject(String guid)
    {
        IdentityCardKindType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardKindType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(IdentityCardType entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        IdentityCardKindType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardKindType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setIdentityCardKindID(entity.getUserCode());
        datagramObject.setIdentityCardKindName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected IdentityCardType findIdentityCardType(String title, String userCode)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (title != null)
            expressions.add(eq(property(ENTITY_ALIAS, IdentityCardType.title()), value(title)));
        else if (userCode != null)
            expressions.add(eq(property(ENTITY_ALIAS, IdentityCardType.userCode()), value(userCode)));
        List<IdentityCardType> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);

        return null;
    }

    @Override
    public ChangedWrapper<IdentityCardType> processDatagramObject(IdentityCardKindType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // IdentityCardKindName init
        String name = StringUtils.trimToNull(datagramObject.getIdentityCardKindName());
        // IdentityCardKindID init
        String userCode = StringUtils.trimToNull(datagramObject.getIdentityCardKindID());

        boolean isNew = false;
        boolean isChanged = false;
        IdentityCardType entity = null;
        CoreCollectionUtils.Pair<IdentityCardType, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findIdentityCardType(name, userCode);
        if (entity == null)
        {
            entity = new IdentityCardType();
            isNew = true;
            isChanged = true;
            entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(IdentityCardType.class));
        }

        // IdentityCardKindName set
        if (name != null && (!name.equals(entity.getTitle()) || !name.equals(entity.getShortTitle())))
        {
            isChanged = true;
            entity.setTitle(name);
            if (isNew)
                entity.setShortTitle(name);
            if (!DataAccessServices.dao().isUnique(IdentityCardType.class, entity, IdentityCardType.P_TITLE))
                throw new ProcessingDatagramObjectException("IdentityCardKindName is not unique!");
        }
        // IdentityCardKindID set
        if (userCode != null && !userCode.equals(entity.getUserCode()))
        {
            isChanged = true;
            entity.setUserCode(userCode);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, null);
    }

    @Override
    public List<IDatagramObject> createTestObjects()
    {
        IdentityCardKindType entity = new IdentityCardKindType();
        entity.setID("cc5d937a-3202-3c23-8a11-cardKindType");
        entity.setIdentityCardKindID("99999");
        entity.setIdentityCardKindName("titleTest");
        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        return datagrams;
    }

    @Override
    public IdentityCardKindType getEmbedded(boolean onlyGuid)
    {
        IdentityCardKindType entity = new IdentityCardKindType();
        entity.setID("cc5d937a-3202-3c23-8a11-cardKindEmbe");
        if (!onlyGuid)
        {
            entity.setIdentityCardKindID("888888");
            entity.setIdentityCardKindName("titleTestEmbedded");
        }
        return entity;
    }

    @Override
    public boolean isCorrect(IdentityCardType entity, IdentityCardKindType datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException
    {
        return datagramObject.getIdentityCardKindName().equals(entity.getTitle()) && datagramObject.getIdentityCardKindName().equals(entity.getShortTitle());
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(IdentityCardType.P_CODE);
        fields.add(IdentityCardType.P_MAX_SERIA_LENGTH);
        fields.add(IdentityCardType.P_SERIA_MASK);
        fields.add(IdentityCardType.L_CITIZENSHIP_DEFAULT);
        fields.add(IdentityCardType.P_MIN_SERIA_LENGTH);
        fields.add(IdentityCardType.P_NUMBER_MASK);
        fields.add(IdentityCardType.P_SHORT_TITLE);
        fields.add(IdentityCardType.P_ONLY_CYRILLIC_IN_FIO);
        fields.add(IdentityCardType.P_NUMBER_REQUIRED);
        fields.add(IdentityCardType.P_MAX_NUMBER_LENGTH);
        fields.add(IdentityCardType.P_MIN_NUMBER_LENGTH);
        fields.add(IdentityCardType.P_SERIA_REQUIRED);
        fields.add(IdentityCardType.P_NUMBER_ONLY_DIGITS);
        fields.add(IdentityCardType.P_LOOKUP_ISSUANCE_PLACE);
        fields.add(IdentityCardType.P_SERIA_ONLY_DIGITS);
        fields.add(IdentityCardType.P_SHOW_SERIA);

        return fields;
    }
}