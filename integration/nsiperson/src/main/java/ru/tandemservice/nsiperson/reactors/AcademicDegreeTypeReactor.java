/* $Id: AcademicDegreeTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.AcademicDegreeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class AcademicDegreeTypeReactor extends BaseEntityReactor<ScienceDegree, AcademicDegreeType>
{
    @Override
    public Class<ScienceDegree> getEntityClass()
    {
        return ScienceDegree.class;
    }

    @Override
    public AcademicDegreeType createDatagramObject(String guid)
    {
        AcademicDegreeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicDegreeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    protected ScienceDegree findScienceDegree(String code, String fullName)
    {
        if (code != null)
        {
            List<ScienceDegree> list = findEntity(eq(property(ENTITY_ALIAS, ScienceDegree.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (fullName != null)
        {
            List<ScienceDegree> list = findEntity(eq(property(ENTITY_ALIAS, ScienceDegree.title()), value(fullName)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public ResultListWrapper createDatagramObject(ScienceDegree entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AcademicDegreeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicDegreeType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAcademicDegreeID(entity.getCode());
        datagramObject.setAcademicDegreeName(entity.getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    @Override
    public ChangedWrapper<ScienceDegree> processDatagramObject(AcademicDegreeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // AcademicDegreeTypeID init
        String code = StringUtils.trimToNull(datagramObject.getAcademicDegreeID());


        // AcademicDegreeTypeName init
        String fullName = StringUtils.trimToNull(datagramObject.getAcademicDegreeName());
        boolean isNew = false;
        boolean isChanged = false;
        ScienceDegree entity = null;

        CoreCollectionUtils.Pair<ScienceDegree, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findScienceDegree(code, fullName);
        if (entity == null)
        {
            entity = new ScienceDegree();
            isNew = true;
            isChanged = true;
        }

        // AcademicDegreeName set
        if (fullName != null && !fullName.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(fullName);
            entity.setShortTitle(fullName);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(ScienceDegree.class, ScienceDegree.P_TITLE, entity.getId(), fullName);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("AcademicDegreeType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("AcademicDegreeType without title");
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("AcademicDegreeType without short title");
        }

        StringBuilder warning = new StringBuilder();

        // AcademicDegreeTypeID set
        // Поле code immutable, поэтому оставляем как есть
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(ScienceDegree.class, entity, ScienceDegree.P_CODE))
            {
                if (code == null)
                    warning.append("[AcademicDegreeType object without AcademicDegreeTypeID!");
                else
                    warning.append("AcademicDegreeTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(ScienceDegree.class);
                warning.append(" AcademicDegreeTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }


            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(ScienceDegree.class, ScienceDegree.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);

        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[AcademicDegreeTypeID is immutable!]");


        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(ScienceDegree.P_SHORT_TITLE);
        fields.add(ScienceDegree.L_TYPE);
        return  fields;
    }
}