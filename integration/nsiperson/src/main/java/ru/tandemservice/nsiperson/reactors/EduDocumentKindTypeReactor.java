/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EduDocumentKindType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 26.08.2015
 */
public class EduDocumentKindTypeReactor extends BaseEntityReactor<EduDocumentKind, EduDocumentKindType>
{
    @Override
    public Class<EduDocumentKind> getEntityClass()
    {
        return EduDocumentKind.class;
    }

    @Override
    public EduDocumentKindType createDatagramObject(String guid)
    {
        EduDocumentKindType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduDocumentKindType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduDocumentKind entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduDocumentKindType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduDocumentKindType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduDocumentKindID(entity.getCode());
        datagramObject.setEduDocumentKindName(entity.getTitle());
        datagramObject.setEduDocumentKindNameShort(entity.getShortTitle());
        datagramObject.setEduDocumentKindCertifyEducationLevel(NsiUtils.formatBoolean(entity.isCertifyEducationLevel()));
        datagramObject.setEduDocumentKindCertifyQualification(NsiUtils.formatBoolean(entity.isCertifyQualification()));
        datagramObject.setEduDocumentKindDescription(entity.getDescription());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EduDocumentKind findEduDocumentKind(String code, String fullName)
    {
        if (code != null)
        {
            List<EduDocumentKind> list = findEntity(eq(property(ENTITY_ALIAS, EduDocumentKind.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (fullName != null)
        {
            List<EduDocumentKind> list = findEntity(eq(property(ENTITY_ALIAS, EduDocumentKind.title()), value(fullName)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }


    @Override
    public ChangedWrapper<EduDocumentKind> processDatagramObject(EduDocumentKindType datagramObject, Map<String, Object> commonCache,
                                                                                Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // EduDocumentKindType init
        String code = StringUtils.trimToNull(datagramObject.getEduDocumentKindID());
        String fullName = StringUtils.trimToNull(datagramObject.getEduDocumentKindName());
        String shortName = StringUtils.trimToNull(datagramObject.getEduDocumentKindNameShort());
        Boolean certifyEducationLevel = NsiUtils.parseBoolean(datagramObject.getEduDocumentKindCertifyEducationLevel());
        Boolean certifyQualification = NsiUtils.parseBoolean(datagramObject.getEduDocumentKindCertifyQualification());
        String description = StringUtils.trimToNull(datagramObject.getEduDocumentKindDescription());

        boolean isNew = false;
        boolean isChanged = false;
        EduDocumentKind entity = null;

        CoreCollectionUtils.Pair<EduDocumentKind, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduDocumentKind(code, fullName);
        if (entity == null)
        {
            entity = new EduDocumentKind();
            isNew = true;
            isChanged = true;
        }

        // EduDocumentKindType set
        if (fullName != null && !fullName.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(fullName);
            boolean isExist = !NsiSyncManager.instance().reactorDAO().isPropertyUnique(EduDocumentKind.class, EduDocumentKind.P_TITLE, entity.getId(), fullName);
            if (isExist)
            {
                throw new ProcessingDatagramObjectException("EduDocumentKindType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EduDocumentKindType object without title");
        }

        if (shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EduDocumentKindType object without short title");
        }

        if (certifyEducationLevel != null && !certifyEducationLevel.equals(entity.isCertifyEducationLevel()))
        {
            isChanged = true;
            entity.setCertifyEducationLevel(certifyEducationLevel);
        }
        if (certifyQualification != null && !certifyQualification.equals(entity.isQualificationRequired()))
        {
            isChanged = true;
            entity.setCertifyQualification(certifyQualification);
        }
        if (description != null && !description.equals(entity.getDescription()))
        {
            isChanged = true;
            entity.setDescription(description);
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduDocumentKind.class, entity, EduDocumentKind.P_CODE))
            {
                if (code == null)
                    warning.append("[EduDocumentKindType object without EduDocumentKindTypeID!");
                else
                    warning.append("EduDocumentKindTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduDocumentKind.class);
                warning.append(" EduDocumentKindTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(EduDocumentKind.class, EduDocumentKind.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduDocumentKindTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EduDocumentKind.P_DEFAULT_QUALIFICATION);
        fields.add(EduDocumentKind.P_ISSUANCE_DATE_REQUIRED);
        fields.add(EduDocumentKind.P_RU_ONLY);
        fields.add(EduDocumentKind.P_SERIA_ONLY_LETTERS);
        fields.add(EduDocumentKind.P_SERIA_ALLOWED);
        fields.add(EduDocumentKind.P_SERIA_REQUIRED);
        fields.add(EduDocumentKind.P_AVG_MARK_REQUIRED);
        fields.add(EduDocumentKind.P_AVG_MARK_ALLOWED);
        fields.add(EduDocumentKind.P_MARK_STATISTIC_ALLOWED);
        fields.add(EduDocumentKind.P_MARK_STATISTIC_REQUIRED);
        fields.add(EduDocumentKind.L_FIXED_EDUCATION_LEVEL);
        fields.add(EduDocumentKind.P_REGISTRATION_NUMBER_ALLOWED);
        fields.add(EduDocumentKind.P_REGISTRATION_NUMBER_REQUIRED);
        fields.add(EduDocumentKind.P_NUMBER_ONLY_DIGITS);
        fields.add(EduDocumentKind.P_EDU_PROGRAM_SUBJECT_ALLOWED);
        fields.add(EduDocumentKind.P_EDU_PROGRAM_SUBJECT_REQUIRED);

        return  fields;
    }
}
