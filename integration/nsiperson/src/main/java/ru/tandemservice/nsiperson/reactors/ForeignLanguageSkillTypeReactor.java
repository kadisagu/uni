/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguageSkill;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.ForeignLanguageSkillType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 26.08.2015
 */
public class ForeignLanguageSkillTypeReactor extends BaseEntityReactor<ForeignLanguageSkill, ForeignLanguageSkillType>
{
    @Override
    public Class<ForeignLanguageSkill> getEntityClass()
    {
        return ForeignLanguageSkill.class;
    }

    protected ForeignLanguageSkill findForeignLanguageSkill(String code, String title)
    {
        if (code != null)
        {
            List<ForeignLanguageSkill> list = findEntity(eq(property(ENTITY_ALIAS, ForeignLanguageSkill.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (title != null)
        {
            List<ForeignLanguageSkill> list = findEntity(eq(property(ENTITY_ALIAS, ForeignLanguageSkill.title()), value(title)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public ForeignLanguageSkillType createDatagramObject(String guid)
    {
        ForeignLanguageSkillType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createForeignLanguageSkillType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(ForeignLanguageSkill entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ForeignLanguageSkillType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createForeignLanguageSkillType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setForeignLanguageSkillID(entity.getCode());
        datagramObject.setForeignLanguageSkillName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    @Override
    public ChangedWrapper<ForeignLanguageSkill> processDatagramObject(ForeignLanguageSkillType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // ForeignLanguageSkill init
        String code = StringUtils.trimToNull(datagramObject.getForeignLanguageSkillID());
        String title = StringUtils.trimToNull(datagramObject.getForeignLanguageSkillName());

        boolean isNew = false;
        boolean isChanged = false;
        ForeignLanguageSkill entity = null;

        CoreCollectionUtils.Pair<ForeignLanguageSkill, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findForeignLanguageSkill(code, title);
        if (entity == null)
        {
            entity = new ForeignLanguageSkill();
            isNew = true;
            isChanged = true;
        }

        // ForeignLanguageSkillType set
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isExist = !NsiSyncManager.instance().reactorDAO().isPropertyUnique(ForeignLanguageSkill.class, ForeignLanguageSkill.P_TITLE, entity.getId(), title);
            if (isExist)
            {
                throw new ProcessingDatagramObjectException("ForeignLanguageSkillType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("ForeignLanguageSkillType object without title");
        }


        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(ForeignLanguageSkill.class, entity, ForeignLanguageSkill.P_CODE))
            {
                if (code == null)
                    warning.append("[ForeignLanguageSkillType object without ForeignLanguageSkillTypeID!");
                else
                    warning.append("ForeignLanguageSkillTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(ForeignLanguageSkill.class);
                warning.append(" ForeignLanguageSkillTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }

            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(ForeignLanguageSkill.class, ForeignLanguageSkill.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[ForeignLanguageSkillTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
