/* $Id: HumanAcademicDegreeTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceBranch;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AcademicDegreeType;
import ru.tandemservice.nsiclient.datagram.HumanAcademicDegreeType;
import ru.tandemservice.nsiclient.datagram.HumanType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.07.2015
 */
public class HumanAcademicDegreeTypeReactor extends BaseEntityReactor<PersonAcademicDegree, HumanAcademicDegreeType>
{
    private static final String HUMAN = "HumanID";
    private static final String ACADEMIC_DEGREE = "AcademicDegreeID";

    @Override
    public Class<PersonAcademicDegree> getEntityClass()
    {
        return PersonAcademicDegree.class;
    }

    @Override
    public void collectUnknownDatagrams(HumanAcademicDegreeType datagramObject, IUnknownDatagramsCollector collector)
    {
        HumanAcademicDegreeType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null)
            collector.check(humanType.getID(), HumanType.class);

        HumanAcademicDegreeType.AcademicDegreeID academicDegreeID = datagramObject.getAcademicDegreeID();
        AcademicDegreeType academicDegreeType = academicDegreeID == null ? null : academicDegreeID.getAcademicDegree();
        if (academicDegreeType != null)
            collector.check(academicDegreeType.getID(), AcademicDegreeType.class);
    }

    @Override
    public HumanAcademicDegreeType createDatagramObject(String guid)
    {
        HumanAcademicDegreeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicDegreeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PersonAcademicDegree entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        HumanAcademicDegreeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicDegreeType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        HumanAcademicDegreeType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicDegreeTypeHumanID();
        humanID.setHuman(humanType.getObject());
        datagramObject.setHumanID(humanID);
        if (humanType.getList() != null)
            result.addAll(humanType.getList());

        if(entity.getAcademicDegree() != null) {
            ProcessedDatagramObject<AcademicDegreeType> academicDegreeType = createDatagramObject(AcademicDegreeType.class, entity.getAcademicDegree(), commonCache, warning);
            HumanAcademicDegreeType.AcademicDegreeID academicDegreeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicDegreeTypeAcademicDegreeID();
            academicDegreeID.setAcademicDegree(academicDegreeType.getObject());
            datagramObject.setAcademicDegreeID(academicDegreeID);
            if (academicDegreeType.getList() != null)
                result.addAll(academicDegreeType.getList());
        }

        datagramObject.setHumanAcademicDegreeDate(NsiUtils.formatDate(entity.getDate()));
        datagramObject.setHumanAcademicDegreeDiplomaNumber(entity.getNumber());
        datagramObject.setHumanAcademicDegreeDSID(entity.getCouncil());
        datagramObject.setHumanAcademicDegreeOrganization(entity.getIssuancePlace());
        datagramObject.setHumanAcademicDegreeScienceID(entity.getScienceBranch() != null ? entity.getScienceBranch().getCode() : null);

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected PersonAcademicDegree findPersonAcademicDegree(Person person)
    {
        if (person != null)
        {
            List<PersonAcademicDegree> list = findEntity(eq(property(ENTITY_ALIAS, PersonAcademicDegree.person().id()), value(person.getId())));
            if (list.size() == 1)
                return list.get(0);
        }

        return null;
    }

    @Override
    public ChangedWrapper<PersonAcademicDegree> processDatagramObject(HumanAcademicDegreeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        HumanAcademicDegreeType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);

        HumanAcademicDegreeType.AcademicDegreeID academicDegreeID = datagramObject.getAcademicDegreeID();
        AcademicDegreeType academicDegreeType = academicDegreeID == null ? null : academicDegreeID.getAcademicDegree();
        ScienceDegree scienceDegree = getOrCreate(AcademicDegreeType.class, params, commonCache, academicDegreeType, ACADEMIC_DEGREE, null, warning);

        Date date = NsiUtils.parseDate(datagramObject.getHumanAcademicDegreeDate(), "HumanAcademicDegreeDate");
        String number = StringUtils.trimToNull(datagramObject.getHumanAcademicDegreeDiplomaNumber());
        String issuancePlace = StringUtils.trimToNull(datagramObject.getHumanAcademicDegreeOrganization());
        String council = StringUtils.trimToNull(datagramObject.getHumanAcademicDegreeDSID());
        String scienceBranchCode = StringUtils.trimToNull(datagramObject.getHumanAcademicDegreeScienceID());

        boolean isNew = false;
        boolean isChanged = false;
        PersonAcademicDegree entity = null;


        CoreCollectionUtils.Pair<PersonAcademicDegree, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findPersonAcademicDegree(person);
        if (entity == null)
        {
            entity = new PersonAcademicDegree();
            isNew = true;
            isChanged = true;
        }

        if (person != null && !person.equals(entity.getPerson()))
        {
            isChanged = true;
            entity.setPerson(person);
        }
        if (entity.getPerson() == null)
        {
            throw new ProcessingDatagramObjectException("HumanAcademicDegreeType object without HumanID!");
        }

        if (scienceDegree != null && !scienceDegree.getId().equals(entity.getAcademicDegree() != null ? entity.getAcademicDegree().getId() : null))
        {
            isChanged = true;
            entity.setAcademicDegree(scienceDegree);
        }
        if (date != null && !date.equals(entity.getDate()))
        {
            isChanged = true;
            entity.setDate(date);
        }

        if (number != null && !number.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(number);
        }

        if (issuancePlace != null && !issuancePlace.equals(entity.getIssuancePlace()))
        {
            isChanged = true;
            entity.setIssuancePlace(issuancePlace);
        }

        if (council != null && !council.equals(entity.getCouncil()))
        {
            isChanged = true;
            entity.setCouncil(council);
        }

        if (scienceBranchCode != null && !scienceBranchCode.equals(entity.getScienceBranch() != null ? entity.getScienceBranch().getCode() : null))
        {
            isChanged = true;
            ScienceBranch branch = DataAccessServices.dao().getByCode(ScienceBranch.class, scienceBranchCode);
            if (branch == null)
            {
                throw new ProcessingDatagramObjectException("ScienceBranch with code " + scienceBranchCode + " doesn't exist");
            }
            entity.setScienceBranch(branch);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<PersonAcademicDegree> w, HumanAcademicDegreeType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(AcademicDegreeType.class, session, params, ACADEMIC_DEGREE, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(PersonAcademicDegree.P_DIPLOM_FILE_TYPE);
        fields.add(PersonAcademicDegree.P_DIPLOM_FILE_NAME);
        fields.add(PersonAcademicDegree.L_DIPLOM_FILE);
        fields.add(PersonAcademicDegree.P_COPY_TRUE);
        fields.add(PersonAcademicDegree.P_ISSUANCE_DATE);
        fields.add(PersonAcademicDegree.P_SERIA);

        return  fields;
    }
}