/* $Id$ */
package ru.tandemservice.nsiperson.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<String[]> hidedExtPoint()
    {
        return itemListExtension(_catalogManager.hidedExtPoint())
                .add(IdentityCard.ENTITY_NAME, new String[]{IdentityCard.L_PHOTO})
                .add(PersonAcademicDegree.ENTITY_NAME, new String[]{PersonAcademicDegree.L_DIPLOM_FILE})
                .create();
    }

    @Bean
    public ItemListExtension<String> titleExtPoint()
    {
        return itemListExtension(_catalogManager.titleExtPoint())
                .add(PersonContactData.ENTITY_NAME, "allPhones")
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> orderableExtPoint()
    {
        return itemListExtension(_catalogManager.orderableExtPoint())
                .add(PersonContactData.ENTITY_NAME, false)
                .add(PersonEduInstitution.ENTITY_NAME, false)
                .add(IdentityCard.ENTITY_NAME, false)
                .add(Person.ENTITY_NAME, false)
                .add(PersonAcademicDegree.ENTITY_NAME, false)
                .add(PersonAcademicStatus.ENTITY_NAME, false)
                .add(PersonEduDocument.ENTITY_NAME, false)
                .add(PersonForeignLanguage.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> filterableExtPoint()
    {
        return itemListExtension(_catalogManager.filterableExtPoint())
                .add(PersonContactData.ENTITY_NAME, false)
                .add(PersonEduInstitution.ENTITY_NAME, false)
                .add(IdentityCard.ENTITY_NAME, false)
                .add(Person.ENTITY_NAME, false)
                .add(PersonAcademicDegree.ENTITY_NAME, false)
                .add(PersonAcademicStatus.ENTITY_NAME, false)
                .add(PersonEduDocument.ENTITY_NAME, false)
                .add(PersonForeignLanguage.ENTITY_NAME, false)
                .create();
    }
}
