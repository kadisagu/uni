/* $Id: IdentityCardTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.fias.base.entity.NoCitizenship;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class IdentityCardTypeReactor extends BaseEntityReactor<IdentityCard, IdentityCardType>
{
    private static final String HUMAN = "HumanID";
    private static final String IDENTITY_CARD = "IdentityCardKindID";
    private static final String ADDRESS = "AddressID";
    private static final String ADDRESS_STRING = "AddressStringID";

    @Override
    public Class<IdentityCard> getEntityClass()
    {
        return IdentityCard.class;
    }

    @Override
    public IdentityCardType createDatagramObject(String guid)
    {
        IdentityCardType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(IdentityCard entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        IdentityCardType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(4);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setIdentityCardPeriodFrom(null);
        datagramObject.setIdentityCardPeriodTo(null);
        datagramObject.setIdentityCardSeries(entity.getSeria());
        datagramObject.setIdentityCardNumber(entity.getNumber());
        datagramObject.setIdentityCardDepartmentCode(entity.getIssuanceCode());
        datagramObject.setIdentityCardWhoGive(entity.getIssuancePlace());
        datagramObject.setIdentityCardDateGive(NsiUtils.formatDate(entity.getIssuanceDate()));
        datagramObject.setIdentityCardDayOfEntryFrom(NsiUtils.formatDate(entity.getRegistrationPeriodFrom()));
        datagramObject.setIdentityCardDayOfEntryTo(NsiUtils.formatDate(entity.getRegistrationPeriodTo()));
        datagramObject.setIdentityCardLastName(entity.getLastName());
        datagramObject.setIdentityCardFirstName(entity.getFirstName());
        datagramObject.setIdentityCardMiddleName(entity.getMiddleName());
        datagramObject.setIdentityCardMain(null);
        datagramObject.setIdentityCardBirthDate(NsiUtils.formatDate(entity.getBirthDate()));
        datagramObject.setIdentityCardBirthPlace(entity.getBirthPlace());
        if(entity.getNationality() != null)
            datagramObject.setIdentityCardNationality(entity.getNationality().getTitle());

        Person person = entity.getPerson();
        StringBuilder warning = new StringBuilder();
        if (person != null)
        {
            ProcessedDatagramObject<HumanType> humanResult = createDatagramObject(HumanType.class, person, commonCache, warning);
            IdentityCardType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardTypeHumanID();
            humanID.setHuman(humanResult.getObject());
            datagramObject.setHumanID(humanID);

            if (humanResult.getList() != null)
                result.addAll(humanResult.getList());
        }
        org.tandemframework.shared.person.catalog.entity.IdentityCardType type = entity.getCardType();
        ProcessedDatagramObject<IdentityCardKindType> r = createDatagramObject(IdentityCardKindType.class, type, commonCache, warning);
        IdentityCardType.IdentityCardKindID identityCardKindID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardTypeIdentityCardKindID();
        identityCardKindID.setIdentityCardKind(r.getObject());
        datagramObject.setIdentityCardKindID(identityCardKindID);

        if (entity.getAddress() != null && entity.getAddress() instanceof AddressString)
        {
            ProcessedDatagramObject<AddressStringType> addrResult = createDatagramObject(AddressStringType.class, entity.getAddress(), commonCache, warning);
            IdentityCardType.AddressStringID addrId = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardTypeAddressStringID();
            addrId.setAddressString(addrResult.getObject());
            datagramObject.setAddressStringID(addrId);

            if (addrResult.getList() != null) result.addAll(addrResult.getList());
        }
        else if (entity.getAddress() != null && entity.getAddress() instanceof AddressRu)
        {
            ProcessedDatagramObject<AddressRUType> addrResult = createDatagramObject(AddressRUType.class, entity.getAddress(), commonCache, warning);
            IdentityCardType.AddressID addrID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createIdentityCardTypeAddressID();
            addrID.setAddress(addrResult.getObject());
            datagramObject.setAddressID(addrID);

            if (addrResult.getList() != null) result.addAll(addrResult.getList());
        }

        if(r.getList() != null) result.addAll(r.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(IdentityCardType datagramObject, IUnknownDatagramsCollector collector) {
        IdentityCardType.IdentityCardKindID identityCardKindID = datagramObject.getIdentityCardKindID();
        IdentityCardKindType identityCardKindType = identityCardKindID == null ? null : identityCardKindID.getIdentityCardKind();
        if(identityCardKindType != null) collector.check(identityCardKindType.getID(), IdentityCardKindType.class);

        IdentityCardType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if(humanType != null) collector.check(humanType.getID(), HumanType.class);

        IdentityCardType.AddressStringID addressStringID = datagramObject.getAddressStringID();
        AddressStringType addressStringType = addressStringID == null ? null : addressStringID.getAddressString();
        if(addressStringType != null) collector.check(addressStringType.getID(), AddressStringType.class);

        IdentityCardType.AddressID addressID = datagramObject.getAddressID();
        AddressRUType addressRUType = addressID == null ? null : addressID.getAddress();
        if(addressRUType != null) collector.check(addressRUType.getID(), AddressRUType.class);
    }

    protected IdentityCard findIdentityCard(org.tandemframework.shared.person.catalog.entity.IdentityCardType ict, String series, String number, Date dateGive, String firstName, String lastName, String middleName, Long personId)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (ict != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.cardType().id()), value(ict.getId())));
        if (series != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.seria()), value(series)));
        if (number != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.number()), value(number)));
        if (dateGive != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.issuanceDate()), commonValue(dateGive)));
        if (firstName != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.firstName()), value(firstName)));
        if (lastName != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.lastName()), value(lastName)));
        if (middleName != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.middleName()), value(middleName)));
        if (personId != null) expressions.add(eq(property(ENTITY_ALIAS, IdentityCard.person().id()), value(personId)));
        List<IdentityCard> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<IdentityCard> processDatagramObject(IdentityCardType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // IdentityCardFirstName init
        String firstName = StringUtils.trimToNull(datagramObject.getIdentityCardFirstName());
        // IdentityCardMiddleName init
        String middleName = StringUtils.trimToNull(datagramObject.getIdentityCardMiddleName());
        // IdentityCardLastName init
        String lastName = StringUtils.trimToNull(datagramObject.getIdentityCardLastName());
        // IdentityCardSeries init
        String series = StringUtils.trimToNull(datagramObject.getIdentityCardSeries());
        // IdentityCardNumber init
        String number = StringUtils.trimToNull(datagramObject.getIdentityCardNumber());
        // IdentityCardDepartmentCode init
        String departmentCode = StringUtils.trimToNull(datagramObject.getIdentityCardDepartmentCode());
        // IdentityCardWhoGive init
        String whoGive = StringUtils.trimToNull(datagramObject.getIdentityCardWhoGive());
        // IdentityCardDateGive init
        Date dateGive = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getIdentityCardDateGive()), "IdentityCardDateGive");
        // IdentityCardDayOfEntryFrom init
        Date dayOfEntryFrom = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getIdentityCardDayOfEntryFrom()), "IdentityCardDayOfEntryFrom");
        // IdentityCardDayOfEntryTo init
        Date dayOfEntryTo = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getIdentityCardDayOfEntryTo()), "IdentityCardDayOfEntryTo");
        // IdentityCardBirthDate init
        Date birthDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getIdentityCardBirthDate()), "IdentityCardBirthDate");
        // IdentityCardBirthPlace init
        String birthPlace = StringUtils.trimToNull(datagramObject.getIdentityCardBirthPlace());

        String nationalityTitle = StringUtils.trimToNull(datagramObject.getIdentityCardNationality());
        Nationality nationality = findNationality(nationalityTitle);

        // IdentityCardKindID init
        IdentityCardType.IdentityCardKindID identityCardKindID = datagramObject.getIdentityCardKindID();
        IdentityCardKindType identityCardKindType = identityCardKindID == null ? null : identityCardKindID.getIdentityCardKind();
        org.tandemframework.shared.person.catalog.entity.IdentityCardType ict = getOrCreate(IdentityCardKindType.class, params, commonCache, identityCardKindType, IDENTITY_CARD, null, warning);
        // HumanID init
        IdentityCardType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();

        IdentityCardType.AddressStringID addressStringID = datagramObject.getAddressStringID();
        AddressStringType addressStringType = addressStringID == null ? null : addressStringID.getAddressString();
        AddressBase addressBase = getOrCreate(AddressStringType.class, params, commonCache, addressStringType, ADDRESS_STRING, null, warning);

        if(null == addressBase)
        {
            IdentityCardType.AddressID addressID = datagramObject.getAddressID();
            AddressRUType addressRUType = addressID == null ? null : addressID.getAddress();
            addressBase = getOrCreate(AddressRUType.class, params, commonCache, addressRUType, ADDRESS, null, warning);
        }

        Person person = null;

        boolean isNew = false;
        boolean isChanged = false;
        IdentityCard entity = null;
        CoreCollectionUtils.Pair<IdentityCard, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);

        if (humanType != null)
        {
            Map<String, Object> humanParams = new HashMap<>(1);
            humanParams.put(HumanTypeReactor.EXISTED_IDENTITY_CARD, entity);
            person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, humanParams, warning);
        }

        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findIdentityCard(ict, series, number, dateGive, firstName, lastName, middleName, (null != person ? person.getId() : null));
        if (entity == null)
        {
            entity = new IdentityCard();
            isNew = true;
            isChanged = true;
        }

        // IdentityCardLastName set
        if (lastName != null && !lastName.equals(entity.getLastName()))
        {
            isChanged = true;
            entity.setLastName(lastName);
        }
        if (entity.getLastName() == null)
            throw new ProcessingDatagramObjectException("IdentityCardType object without IdentityCardLastName!");

        // IdentityCardFirstName set
        if (firstName != null && !firstName.equals(entity.getFirstName()))
        {
            isChanged = true;
            entity.setFirstName(firstName);
        }
        if (entity.getFirstName() == null)
            throw new ProcessingDatagramObjectException("IdentityCardType object without IdentityCardFirstName!");

        // IdentityCardMiddleName set
        if (middleName != null && !middleName.equals(entity.getMiddleName()))
        {
            isChanged = true;
            entity.setMiddleName(middleName);
        }

        // IdentityCardSeries set
        if (series != null && !series.equals(entity.getSeria()))
        {
            isChanged = true;
            entity.setSeria(series);
        }

        // IdentityCardNumber set
        if (number != null && !number.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(number);
        }

        // IdentityCardDepartmentCode set
        if (departmentCode != null && !departmentCode.equals(entity.getIssuanceCode()))
        {
            isChanged = true;
            entity.setIssuanceCode(departmentCode);
        }

        // IdentityCardWhoGive set
        if (whoGive != null && !whoGive.equals(entity.getIssuancePlace()))
        {
            isChanged = true;
            entity.setIssuancePlace(whoGive);
        }

        // IdentityCardDateGive set
        if (dateGive != null && !dateGive.equals(entity.getIssuanceDate()))
        {
            isChanged = true;
            entity.setIssuanceDate(dateGive);
        }

        // IdentityCardDayOfEntryFrom set
        if (dayOfEntryFrom != null && !dayOfEntryFrom.equals(entity.getRegistrationPeriodFrom()))
        {
            isChanged = true;
            entity.setRegistrationPeriodFrom(dayOfEntryFrom);
        }

        // IdentityCardDayOfEntryTo set
        if (dayOfEntryTo != null && !dayOfEntryTo.equals(entity.getRegistrationPeriodTo()))
        {
            isChanged = true;
            entity.setRegistrationPeriodTo(dayOfEntryTo);
        }

        // IdentityCardBirthDate set
        if (birthDate != null && !birthDate.equals(entity.getBirthDate()))
        {
            isChanged = true;
            entity.setBirthDate(birthDate);
        }

        // IdentityCardBirthPlace set
        if (birthPlace != null && !birthPlace.equals(entity.getBirthPlace()))
        {
            isChanged = true;
            entity.setBirthPlace(birthPlace);
        }

        // IdentityCardKindID set
        if (identityCardKindType == null && isNew)
            entity.setCardType(DataAccessServices.dao().get(org.tandemframework.shared.person.catalog.entity.IdentityCardType.class, org.tandemframework.shared.person.catalog.entity.IdentityCardType.P_CODE, IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA));
        if(identityCardKindType != null && !ict.equals(entity.getCardType())) {
            entity.setCardType(ict);
        }

        if (addressBase!=null && !addressBase.equals(entity.getAddress()))
        {
            isChanged = true;
            entity.setAddress(addressBase);
        }

        if (nationality!=null && !nationality.equals(entity.getNationality()))
        {
            isChanged = true;
            entity.setNationality(nationality);
        }

        // HumanID set
        if (humanType == null && isNew)
            throw new ProcessingDatagramObjectException("IdentityCardType object without HumanID!");
        if (humanType != null)
        {
            Map<String, Object> humanParams = new HashMap<>(1);
            humanParams.put(HumanTypeReactor.EXISTED_IDENTITY_CARD, entity);
            person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, humanParams, warning);

            @SuppressWarnings("unchecked")
            ChangedWrapper<Person> pcw = (ChangedWrapper<Person>) params.get(HUMAN);
            if(pcw == null || !pcw.isCreated()) {
                // в этом случае персона уже существовала, а это значит у неё уже есть удостоверение личности
                entity.setSex(person.getIdentityCard().getSex());
                entity.setCitizenship(person.getIdentityCard().getCitizenship());
            }
            //person.setIdentityCard(entity);
            entity.setPerson(person);
        }

        if (null == entity.getSex())
            entity.setSex(ISharedBaseDao.instance.get().getCatalogItem(Sex.class, SexCodes.MALE));

        if (null != entity.getCitizenship())
            entity.setCitizenship(DataAccessServices.dao().getList(NoCitizenship.class).get(0));

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<IdentityCard> w, IdentityCardType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();

        try
        {
            if (params != null)
            {

                saveChildEntity(IdentityCardKindType.class, session, params, IDENTITY_CARD, nsiPackage);

                @SuppressWarnings("unchecked")
                ChangedWrapper<Person> pcw = (ChangedWrapper<Person>) params.get(HUMAN);

                if (pcw != null)
                {
                    HumanTypeReactor reactor = (HumanTypeReactor) ReactorHolder.getReactorMap().get(HumanType.class.getSimpleName());
                    reactor.save(session, pcw, (HumanType) params.get(HUMAN + PARAM), nsiPackage);
                    //session.flush();

                    // объект в сессии обновился, поэтому обновляем его из HumanTypeReactor
                    w = new ChangedWrapper<>(w.isCreated(), w.isChanged(), w.getNsiEntity(), pcw.getResult().getIdentityCard(), w.getParams(), w.getWarning());
                    w.getResult().setPerson(pcw.getResult());
                }

                saveChildEntity(AddressStringType.class, session, params, ADDRESS_STRING, nsiPackage);
                saveChildEntity(AddressRUType.class, session, params, ADDRESS, nsiPackage);
            }

            Person person = w.getResult().getPerson();
            if(null != person) session.saveOrUpdate(person);

            w.merge(session);
            super.save(session, w, datagramObject, nsiPackage);
            IdentityCard card = w.getResult();
            //session.flush();

            if(null != person)
            {
                card.setPerson(person);
                session.saveOrUpdate(card);
                session.flush();

                person.setIdentityCard(card);
                session.saveOrUpdate(person);
            }

        } catch (Exception e)
        {
            System.err.println("\n\n!!!!! " + NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC.format(new Date()));
            e.printStackTrace();
            System.err.println("\n\n!!!!! ");
            throw e;
        }
    }

    public List<IDatagramObject> createTestObjects()
    {
        IdentityCardType entity = new IdentityCardType();
        entity.setID("cc5d937a-3202-3c23-8a11-cardxxxxxxxX");
        entity.setHumanID(new IdentityCardType.HumanID());
        entity.setIdentityCardKindID(new IdentityCardType.IdentityCardKindID());
        entity.setIdentityCardBirthDate("1954-02-03");
        entity.setIdentityCardBirthPlace("Москва");
        entity.setIdentityCardFirstName("firstNameTest");
        entity.setIdentityCardLastName("lastNameTest");
        entity.setIdentityCardMiddleName("middleNameTest");
        entity.setIdentityCardDateGive("2014-02-03");
        entity.setIdentityCardDayOfEntryFrom("2014-02-03");
        entity.setIdentityCardDayOfEntryTo("2019-02-03");
        entity.setIdentityCardNumber("12345");
        entity.setIdentityCardSeries("12345678");
        entity.setIdentityCardWhoGive("паспортный стол");
        entity.setIdentityCardDepartmentCode("2131");

        @SuppressWarnings("unchecked")
        INsiEntityReactor<org.tandemframework.shared.person.catalog.entity.IdentityCardType, IdentityCardKindType> reactor =
                (INsiEntityReactor<org.tandemframework.shared.person.catalog.entity.IdentityCardType, IdentityCardKindType>) ReactorHolder.getReactorMap().get(IdentityCardKindType.class.getSimpleName());

        entity.getHumanID().setHuman((HumanType) HumanTypeReactor.getFirstHumanTypeForEmbedded(true).get(0));
        entity.getIdentityCardKindID().setIdentityCardKind(reactor.getEmbedded(true));

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        datagrams.addAll(HumanTypeReactor.getFirstHumanTypeForEmbedded(false));
        datagrams.add(reactor.getEmbedded(false));
        return datagrams;
    }

    @Override
    public boolean isCorrect(IdentityCard entity, IdentityCardType datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException {
        return datagramObject.getIdentityCardLastName().equals(entity.getLastName())
                && datagramObject.getIdentityCardFirstName().equals(entity.getFirstName())
                && datagramObject.getIdentityCardMiddleName().equals(entity.getMiddleName())
                && datagramObject.getIdentityCardBirthPlace().equals(entity.getBirthPlace())
                && datagramObject.getIdentityCardBirthDate().equals(NsiUtils.DATE_FORMAT.format(entity.getBirthDate()))
                && datagramObject.getIdentityCardSeries().equals(entity.getSeria())
                && datagramObject.getIdentityCardNumber().equals(entity.getNumber());
    }

    public static Nationality findNationality(String title)
    {
        if (title != null && !title.isEmpty())
        {
            return DataAccessServices.dao().get(Nationality.class, Nationality.P_TITLE, title);
        }
        return null;
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(IdentityCard.L_PERSON);
        fields.add(IdentityCard.L_PHOTO);
        fields.add(IdentityCard.L_ADDRESS);
        return  fields;
    }
}