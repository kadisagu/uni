/* $Id: HumanAcademicStatusTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.AcademicStatusType;
import ru.tandemservice.nsiclient.datagram.HumanAcademicStatusType;
import ru.tandemservice.nsiclient.datagram.HumanType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.07.2015
 */
public class HumanAcademicStatusTypeReactor extends BaseEntityReactor<PersonAcademicStatus, HumanAcademicStatusType>
{
    private static final String HUMAN = "HumanID";
    private static final String ACADEMIC_STATUS = "AcademicStatusID";

    @Override
    public Class<PersonAcademicStatus> getEntityClass()
    {
        return PersonAcademicStatus.class;
    }

    @Override
    public void collectUnknownDatagrams(HumanAcademicStatusType datagramObject, IUnknownDatagramsCollector collector)
    {
        HumanAcademicStatusType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null)
            collector.check(humanType.getID(), HumanType.class);

        HumanAcademicStatusType.AcademicStatusID academicStatusID = datagramObject.getAcademicStatusID();
        AcademicStatusType academicStatusType = academicStatusID == null ? null : academicStatusID.getAcademicStatus();
        if (academicStatusType != null)
            collector.check(academicStatusType.getID(), AcademicStatusType.class);
    }

    @Override
    public HumanAcademicStatusType createDatagramObject(String guid)
    {
        HumanAcademicStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicStatusType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PersonAcademicStatus entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        HumanAcademicStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicStatusType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(3);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        HumanAcademicStatusType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicStatusTypeHumanID();
        humanID.setHuman(humanType.getObject());
        datagramObject.setHumanID(humanID);
        if (humanType.getList() != null)
            result.addAll(humanType.getList());

        if (entity.getAcademicStatus() != null)
        {
            ProcessedDatagramObject<AcademicStatusType> academicStatusType = createDatagramObject(AcademicStatusType.class, entity.getAcademicStatus(), commonCache, warning);
            HumanAcademicStatusType.AcademicStatusID academicStatusID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanAcademicStatusTypeAcademicStatusID();
            academicStatusID.setAcademicStatus(academicStatusType.getObject());
            datagramObject.setAcademicStatusID(academicStatusID);
            if (academicStatusType.getList() != null)
                result.addAll(academicStatusType.getList());
        }
        datagramObject.setHumanAcademicStatusDate(NsiUtils.formatDate(entity.getDate()));
        datagramObject.setHumanAcademicStatusDiplomaNumber(entity.getNumber());
        datagramObject.setHumanAcademicStatusOrganization(entity.getIssuancePlace());


        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected PersonAcademicStatus findPersonAcademicStatus(Person person, ScienceStatus scienceStatus)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (person != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonAcademicStatus.person()), value(person)));
        if (scienceStatus != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonAcademicStatus.academicStatus()), value(scienceStatus)));

        List<PersonAcademicStatus> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
        {
            return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<PersonAcademicStatus> processDatagramObject(HumanAcademicStatusType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        StringBuilder warning = new StringBuilder();
        Map<String, Object> params = new HashMap<>();

        HumanAcademicStatusType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);

        HumanAcademicStatusType.AcademicStatusID academicStatusID = datagramObject.getAcademicStatusID();
        AcademicStatusType academicStatusType = academicStatusID == null ? null : academicStatusID.getAcademicStatus();
        ScienceStatus scienceStatus = getOrCreate(AcademicStatusType.class, params, commonCache, academicStatusType, ACADEMIC_STATUS, null, warning);

        Date date = NsiUtils.parseDate(datagramObject.getHumanAcademicStatusDate(), "HumanAcademicStatusDate");
        String number = StringUtils.trimToNull(datagramObject.getHumanAcademicStatusDiplomaNumber());
        String issuancePlace = StringUtils.trimToNull(datagramObject.getHumanAcademicStatusOrganization());

        boolean isNew = false;
        boolean isChanged = false;
        PersonAcademicStatus entity = null;


        CoreCollectionUtils.Pair<PersonAcademicStatus, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findPersonAcademicStatus(person, scienceStatus);
        if (entity == null)
        {
            entity = new PersonAcademicStatus();
            isNew = true;
            isChanged = true;
        }


        if (person != null && !person.equals(entity.getPerson()))
        {
            isChanged = true;
            entity.setPerson(person);
        }
        if (entity.getPerson() == null)
        {
            throw new ProcessingDatagramObjectException("HumanAcademicStatusType object without HumanID!");
        }

        if (scienceStatus != null && !scienceStatus.getId().equals(entity.getAcademicStatus() != null ? entity.getAcademicStatus().getId() : null))
        {
            isChanged = true;
            entity.setAcademicStatus(scienceStatus);
        }
        if (date != null && !date.equals(entity.getDate()))
        {
            isChanged = true;
            entity.setDate(date);
        }

        if (number != null && !number.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(number);
        }

        if (issuancePlace != null && !issuancePlace.equals(entity.getIssuancePlace()))
        {
            isChanged = true;
            entity.setIssuancePlace(issuancePlace);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<PersonAcademicStatus> w, HumanAcademicStatusType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(AcademicStatusType.class, session, params, ACADEMIC_STATUS, nsiPackage);
        }


        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(PersonAcademicStatus.P_CERTIFICATE_FILE_TYPE);
        fields.add(PersonAcademicStatus.P_COPY_TRUE);
        fields.add(PersonAcademicStatus.P_ISSUANCE_DATE);
        fields.add(PersonAcademicStatus.P_CERTIFICATE_FILE_NAME);
        fields.add(PersonAcademicStatus.L_CERTIFICATE_FILE);
        fields.add(PersonAcademicStatus.P_SERIA);
        fields.add(PersonAcademicStatus.P_SCIENCE_SPECIALITY);

        return  fields;
    }
}