/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EduLevelType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 26.08.2015
 */
public class EduLevelTypeReactor extends BaseEntityReactor<EduLevel, EduLevelType>
{
    private static final String PARENT_EDU_LEVEL = "ParentEduLevelID";

    @Override
    public Class<EduLevel> getEntityClass()
    {
        return EduLevel.class;
    }

    @Override
    public void collectUnknownDatagrams(EduLevelType datagramObject, IUnknownDatagramsCollector collector)
    {
        EduLevelType.ParentEduLevelID parentEduLevelID = datagramObject.getParentEduLevelID();
        EduLevelType parentEduLevel = parentEduLevelID == null ? null : parentEduLevelID.getEduLevel();
        if (parentEduLevel != null) collector.check(parentEduLevel.getID(), EduLevelType.class);
    }

    protected EduLevel findEduLevel(String code, String fullName)
    {
        if (code != null)
        {
            List<EduLevel> list = findEntity(eq(property(ENTITY_ALIAS, EduLevel.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (fullName != null)
        {
            List<EduLevel> list = findEntity(eq(property(ENTITY_ALIAS, EduLevel.title()), value(fullName)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public EduLevelType createDatagramObject(String guid)
    {
        EduLevelType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduLevelType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduLevel entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduLevelType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduLevelType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduLevelID(entity.getCode());
        datagramObject.setEduLevelName(entity.getTitle());
        datagramObject.setEduLevelNameShort(entity.getShortTitle());
        datagramObject.setEduLevelLevel(NsiUtils.formatBoolean(entity.isLevel()));

        if (entity.getHierarhyParent()!=null)
        {
            ProcessedDatagramObject<EduLevelType> parentEduLevelType = createDatagramObject(EduLevelType.class, entity.getHierarhyParent(), commonCache, warning);
            EduLevelType.ParentEduLevelID parentEduLevelID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduLevelTypeParentEduLevelID();
            parentEduLevelID.setEduLevel(parentEduLevelType.getObject());
            datagramObject.setParentEduLevelID(parentEduLevelID);
            if (parentEduLevelType.getList() != null)
                result.addAll(parentEduLevelType.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<EduLevel> processDatagramObject(EduLevelType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EduLevel init
        EduLevelType.ParentEduLevelID parentEduLevelID = datagramObject.getParentEduLevelID();
        EduLevel parentEduLevel = null;
        if (parentEduLevelID != null)
        {
            EduLevelType parentEduLevelType = parentEduLevelID.getEduLevel();
            parentEduLevel = getOrCreate(EduLevelType.class, params, commonCache, parentEduLevelType, PARENT_EDU_LEVEL, null, warning);
        }
        String code = StringUtils.trimToNull(datagramObject.getEduLevelID());
        String fullName = StringUtils.trimToNull(datagramObject.getEduLevelName());
        String shortName = StringUtils.trimToNull(datagramObject.getEduLevelNameShort());
        Boolean eduLevelLevel = NsiUtils.parseBoolean(datagramObject.getEduLevelLevel());


        boolean isNew = false;
        boolean isChanged = false;
        EduLevel entity = null;


        CoreCollectionUtils.Pair<EduLevel, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduLevel(code, fullName);
        if (entity == null)
        {
            entity = new EduLevel();
            isNew = true;
            isChanged = true;
        }

        // EduLevelType set
        if (fullName != null && !fullName.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(fullName);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(EduLevel.class, EduLevel.P_TITLE, entity.getId(), fullName);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EduLevelType title is not unique");
            }
        }

        if (shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EduLevelType object without short title");
        }

        if (eduLevelLevel != null && !eduLevelLevel.equals(entity.isLevel()))
        {
            isChanged = true;
            entity.setLevel(eduLevelLevel);
        }
        if (parentEduLevel != null && !parentEduLevel.equals(entity.getHierarhyParent()))
        {
            isChanged = true;
            entity.setParent(parentEduLevel);
        }
        if (entity.getHierarhyParent() != null && entity.getHierarhyParent().getCode().equals(entity.getCode()))
        {
            throw new ProcessingDatagramObjectException("EduLevelType object code can't to be a parent to itself. Change HierarhyParent field!");
        }

        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduLevel.class, entity, EduLevel.P_CODE))
            {
                if (code == null)
                    warning.append("[EduDocumentKindType object without EduLevelTypeID!");
                else
                    warning.append("EduLevelTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduLevel.class);
                warning.append(" EduLevelTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduLevelTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EduLevel> w, EduLevelType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EduLevelType.class, session, params, PARENT_EDU_LEVEL, nsiPackage);
        }


        super.save(session, w, datagramObject, nsiPackage);
    }
}
