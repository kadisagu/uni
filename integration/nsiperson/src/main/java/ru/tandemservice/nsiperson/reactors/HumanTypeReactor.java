/* $Id: HumanTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.sec.entity.codes.AuthenticationTypeCodes;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.IOperationReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2013
 */
public class HumanTypeReactor extends BaseEntityReactor<Person, HumanType>
{
    private static final String CITIZENSHIP = "HumanCitizenshipID";
    private static final String ADDRESS = "HumanAddressRuID";
    private static final String ADDRESS_STRING = "HumanAddressStringID";
    private static final String PRINCIPAL = "HumanPrincipalID";
    private static final String IDENTITY_CARD = "identityCard";
    private static final String CONTACT_DATA = "contactData";
    private static final String RELATIONS = "relations";
    private static final String PERSON_ROLE = "personRole";

    public static final String EXISTED_IDENTITY_CARD = "e";

    private IOperationReactor _deleteOperationReactor;

    private static List<Long> getPrincipalIdsByPersonIds(Collection<Long> entityIds)
    {
        List<Long> principals = new ArrayList<>(entityIds.size());

        BatchUtils.execute(entityIds, 200, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> entityIdsPortion)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Person2PrincipalRelation.class, "e")
                        .column(property("e", Person2PrincipalRelation.principal().id()))
                        .where(in(property("e", Person2PrincipalRelation.person().id()), entityIdsPortion));
                principals.addAll(DataAccessServices.dao().<Long>getList(builder));
            }
        });
        return principals;
    }

    @Override
    public IOperationReactor getDeleteOperationReactor()
    {
        if (_deleteOperationReactor == null)
            _deleteOperationReactor = new IOperationReactor()
            {
                @Override
                public List<IDatagramObject> process(List<IDatagramObject> items, NsiPackage nsiPackage, Map<String, Object> commonCache, Map<String, Object> reactorCache, StringBuilder warnLog, StringBuilder errorLog)
                {
                    Class<Person> c = getEntityClass();
                    String className = c.getSimpleName();
                    ILogger logger = NsiRequestHandlerService.instance().getLogger();

                    // Подготавливаем сет идентификаторов ОБ для инициализации мапов
                    List<Long> entityIds = NsiSyncManager.instance().dao().getEntityIdsByDatagramObjects(items);

                    // Не указан ни один идентификатор сущности НСИ на удаление, либо запрос предполагает тотальное удаление всех элементов из ОБ
                    if (null == entityIds)
                    {
                        logger.log(Level.ERROR, "---------- There are no any GUID specified for the delete operation. Whole entity set could not be deleted ----------");
                        if(errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append("Could not delete whole entity set. Please, specify entity ids list to delete.");
                        return null;
                    }

                    // Инициализируем мапы объектов и связанных с ними GUID'ов
                    Map<String, CoreCollectionUtils.Pair<Person, NsiEntity>> idToEntityMap = NsiSyncManager.instance().dao().getEntityWithNsiIdsMap(c, entityIds);
                    if (logger != null)
                        logger.log(Level.INFO, "---------- Whole elements list (" + idToEntityMap.values().size() + " items) for " + className + " catalog was received from database ----------");

                    // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов)
                    List<String> nonDeletableGuids = NsiSyncManager.instance().dao().getNonDeletableGuids(getEntityClass(), entityIds);

                    // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
                    if (!nonDeletableGuids.isEmpty())
                    {
                        if (logger != null)
                            logger.log(Level.ERROR, "---------- Deleting requested list (" + items.size() + " items) for " + className + " catalog was failed. ----------");
                        if(errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. Please, check dependencies for the objects ")
                                .append(CommonBaseStringUtil.joinNotEmpty(nonDeletableGuids, ", "))
                                .append('.');
                        return null;
                    }

                    // Подготавливаем список объектов реально обработанных и GUID'ов, не найденных в системе
                    List<String> strangeGuids = new ArrayList<>();
                    List<IDatagramObject> deletedObjectsList = new ArrayList<>();
                    for (IDatagramObject datagramObject : items)
                    {
                        String guid = StringUtils.trimToNull(datagramObject.getID());
                        CoreCollectionUtils.Pair<Person, NsiEntity> pair = idToEntityMap.get(guid);
                        if (null != pair)
                        {
                            deletedObjectsList.add(createDatagramObject(pair.getY().getGuid()));
                            NsiSyncManager.instance().dao().createEntityLog(pair.getY(), getCatalogType(), nsiPackage, null);
                        }
                        else
                            strangeGuids.add(datagramObject.getID());
                    }

                    // Удаляем удалябельные объекты и связанные с ними идентификаторы НСИ
//                    for(Long id : entityIds) {
//                        Person p = DataAccessServices.dao().get(id);
//                        Person2PrincipalRelation rel = DataAccessServices.dao().get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, p);
//                        Principal principal = rel.getPrincipal();
//                        DataAccessServices.dao().delete(id);
//                        DataAccessServices.dao().delete(principal);
//                        NsiSyncManager.instance().dao().deleteNsiEntities(Collections.singletonList(id));
//                    }
                    List<Long> principalIds = getPrincipalIdsByPersonIds(entityIds);
                    NsiSyncManager.instance().dao().deleteEntities(c, entityIds);
                    NsiSyncManager.instance().reactorDAO().batchDeleteEntities(Principal.class, principalIds);

                    // Выдаём предупреждение, если среди переданных GUID'ов имеются не найденные в ОБ. Удалять нечего.
                    if (!strangeGuids.isEmpty())
                    {
                        if (logger != null)
                            logger.log(Level.WARN, "---------- Deleting requested list (" + deletedObjectsList.size() + " items) for " + className + " catalog was executed with warnings ----------");
                        if(warnLog.length() > 0)
                            warnLog.append('\n');
                        warnLog.append("One, or more entities were not found at OB by given GUID's, so they were ignored: ")
                                .append(CommonBaseStringUtil.joinNotEmpty(strangeGuids, ", "));
                    }

                    if (logger != null)
                        logger.log(Level.INFO, "---------- Deleting requested elements list (" + deletedObjectsList.size() + " items) for " + className + " catalog was executed successfully ----------");

                    return deletedObjectsList;
                }
            };
        return _deleteOperationReactor;
    }

    @Override
    public Class<Person> getEntityClass()
    {
        return Person.class;
    }

    @Override
    public HumanType createDatagramObject(String guid)
    {
        HumanType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(Person entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        HumanType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        IdentityCard identityCard = entity.getIdentityCard();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setHumanLastName(identityCard.getLastName());
        datagramObject.setHumanFirstName(identityCard.getFirstName());
        datagramObject.setHumanMiddleName(identityCard.getMiddleName() == null ? "" : identityCard.getMiddleName());

        Principal principal = PersonManager.instance().dao().getPrincipal(entity);
        datagramObject.setHumanLogin(principal != null ? principal.getLogin() : null);
        datagramObject.setHumanBirthDate(NsiUtils.formatDate(identityCard.getBirthDate()));
        datagramObject.setHumanBasicEmail(entity.getEmail());
        datagramObject.setHumanINN(entity.getInnNumber());
        datagramObject.setHumanSNILS(entity.getSnilsNumber());
        datagramObject.setHumanBirthPlace(identityCard.getBirthPlace());
        datagramObject.setHumanSex(SexCodes.MALE.equals(identityCard.getSex().getCode()) ? NsiUtils.TYPE_SEX_MALE : NsiUtils.TYPE_SEX_FEMALE);

        ICitizenship citizenship = identityCard.getCitizenship();

        StringBuilder warning = new StringBuilder();
        if (!(citizenship instanceof AddressCountry))
            warning.append("[Entity is not AddressCountry type!]");
        else
        {
            ProcessedDatagramObject<OksmType> r = createDatagramObject(OksmType.class, citizenship, commonCache, warning);
            HumanType.HumanCitizenshipID citizenshipId = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanTypeHumanCitizenshipID();
            citizenshipId.setOksm(r.getObject());
            datagramObject.setHumanCitizenshipID(citizenshipId);

            if (r.getList() != null)
                result.addAll(r.getList());
        }

        if(null != entity.getAddress())
        {
            if (entity.getAddress() instanceof AddressString)
            {
                ProcessedDatagramObject<AddressStringType> addrStrResult = createDatagramObject(AddressStringType.class, entity.getAddress(), commonCache, warning);
                HumanType.HumanAddressStringID addressStringID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanTypeHumanAddressStringID();
                addressStringID.setAddressString(addrStrResult.getObject());
                datagramObject.setHumanAddressStringID(addressStringID);
                if (addrStrResult.getList() != null) result.addAll(addrStrResult.getList());
            } else if (entity.getAddress() instanceof AddressRu)
            {
                ProcessedDatagramObject<AddressRUType> addrResult = createDatagramObject(AddressRUType.class, entity.getAddress(), commonCache, warning);
                HumanType.HumanAddressRuID addressRuID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanTypeHumanAddressRuID();
                addressRuID.setAddressRu(addrResult.getObject());
                datagramObject.setHumanAddressRuID(addressRuID);
                if (addrResult.getList() != null) result.addAll(addrResult.getList());
            }
        }

        if (null != principal)
        {
            ProcessedDatagramObject<PrincipalType> principalResult = createDatagramObject(PrincipalType.class, principal, commonCache, warning);
            HumanType.HumanPrincipalID principalID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanTypePrincipalID();
            principalID.setPrincipal(principalResult.getObject());
            datagramObject.setHumanPrincipalID(principalID);
            if (principalResult.getList() != null) result.addAll(principalResult.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(HumanType datagramObject, IUnknownDatagramsCollector collector)
    {
        HumanType.HumanCitizenshipID citizenshipID = datagramObject.getHumanCitizenshipID();
        OksmType oksm = citizenshipID == null ? null : citizenshipID.getOksm();
        if (oksm != null) collector.check(oksm.getID(), OksmType.class);

        HumanType.HumanAddressRuID addressRuID = datagramObject.getHumanAddressRuID();
        AddressRUType addr = addressRuID == null ? null : addressRuID.getAddressRu();
        if (addr != null) collector.check(addr.getID(), AddressRUType.class);

        HumanType.HumanAddressStringID addressStringID = datagramObject.getHumanAddressStringID();
        AddressStringType addrStr = addressStringID == null ? null : addressStringID.getAddressString();
        if (addrStr != null) collector.check(addrStr.getID(), AddressStringType.class);

        HumanType.HumanPrincipalID principalID = datagramObject.getHumanPrincipalID();
        PrincipalType principalType = principalID == null ? null : principalID.getPrincipal();
        if (principalType != null) collector.check(principalType.getID(), PrincipalType.class);
    }

    protected Person findPerson(String lastName, String firstName, String middleName, Date birthDate, String inn, Sex sex)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (lastName != null)
            expressions.add(eq(property(ENTITY_ALIAS, Person.identityCard().lastName()), value(lastName)));
        if (firstName != null)
            expressions.add(eq(property(ENTITY_ALIAS, Person.identityCard().firstName()), value(firstName)));
        if (middleName != null)
            expressions.add(eq(property(ENTITY_ALIAS, Person.identityCard().middleName()), value(middleName)));
        if (birthDate != null)
            expressions.add(eq(property(ENTITY_ALIAS, Person.identityCard().birthDate()), commonValue(birthDate, PropertyType.DATE)));
        if (inn != null)
            expressions.add(eq(property(ENTITY_ALIAS, Person.innNumber()), commonValue(inn)));
        if (sex != null)
            expressions.add(eq(property(ENTITY_ALIAS, Person.identityCard().sex().id()), commonValue(sex.getId())));
        if (expressions.isEmpty())
            return null;
        List<Person> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<Person> processDatagramObject(HumanType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();
        IdentityCard identityCard = additionalParams == null ? null : (IdentityCard) additionalParams.get(EXISTED_IDENTITY_CARD);
        if (identityCard != null) params.put(EXISTED_IDENTITY_CARD, identityCard);

        ILogger logger = NsiRequestHandlerService.instance().getLogger();

        // HumanID init
        String code = StringUtils.trimToNull(datagramObject.getHumanID());
        // HumanLastName init
        String lastName = StringUtils.trimToNull(datagramObject.getHumanLastName());
        // HumanFirstName init
        String firstName = StringUtils.trimToNull(datagramObject.getHumanFirstName());
        // HumanMiddleName init
        String middleName = StringUtils.trimToNull(datagramObject.getHumanMiddleName());
        // HumanLogin init
        String login = StringUtils.trimToNull(datagramObject.getHumanLogin());
        // HumanBirthDate init
        Date birthDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getHumanBirthDate()), "HumanBirthDate");
        // HumanBasicEmail init
        String basicEmail = StringUtils.trimToNull(datagramObject.getHumanBasicEmail());
        // HumanINN init
        String inn = StringUtils.trimToNull(datagramObject.getHumanINN());
        // HumanSNILS init
        String snils = StringUtils.trimToNull(datagramObject.getHumanSNILS());
        // HumanBirthPlace init
        String birthPlace = StringUtils.trimToNull(datagramObject.getHumanBirthPlace());
        // HumanBirthPlaceOKATO init
        String birthPlaceOKATO = StringUtils.trimToNull(datagramObject.getHumanBirthPlaceOKATO());
        // HumanSex init
        String sexString = StringUtils.trimToNull(datagramObject.getHumanSex());
        Sex sex = null;
        if (sexString != null)
        {
            if (sexString.equalsIgnoreCase(NsiUtils.TYPE_SEX_MALE))
                sex = DataAccessServices.dao().get(Sex.class, Sex.code(), SexCodes.MALE);
            if (sexString.equalsIgnoreCase(NsiUtils.TYPE_SEX_FEMALE))
                sex = DataAccessServices.dao().get(Sex.class, Sex.code(), SexCodes.FEMALE);
        }
        // HumanCitizenshipID init
        HumanType.HumanCitizenshipID citizenshipID = datagramObject.getHumanCitizenshipID();
        OksmType oksm = citizenshipID == null ? null : citizenshipID.getOksm();
        AddressCountry addressCountry = getOrCreate(OksmType.class, params, commonCache, oksm, CITIZENSHIP, null, warning);

        // HumanAddressStringID init
        HumanType.HumanAddressStringID addressStringID = datagramObject.getHumanAddressStringID();
        AddressStringType addrStr = addressStringID == null ? null : addressStringID.getAddressString();
        AddressBase address = getOrCreate(AddressStringType.class, params, commonCache, addrStr, ADDRESS_STRING, null, warning);

//        // PrincipalID init
//        HumanType.HumanPrincipalID humanPrincipalID = datagramObject.getHumanPrincipalID();
//        PrincipalType principalType = humanPrincipalID == null ? null : humanPrincipalID.getPrincipal();
//        Principal principal = getOrCreate(PrincipalType.class, params, commonCache, principalType, PRINCIPAL, null, warning);

        if (null == address)
        {
            // HumanAddressRuID init
            HumanType.HumanAddressRuID addressRuID = datagramObject.getHumanAddressRuID();
            AddressRUType addr = addressRuID == null ? null : addressRuID.getAddressRu();
            address = getOrCreate(AddressRUType.class, params, commonCache, addr, ADDRESS, null, warning);
        }

        boolean isNew = false;
        boolean isChanged = false;
        boolean isIdentityCardChanged = false;
        boolean isContactDataChanged = false;
        Person entity = null;
        Person2PrincipalRelation relation;
        Principal principal;
        CoreCollectionUtils.Pair<Person, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findPerson(lastName, firstName, middleName, birthDate, inn, sex);
        if (entity == null)
        {
//            throw new ProcessingDatagramObjectException("Пришедшая из НСИ персона не найдена в базе");
            entity = new Person();
            entity.setContactData(new PersonContactData());
            if (identityCard == null)
            {
                identityCard = new IdentityCard();
                identityCard.setCardType(DataAccessServices.dao().get(IdentityCardType.class, IdentityCardType.P_CODE, IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA));
                isIdentityCardChanged = true;
            }
            entity.setIdentityCard(identityCard);
            isContactDataChanged = true;

            // заполним данные принципала и связи персоны с принципалом
            principal = new Principal();
            principal.setActive(true);
            principal.setAuthenticationType(DataAccessServices.dao().get(AuthenticationType.class, AuthenticationType.code(), AuthenticationTypeCodes.SIMPLE));
            relation = new Person2PrincipalRelation();
            relation.setPerson(entity);
            relation.setPrincipal(principal);
            params.put(PRINCIPAL, principal);
            params.put(RELATIONS, relation);


            isChanged = true;
            isNew = true;
        }
        else
        {
            // если персона есть, то попробуем взять принципала
            relation = DataAccessServices.dao().getByNaturalId(new Person2PrincipalRelation.NaturalId(entity));
            principal = relation != null ? relation.getPrincipal() : null;

            if (identityCard == null)
                identityCard = entity.getIdentityCard();
        }

        // PrincipalID set
//        if (principal != null)
//        {
//            principal.setActive(true);
//            principal.setAuthenticationType(DataAccessServices.dao().get(AuthenticationType.class, AuthenticationType.code(), AuthenticationTypeCodes.SIMPLE));
//
//            // если и логин не пришёл, то нужно сгенерить новый
////            if (login == null)
////                login = PersonManager.instance().dao().getUniqueLogin(entity);
//
//            if (login != null && !login.equals(principal.getLogin()))
//            {
//                Principal tempPrincipal = DataAccessServices.dao().get(Principal.class, Principal.P_LOGIN, login);
//                if (tempPrincipal != null)
//                    throw new ProcessingDatagramObjectException("Login «" + login + "» is occupied.");
//
//                // ограничение на логин в 255 символов
//                if (login.length() > 255)
//                    throw new ProcessingDatagramObjectException("Длина строки в поле «login» должна быть в диапазоне от 0 до 255 символов.");
//                principal.setLogin(login);
//                principal.assignNewPassword(login);
//            }
//
//            List<PersonRole> personRoles = DataAccessServices.dao().getList(PersonRole.class, PersonRole.L_PERSON, entity);
//            if (personRoles.isEmpty())
//                logger.log(Level.ERROR, "Для переданной из НСИ персоны нет ни одной роли (для установки принципала нужна одна)");
//            if (personRoles.size() > 1)
//                logger.log(Level.ERROR, "Для переданной из НСИ персоны найдено более одной роли (для установки принципала нужна одна)");
//            PersonRole personRole = personRoles.size() == 1 ? personRoles.get(0) : null;
//            if (personRole != null)
//            {
//                personRole.setPrincipal(principal);
//                params.put(PERSON_ROLE, personRole);
//            }
//
//            if (relation == null) relation = new Person2PrincipalRelation();
//            relation.setPrincipal(principal);
//            relation.setPerson(entity);
//            params.put(RELATIONS, relation);
//
//            isChanged = true;
//        }

        // HumanLastName set
        if (lastName != null && !lastName.equals(identityCard.getLastName()))
        {
            isIdentityCardChanged = true;
            identityCard.setLastName(lastName);
        }
        // HumanFirstName set
        if (firstName != null && !firstName.equals(identityCard.getFirstName()))
        {
            isIdentityCardChanged = true;
            identityCard.setFirstName(firstName);
        }
        // HumanMiddleName set
        if (datagramObject.getHumanMiddleName() != null)
        {
            if (datagramObject.getHumanMiddleName().equals("") && identityCard.getMiddleName() != null)
            {
                isIdentityCardChanged = true;
                identityCard.setMiddleName(null);
            }
            else
            {
                if (middleName != null && !middleName.equals(identityCard.getMiddleName()))
                {
                    isIdentityCardChanged = true;
                    identityCard.setMiddleName(middleName);
                }
            }
        }

        // HumanLogin set
        if (login == null && isNew)
        {
            // В случае, если пришла пустая строка, то генерируем логин.
            login = PersonManager.instance().dao().getUniqueLogin(entity);
//            principal.setLogin(login);
//            principal.assignNewPassword(login);
        }
        if (null != login && !login.equals(principal.getLogin()))
        {
            Principal tempPrincipal = DataAccessServices.dao().get(Principal.class, Principal.P_LOGIN, login);
            if (tempPrincipal != null && !tempPrincipal.equals(principal))
                throw new ProcessingDatagramObjectException("Login «" + login + "» is occupied.");

            // ограничение на логин в 255 символов
            if (login.length() > 255)
                throw new ProcessingDatagramObjectException("Длина строки в поле «login» должна быть в диапазоне от 0 до 255 символов.");
            principal.setLogin(login);
            principal.assignNewPassword(login);
            params.put(PRINCIPAL, principal);
        }

        // HumanBirthDate set
        if (birthDate != null && !birthDate.equals(identityCard.getBirthDate()))
        {
            isIdentityCardChanged = true;
            identityCard.setBirthDate(birthDate);
        }
        // HumanBasicEmail set
        if (basicEmail != null && !basicEmail.equals(entity.getContactData().getEmail()))
        {
            isContactDataChanged = true;
            entity.getContactData().setEmail(basicEmail);
        }
        // HumanINN set
        if (inn != null && !inn.equals(entity.getInnNumber()))
        {
            isChanged = true;
            entity.setInnNumber(inn);
        }
        // HumanSNILS set
        if (snils != null && !snils.equals(entity.getSnilsNumber()))
        {
            isChanged = true;
            entity.setSnilsNumber(snils);
        }
        // HumanBirthPlace set
        if (birthPlace != null && !birthPlace.equals(identityCard.getBirthPlace()))
        {
            isIdentityCardChanged = true;
            identityCard.setBirthPlace(birthPlace);
        }
        // HumanSex set
        if (sex == null && isNew)
            throw new ProcessingDatagramObjectException("HumanType object without HumanSex!");
        if (sex != null && !sex.equals(identityCard.getSex()))
        {
            isIdentityCardChanged = true;
            identityCard.setSex(sex);
        }
        // HumanCitizenshipID set
        if (identityCard == null && isNew)
            throw new ProcessingDatagramObjectException("IdentityCardType object without HumanID!");
        if (addressCountry != null && !addressCountry.equals(identityCard.getCitizenship()))
        {
            isIdentityCardChanged = true;
            identityCard.setCitizenship(addressCountry);
        }
        if (address != null && !address.equals(entity.getAddress()))
        {
            entity.setAddress(address);
        }
        if (identityCard.getCitizenship() == null)
        {
            isIdentityCardChanged = true;
            NoCitizenship noCitizenship = DataAccessServices.dao().getList(NoCitizenship.class).get(0);
            identityCard.setCitizenship(noCitizenship);
        }

        if (isIdentityCardChanged)
        {
            isChanged = true;
            params.put(IDENTITY_CARD, true);
        }

        if (isContactDataChanged)
        {
            isChanged = true;
            params.put(CONTACT_DATA, true);
        }

        if (entity.getIdentityCard().getCardType() == null)
        {
            IdentityCardType cardType = DataAccessServices.dao().get(IdentityCardType.class, IdentityCardType.P_CODE, IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA);
            entity.getIdentityCard().setCardType(cardType);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<Person> w, HumanType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        Person entity = w.getResult();

        if (params != null)
        {
            saveChildEntity(OksmType.class, session, params, CITIZENSHIP, nsiPackage);
            saveChildEntity(AddressStringType.class, session, params, ADDRESS_STRING, nsiPackage);
            saveChildEntity(AddressRUType.class, session, params, ADDRESS, nsiPackage);
//            saveChildEntity(PrincipalType.class, session, params, PRINCIPAL, nsiPackage);

            IdentityCard identityCard = (IdentityCard) params.get(EXISTED_IDENTITY_CARD);
            if (identityCard != null)
            {
                identityCard.setPerson(null);
                entity.setIdentityCard((IdentityCard) session.merge(identityCard));
            }
            if (params.containsKey(IDENTITY_CARD) || identityCard != null)
                session.saveOrUpdate(entity.getIdentityCard());

            PersonContactData contactData = entity.getContactData();
            if (params.containsKey(CONTACT_DATA))
                session.saveOrUpdate(contactData);
        }

        super.save(session, w, datagramObject, nsiPackage);

        if (params != null)
        {
            Principal principal = (Principal) params.get(PRINCIPAL);
            if (principal != null)
            {
                session.saveOrUpdate(principal);
            }

            Person2PrincipalRelation relation = (Person2PrincipalRelation) params.get(RELATIONS);
            if (relation != null)
            {
                session.saveOrUpdate(relation);
            }

            PersonRole personRole = (PersonRole) params.get(PERSON_ROLE);
            if (personRole != null)
            {
                session.saveOrUpdate(personRole);
            }
        }
    }

    @Override
    public List<IDatagramObject> createTestObjects()
    {
        HumanType entity = new HumanType();
        entity.setID("cc5d937a-3202-3c23-8a11-humanTypexxX");
        entity.setHumanID("99999");
        entity.setHumanFirstName("firstNameTest");
        entity.setHumanLastName("lastNameTest");
        entity.setHumanMiddleName("middleNameTest");
        entity.setHumanBirthDate("1954-02-03");
        entity.setHumanBirthPlace("Москва");
        entity.setHumanSex(NsiUtils.TYPE_SEX_MALE);
        entity.setHumanINN("11111111");
        entity.setHumanLogin("login");
        entity.setHumanCitizenshipID(new HumanType.HumanCitizenshipID());
        @SuppressWarnings("unchecked")
        INsiEntityReactor<AddressCountry, OksmType> reactor = (INsiEntityReactor<AddressCountry, OksmType>) ReactorHolder.getReactorMap().get(OksmType.class.getSimpleName());
        entity.getHumanCitizenshipID().setOksm(reactor.getEmbedded(true));

        entity.setHumanAddressRuID(new HumanType.HumanAddressRuID());
        @SuppressWarnings("unchecked")
        INsiEntityReactor<AddressBase, AddressRUType> addrReactor = (INsiEntityReactor<AddressBase, AddressRUType>) ReactorHolder.getReactorMap().get(AddressRUType.class.getSimpleName());
        entity.getHumanAddressRuID().setAddressRu(addrReactor.getEmbedded(true));

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        datagrams.add(reactor.getEmbedded(false));
        return datagrams;
    }

    public static List<IDatagramObject> getFirstHumanTypeForEmbedded(boolean onlyGuid)
    {
        HumanType entity = new HumanType();
        entity.setID("cc5d937a-3202-3c23-8a11-humanEmbedd1");
        List<IDatagramObject> datagrams = new ArrayList<>();

        datagrams.add(entity);

        if (!onlyGuid)
        {
            entity.setHumanID("8888881");
            entity.setHumanFirstName("firstNameTest2");
            entity.setHumanLastName("lastNameTest2");
            entity.setHumanMiddleName("middleNameTest2");
            entity.setHumanBirthDate("1954-02-03");
            entity.setHumanBirthPlace("Москва");
            entity.setHumanSex(NsiUtils.TYPE_SEX_MALE);
            entity.setHumanINN("22222222");
            entity.setHumanLogin("login2");
            entity.setHumanCitizenshipID(new HumanType.HumanCitizenshipID());
            @SuppressWarnings("unchecked")
            INsiEntityReactor<AddressCountry, OksmType> reactor = (INsiEntityReactor<AddressCountry, OksmType>) ReactorHolder.getReactorMap().get(OksmType.class.getSimpleName());
            entity.getHumanCitizenshipID().setOksm(reactor.getEmbedded(true));

            entity.setHumanAddressRuID(new HumanType.HumanAddressRuID());
            @SuppressWarnings("unchecked")
            INsiEntityReactor<AddressBase, AddressRUType> addrReactor = (INsiEntityReactor<AddressBase, AddressRUType>) ReactorHolder.getReactorMap().get(AddressRUType.class.getSimpleName());
            entity.getHumanAddressRuID().setAddressRu(addrReactor.getEmbedded(true));

            datagrams.add(reactor.getEmbedded(false));
        }
        return datagrams;
    }

    public static List<IDatagramObject> getSecondHumanTypeForEmbedded(boolean onlyGuid)
    {
        HumanType entity = new HumanType();
        entity.setID("cc5d937a-3202-3c23-8a11-humanEmbedd2");
        List<IDatagramObject> datagrams = new ArrayList<>();

        datagrams.add(entity);

        if (!onlyGuid)
        {
            entity.setHumanID("8888882");
            entity.setHumanFirstName("firstNameTest3");
            entity.setHumanLastName("lastNameTest3");
            entity.setHumanMiddleName("middleNameTest3");
            entity.setHumanBirthDate("1954-02-03");
            entity.setHumanBirthPlace("Москва");
            entity.setHumanSex(NsiUtils.TYPE_SEX_MALE);
            entity.setHumanINN("33333333");
            entity.setHumanLogin("login3");
            @SuppressWarnings("unchecked")
            INsiEntityReactor<AddressCountry, OksmType> reactor = (INsiEntityReactor<AddressCountry, OksmType>) ReactorHolder.getReactorMap().get(OksmType.class.getSimpleName());
            entity.setHumanCitizenshipID(new HumanType.HumanCitizenshipID());
            entity.getHumanCitizenshipID().setOksm(reactor.getEmbedded(true));

            @SuppressWarnings("unchecked")
            INsiEntityReactor<AddressBase, AddressRUType> addrReactor = (INsiEntityReactor<AddressBase, AddressRUType>) ReactorHolder.getReactorMap().get(AddressRUType.class.getSimpleName());
            entity.setHumanAddressRuID(new HumanType.HumanAddressRuID());
            entity.getHumanAddressRuID().setAddressRu(addrReactor.getEmbedded(true));

            datagrams.add(reactor.getEmbedded(false));
        }
        return datagrams;
    }

    @Override
    public boolean isCorrect(Person entity, HumanType datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException
    {
        return datagramObject.getHumanLastName().equals(entity.getIdentityCard().getLastName())
                && datagramObject.getHumanFirstName().equals(entity.getIdentityCard().getFirstName())
                && datagramObject.getHumanMiddleName().equals(entity.getIdentityCard().getMiddleName())
                && datagramObject.getHumanBirthPlace().equals(entity.getIdentityCard().getBirthPlace())
                && datagramObject.getHumanINN().equals(entity.getInnNumber())
                && datagramObject.getHumanSex().equals(SexCodes.MALE.equals(entity.getIdentityCard().getSex().getCode()) ? NsiUtils.TYPE_SEX_MALE : NsiUtils.TYPE_SEX_FEMALE);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(Person.P_SERVICE_LENGTH_DAYS);
        fields.add(Person.P_SERVICE_LENGTH_MONTHS);
        fields.add(Person.P_SERVICE_LENGTH_YEARS);
        fields.add(Person.P_WORK_PLACE_POSITION);
        fields.add(Person.L_CONTACT_DATA);
        fields.add(Person.L_ADDRESS);
        fields.add(Person.P_LONG_SERVICE);
        fields.add(Person.P_CHILD_COUNT);
        fields.add(Person.L_IDENTITY_CARD);
        fields.add(Person.L_PENSION_TYPE);
        fields.add(Person.P_PENSION_ISSUANCE_DATE);
        fields.add(Person.P_NEED_DORMITORY);
        fields.add(Person.L_FLAT_PRESENCE);
        fields.add(Person.L_BLOOD_GROUP);
        fields.add(Person.L_BLOOD_GROUP);
        fields.add(Person.L_BIRTH_COUNTRY);
        fields.add(Person.P_DEPENDANT);
        fields.add(Person.P_WORK_PLACE);
        fields.add(Person.L_FAMILY_STATUS);

        return  fields;
    }
}