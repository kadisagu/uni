/* $Id: AcademicStatusTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.AcademicStatusType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class AcademicStatusTypeReactor extends BaseEntityReactor<ScienceStatus, AcademicStatusType>
{
    @Override
    public Class<ScienceStatus> getEntityClass()
    {
        return ScienceStatus.class;
    }

    @Override
    public AcademicStatusType createDatagramObject(String guid)
    {
        AcademicStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicStatusType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(ScienceStatus entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        AcademicStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createAcademicStatusType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setAcademicStatusID(entity.getCode());
        datagramObject.setAcademicStatusName(entity.getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected ScienceStatus findScienceStatus(String code, String title)
    {
        if (code != null)
        {
            List<ScienceStatus> list = findEntity(eq(property(ENTITY_ALIAS, ScienceStatus.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (title != null)
        {
            List<ScienceStatus> list = findEntity(eq(property(ENTITY_ALIAS, ScienceStatus.title()), value(title)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<ScienceStatus> processDatagramObject(AcademicStatusType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // AcademicStatusTypeID init
        String code = StringUtils.trimToNull(datagramObject.getAcademicStatusID());
        // AcademicStatusTypeName init
        String title = StringUtils.trimToNull(datagramObject.getAcademicStatusName());
        boolean isNew = false;
        boolean isChanged = false;
        ScienceStatus entity = null;

        CoreCollectionUtils.Pair<ScienceStatus, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findScienceStatus(code, title);
        if (entity == null)
        {
            entity = new ScienceStatus();
            isNew = true;
            isChanged = true;
        }


        // AcademicStatusName set
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            entity.setShortTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(ScienceStatus.class, ScienceStatus.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("AcademicStatusType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("AcademicStatusType object without title");
        }

        StringBuilder warning = new StringBuilder();

        // AcademicStatusTypeID set
        // Поле code immutable, поэтому оставляем как есть
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(ScienceStatus.class, entity, ScienceStatus.P_CODE))
            {
                if (code == null)
                    warning.append("[AcademicStatusType object without AcademicStatusTypeID!");
                else
                    warning.append("AcademicStatusTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(ScienceStatus.class);
                warning.append(" AcademicStatusTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }

            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(ScienceStatus.class, ScienceStatus.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);

        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[AcademicStatusTypeID is immutable!]");


        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(ScienceStatus.P_SHORT_TITLE);
        fields.add(ScienceStatus.L_TYPE);
        return  fields;
    }
}