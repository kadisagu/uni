/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.sec.entity.codes.AuthenticationTypeCodes;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.PrincipalType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ConditionalDatagramArrayList;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 29.09.2016
 */
public class PrincipalTypeReactor extends BaseEntityReactor<Principal, PrincipalType>
{
    @Override
    public Class<Principal> getEntityClass()
    {
        return Principal.class;
    }

    @Override
    public PrincipalType createDatagramObject(String guid)
    {
        PrincipalType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPrincipalType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(Principal entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        PrincipalType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPrincipalType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setPrincipalLogin(entity.getLogin());
        datagramObject.setPrincipalPasswordHash(entity.getPasswordHash());
        datagramObject.setPrincipalPasswordSalt(entity.getPasswordSalt());

        return new ResultListWrapper(null, result);
    }

    @Override
    public ChangedWrapper<Principal> processDatagramObject(@NotNull PrincipalType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();

        // PrincipalLogin init
        String login = StringUtils.trimToNull(datagramObject.getPrincipalLogin());
        // PrincipalPasswordHash init
        String passwordHash = StringUtils.trimToNull(datagramObject.getPrincipalPasswordHash());
        // PrincipalPasswordSalt init
        String passwordSalt = StringUtils.trimToNull(datagramObject.getPrincipalPasswordSalt());

        boolean isNew = false;
        boolean isChanged = false;
        Principal entity = null;

        CoreCollectionUtils.Pair<Principal, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findPrincipal(login);
        if (entity == null)
        {
            entity = new Principal();
            isNew = true;
            isChanged = true;
        }

        // PrincipalLogin set
        if (login != null && !login.equals(entity.getLogin()))
        {
            // ограничение на логин в 255 символов
            if(login.length() > 255)
                throw new ProcessingDatagramObjectException("Длина строки в поле «login» должна быть в диапазоне от 0 до 255 символов.");
            entity.setLogin(login);
            isChanged = true;
        }

        // PrincipalPasswordHash set
        if (passwordHash != null && !passwordHash.equals(entity.getPasswordHash()))
        {
            entity.setPasswordHash(passwordHash);
            isChanged = true;
        }

        // PrincipalPasswordSalt set
        if (passwordSalt != null && !passwordSalt.equals(entity.getPasswordSalt()))
        {
            entity.setPasswordSalt(passwordSalt);
            isChanged = true;
        }

        if (entity.getAuthenticationType() == null)
            entity.setAuthenticationType(DataAccessServices.dao().getByCode(AuthenticationType.class, AuthenticationTypeCodes.SIMPLE));

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, null);
    }

    private Principal findPrincipal(String login)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (login != null)
            expressions.add(eq(property(ENTITY_ALIAS, Principal.login()), value(login)));
        if (expressions.isEmpty())
            return null;
        List<Principal> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }
}
