/* $Id: ForeignLanguageTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.ForeignLanguageType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class ForeignLanguageTypeReactor extends BaseEntityReactor<ForeignLanguage, ForeignLanguageType>
{
    @Override
    public Class<ForeignLanguage> getEntityClass()
    {
        return ForeignLanguage.class;
    }

    @Override
    public ForeignLanguageType createDatagramObject(String guid)
    {
        ForeignLanguageType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createForeignLanguageType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(ForeignLanguage entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        ForeignLanguageType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createForeignLanguageType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setForeignLanguageID(entity.getCode());
        datagramObject.setForeignLanguageName(entity.getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected ForeignLanguage findForeignLanguage(String code, String title)
    {
        if (code != null)
        {
            List<ForeignLanguage> list = findEntity(eq(property(ENTITY_ALIAS, ForeignLanguage.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<ForeignLanguage> list = findEntity(eq(property(ENTITY_ALIAS, ForeignLanguage.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<ForeignLanguage> processDatagramObject(ForeignLanguageType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getForeignLanguageID());
        String title = StringUtils.trimToNull(datagramObject.getForeignLanguageName());

        boolean isNew = false;
        boolean isChanged = false;
        ForeignLanguage entity = null;

        CoreCollectionUtils.Pair<ForeignLanguage, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findForeignLanguage(code, title);
        if (entity == null)
        {
            entity = new ForeignLanguage();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(ForeignLanguage.class, ForeignLanguage.P_TITLE, entity.getId(), title);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("ForeignLanguageType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("ForeignLanguageType object without title");
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(ForeignLanguage.class, entity, ForeignLanguage.P_CODE))
            {
                if (code == null)
                    warning.append("[ForeignLanguageType object without ForeignLanguageTypeID!");
                else
                    warning.append("ForeignLanguageTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(ForeignLanguage.class);
                warning.append(" ForeignLanguageTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(ForeignLanguage.class, ForeignLanguage.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[ForeignLanguageTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}