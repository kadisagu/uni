/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 26.08.2015
 */
public class PersonEduDocumentTypeReactor extends BaseEntityReactor<PersonEduDocument, PersonEduDocumentType>
{
    private static final String ADDRESS = "AddressID";
    private static final String EDU_DOCUMENT_KIND = "EduDocumentKindID";
    private static final String EDU_LEVEL = "eduLevelID";
    private static final String GRADUATION_HONOR = "graduationHonourID";
    private static final String HUMAN = "humanID";

    @Override
    public Class<PersonEduDocument> getEntityClass()
    {
        return PersonEduDocument.class;
    }

    @Override
    public void collectUnknownDatagrams(PersonEduDocumentType datagramObject, IUnknownDatagramsCollector collector)
    {
        PersonEduDocumentType.AddressID addressID = datagramObject.getAddressID();
        AddressType addressType = addressID == null ? null : addressID.getAddress();
        if (addressType != null)
            collector.check(addressType.getID(), AddressType.class);

        PersonEduDocumentType.EduDocumentKindID eduDocumentKindID = datagramObject.getEduDocumentKindID();
        EduDocumentKindType eduDocumentKindType = eduDocumentKindID == null ? null : eduDocumentKindID.getEduDocumentKind();
        if (eduDocumentKindType != null)
            collector.check(eduDocumentKindType.getID(), EduDocumentKindType.class);

        PersonEduDocumentType.EduLevelID eduLevelID = datagramObject.getEduLevelID();
        EduLevelType eduLevelType = eduLevelID == null ? null : eduLevelID.getEduLevel();
        if (eduLevelType != null)
            collector.check(eduLevelType.getID(), EduLevelType.class);

        PersonEduDocumentType.GraduationHonourID graduationHonourID = datagramObject.getGraduationHonourID();
        GraduationHonourType graduationHonourType = graduationHonourID == null ? null : graduationHonourID.getGraduationHonour();
        if (graduationHonourType != null)
            collector.check(graduationHonourType.getID(), GraduationHonourType.class);

        PersonEduDocumentType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null)
            collector.check(humanType.getID(), HumanType.class);
    }

    protected PersonEduDocument findPersonEduDocument(Person person, EduDocumentKind eduDocumentKind, AddressItem eduOrganizationAddressItem,
                                                      String eduOrganization, String number, Integer yearEnd)
    {
        List<IExpression> expressions = new ArrayList<>();
        if (person != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonEduDocument.person()), value(person)));
        if (eduDocumentKind != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonEduDocument.eduDocumentKind()), value(eduDocumentKind)));
        if (eduOrganization != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonEduDocument.eduOrganization()), value(eduOrganization)));
        if (eduOrganizationAddressItem != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonEduDocument.eduOrganizationAddressItem()), value(eduOrganizationAddressItem)));
        if (number != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonEduDocument.number()), value(number)));
        if (yearEnd != null)
            expressions.add(eq(property(ENTITY_ALIAS, PersonEduDocument.yearEnd()), commonValue(yearEnd)));
        List<PersonEduDocument> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public PersonEduDocumentType createDatagramObject(String guid)
    {
        PersonEduDocumentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PersonEduDocument entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        PersonEduDocumentType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentType();

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(6);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setPersonEduDocumentDocumentEducationLevel(entity.getDocumentEducationLevel());
        datagramObject.setPersonEduDocumentEduOrganization(entity.getEduOrganization());
        datagramObject.setPersonEduDocumentEduProgramSubject(entity.getEduProgramSubject());
        datagramObject.setPersonEduDocumentIssuanceDate(NsiUtils.formatDate(entity.getIssuanceDate()));
        datagramObject.setPersonEduDocumentQualification(entity.getQualification());
        datagramObject.setPersonEduDocumentNumber(entity.getNumber());
        datagramObject.setPersonEduDocumentRegistrationNumber(entity.getRegistrationNumber());
        datagramObject.setPersonEduDocumentSeria(entity.getSeria());
        datagramObject.setPersonEduDocumentYearEnd(String.valueOf(entity.getYearEnd()));
        datagramObject.setPersonEduDocumentAvgMarkAsLong(entity.getAvgMarkAsLong() != null ? Long.toString(entity.getAvgMarkAsLong()) : null);
        datagramObject.setPersonEduDocumentMark3(entity.getMark3() != null ? Long.toString(entity.getMark3()) : null);
        datagramObject.setPersonEduDocumentMark4(entity.getMark4() != null ? Long.toString(entity.getMark4()) : null);
        datagramObject.setPersonEduDocumentMark5(entity.getMark5() != null ? Long.toString(entity.getMark5()) : null);

        ProcessedDatagramObject<AddressType> addressType = createDatagramObject(AddressType.class, entity.getEduOrganizationAddressItem(), commonCache, warning);
        PersonEduDocumentType.AddressID addressTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentTypeAddressID();
        addressTypeID.setAddress(addressType.getObject());
        datagramObject.setAddressID(addressTypeID);
        if (addressType.getList() != null)
            result.addAll(addressType.getList());

        ProcessedDatagramObject<EduDocumentKindType> eduDocumentKindType = createDatagramObject(EduDocumentKindType.class, entity.getEduDocumentKind(), commonCache, warning);
        PersonEduDocumentType.EduDocumentKindID eduDocumentKindTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentTypeEduDocumentKindID();
        eduDocumentKindTypeID.setEduDocumentKind(eduDocumentKindType.getObject());
        datagramObject.setEduDocumentKindID(eduDocumentKindTypeID);
        if (eduDocumentKindType.getList() != null)
            result.addAll(eduDocumentKindType.getList());

        if(entity.getEduLevel() != null) {
            ProcessedDatagramObject<EduLevelType> eduLevelType = createDatagramObject(EduLevelType.class, entity.getEduLevel(), commonCache, warning);
            PersonEduDocumentType.EduLevelID eduLevelID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentTypeEduLevelID();
            eduLevelID.setEduLevel(eduLevelType.getObject());
            datagramObject.setEduLevelID(eduLevelID);
            if (eduLevelType.getList() != null)
                result.addAll(eduLevelType.getList());
        }

        if(entity.getGraduationHonour() != null) {
            ProcessedDatagramObject<GraduationHonourType> graduationHonourType = createDatagramObject(GraduationHonourType.class, entity.getGraduationHonour(), commonCache, warning);
            PersonEduDocumentType.GraduationHonourID graduationHonourID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentTypeGraduationHonourID();
            graduationHonourID.setGraduationHonour(graduationHonourType.getObject());
            datagramObject.setGraduationHonourID(graduationHonourID);
            if (graduationHonourType.getList() != null)
                result.addAll(graduationHonourType.getList());
        }

        ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, entity.getPerson(), commonCache, warning);
        PersonEduDocumentType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPersonEduDocumentTypeHumanID();
        humanID.setHuman(humanType.getObject());
        datagramObject.setHumanID(humanID);
        if (humanType.getList() != null)
            result.addAll(humanType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<PersonEduDocument> processDatagramObject(PersonEduDocumentType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // PersonEduDocument init
        PersonEduDocumentType.AddressID addressID = datagramObject.getAddressID();
        AddressType addressType = addressID.getAddress();
        AddressItem addressItem = getOrCreate(AddressType.class, params, commonCache, addressType, ADDRESS, null, warning);

        PersonEduDocumentType.EduDocumentKindID eduDocumentKindID = datagramObject.getEduDocumentKindID();
        EduDocumentKindType eduDocumentKindType = eduDocumentKindID == null ? null : eduDocumentKindID.getEduDocumentKind();
        EduDocumentKind eduDocumentKind = getOrCreate(EduDocumentKindType.class, params, commonCache, eduDocumentKindType, EDU_DOCUMENT_KIND, null, warning);

        PersonEduDocumentType.EduLevelID eduLevelID = datagramObject.getEduLevelID();
        EduLevelType eduLevelType = eduLevelID == null ? null : eduLevelID.getEduLevel();
        EduLevel eduLevel = getOrCreate(EduLevelType.class, params, commonCache, eduLevelType, EDU_LEVEL, null, warning);

        PersonEduDocumentType.GraduationHonourID graduationHonourID = datagramObject.getGraduationHonourID();
        GraduationHonourType graduationHonourType = graduationHonourID == null ? null : graduationHonourID.getGraduationHonour();
        GraduationHonour graduationHonour = getOrCreate(GraduationHonourType.class, params, commonCache, graduationHonourType, GRADUATION_HONOR, null, warning);

        PersonEduDocumentType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);

        String documentEducationLevel = StringUtils.trimToNull(datagramObject.getPersonEduDocumentDocumentEducationLevel());
        String eduOrganization = StringUtils.trimToNull(datagramObject.getPersonEduDocumentEduOrganization());
        String eduProgramSubject = StringUtils.trimToNull(datagramObject.getPersonEduDocumentEduProgramSubject());
        Date issuanceDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getPersonEduDocumentIssuanceDate()), "PersonEduDocumentIssuanceDate");
        String qualification = StringUtils.trimToNull(datagramObject.getPersonEduDocumentQualification());
        String number = StringUtils.trimToNull(datagramObject.getPersonEduDocumentNumber());
        String registrationNumber = StringUtils.trimToNull(datagramObject.getPersonEduDocumentRegistrationNumber());
        String seria = StringUtils.trimToNull(datagramObject.getPersonEduDocumentSeria());
        Integer yearEnd = StringUtils.isNumeric(StringUtils.trimToNull(datagramObject.getPersonEduDocumentYearEnd())) ?
                Integer.parseInt(StringUtils.trimToNull(datagramObject.getPersonEduDocumentYearEnd())) : null;
        Long avgMarkAsLong = StringUtils.isNumeric(StringUtils.trimToNull(datagramObject.getPersonEduDocumentAvgMarkAsLong().replace("-", ""))) ?
                Long.parseLong(StringUtils.trimToNull(datagramObject.getPersonEduDocumentAvgMarkAsLong())) : null;
        Integer mark3 = StringUtils.isNumeric(StringUtils.trimToNull(datagramObject.getPersonEduDocumentMark3().replace("-", ""))) ?
                Integer.parseInt(StringUtils.trimToNull(datagramObject.getPersonEduDocumentMark3())) : null;
        Integer mark4 = StringUtils.isNumeric(StringUtils.trimToNull(datagramObject.getPersonEduDocumentMark4().replace("-", ""))) ?
                Integer.parseInt(StringUtils.trimToNull(datagramObject.getPersonEduDocumentMark4())) : null;
        Integer mark5 = StringUtils.isNumeric(StringUtils.trimToNull(datagramObject.getPersonEduDocumentMark5().replace("-", ""))) ?
                Integer.parseInt(StringUtils.trimToNull(datagramObject.getPersonEduDocumentMark5())) : null;

        boolean isNew = false;
        boolean isChanged = false;
        PersonEduDocument entity = null;

        CoreCollectionUtils.Pair<PersonEduDocument, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findPersonEduDocument(person, eduDocumentKind, addressItem, eduOrganization, number, yearEnd);
        if (entity == null)
        {
            entity = new PersonEduDocument();
            isNew = true;
            isChanged = true;
        }

        // PersonEduDocumentType set

        if (person != null && !person.equals(entity.getPerson()))
        {
            isChanged = true;
            entity.setPerson(person);
        }
        if (entity.getPerson() == null)
        {
            throw new ProcessingDatagramObjectException("PersonEduDocumentType object without HumanID");
        }
        if (addressItem != null && !addressItem.equals(entity.getEduOrganizationAddressItem()))
        {
            isChanged = true;
            entity.setEduOrganizationAddressItem(addressItem);
        }
        if (entity.getEduOrganizationAddressItem() == null)
        {
            throw new ProcessingDatagramObjectException("PersonEduDocumentType object without AddressID");
        }

        if (eduDocumentKind != null && !eduDocumentKind.equals(entity.getEduDocumentKind()))
        {
            isChanged = true;
            entity.setEduDocumentKind(eduDocumentKind);
        }
        if (entity.getEduDocumentKind() == null)
        {
            throw new ProcessingDatagramObjectException("PersonEduDocumentType object without EduDocumentKindID");
        }

        if (eduLevel != null && !eduLevel.equals(entity.getEduLevel()))
        {
            isChanged = true;
            entity.setEduLevel(eduLevel);
        }
        if (graduationHonour != null && !graduationHonour.equals(entity.getGraduationHonour()))
        {
            isChanged = true;
            entity.setGraduationHonour(graduationHonour);
        }

        if (eduOrganization != null && !eduOrganization.equals(entity.getEduOrganization()))
        {
            isChanged = true;
            entity.setEduOrganization(eduOrganization);
        }
        if (entity.getEduOrganization() == null)
        {
            throw new ProcessingDatagramObjectException("PersonEduDocumentType object without PersonEduDocumentEduOrganization");
        }

        if (number != null && !number.equals(entity.getNumber()))
        {
            isChanged = true;
            entity.setNumber(number);
        }
        if (entity.getNumber() == null)
        {
            throw new ProcessingDatagramObjectException("PersonEduDocumentType object without PersonEduDocumentNumber");
        }
        if (yearEnd != null && !yearEnd.equals(entity.getYearEnd()))
        {
            isChanged = true;
            entity.setYearEnd(yearEnd);
        }
        if (documentEducationLevel != null && !documentEducationLevel.equals(entity.getDocumentEducationLevel()))
        {
            isChanged = true;
            entity.setDocumentEducationLevel(documentEducationLevel);
        }
        if (eduProgramSubject != null && !eduProgramSubject.equals(entity.getEduProgramSubject()))
        {
            isChanged = true;
            entity.setEduProgramSubject(eduProgramSubject);
        }
        if (issuanceDate != null && !issuanceDate.equals(entity.getIssuanceDate()))
        {
            isChanged = true;
            entity.setIssuanceDate(issuanceDate);
        }
        if (qualification != null && !qualification.equals(entity.getQualification()))
        {
            isChanged = true;
            entity.setQualification(qualification);
        }
        if (registrationNumber != null && !registrationNumber.equals(entity.getRegistrationNumber()))
        {
            isChanged = true;
            entity.setRegistrationNumber(registrationNumber);
        }
        if (seria != null && !seria.equals(entity.getSeria()))
        {
            isChanged = true;
            entity.setSeria(seria);
        }
        if (avgMarkAsLong != null && !avgMarkAsLong.equals(entity.getAvgMarkAsLong()))
        {
            isChanged = true;
            entity.setAvgMarkAsLong(avgMarkAsLong);
        }

        if (mark3 != null && !mark3.equals(entity.getMark3()))
        {
            isChanged = true;
            entity.setMark3(mark3);
        }
        if (mark4 != null && !mark4.equals(entity.getMark4()))
        {
            isChanged = true;
            entity.setMark4(mark4);
        }
        if (mark5 != null && !mark5.equals(entity.getMark5()))
        {
            isChanged = true;
            entity.setMark5(mark5);
        }
        if (avgMarkAsLong < 0 || mark3 < 0 || mark4 < 0 || mark5 < 0)
        {
            throw new ProcessingDatagramObjectException("PersonEduDocumentType object's mark must be greater than zero.");
        }
        if(isNew)
        {
            entity.setCreationDate(new Date());
        }

        if ((entity.getDocumentKindDetailed() == null && entity.getEduDocumentKind().getCode().equals(EduDocumentKindCodes.OTHER))
                ||(entity.getDocumentKindDetailed() != null && !entity.getEduDocumentKind().getCode().equals(EduDocumentKindCodes.OTHER)) )
        {
            throw new ProcessingDatagramObjectException("Уточнять вид по документу можно только для документов с видом «Иной», и тогда это поле обязательно.");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<PersonEduDocument> w, PersonEduDocumentType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(AddressRUType.class, session, params, ADDRESS, nsiPackage);
            saveChildEntity(EduDocumentKindType.class, session, params, EDU_DOCUMENT_KIND, nsiPackage);
            saveChildEntity(EduLevelType.class, session, params, EDU_LEVEL, nsiPackage);
            saveChildEntity(GraduationHonourType.class, session, params, GRADUATION_HONOR, nsiPackage);
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(PersonEduDocument.L_SCAN_COPY);
        return  fields;
    }
}
