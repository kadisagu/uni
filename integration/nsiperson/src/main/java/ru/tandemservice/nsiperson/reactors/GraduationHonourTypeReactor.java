/* $Id$ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.GraduationHonourType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 26.08.2015
 */
public class GraduationHonourTypeReactor extends BaseEntityReactor<GraduationHonour, GraduationHonourType>
{
    @Override
    public Class<GraduationHonour> getEntityClass()
    {
        return GraduationHonour.class;
    }

    protected GraduationHonour findGraduationHonour(String code, String title)
    {
        if (code != null)
        {
            List<GraduationHonour> list = findEntity(eq(property(ENTITY_ALIAS, GraduationHonour.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (title != null)
        {
            List<GraduationHonour> list = findEntity(eq(property(ENTITY_ALIAS, GraduationHonour.title()), value(title)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public GraduationHonourType createDatagramObject(String guid)
    {
        GraduationHonourType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createGraduationHonourType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(GraduationHonour entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        GraduationHonourType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createGraduationHonourType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setGraduationHonourID(entity.getCode());
        datagramObject.setGraduationHonourName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    @Override
    public ChangedWrapper<GraduationHonour> processDatagramObject(GraduationHonourType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // ForeignLanguageSkill init
        String code = StringUtils.trimToNull(datagramObject.getGraduationHonourID());
        String title = StringUtils.trimToNull(datagramObject.getGraduationHonourName());

        boolean isNew = false;
        boolean isChanged = false;
        GraduationHonour entity = null;

        CoreCollectionUtils.Pair<GraduationHonour, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findGraduationHonour(code, title);
        if (entity == null)
        {
            entity = new GraduationHonour();
            isNew = true;
            isChanged = true;
        }

        // ForeignLanguageSkillType set
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
            boolean isExist = !NsiSyncManager.instance().reactorDAO().isPropertyUnique(GraduationHonour.class, GraduationHonour.P_TITLE, entity.getId(), title);
            if (isExist)
            {
                throw new ProcessingDatagramObjectException("GraduationHonourType title is not unique");
            }
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("GraduationHonourType object without title");
        }


        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(GraduationHonour.class, entity, GraduationHonour.P_CODE))
            {
                if (code == null)
                    warning.append("[GraduationHonourType object without GraduationHonourTypeID!");
                else
                    warning.append("GraduationHonourTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(GraduationHonour.class);
                warning.append(" GraduationHonourTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(GraduationHonour.class, GraduationHonour.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[GraduationHonourTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
