/* $Id: HumanContactDataTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiperson.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.HumanContactDataType;
import ru.tandemservice.nsiclient.datagram.HumanType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 07.07.2015
 */
public class HumanContactDataTypeReactor extends BaseEntityReactor<PersonContactData, HumanContactDataType>
{
    private static final String HUMAN = "HumanID";

    @Override
    public Class<PersonContactData> getEntityClass()
    {
        return PersonContactData.class;
    }

    @Override
    public void collectUnknownDatagrams(HumanContactDataType datagramObject, IUnknownDatagramsCollector collector)
    {
        HumanContactDataType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if (humanType != null) collector.check(humanType.getID(), HumanType.class);
    }

    @Override
    public HumanContactDataType createDatagramObject(String guid)
    {
        HumanContactDataType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanContactDataType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PersonContactData entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        HumanContactDataType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanContactDataType();
        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        Person person = DataAccessServices.dao().get(Person.class, Person.L_CONTACT_DATA, entity);
        if(person != null) {
            ProcessedDatagramObject<HumanType> humanType = createDatagramObject(HumanType.class, person, commonCache, warning);
            HumanContactDataType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createHumanContactDataTypeHumanID();
            humanID.setHuman(humanType.getObject());
            datagramObject.setHumanID(humanID);
            if (humanType.getList() != null)
                result.addAll(humanType.getList());
        }

        datagramObject.setHumanContactDataEmail(entity.getEmail());
        datagramObject.setHumanContactDataPhoneDefault(entity.getPhoneDefault());
        datagramObject.setHumanContactDataOther(entity.getOther());
        datagramObject.setHumanContactDataPhoneFact(entity.getPhoneFact());
        datagramObject.setHumanContactDataPhoneMobile(entity.getPhoneMobile());
        datagramObject.setHumanContactDataPhoneReg(entity.getPhoneReg());
        datagramObject.setHumanContactDataPhoneRegTemp(entity.getPhoneRegTemp());
        datagramObject.setHumanContactDataPhoneRelatives(entity.getPhoneRelatives());
        datagramObject.setHumanContactDataPhoneWork(entity.getPhoneWork());


        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected PersonContactData findPersonContactData(Person person)
    {
        if (person != null)
        {
            List<PersonContactData> list = findEntity(eq(property(ENTITY_ALIAS, PersonContactData.id()), value(person.getContactData().getId())));
            if (list.size() == 1)
            {
                return list.get(0);
            }
        }

        return null;
    }

    @Override
    public ChangedWrapper<PersonContactData> processDatagramObject(HumanContactDataType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();

        StringBuilder warning = new StringBuilder();

        HumanContactDataType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);
        String email = StringUtils.trimToNull(datagramObject.getHumanContactDataEmail());
        String other = StringUtils.trimToNull(datagramObject.getHumanContactDataOther());
        String phoneDefault = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneDefault());
        String phoneFact = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneFact());
        String phoneMobile = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneMobile());
        String phoneReg = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneReg());
        String phoneRegTemp = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneRegTemp());
        String phoneRelatives = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneRelatives());
        String phoneWork = StringUtils.trimToNull(datagramObject.getHumanContactDataPhoneWork());

        boolean isNew = false;
        boolean isChanged = false;
        PersonContactData entity = null;

        CoreCollectionUtils.Pair<PersonContactData, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findPersonContactData(person);
        if (entity == null)
        {
            entity = new PersonContactData();
            isNew = true;
            isChanged = true;
        }

        if (email != null && !email.equals(entity.getEmail()))
        {
            isChanged = true;
            entity.setEmail(email);
        }
        if (other != null && !other.equals(entity.getOther()))
        {
            isChanged = true;
            entity.setOther(other);
        }
        if (phoneDefault != null && !phoneDefault.equals(entity.getPhoneDefault()))
        {
            isChanged = true;
            entity.setPhoneDefault(phoneDefault);
        }
        if (phoneFact != null && !phoneFact.equals(entity.getPhoneFact()))
        {
            isChanged = true;
            entity.setPhoneFact(phoneFact);
        }
        if (phoneMobile != null && !phoneMobile.equals(entity.getPhoneMobile()))
        {
            isChanged = true;
            entity.setPhoneMobile(phoneMobile);
        }
        if (phoneReg != null && !phoneReg.equals(entity.getPhoneReg()))
        {
            isChanged = true;
            entity.setPhoneReg(phoneReg);
        }
        if (phoneRegTemp != null && !phoneRegTemp.equals(entity.getPhoneRegTemp()))
        {
            isChanged = true;
            entity.setPhoneRegTemp(phoneRegTemp);
        }
        if (phoneRelatives != null && !phoneRelatives.equals(entity.getPhoneRelatives()))
        {
            isChanged = true;
            entity.setPhoneRelatives(phoneRelatives);
        }
        if (phoneWork != null && !phoneWork.equals(entity.getPhoneWork()))
        {
            isChanged = true;
            entity.setPhoneWork(phoneWork);
        }

        if (isNew && person == null)
            throw new ProcessingDatagramObjectException("HumanContactDataType object without HumanID!");

        if (person != null && !entity.equals(person.getContactData()))
        {
            isChanged = true;
            person.getContactData().update(entity);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<PersonContactData> w, HumanContactDataType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }
}