//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.07.02 at 01:39:16 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника Контактные данные физического лица
 * 
 * <p>Java class for HumanContactDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HumanContactDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneReg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneRegTemp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneFact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneDefault" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneWork" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataPhoneRelatives" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanContactDataOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Human" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HumanContactDataType", propOrder = {

})
public class HumanContactDataType implements IDatagramObject
{

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "HumanContactDataPhoneReg")
    protected String humanContactDataPhoneReg;
    @XmlElement(name = "HumanContactDataPhoneRegTemp")
    protected String humanContactDataPhoneRegTemp;
    @XmlElement(name = "HumanContactDataPhoneFact")
    protected String humanContactDataPhoneFact;
    @XmlElement(name = "HumanContactDataPhoneDefault")
    protected String humanContactDataPhoneDefault;
    @XmlElement(name = "HumanContactDataPhoneWork")
    protected String humanContactDataPhoneWork;
    @XmlElement(name = "HumanContactDataPhoneMobile")
    protected String humanContactDataPhoneMobile;
    @XmlElement(name = "HumanContactDataPhoneRelatives")
    protected String humanContactDataPhoneRelatives;
    @XmlElement(name = "HumanContactDataEmail")
    protected String humanContactDataEmail;
    @XmlElement(name = "HumanContactDataOther")
    protected String humanContactDataOther;
    @XmlElement(name = "HumanID")
    protected HumanID humanID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneReg() {
        return humanContactDataPhoneReg;
    }

    /**
     * Sets the value of the humanContactDataPhoneReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneReg(String value) {
        this.humanContactDataPhoneReg = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneRegTemp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneRegTemp() {
        return humanContactDataPhoneRegTemp;
    }

    /**
     * Sets the value of the humanContactDataPhoneRegTemp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneRegTemp(String value) {
        this.humanContactDataPhoneRegTemp = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneFact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneFact() {
        return humanContactDataPhoneFact;
    }

    /**
     * Sets the value of the humanContactDataPhoneFact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneFact(String value) {
        this.humanContactDataPhoneFact = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneDefault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneDefault() {
        return humanContactDataPhoneDefault;
    }

    /**
     * Sets the value of the humanContactDataPhoneDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneDefault(String value) {
        this.humanContactDataPhoneDefault = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneWork property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneWork() {
        return humanContactDataPhoneWork;
    }

    /**
     * Sets the value of the humanContactDataPhoneWork property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneWork(String value) {
        this.humanContactDataPhoneWork = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneMobile() {
        return humanContactDataPhoneMobile;
    }

    /**
     * Sets the value of the humanContactDataPhoneMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneMobile(String value) {
        this.humanContactDataPhoneMobile = value;
    }

    /**
     * Gets the value of the humanContactDataPhoneRelatives property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataPhoneRelatives() {
        return humanContactDataPhoneRelatives;
    }

    /**
     * Sets the value of the humanContactDataPhoneRelatives property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataPhoneRelatives(String value) {
        this.humanContactDataPhoneRelatives = value;
    }

    /**
     * Gets the value of the humanContactDataEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataEmail() {
        return humanContactDataEmail;
    }

    /**
     * Sets the value of the humanContactDataEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataEmail(String value) {
        this.humanContactDataEmail = value;
    }

    /**
     * Gets the value of the humanContactDataOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanContactDataOther() {
        return humanContactDataOther;
    }

    /**
     * Sets the value of the humanContactDataOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanContactDataOther(String value) {
        this.humanContactDataOther = value;
    }

    /**
     * Gets the value of the humanID property.
     * 
     * @return
     *     possible object is
     *     {@link HumanID }
     *     
     */
    public HumanID getHumanID() {
        return humanID;
    }

    /**
     * Sets the value of the humanID property.
     * 
     * @param value
     *     allowed object is
     *     {@link HumanID }
     *     
     */
    public void setHumanID(HumanID value) {
        this.humanID = value;
    }

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Human" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class HumanID {

        @XmlElement(name = "Human")
        protected HumanType human;

        /**
         * Gets the value of the human property.
         * 
         * @return
         *     possible object is
         *     {@link HumanType }
         *     
         */
        public HumanType getHuman() {
            return human;
        }

        /**
         * Sets the value of the human property.
         * 
         * @param value
         *     allowed object is
         *     {@link HumanType }
         *     
         */
        public void setHuman(HumanType value) {
            this.human = value;
        }

    }

}
