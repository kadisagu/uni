//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.25 at 02:06:34 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


/**
 * Элемент справочника Иностранный язык персоны
 * 
 * <p>Java class for PersonForeignLanguageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonForeignLanguageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="HumanID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Human" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ForeignLanguageID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ForeignLanguage" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ForeignLanguageType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ForeignLanguageSkillID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ForeignLanguageSkill" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ForeignLanguageSkillType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PersonForeignLanguageMain" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonForeignLanguageType", propOrder = {

})
public class PersonForeignLanguageType implements IDatagramObject
{

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "HumanID")
    protected HumanID humanID;
    @XmlElement(name = "ForeignLanguageID")
    protected ForeignLanguageID foreignLanguageID;
    @XmlElement(name = "ForeignLanguageSkillID")
    protected ForeignLanguageSkillID foreignLanguageSkillID;
    @XmlElement(name = "PersonForeignLanguageMain")
    @XmlSchemaType(name = "anySimpleType")
    protected String personForeignLanguageMain;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the humanID property.
     *
     * @return
     *     possible object is
     *     {@link HumanID }
     *
     */
    public HumanID getHumanID() {
        return humanID;
    }

    /**
     * Sets the value of the humanID property.
     *
     * @param value
     *     allowed object is
     *     {@link HumanID }
     *
     */
    public void setHumanID(HumanID value) {
        this.humanID = value;
    }

    /**
     * Gets the value of the foreignLanguageID property.
     *
     * @return
     *     possible object is
     *     {@link ForeignLanguageID }
     *
     */
    public ForeignLanguageID getForeignLanguageID() {
        return foreignLanguageID;
    }

    /**
     * Sets the value of the foreignLanguageID property.
     *
     * @param value
     *     allowed object is
     *     {@link ForeignLanguageID }
     *
     */
    public void setForeignLanguageID(ForeignLanguageID value) {
        this.foreignLanguageID = value;
    }

    /**
     * Gets the value of the foreignLanguageSkillID property.
     *
     * @return
     *     possible object is
     *     {@link ForeignLanguageSkillID }
     *
     */
    public ForeignLanguageSkillID getForeignLanguageSkillID() {
        return foreignLanguageSkillID;
    }

    /**
     * Sets the value of the foreignLanguageSkillID property.
     *
     * @param value
     *     allowed object is
     *     {@link ForeignLanguageSkillID }
     *
     */
    public void setForeignLanguageSkillID(ForeignLanguageSkillID value) {
        this.foreignLanguageSkillID = value;
    }

    /**
     * Gets the value of the personForeignLanguageMain property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPersonForeignLanguageMain() {
        return personForeignLanguageMain;
    }

    /**
     * Sets the value of the personForeignLanguageMain property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPersonForeignLanguageMain(String value) {
        this.personForeignLanguageMain = value;
    }

    /**
     * Gets the value of the oid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     *
     * <p>
     * the map is keyed by the name of the attribute and
     * the value is the string value of the attribute.
     *
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     *
     *
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ForeignLanguage" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ForeignLanguageType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class ForeignLanguageID {

        @XmlElement(name = "ForeignLanguage")
        protected ForeignLanguageType foreignLanguage;

        /**
         * Gets the value of the foreignLanguage property.
         *
         * @return
         *     possible object is
         *     {@link ForeignLanguageType }
         *
         */
        public ForeignLanguageType getForeignLanguage() {
            return foreignLanguage;
        }

        /**
         * Sets the value of the foreignLanguage property.
         *
         * @param value
         *     allowed object is
         *     {@link ForeignLanguageType }
         *
         */
        public void setForeignLanguage(ForeignLanguageType value) {
            this.foreignLanguage = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ForeignLanguageSkill" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}ForeignLanguageSkillType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class ForeignLanguageSkillID {

        @XmlElement(name = "ForeignLanguageSkill")
        protected ForeignLanguageSkillType foreignLanguageSkill;

        /**
         * Gets the value of the foreignLanguageSkill property.
         *
         * @return
         *     possible object is
         *     {@link ForeignLanguageSkillType }
         *
         */
        public ForeignLanguageSkillType getForeignLanguageSkill() {
            return foreignLanguageSkill;
        }

        /**
         * Sets the value of the foreignLanguageSkill property.
         *
         * @param value
         *     allowed object is
         *     {@link ForeignLanguageSkillType }
         *
         */
        public void setForeignLanguageSkill(ForeignLanguageSkillType value) {
            this.foreignLanguageSkill = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Human" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class HumanID {

        @XmlElement(name = "Human")
        protected HumanType human;

        /**
         * Gets the value of the human property.
         *
         * @return
         *     possible object is
         *     {@link HumanType }
         *
         */
        public HumanType getHuman() {
            return human;
        }

        /**
         * Sets the value of the human property.
         *
         * @param value
         *     allowed object is
         *     {@link HumanType }
         *     
         */
        public void setHuman(HumanType value) {
            this.human = value;
        }

    }

}
