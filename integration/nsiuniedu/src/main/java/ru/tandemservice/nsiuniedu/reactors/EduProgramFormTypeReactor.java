/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EduProgramFormType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduProgramFormTypeReactor extends BaseEntityReactor<EduProgramForm, EduProgramFormType>
{
    @Override
    public Class<EduProgramForm> getEntityClass()
    {
        return EduProgramForm.class;
    }

    @Override
    public EduProgramFormType createDatagramObject(String guid)
    {
        EduProgramFormType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramFormType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramForm entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramFormType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramFormType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramFormID(entity.getCode());
        datagramObject.setEduProgramFormName(entity.getTitle());
        datagramObject.setEduProgramFormNameShort(entity.getShortTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EduProgramForm findEduProgramForm(String code, String name, String shortName)
    {
        if (code != null)
        {
            List<EduProgramForm> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramForm.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (name != null)
        {
            List<EduProgramForm> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramForm.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortName != null)
        {
            List<EduProgramForm> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramForm.shortTitle()), value(shortName)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduProgramForm> processDatagramObject(EduProgramFormType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // EduProgramForm init
        String code = StringUtils.trimToNull(datagramObject.getEduProgramFormID());
        String name = StringUtils.trimToNull(datagramObject.getEduProgramFormName());
        String shortName = StringUtils.trimToNull(datagramObject.getEduProgramFormNameShort());

        boolean isNew = false;
        boolean isChanged = false;
        EduProgramForm entity = null;

        CoreCollectionUtils.Pair<EduProgramForm, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduProgramForm(code, name, shortName);
        if (entity == null)
        {
            entity = new EduProgramForm();
            isNew = true;
            isChanged = true;
        }

        // EduProgramForm set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EduProgramForm.class, entity, EduProgramForm.P_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EduProgramFormType EduProgramFormName is not unique");
            }
        }

        if (shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EduProgramForm.class, entity, EduProgramForm.P_SHORT_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EduProgramFormType EduProgramFormShortName is not unique");
            }
        }
        if (null == entity.getShortTitle())
        {
            throw new ProcessingDatagramObjectException("EduProgramFormType object without EduProgramFormShortName!");
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduProgramForm.class, entity, EduProgramForm.P_CODE))
            {
                if (code == null)
                    warning.append("[EduProgramFormType object without EduProgramFormTypeID!");
                else
                    warning.append("EduProgramFormTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramForm.class);
                warning.append(" EduProgramFormTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduProgramFormTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
