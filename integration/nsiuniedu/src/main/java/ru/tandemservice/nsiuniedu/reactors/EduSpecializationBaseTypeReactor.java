/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.EduProgramSubjectType;
import ru.tandemservice.nsiclient.datagram.EduSpecializationBaseType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduSpecializationBaseTypeReactor extends BaseEntityReactor<EduProgramSpecialization, EduSpecializationBaseType>
{
    private static final String PROGRAM_SUBJECT = "programSubjectID";

    @Override
    public Class<EduProgramSpecialization> getEntityClass()
    {
        return EduProgramSpecialization.class;
    }

    @Override
    public void collectUnknownDatagrams(EduSpecializationBaseType datagramObject, IUnknownDatagramsCollector collector)
    {
        EduSpecializationBaseType.EduProgramSubjectID programSubjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType programSubjectType = programSubjectID == null ? null : programSubjectID.getEduProgramSubject();
        if (programSubjectType != null)
            collector.check(programSubjectType.getID(), EduProgramSubjectType.class);
    }

    @Override
    public EduSpecializationBaseType createDatagramObject(String guid)
    {
        EduSpecializationBaseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduSpecializationBaseType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramSpecialization entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduSpecializationBaseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduSpecializationBaseType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduSpecializationBaseID(entity.getCode());
        datagramObject.setEduSpecializationBaseNameShort(entity.getShortTitle());
        if (entity instanceof EduProgramSpecializationRoot)
        {
            datagramObject.setEduSpecializationBaseRoot(NsiUtils.formatBoolean(true));
        }
        else
        {
            datagramObject.setEduSpecializationBaseRoot(NsiUtils.formatBoolean(false));
        }

        ProcessedDatagramObject<EduProgramSubjectType> programSubjectType = createDatagramObject(EduProgramSubjectType.class, entity.getProgramSubject(), commonCache, warning);
        EduSpecializationBaseType.EduProgramSubjectID programSubjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduSpecializationBaseTypeEduProgramSubjectID();
        programSubjectID.setEduProgramSubject(programSubjectType.getObject());
        datagramObject.setEduProgramSubjectID(programSubjectID);
        if (programSubjectType.getList() != null)
            result.addAll(programSubjectType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EduProgramSpecialization findEduProgramSpecialization(String code)
    {
        if (code != null)
        {
            List<EduProgramSpecialization> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramSpecialization.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }


    @Override
    public ChangedWrapper<EduProgramSpecialization> processDatagramObject(EduSpecializationBaseType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // EduProgramForm init
        String code = StringUtils.trimToNull(datagramObject.getEduSpecializationBaseID());
        String nameShort = StringUtils.trimToNull(datagramObject.getEduSpecializationBaseNameShort());

        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        EduSpecializationBaseType.EduProgramSubjectID programSubjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType programSubjectType = programSubjectID == null ? null : programSubjectID.getEduProgramSubject();
        EduProgramSubject programSubject = getOrCreate(EduProgramSubjectType.class, params, commonCache, programSubjectType, PROGRAM_SUBJECT, null, warning);
        Boolean isRoot = NsiUtils.parseBoolean(datagramObject.getEduSpecializationBaseRoot());

        boolean isNew = false;
        boolean isChanged = false;
        EduProgramSpecialization entity = null;

        CoreCollectionUtils.Pair<EduProgramSpecialization, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduProgramSpecialization(code);
        if (entity == null)
        {
            if (Boolean.TRUE.equals(isRoot))
            {
                entity = new EduProgramSpecializationRoot();
            }
            else
            {
                entity = new EduProgramSpecializationChild();
            }
            isNew = true;
            isChanged = true;
        }

        // EduProgramSpecialization set
        if (nameShort != null && !nameShort.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setTitle(nameShort);
            entity.setShortTitle(nameShort);
        }

        if (programSubject != null && !programSubject.equals(entity.getProgramSubject()))
        {
            isChanged = true;
            entity.setProgramSubject(programSubject);
        }

        if (null == entity.getProgramSubject())
        {
            throw new ProcessingDatagramObjectException("EduSpecializationBaseType object without EduProgramSubjectID");
        }

        if (entity instanceof EduProgramSpecializationRoot && !DataAccessServices.dao().isUnique(EduProgramSpecialization.class, entity, EduProgramSpecialization.L_PROGRAM_SUBJECT))
        {
            throw new ProcessingDatagramObjectException("EduSpecializationBaseType object has not unique EduProgramSubjectID");
        }


        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduProgramSpecialization.class, entity, EduProgramSpecialization.P_CODE))
            {
                if (code == null)
                    warning.append("[EduSpecializationBaseType object without EduSpecializationBaseID!");
                else
                    warning.append("EduSpecializationBaseID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramSpecialization.class);
                warning.append(" EduSpecializationBaseID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduSpecializationBaseID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EduProgramSpecialization> w, EduSpecializationBaseType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EduProgramSubjectType.class, session, params, PROGRAM_SUBJECT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EduProgramSpecialization.P_TITLE);

        return  fields;
    }
}
