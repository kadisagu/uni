//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.25 at 06:19:48 PM YEKT
//


package ru.tandemservice.nsiuniedu.reactors;

import ru.tandemservice.nsiclient.datagram.*;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.nsi.datagram package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.nsi.datagram
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonForeignLanguageType }
     *
     */
    public PersonForeignLanguageType createPersonForeignLanguageType() {
        return new PersonForeignLanguageType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.datagram.PersonEduDocumentType }
     *
     */
    public PersonEduDocumentType createPersonEduDocumentType() {
        return new PersonEduDocumentType();
    }

    /**
     * Create an instance of {@link EduSpecializationBaseType }
     *
     */
    public EduSpecializationBaseType createEduSpecializationBaseType() {
        return new EduSpecializationBaseType();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType }
     *
     */
    public EduProgramHigherProfType createEduProgramHigherProfType() {
        return new EduProgramHigherProfType();
    }

    /**
     * Create an instance of {@link EduLevelType }
     *
     */
    public EduLevelType createEduLevelType() {
        return new EduLevelType();
    }

    /**
     * Create an instance of {@link AddressTypeType }
     *
     */
    public AddressTypeType createAddressTypeType() {
        return new AddressTypeType();
    }

    /**
     * Create an instance of {@link AddressType }
     *
     */
    public AddressType createAddressType() {
        return new AddressType();
    }


    /**
     * Create an instance of {@link AddressLevelType }
     *
     */
    public AddressLevelType createAddressLevelType() {
        return new AddressLevelType();
    }

    /**
     * Create an instance of {@link DepartmentType }
     *
     */
    public DepartmentType createDepartmentType() {
        return new DepartmentType();
    }

    /**
     * Create an instance of {@link EduDocumentKindType }
     *
     */
    public EduDocumentKindType createEduDocumentKindType() {
        return new EduDocumentKindType();
    }

    /**
     * Create an instance of {@link EduProgramDurationType }
     *
     */
    public EduProgramDurationType createEduProgramDurationType() {
        return new EduProgramDurationType();
    }

    /**
     * Create an instance of {@link EduProgramFormType }
     *
     */
    public EduProgramFormType createEduProgramFormType() {
        return new EduProgramFormType();
    }

    /**
     * Create an instance of {@link EduProgramQualificationType }
     *
     */
    public EduProgramQualificationType createEduProgramQualificationType() {
        return new EduProgramQualificationType();
    }

    /**
     * Create an instance of {@link EduProgramSubjectType }
     *
     */
    public EduProgramSubjectType createEduProgramSubjectType() {
        return new EduProgramSubjectType();
    }

    /**
     * Create an instance of {@link EduProgramTraitType }
     *
     */
    public EduProgramTraitType createEduProgramTraitType() {
        return new EduProgramTraitType();
    }

    /**
     * Create an instance of {@link ForeignLanguageType }
     *
     */
    public ForeignLanguageType createForeignLanguageType() {
        return new ForeignLanguageType();
    }

    /**
     * Create an instance of {@link ForeignLanguageSkillType }
     *
     */
    public ForeignLanguageSkillType createForeignLanguageSkillType() {
        return new ForeignLanguageSkillType();
    }

    /**
     * Create an instance of {@link GraduationHonourType }
     *
     */
    public GraduationHonourType createGraduationHonourType() {
        return new GraduationHonourType();
    }

    /**
     * Create an instance of {@link HumanType }
     *
     */
    public HumanType createHumanType() {
        return new HumanType();
    }

    /**
     * Create an instance of {@link IdentityCardType }
     *
     */
    public IdentityCardType createIdentityCardType() {
        return new IdentityCardType();
    }

    /**
     * Create an instance of {@link OksmType }
     *
     */
    public OksmType createOksmType() {
        return new OksmType();
    }

    /**
     * Create an instance of {@link PersonForeignLanguageType.HumanID }
     *
     */
    public PersonForeignLanguageType.HumanID createPersonForeignLanguageTypeHumanID() {
        return new PersonForeignLanguageType.HumanID();
    }

    /**
     * Create an instance of {@link PersonForeignLanguageType.ForeignLanguageID }
     *
     */
    public PersonForeignLanguageType.ForeignLanguageID createPersonForeignLanguageTypeForeignLanguageID() {
        return new PersonForeignLanguageType.ForeignLanguageID();
    }

    /**
     * Create an instance of {@link PersonForeignLanguageType.ForeignLanguageSkillID }
     *
     */
    public PersonForeignLanguageType.ForeignLanguageSkillID createPersonForeignLanguageTypeForeignLanguageSkillID() {
        return new PersonForeignLanguageType.ForeignLanguageSkillID();
    }

    /**
     * Create an instance of {@link PersonEduDocumentType.HumanID }
     *
     */
    public PersonEduDocumentType.HumanID createPersonEduDocumentTypeHumanID() {
        return new PersonEduDocumentType.HumanID();
    }

    /**
     * Create an instance of {@link PersonEduDocumentType.EduDocumentKindID }
     *
     */
    public PersonEduDocumentType.EduDocumentKindID createPersonEduDocumentTypeEduDocumentKindID() {
        return new PersonEduDocumentType.EduDocumentKindID();
    }

    /**
     * Create an instance of {@link PersonEduDocumentType.AddressID }
     *
     */
    public PersonEduDocumentType.AddressID createPersonEduDocumentTypeAddressID() {
        return new PersonEduDocumentType.AddressID();
    }

    /**
     * Create an instance of {@link PersonEduDocumentType.EduLevelID }
     *
     */
    public PersonEduDocumentType.EduLevelID createPersonEduDocumentTypeEduLevelID() {
        return new PersonEduDocumentType.EduLevelID();
    }

    /**
     * Create an instance of {@link PersonEduDocumentType.GraduationHonourID }
     *
     */
    public PersonEduDocumentType.GraduationHonourID createPersonEduDocumentTypeGraduationHonourID() {
        return new PersonEduDocumentType.GraduationHonourID();
    }

    /**
     * Create an instance of {@link EduSpecializationBaseType.EduProgramSubjectID }
     *
     */
    public EduSpecializationBaseType.EduProgramSubjectID createEduSpecializationBaseTypeEduProgramSubjectID() {
        return new EduSpecializationBaseType.EduProgramSubjectID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.DepartmentID }
     *
     */
    public EduProgramHigherProfType.DepartmentID createEduProgramHigherProfTypeDepartmentID() {
        return new EduProgramHigherProfType.DepartmentID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.OwnerOrgUnitID }
     *
     */
    public EduProgramHigherProfType.OwnerOrgUnitID createEduProgramHigherProfTypeOwnerOrgUnitID() {
        return new EduProgramHigherProfType.OwnerOrgUnitID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduProgramFormID }
     *
     */
    public EduProgramHigherProfType.EduProgramFormID createEduProgramHigherProfTypeEduProgramFormID() {
        return new EduProgramHigherProfType.EduProgramFormID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduProgramDurationID }
     *
     */
    public EduProgramHigherProfType.EduProgramDurationID createEduProgramHigherProfTypeEduProgramDurationID() {
        return new EduProgramHigherProfType.EduProgramDurationID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduProgramTraitID }
     *
     */
    public EduProgramHigherProfType.EduProgramTraitID createEduProgramHigherProfTypeEduProgramTraitID() {
        return new EduProgramHigherProfType.EduProgramTraitID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduSpecializationBaseID }
     *
     */
    public EduProgramHigherProfType.EduSpecializationBaseID createEduProgramHigherProfTypeEduSpecializationBaseID() {
        return new EduProgramHigherProfType.EduSpecializationBaseID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduProgramQualificationID }
     *
     */
    public EduProgramHigherProfType.EduProgramQualificationID createEduProgramHigherProfTypeEduProgramQualificationID() {
        return new EduProgramHigherProfType.EduProgramQualificationID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduLevelID }
     *
     */
    public EduProgramHigherProfType.EduLevelID createEduProgramHigherProfTypeEduLevelID() {
        return new EduProgramHigherProfType.EduLevelID();
    }

    /**
     * Create an instance of {@link EduProgramHigherProfType.EduProgramSubjectID }
     *
     */
    public EduProgramHigherProfType.EduProgramSubjectID createEduProgramHigherProfTypeEduProgramSubjectID() {
        return new EduProgramHigherProfType.EduProgramSubjectID();
    }

    /**
     * Create an instance of {@link EduLevelType.ParentEduLevelID }
     *
     */
    public EduLevelType.ParentEduLevelID createEduLevelTypeParentEduLevelID() {
        return new EduLevelType.ParentEduLevelID();
    }

    /**
     * Create an instance of {@link AddressTypeType.AddressLevelID }
     *
     */
    public AddressTypeType.AddressLevelID createAddressTypeTypeAddressLevelID() {
        return new AddressTypeType.AddressLevelID();
    }

    /**
     * Create an instance of {@link AddressType.OksmID }
     *
     */
    public AddressType.OksmID createAddressTypeOksmID() {
        return new AddressType.OksmID();
    }

    /**
     * Create an instance of {@link AddressType.ParentAddressID }
     *
     */
    public AddressType.ParentAddressID createAddressTypeParentAddressID() {
        return new AddressType.ParentAddressID();
    }

    /**
     * Create an instance of {@link AddressType.AddressTypeID }
     *
     */
    public AddressType.AddressTypeID createAddressTypeAddressTypeID() {
        return new AddressType.AddressTypeID();
    }

    /**
     * Create an instance of {@link EduCtrEducationPromiseType }
     *
     */
    public EduCtrEducationPromiseType createEduCtrEducationPromiseType() {
        return new EduCtrEducationPromiseType();
    }

    /**
     * Create an instance of {@link EduCtrEducationPromiseType.ContractVersionContractorID }
     *
     */
    public EduCtrEducationPromiseType.ContractVersionContractorID createEduCtrEducationPromiseTypeContractVersionContractorID() {
        return new EduCtrEducationPromiseType.ContractVersionContractorID();
    }

    /**
     * Create an instance of {@link EducationYearType }
     *
     */
    public EducationYearType createEducationYearType() {
        return new EducationYearType();
    }
}
