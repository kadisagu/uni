/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EduProgramTraitType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduProgramTraitTypeReactor extends BaseEntityReactor<EduProgramTrait, EduProgramTraitType>
{
    @Override
    public Class<EduProgramTrait> getEntityClass()
    {
        return EduProgramTrait.class;
    }

    @Override
    public EduProgramTraitType createDatagramObject(String guid)
    {
        EduProgramTraitType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramTraitType();
        if (null != guid)
            datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramTrait entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramTraitType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramTraitType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramTraitID(entity.getCode());
        datagramObject.setEduProgramTraitName(entity.getTitle());
        datagramObject.setEduProgramTraitNameShort(entity.getShortTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EduProgramTrait findEduProgramTrait(String code, String name)
    {
        if (code != null)
        {
            List<EduProgramTrait> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramTrait.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (name != null)
        {
            List<EduProgramTrait> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramTrait.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduProgramTrait> processDatagramObject(EduProgramTraitType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // EduProgramTrait init
        String code = StringUtils.trimToNull(datagramObject.getEduProgramTraitID());
        String name = StringUtils.trimToNull(datagramObject.getEduProgramTraitName());
        String shortName = StringUtils.trimToNull(datagramObject.getEduProgramTraitNameShort());

        boolean isNew = false;
        boolean isChanged = false;
        EduProgramTrait entity = null;

        CoreCollectionUtils.Pair<EduProgramTrait, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduProgramTrait(code, name);
        if (entity == null)
        {
            entity = new EduProgramTrait();
            isNew = true;
            isChanged = true;
        }

        // EduProgramTrait set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            entity.setDefaultTitle(name);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EduProgramTrait.class, entity, EduProgramTrait.P_TITLE);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EduProgramTraitType EduProgramTraitName is not unique");
            }
        }
        if (null == entity.getDefaultTitle())
        {
            throw new ProcessingDatagramObjectException("EduProgramTraitType object without EduProgramTraitName!");
        }
        if (shortName != null && !shortName.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortName);
        }
        if (null == entity.getShortTitle())
        {
            throw new ProcessingDatagramObjectException("EduProgramTraitType object without EduProgramTraitShortName!");
        }


        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduProgramTrait.class, entity, EduProgramTrait.P_CODE))
            {
                if (code == null)
                    warning.append("[EduProgramTraitType object without EduProgramTraitTypeID!");
                else
                    warning.append("EduProgramTraitTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramTrait.class);
                warning.append(" EduProgramTraitTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduProgramTraitTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EduProgramTrait.P_DEFAULT_TITLE);
        return  fields;
    }
}
