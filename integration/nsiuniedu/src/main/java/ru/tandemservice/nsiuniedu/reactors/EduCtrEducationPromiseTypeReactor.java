/* $Id:$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.ContractVersionContractorType;
import ru.tandemservice.nsiclient.datagram.EduCtrEducationPromiseType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 25.09.2015
 */
public class EduCtrEducationPromiseTypeReactor extends BaseEntityReactor<EduCtrEducationPromise, EduCtrEducationPromiseType> {
    private static final String SOURCE = "ContractVersionContractorID";
    private static final String DESTINATION = "DestinationID";

    @Override
    public Class<EduCtrEducationPromise> getEntityClass() {
        return EduCtrEducationPromise.class;
    }

    @Override
    public void collectUnknownDatagrams(EduCtrEducationPromiseType datagramObject, IUnknownDatagramsCollector collector) {
        EduCtrEducationPromiseType.ContractVersionContractorID sourceID = datagramObject.getContractVersionContractorID();
        ContractVersionContractorType contractorType = sourceID == null ? null : sourceID.getContractVersionContractor();
        if (contractorType != null) collector.check(contractorType.getID(), ContractVersionContractorType.class);

        EduCtrEducationPromiseType.DestinationID destinationID = datagramObject.getDestinationID();
        contractorType = destinationID == null ? null : destinationID.getContractVersionContractor();
        if (contractorType != null) collector.check(contractorType.getID(), ContractVersionContractorType.class);
    }

    @Override
    public EduCtrEducationPromiseType createDatagramObject(String guid) {
        EduCtrEducationPromiseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrEducationPromiseType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduCtrEducationPromise entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        EduCtrEducationPromiseType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrEducationPromiseType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduCtrEducationPromiseProgramName(entity.getEduProgram().getTitle());

        StringBuilder warning = new StringBuilder();
        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(9);
        result.addWithoutConditions(datagramObject);

        ProcessedDatagramObject<ContractVersionContractorType> contractorType = createDatagramObject(ContractVersionContractorType.class, entity.getSrc(), commonCache, warning);
        EduCtrEducationPromiseType.ContractVersionContractorID contractorID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduCtrEducationPromiseTypeContractVersionContractorID();
        contractorID.setContractVersionContractor(contractorType.getObject());
        datagramObject.setContractVersionContractorID(contractorID);
        if (contractorType.getList() != null)
            result.addAll(contractorType.getList());

        contractorType = createDatagramObject(ContractVersionContractorType.class, entity.getDst(), commonCache, warning);
        EduCtrEducationPromiseType.DestinationID destinationID = new EduCtrEducationPromiseType.DestinationID();
        destinationID.setContractVersionContractor(contractorType.getObject());
        datagramObject.setDestinationID(destinationID);
        if (contractorType.getList() != null)
            result.addAll(contractorType.getList());
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EduProgramProf findEduProgramProf(String title) {
        if (title != null)
        {
            List<EduProgramProf> list = findEntity(EduProgramProf.class, eq(property(ENTITY_ALIAS, EduProgramProf.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    protected EduCtrEducationPromise findEduCtrEducationPromise(String title, CtrContractVersionContractor source, CtrContractVersionContractor destination) {
        List<IExpression> expressions = new ArrayList<>();
        if (title != null)
            expressions.add(eq(property(ENTITY_ALIAS, EduCtrEducationPromise.eduProgram().title()), value(title)));
        if (source != null)
            expressions.add(eq(property(ENTITY_ALIAS, EduCtrEducationPromise.src().id()), value(source.getId())));
        if (destination != null)
            expressions.add(eq(property(ENTITY_ALIAS, EduCtrEducationPromise.dst().id()), value(destination.getId())));

        if(!expressions.isEmpty())
        {
            List<EduCtrEducationPromise> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduCtrEducationPromise> processDatagramObject(EduCtrEducationPromiseType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        String programName = datagramObject.getEduCtrEducationPromiseProgramName();
        EduCtrEducationPromiseType.ContractVersionContractorID sourceID = datagramObject.getContractVersionContractorID();
        ContractVersionContractorType contractorType = sourceID == null ? null : sourceID.getContractVersionContractor();
        CtrContractVersionContractor source = getOrCreate(ContractVersionContractorType.class, params, commonCache, contractorType, SOURCE, null, warning);

        EduCtrEducationPromiseType.DestinationID destinationID = datagramObject.getDestinationID();
        contractorType = destinationID == null ? null : destinationID.getContractVersionContractor();
        CtrContractVersionContractor destination = getOrCreate(ContractVersionContractorType.class, params, commonCache, contractorType, DESTINATION, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EduCtrEducationPromise entity = null;

        CoreCollectionUtils.Pair<EduCtrEducationPromise, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduCtrEducationPromise(programName, source, destination);
        if (entity == null)
        {
            entity = new EduCtrEducationPromise();
            isNew = true;
            isChanged = true;
            entity.setDeadlineDate(new Date());
            if (programName == null)
                throw new ProcessingDatagramObjectException("EduCtrEducationPromiseType object without EduCtrEducationPromiseProgramName!");
            EduProgramProf programProf = findEduProgramProf(programName);
            if (programProf == null)
                throw new ProcessingDatagramObjectException("EduProgramProf with title '" + programName + "' not found!");
            entity.setEduProgram(programProf);
        }

        if (programName != null && !programName.equals(entity.getEduProgram().getTitle()))
        {
            EduProgramProf programProf = findEduProgramProf(programName);
            if (programProf == null)
                warning.append("[EduProgramProf with title '").append(programName).append("' not found]");
            else {
                isChanged = true;
                entity.setEduProgram(programProf);
            }
        }

        if (source != null && !source.equals(entity.getSrc()))
        {
            isChanged = true;
            entity.setSrc(source);
        }
        if (entity.getSrc() == null)
            throw new ProcessingDatagramObjectException("EduCtrEducationPromiseType object without ContractVersionContractorID!");

        if (destination != null && !destination.equals(entity.getDst()))
        {
            isChanged = true;
            entity.setDst(destination);
        }
        if (entity.getDst() == null)
            throw new ProcessingDatagramObjectException("EduCtrEducationPromiseType object without DestinationID!");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EduCtrEducationPromise> w, EduCtrEducationPromiseType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(ContractVersionContractorType.class, session, params, SOURCE, nsiPackage);
            saveChildEntity(ContractVersionContractorType.class, session, params, DESTINATION, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EduCtrEducationPromise.P_COMMENT);
        fields.add(EduCtrEducationPromise.P_DEADLINE_DATE);
        return  fields;
    }
}
