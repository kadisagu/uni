/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduProgramHigherProfTypeReactor extends BaseEntityReactor<EduProgramHigherProf, EduProgramHigherProfType>
{
    private static final String INSTITUTION_ORGUNIT = "institutionOrgUnitID";
    private static final String OWNER_ORGUNIT = "ownerOrgUnitID";
    private static final String PROGRAM_FORM = "programFormID";
    private static final String PROGRAM_DURATION = "programDurationID";
    private static final String PROGRAM_TRAIT = "programTraitID";
    private static final String EDU_LEVEL = "eduLevelID";
    private static final String PROGRAM_SUBJECT = "programSubjectID";
    private static final String PROGRAM_QUALIFICATION = "programQualificationID";
    private static final String SPECIALIZATION = "specializationID";

    @Override
    public Class<EduProgramHigherProf> getEntityClass()
    {
        return EduProgramHigherProf.class;
    }

    @Override
    public void collectUnknownDatagrams(EduProgramHigherProfType datagramObject, IUnknownDatagramsCollector collector)
    {
        EduProgramHigherProfType.EduProgramQualificationID programQualificationID = datagramObject.getEduProgramQualificationID();
        EduProgramQualificationType programQualificationType = programQualificationID == null ? null : programQualificationID.getEduProgramQualification();
        if (programQualificationType != null)
            collector.check(programQualificationType.getID(), EduProgramQualificationType.class);

        EduProgramHigherProfType.EduSpecializationBaseID specializationBaseID = datagramObject.getEduSpecializationBaseID();
        EduSpecializationBaseType specializationBaseType = specializationBaseID == null ? null : specializationBaseID.getEduSpecializationBase();
        if (specializationBaseType != null)
            collector.check(specializationBaseType.getID(), EduSpecializationBaseType.class);

        EduProgramHigherProfType.OwnerOrgUnitID ownerOrgUnitID = datagramObject.getOwnerOrgUnitID();
        DepartmentType ownerOrgUnitType = ownerOrgUnitID == null ? null : ownerOrgUnitID.getDepartment();
        if (ownerOrgUnitType != null)
            collector.check(ownerOrgUnitType.getID(), DepartmentType.class);

        EduProgramHigherProfType.EduProgramTraitID traitID = datagramObject.getEduProgramTraitID();
        EduProgramTraitType traitType = traitID == null ? null : traitID.getEduProgramTrait();
        if (traitType != null)
            collector.check(traitType.getID(), EduProgramTraitType.class);

        EduProgramHigherProfType.EduProgramSubjectID subjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType subjectType = subjectID == null ? null : subjectID.getEduProgramSubject();
        if (subjectType != null)
            collector.check(subjectType.getID(), EduProgramSubjectType.class);

        EduProgramHigherProfType.EduProgramFormID formID = datagramObject.getEduProgramFormID();
        EduProgramFormType formType = formID == null ? null : formID.getEduProgramForm();
        if (formType != null)
            collector.check(formType.getID(), EduProgramFormType.class);

        EduProgramHigherProfType.EduProgramDurationID durationID = datagramObject.getEduProgramDurationID();
        EduProgramDurationType durationType = durationID == null ? null : durationID.getEduProgramDuration();
        if (durationType != null)
            collector.check(durationType.getID(), EduProgramDurationType.class);

        EduProgramHigherProfType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if (departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);

        EduProgramHigherProfType.EduLevelID levelID = datagramObject.getEduLevelID();
        EduLevelType levelType = levelID == null ? null : levelID.getEduLevel();
        if (levelType != null)
            collector.check(levelType.getID(), EduLevelType.class);
    }

    @Override
    public EduProgramHigherProfType createDatagramObject(String guid)
    {
        EduProgramHigherProfType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramHigherProf entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramHigherProfType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfType();
        StringBuilder warning = new StringBuilder();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(9);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramHigherProfKind(entity.getKind().getTitle());
        datagramObject.setEduProgramHigherProfName(entity.getTitle());
        datagramObject.setEduProgramHigherProfShortName(entity.getShortTitle());
        datagramObject.setEduProgramHigherProfYear(String.valueOf(entity.getYear().getIntValue()));
        datagramObject.setEduProgramHigherOrientation(entity.getProgramOrientation() != null ? entity.getProgramOrientation().getCode() : null);

        ProcessedDatagramObject<EduProgramQualificationType> programQualificationType = createDatagramObject(EduProgramQualificationType.class, entity.getProgramQualification(), commonCache, warning);
        EduProgramHigherProfType.EduProgramQualificationID programQualificationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduProgramQualificationID();
        programQualificationID.setEduProgramQualification(programQualificationType.getObject());
        datagramObject.setEduProgramQualificationID(programQualificationID);
        if (programQualificationType.getList() != null)
            result.addAll(programQualificationType.getList());

        ProcessedDatagramObject<EduSpecializationBaseType> specializationBaseType = createDatagramObject(EduSpecializationBaseType.class, entity.getProgramSpecialization(), commonCache, warning);
        EduProgramHigherProfType.EduSpecializationBaseID specializationBaseID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduSpecializationBaseID();
        specializationBaseID.setEduSpecializationBase(specializationBaseType.getObject());
        datagramObject.setEduSpecializationBaseID(specializationBaseID);
        if (specializationBaseType.getList() != null)
            result.addAll(specializationBaseType.getList());

        ProcessedDatagramObject<DepartmentType> departmentType = createDatagramObject(DepartmentType.class, entity.getInstitutionOrgUnit().getOrgUnit(), commonCache, warning);
        EduProgramHigherProfType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeDepartmentID();
        departmentID.setDepartment(departmentType.getObject());
        datagramObject.setDepartmentID(departmentID);
        if (departmentType.getList() != null)
            result.addAll(departmentType.getList());

        ProcessedDatagramObject<DepartmentType> ownerDepartmentType = createDatagramObject(DepartmentType.class, entity.getOwnerOrgUnit().getOrgUnit(), commonCache, warning);
        EduProgramHigherProfType.OwnerOrgUnitID ownerOrgUnitID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeOwnerOrgUnitID();
        ownerOrgUnitID.setDepartment(ownerDepartmentType.getObject());
        datagramObject.setOwnerOrgUnitID(ownerOrgUnitID);
        if (ownerDepartmentType.getList() != null)
            result.addAll(ownerDepartmentType.getList());

        ProcessedDatagramObject<EduProgramDurationType> durationType = createDatagramObject(EduProgramDurationType.class, entity.getDuration(), commonCache, warning);
        EduProgramHigherProfType.EduProgramDurationID durationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduProgramDurationID();
        durationID.setEduProgramDuration(durationType.getObject());
        datagramObject.setEduProgramDurationID(durationID);
        if (durationType.getList() != null)
            result.addAll(durationType.getList());

        ProcessedDatagramObject<EduProgramFormType> programFormType = createDatagramObject(EduProgramFormType.class, entity.getForm(), commonCache, warning);
        EduProgramHigherProfType.EduProgramFormID formID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduProgramFormID();
        formID.setEduProgramForm(programFormType.getObject());
        datagramObject.setEduProgramFormID(formID);
        if (programFormType.getList() != null)
            result.addAll(programFormType.getList());

        ProcessedDatagramObject<EduProgramSubjectType> subjectType = createDatagramObject(EduProgramSubjectType.class, entity.getProgramSubject(), commonCache, warning);
        EduProgramHigherProfType.EduProgramSubjectID subjectID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduProgramSubjectID();
        subjectID.setEduProgramSubject(subjectType.getObject());
        datagramObject.setEduProgramSubjectID(subjectID);
        if (subjectType.getList() != null)
            result.addAll(subjectType.getList());

        if (entity.getEduProgramTrait() != null)
        {
            ProcessedDatagramObject<EduProgramTraitType> traitType = createDatagramObject(EduProgramTraitType.class, entity.getEduProgramTrait(), commonCache, warning);
            EduProgramHigherProfType.EduProgramTraitID traitID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduProgramTraitID();
            traitID.setEduProgramTrait(traitType.getObject());
            datagramObject.setEduProgramTraitID(traitID);
            if (traitType.getList() != null)
                result.addAll(traitType.getList());
        }

        ProcessedDatagramObject<EduLevelType> levelType = createDatagramObject(EduLevelType.class, entity.getBaseLevel(), commonCache, warning);
        EduProgramHigherProfType.EduLevelID levelID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramHigherProfTypeEduLevelID();
        levelID.setEduLevel(levelType.getObject());
        datagramObject.setEduLevelID(levelID);
        if (levelType.getList() != null)
            result.addAll(levelType.getList());

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    protected EduProgramHigherProf findEduProgramHigherProf(String uniqueKey, EducationYear educationYear)
    {
        if (uniqueKey != null && educationYear != null)
        {
            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, EduProgramHigherProf.programUniqueKey()), value(uniqueKey)));
            expressions.add(eq(property(ENTITY_ALIAS, EduProgramHigherProf.year()), value(educationYear)));
            List<EduProgramHigherProf> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduProgramHigherProf> processDatagramObject(EduProgramHigherProfType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EduProgramHigherProf init
        EduProgramHigherProfType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit instOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, departmentType, INSTITUTION_ORGUNIT, null, warning);
        EduInstitutionOrgUnit institutionOrgUnit = getInstitutionOrgUnit(instOrgUnit);

        EduProgramHigherProfType.OwnerOrgUnitID ownerOrgUnitID = datagramObject.getOwnerOrgUnitID();
        DepartmentType ownerType = ownerOrgUnitID == null ? null : ownerOrgUnitID.getDepartment();
        OrgUnit ownOrgUnit = getOrCreate(DepartmentType.class, params, commonCache, ownerType, OWNER_ORGUNIT, null, warning);
        EduOwnerOrgUnit eduOwnerOrgUnit = getEduOwnerOrgUnit(ownOrgUnit);

        EduProgramHigherProfType.EduProgramDurationID durationID = datagramObject.getEduProgramDurationID();
        EduProgramDurationType durationType = durationID == null ? null : durationID.getEduProgramDuration();
        EduProgramDuration duration = getOrCreate(EduProgramDurationType.class, params, commonCache, durationType, PROGRAM_DURATION, null, warning);

        EduProgramHigherProfType.EduProgramFormID formID = datagramObject.getEduProgramFormID();
        EduProgramFormType formType = formID == null ? null : formID.getEduProgramForm();
        EduProgramForm programForm = getOrCreate(EduProgramFormType.class, params, commonCache, formType, PROGRAM_FORM, null, warning);

        EduProgramHigherProfType.EduProgramTraitID traitID = datagramObject.getEduProgramTraitID();
        EduProgramTraitType traitType = traitID == null ? null : traitID.getEduProgramTrait();
        EduProgramTrait programTrait = getOrCreate(EduProgramTraitType.class, params, commonCache, traitType, PROGRAM_TRAIT, null, warning);

        EduProgramHigherProfType.EduProgramSubjectID subjectID = datagramObject.getEduProgramSubjectID();
        EduProgramSubjectType subjectType = subjectID == null ? null : subjectID.getEduProgramSubject();
        EduProgramSubject programSubject = getOrCreate(EduProgramSubjectType.class, params, commonCache, subjectType, PROGRAM_SUBJECT, null, warning);

        EduProgramHigherProfType.EduLevelID levelID = datagramObject.getEduLevelID();
        EduLevelType levelType = levelID == null ? null : levelID.getEduLevel();
        EduLevel eduLevel = getOrCreate(EduLevelType.class, params, commonCache, levelType, EDU_LEVEL, null, warning);

        EduProgramHigherProfType.EduProgramQualificationID qualificationID = datagramObject.getEduProgramQualificationID();
        EduProgramQualificationType qualificationType = qualificationID == null ? null : qualificationID.getEduProgramQualification();
        EduProgramQualification programQualification = getOrCreate(EduProgramQualificationType.class, params, commonCache, qualificationType, PROGRAM_QUALIFICATION, null, warning);

        EduProgramHigherProfType.EduSpecializationBaseID specializationBaseID = datagramObject.getEduSpecializationBaseID();
        EduSpecializationBaseType specializationBaseType = specializationBaseID == null ? null : specializationBaseID.getEduSpecializationBase();
        EduProgramSpecialization programSpecialization = getOrCreate(EduSpecializationBaseType.class, params, commonCache, specializationBaseType, SPECIALIZATION, null, warning);

        String name = StringUtils.trimToNull(datagramObject.getEduProgramHigherProfName());
        String nameShot = StringUtils.trimToNull(datagramObject.getEduProgramHigherProfShortName());
        EducationYear educationYear = getEducationYear(NsiUtils.parseInteger(datagramObject.getEduProgramHigherProfYear(), "EduProgramHigherProfYear"));
        EduProgramKind programKind = getEduProgramKind(StringUtils.trimToNull(datagramObject.getEduProgramHigherProfKind()));
        EduProgramOrientation orientation = getEduProgramOrientation(StringUtils.trimToNull(datagramObject.getEduProgramHigheOrientation()));

        boolean isNew = false;
        boolean isChanged = false;
        EduProgramHigherProf entity = null;

        CoreCollectionUtils.Pair<EduProgramHigherProf, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
        {

            //создаем новую сущность с целью вычисления uniqueKey
            entity = new EduProgramHigherProf();
            entity.setInstitutionOrgUnit(institutionOrgUnit);
            entity.setKind(programKind);
            entity.setProgramSpecialization(programSpecialization);
            entity.setProgramSubject(programSubject);
            entity.setBaseLevel(eduLevel);
            entity.setDuration(duration);
            entity.setEduProgramTrait(programTrait);
            entity.setForm(programForm);
            entity.setOwnerOrgUnit(eduOwnerOrgUnit);
            entity.setProgramQualification(programQualification);
            entity.setYear(educationYear);
            entity.setTitle(name);
            entity.setShortTitle(nameShot);
            String programUniqueKey = entity.generateUniqueKey();

            entity = findEduProgramHigherProf(programUniqueKey, educationYear);
        }
        if (entity == null)
        {
            entity = new EduProgramHigherProf();
            isNew = true;
            isChanged = true;
        }

        // EduProgramHigherProf set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }

        if (nameShot != null && !nameShot.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(nameShot);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramHigherProfShortName");
        }

        if (educationYear != null && !educationYear.equals(entity.getYear()))
        {
            isChanged = true;
            entity.setYear(educationYear);
        }
        if (entity.getYear() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramHigherProfYear");
        }

        if (programKind != null && !programKind.equals(entity.getKind()))
        {
            isChanged = true;
            entity.setKind(programKind);
        }
        if (entity.getKind() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramHigherProfKind");
        }

        if (institutionOrgUnit != null && !institutionOrgUnit.equals(entity.getInstitutionOrgUnit()))
        {
            isChanged = true;
            entity.setInstitutionOrgUnit(institutionOrgUnit);
        }
        if (entity.getInstitutionOrgUnit() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without DepartmentID");
        }

        if (eduOwnerOrgUnit != null && !eduOwnerOrgUnit.equals(entity.getOwnerOrgUnit()))
        {
            isChanged = true;
            entity.setOwnerOrgUnit(eduOwnerOrgUnit);
        }
        if (entity.getInstitutionOrgUnit() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without OwnerOrgUnitID");
        }

        if (duration != null && !duration.equals(entity.getDuration()))
        {
            isChanged = true;
            entity.setDuration(duration);
        }
        if (entity.getDuration() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramDurationID");
        }

        if (programForm != null && !programForm.equals(entity.getForm()))
        {
            isChanged = true;
            entity.setForm(programForm);
        }
        if (entity.getForm() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramFormID");
        }

        if (programTrait != null && !programTrait.equals(entity.getEduProgramTrait()))
        {
            isChanged = true;
            entity.setEduProgramTrait(programTrait);
        }

        if (programSubject != null && !programSubject.equals(entity.getProgramSubject()))
        {
            isChanged = true;
            entity.setProgramSubject(programSubject);
        }
        if (entity.getProgramSubject() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramSubjectID");
        }

        if (eduLevel != null && !eduLevel.equals(entity.getBaseLevel()))
        {
            isChanged = true;
            entity.setBaseLevel(eduLevel);
        }
        if (entity.getBaseLevel() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduLevelID");
        }

        if (programQualification != null && !programQualification.equals(entity.getProgramQualification()))
        {
            isChanged = true;
            entity.setProgramQualification(programQualification);
        }
        if (entity.getProgramQualification() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduProgramQualificationID");
        }

        if (programSpecialization != null && !programSpecialization.equals(entity.getProgramSpecialization()))
        {
            isChanged = true;
            entity.setProgramSpecialization(programSpecialization);
        }
        if (entity.getProgramSpecialization() == null)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType object without EduSpecializationBaseID");
        }

        if (orientation != null && !orientation.equals(entity.getProgramOrientation()))
        {
            entity.setProgramOrientation(orientation);
        }

        entity.setProgramUniqueKey(entity.generateUniqueKey());
        if (entity.getProgramUniqueKey() != null && entity.getProgramUniqueKey().length() > 255)
        {
            throw new ProcessingDatagramObjectException("EduProgramHigherProfType's uniqueKey combination exceeds the permission length (255 symbols)");
        }
        if (entity.getProgramUniqueKey() != null && entity.getYear() != null)
        {
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EduProgramHigherProf.class, entity, EduProgramHigherProf.L_YEAR, EduProgramHigherProf.P_PROGRAM_UNIQUE_KEY);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EduProgramHigherProfType name/uniqueKey combination is not unique");
            }
        }
        if (entity.getProgramSubject() != null && entity.getKind() != null)
        {
            if (!entity.getProgramSubject().getEduProgramKind().equals(entity.getKind()))
            {
                throw new ProcessingDatagramObjectException("EduProgramHigherProfType required condition: programSubject.subjectIndex.programKind = kind");
            }
        }

        if (entity.getKind() != null)
        {
            if (!entity.getKind().isProgramHigherProf())
            {
                throw new ProcessingDatagramObjectException("EduProgramHigherProfType required condition: programKind is high level");
            }
        }

        if (entity.getProgramSubject() != null && entity.getProgramQualification() != null)
        {
            if (!entity.getProgramSubject().getSubjectIndex().equals(entity.getProgramQualification().getSubjectIndex()))
            {
                throw new ProcessingDatagramObjectException("EduProgramHigherProfType required condition: programSubject.subjectIndex = programQualification.subjectIndex");
            }
        }

        if (entity.getProgramSubject() != null && entity.getProgramSpecialization() != null)
        {
            if (!entity.getProgramSubject().equals(entity.getProgramSpecialization().getProgramSubject()))
            {
                throw new ProcessingDatagramObjectException("EduProgramHigherProfType required condition: programSubject = programSpecialization.programSubject");
            }
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EduProgramHigherProf> w, EduProgramHigherProfType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EduProgramFormType.class, session, params, PROGRAM_FORM, nsiPackage);
            saveChildEntity(EduProgramDurationType.class, session, params, PROGRAM_DURATION, nsiPackage);
            saveChildEntity(EduProgramTraitType.class, session, params, PROGRAM_TRAIT, nsiPackage);
            saveChildEntity(EduSpecializationBaseType.class, session, params, SPECIALIZATION, nsiPackage);
            saveChildEntity(EduProgramQualificationType.class, session, params, PROGRAM_QUALIFICATION, nsiPackage);
            saveChildEntity(EduLevelType.class, session, params, EDU_LEVEL, nsiPackage);
            saveChildEntity(EduProgramSubjectType.class, session, params, PROGRAM_SUBJECT, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, INSTITUTION_ORGUNIT, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, OWNER_ORGUNIT, nsiPackage);
        }
        EduProgramHigherProfManager.instance().dao().saveOrUpdateEduProgramHigherProf(w.getResult(), w.getResult().getProgramSpecialization());

        super.save(session, w, datagramObject, nsiPackage);
    }

    private static EduInstitutionOrgUnit getInstitutionOrgUnit(OrgUnit orgUnit)
    {
        if (orgUnit != null)
        {
            return DataAccessServices.dao().get(EduInstitutionOrgUnit.class, EduInstitutionOrgUnit.orgUnit(), orgUnit);
        }
        return null;
    }

    private static EduOwnerOrgUnit getEduOwnerOrgUnit(OrgUnit orgUnit)
    {
        if (orgUnit != null)
        {
            return DataAccessServices.dao().get(EduOwnerOrgUnit.class, EduOwnerOrgUnit.orgUnit(), orgUnit);
        }
        return null;
    }

    private static EducationYear getEducationYear(Integer yearInt)
    {
        if (yearInt != null)
        {
            return DataAccessServices.dao().get(EducationYear.class, EducationYear.intValue(), yearInt);
        }
        return null;
    }

    private static EduProgramKind getEduProgramKind(String title)
    {
        if (title != null)
        {
            return DataAccessServices.dao().get(EduProgramKind.class, EduProgramKind.title(), title);
        }
        return null;
    }

    private static EduProgramOrientation getEduProgramOrientation(String code)
    {
        if (code != null)
        {
            return DataAccessServices.dao().get(EduProgramOrientation.class, EduProgramOrientation.code(), code);
        }
        return null;
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EduProgramHigherProf.P_PROGRAM_UNIQUE_KEY);

        return  fields;
    }
}
