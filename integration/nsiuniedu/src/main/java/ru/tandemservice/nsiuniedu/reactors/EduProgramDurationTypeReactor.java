/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EduProgramDurationType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduProgramDurationTypeReactor extends BaseEntityReactor<EduProgramDuration, EduProgramDurationType>
{
    @Override
    public Class<EduProgramDuration> getEntityClass()
    {
        return EduProgramDuration.class;
    }

    @Override
    public EduProgramDurationType createDatagramObject(String guid)
    {
        EduProgramDurationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDurationType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramDuration entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramDurationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDurationType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramDurationID(entity.getCode());
        datagramObject.setEduProgramDurationName(entity.getTitle());
        datagramObject.setEduProgramDurationNumberOfMonths(String.valueOf(entity.getNumberOfMonths()));
        datagramObject.setEduProgramDurationNumberOfYears(String.valueOf(entity.getNumberOfYears()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EduProgramDuration findEduProgramDuration(String code, String name, Integer months, Integer years)
    {
        if (code != null)
        {
            List<EduProgramDuration> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramDuration.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (name != null)
        {
            List<EduProgramDuration> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramDuration.title()), value(name)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (months != null && years != null)
        {
            List<IExpression> expressions = new ArrayList<>();
            expressions.add(eq(property(ENTITY_ALIAS, EduProgramDuration.numberOfMonths()), value(months)));
            expressions.add(eq(property(ENTITY_ALIAS, EduProgramDuration.numberOfYears()), value(years)));
            List<EduProgramDuration> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduProgramDuration> processDatagramObject(EduProgramDurationType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        // EduProgramDuration init
        String code = StringUtils.trimToNull(datagramObject.getEduProgramDurationID());
        String name = StringUtils.trimToNull(datagramObject.getEduProgramDurationName());
        Integer numberOfMonths = NsiUtils.parseInteger(datagramObject.getEduProgramDurationNumberOfMonths(), "EduProgramDurationNumberOfMonths");
        Integer numberOfYears = NsiUtils.parseInteger(datagramObject.getEduProgramDurationNumberOfYears(), "EduProgramDurationNumberOfYears");

        boolean isNew = false;
        boolean isChanged = false;
        EduProgramDuration entity = null;

        CoreCollectionUtils.Pair<EduProgramDuration, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduProgramDuration(code, name, numberOfMonths, numberOfYears);
        if (entity == null)
        {
            entity = new EduProgramDuration();
            isNew = true;
            isChanged = true;
        }

        // EduProgramDuration set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
            boolean isUnique = NsiSyncManager.instance().reactorDAO().isPropertyUnique(EduProgramDuration.class, EduProgramDuration.P_TITLE, entity.getId(), name);
            if (!isUnique)
            {
                throw new ProcessingDatagramObjectException("EduProgramDurationType title is not unique");
            }
        }

        if (numberOfMonths != null && !numberOfMonths.equals(entity.getNumberOfMonths()))
        {
            isChanged = true;
            entity.setNumberOfMonths(numberOfMonths);
        }

        if (numberOfYears != null && !numberOfYears.equals(entity.getNumberOfYears()))
        {
            isChanged = true;
            entity.setNumberOfYears(numberOfYears);
        }

        if (!NsiSyncManager.instance().reactorDAO().isPropertiesUnique(EduProgramDuration.class, entity, EduProgramDuration.P_NUMBER_OF_MONTHS, EduProgramDuration.P_NUMBER_OF_YEARS))
        {
            throw new ProcessingDatagramObjectException("EduProgramDurationType numberOfMonths/numberOfYears combination is not unique: "+ entity.getNumberOfYears()+": "+entity.getNumberOfMonths());
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduProgramDuration.class, entity, EduProgramDuration.P_CODE))
            {
                if (code == null)
                    warning.append("[EduProgramDurationType object without EduProgramDurationTypeID!");
                else
                    warning.append("[EduProgramDurationTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramDuration.class);
                warning.append(" EduProgramDurationTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduProgramDurationTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}
