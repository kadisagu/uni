/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.EduProgramSubjectType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexGenerationCodes;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduProgramSubjectTypeReactor extends BaseEntityReactor<EduProgramSubject, EduProgramSubjectType>
{
    @Override
    public Class<EduProgramSubject> getEntityClass()
    {
        return EduProgramSubject.class;
    }

    @Override
    public EduProgramSubjectType createDatagramObject(String guid)
    {
        EduProgramSubjectType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramSubjectType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramSubject entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramSubjectType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramSubjectType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramSubjectID(entity.getCode());
        datagramObject.setEduProgramSubjectName(entity.getTitle());
        datagramObject.setEduProgramSubjectNameShort(entity.getShortTitle());
        datagramObject.setEduProgramSubjectSubjectIndex(entity.getSubjectIndex().getTitle());
        datagramObject.setEduProgramSubjectSubjectCode(entity.getSubjectCode());
        datagramObject.setEduProgramSubjectOutOfSyncDate(NsiUtils.formatDate(entity.getOutOfSyncDate()));
        if (entity instanceof EduProgramSubject2009)
        {
            datagramObject.setEduProgramSubjectGroup(((EduProgramSubject2009) entity).getGroup().getTitle());
        }
        else if (entity instanceof EduProgramSubject2013)
        {
            datagramObject.setEduProgramSubjectGroup(((EduProgramSubject2013) entity).getGroup().getTitle());
        }
        else if (entity instanceof EduProgramSubjectOkso)
        {
            datagramObject.setEduProgramSubjectItem(((EduProgramSubjectOkso) entity).getItem().getCode());
        }

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EduProgramSubject findEduProgramSubject(String code)
    {
        if (code != null)
        {
            List<EduProgramSubject> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramSubject.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduProgramSubject> processDatagramObject(EduProgramSubjectType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // EduProgramSubject init
        String code = StringUtils.trimToNull(datagramObject.getEduProgramSubjectID());
        String name = StringUtils.trimToNull(datagramObject.getEduProgramSubjectName());
        String nameShort = StringUtils.trimToNull(datagramObject.getEduProgramSubjectNameShort());
        String subjectIndexName = StringUtils.trimToNull(datagramObject.getEduProgramSubjectSubjectIndex());
        EduProgramSubjectIndex eduProgramSubjectIndex = findEduProgramSubjectIndex(subjectIndexName);
        String subjectCode = StringUtils.trimToNull(datagramObject.getEduProgramSubjectSubjectCode());
        Date outOfSynchDate = NsiUtils.parseDate(datagramObject.getEduProgramSubjectOutOfSyncDate(), "EduProgramSubjectOutOfSyncDate");
        String generationCode = eduProgramSubjectIndex != null ? eduProgramSubjectIndex.getGeneration().getCode() : null;
        EduProgramSubjectOksoGroup subjectOksoGroup = findEduProgramSubjectOksoGroup(datagramObject.getEduProgramSubjectGroup());
        EduProgramSubject2013Group subject2013Group = findEduProgramSubject2013Group(datagramObject.getEduProgramSubjectGroup());
        EduProgramSubjectOksoItem subjectOksoItem = findEduProgramSubjectOksoItem(datagramObject.getEduProgramSubjectItem());

        boolean isNew = false;
        boolean isChanged = false;
        EduProgramSubject entity = null;

        CoreCollectionUtils.Pair<EduProgramSubject, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduProgramSubject(code);
        if (entity == null)
        {
            if (EduProgramSubjectIndexGenerationCodes.FGOS_2009.equals(generationCode))
            {
                entity = new EduProgramSubject2009();
            }
            else if (EduProgramSubjectIndexGenerationCodes.FGOS_2013.equals(generationCode))
            {
                entity = new EduProgramSubject2013();
            }
            else if (EduProgramSubjectIndexGenerationCodes.OKSO.equals(generationCode))
            {
                entity = new EduProgramSubjectOkso();
            }
            isNew = true;
            isChanged = true;
        }

        // EduProgramSubject set
        if (null == entity)
        {
            throw new ProcessingDatagramObjectException("EduProgramSubjectType object without EduProgramSubjectSubjectIndex!");
        }

        if (eduProgramSubjectIndex != null && !eduProgramSubjectIndex.equals(entity.getSubjectIndex()))
        {
            isChanged = true;
            entity.setSubjectIndex(eduProgramSubjectIndex);
        }
        if (null == entity.getSubjectIndex())
        {
            throw new ProcessingDatagramObjectException("EduProgramSubjectType object without EduProgramSubjectSubjectIndex!");
        }
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }

        if (nameShort != null && !nameShort.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(nameShort);
        }

        if (subjectCode != null && !subjectCode.equals(entity.getSubjectCode()))
        {
            isChanged = true;
            entity.setSubjectCode(subjectCode);
        }
        if (outOfSynchDate != null && !outOfSynchDate.equals(entity.getOutOfSyncDate()))
        {
            isChanged = true;
            entity.setOutOfSyncDate(outOfSynchDate);
        }
        if (subjectOksoGroup != null && entity instanceof EduProgramSubject2009 && !subjectOksoGroup.equals(((EduProgramSubject2009) entity).getGroup()))
        {
            isChanged = true;
            ((EduProgramSubject2009) entity).setGroup(subjectOksoGroup);
        }
        if (entity instanceof EduProgramSubject2009 && null == ((EduProgramSubject2009) entity).getGroup())
        {
            throw new ProcessingDatagramObjectException("EduProgramSubjectType object without EduProgramSubjectGroup");
        }

        if (subject2013Group != null && entity instanceof EduProgramSubject2013 && !subject2013Group.equals(((EduProgramSubject2013) entity).getGroup()))
        {
            isChanged = true;
            ((EduProgramSubject2013) entity).setGroup(subject2013Group);
        }
        if (entity instanceof EduProgramSubject2013 && null == ((EduProgramSubject2013) entity).getGroup())
        {
            throw new ProcessingDatagramObjectException("EduProgramSubjectType object without EduProgramSubjectGroup");
        }

        if (subjectOksoItem != null && entity instanceof EduProgramSubjectOkso && !subjectOksoItem.equals(((EduProgramSubjectOkso) entity).getItem()))
        {
            isChanged = true;
            ((EduProgramSubjectOkso) entity).setItem(subjectOksoItem);
        }
        if (entity instanceof EduProgramSubjectOkso && null == ((EduProgramSubjectOkso) entity).getItem())
        {
            throw new ProcessingDatagramObjectException("EduProgramSubjectType object without EduProgramSubjectItem");
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduProgramSubject.class, entity, EduProgramSubject.P_CODE))
            {
                if (code == null)
                    warning.append("[EduProgramSubjectType object without EduProgramSubjectID!");
                else
                    warning.append("EduProgramSubjectID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramSubject.class);
                warning.append(" EduProgramSubjectID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduProgramSubjectID" +
                                   " is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    private static EduProgramSubjectIndex findEduProgramSubjectIndex(String name)
    {
        if (name != null && !name.isEmpty())
        {
            return DataAccessServices.dao().get(EduProgramSubjectIndex.class, EduProgramSubjectIndex.P_TITLE, name);
        }
        return null;
    }

    private static EduProgramSubject2013Group findEduProgramSubject2013Group(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (trimmedTitle != null)
        {
            return DataAccessServices.dao().get(EduProgramSubject2013Group.class, EduProgramSubject2013Group.P_TITLE, trimmedTitle);
        }
        return null;
    }

    private static EduProgramSubjectOksoGroup findEduProgramSubjectOksoGroup(String title)
    {
        String trimmedTitle = StringUtils.trimToNull(title);
        if (trimmedTitle != null)
        {
            return DataAccessServices.dao().get(EduProgramSubjectOksoGroup.class, EduProgramSubjectOksoGroup.P_TITLE, trimmedTitle);
        }
        return null;
    }

    private static EduProgramSubjectOksoItem findEduProgramSubjectOksoItem(String code)
    {
        String trimmedCode = StringUtils.trimToNull(code);
        if (trimmedCode != null)
        {
            return DataAccessServices.dao().get(EduProgramSubjectOksoItem.class, EduProgramSubjectOksoItem.P_CODE, trimmedCode);
        }
        return null;
    }
}
