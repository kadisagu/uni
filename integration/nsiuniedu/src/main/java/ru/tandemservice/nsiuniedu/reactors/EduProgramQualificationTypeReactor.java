/* $Id$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.EduProgramQualificationType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 08.09.2015
 */
public class EduProgramQualificationTypeReactor extends BaseEntityReactor<EduProgramQualification, EduProgramQualificationType>
{
    @Override
    public Class<EduProgramQualification> getEntityClass()
    {
        return EduProgramQualification.class;
    }

    @Override
    public EduProgramQualificationType createDatagramObject(String guid)
    {
        EduProgramQualificationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramQualificationType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EduProgramQualification entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramQualificationType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramQualificationType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramQualificationID(entity.getCode());
        datagramObject.setEduProgramQualificationName(entity.getTitle());
        datagramObject.setEduProgramQualificationQualificationLevelCode(entity.getQualificationLevelCode());
        datagramObject.setEduProgramQualificationSubjectIndex(entity.getSubjectIndex().getTitle());
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EduProgramQualification findEduProgramQualification(String code)
    {
        if (code != null)
        {
            List<EduProgramQualification> list = findEntity(eq(property(ENTITY_ALIAS, EduProgramQualification.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EduProgramQualification> processDatagramObject(EduProgramQualificationType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // EduProgramQualification init
        String code = StringUtils.trimToNull(datagramObject.getEduProgramQualificationID());
        String name = StringUtils.trimToNull(datagramObject.getEduProgramQualificationName());
        String subjectIndexName = StringUtils.trimToNull(datagramObject.getEduProgramQualificationSubjectIndex());
        EduProgramSubjectIndex eduProgramSubjectIndex = findEduProgramSubjectIndex(subjectIndexName);
        String qualificationLevelCode = StringUtils.trimToNull(datagramObject.getEduProgramQualificationQualificationLevelCode());


        boolean isNew = false;
        boolean isChanged = false;
        EduProgramQualification entity = null;

        CoreCollectionUtils.Pair<EduProgramQualification, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEduProgramQualification(code);
        if (entity == null)
        {
            entity = new EduProgramQualification();
            isNew = true;
            isChanged = true;
        }

        // EduProgramQualification set
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }
        if (qualificationLevelCode != null && !qualificationLevelCode.equals(entity.getQualificationLevelCode()))
        {
            isChanged = true;
            entity.setQualificationLevelCode(qualificationLevelCode);
        }
        if (eduProgramSubjectIndex != null && !eduProgramSubjectIndex.equals(entity.getSubjectIndex()))
        {
            isChanged = true;
            entity.setSubjectIndex(eduProgramSubjectIndex);
        }
        if (null == entity.getSubjectIndex())
        {
            throw new ProcessingDatagramObjectException("EduProgramQualificationType object without EduProgramQualificationSubjectIndex!");
        }


        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EduProgramQualification.class, entity, EduProgramQualification.P_CODE))
            {
                if (code == null)
                    warning.append("[EduProgramQualificationType object without EduProgramQualificationID!");
                else
                    warning.append("EduProgramQualificationID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramQualification.class);
                warning.append(" EduProgramQualificationID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EduProgramQualificationID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    private EduProgramSubjectIndex findEduProgramSubjectIndex(String name)
    {
        if (name != null && !name.isEmpty())
        {
            return DataAccessServices.dao().get(EduProgramSubjectIndex.class, EduProgramSubjectIndex.P_TITLE, name);
        }
        return null;
    }
}
