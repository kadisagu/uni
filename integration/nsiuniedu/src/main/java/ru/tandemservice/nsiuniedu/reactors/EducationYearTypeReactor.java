/* $Id:$ */
package ru.tandemservice.nsiuniedu.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.nsiclient.datagram.EducationYearType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 15.08.2016
 */
public class EducationYearTypeReactor extends BaseEntityReactor<EducationYear, EducationYearType>
{
    @Override
    public Class<EducationYear> getEntityClass()
    {
        return EducationYear.class;
    }

    @Override
    public EducationYearType createDatagramObject(String guid)
    {
        EducationYearType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationYearType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EducationYear entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EducationYearType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEducationYearType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEducationYearID(entity.getCode());
        datagramObject.setEducationYearName(entity.getTitle());
        datagramObject.setEducationYearNumber(String.valueOf(entity.getIntValue()));
        datagramObject.setEducationYearCurrent(NsiUtils.formatBoolean(entity.getCurrent()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EducationYear findEducationYear(String code, String title, Integer educationYearNumber)
    {
        if (code != null)
        {
            List<EducationYear> list = findEntity(eq(property(ENTITY_ALIAS, EducationYear.code()), value(code)));
            if (list.size() == 1) return list.get(0);
        }
        if (title != null)
        {
            List<EducationYear> list = findEntity(eq(property(ENTITY_ALIAS, EducationYear.title()), value(title)));
            if (list.size() == 1) return list.get(0);
        }
        if (educationYearNumber != null)
        {
            List<EducationYear> list = findEntity(eq(property(ENTITY_ALIAS, EducationYear.intValue()), value(educationYearNumber)));
            if (list.size() == 1) return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EducationYear> processDatagramObject(EducationYearType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getEducationYearID());
        Boolean educationYearCurrent = NsiUtils.parseBoolean(datagramObject.getEducationYearCurrent());
        Integer educationYearNumber = NsiUtils.parseInteger(datagramObject.getEducationYearNumber(), "EducationYearNumber");
        String title = StringUtils.trimToNull(datagramObject.getEducationYearName());

        boolean isNew = false;
        boolean isChanged = false;
        EducationYear entity = null;

        CoreCollectionUtils.Pair<EducationYear, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findEducationYear(code, title, educationYearNumber);
        if (entity == null)
        {
            entity = new EducationYear();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        //educationYear number
        if (educationYearNumber != null && educationYearNumber != entity.getIntValue())
        {
            isChanged = true;
            entity.setIntValue(educationYearNumber);
        }
        if (isNew && educationYearNumber == null)
        {
            throw new ProcessingDatagramObjectException("EducationYearType object without educationYearNumber");
        }

        if (educationYearCurrent != null)
        {
            isChanged = true;
            entity.setCurrent(educationYearCurrent);
        }

        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(EducationYear.class, entity, EducationYear.P_CODE))
            {
                if (code == null)
                    warning.append("[EducationYearType object without EducationYearTypeID!");
                else
                    warning.append("EducationYearTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(EducationYear.class);
                warning.append(" EducationYearTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
        } else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EducationYearTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }
}