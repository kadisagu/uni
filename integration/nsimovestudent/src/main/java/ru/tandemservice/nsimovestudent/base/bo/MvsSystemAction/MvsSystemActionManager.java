/* $Id:$ */
package ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.logic.INsiMvsDao;
import ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.logic.NsiMvsDao;

/**
 * @author Dmitry Seleznev
 * @since 16.03.2017
 */
@Configuration
public class MvsSystemActionManager extends BusinessObjectManager
{
    public static MvsSystemActionManager instance()
    {
        return instance(MvsSystemActionManager.class);
    }

    @Bean
    public INsiMvsDao dao()
    {
        return new NsiMvsDao();
    }
}