/* $Id:$ */
package ru.tandemservice.nsimovestudent.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.StudentOrderTypeType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 21.09.2016
 */
public class StudentOrderTypeTypeReactor extends BaseEntityReactor<StudentExtractType, StudentOrderTypeType>
{
    @Override
    public Class<StudentExtractType> getEntityClass()
    {
        return StudentExtractType.class;
    }

    @Override
    public StudentOrderTypeType createDatagramObject(String guid)
    {
        StudentOrderTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentOrderTypeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(StudentExtractType entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        StudentOrderTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentOrderTypeType();
        List<IDatagramObject> result = new ArrayList<>(8);
        result.add(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setStudentOrderTypeID(entity.getCode());
        datagramObject.setStudentOrderTypeName(entity.getTitle());

        return new ResultListWrapper(null, result);
    }

    private StudentExtractType findStudentExtractType(String code, String title)
    {
        if (null == code || null == title) return null;

        List<IExpression> expressions = new ArrayList<>();
        if (code != null) expressions.add(eq(property(ENTITY_ALIAS, StudentExtractType.code()), value(code)));
        if (title != null) expressions.add(eq(property(ENTITY_ALIAS, StudentExtractType.title()), value(title)));

        if (expressions.isEmpty()) return null;
        List<StudentExtractType> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);

        return null;
    }

    @Override
    public ChangedWrapper<StudentExtractType> processDatagramObject(StudentOrderTypeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();

        String code = StringUtils.trimToNull(datagramObject.getStudentOrderTypeID());
        String title = StringUtils.trimToNull(datagramObject.getStudentOrderTypeName());

        boolean isNew = false;
        boolean isChanged = false;
        StudentExtractType entity = null;
        CoreCollectionUtils.Pair<StudentExtractType, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null) entity = pair.getX();
        if (entity == null) entity = findStudentExtractType(code, title);
        if (entity == null)
        {
            entity = new StudentExtractType();
            isNew = true;
            isChanged = true;
        }

        if (code != null && !code.equals(entity.getCode()))
        {
            isChanged = true;
            entity.setCode(code);
        }

        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, null);
    }
}