/* $Id:$ */
package ru.tandemservice.nsimovestudent.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.StudentOrderType;
import ru.tandemservice.nsiclient.datagram.StudentOrderTypeType;
import ru.tandemservice.nsiclient.datagram.StudentType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessedDatagramObject;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 21.09.2016
 */
public class StudentOrderTypeReactor extends BaseEntityReactor<AbstractStudentOrder, StudentOrderType>
{
    private static final String STUDENT_ORDER_TYPE = "StudentOrderTypeID";
    private static final String STUDENT = "StudentID";
    private static final String EXTRACT_PARAM = "extract";
    private static final String PARAGRAPH_PARAM = "paragraph";

    @Override
    public Class<AbstractStudentOrder> getEntityClass()
    {
        return AbstractStudentOrder.class;
    }

    @Override
    public void collectUnknownDatagrams(StudentOrderType datagramObject, IUnknownDatagramsCollector collector)
    {
        StudentOrderType.StudentOrderTypeID orderTypeID = datagramObject.getStudentOrderTypeID();
        StudentOrderTypeType orderTypeType = orderTypeID == null ? null : orderTypeID.getStudentOrderType();
        if (orderTypeType != null) collector.check(orderTypeType.getID(), StudentOrderTypeType.class);

        StudentOrderType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        if (studentType != null) collector.check(studentType.getID(), StudentType.class);
    }

    @Override
    public StudentOrderType createDatagramObject(String guid)
    {
        StudentOrderType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentOrderType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(AbstractStudentOrder entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        StringBuilder warning = new StringBuilder();

        StudentOrderType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentOrderType();
        List<IDatagramObject> result = new ArrayList<>(8);
        result.add(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setStudentOrderNumber(entity.getNumber());
        datagramObject.setStudentOrderRegDate(NsiUtils.formatDate(entity.getCommitDate()));

        AbstractStudentExtract firstExtract = entity.getExtractCount() != 1 ? null :
                MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(entity.getId());

        StudentExtractType extractType = null != entity.getType() ? (StudentExtractType) entity.getType() : null;

        if (null == firstExtract)
        {
            datagramObject.setStudentOrderComment(entity.getTitle());
        } else
        {
            if (null == extractType) extractType = firstExtract.getType();
            datagramObject.setStudentOrderComment(firstExtract.getGeneratedComment(false));
        }

        if (null != extractType)
        {
            ProcessedDatagramObject<StudentOrderTypeType> orderType = createDatagramObject(StudentOrderTypeType.class, extractType, commonCache, warning);
            StudentOrderType.StudentOrderTypeID typeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentOrderTypeStudentOrderTypeID();
            typeID.setStudentOrderType(orderType.getObject());
            datagramObject.setStudentOrderTypeID(typeID);
            if (orderType.getList() != null) result.addAll(orderType.getList());
        }

        Student student = null != firstExtract ? firstExtract.getEntity() : null;
        if (student != null)
        {
            ProcessedDatagramObject<StudentType> studentType = createDatagramObject(StudentType.class, student, commonCache, warning);
            StudentOrderType.StudentID studentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createStudentOrderTypeStudentID();
            studentID.setStudent(studentType.getObject());
            datagramObject.setStudentID(studentID);
            if (studentType.getList() != null) result.addAll(studentType.getList());
        }

        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    private AbstractStudentOrder findAbstractStudentOrder(String orderNumber, Date orderDate)
    {
        if (null == orderNumber || null == orderDate) return null;

        List<IExpression> expressions = new ArrayList<>();
        Date dateFrom = CoreDateUtils.getDayFirstTimeMoment(orderDate);
        Date dateTo = CoreDateUtils.getNextDayFirstTimeMoment(orderDate, 1);
        expressions.add(eq(property(ENTITY_ALIAS, AbstractStudentOrder.number()), value(orderNumber)));
        expressions.add(ge(property(ENTITY_ALIAS, AbstractStudentOrder.commitDate()), value(dateFrom, PropertyType.DATE)));
        expressions.add(lt(property(ENTITY_ALIAS, AbstractStudentOrder.commitDate()), value(dateTo, PropertyType.DATE)));

        if (expressions.isEmpty()) return null;
        List<AbstractStudentOrder> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if (list.size() == 1) return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<AbstractStudentOrder> processDatagramObject(StudentOrderType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        String orderNumber = StringUtils.trimToNull(datagramObject.getStudentOrderNumber());
        Date orderDate = NsiUtils.parseDate(datagramObject.getStudentOrderRegDate(), "StudentOrderRegDate");

        StudentOrderType.StudentOrderTypeID orderTypeID = datagramObject.getStudentOrderTypeID();
        StudentOrderTypeType orderTypeType = orderTypeID == null ? null : orderTypeID.getStudentOrderType();
        StudentExtractType orderType = getOrCreate(StudentOrderTypeType.class, params, commonCache, orderTypeType, STUDENT_ORDER_TYPE, null, warning);

        StudentOrderType.StudentID studentID = datagramObject.getStudentID();
        StudentType studentType = studentID == null ? null : studentID.getStudent();
        Student student = getOrCreate(StudentType.class, params, commonCache, studentType, STUDENT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        AbstractStudentOrder entity = null;
        CoreCollectionUtils.Pair<AbstractStudentOrder, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
//        if (pair != null) entity = pair.getX();
//        if (entity == null) entity = findAbstractStudentOrder(orderNumber, orderDate);
//        if (entity == null)
//        {
            isNew = true;
            isChanged = true;
            entity = new StudentOtherOrder();
            entity.setCreateDate(new Date());
            entity.setState(ISharedBaseDao.instance.get().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
//        }

        if (isNew && null == orderType)
            throw new ProcessingDatagramObjectException("StudentOrderType object without StudentOrderTypeID!");
        if (isNew && null == student)
            throw new ProcessingDatagramObjectException("StudentOrderType object without StudentID!");

        AbstractStudentExtract extract = !(entity instanceof StudentOtherOrder) ? null :
                MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(entity.getId());

        AbstractStudentParagraph paragraph = null != extract ? extract.getParagraph() : null;

        if (entity instanceof StudentOtherOrder)
        {
            if (null == extract)
            {
                extract = new OtherStudentExtract();
                extract.setNumber(1);
                extract.setCreateDate(new Date());
                extract.setCommitted(true);
                extract.setState(ISharedBaseDao.instance.get().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));
                extract.setStudentTitle("-");
            }

            if (null == paragraph)
            {
                paragraph = new StudentOtherParagraph();
                paragraph.setNumber(1);
                paragraph.setOrder(entity);
            }
            extract.setParagraph(paragraph);
        }

//        if (orderNumber != null && (null == entity.getNumber() || !orderNumber.equals(entity.getNumber())))
//        {
//            isChanged = true;
//            entity.setNumber(orderNumber);
//        }
        if (orderNumber == null)
            orderNumber = DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate == null? entity.getCreateDate() : orderDate);
        entity.setNumber(getNextNumber(orderNumber, orderDate != null? orderDate:entity.getCreateDate()));


        if (orderDate != null && (null == entity.getCommitDate() || !orderDate.equals(entity.getCommitDate())))
        {
            isChanged = true;
            entity.setCommitDate(orderDate);
            entity.setCreateDate(orderDate);
            if (null != extract) extract.setCreateDate(orderDate);
        }

        if (orderType != null && (null == entity.getType() || !orderType.equals(entity.getType())))
        {
            if (null != extract)
            {
                isChanged = true;
                if (null == extract.getType() || !orderType.equals(extract.getType()))
                    extract.setType(orderType);
            }
        }

        if (null != student)
        {
            extract.setStudentTitle(student.getPerson().getFullFio());
            extract.setEntity(student);
            extract.setStudentTitleStr(student.getPerson().getFullFio());
            extract.setStudentStatusStr(student.getStatus().getTitle());
            extract.setPersonalNumberStr(String.valueOf(student.getPersonalNumber()));
            extract.setCourseStr(student.getCourse().getTitle());
            extract.setGroupStr(null != student.getGroup() ? student.getGroup().getTitle() : "-");
            extract.setCompensationTypeStr(student.getCompensationType().getShortTitle());
            extract.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getFullTitle());
            extract.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getFullTitle());
            extract.setEducationLevelHighSchoolStr((String) student.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
            extract.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
            extract.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
            extract.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
            extract.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
        }

        if (null != extract) params.put(EXTRACT_PARAM, extract);
        if (null != paragraph) params.put(PARAGRAPH_PARAM, paragraph);

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<AbstractStudentOrder> w, StudentOrderType datagramObject, NsiPackage nsiPackage)
    {
        try
        {
            Map<String, Object> params = w.getParams();
            if (params != null)
            {
                System.out.println("################# Childs start save");
                saveChildEntity(StudentOrderTypeType.class, session, params, STUDENT_ORDER_TYPE, nsiPackage);
                saveChildEntity(StudentType.class, session, params, STUDENT, nsiPackage);
                System.out.println("################# Childs save");
            }

            System.out.println("################# Order start save");
            super.save(session, w, datagramObject, nsiPackage);
            session.flush();
            System.out.println("################# Order save");

            System.out.println("################# Order start update");
            w.getResult().setState(ISharedBaseDao.instance.get().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
            session.update(w.getResult());
            System.out.println("################# Order update");

            if (params != null)
            {
                System.out.println("################# Paragraph start save");
                if (null != params.get(PARAGRAPH_PARAM)) session.saveOrUpdate(params.get(PARAGRAPH_PARAM));
                System.out.println("################# Paragraph save");
                if (null != params.get(EXTRACT_PARAM)) session.saveOrUpdate(params.get(EXTRACT_PARAM));

                session.flush();
                System.out.println("################# Extract save");

                if (null != params.get(EXTRACT_PARAM))
                {
                    System.out.println("################# Extract start update");
                    OtherStudentExtract extract = (OtherStudentExtract) params.get(EXTRACT_PARAM);
                    extract.setState(ISharedBaseDao.instance.get().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
                    session.update(extract);
                    System.out.println("################# Extract update");
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }


    private String getNextNumber(String orderNumber, Date orderDate)
    {
        int i = 1;
        String currentNumber = orderNumber;
        while (true)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentOrder.class, "o")
                    .column("o.id")
                    .where(eq(property("o", AbstractStudentOrder.number()), value(currentNumber)));
            FilterUtils.applyInYearFilter(builder, "o", AbstractStudentOrder.P_COMMIT_DATE, CoreDateUtils.getYear(orderDate));

            if (!ISharedBaseDao.instance.get().existsEntity(builder.buildQuery()))
                return currentNumber;

            currentNumber = orderNumber + "/" + i++;

        }

    }
}