/* $Id:$ */
package ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.logic;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 16.03.2017
 */
public interface INsiMvsDao
{
    /**
     * Удаляет "безхозные" выписки, приказы и параграфы прочих приказов.
     * Удаленю подлежат:
     * 1. выписки из прочих приказов без параграфа
     * 2. выписки из прочих приказов, включенные в параграфы, но не включенные в приказы
     * 3. параграфы прочих приказов, не имеющих выписок
     * 4. параграфы прочих приказов, не включенные в приказ
     * 5. прочие приказы, в которые не включен ни один параграф
     */
    void doRemoveIncorrectOtherOrders();

    /**
     * Возвращает список врапперов прочих приказов и выписок дублей для удаления.
     *
     * @return - список врапперов прочих приказов-дублей, подлежащих удалению
     */
    List<OrderDeDuplicationWrapper> getOrdersWrappersToDel();

    /**
     * Удаляет выписки, параграфы и прочие приказы, являющиеся дублями.
     *
     * @param ordersToDel - список врапперов прочих приказов-дублей.
     */
    void doDeleteDuplicates(Collection<OrderDeDuplicationWrapper> ordersToDel);

    /**
     * Удаляет "осиротевшие" сущности НСИ, хранящие GUID'ы объектов.
     * Требуется вызвать, чтобы освободить GUID'ы после удаления дублей.
     */
    void doDeleteOrphanOrderNsiEntity();
}