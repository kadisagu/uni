/* $Id:$ */
package ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.logic;

import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.03.2017
 */
public class NsiMvsDao extends BaseModifyAggregateDAO implements INsiMvsDao
{
    @Override
    public void doRemoveIncorrectOtherOrders()
    {
        // Сначала удаляем осиротевшие выписки, параграфы и приказы: прочие приказы всегда содержат один параграф и одну выписку,
        // если выписка существует без приказа, или без параграфа, то она удаляется
        // если параграф существует без выписки и без приказа, то она подлежит удалению
        // если приказ существует без параграфа и без выписки, то он удаляется

        // Удаляем выписки, не включенные в параграфы
        DQLSelectBuilder orphanExtractSubBuilder = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e")
                .where(isNull(property("e", OtherStudentExtract.paragraph())))
                .column(property("e", OtherStudentExtract.id()));

        DQLDeleteBuilder orphanExtractDelBuilder = new DQLDeleteBuilder(OtherStudentExtract.class)
                .where(in(property(OtherStudentExtract.id()), orphanExtractSubBuilder.buildQuery()));
        orphanExtractDelBuilder.createStatement(getSession()).execute();

        // Удаляем выписки, включенные в параграф, но не включенные в приказ
        DQLSelectBuilder orphanParagraphExtractSubBuilder = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e")
                .where(isNull(property("e", OtherStudentExtract.paragraph().order())))
                .column(property("e", OtherStudentExtract.id()));

        DQLDeleteBuilder orphanParagraphExtractDelBuilder = new DQLDeleteBuilder(OtherStudentExtract.class)
                .where(in(property(OtherStudentExtract.id()), orphanParagraphExtractSubBuilder.buildQuery()));
        orphanParagraphExtractDelBuilder.createStatement(getSession()).execute();

        // Удаляем параграфы, включенные в приказ, но не имеющие выписки
        DQLSelectBuilder orphanParagraphWithoutExtractSubBuilder = new DQLSelectBuilder().fromEntity(StudentOtherParagraph.class, "p")
                .joinEntity("p", DQLJoinType.left, OtherStudentExtract.class, "e", eq(property("p", StudentOtherParagraph.id()), property("e", OtherStudentExtract.paragraph())))
                .where(isNull(property("e", OtherStudentExtract.id()))).column(property("p", StudentOtherParagraph.id()));

        DQLDeleteBuilder orphanParagraphWithoutExtractDelBuilder = new DQLDeleteBuilder(StudentOtherParagraph.class)
                .where(in(property(StudentOtherParagraph.id()), orphanParagraphWithoutExtractSubBuilder.buildQuery()));
        orphanParagraphWithoutExtractDelBuilder.createStatement(getSession()).execute();

        // Удаляем параграфы, не включенные в приказ
        DQLSelectBuilder orphanParagraphSubBuilder = new DQLSelectBuilder().fromEntity(StudentOtherParagraph.class, "e")
                .where(isNull(property("e", StudentOtherParagraph.order())))
                .column(property("e", StudentOtherParagraph.id()));

        DQLDeleteBuilder orphanParagraphDelBuilder = new DQLDeleteBuilder(StudentOtherParagraph.class)
                .where(in(property(StudentOtherParagraph.id()), orphanParagraphSubBuilder.buildQuery()));
        orphanParagraphDelBuilder.createStatement(getSession()).execute();

        // Удаляем приказы, на которые не ссылается ни один параграф
        DQLSelectBuilder orphanOrderSubBuilder = new DQLSelectBuilder().fromEntity(StudentOtherOrder.class, "e")
                .joinEntity("e", DQLJoinType.left, AbstractStudentParagraph.class, "p", eq(property("e", StudentOtherOrder.id()), property("p", AbstractStudentParagraph.order().id())))
                .where(isNull(property("p"))).column(property("e", StudentOtherOrder.id()));

        DQLDeleteBuilder orphanOrderDelBuilder = new DQLDeleteBuilder(StudentOtherOrder.class)
                .where(in(property(StudentOtherOrder.id()), orphanOrderSubBuilder.buildQuery()));
        orphanOrderDelBuilder.createStatement(getSession()).execute();
    }

    @Override
    public List<OrderDeDuplicationWrapper> getOrdersWrappersToDel()
    {
        // Находим и удаляем все дубли приказов.
        // Дубли определяем следующим образом:
        // 1. берем все приказы студента одного типа
        // 2. совпадающие по номеру и дате считаем дублями, причём учитывается наличие в номере разделителя "/", сам разделитель и всё, что после него отбрасывается.
        // Дубли сравниваются по дате создания объекта выписки (определяется из идентификатора) и оставляется наиболее свежий из дублей.
        // Все более старые дубли удаляются
        List<Object[]> orders = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e")
                .column(property("e", OtherStudentExtract.id()))
                .column(property("e", OtherStudentExtract.paragraph().order().id()))
                .column(property("e", OtherStudentExtract.entity().id()))
                .column(property("e", OtherStudentExtract.type().id()))
                .column(property("e", OtherStudentExtract.paragraph().order().number()))
                .column(property("e", OtherStudentExtract.paragraph().order().commitDate()))
                .order(property("e", OtherStudentExtract.entity().id()))
                .order(property("e", OtherStudentExtract.type().id()))
                .order(property("e", OtherStudentExtract.paragraph().order().commitDate()))
                .createStatement(getSession()).list();

        List<OrderDeDuplicationWrapper> ordersToDel = new ArrayList<>();
        Map<CoreCollectionUtils.Pair<Long, Long>, Map<CoreCollectionUtils.Pair<String, Date>, OrderDeDuplicationWrapper>> map = new HashMap<>();
        for (Object[] item : orders)
        {
            OrderDeDuplicationWrapper wrapper = new OrderDeDuplicationWrapper(item);
            CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(wrapper.getStudentId(), wrapper.getTypeId());
            CoreCollectionUtils.Pair<String, Date> numDateKey = new CoreCollectionUtils.Pair<>(wrapper.getNumberOrig(), wrapper.getDate());

            Map<CoreCollectionUtils.Pair<String, Date>, OrderDeDuplicationWrapper> studentTypedOrdersMap = map.get(key);
            if (null == studentTypedOrdersMap)
            {
                studentTypedOrdersMap = new HashMap<>();
                studentTypedOrdersMap.put(numDateKey, wrapper);
            } else
            {
                OrderDeDuplicationWrapper otherWrapper = studentTypedOrdersMap.get(numDateKey);
                OrderDeDuplicationWrapper resultWrapper = wrapper.getActualOrder(otherWrapper);
                studentTypedOrdersMap.put(numDateKey, resultWrapper);

                if (null != otherWrapper)
                {
                    if (otherWrapper.equals(resultWrapper)) ordersToDel.add(wrapper);
                    else ordersToDel.add(otherWrapper);
                }
            }
            map.put(key, studentTypedOrdersMap);
        }

        return ordersToDel;
    }

    @Override
    public void doDeleteDuplicates(Collection<OrderDeDuplicationWrapper> ordersToDel)
    {
        Set<Long> orderIdToDelSet = new HashSet<>();
        Set<Long> extractIdToDelSet = new HashSet<>();
        for (OrderDeDuplicationWrapper wrapper : ordersToDel)
        {
            orderIdToDelSet.add(wrapper.getOrderId());
            extractIdToDelSet.add(wrapper.getExtractId());
        }

        DQLDeleteBuilder duplicatedExtractDelBuilder = new DQLDeleteBuilder(OtherStudentExtract.class)
                .where(in(property(OtherStudentExtract.id()), extractIdToDelSet));
        duplicatedExtractDelBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder duplicatedParagraphDelBuilder = new DQLDeleteBuilder(StudentOtherParagraph.class)
                .where(in(property(StudentOtherParagraph.order().id()), orderIdToDelSet));
        duplicatedParagraphDelBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder duplicatedOrderDelBuilder = new DQLDeleteBuilder(StudentOtherOrder.class)
                .where(in(property(StudentOtherOrder.id()), orderIdToDelSet));
        duplicatedOrderDelBuilder.createStatement(getSession()).execute();
    }

    @Override
    public void doDeleteOrphanOrderNsiEntity()
    {
        // Удаляем GUID'ы, на которые не ссылается ни один параграф
        DQLSelectBuilder orphanNsiEntitySubBuilder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "e")
                .joinEntity("e", DQLJoinType.left, AbstractStudentOrder.class, "o", eq(property("e", NsiEntity.entityId()), property("o", AbstractStudentOrder.id())))
                .where(eq(property("e", NsiEntity.entityType()), value(AbstractStudentOrder.ENTITY_CLASS)))
                .where(isNull(property("o"))).column(property("e", NsiEntity.id()));

        DQLDeleteBuilder orphanNsiEntityDelBuilder = new DQLDeleteBuilder(NsiEntity.class)
                .where(in(property(NsiEntity.id()), orphanNsiEntitySubBuilder.buildQuery()));
        orphanNsiEntityDelBuilder.createStatement(getSession()).execute();
    }
}