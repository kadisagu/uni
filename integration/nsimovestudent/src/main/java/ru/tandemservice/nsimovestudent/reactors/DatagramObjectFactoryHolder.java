/* $Id:$ */
package ru.tandemservice.nsimovestudent.reactors;

/**
 * @author Dmitry Seleznev
 * @since 21.09.2016
 */
public class DatagramObjectFactoryHolder
{
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
}