/* $Id:$ */
package ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.tool.IdGen;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 17.03.2017
 */
public class OrderDeDuplicationWrapper
{
    private Long _orderId;
    private Long _extractId;
    private Long _studentId;
    private Long _typeId;
    private String _numberOrig;
    private String _number;
    private Date _date;
    private Date _dateCreated;

    public OrderDeDuplicationWrapper(Object[] item)
    {
        _extractId = (Long) item[0];
        _orderId = (Long) item[1];
        _studentId = (Long) item[2];
        _typeId = (Long) item[3];
        _number = (String) item[4];
        _date = (Date) item[5];
        _numberOrig = getOrigNumber(_number);
        _dateCreated = new Date(IdGen.getTime(_extractId));
    }

    private String getOrigNumber(String srcNumber)
    {
        if (null == StringUtils.trimToNull(srcNumber)) return null;
        if (srcNumber.contains("/"))
        {
            int slashIndex = srcNumber.indexOf("/");
            return srcNumber.substring(0, slashIndex);
        }
        return srcNumber;
    }

    public OrderDeDuplicationWrapper getActualOrder(OrderDeDuplicationWrapper otherOrder)
    {
        if (null == otherOrder) return this;

        if (_dateCreated.getTime() > otherOrder.getDateCreated().getTime())
            return this;

        return otherOrder;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public Long getTypeId()
    {
        return _typeId;
    }

    public String getNumberOrig()
    {
        return _numberOrig;
    }

    public String getNumber()
    {
        return _number;
    }

    public Date getDate()
    {
        return _date;
    }

    public Date getDateCreated()
    {
        return _dateCreated;
    }
}