/* $Id:$ */
package ru.tandemservice.nsimovestudent.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Dmitry Seleznev
 * @since 16.03.2017
 */
@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String NSI_MOVESTUDENT_SYSTEM_ACTION_PUB_ADDON_NAME = "nsimovestudentSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(NSI_MOVESTUDENT_SYSTEM_ACTION_PUB_ADDON_NAME, SystemActionPubMovestudentAddon.class))
                .create();
    }
}