/* $Id:$ */
package ru.tandemservice.nsimovestudent.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.BatchUtils;
import ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.MvsSystemActionManager;
import ru.tandemservice.nsimovestudent.base.bo.MvsSystemAction.logic.OrderDeDuplicationWrapper;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.03.2017
 */
public class SystemActionPubMovestudentAddon extends UIAddon
{
    public SystemActionPubMovestudentAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickMvsRemoveOrderDuplicates()
    {
        MvsSystemActionManager.instance().dao().doRemoveIncorrectOtherOrders();

        List<OrderDeDuplicationWrapper> duplicatedOrderList = MvsSystemActionManager.instance().dao().getOrdersWrappersToDel();

        BatchUtils.execute(duplicatedOrderList, 500, collection ->
        {
            MvsSystemActionManager.instance().dao().doDeleteDuplicates(collection);
        });

        MvsSystemActionManager.instance().dao().doDeleteOrphanOrderNsiEntity();
    }
}