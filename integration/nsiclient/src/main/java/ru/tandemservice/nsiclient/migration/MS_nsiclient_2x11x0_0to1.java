package ru.tandemservice.nsiclient.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.util.List;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_nsiclient_2x11x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nsiEntityLog

		// создано обязательное свойство guid
        if (!tool.columnExists("nsientitylog_t", "guid_p"))
        {


            // Удаляем сущности без привязки к NsiEntity
            tool.executeUpdate("DELETE FROM nsientitylog_t WHERE entity_id is NULL");

            // создать колонку
			tool.createColumn("nsientitylog_t", new DBColumn("guid_p", DBType.createVarchar(36)));

			tool.executeUpdate(
                    "UPDATE nsientitylog_t " +
                            "SET guid_p = e.guid_p " +
                            "FROM (" +
                            "SELECT id, guid_p FROM nsientity_t) e " +
                            "WHERE e.id = nsientitylog_t.entity_id"
            );

			// сделать колонку NOT NULL
			tool.setColumnNullable("nsientitylog_t", "guid_p", false);

		}


    }
}