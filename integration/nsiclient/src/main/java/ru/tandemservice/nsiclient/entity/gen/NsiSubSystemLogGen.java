package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Журнал НСИ для подсистемы НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiSubSystemLogGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiSubSystemLog";
    public static final String ENTITY_NAME = "nsiSubSystemLog";
    public static final int VERSION_HASH = 1850191593;
    private static IEntityMeta ENTITY_META;

    public static final String P_GUID = "guid";
    public static final String P_DESTINATION = "destination";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_STATUS = "status";
    public static final String P_ADDITIONAL_STATUS = "additionalStatus";
    public static final String P_HEADER = "header";
    public static final String P_ERROR = "error";
    public static final String P_COMMENT = "comment";
    public static final String P_ATTEMPT_NUMBER = "attemptNumber";
    public static final String L_PACK = "pack";
    public static final String L_PREV_ATTEMPT_LOG = "prevAttemptLog";

    private String _guid;     // Идентификатор сообщения
    private String _destination;     // Код подсистемы получателя
    private Date _startDate;     // Дата и время отправки сообщения
    private Date _endDate;     // Дата и время завершения обработки сообщения
    private int _status = 0;     // Статус сообщения
    private int _additionalStatus = 0;     // Дополнительный статус сообщения
    private String _header;     // Технический заголовок сообщения
    private String _error;     // SOAP Fault сообщения
    private String _comment;     // Комментарий
    private int _attemptNumber = 0;     // Номер попытки отправки пакета
    private NsiPackage _pack;     // Пакет НСИ
    private NsiSubSystemLog _prevAttemptLog;     // Ссылка на строку лога предыдущей попытки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор сообщения. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getGuid()
    {
        return _guid;
    }

    /**
     * @param guid Идентификатор сообщения. Свойство не может быть null и должно быть уникальным.
     */
    public void setGuid(String guid)
    {
        dirty(_guid, guid);
        _guid = guid;
    }

    /**
     * @return Код подсистемы получателя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDestination()
    {
        return _destination;
    }

    /**
     * @param destination Код подсистемы получателя. Свойство не может быть null.
     */
    public void setDestination(String destination)
    {
        dirty(_destination, destination);
        _destination = destination;
    }

    /**
     * @return Дата и время отправки сообщения. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата и время отправки сообщения. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата и время завершения обработки сообщения.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата и время завершения обработки сообщения.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Статус сообщения. Свойство не может быть null.
     */
    @NotNull
    public int getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус сообщения. Свойство не может быть null.
     */
    public void setStatus(int status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Дополнительный статус сообщения. Свойство не может быть null.
     */
    @NotNull
    public int getAdditionalStatus()
    {
        return _additionalStatus;
    }

    /**
     * @param additionalStatus Дополнительный статус сообщения. Свойство не может быть null.
     */
    public void setAdditionalStatus(int additionalStatus)
    {
        dirty(_additionalStatus, additionalStatus);
        _additionalStatus = additionalStatus;
    }

    /**
     * @return Технический заголовок сообщения.
     */
    public String getHeader()
    {
        initLazyForGet("header");
        return _header;
    }

    /**
     * @param header Технический заголовок сообщения.
     */
    public void setHeader(String header)
    {
        initLazyForSet("header");
        dirty(_header, header);
        _header = header;
    }

    /**
     * @return SOAP Fault сообщения.
     */
    public String getError()
    {
        initLazyForGet("error");
        return _error;
    }

    /**
     * @param error SOAP Fault сообщения.
     */
    public void setError(String error)
    {
        initLazyForSet("error");
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Номер попытки отправки пакета. Свойство не может быть null.
     */
    @NotNull
    public int getAttemptNumber()
    {
        return _attemptNumber;
    }

    /**
     * @param attemptNumber Номер попытки отправки пакета. Свойство не может быть null.
     */
    public void setAttemptNumber(int attemptNumber)
    {
        dirty(_attemptNumber, attemptNumber);
        _attemptNumber = attemptNumber;
    }

    /**
     * @return Пакет НСИ. Свойство не может быть null.
     */
    @NotNull
    public NsiPackage getPack()
    {
        return _pack;
    }

    /**
     * @param pack Пакет НСИ. Свойство не может быть null.
     */
    public void setPack(NsiPackage pack)
    {
        dirty(_pack, pack);
        _pack = pack;
    }

    /**
     * @return Ссылка на строку лога предыдущей попытки.
     */
    public NsiSubSystemLog getPrevAttemptLog()
    {
        return _prevAttemptLog;
    }

    /**
     * @param prevAttemptLog Ссылка на строку лога предыдущей попытки.
     */
    public void setPrevAttemptLog(NsiSubSystemLog prevAttemptLog)
    {
        dirty(_prevAttemptLog, prevAttemptLog);
        _prevAttemptLog = prevAttemptLog;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiSubSystemLogGen)
        {
            setGuid(((NsiSubSystemLog)another).getGuid());
            setDestination(((NsiSubSystemLog)another).getDestination());
            setStartDate(((NsiSubSystemLog)another).getStartDate());
            setEndDate(((NsiSubSystemLog)another).getEndDate());
            setStatus(((NsiSubSystemLog)another).getStatus());
            setAdditionalStatus(((NsiSubSystemLog)another).getAdditionalStatus());
            setHeader(((NsiSubSystemLog)another).getHeader());
            setError(((NsiSubSystemLog)another).getError());
            setComment(((NsiSubSystemLog)another).getComment());
            setAttemptNumber(((NsiSubSystemLog)another).getAttemptNumber());
            setPack(((NsiSubSystemLog)another).getPack());
            setPrevAttemptLog(((NsiSubSystemLog)another).getPrevAttemptLog());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiSubSystemLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiSubSystemLog.class;
        }

        public T newInstance()
        {
            return (T) new NsiSubSystemLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "guid":
                    return obj.getGuid();
                case "destination":
                    return obj.getDestination();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "status":
                    return obj.getStatus();
                case "additionalStatus":
                    return obj.getAdditionalStatus();
                case "header":
                    return obj.getHeader();
                case "error":
                    return obj.getError();
                case "comment":
                    return obj.getComment();
                case "attemptNumber":
                    return obj.getAttemptNumber();
                case "pack":
                    return obj.getPack();
                case "prevAttemptLog":
                    return obj.getPrevAttemptLog();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "guid":
                    obj.setGuid((String) value);
                    return;
                case "destination":
                    obj.setDestination((String) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "status":
                    obj.setStatus((Integer) value);
                    return;
                case "additionalStatus":
                    obj.setAdditionalStatus((Integer) value);
                    return;
                case "header":
                    obj.setHeader((String) value);
                    return;
                case "error":
                    obj.setError((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "attemptNumber":
                    obj.setAttemptNumber((Integer) value);
                    return;
                case "pack":
                    obj.setPack((NsiPackage) value);
                    return;
                case "prevAttemptLog":
                    obj.setPrevAttemptLog((NsiSubSystemLog) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "guid":
                        return true;
                case "destination":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "status":
                        return true;
                case "additionalStatus":
                        return true;
                case "header":
                        return true;
                case "error":
                        return true;
                case "comment":
                        return true;
                case "attemptNumber":
                        return true;
                case "pack":
                        return true;
                case "prevAttemptLog":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "guid":
                    return true;
                case "destination":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "status":
                    return true;
                case "additionalStatus":
                    return true;
                case "header":
                    return true;
                case "error":
                    return true;
                case "comment":
                    return true;
                case "attemptNumber":
                    return true;
                case "pack":
                    return true;
                case "prevAttemptLog":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "guid":
                    return String.class;
                case "destination":
                    return String.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "status":
                    return Integer.class;
                case "additionalStatus":
                    return Integer.class;
                case "header":
                    return String.class;
                case "error":
                    return String.class;
                case "comment":
                    return String.class;
                case "attemptNumber":
                    return Integer.class;
                case "pack":
                    return NsiPackage.class;
                case "prevAttemptLog":
                    return NsiSubSystemLog.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiSubSystemLog> _dslPath = new Path<NsiSubSystemLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiSubSystemLog");
    }
            

    /**
     * @return Идентификатор сообщения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getGuid()
     */
    public static PropertyPath<String> guid()
    {
        return _dslPath.guid();
    }

    /**
     * @return Код подсистемы получателя. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getDestination()
     */
    public static PropertyPath<String> destination()
    {
        return _dslPath.destination();
    }

    /**
     * @return Дата и время отправки сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата и время завершения обработки сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Статус сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getStatus()
     */
    public static PropertyPath<Integer> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Дополнительный статус сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getAdditionalStatus()
     */
    public static PropertyPath<Integer> additionalStatus()
    {
        return _dslPath.additionalStatus();
    }

    /**
     * @return Технический заголовок сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getHeader()
     */
    public static PropertyPath<String> header()
    {
        return _dslPath.header();
    }

    /**
     * @return SOAP Fault сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getError()
     */
    public static PropertyPath<String> error()
    {
        return _dslPath.error();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Номер попытки отправки пакета. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getAttemptNumber()
     */
    public static PropertyPath<Integer> attemptNumber()
    {
        return _dslPath.attemptNumber();
    }

    /**
     * @return Пакет НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getPack()
     */
    public static NsiPackage.Path<NsiPackage> pack()
    {
        return _dslPath.pack();
    }

    /**
     * @return Ссылка на строку лога предыдущей попытки.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getPrevAttemptLog()
     */
    public static NsiSubSystemLog.Path<NsiSubSystemLog> prevAttemptLog()
    {
        return _dslPath.prevAttemptLog();
    }

    public static class Path<E extends NsiSubSystemLog> extends EntityPath<E>
    {
        private PropertyPath<String> _guid;
        private PropertyPath<String> _destination;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Integer> _status;
        private PropertyPath<Integer> _additionalStatus;
        private PropertyPath<String> _header;
        private PropertyPath<String> _error;
        private PropertyPath<String> _comment;
        private PropertyPath<Integer> _attemptNumber;
        private NsiPackage.Path<NsiPackage> _pack;
        private NsiSubSystemLog.Path<NsiSubSystemLog> _prevAttemptLog;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор сообщения. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getGuid()
     */
        public PropertyPath<String> guid()
        {
            if(_guid == null )
                _guid = new PropertyPath<String>(NsiSubSystemLogGen.P_GUID, this);
            return _guid;
        }

    /**
     * @return Код подсистемы получателя. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getDestination()
     */
        public PropertyPath<String> destination()
        {
            if(_destination == null )
                _destination = new PropertyPath<String>(NsiSubSystemLogGen.P_DESTINATION, this);
            return _destination;
        }

    /**
     * @return Дата и время отправки сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(NsiSubSystemLogGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата и время завершения обработки сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(NsiSubSystemLogGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Статус сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getStatus()
     */
        public PropertyPath<Integer> status()
        {
            if(_status == null )
                _status = new PropertyPath<Integer>(NsiSubSystemLogGen.P_STATUS, this);
            return _status;
        }

    /**
     * @return Дополнительный статус сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getAdditionalStatus()
     */
        public PropertyPath<Integer> additionalStatus()
        {
            if(_additionalStatus == null )
                _additionalStatus = new PropertyPath<Integer>(NsiSubSystemLogGen.P_ADDITIONAL_STATUS, this);
            return _additionalStatus;
        }

    /**
     * @return Технический заголовок сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getHeader()
     */
        public PropertyPath<String> header()
        {
            if(_header == null )
                _header = new PropertyPath<String>(NsiSubSystemLogGen.P_HEADER, this);
            return _header;
        }

    /**
     * @return SOAP Fault сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getError()
     */
        public PropertyPath<String> error()
        {
            if(_error == null )
                _error = new PropertyPath<String>(NsiSubSystemLogGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(NsiSubSystemLogGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Номер попытки отправки пакета. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getAttemptNumber()
     */
        public PropertyPath<Integer> attemptNumber()
        {
            if(_attemptNumber == null )
                _attemptNumber = new PropertyPath<Integer>(NsiSubSystemLogGen.P_ATTEMPT_NUMBER, this);
            return _attemptNumber;
        }

    /**
     * @return Пакет НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getPack()
     */
        public NsiPackage.Path<NsiPackage> pack()
        {
            if(_pack == null )
                _pack = new NsiPackage.Path<NsiPackage>(L_PACK, this);
            return _pack;
        }

    /**
     * @return Ссылка на строку лога предыдущей попытки.
     * @see ru.tandemservice.nsiclient.entity.NsiSubSystemLog#getPrevAttemptLog()
     */
        public NsiSubSystemLog.Path<NsiSubSystemLog> prevAttemptLog()
        {
            if(_prevAttemptLog == null )
                _prevAttemptLog = new NsiSubSystemLog.Path<NsiSubSystemLog>(L_PREV_ATTEMPT_LOG, this);
            return _prevAttemptLog;
        }

        public Class getEntityClass()
        {
            return NsiSubSystemLog.class;
        }

        public String getEntityName()
        {
            return "nsiSubSystemLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
