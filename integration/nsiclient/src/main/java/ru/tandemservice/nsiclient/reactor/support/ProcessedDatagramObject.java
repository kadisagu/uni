/* $Id: ProcessedDatagramObject.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 22.05.2015
 */
public class ProcessedDatagramObject<V extends IDatagramObject>
{
    private List<IDatagramObject> _list;
    private V _object;

    public ProcessedDatagramObject(V object, List<IDatagramObject> list) {
        _list = list;
        _object = object;
    }

    public List<IDatagramObject> getList() {
        return _list;
    }

    public V getObject() {
        return _object;
    }
}
