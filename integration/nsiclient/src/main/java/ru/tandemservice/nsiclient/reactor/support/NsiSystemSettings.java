package ru.tandemservice.nsiclient.reactor.support;

import com.google.common.collect.Lists;
import org.apache.commons.lang.ArrayUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.NsiSettingsManager;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.codes.NsiSubSystemCodes;

import java.util.List;

public class NsiSystemSettings implements INsiSettings
{
    private static final int MAX_PACKAGE_SIZE = 1000;

    @Override
    public int[] getTimeIntervals() {
        String timeIntervals = NsiSettingsManager.instance().dao().getNotNullSettings().getTimeIntervals();
        if(timeIntervals != null)
        {
            String[] values = timeIntervals.split(",");
            List<Integer> integers = Lists.newArrayListWithExpectedSize(values.length);
            if(values.length > 0)
            {
                for(String s: values)
                    try {
                        integers.add(Integer.parseInt(s));
                    } catch (NumberFormatException e) {
                        ;
                    }
                return ArrayUtils.toPrimitive(integers.toArray(new Integer[integers.size()]));
            }
        }
        return null;
    }

    @Override
    public String getNsiAddress() {
        NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.code(), NsiSubSystemCodes.NSI);
        return subSystem.getAddress();
    }

    @Override
    public String getSystemCode() {
        NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.code(), NsiSubSystemCodes.LOCAL);
        return subSystem.getUserCode();
    }

    @Override
    public int getPackageSize() {
        Integer maxPackageSize = NsiSettingsManager.instance().dao().getNotNullSettings().getMaxPackageSize();
        if(maxPackageSize != null)
            return maxPackageSize.intValue();
        return MAX_PACKAGE_SIZE;
    }

    @Override
    public boolean isAsync() {
        return NsiSettingsManager.instance().dao().getNotNullSettings().isAsync();
    }
}
