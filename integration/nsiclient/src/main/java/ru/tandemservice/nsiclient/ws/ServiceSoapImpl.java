/* $Id: ServiceSoapImpl.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.ws;

import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.BindingType;

@WebService(name = "ServiceSoap", endpointInterface = "ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceSoap", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public class ServiceSoapImpl implements ServiceSoap
{
    @Override
    public ServiceResponseType update(@WebParam(name = "updateRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        return NsiRequestHandlerService.instance().process(parameters, NsiUtils.OPERATION_TYPE_UPDATE);
    }

    @Override
    public ServiceResponseType delete(@WebParam(name = "deleteRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        return NsiRequestHandlerService.instance().process(parameters, NsiUtils.OPERATION_TYPE_DELETE);
    }

    @Override
    public ServiceResponseType insert(@WebParam(name = "insertRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        return NsiRequestHandlerService.instance().process(parameters, NsiUtils.OPERATION_TYPE_INSERT);
    }

    @Override
    public ServiceResponseType retrieve(@WebParam(name = "retrieveRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        return NsiRequestHandlerService.instance().process(parameters, NsiUtils.OPERATION_TYPE_RETRIEVE);
    }

    @Override
    public ServiceResponseType initialize(@WebParam(name = "initializeRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        return NsiRequestHandlerService.instance().dummyResponse(parameters);
    }

    @Override
    public ServiceResponseType async(AsyncRequestType parameters) {
        return NsiRequestHandlerService.instance().asyncProcess(parameters);
    }

    @Override
    public ServiceResponseType2 route(@WebParam(name = "routeRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") RouteRequestType parameters)
    {
        return null;
    }

    @Override
    public ServiceResponseType2 deliver(@WebParam(name = "deliverRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") DeliverRequestType parameters)
    {
        return null;
    }

    @Override
    public ServiceResponseType commit(@WebParam(name = "commitRequest", targetNamespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        return null;
    }
}