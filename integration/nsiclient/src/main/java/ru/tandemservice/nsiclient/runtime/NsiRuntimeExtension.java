/* $Id: NsiRuntimeExtension.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Nikonov
 * @since 10.04.2015
 */
public class NsiRuntimeExtension implements IRuntimeExtension
{
    @Override
    public void init(Object object) {
        NsiCatalogManager.instance().dao().checkCatalogs();
        NsiCatalogManager.instance().dao().migration20151005();
    }

    @Override
    public void destroy() {

    }
}