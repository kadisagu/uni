/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;

import java.util.List;
import java.util.Set;

/**
 * @author Andrey Avetisov
 * @since 05.10.2015
 */
public interface INsiReactorTestGeneratorService
{
    /**
     * Метод генерирует и сохраняет в список сущность требуемого класса, со случайно-заполненными полями.
     * <p>
     * Ссылочные поля заполняются следующими способами:
     * 1) если ссылка на справочник (в объекте есть поле "code") - то происходит выбор случайного элемента из заполненого справочника.
     * 2) иначе (или если в справочник пуст) происходит рекурсинвая генерация вложенной сущности (и всего того, что необходимо дляя ее заполнения).
     *
     * @param entityClass        класс сущности уни, экземпляр которого необходимо сгенерировать.
     * @param createdEntityList  после отработки метода в данный список будут помещены сгенерированные сущности,
     *                           причем в порядке, требуемом для их дальнейшего сохранения.
     * @param processedEntitySet после отработки метода в данный список будут помещены обработанные сущности
     *                           от createdEntityList отличается тем, что сюда могут быть помещенны сущности
     *                           созданные до работы генератора (например, элементы справочника), на которые прямым
     *                           или косвенным образом есть ссылки от генерируемой сущности.
     *
     * @return возвращает экземпляр сгенерированного класса.
     * @throws InstantiationException
     * @throws IllegalAccessException
     */

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    IEntity generateEntities(Class entityClass, final List<IEntity> createdEntityList, final Set<IEntity> processedEntitySet) throws InstantiationException, IllegalAccessException;

}
