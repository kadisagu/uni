package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.reactor.support.ProcessingException;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.Datagram;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.RoutingHeaderType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceRequestType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceResponseType;

import java.util.List;
import java.util.Map;

public interface INsiSyncDAO
{
    <T> List<CoreCollectionUtils.Pair<T, NsiEntity>> getEntityWithNsiEntityList(final Class<T> entityClass, List<Long> entityIds);

    void createEntityLogs(NsiPackage nsiPackage, List<IDatagramObject> datagramElements);

    NsiSubSystemLog createLog(ServiceRequestType request, boolean isIncoming, List<IDatagramObject> datagramElements);

    NsiSubSystemLog createLog(String sourceId, String operationType, String destinationId, RoutingHeaderType header, String messageId, String parentId, Datagram datagram, boolean isIncoming);

    NsiSubSystemLog createLog(NsiSubSystemLog nsiSubSystemLog, RoutingHeaderType header);

    void updateLogStatusWaitResponse(NsiSubSystemLog subSystemLog);

    void updateLogStatusDelivered(NsiSubSystemLog subSystemLog);

    void finishLog(NsiSubSystemLog subSystemLog, String comment, boolean error, boolean hasWarning);

    void updateStatusInWork(NsiPackage nsiPackage);

    List<NsiSubSystemLogWrapper> getSubSystemLogs(NsiPackage pack);

    void send(NsiPackage pack, List<NsiSubSystemLogWrapper> logs) throws ProcessingException;

    List<Long> getEntityIdsByDatagramObjects(List<IDatagramObject> datagramObjects);

    List<Long> getEntityIdsByGuids(List<String> guids);

    List<String> getNonDeletableGuids(Class<? extends IEntity> entityClass, List<Long> entityIds);

    <T extends IEntity> Map<String, CoreCollectionUtils.Pair<T, NsiEntity>> getEntityWithNsiIdsMap(Class<T> entityClass, List<Long> entityIds);

    int deleteEntities(Class<? extends IEntity> entityClass, List<Long> entityIds);

    int deleteNsiEntities(List<Long> entityIds);

    NsiEntity saveOrUpdateNsiEntity(IEntity entity, NsiEntity nsiEntity, String guid, String catalogType);

    void doResolveLogToEntityReference(NsiEntity entity);

    void updateSyncTime(Long catalogId);

    <T> List<T> getEntityWithoutNsiEntityList(Class<T> entityClass);

    <T> List<String> getGuids(Class<T> entityClass, List<Long> entityIds);

    <T> List<NsiEntity> getNsiEntitiesByEntityIds(Class<T> entityClass, List<Long> entityIds);

    void sendToNsiQueue(Map<String, IDatagramObject> guidToDatagram, String operationType);

    void deleteNsiEntitiesByGuids(List<String> guids);

    void createInitialEntityLog(String guid, String catalogType, NsiPackage nsiPackage);

    void createEntityLog(NsiEntity entity, String catalogType, NsiPackage nsiPackage, String warning);

    Long createQueueItemForLogRow(NsiSubSystemLog log);

    //NsiPackage createOutgoingOperationLog(List<IDatagramObject> datagramElements, String operationType, String warning);

    NsiSubSystemLog updateStartOutcomingSubsystemLog(NsiSubSystemLog subSystemLog, boolean isAutoRepeat, boolean isManualRestart, boolean isWaitForResponse, String comment, Exception exception, ServiceResponseType response);

    List<Long> updateNsiEntitiesByDatagramObjects(List<IDatagramObject> datagramObjects);

    void clearSession();
}
