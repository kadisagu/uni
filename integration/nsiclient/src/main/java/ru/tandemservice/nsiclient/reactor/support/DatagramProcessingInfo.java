/* $Id:$ */
package ru.tandemservice.nsiclient.reactor.support;

/**
 * Информация об результатах обработки, содержит предупреждения и некритичные ошибки.
 * @author Andrey Nikonov
 * @since 26.11.2015
 */
public class DatagramProcessingInfo {
    private String _error;
    private String _warn;

    public DatagramProcessingInfo(String error, String warn) {
        _error = error;
        _warn = warn;
    }

    public String getError() {
        return _error;
    }

    public String getWarn() {
        return _warn;
    }
}
