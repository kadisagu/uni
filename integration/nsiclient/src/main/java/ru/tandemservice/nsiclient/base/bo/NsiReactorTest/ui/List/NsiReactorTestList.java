/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.NsiSubSystemManager;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

/**
 * @author Andrey Avetisov
 * @since 05.10.2015
 */

@Configuration
public class NsiReactorTestList extends BusinessComponentManager
{
    public static final String CATALOG_LIST_DS = "catalogListDS";
    public static final String SUBSYSTEM_LIST_DS = "subSystemListDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CATALOG_LIST_DS, catalogList(), NsiCatalogManager.instance().catalogsListDSHandler()))
                .addDataSource(selectDS(SUBSYSTEM_LIST_DS, NsiSubSystemManager.instance().subSystemDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint catalogList()
    {
        return columnListExtPointBuilder(CATALOG_LIST_DS)
                .addColumn(textColumn("title", NsiCatalogType.title()).order())
                .addColumn(textColumn("nsiEntity", NsiCatalogType.code()).order())
                .addColumn(textColumn("uniEntity", NsiCatalogType.nsiCode()).order())
                .addColumn(actionColumn("runTest", CommonDefines.ICON_EXECUTE, "onClickRunTest"))
                .addColumn(blockColumn("result", "resultBlock"))
                .create();
    }
}
