/* $Id: IOperationReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiPackage;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
@FunctionalInterface
public interface IOperationReactor {
    List<IDatagramObject> process(List<IDatagramObject> items, NsiPackage nsiPackage, Map<String, Object> commonCache, Map<String, Object> reactorCache, StringBuilder warnLog, StringBuilder errorLog);
}