package ru.tandemservice.nsiclient.reactor.support;

import org.apache.log4j.Level;

/**
 * Created by andrey on 4/20/15.
 */
public interface ILogger
{
    org.apache.log4j.Logger getLog4jLogger();

    void log(Level loggingLevel, String message);
}
