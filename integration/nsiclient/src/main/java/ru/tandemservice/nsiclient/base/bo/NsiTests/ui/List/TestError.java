/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiTests.ui.List;

import org.tandemframework.core.view.formatter.NewLineFormatter;

/**
 * @author Andrey Nikonov
 * @since 18.05.2015
 */
public class TestError
{
    private Throwable _exception;
    private String _title;
    private String _stackTrace;

    public TestError(Throwable exception, String title, String stackTrace) {
        _exception = exception;
        _title = title;
        _stackTrace = stackTrace;
    }

    public Throwable getException() {
        return _exception;
    }

    public String getTitle() {
        return _title;
    }

    public String getStackTrace() {
        return NewLineFormatter.SIMPLE.format(_stackTrace);
    }
}
