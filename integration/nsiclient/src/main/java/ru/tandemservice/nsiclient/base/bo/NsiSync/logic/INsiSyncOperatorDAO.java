/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.support.IResultProcessor;
import ru.tandemservice.nsiclient.reactor.support.SyncOperatorResult;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 21.10.2015
 */
public interface INsiSyncOperatorDAO {
    SyncOperatorResult update(List<IDatagramObject> datagramElements, NsiPackage nsiPackage, IResultProcessor resultProcessor, String messageId);
    SyncOperatorResult delete(List<IDatagramObject> datagramElements, NsiPackage nsiPackage);
    SyncOperatorResult retrieve(List<IDatagramObject> datagramElements, NsiPackage nsiPackage);
}
