package ru.tandemservice.nsiclient.base.bo.NsiPackage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.logic.INsiPackageDAO;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.logic.NsiPackageDAO;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.logic.NsiPackageDSHandler;
import ru.tandemservice.nsiclient.entity.NsiPackage;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiPackageManager extends BusinessObjectManager
{
    public static DateFormatter DATE_FORMATTER_WITH_MILLIS = new DateFormatter("dd.MM.yyyy HH:mm:ss.SSS");

    public static NsiPackageManager instance()
    {
        return instance(NsiPackageManager.class);
    }

    @Bean
    public INsiPackageDAO dao()
    {
        return new NsiPackageDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler packageDSHandler()
    {
        return new NsiPackageDSHandler(getName());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> messageStatusExtPoint()
    {
        return itemList(DataWrapper.class).
                add(Integer.toString(NsiPackage.STATUS_DRAFT), new DataWrapper(NsiPackage.STATUS_DRAFT, "Инициализация")).
                add(Integer.toString(NsiPackage.STATUS_IN_WORK), new DataWrapper(NsiPackage.STATUS_IN_WORK, "В обработке")).
                add(Integer.toString(NsiPackage.STATUS_EXECUTED), new DataWrapper(NsiPackage.STATUS_EXECUTED, "Обработан")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler messageStatusComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(NsiPackageManager.instance().messageStatusExtPoint());
    }
}
