package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Очередь событий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiQueueGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiQueue";
    public static final String ENTITY_NAME = "nsiQueue";
    public static final int VERSION_HASH = 1754758456;
    private static IEntityMeta ENTITY_META;

    public static final String L_LOG = "log";
    public static final String L_DESTINATION = "destination";
    public static final String P_CREATE_DATE = "createDate";

    private NsiSubSystemLog _log;     // Журнал НСИ
    private NsiSubSystem _destination;     // Подсистема получатель сообщения
    private Date _createDate;     // Дата и время постановки в очередь

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Журнал НСИ. Свойство не может быть null.
     */
    @NotNull
    public NsiSubSystemLog getLog()
    {
        return _log;
    }

    /**
     * @param log Журнал НСИ. Свойство не может быть null.
     */
    public void setLog(NsiSubSystemLog log)
    {
        dirty(_log, log);
        _log = log;
    }

    /**
     * @return Подсистема получатель сообщения. Свойство не может быть null.
     */
    @NotNull
    public NsiSubSystem getDestination()
    {
        return _destination;
    }

    /**
     * @param destination Подсистема получатель сообщения. Свойство не может быть null.
     */
    public void setDestination(NsiSubSystem destination)
    {
        dirty(_destination, destination);
        _destination = destination;
    }

    /**
     * @return Дата и время постановки в очередь. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата и время постановки в очередь. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiQueueGen)
        {
            setLog(((NsiQueue)another).getLog());
            setDestination(((NsiQueue)another).getDestination());
            setCreateDate(((NsiQueue)another).getCreateDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiQueueGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiQueue.class;
        }

        public T newInstance()
        {
            return (T) new NsiQueue();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "log":
                    return obj.getLog();
                case "destination":
                    return obj.getDestination();
                case "createDate":
                    return obj.getCreateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "log":
                    obj.setLog((NsiSubSystemLog) value);
                    return;
                case "destination":
                    obj.setDestination((NsiSubSystem) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "log":
                        return true;
                case "destination":
                        return true;
                case "createDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "log":
                    return true;
                case "destination":
                    return true;
                case "createDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "log":
                    return NsiSubSystemLog.class;
                case "destination":
                    return NsiSubSystem.class;
                case "createDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiQueue> _dslPath = new Path<NsiQueue>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiQueue");
    }
            

    /**
     * @return Журнал НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiQueue#getLog()
     */
    public static NsiSubSystemLog.Path<NsiSubSystemLog> log()
    {
        return _dslPath.log();
    }

    /**
     * @return Подсистема получатель сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiQueue#getDestination()
     */
    public static NsiSubSystem.Path<NsiSubSystem> destination()
    {
        return _dslPath.destination();
    }

    /**
     * @return Дата и время постановки в очередь. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiQueue#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    public static class Path<E extends NsiQueue> extends EntityPath<E>
    {
        private NsiSubSystemLog.Path<NsiSubSystemLog> _log;
        private NsiSubSystem.Path<NsiSubSystem> _destination;
        private PropertyPath<Date> _createDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Журнал НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiQueue#getLog()
     */
        public NsiSubSystemLog.Path<NsiSubSystemLog> log()
        {
            if(_log == null )
                _log = new NsiSubSystemLog.Path<NsiSubSystemLog>(L_LOG, this);
            return _log;
        }

    /**
     * @return Подсистема получатель сообщения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiQueue#getDestination()
     */
        public NsiSubSystem.Path<NsiSubSystem> destination()
        {
            if(_destination == null )
                _destination = new NsiSubSystem.Path<NsiSubSystem>(L_DESTINATION, this);
            return _destination;
        }

    /**
     * @return Дата и время постановки в очередь. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiQueue#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(NsiQueueGen.P_CREATE_DATE, this);
            return _createDate;
        }

        public Class getEntityClass()
        {
            return NsiQueue.class;
        }

        public String getEntityName()
        {
            return "nsiQueue";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
