package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.InfoTab;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiPackageInfoTab extends BusinessComponentManager
{
}