/* $Id: NsiSubSystemInfoTabUI.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.InfoTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.EditAddress.NsiSubSystemEditAddress;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
public class NsiSubSystemInfoTabUI extends UIPresenter
{
    private Long _id;
    private NsiSubSystem _catalog;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public void onComponentRefresh()
    {
        _catalog = DataAccessServices.dao().get(NsiSubSystem.class, _id);
    }

    public void onEditAddress()
    {
        _uiActivation.asRegionDialog(NsiSubSystemEditAddress.class)
                .parameter(UIPresenter.PUBLISHER_ID, _id)
                .activate();
    }
}
