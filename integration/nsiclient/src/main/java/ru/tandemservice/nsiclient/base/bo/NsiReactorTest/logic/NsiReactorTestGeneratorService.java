/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.tandemframework.common.base.entity.IPersistentBpmParticipant;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.meta.entity.IRelationSubject;
import org.tandemframework.core.meta.entity.impl.FieldPropertyMeta;
import org.tandemframework.core.meta.entity.impl.ManyToOneMeta;
import org.tandemframework.core.meta.entity.impl.PropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressType;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EduLevel;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Avetisov
 * @since 05.10.2015
 */
public class NsiReactorTestGeneratorService implements INsiReactorTestGeneratorService
{

    private static final int MAX_NUMERIC_VALUE = 100_000;
    private static final int MAX_STRING_VALUE = 50;
    private static Random random = new Random();

    //В целях генерации рандомной сущности иногда бывает необходимо работать с классами сторонних модулей (для обхода констрейнтов),
    //и эта работа выполняется через приведенные ниже константы.
    // Решение не безопасно, но используется для проведения единичных тестирований НСИ.
    private static String educationLevelsHighSchoolClass = "ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool";
    private static String ctrContractVersionClass = "org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion";
    private static String educationLevelsClass = "ru.tandemservice.uni.entity.catalog.EducationLevels";
    private static String orgUnitTypePostRelationClass = "org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation";
    private static String eppWorkPlanClass = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlan";
    private static String eduProgramClass = "ru.tandemservice.uniedu.program.entity.EduProgram";
    private static String eduOwnerOrgUnitClass = "ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit";
    private static String orgUnitClass = "org.tandemframework.shared.organization.base.entity.OrgUnit";
    private static String iCtrPriceElementClass = "org.tandemframework.shared.ctr.base.entity.ICtrPriceElement";
    private static String eduProgramSubjectClass = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject";
    private static String eduProgramQualificationClass = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification";
    private static String eduProgramSubjectIndexClass = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex";
    private static String eduProgramSubjectQualificationClass = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification";
    private static String ctrContractVersionContractorClass = "org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor";
    private static String contactorPersonClass = "org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson";
    private static String eduProgramSubjectOksoClass = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso";
    private static String eduProgramHigherProfClass = "ru.tandemservice.uniedu.program.entity.EduProgramHigherProf";
    private static String eduProgramKindClass = "ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind";
    private static String eduProgramSpecializationClass = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization";
    private static String eppStateClass = "ru.tandemservice.uniepp.entity.catalog.EppState";


    @Override
    public IEntity generateEntities(Class entityClass, final List<IEntity> createdEntityList, final Set<IEntity> processedEntitySet) throws InstantiationException, IllegalAccessException
    {
        if (createdEntityList.size() > 0 || processedEntitySet.size() > 0)
        {
            throw new InstantiationException("processedEntitySet and createdEntityList must be empty!");
        }
        List<IEntity> entityList = new ArrayList<>();


        IEntity baseEntity = generateRandomEntity(entityClass, true, entityList);

        //сущности в списке частично упордочены, в обратном порядке,
        //для снижения числа итераций при сохранениий, делаем reverse.
        Collections.reverse(entityList);

        //Заполняем список createdEntityList и набор processedEntitySet.
        //Для дальнейшего удобства в createdEntityList сущности помещаются
        // в порядке их возможного сохранения.
        while (processedEntitySet.size() != entityList.size())
        {
            for (IEntity entity : entityList)
            {
                if (!processedEntitySet.contains(entity) && canSave(entity, createdEntityList))
                {
                    //в список createdEntityList добавляем только те сущности,
                    // которые в дальнейшем необходимо сохранять. Справочники сюда не входят.
                    if (entity.getId() == null)
                    {
                        createdEntityList.add(entity);
                    }
                    processedEntitySet.add(entity);
                }
            }
        }

        return baseEntity;
    }


    /**
     * Проверяет можно ли сохранять переданную сущность (все ли обязательные ссылочне поля заполненны).
     *
     * @param entity проверяемая сущность
     * @return true - у сущность нет не
     */
    private static boolean canSave(IEntity entity, List<IEntity> createdEntityList)
    {
        IEntityMeta entityMeta = EntityRuntime.getMeta(entity);

        for (String propTitle : entityMeta.getPropertyNames())
        {
            if (!(entityMeta.getProperty((propTitle)) instanceof ManyToOneMeta))
            {
                continue;
            }
            if (!((ManyToOneMeta) entityMeta.getProperty(propTitle)).isRequired())
            {
                continue;
            }

            //если список уже обработанных сущностей не содержит сущность
            //для заполяемого поля, то сохранять - нельзя.
            IEntity linkedEntity = (IEntity) entity.getProperty(propTitle);
            Long id = linkedEntity.getId();
            if (id == null && !createdEntityList.contains(linkedEntity))
                return false;
        }

        return true;
    }

    /**
     * Создает и сохраняет в базе сущность, со случайным образом заполненными полями.
     * Может вызываться рекурсивно, в случае если поля сущности являются ссылками на другие сущности.
     *
     * @param clazz               класс генерируемой сущности.
     * @param isBaseEntity        является ли сущность корневой (или метод был вызван рекурсивно для создания вложенной сущности).
     * @param generatedEntityList список со сгенерирвоанными сущностями.
     * @return возвращает сгенерированную сущность.
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static IEntity generateRandomEntity(Class clazz, Boolean isBaseEntity, final List<IEntity> generatedEntityList) throws IllegalAccessException, InstantiationException
    {
        IEntity entity = getNonAbstarctEntity(clazz);

        if (entity == null)
        {
            return null;
        }

        generatedEntityList.add(entity);
        IEntityMeta meta = EntityRuntime.getMeta(entity);

        List<String> propertyList = new ArrayList<>(meta.getPropertyNames());
        if (propertyList.contains("kind"))
        {
            //вид образовательной программы должен быть взят из перечня, поэтому заполняем это поле в конце.
            Collections.swap(propertyList, propertyList.indexOf("kind"), propertyList.size() - 1);
        }
        for (String propertyTitle : propertyList)
        {
            if (propertyTitle.equals(IEntity.P_ID) || propertyTitle.equals(IEntity.P_CLASS) || propertyTitle.equals(PersonEduInstitution.ENTITY_NAME)
                    || (meta.getName().equals("ctrContractVersion") && propertyTitle.equals("print"))
                    )
            {
                continue;
            }
            IPropertyMeta propertyMeta = meta.getProperty(propertyTitle);
            setPrimitiveValue(entity, propertyMeta);
            setLinkedValue(entity, propertyMeta, isBaseEntity, generatedEntityList);
        }

        if (clazz.equals(PersonContactData.class))
        {
            IEntity person = generateRandomEntity(Person.class, false, generatedEntityList);

            //если сущность не базовая, то персона уже есть в generatedEntityList, поэтому удаляем дубль
            if (!isBaseEntity)
            {
                generatedEntityList.remove(person);
            }
        }

        return entity;
    }

    private static void setLinkedValue(IEntity entity, IPropertyMeta propertyMeta, boolean isBaseEntity, final List<IEntity> generatedEntityList) throws InstantiationException, IllegalAccessException
    {
        if (propertyMeta instanceof IRelationSubject)
        {
            FieldPropertyMeta linkedProperty = (FieldPropertyMeta) propertyMeta;

            //проверяем нужно ли вообще заполнять свойство
            //заполнять необходимо только обязательные поля и все поля корневой сущности
            if (isNeedToFill(linkedProperty, isBaseEntity, entity))
            {
                IEntity innerEntity = null;

                //если для заполнения нужна сущность из справочника, то пытаемcя
                //найти ее в базе.
                if (!isNeedGenerate(entity, propertyMeta))
                {
                    innerEntity = getRandomCatalogEntity(propertyMeta.getValueType(), entity);
                    if (innerEntity != null && !generatedEntityList.contains(innerEntity))
                    {
                        generatedEntityList.add(innerEntity);
                    }
                }
                // если в справочниках ничего нет,
                // или ссылка не на справочник, то ищем
                // сущность среди сгенерированных или генерируем новую.
                if (innerEntity == null)
                {
                    //пытаемся найти среди уже сгенерированных сущностей
                    innerEntity = getInnerEntityFromCreatedList((IRelationSubject) linkedProperty, generatedEntityList);

                    //если не нашли (или нашли объект того же класса, что и у сущности, что может быть при заполеннии предков),
                    // то генерируем новую сущность
                    if (innerEntity == null || linkedProperty.getValueType().equals(entity.getClass()))
                    {
                        innerEntity = generateRandomEntity(linkedProperty.getValueType(), false, generatedEntityList);
                    }
                }

                entity.setProperty(propertyMeta.getPropertyPath(), innerEntity);
            }
        }
    }

    private static void setPrimitiveValue(IEntity entity, IPropertyMeta propertyMeta)
    {
        Class propertyClass = propertyMeta.getValueType();
        if (((FieldPropertyMeta) propertyMeta).getFormula() == null)
        {
            if (propertyClass.equals(String.class))
            {
                setStringProperty(entity, propertyMeta);
            }
            else if (propertyClass.equals(int.class) || propertyClass.equals(Integer.class))
            {
                setIntegerProperty(entity, propertyMeta);
            }
            else if (propertyClass.equals(long.class) || propertyClass.equals(Long.class))
            {
                setLongProperty(entity, propertyMeta);
            }
            else if (propertyClass.equals(double.class) || propertyClass.equals(Double.class))
            {
                setDoubleProperty(entity, propertyMeta);
            }
            else if (propertyClass.equals(float.class) || propertyClass.equals(Float.class))
            {
                setFloatProperty(entity, propertyMeta);
            }
            else if (propertyClass.equals(boolean.class) || propertyClass.equals(Boolean.class))
            {
                setBooleanProperty(entity, propertyMeta);
            }
            else if (propertyClass.equals(Date.class))
            {
                setDateProperty(entity, propertyMeta);
            }
        }
    }

    /**
     * Генерирует строковое значение и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setStringProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        String value = getGeneratedString(entity, propertyMeta);
        entity.setProperty(propertyMeta.getPropertyPath(), value);
    }

    /**
     * Возвращает случайно-сгенерированное строковое значени.
     * Длинна возвращеммой строк зависит
     * от ограниченй наложннеых на переданное свойство,
     * но не больше 300 и не меньше 1.
     *
     * @param propertyMeta свойство, для готорго генерируется строка
     * @return случайно-сгенерированное строковое значени
     */
    private static String getGeneratedString(IEntity entity, IPropertyMeta propertyMeta)
    {
        if (entity instanceof AddressItem || entity instanceof AddressType)
        {
            int propertyLenght = ((PropertyMeta) propertyMeta).getLength();
            //чтобы не преполнялась длинна автообновляемых полей необходимо ввести доб. ограничения.
            int strLenght = random.nextInt(propertyLenght <= 15 ? propertyLenght : 15);
            return RandomStringUtils.randomAlphabetic(strLenght == 0 ? 1 : strLenght);
        }
        if (propertyMeta.getName().equals("documentKindDetailed"))
        {
            return null;
        }
        int propertyLenght = ((PropertyMeta) propertyMeta).getLength();
        int strLenght = random.nextInt(propertyLenght <= MAX_STRING_VALUE ? propertyLenght : MAX_STRING_VALUE);
        return RandomStringUtils.randomAlphabetic(strLenght == 0 ? 1 : strLenght);
    }

    /**
     * Генерирует булевое значение и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setBooleanProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        if (propertyMeta.getName().equals("loadPresentationInWeeks"))
        {
            entity.setProperty(propertyMeta.getPropertyPath(), false);
        }
        else
        {
            Boolean value = random.nextBoolean();
            entity.setProperty(propertyMeta.getPropertyPath(), value);
        }
    }

    /**
     * Генерирует значение типа Long и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setLongProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        Long value = (long) random.nextInt(MAX_NUMERIC_VALUE);
        entity.setProperty(propertyMeta.getPropertyPath(), value);
    }

    /**
     * Генерирует значение типа Integer и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setIntegerProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        Integer value;
        if (propertyMeta.getName().equals("numberOfMonths"))
        {
            value = random.nextInt(12);
        }
        else if (propertyMeta.getName().equals("numberOfYears"))
        {
            value = random.nextInt(99);
        }
        else
        {
            value = random.nextInt(MAX_NUMERIC_VALUE);
        }
        entity.setProperty(propertyMeta.getPropertyPath(), value);
    }

    /**
     * Генерирует значение типа Float и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setFloatProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        Float value = random.nextInt(MAX_NUMERIC_VALUE) + random.nextFloat();
        entity.setProperty(propertyMeta.getPropertyPath(), value);
    }

    /**
     * Генерирует значение типа Double и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setDoubleProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        Double value = random.nextInt(MAX_NUMERIC_VALUE) + random.nextDouble();
        entity.setProperty(propertyMeta.getPropertyPath(), value);
    }

    /**
     * Генерирует значение типа Date и записывает его в переданную сушность.
     *
     * @param entity       сущность
     * @param propertyMeta заполняемое свойство
     */
    private static void setDateProperty(IEntity entity, IPropertyMeta propertyMeta)
    {
        Date value;
        if (entity.getClass().getTypeName().equals(ctrContractVersionClass)
                && (propertyMeta.getName().equals("archivingDate")
                || propertyMeta.getName().equals("activationDate")
                || propertyMeta.getName().equals("removalDate"))
                )
        {
            value = null;
        }


        else

        {
            int minDay;
            int maxDay;
            if (propertyMeta.getName().toLowerCase().contains(IdentityCard.P_REGISTRATION_PERIOD_TO.toLowerCase()))
            {
                int currentYear = Calendar.getInstance().get(Calendar.YEAR);
                minDay = (int) LocalDate.of(currentYear + 1, 1, 1).toEpochDay();
                maxDay = (int) LocalDate.of(currentYear + 2, 6, 1).toEpochDay();
            }
            else if (propertyMeta.getName().toLowerCase().contains("from"))
            {
                minDay = (int) LocalDate.of(2015, 1, 1).toEpochDay();
                maxDay = (int) LocalDate.of(2015, 6, 1).toEpochDay();
            }
            else if (propertyMeta.getName().toLowerCase().contains("to"))
            {
                minDay = (int) LocalDate.of(2015, 7, 1).toEpochDay();
                maxDay = (int) LocalDate.of(2015, 12, 1).toEpochDay();
            }
            else
            {
                minDay = (int) LocalDate.of(2005, 1, 1).toEpochDay();
                maxDay = (int) LocalDate.of(2016, 1, 1).toEpochDay();
            }
            long randomDay = minDay + random.nextInt(maxDay - minDay);
            LocalDate localDate = LocalDate.ofEpochDay(randomDay);
            value = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }

        entity.setProperty(propertyMeta.getPropertyPath(), value);

    }


    /**
     * Метод проверяет нужно ли заполнять поле сущности.
     * В обязательном порядке необходимо проинициализировать поля
     * базовой (корневой) сущности, и обязательные поля
     *
     * @param propertyMeta проверяемое поле
     * @param entity       проверяемая сущность
     * @param isBaseEntity явлется ли сущность корневой
     * @return true - поле должно быть заполненно, false - поле можно не заполнять.
     */

    private static boolean isNeedToFill(FieldPropertyMeta propertyMeta, boolean isBaseEntity, IEntity entity)
    {
        //не заполняем assignedQualification, чтобы избежать проблем с констрейнтами
        if (entity.getClass().getName().equals(educationLevelsHighSchoolClass)
                && propertyMeta.getName().equals("assignedQualification"))
            return false;

        if (entity.getClass().getName().contains(educationLevelsClass)
                && propertyMeta.getName().equals("eduProgramSubject"))
            return false;

        return propertyMeta.isRequired() || isBaseEntity;
    }


    /**
     * В связи с рядом ограничений некоторые вложенные сущности
     * надо генерировать, а не брать готовыми из базы.
     * Например при генерации персоны, для нее необходимо создать
     * контактную информацию, а не взять из базы первую попавшуюся.
     *
     * @param propertyMeta свойство сущности для которого выполняется проверка
     * @return true - данные для заполнения свойства должны быть сгенерированны,
     * false - можно взять готовые данные из базы
     */
    private static boolean isNeedGenerate(IEntity entity, IPropertyMeta propertyMeta)
    {
        //берем эти сущности из базы
        if (propertyMeta.getValueType().getName().equals(orgUnitTypePostRelationClass))
            return false;
        if (propertyMeta.getValueType().getName().equals(eppWorkPlanClass))
            return false;
        if (propertyMeta.getValueType().getName().equals(eduProgramClass))
            return false;

        if (entity.getClass().getName().equals(eduOwnerOrgUnitClass)
                && propertyMeta.getValueType().getName().equals(orgUnitClass))
            return true;
        if (propertyMeta.getValueType().getName().equals(orgUnitClass))
            return false;
        if (propertyMeta.getValueType().getName().equals(iCtrPriceElementClass))
            return false;

        //EduProgramSubject должна быть сгенерирована
        // (т.к. в некоторых случая нам каждый раз нужно новое направление, например, при генерации eduProgramSpecializationRoot)
        if (propertyMeta.getValueType().getName().equals(eduProgramSubjectClass))
            return true;
        if (entity.getClass().getName().equals(eduProgramQualificationClass)
                && propertyMeta.getValueType().getName().equals(eduProgramSubjectIndexClass))
            return true;
        if (propertyMeta.getValueType().getName().equals(educationLevelsClass))
            return true;
        if (entity.getClass().getName().equals(eduProgramSubjectQualificationClass)
                && propertyMeta.getValueType().getName().equals(eduProgramQualificationClass))
            return true;
        if (propertyMeta.getValueType().getName().equals("ru.tandemservice.uni.entity.catalog.DevelopGrid"))
            return true;
        //если среди полей сущносте есть "код", то скорее всего это справочник, и его можно найти в базе.
        return !EntityRuntime.getMeta(propertyMeta.getValueType()).getPropertyNames().contains("code");
    }


    /**
     * Метод получает класс сущности, и возвращает экземпляр сущности.
     * Если передан класс абстарктной сущности,
     * то создается объект наследника класса.
     *
     * @param clazz класс по которому создается объект
     * @return экземпляр IEntity
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static IEntity getNonAbstarctEntity(Class clazz) throws IllegalAccessException, InstantiationException
    {
        IEntity result;

        IEntityMeta classMeta = EntityRuntime.getMeta(clazz);
        if (classMeta.getEntityClass().equals(IPersistentBpmParticipant.class))
        {
            return null;
        }
        else if (classMeta.isAbstract())
        {
            if (classMeta.isInterface())
                result = getNonAbstarctEntity(classMeta.getImplementors().iterator().next().getEntityClass());
            else
            {
                if (classMeta.getEntityClass().equals(AddressBase.class))
                {
                    Iterator<IEntityMeta> addressIterator = classMeta.getChildren().iterator();
                    addressIterator.next();
                    result = getNonAbstarctEntity(addressIterator.next().getChildren().iterator().next().getEntityClass());
                }
                if (classMeta.getEntityClass().getName().equals(educationLevelsClass))
                {
                    return getNonAbstarctEntity(((IEntityMeta) classMeta.getChildren().toArray()[3]).getEntityClass());
                }
                else
                {
                    result = getNonAbstarctEntity(classMeta.getChildren().iterator().next().getEntityClass());
                }
            }
        }
        else
        {
            result = (IEntity) clazz.newInstance();
        }

        return result;
    }

    /**
     * Метод принимает на вход сущность для которой заполняется
     * ссылочное поле, метаинформацию о заполняемом поле и
     * список уже созданных сущносетй.
     * И если в списке есть подходящая для заполнения переданного
     * поля сущность (того же класса, который требуется для заполнения поля),
     * то возвращем ее.
     *
     * @param propertyMeta заполняемое поле.
     * @param entityList   список уже созданных сущностей.
     * @return возвращет ссылку на сущность, подходящую для заполнения поля.
     */
    private static IEntity getInnerEntityFromCreatedList(IRelationSubject propertyMeta, final List<IEntity> entityList)
    {
        Class propertyClass = propertyMeta.getRemoteEntity().getEntityClass();

        if (propertyMeta.getRemoteEntity().getEntityClass().equals(IPersistentBpmParticipant.class)
                || propertyMeta.getRemoteEntity().getEntityClass().equals(Principal.class)
                || propertyMeta.getContainer().getOwner().getName().equals("eduOwnerOrgUnit")
                || propertyClass.getName().equals(ctrContractVersionContractorClass)
                || propertyClass.getName().equals(contactorPersonClass))
        {
            return null;
        }
        else
        {
            for (IEntity createdEntity : entityList)
            {
                if (propertyClass.isInstance(createdEntity))
                {
                    return createdEntity;
                }
            }
        }

        return null;
    }


    /**
     * Возвращает из справочника обьект переданного класса.
     *
     * @param catalogEntityClass класс по которому генерируется объект за заполнения
     * @param entity             сущность для которой генерируются значения
     * @return возвращает случайную сущность из справочника
     */
    @SuppressWarnings("unchecked")
    private static IEntity getRandomCatalogEntity(Class catalogEntityClass, IEntity entity)
    {
        List<? extends IEntity> catalogList = null;
        if (catalogEntityClass.equals(EduLevel.class))
        {
            catalogList = DataAccessServices.dao().getList(EduLevel.class, EduLevel.P_LEVEL, true);
        }
        if (catalogEntityClass.getName().equals("ru.tandemservice.uni.entity.catalog.Term"))
        {
            catalogList = DataAccessServices.dao().getList(catalogEntityClass).subList(0,0);
        }
        if (catalogEntityClass.getName().equals(eppStateClass))
        {
            return DataAccessServices.dao().get(catalogEntityClass, ICatalogItem.CATALOG_ITEM_CODE, "1");
        }

        //пусть всегда будет РФ
        if (catalogEntityClass.equals(AddressCountry.class))
        {
            catalogList = DataAccessServices.dao().getList(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE);
            return catalogList.get(0);
        }
        //пусть всегда будет Москва
        if (catalogEntityClass.equals(AddressItem.class))
        {
            catalogList = DataAccessServices.dao().getList(AddressItem.class, AddressItem.title(), "Москва");
            return catalogList.get(0);
        }

        // вынесено в отдельный метод из-за большого числа констрейнтов
        if (entity.getClass().getName().toLowerCase().contains("eduprogramhigherprof"))
        {
            catalogList = getCustomizedGeneratedListForEduProgramProf(catalogEntityClass, entity);
        }

        if (entity.getClass().getName().equals("ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf") && catalogEntityClass.getName().equals(eduProgramQualificationClass)) {
            return null;
        }

        if (catalogList == null)
        {
            catalogList = DataAccessServices.dao().getList(catalogEntityClass);
        }
        //в текущей реализации всегда генерируется НПО для ОКСО, это надо учесть при поиске перечня НПО
        if (entity.getClass().getName().equalsIgnoreCase(eduProgramSubjectOksoClass)
                && catalogEntityClass.getName().equalsIgnoreCase(eduProgramSubjectIndexClass))
        {
            catalogList = getCustomizedEduProgramSubjectIndex(catalogEntityClass);
        }
        if (entity.getClass().getName().equalsIgnoreCase(eduProgramHigherProfClass)
                && catalogEntityClass.getName().equalsIgnoreCase(eduProgramKindClass))
        {
            //вид образовательной программы берем из перечня (необходимо для соблдения контсрейнтов)
            return (IEntity) ((IEntity) ((IEntity) entity.getProperty("programSubject")).getProperty("subjectIndex")).getProperty("programKind");
        }
        //если подходящее для заполнения значение есть в базе и нет никаких ограничений,
        // для того чтобы использовать его при генерации сущности, то используем его.
        if (!CollectionUtils.isEmpty(catalogList))
        {
            int catalogIdx = -1;
            while (catalogIdx < 0)
            {
                catalogIdx = random.nextInt(catalogList.size());
            }
            return catalogList.get(catalogIdx);
        }
        return null;
    }

    /**
     * Метод возвращает сущности, которые могут быть использованые при заполнений
     * EduProgramHigherProf (Образовательная программа ВПО), с учетом констрейнтов.
     *
     * @param catalogEntityClass класс по которому генерируется объект за заполнения
     * @param entity             сущность для которой генерируются значения
     * @return
     */
    @SuppressWarnings("unchecked")
    private static List<? extends IEntity> getCustomizedGeneratedListForEduProgramProf(Class catalogEntityClass, IEntity entity)
    {
        List<? extends IEntity> catalogList;

        if (catalogEntityClass.getName().toLowerCase().contains("eduprogramkind"))
        {
            try
            {
                Class specialization = Class.forName(eduProgramSpecializationClass);
                List<? extends IEntity> specializationList = DataAccessServices.dao().getList(specialization);
                catalogList = specializationList.stream()
                        .map(item -> (IEntity) ((IEntity) ((IEntity) item.getProperty("programSubject")).getProperty("subjectIndex")).getProperty("programKind"))
                        .filter(item -> (boolean) item.getProperty("higher"))
                        .collect(Collectors.toList());
                return catalogList;
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }

        }
        //чтобы в дальнейшем не создавать специализацию для выбранного направления,
        //находим направление для уже созданной специализации.
        //Вид образовательной программы перечня направления должен совпадать с видом ОП
        else if (catalogEntityClass.getName().toLowerCase().contains("eduprogramsubject"))
        {
            try
            {
                Class specialization = Class.forName(eduProgramSpecializationClass);
                List<? extends IEntity> specializationList = DataAccessServices.dao().getList(specialization);
                catalogList = specializationList.stream()
                        .map(item -> (IEntity) item.getProperty("programSubject")).collect(Collectors.toList());
                catalogList = catalogList.stream()
                        .filter(item -> (((IEntity) item.getProperty("subjectIndex")).getProperty("programKind").equals(entity.getProperty("kind"))))
                        .collect(Collectors.toList());
                return catalogList;
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }
        //ищем специализацию соответствующуюю направлению
        else if (catalogEntityClass.getName().toLowerCase().contains("eduprogramspecialization"))
        {
            catalogList = DataAccessServices.dao().getList(catalogEntityClass);
            catalogList = catalogList.stream()
                    .filter(item -> (item.getProperty("programSubject").equals(entity.getProperty("programSubject"))))
                    .collect(Collectors.toList());
            return catalogList;
        }

        else if (catalogEntityClass.getName().toLowerCase().contains("eduprogramqualification"))
        {
            catalogList = DataAccessServices.dao().getList(catalogEntityClass);
            catalogList = catalogList.stream()
                    .filter(item -> (item.getProperty("subjectIndex").equals(((IEntity) entity.getProperty("programSubject")).getProperty("subjectIndex"))))
                    .collect(Collectors.toList());
            return catalogList;
        }
        return null;
    }


    /**
     * Находит все перечни для ОКСО
     *
     * @param catalogEntityClass класс перечня НПО
     * @return возвращает сипсок перечней ОКСО
     */
    @SuppressWarnings("unchecked")
    private static List<? extends IEntity> getCustomizedEduProgramSubjectIndex(Class catalogEntityClass)
    {
        List<? extends IEntity> catalogList;
        catalogList = DataAccessServices.dao().getList(catalogEntityClass);

        catalogList = catalogList.stream()
                .filter(item -> (item.getProperty("code").toString().contains("2005.6")))
                .collect(Collectors.toList());
        return catalogList;
    }

}
