package ru.tandemservice.nsiclient.reactor.support;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;

import java.util.List;

@FunctionalInterface
public interface IResponseAction
{
    /**
     *
     * @param subSystemLog
     * @param list
     * @return
     * @throws CriticalException В случае сбоя
     */
    DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list) throws CriticalException;
}