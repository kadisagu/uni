/* $Id: NsiCollectorDaemon.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2015
 */
public class NsiCollectorDaemon extends SharedBaseDao implements INsiCollectorDaemon
{
    public static final String NSI_SERVICE_AUTOSYNC = "nsi.service.autosync";

    private static boolean sync_locked;

    public static final int DAEMON_ITERATION_TIME = 1;
    private static final Set<Long> ADDED_UPDATED_ENITYTY_IDS = Collections.synchronizedSet(new HashSet<>());
    private static final Set<Long> DELETED_ENTITY_IDS = Collections.synchronizedSet(new HashSet<>());
    private static final Set<Long> ENTITY_IDS_TO_IGNORE = Collections.synchronizedSet(new HashSet<>());
    //public static final Object lock = new Object();

    @Override
    public void registerChangedEntities(Long... ids)
    {
        for (Long id : ids)
            if (ENTITY_IDS_TO_IGNORE.contains(id))
                ENTITY_IDS_TO_IGNORE.remove(id);
            else
                ADDED_UPDATED_ENITYTY_IDS.add(id);
    }

    @Override
    public void registerDeletedEntities(Long... ids)
    {
        for (Long id : ids)
            if (ENTITY_IDS_TO_IGNORE.contains(id))
                ENTITY_IDS_TO_IGNORE.remove(id);
            else
                DELETED_ENTITY_IDS.add(id);
    }

    @Override
    public void registerIgnoredEntities(Long... ids)
    {
        ENTITY_IDS_TO_IGNORE.addAll(Arrays.asList(ids));
    }

    @Override
    public void removeEntities(Long... ids)
    {
        ADDED_UPDATED_ENITYTY_IDS.removeAll(Arrays.asList(ids));
        DELETED_ENTITY_IDS.removeAll(Arrays.asList(ids));
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(NsiCollectorDaemon.class.getName(), DAEMON_ITERATION_TIME, INsiCollectorDaemon.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли автосинхронизация. Если пропертя отсутствует в конфигах, то прерываем выполнение демона
            String property = ApplicationRuntime.getProperty(NSI_SERVICE_AUTOSYNC);
            if (null == property) return;

            if (!Boolean.parseBoolean(property)) return;

            if (ADDED_UPDATED_ENITYTY_IDS.isEmpty() && DELETED_ENTITY_IDS.isEmpty()) return;

            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked) return;

            sync_locked = true;

            // отключаем логирование в базу и прочие системные штуки
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

            try
            {
                List<Long> updateIdsList = new ArrayList<>(ADDED_UPDATED_ENITYTY_IDS);
                ADDED_UPDATED_ENITYTY_IDS.removeAll(updateIdsList);
                List<Long> deleteIdsList = new ArrayList<>(DELETED_ENTITY_IDS);
                DELETED_ENTITY_IDS.removeAll(deleteIdsList);

                Map<String, IDatagramObject> objectsToUpdate = new HashMap<>();
                Map<String, IDatagramObject> objectsToDelete = new HashMap<>();
                Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactorMap = ReactorHolder.getReactorMap();
                for (Map.Entry<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> entry : reactorMap.entrySet())
                {
                    INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = entry.getValue();
                    NsiCatalogType catalogType = DataAccessServices.dao().get(NsiCatalogType.class, NsiCatalogType.code(), reactor.getCatalogType());
                    // отправляем изменения только после синхронизации
                    if (catalogType.isAutoDeliveryChangesAllowed() && catalogType.isReadWriteAllowed())
                    {
                        if (!updateIdsList.isEmpty())
                            objectsToUpdate.putAll(reactor.createDatagramObjectMap(updateIdsList, false));
                        if (!deleteIdsList.isEmpty())
                            objectsToDelete.putAll(reactor.createDatagramObjectMap(deleteIdsList, true));
                    }
                }

                if (!objectsToUpdate.isEmpty())
                {
                    NsiSyncManager.instance().dao().sendToNsiQueue(objectsToUpdate, NsiUtils.OPERATION_TYPE_UPDATE);
                }
                if (!objectsToDelete.isEmpty())
                {
                    NsiSyncManager.instance().dao().sendToNsiQueue(objectsToDelete, NsiUtils.OPERATION_TYPE_DELETE);
                }
                // Дёргаем демон, занимающийся отправкой пакетов в НСИ
                NsiDeliveryDaemon.DAEMON.wakeUpDaemon();
            } catch (Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                logger.warn(t.getMessage(), t);
            } finally
            {
                // включаем штуки и логирование обратно
                Debug.resumeLogging();
                eventLock.release();
            }

            sync_locked = false;
        }

    };
}
