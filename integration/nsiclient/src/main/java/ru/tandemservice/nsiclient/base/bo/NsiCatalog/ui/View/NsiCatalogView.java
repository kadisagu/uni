package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementsTab.NsiCatalogElementsTab;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.List.NsiPackageList;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiCatalogView extends BusinessComponentManager
{
    public static final String CODE = "code";
    public static final String SELECTED_TAB = "selectedTab";

    public static final String TAB_PANEL = "tabPanel";
    public static final String ELEMENTS_TAB = "elementsTab";
    public static final String PACKAGES_TAB = "packagesTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(ELEMENTS_TAB, NsiCatalogElementsTab.class).parameters("ui:tabParameter"))
                .addTab(componentTab(PACKAGES_TAB, NsiPackageList.class).parameters("ui:logListTabParameter"))
                .create();
    }
}
