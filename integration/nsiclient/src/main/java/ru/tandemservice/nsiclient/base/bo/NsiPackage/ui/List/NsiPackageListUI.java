package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.logic.NsiPackageDSHandler;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.support.NsiPackageWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send.INsiSubSystemSendAction;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send.NsiSubSystemSend;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.reactor.support.ProcessingException;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id"),
        @Bind(key = NsiPackageList.CATALOG_CODE, binding = "code")
})
public class NsiPackageListUI extends UIPresenter
{
    private Long _id;
    private String _code;
    private NsiCatalogType _catalog;
    private String _mode;
    private boolean _filterApplied;

    private static final String SETTINGS_FILTER_VISIBLE = "filtersVisible";

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getCode() {
        return _code;
    }

    public void setCode(String code) {
        _code = code;
    }

    @Override
    public void onComponentActivate()
    {
        _mode = "all";
        if(_code != null)
        {
            _catalog = DataAccessServices.dao().getByCode(NsiCatalogType.class, _code);
            _mode = "catalog";
        }
        if(_id != null)
            _mode = "element";
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NsiPackageList.OPERATION_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true,
                    NsiPackageDSHandler.CREATE_DATE_FROM,
                    NsiPackageDSHandler.CREATE_DATE_TO,
                    NsiPackageDSHandler.MESSAGE_GUID,
                    NsiPackageDSHandler.DIRECTION_TYPE,
                    NsiPackageDSHandler.OPERATION_TYPE,
                    NsiPackageDSHandler.MESSAGE_STATUS,
                    NsiPackageDSHandler.ENTITY_GUID,
                    NsiPackageDSHandler.ANY_XML_BODY_GUID,
                    NsiPackageDSHandler.CATALOG_ID,
                    NsiPackageDSHandler.ERROR,
                    NsiPackageDSHandler.WARNING));

            if(_id != null) dataSource.put(NsiPackageDSHandler.ENTITY_ID, _id);
            if(_catalog != null) dataSource.put(NsiPackageDSHandler.CATALOG_ID, _catalog.getId());
        }
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        for(String filterName : NsiPackageDSHandler.FILTERS_SET) _uiSettings.remove(filterName);
        getSettings().save();
    }

    public void onClickShowHideFilters()
    {
        Boolean filtersVisible = getSettings().get(SETTINGS_FILTER_VISIBLE, Boolean.class);
        if(filtersVisible == null) filtersVisible = false;
        getSettings().set(SETTINGS_FILTER_VISIBLE, !filtersVisible);
        getSettings().save();
    }

    public void onSendMessageAgain()
    {
        final NsiPackageWrapper wrapper = getConfig().getDataSource(NsiPackageList.OPERATION_LOG_DS).getRecordById(getListenerParameterAsLong());
        final List<NsiSubSystemLogWrapper> wrapperList = NsiSyncManager.instance().dao().getSubSystemLogs(wrapper.getPack());

        _uiActivation.asRegionDialog(NsiSubSystemSend.class)
                .parameter(NsiSubSystemSend.SELECT_ACTION, new INsiSubSystemSendAction()
                {
                    @Override
                    public void selectSubSystems(List<NsiSubSystemLogWrapper> wrappers)
                    {
                        // отключаем логирование в базу и прочие системные штуки
                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                        try
                        {
                            NsiSyncManager.instance().dao().send(wrapper.getPack(), wrappers);
                        } catch (ProcessingException e) {
                            throw new ApplicationException(e.getMessage(), e);
                        } finally
                        {
                            // включаем штуки и логирование обратно
                            Debug.resumeLogging();
                            eventLock.release();
                        }
                    }

                    @Override
                    public List<NsiSubSystemLogWrapper> getSubSystemLogWrappers() {
                        return wrapperList;
                    }
                })
                .activate();
    }

    public void onGetMessagePack()
    {
        NsiPackageWrapper wrapper = getConfig().getDataSource(NsiPackageList.OPERATION_LOG_DS).getRecordById(getListenerParameterAsLong());
        String text = NsiUtils.createPackFile(wrapper.getPack());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt").fileName(wrapper.getPack().getId() + ".txt").document(text.getBytes()), true);
    }

    @Override
    public String getSettingsKey()
    {
        return _mode + '.' + super.getSettingsKey();
    }

    public String getListSettingsKey()
    {
        return _mode + '.' + super.getSettingsKey() + ".list";
    }

    public boolean isShowCatalogFilter()
    {
        return _code == null && _id == null;
    }

    public boolean isShowEntityFilter()
    {
        return _id == null;
    }

    public boolean isSendPackageDisabled()
    {
        NsiPackageWrapper wrapper = getConfig().getDataSource(NsiPackageList.OPERATION_LOG_DS).getCurrent();
        return NsiSyncManager.Operations.retrieve.name().equals(wrapper.getPack().getType());
    }
}