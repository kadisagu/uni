/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiTests.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.testng.ITestNGMethod;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 18.05.2015
 */
public class TestWrapper extends DataWrapper
{
    private String _title;
    private XmlSuite _suite;
    private XmlTest _test;

    public XmlClass getXmlClass() {
        return _xmlClass;
    }

    private XmlClass _xmlClass;
    private String _error;
    private ITestNGMethod _method;
    private String _time;
    private List<TestWrapper> _children = new ArrayList<>();

    public List<TestError> getErrors() {
        return _errors;
    }

    private List<TestError> _errors = new ArrayList<>();

    public String getTime() {
        return _time;
    }

    public void setTime(String time) {
        _time = time;
    }

    public ITestNGMethod getMethod() {
        return _method;
    }

    public String getError() {
        return _error;
    }

    public void setError(String error) {
        _error = error;
    }

    public TestWrapper(long id, String title, XmlSuite suite, XmlTest test, XmlClass xmlClass, ITestNGMethod method)
    {
        super(id, title);
        _title = title;
        _suite = suite;
        _test = test;
        _xmlClass = xmlClass;
        _method = method;
    }

    public String getTitle()
    {
        return _title;
    }

    public XmlSuite getSuite()
    {
        return _suite;
    }

    public XmlTest getTest()
    {
        return _test;
    }

    public List<TestWrapper> getChildren()
    {
        return _children;
    }

    public void clearErrors() {
        _error = null;
        _time = null;
        _errors = new ArrayList<>();
        for(int i = 0; i < _children.size();)
        {
            TestWrapper testWrapper = _children.get(i);
            if(testWrapper.getMethod() != null)
                _children.remove(i);
            else {
                i++;
                testWrapper.clearErrors();
            }
        }
    }
}
