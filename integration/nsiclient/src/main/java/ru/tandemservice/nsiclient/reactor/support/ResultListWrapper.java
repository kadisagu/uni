/* $Id: ResultListWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
public class ResultListWrapper
{
    private String _warning;
    private List<IDatagramObject> _list;

    public List<IDatagramObject> getList() {
        return _list;
    }

    public void setList(List<IDatagramObject> list) {
        _list = list;
    }

    public String getWarning()
    {
        return _warning;
    }

    public void setWarning(String warning)
    {
        _warning = warning;
    }

    public ResultListWrapper(String warning, List<IDatagramObject> list)
    {
        _warning = warning;
        _list = list;
    }
}