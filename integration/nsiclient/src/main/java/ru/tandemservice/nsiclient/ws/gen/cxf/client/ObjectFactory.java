
package ru.tandemservice.nsiclient.ws.gen.cxf.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.nsi.nsiclient.common.ws.server.gen package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CommitResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "commitResponse");
    private final static QName _InsertResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "insertResponse");
    private final static QName _RetrieveResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "retrieveResponse");
    private final static QName _RouteResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "routeResponse");
    private final static QName _DeleteRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "deleteRequest");
    private final static QName _CommitRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "commitRequest");
    private final static QName _InitializeRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "initializeRequest");
    private final static QName _RetrieveRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "retrieveRequest");
    private final static QName _DeliverResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "deliverResponse");
    private final static QName _UpdateRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "updateRequest");
    private final static QName _InsertRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "insertRequest");
    private final static QName _RouteRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "routeRequest");
    private final static QName _UpdateResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "updateResponse");
    private final static QName _InitializeResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "initializeResponse");
    private final static QName _DeliverRequest_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "deliverRequest");
    private final static QName _DeleteResponse_QNAME = new QName("http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", "deleteResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.nsi.nsiclient.common.ws.server.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.RouteRequestType }
     *
     */
    public RouteRequestType createRouteRequestType() {
        return new RouteRequestType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType }
     *
     */
    public ServiceResponseType createServiceResponseType() {
        return new ServiceResponseType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType }
     *
     */
    public ServiceRequestType createServiceRequestType() {
        return new ServiceRequestType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType2 }
     *
     */
    public ServiceResponseType2 createServiceResponseType2() {
        return new ServiceResponseType2();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.CommitRequestType }
     *
     */
    public CommitRequestType createCommitRequestType() {
        return new CommitRequestType();
    }

    /**
     * Create an instance of {@link DeliverRequestType }
     *
     */
    public DeliverRequestType createDeliverRequestType() {
        return new DeliverRequestType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.RoutingHeaderType }
     *
     */
    public RoutingHeaderType createRoutingHeaderType() {
        return new RoutingHeaderType();
    }

    /**
     * Create an instance of {@link DeliverDataType }
     *
     */
    public DeliverDataType createDeliverDataType() {
        return new DeliverDataType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.CommitDataType }
     *
     */
    public CommitDataType createCommitDataType() {
        return new CommitDataType();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.RouteRequestType.Identifiers }
     *
     */
    public RouteRequestType.Identifiers createRouteRequestTypeIdentifiers() {
        return new RouteRequestType.Identifiers();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType.Datagram }
     *
     */
    public ServiceResponseType.Datagram createServiceResponseTypeDatagram() {
        return new ServiceResponseType.Datagram();
    }

    /**
     * Create an instance of {@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType.Datagram }
     *
     */
    public ServiceRequestType.Datagram createServiceRequestTypeDatagram() {
        return new ServiceRequestType.Datagram();
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType2 }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "commitResponse")
    public JAXBElement<ServiceResponseType2> createCommitResponse(ServiceResponseType2 value) {
        return new JAXBElement<ServiceResponseType2>(_CommitResponse_QNAME, ServiceResponseType2 .class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "insertResponse")
    public JAXBElement<ServiceResponseType> createInsertResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_InsertResponse_QNAME, ServiceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "retrieveResponse")
    public JAXBElement<ServiceResponseType> createRetrieveResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_RetrieveResponse_QNAME, ServiceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType2 }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "routeResponse")
    public JAXBElement<ServiceResponseType2> createRouteResponse(ServiceResponseType2 value) {
        return new JAXBElement<ServiceResponseType2>(_RouteResponse_QNAME, ServiceResponseType2 .class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "deleteRequest")
    public JAXBElement<ServiceRequestType> createDeleteRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_DeleteRequest_QNAME, ServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.CommitRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "commitRequest")
    public JAXBElement<CommitRequestType> createCommitRequest(CommitRequestType value) {
        return new JAXBElement<CommitRequestType>(_CommitRequest_QNAME, CommitRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "initializeRequest")
    public JAXBElement<ServiceRequestType> createInitializeRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_InitializeRequest_QNAME, ServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "retrieveRequest")
    public JAXBElement<ServiceRequestType> createRetrieveRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_RetrieveRequest_QNAME, ServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType2 }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "deliverResponse")
    public JAXBElement<ServiceResponseType2> createDeliverResponse(ServiceResponseType2 value) {
        return new JAXBElement<ServiceResponseType2>(_DeliverResponse_QNAME, ServiceResponseType2 .class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "updateRequest")
    public JAXBElement<ServiceRequestType> createUpdateRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_UpdateRequest_QNAME, ServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "insertRequest")
    public JAXBElement<ServiceRequestType> createInsertRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_InsertRequest_QNAME, ServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.RouteRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "routeRequest")
    public JAXBElement<RouteRequestType> createRouteRequest(RouteRequestType value) {
        return new JAXBElement<RouteRequestType>(_RouteRequest_QNAME, RouteRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "updateResponse")
    public JAXBElement<ServiceResponseType> createUpdateResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_UpdateResponse_QNAME, ServiceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "initializeResponse")
    public JAXBElement<ServiceResponseType> createInitializeResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_InitializeResponse_QNAME, ServiceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link DeliverRequestType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "deliverRequest")
    public JAXBElement<DeliverRequestType> createDeliverRequest(DeliverRequestType value) {
        return new JAXBElement<DeliverRequestType>(_DeliverRequest_QNAME, DeliverRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ru.tandemservice.nsiclient.ws.gen.cxf.client.ServiceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service", name = "deleteResponse")
    public JAXBElement<ServiceResponseType> createDeleteResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_DeleteResponse_QNAME, ServiceResponseType.class, null, value);
    }

}
