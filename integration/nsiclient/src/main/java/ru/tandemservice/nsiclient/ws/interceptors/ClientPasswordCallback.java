/* $Id: ClientPasswordCallback.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.ws.interceptors;

import org.apache.hivemind.ApplicationRuntimeException;
import org.apache.ws.security.WSPasswordCallback;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.AuthSettings.NsiSettingsAuthSettingsUI;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

/**
 * @author Andrey Avetisov
 * @since 03.07.2015
 */
public class ClientPasswordCallback implements CallbackHandler
{
    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
    {
        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];

        String pass = ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_PASSWORD);
        if (null == pass)
        {
            throw new ApplicationRuntimeException("Can't fount client password");
        }
        // set the password for our message.
        pc.setPassword(pass);
    }
}
