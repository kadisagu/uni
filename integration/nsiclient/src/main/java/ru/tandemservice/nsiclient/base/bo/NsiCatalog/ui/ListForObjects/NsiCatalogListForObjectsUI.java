package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ListForObjects;

import org.apache.tapestry.IRequestCycle;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.IScriptAction;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiPackFormingQueue;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ProcessingException;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.utils.Synchronizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
public class NsiCatalogListForObjectsUI extends UIPresenter
{
    private boolean _syncLinkVisible;
    private final DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yyyy HH:mm:ss");

    @Override
    public void onComponentRefresh()
    {
        _syncLinkVisible = Synchronizer.INSTANCE.getThread() != null;
    }

    private List<NsiCatalogType> getSelectedCatalogs()
    {
        Collection<IEntity> selected = ((BaseSearchListDataSource) getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS)).getOptionColumnSelectedObjects("select");
        if (selected.isEmpty()) return null;

        List<NsiCatalogType> selectedCatalogs = new ArrayList<>(selected.size());
        List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> sortedReactorList = ReactorHolder.getSortedReactorList();
        for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : sortedReactorList)
        {
            NsiCatalogType catalogType = DataAccessServices.dao().get(NsiCatalogType.class, NsiCatalogType.code(), reactor.getCatalogType());
            if (selected.contains(catalogType)) selectedCatalogs.add(catalogType);
        }
        return selectedCatalogs;
    }

    public void onSendToNSIAll()
    {
        List<NsiCatalogType> selectedCatalogs = getSelectedCatalogs();
        if (selectedCatalogs == null || selectedCatalogs.isEmpty()) return;

        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setMaxValue(selectedCatalogs.size());

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; !_cancelled && i < selectedCatalogs.size(); i++)
                    {
                        NsiCatalogType catalogType = selectedCatalogs.get(i);
                        state.setCurrentValue(i);
                        try
                        {
                            message.append("Каталог «" + catalogType.getTitle() + "» " + Synchronizer.INSTANCE.sendToNSI(catalogType, false));
                        } catch (ProcessingException e)
                        {
                            Debug.exception(e);
                            Debug.saveDebug();
                            message.append("Ошибка синхронизации каталога " + catalogType.getTitle() + ": " + e.getMessage());
                        }
                    }

                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталогов", process, ProcessDisplayMode.custom);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onSendToNSIConditionless()
    {
        List<NsiCatalogType> selectedCatalogs = getSelectedCatalogs();
        if (selectedCatalogs == null || selectedCatalogs.isEmpty())
            return;
        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setMaxValue(selectedCatalogs.size());

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; !_cancelled && i < selectedCatalogs.size(); i++)
                    {
                        NsiCatalogType catalogType = selectedCatalogs.get(i);
                        state.setCurrentValue(i);
                        try
                        {
                            message.append("Каталог «" + catalogType.getTitle() + "» " + Synchronizer.INSTANCE.sendFullCatalogToNSI(catalogType, false));
                        } catch (ProcessingException e)
                        {
                            Debug.exception(e);
                            Debug.saveDebug();
                            message.append("Ошибка синхронизации каталога " + catalogType.getTitle() + ": " + e.getMessage());
                        }
                    }

                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталогов", process, ProcessDisplayMode.custom);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onGetFromNSIAll()
    {
        List<NsiCatalogType> selectedCatalogs = getSelectedCatalogs();
        if (selectedCatalogs == null || selectedCatalogs.isEmpty()) return;

        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setMaxValue(selectedCatalogs.size());

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; !_cancelled && i < selectedCatalogs.size(); i++)
                    {
                        NsiCatalogType catalogType = selectedCatalogs.get(i);
                        state.setCurrentValue(i);
                        try
                        {
                            message.append("Каталог «" + catalogType.getTitle() + "» " + Synchronizer.INSTANCE.getFromNSI(catalogType));
                        } catch (ProcessingException e)
                        {
                            Debug.exception(e);
                            Debug.saveDebug();
                            message.append("Ошибка синхронизации каталога " + catalogType.getTitle() + ": " + e.getMessage());
                        }
                    }

                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталогов", process, ProcessDisplayMode.custom);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onRetrieveAllFromNsiAll()
    {
        List<NsiCatalogType> selectedCatalogs = getSelectedCatalogs();
        if (selectedCatalogs == null || selectedCatalogs.isEmpty()) return;

        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setMaxValue(selectedCatalogs.size());

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; !_cancelled && i < selectedCatalogs.size(); i++)
                    {
                        NsiCatalogType catalogType = selectedCatalogs.get(i);
                        state.setCurrentValue(i);
                        try
                        {
                            message.append("Каталог «" + catalogType.getTitle() + "» " + Synchronizer.INSTANCE.retrieveAllFromNsi(catalogType));
                        } catch (ProcessingException e)
                        {
                            Debug.exception(e);
                            Debug.saveDebug();
                            message.append("Ошибка получения данных каталога " + catalogType.getTitle() + ": " + e.getMessage());
                        }
                    }

                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Получение данных для каталогов", process, ProcessDisplayMode.custom);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onSendNewToNSIAll()
    {
        List<NsiCatalogType> selectedCatalogs = getSelectedCatalogs();
        if (selectedCatalogs == null || selectedCatalogs.isEmpty()) return;

        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setMaxValue(selectedCatalogs.size());

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; !_cancelled && i < selectedCatalogs.size(); i++)
                    {
                        NsiCatalogType catalogType = selectedCatalogs.get(i);
                        state.setCurrentValue(i);
                        try
                        {
                            message.append("Каталог «" + catalogType.getTitle() + "» " + Synchronizer.INSTANCE.sendNewToNSI(catalogType));
                        } catch (ProcessingException e)
                        {
                            Debug.exception(e);
                            Debug.saveDebug();
                            message.append("Ошибка отправки новых данных каталога " + catalogType.getTitle() + ": " + e.getMessage());
                        }
                    }

                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Отправка новых данных каталогов", process, ProcessDisplayMode.custom);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onGet()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        IBackgroundProcess process = new IBackgroundProcess()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    String message;
                    try
                    {
                        message = Synchronizer.INSTANCE.getFromNSI(catalog);
                    } catch (ProcessingException e)
                    {
                        e.printStackTrace();
                        throw new ApplicationException(e.getMessage(), e);
                    }
                    return new ProcessResult(message);
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталога «" + catalog.getTitle() + '»', process, ProcessDisplayMode.unknown);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onSend()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        IBackgroundProcess process = new IBackgroundProcess()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    String message;
                    try
                    {
                        message = Synchronizer.INSTANCE.sendToNSI(catalog, true);
                    } catch (ProcessingException e)
                    {
                        throw new ApplicationException(e.getMessage(), e);
                    }
                    return new ProcessResult(message);
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталога «" + catalog.getTitle() + '»', process, ProcessDisplayMode.unknown);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onRetrieveAllFromNsi()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        IBackgroundProcess process = new IBackgroundProcess()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    String message;
                    try
                    {
                        message = Synchronizer.INSTANCE.retrieveAllFromNsi(catalog);
                    } catch (ProcessingException e)
                    {
                        throw new ApplicationException(e.getMessage(), e);
                    }
                    return new ProcessResult(message);
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталога «" + catalog.getTitle() + '»', process, ProcessDisplayMode.unknown);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onSendNewToNSI()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        IBackgroundProcess process = new IBackgroundProcess()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    String message;
                    try
                    {
                        message = Synchronizer.INSTANCE.sendNewToNSI(catalog);
                    } catch (ProcessingException e)
                    {
                        throw new ApplicationException(e.getMessage(), e);
                    }
                    return new ProcessResult(message);
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                Synchronizer.INSTANCE.cancel();
            }
        };

        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталога «" + catalog.getTitle() + '»', process, ProcessDisplayMode.unknown);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public void onShowSync()
    {
        if (Synchronizer.INSTANCE.getThread() == null)
            TapSupportUtils.addInitScript(new IScriptAction()
            {
                @Override
                public String getScript(IRequestCycle cycle)
                {
                    return "alert(\"Нет незавершенной синхронизации\")";
                }
            });
        else
            BusinessComponentUtils.runProcess(getSynchronizer().getThread());
        // onCancelSync() Synchronizer.INSTANCE.cancel();
    }

    public void onChangeReadAllowed()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        catalog.setReadAllowed(!catalog.isReadAllowed());
        DataAccessServices.dao().update(catalog);
    }

    public void onChangeReadWriteAllowed()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        catalog.setReadWriteAllowed(!catalog.isReadWriteAllowed());
        DataAccessServices.dao().update(catalog);
    }

    public void onChangeAutoDeliveryChangesAllowed()
    {
        NsiCatalogType catalog = getConfig().getDataSource(NsiCatalogListForObjects.CATALOG_LIST_DS).getRecordById(getListenerParameterAsLong());
        catalog.setAutoDeliveryChangesAllowed(!catalog.isAutoDeliveryChangesAllowed());
        DataAccessServices.dao().update(catalog);
    }

    public void onResumeSync()
    {
        NsiCatalogType catalog1 = null;
        List<NsiPackFormingQueue> queue = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(NsiPackFormingQueue.class, "e").column(DQLExpressions.property("e")).top(1));
        if (!queue.isEmpty())
        {
            IEntityMeta meta = EntityRuntime.getMeta(queue.get(0).getEntityId());
            if (null != meta)
            {
                List<NsiCatalogType> catalogs = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(NsiCatalogType.class, "e").column(DQLExpressions.property("e"))
                        .where(DQLExpressions.eq(DQLExpressions.property(NsiCatalogType.nsiCode().fromAlias("e")), DQLExpressions.value(meta.getClassName()))));
                if (!catalogs.isEmpty()) catalog1 = catalogs.get(0);
            }
        }

        final NsiCatalogType catalog = catalog1;

        List<NsiCatalogType> selectedCatalogs = getSelectedCatalogs();
        if (null == catalog) return;

        IBackgroundProcess process = new IBackgroundProcess()
        {
            private boolean _cancelled;

            @Override
            public ProcessResult run(ProcessState state)
            {
                state.setMaxValue(1);

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    StringBuilder message = new StringBuilder();
                    state.setCurrentValue(1);
                    try
                    {
                        message.append("Каталог «" + catalog.getTitle() + "» " + Synchronizer.INSTANCE.sendFullCatalogToNSI(catalog, true));
                    } catch (ProcessingException e)
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                        message.append("Ошибка синхронизации каталога " + catalog.getTitle() + ": " + e.getMessage());
                    }

                    return new ProcessResult(message.toString());
                } catch (Throwable e)
                {
                    if (!(e instanceof ApplicationException))
                    {
                        Debug.exception(e);
                        Debug.saveDebug();
                    }
                    return new ProcessResult(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY) + ": " + e.getMessage(), true);
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                    Synchronizer.INSTANCE.finish();
                }
            }

            @Override
            public void cancel()
            {
                _cancelled = Synchronizer.INSTANCE.cancel();
            }
        };
        BackgroundProcessThread thread = new BackgroundProcessThread("Синхронизация каталогов", process, ProcessDisplayMode.custom);
        Synchronizer.INSTANCE.runProcess(thread);
    }

    public boolean isResumeSyncLinkVisible()
    {
        int queueCount = DataAccessServices.dao().getCount(new DQLSelectBuilder().fromEntity(NsiPackFormingQueue.class, "e").column(DQLExpressions.property("e")));
        return Synchronizer.INSTANCE.getThread() == null && queueCount > 0;
    }

    public String getResumeCatalogTitle()
    {
        List<NsiPackFormingQueue> queue = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(NsiPackFormingQueue.class, "e").column(DQLExpressions.property("e")).top(1));
        if (!queue.isEmpty())
        {
            IEntityMeta meta = EntityRuntime.getMeta(queue.get(0).getEntityId());
            if (null != meta)
            {
                List<String> catalogTitle = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(NsiCatalogType.class, "e").column(DQLExpressions.property(NsiCatalogType.title().fromAlias("e")))
                        .where(DQLExpressions.eq(DQLExpressions.property(NsiCatalogType.nsiCode().fromAlias("e")), DQLExpressions.value(meta.getClassName()))));
                if (!catalogTitle.isEmpty()) return catalogTitle.get(0);
            }
        }
        return null;
    }

    public String getStartDateStr()
    {
        return DATE_FORMATTER.format(getSynchronizer().getStartDate());
    }

    public String getFinishDateStr()
    {
        return DATE_FORMATTER.format(getSynchronizer().getFinishDate());
    }

    public Synchronizer getSynchronizer()
    {
        return Synchronizer.INSTANCE;
    }
}
