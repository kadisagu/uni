package ru.tandemservice.nsiclient.reactor.support;

/**
 * Created by andrey on 4/21/15.
 */
public interface INsiSettings {
    int[] getTimeIntervals();

    String getNsiAddress();

    String getSystemCode();

    int getPackageSize();

    boolean isAsync();
}
