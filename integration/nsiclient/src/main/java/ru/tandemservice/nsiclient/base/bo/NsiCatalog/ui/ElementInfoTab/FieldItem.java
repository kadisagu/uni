/* $Id: FieldItem.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementInfoTab;

import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 13.03.2015
 */
public class FieldItem
{
    private String _title;
    private String _value;
    private Map<String, Object> _parameters;
    private boolean _link;

    public FieldItem(String title, String value, Map<String, Object> parameters)
    {
        _title = title;
        _value = value;
        _parameters = parameters;
        _link = _parameters != null;
    }

    public String getTitle()
    {
        return _title;
    }

    public String getValue()
    {
        return _value;
    }

    public Map<String, Object> getParameters()
    {
        return _parameters;
    }

    public boolean isLink()
    {
        return _link;
    }
}