/* $Id: ResultWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

/**
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
public class ResultWrapper<T>
{
    private String _warning;
    private T _result;

    public T getResult() {
        return _result;
    }
    public String getWarning()
    {
        return _warning;
    }

    public ResultWrapper(String warning, T result)
    {
        _warning = warning;
        _result = result;
    }
}