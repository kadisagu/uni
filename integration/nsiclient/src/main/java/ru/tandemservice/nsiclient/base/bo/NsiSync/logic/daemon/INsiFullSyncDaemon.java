/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.process.BackgroundProcessThread;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Dmitry Seleznev
 * @since 27.10.2016
 */
public interface INsiFullSyncDaemon<T extends IEntity, V extends IDatagramObject>
{
    final String GLOBAL_DAEMON_LOCK = INsiFullSyncDaemon.class.getName() + ".global-lock";
    final SpringBeanCache<INsiFullSyncDaemon> instance = new SpringBeanCache<>(INsiFullSyncDaemon.class.getName());

    void doInit(BackgroundProcessThread thread, INsiEntityReactor<T, V> reactor, AtomicInteger packagesAll, String prefix, boolean resume);

    void doSendToNsiItems(List<CoreCollectionUtils.Pair<T, NsiEntity>> entityList);

    void createCatalogIdStackForPackSend(List<Long> entityIdList);

    void deleteSentFromQueue(List<Long> queueIdList);

    List<Object[]> getObjectsPortion();

    void doInterruptFullSync();

    List<Object[]> doEnsureGuidExistsOrCreateIt(List<Object[]> srcItems);
}