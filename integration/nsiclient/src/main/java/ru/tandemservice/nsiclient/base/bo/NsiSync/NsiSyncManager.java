package ru.tandemservice.nsiclient.base.bo.NsiSync;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.*;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSyncManager extends BusinessObjectManager {
    @Bean
    public INsiSyncDAO dao()
    {
        return new NsiSyncDAO();
    }

    @Bean
    public INsiSyncOperatorDAO operatorDAO()
    {
        return new NsiSyncOperatorDAO();
    }

    @Bean
    public IReactorDAO reactorDAO()
    {
        return new ReactorDAO();
    }

    public static NsiSyncManager instance()
    {
        return instance(NsiSyncManager.class);
    }

    @Bean
    public EntityComboDataSourceHandler catalogTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), NsiCatalogType.class);
    }

    public static final DateFormatter DATE_FORMATTER_WITH_TIME_MILLISEC = new DateFormatter("dd.MM.yyyy HH:mm:ss.SSS");

    public static class DirectionFormatter implements IFormatter<Boolean> {
        public static final DirectionFormatter INSTANCE = new DirectionFormatter();

        @Override
        public String format(Boolean source) {
            return Boolean.TRUE.equals(source) ? "Входящее" : "Исходящее";
        }
    }

    public static final Long DIRECTION_IN = 0L;
    public static final Long DIRECTION_OUT = 1L;

    @Bean
    public ItemListExtPoint<DataWrapper> directionExtPoint()
    {
        return itemList(DataWrapper.class).
                add(DIRECTION_IN.toString(), new DataWrapper(DIRECTION_IN, "Входящее")).
                add(DIRECTION_OUT.toString(), new DataWrapper(DIRECTION_OUT, "Исходящее")).
                create();
    }

    public enum Operations {
        retrieve,
        insert,
        update,
        delete
    }

    @Bean
    public ItemListExtPoint<DataWrapper> operationTypeExtPoint()
    {
        return itemList(DataWrapper.class).
                add(Operations.retrieve.name(), new DataWrapper(Operations.retrieve.ordinal(), "retrieve")).
                add(Operations.insert.name(), new DataWrapper(Operations.insert.ordinal(), "insert")).
                add(Operations.update.name(), new DataWrapper(Operations.update.ordinal(), "update")).
                add(Operations.delete.name(), new DataWrapper(Operations.delete.ordinal(), "delete")).
                create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> operationTypeComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(instance().operationTypeExtPoint());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> directionComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(instance().directionExtPoint());
    }
}
