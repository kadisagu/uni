/* $Id: FilterItem.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiCatalog.support;

import org.tandemframework.caf.ui.datasource.select.SelectDataSource;

/**
 * @author Andrey Nikonov
 * @since 06.03.2015
 */
public class FilterItem
{
    private Object _value;
    private String _title;
    private String _fieldName;
    private TYPE _type;
    private SelectDataSource _ds;

    public FilterItem(String fieldName, String title, TYPE type, Object value, SelectDataSource ds)
    {
        _fieldName = fieldName;
        _value = value;
        _title = title;
        _type = type;
        _ds = ds;
    }

    public SelectDataSource getDs() {
        return _ds;
    }

    public Object getValue()
    {
        return _value;
    }

    public void setValue(Object value)
    {
        _value = value;
    }

    public String getTitle()
    {
        return _title;
    }

    public TYPE getType()
    {
        return _type;
    }

    public String getFieldName()
    {
        return _fieldName;
    }

    public enum TYPE
    {
        BOOLEAN,
        TEXT,
        INTEGER,
        DOUBLE,
        DATE,
        ENTITY
    }
}