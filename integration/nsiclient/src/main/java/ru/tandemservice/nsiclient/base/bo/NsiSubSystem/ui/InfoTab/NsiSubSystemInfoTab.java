/* $Id: NsiSubSystemInfoTab.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.InfoTab;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSubSystemInfoTab extends BusinessComponentManager
{
}
