package ru.tandemservice.nsiclient.reactor.support;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by andrey on 4/21/15.
 */
public class Logger implements ILogger
{
    private static final org.apache.log4j.Logger log4j_logger = org.apache.log4j.Logger.getLogger(Logger.class);

    @Override
    public synchronized org.apache.log4j.Logger getLog4jLogger()
    {
        return log4j_logger;
    }

    @Override
    public void log(Level loggingLevel, String message)
    {
        // проверяем, есть ли уже appender
        Appender appender = getLog4jLogger().getAppender("NsiSyncAppender");

        if (null != appender)
            getLog4jLogger().log(loggingLevel, message);
        else {
            FileAppender fileAppender = null;
            try
            {
                // добавляем, если нет
                final String path = ApplicationRuntime.getAppInstallPath();
                Calendar cal = CoreDateUtils.createCalendar(new Date());
                String logFileName = "NsiSync_" + cal.get(Calendar.YEAR) + '_' + (cal.get(Calendar.MONTH) + 1) + '_' + cal.get(Calendar.DAY_OF_MONTH) + ".log";
                try
                {
                    fileAppender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
                    fileAppender.setName("NsiSyncAppender");
                    fileAppender.setThreshold(Level.INFO);
                    getLog4jLogger().addAppender(fileAppender);
                    getLog4jLogger().setLevel(Level.INFO);
                } catch (IOException ex)
                {

                }

                if (null != fileAppender)
                    getLog4jLogger().log(loggingLevel, message);
            } finally
            {
                // и отцепляем, если добавляли
                if (null != fileAppender)
                    getLog4jLogger().removeAppender(fileAppender);
            }
        }
    }
}
