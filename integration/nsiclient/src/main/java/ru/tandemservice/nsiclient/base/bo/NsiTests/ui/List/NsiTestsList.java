/* $Id: NsiTestsList.java 44273 2015-07-24 13:42:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiTests.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.NsiSubSystemManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 15.05.2015
 */

@Configuration
public class NsiTestsList extends BusinessComponentManager
{
    public static final String CATALOG_LIST_DS = "catalogListDS";
    public static final String SUBSYSTEM_LIST_DS = "subSystemListDS";
    public static final String TEST_DS = "testDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(CATALOG_LIST_DS, NsiSyncManager.instance().catalogTypeDSHandler()))
                .addDataSource(selectDS(SUBSYSTEM_LIST_DS, NsiSubSystemManager.instance().subSystemDSHandler()))
                .addDataSource(searchListDS(TEST_DS).columnListExtPoint(testColumnList()).rowCustomizer(new IRowCustomizer<TestWrapper>() {
                    @Override
                    public String getHeadRowStyle() {
                        return null;
                    }

                    @Override
                    public String getRowStyle(TestWrapper entity) {
                        if (entity.getError() != null)
                            return "background-color: #FFDDDD;";
                        if (entity.getTime() != null)
                            return "background-color: #DDFFDD;";
                        return null;
                    }

                    @Override
                    public boolean isClickable(AbstractColumn<?> column, TestWrapper rowEntity) {
                        return false;
                    }

                    @Override
                    public boolean isClickable(IPublisherLinkColumn column, TestWrapper rowEntity, IEntity itemEntity) {
                        return false;
                    }
                }).handler(testDSHandler()).
                        create())
                .create();
    }

    @Bean
    public ColumnListExtPoint testColumnList()
    {
        return columnListExtPointBuilder(TEST_DS)
                .addColumn(textColumn("title", "title").treeable(true).width("200px").required(true))
                .addColumn(textColumn("time", "time").width("100px"))
                .addColumn(blockColumn("error", "errorColumn"))
                .addColumn(actionColumn("run", CommonDefines.ICON_EXECUTE, "onRunTest"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler testDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<TestWrapper> list = context.get(UIDefines.COMBO_OBJECT_LIST);
                return ListOutputBuilder.get(input, list).pageable(false).build();
            }
        };
    }
}
