/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest.ui.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.*;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.NsiReactorTestManager;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic.INsiReactorTestDao;
import ru.tandemservice.nsiclient.base.bo.NsiTests.ui.List.NsiTestsList;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 05.10.2015
 */
public class NsiReactorTestListUI extends UIPresenter
{
    private Map<Long, String> _catalogTestResultMap = new HashMap<>();
    private int numTest = 1;
    private final Logger logger = Logger.getLogger(this.getClass());
    private int successfulTests;
    private int runningTests;


    @Override
    public void onComponentActivate()
    {
        initAppender(this.logger);
    }


    public void onClickOldTests()
    {
        getActivationBuilder().asRegionDialog(NsiTestsList.class).activate();
    }

    public void onClickExecuteTests()
    {
        int success = 0;
        INsiReactorTestDao dao = NsiReactorTestManager.instance().dao();
        List<NsiCatalogType> catalogTypeList = getConfig().getDataSources().get(NsiReactorTestList.CATALOG_LIST_DS).getRecords();

        for (NsiCatalogType catalogType : catalogTypeList) {
            success = 0;
            for (int i = 0; i < numTest; i++)
            {
                List<IEntity> createdEntityList = new ArrayList<>();
                try
                {
                    runningTests++;
                    boolean isTestSuccess = dao.runReactorTest(catalogType.getId(), createdEntityList, logger);
                    if (isTestSuccess)
                        success++;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            successfulTests += success;
            String result = NewLineFormatter.NOBR_IN_LINES.format("success: " + success + "; \n" + "failed: " + (numTest - success) + ";");
            _catalogTestResultMap.put(catalogType.getId(), result);
        }
    }

    public void onClickRunTest()
    {
        Long catalogId = getListenerParameterAsLong();
        INsiReactorTestDao dao = NsiReactorTestManager.instance().dao();
        int success = 0;
        for (int i = 0; i < numTest; i++)
        {
            List<IEntity> createdEntityList = new ArrayList<>();
            try
            {
                runningTests++;
                boolean isTestSuccess = dao.runReactorTest(catalogId, createdEntityList, logger);
                if (isTestSuccess)
                    success++;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        successfulTests += success;
        String result = NewLineFormatter.NOBR_IN_LINES.format("success: " + success + "; \n" + "failed: " + (numTest - success) + ";");
        _catalogTestResultMap.put(catalogId, result);
    }


    public void onClickClearCounters()
    {
        successfulTests = 0;
        runningTests = 0;
        _catalogTestResultMap.clear();
    }

    public String getResult()
    {
        String result = _catalogTestResultMap.get(getCurrentRecord().getId());
        return result != null ? result : "";
    }

    private NsiCatalogType getCurrentRecord()
    {
        return getConfig().getDataSources().get(NsiReactorTestList.CATALOG_LIST_DS).getCurrent();
    }

    public int getNumTest()
    {
        return numTest;
    }

    public void setNumTest(int numTest)
    {
        this.numTest = numTest;
    }

    public String getTestResult()
    {
        return NewLineFormatter.NOBR_IN_LINES.format("Всего успешных тестов: " + successfulTests + " из: " + runningTests
                                                             + "; \n Success rate: " + (runningTests > 0 ? 100 * successfulTests / runningTests : "-") + "%");
    }


    // ----------- utils

    /**
     * Регистрируем аппендер в отдельный файл.
     *
     * @param logger наш логгер
     */
    private void initAppender(final Logger logger)
    {
        try
        {
            if (!logger.getAllAppenders().hasMoreElements())
            {
                Layout pattern = new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n");
                logger.addAppender(new ConsoleAppender(pattern));

                final String name = this.getClass().getName().toLowerCase().replace('.', '-').replace('$', '-');
                final String path = ApplicationRuntime.getAppInstallPath();
                final FileAppender appender = new FileAppender(pattern, FilenameUtils.concat(path, "tomcat/logs/reactorTest.log"));
                appender.setName("reactorTest-" + name + "-appender");
                appender.setThreshold(Level.INFO);
                logger.setLevel(Level.INFO);
                logger.addAppender(appender);
                logger.setAdditivity(false);
            }
        }
        catch (final Throwable t)
        {
            t.printStackTrace();
        }
    }
}
