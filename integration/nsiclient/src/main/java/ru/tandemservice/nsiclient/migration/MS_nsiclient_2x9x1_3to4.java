package ru.tandemservice.nsiclient.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_nsiclient_2x9x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nsiCatalogType

		// создано обязательное свойство autoDeliveryChangesAllowed
        if(!tool.columnExists("nsicatalogtype_t", "autodeliverychangesallowed_p"))
		{
			// создать колонку
			tool.createColumn("nsicatalogtype_t", new DBColumn("autodeliverychangesallowed_p", DBType.BOOLEAN));
			tool.executeUpdate("update nsicatalogtype_t set autodeliverychangesallowed_p=? where autodeliverychangesallowed_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("nsicatalogtype_t", "autodeliverychangesallowed_p", false);

		}


    }
}