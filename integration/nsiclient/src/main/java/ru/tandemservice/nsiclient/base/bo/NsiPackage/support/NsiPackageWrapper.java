/* $Id: NsiPackageWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiPackage.support;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.entity.NsiPackage;

/**
 * @author Andrey Nikonov
 * @since 11.03.2015
 */
public class NsiPackageWrapper extends DataWrapper
{
    public static final String PACK = "pack";
    public static final String CATALOG_CODES = "catalogCodes";
    public static final String SUB_SYSTEM_CODES = "subSystemCodes";

    private NsiPackage _pack;
    private String _catalogCodes;
    private String _subSystemCodes;

    public NsiPackageWrapper(NsiPackage pack, String catalogCodes, String subSystemCodes)
    {
        super(pack.getId(), "");
        _pack = pack;
        _catalogCodes = catalogCodes;
        _subSystemCodes = subSystemCodes;
    }

    public NsiPackage getPack()
    {
        return _pack;
    }

    public String getCatalogCodes()
    {
        return _catalogCodes;
    }

    public String getSubSystemCodes()
    {
        return _subSystemCodes;
    }
}