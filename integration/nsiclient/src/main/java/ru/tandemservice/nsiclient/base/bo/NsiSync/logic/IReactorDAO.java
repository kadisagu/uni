/* $Id: IReactorDAO.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 19.05.2015
 */
public interface IReactorDAO
{
    /**
     * Удаление порций сущностей запросами
     * @param entityClass класс сущности
     * @param entityIds идентификаторы сущностей
     */
    void batchDeleteEntities(Class<? extends IEntity> entityClass, List<Long> entityIds);

    /**
     * Находит незанятое значение свойства в сущности.
     * При наличии заданного значения свойства новое генерится как prefix + инкремент
     * @param entityClass класс, для которого нужно найти свободное поле
     * @param property свойство, для которого ищется значение
     * @param id идентификатор сущности, для которой генерится значение свойства, необязателен
     * @param prefix префикс для значения свойства
     * @return свободное значение свойства
     */
    String findUniqueProperty(Class<? extends IEntity> entityClass, String property, Long id, String prefix);

    boolean isPropertyUnique(Class<? extends IEntity> entityClass, String property, Long id, String title);

    /**
     * Проверяет является ли сущность с переданым набором свойств уникальной
     * @param entityClass класс, в котором ищем уникальную сущность
     * @param entity сущность
     * @param properties набор свойств для проверки уникальности
     * @return true - сочетание указанных полей уникально для переданной сущности, иначе - false.
     */
    @Transactional(readOnly = true)
    <T extends IEntity> boolean isPropertiesUnique(Class<T> entityClass, T entity, String... properties);
}
