/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiQueue;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.nsiclient.base.bo.NsiQueue.logic.NsiQueueDSHandler;

/**
 * @author Dmitry Seleznev
 * @since 18.01.2017
 */
@Configuration
public class NsiQueueManager extends BusinessObjectManager
{
    public static NsiQueueManager instance()
    {
        return instance(NsiQueueManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler queueDSHandler()
    {
        return new NsiQueueDSHandler(getName());
    }
}