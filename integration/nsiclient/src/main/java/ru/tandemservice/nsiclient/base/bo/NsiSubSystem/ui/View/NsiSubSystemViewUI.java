/* $Id: NsiSubSystemViewUI.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

import java.text.MessageFormat;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
@State({
        @Bind(key = NsiSubSystemView.SELECTED_TAB, binding = "selectedTab")
})
public class NsiSubSystemViewUI extends UIPresenter
{
    private Long _id;
    private String _selectedTab;
    private String _title;

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public void onChangeTab()
    {
        _uiSettings.set("selectedTab", _selectedTab);
        saveSettings();
    }

    @Override
    public void onComponentActivate()
    {
        if(_selectedTab == null)
            _selectedTab = _uiSettings.get("selectedTab");
        NsiSubSystem catalog = DataAccessServices.dao().get(NsiSubSystem.class, _id);
        _title = catalog.getTitle();
    }

    public String getPageTitle()
    {
        return MessageFormat.format(getConfig().getProperty("page.title"), _title);
    }

    public Map<String, Object> getTabParameter()
    {
        return ParametersMap.createWith(UIPresenter.PUBLISHER_ID, _id);
    }
}
