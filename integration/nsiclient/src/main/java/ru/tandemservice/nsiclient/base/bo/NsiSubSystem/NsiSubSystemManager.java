package ru.tandemservice.nsiclient.base.bo.NsiSubSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSubSystemManager extends BusinessObjectManager
{
    public static NsiSubSystemManager instance()
    {
        return instance(NsiSubSystemManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler subSystemListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), NsiSubSystem.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler subSystemDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), NsiSubSystem.class);
    }
}
