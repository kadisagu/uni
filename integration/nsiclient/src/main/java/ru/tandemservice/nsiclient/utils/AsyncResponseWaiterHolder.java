/* $Id:$ */
package ru.tandemservice.nsiclient.utils;

import com.google.common.collect.Maps;
import org.apache.log4j.Level;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.reactor.support.DatagramProcessingInfo;
import ru.tandemservice.nsiclient.reactor.support.IResponseAction;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 10.11.2015
 */
public class AsyncResponseWaiterHolder {
    public static AsyncResponseWaiterHolder INSTANCE = new AsyncResponseWaiterHolder();

    public Map<String, AsyncResponseWaiter> getWaiterMap() {
        return _waiterMap;
    }

    private final Map<String, AsyncResponseWaiter> _waiterMap = Maps.newConcurrentMap();

    public void dataReady(String guid, NsiSubSystemLog subSystemLog, List<IDatagramObject> datagramElements, boolean finished, String asyncRequestWarning) {
        AsyncResponseWaiter waiter = _waiterMap.get(guid);
        if(waiter != null)
            waiter.dataReady(subSystemLog, datagramElements, finished, asyncRequestWarning);
        else
            NsiRequestHandlerService.instance().getLogger().log(Level.ERROR, "Unknown waiter. May be process was terminated manually.");
    }

    public void stop(String guid) {
        AsyncResponseWaiter waiter = _waiterMap.get(guid);
        if(waiter != null)
            waiter.stop();
    }

    // not thread safe
    public DatagramProcessingInfo launchWaiting(String guid, IResponseAction action) {
        AsyncResponseWaiter waiter = _waiterMap.get(guid);
        if(waiter == null) {
            waiter = new AsyncResponseWaiter(action);
            _waiterMap.put(guid, waiter);
            DatagramProcessingInfo resultWrapper = waiter.launchWaiting();
            _waiterMap.remove(guid);
            return resultWrapper;
        }
        return null;
    }
}
