package ru.tandemservice.nsiclient.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Меняем местами code_p и nsicode_p в nsicatalogtype_t, т.к. справочники должны быть уникальны по датаграмме из НСИ, но могут иметь одинаковые сущности
 * Предыдущая миграция была с ошибкой, поэтому дублирую её
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_nsiclient_2x9x1_2to3 extends IndependentMigrationScript
{
    public static final String UNDEFINED = "undefined";
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.columnExists("nsientity_t", "type_p"))
        {
            tool.createColumn("nsientity_t", new DBColumn("type_p", DBType.createVarchar(255)));


            // задать значение по умолчанию
            java.lang.String defaultType = null;

            PreparedStatement selectNsiCatalogCodes = tool.prepareStatement("SELECT nsicode_p, code_p FROM nsicatalogtype_t");
            selectNsiCatalogCodes.execute();
            ResultSet res = selectNsiCatalogCodes.getResultSet();
            while (res.next())
            {

                String catalogCode = res.getString(1);
                if (catalogCode.contains("\\."))
                {
                    continue;
                }
                catalogCode = StringUtils.uncapitalize(catalogCode);

                String uniEntityFullName = res.getString(2);
                String[] splittedNames = uniEntityFullName.split("\\.");
                String uniEntityName = splittedNames[splittedNames.length - 1];
                uniEntityName = StringUtils.uncapitalize(uniEntityName);

                tool.executeUpdate("update nsientity_t set type_p=? where type_p is null and entitytype_p=?", catalogCode, uniEntityName);
            }


            tool.executeUpdate("update nsientity_t set type_p='" + UNDEFINED + "' where type_p is null");

            // сделать колонку NOT NULL
            tool.setColumnNullable("nsientity_t", "type_p", false);
        }



        Statement stmt = tool.getConnection().createStatement();
        stmt.executeUpdate("delete from nsicatalogtype_t where code_p like '%.%'");
    }
}