/* $Id: BaseWebServiceClientTest.java 44966 2015-09-04 11:49:04Z aavetisov $ */
package ru.tandemservice.nsiclient.base.bo.NsiTests.logic;

import org.apache.axis.message.MessageElement;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ILinkFactoryService;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.codes.NsiSubSystemCodes;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;

/**
 * @author Andrey Avetisov
 * @since 13.05.2015
 */
public class BaseWebServiceClientTest extends SharedBaseDao implements IBaseWebServiceClientTest
{
    public static final String WS_URL = ApplicationRuntime.getProperty(ILinkFactoryService.LINK_FACTORY_SERVICE_URL_BASE) + "/services/NSIService?WSDL";

    protected static ServiceRequestType getRequest(List<IDatagramObject> datagrams, NsiSubSystem subSystem)
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId("mgu");
        header.setDestinationId(subSystem.getUserCode());
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        Datagram datagram = new Datagram();

        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().addAll(datagrams);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));

        MessageElement datagramOut = new org.apache.axis.message.SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);
        return request;
    }

    @Override
    public boolean insertTest(List<IDatagramObject> datagrams, NsiSubSystem subSystem) throws Exception
    {
        String url = StringUtils.isEmpty(subSystem.getAddress()) ? WS_URL : subSystem.getAddress();
        getSession().clear();
        ServiceRequestType request = getRequest(datagrams, subSystem);
        getService(url).insert(request);

        request = getRequest(datagrams, subSystem);
        ServiceResponseType responseType = getService(url).retrieve(request);
        List<IDatagramObject> retrievedDatagram = NsiUtils.getDatagramElements(responseType.getDatagram());
        return isCorrectRetrieve(datagrams, retrievedDatagram);

    }

    @Override
    public boolean retrieveTest(List<IDatagramObject> datagrams, NsiSubSystem subSystem) throws Exception
    {
        String url = StringUtils.isEmpty(subSystem.getAddress()) ? WS_URL : subSystem.getAddress();
        getSession().clear();
        ServiceRequestType request = getRequest(datagrams.subList(0, 1), subSystem);
        ServiceResponseType responseType = getService(url).retrieve(request);
        List<IDatagramObject> retrievedDatagram = NsiUtils.getDatagramElements(responseType.getDatagram());
        return isCorrectRetrieve(datagrams, retrievedDatagram);
    }


    @Override
    public boolean deleteTest(List<IDatagramObject> datagrams, NsiSubSystem subSystem) throws Exception
    {
        String url = StringUtils.isEmpty(subSystem.getAddress()) ? WS_URL : subSystem.getAddress();
        getSession().clear();
        ServiceRequestType request = getRequest(datagrams.subList(0, 1), subSystem);
        getService(url).delete(request);
        if (subSystem.getCode().equals(NsiSubSystemCodes.LOCAL))
        {
            NsiEntity nsiEntity = get(NsiEntity.class, NsiEntity.P_GUID, datagrams.get(0).getID());
            return get(nsiEntity.getEntityId()) == null;
        }
        else
        {
            request = getRequest(datagrams.subList(0, 1), subSystem);
            ServiceResponseType responseType = getService(url).retrieve(request);
            List<IDatagramObject> retrievedDatagram = NsiUtils.getDatagramElements(responseType.getDatagram());
            return CollectionUtils.isEmpty(retrievedDatagram);
        }
    }

    private IDatagramObject getDatagramObjectForCheck(IDatagramObject datagramObject, List<IDatagramObject> datagramForCheckRemoteOperation)
    {
        for (IDatagramObject item : datagramForCheckRemoteOperation)
        {
            if (item.getClass().equals(datagramObject.getClass()))
            {
                return item;
            }
        }
        return null;
    }

    private ServiceSoap getService(String url) throws MalformedURLException
    {
        return new ServiceSoapImplService(new URL(url), ServiceSoapImplService.SERVICE).getServiceSoapPort();
    }

    //проверка операции retrieve:
    private boolean isCorrectRetrieve(List<IDatagramObject> retrievedDatagram, List<IDatagramObject> datagramForCheckRemoteOperation) throws ProcessingDatagramObjectException
    {
        if (CollectionUtils.isEmpty(retrievedDatagram))
            return false;
        IDatagramObject datagramObject = retrievedDatagram.get(0);

        IDatagramObject datagramObjectForCheck = null;
        if (!CollectionUtils.isEmpty(datagramForCheckRemoteOperation))
        {
            datagramObjectForCheck = getDatagramObjectForCheck(datagramObject, datagramForCheckRemoteOperation);
        }
        INsiEntityReactor reactor = ReactorHolder.getReactorMap().get(datagramObject.getClass().getSimpleName());
        return reactor.isCorrect(datagramObject, datagramObjectForCheck, retrievedDatagram);
    }
}
