/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;

/**
 * @author Andrey Nikonov
 * @since 14.12.2015
 */
public interface IUnknownDatagramsCollector {
    void check(String guid, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor);
    <K extends IDatagramObject> void check(String guid, Class<K> clazz);
}
