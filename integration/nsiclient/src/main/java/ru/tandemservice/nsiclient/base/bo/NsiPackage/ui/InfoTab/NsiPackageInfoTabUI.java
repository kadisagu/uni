package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.InfoTab;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.springframework.web.util.HtmlUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView.NsiCatalogElementView;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send.INsiSubSystemSendAction;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send.NsiSubSystemSend;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.reactor.support.ProcessingException;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
public class NsiPackageInfoTabUI extends UIPresenter
{
    private Long _id;
    private NsiPackage _pack;
    private String _catalogs;
    private String _subSystems;
    private String _elements;
    private String _status;
    private String _body;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public void onComponentActivate()
    {
        _pack = DataAccessServices.dao().getNotNull(NsiPackage.class, _id);
        DQLSelectBuilder subSystemBuilder = new DQLSelectBuilder().fromEntity(NsiSubSystemLog.class, "e")
                .column(property("e", NsiSubSystemLog.destination())).distinct()
                .where(eq(property("e", NsiSubSystemLog.pack().id()), commonValue(_pack.getId(), PropertyType.LONG)))
                .order(property("e", NsiSubSystemLog.destination()));
        List<String> subSystems = subSystemBuilder.createStatement(getSupport().getSession()).list();

        _subSystems = Joiner.on(", ").join(subSystems);

        DQLSelectBuilder entityBuilder = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e")
                .joinEntity("e", DQLJoinType.inner, NsiCatalogType.class, "nct", eq(property("e", NsiEntityLog.catalogType()), property("nct", NsiCatalogType.code())))
                .column(property("e", NsiEntityLog.entity()))
                .column(property("nct", NsiCatalogType.title()))
                .where(eq(property("e", NsiEntityLog.pack().id()), commonValue(_pack.getId(), PropertyType.LONG)))
                .order(property("nct", NsiCatalogType.title()));
        List<Object[]> objectList = entityBuilder.createStatement(getSupport().getSession()).list();
        Set<String> catalogs = Sets.newTreeSet();
        Map<String, Long> guidToIdMap = new HashMap<>();

        List<String> links = Lists.newArrayListWithExpectedSize(objectList.size());
        for (Object[] obj : objectList)
        {
            NsiEntity entity = (NsiEntity) obj[0];
            catalogs.add((String) obj[1]);
            guidToIdMap.put(entity.getGuid(), entity.getId());
            Map<String, Object> parameters = Maps.newHashMap();
            parameters.put(IUIPresenter.PUBLISHER_ID, entity.getId());
            parameters.put(NsiCatalogElementView.SELECTED_TAB, NsiCatalogElementView.INFO_TAB);
            String link = CoreServices.linkFactoryService().getLink(null, NsiCatalogElementView.class.getSimpleName(), parameters, null, false);
            links.add("<a href=\"" + HtmlUtils.htmlEscape(link) + "\">" + entity.getGuid() + "</a>");
        }

        _catalogs = Joiner.on(", ").join(catalogs);
        String fullElements = Joiner.on(", ").join(links);

        if (links.size() > 3)
        {
            String id = "divId";
            String link = "<script>jQuery(\'#" + id + "\').click(function() {var html = \'" + fullElements.replaceAll("\"", "&quot;") + "\';$(this).html(html);});</script>";
            _elements = "<div id=\"" + id + "\">" + Joiner.on(", ").join(links.subList(0, 3)) + "&nbsp;..." + "</div>" + link;
        }
        else
            _elements = fullElements;

        String style = _pack.isError() ? "color:red;" : _pack.isWarning() ? "color:#CCCC00;" : null;
        _status = "<span " + (style != null ? "style=\"" + style + "\">" : ">") + _pack.getMessageStatus() + "</span>";
        try
        {
            _body = NsiUtils.formatXml(_pack.getBody(), guidToIdMap);
        }
        catch (Exception e)
        {
            _body = _pack.getBody();
        }
    }

    public String getIncomingValue()
    {
        return NsiSyncManager.DirectionFormatter.INSTANCE.format(_pack.isIncoming());
    }

    public void onSendPackage()
    {
        final List<NsiSubSystemLogWrapper> wrapperList = NsiSyncManager.instance().dao().getSubSystemLogs(_pack);

        _uiActivation.asRegionDialog(NsiSubSystemSend.class)
                .parameter(NsiSubSystemSend.SELECT_ACTION, new INsiSubSystemSendAction()
                {
                    @Override
                    public void selectSubSystems(List<NsiSubSystemLogWrapper> wrappers)
                    {
                        // отключаем логирование в базу и прочие системные штуки
                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                        try
                        {
                            NsiSyncManager.instance().dao().send(_pack, wrappers);
                        }
                        catch (ProcessingException e)
                        {
                            throw new ApplicationException(e.getMessage(), e);
                        } finally
                        {
                            // включаем штуки и логирование обратно
                            Debug.resumeLogging();
                            eventLock.release();
                        }
                    }

                    @Override
                    public List<NsiSubSystemLogWrapper> getSubSystemLogWrappers()
                    {
                        return wrapperList;
                    }
                })
                .activate();
    }

    public void onGetPack()
    {
        String text = NsiUtils.createPackFile(_pack);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt").fileName(_pack.getId() + ".txt").document(text.getBytes()), true);
    }

    public boolean isSendPackageDisabled()
    {
        return NsiSyncManager.Operations.retrieve.name().equals(_pack.getType());
    }
}