/* $Id:$ */
package ru.tandemservice.nsiclient.reactor.support;

/**
 * Критичная ошибка обработки пакетов.
 * @author Andrey Nikonov
 * @since 26.11.2015
 */
public class CriticalException extends Exception
{
    public CriticalException(String message)
    {
        super(message);
    }

    public CriticalException(Exception e)
    {
        super(e);
    }
}
