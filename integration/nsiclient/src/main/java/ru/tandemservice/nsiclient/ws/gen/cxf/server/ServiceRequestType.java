
package ru.tandemservice.nsiclient.ws.gen.cxf.server;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for ServiceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="routingHeader" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}RoutingHeaderType" minOccurs="0"/>
 *         &lt;element name="datagram" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}datagram"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceRequestType", propOrder = {
    "routingHeader",
    "datagram"
})
public class ServiceRequestType {

    protected RoutingHeaderType routingHeader;
    @XmlElement(required = true)
    protected Datagram datagram;

    /**
     * Gets the value of the routingHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RoutingHeaderType }
     *     
     */
    public RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }

    /**
     * Sets the value of the routingHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingHeaderType }
     *     
     */
    public void setRoutingHeader(RoutingHeaderType value) {
        this.routingHeader = value;
    }

    /**
     * Gets the value of the datagram property.
     * 
     * @return
     *     possible object is
     *     {@link Datagram }
     *     
     */
    public Datagram getDatagram() {
        return datagram;
    }

    /**
     * Sets the value of the datagram property.
     * 
     * @param value
     *     allowed object is
     *     {@link Datagram }
     *     
     */
    public void setDatagram(Datagram value) {
        this.datagram = value;
    }

}
