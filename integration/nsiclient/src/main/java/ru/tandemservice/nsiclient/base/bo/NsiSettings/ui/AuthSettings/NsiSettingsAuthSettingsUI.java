/* $Id: NsiSettingsAuthSettingsUI.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.AuthSettings;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.runtime.ApplicationRuntime;

import java.io.File;

/**
 * @author Andrey Avetisov
 * @since 02.07.2015
 */
public class NsiSettingsAuthSettingsUI extends UIPresenter
{
    public static String CLIENT_KEY_STORE = "javax.net.ssl.keyStore";
    public static String CLIENT_KEY_STORE_PASSWORD = "javax.net.ssl.keyStorePassword";
    public static String CLIENT_TRUST_STORE = "javax.net.ssl.trustStore";
    public static String CLIENT_TRUST_STORE_PASSWORD = "javax.net.ssl.trustStorePassword";

    public static String CLIENT_SSL = "ws.client.ssl";
    public static String CLIENT_LOGIN = "ws.client.login";
    public static String CLIENT_PASSWORD = "ws.client.password";

    private String _wsClientKeyStore;
    private String _wsClientKeyStorePassword;
    private String _wsClientLogin;
    private String _wsClientPassword;
    private DataWrapper _enableSSL;


    @Override
    public void onComponentRefresh()
    {
        DataWrapper sslOption = new DataWrapper(NsiSettingsAuthSettings.SSL_ID, "ssl");
        DataWrapper simpleAuthOption = new DataWrapper(NsiSettingsAuthSettings.SIMPLE_ID, "simple auth");

        //deduplicated persons
        _enableSSL = Boolean.parseBoolean(ApplicationRuntime.getProperty(CLIENT_SSL)) ? sslOption : simpleAuthOption;

        setWsClientLogin(ApplicationRuntime.getProperty(CLIENT_LOGIN));
        setWsClientPassword(ApplicationRuntime.getProperty(CLIENT_PASSWORD));

        setWsClientKeyStore(ApplicationRuntime.getProperty(CLIENT_KEY_STORE));
        setWsClientKeyStorePassword(ApplicationRuntime.getProperty(CLIENT_KEY_STORE_PASSWORD));
    }


    // Utils
    public boolean isShowSimpleAuthParam()
    {
        return getEnableSSL().getId().equals(NsiSettingsAuthSettings.SIMPLE_ID);
    }


    //getters & setters
    public String getWsClientKeyStore()
    {
        return _wsClientKeyStore;
    }

    public void setWsClientKeyStore(String wsClientKeyStore)
    {
        _wsClientKeyStore = wsClientKeyStore;
    }

    public String getWsClientKeyStorePassword()
    {
        return _wsClientKeyStorePassword;
    }

    public void setWsClientKeyStorePassword(String wsClientKeyStorePassword)
    {
        _wsClientKeyStorePassword = wsClientKeyStorePassword;
    }

    public DataWrapper getEnableSSL()
    {
        return _enableSSL;
    }

    public void setEnableSSL(DataWrapper enableSSL)
    {
        _enableSSL = enableSSL;
    }

    public String getWsClientLogin()
    {
        return _wsClientLogin;
    }

    public void setWsClientLogin(String wsClientLogin)
    {
        _wsClientLogin = wsClientLogin;
    }

    public String getWsClientPassword()
    {
        return _wsClientPassword;
    }

    public void setWsClientPassword(String wsClientPassword)
    {
        _wsClientPassword = wsClientPassword;
    }

    // Listeners
    public void onClickApply() throws Exception
    {
        PropertiesConfiguration config = new PropertiesConfiguration(new File(ApplicationRuntime.getAppConfigPath(), "app.properties"));

        if (isShowSimpleAuthParam())
        {
            config.setProperty(CLIENT_SSL, false);
            ApplicationRuntime.getProperties().setProperty(CLIENT_SSL, "false");

            setWsClientPassword(StringUtils.trimToEmpty(getWsClientPassword()));
            setWsClientLogin(StringUtils.trimToEmpty(getWsClientLogin()));

            config.setProperty(CLIENT_PASSWORD, getWsClientPassword());
            ApplicationRuntime.getProperties().setProperty(CLIENT_PASSWORD, getWsClientPassword());

            config.setProperty(CLIENT_LOGIN, getWsClientLogin());
            ApplicationRuntime.getProperties().setProperty(CLIENT_LOGIN, getWsClientLogin());

            System.getProperties().remove(CLIENT_KEY_STORE);
            System.getProperties().remove(CLIENT_KEY_STORE_PASSWORD);
            System.getProperties().remove(CLIENT_TRUST_STORE);
            System.getProperties().remove(CLIENT_TRUST_STORE_PASSWORD);

            config.clearProperty(CLIENT_KEY_STORE);
            config.clearProperty(CLIENT_KEY_STORE_PASSWORD);
        }
        else
        {
            config.setProperty(CLIENT_SSL, true);
            ApplicationRuntime.getProperties().setProperty(CLIENT_SSL, "true");

            setWsClientKeyStore(StringUtils.trimToEmpty(getWsClientKeyStore()));
            setWsClientKeyStorePassword(StringUtils.trimToEmpty(getWsClientKeyStorePassword()));

            config.setProperty(CLIENT_KEY_STORE, getWsClientKeyStore());
            ApplicationRuntime.getProperties().setProperty(CLIENT_KEY_STORE, getWsClientKeyStore());

            config.setProperty(CLIENT_KEY_STORE_PASSWORD, getWsClientKeyStorePassword());
            ApplicationRuntime.getProperties().setProperty(CLIENT_KEY_STORE_PASSWORD, getWsClientKeyStorePassword());

            System.setProperty(CLIENT_KEY_STORE, getWsClientKeyStore());
            System.setProperty(CLIENT_KEY_STORE_PASSWORD, getWsClientKeyStorePassword());
            System.setProperty(CLIENT_TRUST_STORE, getWsClientKeyStore());
            System.setProperty(CLIENT_TRUST_STORE_PASSWORD, getWsClientKeyStorePassword());

            config.clearProperty(CLIENT_LOGIN);
            config.clearProperty(CLIENT_PASSWORD);

        }
        config.getLayout().setBlancLinesBefore(CLIENT_SSL, 1);
        config.getLayout().setComment(CLIENT_SSL, "web services settings");

        config.save();
    }
}
