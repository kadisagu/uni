/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.ui.AsyncWaitersList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.nsiclient.utils.AsyncResponseWaiterHolder;

/**
 * @author Andrey Nikonov
 * @since 13.11.2015
 */
public class NsiSyncAsyncWaitersListUI extends UIPresenter {
    public void onDeleteEntityFromList() {
        DataWrapper wrapper = getConfig().getDataSource(NsiSyncAsyncWaitersList.WAITER_LIST_DS).getRecordById(getListenerParameterAsLong());
        AsyncResponseWaiterHolder.INSTANCE.stop(wrapper.getTitle());
    }
}
