/* $Id: IResultProcessor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;

/**
 * @author Andrey Nikonov
 * @since 22.05.2015
 */
public interface IResultProcessor
{
    <V extends IDatagramObject, T extends  IEntity> void processResult(V datagramObject, ChangedWrapper<T> resultWrapper);

    <V extends IDatagramObject> void exception(V datagramObject, ProcessingDatagramObjectException exception);
}
