package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.ConfigList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.NsiSubSystemManager;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.View.NsiSubSystemView;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSubSystemConfigList extends BusinessComponentManager
{
    public static final String SUB_SYSTEM_LIST_DS = "subSystemListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SUB_SYSTEM_LIST_DS, catalogListCL(), NsiSubSystemManager.instance().subSystemListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint catalogListCL()
    {
        return columnListExtPointBuilder(SUB_SYSTEM_LIST_DS)
                .addColumn(publisherColumn("title", NsiSubSystem.title())
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity) {
                                Map<String, Object> params = new HashMap<>();
                                params.put(UIPresenter.PUBLISHER_ID, entity.getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return NsiSubSystemView.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("code", NsiSubSystem.userCode()).order())
                .create();
    }
}
