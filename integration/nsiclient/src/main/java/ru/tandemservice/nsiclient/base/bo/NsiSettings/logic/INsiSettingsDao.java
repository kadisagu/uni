package ru.tandemservice.nsiclient.base.bo.NsiSettings.logic;

import ru.tandemservice.nsiclient.entity.NsiSettings;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
public interface INsiSettingsDao
{
    NsiSettings getNotNullSettings();
}
