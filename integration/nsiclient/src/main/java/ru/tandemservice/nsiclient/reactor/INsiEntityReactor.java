/* $Id: INsiEntityReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.support.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public interface INsiEntityReactor<T extends IEntity, V extends IDatagramObject>
{
    /**
     * Реактор на операцию {@link ru.tandemservice.nsiclient.utils.NsiUtils#OPERATION_TYPE_RETRIEVE}
     * @return
     */
    IOperationReactor getRetrieveOperationReactor();

    /**
     * Реактор на операцию {@link ru.tandemservice.nsiclient.utils.NsiUtils#OPERATION_TYPE_UPDATE}
     * @return
     */
    IOperationReactor getUpdateOperationReactor();

    /**
     * Реактор на операцию {@link ru.tandemservice.nsiclient.utils.NsiUtils#OPERATION_TYPE_DELETE}
     * @return
     */
    IOperationReactor getDeleteOperationReactor();

    /**
     * Объекты какого типа обрабатывает реактор
     * @return
     */
    Class<T> getEntityClass();

    /**
     * Вызывается перед вызовом processDatagramObject. Может быть вызван несколько раз
     * @param items
     * @return
     * @throws ru.tandemservice.nsiclient.reactor.support.ProcessingException
     */
    void preUpdate(List<IDatagramObject> items, Map<String, Object> reactorCache) throws ProcessingException;

    /**
     *
     * @param items
     * @return
     * @throws ru.tandemservice.nsiclient.reactor.support.ProcessingException
     */
    Map<String, Object> preDelete(List<IDatagramObject> items) throws ProcessingException;

    /**
     *
     * @param items
     * @return
     * @throws ru.tandemservice.nsiclient.reactor.support.ProcessingException
     */
    Map<String, Object> preRetrieve(List<IDatagramObject> items) throws ProcessingException;

    /**
     * Собирает отсутствующие, но нужные для обработки запроса объектов
     * @param datagramObject
     * @param collector
     */
    void collectUnknownDatagrams(V datagramObject, IUnknownDatagramsCollector collector);

    /**
     * Создание объекта для передачи в НСИ на основе GUID. Фактически создается пустой объект с заполненным полем guid
     * @param guid
     * @return
     */
    V createDatagramObject(String guid);

    /**
     * Создание объекта для передачи в НСИ на основе сущности и описания связи сущности и объекта в НСИ
     * @param entity
     * @param nsiEntity
     * @param commonCache
     * @return
     */
    ResultListWrapper createDatagramObject(T entity, NsiEntity nsiEntity, Map<String, Object> commonCache);

    /**
     * Создание сущности по объекту датаграммы из НСИ
     * @param datagramObject
     * @return Результат не может быть null
     * @throws ProcessingException
     */
    @NotNull
    ChangedWrapper<T> processDatagramObject(@NotNull V datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException;

    /**
     * Вызывает удаление объектов в НСИ при удалении сущностей локально
     * @param entities
     * @throws ProcessingException
     */
    void deleteItemsNSI(List<Long> entities) throws ProcessingException;

    Map<String, IDatagramObject> createDatagramObjectMap(List<Long> entities, boolean isDeleteOperation) throws ProcessingException;

    void setCatalogType(@NotNull String nsiCode);

    @NotNull
    String getCatalogType();

    void save(Session session, ChangedWrapper<T> w, V datagramObject, NsiPackage nsiPackage);

    // for test use
    List<IDatagramObject> createTestObjects();

    V getEmbedded(boolean onlyGuid);

    boolean isCorrect(T entity, V datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException;

    boolean isCorrect(V datagramObject, V datagramForCheckRemoteOperation, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException;

    /**
     * List of fields that do not need to be validated in the test
     * @return List
     */
    List<String> getNotValidatedFieldList();
}