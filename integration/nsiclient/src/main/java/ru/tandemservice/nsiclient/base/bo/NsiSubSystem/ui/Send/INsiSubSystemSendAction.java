/* $Id: INsiSubSystemSendAction.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send;

import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 16.03.2015
 */
public interface INsiSubSystemSendAction
{
    void selectSubSystems(List<NsiSubSystemLogWrapper> wrappers);

    List<NsiSubSystemLogWrapper> getSubSystemLogWrappers();
}