package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementInfoTab;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.util.*;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@State
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
        })
public class NsiCatalogElementInfoTabUI extends UIPresenter
{
    private Long _id;
    private FieldItem _currValue;
    private List<FieldItem> _values;

    public FieldItem getCurrValue()
    {
        return _currValue;
    }

    public List<FieldItem> getValues()
    {
        return _values;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public void onComponentActivate() {
        NsiEntity entity = DataAccessServices.dao().getNotNull(_id);
        _values = Lists.newArrayList();

        IEntityMeta meta = EntityRuntime.getMeta(entity);
        Collection<IFieldPropertyMeta> declaredFields = meta.getDeclaredFields();
        List<IFieldPropertyMeta> fields = declaredFields instanceof List ? (List<IFieldPropertyMeta>) declaredFields : Lists.newArrayList(declaredFields);
        Collections.sort(fields, new Comparator<IFieldPropertyMeta>() {
            @Override
            public int compare(IFieldPropertyMeta o1, IFieldPropertyMeta o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
        for (IFieldPropertyMeta field : fields)
            if (!NsiCatalogManager.isServiceField(field.getName()))
            {
                Object value = entity.getProperty(field.getName());
                Map<String, Object> parameters = value instanceof NsiEntity ? ParametersMap.createWith(UIPresenter.PUBLISHER_ID, ((NsiEntity) value).getId()) : null;
                _values.add(new FieldItem(field.getTitle(), getStringFromObject(value), parameters));
            }
    }

    private static String getStringFromObject(Object object)
    {
        if(object == null)
            return null;
        if(object instanceof String)
            return (String) object;
        if(object instanceof Boolean)
            return (Boolean) object ? "да" : "нет";
        return object.toString();
    }
}
