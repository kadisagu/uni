package ru.tandemservice.nsiclient.base.bo.NsiSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.logic.INsiSettingsDao;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.logic.NsiSettingsDao;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.PackageSettings.NsiSettingsPackageSettingsUI;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSettingsManager extends BusinessObjectManager
{
    public static NsiSettingsManager instance()
    {
        return instance(NsiSettingsManager.class);
    }

    @Bean
    public INsiSettingsDao dao()
    {
        return new NsiSettingsDao();
    }

    public boolean isUseShortLinksRepresentationInDatagrams()
    {
        if (ApplicationRuntime.existProperty(NsiSettingsPackageSettingsUI.NSI_SHORT_ENTITY_PROPERTY))
            return Boolean.parseBoolean(ApplicationRuntime.getProperty(NsiSettingsPackageSettingsUI.NSI_SHORT_ENTITY_PROPERTY));

        return false;
    }
}
