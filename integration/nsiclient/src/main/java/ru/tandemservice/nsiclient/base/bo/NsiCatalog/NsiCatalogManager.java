package ru.tandemservice.nsiclient.base.bo.NsiCatalog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic.INsiCatalogDAO;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic.NsiCatalogDAO;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic.NsiCatalogElementsListDSHandler;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiCatalogManager extends BusinessObjectManager
{
    public static NsiCatalogManager instance()
    {
        return instance(NsiCatalogManager.class);
    }

    @Bean
    public INsiCatalogDAO dao()
    {
        return new NsiCatalogDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler catalogsListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiCatalogType.class, "e");
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).order().build();
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler comboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), null);
    }

    /**
     * Точка расширения для хэндлера в фильтре по сущности
     * @return
     */
    @Bean
    public ItemListExtPoint<IDefaultComboDataSourceHandler> handlerExtPoint()
    {
        return itemList(IDefaultComboDataSourceHandler.class).
                create();
    }

    /**
     * Точка расширения для сортировки в колонках сущности
     * @return
     */
    @Bean
    public ItemListExtPoint<OrderDescription[]> ordersExtPoint()
    {
        return itemList(OrderDescription[].class).
                create();
    }

    /**
     * Точка расширения для отображения в колонках сущности
     * @return
     */
    @Bean
    public ItemListExtPoint<String> titleExtPoint()
    {
        return itemList(String.class).
                create();
    }

    /**
     * Скрытые поля в колонках и фильтрах для сущности
     * @return
     */
    @Bean
    public ItemListExtPoint<String[]> hidedExtPoint()
    {
        return itemList(String[].class)
                .create();
    }

    /**
     * Можно ли сортировать по данной сущности. Если указано нет, то сортировка не разрешена.
     * Иначе сортировка берется из ordersExtPoint(). Если не указана, то сортируется по title.
     * @return
     */
    @Bean
    public ItemListExtPoint<Boolean> orderableExtPoint()
    {
        return itemList(Boolean.class)
                .add(DatabaseFile.ENTITY_NAME, false)
                .create();
    }

    /**
     * Можно ли фильтровать по данной сущности. Если указано нет, то фильтр не отображается.
     * Иначе хэндлер для фильтра берется из handlerExtPoint(). Если там не указан, то используется comboDSHandler().
     * @return
     */
    @Bean
    public ItemListExtPoint<Boolean> filterableExtPoint()
    {
        return itemList(Boolean.class)
                .add(DatabaseFile.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler nsiCatalogElementsListDSHandler()
    {
        return new NsiCatalogElementsListDSHandler(getName());
    }

    public static boolean isServiceField(String fieldName)
    {
        return NsiEntity.P_DELETED.equals(fieldName) || NsiEntity.P_CREATE_DATE.equals(fieldName) || NsiEntity.P_UPDATE_DATE.equals(fieldName);
    }
}
