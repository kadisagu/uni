/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic.INsiReactorTestDao;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic.INsiReactorTestGeneratorService;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic.NsiReactorTestDao;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic.NsiReactorTestGeneratorService;

/**
 * @author Andrey Avetisov
 * @since 05.10.2015
 */

@Configuration
public class NsiReactorTestManager extends BusinessObjectManager
{
    public static NsiReactorTestManager instance()
    {
        return instance(NsiReactorTestManager.class);
    }

    @Bean
    public INsiReactorTestDao dao()
    {
        return new NsiReactorTestDao();
    }

    @Bean
    public INsiReactorTestGeneratorService generatorService()
    {
        return new NsiReactorTestGeneratorService();
    }
}
