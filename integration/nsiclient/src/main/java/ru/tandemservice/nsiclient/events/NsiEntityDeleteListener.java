/* $Id: NsiEntityDeleteListener.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.events;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.INsiCollectorDaemon;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import java.util.Collection;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
public class NsiEntityDeleteListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> map = ReactorHolder.getReactorMap();
        for (Map.Entry<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> entry : map.entrySet())
            DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, entry.getValue().getEntityClass(), this);
    }

    @Override
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {
        INsiCollectorDaemon.instance.get().registerDeletedEntities(params.toArray(new Long[]{}));
    }
}
