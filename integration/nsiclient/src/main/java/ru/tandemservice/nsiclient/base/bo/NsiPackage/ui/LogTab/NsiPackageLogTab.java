package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.LogTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.NsiSubSystemLogManager;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.ui.View.NsiSubSystemLogView;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiPackageLogTab extends BusinessComponentManager
{
    public static final String LOG_DS = "logDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LOG_DS, logCL(), NsiSubSystemLogManager.instance().subSystemLogListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint logCL()
    {
        return columnListExtPointBuilder(LOG_DS)
                .addColumn(publisherColumn("startDate", NsiSubSystemLog.startDate()).width("145px").formatter(NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC)
                        .publisherLinkResolver(new IPublisherLinkResolver() {
                            @Override
                            public Object getParameters(IEntity entity) {
                                Map<String, Object> params = new HashMap<>();
                                params.put(UIPresenter.PUBLISHER_ID, entity.getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity) {
                                return NsiSubSystemLogView.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("guid", NsiSubSystemLog.guid()).order())
                .addColumn(textColumn("source", NsiSubSystemLog.pack().source()).order())
                .addColumn(textColumn("destination", NsiSubSystemLog.destination()).order())
                .addColumn(textColumn("status", "messageStatus").styleResolver(new IStyleResolver() {
                    @Override
                    public String getStyle(IEntity rowEntity) {
                        NsiSubSystemLog log = (NsiSubSystemLog) rowEntity;
                        if (log.getAdditionalStatus() == NsiSubSystemLog.ADDITIONAL_STATUS_ERROR)
                            return "color:red;";
                        if (log.getAdditionalStatus() == NsiSubSystemLog.ADDITIONAL_STATUS_WARNING)
                            return "color:#CCCC00;";
                        return null;
                    }
                }).order())
                .addColumn(dateColumn("endDate", NsiSubSystemLog.endDate()).width("145px").formatter(NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC).order())
                .addColumn(textColumn("comment", NsiSubSystemLog.comment()).order())
                .addColumn(actionColumn("getPack").icon("template_save").listener("onGetPack"))
                .addColumn(actionColumn("reSend").icon("daemon").listener("onSend").disabled("ui:sendPackageDisabled").alert(alert(LOG_DS + ".send.alert", NsiSubSystemLog.destination())).permissionKey("nsiPackageSend"))
                .create();
    }
}