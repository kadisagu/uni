/* $Id:$ */
package ru.tandemservice.nsiclient.reactor.support;

import ru.tandemservice.nsiclient.base.bo.NsiSettings.NsiSettingsManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Dmitry Seleznev
 * @since 19.10.2016
 */
public class ConditionalDatagramArrayList<E> extends ArrayList<E>
{
    public ConditionalDatagramArrayList(int initialCapacity)
    {
        super(initialCapacity);
    }

    public ConditionalDatagramArrayList()
    {
    }

    public ConditionalDatagramArrayList(Collection<? extends E> c)
    {
        super(c);
    }

    public boolean addWithoutConditions(E e)
    {
        return super.add(e);
    }

    @Override
    public boolean add(E e)
    {
        if (!NsiSettingsManager.instance().isUseShortLinksRepresentationInDatagrams())
            return super.add(e);

        return true;
    }

    @Override
    public void add(int index, E element)
    {
        if (!NsiSettingsManager.instance().isUseShortLinksRepresentationInDatagrams())
            super.add(index, element);
    }

    @Override
    public boolean addAll(Collection<? extends E> c)
    {
        if (!NsiSettingsManager.instance().isUseShortLinksRepresentationInDatagrams())
            return super.addAll(c);

        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c)
    {
        if (!NsiSettingsManager.instance().isUseShortLinksRepresentationInDatagrams())
            return super.addAll(index, c);

        return true;
    }
}