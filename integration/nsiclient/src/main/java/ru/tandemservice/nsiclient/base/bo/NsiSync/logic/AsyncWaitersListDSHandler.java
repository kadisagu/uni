/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.utils.AsyncResponseWaiterHolder;
import ru.tandemservice.nsiclient.utils.AsyncResponseWaiter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 13.11.2015
 */
public class AsyncWaitersListDSHandler extends DefaultSearchDataSourceHandler {

    public AsyncWaitersListDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<DataWrapper> list = new ArrayList<>();
        int i = 0;
        for(Map.Entry<String, AsyncResponseWaiter> entry : AsyncResponseWaiterHolder.INSTANCE.getWaiterMap().entrySet()) {
            list.add(new DataWrapper(i++, entry.getKey()));
        }
        return ListOutputBuilder.get(input, list).build();
    }
}
