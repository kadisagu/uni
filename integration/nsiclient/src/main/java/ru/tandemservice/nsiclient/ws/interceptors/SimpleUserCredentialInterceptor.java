/* $Id$ */
package ru.tandemservice.nsiclient.ws.interceptors;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.saaj.SAAJInInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.AuthSettings.NsiSettingsAuthSettingsUI;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 25.06.2015
 */
public class SimpleUserCredentialInterceptor extends WSS4JInInterceptor
{

    public SimpleUserCredentialInterceptor(Map<String, Object> properties)
    {
        super(properties);
    }

    @Override
    public void handleMessage(SoapMessage msg) throws Fault
    {
        // Вывод входящих SOAP-пакетов в лог
        String printIncomingSoapMessages = ApplicationRuntime.getProperty("ws.printIncomingSoapMessages");
        if(null != printIncomingSoapMessages && Boolean.valueOf(printIncomingSoapMessages))
            printSoapMessage(msg);

        String isSSL = ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_SSL);
        if (null == isSSL)
        {
            throw new RuntimeException("Property \""+NsiSettingsAuthSettingsUI.CLIENT_SSL+"\" can not be found");
        }
        if (!Boolean.valueOf(isSSL))
        {
            super.handleMessage(msg);
        }
    }

    /**
     * Выводит структурированные входящие SAOP-пакеты в лог
     * @param msg - входящее сообщение
     */
    private void printSoapMessage(SoapMessage msg)
    {
        SAAJInInterceptor.INSTANCE.handleMessage(msg);
        SOAPMessage mess = msg.getContent(SOAPMessage.class);
        StringBuilder builder = new StringBuilder();

        try
        {
            builder.append("\n\n---------------------------------------------------------------------------------------\n");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mess.writeTo(baos);
            builder.append(NsiUtils.getXmlFormatted(baos.toString())).append("\n");
        } catch (Exception e)
        {
            builder.append("Exception in SOAP Handler: " + e);
        }

        builder.append("---------------------------------------------------------------------------------------\n");
        System.out.println(builder.toString());
    }
}