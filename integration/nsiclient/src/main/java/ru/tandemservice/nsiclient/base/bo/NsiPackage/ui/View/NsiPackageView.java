package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.InfoTab.NsiPackageInfoTab;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.LogTab.NsiPackageLogTab;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiPackageView extends BusinessComponentManager
{
    public static final String SELECTED_TAB = "selectedTab";

    public static final String TAB_PANEL = "tabPanel";
    public static final String INFO_TAB = "infoTab";
    public static final String LOG_TAB = "logTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(INFO_TAB, NsiPackageInfoTab.class).parameters("ui:tabParameter"))
                .addTab(componentTab(LOG_TAB, NsiPackageLogTab.class).parameters("ui:tabParameter"))
                .create();
    }
}