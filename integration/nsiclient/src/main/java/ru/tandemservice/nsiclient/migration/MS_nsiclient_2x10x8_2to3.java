package ru.tandemservice.nsiclient.migration;

import com.google.common.collect.Lists;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;

import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;


@SuppressWarnings({"unused", "deprecation"})
public class MS_nsiclient_2x10x8_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.8")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        List<Object[]> entityIds = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                "SELECT entityid_p FROM nsientity_t"
        );
        Map<String, List<Long>> tableNameToIds = new HashMap<>();
        for (Object[] object : entityIds) {
            Long entityId = (Long) object[0];
            IEntityMeta entityMeta = EntityRuntime.getMeta(entityId);
            if (entityMeta == null) continue;
            String tableName = entityMeta.getTableName();
            if (!tableNameToIds.containsKey(tableName)) {
                tableNameToIds.put(tableName, new ArrayList<>());
            }
            tableNameToIds.get(tableName).add(entityId);
        }

        for (String tableName : tableNameToIds.keySet()) {
            for (List<Long> ids : Lists.partition(tableNameToIds.get(tableName), DQL.MAX_VALUES_ROW_NUMBER)) {
                List<Long> nsiEntityIdsToDelete = tool.executeQuery(
                        MigrationUtils.processor(Long.class),
                        "SELECT nsientity_t.id FROM nsientity_t " +
                                "LEFT JOIN " + tableName + " e ON nsientity_t.entityid_p = e.id " +
                                "WHERE e.id IS NULL " +
                                "AND nsientity_t.entityid_p IN (" + MigrationUtils.idsToStr(ids) + ")")
                        .stream()
                        .map(row -> (Long) row[0])
                        .collect(Collectors.toList());

                if (!nsiEntityIdsToDelete.isEmpty()) {
                    String idsAsString = MigrationUtils.idsToStr(nsiEntityIdsToDelete);
                    tool.executeUpdate("UPDATE nsientitylog_t " +
                            "SET entity_id = null " +
                            "WHERE entity_id " +
                            "IN (" + idsAsString + ")");
                    tool.executeUpdate("DELETE FROM nsientity_t WHERE id IN (" + idsAsString + ")");
                }
            }
        }

    }

}