package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiEntityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiEntity";
    public static final String ENTITY_NAME = "nsiEntity";
    public static final int VERSION_HASH = -705565099;
    private static IEntityMeta ENTITY_META;

    public static final String P_GUID = "guid";
    public static final String P_DELETED = "deleted";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_UPDATE_DATE = "updateDate";
    public static final String P_ENTITY_ID = "entityId";
    public static final String P_ENTITY_TYPE = "entityType";
    public static final String P_TYPE = "type";
    public static final String P_SYNC = "sync";

    private String _guid;     // GUID
    private boolean _deleted = false;     // Удален
    private Date _createDate;     // Дата и время создания
    private Date _updateDate;     // Дата и время последнего изменения
    private long _entityId;     // Идентификатор сущности
    private String _entityType;     // Тип сущности
    private String _type;     // Тип каталога
    private boolean _sync = false;     // Синхронизирован с НСИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return GUID. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=36)
    public String getGuid()
    {
        return _guid;
    }

    /**
     * @param guid GUID. Свойство не может быть null и должно быть уникальным.
     */
    public void setGuid(String guid)
    {
        dirty(_guid, guid);
        _guid = guid;
    }

    /**
     * @return Удален. Свойство не может быть null.
     */
    @NotNull
    public boolean isDeleted()
    {
        return _deleted;
    }

    /**
     * @param deleted Удален. Свойство не может быть null.
     */
    public void setDeleted(boolean deleted)
    {
        dirty(_deleted, deleted);
        _deleted = deleted;
    }

    /**
     * @return Дата и время создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата и время создания. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата и время последнего изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getUpdateDate()
    {
        return _updateDate;
    }

    /**
     * @param updateDate Дата и время последнего изменения. Свойство не может быть null.
     */
    public void setUpdateDate(Date updateDate)
    {
        dirty(_updateDate, updateDate);
        _updateDate = updateDate;
    }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор сущности. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип сущности. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип сущности. Свойство не может быть null.
     */
    public void setEntityType(String entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Тип каталога. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getType()
    {
        return _type;
    }

    /**
     * @param type Тип каталога. Свойство не может быть null.
     */
    public void setType(String type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Синхронизирован с НСИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isSync()
    {
        return _sync;
    }

    /**
     * @param sync Синхронизирован с НСИ. Свойство не может быть null.
     */
    public void setSync(boolean sync)
    {
        dirty(_sync, sync);
        _sync = sync;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiEntityGen)
        {
            setGuid(((NsiEntity)another).getGuid());
            setDeleted(((NsiEntity)another).isDeleted());
            setCreateDate(((NsiEntity)another).getCreateDate());
            setUpdateDate(((NsiEntity)another).getUpdateDate());
            setEntityId(((NsiEntity)another).getEntityId());
            setEntityType(((NsiEntity)another).getEntityType());
            setType(((NsiEntity)another).getType());
            setSync(((NsiEntity)another).isSync());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiEntityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiEntity.class;
        }

        public T newInstance()
        {
            return (T) new NsiEntity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "guid":
                    return obj.getGuid();
                case "deleted":
                    return obj.isDeleted();
                case "createDate":
                    return obj.getCreateDate();
                case "updateDate":
                    return obj.getUpdateDate();
                case "entityId":
                    return obj.getEntityId();
                case "entityType":
                    return obj.getEntityType();
                case "type":
                    return obj.getType();
                case "sync":
                    return obj.isSync();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "guid":
                    obj.setGuid((String) value);
                    return;
                case "deleted":
                    obj.setDeleted((Boolean) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "updateDate":
                    obj.setUpdateDate((Date) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "entityType":
                    obj.setEntityType((String) value);
                    return;
                case "type":
                    obj.setType((String) value);
                    return;
                case "sync":
                    obj.setSync((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "guid":
                        return true;
                case "deleted":
                        return true;
                case "createDate":
                        return true;
                case "updateDate":
                        return true;
                case "entityId":
                        return true;
                case "entityType":
                        return true;
                case "type":
                        return true;
                case "sync":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "guid":
                    return true;
                case "deleted":
                    return true;
                case "createDate":
                    return true;
                case "updateDate":
                    return true;
                case "entityId":
                    return true;
                case "entityType":
                    return true;
                case "type":
                    return true;
                case "sync":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "guid":
                    return String.class;
                case "deleted":
                    return Boolean.class;
                case "createDate":
                    return Date.class;
                case "updateDate":
                    return Date.class;
                case "entityId":
                    return Long.class;
                case "entityType":
                    return String.class;
                case "type":
                    return String.class;
                case "sync":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiEntity> _dslPath = new Path<NsiEntity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiEntity");
    }
            

    /**
     * @return GUID. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getGuid()
     */
    public static PropertyPath<String> guid()
    {
        return _dslPath.guid();
    }

    /**
     * @return Удален. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#isDeleted()
     */
    public static PropertyPath<Boolean> deleted()
    {
        return _dslPath.deleted();
    }

    /**
     * @return Дата и время создания. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата и время последнего изменения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getUpdateDate()
     */
    public static PropertyPath<Date> updateDate()
    {
        return _dslPath.updateDate();
    }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип сущности. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getEntityType()
     */
    public static PropertyPath<String> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Тип каталога. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getType()
     */
    public static PropertyPath<String> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Синхронизирован с НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#isSync()
     */
    public static PropertyPath<Boolean> sync()
    {
        return _dslPath.sync();
    }

    public static class Path<E extends NsiEntity> extends EntityPath<E>
    {
        private PropertyPath<String> _guid;
        private PropertyPath<Boolean> _deleted;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _updateDate;
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _entityType;
        private PropertyPath<String> _type;
        private PropertyPath<Boolean> _sync;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return GUID. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getGuid()
     */
        public PropertyPath<String> guid()
        {
            if(_guid == null )
                _guid = new PropertyPath<String>(NsiEntityGen.P_GUID, this);
            return _guid;
        }

    /**
     * @return Удален. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#isDeleted()
     */
        public PropertyPath<Boolean> deleted()
        {
            if(_deleted == null )
                _deleted = new PropertyPath<Boolean>(NsiEntityGen.P_DELETED, this);
            return _deleted;
        }

    /**
     * @return Дата и время создания. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(NsiEntityGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата и время последнего изменения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getUpdateDate()
     */
        public PropertyPath<Date> updateDate()
        {
            if(_updateDate == null )
                _updateDate = new PropertyPath<Date>(NsiEntityGen.P_UPDATE_DATE, this);
            return _updateDate;
        }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(NsiEntityGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип сущности. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getEntityType()
     */
        public PropertyPath<String> entityType()
        {
            if(_entityType == null )
                _entityType = new PropertyPath<String>(NsiEntityGen.P_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Тип каталога. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#getType()
     */
        public PropertyPath<String> type()
        {
            if(_type == null )
                _type = new PropertyPath<String>(NsiEntityGen.P_TYPE, this);
            return _type;
        }

    /**
     * @return Синхронизирован с НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntity#isSync()
     */
        public PropertyPath<Boolean> sync()
        {
            if(_sync == null )
                _sync = new PropertyPath<Boolean>(NsiEntityGen.P_SYNC, this);
            return _sync;
        }

        public Class getEntityClass()
        {
            return NsiEntity.class;
        }

        public String getEntityName()
        {
            return "nsiEntity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
