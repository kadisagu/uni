/* $Id:$ */
package ru.tandemservice.nsiclient.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.process.BackgroundProcessThread;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.TapSupportUtils;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.NsiSyncOperatorDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.INsiCollectorDaemon;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.INsiFullSyncDaemon;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.NsiFullSyncDaemon;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackFormingQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiHierarchyReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 11.11.2015
 */
public class Synchronizer {
    public static final Synchronizer INSTANCE = new Synchronizer();
    private boolean _cancelled;
    private Long _initiator;
    private BackgroundProcessThread _thread;
    private Date _startDate;
    private Date _finishDate;
    private String _message;
    StringBuilder _warn = new StringBuilder();
    StringBuilder _errors = new StringBuilder();

    public String getMessage() {
        return _message;
    }

    public BackgroundProcessThread getThread() {
        return _thread;
    }

    public boolean cancel() {
        if(UserContext.getInstance().isLoggedIn() && UserContext.getInstance().getPrincipalId().equals(_initiator)) {
            INsiFullSyncDaemon.instance.get().doInterruptFullSync();
            _cancelled = true;
            return true;
        }
        return false;
    }

    public void finish() {
        if(_thread != null) {
            synchronized (_thread) {
                _cancelled = false;
                _initiator = null;
                _finishDate = new Date();
                _thread = null;
            }
        }
    }

    private void init() throws ProcessingException {
        if(!UserContext.getInstance().isLoggedIn())
            throw new ProcessingException("Запуск синхронизации невозможен.");
        _initiator = UserContext.getInstance().getPrincipalId();
        _cancelled = false;
        _startDate = new Date();
        _finishDate = null;
        _warn = new StringBuilder();
        _errors = new StringBuilder();
        _message = null;
    }

    public synchronized void runProcess(BackgroundProcessThread backgroundProcessThread) {
        if (_thread != null)
            TapSupportUtils.addInitScript(cycle -> "alert(\"Запуск синхронизации невозможен, текущая синхронизация не завершена\")");
        else {
            _thread = backgroundProcessThread;
            _thread.getState().setRefreshTime(1000);
            BusinessComponentUtils.runProcess(_thread);
        }
    }

    /**
     * Получить изменения из НСИ
     */
    public <T extends IEntity, V extends IDatagramObject> String getFromNSI(NsiCatalogType catalogType) throws ProcessingException
    {
        String prefix = "Каталог «" + catalogType.getTitle() + "». ";
        init();

        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        @SuppressWarnings("unchecked")
        INsiEntityReactor<T, V> reactor = (INsiEntityReactor<T, V>) reactors.get(catalogType.getCode());
        ILogger logger = NsiRequestHandlerService.instance().getLogger();
        if (!catalogType.isReadAllowed())
            return "Синхронизация невозможна: нет прав на чтение каталога " + reactor.getCatalogType() + " в НСИ";

        Class<T> reactorEntityClass = reactor.getEntityClass();
        IEntityMeta meta = EntityRuntime.getMeta(reactorEntityClass);
        String className = reactorEntityClass.getSimpleName();

        logger.log(Level.INFO, "Synchronization for " + className + " catalog have started");
        _thread.getState().setMessage(prefix + "Получение данных");

        DQLSelectBuilder countBuilder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "e")
                .where(eq(property("e", NsiEntity.entityType()), value(meta.getName())));
        int count = DataAccessServices.dao().getCount(countBuilder);
        AtomicInteger all = new AtomicInteger(count);

        // нужно, чтобы запрос с in смог выполниться
        int packageSize = NsiRequestHandlerService.instance().getSettings().getPackageSize();
        AtomicInteger packageCount = new AtomicInteger((count - 1) / packageSize + 1);
        AtomicInteger packageNumber = new AtomicInteger(0);
        AtomicInteger processedDatagramsCount = new AtomicInteger(0);
        AtomicInteger insertedDatagramsCount = new AtomicInteger(0);
        AtomicInteger updatedDatagramsCount = new AtomicInteger(0);
        boolean isProcessNotFinished = count > 0;
        Long lastEntityId = null;
        while (isProcessNotFinished && !_cancelled) {
            _thread.getState().setMessage(prefix + "Получение данных. Подготовка пакета " + packageNumber + " из " + packageCount.get() + ". Обработано " + processedDatagramsCount.intValue() + " из " + all.intValue());
            boolean isPackageReady = false;
            List<String> guids = new ArrayList<>(packageSize);
            int limit = Math.min(packageSize, DQL.MAX_VALUES_ROW_NUMBER);
            while (!isPackageReady) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                        .top(limit)
                        .column(property("nsi", NsiEntity.guid()))
                        .where(eq(property("nsi", NsiEntity.entityType()), value(meta.getName())))
                        .order(property("nsi", NsiEntity.id()));
                if (lastEntityId != null)
                    builder.where(gt(property("nsi", NsiEntity.id()), value(lastEntityId)));
                List<String> available = DataAccessServices.dao().getList(builder);
                guids.addAll(available);
                if(available.isEmpty() || guids.size() >= packageSize)
                    isPackageReady = true;
                else {
                    lastEntityId = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.guid(), guids.get(guids.size() - 1)).getId();
                    limit = Math.min(packageSize - guids.size(), DQL.MAX_VALUES_ROW_NUMBER);
                }
            }
            isProcessNotFinished = !guids.isEmpty();
            if (isProcessNotFinished) {
                List<IDatagramObject> list = new ArrayList<>(guids.size());
                for (String guid : guids)
                    list.add(reactor.createDatagramObject(guid));
                _thread.getState().setMessage(prefix + "Получение данных. Отправка запроса " + packageNumber + " из " + packageCount.get() + ". Обработано " + processedDatagramsCount.intValue() + " из " + all.intValue());
                ResultWrapper<List<String>> result = NsiRequestHandlerService.instance().executeNSIAction(list, null, NsiUtils.OPERATION_TYPE_RETRIEVE, null, new IResponseAction() {
                    @Override
                    public DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list1) throws CriticalException {
                        packageNumber.getAndIncrement();
                        if (packageCount.get() < packageNumber.get())
                            packageCount.set(packageNumber.get());
                        List<Long> entityIds = new ArrayList<>();
                        SyncOperatorResult operatorResult = NsiSyncManager.instance().operatorDAO().update(list1, subSystemLog.getPack(), new IResultProcessor() {
                            @Override
                            public <T1 extends IDatagramObject, V1 extends IEntity> void processResult(T1 datagramObject, ChangedWrapper<V1> resultWrapper) {
                                if (resultWrapper.isCreated()) {
                                    insertedDatagramsCount.getAndIncrement();
                                    entityIds.add(resultWrapper.getNsiEntity().getEntityId());
                                } else if (resultWrapper.isChanged()) {
                                    updatedDatagramsCount.getAndIncrement();
                                    entityIds.add(resultWrapper.getNsiEntity().getEntityId());
                                }
                                if (all.get() < processedDatagramsCount.incrementAndGet())
                                    all.getAndIncrement();
                                _thread.getState().setMessage(prefix + "Получение данных. Обработка запроса " + packageNumber + " из " + packageCount.get() + ". Обработано " + processedDatagramsCount.intValue() + " из " + all.intValue());
                            }

                            @Override
                            public <V extends IDatagramObject> void exception(V datagramObject, ProcessingDatagramObjectException exception) {
                                if (all.get() < processedDatagramsCount.incrementAndGet())
                                    all.getAndIncrement();
                            }
                        }, subSystemLog.getGuid());

                        // регистрируем, что пересылать измененные сущности не надо
                        if (!entityIds.isEmpty())
                            INsiCollectorDaemon.instance.get().removeEntities(entityIds.toArray(new Long[entityIds.size()]));
                        return new DatagramProcessingInfo(operatorResult.getError(), operatorResult.getWarning());
                    }
                }, NsiRequestHandlerService.instance().getSettings().isAsync());

                NsiUtils.appendMessage(_warn, result.getWarning());
                NsiUtils.appendMessage(_errors, StringUtils.trimToNull(StringUtils.join(result.getResult(), '\n')));
            }

            NsiSyncManager.instance().dao().clearSession();
        }

        // Изменяем время последней синхронизации для справочника в целом
        NsiSyncManager.instance().dao().updateSyncTime(catalogType.getId());
        logger.log(Level.INFO, "Get for " + className + " catalog was finished");

        StringBuilder endStr = new StringBuilder(" Добавлено элементов локально: ").append(insertedDatagramsCount.get()).append('.')
                .append(" Обновлено элементов локально: ").append(updatedDatagramsCount.get()).append('.')
                .append(" Синхронизировано элементов: ").append(processedDatagramsCount.get()).append('.');

        StringBuilder message = new StringBuilder(_cancelled ? "Синхронизация прервана." : "Синхронизация завершена.");
        if(_warn.length() > 0)
            message.append("\nПредупреждения: ").append(_warn);
        if(_errors.length() > 0)
            message.append("\nОшибки: ").append(_errors);
        message.append(endStr);

        _message = (_cancelled ? "Получение данных прервано." : "Получение данных завершено.") + endStr;
        return message.toString();
    }

    /**
     * Полная отправка содержимого справочника в НСИ с использованием очередей отправки
     */
    public <T extends IEntity, V extends IDatagramObject> String sendFullCatalogToNSI(NsiCatalogType catalogType, boolean resume) throws ProcessingException
    {
        String prefix = "Каталог «" + catalogType.getTitle() + "». ";
        init();

        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        @SuppressWarnings("unchecked")
        INsiEntityReactor<T, V> reactor = (INsiEntityReactor<T, V>) reactors.get(catalogType.getCode());
        ILogger logger = NsiRequestHandlerService.instance().getLogger();

        if (null == reactor)
            return "Отправка данных невозможна: не найден обработчик для каталога " + reactor.getCatalogType();

        if(!catalogType.isReadWriteAllowed())
            return "Отправка данных невозможна: нет прав на запись каталога " + reactor.getCatalogType() + " в НСИ";

        Class<T> reactorEntityClass = reactor.getEntityClass();
        IEntityMeta meta = EntityRuntime.getMeta(reactorEntityClass);
        String className = reactorEntityClass.getSimpleName();

        int all = resume ? DataAccessServices.dao().getCount(new DQLSelectBuilder().fromEntity(NsiPackFormingQueue.class, "e"))
                : DataAccessServices.dao().getCount(new DQLSelectBuilder().fromEntity(reactorEntityClass, "e"));
        _thread.getState().setMessage(prefix + " Формирование списка объектов на отправку (0 из " + all + ")");

        int packageSize = NsiRequestHandlerService.instance().getSettings().getPackageSize();
        AtomicInteger packagesAll = new AtomicInteger((all - 1)/packageSize + 1);

        INsiFullSyncDaemon.instance.get().doInit(_thread, reactor, packagesAll, prefix, resume);

        if (!resume)
        {
            Long lastEntityId = null;
            boolean isQueueCreationFinished = false;
            List<Long> insertList = new ArrayList<>();
            int limit = Math.min(packageSize, DQL.MAX_VALUES_ROW_NUMBER);

            while (!isQueueCreationFinished && !_cancelled)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(reactorEntityClass, "e").top(limit).column(property("e", IEntity.P_ID)).column(property("nsi", IEntity.P_ID))
                        .joinEntity("e", DQLJoinType.left, NsiPackFormingQueue.class, "nsi", eq(property("e", IEntity.P_ID), property("nsi", NsiPackFormingQueue.entityId())))
                        .order(property("e", IEntity.P_ID));

                if (lastEntityId != null) builder.where(gt(property("e", IEntity.P_ID), value(lastEntityId)));
                List<Object[]> available = DataAccessServices.dao().getList(builder);

                List<Long> entityIdList = new ArrayList<>();
                for (Object[] item : DataAccessServices.dao().<Object[]>getList(builder))
                {
                    if (null == item[1]) entityIdList.add((Long) item[0]);
                }

                INsiFullSyncDaemon.instance.get().createCatalogIdStackForPackSend(entityIdList);

                insertList.addAll(entityIdList);
                _thread.getState().setMessage(prefix + " Формирование списка объектов на отправку (" + insertList.size() + " из " + all + ")");

                if (available.isEmpty() || insertList.size() >= all)
                    isQueueCreationFinished = true;
                else
                {
                    lastEntityId = (insertList.get(insertList.size() - 1));
                    limit = Math.min(all - insertList.size(), DQL.MAX_VALUES_ROW_NUMBER);
                }
            }
        }

        // Снимаем с демона блокировку и подпинываем его к началу формирования пакетов
        NsiFullSyncDaemon.unlockDaemon();
        NsiFullSyncDaemon.DAEMON.wakeUpDaemon();

        while(!NsiFullSyncDaemon.isFinished())
        {
            try
            {
                Thread.sleep(1000);
                NsiFullSyncDaemon.DAEMON.wakeUpDaemon();
            }
            catch (Exception e)
            {
            }
        }

        // Изменяем время последней синхронизации для справочника в целом
        NsiSyncManager.instance().dao().updateSyncTime(catalogType.getId());
        logger.log(Level.INFO, "Send for " + className + " catalog was finished");

        StringBuilder endStr = new StringBuilder(" Синхронизировано элементов: ").append(NsiFullSyncDaemon.getCount()).append('.');

        StringBuilder message = new StringBuilder(_cancelled ? "Синхронизация прервана." : "Синхронизация завершена.");
        if(NsiFullSyncDaemon.getWarn().length() > 0)
            message.append("\nПредупреждения: ").append(NsiFullSyncDaemon.getWarn());
        if(NsiFullSyncDaemon.getErrors().length() > 0)
            message.append("\nОшибки: ").append(NsiFullSyncDaemon.getErrors());
        message.append(endStr);

        _message = (_cancelled ? "Отправка данных прервано." : "Отправка данных завершена.") + endStr;
        return message.toString();
    }

    /**
     * Отправить изменения в НСИ
     */
    public <T extends IEntity, V extends IDatagramObject> String sendToNSI(NsiCatalogType catalogType, boolean reSync) throws ProcessingException
    {
        String prefix = "Каталог «" + catalogType.getTitle() + "». ";
        init();

        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        @SuppressWarnings("unchecked")
        INsiEntityReactor<T, V> reactor = (INsiEntityReactor<T, V>) reactors.get(catalogType.getCode());
        ILogger logger = NsiRequestHandlerService.instance().getLogger();
        if(!catalogType.isReadWriteAllowed())
            return "Отправка данных невозможна: нет прав на запись каталога " + reactor.getCatalogType() + " в НСИ";

        Class<T> reactorEntityClass = reactor.getEntityClass();
        IEntityMeta meta = EntityRuntime.getMeta(reactorEntityClass);
        String className = reactorEntityClass.getSimpleName();

        logger.log(Level.INFO, "Synchronization for " + className + " catalog have started");
        _thread.getState().setMessage(prefix + "Получение данных");

        DQLSelectBuilder countBuilder = reSync ? new DQLSelectBuilder().fromEntity(reactorEntityClass, "e")
                : new DQLSelectBuilder().fromEntity(NsiEntity.class, "e")
                .where(eq(property("e", NsiEntity.entityType()), value(meta.getName())));

        int all = DataAccessServices.dao().getCount(countBuilder);

        // нужно, чтобы запрос с in смог выполниться
        int packageSize = NsiRequestHandlerService.instance().getSettings().getPackageSize();
        AtomicInteger packagesAll = new AtomicInteger((all - 1)/packageSize + 1);
        AtomicInteger count = new AtomicInteger(0);
        boolean isProcessFinished = all == 0;
        Long lastEntityId = null;
        AtomicInteger packageCount = new AtomicInteger(1);
        while(!isProcessFinished && !_cancelled) {
            _thread.getState().setMessage(prefix + "Отправка данных. Подготовка пакета " + packageCount + " из " + packagesAll.get());
            boolean isPackageReady = false;
            List<Object[]> insertList = new ArrayList<>(packageSize);
            int limit = Math.min(packageSize, DQL.MAX_VALUES_ROW_NUMBER);
            while (!isPackageReady) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(reactorEntityClass, "e").top(limit).column(property("e")).column(property("nsi"))
                        .joinEntity("e", DQLJoinType.left, NsiEntity.class, "nsi", eq(property("e", IEntity.P_ID), property("nsi", NsiEntity.entityId())))
                        //.where(or(isNull(property("nsi")), eq(property("nsi", NsiEntity.entityType()), value(meta.getName()))))
                        .order(property("e", IEntity.P_ID));

                if(lastEntityId != null) builder.where(gt(property("e", IEntity.P_ID), value(lastEntityId)));

                List<Object[]> available = DataAccessServices.dao().getList(builder);
                insertList.addAll(available);
                if(available.isEmpty() || insertList.size() >= packageSize)
                    isPackageReady = true;
                else {
                    lastEntityId = ((IEntity) insertList.get(insertList.size() - 1)[0]).getId();
                    limit = Math.min(packageSize - insertList.size(), DQL.MAX_VALUES_ROW_NUMBER);
                }
            }

            isProcessFinished = insertList.isEmpty();
            if(!isProcessFinished) {
                List<Pair<T, NsiEntity>> entityList = new ArrayList<>(insertList.size());
                for (Object[] objs : insertList)
                    entityList.add(new Pair<>((T) objs[0], (NsiEntity) objs[1]));

                executeInsertNSIAction(reactor, entityList, count, prefix, packageCount, packagesAll);

                count.addAndGet(insertList.size());
                isProcessFinished = count.get() >= all;

                NsiSyncManager.instance().dao().clearSession();
            }
        }

        // Изменяем время последней синхронизации для справочника в целом
        NsiSyncManager.instance().dao().updateSyncTime(catalogType.getId());
        logger.log(Level.INFO, "Send for " + className + " catalog was finished");

        StringBuilder endStr = new StringBuilder(" Синхронизировано элементов: ").append(count.get()).append('.');

        StringBuilder message = new StringBuilder(_cancelled ? "Синхронизация прервана." : "Синхронизация завершена.");
        if(_warn.length() > 0)
            message.append("\nПредупреждения: ").append(_warn);
        if(_errors.length() > 0)
            message.append("\nОшибки: ").append(_errors);
        message.append(endStr);

        _message = (_cancelled ? "Отправка данных прервано." : "Отправка данных завершена.") + endStr;
        return message.toString();
    }

    /**
     * Получить все из НСИ
     */
    public <T extends IEntity, V extends IDatagramObject> String retrieveAllFromNsi(NsiCatalogType catalogType) throws ProcessingException
    {
        String prefix = "Каталог «" + catalogType.getTitle() + "». Получение данных. ";
        init();

        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        @SuppressWarnings("unchecked")
        INsiEntityReactor<T, V> reactor = (INsiEntityReactor<T, V>) reactors.get(catalogType.getCode());
        ILogger logger = NsiRequestHandlerService.instance().getLogger();
        if(!catalogType.isReadAllowed())
            return "Получение данных невозможно: нет прав на чтение каталога " + reactor.getCatalogType() + " в НСИ";

        Class<T> reactorEntityClass = reactor.getEntityClass();
        String className = reactorEntityClass.getSimpleName();

        logger.log(Level.INFO, "Retrieving for " + className + " catalog started");
        _thread.getState().setMessage(prefix + "Подготовка запроса в НСИ.");

        List<IDatagramObject> list = Collections.singletonList(reactor.createDatagramObject(null));
        AtomicInteger processedDatagramsCount = new AtomicInteger(0);
        AtomicInteger updatedDatagramsCount = new AtomicInteger(0);
        AtomicInteger insertedDatagramsCount = new AtomicInteger(0);
        AtomicInteger packageNumber = new AtomicInteger(0);
        IResponseAction responseAction = new IResponseAction() {
            // IResponseAction может вызываться как внутри INsiCallable, так и сам по себе
            @Override
            public DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list1) throws CriticalException {
                packageNumber.getAndIncrement();
                _thread.getState().setMessage(prefix + "Обновление данных по пакету " + packageNumber.get() + ". Всего обработано " + processedDatagramsCount.get());
                List<Long> entityIds = new ArrayList<>();
                SyncOperatorResult operatorResult = NsiSyncManager.instance().operatorDAO().update(list1, subSystemLog.getPack(), new IResultProcessor() {
                    @Override
                    public <M extends IDatagramObject, N extends IEntity> void processResult(M datagramObject, ChangedWrapper<N> resultWrapper) {
                        if (resultWrapper.isCreated()) {
                            insertedDatagramsCount.getAndIncrement();
                            entityIds.add(resultWrapper.getNsiEntity().getEntityId());
                        } else if (resultWrapper.isChanged()) {
                            updatedDatagramsCount.getAndIncrement();
                            entityIds.add(resultWrapper.getNsiEntity().getEntityId());
                        }
                        processedDatagramsCount.incrementAndGet();
                        _thread.getState().setMessage(prefix + "Обновление данных по пакету " + packageNumber.get() + ". Всего обработано " + processedDatagramsCount.get());
                    }

                    @Override
                    public <V extends IDatagramObject> void exception(V datagramObject, ProcessingDatagramObjectException exception) {
                        processedDatagramsCount.incrementAndGet();
                    }
                }, subSystemLog.getGuid());

                // регистрируем, что пересылать измененные сущности не надо
                if (!entityIds.isEmpty())
                    INsiCollectorDaemon.instance.get().removeEntities(entityIds.toArray(new Long[entityIds.size()]));

                return new DatagramProcessingInfo(operatorResult.getError(), operatorResult.getWarning());
            }
        };
        ResultWrapper<List<String>> result = NsiRequestHandlerService.instance().executeNSIAction(list, null, NsiUtils.OPERATION_TYPE_RETRIEVE, null, responseAction, true);

        NsiUtils.appendMessage(_warn, result.getWarning());
        NsiUtils.appendMessage(_errors, StringUtils.trimToNull(StringUtils.join(result.getResult(), '\n')));

        NsiSyncManager.instance().dao().updateSyncTime(catalogType.getId());
        logger.log(Level.INFO, "Retrieving for " + className + " catalog was finished");

        StringBuilder endStr = new StringBuilder(" Получено элементов: ").append(processedDatagramsCount.get())
                .append(". Добавлено элементов локально: ").append(insertedDatagramsCount.intValue()).append('.')
                .append(" Обновлено элементов локально: ").append(updatedDatagramsCount.intValue()).append('.');

        StringBuilder message = new StringBuilder(_cancelled ? "Получение данных прервано." : "Получение данных завершено.");
        if(_warn.length() > 0)
            message.append("\nПредупреждения: ").append(_warn);
        if(_errors.length() > 0)
            message.append("\nОшибки: ").append(_errors);
        message.append(endStr);

        NsiSyncManager.instance().dao().clearSession();
        _message = (_cancelled ? "Получение данных прервано." : "Получение данных завершено.") + endStr;
        return message.toString();
    }

    /**
     * Отправить новые в НСИ
     */
    public <T extends IEntity, V extends IDatagramObject> String sendNewToNSI(NsiCatalogType catalogType) throws ProcessingException
    {
        String prefix = "Каталог «" + catalogType.getTitle() + "». ";
        init();

        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        @SuppressWarnings("unchecked")
        INsiEntityReactor<T, V> reactor = (INsiEntityReactor<T, V>) reactors.get(catalogType.getCode());
        ILogger logger = NsiRequestHandlerService.instance().getLogger();
        if(!catalogType.isReadWriteAllowed())
            return "Отправка данных невозможна: нет прав на запись каталога " + reactor.getCatalogType() + " в НСИ";

        Class<T> reactorEntityClass = reactor.getEntityClass();
        String className = reactorEntityClass.getSimpleName();

        logger.log(Level.INFO, "Sending new for " + className + " catalog have started");

        boolean isHierarchical = reactor instanceof INsiHierarchyReactor;
        int packageSize = NsiRequestHandlerService.instance().getSettings().getPackageSize();
        AtomicInteger packageCount = new AtomicInteger(1);
        AtomicInteger count = new AtomicInteger(0);

        boolean isProcessNotFinished = true;

        while(isProcessNotFinished && !_cancelled) {
            _thread.getState().setMessage(prefix + "Отправка данных. Выполнение запроса. Обработано " + count.get());
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                    .column(property("nsi", NsiEntity.entityId()))
                    .where(eq(property("nsi", NsiEntity.type()), value(catalogType.getCode())))
                    .where(eq(property("nsi", NsiEntity.entityId()), property("e", IEntity.P_ID)));
            List<T> insertList = isHierarchical ? ((INsiHierarchyReactor) reactor).getHierarchyList(packageSize) :
                    DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(reactorEntityClass, "e")
                    .top(packageSize).column(property("e")).where(notExists(builder.buildQuery())));

            isProcessNotFinished = isHierarchical ? insertList.size() > 0 : insertList.size() == packageSize;
            List<Pair<T, NsiEntity>> entityList = new ArrayList<>(insertList.size());

            int index = 0;
            for (T insertItem : insertList) {
                _thread.getState().setMessage(prefix + "Отправка данных. Создание связей (" + index++ + " из " + insertList.size() + "). Всего обработано " + count.get());
                entityList.add(new Pair<>(insertItem, NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(insertItem, null, null, reactor.getCatalogType())));
            }

            executeInsertNSIAction(reactor, entityList, count, prefix, packageCount, null);
            NsiSyncManager.instance().dao().clearSession();

            count.addAndGet(insertList.size());
        }

        // Изменяем время последней синхронизации для справочника в целом
        NsiSyncManager.instance().dao().updateSyncTime(catalogType.getId());
        logger.log(Level.INFO, "Sending new for " + className + " catalog was finished");

        StringBuilder endStr = new StringBuilder(" Добавлено элементов в НСИ: ").append(count.get()).append('.');

        StringBuilder message = new StringBuilder(_cancelled ? "Синхронизация прервана." : "Синхронизация завершена.");
        if(_warn.length() > 0)
            message.append("\nПредупреждения: ").append(_warn);
        if(_errors.length() > 0)
            message.append("\nОшибки: ").append(_errors);
        message.append(endStr);
        _message = (_cancelled ? "Получение данных прервано." : "Получение данных завершено.") + endStr;
        return message.toString();
    }

    /**
     * Выполнение операции INSERT в НСИ
     * @param reactor реактор (нужен для создания объекта датаграммы)
     * @param entityList список чего отправляем (пары сущность-NsiEntity)
     * @param count счетчик общего кол-ва отправленных элементов
     * @param packageCount счетчик кол-ва пакетов
     * @param packagesAll счетчик общего кол-ва пакетов
     * @param <T>
     * @param <V>
     * @throws ProcessingException
     */
    private <T extends IEntity, V extends IDatagramObject> void executeInsertNSIAction(INsiEntityReactor<T, V> reactor, List<Pair<T, NsiEntity>> entityList, AtomicInteger count, String prefix, AtomicInteger packageCount, AtomicInteger packagesAll) throws ProcessingException {
        Map<String, Object> commonCache = new HashMap<>(1);
        commonCache.put(NsiSyncOperatorDAO.PROCESSED, new HashSet<Long>());
        List<IDatagramObject> nsiInsertEntityList = new ArrayList<>(entityList.size());
        int index = 1;
        for (Pair<T, NsiEntity> pair : entityList) {
            ResultListWrapper wrapper = reactor.createDatagramObject(pair.getX(), pair.getY(), commonCache);
            List<IDatagramObject> list = wrapper.getList();
            IDatagramObject obj = list.get(0);
            nsiInsertEntityList.add(obj);
            NsiUtils.appendMessage(_warn, wrapper.getWarning());
            _thread.getState().setMessage(prefix + "Отправка данных. Подготовка датаграмм (" + index++ + " из " + entityList.size() + "). Всего обработано " + count.get());
        }

        String packageAllCountStr = packagesAll != null ? " из " + packagesAll.get() : "";
        IResponseAction responseAction = new IResponseAction() {
            // ничего не делаем, только говорим, что данные сущности уже синхронизированы (todo может вообще убрать этот код?)
            @Override
            public DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list) throws CriticalException {
                _thread.getState().setMessage(prefix + "Обновление данных по пакету " + packageCount + packageAllCountStr);
                List<Long> entityIds = NsiSyncManager.instance().dao().updateNsiEntitiesByDatagramObjects(list);
                // регистрируем, что пересылать измененные сущности не надо
                if (entityIds != null)
                    INsiCollectorDaemon.instance.get().removeEntities(entityIds.toArray(new Long[entityIds.size()]));
                return null;
            }
        };
        if (!nsiInsertEntityList.isEmpty()) {
            _thread.getState().setMessage(prefix + "Отправка данных в НСИ. Пакет № " + packageCount + packageAllCountStr + ". Всего обработано " + count.get());
            // Собственно, производим отправку свежедобавляемых элементов в НСИ
            ResultWrapper<List<String>> result = NsiRequestHandlerService.instance().executeNSIAction(nsiInsertEntityList, null, NsiUtils.OPERATION_TYPE_UPDATE, null, responseAction, NsiRequestHandlerService.instance().getSettings().isAsync());

            NsiUtils.appendMessage(_warn, result.getWarning());
            NsiUtils.appendMessage(_errors, StringUtils.trimToNull(StringUtils.join(result.getResult(), '\n')));
        }

        packageCount.addAndGet(1);
        if(packagesAll != null && packagesAll.get() < packageCount.get())
            packagesAll.set(packageCount.get());
    }

    public Date getStartDate() {
        return _startDate;
    }

    public Date getFinishDate() {
        return _finishDate;
    }

    public String getWarn() {
        return _warn.toString();
    }

    public String getErrors() {
        return _errors.toString();
    }
}
