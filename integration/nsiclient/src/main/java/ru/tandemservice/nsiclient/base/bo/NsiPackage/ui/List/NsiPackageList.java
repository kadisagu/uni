package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.NsiPackageManager;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.support.NsiPackageWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.View.NsiPackageView;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiPackageList extends BusinessComponentManager
{
    public static final String OPERATION_LOG_DS = "operationLogDS";
    public static final String EVENT_TYPE_DS = "eventTypeOptionDS";
    public static final String OPERATION_TYPE_DS = "operationTypeOptionDS";
    public static final String MESSAGE_STATUS_DS = "messageStatusOptionDS";
    public static final String CATALOG_TYPE_DS = "catalogTypeOptionDS";

    public static final String CATALOG_CODE = "catalogCode";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(OPERATION_LOG_DS, catalogLogCL(), NsiPackageManager.instance().packageDSHandler()))
                .addDataSource(selectDS(MESSAGE_STATUS_DS, NsiPackageManager.instance().messageStatusComboDSHandler()))
                .addDataSource(selectDS(OPERATION_TYPE_DS, NsiSyncManager.instance().operationTypeComboDSHandler()))
                .addDataSource(selectDS(EVENT_TYPE_DS, NsiSyncManager.instance().directionComboDSHandler()))
                .addDataSource(selectDS(CATALOG_TYPE_DS, NsiSyncManager.instance().catalogTypeDSHandler()
                        .filter(NsiCatalogType.title()).filter(NsiCatalogType.code()).order(NsiCatalogType.title()))
                        .addColumn(NsiCatalogType.title().s()).addColumn(NsiCatalogType.code().s())
                )
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint catalogLogCL()
    {
        return columnListExtPointBuilder(OPERATION_LOG_DS)
                .addColumn(publisherColumn("creationDate", NsiPackageWrapper.PACK + '.' + NsiPackage.creationDate()).width("145px").formatter(NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC)
                        .publisherLinkResolver(new IPublisherLinkResolver() {
                            @Override
                            public Object getParameters(IEntity entity) {
                                Map<String, Object> params = new HashMap<>();
                                params.put(UIPresenter.PUBLISHER_ID, entity.getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity) {
                                return NsiPackageView.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("direction", NsiPackageWrapper.PACK + '.' + NsiPackage.incoming()).formatter(NsiSyncManager.DirectionFormatter.INSTANCE).order())
                .addColumn(textColumn("catalogs", NsiPackageWrapper.CATALOG_CODES).visible("ui:showCatalogFilter"))
                .addColumn(textColumn("destinations", NsiPackageWrapper.SUB_SYSTEM_CODES))
                .addColumn(textColumn("type", NsiPackageWrapper.PACK + '.' + NsiPackage.type()).order())
                .addColumn(textColumn("source", NsiPackageWrapper.PACK + '.' + NsiPackage.source()).order())
                .addColumn(textColumn("status", NsiPackageWrapper.PACK + '.' + "messageStatus").styleResolver(new IStyleResolver() {
                    @Override
                    public String getStyle(IEntity rowEntity) {
                        NsiPackage log = ((NsiPackageWrapper) rowEntity).getPack();
                        if (log.isError())
                            return "color:red;";
                        if (log.isWarning())
                            return "color:#CCCC00;";
                        return null;
                    }
                }).order())
                .addColumn(dateColumn("endDate", NsiPackageWrapper.PACK + '.' + NsiPackage.endDate()).width("145px").formatter(NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC).order())
                .addColumn(actionColumn("getPack").icon("template_save").listener("onGetMessagePack"))
                .addColumn(actionColumn("reSend").icon("daemon").listener("onSendMessageAgain").disabled("ui:sendPackageDisabled").permissionKey("nsiPackageSend"))
                .create();
    }
}