/* $Id: ReactorDAO.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import com.google.common.collect.Lists;
import org.apache.log4j.Level;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 19.05.2015
 */
@Transactional
public class ReactorDAO extends BaseModifyAggregateDAO implements IReactorDAO
{
    @Override
    public void batchDeleteEntities(Class<? extends IEntity> entityClass, List<Long> entityIds)
    {
        for (List<Long> elements : Lists.partition(entityIds, DQL.MAX_VALUES_ROW_NUMBER))
            new DQLDeleteBuilder(entityClass)
                    //.where(new DQLCanDeleteExpressionBuilder(entityClass, "id").getExpression())
                    .where(in(property(IEntity.P_ID), elements))
                    .createStatement(getSession()).execute();
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isPropertyUnique(Class<? extends IEntity> entityClass, String property, Long id, String title)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(entityClass, "e")
                .column(DQLFunctions.count(property("e")))
                .where(eq(property("e", property), value(title)));
        if (id != null)
            builder.where(ne(property("e", IEntity.P_ID), value(id)));
        Number cnt = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        return cnt == null || cnt.intValue() == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public String findUniqueProperty(Class<? extends IEntity> entityClass, String property, Long id, String prefix)
    {
        if(isPropertyUnique(entityClass, property, id, prefix))
            return prefix;
        int count = DataAccessServices.dao().getCount(entityClass);
        int i = count;
        while(!isPropertyUnique(entityClass, property, id, prefix + i))
            i++;
        NsiRequestHandlerService.instance().getLogger().log(Level.INFO, (i - count + 1) + " count for search title with prefix " + prefix + " for type " + entityClass.getSimpleName());
        return prefix + i;
    }

    @Override
    public <T extends IEntity> boolean isPropertiesUnique(Class<T> entityClass, T entity, String... properties)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(entityClass, "e")
                .column(DQLFunctions.count(property("e")));
                for (String property : properties)
                {
                    builder.where(eq(property("e", property), commonValue(entity.getProperty(property))));
                }
        if (entity.getId() != null)
            builder.where(ne(property("e", IEntity.P_ID), value(entity.getId())));
        Number cnt = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        return cnt == null || cnt.intValue() == 0;
    }
}
