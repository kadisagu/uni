/* $Id: NsiSubSystemView.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.InfoTab.NsiSubSystemInfoTab;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSubSystemView extends BusinessComponentManager
{
    public static final String SELECTED_TAB = "selectedTab";

    public static final String TAB_PANEL = "tabPanel";
    public static final String INFO_TAB = "infoTab";
    public static final String PERMISSIONS_TAB = "permissionsTab";
    public static final String SUBSCRIBES_TAB = "subscribesTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(INFO_TAB, NsiSubSystemInfoTab.class).parameters("ui:tabParameter"))
                .create();
    }
}
