package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementInfoTab.NsiCatalogElementInfoTab;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementServiceTab.NsiCatalogElementServiceTab;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.List.NsiPackageList;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiCatalogElementView extends BusinessComponentManager
{
    public static final String SELECTED_TAB = "selectedTab";

    public static final String TAB_PANEL = "tabPanel";
    public static final String INFO_TAB = "infoTab";
    public static final String SERVICE_TAB = "serviceTab";
    public static final String PACKAGES_TAB = "packagesTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(INFO_TAB, NsiCatalogElementInfoTab.class).parameters("ui:tabParameter"))
                .addTab(componentTab(SERVICE_TAB, NsiCatalogElementServiceTab.class).parameters("ui:tabParameter"))
                .addTab(componentTab(PACKAGES_TAB, NsiPackageList.class).parameters("ui:tabParameter"))
                .create();
    }
}
