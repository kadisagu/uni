package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ListForObjects;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.View.NsiCatalogView;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiCatalogListForObjects extends BusinessComponentManager
{
    public static final String CATALOG_LIST_DS = "catalogListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CATALOG_LIST_DS, catalogListCL(), NsiCatalogManager.instance().catalogsListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint catalogListCL()
    {
        return columnListExtPointBuilder(CATALOG_LIST_DS)
                .addColumn(checkboxColumn("select").selectCaption("Выбрано").create())
                .addColumn(publisherColumn("title", NsiCatalogType.title())
                        .publisherLinkResolver(new IPublisherLinkResolver() {
                             @Override
                            public Object getParameters(IEntity entity) {
                                Map<String, Object> params = new HashMap<>(1);
                                params.put(NsiCatalogView.CODE, ((NsiCatalogType) entity).getCode());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity) {
                                return NsiCatalogView.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("code", NsiCatalogType.code()).order())
                .addColumn(textColumn("nsiCode", NsiCatalogType.nsiCode()).order())
                .addColumn(toggleColumn("readAllowed", NsiCatalogType.readAllowed()).toggleOffListener("onChangeReadAllowed").toggleOnListener("onChangeReadAllowed"))
                .addColumn(toggleColumn("readWriteAllowed", NsiCatalogType.readWriteAllowed()).toggleOffListener("onChangeReadWriteAllowed").toggleOnListener("onChangeReadWriteAllowed"))
                .addColumn(toggleColumn("autoDeliveryChangesAllowed", NsiCatalogType.autoDeliveryChangesAllowed()).toggleOffListener("onChangeAutoDeliveryChangesAllowed").toggleOnListener("onChangeAutoDeliveryChangesAllowed"))
                .addColumn(dateColumn("syncDate", NsiCatalogType.syncDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).width("110px").order())
                .addColumn(actionColumn("retrieveAll").icon("very_down").listener("onRetrieveAllFromNsi").permissionKey("nsiCatalogSync"))
                .addColumn(actionColumn("sendNew").icon("very_up").listener("onSendNewToNSI").permissionKey("nsiCatalogSync"))
                .addColumn(actionColumn("get").icon("down").listener("onGet").permissionKey("nsiCatalogSync"))
                .addColumn(actionColumn("send").icon("up").listener("onSend").permissionKey("nsiCatalogSync"))
                .create();
    }
}
