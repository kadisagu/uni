
package ru.tandemservice.nsiclient.ws.gen.cxf.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for ServiceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="callCC" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}callCCType"/>
 *         &lt;element name="callRC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="routingHeader" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}RoutingHeaderType" minOccurs="0"/>
 *         &lt;element name="datagram" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}datagram"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceResponseType", propOrder = {
    "callCC",
    "callRC",
    "routingHeader",
    "datagram"
})
public class ServiceResponseType {

    @XmlElement(required = true)
    protected BigInteger callCC;
    protected String callRC;
    protected RoutingHeaderType routingHeader;
    @XmlElement(required = true)
    protected Datagram datagram;

    /**
     * Gets the value of the callCC property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCallCC() {
        return callCC;
    }

    /**
     * Sets the value of the callCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCallCC(BigInteger value) {
        this.callCC = value;
    }

    /**
     * Gets the value of the callRC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallRC() {
        return callRC;
    }

    /**
     * Sets the value of the callRC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallRC(String value) {
        this.callRC = value;
    }

    /**
     * Gets the value of the routingHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RoutingHeaderType }
     *     
     */
    public RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }

    /**
     * Sets the value of the routingHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingHeaderType }
     *     
     */
    public void setRoutingHeader(RoutingHeaderType value) {
        this.routingHeader = value;
    }

    /**
     * Gets the value of the datagram property.
     * 
     * @return
     *     possible object is
     *     {@link Datagram }
     *     
     */
    public Datagram getDatagram() {
        return datagram;
    }

    /**
     * Sets the value of the datagram property.
     * 
     * @param value
     *     allowed object is
     *     {@link Datagram }
     *     
     */
    public void setDatagram(Datagram value) {
        this.datagram = value;
    }

}
