/* $Id: INsiCatalogDAO.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic;

/**
 * @author Andrey Nikonov
 * @since 10.04.2015
 */
public interface INsiCatalogDAO
{
    void checkCatalogs();

    void migration20151005();
}