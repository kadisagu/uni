/* $Id: NsiDeliveryDaemon.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2015
 */
public class NsiDeliveryDaemon extends SharedBaseDao implements INsiDeliveryDaemon
{
    public static final int DAEMON_ITERATION_TIME = 1;

    private static boolean sync_locked = false;
    private static boolean manual_locked = false;

    private static int attemptsCount = 0;
    private static Date deliveryLockTime = null;
    private static boolean deliveryLocked = false;
    private static Long queueItemToReDeliver = null;
    private static String currentMessageGuid = null;
    private static Date currentMessageStartDate = null;
    private static boolean interruptDaemon = false;
    private static Set<String> interruptionRequestedForSet = new HashSet<>();

    public static void lockDaemon()
    {
        manual_locked = true;
    }

    public static void unlockDaemon()
    {
        manual_locked = false;
        deliveryLocked = false;
        deliveryLockTime = null;
    }

    public static boolean isLocked()
    {
        return manual_locked;
    }

    public static boolean isLockedByTransportError()
    {
        return deliveryLocked;
    }

    public static void lockByTransportError(Long logRowId)
    {
        deliveryLockTime = new Date();
        deliveryLocked = true;
        queueItemToReDeliver = logRowId;
    }

    public static void unlockAfterTransportError()
    {
        deliveryLockTime = null;
        deliveryLocked = false;
        queueItemToReDeliver = null;
        attemptsCount = 0;
        currentMessageGuid = null;
        currentMessageStartDate = null;
    }

    public static void setCurrentMessageGuid(String messageGuid)
    {
        currentMessageGuid = messageGuid;
        currentMessageStartDate = new Date();
    }

    public static void interruptMessageProccessing(String messageGuid)
    {
        interruptDaemon = true;

        if (null != messageGuid) interruptionRequestedForSet.add(messageGuid);
        else interruptionRequestedForSet.add(currentMessageGuid);
        //unlockAfterTransportError();
        //unlockDaemon();
        sync_locked = false;
    }

    public static void finalizeMessageProcessing(String messageGuid)
    {
        interruptionRequestedForSet.remove(messageGuid);
        unlockAfterTransportError();
    }

    public static boolean isPackExecutionShouldBeInterrupted(String subsystemLogGuid)
    {
        return null != subsystemLogGuid && interruptionRequestedForSet.contains(subsystemLogGuid);
    }

    public static boolean isCurrentMessageGuid(String messageGuid)
    {
        return null != currentMessageGuid && null != messageGuid && currentMessageGuid.equals(messageGuid);
    }

    public static Date getCurrentMessageStartDate(String messageGuid)
    {
        if (null != currentMessageGuid && null != messageGuid && currentMessageGuid.equals(messageGuid))
            return currentMessageStartDate;

        return null;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(NsiDeliveryDaemon.class.getName(), DAEMON_ITERATION_TIME, INsiDeliveryDaemon.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            interruptDaemon = false;

            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked || manual_locked) return;

            final INsiDeliveryDaemon dao = INsiDeliveryDaemon.instance.get();

            // отключаем логирование в базу и прочие системные штуки
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

            try
            {
                int processedItems = -1;
                long startTime = System.currentTimeMillis();
                while (System.currentTimeMillis() - startTime < DAEMON_ITERATION_TIME * 50000 && 0 != processedItems)
                {
                    if (interruptDaemon) return; // Принудительное прерывание

                    sync_locked = true;

                    try
                    {
                        dao.doCleanupQueueFromLoopedPacks();
                        if (interruptDaemon) return; // Принудительное прерывание

                        List<Long> queueIdList = dao.getPreparedPackageIds(queueItemToReDeliver);
                        if (queueIdList.isEmpty()) processedItems = 0;
                        if (interruptDaemon) return; // Принудительное прерывание

                        if (deliveryLocked)
                        {
                            sync_locked = false;
                            return;
                        }

                        for (Long queueItemId : queueIdList)
                        {
                            if (interruptDaemon) return; // Принудительное прерывание

                            if (deliveryLocked)
                            {
                                sync_locked = false;
                                return;
                            }

                            System.out.println("NsiQueue ID=" + queueItemId + " processing has started"); //TODO Нужна многопоточность. Так зависают пакеты, пока ждут одного очень медленного.
                            // TODO: Во-первых, нужен адекватный механизм отслеживания мертворожденных пакетов
                            // TODO: Во-вторых, нужен список, в котором можно было бы отслеживать очереди пакетов и управлять ими
                            // TODO: В третьих нужен адекватный механизм прерывания отправки подвисших пакетов
                            // TODO: Нужно как-то отлавливать пакеты, отвалившиеся по таймауту и давать им не более двух попыток? Как понять, что сам по себе сервис доступен и работает, но при этом не справляется с конкретным пакетом?
                            NsiRequestHandlerService.instance().executeNSIAction(queueItemId, false);
                        }
                        Thread.sleep(2000);
                    } catch (final Throwable t)
                    {
                        sync_locked = false;
                        unlockAfterTransportError();
                        Debug.exception(t.getMessage(), t);
                        this.logger.warn(t.getMessage(), t);
                    }
                }
            } catch (Throwable t)
            {
                Debug.exception(t.getMessage(), t);
                throw t;
            } finally
            {
                // включаем штуки и логирование обратно
                Debug.resumeLogging();
                eventLock.release();
            }

            sync_locked = false;
            if (!deliveryLocked) unlockAfterTransportError();
        }
    };

    @Override
    public List<Long> getPreparedPackageIds(Long queueId)
    {
        if (null != queueId)
            return Collections.singletonList(queueId);

        return new DQLSelectBuilder().fromEntity(NsiQueue.class, "e")
                .column(property("e", NsiQueue.id())).top(10)
                .order(property("e", NsiQueue.log().startDate()))
                .createStatement(getSession()).list();
    }

    @Override
    public void doCleanupQueueFromLoopedPacks()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiQueue.class, "e").column(property("e", NsiQueue.log().pack().id()))
                .group(property(NsiQueue.log().pack().id().fromAlias("e")))
                .having(ge(count(DQLPredicateType.distinct, property(NsiQueue.id().fromAlias("e"))), value(20)));

        DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(NsiSubSystemLog.class, "ssl").column(property("ssl", NsiSubSystemLog.id()))
                .where(in(property("ssl", NsiSubSystemLog.pack().id()), builder.buildQuery()));

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(NsiQueue.class);
        deleteBuilder.where(in(property(NsiQueue.log().id()), builder1.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
        getSession().flush();
    }
}
