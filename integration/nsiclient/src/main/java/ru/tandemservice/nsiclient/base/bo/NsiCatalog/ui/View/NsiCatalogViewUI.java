package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementsTab.NsiCatalogElementsTab;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.List.NsiPackageList;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.text.MessageFormat;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = NsiCatalogView.CODE, binding = "code", required = true)
})
@State({
        @Bind(key = NsiCatalogView.SELECTED_TAB, binding = "selectedTab")
})
public class NsiCatalogViewUI extends UIPresenter
{
    private String _code;
    private String _selectedTab;
    private String _title;
    private String _entityCode;

    public String getCode()
    {
        return _code;
    }

    public void setCode(String code)
    {
        _code = code;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getPageTitle()
    {
        return MessageFormat.format(getConfig().getProperty("page.title"), _title);
    }

    public Map<String, Object> getTabParameter()
    {
        return ParametersMap.createWith(NsiCatalogElementsTab.ENTITY_CODE, _entityCode);
    }

    public Map<String, Object> getLogListTabParameter()
    {
        return ParametersMap.createWith(NsiPackageList.CATALOG_CODE, _code);
    }

    public void onChangeTab()
    {
        _uiSettings.set("selectedTab", _selectedTab);
        saveSettings();
    }

    @Override
    public void onComponentActivate()
    {
        if(_selectedTab == null)
            _selectedTab = _uiSettings.get("selectedTab");
        NsiCatalogType catalog = DataAccessServices.dao().getByCode(NsiCatalogType.class, _code);
        _title = catalog.getTitle();
        _entityCode = catalog.getNsiCode();
    }
}
