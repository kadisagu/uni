/* $Id: ReactorHolder.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.utils;

import org.springframework.beans.factory.BeanCreationException;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Хранит полный перечень реакторов, регистрируемых в spring.xml в качестве bean'ов
 */
public class ReactorHolder
{

    private Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> _reactors;
    private static List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> _sortedReactors;

    public static List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> getSortedReactorList()
    {
        List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> localInstance = _sortedReactors;
        if (localInstance == null)
        {
            synchronized (ReactorHolder.class)
            {
                localInstance = _sortedReactors;
                if (localInstance == null)
                {
                    _sortedReactors = localInstance = getSortedReactors();
                }
            }
        }
        return localInstance;
    }


    private static List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> getSortedReactors()
    {
        // Класс датаграммы -> Реактор
        Map<Class, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> datagramToReactor = new HashMap<>();
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactorMap = getReactorMap();
        for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : reactorMap.values())
        {
            if (reactor.getClass().isInterface()) continue;

            List<Method> methods = Arrays.asList(reactor.getClass().getDeclaredMethods());
            for (Method method : methods)
            {
                // Ищем метод определющий класс датаграммы
                if (method.getName().equals("createDatagramObject"))
                    if (method.getReturnType().getName().contains("Type"))
                        datagramToReactor.put(method.getReturnType(), reactor);
            }
        }

        Map<Class, Set<Class>> dependencyMap = datagramDependencyMap(new ArrayList<>(datagramToReactor.keySet()));
        List<Class> sortedDatagramClasses = new ArrayList<>();
        for (Class datagramClass : dependencyMap.keySet())
        {
            // Вставляем в отсортированный список датаграммы, которые не зависят от других датаграмм
            if (dependencyMap.get(datagramClass).size() == 0) {
                sortedDatagramClasses.add(datagramClass);
            }
        }
        // Убираем отсортированные датаграммы из обработки и пытаемся разрулить зависимости
        sortedDatagramClasses.forEach(dependencyMap::remove);
        resolveDependencies(sortedDatagramClasses, dependencyMap, dependencyMap.size());

        // Если остались неразрешенные связи - удаляем необязательные связи и пытаемся еще раз разрешить зависимости
        if (dependencyMap.size() > 0)
        {
            Map<Class, Set<Class>> datagramToOptionDependencies = getOptionalDependenciesMap(dependencyMap, datagramToReactor);
            datagramToOptionDependencies.keySet()
                    .forEach(datagramClass ->
                            dependencyMap.get(datagramClass).removeAll(datagramToOptionDependencies.get(datagramClass)));
            resolveDependencies(sortedDatagramClasses, dependencyMap, dependencyMap.size());
        }

        List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> sortedReactorList = new ArrayList<>();
        sortedDatagramClasses.forEach(datagram -> sortedReactorList.add(datagramToReactor.get(datagram)));

        // Добавляем реакторы, которые не смогли отсортировать (если такие есть)
        datagramToReactor.values()
                .stream()
                .filter(reactor -> !sortedReactorList.contains(reactor))
                .forEach(sortedReactorList::add);

        return sortedReactorList;
    }

    /**
     * Вовзращает мапу Класс датаграммы -> Множество необязательных зависимостей
     *
     * <p>Необязательность зависимости определяется так:
     * <p>Если основная датаграмма ссылается на сущность другой датаграммы, но при этом
     * таблица сущности основной датаграммы не ссылается на таблицу сущности другой
     * датаграммы, то такая связь считается необязательной.
     */
    private static Map<Class, Set<Class>> getOptionalDependenciesMap(Map<Class, Set<Class>> dependencyMap, Map<Class, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> datagramToReactor)
    {
        // Класс датаграммы -> Мета сущности из которой формируется датаграмма в реакторе
        Map<Class, IEntityMeta> datagramToMeta = new HashMap<>();
        for (Class datagramClass : dependencyMap.keySet())
        {
            Class<?> reactorClass = datagramToReactor.get(datagramClass).getClass();
            try
            {
                Constructor<?> constructor = reactorClass.getConstructor();
                Object object = constructor.newInstance();
                // Создаем объект реактора и вызываем у него метод getEntityClass, получая класс персистентой сущности
                Method getEntityClassMethod = object.getClass().getMethod("getEntityClass");
                Class entityClass = (Class)getEntityClassMethod.invoke(object);
                IEntityMeta meta = EntityRuntime.getMeta(entityClass);
                if (meta != null)
                    datagramToMeta.put(datagramClass, meta);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e)
            {
                e.printStackTrace();
            }
        }

        // Класс датаграммы -> Множество необязательных зависимостей
        Map<Class, Set<Class>> datagramToOptionDependencies = new HashMap<>();
        for (Class datagramClass : dependencyMap.keySet())
        {
            IEntityMeta meta = datagramToMeta.get(datagramClass);
            if (meta == null) continue;
            // Получаем имена таблиц на которые ссылается сущность
            List<String> dependsOnTables = meta.getOutgoingRelations()
                    .stream()
                    .map(outgoingRelation -> outgoingRelation.getPrimaryEntity().getTableName())
                    .collect(Collectors.toList());

            datagramToOptionDependencies.put(datagramClass, new HashSet<>());
            for (Class dependency :  dependencyMap.get(datagramClass))
            {
                IEntityMeta dependencyMeta = datagramToMeta.get(dependency);
                //Если мета null, скорее всего сущность уже отсортирована
                if (dependencyMeta != null)
                {
                    String dependencyTableName = dependencyMeta.getTableName();
                    // Если таблица сущности датаграммы не ссылается на таблицу сущности датаграммы от которой
                    // она зависит - связь не обязательная
                    if (!dependsOnTables.contains(dependencyTableName))
                    {
                        datagramToOptionDependencies.get(datagramClass).add(dependency);
                    }
                }
            }
        }
        return datagramToOptionDependencies;
    }

    /**
     * Разрешает зависимости, добавляя датаграммы с разрешенными зависимостями в спискок отсортированных датаграмм и
     * удаляя их из последуюшей обработки.
     *
     * <p>Если список отсортированных классов датаграмм содержит все классы необходимые для какой-либо датаграммы,
     * она считается разрешенной и добавляется в отсортированный список.
     *
     * <p>Метод заканчивает работу, когда не остается неразрешенных датаграмм, либо если два вызова подряд
     * не удается разрешить ни одной зависимости.
     */
    private static void resolveDependencies(final List<Class> sortedDatagramClasses, final Map<Class, Set<Class>> dependencyMap, int dependencyMapSize)
    {
        for (Class datagramClass : dependencyMap.keySet())
        {
            if (sortedDatagramClasses.containsAll(dependencyMap.get(datagramClass)))
                sortedDatagramClasses.add(datagramClass);
        }
        sortedDatagramClasses.forEach(dependencyMap::remove);

        if (dependencyMap.size() > 0 && dependencyMapSize != dependencyMap.size())
            resolveDependencies(sortedDatagramClasses, dependencyMap, dependencyMap.size());
    }

    /**
     * Строит мапу зависимости датаграммы по вложенным классам в нее классам других датаграмм.
     */
    private static Map<Class, Set<Class>> datagramDependencyMap(List<Class> datagramClasses)
    {
        Map<Class, Set<Class>> dependencyMap = new HashMap<>();
        for (Class datagramClass : datagramClasses)
        {
            dependencyMap.put(datagramClass, new HashSet<>());
            for (Class dependsOn : datagramClass.getDeclaredClasses())
            {
                for (Field field : dependsOn.getDeclaredFields())
                {
                    // Находим зависимости, исключая рекурсивные ссылки
                    if (field.getType().getName().contains("Type") && field.getType() != datagramClass)
                        dependencyMap.get(datagramClass).add(field.getType());
                }
            }
        }
        return dependencyMap;
    }

    public Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> getReactors()
    {
        return _reactors;
    }

    public void setReactors(Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors)
    {
        _reactors = reactors;
    }

    public static Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> getReactorMap() throws BeanCreationException
    {
        ReactorHolder service = (ReactorHolder) ApplicationRuntime.getBean("nsiReactorHolder");

        if (service == null || service.getReactors() == null)
        {
            throw new ApplicationException("Reactor's list is empty. Put reactor in NsiReactorHolder bean");
        } else
        {
            return service.getReactors();
        }


    }
}