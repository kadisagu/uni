/* $Id: DatagramObjectWithDependencies.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;

import java.util.*;

/**
 * @author Andrey Nikonov
 * @since 29.06.2015
 */
public class DatagramObjectWithDependencies
{
    private IDatagramObject _object;
    private List<IDatagramObject> _dependencies;

    public DatagramObjectWithDependencies(IDatagramObject object, List<IDatagramObject> dependencies)
    {
        _object = object;
        _dependencies = dependencies;
    }

    public IDatagramObject getObject()
    {
        return _object;
    }

    public List<IDatagramObject> getDependencies()
    {
        return _dependencies;
    }

    // todo этот код не делает то, что должен делать, а должен формировать список датаграмм со всеми зависимостями без повторений. Не дописан, потому что пока не нужно.
    public static List<IDatagramObject> fillListWithDependencies(Map<String, DatagramObjectWithDependencies> dependencyMap, List<IDatagramObject> source)
    {
        Set<String> guids = new HashSet<>();
        List<IDatagramObject> result = new ArrayList<>();
        for(IDatagramObject element: source) {
            DatagramObjectWithDependencies root = dependencyMap.remove(element.getID());
            if (root != null && !guids.contains(element.getID())) {
                result.add(root.getObject());
                guids.add(element.getID());
                List<IDatagramObject> list = root.getDependencies();
                if (list != null)
                    for (IDatagramObject object : list) {
                        if(!guids.contains(object.getID())) {
                            result.add(object);
                            guids.add(object.getID());
                        }
                    }
            }
        }
        return result;
    }
}
