package ru.tandemservice.nsiclient.entity;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.NsiSubSystemLogManager;
import ru.tandemservice.nsiclient.entity.gen.NsiSubSystemLogGen;

/** @see ru.tandemservice.nsiclient.entity.gen.NsiSubSystemLogGen */
public class NsiSubSystemLog extends NsiSubSystemLogGen
{
    public static int STATUS_DRAFT = 0;
    public static int STATUS_WAIT_RESPONSE = 1;
    public static int STATUS_DELIVERED = 2;
    public static int STATUS_EXECUTED = 3;

    public static int ADDITIONAL_STATUS_SUCCESS = 0;
    public static int ADDITIONAL_STATUS_WARNING = 1;
    public static int ADDITIONAL_STATUS_ERROR = 2;

    public String getMessageStatus()
    {
        DataWrapper wrapper = NsiSubSystemLogManager.instance().messageStatusExtPoint().getItem(Integer.toString(getStatus()));
        return wrapper != null ? wrapper.getTitle() : "Не известен";
    }

    public String getMessageAdditionalStatus()
    {
        DataWrapper wrapper = NsiSubSystemLogManager.instance().messageAdditionalStatusExtPoint().getItem(Integer.toString(getAdditionalStatus()));
        return wrapper != null ? wrapper.getTitle() : "Не известен";
    }
}