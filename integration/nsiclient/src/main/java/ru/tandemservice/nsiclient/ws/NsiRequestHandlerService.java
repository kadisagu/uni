package ru.tandemservice.nsiclient.ws;

import com.google.common.collect.Lists;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.NsiDeliveryDaemon;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.codes.NsiSubSystemCodes;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.AsyncResponseWaiterHolder;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import javax.xml.ws.WebServiceException;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NsiRequestHandlerService
{
    public static final String RESPONSE_CODE_TITLE_OK = "OK";
    public static final String RESPONSE_CODE_TITLE_ERR_PREFIX = "Could not process the request: ";
    public static final String RESPONSE_CODE_TITLE_INTERNAL_ERR_PREFIX = "Internal error: ";
    public static final String RESPONSE_CODE_TITLE_WARN_PREFIX = "Request was processed with warnings: ";

    private static volatile NsiRequestHandlerService _service;
    private final ILogger _logger = new Logger();
    private final INsiSettings _settings = new NsiSystemSettings();

    public static NsiRequestHandlerService instance() {
        NsiRequestHandlerService localInstance = _service;
        // double-checking pattern
        if (localInstance == null) {
            synchronized (NsiRequestHandlerService.class) {
                localInstance = _service;
                if (localInstance == null) {
                    _service = localInstance = (NsiRequestHandlerService) ApplicationRuntime.getBean("nsiRequestHandlerService");
                }
            }
        }
        return localInstance;
    }

    public ILogger getLogger() {
        return _logger;
    }

    public INsiSettings getSettings() {
        return _settings;
    }

    public ServiceResponseType asyncProcess(AsyncRequestType request)
    {
        ServiceResponseType response = new ServiceResponseType();
        RoutingHeaderType header = new RoutingHeaderType();
        response.setRoutingHeader(header);

        // формируем псевдозаголовок для лога todo может выделить в отдельный класс заголовок? или лучше логировать весь входящий запрос, а на карточке лога уже парсить его и показывать или не показывать заголовок.
        RoutingHeaderType routingHeaderType = new RoutingHeaderType();
        routingHeaderType.setAsync(true);
        routingHeaderType.setOperationType(request.getOperationType());
        routingHeaderType.setElementsCount(null);
        routingHeaderType.setSourceId(request.getSourceId());
        routingHeaderType.setCorrelationId(request.getCorrelationId());
        routingHeaderType.setDestinationId(request.getDestinationId());
        routingHeaderType.setMessageId(request.getMessageId());
        routingHeaderType.setParentId(request.getParentId());
        routingHeaderType.setReplyDestinationId(null);
        routingHeaderType.setTicketDestinationId(null);

        // отключаем логирование в базу и прочие системные штуки
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

        NsiSubSystemLog subSystemLog = NsiSyncManager.instance().dao().createLog(request.getSourceId(), request.getOperationType(),
                                                                                 request.getDestinationId(), routingHeaderType,
                                                                                 request.getMessageId(), request.getParentId(),
                                                                                 request.getDatagram(), true);
        boolean finished = request.isLast() == null ? true : request.isLast();

        try {
            header.setSourceId(getSettings().getSystemCode());
            NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.code(), NsiSubSystemCodes.NSI);
            header.setDestinationId(subSystem.getUserCode());
            header.setMessageId(request.getMessageId());
            header.setOperationType(request.getOperationType());

            NsiSyncManager.instance().dao().updateStatusInWork(subSystemLog.getPack());
            getLogger().log(Level.INFO, request.getOperationType() + " started");

            String asyncRequestId = StringUtils.trimToNull(request.getCorrelationId());
            if(asyncRequestId == null)
                throw new ProcessingException("Unknown request");
            NsiSubSystemLog asyncRequestSystemLog = DataAccessServices.dao().get(NsiSubSystemLog.class, NsiSubSystemLog.P_GUID, asyncRequestId);
            if(asyncRequestSystemLog == null)
                throw new ProcessingException("Unknown request: request with messageId " + asyncRequestId + " not found");

            // сообщение об ошибке, не нужно обрабатывать датаграммы
            if(NsiUtils.RESPONSE_CODE_NUMBER_ERROR.equals(request.getCallCC())) {
                NsiSyncManager.instance().dao().finishLog(subSystemLog, "Async requets with error: " + request.getCallRC(), true, false);
                response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_SUCCESS);
                response.setCallRC(RESPONSE_CODE_TITLE_OK);
                return response;
            }

            List<IDatagramObject> datagramElements = NsiUtils.getDatagramElements(request.getDatagram());
            NsiSyncManager.instance().dao().createEntityLogs(subSystemLog.getPack(), datagramElements);

            // закрытие лога подсистемы внутри метода
            AsyncResponseWaiterHolder.INSTANCE.dataReady(asyncRequestSystemLog.getGuid(), subSystemLog, datagramElements, finished, NsiUtils.RESPONSE_CODE_NUMBER_WARN.equals(request.getCallCC()) ? request.getCallRC() : null);
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_SUCCESS);
            response.setCallRC(RESPONSE_CODE_TITLE_OK);
            return response;
        } catch (ProcessingException e) {
            e.printStackTrace();
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + e.getMessage());
            NsiSyncManager.instance().dao().finishLog(subSystemLog, "Error processing asyn request: " + e.getMessage(), true, false);
            return response;
        } catch (Throwable e) {
            Debug.exception(e);
            Debug.saveDebug();
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_INTERNAL_ERR_PREFIX + e.getMessage());
            NsiSyncManager.instance().dao().finishLog(subSystemLog, "Error processing asyn request: " + e.getMessage(), true, false);
            return response;
        } finally {
            getLogger().log(Level.INFO, request.getOperationType() + " finished");

            // включаем штуки и логирование обратно
            Debug.resumeLogging();
            eventLock.release();
        }
    }

    public ServiceResponseType process(ServiceRequestType request, String operationType)
    {
        ServiceResponseType response = new ServiceResponseType();
        RoutingHeaderType requestHeader = request.getRoutingHeader();
        RoutingHeaderType header = new RoutingHeaderType();
        response.setRoutingHeader(header);

        if (null == requestHeader.getOperationType())
            requestHeader.setOperationType(operationType);

        // отключаем логирование в базу и прочие системные штуки
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

        NsiSubSystemLog subSystemLog = NsiSyncManager.instance().dao().createLog(request, true, null);
        SyncOperatorResult result;

        try {
            List<IDatagramObject> datagramElements = validateRequest(request, operationType);
            NsiSyncManager.instance().dao().createEntityLogs(subSystemLog.getPack(), datagramElements);

            header.setSourceId(getSettings().getSystemCode());
            NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.code(), NsiSubSystemCodes.NSI);
            header.setDestinationId(subSystem.getUserCode());
            header.setMessageId(requestHeader.getMessageId());
            header.setOperationType(requestHeader.getOperationType());

            NsiSyncManager.instance().dao().updateStatusInWork(subSystemLog.getPack());
            getLogger().log(Level.INFO, operationType + " started");
            result = process(datagramElements, operationType, subSystemLog.getPack(), requestHeader.getMessageId());
            getLogger().log(Level.INFO, operationType + " finished");
        } catch (ProcessingException e) {
            e.printStackTrace();
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + e.getMessage());
            NsiSyncManager.instance().dao().finishLog(subSystemLog, getResponseCode(response), true, false);
            return response;
        } catch (Throwable e) {
            Debug.exception(e);
            Debug.saveDebug();
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_INTERNAL_ERR_PREFIX + e.getMessage());
            NsiSyncManager.instance().dao().finishLog(subSystemLog, getResponseCode(response), true, false);
            return response;
        } finally
        {
            // включаем штуки и логирование обратно
            Debug.resumeLogging();
            eventLock.release();
        }

        String warning = result.getWarning();
        String error = result.getError();
        if(error != null)
        {
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_INTERNAL_ERR_PREFIX + '\n' + error);
        } else if(warning != null) {
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_WARN);
            response.setCallRC(RESPONSE_CODE_TITLE_WARN_PREFIX + '\n' + warning);
        } else {
            response.setCallCC(NsiUtils.RESPONSE_CODE_NUMBER_SUCCESS);
            response.setCallRC(RESPONSE_CODE_TITLE_OK);
        }

        IXDatagram datagram = NsiUtils.createXDatagram();
        datagram.getEntityList().addAll(result.getList());
        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(datagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        Datagram d = new Datagram();
        d.getContent().add(datagramOut);
        response.setDatagram(d);

        // отключаем логирование в базу и прочие системные штуки
        final IEventServiceLock eventLock1 = CoreServices.eventService().lock();
        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

        try
        {
            NsiSyncManager.instance().dao().finishLog(subSystemLog, getResponseCode(response), false, warning != null);
        } catch (Throwable t)
        {
            Debug.exception(t.getMessage(), t);
            throw t;
        } finally
        {
            // включаем штуки и логирование обратно
            Debug.resumeLogging();
            eventLock1.release();
        }

        NsiDeliveryDaemon.DAEMON.wakeUpDaemon();

        return response;
    }

    private static SyncOperatorResult process(List<IDatagramObject> datagramElements, String operationType, NsiPackage nsiPackage, String messageId) throws ProcessingException
    {
        if(NsiUtils.OPERATION_TYPE_DELETE.equalsIgnoreCase(operationType))
            return NsiSyncManager.instance().operatorDAO().delete(datagramElements, nsiPackage);
        if(NsiUtils.OPERATION_TYPE_INSERT.equalsIgnoreCase(operationType))
            return NsiSyncManager.instance().operatorDAO().update(datagramElements, nsiPackage, null, messageId);
        if(NsiUtils.OPERATION_TYPE_UPDATE.equalsIgnoreCase(operationType))
            return NsiSyncManager.instance().operatorDAO().update(datagramElements, nsiPackage, null, messageId);
        if(NsiUtils.OPERATION_TYPE_RETRIEVE.equalsIgnoreCase(operationType))
            return NsiSyncManager.instance().operatorDAO().retrieve(datagramElements, nsiPackage);
        throw new ProcessingException("Unknown operation type: " + operationType);
    }

    public ServiceResponseType dummyResponse(ServiceRequestType nsiRequest)
    {
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId(getSettings().getSystemCode());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        ServiceResponseType response = new ServiceResponseType();
        response.setRoutingHeader(header);
        response.setCallCC(BigInteger.valueOf(0L));
        response.setCallRC("It's OK. Just test response.");

        return response;
    }

    private static List<IDatagramObject> validateRequest(ServiceRequestType request, String operationType) throws ProcessingException
    {
        RoutingHeaderType header = request.getRoutingHeader();
        if (null == header)
            throw new ProcessingException("No request header was found.");
        if (null == header.getOperationType())
            throw new ProcessingException("Operation type is not specified.");
        if (!operationType.equals(header.getOperationType()))
            throw new ProcessingException("Operation type '" + header.getOperationType() + "' could not be processed by the '" + operationType + "' method.");
        if (null == request.getDatagram())
            throw new ProcessingException("No datagram specified.");
        List<IDatagramObject> datagramElements;
        try {
            datagramElements = NsiUtils.getDatagramElements(request.getDatagram());
        } catch (Exception e) {
            throw new ProcessingException(e);
        }
        if (null == datagramElements || datagramElements.isEmpty())
            throw new ProcessingException("There is no elements in datagram specified.");
        return datagramElements;
    }

    private ServiceResponseType performRequest(ServiceSoap servicePort, ServiceRequestType request, String operationType) throws Exception
    {
        int tryCount = 0;
        int[] timeIntervals = null;
        boolean retry = true;
        while (retry) {

            // Принудительное прерывание
            if(NsiDeliveryDaemon.isPackExecutionShouldBeInterrupted(request.getRoutingHeader().getMessageId()))
            {
                NsiDeliveryDaemon.finalizeMessageProcessing(request.getRoutingHeader().getMessageId());
                return null;
            }

            try {
                if (NsiUtils.OPERATION_TYPE_INSERT.equals(operationType))
                    return servicePort.insert(request);
                if (NsiUtils.OPERATION_TYPE_UPDATE.equals(operationType))
                    return servicePort.update(request);
                if (NsiUtils.OPERATION_TYPE_DELETE.equals(operationType))
                    return servicePort.delete(request);
                if (NsiUtils.OPERATION_TYPE_RETRIEVE.equals(operationType))
                    return servicePort.retrieve(request);
                throw new IllegalArgumentException("Operation " + operationType + " is not supported!");
            } catch (WebServiceException e) {
                e.printStackTrace();
                getLogger().log(Level.INFO, "!!!!!! TIMEOUT (" + (tryCount + 1) + ") !!!!!!");
                if (timeIntervals == null)
                    timeIntervals = getSettings().getTimeIntervals();

                // Принудительное прерывание
                if(NsiDeliveryDaemon.isPackExecutionShouldBeInterrupted(request.getRoutingHeader().getMessageId()))
                {
                    NsiDeliveryDaemon.finalizeMessageProcessing(request.getRoutingHeader().getMessageId());
                    return null;
                }

                //TODO: правильно так, но тогда все потоки встанут в ожидании коннекта: if (timeIntervals == null || timeIntervals.length > tryCount)
                if (timeIntervals == null || timeIntervals.length <= tryCount)
                    retry = false;
                else {
                    Thread.sleep(timeIntervals[tryCount]);
                    tryCount++;
                }
            }
        }
        return null;
    }

    private ServiceSoap getServiceSoap() throws ProcessingException
    {
        if(StringUtils.isEmpty(getSettings().getNsiAddress()))
            throw new ProcessingException("Не указан адрес системы НСИ.");

        return new ServiceSoapImplService(getSettings().getNsiAddress()).getServiceSoapPort();
    }

    /**
     * Метод для отправки запросов в НСИ и их обработки
     * @param items список датаграмм для отправки
     * @param dependencyMap карта зависимостей для уменьшения кол-ва запросов из НСИ (сейчас не используется, но в принципе, если нужно отправлять за раз все связанные сущности, то можно использовать эту структуру данных)
     * @param operationType тип операции (update, retrieve, etc)
     * @param parentMessageId идентификатор операции
     * @param action дополнительный обработчик результата
     * @param async флаг асинхронной операции
     * @return Список датаграмм и предупрждения, если есть
     * @throws ProcessingException
     */
    public ResultWrapper<List<String>> executeNSIAction(List<IDatagramObject> items, Map<String, DatagramObjectWithDependencies> dependencyMap, String operationType, String parentMessageId, IResponseAction action, boolean async) throws ProcessingException
    {
        ServiceSoap servicePort = getServiceSoap();
        StringBuilder warnLog = new StringBuilder();
        List<String> errors = new ArrayList<>();

        getLogger().log(Level.INFO, "Executing " + (async ? "async " : "") + operationType + " for " + items.size() + " element(s) to NSI" + (parentMessageId != null ? ". Parent message: " + parentMessageId : ""));
        String localParentMessageId = parentMessageId;
        int portionIndex = 1;
        int portionSize = getSettings().getPackageSize();
        for (List<IDatagramObject> elements : Lists.partition(items, portionSize)) {
            NsiSubSystemLog subSystemLog = null;
            try {
                List<IDatagramObject> datagramElements = dependencyMap == null ? elements : DatagramObjectWithDependencies.fillListWithDependencies(dependencyMap, elements);
                ServiceRequestType request = NsiUtils.createServiceRequestType(localParentMessageId, operationType, NsiUtils.createServiceRequestTypeDatagram(datagramElements), async);
                localParentMessageId = request.getRoutingHeader().getMessageId();
                subSystemLog = NsiSyncManager.instance().dao().createLog(request, false, datagramElements);
                NsiSyncManager.instance().dao().updateStatusInWork(subSystemLog.getPack());
                NsiSyncManager.instance().dao().updateLogStatusWaitResponse(subSystemLog);
                ServiceResponseType response = performRequest(servicePort, request, operationType);
                if (response == null)
                    // не смогли получить данные из ответа, дальше запросы не отправляем
                    throw new Exception("Пакет № " + portionIndex + " не доставлен в НСИ");
                NsiSyncManager.instance().dao().updateLogStatusDelivered(subSystemLog);

                String error = null;
                String warning = null;
                if (NsiUtils.RESPONSE_CODE_NUMBER_ERROR.equals(response.getCallCC())) {
                    error = getResponseCode(response);
                    getLogger().log(Level.ERROR, error);
                } else {
                    NsiSyncManager.instance().dao().updateStatusInWork(subSystemLog.getPack());
                    if (NsiUtils.RESPONSE_CODE_NUMBER_ASYNC_RESPONSE.equals(response.getCallCC())) {
                        DatagramProcessingInfo resultWrapper = AsyncResponseWaiterHolder.INSTANCE.launchWaiting(request.getRoutingHeader().getMessageId(), action);
                        if(resultWrapper == null)
                            error = "Таймаут!";
                        else {
                            warning = resultWrapper.getWarn();
                            error = resultWrapper.getError();
                        }
                    } else {
                        if (NsiUtils.RESPONSE_CODE_NUMBER_WARN.equals(response.getCallCC()))
                            NsiUtils.appendMessage(warnLog, response.getCallRC());

                        if (response.getDatagram() != null && response.getDatagram().getContent() != null) {
                            List<IDatagramObject> list;
                            try {
                                list = NsiUtils.getDatagramElements(response.getDatagram());
                            } catch (Exception e) {
                                // что-то непонятное, на всякий случай запишем в базу
                                throw new ProcessingException(e);
                            }

                            if (action != null) {
                                DatagramProcessingInfo info = action.action(subSystemLog, list);
                                if(info != null) {
                                    warning = info.getWarn();
                                    error = info.getError();
                                }
                            }
                        }
                    }
                }
                StringBuilder comment = new StringBuilder();
                if(error != null)
                    NsiUtils.appendMessage(comment, "Error: " + error);
                if(warning != null)
                    NsiUtils.appendMessage(comment, "Warning: " + warning);

                if(error != null)
                    errors.add(error);
                NsiUtils.appendMessage(warnLog, warning);
                NsiSyncManager.instance().dao().finishLog(subSystemLog, comment.toString(), error != null, warning != null);
                getLogger().log(Level.INFO, elements.size() + (elements.size() == 1 ? " " : " more ") + "element(s) were processed at NSI");
            } catch (ProcessingException e) {
                Debug.exception(e);
                Debug.saveDebug();
                String error ="Some ERROR has occured while processing element(s) at UNI:\n" + e.getMessage();
                getLogger().log(Level.ERROR, error);
                errors.add(error);
                NsiSyncManager.instance().dao().finishLog(subSystemLog, error, true, false);
            } catch (CriticalException e) {
                Debug.exception(e);
                Debug.saveDebug();
                String error ="Some ERROR has occured while processing element(s) at UNI:\n" + e.getMessage();
                getLogger().log(Level.ERROR, error);
                errors.add(error);
                NsiSyncManager.instance().dao().finishLog(subSystemLog, error, true, false);
                // залогировали, пробрасываем дальше
                throw new ProcessingException(e);
            }
            catch (Throwable t) {
                Debug.exception(t);
                Debug.saveDebug();
                StringBuilder err = new StringBuilder("Some ERROR has occured while processing element(s) at NSI:\n").append(t.getMessage());
                getLogger().log(Level.ERROR, err.toString());
                errors.add(err.toString());
                if (subSystemLog != null)
                    NsiSyncManager.instance().dao().finishLog(subSystemLog, err.toString(), true, false);
                // что-то критичное, прерываем выполнение.
                throw new ProcessingException(new Exception(t));
            }
            portionIndex++;
        }

        getLogger().log(Level.INFO, "Executing " + operationType + " for element(s) to NSI was finished" + (parentMessageId != null ? ". Parent message: " + parentMessageId : ""));
        return new ResultWrapper<>(warnLog.length() > 0 ? warnLog.toString() : null, errors);
    }

    public void executeNSIAction(NsiSubSystemLog nsiSubSystemLog) throws ProcessingException
    {
        ServiceSoap servicePort = getServiceSoap();
        String operationType = nsiSubSystemLog.getPack().getType();

        getLogger().log(Level.INFO, "Executing " + operationType + " for element(s) at NSI");
        Datagram datagram = NsiUtils.fromXml(Datagram.class, nsiSubSystemLog.getPack().getBody().getBytes());

        NsiSubSystemLog subSystemLog = null;
        try {
            ServiceRequestType request = NsiUtils.createServiceRequestType(null, operationType, datagram, NsiRequestHandlerService.instance().getSettings().isAsync());

            subSystemLog = NsiSyncManager.instance().dao().createLog(nsiSubSystemLog, request.getRoutingHeader());
            NsiSyncManager.instance().dao().updateStatusInWork(subSystemLog.getPack());
            NsiSyncManager.instance().dao().updateLogStatusWaitResponse(subSystemLog);
            ServiceResponseType response = performRequest(servicePort, request, operationType);
            if (response == null)
                throw new ProcessingException("Element(s) doesn't delivered to NSI");
            NsiSyncManager.instance().dao().updateLogStatusDelivered(subSystemLog);

            if (NsiUtils.RESPONSE_CODE_NUMBER_ERROR.equals(response.getCallCC())) {
                String responseCode = getResponseCode(response);
                getLogger().log(Level.ERROR, responseCode);
                NsiSyncManager.instance().dao().finishLog(subSystemLog, responseCode, true, false);
            } else {
                NsiSyncManager.instance().dao().finishLog(subSystemLog, null, false, false);
            }
        } catch (Throwable t) {
            t.printStackTrace();
            StringBuilder err = new StringBuilder("||||||||| Some ERROR has occured while processing element(s) at NSI:\n");
            err.append(t.getMessage());

            if (subSystemLog != null)
                NsiSyncManager.instance().dao().finishLog(subSystemLog, err.toString(), true, false);
            getLogger().log(Level.ERROR, err.toString());
            throw new ProcessingException(t.getMessage());
        }

        getLogger().log(Level.INFO, "Executing " + operationType + " for element(s) at NSI was finished");
    }

    public static String getResponseCode(ServiceResponseType response)
    {
        return "Response from NSI:\n" + "\t\tCallCC = " + response.getCallCC().intValue() + "\t\tCallRC = " + response.getCallRC();
    }

    public void executeNSIAction(Long queueItemId, boolean manual) throws RuntimeException
    {
        NsiQueue queueItem = DataAccessServices.dao().getNotNull(NsiQueue.class, queueItemId);
        String systemCode = queueItem.getDestination().getUserCode();
        getLogger().log(Level.INFO, "---------- " + (manual ? "Re-" : "") + "Executing " + queueItem.getLog().getPack().getType() + " for elements at " + systemCode + '.' + (manual ? (" Src message GUID = " + queueItem.getLog().getGuid()) : "") + " ----------");

        NsiSubSystemLog log = queueItem.getLog();
        NsiDeliveryDaemon.setCurrentMessageGuid(log.getGuid());

        try
        {
            ServiceSoap port = getServiceSoap();

            // Принудительное прерывание
            if(NsiDeliveryDaemon.isPackExecutionShouldBeInterrupted(log.getGuid()))
            {
                NsiDeliveryDaemon.finalizeMessageProcessing(log.getGuid());
                return;
            }

            String comment = manual ? ("Повторная отправка пакета " + queueItem.getLog().getGuid()) : null;
            log = NsiSyncManager.instance().dao().updateStartOutcomingSubsystemLog(log, true, false, true, comment, null, null);

            ServiceRequestType request = NsiUtils.createServiceRequestType(queueItem, NsiRequestHandlerService.instance().getSettings().isAsync());
            ServiceResponseType response;

            try {
                response = performRequest(port, request, queueItem.getLog().getPack().getType());
            } catch (Exception e) {
                log = NsiSyncManager.instance().dao().updateStartOutcomingSubsystemLog(log, false, false, false, e.toString(), e, null);
                throw new ProcessingException(e);
            }

            if (null != response)
            {
                String err = getResponseCode(response);
                if (NsiUtils.RESPONSE_CODE_NUMBER_ERROR.equals(response.getCallCC()))
                    getLogger().log(Level.ERROR, err);
                log = NsiSyncManager.instance().dao().updateStartOutcomingSubsystemLog(log, false, false, false, err, null, response);
            }
            else
            {
                log = NsiSyncManager.instance().dao().updateStartOutcomingSubsystemLog(log, false, false, false, "Адресат не доступен", null, null);
            }

        } catch (Exception t)
        {
            t.printStackTrace();
            StringBuilder err = new StringBuilder("||||||||| Some ERROR has occured while processing elements at " + systemCode + ":\n");
            err.append(t.getMessage());

            getLogger().log(Level.ERROR, err.toString());
            NsiSyncManager.instance().dao().updateStartOutcomingSubsystemLog(log, false, false, false, err.toString(), t, null);
        }

        getLogger().log(Level.INFO, "---------- " + (manual ? "Re-" : "") + "Executing " + queueItem.getLog().getPack().getType() + " for elements at " + systemCode + " was finished." + (manual ? (" Src message GUID = " + queueItem.getLog().getGuid()) : "") + " ----------");
    }
}