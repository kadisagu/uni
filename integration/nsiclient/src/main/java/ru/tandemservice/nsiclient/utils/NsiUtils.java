/* $Id: NsiUtils.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.utils;

import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.common.reflect.ClassPath;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.commons.lang.StringUtils;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.springframework.web.util.HtmlUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView.NsiCatalogElementView;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.NsiPackageManager;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.NsiSubSystemLogManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.codes.NsiSubSystemCodes;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.tapestry.formatter.XmlFormatter;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.Datagram;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.RoutingHeaderType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceRequestType;

import javax.xml.bind.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 09.08.2013
 */
public class NsiUtils
{
    public static IXDatagram createXDatagram()
    {
        INsiBean bean = (INsiBean) ApplicationRuntime.getBean("nsiBean");
        return bean.createXDatagram();
    }

    public static final String OPERATION_TYPE_RETRIEVE = "retrieve";
    public static final String OPERATION_TYPE_INSERT = "insert";
    public static final String OPERATION_TYPE_UPDATE = "update";
    public static final String OPERATION_TYPE_DELETE = "delete";
    public static final String OPERATION_TYPE_DELIVERY = "delivery";

    public static final BigInteger RESPONSE_CODE_NUMBER_SUCCESS = BigInteger.valueOf(0L);
    public static final BigInteger RESPONSE_CODE_NUMBER_WARN = BigInteger.valueOf(1L);
    public static final BigInteger RESPONSE_CODE_NUMBER_ERROR = BigInteger.valueOf(2L);
    public static final BigInteger RESPONSE_CODE_NUMBER_ASYNC_RESPONSE = BigInteger.valueOf(3L);

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String TRUE = "1";
    public static final String FALSE = "0";

    public static final String TYPE_ASSISTANT_VICE = "Заместитель";
    public static final String TYPE_ASSISTANT_HELPER = "Помощник";

    public static final String TYPE_SEX_MALE = "Мужской";
    public static final String TYPE_SEX_FEMALE = "Женский";

    public static String generateGUID(Long entityId)
    {
        return UUID.nameUUIDFromBytes(String.valueOf(entityId).getBytes(Charsets.UTF_8)).toString();
    }

    private static final HashMap<String, JAXBContext> classNameToJaxbContext = new HashMap<>();

    private static final Unmarshaller XDATAGRAM_UNMARSHALLER = createUnmarshaller();

    private static Unmarshaller createUnmarshaller()
    {
        try
        {
            List<Class> classList = new ArrayList<>();
            Set<ClassPath.ClassInfo> classInfos = ClassPath.from(IXDatagram.class.getClassLoader()).getTopLevelClasses(IXDatagram.class.getPackage().getName());
            for (ClassPath.ClassInfo classInfo : classInfos)
            {
                Class clazz = classInfo.load();
                if (!clazz.isInterface())
                    classList.add(clazz);
            }
            JAXBContext jc = JAXBContext.newInstance(classList.toArray(new Class[classList.size()]));
            return jc.createUnmarshaller();
        }
        catch (IOException | JAXBException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Восстанавливает из планарного xml-документа реальные объекты датаграммы.
     * Используется для получения датаграммы пакета при повторе обработки сохраненного в БД пакета.
     *
     * @param xml - исходный планарный xml-документ в виде массива byte
     * @return - восстановленный объект.
     */
    public static IXDatagram fromXml(byte[] xml)
    {
        try
        {
            Object object = XDATAGRAM_UNMARSHALLER.unmarshal(new ByteArrayInputStream(xml));
            return IXDatagram.class.cast(object);
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static <T> T fromXml(Class<T> clazz, byte[] xml)
    {
        try
        {
            if (!classNameToJaxbContext.containsKey(clazz.getName()))
                classNameToJaxbContext.put(clazz.getName(), JAXBContext.newInstance(clazz));

            JAXBContext jc = classNameToJaxbContext.get(clazz.getName());
            Unmarshaller u = jc.createUnmarshaller();
            Object object = u.unmarshal(new ByteArrayInputStream(xml));

            if (object instanceof JAXBElement)
            {
                JAXBElement element = (JAXBElement) object;
                if ("error".equalsIgnoreCase(element.getName().getLocalPart()))
                {
                    throw null;//IFisService.FisError(element, element.getValue().toString()); // корневой тег с ошибкой
                }
            }

            return clazz.cast(object);
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static byte[] toXml(Object root)
    {
        try
        {
            /*if (!classNameToJaxbContext.containsKey(root.getClass().getName()))
                classNameToJaxbContext.put(root.getClass().getName(), JAXBContext.newInstance(root.getClass()));*/

            JAXBContext jc = JAXBContext.newInstance(root.getClass()); //classNameToJaxbContext.get(root.getClass().getName());
            Marshaller m = jc.createMarshaller();
            //m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            //m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            return ba.toByteArray();
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static String toXML(ElementNSImpl document) throws IOException
    {
        OutputFormat format = new OutputFormat();
        format.setLineWidth(65);
        format.setIndenting(true);
        format.setIndent(2);
        Writer out = new StringWriter();
        XMLSerializer serializer = new XMLSerializer(out, format);
        serializer.serialize(document);

        return out.toString();
    }

    public static List<IDatagramObject> getDatagramElements(Datagram datagram) throws Exception
    {
        if(datagram == null)
            return null;
        ElementNSImpl respMsg = datagram.getContent().isEmpty() ? null : (ElementNSImpl) datagram.getContent().get(0);
        if (null != respMsg)
        {
            IXDatagram respDatagram = fromXml(toXML(respMsg).getBytes(Charsets.UTF_8));
            return respDatagram.getEntityList();
        }
        return null;
    }

    /**
     * Формирует файл пакета с описанием основных атрибутов пакета и результатов обработки.
     * Данный файл в последствии может быть загружен извне в систему и выполнен
     *
     * @param pack - пакет
     * @return - содержимое файла пакета
     */
    public static String createPackFile(NsiPackage pack)
    {
        StringBuilder resultBody = new StringBuilder();
        DataWrapper wrapper = NsiPackageManager.instance().messageStatusExtPoint().getItem(Long.toString(pack.getStatus()));
        String statusString = wrapper != null ? wrapper.getTitle() : "Не известен";

        String direction = pack.isIncoming() ? NsiSyncManager.DIRECTION_IN.toString() : NsiSyncManager.DIRECTION_OUT.toString();
        wrapper = NsiSyncManager.instance().directionExtPoint().getItem(pack.isIncoming() ? NsiSyncManager.DIRECTION_IN.toString() : NsiSyncManager.DIRECTION_OUT.toString());
        String directionString = wrapper != null ? wrapper.getTitle() : "Не известно";

        Pair<String, String> pair = NsiPackageManager.instance().dao().findElementsAndCatalogs(pack);

        resultBody
                .append("Дата формирования пакета:                                                ").append(NsiPackageManager.DATE_FORMATTER_WITH_MILLIS.format(pack.getCreationDate())).append("\r\n")
                .append("Тип:                                                                     ").append(direction).append(" (").append(directionString).append(')').append("\r\n")
                .append("Справочники НСИ:                                                         ").append(pair.getY()).append("\r\n")
                .append("Идентификаторы элементов справочника:                                    ").append(pair.getX()).append("\r\n")
                .append("Тип операции с данными:                                                  ").append(pack.getType()).append("\r\n")
                .append("Код системы-отправителя сообщения:                                       ").append(pack.getSource()).append("\r\n")
                .append("Коды систем-получателей сообщения:                                       ").append(StringUtils.defaultString(pack.getDestination())).append("\r\n")
                .append("Статус обработки:                                                        ").append(pack.getStatus()).append(" (").append(statusString).append(')').append("\r\n\r\n")
                .append("Тело сообщения:                                                        \r\n").append(StringUtils.defaultString(getXmlFormatted(pack.getBody()))).append("\r\n\r\n");

        return resultBody.toString();
    }

    public static String createPackFile(NsiSubSystemLog log)
    {
        StringBuilder resultBody = new StringBuilder();
        DataWrapper wrapper = NsiSubSystemLogManager.instance().messageStatusExtPoint().getItem(Long.toString(log.getPack().getStatus()));
        String statusString = wrapper != null ? wrapper.getTitle() : "Не известен";

        String direction = log.getPack().isIncoming() ? NsiSyncManager.DIRECTION_IN.toString() : NsiSyncManager.DIRECTION_OUT.toString();
        wrapper = NsiSyncManager.instance().directionExtPoint().getItem(log.getPack().isIncoming() ? NsiSyncManager.DIRECTION_IN.toString() : NsiSyncManager.DIRECTION_OUT.toString());
        String directionString = wrapper != null ? wrapper.getTitle() : "Не известно";

        Pair<String, String> pair = NsiPackageManager.instance().dao().findElementsAndCatalogs(log.getPack());

        resultBody
                .append("Дата приема/отправки сообщения:                                          ").append(NsiPackageManager.DATE_FORMATTER_WITH_MILLIS.format(log.getStartDate())).append("\r\n")
                .append("Дата завершения обработки:                                               ").append(NsiPackageManager.DATE_FORMATTER_WITH_MILLIS.format(log.getEndDate())).append("\r\n")
                .append("Тип:                                                                     ").append(direction).append(" (").append(directionString).append(')').append("\r\n")
                .append("Справочники НСИ:                                                         ").append(pair.getY()).append("\r\n")
                .append("Идентификаторы элементов справочника:                                    ").append(pair.getX()).append("\r\n")
                .append("Тип операции с данными:                                                  ").append(log.getPack().getType()).append("\r\n")
                .append("Идентификатор сообщения:                                                 ").append(log.getGuid()).append("\r\n")
//                .append("Идентификатор сообщения, ответом на которое является это сообщение:    ").append(StringUtils.defaultString(log.getCorrelationId())).append("\r\n")
//                .append("Идентификатор родительского сообщения по отношению к этому сообщению:  ").append(StringUtils.defaultString(log.getParentId())).append("\r\n")
                .append("Код системы-отправителя сообщения:                                       ").append(log.getPack().getSource()).append("\r\n")
                .append("Код системы-получателя сообщения:                                        ").append(StringUtils.defaultString(log.getPack().getDestination())).append("\r\n")
                .append("Статус сообщения:                                                        ").append(log.getStatus()).append(" (").append(statusString).append(')').append("\r\n\r\n")
                .append("Комментарий к сообщению:                                                 ").append(StringUtils.defaultString(log.getComment())).append("\r\n\r\n")
                .append("Технический заголовок сообщения:                                       \r\n").append(StringUtils.defaultString(getXmlFormatted(log.getHeader())).replaceAll("x-datagram", "header")).append("\r\n\r\n")
                .append("Тело сообщения:                                                        \r\n").append(StringUtils.defaultString(getXmlFormatted(log.getPack().getBody()))).append("\r\n\r\n")
                .append("SOAP Fault сообщения:                                                  ").append(StringUtils.defaultString(log.getError())).append("\r\n\r\n");

        return resultBody.toString();
    }

    /**
     * Форматирует вулевое значение в строковое представление, пригодное для передачи в датаграмме.
     *
     * @param value - булево значение
     * @return - сконвертированное
     */
    public static String formatBoolean(Boolean value)
    {
        if (null == value)
            return null;
        if (value)
            return NsiUtils.TRUE;
        return NsiUtils.FALSE;
    }

    /**
     * Преобразует строковое предстваление булевого значения датаграммы в булево значение персистентных объектов
     *
     * @param valueStr - строковое значение из датаграммы
     * @return - булево значение для персистентного объекта.
     */
    public static Boolean parseBoolean(String valueStr)
    {
        //todo refactoring for the case with nonBoolean value
        String trimmedValue = StringUtils.trimToNull(valueStr);
        if (null == trimmedValue)
            return null;
        if (NsiUtils.TRUE.equals(trimmedValue))
            return true;
        if (NsiUtils.FALSE.equals(trimmedValue))
            return false;
        return null;
    }

    /* Преобразует строковое предстваление целочисленного значения датаграммы в целочисленное значение персистентных объектов
    *
    * @param valueStr - строковое значение из датаграммы
    * @field - поле для парсинга
    * @return - целочисленное значение для персистентного объекта.
    */
    public static Integer parseInteger(String valueStr, String field) throws ProcessingDatagramObjectException
    {
        String trimmedValue = StringUtils.trimToNull(valueStr);
        if (null == trimmedValue)
        {
            return null;
        }
        try
        {
            return Integer.valueOf(trimmedValue);
        }
        catch (NumberFormatException e)
        {
            throw new ProcessingDatagramObjectException("can't cast " + field + " field to Integer.");
        }
    }

    public static Long parseLong(String valueStr, String field) throws ProcessingDatagramObjectException
    {
        String trimmedValue = StringUtils.trimToNull(valueStr);
        if (null == trimmedValue)
            return null;
        try
        {
            return Long.valueOf(trimmedValue);
        }
        catch (NumberFormatException e)
        {
            throw new ProcessingDatagramObjectException("can't cast " + field + " field to Long.");
        }
    }

    /**
     * Форматриует дату в строковом представлении, пригодном для передачи в датаграмме
     *
     * @param date - дата
     * @return - сконвертированное значение
     */
    public static String formatDate(Date date)
    {
        if (null == date)
            return null;
        return NsiUtils.DATE_FORMAT.format(date);
    }

    /**
     * Преобразует строковое представление даты из датаграммы в дату, пригодную для хранения в БД
     * @param dateStr - строковый вариант даты
     * @param field - поле для парсинга
     * @return - дата
     */
    public static Date parseDate(String dateStr, String field) throws ProcessingDatagramObjectException
    {
        String trimmedValue = StringUtils.trimToNull(dateStr);
        if (null == trimmedValue)
            return null;
        try
        {
            return NsiUtils.DATE_FORMAT.parse(trimmedValue);
        }
        catch (ParseException e)
        {
            throw new ProcessingDatagramObjectException("can't cast " + field + " field to Date. Expected date format - " + DATE_FORMAT.toPattern());
        }
    }

    public static String formatXml(String src, Map<String, Long> guidToIdMap) throws Exception
    {
        if (null == src || src.trim().isEmpty())
            return null;
        String result = XmlFormatter.INSTANCE.format(getXmlFormatted(src));

        if (guidToIdMap != null && !guidToIdMap.isEmpty())
            for (Map.Entry<String, Long> entry : guidToIdMap.entrySet())
            {
                if (entry.getValue() == null) continue;
                Map<String, Object> parameters = Maps.newHashMap();
                parameters.put(NsiCatalogElementView.SELECTED_TAB, NsiCatalogElementView.INFO_TAB);
                parameters.put(IUIPresenter.PUBLISHER_ID, entry.getValue());

                String link = CoreServices.linkFactoryService().getExternalLink(null, NsiCatalogElementView.class.getSimpleName(), parameters, null);
                result = result.replaceAll("&lt;ID&gt;" + entry.getKey() + "&lt;/ID&gt;", "&lt;ID&gt;<a href=\"" + HtmlUtils.htmlEscape(link) + "\">" + entry.getKey() + "</a>&lt;/ID&gt;");
            }

        return result;
    }

    /**
     * Возвращает отформатированный XML-документ.
     * Используется для вывода xml в файл в удобочитаемом виде.
     *
     * @param src - неформатированный xml-документ
     * @return - отформатированный xml-документ
     */
    public static String getXmlFormatted(String src)
    {
        if (null == src || src.trim().isEmpty())
            return null;

        try
        {
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(src));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(is);

            DOMSource domSource = new DOMSource(doc);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING, "Utf8");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);
            return sw.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Форматирует текст для отображения в HTML
     *
     * @param str - исходный текст
     * @return - отформатированный текст
     */
    public static String formatPlainTextForHtml(String str)
    {
        if (null == StringUtils.trimToNull(str))
            return "";
        return str.replaceAll("\t", ".    ").replaceAll("\n", "<br/>").replaceAll(" ", "&nbsp;");
    }

    public static ServiceRequestType createServiceRequestType(String parentMessageId, String operationType, Datagram datagram, boolean isAsync)
    {
        NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.code(), NsiSubSystemCodes.NSI);
        return createServiceRequestType(operationType, generateGUID(new Date().getTime()), parentMessageId, NsiRequestHandlerService.instance().getSettings().getSystemCode(), subSystem.getUserCode(), datagram, isAsync);
    }

    public static Datagram createServiceRequestTypeDatagram(List<IDatagramObject> datagramElements)
    {
        Datagram datagram = new Datagram();
        IXDatagram xDatagram = createXDatagram();
        xDatagram.getEntityList().addAll(datagramElements);
        ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        return datagram;
    }

    public static ServiceRequestType createServiceRequestType(NsiQueue queueItem, boolean isAsync)
    {
        Datagram datagram = fromXml(Datagram.class, queueItem.getLog().getPack().getBody().getBytes(Charsets.UTF_8));
        return createServiceRequestType(queueItem.getLog().getPack().getType(), queueItem.getLog().getGuid(), null, queueItem.getLog().getPack().getSource(), queueItem.getDestination().getUserCode(), datagram, isAsync);
    }

    private static ServiceRequestType createServiceRequestType(String operationType, String messageId, String parentMessageId, String sourceId, String destinationId, Datagram datagram, boolean isAsync)
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setOperationType(operationType);
        header.setMessageId(messageId);
        header.setSourceId(sourceId);
        header.setDestinationId(destinationId);
        header.setParentId(parentMessageId);
        header.setAsync(isAsync);
        header.setElementsCount(BigInteger.valueOf(NsiRequestHandlerService.instance().getSettings().getPackageSize()));

        request.setRoutingHeader(header);
        request.setDatagram(datagram);
        return request;
    }

    public static void appendMessage(StringBuilder log, String message) {
        if(message != null && message.length() > 0) {
            if(log.length() > 0)
                log.append('\n');
            log.append(message);
        }
    }

    public static void appendMessage(StringBuilder log, StringBuilder message) {
        if(message != null && message.length() > 0) {
            if(log.length() > 0)
                log.append('\n');
            log.append(message);
        }
    }
}