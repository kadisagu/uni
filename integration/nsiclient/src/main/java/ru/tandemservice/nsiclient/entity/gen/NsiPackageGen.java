package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiPackageGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiPackage";
    public static final String ENTITY_NAME = "nsiPackage";
    public static final int VERSION_HASH = -667604251;
    private static IEntityMeta ENTITY_META;

    public static final String P_TYPE = "type";
    public static final String P_INCOMING = "incoming";
    public static final String P_BODY = "body";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_SOURCE = "source";
    public static final String P_DESTINATION = "destination";
    public static final String P_STATUS = "status";
    public static final String P_END_DATE = "endDate";
    public static final String P_ERROR = "error";
    public static final String P_WARNING = "warning";

    private String _type;     // Тип операции
    private boolean _incoming;     // Входящий пакет
    private String _body;     // Тело сообщения
    private Date _creationDate;     // Дата и время начала обработки
    private String _source;     // Код подсистемы отправителя
    private String _destination;     // Коды подсистем получателей
    private int _status = 0;     // Статус
    private Date _endDate;     // Дата и время завершения обработки
    private boolean _error = false;     // Есть ошибки
    private boolean _warning = false;     // Есть предупреждения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип операции. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getType()
    {
        return _type;
    }

    /**
     * @param type Тип операции. Свойство не может быть null.
     */
    public void setType(String type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Входящий пакет. Свойство не может быть null.
     */
    @NotNull
    public boolean isIncoming()
    {
        return _incoming;
    }

    /**
     * @param incoming Входящий пакет. Свойство не может быть null.
     */
    public void setIncoming(boolean incoming)
    {
        dirty(_incoming, incoming);
        _incoming = incoming;
    }

    /**
     * @return Тело сообщения.
     */
    public String getBody()
    {
        initLazyForGet("body");
        return _body;
    }

    /**
     * @param body Тело сообщения.
     */
    public void setBody(String body)
    {
        initLazyForSet("body");
        dirty(_body, body);
        _body = body;
    }

    /**
     * @return Дата и время начала обработки. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата и время начала обработки. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Код подсистемы отправителя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSource()
    {
        return _source;
    }

    /**
     * @param source Код подсистемы отправителя. Свойство не может быть null.
     */
    public void setSource(String source)
    {
        dirty(_source, source);
        _source = source;
    }

    /**
     * @return Коды подсистем получателей.
     */
    @Length(max=255)
    public String getDestination()
    {
        return _destination;
    }

    /**
     * @param destination Коды подсистем получателей.
     */
    public void setDestination(String destination)
    {
        dirty(_destination, destination);
        _destination = destination;
    }

    /**
     * @return Статус. Свойство не может быть null.
     */
    @NotNull
    public int getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус. Свойство не может быть null.
     */
    public void setStatus(int status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Дата и время завершения обработки.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата и время завершения обработки.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Есть ошибки. Свойство не может быть null.
     */
    @NotNull
    public boolean isError()
    {
        return _error;
    }

    /**
     * @param error Есть ошибки. Свойство не может быть null.
     */
    public void setError(boolean error)
    {
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return Есть предупреждения. Свойство не может быть null.
     */
    @NotNull
    public boolean isWarning()
    {
        return _warning;
    }

    /**
     * @param warning Есть предупреждения. Свойство не может быть null.
     */
    public void setWarning(boolean warning)
    {
        dirty(_warning, warning);
        _warning = warning;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiPackageGen)
        {
            setType(((NsiPackage)another).getType());
            setIncoming(((NsiPackage)another).isIncoming());
            setBody(((NsiPackage)another).getBody());
            setCreationDate(((NsiPackage)another).getCreationDate());
            setSource(((NsiPackage)another).getSource());
            setDestination(((NsiPackage)another).getDestination());
            setStatus(((NsiPackage)another).getStatus());
            setEndDate(((NsiPackage)another).getEndDate());
            setError(((NsiPackage)another).isError());
            setWarning(((NsiPackage)another).isWarning());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiPackageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiPackage.class;
        }

        public T newInstance()
        {
            return (T) new NsiPackage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "incoming":
                    return obj.isIncoming();
                case "body":
                    return obj.getBody();
                case "creationDate":
                    return obj.getCreationDate();
                case "source":
                    return obj.getSource();
                case "destination":
                    return obj.getDestination();
                case "status":
                    return obj.getStatus();
                case "endDate":
                    return obj.getEndDate();
                case "error":
                    return obj.isError();
                case "warning":
                    return obj.isWarning();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((String) value);
                    return;
                case "incoming":
                    obj.setIncoming((Boolean) value);
                    return;
                case "body":
                    obj.setBody((String) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "source":
                    obj.setSource((String) value);
                    return;
                case "destination":
                    obj.setDestination((String) value);
                    return;
                case "status":
                    obj.setStatus((Integer) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "error":
                    obj.setError((Boolean) value);
                    return;
                case "warning":
                    obj.setWarning((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "incoming":
                        return true;
                case "body":
                        return true;
                case "creationDate":
                        return true;
                case "source":
                        return true;
                case "destination":
                        return true;
                case "status":
                        return true;
                case "endDate":
                        return true;
                case "error":
                        return true;
                case "warning":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "incoming":
                    return true;
                case "body":
                    return true;
                case "creationDate":
                    return true;
                case "source":
                    return true;
                case "destination":
                    return true;
                case "status":
                    return true;
                case "endDate":
                    return true;
                case "error":
                    return true;
                case "warning":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return String.class;
                case "incoming":
                    return Boolean.class;
                case "body":
                    return String.class;
                case "creationDate":
                    return Date.class;
                case "source":
                    return String.class;
                case "destination":
                    return String.class;
                case "status":
                    return Integer.class;
                case "endDate":
                    return Date.class;
                case "error":
                    return Boolean.class;
                case "warning":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiPackage> _dslPath = new Path<NsiPackage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiPackage");
    }
            

    /**
     * @return Тип операции. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getType()
     */
    public static PropertyPath<String> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Входящий пакет. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#isIncoming()
     */
    public static PropertyPath<Boolean> incoming()
    {
        return _dslPath.incoming();
    }

    /**
     * @return Тело сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getBody()
     */
    public static PropertyPath<String> body()
    {
        return _dslPath.body();
    }

    /**
     * @return Дата и время начала обработки. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Код подсистемы отправителя. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getSource()
     */
    public static PropertyPath<String> source()
    {
        return _dslPath.source();
    }

    /**
     * @return Коды подсистем получателей.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getDestination()
     */
    public static PropertyPath<String> destination()
    {
        return _dslPath.destination();
    }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getStatus()
     */
    public static PropertyPath<Integer> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Дата и время завершения обработки.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Есть ошибки. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#isError()
     */
    public static PropertyPath<Boolean> error()
    {
        return _dslPath.error();
    }

    /**
     * @return Есть предупреждения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#isWarning()
     */
    public static PropertyPath<Boolean> warning()
    {
        return _dslPath.warning();
    }

    public static class Path<E extends NsiPackage> extends EntityPath<E>
    {
        private PropertyPath<String> _type;
        private PropertyPath<Boolean> _incoming;
        private PropertyPath<String> _body;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<String> _source;
        private PropertyPath<String> _destination;
        private PropertyPath<Integer> _status;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _error;
        private PropertyPath<Boolean> _warning;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип операции. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getType()
     */
        public PropertyPath<String> type()
        {
            if(_type == null )
                _type = new PropertyPath<String>(NsiPackageGen.P_TYPE, this);
            return _type;
        }

    /**
     * @return Входящий пакет. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#isIncoming()
     */
        public PropertyPath<Boolean> incoming()
        {
            if(_incoming == null )
                _incoming = new PropertyPath<Boolean>(NsiPackageGen.P_INCOMING, this);
            return _incoming;
        }

    /**
     * @return Тело сообщения.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getBody()
     */
        public PropertyPath<String> body()
        {
            if(_body == null )
                _body = new PropertyPath<String>(NsiPackageGen.P_BODY, this);
            return _body;
        }

    /**
     * @return Дата и время начала обработки. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(NsiPackageGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Код подсистемы отправителя. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getSource()
     */
        public PropertyPath<String> source()
        {
            if(_source == null )
                _source = new PropertyPath<String>(NsiPackageGen.P_SOURCE, this);
            return _source;
        }

    /**
     * @return Коды подсистем получателей.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getDestination()
     */
        public PropertyPath<String> destination()
        {
            if(_destination == null )
                _destination = new PropertyPath<String>(NsiPackageGen.P_DESTINATION, this);
            return _destination;
        }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getStatus()
     */
        public PropertyPath<Integer> status()
        {
            if(_status == null )
                _status = new PropertyPath<Integer>(NsiPackageGen.P_STATUS, this);
            return _status;
        }

    /**
     * @return Дата и время завершения обработки.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(NsiPackageGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Есть ошибки. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#isError()
     */
        public PropertyPath<Boolean> error()
        {
            if(_error == null )
                _error = new PropertyPath<Boolean>(NsiPackageGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return Есть предупреждения. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackage#isWarning()
     */
        public PropertyPath<Boolean> warning()
        {
            if(_warning == null )
                _warning = new PropertyPath<Boolean>(NsiPackageGen.P_WARNING, this);
            return _warning;
        }

        public Class getEntityClass()
        {
            return NsiPackage.class;
        }

        public String getEntityName()
        {
            return "nsiPackage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
