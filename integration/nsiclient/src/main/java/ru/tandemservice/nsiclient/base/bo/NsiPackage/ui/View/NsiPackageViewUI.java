package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.View.NsiCatalogView;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.NsiPackageManager;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.support.NsiPackageWrapper;
import ru.tandemservice.nsiclient.entity.NsiPackage;

import java.text.MessageFormat;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
@State({
        @Bind(key = NsiPackageView.SELECTED_TAB, binding = "selectedTab")
})
public class NsiPackageViewUI extends UIPresenter
{
    private Long _id;
    private String _selectedTab;
    private NsiPackageWrapper _pack;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Map<String, Object> getTabParameter()
    {
        return ParametersMap.createWith(UIPresenter.PUBLISHER_ID, _id);
    }

    public String getPageTitle()
    {
        return MessageFormat.format(getConfig().getProperty("page.title"), _pack.getPack().getType(), _pack.getPack().getSource(), NsiPackageManager.DATE_FORMATTER_WITH_MILLIS.format(_pack.getPack().getCreationDate()));
    }

    public void onChangeTab()
    {
        _uiSettings.set(NsiCatalogView.SELECTED_TAB, _selectedTab);
        saveSettings();
    }

    @Override
    public void onComponentActivate()
    {
        if(_selectedTab == null)
            _selectedTab = _uiSettings.get(NsiCatalogView.SELECTED_TAB);
        NsiPackage pack = DataAccessServices.dao().get(NsiPackage.class, _id);
        _pack = new NsiPackageWrapper(pack, "", "");
    }
}