package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Журнал НСИ для сущности НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiEntityLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiEntityLog";
    public static final String ENTITY_NAME = "nsiEntityLog";
    public static final int VERSION_HASH = 1058306499;
    private static IEntityMeta ENTITY_META;

    public static final String P_ERROR = "error";
    public static final String P_WARNING = "warning";
    public static final String P_CATALOG_TYPE = "catalogType";
    public static final String P_GUID = "guid";
    public static final String L_ENTITY = "entity";
    public static final String L_PACK = "pack";

    private String _error;     // Описание ошибки
    private String _warning;     // Описание предупреждения
    private String _catalogType;     // Код справочника НСИ
    private String _guid;     // GUID
    private NsiEntity _entity;     // Сущность НСИ
    private NsiPackage _pack;     // Пакет НСИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Описание ошибки.
     */
    @Length(max=255)
    public String getError()
    {
        return _error;
    }

    /**
     * @param error Описание ошибки.
     */
    public void setError(String error)
    {
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return Описание предупреждения.
     */
    @Length(max=255)
    public String getWarning()
    {
        return _warning;
    }

    /**
     * @param warning Описание предупреждения.
     */
    public void setWarning(String warning)
    {
        dirty(_warning, warning);
        _warning = warning;
    }

    /**
     * @return Код справочника НСИ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCatalogType()
    {
        return _catalogType;
    }

    /**
     * @param catalogType Код справочника НСИ. Свойство не может быть null.
     */
    public void setCatalogType(String catalogType)
    {
        dirty(_catalogType, catalogType);
        _catalogType = catalogType;
    }

    /**
     * @return GUID.
     */
    @Length(max=36)
    public String getGuid()
    {
        return _guid;
    }

    /**
     * @param guid GUID.
     */
    public void setGuid(String guid)
    {
        dirty(_guid, guid);
        _guid = guid;
    }

    /**
     * @return Сущность НСИ.
     */
    public NsiEntity getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Сущность НСИ.
     */
    public void setEntity(NsiEntity entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Пакет НСИ. Свойство не может быть null.
     */
    @NotNull
    public NsiPackage getPack()
    {
        return _pack;
    }

    /**
     * @param pack Пакет НСИ. Свойство не может быть null.
     */
    public void setPack(NsiPackage pack)
    {
        dirty(_pack, pack);
        _pack = pack;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiEntityLogGen)
        {
            setError(((NsiEntityLog)another).getError());
            setWarning(((NsiEntityLog)another).getWarning());
            setCatalogType(((NsiEntityLog)another).getCatalogType());
            setGuid(((NsiEntityLog)another).getGuid());
            setEntity(((NsiEntityLog)another).getEntity());
            setPack(((NsiEntityLog)another).getPack());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiEntityLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiEntityLog.class;
        }

        public T newInstance()
        {
            return (T) new NsiEntityLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "error":
                    return obj.getError();
                case "warning":
                    return obj.getWarning();
                case "catalogType":
                    return obj.getCatalogType();
                case "guid":
                    return obj.getGuid();
                case "entity":
                    return obj.getEntity();
                case "pack":
                    return obj.getPack();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "error":
                    obj.setError((String) value);
                    return;
                case "warning":
                    obj.setWarning((String) value);
                    return;
                case "catalogType":
                    obj.setCatalogType((String) value);
                    return;
                case "guid":
                    obj.setGuid((String) value);
                    return;
                case "entity":
                    obj.setEntity((NsiEntity) value);
                    return;
                case "pack":
                    obj.setPack((NsiPackage) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "error":
                        return true;
                case "warning":
                        return true;
                case "catalogType":
                        return true;
                case "guid":
                        return true;
                case "entity":
                        return true;
                case "pack":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "error":
                    return true;
                case "warning":
                    return true;
                case "catalogType":
                    return true;
                case "guid":
                    return true;
                case "entity":
                    return true;
                case "pack":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "error":
                    return String.class;
                case "warning":
                    return String.class;
                case "catalogType":
                    return String.class;
                case "guid":
                    return String.class;
                case "entity":
                    return NsiEntity.class;
                case "pack":
                    return NsiPackage.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiEntityLog> _dslPath = new Path<NsiEntityLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiEntityLog");
    }
            

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getError()
     */
    public static PropertyPath<String> error()
    {
        return _dslPath.error();
    }

    /**
     * @return Описание предупреждения.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getWarning()
     */
    public static PropertyPath<String> warning()
    {
        return _dslPath.warning();
    }

    /**
     * @return Код справочника НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getCatalogType()
     */
    public static PropertyPath<String> catalogType()
    {
        return _dslPath.catalogType();
    }

    /**
     * @return GUID.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getGuid()
     */
    public static PropertyPath<String> guid()
    {
        return _dslPath.guid();
    }

    /**
     * @return Сущность НСИ.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getEntity()
     */
    public static NsiEntity.Path<NsiEntity> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Пакет НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getPack()
     */
    public static NsiPackage.Path<NsiPackage> pack()
    {
        return _dslPath.pack();
    }

    public static class Path<E extends NsiEntityLog> extends EntityPath<E>
    {
        private PropertyPath<String> _error;
        private PropertyPath<String> _warning;
        private PropertyPath<String> _catalogType;
        private PropertyPath<String> _guid;
        private NsiEntity.Path<NsiEntity> _entity;
        private NsiPackage.Path<NsiPackage> _pack;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getError()
     */
        public PropertyPath<String> error()
        {
            if(_error == null )
                _error = new PropertyPath<String>(NsiEntityLogGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return Описание предупреждения.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getWarning()
     */
        public PropertyPath<String> warning()
        {
            if(_warning == null )
                _warning = new PropertyPath<String>(NsiEntityLogGen.P_WARNING, this);
            return _warning;
        }

    /**
     * @return Код справочника НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getCatalogType()
     */
        public PropertyPath<String> catalogType()
        {
            if(_catalogType == null )
                _catalogType = new PropertyPath<String>(NsiEntityLogGen.P_CATALOG_TYPE, this);
            return _catalogType;
        }

    /**
     * @return GUID.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getGuid()
     */
        public PropertyPath<String> guid()
        {
            if(_guid == null )
                _guid = new PropertyPath<String>(NsiEntityLogGen.P_GUID, this);
            return _guid;
        }

    /**
     * @return Сущность НСИ.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getEntity()
     */
        public NsiEntity.Path<NsiEntity> entity()
        {
            if(_entity == null )
                _entity = new NsiEntity.Path<NsiEntity>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Пакет НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiEntityLog#getPack()
     */
        public NsiPackage.Path<NsiPackage> pack()
        {
            if(_pack == null )
                _pack = new NsiPackage.Path<NsiPackage>(L_PACK, this);
            return _pack;
        }

        public Class getEntityClass()
        {
            return NsiEntityLog.class;
        }

        public String getEntityName()
        {
            return "nsiEntityLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
