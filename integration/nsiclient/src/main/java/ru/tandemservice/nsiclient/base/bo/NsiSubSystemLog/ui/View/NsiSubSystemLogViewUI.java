package ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.ui.View;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.util.HtmlUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView.NsiCatalogElementView;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.View.NsiPackageView;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.tapestry.formatter.XmlFormatter;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
public class NsiSubSystemLogViewUI extends UIPresenter
{
    private Long _id;
    private NsiSubSystemLog _log;
    private String _catalogs;
    private String _elements;
    private String _status;
    private String _body;
    private String _header;
    private boolean _error;
    private String _additionalStatus;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public void onComponentActivate()
    {
        _log = DataAccessServices.dao().getNotNull(NsiSubSystemLog.class, _id);
        _error = StringUtils.isNotEmpty(_log.getError());

        DQLSelectBuilder entityBuilder = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e")
                .column(property("e", NsiEntityLog.entity()))
                .column(property("e", NsiEntityLog.entity().entityType()))
                .where(eq(property("e", NsiEntityLog.pack().id()), commonValue(_log.getPack().getId(), PropertyType.LONG)))
                .order(property("e", NsiEntityLog.entity().entityType()));
        List<Object[]> objectList = entityBuilder.createStatement(getSupport().getSession()).list();
        Set<String> catalogs = Sets.newTreeSet();
        Map<String, Long> guidToIdMap = new HashMap<>();

        List<String> links = Lists.newArrayListWithExpectedSize(objectList.size());
        for(Object[] obj: objectList)
        {
            NsiEntity entity = (NsiEntity) obj[0];
            catalogs.add((String) obj[1]);
            guidToIdMap.put(entity.getGuid(), entity.getId());
            Map<String, Object> parameters = Maps.newHashMap();
            parameters.put(IUIPresenter.PUBLISHER_ID, entity.getId());
            parameters.put(NsiCatalogElementView.SELECTED_TAB, NsiCatalogElementView.INFO_TAB);
            String link = CoreServices.linkFactoryService().getLink(null, NsiCatalogElementView.class.getSimpleName(), parameters, null, false);
            links.add("<a href=\"" + HtmlUtils.htmlEscape(link) + "\">" + entity.getGuid() + "</a>");
        }

        _catalogs = Joiner.on(", ").join(catalogs);
        String fullElements = Joiner.on(", ").join(links);
        if (links.size() > 3) {
            String id = "divId";
            String link = "<script>jQuery(\'#" + id + "\').click(function() {var html = \'" + fullElements.replaceAll("\"", "&quot;") + "\';$(this).html(html);});</script>";
            _elements = "<div id=\"" +id + "\">" + Joiner.on(", ").join(links.subList(0, 3)) + "&nbsp;..." + "</div>" + link;
        } else
            _elements = fullElements;

        String style = _log.getAdditionalStatus() == NsiSubSystemLog.ADDITIONAL_STATUS_ERROR ? "color:red;" : _log.getAdditionalStatus() == NsiSubSystemLog.ADDITIONAL_STATUS_WARNING ? "color:#CCCC00;" : null;
        _status = "<span " + (style != null ? "style=\"" + style + "\">": ">") + _log.getMessageStatus() + "</span>";
        _additionalStatus = "<span " + (style != null ? "style=\"" + style + "\">" : ">") + _log.getMessageAdditionalStatus() + "</span>";
        _body = _log.getPack().getBody();
        try {
            _body = NsiUtils.formatXml(_body, guidToIdMap);
        } catch (Exception e) {
        }

        _header = _log.getHeader();
        try {
            _header = XmlFormatter.INSTANCE.format(NsiUtils.getXmlFormatted(_header));
        } catch (Exception e) {
        }
    }

    public void onSend()
    {
        // отключаем логирование в базу и прочие системные штуки
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

        try
        {
            if (!_log.getPack().isIncoming())
            {
                Long queueItemId = NsiSyncManager.instance().dao().createQueueItemForLogRow(_log);
                NsiRequestHandlerService.instance().executeNSIAction(queueItemId, true);
            } else
            {
                // TODO повторная обработка входящих пакетов
            }
        } catch (Throwable t)
        {
            Debug.exception(t.getMessage(), t);
            throw t;
        } finally
        {
            // включаем штуки и логирование обратно
            Debug.resumeLogging();
            eventLock.release();
        }
    }

    public String getSendAlert()
    {
        return MessageFormat.format(getConfig().getProperty("button.onSend.alert"), _log.getDestination());
    }

    public void onGetPack()
    {
        String text = NsiUtils.createPackFile(_log);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt").fileName(_log.getGuid() + ".txt").document(text.getBytes()), true);
    }

    public String getCommentFormatted()
    {
        return NsiUtils.formatPlainTextForHtml(_log.getComment());
    }

    public String getErrorFormatted()
    {
        return NsiUtils.formatPlainTextForHtml(_log.getError());
    }

    public String getStickerTitle()
    {
        String packType = _log.getPack().isIncoming() ? "прием" : "отправка";
        String operationType = _log.getPack().getType();
        return "Запись лога от "
                + SimpleDateFormat.getDateInstance(DateFormat.SHORT).format(_log.getStartDate())
                + ": " + packType
                + " пакета на операцию типа "
                + operationType
                + " от подсистемы "
                + _log.getPack().getSource();
    }

    public String getPackType()
    {
        return _log.getPack().isIncoming() ? "Входящий" : "Исходящий";
    }

    public Map getPackParameterMap()
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, _log.getPack().getId())
                .add(NsiPackageView.SELECTED_TAB, NsiPackageView.INFO_TAB);
    }
}