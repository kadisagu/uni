/* $Id: ElementWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.nsiclient.entity.NsiEntity;

/**
 * @author Andrey Nikonov
 * @since 23.06.2015
 */
public class ElementWrapper extends DataWrapper
{
    public static final String ENTITY = "entity";
    public static final String NSI_ENTITY = "nsiEntity";

    private IEntity _entity;
    private NsiEntity _nsiEntity;

    public ElementWrapper(IEntity entity, NsiEntity nsiEntity) {
        super(nsiEntity.getId(), null);
        _entity = entity;
        _nsiEntity = nsiEntity;
    }

    public IEntity getEntity() {
        return _entity;
    }

    public NsiEntity getNsiEntity() {
        return _nsiEntity;
    }
}
