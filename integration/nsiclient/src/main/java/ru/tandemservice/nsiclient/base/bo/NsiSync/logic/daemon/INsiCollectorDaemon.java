/* $Id: INsiCollectorDaemon.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2015
 */
public interface INsiCollectorDaemon
{
    final String GLOBAL_DAEMON_LOCK = INsiCollectorDaemon.class.getName() + ".global-lock";
    final SpringBeanCache<INsiCollectorDaemon> instance = new SpringBeanCache<>(INsiCollectorDaemon.class.getName());

    void registerChangedEntities(Long... ids);
    void registerDeletedEntities(Long... ids);
    void registerIgnoredEntities(Long... ids);
    void removeEntities(Long... ids);
}