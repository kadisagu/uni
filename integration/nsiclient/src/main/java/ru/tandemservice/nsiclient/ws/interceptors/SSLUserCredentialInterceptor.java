/* $Id$ */
package ru.tandemservice.nsiclient.ws.interceptors;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.transport.TLSSessionInfo;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.AuthSettings.NsiSettingsAuthSettingsUI;
import ru.tandemservice.nsiclient.entity.NsiCredential;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceRequestType;

import javax.security.auth.x500.X500Principal;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 19.06.2015
 */
public class SSLUserCredentialInterceptor extends AbstractSoapInterceptor
{
    public SSLUserCredentialInterceptor()
    {
        super(Phase.PRE_INVOKE);
    }

    @Override
    public void handleMessage(SoapMessage soapMessage) throws Fault
    {
        String isSSL = ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_SSL);
        if (null == isSSL)
        {
            throw new Fault(new Exception("Property \""+NsiSettingsAuthSettingsUI.CLIENT_SSL+"\" can not be found"));
        }
        if (Boolean.valueOf(isSSL))
        {
            List content = soapMessage.getContent(List.class);
            String subSystemCode = ((ServiceRequestType) content.get(0)).getRoutingHeader().getSourceId();

            if (soapMessage.get("org.apache.cxf.security.transport.TLSSessionInfo") == null
                    || ((TLSSessionInfo) soapMessage.get("org.apache.cxf.security.transport.TLSSessionInfo")).getPeerCertificates() == null)
            {
                throw new Fault(new Exception("X509 certificate can not be found"));
            }

            X509Certificate[] certs = (X509Certificate[]) ((TLSSessionInfo) soapMessage.get("org.apache.cxf.security.transport.TLSSessionInfo")).getPeerCertificates();


            X509Certificate clientCert = certs[0];
            X500Principal principal = clientCert.getSubjectX500Principal();
            String fingerPrint = "";
            NsiCredential credential = null;
            try
            {
                fingerPrint = DigestUtils.shaHex(clientCert.getEncoded());
                credential = DataAccessServices.dao().get(NsiCredential.class, NsiCredential.P_FINGER_PRINT, fingerPrint);
            }
            catch (CertificateEncodingException e)
            {
                e.printStackTrace();
            }


            if (credential == null || !credential.getSubsystemCode().equals("MuleEsb"))
            {
                if (credential == null)
                {
                    throw new Fault(new Exception("Authentication failed (can't fount ssl credentials in session)"));
                }
                else if (!credential.getPrincipal().equals(principal.getName())
                        || !credential.getSubsystemCode().equals(subSystemCode))
                {
                   throw  new Fault(new Exception("Authentication failed"));
                }

            }
        }
    }
}
