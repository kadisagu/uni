package ru.tandemservice.nsiclient.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Справочники НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiCatalogTypeGen extends EntityBase
 implements INaturalIdentifiable<NsiCatalogTypeGen>, org.tandemframework.common.catalog.entity.IActiveCatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType";
    public static final String ENTITY_NAME = "nsiCatalogType";
    public static final int VERSION_HASH = -2016142836;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_NSI_CODE = "nsiCode";
    public static final String P_SYNC_DATE = "syncDate";
    public static final String P_READ_ALLOWED = "readAllowed";
    public static final String P_READ_WRITE_ALLOWED = "readWriteAllowed";
    public static final String P_AUTO_DELIVERY_CHANGES_ALLOWED = "autoDeliveryChangesAllowed";
    public static final String P_TITLE = "title";
    public static final String P_USER_CODE = "userCode";
    public static final String P_DISABLED_DATE = "disabledDate";
    public static final String P_ENABLED = "enabled";

    private String _code;     // Системный код
    private String _nsiCode;     // Тип справочника
    private Date _syncDate;     // Дата и время синхронизации
    private boolean _readAllowed = true;     // Разрешено чтение из НСИ
    private boolean _readWriteAllowed = true;     // Разрешены чтение и запись в НСИ
    private boolean _autoDeliveryChangesAllowed = false;     // Разрешена отсылка изменений в НСИ
    private String _title;     // Название
    private String _userCode;     // Пользовательский код
    private Date _disabledDate;     // Дата запрещения
    

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип справочника. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNsiCode()
    {
        return _nsiCode;
    }

    /**
     * @param nsiCode Тип справочника. Свойство не может быть null.
     */
    public void setNsiCode(String nsiCode)
    {
        dirty(_nsiCode, nsiCode);
        _nsiCode = nsiCode;
    }

    /**
     * @return Дата и время синхронизации.
     */
    public Date getSyncDate()
    {
        return _syncDate;
    }

    /**
     * @param syncDate Дата и время синхронизации.
     */
    public void setSyncDate(Date syncDate)
    {
        dirty(_syncDate, syncDate);
        _syncDate = syncDate;
    }

    /**
     * @return Разрешено чтение из НСИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isReadAllowed()
    {
        return _readAllowed;
    }

    /**
     * @param readAllowed Разрешено чтение из НСИ. Свойство не может быть null.
     */
    public void setReadAllowed(boolean readAllowed)
    {
        dirty(_readAllowed, readAllowed);
        _readAllowed = readAllowed;
    }

    /**
     * @return Разрешены чтение и запись в НСИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isReadWriteAllowed()
    {
        return _readWriteAllowed;
    }

    /**
     * @param readWriteAllowed Разрешены чтение и запись в НСИ. Свойство не может быть null.
     */
    public void setReadWriteAllowed(boolean readWriteAllowed)
    {
        dirty(_readWriteAllowed, readWriteAllowed);
        _readWriteAllowed = readWriteAllowed;
    }

    /**
     * @return Разрешена отсылка изменений в НСИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isAutoDeliveryChangesAllowed()
    {
        return _autoDeliveryChangesAllowed;
    }

    /**
     * @param autoDeliveryChangesAllowed Разрешена отсылка изменений в НСИ. Свойство не может быть null.
     */
    public void setAutoDeliveryChangesAllowed(boolean autoDeliveryChangesAllowed)
    {
        dirty(_autoDeliveryChangesAllowed, autoDeliveryChangesAllowed);
        _autoDeliveryChangesAllowed = autoDeliveryChangesAllowed;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Пользовательский код.
     */
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Пользовательский код.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Дата запрещения.
     */
    public Date getDisabledDate()
    {
        return _disabledDate;
    }

    /**
     * @param disabledDate Дата запрещения.
     */
    public void setDisabledDate(Date disabledDate)
    {
        dirty(_disabledDate, disabledDate);
        _disabledDate = disabledDate;
    }

    @Override
    public boolean isEnabled()
    {
        return getDisabledDate()==null;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        if( !isInitLazyInProgress() && isEnabled()!=enabled )
        {
            setDisabledDate(enabled ? null : new Date());
        }
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiCatalogTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((NsiCatalogType)another).getCode());
            }
            setNsiCode(((NsiCatalogType)another).getNsiCode());
            setSyncDate(((NsiCatalogType)another).getSyncDate());
            setReadAllowed(((NsiCatalogType)another).isReadAllowed());
            setReadWriteAllowed(((NsiCatalogType)another).isReadWriteAllowed());
            setAutoDeliveryChangesAllowed(((NsiCatalogType)another).isAutoDeliveryChangesAllowed());
            setTitle(((NsiCatalogType)another).getTitle());
            setUserCode(((NsiCatalogType)another).getUserCode());
            setDisabledDate(((NsiCatalogType)another).getDisabledDate());
            setEnabled(((NsiCatalogType)another).isEnabled());
        }
    }

    public INaturalId<NsiCatalogTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<NsiCatalogTypeGen>
    {
        private static final String PROXY_NAME = "NsiCatalogTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof NsiCatalogTypeGen.NaturalId) ) return false;

            NsiCatalogTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiCatalogTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiCatalogType.class;
        }

        public T newInstance()
        {
            return (T) new NsiCatalogType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "nsiCode":
                    return obj.getNsiCode();
                case "syncDate":
                    return obj.getSyncDate();
                case "readAllowed":
                    return obj.isReadAllowed();
                case "readWriteAllowed":
                    return obj.isReadWriteAllowed();
                case "autoDeliveryChangesAllowed":
                    return obj.isAutoDeliveryChangesAllowed();
                case "title":
                    return obj.getTitle();
                case "userCode":
                    return obj.getUserCode();
                case "disabledDate":
                    return obj.getDisabledDate();
                case "enabled":
                    return obj.isEnabled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "nsiCode":
                    obj.setNsiCode((String) value);
                    return;
                case "syncDate":
                    obj.setSyncDate((Date) value);
                    return;
                case "readAllowed":
                    obj.setReadAllowed((Boolean) value);
                    return;
                case "readWriteAllowed":
                    obj.setReadWriteAllowed((Boolean) value);
                    return;
                case "autoDeliveryChangesAllowed":
                    obj.setAutoDeliveryChangesAllowed((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "disabledDate":
                    obj.setDisabledDate((Date) value);
                    return;
                case "enabled":
                    obj.setEnabled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "nsiCode":
                        return true;
                case "syncDate":
                        return true;
                case "readAllowed":
                        return true;
                case "readWriteAllowed":
                        return true;
                case "autoDeliveryChangesAllowed":
                        return true;
                case "title":
                        return true;
                case "userCode":
                        return true;
                case "disabledDate":
                        return true;
                case "enabled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "nsiCode":
                    return true;
                case "syncDate":
                    return true;
                case "readAllowed":
                    return true;
                case "readWriteAllowed":
                    return true;
                case "autoDeliveryChangesAllowed":
                    return true;
                case "title":
                    return true;
                case "userCode":
                    return true;
                case "disabledDate":
                    return true;
                case "enabled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "nsiCode":
                    return String.class;
                case "syncDate":
                    return Date.class;
                case "readAllowed":
                    return Boolean.class;
                case "readWriteAllowed":
                    return Boolean.class;
                case "autoDeliveryChangesAllowed":
                    return Boolean.class;
                case "title":
                    return String.class;
                case "userCode":
                    return String.class;
                case "disabledDate":
                    return Date.class;
                case "enabled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiCatalogType> _dslPath = new Path<NsiCatalogType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiCatalogType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип справочника. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getNsiCode()
     */
    public static PropertyPath<String> nsiCode()
    {
        return _dslPath.nsiCode();
    }

    /**
     * @return Дата и время синхронизации.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getSyncDate()
     */
    public static PropertyPath<Date> syncDate()
    {
        return _dslPath.syncDate();
    }

    /**
     * @return Разрешено чтение из НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isReadAllowed()
     */
    public static PropertyPath<Boolean> readAllowed()
    {
        return _dslPath.readAllowed();
    }

    /**
     * @return Разрешены чтение и запись в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isReadWriteAllowed()
     */
    public static PropertyPath<Boolean> readWriteAllowed()
    {
        return _dslPath.readWriteAllowed();
    }

    /**
     * @return Разрешена отсылка изменений в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isAutoDeliveryChangesAllowed()
     */
    public static PropertyPath<Boolean> autoDeliveryChangesAllowed()
    {
        return _dslPath.autoDeliveryChangesAllowed();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Пользовательский код.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Дата запрещения.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getDisabledDate()
     */
    public static PropertyPath<Date> disabledDate()
    {
        return _dslPath.disabledDate();
    }

    /**
     * @return Используется. Свойство не может быть null.
     *
     * Это формула "case when disabledDate is null then true else false end".
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isEnabled()
     */
    public static PropertyPath<Boolean> enabled()
    {
        return _dslPath.enabled();
    }

    public static class Path<E extends NsiCatalogType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _nsiCode;
        private PropertyPath<Date> _syncDate;
        private PropertyPath<Boolean> _readAllowed;
        private PropertyPath<Boolean> _readWriteAllowed;
        private PropertyPath<Boolean> _autoDeliveryChangesAllowed;
        private PropertyPath<String> _title;
        private PropertyPath<String> _userCode;
        private PropertyPath<Date> _disabledDate;
        private PropertyPath<Boolean> _enabled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(NsiCatalogTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип справочника. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getNsiCode()
     */
        public PropertyPath<String> nsiCode()
        {
            if(_nsiCode == null )
                _nsiCode = new PropertyPath<String>(NsiCatalogTypeGen.P_NSI_CODE, this);
            return _nsiCode;
        }

    /**
     * @return Дата и время синхронизации.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getSyncDate()
     */
        public PropertyPath<Date> syncDate()
        {
            if(_syncDate == null )
                _syncDate = new PropertyPath<Date>(NsiCatalogTypeGen.P_SYNC_DATE, this);
            return _syncDate;
        }

    /**
     * @return Разрешено чтение из НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isReadAllowed()
     */
        public PropertyPath<Boolean> readAllowed()
        {
            if(_readAllowed == null )
                _readAllowed = new PropertyPath<Boolean>(NsiCatalogTypeGen.P_READ_ALLOWED, this);
            return _readAllowed;
        }

    /**
     * @return Разрешены чтение и запись в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isReadWriteAllowed()
     */
        public PropertyPath<Boolean> readWriteAllowed()
        {
            if(_readWriteAllowed == null )
                _readWriteAllowed = new PropertyPath<Boolean>(NsiCatalogTypeGen.P_READ_WRITE_ALLOWED, this);
            return _readWriteAllowed;
        }

    /**
     * @return Разрешена отсылка изменений в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isAutoDeliveryChangesAllowed()
     */
        public PropertyPath<Boolean> autoDeliveryChangesAllowed()
        {
            if(_autoDeliveryChangesAllowed == null )
                _autoDeliveryChangesAllowed = new PropertyPath<Boolean>(NsiCatalogTypeGen.P_AUTO_DELIVERY_CHANGES_ALLOWED, this);
            return _autoDeliveryChangesAllowed;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(NsiCatalogTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Пользовательский код.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(NsiCatalogTypeGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Дата запрещения.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#getDisabledDate()
     */
        public PropertyPath<Date> disabledDate()
        {
            if(_disabledDate == null )
                _disabledDate = new PropertyPath<Date>(NsiCatalogTypeGen.P_DISABLED_DATE, this);
            return _disabledDate;
        }

    /**
     * @return Используется. Свойство не может быть null.
     *
     * Это формула "case when disabledDate is null then true else false end".
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType#isEnabled()
     */
        public PropertyPath<Boolean> enabled()
        {
            if(_enabled == null )
                _enabled = new PropertyPath<Boolean>(NsiCatalogTypeGen.P_ENABLED, this);
            return _enabled;
        }

        public Class getEntityClass()
        {
            return NsiCatalogType.class;
        }

        public String getEntityName()
        {
            return "nsiCatalogType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
