/* $Id: Msu2SystemActionPubAddon.java 44316 2015-07-27 11:42:50Z oleyba $ */
package ru.tandemservice.nsiclient.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.ui.List.NsiReactorTestList;

/**
 * @author Andrey Avetisov
 * @since 15.05.2015
 */
public class SystemActionPubIntegrationAddon extends UIAddon
{
    public SystemActionPubIntegrationAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickNsiTest()
    {
       getActivationBuilder().asDesktopRoot(NsiReactorTestList.class).activate();
    }
}
