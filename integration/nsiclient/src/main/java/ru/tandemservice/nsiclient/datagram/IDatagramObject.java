/* $Id: IDatagramObject.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.datagram;

import javax.xml.namespace.QName;
import java.math.BigInteger;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 10.04.2015
 */
public interface IDatagramObject
{
    /**
     * Возвращает GUID сущности НСИ
     *
     * @return - GUID сущности НСИ
     */
    public String getID();

    /**
     * Присваивает GUID сущности НСИ
     *
     * @param id - GUID сущности НСИ
     */
    public void setID(String id);

    /**
     * Возвращает уникальный идентификатор объекта НСИ (очевидно, в интегрируемой подсистеме)
     *
     * @return - уникальный идентификатор объекта НСИ
     */
    public String getOid();

    /**
     * Присваивает уникальный идентификатор объекта НСИ
     *
     * @param value - уникальный идентификатор объекта НСИ
     */
    public void setOid(String value);

    /**
     * Возвращает флаг нового объекта (признак того, что объект является новым и записывается в подсистему впервые)
     *
     * @return - флаг нового объекта
     */
    public Short getNew();

    /**
     * Присваивает флаг нового объекта
     *
     * @param value - флаг нового объекта
     */
    public void setNew(Short value);

    /**
     * Возвращает флаг удаляемого объекта (признак того, что объект был удалён, или должен быть удалён из НСИ)
     *
     * @return - флаг удаляемого объекта
     */
    public Short getDelete();

    /**
     * Присваивает флаг удаляемого объекта
     *
     * @param value - флаг удаляемого объекта
     */
    public void setDelete(Short value);

    /**
     * Возвращает флаг измененного объекта (признак того, что объект изменился, но уже был в НСИ)
     *
     * @return - флаг измененного объекта
     */
    public Short getChange();

    /**
     * Присваивает флаг измененного объекта
     *
     * @param value - флаг измененного объекта
     */
    public void setChange(Short value);

    /**
     * Возвращает временную метку объекта (что это не знает никто)
     *
     * @return - временная метка объекта
     */
    public BigInteger getTs();

    /**
     * Присваивает временную метку объекта
     *
     * @param value - временная метка объекта
     */
    public void setTs(BigInteger value);

    /**
     * Возвращает список идентификаторов объединяемых записей (если их несколько, то нужно объединить соответствующие объекты на основе того, чей идентификатор указан в теле объекта
     *
     * @return - список идентификаторов объединяемых записей
     */
    public String getMergeDublicates();

    /**
     * Присваивает список идентификаторов объединяемых записей
     *
     * @param value - список идентификаторов объединяемых записей
     */
    public void setMergeDublicates(String value);

    /**
     * Возвращает флаг невалидного объекта (непонятно кто и зачем его присваивает)
     *
     * @return - флаг невалидного объекта
     */
    public Short getError();

    /**
     * Присваивает флаг невалидного объекта
     *
     * @param value - флаг невалидного объекта
     */
    public void setError(Short value);

    /**
     * Возвращает список идентификаторов аналогов объекта
     *
     * @return - список идентификаторов аналогов объекта
     */
    public String getAnalogs();

    /**
     * Присваивает список идентификаторов аналогов объекта
     *
     * @param value - список идентификаторов аналогов объекта
     */
    public void setAnalogs(String value);

    /**
     * Возвращает флаг несогласованного объекта (очевидно, выставляется в случаях, когда объект является дублем, но насколько мне известно, такие объекты не рассылаются из НСИ)
     *
     * @return - флаг несогласованного объекта
     */
    public Short getIsNotConsistent();

    /**
     * Присваивает флаг несогласованного объекта
     *
     * @param value - флаг несогласованного объекта
     */
    public void setIsNotConsistent(Short value);

    /**
     * Возвращает список прочих атрибутов
     *
     * @return - список прочих атрибутов
     */
    public Map<QName, String> getOtherAttributes();

}