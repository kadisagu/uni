/* $Id$ */
package ru.tandemservice.nsiclient.utils;

import ru.tandemservice.nsiclient.datagram.IXDatagram;

/**
 * @author Andrey Nikonov
 * @since 01.09.2015
 */
public interface INsiBean
{
    IXDatagram createXDatagram();
}
