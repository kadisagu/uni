/* $Id$ */
package ru.tandemservice.nsiclient.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 15.01.2016
 */
public class MS_nsiclient_2x9x3_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        /*
         * миграция исправляет баг, вызванные тем, что в nsientity_t, в поле entitytype_p,
         * записывался фактический класс объекта (необходимо было записывать класс соответствующий используемому в реакторе,
         * например для EduProgramSubjectType необходимо записывать "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject",
          * а не "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009"
          * или "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013").
         */
        Map<String, String> classesReactorMap = new HashMap<>(); //тип датаграммы, класс используемый реактором

        classesReactorMap.put("EduProgramSubjectType", "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject");
        classesReactorMap.put("ContactPersonType", "org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson");
        classesReactorMap.put("EduSpecializationBaseType", "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization");
        classesReactorMap.put("EnrProgramSetBaseType", "ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase");
        classesReactorMap.put("EduCtrContractVersionTemplateDataType", "org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData");
        classesReactorMap.put("RegistryDisciplineType", "ru.tandemservice.uniepp.entity.registry.EppRegistryElement");

        for (String datagramType : classesReactorMap.keySet())
        {
            tool.executeUpdate("UPDATE nsientity_t SET entitytype_p = ? WHERE type_p = ?", classesReactorMap.get(datagramType), datagramType);
        }
    }
}
