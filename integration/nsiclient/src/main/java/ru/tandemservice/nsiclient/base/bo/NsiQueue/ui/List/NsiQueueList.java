/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiQueue.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.View.NsiPackageView;
import ru.tandemservice.nsiclient.base.bo.NsiQueue.NsiQueueManager;
import ru.tandemservice.nsiclient.base.bo.NsiQueue.logic.NsiQueueWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 18.01.2017
 */
@Configuration
public class NsiQueueList extends BusinessComponentManager
{
    public static final String OPERATION_LOG_DS = "operationLogDS";
    public static final String EVENT_TYPE_DS = "eventTypeOptionDS";
    public static final String OPERATION_TYPE_DS = "operationTypeOptionDS";
    public static final String CATALOG_TYPE_DS = "catalogTypeOptionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(OPERATION_LOG_DS, catalogLogCL(), NsiQueueManager.instance().queueDSHandler()))
                .addDataSource(selectDS(OPERATION_TYPE_DS, NsiSyncManager.instance().operationTypeComboDSHandler()))
                .addDataSource(selectDS(EVENT_TYPE_DS, NsiSyncManager.instance().directionComboDSHandler()))
                .addDataSource(selectDS(CATALOG_TYPE_DS, NsiSyncManager.instance().catalogTypeDSHandler()
                        .filter(NsiCatalogType.title()).filter(NsiCatalogType.code()).order(NsiCatalogType.title()))
                        .addColumn(NsiCatalogType.title().s()).addColumn(NsiCatalogType.code().s())
                )
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint catalogLogCL()
    {
        return columnListExtPointBuilder(OPERATION_LOG_DS)
                .addColumn(publisherColumn("creationDate", NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().creationDate()).width("145px").formatter(NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC)
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                Map<String, Object> params = new HashMap<>();
                                params.put(UIPresenter.PUBLISHER_ID, ((NsiQueueWrapper)entity).getQueue().getLog().getPack().getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return NsiPackageView.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("guid", NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().guid()).order())
                .addColumn(textColumn("direction", NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().incoming()).formatter(NsiSyncManager.DirectionFormatter.INSTANCE).order())
                .addColumn(textColumn("catalogs", NsiQueueWrapper.CATALOG_CODES))
                .addColumn(textColumn("destinations", NsiQueueWrapper.SUB_SYSTEM_CODES))
                .addColumn(textColumn("type", NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().type()).order())
                .addColumn(textColumn("source", NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().source()).order())
                .addColumn(textColumn("startDate", NsiQueueWrapper.START_DATE).width("145px").formatter(NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC))
                .addColumn(actionColumn("getPack").icon("template_save").listener("onGetMessagePack"))
                .addColumn(actionColumn("delete").icon("delete").listener("onDeleteFromQueue")/*.permissionKey("nsiPackageSend")*/)
                .create();
    }
}