/* $Id: SystemActionPubExt.java 42627 2015-05-15 11:06:04Z aavetisov $ */
package ru.tandemservice.nsiclient.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Andrey Avetisov
 * @since 15.05.2015
 */

@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String NSI_SYSTEM_ACTION_PUB_ADDON_NAME = "nsiSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(NSI_SYSTEM_ACTION_PUB_ADDON_NAME, SystemActionPubIntegrationAddon.class))
                .create();
    }
}
