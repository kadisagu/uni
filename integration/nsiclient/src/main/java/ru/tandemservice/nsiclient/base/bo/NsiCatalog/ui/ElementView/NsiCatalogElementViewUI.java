package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.text.MessageFormat;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
@State({
        @Bind(key = NsiCatalogElementView.SELECTED_TAB, binding = "selectedTab")
})
public class NsiCatalogElementViewUI extends UIPresenter
{
    private Long _id;
    private NsiEntity _entity;
    private String _selectedTab;
    private String _title;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getPageTitle()
    {
        return MessageFormat.format(getConfig().getProperty("page.title"), _title, _entity.getGuid());
    }

    public Map<String, Object> getTabParameter()
    {
        return ParametersMap.createWith(UIPresenter.PUBLISHER_ID, _id);
    }

    public void onChangeTab()
    {
        _uiSettings.set("selectedTab", _selectedTab);
        saveSettings();
    }

    @Override
    public void onComponentActivate()
    {
        if(_selectedTab == null)
            _selectedTab = _uiSettings.get("selectedTab");
        _entity = DataAccessServices.dao().getNotNull(_id);
        IEntityMeta meta = EntityRuntime.getMeta(_entity);
        _title = meta.getTitle();
    }
}
