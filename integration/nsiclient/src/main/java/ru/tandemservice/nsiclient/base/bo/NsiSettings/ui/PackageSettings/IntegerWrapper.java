/* $Id: IntegerWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.PackageSettings;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

/**
 * @author Andrey Nikonov
 * @since 03.03.2015
 */
public class IntegerWrapper extends DataWrapper
{
    private Integer _value;

    public IntegerWrapper(long id, Integer value)
    {
        super(id, Long.toString(id));
        _value = value;
    }

    public Integer getValue() {
        return _value;
    }

    public void setValue(Integer value) {
        _value = value;
    }

    /**
     * For using com.google.common.base.Joiner
     * @return
     */
    @Override
    public String toString()
    {
        return _value.toString();
    }
}