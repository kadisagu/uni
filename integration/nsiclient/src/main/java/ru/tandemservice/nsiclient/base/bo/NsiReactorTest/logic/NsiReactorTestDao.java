/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic;

import org.apache.axis.message.MessageElement;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.meta.entity.impl.FieldPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.util.ValueHolder;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.nsiclient.base.bo.NsiReactorTest.NsiReactorTestManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.NsiSyncOperatorDAO;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.codes.NsiSubSystemCodes;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.IResultProcessor;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.*;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 08.10.2015
 */
public class NsiReactorTestDao extends SharedBaseDao implements INsiReactorTestDao
{
    private Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors;

    public NsiReactorTestDao()
    {
        reactors = ReactorHolder.getReactorMap();
    }


    @Override
    public boolean runReactorTest(Long catalogTypeId, List<IEntity> createdEntityList, Logger logger) throws Exception
    {

        Session session = getSession();
        NsiCatalogType catalogType = get(NsiCatalogType.class, catalogTypeId);
        INsiEntityReactor reactor = reactors.get(catalogType.getCode());

        if (isCantProcessDatagram(reactor))
        {
            return true;
        }
        Set<IEntity> processedEntitySet = new HashSet<>();

        IEntity baseEntity = generateEntity(reactor, session, createdEntityList, processedEntitySet);

        Map<String, String> serializedBaseEntity = createSerializedFields(baseEntity);
        ValueHolder<Map<String, String>> serializedRetrievedEntity = new ValueHolder<>();

        NsiEntity nsiEntity = new DQLSelectBuilder()
                .fromEntity(NsiEntity.class, "n")
                .column("n")
                .where(eqValue(property("n", NsiEntity.P_ENTITY_ID), baseEntity.getId()))
                .where(eqValue(property("n", NsiEntity.P_TYPE), reactor.getCatalogType()))
                .createStatement(getSession()).uniqueResult();

        nsiEntity = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(baseEntity, nsiEntity, null, reactor.getCatalogType());

        Map<String, Object> commonCache = new HashMap<>();
        commonCache.put(NsiSyncOperatorDAO.PROCESSED, new HashSet<Long>());

        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.addAll(reactor.createDatagramObject(baseEntity, nsiEntity, commonCache).getList());
        getSession().clear();

        List<NsiSubSystem> subSystemList = getList(NsiSubSystem.class);
        Map<String, NsiSubSystem> subSystemMap = getSusbsystemMap(subSystemList);

        ServiceRequestType request = getRequest(datagrams, subSystemMap.get(NsiSubSystemCodes.LOCAL).getUserCode(), subSystemMap.get(NsiSubSystemCodes.NSI).getUserCode());
        getService(subSystemMap.get(NsiSubSystemCodes.NSI).getAddress()).insert(request);

        //sent data to NSI
        NsiRequestHandlerService.instance().executeNSIAction(datagrams, null, NsiUtils.OPERATION_TYPE_INSERT, null,
                                                             (subSystemLog, list1) -> {
                                                                 NsiSyncManager.instance().operatorDAO().update(list1, subSystemLog.getPack(), null, subSystemLog.getGuid());
                                                                 return null;
                                                             }, false);


        //in case of PersonContactData the person must be removed, cause person has a link to the contactData
        if (baseEntity instanceof PersonContactData || baseEntity instanceof IdentityCard)
        {
            IdentityCard identityCard = (IdentityCard) createdEntityList.stream()
                    .filter(entity -> entity instanceof IdentityCard)
                    .collect(Collectors.toList()).get(0);
            identityCard.setPerson(null);
            session.update(identityCard);

            IEntity person = createdEntityList.stream()
                    .filter(entity -> entity instanceof Person)
                    .collect(Collectors.toList()).get(0);

            //remove person and personContactData (by cascade)
            session.delete(person);
            session.delete(identityCard);
            session.flush();
        }
        else if (baseEntity instanceof PersonRole)
        {
            refresh(baseEntity);
            PersonRole personRole = (PersonRole) baseEntity;
            Principal principal = personRole.getPrincipal();
            session.delete(personRole);
            session.delete(principal);
            session.flush();
        }
        else
        {
            session.refresh(baseEntity); //if entity was changed by reactor then delete method will drop.
            session.delete(baseEntity);
            session.flush();
        }

        //retrieve data from NSI
        NsiRequestHandlerService.instance().executeNSIAction(datagrams, null, NsiUtils.OPERATION_TYPE_RETRIEVE, null,
                                                             (subSystemLog, list1) -> {
                                                                 NsiSyncManager.instance().operatorDAO().update(list1, subSystemLog.getPack(), new IResultProcessor()
                                                                 {
                                                                     @Override
                                                                     public <T1 extends IDatagramObject, V1 extends IEntity> void processResult(T1 datagramObject, ChangedWrapper<V1> resultWrapper)
                                                                     {
                                                                         if (resultWrapper.isCreated() || resultWrapper.isChanged())
                                                                         {
                                                                             createdEntityList.add(createdEntityList.indexOf(baseEntity), resultWrapper.getResult());
                                                                             serializedRetrievedEntity.setValue(createSerializedFields(resultWrapper.getResult()));
                                                                         }
                                                                     }

                                                                     @Override
                                                                     public <V extends IDatagramObject> void exception(V datagramObject, ProcessingDatagramObjectException exception)
                                                                     {
                                                                         exception.printStackTrace();
                                                                     }
                                                                 }, subSystemLog.getGuid());
                                                                 return null;
                                                             }, false);

        //remove all created entities
/*        createdEntityList.remove(baseEntity);
        Collections.reverse(createdEntityList);

        for(IEntity entity : createdEntityList)
        {
            session.delete(entity);
        }*/

        //if entity has not changed then - success else - fail
        return !isChanged(reactor, serializedBaseEntity, serializedRetrievedEntity.getValue(), logger);
    }

    /**
     * Проверяет может ли реактор обработать входящую датаграмму и создать сущность
     *
     * @param reactor реактор
     * @return true - реатор не сможет обработать датаграмму, false - реактор сможет обработать датаграмму
     */
    private static boolean isCantProcessDatagram(INsiEntityReactor reactor)
    {
        return (reactor.getCatalogType().equals("RegistryDisciplineType") || reactor.getCatalogType().equals("EduProgramDisciplineStatisticsType")
                || reactor.getCatalogType().equals("EducationalProgramType") || reactor.getCatalogType().equals("EduProgramDisciplineInformationType"));
    }


    /**
     * Метод проверяет возникли ли какие-либо изменения в объектах, после того как отработал реактор
     *
     * @param reactor      реактор
     * @param referenceMap сущность, сериализованная до работы реактора
     * @param resultMap    сущность, сериализованная после работы реактора
     * @param logger       логгер
     * @return если сущнсоть изменилась - true, иначе - false
     */
    private static boolean isChanged(INsiEntityReactor reactor, Map<String, String> referenceMap, Map<String, String> resultMap, Logger logger)
    {
        boolean changed = false;

        for (String property : referenceMap.keySet())
        {
            if (isNeedChecked(property, resultMap, reactor)
                    && (!resultMap.containsKey(property) || !resultMap.get(property).equals(referenceMap.get(property))))
            {
                changed = true;

                logger.warn("error in process " + reactor.getCatalogType() + ". Problem with property - \"" + property
                                    + "\" in " + reactor.getEntityClass() + ". Referenced value is " + referenceMap.get(property) +
                                    ", value after processing is " + resultMap.get(property));
            }
        }

        return changed;
    }


    /**
     * Метод возвращает true, если значение поля не должно меняться после работы реактора,
     * если значени может изменится (в некоторых случаях это не является ошибкой), то вернется false.
     *
     * @param property  поле, значение которого проверяется на мутабельность.
     * @param reactor   реактор.
     * @param resultMap сериализованнаый объект класса.
     * @return false - поле мутабельно и проверять его не надо. true - поле не должно меняться и должно быть проверено.
     */
    private static boolean isNeedChecked(String property, Map<String, String> resultMap, INsiEntityReactor reactor)
    {
        if (reactor.getNotValidatedFieldList().contains(property))
            return false;
        if (property.equals("userCode") && !resultMap.get(property).equals("null"))
            return false;

        return true;
    }


    /**
     * Мапа с подсистемами НСИ. Используется для установки полей destination, source и address, в отправляемых пакетах
     *
     * @param subSystemList списко подсистем в системе
     * @return возвращает мапу с подсистемами local и NSI.
     */
    private static Map<String, NsiSubSystem> getSusbsystemMap(List<NsiSubSystem> subSystemList)
    {
        Map<String, NsiSubSystem> subSystemMap = new HashMap<>();
        for (NsiSubSystem subSystem : subSystemList)
        {
            if (subSystem.getCode().equals(NsiSubSystemCodes.LOCAL))
            {
                subSystemMap.put(NsiSubSystemCodes.LOCAL, subSystem);
            }
            else if (subSystem.getCode().equals(NsiSubSystemCodes.NSI))
            {
                subSystemMap.put(NsiSubSystemCodes.NSI, subSystem);
            }
        }

        if (StringUtils.trimToNull(subSystemMap.get(NsiSubSystemCodes.LOCAL).getUserCode()) == null)
        {
            throw new ApplicationException("set local susbsystem code");
        }
        if (StringUtils.trimToNull(subSystemMap.get(NsiSubSystemCodes.NSI).getUserCode()) == null)
        {
            throw new ApplicationException("set nsi server code");
        }
        if (StringUtils.trimToNull(subSystemMap.get(NsiSubSystemCodes.NSI).getAddress()) == null)
        {
            throw new ApplicationException("set nsi server address");
        }
        return subSystemMap;
    }


    private IEntity generateEntity(INsiEntityReactor reactor,
                                   Session session,
                                   List<IEntity> createdEntityList,
                                   Set<IEntity> processedEntitySet) throws IllegalAccessException, InstantiationException
    {
        IEntity baseEntity = NsiReactorTestManager.instance().generatorService().generateEntities(reactor.getEntityClass(), createdEntityList, processedEntitySet);
        for (IEntity entity : createdEntityList)
        {
            save(entity);
        }
        session.flush();

        return baseEntity;
    }

    /**
     * Метод фомрирует запрос к веб-сервису НСИ.
     *
     * @param datagrams       список датаграмм для обработки.
     * @param sourceCode      код отправителя.
     * @param destinationCode код получателя.
     * @return возвращет запрос к веб-сервису.
     */
    private static ServiceRequestType getRequest(List<IDatagramObject> datagrams, String sourceCode, String destinationCode)
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId(sourceCode);
        header.setDestinationId(destinationCode);
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        Datagram datagram = new Datagram();

        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().addAll(datagrams);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));

        MessageElement datagramOut = new org.apache.axis.message.SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);
        return request;
    }

    /**
     * Метод предоставляет доступ в веб-сервису НСИ.
     *
     * @param url адрес веб-сервиса НСИ
     * @return возвращет объект ServiceSoap, который можно использовать для получения доступа к веб-сервисам НСИ.
     */
    private ServiceSoap getService(String url) throws MalformedURLException
    {
        return new ServiceSoapImplService(new URL(url + "?wsdl"), ServiceSoapImplService.SERVICE).getServiceSoapPort();
    }


    /**
     * Сохраняет в мапу текущее состояние полей сущности.
     *
     * @param entity сущность
     * @return возвращает мапу с текущим состоянием полей сущности
     */
    private static Map<String, String> createSerializedFields(IEntity entity)
    {
        Map<String, String> paramsMap = new HashMap<>();
        IEntityMeta meta = EntityRuntime.getMeta(entity);
        for (String propertyTitle : meta.getPropertyNames())
        {
            if (propertyTitle.equals(IEntity.P_ID))
            {
                continue;
            }
            IPropertyMeta propertyMeta = meta.getProperty(propertyTitle);
            if (propertyMeta instanceof FieldPropertyMeta
                    && StringUtils.trimToNull(((FieldPropertyMeta) propertyMeta).getFormula()) != null)
            {
                continue;
            }
            String propertyValue;
            Object param = entity.getProperty(propertyMeta.getPropertyPath());
            if (param instanceof Principal)
            {
                propertyValue = ((Principal) param).getLogin();
            }
            else if (param instanceof IEntity)
            {
                propertyValue = ((IEntity) param).getId().toString();
            }
            else if (param instanceof Date)
            {
                propertyValue = NsiUtils.formatDate((Date) param);
            }
            else
            {
                propertyValue = param == null ? "null" : param.toString();
            }
            paramsMap.put(propertyTitle, propertyValue);
        }

        return paramsMap;
    }

}
