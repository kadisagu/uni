package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.nsiclient.entity.NsiCredential;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сертификат НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiCredentialGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiCredential";
    public static final String ENTITY_NAME = "nsiCredential";
    public static final int VERSION_HASH = 1711404686;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRINCIPAL = "principal";
    public static final String P_FINGER_PRINT = "fingerPrint";
    public static final String P_SUBSYSTEM_CODE = "subsystemCode";

    private String _principal;     // Принципал
    private String _fingerPrint;     // Уникальный номер сертификата
    private String _subsystemCode;     // Код подсистемы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Принципал. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrincipal()
    {
        return _principal;
    }

    /**
     * @param principal Принципал. Свойство не может быть null.
     */
    public void setPrincipal(String principal)
    {
        dirty(_principal, principal);
        _principal = principal;
    }

    /**
     * @return Уникальный номер сертификата. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getFingerPrint()
    {
        return _fingerPrint;
    }

    /**
     * @param fingerPrint Уникальный номер сертификата. Свойство не может быть null и должно быть уникальным.
     */
    public void setFingerPrint(String fingerPrint)
    {
        dirty(_fingerPrint, fingerPrint);
        _fingerPrint = fingerPrint;
    }

    /**
     * @return Код подсистемы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSubsystemCode()
    {
        return _subsystemCode;
    }

    /**
     * @param subsystemCode Код подсистемы. Свойство не может быть null.
     */
    public void setSubsystemCode(String subsystemCode)
    {
        dirty(_subsystemCode, subsystemCode);
        _subsystemCode = subsystemCode;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiCredentialGen)
        {
            setPrincipal(((NsiCredential)another).getPrincipal());
            setFingerPrint(((NsiCredential)another).getFingerPrint());
            setSubsystemCode(((NsiCredential)another).getSubsystemCode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiCredentialGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiCredential.class;
        }

        public T newInstance()
        {
            return (T) new NsiCredential();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "principal":
                    return obj.getPrincipal();
                case "fingerPrint":
                    return obj.getFingerPrint();
                case "subsystemCode":
                    return obj.getSubsystemCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "principal":
                    obj.setPrincipal((String) value);
                    return;
                case "fingerPrint":
                    obj.setFingerPrint((String) value);
                    return;
                case "subsystemCode":
                    obj.setSubsystemCode((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "principal":
                        return true;
                case "fingerPrint":
                        return true;
                case "subsystemCode":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "principal":
                    return true;
                case "fingerPrint":
                    return true;
                case "subsystemCode":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "principal":
                    return String.class;
                case "fingerPrint":
                    return String.class;
                case "subsystemCode":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiCredential> _dslPath = new Path<NsiCredential>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiCredential");
    }
            

    /**
     * @return Принципал. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiCredential#getPrincipal()
     */
    public static PropertyPath<String> principal()
    {
        return _dslPath.principal();
    }

    /**
     * @return Уникальный номер сертификата. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.NsiCredential#getFingerPrint()
     */
    public static PropertyPath<String> fingerPrint()
    {
        return _dslPath.fingerPrint();
    }

    /**
     * @return Код подсистемы. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiCredential#getSubsystemCode()
     */
    public static PropertyPath<String> subsystemCode()
    {
        return _dslPath.subsystemCode();
    }

    public static class Path<E extends NsiCredential> extends EntityPath<E>
    {
        private PropertyPath<String> _principal;
        private PropertyPath<String> _fingerPrint;
        private PropertyPath<String> _subsystemCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Принципал. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiCredential#getPrincipal()
     */
        public PropertyPath<String> principal()
        {
            if(_principal == null )
                _principal = new PropertyPath<String>(NsiCredentialGen.P_PRINCIPAL, this);
            return _principal;
        }

    /**
     * @return Уникальный номер сертификата. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.NsiCredential#getFingerPrint()
     */
        public PropertyPath<String> fingerPrint()
        {
            if(_fingerPrint == null )
                _fingerPrint = new PropertyPath<String>(NsiCredentialGen.P_FINGER_PRINT, this);
            return _fingerPrint;
        }

    /**
     * @return Код подсистемы. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiCredential#getSubsystemCode()
     */
        public PropertyPath<String> subsystemCode()
        {
            if(_subsystemCode == null )
                _subsystemCode = new PropertyPath<String>(NsiCredentialGen.P_SUBSYSTEM_CODE, this);
            return _subsystemCode;
        }

        public Class getEntityClass()
        {
            return NsiCredential.class;
        }

        public String getEntityName()
        {
            return "nsiCredential";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
