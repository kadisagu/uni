/* $Id: NsiSettingSystemConfig.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.AuthSettings;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author Andrey Avetisov
 * @since 02.07.2015
 */
public class NsiSettingSystemConfig
{
    static
    {
        if (null == System.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE)
                && null != ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE))
        {
            System.setProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE, ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE));
        }
        if (null == System.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE_PASSWORD)
                && null != ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE_PASSWORD))
        {
            System.setProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE_PASSWORD, ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE_PASSWORD));
        }
        if (null == System.getProperty(NsiSettingsAuthSettingsUI.CLIENT_TRUST_STORE)
                && null != ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE))
        {
            System.setProperty(NsiSettingsAuthSettingsUI.CLIENT_TRUST_STORE, ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE));
        }
        if (null == System.getProperty(NsiSettingsAuthSettingsUI.CLIENT_TRUST_STORE_PASSWORD)
                && null != ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE_PASSWORD))
        {
            System.setProperty(NsiSettingsAuthSettingsUI.CLIENT_TRUST_STORE_PASSWORD, ApplicationRuntime.getProperty(NsiSettingsAuthSettingsUI.CLIENT_KEY_STORE_PASSWORD));
        }
    }
}
