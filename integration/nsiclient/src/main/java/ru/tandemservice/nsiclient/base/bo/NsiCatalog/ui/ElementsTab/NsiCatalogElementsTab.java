package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementsTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiCatalogElementsTab extends BusinessComponentManager
{
    public static final String ENTITY_CODE = "entityCode";

    public static final String ELEMENT_LIST_DS = "elementListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ELEMENT_LIST_DS, catalogElementsCL(), NsiCatalogManager.instance().nsiCatalogElementsListDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint catalogElementsCL()
    {
        return columnListExtPointBuilder(ELEMENT_LIST_DS)
                .addColumn(actionColumn("view").listener("onViewElement").icon("element_info"))
                .create();
    }
}
