/* $Id: NsiPackageDAO.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiPackage.logic;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 18.03.2015
 */
@Transactional
public class NsiPackageDAO extends BaseModifyAggregateDAO implements INsiPackageDAO
{
    @Override
    public CoreCollectionUtils.Pair<String, String> findElementsAndCatalogs(NsiPackage pack) {
        DQLSelectBuilder entityBuilder = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e")
                .joinEntity("e", DQLJoinType.inner, NsiCatalogType.class, "nct", eq(property("e", NsiEntityLog.catalogType()), property("nct", NsiCatalogType.code())))
                .column(property("e", NsiEntityLog.guid()))
                .column(property("nct", NsiCatalogType.title()))
                .where(eq(property("e", NsiEntityLog.pack().id()), commonValue(pack.getId(), PropertyType.LONG)))
                .order(property("nct", NsiCatalogType.title()));
        List<Object[]> objectList = createStatement(entityBuilder).list();
        Set<String> catalogs = Sets.newTreeSet();

        List<String> links = Lists.newArrayListWithExpectedSize(objectList.size());
        for(Object[] obj: objectList)
        {
            links.add((String) obj[0]);
            catalogs.add((String) obj[1]);
        }
        return new CoreCollectionUtils.Pair<>(Joiner.on(", ").join(links), Joiner.on(", ").join(catalogs));
    }
}