package ru.tandemservice.nsiclient.reactor.support;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrey on 4/24/15.
 */
public class ChangedWrapper<T extends IEntity>
{
    private boolean _changed;
    private boolean _created;
    private NsiEntity _nsiEntity;
    private T _result;
    private String _warning;

    public String getWarning() {
        return _warning;
    }

    public Map<String, Object> getParams() {
        return _params;
    }

    private Map<String, Object> _params = new HashMap<>();

    public T getResult() {
        return _result;
    }

    public void merge(Session session)
    {
        _result = (T) session.merge(_result);
    }

    public boolean isChanged()
    {
        return _changed;
    }
    public boolean isCreated()
    {
        return _created;
    }
    public NsiEntity getNsiEntity()
    {
        return _nsiEntity;
    }
    public void setNsiEntity(NsiEntity nsiEntity)
    {
        _nsiEntity = nsiEntity;
    }

    public ChangedWrapper(boolean created, boolean changed, NsiEntity nsiEntity, T result, Map<String, Object> params, String warning)
    {
        _changed = changed;
        _created = created;
        _result = result;
        _nsiEntity = nsiEntity;
        _params = params;
        _warning = warning;
    }
}
