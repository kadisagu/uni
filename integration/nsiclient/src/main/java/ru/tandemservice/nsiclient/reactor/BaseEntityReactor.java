/* $Id: BaseEntityReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.IActiveCatalogItem;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.NsiSettingsManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.NsiSyncOperatorDAO;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.INsiCollectorDaemon;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
public abstract class BaseEntityReactor<T extends IEntity, V extends IDatagramObject> implements INsiEntityReactor<T, V>
{
    private String _catalogTypeNsiCode;

    public static final String NSI_MAP = "nsiMap";
    public static final String USED_CODES = "usedCodes";
    protected static final String PARAM = "Param";
    protected static final String ENTITY_ALIAS = "e";

    @Override
    public void collectUnknownDatagrams(V datagramObject, IUnknownDatagramsCollector collector) {
    }

    @Override
    public IOperationReactor getDeleteOperationReactor()
    {
        return null;
    }

    @Override
    public IOperationReactor getRetrieveOperationReactor() {
        return null;
    }

    @Override
    public IOperationReactor getUpdateOperationReactor() {
        return null;
    }

    @Override
    public void setCatalogType(String nsiCode) {
        _catalogTypeNsiCode = nsiCode;
    }

    @Override
    public String getCatalogType() {
        return _catalogTypeNsiCode;
    }

    protected List<Long> getFilteredEntityIdList(List<Long> srcListEntityIdList)
    {
        return  srcListEntityIdList;
    }

    @Override
    public void deleteItemsNSI(List<Long> entities) throws ProcessingException
    {
        List<Long> filteredEntityIdList = getFilteredEntityIdList(entities);

        ILogger logger = NsiRequestHandlerService.instance().getLogger();
        logger.log(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM NSI HAVE STARTED ==========");

        List<IDatagramObject> nsiEntityToDelete = new ArrayList<>();
        List<String> guids = NsiSyncManager.instance().dao().getGuids(getEntityClass(), filteredEntityIdList);

        for (String guid : guids)
            nsiEntityToDelete.add(createDatagramObject(guid));

        AtomicInteger count = new AtomicInteger(0);
        NsiRequestHandlerService.instance().executeNSIAction(nsiEntityToDelete, null, NsiUtils.OPERATION_TYPE_DELETE, null, (subSystemLog, list) -> {
            count.addAndGet(list.size());
            return null;
        }, false);
        assert count.intValue() == nsiEntityToDelete.size();

        NsiSyncManager.instance().dao().deleteNsiEntities(filteredEntityIdList);
        logger.log(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM NSI WAS FINISHED ==========");
    }

    @Override
    public Map<String, IDatagramObject> createDatagramObjectMap(List<Long> entities, boolean isDeleteOperation) throws ProcessingException
    {
        List<Long> filteredEntityIdList = getFilteredEntityIdList(entities);

        ILogger logger = NsiRequestHandlerService.instance().getLogger();

        // Мапа сущностей и NsiEntity, которые соответствуют переданным объектам


        Map<String, IDatagramObject> nsiEntityToUpdate = new HashMap<>();
        Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

        Map<String, Object> commonCache = new HashMap<>();
        commonCache.put(NsiSyncOperatorDAO.PROCESSED, new HashSet<Long>());
        if (!isDeleteOperation)
        {
            List<CoreCollectionUtils.Pair<T, NsiEntity>> list = NsiSyncManager.instance().dao().getEntityWithNsiEntityList(getEntityClass(), filteredEntityIdList);
            list.stream().filter(pair -> pair != null).forEach(pair -> {
                // Если идентификатора НСИ у нас ещё нет в списке элементов, то генерим его налету
                if (null == pair.getY()) {
                    NsiEntity e = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(pair.getX(), null, null, getCatalogType());
                    pair.setY(e);
                }

                // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ
                if (!pair.getY().isDeleted()) {
                    if (!alreadyPreparedForSendingToNSIEntitySet.contains(pair.getX().getId())) {
                        ResultListWrapper objects = createDatagramObject(pair.getX(), pair.getY(), commonCache);
                        if (objects.getWarning() != null)
                            logger.log(Level.INFO, objects.getWarning());
                        for (IDatagramObject object : objects.getList())
                            nsiEntityToUpdate.put(object.getID(), object);
                        alreadyPreparedForSendingToNSIEntitySet.add(pair.getX().getId());
                    }
                }
            });
        } else {
            NsiSyncManager.instance().dao().getNsiEntitiesByEntityIds(getEntityClass(), filteredEntityIdList)
                    .stream()
                    .forEach(nsiEntity -> {
                        IDatagramObject object = this.createDatagramObject(nsiEntity.getGuid());
                        nsiEntityToUpdate.put(object.getID(), object);
                    });
        }

        return nsiEntityToUpdate;
    }

    @Override
    public void preUpdate(List<IDatagramObject> items, Map<String, Object> reactorCache) throws ProcessingException
    {
        List<Long> entityIds = NsiSyncManager.instance().dao().getEntityIdsByDatagramObjects(items);

        reactorCache.put(NSI_MAP, NsiSyncManager.instance().dao().getEntityWithNsiIdsMap(getEntityClass(), entityIds));

        if (IActiveCatalogItem.class.isAssignableFrom(getEntityClass()))
        {
            Map<Class<? extends IEntity>, Set<String>> usedCodes = new HashMap<>();
            List<String> catalogCodes = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(getEntityClass(), "e")
                    .column(property("e", ICatalogItem.CATALOG_ITEM_USER_CODE)));

            for (String code : catalogCodes)
            {
                Set<String> usedCatalogCodes = usedCodes.get(getEntityClass());
                if (null == usedCatalogCodes)
                    usedCatalogCodes = new HashSet<>();
                usedCatalogCodes.add(code);
                usedCodes.put(getEntityClass(), usedCatalogCodes);
            }

            reactorCache.put(USED_CODES, usedCodes);
        }
    }

    @Override
    public Map<String, Object> preDelete(List<IDatagramObject> items) throws ProcessingException {
        return null;
    }

    @Override
    public Map<String, Object> preRetrieve(List<IDatagramObject> items) throws ProcessingException {
        return null;
    }

    protected <K1 extends IEntity, K extends IDatagramObject> ProcessedDatagramObject<K> createDatagramObjectShort(Class<K> datagramClass, K1 entity, Map<String, Object> commonCache)
    {
        @SuppressWarnings("unchecked")
        INsiEntityReactor<K1, K> reactor = (INsiEntityReactor<K1, K>) (datagramClass == null ? this : ReactorHolder.getReactorMap().get(datagramClass.getSimpleName()));

        DQLSelectBuilder nsiEntityDql = new DQLSelectBuilder().fromEntity(NsiEntity.class, "n")
                .column("n")
                .where(eqValue(property("n", NsiEntity.P_ENTITY_ID), entity.getId()))
                .where(eqValue(property("n", NsiEntity.P_TYPE), reactor.getCatalogType()));

        NsiEntity nsiEntity = null;
        List<NsiEntity> nsiEntityList = DataAccessServices.dao().getList(nsiEntityDql);
        if (!CollectionUtils.isEmpty(nsiEntityList))
        {
            nsiEntity = nsiEntityList.get(0);
        }

        if (nsiEntity == null)
            nsiEntity = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(entity, null, null, reactor.getCatalogType());

        K result = reactor.createDatagramObject(nsiEntity.getGuid());

        @SuppressWarnings("unchecked")
        Set<Long> processedEntities = (Set<Long>) commonCache.get(NsiSyncOperatorDAO.PROCESSED);
        if(processedEntities.contains(entity.getId()))
            return new ProcessedDatagramObject<>(result, null);

        processedEntities.add(entity.getId());
        K datagramEntity = reactor.createDatagramObject(nsiEntity.getGuid());
        return new ProcessedDatagramObject<>(result, Collections.singletonList(datagramEntity));
    }

    protected <K1 extends IEntity, K extends IDatagramObject> ProcessedDatagramObject<K> createDatagramObject(Class<K> datagramClass, K1 entity, Map<String, Object> commonCache, StringBuilder warningLog)
    {
        if (NsiSettingsManager.instance().isUseShortLinksRepresentationInDatagrams())
            return createDatagramObjectShort(datagramClass, entity, commonCache);

        @SuppressWarnings("unchecked")
        INsiEntityReactor<K1, K> reactor = (INsiEntityReactor<K1, K>) (datagramClass == null ? this : ReactorHolder.getReactorMap().get(datagramClass.getSimpleName()));

        DQLSelectBuilder nsiEntityDql = new DQLSelectBuilder().fromEntity(NsiEntity.class, "n")
                .column("n")
                .where(eqValue(property("n", NsiEntity.P_ENTITY_ID), entity.getId()))
                .where(eqValue(property("n", NsiEntity.P_TYPE), reactor.getCatalogType()));

        NsiEntity nsiEntity = null;
        List<NsiEntity> nsiEntityList = DataAccessServices.dao().getList(nsiEntityDql);
        if (!CollectionUtils.isEmpty(nsiEntityList))
        {
            nsiEntity = nsiEntityList.get(0);
        }

        if (nsiEntity == null)
            nsiEntity = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(entity, null, null, reactor.getCatalogType());

        K result = reactor.createDatagramObject(nsiEntity.getGuid());

        @SuppressWarnings("unchecked")
        Set<Long> processedEntities = (Set<Long>) commonCache.get(NsiSyncOperatorDAO.PROCESSED);
        if(processedEntities.contains(entity.getId()))
            return new ProcessedDatagramObject<>(result, null);

        processedEntities.add(entity.getId());
        ResultListWrapper r = reactor.createDatagramObject(entity, nsiEntity, commonCache);
        if(r.getWarning() != null)
            warningLog.append("[ID ").append(entity.getId()).append(": ").append(r.getWarning()).append(']');
        return new ProcessedDatagramObject<>(result, r.getList());
    }

    @Override
    public void save(Session session, ChangedWrapper<T> w, V datagramObject, NsiPackage nsiPackage)
    {
        T entity = w.getResult();
        if(w.isChanged()) {
            INsiCollectorDaemon.instance.get().registerIgnoredEntities(entity.getId());
            session.saveOrUpdate(entity);
            session.flush();
        }

        NsiEntity nsiEntity = w.getNsiEntity();
        nsiEntity = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(entity, nsiEntity, datagramObject.getID(), getCatalogType());
        w.setNsiEntity(nsiEntity);
        NsiSyncManager.instance().dao().createEntityLog(nsiEntity, getCatalogType(), nsiPackage, w.getWarning());
    }

    /**
     * Для обеспечени псевдо-уникальности кода сущнотей НСИ в справочниках ОБ, предлагается в случае его занятости в ОБ
     * подставлять к коду НСИ некий префикс, а при отдаче в НСИ данный префикс убирать.
     *
     * @param userCode - пользовательский код из НСИ для проверки на уникальность.
     * @return - обработанный пользователский код (либо как есть, либо с префиксом)
     */
    protected String getUserCodeChecked(Map<Class<? extends IEntity>, Set<String>> usedCodes, String userCode)
    {
        if (null == userCode) return null;
        Class<T> clazz = getEntityClass();

        String resultCode = userCode;
        if (usedCodes.containsKey(clazz) && usedCodes.get(clazz).contains(userCode))
        {
            resultCode = "nsi." + userCode;
            if (usedCodes.get(clazz).contains(resultCode))
                resultCode += System.currentTimeMillis();
        }

        Set<String> usedCatalogCodesSet = usedCodes.get(clazz);
        if (null == usedCatalogCodesSet)
            usedCatalogCodesSet = new HashSet<>();
        usedCatalogCodesSet.add(resultCode);
        usedCodes.put(clazz, usedCatalogCodesSet);

        return resultCode;
    }

    /**
     * Убирает префикс, обеспечивающий уникальность пользователского кода в ОБ, при его наличии.
     *
     * @param userCode - пользовательский код справочника ОБ
     * @return - обработанный пользователский код
     */
    protected String getUserCodeForNSIChecked(String userCode)
    {
        if (null == userCode)
            return null;
        return userCode.replaceAll("nsi.", "");
    }

    private static class Wrapper<K> {
        public K getObject() {
            return _object;
        }

        public void setObject(K object) {
            _object = object;
        }

        K _object;
    }

    /**
     * Запрос из НСИ объекта датаграммы
     * @param reactor реактор
     * @param datagramObject датаграмма
     * @param <K1> класс сущности уни
     * @param <K> класс датаграммы
     * @return возвращает сущность полученную из датаграммы
     * @throws ProcessingException
     */
    protected <K1 extends IEntity, K extends IDatagramObject> CoreCollectionUtils.Pair<DatagramProcessingInfo, K1> retrieve(INsiEntityReactor<K1, K> reactor, K datagramObject) throws ProcessingException
    {
        Wrapper<K1> result = new Wrapper<>();
        ResultWrapper<List<String>> r = NsiRequestHandlerService.instance().executeNSIAction(Collections.singletonList(datagramObject), null, NsiUtils.OPERATION_TYPE_RETRIEVE, null, new IResponseAction() {
            @Override
            public DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list) throws CriticalException {
                NsiSyncManager.instance().operatorDAO().update(list, subSystemLog.getPack(), null, subSystemLog.getGuid());
                NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.guid(), datagramObject.getID());
                // есть ли он в базе? если есть, возвращаем
                if (nsiEntity != null) {
                    IEntity entity = DataAccessServices.dao().get(nsiEntity.getEntityId());
                    try {
                        @SuppressWarnings("unchecked")
                        K1 object = (K1) entity;
                        result.setObject(object);
                    } catch (ClassCastException e) {
                        throw new CriticalException("Datagram object " + datagramObject.getID() + " is not " + reactor.getEntityClass().getSimpleName());
                    }
                }
                return null;
            }
        }, false);
        DatagramProcessingInfo info = new DatagramProcessingInfo(StringUtils.trimToNull(StringUtils.join(r.getResult(), '\n')), r.getWarning());
        return new CoreCollectionUtils.Pair<>(info, result.getObject());
    }

    protected List<T> findEntity(IDQLExpression expression)
    {
        return findEntity(getEntityClass(), expression);
    }

    protected <K extends IEntity> List<K> findEntity(Class<K> clazz, IDQLExpression expression)
    {
        IEntityMeta meta = EntityRuntime.getMeta(getEntityClass());
        DQLSelectBuilder nsiEntityBuilder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                .column(property("nsi", NsiEntity.entityId()))
                .where(eq(property("nsi", NsiEntity.entityId()), property(ENTITY_ALIAS, IEntity.P_ID)))
                .where(eq(property("nsi", NsiEntity.entityType()), value(meta.getEntityClass().getName())));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(clazz, ENTITY_ALIAS)
                .column(property(ENTITY_ALIAS))
                .where(expression)
                .where(notExists(nsiEntityBuilder.buildQuery()));

        return DataAccessServices.dao().getList(builder);
    }

    /**
     * Обработка вложенных объектов
     * @param k
     * @param params
     * @param commonCache
     * @param datagramObject
     * @param alias
     * @param <K1>
     * @param <K>
     * @return
     * @throws ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException
     */

    @SuppressWarnings("unchecked")
    protected <K1 extends IEntity, K extends IDatagramObject> K1 getOrCreate(Class<K> k, @NotNull Map<String, Object> params, Map<String, Object> commonCache, K datagramObject, String alias, Map<String, Object> additionalParams, StringBuilder warningLog) throws ProcessingDatagramObjectException
    {
        if(datagramObject == null)
            return null;

        if (params!=null)
        {
            K1 entity = (K1) params.get(alias);
            if (entity != null)
                return entity;
        }
        if(StringUtils.trimToNull(datagramObject.getID()) == null) {
            warningLog.append('[').append(alias).append(" without GUID!]");
            return null;
        }

        INsiEntityReactor<K1, K> reactor = (INsiEntityReactor<K1, K>) ReactorHolder.getReactorMap().get(k.getSimpleName());
        CoreCollectionUtils.Pair<IDatagramObject, ChangedWrapper<K1>> pair = null;
        IDatagramObject object = null;
        // сначала смотрим в кэше, обрабатывается ли данный элемент в датаграмме
        if(commonCache != null) {
            Map<String, CoreCollectionUtils.Pair<IDatagramObject, ChangedWrapper<K1>>> datagramMap = (Map<String, CoreCollectionUtils.Pair<IDatagramObject, ChangedWrapper<K1>>>) commonCache.get(NsiSyncOperatorDAO.ITEMS_MAP);
            pair = datagramMap == null ? null : datagramMap.get(datagramObject.getID());
            // если обрабатывается...
            if(pair != null && pair.getY() != null) {
                object = pair.getX();
                IEntity entity = pair.getY().getResult();
                // если уже обработан, то пробуем вернуть результат обработки
                if(entity != null) {
                    try {
                        return (K1) entity;
                    } catch (ClassCastException e) {
                        throw new ProcessingDatagramObjectException("Datagram object " + datagramObject.getID() + " is not " + reactor.getEntityClass().getSimpleName());
                    }
                }
            }
        }
        // объект больше не встречается в датаграмме
        if(object == null) {
            NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.guid(), datagramObject.getID());
            // есть ли он в базе? если есть, возвращаем
            if (nsiEntity != null) {
                IEntity entity = DataAccessServices.dao().get(nsiEntity.getEntityId());
                try {
                    K1 k1 = (K1) entity;

                    if(pair != null)
                        pair.setY(new ChangedWrapper<>(false, false, nsiEntity, k1, null, null));
                    if (k1 != null)
                        return k1;
                } catch (ClassCastException e) {
                    throw new ProcessingDatagramObjectException("Datagram object " + datagramObject.getID() + " is not " + reactor.getEntityClass().getSimpleName());
                }
            }
            // в базе его тоже нет, запрашиваем
            try {
                CoreCollectionUtils.Pair<DatagramProcessingInfo, K1> retrieveResult = retrieve(reactor, datagramObject);
                if(pair != null)
                    pair.setY(new ChangedWrapper<>(false, false, nsiEntity, retrieveResult.getY(), null, retrieveResult.getX().getWarn()));
                if(retrieveResult.getX().getError() != null || retrieveResult.getX().getWarn() != null)
                    warningLog.append("Error [ID ").append(datagramObject.getID()).append(": ").append(retrieveResult.getX().getError()).append(']');
                return retrieveResult.getY();
            } catch (ProcessingException e) {
                throw new ProcessingDatagramObjectException("Can't retrieve: " + e.getMessage());
            }
        }
        // объект есть в датаграмме и ещё не обработан, обрабатываем
        K castedDatagramObject;
        try {
            castedDatagramObject = (K) object;
        } catch (ClassCastException e) {
            throw new ProcessingDatagramObjectException("Datagram object " + object.getID() + " is not " + k.getSimpleName());
        }

        Map<String, Map<String, Object>> reactorCache = ((Map<String, Map<String, Object>>) commonCache.get(NsiSyncOperatorDAO.REACTOR_CACHE));
        ChangedWrapper<K1> wrapper = reactor.processDatagramObject(castedDatagramObject, commonCache, reactorCache == null ? null : reactorCache.get(reactor.getCatalogType()), additionalParams);
        params.put(alias, wrapper);
        params.put(alias + PARAM, datagramObject);
        if (StringUtils.isNotEmpty(wrapper.getWarning()))
            warningLog.append("[ID ").append(datagramObject.getID()).append(": ").append(wrapper.getWarning()).append(']');
        pair.setY(wrapper);
        return wrapper.getResult();
    }

    protected CoreCollectionUtils.Pair<T, NsiEntity> getFromCache(String guid, Map<String, Object> reactorCache) {
        @SuppressWarnings("unchecked")
        Map<String, CoreCollectionUtils.Pair<T, NsiEntity>> nsiMap = (Map<String, CoreCollectionUtils.Pair<T, NsiEntity>>) reactorCache.get(NSI_MAP);
        if(nsiMap == null)
            return null;
        return nsiMap.get(guid);
    }

    @Override
    public List<IDatagramObject> createTestObjects() {
        return null;
    }

    @Override
    public V getEmbedded(boolean onlyGuid) { return null;}

    @Override
    public boolean isCorrect(T entity, V datagramObject, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException {
        return false;
    }

    protected IDatagramObject findDatagramObject(List<IDatagramObject> retrievedDatagram, String guid)
    {
        for (IDatagramObject datagramObject : retrievedDatagram)
            if (datagramObject.getID().equals(guid))
                return datagramObject;
        return null;
    }

    @Deprecated
    @Override
    public boolean isCorrect(V datagramObject, V datagramForCheckRemoteOperation, List<IDatagramObject> retrievedDatagram) throws ProcessingDatagramObjectException {
        return false;
    }

    private Map<String, Object> getReactorCache(IDatagramObject datagramObject)
    {
        Map<String, Object> reactorCache = new HashMap<>();
        try
        {
            preUpdate(Collections.singletonList(datagramObject), reactorCache);
        }
        catch (ProcessingException e)
        {
            e.printStackTrace();
        }
        return reactorCache;
    }

    protected <K1 extends IEntity, K extends IDatagramObject> void saveChildEntity(Class<K> clazz, Session session, Map<String, Object> params, String alias, NsiPackage nsiPackage) {
        @SuppressWarnings("unchecked")
        ChangedWrapper<K1> wrapper = (ChangedWrapper<K1>) params.get(alias);
        if(wrapper != null) {
            @SuppressWarnings("unchecked")
            INsiEntityReactor<K1, K> reactor = (INsiEntityReactor<K1, K>) ReactorHolder.getReactorMap().get(clazz.getSimpleName());
            reactor.save(session, wrapper, (K) params.get(alias + PARAM), nsiPackage);
            //session.flush();
        }
    }

    /**
     * Метод возвращает список с названием полей, не использующихся в НСИ.
     * Т.к. поля недоступны из НСИ, то их можно не учитывать при тестировании.
     * (данный список используется исключительно в тестах)
     *
     * @return список с названиями полей
     */
    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = new ArrayList<>();
        fields.add("updateDate");
        fields.add("version");
        fields.add("disabledDate");
        return  fields;
    }
}