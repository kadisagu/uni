/* $Id: INsiDeliveryDaemon.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2015
 */
public interface INsiDeliveryDaemon
{
    final String GLOBAL_DAEMON_LOCK = INsiDeliveryDaemon.class.getName() + ".global-lock";
    final SpringBeanCache<INsiDeliveryDaemon> instance = new SpringBeanCache<>(INsiDeliveryDaemon.class.getName());

    /**
     * Производит отправку подготовленного пакета в НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    List<Long> getPreparedPackageIds(Long queueId);

    /**
     * Удаляет зацикленные пакеты.
     * Зацикленными считает те, число попыток по которым превышает 20
     */
    void doCleanupQueueFromLoopedPacks();
}