/* $Id: IBaseWebServiceClientTest.java 44273 2015-07-24 13:42:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiTests.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 18.05.2015
 */
public interface IBaseWebServiceClientTest extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean insertTest(List<IDatagramObject> datagrams, NsiSubSystem subSystem) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean retrieveTest(List<IDatagramObject> datagrams, NsiSubSystem subSystem) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean deleteTest(List<IDatagramObject> datagrams, NsiSubSystem subSystem) throws Exception;
}
