package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.nsiclient.entity.NsiSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiSettingsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiSettings";
    public static final String ENTITY_NAME = "nsiSettings";
    public static final int VERSION_HASH = -1852153220;
    private static IEntityMeta ENTITY_META;

    public static final String P_MAX_PACKAGE_SIZE = "maxPackageSize";
    public static final String P_ASYNC = "async";
    public static final String P_TIME_INTERVALS = "timeIntervals";

    private Integer _maxPackageSize;     // Максимальный размер пакета
    private boolean _async = false;     // Асинхронный обмен
    private String _timeIntervals = "60,60,300,300,60,1800,60,3600,60,14400";     // Время между попытками отправок (через запятую)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Максимальный размер пакета.
     */
    public Integer getMaxPackageSize()
    {
        return _maxPackageSize;
    }

    /**
     * @param maxPackageSize Максимальный размер пакета.
     */
    public void setMaxPackageSize(Integer maxPackageSize)
    {
        dirty(_maxPackageSize, maxPackageSize);
        _maxPackageSize = maxPackageSize;
    }

    /**
     * @return Асинхронный обмен. Свойство не может быть null.
     */
    @NotNull
    public boolean isAsync()
    {
        return _async;
    }

    /**
     * @param async Асинхронный обмен. Свойство не может быть null.
     */
    public void setAsync(boolean async)
    {
        dirty(_async, async);
        _async = async;
    }

    /**
     * @return Время между попытками отправок (через запятую). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTimeIntervals()
    {
        return _timeIntervals;
    }

    /**
     * @param timeIntervals Время между попытками отправок (через запятую). Свойство не может быть null.
     */
    public void setTimeIntervals(String timeIntervals)
    {
        dirty(_timeIntervals, timeIntervals);
        _timeIntervals = timeIntervals;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiSettingsGen)
        {
            setMaxPackageSize(((NsiSettings)another).getMaxPackageSize());
            setAsync(((NsiSettings)another).isAsync());
            setTimeIntervals(((NsiSettings)another).getTimeIntervals());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiSettings.class;
        }

        public T newInstance()
        {
            return (T) new NsiSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "maxPackageSize":
                    return obj.getMaxPackageSize();
                case "async":
                    return obj.isAsync();
                case "timeIntervals":
                    return obj.getTimeIntervals();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "maxPackageSize":
                    obj.setMaxPackageSize((Integer) value);
                    return;
                case "async":
                    obj.setAsync((Boolean) value);
                    return;
                case "timeIntervals":
                    obj.setTimeIntervals((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "maxPackageSize":
                        return true;
                case "async":
                        return true;
                case "timeIntervals":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "maxPackageSize":
                    return true;
                case "async":
                    return true;
                case "timeIntervals":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "maxPackageSize":
                    return Integer.class;
                case "async":
                    return Boolean.class;
                case "timeIntervals":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiSettings> _dslPath = new Path<NsiSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiSettings");
    }
            

    /**
     * @return Максимальный размер пакета.
     * @see ru.tandemservice.nsiclient.entity.NsiSettings#getMaxPackageSize()
     */
    public static PropertyPath<Integer> maxPackageSize()
    {
        return _dslPath.maxPackageSize();
    }

    /**
     * @return Асинхронный обмен. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSettings#isAsync()
     */
    public static PropertyPath<Boolean> async()
    {
        return _dslPath.async();
    }

    /**
     * @return Время между попытками отправок (через запятую). Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSettings#getTimeIntervals()
     */
    public static PropertyPath<String> timeIntervals()
    {
        return _dslPath.timeIntervals();
    }

    public static class Path<E extends NsiSettings> extends EntityPath<E>
    {
        private PropertyPath<Integer> _maxPackageSize;
        private PropertyPath<Boolean> _async;
        private PropertyPath<String> _timeIntervals;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Максимальный размер пакета.
     * @see ru.tandemservice.nsiclient.entity.NsiSettings#getMaxPackageSize()
     */
        public PropertyPath<Integer> maxPackageSize()
        {
            if(_maxPackageSize == null )
                _maxPackageSize = new PropertyPath<Integer>(NsiSettingsGen.P_MAX_PACKAGE_SIZE, this);
            return _maxPackageSize;
        }

    /**
     * @return Асинхронный обмен. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSettings#isAsync()
     */
        public PropertyPath<Boolean> async()
        {
            if(_async == null )
                _async = new PropertyPath<Boolean>(NsiSettingsGen.P_ASYNC, this);
            return _async;
        }

    /**
     * @return Время между попытками отправок (через запятую). Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiSettings#getTimeIntervals()
     */
        public PropertyPath<String> timeIntervals()
        {
            if(_timeIntervals == null )
                _timeIntervals = new PropertyPath<String>(NsiSettingsGen.P_TIME_INTERVALS, this);
            return _timeIntervals;
        }

        public Class getEntityClass()
        {
            return NsiSettings.class;
        }

        public String getEntityName()
        {
            return "nsiSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
