/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.ui.AsyncWaitersList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.AsyncWaitersListDSHandler;

/**
 * @author Andrey Nikonov
 * @since 13.11.2015
 */
@Configuration
public class NsiSyncAsyncWaitersList extends BusinessComponentManager {
    public static final String WAITER_LIST_DS = "waiterListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(WAITER_LIST_DS, catalogListCL(), waiterListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint catalogListCL()
    {
        return columnListExtPointBuilder(WAITER_LIST_DS)
                .addColumn(textColumn("title", DataWrapper.TITLE))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert(WAITER_LIST_DS + DELETE_ALERT_TAIL, DataWrapper.TITLE)))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler waiterListDSHandler()
    {
        return new AsyncWaitersListDSHandler(getName());
    }
}
