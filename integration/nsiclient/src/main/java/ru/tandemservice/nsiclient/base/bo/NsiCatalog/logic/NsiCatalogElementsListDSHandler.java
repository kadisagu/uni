/* $Id: NsiCatalogElementsListDSHandler.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic;

import com.google.common.base.Function;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.support.FilterItem;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 06.03.2015
 */
public class NsiCatalogElementsListDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String FILTERS = "filters";
    public static final String ORDER_DESCRIPTIONS = "orderDescriptions";

    public static final String ENTITY_ALIAS = "e";
    public static final String NSI_ENTITY_ALIAS = "nsi";

    public NsiCatalogElementsListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Class<? extends IEntity> entityClass = getEntityClass(context);
        List<FilterItem> filterItems = context.get(FILTERS);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(entityClass, ENTITY_ALIAS)
                .column(property(ENTITY_ALIAS))
                .column(property(NSI_ENTITY_ALIAS))
                .joinEntity(ENTITY_ALIAS, DQLJoinType.inner, NsiEntity.class, NSI_ENTITY_ALIAS, eq(property(ENTITY_ALIAS, IEntity.P_ID), property(NSI_ENTITY_ALIAS, NsiEntity.entityId())));
        if(filterItems != null)
            for (FilterItem filterItem: filterItems)
            {
                Object value = filterItem.getValue();
                if(value != null)
                {
                    if(filterItem.getType() == FilterItem.TYPE.TEXT)
                    {
                        if (filterItem.getFieldName().equals(NsiEntity.P_GUID))
                        {
                            builder.where(likeUpper(property(NSI_ENTITY_ALIAS, filterItem.getFieldName()), commonValue(CoreStringUtils.escapeLike((String) value, true), PropertyType.STRING)));
                        }
                        else
                        {
                            builder.where(likeUpper(property(ENTITY_ALIAS, filterItem.getFieldName()), commonValue(CoreStringUtils.escapeLike((String) value, true), PropertyType.STRING)));
                        }
                    }
                    else if(filterItem.getType() == FilterItem.TYPE.BOOLEAN)
                    {
                        if (value instanceof DataWrapper)
                            builder.where(eq(property(ENTITY_ALIAS, filterItem.getFieldName()), commonValue(TwinComboDataSourceHandler.getSelectedValue(value), PropertyType.BOOLEAN)));
                    }
                    else if(filterItem.getType() == FilterItem.TYPE.DATE)
                    {
                        if (value instanceof CoreCollectionUtils.Pair)
                        {
                            @SuppressWarnings("unchecked")
                            CoreCollectionUtils.Pair<Date, Date> dates = (CoreCollectionUtils.Pair<Date, Date>) value;
                            builder.where(betweenDays("e." + filterItem.getFieldName(), dates.getX(), dates.getY()));
                        }
                    }
                    else if(filterItem.getType() == FilterItem.TYPE.INTEGER)
                        builder.where(eq(property(ENTITY_ALIAS, filterItem.getFieldName()), commonValue(value, PropertyType.LONG)));
                    else if(filterItem.getType() == FilterItem.TYPE.DOUBLE)
                        builder.where(eq(property(ENTITY_ALIAS, filterItem.getFieldName()), commonValue(value, PropertyType.DOUBLE)));
                    else if(filterItem.getType() == FilterItem.TYPE.ENTITY)
                    {
                        if (value instanceof IEntity)
                            builder.where(eq(property(ENTITY_ALIAS, filterItem.getFieldName() + '.' + IEntity.P_ID), commonValue(((IEntity) value).getId(), PropertyType.LONG)));
                    }
                }
            }
        List<CoreCollectionUtils.Pair<String, OrderDescription[]>> orderDescriptions = context.get(ORDER_DESCRIPTIONS);

        DSOutput output;
        if (orderDescriptions != null)
        {
            DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(getEntityClass(), ENTITY_ALIAS);
            orderRegistry.addAdditionalAlias(NsiEntity.class, NSI_ENTITY_ALIAS);
            for (CoreCollectionUtils.Pair<String, OrderDescription[]> desc : orderDescriptions)
                orderRegistry.setOrders(desc.getX(), desc.getY());
            output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(isPageable()).order(orderRegistry).build();
        }
        else
            output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(isPageable()).order().build();

        return output.transform(new Function<Object[], ElementWrapper>()
                {
                    @Override
                    public ElementWrapper apply(Object[] input)
                    {
                        return new ElementWrapper((IEntity) input[0], (NsiEntity) input[1]);
                    }
                });
    }
}