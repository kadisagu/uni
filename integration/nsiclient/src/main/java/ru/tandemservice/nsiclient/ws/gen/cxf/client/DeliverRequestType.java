
package ru.tandemservice.nsiclient.ws.gen.cxf.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeliverRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeliverRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="routingHeader" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}RoutingHeaderType" minOccurs="0"/>
 *         &lt;element name="confirmation" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service}deliverDataType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeliverRequestType", propOrder = {
    "routingHeader",
    "confirmation"
})
public class DeliverRequestType {

    protected RoutingHeaderType routingHeader;
    @XmlElement(required = true)
    protected DeliverDataType confirmation;

    /**
     * Gets the value of the routingHeader property.
     *
     * @return
     *     possible object is
     *     {@link RoutingHeaderType }
     *
     */
    public RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }

    /**
     * Sets the value of the routingHeader property.
     *
     * @param value
     *     allowed object is
     *     {@link RoutingHeaderType }
     *
     */
    public void setRoutingHeader(RoutingHeaderType value) {
        this.routingHeader = value;
    }

    /**
     * Gets the value of the confirmation property.
     *
     * @return
     *     possible object is
     *     {@link DeliverDataType }
     *
     */
    public DeliverDataType getConfirmation() {
        return confirmation;
    }

    /**
     * Sets the value of the confirmation property.
     *
     * @param value
     *     allowed object is
     *     {@link DeliverDataType }
     *
     */
    public void setConfirmation(DeliverDataType value) {
        this.confirmation = value;
    }

}
