/* $Id: NsiSubSystemEditAddressUI.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.EditAddress;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
})
public class NsiSubSystemEditAddressUI extends UIPresenter
{
    private Long _id;
    private NsiSubSystem _catalog;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public void onComponentActivate()
    {
        _catalog = DataAccessServices.dao().get(NsiSubSystem.class, _id);
    }

    public void onSave()
    {
        DataAccessServices.dao().update(_catalog);
        deactivate();
    }
}
