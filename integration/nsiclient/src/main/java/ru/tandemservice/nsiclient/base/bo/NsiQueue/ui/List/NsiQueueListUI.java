/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiQueue.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.List.NsiPackageList;
import ru.tandemservice.nsiclient.base.bo.NsiQueue.logic.NsiQueueDSHandler;
import ru.tandemservice.nsiclient.base.bo.NsiQueue.logic.NsiQueueWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.NsiDeliveryDaemon;
import ru.tandemservice.nsiclient.utils.NsiUtils;

/**
 * @author Dmitry Seleznev
 * @since 18.01.2017
 */
public class NsiQueueListUI extends UIPresenter
{
    @Override
    public void onComponentActivate()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NsiPackageList.OPERATION_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true,
                    NsiQueueDSHandler.CREATE_DATE_FROM,
                    NsiQueueDSHandler.CREATE_DATE_TO,
                    NsiQueueDSHandler.MESSAGE_GUID,
                    NsiQueueDSHandler.DIRECTION_TYPE,
                    NsiQueueDSHandler.OPERATION_TYPE,
                    NsiQueueDSHandler.CATALOG_ID));
        }
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        for (String filterName : NsiQueueDSHandler.FILTERS_SET) _uiSettings.remove(filterName);
        getSettings().save();
    }

    public boolean isDeliveryDaemonLocked()
    {
        return NsiDeliveryDaemon.isLocked();
    }

    public boolean isDeliveryDaemonActive()
    {
        return NsiDeliveryDaemon.DAEMON.isDaemonAwoke();
    }

    public void onInterruptDeliveryDaemon()
    {
        NsiDeliveryDaemon.interruptMessageProccessing(null);
    }

    public void onLockDeliveryDaemon()
    {
        NsiDeliveryDaemon.lockDaemon();
    }

    public void onUnlockDeliveryDaemon()
    {
        NsiDeliveryDaemon.unlockDaemon();
    }

    public void onDeleteFromQueue()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onGetMessagePack()
    {
        NsiQueueWrapper wrapper = getConfig().getDataSource(NsiQueueList.OPERATION_LOG_DS).getRecordById(getListenerParameterAsLong());
        String text = NsiUtils.createPackFile(wrapper.getQueue().getLog().getPack());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt").fileName(wrapper.getQueue().getLog().getPack().getId() + ".txt").document(text.getBytes()), true);
    }

    public String getListSettingsKey()
    {
        return super.getSettingsKey() + ".list";
    }
}
