/* $Id:$ */
package ru.tandemservice.nsiclient.reactor;

import org.tandemframework.core.entity.IEntity;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 19.09.2016
 */
public interface INsiHierarchyReactor
{
    <T extends IEntity> List<T> getHierarchyList(int packageSize);
}