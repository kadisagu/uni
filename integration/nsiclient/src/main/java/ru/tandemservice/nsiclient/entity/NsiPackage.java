package ru.tandemservice.nsiclient.entity;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.NsiPackageManager;
import ru.tandemservice.nsiclient.entity.gen.NsiPackageGen;

/** @see ru.tandemservice.nsiclient.entity.gen.NsiPackageGen */
public class NsiPackage extends NsiPackageGen
{
    public static int STATUS_DRAFT = 0;
    public static int STATUS_IN_WORK = 1;
    public static int STATUS_EXECUTED = 2;

    public String getMessageStatus()
    {
        DataWrapper wrapper = NsiPackageManager.instance().messageStatusExtPoint().getItem(Integer.toString(getStatus()));
        return wrapper != null ? wrapper.getTitle() : "Не известен";
    }
}