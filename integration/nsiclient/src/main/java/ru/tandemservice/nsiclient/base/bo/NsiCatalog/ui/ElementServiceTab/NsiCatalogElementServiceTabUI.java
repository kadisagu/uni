package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementServiceTab;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView.NsiCatalogElementView;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@State
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true)
        })
public class NsiCatalogElementServiceTabUI extends UIPresenter
{
    private Long _id;
    private String _code;
    private NsiEntity _entity;
    private CoreCollectionUtils.Pair<String, String> _currValue;
    private List<CoreCollectionUtils.Pair<String, String>> _values;

    public CoreCollectionUtils.Pair<String, String> getCurrValue()
    {
        return _currValue;
    }

    public List<CoreCollectionUtils.Pair<String, String>> getValues()
    {
        return _values;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public void onComponentActivate()
    {
        _entity = DataAccessServices.dao().getNotNull(_id);
        _values = Lists.newArrayList();

        IEntityMeta meta = EntityRuntime.getMeta(_entity);
        _code = meta.getName();
        Collection<IFieldPropertyMeta> fields = meta.getDeclaredFields();
        Map<String, IFieldPropertyMeta> fieldsMap = new HashMap<>(3);
        for(IFieldPropertyMeta field : fields)
            if(NsiEntity.P_CREATE_DATE.equals(field.getName()) || NsiEntity.P_UPDATE_DATE.equals(field.getName()))
                fieldsMap.put(field.getName(), field);

        IFieldPropertyMeta field = fieldsMap.get(NsiEntity.P_CREATE_DATE);
        _values.add(new CoreCollectionUtils.Pair<>(field.getTitle(), getStringFromObject(_entity.getProperty(field.getName()))));
        field = fieldsMap.get(NsiEntity.P_UPDATE_DATE);
        _values.add(new CoreCollectionUtils.Pair<>(field.getTitle(), getStringFromObject(_entity.getProperty(field.getName()))));
    }

    private static String getStringFromObject(Object object)
    {
        if(object == null)
            return null;
        if(object instanceof String)
            return (String) object;
        if(object instanceof Boolean)
            return (Boolean) object ? "да" : "нет";
        return object.toString();
    }

    public void onViewElement()
    {
        _uiActivation.asDesktopRoot(NsiCatalogElementView.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter(NsiCatalogElementView.SELECTED_TAB, NsiCatalogElementView.INFO_TAB)
                .activate();
    }
}
