/* $Id$ */
package ru.tandemservice.nsiclient.base.bo.NsiReactorTest.logic;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 08.10.2015
 */
public interface INsiReactorTestDao
{

    /**
     * Запускает тестирования реактора
     *
     * @param catalogTypeId     идентификатор справочника NsiCatalogType, реактор которого нужно протестировать.
     * @param createdEntityList список, куда будут записаны созданные сущнотси (для их последующего удаления)
     * @param logger            логгер
     * @return возвращает true - если объекты в процессе работы реактора не изменились (тест пройден), иначе - false
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    boolean runReactorTest(Long catalogTypeId, List<IEntity> createdEntityList, final Logger logger) throws Exception;
}
