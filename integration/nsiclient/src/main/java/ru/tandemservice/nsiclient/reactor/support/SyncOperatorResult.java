/* $Id: ResultListWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

import ru.tandemservice.nsiclient.datagram.IDatagramObject;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
public class SyncOperatorResult
{
    private final String _warning;
    private String _error;
    private final List<IDatagramObject> _list;

    public List<IDatagramObject> getList() {
        return _list;
    }

    public String getWarning()
    {
        return _warning;
    }

    public String getError()
    {
        return _error;
    }

    public SyncOperatorResult(String warning, List<IDatagramObject> list)
    {
        _warning = warning;
        _list = list;
    }

    public SyncOperatorResult(String warning, String error, List<IDatagramObject> list)
    {
        _warning = warning;
        _error = error;
        _list = list;
    }
}