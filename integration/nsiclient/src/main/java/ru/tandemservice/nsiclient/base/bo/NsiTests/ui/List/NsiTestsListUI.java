/* $Id: NsiTestsListUI.java 44273 2015-07-24 13:42:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiTests.ui.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.core.io.Resource;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.ResourceLocationEnum;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.spring.CoreSpringUtils;
import org.tandemframework.core.view.UIDefines;
import org.testng.*;
import org.testng.xml.Parser;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.xml.sax.SAXException;
import ru.tandemservice.nsiclient.base.bo.NsiTests.NsiTestsManager;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 15.05.2015
 */
public class NsiTestsListUI extends UIPresenter
{
    private NsiCatalogType _catalog;
    private NsiSubSystem _nsiSubSystem;
    private List<TestWrapper> _tests;
    private TestError _testError;
    private boolean _passTest = false;
    private String _message = "";
    long index = 0L;

    @Override
    public void onComponentRefresh()
    {
        _message = "";
        _passTest = false;
    }

    @Override
    public void onComponentActivate()
    {
        _tests = new ArrayList<>();

        try
        {
            List<Resource> resources = CoreSpringUtils.getModuleResourcesByPattern(ApplicationRuntime.getModuleMeta("unimsu2"), ResourceLocationEnum.resources, "test/ReactorsTestSuite.xml");
            if (resources != null)
            {
                Parser parser = new Parser(resources.get(0).getInputStream());
                List<XmlSuite> suites = parser.parseToList();
                for (XmlSuite xmlSuite : suites)
                {
                    TestWrapper wrapper = new TestWrapper(index++, xmlSuite.getName(), xmlSuite, null, null, null);
                    buildTree(wrapper, index);
                    _tests.add(wrapper);
                }
            }
        }
        catch (IOException | ParserConfigurationException | SAXException e)
        {
            e.printStackTrace();
        }
    }

    private void buildTree(TestWrapper parent, Long index)
    {
        XmlSuite parentSuite = parent.getSuite();
        for (XmlSuite xmlSuite : parentSuite.getChildSuites())
        {
            TestWrapper wrapper = new TestWrapper(index++, xmlSuite.getName(), xmlSuite, null, null, null);
            wrapper.setHierarchyParent(parent);
            parent.getChildren().add(wrapper);
            _tests.add(wrapper);
            buildTree(wrapper, index);
        }
        for (XmlTest xmlTest : parentSuite.getTests())
        {
            TestWrapper wrapper = new TestWrapper(index++, xmlTest.getName(), parentSuite, xmlTest, null, null);
            wrapper.setHierarchyParent(parent);
            parent.getChildren().add(wrapper);
            _tests.add(wrapper);

            for (XmlClass xmlClass : xmlTest.getClasses())
            {
                TestWrapper w = new TestWrapper(index++, xmlClass.getName(), parentSuite, xmlTest, xmlClass, null);
                w.setHierarchyParent(wrapper);
                wrapper.getChildren().add(w);
                _tests.add(w);
            }
        }
    }

    public TestWrapper getCurrentTest()
    {
        return getConfig().getDataSource(NsiTestsList.TEST_DS).getCurrent();
    }

    //listeners
    public void onClickExecuteInsertTest() throws Exception
    {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        final INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = reactors.get(_catalog.getCode());
        _passTest = NsiTestsManager.instance().testDao().insertTest(reactor.createTestObjects(), getNsiSubSystem());
        setMessage(_passTest);
    }

    public void onClickExecuteRetrieveTest() throws Exception
    {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        final INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = reactors.get(_catalog.getCode());
        _passTest = NsiTestsManager.instance().testDao().retrieveTest(reactor.createTestObjects(), getNsiSubSystem());
        setMessage(_passTest);
    }

    public void onClickExecuteDeleteTest() throws Exception
    {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        final INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = reactors.get(_catalog.getCode());
        _passTest = NsiTestsManager.instance().testDao().deleteTest(reactor.createTestObjects(), getNsiSubSystem());
        setMessage(_passTest);
    }


    public void onClickExecuteAllTestForCatalog() throws Exception
    {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        final INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = reactors.get(_catalog.getCode());
        _passTest = true;
        boolean passInsertTest = NsiTestsManager.instance().testDao().insertTest(reactor.createTestObjects(), getNsiSubSystem());
        boolean passRetrieveTest = NsiTestsManager.instance().testDao().retrieveTest(reactor.createTestObjects(), getNsiSubSystem());
        boolean passDeleteTest = NsiTestsManager.instance().testDao().deleteTest(reactor.createTestObjects(), getNsiSubSystem());
        setMessage(passInsertTest);
        setMessage(passRetrieveTest);
        setMessage(passDeleteTest);
        _passTest = passInsertTest && passRetrieveTest && passDeleteTest;
    }

    public void onRunTest()
    {
        final TestWrapper test = getConfig().getDataSource(NsiTestsList.TEST_DS).getRecordById(getListenerParameterAsLong());
        //final TestWrapper test = getCurrentTest();
        TestListenerAdapter tla = new TestListenerAdapter()
        {
            @Override
            public void onFinish(final ITestContext testContext)
            {
                IResultMap map = testContext.getFailedTests();
                if (map.size() != 0)
//                    TapSupportUtils.addInitScript(new IScriptAction()
//                    {
//                        @Override
//                        public String getScript(IRequestCycle cycle)
//                        {
//                            Date start = testContext.getStartDate();
//                            Date end = testContext.getEndDate();
//                            long time = end.getTime() - start.getTime();
//                            return "alert(\"Тест выполнен за " + time + "ms \")";
//                        }
//                    });
                {
                    StringBuilder b = new StringBuilder();
                    for (ITestResult result : map.getAllResults())
                    {
                        Throwable throwable = result.getThrowable();
                        TestError error = new TestError(throwable, throwable.toString(), ExceptionUtils.getStackTrace(throwable));
                        b.append(error.getTitle());
                        test.getErrors().add(error);
//                        TestWrapper wrapper = new TestWrapper(index++, result.getMethod().getMethodName(), test.getSuite(), test.getTest(), result.getMethod());
//                        wrapper.setHierarchyParent(test);
//                        wrapper.setError(ExceptionUtils.getStackTrace(result.getThrowable()));
//                        test.getChildren().add(wrapper);
//                        _tests.add(wrapper);
//                        result.getThrowable().printStackTrace();
                    }
                    if (b.length() > 0)
                        test.setError(b.toString());
                }
                Date start = testContext.getStartDate();
                Date end = testContext.getEndDate();
                long time = end.getTime() - start.getTime();
                test.setTime(time + "ms");
            }
        };
        TestNG testng = new TestNG();
        if (test.getXmlClass() != null)
            testng.setTestClasses(new Class[]{test.getXmlClass().getSupportClass()});
        else
        {
            if (test.getTest() != null)
            {
                Class[] classes = new Class[test.getTest().getClasses().size()];
                for (int i = 0; i < classes.length; i++)
                    classes[i] = test.getTest().getClasses().get(i).getSupportClass();
                testng.setTestClasses(classes);
            }
            else
                testng.setXmlSuites(Collections.singletonList(test.getSuite()));
        }

        testng.setGroups("insert");
        testng.addListener(tla);
        test.clearErrors();

        testng.run();
    }

    public NsiSubSystem getNsiSubSystem()
    {
        return _nsiSubSystem;
    }

    public void setNsiSubSystem(NsiSubSystem nsiSubSystem)
    {
        _nsiSubSystem = nsiSubSystem;
    }

    public NsiCatalogType getCatalog()
    {
        return _catalog;
    }

    public void setCatalog(NsiCatalogType catalog)
    {
        _catalog = catalog;
    }

    public boolean isPassTest()
    {
        return _passTest;
    }

    public String getMessage()
    {
        return _message;
    }

    public void setMessage(boolean passTest)
    {
        _message = "";
        _message += "\n";
        _message += getCatalog().getTitle();
        _message += passTest ? ": тест пройден" : ": тест не пройден";
    }

    public boolean isShowMessage()
    {
        return !StringUtils.isEmpty(getMessage());
    }

    private List<IDatagramObject> getDatagramObject()
    {
        switch (getCatalog().getTitle())
        {
//            case NsiTestsList.OKSM_TEST:
//                return TestDatagrams.getOksmDatagram();
//            case NsiTestsList.REL_DEGREE_TEST:
//                return TestDatagrams.getRelDegreeDatagram();
//            case NsiTestsList.POST_TYPE_TEST:
//                return TestDatagrams.getPostType();
//            case NsiTestsList.CARD_KIND_TYPE_TEST:
//                return TestDatagrams.getCardKindType();
//            case NsiTestsList.EXT_ORGUNIT_TEST:
//                return TestDatagrams.getOrganizationType();
//            case NsiTestsList.HUMAN_TYPE_TEST:
//                return TestDatagrams.getHumanType();
//            case NsiTestsList.GRADE_TYPE_TEST:
//                return TestDatagrams.getGradeType();
//            case NsiTestsList.DEPARTMENT_TYPE_TEST:
//                return TestDatagrams.getDepartmentType();
//            case NsiTestsList.EMPLOYEE_TYPE_TEST:
//                return TestDatagrams.getEmployeeType();
//            case NsiTestsList.CARD_TYPE_TEST:
//                return TestDatagrams.getCardType();
//            case NsiTestsList.BPM_REG_DEPARTMENT_TYPE_TEST:
//                return TestDatagrams.getBpmRegDepartmentType();
//            case NsiTestsList.BPM_REG_DEPARTMENT_MEMBER_TEST:
//                return TestDatagrams.getBpmRegDepartmentMemberType();
//            case NsiTestsList.BPM_REG_PERSON_ASSISTANT_TYPE_TEST:
//                return TestDatagrams.getBpmPersonAssistantType();
//            case NsiTestsList.BPM_GROUP_TYPE_TEST:
//                return TestDatagrams.getBpmGroupType();
//            case NsiTestsList.BPM_GROUP_MEMBER_TYPE_TEST:
//                return TestDatagrams.getBpmGroupMemberType();

        }
        return null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NsiTestsList.TEST_DS.equals(dataSource.getName()))
        {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, _tests);
        }
    }
}
