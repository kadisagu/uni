package ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.PackageSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSettingsPackageSettings extends BusinessComponentManager
{
    public static final String TIME_INTERVALS_COUNT_DS = "timeIntervalsCountDS";
    public static final String TIME_INTERVALS_DS = "timeIntervalsDS";

    public static final int INTERVALS_COUNT = 15;

    @Bean
    public ColumnListExtPoint timeIntervalsDS()
    {
        return columnListExtPointBuilder(TIME_INTERVALS_DS)
                .addColumn(textColumn("number", DataWrapper.TITLE).width("60px").required(true))
                .addColumn(blockColumn("timeInterval", "timeIntervalBlockColumn").required(true).create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(TIME_INTERVALS_COUNT_DS, timeIntervalsCountComboDSHandler()))
                .addDataSource(searchListDS(TIME_INTERVALS_DS, timeIntervalsDS()))
                .create();
    }

    @Bean
    public ItemListExtPoint<DataWrapper> timeIntervalsCountExtPoint()
    {
        IItemListExtPointBuilder<DataWrapper> builder = itemList(DataWrapper.class);
        for(int i = 1; i < INTERVALS_COUNT + 1; i++)
        {
            String title = Integer.toString(i);
            builder.add(title, new DataWrapper((long) i, title));
        }
        return builder.create();
    }

    @Bean
    public IDefaultComboDataSourceHandler timeIntervalsCountComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(timeIntervalsCountExtPoint());
    }
}
