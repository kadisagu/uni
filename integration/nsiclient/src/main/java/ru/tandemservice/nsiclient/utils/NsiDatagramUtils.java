/* $Id:$ */
package ru.tandemservice.nsiclient.utils;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author Dmitry Seleznev
 * @since 03.02.2017
 */
public class NsiDatagramUtils
{
    private static final String NSI_DO_NOT_INCLUDE_ANY_FILES_PROP_NAME = "nsi.reactor.doNotIncludeAnyFiles";

    public static final boolean isFilesShouldNotBeIncludedInDatagram()
    {
        if (ApplicationRuntime.existProperty(NSI_DO_NOT_INCLUDE_ANY_FILES_PROP_NAME))
            return Boolean.parseBoolean(ApplicationRuntime.getProperty(NSI_DO_NOT_INCLUDE_ANY_FILES_PROP_NAME));

        return false;
    }
}
