package ru.tandemservice.nsiclient.base.bo.NsiQueue.logic;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.NsiDeliveryDaemon;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiQueue;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 19.01.2017
 */
public class NsiQueueDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String CREATE_DATE_FROM = "createDateFrom";
    public static final String CREATE_DATE_TO = "createDateTo";
    public static final String CATALOG_ID = "catalogId";
    public static final String MESSAGE_GUID = "messageGuid";
    public static final String DIRECTION_TYPE = "directionType";
    public static final String OPERATION_TYPE = "operationType";

    public static final Set<String> FILTERS_SET = new HashSet<>();

    static
    {
        FILTERS_SET.add(CREATE_DATE_FROM);
        FILTERS_SET.add(CREATE_DATE_TO);
        FILTERS_SET.add(CATALOG_ID);
        FILTERS_SET.add(MESSAGE_GUID);
        FILTERS_SET.add(DIRECTION_TYPE);
        FILTERS_SET.add(OPERATION_TYPE);
    }

    public NsiQueueDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        context.getSession().clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiQueue.class, "e")
                .fetchPath(DQLJoinType.inner, NsiQueue.log().fromAlias("e"), true)
                .column(property("e"));

        Date createDateFrom = context.get(CREATE_DATE_FROM);
        if (null != createDateFrom)
            builder.where(ge(property("e", NsiQueue.log().startDate()), value(createDateFrom, PropertyType.DATE)));

        Date createDateTo = context.get(CREATE_DATE_TO);
        if (null != createDateTo)
            builder.where(le(property("e", NsiQueue.log().startDate()), value(createDateTo, PropertyType.DATE)));

        Long directionType = context.get(DIRECTION_TYPE);
        if (null != directionType)
            builder.where(eq(property("e", NsiQueue.log().pack().incoming()), commonValue(NsiSyncManager.DIRECTION_IN.equals(directionType), PropertyType.BOOLEAN)));

        Long catalogType = context.get(CATALOG_ID);
        if (null != catalogType)
        {
            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "el")
                    .joinEntity("el", DQLJoinType.inner, NsiCatalogType.class, "nct", eq(property("el", NsiEntityLog.catalogType()), property("nct", NsiCatalogType.code())))
                    .column(property("el", NsiEntityLog.pack().id()))
                    .where(eq(property("nct", NsiCatalogType.id()), commonValue(catalogType, PropertyType.LONG)));
            builder.where(in(property("e", NsiQueue.log().pack().id()), b.buildQuery()));
        }

        Long operationType = context.get(OPERATION_TYPE);
        if (null != operationType)
            builder.where(eq(property("e", NsiQueue.log().pack().type()), commonValue(NsiSyncManager.Operations.values()[operationType.intValue()].name(), PropertyType.STRING)));

        String messageGuid = context.get(MESSAGE_GUID);
        if (null != messageGuid)
            builder.where(eq(property("e", NsiQueue.log().guid()), commonValue(CoreStringUtils.escapeLike(messageGuid, true).toLowerCase(), PropertyType.STRING)));

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(NsiQueue.class, "e")
                .setOrders(NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().guid(), new OrderDescription(NsiQueue.log().guid()))
                .setOrders(NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().creationDate(), new OrderDescription(NsiQueue.log().pack().creationDate()))
                .setOrders(NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().incoming(), new OrderDescription(NsiQueue.log().pack().incoming()))
                .setOrders(NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().type(), new OrderDescription(NsiQueue.log().pack().type()))
                .setOrders(NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().source(), new OrderDescription(NsiQueue.log().pack().source()))
                .setOrders(NsiQueueWrapper.QUEUE + '.' + NsiQueue.log().pack().endDate(), new OrderDescription(NsiQueue.log().pack().endDate()));

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order(orderRegistry).build();

        Set<Long> packIdSet = new HashSet<>(output.getCountRecord());
        for (NsiQueue queue : output.<NsiQueue>getRecordList()) packIdSet.add(queue.getLog().getPack().getId());

        List<Object[]> catalogTypeItems = new DQLSelectBuilder().fromEntity(NsiQueue.class, "q").fromEntity(NsiEntityLog.class, "l")
                .joinEntity("l", DQLJoinType.inner, NsiCatalogType.class, "nct", eq(property("l", NsiEntityLog.catalogType()), property("nct", NsiCatalogType.code())))
                .column(property("q", NsiQueue.id())).column(property("nct", NsiCatalogType.title()))
                .where(eq(property("q", NsiQueue.log().pack().id()), property("l", NsiEntityLog.pack().id())))
                .where(in(property("q", NsiQueue.log().pack().id()), packIdSet))
                .order(property("q", NsiQueue.id())).order(property("nct", NsiCatalogType.title())).createStatement(context.getSession()).list();

        Map<Long, Set<String>> packIdToCatalogTitleSetMap = new HashMap<>();
        for (Object[] item : catalogTypeItems)
        {
            Set<String> catalogTypeSet = packIdToCatalogTitleSetMap.get(item[0]);
            if (null == catalogTypeSet) catalogTypeSet = new TreeSet<>();
            catalogTypeSet.add((String) item[1]);
            packIdToCatalogTitleSetMap.put((Long) item[0], catalogTypeSet);
        }

        return output.transform(new Function<NsiQueue, NsiQueueWrapper>()
        {
            @Override
            public NsiQueueWrapper apply(NsiQueue input)
            {
                Set<String> catalogs = packIdToCatalogTitleSetMap.get(input.getId());
                if (null == catalogs) catalogs = new HashSet<>();
                NsiQueueWrapper wrapper = new NsiQueueWrapper(input, Joiner.on(", ").join(catalogs), input.getLog().getDestination());
                if (NsiDeliveryDaemon.isCurrentMessageGuid(input.getLog().getGuid()))
                {
                    wrapper.setCurrent(NsiDeliveryDaemon.getCurrentMessageStartDate(input.getLog().getGuid()));
                }
                return wrapper;
            }
        });
    }
}
