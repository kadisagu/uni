/* $Id: SystemActionExtManager.java 42746 2015-05-21 10:43:15Z aavetisov $ */
package ru.tandemservice.nsiclient.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.nsiclient.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Andrey Avetisov
 * @since 15.05.2015
 */

@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        boolean isDeveloperMode = Boolean.parseBoolean(ApplicationRuntime.getProperty("bpm.developer.mode"));
        final IItemListExtensionBuilder<SystemActionDefinition> itemListExtensionBuilder = itemListExtension(_systemActionManager.buttonListExtPoint());
        if (isDeveloperMode)
        {
            itemListExtensionBuilder.add("nsiTest", new SystemActionDefinition("nsiclient", "nsiTest", "onClickNsiTest", SystemActionPubExt.NSI_SYSTEM_ACTION_PUB_ADDON_NAME));
        }
        return itemListExtensionBuilder
                .create();
    }
}
