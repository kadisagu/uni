package ru.tandemservice.nsiclient.base.bo.NsiSettings.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.entity.NsiSettings;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Transactional
public class NsiSettingsDao extends BaseModifyAggregateDAO<NsiSettings> implements INsiSettingsDao
{
    @Override
    public NsiSettings getNotNullSettings()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiSettings.class, "e")
                .top(1);
        NsiSettings settings = createStatement(builder).uniqueResult();
        if(settings == null)
        {
            settings = new NsiSettings();
            baseCreateOrUpdate(settings);
        }
        return settings;
    }
}
