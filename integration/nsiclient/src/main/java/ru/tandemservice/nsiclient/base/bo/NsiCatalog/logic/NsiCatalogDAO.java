/* $Id: NsiCatalogDAO.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic;

import com.google.common.collect.Sets;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.migration.MS_nsiclient_2x9x1_2to3;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Nikonov
 * @since 10.04.2015
 */
@Transactional
public class NsiCatalogDAO extends BaseModifyAggregateDAO implements INsiCatalogDAO
{
    @Override
    public void checkCatalogs() {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = ReactorHolder.getReactorMap();
        Set<String> usedReactors = Sets.newHashSet();
        List<NsiCatalogType> catalogs = DataAccessServices.dao().getList(NsiCatalogType.class);
        for(NsiCatalogType catalog: catalogs)
        {
            INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = reactors.get(catalog.getCode());
            if(reactor != null)
            {
                usedReactors.add(catalog.getCode());
                if(catalog.getDisabledDate() != null)
                {
                    catalog.setDisabledDate(null);
                    baseUpdate(catalog);
                }
                reactor.setCatalogType(catalog.getCode());
            }
            else
            {
                catalog.setDisabledDate(new Date());
                baseUpdate(catalog);
            }
        }

        for(String s: reactors.keySet())
        {
            if (!usedReactors.contains(s))
            {
                INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = reactors.get(s);
                Class<? extends IEntity> c = reactor.getEntityClass();
                IEntityMeta meta = EntityRuntime.getMeta(c);
                if(meta != null)
                {
                    NsiCatalogType catalog = DataAccessServices.dao().get(NsiCatalogType.class, NsiCatalogType.code(), s);
                    if(catalog == null)
                        catalog = new NsiCatalogType();
                    catalog.setNsiCode(meta.getClassName());
                    catalog.setUserCode(s);
                    catalog.setTitle(meta.getTitle());
                    catalog.setCode(s);
                    baseCreate(catalog);
                    reactor.setCatalogType(catalog.getCode());
                }
            }
        }
    }

    private NsiCatalogType findCatalogEntityType(String entityType) {
        IEntityMeta meta = EntityRuntime.getMeta(entityType);
        if(meta != null) {
            List<NsiCatalogType> catalogs = DataAccessServices.dao().getList(NsiCatalogType.class);
            for(NsiCatalogType catalog: catalogs) {
                if(!catalog.getCode().equals("EmployeeDepartmentHeadType")) {
                    IEntityMeta catalogMeta = EntityRuntime.getMeta(catalog.getNsiCode());
                    if (catalogMeta != null) {
                        IEntityMeta meta2 = meta;
                        while (meta2 != null) {
                            if (meta2.equals(catalogMeta))
                                return catalog;
                            meta2 = meta2.getParent();
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void migration20151005() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "e")
                .column(DQLFunctions.countStar())
                .where(eq(property("e", NsiEntity.type()), value(MS_nsiclient_2x9x1_2to3.UNDEFINED)));
        int count = executeIntResultStatement(builder);
        if(count != 0) {
            builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "e")
                    .column(property("e", NsiEntity.entityType()))
                    .group(property("e", NsiEntity.entityType()));

            List<String> entityTypes = createStatement(builder).list();
            for(String entityType: entityTypes) {
                NsiCatalogType catalogType = findCatalogEntityType(entityType);
                if(catalogType != null) {
                    String newEntityType = EntityRuntime.getMeta(catalogType.getNsiCode()).getName();
                    DQLUpdateBuilder update = new DQLUpdateBuilder(NsiEntity.class)
                            .setValue(NsiEntity.P_TYPE, catalogType.getCode())
                            .setValue(NsiEntity.P_ENTITY_TYPE, newEntityType)
                            .where(eq(property(NsiEntity.entityType()), value(entityType)));
                    createStatement(update).execute();
                }
            }
        }

    }
}