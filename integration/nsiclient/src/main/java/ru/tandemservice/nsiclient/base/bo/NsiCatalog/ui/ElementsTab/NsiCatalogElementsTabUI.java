package ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementsTab;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.config.IConfigItem;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.datasource.column.IColumnBuilderBase;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.meta.entity.impl.ManyToOneMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic.ElementWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.logic.NsiCatalogElementsListDSHandler;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.support.FilterItem;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.ui.ElementView.NsiCatalogElementView;
import ru.tandemservice.nsiclient.entity.NsiEntity;

import java.util.*;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = NsiCatalogElementsTab.ENTITY_CODE, binding = "code", required = true)
})
public class NsiCatalogElementsTabUI extends UIPresenter
{
    private String _code;
    private List<FilterItem> _filterItems = new ArrayList<>();
    private Map<String, Object> _filters;
    private FilterItem _filter;
    private boolean _filterApplied;
    private List<CoreCollectionUtils.Pair<String, OrderDescription[]>> _orders = Lists.newArrayList();
    private Map<String, CoreCollectionUtils.Pair<SelectDataSource, IEntityMeta>> _name2DataSourceEntityClassMap = new HashMap<>();

    private static final String SETTINGS_FILTERS = "filters";
    private static final String SETTINGS_FILTER_VISIBLE = "filtersVisible";

    public String getCode()
    {
        return _code;
    }

    public void setCode(String code)
    {
        _code = code;
    }

    public boolean isTextFilter()
    {
        return _filter.getType() == FilterItem.TYPE.TEXT;
    }

    public boolean isEntityFilter()
    {
        return _filter.getType() == FilterItem.TYPE.ENTITY;
    }

    public boolean isBooleanFilter()
    {
        return _filter.getType() == FilterItem.TYPE.BOOLEAN;
    }

    public boolean isDateFilter()
    {
        return _filter.getType() == FilterItem.TYPE.DATE;
    }

    public boolean isIntegerFilter()
    {
        return _filter.getType() == FilterItem.TYPE.INTEGER;
    }

    public boolean isDoubleFilter()
    {
        return _filter.getType() == FilterItem.TYPE.DOUBLE;
    }

    private static IFieldPropertyMeta findGuidField()
    {
        IEntityMeta nsiMeta = EntityRuntime.getMeta(NsiEntity.class);
        Collection<IFieldPropertyMeta> fields = nsiMeta.getDeclaredFields();
        for(IFieldPropertyMeta field: fields)
            if(field.getName().equals(NsiEntity.P_GUID))
                return field;
        return null;
    }

    @Override
    public void onComponentActivate()
    {
        IEntityMeta meta = EntityRuntime.getMeta(_code);
        Collection<IFieldPropertyMeta> declaredFields = meta.getDeclaredFields();
        List<IFieldPropertyMeta> fields = Lists.newArrayList(declaredFields);
        IFieldPropertyMeta guidField = findGuidField();
        if(guidField != null)
            fields.add(guidField);
        Collections.sort(fields, new Comparator<IFieldPropertyMeta>() {
            @Override
            public int compare(IFieldPropertyMeta o1, IFieldPropertyMeta o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });

        _filters = getSettings().get(SETTINGS_FILTERS);
        if(_filters == null)
            _filters = new HashMap<>();

        BaseSearchListDataSource searchDS = getConfig().getDataSource(NsiCatalogElementsTab.ELEMENT_LIST_DS);
        for(IFieldPropertyMeta field : fields)
        {
            String name = field.getName();
            if (!NsiCatalogManager.isServiceField(field.getName()))
            {
                FilterItem.TYPE type = getType(field);

                boolean isRequiredField = false;
                String title = field.getTitle();
                if(!isHided(meta.getName(), field.getName())) {
                    searchDS.addColumn(createColumn(field, type, isRequiredField, title));
                    searchDS.getColumn(name).setLabel(isRequiredField ? title + '*' : title);

                    CoreCollectionUtils.Pair<SelectDataSource, IEntityMeta> pair = createDataSource(field, type);
                    SelectDataSource ds = null;
                    IEntityMeta c = null;
                    if(pair != null)
                    {
                        ds = pair.getX();
                        c = pair.getY();
                    }

                    if(c == null || isFilterable(c.getName()))
                        _filterItems.add(new FilterItem(name, field.getTitle(), type, getValueFromSettings(name, type, c), ds));
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private CoreCollectionUtils.Pair<SelectDataSource, IEntityMeta> createDataSource(IFieldPropertyMeta field, FilterItem.TYPE type)
    {
        if(type != FilterItem.TYPE.ENTITY || !(field instanceof ManyToOneMeta))
            return null;
        IEntityMeta remoteMeta = ((ManyToOneMeta) field).getRemoteEntity();
        CoreCollectionUtils.Pair<SelectDataSource, IEntityMeta> pair = _name2DataSourceEntityClassMap.get(remoteMeta.getName());
        if(pair != null)
            return pair;
        SelectDataSource ds = new SelectDataSource(this, remoteMeta.getName());
        ds.setHandler(getHandler(remoteMeta.getName()));
        getConfig().addDataSource(ds);
        pair = new CoreCollectionUtils.Pair<>(ds, remoteMeta);
        _name2DataSourceEntityClassMap.put(remoteMeta.getName(), pair);

        return pair;
    }

    @SuppressWarnings("rawtypes")
    private IColumnBuilderBase<? extends IColumnBuilderBase, ? extends IConfigItem> createColumn(IFieldPropertyMeta field, FilterItem.TYPE type, boolean isRequiredField, String title)
    {
        String name = field.getName();

        if(NsiEntity.P_GUID.equals(name)) {
            _orders.add(new CoreCollectionUtils.Pair<>(ElementWrapper.NSI_ENTITY + '.' +  field.getName(), new OrderDescription[] {new OrderDescription(NsiCatalogElementsListDSHandler.NSI_ENTITY_ALIAS, field.getName())}));
            return NsiCatalogElementsTab.actionColumn(name, ElementWrapper.NSI_ENTITY + '.' + NsiEntity.P_GUID, "onViewElement").required(isRequiredField).defaultVisible(isRequiredField)
                    .selectCaption(title)
                    .order();
        }

        if(type == FilterItem.TYPE.BOOLEAN) {
            _orders.add(new CoreCollectionUtils.Pair<>(ElementWrapper.ENTITY + '.' +  field.getName(), new OrderDescription[] {new OrderDescription(NsiCatalogElementsListDSHandler.ENTITY_ALIAS, field.getName())}));
            return NsiCatalogElementsTab.booleanColumn(name, ElementWrapper.ENTITY + '.' + name).required(isRequiredField).defaultVisible(isRequiredField)
                    .selectCaption(title)
                    .order();
        }
        if(type == FilterItem.TYPE.ENTITY)
        {
            if(field instanceof ManyToOneMeta)
            {
                IEntityMeta remoteMeta = ((ManyToOneMeta) field).getRemoteEntity();
                String showedTitle = getTitle(remoteMeta);
                _orders.add(new CoreCollectionUtils.Pair<>(ElementWrapper.ENTITY + '.' +  field.getName() + '.' + showedTitle, getOrderDescriptions(remoteMeta.getName(), field.getName())));
                ITextColumnBuilder result = NsiCatalogElementsTab.textColumn(name, ElementWrapper.ENTITY + '.' + name + '.' + showedTitle).required(isRequiredField).defaultVisible(isRequiredField)
                        .selectCaption(title);
                if(isOrderable(remoteMeta.getName()))
                    result.order();
                return result;
            }
        }
        if(type == FilterItem.TYPE.DATE) {
            _orders.add(new CoreCollectionUtils.Pair<>(ElementWrapper.ENTITY + '.' +  field.getName(), new OrderDescription[] {new OrderDescription(NsiCatalogElementsListDSHandler.ENTITY_ALIAS, field.getName())}));
            return NsiCatalogElementsTab.dateColumn(name, ElementWrapper.ENTITY + '.' + name).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).required(isRequiredField).defaultVisible(isRequiredField)
                    .selectCaption(title)
                    .order();
        }
        _orders.add(new CoreCollectionUtils.Pair<>(ElementWrapper.ENTITY + '.' +  field.getName(), new OrderDescription[] {new OrderDescription(NsiCatalogElementsListDSHandler.ENTITY_ALIAS, field.getName())}));
        return NsiCatalogElementsTab.textColumn(name, ElementWrapper.ENTITY + '.' + name).required(isRequiredField).defaultVisible(isRequiredField)
                    .selectCaption(title)
                    .order();
    }

    private IBusinessHandler<? extends DSInput, ? extends DSOutput> getHandler(String name)
    {
        IDefaultComboDataSourceHandler handler = NsiCatalogManager.instance().handlerExtPoint().getItem(name);
        if (handler != null)
            return handler;
        return NsiCatalogManager.instance().comboDSHandler();
    }

    private OrderDescription[] getOrderDescriptions(String entityName, String fieldName)
    {
        OrderDescription[] orderDescriptions = NsiCatalogManager.instance().ordersExtPoint().getItem(entityName);
        if (orderDescriptions != null)
            return orderDescriptions;
        return new OrderDescription[] {new OrderDescription(NsiCatalogElementsListDSHandler.ENTITY_ALIAS, fieldName + ".title")};
    }

    private String getTitle(IEntityMeta remoteMeta)
    {
        String title = NsiCatalogManager.instance().titleExtPoint().getItem(remoteMeta.getName());
        if (title != null)
            return title;
        if(remoteMeta.getProperty("title") == null)
            return "id";
        return "title";
    }

    private boolean isOrderable(String entityName)
    {
        Boolean orderable = NsiCatalogManager.instance().orderableExtPoint().getItem(entityName);
        if (orderable != null)
            return orderable.booleanValue();
        return true;
    }

    private boolean isFilterable(String entityName)
    {
        Boolean filterable = NsiCatalogManager.instance().filterableExtPoint().getItem(entityName);
        if (filterable != null)
            return filterable.booleanValue();
        return true;
    }

    private boolean isHided(String entityName, String fieldName)
    {
        String[] hideList = NsiCatalogManager.instance().hidedExtPoint().getItem(entityName);
        if (hideList == null)
            return false;
        for(String s : hideList)
        if(s.equals(fieldName))
            return true;
        return false;
    }

    private Object getValueFromSettings(String name, FilterItem.TYPE type, IEntityMeta c)
    {
        Object result = null;
        Object value = _filters.get(name);
        if(type == FilterItem.TYPE.BOOLEAN)
        {
            if (value instanceof Boolean) {
                result = (Boolean) value ? TwinComboDataSourceHandler.getYesOption() : TwinComboDataSourceHandler.getNoOption();
            }
        }
        else if(type == FilterItem.TYPE.ENTITY)
        {
            if (value != null && value instanceof Long && c != null) {
                IEntity entity = DataAccessServices.dao().get((Long) value);
                if(entity != null && c.getEntityClass().isInstance(entity))
                    result = entity;
            }
        }
        else if(type == FilterItem.TYPE.TEXT)
        {
            if (value instanceof String) {
                result = value;
            }
        }
        else if(type == FilterItem.TYPE.INTEGER)
        {
            if (value instanceof Integer) {
                result = value;
            }
        }
        else if(type == FilterItem.TYPE.DOUBLE)
        {
            if (value instanceof Number) {
                result = value;
            }
        }
        else if(type == FilterItem.TYPE.DATE)
        {
            if(value == null)
                result = new CoreCollectionUtils.Pair<Date, Date>();
            else if (value instanceof CoreCollectionUtils.Pair)
            {
                @SuppressWarnings("unchecked")
                CoreCollectionUtils.Pair<Date, Date> dates = (CoreCollectionUtils.Pair<Date, Date>) value;
                dates.setX(null);
                dates.setY(null);
                result = dates;
            }
        }
        return result;
    }

    private Object setValueToSettings(Object value, FilterItem.TYPE type)
    {
        if (type == FilterItem.TYPE.BOOLEAN)
        {
            if (value instanceof DataWrapper)
                return TwinComboDataSourceHandler.getSelectedValue(value);
        }
        else if (type == FilterItem.TYPE.DATE)
        {
            if(value instanceof CoreCollectionUtils.Pair)
            {
                @SuppressWarnings("unchecked")
                CoreCollectionUtils.Pair<Date, Date> pair = (CoreCollectionUtils.Pair<Date, Date>) value;
                if(pair.getX() != null || pair.getY() != null)
                    return value;
            }
        }
        else if (type == FilterItem.TYPE.ENTITY)
        {
            if(value instanceof IEntity)
                return ((IEntity) value).getId();
        }
        else if (type == FilterItem.TYPE.TEXT || type == FilterItem.TYPE.DOUBLE || type == FilterItem.TYPE.INTEGER)
            return value;
        return null;
    }

    private static FilterItem.TYPE getType(IFieldPropertyMeta field)
    {
        if(field instanceof ManyToOneMeta)
        {
            return FilterItem.TYPE.ENTITY;
        }
        PropertyType type = field.getPropertyType();
        if(type == PropertyType.DATE || type == PropertyType.TIME || type == PropertyType.TIMESTAMP)
            return FilterItem.TYPE.DATE;
        if(type == PropertyType.INTEGER)
            return FilterItem.TYPE.INTEGER;
        if(type == PropertyType.DOUBLE)
            return FilterItem.TYPE.DOUBLE;
        if(type == PropertyType.BOOLEAN)
            return FilterItem.TYPE.BOOLEAN;
        if(type == PropertyType.ANY)
            return FilterItem.TYPE.ENTITY;
        return FilterItem.TYPE.TEXT;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NsiCatalogElementsTab.ELEMENT_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(NsiCatalogElementsListDSHandler.ENTITY_NAME, _code);
            dataSource.put(NsiCatalogElementsListDSHandler.FILTERS, _filterItems);
            dataSource.put(NsiCatalogElementsListDSHandler.ORDER_DESCRIPTIONS, _orders);
        }
        else
        {
            CoreCollectionUtils.Pair<SelectDataSource, IEntityMeta> pair = _name2DataSourceEntityClassMap.get(dataSource.getName());
            if(pair != null && pair.getY() != null)
                dataSource.put(AbstractReadAggregateHandler.ENTITY_CLASS, pair.getY().getEntityClass());
        }
    }

    public void onViewElement()
    {
        _uiActivation.asDesktopRoot(NsiCatalogElementView.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickShowHideFilters()
    {
        Boolean filtersVisible = getSettings().get(SETTINGS_FILTER_VISIBLE, Boolean.class);
        if(filtersVisible == null)
            filtersVisible = false;
        getSettings().set(SETTINGS_FILTER_VISIBLE, !filtersVisible);
        getSettings().save();
    }

    public void onClickSearch()
    {
        _filters.clear();
        for(FilterItem filterItem: _filterItems)
        {
            Object value = filterItem.getValue();
            if(value != null)
                _filters.put(filterItem.getFieldName(), setValueToSettings(value, filterItem.getType()));
        }
        getSettings().set(SETTINGS_FILTERS, _filters);
        getSettings().save();

        _filterApplied = !_filters.isEmpty();
    }

    public void onClickClear()
    {
        for(FilterItem filterItem: _filterItems)
            if(filterItem.getType() == FilterItem.TYPE.DATE)
                filterItem.setValue(new CoreCollectionUtils.Pair<>());
            else
                filterItem.setValue(null);
        _filters.clear();
        _filterApplied = false;
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), SETTINGS_FILTER_VISIBLE);
    }

    @Override
    public String getSettingsKey()
    {
        return _code + '.' + super.getSettingsKey();
    }
}
