package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSubSystemSend extends BusinessComponentManager
{
    public static final String SUB_SYSTEM_DS = "subSystemDS";

    public static final String SELECT_ACTION = "action";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SUB_SYSTEM_DS, nsiSubSystemListDSHandler())
                        .addColumn(NsiSubSystemLogWrapper.SUB_SYSTEM + '.' + NsiSubSystem.P_USER_CODE)
                        .addColumn(NsiSubSystemLogWrapper.SUB_SYSTEM + '.' + NsiSubSystem.P_TITLE))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler nsiSubSystemListDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName());
    }
}