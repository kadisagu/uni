/* $Id: NsiSubSystemLogListDSHandler.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 02.03.2015
 */
public class NsiSubSystemLogListDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PACKAGE_ID = "packageId";;

    public NsiSubSystemLogListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiSubSystemLog.class, "e")
                .column("e")
                .where(eq(property("e", NsiSubSystemLog.pack().id()), commonValue(context.getNotNull(PACKAGE_ID), PropertyType.LONG)));
        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).order().build();
    }
}