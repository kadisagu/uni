package ru.tandemservice.nsiclient.reactor.support;

public class SynchronizeReport
{
    private String _warning;
    private int _insertCount;
    private int _updateCount;
    private int _deleteCount;
    private int _outgoingInsertCount;
    private int _outgoingUpdateCount;
    private int _outgoingDeleteCount;

    public SynchronizeReport(String warning, int insertCount, int updateCount, int deleteCount, int outgoingInsertCount, int outgoingUpdateCount, int outgoingDeleteCount)
    {
        _warning = warning;
        _insertCount = insertCount;
        _updateCount = updateCount;
        _deleteCount = deleteCount;
        _outgoingInsertCount = outgoingInsertCount;
        _outgoingUpdateCount = outgoingUpdateCount;
        _outgoingDeleteCount = outgoingDeleteCount;
    }

    public int getInsertCount() {
        return _insertCount;
    }

    public int getUpdateCount() {
        return _updateCount;
    }

    public int getDeleteCount() {
        return _deleteCount;
    }

    public int getOutgoingInsertCount() {
        return _outgoingInsertCount;
    }

    public int getOutgoingUpdateCount() {
        return _outgoingUpdateCount;
    }

    public int getOutgoingDeleteCount() {
        return _outgoingDeleteCount;
    }

    public String getWarning() {
        return _warning;
    }
}
