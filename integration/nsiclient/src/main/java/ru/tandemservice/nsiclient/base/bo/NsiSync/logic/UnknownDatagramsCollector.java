/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import java.util.*;

/**
 * @author Andrey Nikonov
 * @since 14.12.2015
 */
public class UnknownDatagramsCollector implements IUnknownDatagramsCollector {
    private Set<String> _guids = new HashSet<>();
    private List<IDatagramObject> _datagrams = new ArrayList<>();
    private Map<String, CoreCollectionUtils.Pair<IDatagramObject, ChangedWrapper<? extends IEntity>>> _datagramMap;

    public UnknownDatagramsCollector(Map<String, CoreCollectionUtils.Pair<IDatagramObject, ChangedWrapper<? extends IEntity>>> map, List<IDatagramObject> requiredElements) {
        _datagramMap = map;
        if(requiredElements != null)
            for(IDatagramObject datagramObject: requiredElements) {
                _datagrams.add(datagramObject);
                _guids.add(datagramObject.getID());
            }
    }

    public List<IDatagramObject> getDatagrams() {
        return _datagrams;
    }

    @Override
    public void check(String guid, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor) {
        if(isNeedRetrieve(guid)) {
            _guids.add(guid);
            _datagrams.add(reactor.createDatagramObject(guid));
        }
    }

    @Override
    public <K extends IDatagramObject> void check(String guid, Class<K> clazz) {
        if(isNeedRetrieve(guid)) {
            _guids.add(guid);
            @SuppressWarnings("unchecked")
            INsiEntityReactor<? extends IEntity, K> reactor = (INsiEntityReactor<? extends IEntity, K>) ReactorHolder.getReactorMap().get(clazz.getSimpleName());
            _datagrams.add(reactor.createDatagramObject(guid));
        }
    }

    private boolean isNeedRetrieve(String guid) {
        // значит, в процессе обработки
        if(_datagramMap.containsKey(guid))
            return false;
        if(_guids.contains(guid))
            return false;
        // todo вынести, чтобы не выполнять кучу этих мелких запросов
        NsiEntity nsiEntity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.guid(), guid);
        // есть ли он в базе? если есть, то запрашивать не надо
        return nsiEntity == null;
    }
}
