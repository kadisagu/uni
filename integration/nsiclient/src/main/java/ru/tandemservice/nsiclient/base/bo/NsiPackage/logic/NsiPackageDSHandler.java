package ru.tandemservice.nsiclient.base.bo.NsiPackage.logic;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.nsiclient.base.bo.NsiPackage.support.NsiPackageWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiEntityLog;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
public class NsiPackageDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String CREATE_DATE_FROM = "createDateFrom";
    public static final String CREATE_DATE_TO = "createDateTo";
    public static final String ANY_XML_BODY_GUID = "anyXmlBodyGuid";
    public static final String ENTITY_GUID = "entityGuid";
    public static final String ENTITY_ID = "entityId";
    public static final String CATALOG_ID = "catalogId";
    public static final String MESSAGE_GUID = "messageGuid";
    public static final String DIRECTION_TYPE = "directionType";
    public static final String OPERATION_TYPE = "operationType";
    public static final String MESSAGE_STATUS = "messageStatus";
    public static final String ERROR = "error";
    public static final String WARNING = "warning";

    public static final Set<String> FILTERS_SET = new HashSet<>();

    static
    {
        FILTERS_SET.add(CREATE_DATE_FROM);
        FILTERS_SET.add(CREATE_DATE_TO);
        FILTERS_SET.add(ANY_XML_BODY_GUID);
        FILTERS_SET.add(ENTITY_GUID);
        FILTERS_SET.add(ENTITY_ID);
        FILTERS_SET.add(CATALOG_ID);
        FILTERS_SET.add(MESSAGE_GUID);
        FILTERS_SET.add(DIRECTION_TYPE);
        FILTERS_SET.add(OPERATION_TYPE);
        FILTERS_SET.add(MESSAGE_STATUS);
        FILTERS_SET.add(ERROR);
        FILTERS_SET.add(WARNING);
    }

    public NsiPackageDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        context.getSession().clear();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiPackage.class, "e")
                .column(property("e"));

        Date createDateFrom = context.get(CREATE_DATE_FROM);
        if (null != createDateFrom)
            builder.where(ge(property("e", NsiPackage.creationDate()), value(createDateFrom, PropertyType.DATE)));

        Date createDateTo = context.get(CREATE_DATE_TO);
        if (null != createDateTo)
            builder.where(le(property("e", NsiPackage.creationDate()), value(createDateTo, PropertyType.DATE)));

        Long directionType = context.get(DIRECTION_TYPE);
        if (null != directionType)
            builder.where(eq(property("e", NsiPackage.incoming()), commonValue(NsiSyncManager.DIRECTION_IN.equals(directionType), PropertyType.BOOLEAN)));

        Long catalogType = context.get(CATALOG_ID);
        if (null != catalogType)
        {
            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "el")
                    .joinEntity("el", DQLJoinType.inner, NsiCatalogType.class, "nct", eq(property("el", NsiEntityLog.catalogType()), property("nct", NsiCatalogType.code())))
                    .column(property("el", NsiEntityLog.pack().id()))
                    .where(eq(property("nct", NsiCatalogType.id()), commonValue(catalogType, PropertyType.LONG)));
            builder.where(in(property("e", NsiPackage.id()), b.buildQuery()));
        }

        Long operationType = context.get(OPERATION_TYPE);
        if (null != operationType)
            builder.where(eq(property("e", NsiPackage.type()), commonValue(NsiSyncManager.Operations.values()[operationType.intValue()].name(), PropertyType.STRING)));

        Long status = context.get(MESSAGE_STATUS);
        if (null != status)
            builder.where(eq(property("e", NsiPackage.status()), commonValue(status, PropertyType.LONG)));

        String messageGuid = context.get(MESSAGE_GUID);
        if (null != messageGuid)
        {
            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(NsiSubSystemLog.class, "el")
                    .column(property("el", NsiSubSystemLog.pack().id()))
                    .where(like(property("el", NsiSubSystemLog.guid()), commonValue(CoreStringUtils.escapeLike(messageGuid, true).toLowerCase(), PropertyType.STRING)));
            builder.where(in(property("e", NsiPackage.id()), b.buildQuery()));
        }

        String entityGuid = context.get(ENTITY_GUID);
        if (null != entityGuid)
        {
            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "el")
                    .column(property("el", NsiEntityLog.pack().id()))
                    .where(like(property("el", NsiEntityLog.entity().guid()), commonValue(CoreStringUtils.escapeLike(entityGuid, true).toLowerCase(), PropertyType.STRING)));
            builder.where(in(property("e", NsiPackage.id()), b.buildQuery()));
        }

        Long entityId = context.get(ENTITY_ID);
        if (null != entityId)
        {
            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "el")
                    .column(property("el", NsiEntityLog.pack().id()))
                    .where(eq(property("el", NsiEntityLog.entity().id()), commonValue(entityId, PropertyType.LONG)));
            builder.where(in(property("e", NsiPackage.id()), b.buildQuery()));
        }

        String anyXmlBodyGuid = context.get(ANY_XML_BODY_GUID);
        if (null != anyXmlBodyGuid)
            builder.where(like(property("e", NsiPackage.body()), commonValue(CoreStringUtils.escapeLike(anyXmlBodyGuid, true).toLowerCase(), PropertyType.STRING)));

        Long error = context.get(ERROR);
        if (null != error)
            builder.where(eq(property("e", NsiPackage.error()), commonValue(TwinComboDataSourceHandler.YES_ID.equals(error), PropertyType.BOOLEAN)));

        Long warning = context.get(WARNING);
        if (null != warning)
            builder.where(eq(property("e", NsiPackage.warning()), commonValue(TwinComboDataSourceHandler.YES_ID.equals(warning), PropertyType.BOOLEAN)));

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(NsiPackage.class, "e")
                .setOrders(NsiPackageWrapper.PACK + '.' + NsiPackage.creationDate(), new OrderDescription(NsiPackage.creationDate()))
                .setOrders(NsiPackageWrapper.PACK + '.' + NsiPackage.incoming(), new OrderDescription(NsiPackage.incoming()))
                .setOrders(NsiPackageWrapper.PACK + '.' + NsiPackage.type(), new OrderDescription(NsiPackage.type()))
                .setOrders(NsiPackageWrapper.PACK + '.' + NsiPackage.source(), new OrderDescription(NsiPackage.source()))
                .setOrders(NsiPackageWrapper.PACK + '.' + "messageStatus", new OrderDescription(NsiPackage.status()))
                .setOrders(NsiPackageWrapper.PACK + '.' + NsiPackage.endDate(), new OrderDescription(NsiPackage.endDate()));

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order(orderRegistry).build();

        Set<Long> packIdSet = new HashSet<>(output.getCountRecord());
        for (NsiPackage pack : output.<NsiPackage>getRecordList()) packIdSet.add(pack.getId());

        List<Object[]> catalogTypeItems = new DQLSelectBuilder().fromEntity(NsiEntityLog.class, "e")
                .joinEntity("e", DQLJoinType.inner, NsiCatalogType.class, "nct", eq(property("e", NsiEntityLog.catalogType()), property("nct", NsiCatalogType.code())))
                .column(property("e", NsiEntityLog.pack().id())).column(property("nct", NsiCatalogType.title())).where(in(property("e", NsiEntityLog.pack().id()), packIdSet))
                .order(property("e", NsiEntityLog.pack().id())).order(property("nct", NsiCatalogType.title())).createStatement(context.getSession()).list();

        Map<Long, Set<String>> packIdToCatalogTitleSetMap = new HashMap<>();
        for (Object[] item : catalogTypeItems)
        {
            Set<String> catalogTypeSet = packIdToCatalogTitleSetMap.get(item[0]);
            if (null == catalogTypeSet) catalogTypeSet = new TreeSet<>();
            catalogTypeSet.add((String) item[1]);
            packIdToCatalogTitleSetMap.put((Long) item[0], catalogTypeSet);
        }

        List<Object[]> subSystemItems = new DQLSelectBuilder().fromEntity(NsiSubSystemLog.class, "e").distinct()
                .column(property("e", NsiSubSystemLog.pack().id())).column(property("e", NsiSubSystemLog.destination()))
                .where(in(property("e", NsiSubSystemLog.pack().id()), packIdSet)).order(property("e", NsiSubSystemLog.pack().id()))
                .order(property("e", NsiSubSystemLog.destination())).createStatement(context.getSession()).list();

        Map<Long, List<String>> packIdToSubsystemListMap = new HashMap<>();
        for (Object[] item : subSystemItems)
        {
            List<String> subsystemList = packIdToSubsystemListMap.get(item[0]);
            if (null == subsystemList) subsystemList = new ArrayList<>();
            subsystemList.add((String) item[1]);
            packIdToSubsystemListMap.put((Long) item[0], subsystemList);
        }

        return output.transform(new Function<NsiPackage, NsiPackageWrapper>()
        {
            @Override
            public NsiPackageWrapper apply(NsiPackage input)
            {
                Set<String> catalogs = packIdToCatalogTitleSetMap.get(input.getId());
                List<String> subsystems = packIdToSubsystemListMap.get(input.getId());
                if(null == catalogs) catalogs = new HashSet<>();
                if(null == subsystems) subsystems = new ArrayList<>();
                return new NsiPackageWrapper(input, Joiner.on(", ").join(catalogs), Joiner.on(", ").join(subsystems));
            }
        });
    }
}
