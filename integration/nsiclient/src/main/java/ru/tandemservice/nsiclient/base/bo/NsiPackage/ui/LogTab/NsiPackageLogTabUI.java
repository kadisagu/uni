package ru.tandemservice.nsiclient.base.bo.NsiPackage.ui.LogTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.logic.NsiSubSystemLogListDSHandler;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id")
})
public class NsiPackageLogTabUI extends UIPresenter
{
    private Long _id;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NsiPackageLogTab.LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put(NsiSubSystemLogListDSHandler.PACKAGE_ID, _id);
        }
    }

    public void onGetPack()
    {
        NsiSubSystemLog log = getConfig().getDataSource(NsiPackageLogTab.LOG_DS).getRecordById(getListenerParameterAsLong());
        String text = NsiUtils.createPackFile(log);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt").fileName(log.getGuid() + ".txt").document(text.getBytes()), true);
    }

    public void onSend()
    {
        // отключаем логирование в базу и прочие системные штуки
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

        try
        {
            NsiSubSystemLog log = getConfig().getDataSource(NsiPackageLogTab.LOG_DS).getRecordById(getListenerParameterAsLong());
            if (!log.getPack().isIncoming())
            {
                Long queueItemId = NsiSyncManager.instance().dao().createQueueItemForLogRow(log);
                NsiRequestHandlerService.instance().executeNSIAction(queueItemId, true);
            } else
            {
                // TODO повторная обработка входящих пакетов
            }
        } catch (Throwable t)
        {
            Debug.exception(t.getMessage(), t);
            throw t;
        } finally
        {
            // включаем штуки и логирование обратно
            Debug.resumeLogging();
            eventLock.release();
        }
    }

    public boolean isSendPackageDisabled()
    {
        NsiSubSystemLog log = getConfig().getDataSource(NsiPackageLogTab.LOG_DS).getCurrent();
        return NsiSyncManager.Operations.retrieve.name().equals(log.getPack().getType());
    }
}