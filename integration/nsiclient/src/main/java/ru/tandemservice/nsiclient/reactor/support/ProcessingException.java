/* $Id: ProcessingException.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.reactor.support;

/**
 * Ошибка обработки пакета.
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
public class ProcessingException extends Exception
{
    public ProcessingException(String message)
    {
        super(message);
    }

    public ProcessingException(Exception e)
    {
        super(e);
    }
}