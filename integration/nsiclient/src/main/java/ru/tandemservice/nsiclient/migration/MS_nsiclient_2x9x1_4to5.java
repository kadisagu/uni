package ru.tandemservice.nsiclient.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_nsiclient_2x9x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nsiEntity

		// создано обязательное свойство type
		{
			// создать колонку
			if (!tool.columnExists("nsientity_t", "type_p"))
			{
				tool.createColumn("nsientity_t", new DBColumn("type_p", DBType.createVarchar(255)));


				// задать значение по умолчанию
				java.lang.String defaultType = null;

				PreparedStatement selectNsiCatalogCodes = tool.prepareStatement("SELECT code_p, nsicode_p FROM nsicatalogtype_t ");
				selectNsiCatalogCodes.execute();
				ResultSet res = selectNsiCatalogCodes.getResultSet();
				while (res.next())
				{
					String catalogCode = res.getString(1);
					catalogCode = StringUtils.uncapitalize(catalogCode);

					String uniEntityFullName = res.getString(2);
					String[] splittedNames = uniEntityFullName.split("\\.");
					String uniEntityName = splittedNames[splittedNames.length - 1];
					uniEntityName = StringUtils.uncapitalize(uniEntityName);

					tool.executeUpdate("update nsientity_t set type_p=? where type_p is null and entitytype_p=?", catalogCode, uniEntityName);
				}

				tool.executeUpdate("update nsientity_t set type_p='undefined' where type_p is null");

				// сделать колонку NOT NULL
				tool.setColumnNullable("nsientity_t", "type_p", false);
			}
		}

		tool.dropConstraint("nsicatalogtype_t", "uk_nsicode_nsicatalogtype");
        tool.dropConstraint("nsicatalogtype_t", "uk_title_nsicatalogtype");
    }
}