package ru.tandemservice.nsiclient.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.nsiclient.entity.NsiPackFormingQueue;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Очередь на формирование пакетов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiPackFormingQueueGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.NsiPackFormingQueue";
    public static final String ENTITY_NAME = "nsiPackFormingQueue";
    public static final int VERSION_HASH = 1091933062;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_CREATE_DATE = "createDate";

    private long _entityId;     // Идентификатор сущности
    private Date _createDate;     // Дата и время постановки в очередь

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор сущности. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Дата и время постановки в очередь. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата и время постановки в очередь. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiPackFormingQueueGen)
        {
            setEntityId(((NsiPackFormingQueue)another).getEntityId());
            setCreateDate(((NsiPackFormingQueue)another).getCreateDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiPackFormingQueueGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiPackFormingQueue.class;
        }

        public T newInstance()
        {
            return (T) new NsiPackFormingQueue();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "createDate":
                    return obj.getCreateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "createDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "createDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "createDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiPackFormingQueue> _dslPath = new Path<NsiPackFormingQueue>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiPackFormingQueue");
    }
            

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackFormingQueue#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Дата и время постановки в очередь. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackFormingQueue#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    public static class Path<E extends NsiPackFormingQueue> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<Date> _createDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackFormingQueue#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(NsiPackFormingQueueGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Дата и время постановки в очередь. Свойство не может быть null.
     * @see ru.tandemservice.nsiclient.entity.NsiPackFormingQueue#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(NsiPackFormingQueueGen.P_CREATE_DATE, this);
            return _createDate;
        }

        public Class getEntityClass()
        {
            return NsiPackFormingQueue.class;
        }

        public String getEntityName()
        {
            return "nsiPackFormingQueue";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
