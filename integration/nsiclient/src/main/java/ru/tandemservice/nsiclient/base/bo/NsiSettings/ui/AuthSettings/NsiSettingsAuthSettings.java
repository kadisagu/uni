/* $Id: NsiSettingsAuthSettings.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.AuthSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Andrey Avetisov
 * @since 02.07.2015
 */
@Configuration
public class NsiSettingsAuthSettings extends BusinessComponentManager
{
    public static final String AUTH_TYPE_DS = "authTypeDS";
    public static final long SSL_ID = 0L;
    public static final long SIMPLE_ID = 1L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(AUTH_TYPE_DS, yesNoComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler yesNoComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(SSL_ID, "SSL")
                .addRecord(SIMPLE_ID, "SIMPLE AUTH");
    }
}
