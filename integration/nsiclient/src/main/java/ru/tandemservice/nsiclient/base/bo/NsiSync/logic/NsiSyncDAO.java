package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon.NsiDeliveryDaemon;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.*;
import ru.tandemservice.nsiclient.entity.catalog.NsiCatalogType;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import ru.tandemservice.nsiclient.entity.catalog.gen.NsiCatalogTypeGen;
import ru.tandemservice.nsiclient.entity.gen.NsiEntityGen;
import ru.tandemservice.nsiclient.reactor.support.ILogger;
import ru.tandemservice.nsiclient.reactor.support.Logger;
import ru.tandemservice.nsiclient.reactor.support.ProcessingException;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.Datagram;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.RoutingHeaderType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceRequestType;
import ru.tandemservice.nsiclient.ws.gen.cxf.server.ServiceResponseType;

import javax.xml.ws.WebServiceException;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Transactional
public class NsiSyncDAO extends BaseModifyAggregateDAO implements INsiSyncDAO
{
    @Override
    public NsiSubSystemLog createLog(ServiceRequestType request, boolean isIncoming, List<IDatagramObject> datagramElements)
    {
        RoutingHeaderType header = request.getRoutingHeader();
        NsiSubSystemLog subSystemLog = NsiSyncManager.instance().dao().createLog(header == null ? null : header.getSourceId(), header == null ? null : header.getOperationType(), header == null ? null : header.getDestinationId(), header, header == null ? null : header.getMessageId(),
                header == null ? null : header.getParentId(), request.getDatagram(), isIncoming);
        if (!isIncoming && datagramElements != null)
            NsiSyncManager.instance().dao().createEntityLogs(subSystemLog.getPack(), datagramElements);
        return subSystemLog;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public NsiSubSystemLog createLog(String sourceId, String operationType, String destinationId, RoutingHeaderType header, String messageId, String parentId, Datagram datagram, boolean isIncoming)
    {
        if(sourceId == null)
            throw new ApplicationException("Система-источник не указана в пакете. Возможно не указан код локальной подсистемы.");
        NsiPackage nsiPackage = new NsiPackage();
        nsiPackage.setCreationDate(new Date());
        nsiPackage.setIncoming(isIncoming);

        NsiSubSystemLog subSystemLog = new NsiSubSystemLog();
        nsiPackage.setSource(sourceId);
        nsiPackage.setType(operationType);
        nsiPackage.setDestination(destinationId);

        if (null != header)
            subSystemLog.setHeader(new String(NsiUtils.toXml(header), Charsets.UTF_8));

        subSystemLog.setDestination(destinationId);
        subSystemLog.setGuid(messageId);

        if (parentId != null)
        {
            NsiSubSystemLog parent = DataAccessServices.dao().get(NsiSubSystemLog.class, NsiSubSystemLog.guid(), parentId);
            subSystemLog.setPrevAttemptLog(parent);
        }
        if (null != datagram)
            nsiPackage.setBody(new String(NsiUtils.toXml(datagram), Charsets.UTF_8));

        baseCreateOrUpdate(nsiPackage);

        subSystemLog.setPack(nsiPackage);
        subSystemLog.setStartDate(nsiPackage.getCreationDate());
        subSystemLog.setStatus(isIncoming ? NsiSubSystemLog.STATUS_DELIVERED : NsiSubSystemLog.STATUS_DRAFT);
        baseCreateOrUpdate(subSystemLog);
        return subSystemLog;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createEntityLogs(NsiPackage nsiPackage, List<IDatagramObject> datagramElements)
    {
        Set<String> catalogTypeSet = datagramElements.stream().map(e->e.getClass().getSimpleName()).collect(Collectors.toSet());
        List<NsiCatalogType> catalogList = DataAccessServices.dao().getList(NsiCatalogType.class, NsiCatalogType.code(), catalogTypeSet);
        Map<String, NsiCatalogType> catalogTypeMap = catalogList.stream().collect(Collectors.toMap(NsiCatalogTypeGen::getCode, e->e));

        List<String> guidList = datagramElements.stream().map(IDatagramObject::getID).collect(Collectors.toList());
        List<NsiEntity> objectList = DataAccessServices.dao().getList(NsiEntity.class, NsiEntity.guid(), guidList);

        Map<String, NsiEntity> objectMap = objectList.stream().collect(Collectors.toMap(NsiEntityGen::getGuid, e->e));

        for (IDatagramObject datagramElement : datagramElements)
        {
            String catalogType = datagramElement.getClass().getSimpleName();
//            NsiCatalogType catalog = DataAccessServices.dao().get(NsiCatalogType.class, NsiCatalogType.code(), catalogType);

            if (null != catalogTypeMap.get(datagramElement.getClass().getSimpleName()))
            {
//                NsiEntity entity = DataAccessServices.dao().get(NsiEntity.class, NsiEntity.P_GUID, datagramElement.getID());
                NsiEntity entity = objectMap.get(datagramElement.getID());
                if (entity != null)
                    createEntityLog(entity, catalogType, nsiPackage, null);
                else
                    createInitialEntityLog(datagramElement.getID(), catalogType, nsiPackage);

            }
        }
    }

    @Transactional
    public void sendToNsiQueue(Map<String, IDatagramObject> guidToDatagram, String operationType)
    {
        List<IDatagramObject> datagramElements = new ArrayList<>(guidToDatagram.values());
        ServiceRequestType request = NsiUtils.createServiceRequestType(
                null,
                operationType,
                NsiUtils.createServiceRequestTypeDatagram(datagramElements),
                NsiRequestHandlerService.instance().getSettings().isAsync()
        );

        NsiSubSystemLog log = NsiSyncManager.instance().dao().createLog(request, false, datagramElements);
        NsiQueue queue = new NsiQueue();
        queue.setCreateDate(new Date());
        NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.userCode(), log.getDestination());
        queue.setDestination(subSystem);
        queue.setLog(log);
        DataAccessServices.dao().saveOrUpdate(queue);

        if (operationType.equals(NsiUtils.OPERATION_TYPE_DELETE)) {
            NsiSyncManager.instance().dao().deleteNsiEntitiesByGuids(new ArrayList<>(guidToDatagram.keySet()));
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public NsiSubSystemLog createLog(NsiSubSystemLog nsiSubSystemLog, RoutingHeaderType header)
    {
        NsiSubSystemLog subSystemLog = new NsiSubSystemLog();

        if (null != header)
        {
            subSystemLog.setHeader(new String(NsiUtils.toXml(header), Charsets.UTF_8));
            subSystemLog.setDestination(header.getDestinationId());
            subSystemLog.setGuid(header.getMessageId());

            if (header.getParentId() != null)
            {
                NsiSubSystemLog parent = DataAccessServices.dao().get(NsiSubSystemLog.class, NsiSubSystemLog.guid(), header.getParentId());
                subSystemLog.setPrevAttemptLog(parent);
            }
        }
        subSystemLog.setStartDate(new Date());
        subSystemLog.setStatus(NsiSubSystemLog.STATUS_DRAFT);
        subSystemLog.setPack(nsiSubSystemLog.getPack());
        subSystemLog.setComment("Повторная отправка пакета " + nsiSubSystemLog.getGuid());
        baseCreateOrUpdate(subSystemLog);
        return subSystemLog;
    }

    @Override
    public void updateLogStatusWaitResponse(NsiSubSystemLog subSystemLog)
    {
        subSystemLog.setStatus(NsiSubSystemLog.STATUS_WAIT_RESPONSE);
        baseUpdate(subSystemLog);
    }

    @Override
    public void updateLogStatusDelivered(NsiSubSystemLog subSystemLog)
    {
        subSystemLog.setStatus(NsiSubSystemLog.STATUS_DELIVERED);
        baseUpdate(subSystemLog);
    }

    @Override
    public void finishLog(NsiSubSystemLog subSystemLog, String comment, boolean error, boolean hasWarning)
    {
        Date now = new Date();
        NsiPackage nsiPackage = subSystemLog.getPack();
        nsiPackage.setError(error);
        nsiPackage.setEndDate(now);
        nsiPackage.setStatus(NsiPackage.STATUS_EXECUTED);
        nsiPackage.setWarning(hasWarning);
        baseUpdate(nsiPackage);

        subSystemLog.setStatus(NsiSubSystemLog.STATUS_EXECUTED);
        subSystemLog.setAdditionalStatus(error ? NsiSubSystemLog.ADDITIONAL_STATUS_ERROR : hasWarning ? NsiSubSystemLog.ADDITIONAL_STATUS_WARNING : NsiSubSystemLog.ADDITIONAL_STATUS_SUCCESS);
        subSystemLog.setEndDate(now);
        if (comment != null)
            subSystemLog.setError(comment);

        baseUpdate(subSystemLog);
    }

    @Override
    public void updateStatusInWork(NsiPackage nsiPackage)
    {
        nsiPackage.setStatus(NsiPackage.STATUS_IN_WORK);
        baseUpdate(nsiPackage);
    }

    /**
     * Создает NsiEntityLog без привязки к NsiEntity.
     * Необходимо на этапе, когда из НСИ приходят сущности, которых еще нет в базе,
     * но уже нужно записать лог.
     *
     * @param guid GUID сущности, к которой (будет) привязан NsiEntityLog.
     */
    @Override
    public void createInitialEntityLog(String guid, String catalogType, NsiPackage nsiPackage)
    {
        NsiEntityLog entityLog = new NsiEntityLog();
        entityLog.setGuid(guid);
        entityLog.setCatalogType(catalogType);
        entityLog.setPack(nsiPackage);
        baseCreateOrUpdate(entityLog);
    }

    @Override
    public void createEntityLog(NsiEntity entity, String catalogType, NsiPackage nsiPackage, String warning)
    {
        NsiEntityLog entityLog = new NsiEntityLog();
        entityLog.setCatalogType(catalogType);
        entityLog.setEntity(entity);
        entityLog.setGuid(entity.getGuid());
        entityLog.setPack(nsiPackage);
        entityLog.setWarning(warning);
        baseCreateOrUpdate(entityLog);
    }

    @Override
    @Transactional(readOnly = true)
    public List<NsiSubSystemLogWrapper> getSubSystemLogs(NsiPackage pack)
    {
        if (pack == null)
            return new ArrayList<>();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiSubSystemLog.class, "e")
                .joinEntity("e", DQLJoinType.left, NsiSubSystem.class, "s", eq(property("e", NsiSubSystemLog.destination()), property("s", NsiSubSystem.userCode())))
                .column(property("e"))
                .column(property("s"))
                .where(eq(property("e", NsiSubSystemLog.pack().id()), commonValue(pack.getId(), PropertyType.LONG)))
                .order(property("s", NsiSubSystem.userCode()));
        List<Object[]> objects = createStatement(builder).list();
        List<NsiSubSystemLogWrapper> wrappers = Lists.newArrayListWithExpectedSize(objects.size());
        wrappers.addAll(objects.stream().map(objs -> new NsiSubSystemLogWrapper((NsiSubSystem) objs[1], (NsiSubSystemLog) objs[0])).collect(Collectors.toList()));
        return wrappers;
    }

    @Override
    public void send(NsiPackage pack, List<NsiSubSystemLogWrapper> logs) throws ProcessingException
    {
        for (NsiSubSystemLogWrapper wrapper : logs)
        {
            NsiSubSystemLog log = wrapper.getSubSystemLog();
            if (log == null)
            {
                log = new NsiSubSystemLog();
                log.setPack(pack);
                log.setDestination(wrapper.getSubSystem().getUserCode());
                baseCreate(log);
            }
            NsiRequestHandlerService.instance().executeNSIAction(log);
        }
    }

    @Override
    public int deleteEntities(Class<? extends IEntity> entityClass, List<Long> entityIds)
    {
        int count = 0;

        for (List<Long> elements : Lists.partition(entityIds, 200)) {
            new DQLDeleteBuilder(entityClass)
                    //.where(new DQLCanDeleteExpressionBuilder(entityClass, "id").getExpression())
                    .where(in(property(IEntity.P_ID), elements))
                    .createStatement(getSession()).execute();

            count += deleteNsiEntities(elements);
        }
        return count;
    }

    @Override
    public int deleteNsiEntities(List<Long> entityIds)
    {
        int count = 0;

        for (List<Long> elements : Lists.partition(entityIds, 200))
            count += new DQLUpdateBuilder(NsiEntity.class)
                    .set(NsiEntity.P_DELETED, value(true))
                    .set(NsiEntity.P_UPDATE_DATE, commonValue(new Date()))
                    .where(in(property(NsiEntity.entityId()), elements))
                    .createStatement(getSession()).execute();
        return count;
    }

    @Override
    public List<String> getNonDeletableGuids(Class<? extends IEntity> entityClass, List<Long> entityIds)
    {
        if (null == entityIds || entityIds.isEmpty())
            return new ArrayList<>();

        List<String> list = new ArrayList<>();

        for (List<Long> elements : Lists.partition(entityIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(entityClass, "e")
                    .column(property("e", IEntity.P_ID))
                    .where(new DQLCanDeleteExpressionBuilder(entityClass, "e").getExpression())
                    .where(in(property("e", IEntity.P_ID), elements));

            list.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e")
                    .column(property("nsi", NsiEntity.guid()))
                    .joinEntity("e", DQLJoinType.inner, NsiEntity.class, "nsi", eq(property("e", IEntity.P_ID), property("nsi", NsiEntity.entityId())))
                    .where(notIn(property("e", IEntity.P_ID), sub.buildQuery()))
                    .where(in(property("e", IEntity.P_ID), elements))
                    .createStatement(getSession()).<String>list());
        }

        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> getEntityIdsByDatagramObjects(List<IDatagramObject> datagramObjects)
    {
        if (null == datagramObjects || datagramObjects.isEmpty())
            return null;

        List<String> guids = Lists.newArrayListWithExpectedSize(datagramObjects.size());
        for (IDatagramObject datagramObject : datagramObjects)
        {
            String id = datagramObject.getID();
            if (null == id)
                return null; //Если GUID у сущности НСИ не задан, значит нужны все ИД
            guids.add(id);
        }

        return getEntityIdsByGuids(guids);
    }

    @Override
    public List<Long> updateNsiEntitiesByDatagramObjects(List<IDatagramObject> datagramObjects)
    {
        if (null == datagramObjects || datagramObjects.isEmpty())
            return null;

        List<String> guids = Lists.newArrayListWithExpectedSize(datagramObjects.size());
        for (IDatagramObject datagramObject : datagramObjects)
        {
            String id = datagramObject.getID();
            if (id != null)
                guids.add(id);
        }

        Date now = new Date();
        for (List<String> elements : Lists.partition(guids, DQL.MAX_VALUES_ROW_NUMBER))
            new DQLUpdateBuilder(NsiEntity.class)
                    .setValue(NsiEntity.P_SYNC, true)
                    .setValue(NsiEntity.P_DELETED, false)
                    .setValue(NsiEntity.P_UPDATE_DATE, now)
                    .where(in(property(NsiEntity.guid()), elements))
                    .createStatement(getSession()).execute();
        return getEntityIdsByGuids(guids);
    }

    @Override
    public void clearSession() {
        getSession().flush();
        getSession().clear();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> getEntityIdsByGuids(List<String> guids)
    {
        Set<Long> result = new HashSet<>();
        for (List<String> elements : Lists.partition(guids, DQL.MAX_VALUES_ROW_NUMBER))
            result.addAll(new DQLSelectBuilder().fromEntity(NsiEntity.class, "e")
                    .column(property("e", NsiEntity.entityId()))
                    .where(in(property("e", NsiEntity.guid()), elements))
                    .createStatement(getSession()).<Long>list());

        return new ArrayList<>(result);
    }

    @Override
    @Transactional(readOnly = true)
    public <T> List<String> getGuids(Class<T> entityClass, List<Long> entityIds)
    {
        if (null == entityIds || entityIds.isEmpty())
            return null;

        List<String> entityList = new ArrayList<>();
        for (List<Long> elements : Lists.partition(entityIds, DQL.MAX_VALUES_ROW_NUMBER))
            entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e")
                    .column(property("nsi", NsiEntity.guid()))
                    .joinEntity("e", DQLJoinType.left, NsiEntity.class, "nsi", eq(property("e", IEntity.P_ID), property("nsi", NsiEntity.entityId())))
                    .where(in(property("e", IEntity.P_ID), elements))
                    .createStatement(getSession()).<String>list());
        return entityList;
    }

    @Override
    public <T> List<NsiEntity> getNsiEntitiesByEntityIds(Class<T> entityClass, List<Long> entityIds) {
        String entityType = entityClass.getName();
        List<NsiEntity> resultList = new ArrayList<>();
        for (List<Long> elements : Lists.partition(entityIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                    .column(property("nsi"))
                    .where(in(property("nsi", NsiEntity.entityId()), elements))
                    .where(eq(property("nsi", NsiEntity.entityType()), value(entityType)));
            resultList.addAll(createStatement(builder).list());
        }
        return resultList;
    }

    @Override
    public void deleteNsiEntitiesByGuids(List<String> guids) {
        for (List<String> elements : Lists.partition(guids, DQL.MAX_VALUES_ROW_NUMBER)) {
            new DQLDeleteBuilder(NsiEntity.class)
                    .where(in(property(NsiEntity.guid()), elements))
                    .createStatement(getSession()).execute();
        }
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public <T> List<Pair<T, NsiEntity>> getEntityWithNsiEntityList(Class<T> entityClass, List<Long> entityIds)
    {
        List<Pair<T, NsiEntity>> list = new ArrayList<>();
        if (null == entityIds)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(entityClass, "e")
                    .column(property("e"))
                    .column(property("nsi"))
                    .joinEntity("e", DQLJoinType.left, NsiEntity.class, "nsi", eq(property("e", IEntity.P_ID), property("nsi", NsiEntity.entityId())));
            List<Object[]> objects = createStatement(builder).list();
            list.addAll(objects.stream().map(objs -> new Pair<>((T) objs[0], (NsiEntity) objs[1])).collect(Collectors.toList()));
        }
        else
            for (List<Long> elements : Lists.partition(entityIds, DQL.MAX_VALUES_ROW_NUMBER)) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(entityClass, "e")
                        .column(property("e"))
                        .column(property("nsi"))
                        .joinEntity("e", DQLJoinType.left, NsiEntity.class, "nsi", eq(property("e", IEntity.P_ID), property("nsi", NsiEntity.entityId())))
                        .where(in(property("e", IEntity.P_ID), elements));
                List<Object[]> objects = createStatement(builder).list();
                for (Object[] objs : objects)
                    list.add(new Pair<>((T) objs[0], (NsiEntity) objs[1]));
            }
        return list;
    }

    /**
     * Метод возвращает мапу ид сущностей на саму сущность и соотв. ей nsiEntity
     *
     * @param entityClass
     * @param entityIds   идентификаторы сохраняемых сущностей uni
     * @param <T>
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public <T extends IEntity> Map<String, Pair<T, NsiEntity>> getEntityWithNsiIdsMap(Class<T> entityClass, List<Long> entityIds)
    {
        getSession().clear();

        Map<String, Pair<T, NsiEntity>> result = new HashMap<>();
        //связи между существующими сущностями УНИ и сущностями НСИ
        List<Pair<T, NsiEntity>> entityList = getEntityWithNsiEntityList(entityClass, entityIds);

        // На случай, если объект удалён, но остался его GUID, нужно поднять в мапу эти guid'ы
        List<Long> deletedEntityIds = null;
        if (entityIds != null && !entityIds.isEmpty())
        {
            deletedEntityIds = new ArrayList<>(entityIds);
            //находим id сущностей, уже сохранненых в базе. Их guid'ы не нужны
            Set<Long> persistentEntityIds = entityList.stream().map(p -> p.getX().getId()).collect(Collectors.toSet());
            deletedEntityIds.removeAll(persistentEntityIds);
        }
        if (deletedEntityIds != null && !deletedEntityIds.isEmpty())
        {
            if (null != entityClass)
            {
                for (List<Long> elements : Lists.partition(deletedEntityIds, DQL.MAX_VALUES_ROW_NUMBER)) {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                            .column(property("nsi"))
                            .where(and(
                                    eq(property("nsi", NsiEntity.entityType()), value(entityClass.getName())),
                                    in(property("nsi", NsiEntity.P_ENTITY_ID), elements)
                            ));

                    List<NsiEntity> nsiEntityList = createStatement(builder).list();

                    for (NsiEntity nsiEntity : nsiEntityList)
                        entityList.add(new Pair<>(null, nsiEntity));
                }
            }

        }

        entityList.stream()
                .filter(pair -> null != pair.getY())
                .forEach(pair -> result.put(pair.getY().getGuid(), pair));

        return result;
    }

    @Override
    public NsiEntity saveOrUpdateNsiEntity(IEntity entity, NsiEntity nsiEntity, String guid, String catalogType)
    {
        if (null == entity.getId())
            return null;

        Date now = new Date();
        boolean isNew = null == nsiEntity;
        if (isNew)
        {
            nsiEntity = new NsiEntity();
            if (guid == null)
                guid = UUID.nameUUIDFromBytes(String.valueOf(entity.getId() + catalogType).getBytes(Charsets.UTF_8)).toString();
            nsiEntity.setGuid(guid);
            nsiEntity.setEntityId(entity.getId());
            nsiEntity.setEntityType(ReactorHolder.getReactorMap().get(catalogType).getEntityClass().getName());
            nsiEntity.setType(catalogType);
            nsiEntity.setCreateDate(now);
        }
        else
            nsiEntity.setSync(true);
        if (!entity.getId().equals(nsiEntity.getEntityId()))
        {
            nsiEntity.setEntityId(entity.getId());
        }
        nsiEntity.setDeleted(false);

        nsiEntity.setUpdateDate(now);
        getSession().saveOrUpdate(nsiEntity);
        getSession().flush();

        if (isNew) {
            doResolveLogToEntityReference(nsiEntity);
            new Logger().log(Level.INFO, "NSI object with GUID=" + nsiEntity.getGuid() + " was successfully added");
        }
        else
            new Logger().log(Level.INFO, "NSI object with GUID=" + nsiEntity.getGuid() + " was successfully updated");
        return nsiEntity;
    }

    @Override
    public void doResolveLogToEntityReference(NsiEntity entity)
    {
        new DQLUpdateBuilder(NsiEntityLog.class)
                .set(NsiEntityLog.entity().s(), commonValue(entity))
                .where(eq(property(NsiEntityLog.guid()), value(entity.getGuid())))
                .where(isNull(property(NsiEntityLog.entity())))
                .createStatement(getSession()).execute();
    }

    @Override
    public void updateSyncTime(Long catalogId)
    {
        NsiCatalogType catalog = DataAccessServices.dao().get(NsiCatalogType.class, catalogId);
        if (catalog != null)
        {
            catalog.setSyncDate(new Date());
            baseUpdate(catalog);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public <T> List<T> getEntityWithoutNsiEntityList(Class<T> entityClass)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiEntity.class, "nsi")
                .column(property("nsi", NsiEntity.entityId()));

        return new DQLSelectBuilder().fromEntity(entityClass, "e")
                .column(property("e"))
                .where(notIn(property("e", IEntity.P_ID), builder.buildQuery()))
                .createStatement(getSession()).list();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public NsiSubSystemLog updateStartOutcomingSubsystemLog(NsiSubSystemLog subSystemLog, boolean isAutoRepeat, boolean isManualRestart, boolean isWaitForResponse, String comment, Exception exception, ServiceResponseType response)
    {
        ILogger serviceLogger = NsiRequestHandlerService.instance().getLogger();
        if (null == subSystemLog)
        {
            serviceLogger.log(Level.ERROR, "!!!!!!! Some error has occurred, but was not logged in GUI !!!!!!\n SubsystemLog could not be found for given Id at database.");
            return null;
        }

        NsiQueue queue = DataAccessServices.dao().get(NsiQueue.class, NsiQueue.log(), subSystemLog);

        if (isAutoRepeat || isManualRestart)
        {
            if (subSystemLog.getAttemptNumber() == 0 && !isManualRestart)
            {
                if (NsiSubSystemLog.STATUS_WAIT_RESPONSE != subSystemLog.getStatus())
                {
                    subSystemLog.setAttemptNumber(subSystemLog.getAttemptNumber() + 1);
                    baseCreateOrUpdate(subSystemLog);
                }
            }
            else
            {
                RoutingHeaderType header = new RoutingHeaderType();
                header.setOperationType(subSystemLog.getPack().getType());
                header.setDestinationId(subSystemLog.getDestination());
                header.setMessageId(NsiUtils.generateGUID(new Date().getTime()));
                header.setSourceId(NsiRequestHandlerService.instance().getSettings().getSystemCode());
                header.setCorrelationId(subSystemLog.getGuid());

                NsiSubSystemLog oldSubSystemLog = subSystemLog;
                subSystemLog = new NsiSubSystemLog();
                subSystemLog.setStartDate(new Date());
                subSystemLog.setStatus(NsiSubSystemLog.STATUS_DRAFT);
                subSystemLog.setHeader(new String(NsiUtils.toXml(header), Charsets.UTF_8));
                if (isManualRestart)
                    subSystemLog.setAttemptNumber(1);
                else
                    subSystemLog.setAttemptNumber(oldSubSystemLog.getAttemptNumber() + 1);
                subSystemLog.setDestination(oldSubSystemLog.getDestination());
                subSystemLog.setPack(oldSubSystemLog.getPack());
                subSystemLog.setGuid(header.getMessageId());
                subSystemLog.setPrevAttemptLog(subSystemLog);
                baseCreateOrUpdate(subSystemLog);

                if (null == queue)
                {
                    queue = new NsiQueue();
                    queue.setCreateDate(new Date());
                    NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.userCode(), subSystemLog.getDestination());
                    queue.setDestination(subSystem);
                    queue.setLog(subSystemLog);
                    baseCreateOrUpdate(queue);
                }
                else
                {
                    queue.setLog(subSystemLog);
                    baseCreateOrUpdate(queue);
                }
            }
        }

        if (isWaitForResponse)
        {
            subSystemLog.setStartDate(new Date());
            subSystemLog.setStatus(NsiSubSystemLog.STATUS_WAIT_RESPONSE);
        }

        subSystemLog.setComment(comment);
        if (null != response)
        {
            subSystemLog.setStatus(NsiSubSystemLog.STATUS_EXECUTED);
            subSystemLog.setAdditionalStatus(response.getCallCC().intValue());
            if (null != StringUtils.trimToNull(response.getCallRC()))
            {
                subSystemLog.setComment(response.getCallRC());
            }
        }

        boolean isTransportError = false;
        if (null != exception)
        {
            if (null == response)
                subSystemLog.setAdditionalStatus(NsiSubSystemLog.ADDITIONAL_STATUS_ERROR);

            if (exception instanceof WebServiceException)
            {
                subSystemLog.setStatus(NsiSubSystemLog.STATUS_EXECUTED);
                subSystemLog.setComment(exception.getMessage());
                subSystemLog.setError(exception.getMessage());

                //subSystemLog.setAdditionalStatus(NsiSubSystemLog.ADDITIONAL_STATUS_TRANSPORT_ERROR);
                subSystemLog.setAdditionalStatus(NsiSubSystemLog.ADDITIONAL_STATUS_ERROR);
                NsiDeliveryDaemon.lockByTransportError(subSystemLog.getId());
                isTransportError = true;
            }
            else
                subSystemLog.setError(exception.getCause() == null ? exception.getMessage() : exception.getCause().toString());
        }

        if (null == response && !isWaitForResponse)
        {
            subSystemLog.setStatus(NsiSubSystemLog.STATUS_EXECUTED);
            subSystemLog.setAdditionalStatus(NsiSubSystemLog.ADDITIONAL_STATUS_ERROR);
        }

        baseCreateOrUpdate(subSystemLog);

        // В очереди имеет смысл оставлять пакет только в случае транспортной ошибки, либо в случае ожидания ответа
        if (null != queue && NsiSubSystemLog.STATUS_WAIT_RESPONSE != subSystemLog.getStatus() && !isTransportError)
            baseDelete(queue);

        return subSystemLog;
    }

    @Override
    public Long createQueueItemForLogRow(NsiSubSystemLog log)
    {
        NsiQueue queue = DataAccessServices.dao().get(NsiQueue.class, NsiQueue.log().id(), log.getId());
        if (null == queue)
        {
            queue = new NsiQueue();
            NsiSubSystem subSystem = DataAccessServices.dao().get(NsiSubSystem.class, NsiSubSystem.userCode(), log.getDestination());
            queue.setDestination(subSystem);
            queue.setCreateDate(new Date());
            queue.setLog(log);
            baseCreate(queue);
        }

        return queue.getId();
    }
}
