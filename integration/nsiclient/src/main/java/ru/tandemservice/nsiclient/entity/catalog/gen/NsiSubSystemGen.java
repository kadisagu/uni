package ru.tandemservice.nsiclient.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подсистемы НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiSubSystemGen extends EntityBase
 implements INaturalIdentifiable<NsiSubSystemGen>, org.tandemframework.common.catalog.entity.IActiveCatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem";
    public static final String ENTITY_NAME = "nsiSubSystem";
    public static final int VERSION_HASH = -1790918133;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ADDRESS = "address";
    public static final String P_TITLE = "title";
    public static final String P_USER_CODE = "userCode";
    public static final String P_DISABLED_DATE = "disabledDate";
    public static final String P_ENABLED = "enabled";

    private String _code;     // Системный код
    private String _address;     // Адрес адаптера
    private String _title;     // Название
    private String _userCode;     // Пользовательский код
    private Date _disabledDate;     // Дата запрещения
    

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Адрес адаптера.
     */
    @Length(max=2000)
    public String getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес адаптера.
     */
    public void setAddress(String address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Пользовательский код. Свойство должно быть уникальным.
     */
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Пользовательский код. Свойство должно быть уникальным.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Дата запрещения.
     */
    public Date getDisabledDate()
    {
        return _disabledDate;
    }

    /**
     * @param disabledDate Дата запрещения.
     */
    public void setDisabledDate(Date disabledDate)
    {
        dirty(_disabledDate, disabledDate);
        _disabledDate = disabledDate;
    }

    @Override
    public boolean isEnabled()
    {
        return getDisabledDate()==null;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        if( !isInitLazyInProgress() && isEnabled()!=enabled )
        {
            setDisabledDate(enabled ? null : new Date());
        }
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiSubSystemGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((NsiSubSystem)another).getCode());
            }
            setAddress(((NsiSubSystem)another).getAddress());
            setTitle(((NsiSubSystem)another).getTitle());
            setUserCode(((NsiSubSystem)another).getUserCode());
            setDisabledDate(((NsiSubSystem)another).getDisabledDate());
            setEnabled(((NsiSubSystem)another).isEnabled());
        }
    }

    public INaturalId<NsiSubSystemGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<NsiSubSystemGen>
    {
        private static final String PROXY_NAME = "NsiSubSystemNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof NsiSubSystemGen.NaturalId) ) return false;

            NsiSubSystemGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiSubSystemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiSubSystem.class;
        }

        public T newInstance()
        {
            return (T) new NsiSubSystem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "address":
                    return obj.getAddress();
                case "title":
                    return obj.getTitle();
                case "userCode":
                    return obj.getUserCode();
                case "disabledDate":
                    return obj.getDisabledDate();
                case "enabled":
                    return obj.isEnabled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "address":
                    obj.setAddress((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "disabledDate":
                    obj.setDisabledDate((Date) value);
                    return;
                case "enabled":
                    obj.setEnabled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "address":
                        return true;
                case "title":
                        return true;
                case "userCode":
                        return true;
                case "disabledDate":
                        return true;
                case "enabled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "address":
                    return true;
                case "title":
                    return true;
                case "userCode":
                    return true;
                case "disabledDate":
                    return true;
                case "enabled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "address":
                    return String.class;
                case "title":
                    return String.class;
                case "userCode":
                    return String.class;
                case "disabledDate":
                    return Date.class;
                case "enabled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiSubSystem> _dslPath = new Path<NsiSubSystem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiSubSystem");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Адрес адаптера.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getAddress()
     */
    public static PropertyPath<String> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Пользовательский код. Свойство должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Дата запрещения.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getDisabledDate()
     */
    public static PropertyPath<Date> disabledDate()
    {
        return _dslPath.disabledDate();
    }

    /**
     * @return Используется. Свойство не может быть null.
     *
     * Это формула "case when disabledDate is null then true else false end".
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#isEnabled()
     */
    public static PropertyPath<Boolean> enabled()
    {
        return _dslPath.enabled();
    }

    public static class Path<E extends NsiSubSystem> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _address;
        private PropertyPath<String> _title;
        private PropertyPath<String> _userCode;
        private PropertyPath<Date> _disabledDate;
        private PropertyPath<Boolean> _enabled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(NsiSubSystemGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Адрес адаптера.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getAddress()
     */
        public PropertyPath<String> address()
        {
            if(_address == null )
                _address = new PropertyPath<String>(NsiSubSystemGen.P_ADDRESS, this);
            return _address;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(NsiSubSystemGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Пользовательский код. Свойство должно быть уникальным.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(NsiSubSystemGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Дата запрещения.
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#getDisabledDate()
     */
        public PropertyPath<Date> disabledDate()
        {
            if(_disabledDate == null )
                _disabledDate = new PropertyPath<Date>(NsiSubSystemGen.P_DISABLED_DATE, this);
            return _disabledDate;
        }

    /**
     * @return Используется. Свойство не может быть null.
     *
     * Это формула "case when disabledDate is null then true else false end".
     * @see ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem#isEnabled()
     */
        public PropertyPath<Boolean> enabled()
        {
            if(_enabled == null )
                _enabled = new PropertyPath<Boolean>(NsiSubSystemGen.P_ENABLED, this);
            return _enabled;
        }

        public Class getEntityClass()
        {
            return NsiSubSystem.class;
        }

        public String getEntityName()
        {
            return "nsiSubSystem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
