package ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystemLog.logic.NsiSubSystemLogListDSHandler;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Configuration
public class NsiSubSystemLogManager extends BusinessObjectManager
{
    public static NsiSubSystemLogManager instance()
    {
        return instance(NsiSubSystemLogManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler subSystemLogListDSHandler()
    {
        return new NsiSubSystemLogListDSHandler(getName());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> messageStatusExtPoint()
    {
        return itemList(DataWrapper.class).
                add(Integer.toString(NsiSubSystemLog.STATUS_DRAFT), new DataWrapper(NsiSubSystemLog.STATUS_DRAFT, "Инициализация")).
                add(Integer.toString(NsiSubSystemLog.STATUS_WAIT_RESPONSE), new DataWrapper(NsiSubSystemLog.STATUS_WAIT_RESPONSE, "Ожидание ответа")).
                add(Integer.toString(NsiSubSystemLog.STATUS_DELIVERED), new DataWrapper(NsiSubSystemLog.STATUS_DELIVERED, "Доставлен")).
                add(Integer.toString(NsiSubSystemLog.STATUS_EXECUTED), new DataWrapper(NsiSubSystemLog.STATUS_EXECUTED, "Обработан")).
                create();
    }

    @Bean
    public ItemListExtPoint<DataWrapper> messageAdditionalStatusExtPoint()
    {
        return itemList(DataWrapper.class).
                add(Integer.toString(NsiSubSystemLog.ADDITIONAL_STATUS_WARNING), new DataWrapper(NsiSubSystemLog.ADDITIONAL_STATUS_WARNING, "Предупреждение")).
                add(Integer.toString(NsiSubSystemLog.ADDITIONAL_STATUS_ERROR), new DataWrapper(NsiSubSystemLog.ADDITIONAL_STATUS_ERROR, "Ошибка")).
                add(Integer.toString(NsiSubSystemLog.ADDITIONAL_STATUS_SUCCESS), new DataWrapper(NsiSubSystemLog.ADDITIONAL_STATUS_SUCCESS, "Успешно")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler messageStatusComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(NsiSubSystemLogManager.instance().messageStatusExtPoint());
    }
}
