package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.ui.Send;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support.NsiSubSystemLogWrapper;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
@Input({
        @Bind(key = NsiSubSystemSend.SELECT_ACTION, binding = "action", required = true)
})
public class NsiSubSystemSendUI extends UIPresenter
{
    private INsiSubSystemSendAction _action;
    private Boolean _notFinished = true;
    private List<NsiSubSystemLogWrapper> _allSubSystems = Lists.newArrayList();
    private List<NsiSubSystemLogWrapper> _subSystems;

    public INsiSubSystemSendAction getAction() {
        return _action;
    }

    public void setAction(INsiSubSystemSendAction action) {
        _action = action;
    }

    @Override
    public void onComponentActivate()
    {
        _allSubSystems = _action.getSubSystemLogWrappers();
        _subSystems = Lists.newArrayList();
        for (NsiSubSystemLogWrapper log : _allSubSystems)
            if (log.getSubSystemLog().getStatus() != NsiSubSystemLog.STATUS_EXECUTED || log.getSubSystemLog().getAdditionalStatus() != NsiSubSystemLog.ADDITIONAL_STATUS_SUCCESS)
                _subSystems.add(log);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NsiSubSystemSend.SUB_SYSTEM_DS.equals(dataSource.getName()))
        {
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, _allSubSystems);
        }
    }

    public void onClickApply()
    {
        _action.selectSubSystems(_subSystems);
        deactivate();
    }

    public void onChangeNotFinished()
    {
        if(_notFinished)
            _allSubSystems = _action.getSubSystemLogWrappers();
        else
        {
            List<NsiSubSystem> logs = DataAccessServices.dao().getList(NsiSubSystem.class, NsiSubSystem.title().s());
            _allSubSystems = Lists.newArrayListWithExpectedSize(logs.size());
            for(NsiSubSystem log: logs)
                _allSubSystems.add(new NsiSubSystemLogWrapper(log, null));
        }
        int i = 0;
        while (i < _subSystems.size())
        {
            if(!_allSubSystems.contains(_subSystems.get(i)))
                _subSystems.remove(i);
            else
                i++;
        }
    }
}