/* $Id: NsiTestsManager.java 42647 2015-05-18 10:38:24Z aavetisov $ */
package ru.tandemservice.nsiclient.base.bo.NsiTests;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.nsiclient.base.bo.NsiTests.logic.BaseWebServiceClientTest;
import ru.tandemservice.nsiclient.base.bo.NsiTests.logic.IBaseWebServiceClientTest;

/**
 * @author Andrey Avetisov
 * @since 15.05.2015
 */

@Configuration
public class NsiTestsManager extends BusinessObjectManager
{
    public static NsiTestsManager instance()
    {
        return instance(NsiTestsManager.class);
    }

    @Bean
    public IBaseWebServiceClientTest testDao() {return new BaseWebServiceClientTest();}
}
