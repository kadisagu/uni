/* $Id: NsiSubSystemLogWrapper.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiSubSystem.support;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.entity.catalog.NsiSubSystem;

/**
 * @author Andrey Nikonov
 * @since 16.03.2015
 */
public class NsiSubSystemLogWrapper extends DataWrapper
{
    public static final String SUB_SYSTEM_LOG = "subSystemLog";
    public static final String SUB_SYSTEM = "subSystem";

    private NsiSubSystemLog _subSystemLog;
    private NsiSubSystem _subSystem;

    public NsiSubSystemLogWrapper(NsiSubSystem subSystem, NsiSubSystemLog subSystemLog) {
        super(subSystem.getId(), "");
        _subSystemLog = subSystemLog;
        _subSystem = subSystem;
    }

    public NsiSubSystemLog getSubSystemLog()
    {
        return _subSystemLog;
    }

    public NsiSubSystem getSubSystem()
    {
        return _subSystem;
    }
}