package ru.tandemservice.nsiclient.reactor.support;

/**
 * Ошибка обработки объекта датаграммы.
 * @author Andrey Nikonov
 * @since 09.04.2015
 */
public class ProcessingDatagramObjectException extends Exception
{
    public ProcessingDatagramObjectException(String message) {
        super(message);
    }
}
