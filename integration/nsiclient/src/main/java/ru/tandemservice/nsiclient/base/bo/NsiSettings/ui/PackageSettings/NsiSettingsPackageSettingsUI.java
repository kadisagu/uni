package ru.tandemservice.nsiclient.base.bo.NsiSettings.ui.PackageSettings;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.tapestry.IRequestCycle;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.IScriptAction;
import ru.tandemservice.nsiclient.base.bo.NsiSettings.NsiSettingsManager;
import ru.tandemservice.nsiclient.entity.NsiSettings;

import java.io.File;
import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 18.02.2015
 */
public class NsiSettingsPackageSettingsUI extends UIPresenter
{
    public static final String NSI_SHORT_ENTITY_PROPERTY = "nsi.ws.datagram.shortEntityInDatagrams";

    private NsiSettings _nsiSettings;
    private DataWrapper _timeIntervalsCount;
    private List<IntegerWrapper> _timeIntervals;
    private boolean _shortEntity = false;

    private static final int DEFAULT_VALUE = 14400;

    public void init()
    {
        _nsiSettings = NsiSettingsManager.instance().dao().getNotNullSettings();
        String timeIntervals = _nsiSettings.getTimeIntervals();
        int timeIntervalsCount = 1;
        String[] intervals = null;
        if(timeIntervals != null)
        {
            intervals = timeIntervals.split(",");
            if(intervals.length > 0)
                timeIntervalsCount = intervals.length;
        }

        _timeIntervalsCount = new DataWrapper((long) timeIntervalsCount, "");
        _timeIntervals = Lists.newArrayListWithExpectedSize(_timeIntervalsCount.getId().intValue());
        if(intervals != null)
            for (String interval : intervals)
                _timeIntervals.add(new IntegerWrapper(_timeIntervals.size() + 1, Integer.parseInt(interval)));
        else
            _timeIntervals.add(new IntegerWrapper(_timeIntervals.size() + 1, DEFAULT_VALUE));
    }

    @Override
    public void onComponentActivate()
    {
        init();
        final PageableSearchListDataSource ds = _uiConfig.getDataSource(NsiSettingsPackageSettings.TIME_INTERVALS_DS);
        ds.setDelegate(new IListDataSourceDelegate()
        {
            @SuppressWarnings("unchecked")
            @Override
            public void updateListDataSource(IBusinessComponent component)
            {
                ds.setCountRecord(_timeIntervals.size());
                CommonBaseSearchListUtil.createPage(ds.getLegacyDataSource(), _timeIntervals);
            }
        });
    }

    @Override
    public void onComponentRefresh()
    {
        if (ApplicationRuntime.existProperty(NSI_SHORT_ENTITY_PROPERTY))
            _shortEntity = Boolean.parseBoolean(ApplicationRuntime.getProperty(NSI_SHORT_ENTITY_PROPERTY));
    }

    public void onChangeTimeIntervalsCount()
    {
        if(_timeIntervalsCount == null)
            return;
        int count = _timeIntervalsCount.getId().intValue();
        if(count == _timeIntervals.size())
            return;
        if(count < _timeIntervals.size())
        {
            _timeIntervals = _timeIntervals.subList(0, count);
        }
        else
        {
            int i = count - _timeIntervals.size();
            while (i-- > 0)
                _timeIntervals.add(new IntegerWrapper(_timeIntervals.size() + 1, DEFAULT_VALUE));
        }
    }

    public Integer getTimeInterval()
    {
        IntegerWrapper wrapper = _uiConfig.getDataSource(NsiSettingsPackageSettings.TIME_INTERVALS_DS).getCurrent();
        return wrapper.getValue();
    }

    public void setTimeInterval(Integer timeInterval)
    {
        IntegerWrapper wrapper = _uiConfig.getDataSource(NsiSettingsPackageSettings.TIME_INTERVALS_DS).getCurrent();
        wrapper.setValue(timeInterval);
    }

    public void onSave() throws Exception
    {
        PropertiesConfiguration config = new PropertiesConfiguration(new File(ApplicationRuntime.getAppConfigPath(), "app.properties"));
        config.setProperty(NSI_SHORT_ENTITY_PROPERTY, String.valueOf(_shortEntity));
        ApplicationRuntime.getProperties().setProperty(NSI_SHORT_ENTITY_PROPERTY, String.valueOf(_shortEntity));
        config.getLayout().setBlancLinesBefore(NSI_SHORT_ENTITY_PROPERTY, 1);
        config.getLayout().setComment(NSI_SHORT_ENTITY_PROPERTY, "datagram settings");
        config.save();

        _nsiSettings.setTimeIntervals(Joiner.on(',').join(_timeIntervals));
        DataAccessServices.dao().update(_nsiSettings);
        TapSupportUtils.addInitScript(new IScriptAction()
        {
            @Override
            public String getScript(IRequestCycle cycle)
            {
                return "alert(\"Изменения сохранены\")";
            }
        });
    }

    public void onCancel()
    {
        init();
    }

    public boolean isShortEntity()
    {
        return _shortEntity;
    }

    public void setShortEntity(boolean shortEntity)
    {
        _shortEntity = shortEntity;
    }
}
