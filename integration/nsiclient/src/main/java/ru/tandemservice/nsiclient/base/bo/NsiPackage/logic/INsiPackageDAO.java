/* $Id: INsiPackageDAO.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient.base.bo.NsiPackage.logic;

import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.nsiclient.entity.NsiPackage;

/**
 * @author Andrey Nikonov
 * @since 18.03.2015
 */
public interface INsiPackageDAO
{
    CoreCollectionUtils.Pair<String, String> findElementsAndCatalogs(NsiPackage pack);
}