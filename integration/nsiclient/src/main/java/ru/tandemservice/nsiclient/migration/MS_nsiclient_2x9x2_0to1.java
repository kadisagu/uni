package ru.tandemservice.nsiclient.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_nsiclient_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nsiSettings

		// создано обязательное свойство async
        if(!tool.columnExists("nsisettings_t", "async_p"))
		{
			// создать колонку
			tool.createColumn("nsisettings_t", new DBColumn("async_p", DBType.BOOLEAN));
			tool.executeUpdate("update nsisettings_t set async_p=? where async_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("nsisettings_t", "async_p", false);
		}
    }
}