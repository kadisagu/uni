/* $Id:$ */
package ru.tandemservice.nsiclient.utils;

import org.apache.log4j.Level;
import org.tandemframework.core.debug.Debug;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.reactor.support.DatagramProcessingInfo;
import ru.tandemservice.nsiclient.reactor.support.IResponseAction;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.List;

/**
 * @author Andrey Nikonov
 * @since 13.11.2015
 */
public class AsyncResponseWaiter {
    public static final long TIMEOUT = 1000000000L;
    private final Object _object = new Object();
    private IResponseAction _action;
    private StringBuilder _errorLog = new StringBuilder();
    private StringBuilder _warnLog = new StringBuilder();

    public AsyncResponseWaiter(IResponseAction action) {
        _action = action;
    }

    public DatagramProcessingInfo launchWaiting() {
        synchronized (_object) {
            try {
                _object.wait(TIMEOUT);
            } catch (InterruptedException e) {
                // в любом случае выходим
            }
            return new DatagramProcessingInfo(_errorLog.length() == 0 ? null : _errorLog.toString(), _warnLog.length() == 0 ? null : _warnLog.toString());
        }
    }

    public void dataReady(NsiSubSystemLog subSystemLog, List<IDatagramObject> datagramElements, boolean finished, String asyncRequestWarning) {
        synchronized (_object) {
            StringBuilder warns = new StringBuilder();
            String error = null;

            try {
                if (_action != null) {
                    DatagramProcessingInfo info = _action.action(subSystemLog, datagramElements);
                    if(info != null) {
                        NsiUtils.appendMessage(warns, info.getWarn());
                        error = info.getError();
                    }
                }

                StringBuilder comment = new StringBuilder(asyncRequestWarning == null ? "" : asyncRequestWarning);
                NsiUtils.appendMessage(comment, warns);

                NsiSyncManager.instance().dao().finishLog(subSystemLog, comment.toString(), false, comment.length() > 0);
            } catch (Throwable t) {
                Debug.exception(t);
                Debug.saveDebug();
                NsiRequestHandlerService.instance().getLogger().log(Level.ERROR, "Unknown error in executeInsertNSIAction: " + t.getMessage());
                error = error == null ? t.getMessage() : error + '\n' + t.getMessage();

                StringBuilder comment = new StringBuilder(asyncRequestWarning == null ? "" : asyncRequestWarning);
                NsiUtils.appendMessage(comment, error);
                NsiUtils.appendMessage(comment, warns);

                NsiSyncManager.instance().dao().finishLog(subSystemLog, comment.toString(), true, asyncRequestWarning != null);
            }

            DatagramProcessingInfo resultWrapper = new DatagramProcessingInfo(error, warns.length() == 0 ? null : warns.toString());
            if(resultWrapper.getError() != null)
                _errorLog.append(resultWrapper.getError());
            if(resultWrapper.getWarn() != null)
                _warnLog.append(resultWrapper.getWarn());
            if(finished)
                stop();
        }
    }

    public void stop() {
        synchronized (_object) {
            _object.notify();
        }
    }
}