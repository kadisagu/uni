/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic.daemon;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.process.BackgroundProcessThread;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLInsertValuesBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.NsiSyncOperatorDAO;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackFormingQueue;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 27.10.2016
 */
public class NsiFullSyncDaemon<T extends IEntity, V extends IDatagramObject> extends SharedBaseDao implements INsiFullSyncDaemon<T, V>
{
    //public static final String NSI_SERVICE_AUTOSYNC = "nsi.service.autosync";

    public static final int DAEMON_ITERATION_TIME = 1;
    private static BackgroundProcessThread _thread = null;
    private static boolean _cancelled = false;
    private static boolean _sync_locked = false;
    private static boolean _sync_finished = false;

    private static String _prefix = null;
    private static INsiEntityReactor _reactor = null;
    private static StringBuilder _warn = new StringBuilder();
    private static StringBuilder _errors = new StringBuilder();
    private static AtomicInteger _count = new AtomicInteger(0);
    private static AtomicInteger _packageCount = new AtomicInteger(0);
    private static AtomicInteger _packagesAll = new AtomicInteger(0);
    private static int _errCnt = 0;

    public void doInit(BackgroundProcessThread thread, INsiEntityReactor<T, V> reactor, AtomicInteger packagesAll, String prefix, boolean resume)
    {
        _sync_locked = true; // Блокируем работу демона на период формирования списка сущностей на отправку

        _prefix = prefix;
        _thread = thread;
        _cancelled = false;
        _sync_finished = false;
        _reactor = reactor;
        _packagesAll = packagesAll;
        _warn = new StringBuilder();
        _errors = new StringBuilder();
        _count = new AtomicInteger(0);
        _packageCount = new AtomicInteger(0);
        _errCnt = 0;

        // Удаляем все записи из буферной таблицы (нужно избавиться от меток, которые остались из-за ошибки на предыдущей синхронизации)
        if(!resume)
        {
            DQLDeleteBuilder deleteBuilder1 = new DQLDeleteBuilder(NsiPackFormingQueue.class);
            deleteBuilder1.createStatement(getSession()).execute();
        }
    }

    public static void unlockDaemon()
    {
        _sync_locked = false;
    }

    public static boolean isFinished()
    {
        return _sync_finished;
    }

    @Override
    public void createCatalogIdStackForPackSend(List<Long> entityIdList)
    {
        int updCnt = 0;
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(NsiPackFormingQueue.class);
        for (Long entityId : entityIdList)
        {
            if (_cancelled) return;

            insertBuilder.value(NsiPackFormingQueue.createDate().s(), new Date());
            insertBuilder.value(NsiPackFormingQueue.entityId().s(), entityId);
            insertBuilder.addBatch();
            updCnt++;

            if (updCnt == 300)
            {
                createStatement(insertBuilder).execute();
                insertBuilder = new DQLInsertValuesBuilder(NsiPackFormingQueue.class);
                updCnt = 0;
            }

        }

        if (updCnt > 0) createStatement(insertBuilder).execute();

    }

    public void doInterruptFullSync()
    {
        _cancelled = true;
        DQLDeleteBuilder deleteBuilder1 = new DQLDeleteBuilder(NsiPackFormingQueue.class);
        deleteBuilder1.createStatement(getSession()).execute();
    }

    public List<Object[]> getObjectsPortion()
    {
        if(null == _reactor) return new ArrayList<>();

        int packageSize = NsiRequestHandlerService.instance().getSettings().getPackageSize();

        Class<T> reactorEntityClass = _reactor.getEntityClass();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiPackFormingQueue.class, "q").column(property("q")).column(property("e")).column(property("n")).top(packageSize)
                .joinEntity("q", DQLJoinType.inner, reactorEntityClass, "e", eq(property(NsiPackFormingQueue.entityId().fromAlias("q")), property("e", IEntity.P_ID)))
                .joinEntity("q", DQLJoinType.left, NsiEntity.class, "n", eq(property(NsiPackFormingQueue.entityId().fromAlias("q")), property("n", NsiEntity.entityId())))
                .order(property(NsiPackFormingQueue.createDate().fromAlias("q")));

        return builder.createStatement(getSession()).list();
    }

    public void deleteSentFromQueue(List<Long> queueIdList)
    {
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(NsiPackFormingQueue.class).where(in(property(NsiPackFormingQueue.id()), queueIdList));
        deleteBuilder.createStatement(getSession()).execute();
    }

    public void doSendToNsiItems(List<CoreCollectionUtils.Pair<T, NsiEntity>> entityList)
    {
        Map<String, Object> commonCache = new HashMap<>(1);
        commonCache.put(NsiSyncOperatorDAO.PROCESSED, new HashSet<Long>());
        List<IDatagramObject> nsiInsertEntityList = new ArrayList<>(entityList.size());
        int index = 1;
        for (CoreCollectionUtils.Pair<T, NsiEntity> pair : entityList)
        {
            if(_cancelled) return;

            ResultListWrapper wrapper = _reactor.createDatagramObject(pair.getX(), pair.getY(), commonCache);
            List<IDatagramObject> list = wrapper.getList();
            IDatagramObject obj = list.get(0);
            nsiInsertEntityList.add(obj);
            NsiUtils.appendMessage(_warn, wrapper.getWarning());
            _thread.getState().setMessage(_prefix + "Отправка данных. Подготовка датаграмм (" + index++ + " из " + entityList.size() + "). Всего обработано " + _count.get());
        }

        String packageAllCountStr = _packagesAll != null ? " из " + _packagesAll.get() : "";
        IResponseAction responseAction = new IResponseAction() {
            // ничего не делаем, только говорим, что данные сущности уже синхронизированы (todo может вообще убрать этот код?)
            @Override
            public DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list) throws CriticalException
            {
                _thread.getState().setMessage(_prefix + "Обновление данных по пакету " + _packageCount + packageAllCountStr);
                List<Long> entityIds = NsiSyncManager.instance().dao().updateNsiEntitiesByDatagramObjects(list);
                // регистрируем, что пересылать измененные сущности не надо
                if (entityIds != null)
                    INsiCollectorDaemon.instance.get().removeEntities(entityIds.toArray(new Long[entityIds.size()]));
                return null;
            }
        };

        if(_cancelled) return;

        if (!nsiInsertEntityList.isEmpty()) {
            _thread.getState().setMessage(_prefix + "Отправка данных в НСИ. Пакет № " + _packageCount + packageAllCountStr + ". Всего обработано " + _count.get());
            // Собственно, производим отправку свежедобавляемых элементов в НСИ

            try
            {
                ResultWrapper<List<String>> result = NsiRequestHandlerService.instance().executeNSIAction(nsiInsertEntityList, null, NsiUtils.OPERATION_TYPE_UPDATE, null, responseAction, NsiRequestHandlerService.instance().getSettings().isAsync());
                NsiUtils.appendMessage(_warn, result.getWarning());
                NsiUtils.appendMessage(_errors, StringUtils.trimToNull(StringUtils.join(result.getResult(), '\n')));
            }
            catch (Exception e)
            {
                System.err.println("################## " + NsiSyncManager.DATE_FORMATTER_WITH_TIME_MILLISEC.format(new Date()));
                e.printStackTrace();
                _errCnt ++;
            }

            _count.addAndGet(nsiInsertEntityList.size());
        }

        _packageCount.addAndGet(1);
        if(_packagesAll != null && _packagesAll.get() < _packageCount.get())
            _packagesAll.set(_packageCount.get());
    }

    @Override
    public List<Object[]> doEnsureGuidExistsOrCreateIt(List<Object[]> srcItems)
    {
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactorMap = ReactorHolder.getReactorMap();
        Map<Class, INsiEntityReactor> entityCalssToReactorMap = new HashMap<>();
        for (Map.Entry<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> entry : reactorMap.entrySet())
        {
            INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor = entry.getValue();
            entityCalssToReactorMap.put(reactor.getEntityClass(), reactor);
        }

        List<Object[]> result = new ArrayList<>(srcItems.size());

        for (Object[] item : srcItems)
        {
            if (null == item[2])
            {
                IEntity entity = (IEntity) item[1];
                INsiEntityReactor reactor = entityCalssToReactorMap.get(entity.getClass());
                if (null != reactor)
                {
                    NsiEntity nsiEntity = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(entity, null, null, reactor.getCatalogType());
                    result.add(new Object[]{item[0], entity, nsiEntity});
                }
            } else result.add(item);
        }
        return result;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(NsiFullSyncDaemon.class.getName(), DAEMON_ITERATION_TIME, INsiCollectorDaemon.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            if(!_sync_locked)
            {
                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    List<Object[]> preItems = INsiFullSyncDaemon.instance.get().getObjectsPortion();
                    List<Object[]> items = INsiFullSyncDaemon.instance.get().doEnsureGuidExistsOrCreateIt(preItems);
                    if (items.size() == 0)
                    {
                        _sync_finished = true;
                        return;
                    }

                    List<Long> queueIdToDel = new ArrayList<>(items.size());
                    List<CoreCollectionUtils.Pair<IEntity, NsiEntity>> entityList = new ArrayList<>(items.size());
                    for (Object[] item : items)
                    {
                        queueIdToDel.add(((NsiPackFormingQueue) item[0]).getId());
                        entityList.add(new CoreCollectionUtils.Pair<>((IEntity) item[1], (NsiEntity) item[2]));
                    }

                    INsiFullSyncDaemon.instance.get().doSendToNsiItems(entityList);
                    INsiFullSyncDaemon.instance.get().deleteSentFromQueue(queueIdToDel);
                } catch (Throwable t)
                {
                    Debug.exception(t.getMessage(), t);
                    throw t;
                } finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                }
            }
        }
    };

    public static int getErrCnt()
    {
        return _errCnt;
    }

    public static StringBuilder getWarn()
    {
        return _warn;
    }

    public static StringBuilder getErrors()
    {
        return _errors;
    }

    public static int getCount()
    {
        return _count.get();
    }
}
