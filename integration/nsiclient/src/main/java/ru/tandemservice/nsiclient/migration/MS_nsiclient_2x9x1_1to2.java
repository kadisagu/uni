package ru.tandemservice.nsiclient.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Меняем местами code_p и nsicode_p в nsicatalogtype_t, т.к. справочники должны быть уникальны по датаграмме из НСИ, но могут иметь одинаковые сущности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_nsiclient_2x9x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
    }
}