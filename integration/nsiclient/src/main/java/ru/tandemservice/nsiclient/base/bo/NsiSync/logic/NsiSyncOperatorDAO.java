/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiSync.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.hibernate.FlushMode;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.entity.NsiSubSystemLog;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.IOperationReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiclient.ws.NsiRequestHandlerService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Nikonov
 * @since 21.10.2015
 */
@Transactional
public class NsiSyncOperatorDAO extends SharedBaseDao implements INsiSyncOperatorDAO {
    public static final String ITEMS_MAP = "itemsMap";
    public static final String REACTOR_CACHE = "reactorCache";
    public static final String PROCESSED = "processed";

    @FunctionalInterface
    private interface IOperationProcess {
        List<IDatagramObject> process(List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors, Map<String, List<IDatagramObject>> nsiEntityTypeToDatagramListMap, StringBuilder warnLog, StringBuilder errorLog);
    }

    private static Map<String, List<IDatagramObject>> createNsiEntityTypeToEntityListMap(List<IDatagramObject> datagramElements)
    {
        Map<String, List<IDatagramObject>> map = new HashMap<>();
        for (IDatagramObject item : datagramElements)
        {
            String nsiEntityType = item.getClass().getSimpleName();
            List<IDatagramObject> entityList = map.get(nsiEntityType);
            if (null == entityList)
                entityList = new ArrayList<>();
            entityList.add(item);
            map.put(nsiEntityType, entityList);
        }
        return map;
    }

    public List<IDatagramObject> processDatagrams(List<IDatagramObject> datagramElements, IOperationProcess operationProcess, StringBuilder warnLog, StringBuilder errorLog) {
        ILogger serviceLogger = NsiRequestHandlerService.instance().getLogger();
        Map<String, INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactorMap = ReactorHolder.getReactorMap();
        List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> sortedReactorList = ReactorHolder.getSortedReactorList();
        Map<String, List<IDatagramObject>> nsiEntityTypeToEntityListMap = createNsiEntityTypeToEntityListMap(datagramElements);
        StringBuilder unsupported = new StringBuilder();

        List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors = new ArrayList<>();
        for(INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor: sortedReactorList) {
            List<IDatagramObject> list = nsiEntityTypeToEntityListMap.get(reactor.getCatalogType());
            if(list != null)
                reactors.add(reactor);
        }
        if(reactorMap != null)
            nsiEntityTypeToEntityListMap.entrySet().stream().filter(entry -> reactorMap.get(entry.getKey()) == null).forEach(entry -> {
                serviceLogger.getLog4jLogger().error("There are some unrecognized/unsupported elements in datagram.");
                if (unsupported.length() > 0)
                    unsupported.append(", ");
                unsupported.append('\'').append(entry.getKey()).append('\'');
            });

        List<IDatagramObject> result = operationProcess.process(reactors, nsiEntityTypeToEntityListMap, warnLog, errorLog);

        if(unsupported.length() > 0) {
            if (warnLog.length() > 0)
                warnLog.append('\n');
            warnLog.append("System does not support next catalogs: ").append(unsupported).append('.');
        }
        return result;
    }

    private List<IDatagramObject> execute(List<IDatagramObject> datagramElements, NsiPackage nsiPackage, IResultProcessor resultProcessor, Map<String, Object> commonCache, StringBuilder warnLog, StringBuilder errorLog) {
        @SuppressWarnings("unchecked")
        Map<String, Map<String, Object>> allReactorsCache = (Map<String, Map<String, Object>>) commonCache.get(REACTOR_CACHE);

        IOperationProcess operationProcess = (reactors, nsiEntityTypeToDatagramListMap, internalWarnLog, internalErrorLog) -> {
            List<IDatagramObject> result = new ArrayList<>();

            for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : reactors)
            {
                String name = reactor.getCatalogType();
                List<IDatagramObject> items = nsiEntityTypeToDatagramListMap.get(name);

                List<IDatagramObject> r;
                IOperationReactor operationReactor = reactor.getUpdateOperationReactor();
                if(operationReactor != null) {
                    Map<String, Object> reactorCache = allReactorsCache == null ? null : allReactorsCache.get(name);
                    r = operationReactor.process(datagramElements, nsiPackage, commonCache, reactorCache, warnLog, errorLog);
                } else
                    r = updateDatagramObjectsInReactor(reactor, items, commonCache, nsiPackage, resultProcessor, internalWarnLog, internalErrorLog);

                if(r != null && !r.isEmpty())
                    result.addAll(r);
            }
            return result;
        };

        return processDatagrams(datagramElements, operationProcess, warnLog, errorLog);
    }

    /**
     * Возвращает мн-во неизвестных датаграмм в пакете
     * @param reactors
     * @param nsiEntityTypeToDatagramListMap
     * @param itemsMap
     * @param requiredElements
     * @return
     */
    private static <V extends IDatagramObject> List<IDatagramObject> findUnknownDatagrams(List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors, Map<String, List<IDatagramObject>> nsiEntityTypeToDatagramListMap, Map<String, Pair<IDatagramObject, ChangedWrapper<? extends IEntity>>> itemsMap, List<IDatagramObject> requiredElements) {
        UnknownDatagramsCollector collector = new UnknownDatagramsCollector(itemsMap, requiredElements);

        for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : reactors) {
            String name = reactor.getCatalogType();
            @SuppressWarnings("unchecked")
            INsiEntityReactor<? extends IEntity, V> r = (INsiEntityReactor<? extends IEntity, V>) reactor;
            @SuppressWarnings("unchecked")
            List<V> items = (List<V>) nsiEntityTypeToDatagramListMap.get(name);

            for (V datagramObject : items)
                r.collectUnknownDatagrams(datagramObject, collector);
        }
        return collector.getDatagrams();
    }

    /**
     *
     * @param allElements       все датаграммы
     * @param datagramElements  текущие обрабатываемые датаграммы
     * @param requiredElements  требуемые датаграммы
     * @param nsiPackage        пакет
     * @param resultProcessor   обработчик
     * @param commonCache       кэш
     * @param retrieveCount     номер попытки получения данных (todo сделать ограничение на кол-во попыток)
     * @param messageId
     * @param warnLog           лог предупреждений
     * @param errorLog          лог ошибок
     * @return
     */
    private List<IDatagramObject> preUpdateInternal(List<IDatagramObject> allElements, List<IDatagramObject> datagramElements, List<IDatagramObject> requiredElements, NsiPackage nsiPackage, IResultProcessor resultProcessor, Map<String, Object> commonCache, int retrieveCount, String messageId, StringBuilder warnLog, StringBuilder errorLog) {
        // результаты обработки пакета, ключ - guid, значение - пара объект датаграммы и соответствующая сущность
        @SuppressWarnings("unchecked")
        Map<String, Pair<IDatagramObject, ChangedWrapper<? extends IEntity>>> map = (Map<String, Pair<IDatagramObject, ChangedWrapper<? extends IEntity>>>) commonCache.get(ITEMS_MAP);
        if(map == null) {
            map = new HashMap<>(datagramElements.size());
            commonCache.put(ITEMS_MAP, map);
        }
        // заполняем парами, где вместо сущности null, в процессе обработки туда будут подставляться реальные сущности
        for (IDatagramObject datagramObject : datagramElements)
            map.put(datagramObject.getID(), new Pair<>(datagramObject, null));

        // для обработки в лямбде нужен final
        Map<String, Pair<IDatagramObject, ChangedWrapper<? extends IEntity>>> itemsMap = map;
        @SuppressWarnings("unchecked")
        Map<String, Map<String, Object>> allReactorsCache = (Map<String, Map<String, Object>>) commonCache.get(REACTOR_CACHE);

        IOperationProcess operationProcess = new IOperationProcess() {
            @Override
            public List<IDatagramObject> process(List<INsiEntityReactor<? extends IEntity, ? extends IDatagramObject>> reactors, Map<String, List<IDatagramObject>> nsiEntityTypeToDatagramListMap,
                                                 StringBuilder internalWarnLog, StringBuilder internalErrorLog)
            {
                ILogger serviceLogger = NsiRequestHandlerService.instance().getLogger();

                for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : reactors)
                {
                    String name = reactor.getCatalogType();
                    List<IDatagramObject> items = nsiEntityTypeToDatagramListMap.get(name);
                    Map<String, Object> reactorCache = allReactorsCache.get(name);
                    if (reactorCache == null) {
                        reactorCache = new HashMap<>();
                        allReactorsCache.put(name, reactorCache);
                    }
                    // если preUpdate упал, то мы не можем обработать такой реактор
                    try {
                        reactor.preUpdate(items, reactorCache);
                    } catch (ProcessingException e) {
                        serviceLogger.log(Level.ERROR, "Error in preUpdate for " + reactor.getCatalogType() + " reactor: " + e.getMessage());
                        if (errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append(e.getMessage());
                    }
                }

                // мн-во неизвестных датаграмм
                List<IDatagramObject> unknownDatagrams = findUnknownDatagrams(reactors, nsiEntityTypeToDatagramListMap, itemsMap, requiredElements);
                // если мн-во неизвестных guid не пусто, то делаем запрос к НСИ
                if (!unknownDatagrams.isEmpty()) {
                    serviceLogger.log(Level.INFO, "Retrieve count " + retrieveCount + ". There are " + unknownDatagrams.size() + " unknown datagrams");
                    // запрос в НСИ может не пройти, игнорируем
                    try {
                        // список полученных из НСИ
                        List<IDatagramObject> retrievedFromNSI = new ArrayList<>();
                        // todo почему синхронный запрос? здесь, конечно, ожидается небольшое кол-во неизвестных элементов, но тем не менее
                        NsiRequestHandlerService.instance().executeNSIAction(unknownDatagrams, null, NsiUtils.OPERATION_TYPE_RETRIEVE, messageId, new IResponseAction() {
                            @Override
                            public DatagramProcessingInfo action(NsiSubSystemLog subSystemLog, List<IDatagramObject> list) throws CriticalException {
                                retrievedFromNSI.addAll(list);
                                return null;
                            }
                        }, false);

                        serviceLogger.log(Level.INFO, "Retrieved " + retrievedFromNSI.size() + " datagrams from NSI as response for unknown datagrams");
                        allElements.addAll(retrievedFromNSI);

                        // список недополученных из НСИ
                        List<IDatagramObject> notRetrievedDatagrams = new ArrayList<>();
                        for (IDatagramObject unknownDatagram: unknownDatagrams) {
                            boolean isNotFound = true;
                            int i = 0;
                            while (isNotFound && i < retrievedFromNSI.size())
                                isNotFound = !(retrievedFromNSI.get(i++).getID().equals(unknownDatagram.getID()));
                            if(isNotFound)
                                notRetrievedDatagrams.add(unknownDatagram);
                        }
                        serviceLogger.log(Level.INFO, "Unknown " + notRetrievedDatagrams.size() + " datagrams from NSI as response for unknown datagrams");

                        // обрабатываем полученные датаграммы, может там окажутся ещё неизвестные нам ссылки
                        return preUpdateInternal(allElements, retrievedFromNSI, notRetrievedDatagrams, nsiPackage, resultProcessor, commonCache, retrieveCount + 1, messageId, warnLog, errorLog);
                    } catch (ProcessingException e) {
                        if (errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append(e.getMessage());
                    }
                }
                return execute(allElements, nsiPackage, resultProcessor, commonCache, warnLog, errorLog);
            }
        };

        return processDatagrams(datagramElements, operationProcess, warnLog, errorLog);
    }

    @Override
    public SyncOperatorResult update(List<IDatagramObject> datagramElements, NsiPackage nsiPackage, IResultProcessor resultProcessor, String messageId)
    {
        FlushMode mode = getSession().getFlushMode();

        try {
            // устанавливаем ручной режим, чтобы вызов readonly методов из сторонних dao не вызывал flush
            getSession().setFlushMode(FlushMode.MANUAL);
            Map<String, Object> commonCache = new HashMap<>();
            commonCache.put(REACTOR_CACHE, new HashMap<String, Map<String, Object>>());
            StringBuilder warnLog = new StringBuilder();
            StringBuilder errorLog = new StringBuilder();
            List<IDatagramObject> result = preUpdateInternal(datagramElements, datagramElements, null, nsiPackage, resultProcessor, commonCache, 0, messageId, warnLog, errorLog);
            return new SyncOperatorResult(warnLog.length() > 0 ? warnLog.toString() : null, errorLog.length() > 0 ? errorLog.toString() : null, result);
        } finally {
            getSession().setFlushMode(mode);
        }
    }

    /**
     * Обработчик объектов датаграммы фильтрованных для реактора
     * @param reactor   какой реактор обрабатывает датаграммы
     * @param items     список датаграмм для обработки
     * @param commonCache   кэш процесса обработки пакета
     * @param nsiPackage    пакет (для логирования)
     * @param resultProcessor         дополнительный обработчик объекта датаграммы, вызывается после обработки объекта датаграммы и сохранения изменений в базу
     * @param warnLog   лог предупреждений
     * @param errorLog  лог ошибок
     * @param <T> класс сущности уни
     * @param <V> класс датаграммы
     * @return список обработанных реактором датаграмм
     */
    private <T extends IEntity, V extends IDatagramObject> List<IDatagramObject> updateDatagramObjectsInReactor(INsiEntityReactor<T, V> reactor, List<IDatagramObject> items, Map<String, Object> commonCache, NsiPackage nsiPackage, IResultProcessor resultProcessor, StringBuilder warnLog, StringBuilder errorLog)
    {
        ILogger serviceLogger = NsiRequestHandlerService.instance().getLogger();
        String className = reactor.getCatalogType();
        // результаты обработки пакета, ключ - guid, значение - пара объект датаграммы и соответствующая сущность
        @SuppressWarnings("unchecked")
        Map<String, Pair<IDatagramObject, ChangedWrapper<T>>> map = (Map<String, Pair<IDatagramObject, ChangedWrapper<T>>>) commonCache.get(ITEMS_MAP);

        List<IDatagramObject> processedDatagrams = new ArrayList<>();
        boolean hasEmptyGuid = false;

        for (IDatagramObject datagramObject : items)
        {
            // Если сущность помечена как неконсистентная (например, дубль), то её не нужно учитывать
            if (null == datagramObject.getIsNotConsistent() || 1 != datagramObject.getIsNotConsistent()) {
                String guid = StringUtils.trimToNull(datagramObject.getID());
                // Если у какой-то из сущностей в пакете не указан GUID, то обрабатывать такую сущность нельзя, будем писать в лог
                if (null == guid) {
                    hasEmptyGuid = true;
                    serviceLogger.log(Level.ERROR, "Processing entities for " + className + " catalog was failed: there is element without GUID.");
                } else {
                    Pair<IDatagramObject, ChangedWrapper<T>> pair = map.get(guid);
                    // если нет в результатах обработки...
                    if (pair.getY() == null) {
                        // ...то начинаем обработку
                        processedDatagrams.add(datagramObject);

                        V obj = null;
                        // у нас ранее были отфильтрованы объекты, поэтому такой cast не должен выкинуть ошибку. Но вдруг код поменяется.
                        try {
                            obj = (V) datagramObject;
                        } catch (ClassCastException e) {
                            if (errorLog.length() > 0)
                                errorLog.append('\n');
                            String error = "Error with object with GUID " + guid + ". Datagram object " + datagramObject.getID() + " is not " + className;
                            errorLog.append(error);
                            serviceLogger.log(Level.ERROR, error);
                        }

                        try {
                            // обрабатываем датаграмму в реакторе
                            @SuppressWarnings("unchecked")
                            ChangedWrapper<T> changedWrapper = reactor.processDatagramObject(obj, commonCache, ((Map<String, Map<String, Object>>) commonCache.get(REACTOR_CACHE)).get(className), null);
                            // такого быть не должно, реактор может выкинуть ProcessingDatagramObjectException, но null возвращать не должен
                            if (null == changedWrapper)
                                serviceLogger.log(Level.ERROR, "Error with object with GUID " + guid + ". The result of processing in the " + className + " reactor is null.");
                            else {
                                // закидываем изменения в результаты обработки пакета
                                pair.setY(changedWrapper);
                                if (StringUtils.isNotEmpty(changedWrapper.getWarning())) {
                                    if (warnLog.length() > 0)
                                        warnLog.append('\n');
                                    String warn = "Warning with object with GUID " + guid + ": " + changedWrapper.getWarning();
                                    warnLog.append(warn);
                                    serviceLogger.log(Level.WARN, warn);
                                }
                                // нужен final, поэтому дополнительная переменная
                                V datagramObjectV = obj;
                                DataAccessServices.dao().doInTransaction(session -> {
                                    reactor.save(session, changedWrapper, datagramObjectV, nsiPackage);
                                    // скидываем изменения в базу, т.к. у нас ручной flush
                                    session.flush();
                                    return null;
                                });
                                if (resultProcessor != null)
                                    resultProcessor.processResult(obj, changedWrapper);
                            }
                        } catch (ProcessingDatagramObjectException e) {
                            serviceLogger.log(Level.ERROR, "Error with object with GUID " + guid + ". The result of processing in the " + className + " reactor: " + e.getMessage());
                            if (errorLog.length() > 0)
                                errorLog.append('\n');
                            errorLog.append("Error with object with GUID ").append(guid).append(": ").append(e.getMessage());
                            if (resultProcessor != null)
                                resultProcessor.exception(obj, e);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            // в любом случае, нужно очистить сессию, т.к. processDatagramObject мог изменить сущности, которые мы не хотим сохранять
                            getSession().clear();
                        }
                    } else if (resultProcessor != null)
                        resultProcessor.processResult(pair.getX(), pair.getY());
                }
            }
        }

        if(hasEmptyGuid)
        {
            if (errorLog.length() > 0)
                errorLog.append('\n');
            errorLog.append("There are one or more elements without GUID!");
        }

        return processedDatagrams;
    }

    @Override
    public SyncOperatorResult delete(List<IDatagramObject> datagramElements, NsiPackage nsiPackage) {
        IOperationProcess operationProcess = (reactors, nsiEntityTypeToDatagramListMap, warnLog, errorLog) -> {
            List<IDatagramObject> result = new ArrayList<>();
            for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : reactors) {
                String name = reactor.getCatalogType();
                List<IDatagramObject> items = nsiEntityTypeToDatagramListMap.get(name);

                List<IDatagramObject> r = null;
                IOperationReactor operationReactor = reactor.getDeleteOperationReactor();
                if(operationReactor != null) {
                    try {
                        // если в preDelete возникло исключение, то прерываем исполнение этого реактора
                        Map<String, Object> reactorCache = reactor.preDelete(items);
                        r = operationReactor.process(datagramElements, nsiPackage, null, reactorCache, warnLog, errorLog);
                    } catch (ProcessingException e) {
                        if (errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append(e.getMessage());
                    }
                } else
                    r = delete(reactor, items, nsiPackage, warnLog, errorLog);

                if(r != null && !r.isEmpty())
                    result.addAll(r);
            }
            return result;
        };

        StringBuilder warnLog = new StringBuilder();
        StringBuilder errorLog = new StringBuilder();
        List<IDatagramObject> result = processDatagrams(datagramElements, operationProcess, warnLog, errorLog);
        return new SyncOperatorResult(warnLog.length() > 0 ? warnLog.toString() : null, errorLog.length() > 0 ? errorLog.toString() : null, result);
    }

    private static <T extends IEntity, V extends IDatagramObject> List<IDatagramObject> delete(INsiEntityReactor<T, V> reactor, List<IDatagramObject> datagramElements, NsiPackage nsiPackage, StringBuilder warnLog, StringBuilder errorLog) {
        ILogger serviceLogger = NsiRequestHandlerService.instance().getLogger();
        Class<T> c = reactor.getEntityClass();
        String className = c.getSimpleName();

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        List<Long> entityIds = NsiSyncManager.instance().dao().getEntityIdsByDatagramObjects(datagramElements);

        // Не указан ни один идентификатор сущности НСИ на удаление, либо запрос предполагает тотальное удаление всех элементов из ОБ
        if (null == entityIds)
        {
            serviceLogger.log(Level.ERROR, "---------- There are no any GUID specified for the delete operation. Whole entity set could not be deleted ----------");
            if(errorLog.length() > 0)
                errorLog.append('\n');
            errorLog.append("Could not delete whole entity set. Please, specify entity ids list to delete.");
            return null;
        }

        // Инициализируем мапы объектов и связанных с ними GUID'ов
        Map<String, Pair<T, NsiEntity>> idToEntityMap = NsiSyncManager.instance().dao().getEntityWithNsiIdsMap(c, entityIds);
        serviceLogger.log(Level.INFO, "---------- Whole elements list (" + idToEntityMap.values().size() + " items) for " + className + " catalog was received from database ----------");

        // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов)
        List<String> nonDeletableGuids = NsiSyncManager.instance().dao().getNonDeletableGuids(c, entityIds);

        // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
        if (!nonDeletableGuids.isEmpty())
        {
            serviceLogger.log(Level.ERROR, "---------- Deleting requested list (" + datagramElements.size() + " items) for " + className + " catalog was failed. ----------");
            if(errorLog.length() > 0)
                errorLog.append('\n');
            errorLog.append("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. " +
                                    "Please, check dependencies for the objects ").append(CommonBaseStringUtil.joinNotEmpty(nonDeletableGuids, ", ")).append('.');
            return null;
        }

        // Подготавливаем список объектов реально обработанных и GUID'ов, не найденных в системе
        List<String> strangeGuids = new ArrayList<>();
        List<IDatagramObject> deletedObjectsList = new ArrayList<>();
        for (IDatagramObject datagramObject : datagramElements)
        {
            String guid = StringUtils.trimToNull(datagramObject.getID());
            Pair<T, NsiEntity> pair = idToEntityMap.get(guid);
            if (null != pair)
            {
                deletedObjectsList.add(reactor.createDatagramObject(pair.getY().getGuid()));
                NsiSyncManager.instance().dao().createEntityLog(pair.getY(), reactor.getCatalogType(), nsiPackage, null);
            }
            else
                strangeGuids.add(datagramObject.getID());
        }

        // Удаляем удалябельные объекты и связанные с ними идентификаторы НСИ
        NsiSyncManager.instance().dao().deleteEntities(c, entityIds);

        // Выдаём предупреждение, если среди переданных GUID'ов имеются не найденные в ОБ. Удалять нечего.
        if (!strangeGuids.isEmpty())
        {
            serviceLogger.log(Level.WARN, "---------- Deleting requested list (" + deletedObjectsList.size() + " items) for " + className + " catalog was executed with warnings ----------");
            if(warnLog.length() > 0)
                warnLog.append('\n');
            warnLog.append("One, or more entities were not found at OB by given GUID's, so they were ignored: ").append(CommonBaseStringUtil.joinNotEmpty(strangeGuids, ", "));
        }

        serviceLogger.log(Level.INFO, "---------- Deleting requested elements list (" + deletedObjectsList.size() + " items) for " + className + " catalog was executed successfully ----------");

        return deletedObjectsList;
    }

    @Override
    public SyncOperatorResult retrieve(List<IDatagramObject> datagramElements, NsiPackage nsiPackage)
    {
        Map<String, Object> commonCache = new HashMap<>();
        commonCache.put(PROCESSED, new HashSet<Long>());

        IOperationProcess operationProcess = (reactors, nsiEntityTypeToDatagramListMap, warnLog, errorLog) -> {
            List<IDatagramObject> result = new ArrayList<>();

            for (INsiEntityReactor<? extends IEntity, ? extends IDatagramObject> reactor : reactors) {
                String name = reactor.getCatalogType();
                List<IDatagramObject> items = nsiEntityTypeToDatagramListMap.get(name);

                List<IDatagramObject> r = null;
                IOperationReactor operationReactor = reactor.getRetrieveOperationReactor();
                if(operationReactor != null) {
                    try {
                        // если в preRetrieve возникло исключение, то прерываем исполнение этого реактора
                        Map<String, Object> reactorCache = reactor.preRetrieve(items);
                        r = operationReactor.process(datagramElements, nsiPackage, commonCache, reactorCache, warnLog, errorLog);
                    } catch (ProcessingException e) {
                        if (errorLog.length() > 0)
                            errorLog.append('\n');
                        errorLog.append(e.getMessage());
                    }
                } else
                    r = retrieve(reactor, items, commonCache, nsiPackage);

                if(r != null && !r.isEmpty())
                    result.addAll(r);
            }
            return result;
        };

        StringBuilder warnLog = new StringBuilder();
        StringBuilder errorLog = new StringBuilder();
        List<IDatagramObject> result = processDatagrams(datagramElements, operationProcess, warnLog, errorLog);
        return new SyncOperatorResult(warnLog.length() > 0 ? warnLog.toString() : null, errorLog.length() > 0 ? errorLog.toString() : null, result);
    }

    private <T extends IEntity, V extends IDatagramObject> List<IDatagramObject> retrieve(INsiEntityReactor<T, V> reactor, List<IDatagramObject> datagramElements, Map<String, Object> commonCache, NsiPackage nsiPackage) {
        ILogger serviceLogger = NsiRequestHandlerService.instance().getLogger();
        Class<T> clazz = reactor.getEntityClass();
        String className = clazz.getSimpleName();

        // Подготавливаем список идентификаторов для инициализации мапов
        List<Long> entityIds = NsiSyncManager.instance().dao().getEntityIdsByDatagramObjects(datagramElements);

        // Мапа сущностей и NsiEntity, которые соответствуют переданным объектам
        Map<String, Pair<T, NsiEntity>> idToEntityMap = NsiSyncManager.instance().dao().getEntityWithNsiIdsMap(clazz, entityIds);
        List<Pair<T, NsiEntity>> pairsToRetrieve = getPairsToRetrieve(datagramElements, idToEntityMap);

        List<IDatagramObject> result = new ArrayList<>(pairsToRetrieve.size());
        @SuppressWarnings("unchecked")
        Set<Long> alreadyPreparedItemIdList = (Set<Long>) commonCache.get(PROCESSED);

        // Подготавливаем список сущностей НСИ для возврата в ответе
        // удаленные объекты не возвращаем
        pairsToRetrieve.stream().filter(pair -> null != pair.getX() && !alreadyPreparedItemIdList.contains(pair.getX().getId()) && (pair.getY() == null || !pair.getY().isDeleted())).forEach(pair -> {
            // Если идентификатора НСИ у нас ещё нет в списке элементов, то генерим его налету
            if (null == pair.getY()) {
                NsiEntity e = NsiSyncManager.instance().dao().saveOrUpdateNsiEntity(pair.getX(), null, null, reactor.getCatalogType());
                pair.setY(e);
            }
            ResultListWrapper objects = reactor.createDatagramObject(pair.getX(), pair.getY(), commonCache);
            NsiSyncManager.instance().dao().createEntityLog(pair.getY(), reactor.getCatalogType(), nsiPackage, objects.getWarning());
            result.addAll(objects.getList());
            alreadyPreparedItemIdList.add(pair.getX().getId());
        });

        serviceLogger.log(Level.INFO, "---------- Whole requested catalog elements list (" + result.size() + " items) for " + className + " catalog was sent back ----------");
        return result;
    }

    /**
     * Подготавливаем список пар сущность/идентификатор НСИ для последующего формирования списка сущностей НСИ
     * @param items
     * @param idToEntityMap
     * @return
     */
    protected <T extends IEntity> List<Pair<T, NsiEntity>> getPairsToRetrieve(List<IDatagramObject> items, Map<String, Pair<T, NsiEntity>> idToEntityMap)
    {
        List<Pair<T, NsiEntity>> pairsToRetrieve = new ArrayList<>();
        for (IDatagramObject item : items)
        {
            if (null == item.getID())
            {
                // Дабы случайно не вернуть номинально удалённую сущность НСИ, но оставленную в системе
                pairsToRetrieve.addAll(idToEntityMap.values().stream().filter(pair -> null == pair.getY() || !pair.getY().isDeleted()).collect(Collectors.toList()));
                return pairsToRetrieve;
            }

            Pair<T, NsiEntity> pair = idToEntityMap.get(item.getID());
            if (null != pair)
                pairsToRetrieve.add(pair);
        }
        return pairsToRetrieve;
    }
}
