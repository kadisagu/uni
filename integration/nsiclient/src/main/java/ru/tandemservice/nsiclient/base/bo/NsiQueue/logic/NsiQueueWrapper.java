/* $Id:$ */
package ru.tandemservice.nsiclient.base.bo.NsiQueue.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.nsiclient.entity.NsiQueue;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 19.01.2017
 */
public class NsiQueueWrapper extends DataWrapper
{
    public static final String QUEUE = "queue";
    public static final String CATALOG_CODES = "catalogCodes";
    public static final String SUB_SYSTEM_CODES = "subSystemCodes";
    public static final String START_DATE = "startDate";

    private NsiQueue _queue;
    private String _catalogCodes;
    private String _subSystemCodes;
    private boolean _current = false;
    private Date _startDate = null;

    public NsiQueueWrapper(NsiQueue queue, String catalogCodes, String subSystemCodes)
    {
        super(queue.getId(), "");
        _queue = queue;
        _catalogCodes = catalogCodes;
        _subSystemCodes = subSystemCodes;
    }

    public NsiQueue getQueue()
    {
        return _queue;
    }

    public String getCatalogCodes()
    {
        return _catalogCodes;
    }

    public String getSubSystemCodes()
    {
        return _subSystemCodes;
    }

    public boolean isCurrent()
    {
        return _current;
    }

    public void setCurrent(Date startDate)
    {
        _current = true;
        _startDate = startDate;
    }

    public Date getStartDate()
    {
        return _startDate;
    }
}