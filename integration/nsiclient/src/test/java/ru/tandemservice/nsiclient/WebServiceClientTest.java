/* $Id: WebServiceClientTest.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiclient;

import org.junit.Test;

import javax.xml.soap.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author Andrey Nikonov
 * @since 20.03.2015
 */
public class WebServiceClientTest
{
    /*@Test
    public void clientServiceTest() throws Exception
    {
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId("OB");
        header.setDestinationId("NSI");
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        ServiceRequestType.Datagram datagram = new ServiceRequestType.Datagram();

        IXDatagram xDatagram = NsiUtils.createXDatagram();
        xDatagram.getEntityList().add(createRelDegreeType());

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));
        MessageElement datagramOut = new org.apache.axis.message.SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);

        ServiceSoapImplService service = new ServiceSoapImplService(new URL("http://localhost:8080/services/NSIService?WSDL"), ServiceSoapImplService.SERVICE);
        service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).delete(request);
    }*/

    @Test
    public void clientServiceTest1() throws Exception
    {
       /* ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId("OB");
        header.setDestinationId("NSI");
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        ServiceRequestType.Datagram datagram = new ServiceRequestType.Datagram();

        XDatagram xDatagram = new ru.tandemservice.nsiclient.datagram.ObjectFactory().createXDatagram();
        xDatagram.getEntityList().add(createRelDegreeType());

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiUtils.toXml(xDatagram));
        MessageElement datagramOut = new org.apache.axis.message.SOAPBodyElement(inStream);
        datagram.getContent().add(datagramOut);
        request.setDatagram(datagram);


        byte[] b = NsiUtils.toXml(xDatagram);
        String text = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "<soap:Body>\n" +
                "<insertRequest xmlns=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service\"><routingHeader><messageId>74b36290-c622-3d9b-8172-96ea35985274</messageId><sourceId>OB</sourceId><destinationId>NSI</destinationId></routingHeader><datagram>" +
                new String(b) +
                "</datagram></insertRequest></soap:Body></soap:Envelope>";
        SOAPMessage request1 = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createMessage(new MimeHeaders(), new ByteArrayInputStream(text.getBytes()));
        SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = sfc.createConnection();
        URL endpoint = new URL("http://localhost:8090/services/NSIService?WSDL");
        SOAPMessage response = connection.call(request1, endpoint);
        System.out.println(response.getContentDescription());*/

        //ServiceSoapImplService service = new ServiceSoapImplService(new URL("http://localhost:8090/services/NSIService?WSDL"), ServiceSoapImplService.SERVICE);
        //service.getPort(ServiceSoapImplService.ServiceSoapPort, ServiceSoap.class).insert(request);

        //SOAPMessage request = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createMessage(new MimeHeaders(), new ByteArrayInputStream(text.getBytes()));

        //request.
        //NsiUtils.toXml(request);
//        NsiRequestHandlerService.instance().process(request, NsiUtils.OPERATION_TYPE_INSERT);
//        SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
//        SOAPConnection connection = sfc.createConnection();
//        SOAPMessage request1 = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createMessage(new MimeHeaders(), new ByteArrayInputStream(NsiUtils.toXml(request)));
//        URL endpoint = new URL("http://localhost:8090/services/NSIService?WSDL");
//        SOAPMessage response = connection.call(request1, endpoint);
//        System.out.println(response.getContentDescription());
    }

    @Test
    public void clientServiceRowTestSoap11_1() throws Exception
    {
        String text = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "<soap:Body>\n" +
                "<insertRequest xmlns=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service\"><routingHeader><messageId>74b36290-c622-3d9b-8172-96ea35985278</messageId><sourceId>OB</sourceId><destinationId>NSI</destinationId></routingHeader><datagram><x-datagram:x-datagram xmlns:x-datagram=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0\" xmlns=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0\">\n" +
                "    <RelDegree>\n" +
                "        <ID>cc5d937a-3202-3c23-8a77-xxxxxxxxxx2X</ID>\n" +
                "        <RelDegreeID>77</RelDegreeID>\n" +
                "        <RelDegreeName></RelDegreeName>\n" +
                "    </RelDegree>\n" +
                "</x-datagram:x-datagram></datagram></insertRequest></soap:Body></soap:Envelope>";
        SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = sfc.createConnection();
        SOAPMessage request = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createMessage(new MimeHeaders(), new ByteArrayInputStream(text.getBytes()));
        URL endpoint = new URL("http://localhost:8090/services/NSIService?WSDL");
        SOAPMessage response = connection.call(request, endpoint);
        System.out.println(response.getContentDescription());
    }

    @Test
    public void clientServiceRowTestSoap11_2() throws Exception
    {
        URL u = new URL("http://localhost:8090/services/NSIService?WSDL");
        URLConnection uc = u.openConnection();
        HttpURLConnection connection = (HttpURLConnection) uc;

        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("SOAPAction", "http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service/insert");
        connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");

        OutputStream out = connection.getOutputStream();
        Writer wout = new OutputStreamWriter(out);
        wout.write("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "<soap:Body>\n" +
                "<insertRequest xmlns=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Service\"><routingHeader><messageId>74b36290-c622-3d9b-8172-96ea35985274</messageId><sourceId>OB</sourceId><destinationId>NSI</destinationId></routingHeader><datagram><x-datagram:x-datagram xmlns:x-datagram=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0\" xmlns=\"http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0\">\n" +
                "    <RelDegree>\n" +
                "        <ID>cc5d937a-3202-3c23-8a77-xxxxxxxxxxxX</ID>\n" +
                "        <RelDegreeID>77</RelDegreeID>\n" +
                "        <RelDegreeName></RelDegreeName>\n" +
                "    </RelDegree>\n" +
                "</x-datagram:x-datagram></datagram></insertRequest></soap:Body></soap:Envelope>");

        wout.flush();
        wout.close();

        InputStream in = connection.getInputStream();
        int c;
        while ((c = in.read()) != -1)
            System.out.write(c);
        in.close();
    }

    public static void main(String[] args) {
        new WorkerThread().start();
        try {
            Thread.sleep(7500);
        } catch (InterruptedException e) {}
        System.out.println("Main Thread ending") ;
    }

    static class WorkerThread extends Thread {

        public WorkerThread() {
            setDaemon(false) ;   // When false, (i.e. when it's a user thread),
            // the Worker thread continues to run.
            // When true, (i.e. when it's a daemon thread),
            // the Worker thread terminates when the main
            // thread terminates.
        }

        public void run() {
            int count=0 ;
            while (true) {
                System.out.println("Hello from Worker "+count++) ;
                try {
                    sleep(5000);
                } catch (InterruptedException e) {}
            }
        }
    }
}