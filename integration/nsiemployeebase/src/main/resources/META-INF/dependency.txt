org.tandemframework core 1.6.18-SNAPSHOT 
org.tandemframework db-support 1.6.18-SNAPSHOT 
org.tandemframework hib-support 1.6.18-SNAPSHOT 
org.tandemframework rtf 1.6.18-SNAPSHOT 
org.tandemframework caf 1.6.18-SNAPSHOT 
org.tandemframework tap-support 1.6.18-SNAPSHOT 
org.tandemframework common 1.6.18-SNAPSHOT 
org.tandemframework sec 1.6.18-SNAPSHOT 
org.tandemframework.shared sandbox 1.11.4-SH-SNAPSHOT Sandbox
org.tandemframework.shared commonbase 1.11.4-SH-SNAPSHOT Общие прикладные функции
org.tandemframework.shared fias 1.11.4-SH-SNAPSHOT Реестр адресов
org.tandemframework.shared organization 1.11.4-SH-SNAPSHOT Оргструктура
org.tandemframework.shared person 1.11.4-SH-SNAPSHOT Персоны
org.tandemframework.shared employeebase 1.11.4-SH-SNAPSHOT Кадровый реестр
org.tandemframework.shared ctr 1.11.4-SH-SNAPSHOT Базовые договоры
org.tandemframework.shared cxfws 1.11.4-SH-SNAPSHOT Веб-сервисы CXF
ru.tandemservice.nsiclient nsiclient 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (базовые механизмы для обмена с НСИ)
ru.tandemservice.nsiclient nsifias 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Реестр адресов)
ru.tandemservice.nsiclient nsiperson 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Персоны)
ru.tandemservice.nsiclient nsiorganization 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Оргструктура)
ru.tandemservice.nsiclient nsiemployeebase 2.11.4-UNI-SNAPSHOT Интеграция с НСИ (модуль Кадровый реестр)
