/* $Id$ */
package ru.tandemservice.nsiemployeebase.base.ext.NsiCatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.common.base.entity.IPersistentEmployeePost;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import ru.tandemservice.nsiclient.base.bo.NsiCatalog.NsiCatalogManager;

/**
 * @author Andrey Avetisov
 * @since 22.09.2015
 */
@Configuration
public class NsiCatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private NsiCatalogManager _catalogManager;

    @Bean
    public ItemListExtension<String[]> hidedExtPoint()
    {
        return itemListExtension(_catalogManager.hidedExtPoint())
                .add(EmployeePost.ENTITY_NAME, new String[]{EmployeePost.P_LOCAL_ROLE_CONTEXT_TITLE, EmployeePost.L_PRINCIPAL})
                .create();
    }


    @Bean
    public ItemListExtension<IDefaultComboDataSourceHandler> handlerExtPoint()
    {
        return itemListExtension(_catalogManager.handlerExtPoint())
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> orderableExtPoint()
    {
        return itemListExtension(_catalogManager.orderableExtPoint())
                .add(EmployeePost.ENTITY_NAME, false)
                .add(IPersistentEmployeePost.class.getName(), false)
                .add(Employee.ENTITY_NAME, false)
                .add(OrgUnitTypePostRelation.ENTITY_NAME, false)
                .create();
    }

    @Bean
    public ItemListExtension<Boolean> filterableExtPoint()
    {
        return itemListExtension(_catalogManager.filterableExtPoint())
                .add(EmployeePost.ENTITY_NAME, false)
                .add(IPersistentEmployeePost.class.getName(), false)
                .add(Employee.ENTITY_NAME, false)
                .add(OrgUnitTypePostRelation.ENTITY_NAME, false)
                .create();
    }
}