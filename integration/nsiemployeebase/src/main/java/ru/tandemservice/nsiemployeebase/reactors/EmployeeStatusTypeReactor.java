/* $Id: EmployeeStatusTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiemployeebase.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EmployeeStatusType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 06.07.2015
 */
public class EmployeeStatusTypeReactor extends BaseEntityReactor<EmployeePostStatus, EmployeeStatusType>
{
    @Override
    public Class<EmployeePostStatus> getEntityClass()
    {
        return EmployeePostStatus.class;
    }

    @Override
    public EmployeeStatusType createDatagramObject(String guid)
    {
        EmployeeStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeStatusType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EmployeePostStatus entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EmployeeStatusType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeStatusType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEmployeeStatusID(entity.getCode());
        datagramObject.setEmployeeStatusName(entity.getTitle());
        datagramObject.setEmployeeStatusNameShort(entity.getShortTitle());
        datagramObject.setEmployeeStatusActive(NsiUtils.formatBoolean(entity.isActive()));
        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EmployeePostStatus findEmployeePostStatus(String code, String title, String shortTitle)
    {
        if (code != null)
        {
            List<EmployeePostStatus> list = findEntity(eq(property(ENTITY_ALIAS, EmployeePostStatus.code()), value(code)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (title != null)
        {
            List<EmployeePostStatus> list = findEntity(eq(property(ENTITY_ALIAS, EmployeePostStatus.title()), value(title)));
            if (list.size() == 1)
                return list.get(0);
        }
        if (shortTitle != null)
        {
            List<EmployeePostStatus> list = findEntity(eq(property(ENTITY_ALIAS, EmployeePostStatus.shortTitle()), value(shortTitle)));
            if (list.size() == 1)
                return list.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EmployeePostStatus> processDatagramObject(EmployeeStatusType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        String code = StringUtils.trimToNull(datagramObject.getEmployeeStatusID());
        String title = StringUtils.trimToNull(datagramObject.getEmployeeStatusName());
        String shortTitle = StringUtils.trimToNull(datagramObject.getEmployeeStatusNameShort());
        Boolean employeeStatusActive = NsiUtils.parseBoolean(datagramObject.getEmployeeStatusActive());

        boolean isNew = false;
        boolean isChanged = false;
        EmployeePostStatus entity = null;

        CoreCollectionUtils.Pair<EmployeePostStatus, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findEmployeePostStatus(code, title, shortTitle);
        if (entity == null)
        {
            entity = new EmployeePostStatus();
            isNew = true;
            isChanged = true;
        }

        //title
        if (title != null && !title.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(title);
        }
        if (entity.getTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EmployeeStatusType object without title");
        }

        //short title
        if (shortTitle != null && !shortTitle.equals(entity.getShortTitle()))
        {
            isChanged = true;
            entity.setShortTitle(shortTitle);
        }
        if (entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EmployeeStatusType object without short title");
        }

        if (employeeStatusActive!=null && !employeeStatusActive.equals(entity.isActive()))
        {
            isChanged = true;
            entity.setActive(employeeStatusActive);
        }
        if (isNew && employeeStatusActive == null)
        {
            throw new ProcessingDatagramObjectException("EmployeeStatusType object without employeeStatusActive");
        }

        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setActual(true);
            entity.setPermitLogin(true);
            entity.setCode(code);
            if (code == null)
                throw new ProcessingDatagramObjectException("EmployeeStatusType object without EmployeePostStatusID");
            if (!DataAccessServices.dao().isUnique(EmployeePostStatus.class, entity, EmployeePostStatus.P_CODE))
                throw new ProcessingDatagramObjectException("EmployeePostStatusID is not unique!");

            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(EmployeePostStatus.class, EmployeePostStatus.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[EmployeePostStatusID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EmployeePostStatus.P_ACTUAL);
        fields.add(EmployeePostStatus.P_PERMIT_LOGIN);
        return  fields;
    }
}
