/* $Id: EmployeeTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiemployeebase.reactors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.query.IExpression;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;
import ru.tandemservice.nsiclient.utils.ReactorHolder;
import ru.tandemservice.nsiperson.reactors.HumanTypeReactor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class EmployeeTypeReactor extends BaseEntityReactor<EmployeePost, EmployeeType>
{
    private static final String HUMAN = "HumanID";
    private static final String DEPARTMENT = "EmployeeDepartmentID";
    private static final String STATUS = "EmployeeStatusID";
    private static final String POST = "PostID";
    private static final String POST_TYPE = "PostTypeID";
    private static final String GRADE = "EmployeeGradeID";
    private static final String RELATION = "relation";
    private static final String EMPLOYEE = "employee";

    @Override
    public Class<EmployeePost> getEntityClass()
    {
        return EmployeePost.class;
    }

    @Override
    public EmployeeType createDatagramObject(String guid)
    {
        EmployeeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EmployeePost entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EmployeeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        Employee employee = entity.getEmployee();
        datagramObject.setEmployeeID(employee.getEmployeeCode());
        datagramObject.setEmployeeContractKind(null);
        datagramObject.setEmployeeEmploymentKind(null);
        datagramObject.setEmployeePhoneNumber(entity.getPhone());
        datagramObject.setEmployeeMobilePhoneNumber(entity.getMobilePhone());
        datagramObject.setEmployeeRate(null);
        datagramObject.setEmployeeEmploymentDate(NsiUtils.formatDate(entity.getPostDate()));
        datagramObject.setEmployeeDischargeDate(NsiUtils.formatDate(entity.getDismissalDate()));
        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<HumanType> humanResult = createDatagramObject(HumanType.class, employee.getPerson(), commonCache, warning);
        EmployeeType.HumanID humanID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeTypeHumanID();
        humanID.setHuman(humanResult.getObject());
        datagramObject.setHumanID(humanID);
        if (humanResult.getList() != null)

            result.addAll(humanResult.getList());


        if (entity.getEtksLevels()!= null)
        {
            ProcessedDatagramObject<GradeType> gradeResult = createDatagramObject(GradeType.class, entity.getEtksLevels(), commonCache, warning);
            EmployeeType.EmployeeGradeID gradeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeTypeEmployeeGradeID();
            gradeID.setGrade(gradeResult.getObject());
            datagramObject.setEmployeeGradeID(gradeID);
            if(gradeResult.getList() != null)
                result.addAll(gradeResult.getList());
        }

        if (entity.getOrgUnit() != null)
        {
            ProcessedDatagramObject<DepartmentType> departmentResult = createDatagramObject(DepartmentType.class, entity.getOrgUnit(), commonCache, warning);
            EmployeeType.EmployeeDepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeTypeEmployeeDepartmentID();
            departmentID.setDepartment(departmentResult.getObject());
            datagramObject.setEmployeeDepartmentID(departmentID);
            if (departmentResult.getList() != null)
                result.addAll(departmentResult.getList());
        }

        if (entity.getPostRelation() != null && entity.getPostRelation().getPostBoundedWithQGandQL() != null)
        {
            ProcessedDatagramObject<PostType> postResult = createDatagramObject(PostType.class, entity.getPostRelation().getPostBoundedWithQGandQL(), commonCache, warning);
            EmployeeType.PostID postID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeTypePostID();
            postID.setPost(postResult.getObject());
            datagramObject.setPostID(postID);
            if (postResult.getList() != null)
                result.addAll(postResult.getList());
        }

        if (entity.getPostType() != null)
        {
            ProcessedDatagramObject<EmploymentTypeType> employmentTypResult = createDatagramObject(EmploymentTypeType.class, entity.getPostType(), commonCache, warning);
            EmployeeType.EmploymentTypeID employmentTypeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmploymentTypeID();
            employmentTypeID.setEmploymentType(employmentTypResult.getObject());
            datagramObject.setEmploymentTypeID(employmentTypeID);
            if (employmentTypResult.getList() != null)
                result.addAll(employmentTypResult.getList());
        }

        @SuppressWarnings("unchecked")
        INsiEntityReactor<EmployeePostStatus, EmployeeStatusType> reactor = (INsiEntityReactor<EmployeePostStatus, EmployeeStatusType>) ReactorHolder.getReactorMap().get(EmployeeStatusType.class.getSimpleName());
        if(reactor != null) {
            ProcessedDatagramObject<EmployeeStatusType> statusType = createDatagramObject(EmployeeStatusType.class, entity.getPostStatus(), commonCache, warning);
            EmployeeType.EmployeeStatusID statusID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeTypeEmployeeStatusID();
            statusID.setStatus(statusType.getObject());
            datagramObject.setEmployeeStatusID(statusID);
            if (statusType.getList() != null)
                result.addAll(statusType.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(EmployeeType datagramObject, IUnknownDatagramsCollector collector) {
        EmployeeType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        if(humanType != null)
            collector.check(humanType.getID(), HumanType.class);

        EmployeeType.EmployeeGradeID gradeID = datagramObject.getEmployeeGradeID();
        GradeType gradeType = gradeID == null ? null : gradeID.getGrade();
        if(gradeType != null)
            collector.check(gradeType.getID(), GradeType.class);

        EmployeeType.EmployeeDepartmentID departmentID = datagramObject.getEmployeeDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if(departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);

        EmployeeType.PostID postID = datagramObject.getPostID();
        PostType postType = postID == null ? null : postID.getPost();
        if(postType != null)
            collector.check(postType.getID(), PostType.class);

        @SuppressWarnings("unchecked")
        INsiEntityReactor<EmployeePostStatus, EmployeeStatusType> reactor = (INsiEntityReactor<EmployeePostStatus, EmployeeStatusType>) ReactorHolder.getReactorMap().get(EmployeeStatusType.class.getSimpleName());
        if(reactor != null) {
            EmployeeType.EmployeeStatusID statusID = datagramObject.getEmployeeStatusID();
            EmployeeStatusType statusType = statusID == null ? null : statusID.getStatus();
            if (statusType != null)
                collector.check(statusType.getID(), reactor);
        }
    }

    protected EmployeePost findEmployeePost(Long orgUnitId, Long personId, Long postId)
    {
        if(orgUnitId == null || personId == null)
            return null;
        List<IExpression> expressions = new ArrayList<>();
        expressions.add(eq(property(ENTITY_ALIAS, EmployeePost.orgUnit().id()), value(orgUnitId)));
        expressions.add(eq(property(ENTITY_ALIAS, EmployeePost.employee().person().id()), value(personId)));
        if(postId != null)
            expressions.add(eq(property(ENTITY_ALIAS, EmployeePost.postRelation().postBoundedWithQGandQL().id()), value(postId)));
        List<EmployeePost> list = findEntity(and(expressions.toArray(new IDQLExpression[expressions.size()])));
        if(list.size() == 1)
            return list.get(0);
        return null;
    }

    protected OrgUnitTypePostRelation findOrgUnitTypePostRelation(OrgUnitType orgUnitType, PostBoundedWithQGandQL post)
    {
        if(orgUnitType == null || post == null)
            return null;
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnitTypePostRelation.class, "e")
                .column(property("e"))
                .where(eq(property("e", OrgUnitTypePostRelation.orgUnitType().id()), value(orgUnitType.getId())))
                .where(eq(property("e", OrgUnitTypePostRelation.postBoundedWithQGandQL().id()), value(post.getId())));
        List<OrgUnitTypePostRelation> list = DataAccessServices.dao().getList(builder);
        if(list.size() == 1)
            return list.get(0);
        return null;
    }

    @Override
    public ChangedWrapper<EmployeePost> processDatagramObject(EmployeeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        String code = StringUtils.trimToNull(datagramObject.getEmployeeID());
        String contractKind = StringUtils.trimToNull(datagramObject.getEmployeeContractKind());
        String employmentKind = StringUtils.trimToNull(datagramObject.getEmployeeEmploymentKind());
        String phoneNumber = StringUtils.trimToNull(datagramObject.getEmployeePhoneNumber());
        String mobilePhoneNumber = StringUtils.trimToNull(datagramObject.getEmployeeMobilePhoneNumber());
        String rate = StringUtils.trimToNull(datagramObject.getEmployeeRate());
        Date employmentDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getEmployeeEmploymentDate()), "EmployeeEmploymentDate");
        Date dischargeDate = NsiUtils.parseDate(StringUtils.trimToNull(datagramObject.getEmployeeDischargeDate()), "EmployeeDischargeDat");

        // OrganizationID init
        EmployeeType.OrganizationID organizationID = datagramObject.getOrganizationID();
        // HumanID init
        EmployeeType.HumanID humanID = datagramObject.getHumanID();
        HumanType humanType = humanID == null ? null : humanID.getHuman();
        Person person = getOrCreate(HumanType.class, params, commonCache, humanType, HUMAN, null, warning);
        // EmployeeGradeID init
        EmployeeType.EmployeeGradeID gradeID = datagramObject.getEmployeeGradeID();
        GradeType gradeType = gradeID == null ? null : gradeID.getGrade();
        EtksLevels levels = getOrCreate(GradeType.class, params, commonCache, gradeType, GRADE, null, warning);
        // EmployeeDepartmentID init
        EmployeeType.EmployeeDepartmentID departmentID = datagramObject.getEmployeeDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit orgUnit = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT, null, warning);
        // EmployeeStatusID init
        EmployeeType.EmployeeStatusID employeeStatusID = datagramObject.getEmployeeStatusID();
        EmployeeStatusType employeeStatusType = employeeStatusID == null ? null : employeeStatusID.getStatus();
        EmployeePostStatus postStatus = getOrCreate(EmployeeStatusType.class, params, commonCache, employeeStatusType, STATUS, null, warning);
        // EmploymentTypeID init
        EmployeeType.EmploymentTypeID employmentTypeID = datagramObject.getEmploymentTypeID();
        EmploymentTypeType employmentTypeType = employmentTypeID == null ? null : employmentTypeID.getEmploymentType();
        org.tandemframework.shared.employeebase.catalog.entity.PostType uniPostType = getOrCreate(EmploymentTypeType.class, params, commonCache, employmentTypeType, POST_TYPE, null, warning);
        // PostID init
        EmployeeType.PostID postID = datagramObject.getPostID();
        PostType postType = postID == null ? null : postID.getPost();
        Map<String, Object> postParams = new HashMap<>(1);
        postParams.put(PostTypeReactor.ETKS_LEVELS, levels);
        PostBoundedWithQGandQL post = getOrCreate(PostType.class, params, commonCache, postType, POST, postParams, warning);

        boolean isNew = false;
        boolean isChanged = false;
        EmployeePost entity = null;
        OrgUnitTypePostRelation postRelation;
        boolean postRelationChanged = false;
        CoreCollectionUtils.Pair<EmployeePost, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findEmployeePost(orgUnit == null ? null : orgUnit.getId(), person == null ? null : person.getId(), post == null ? null : post.getId());
        if(entity == null)
        {
            entity = new EmployeePost();
            isNew = true;
            isChanged = true;

            Employee employee = new Employee();
            entity.setEmployee(employee);
            params.put(EMPLOYEE, employee);

            entity.setPostStatus(SharedBaseDao.instance.get().getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE));

            postRelation = new OrgUnitTypePostRelation();
            postRelationChanged = true;
        } else
            postRelation = entity.getPostRelation();

        // EmployeeID set
        if (null == code && isNew)
            throw new ProcessingDatagramObjectException("EmployeeType object without EmployeeID!");
        if(code != null && !code.equals(entity.getEmployee().getEmployeeCode()))
        {
            isChanged = true;
            entity.getEmployee().setEmployeeCode(code);
            params.put(EMPLOYEE, entity.getEmployee());
        }
        // EmployeePhoneNumber set
        if(phoneNumber != null && !phoneNumber.equals(entity.getPhone()))
        {
            isChanged = true;
            entity.setPhone(phoneNumber);
        }
        // EmployeeMobilePhoneNumber set
        if(mobilePhoneNumber != null && !mobilePhoneNumber.equals(entity.getMobilePhone()))
        {
            isChanged = true;
            entity.setMobilePhone(mobilePhoneNumber);
        }
        // EmployeeEmploymentDate set
        if(employmentDate == null && isNew)
            employmentDate = new Date();
        if(employmentDate != null && !employmentDate.equals(entity.getPostDate()))
        {
            isChanged = true;
            entity.setPostDate(employmentDate);
        }
        // EmployeeDischargeDate set
        if(dischargeDate != null && !dischargeDate.equals(entity.getDismissalDate()))
        {
            isChanged = true;
            entity.setDismissalDate(dischargeDate);
            entity.setPostStatus(SharedBaseDao.instance.get().getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_FIRED));
        }
        // HumanID set
        if(person != null && !person.equals(entity.getEmployee().getPerson()))
        {
            isChanged = true;
            entity.getEmployee().setPerson(person);
        }
        if(entity.getEmployee().getPerson() == null)
            throw new ProcessingDatagramObjectException("EmployeeType object without HumanID!");

        if (levels != null && !levels.equals(entity.getEtksLevels()))
        {
            isChanged = true;
            entity.setEtksLevels(levels);
        }

        // PostID set
        if(post != null && !post.equals(postRelation.getPostBoundedWithQGandQL()))
        {
            postRelationChanged = true;
        }
        // EmployeeDepartmentID set
        if(orgUnit != null && !orgUnit.equals(entity.getOrgUnit()))
        {
            postRelationChanged = true;
            entity.setOrgUnit(orgUnit);
        }

        // EmployeeDepartmentID set
        if(uniPostType != null && !uniPostType.equals(entity.getPostType()))
        {
            isChanged = true;
            entity.setPostType(uniPostType);
        }
        if (entity.getPostType() == null)
        {
            throw new ProcessingDatagramObjectException("EmployeeType object without EmploymentTypeID!");
        }

        if(entity.getOrgUnit() == null)
            throw new ProcessingDatagramObjectException("EmployeeType object without EmployeeDepartmentID!");

        if(postRelationChanged)
        {
            postRelation = findOrgUnitTypePostRelation(orgUnit.getOrgUnitType(), post);
            if(postRelation == null) {
                postRelation = new OrgUnitTypePostRelation();
                postRelation.setMultiPost(true);
                postRelation.setHeaderPost(false);
                postRelation.setOrgUnitType(orgUnit.getOrgUnitType());
                postRelation.setPostBoundedWithQGandQL(post);
            }
            params.put(RELATION, true);
            entity.setPostRelation(postRelation);
        }

        if(postRelation.getPostBoundedWithQGandQL() == null)
            throw new ProcessingDatagramObjectException("EmployeeType object without PostID!");


        // EmployeeStatusID set
        if(postStatus != null && !postStatus.equals(entity.getPostStatus()))
        {
            isChanged = true;
            entity.setPostStatus(postStatus);
        }

        if(entity.getPostStatus() == null)
        {
            throw new ProcessingDatagramObjectException("EmployeeType object without EmployeeStatusID!");
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<EmployeePost> w, EmployeeType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        EmployeePost entity = w.getResult();
        if(params != null) {
            saveChildEntity(HumanType.class, session, params, HUMAN, nsiPackage);
            saveChildEntity(EmployeeStatusType.class, session, params, STATUS, nsiPackage);

            Employee employee = (Employee) params.get(EMPLOYEE);
            if (employee != null)
                session.saveOrUpdate(employee);

            saveChildEntity(GradeType.class, session, params, GRADE, nsiPackage);
            saveChildEntity(PostType.class, session, params, POST, nsiPackage);

            if (params.containsKey(RELATION))
                session.saveOrUpdate(entity.getPostRelation());

            saveChildEntity(DepartmentType.class, session, params, DEPARTMENT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }



    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(EmployeePost.P_ORDER_NUMBER);
        fields.add(EmployeePost.P_ORDER_DATE);
        fields.add(EmployeePost.P_INNER_PHONE);
        fields.add(EmployeePost.P_HAS_INTERCITY_COMM_LINE);
        fields.add(EmployeePost.L_INSTEAD_OF_EMPLOYEE_POST);
        fields.add(EmployeePost.P_HOUSE_UNIT);
        fields.add(EmployeePost.P_AUDIENCE);
        fields.add(EmployeePost.L_WORK_WEEK_DURATION);
        fields.add(EmployeePost.L_WEEK_WORK_LOAD);
        fields.add(EmployeePost.L_RAISING_COEFFICIENT);
        fields.add(EmployeePost.P_FREELANCE);
        fields.add(EmployeePost.P_HAS_MOBILE_COMM_ACCESS);
        fields.add(EmployeePost.P_MAIN_JOB);
        fields.add(EmployeePost.P_SALARY);
        fields.add(EmployeePost.L_COMPETITION_TYPE);
        fields.add(EmployeePost.P_WEEKEND_OUT_DATE);
        fields.add(EmployeePost.P_HOURLY_PAID);
        fields.add(EmployeePost.L_EMPLOYEE);
        fields.add(EmployeePost.L_POST_RELATION);
        return  fields;
    }
}
