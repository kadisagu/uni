/* $Id: PostTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiemployeebase.reactors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.datagram.PostType;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.NsiUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Nikonov
 * @since 06.05.2015
 */
public class PostTypeReactor extends BaseEntityReactor<PostBoundedWithQGandQL, PostType>
{
    private static final String TITLE_MAP = "postTitleLowerToPostMap";
    private static final String QUALIFICATION_LEVEL = "QualificationLevel";
    private static final String QUALIFICATION_GROUP = "ProfQualificationGroup";
    private static final String POST = "post";

    public static final String ETKS_LEVELS = "e";

    @Override
    public Class<PostBoundedWithQGandQL> getEntityClass()
    {
        return PostBoundedWithQGandQL.class;
    }

    @Override
    public PostType createDatagramObject(String guid)
    {
        PostType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPostType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PostBoundedWithQGandQL entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        PostType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createPostType();
        datagramObject.setID(nsiEntity.getGuid());

        datagramObject.setPostID(entity.getUserCode());
        datagramObject.setPostName(entity.getTitle());
        datagramObject.setPostAUP(NsiUtils.formatBoolean(entity.getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.MANAGEMENT_STAFF)));
        datagramObject.setPostPPS(NsiUtils.formatBoolean(entity.getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.EDU_STAFF)));
        datagramObject.setPostKind(entity.getPost().getEmployeeType().getTitle());
        datagramObject.setPostPosition(entity.getPost().getCode());
        datagramObject.setPostVUS(null);
        datagramObject.setPostMilitaryDiscountCategory(null);
        datagramObject.setPostOkpdtr(null);
        datagramObject.setGradeOfPost(null);

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected PostBoundedWithQGandQL findPostBoundedWithQGandQL(String title)
    {
        if (title != null)
        {
            List<PostBoundedWithQGandQL> postBoundedWithQGandQLList = findEntity(eqValue(property(ENTITY_ALIAS, PostBoundedWithQGandQL.title()), title));
            if (postBoundedWithQGandQLList.size() == 1)
                return postBoundedWithQGandQLList.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<PostBoundedWithQGandQL> processDatagramObject(PostType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();

        // PostID init
        String code = StringUtils.trimToNull(datagramObject.getPostID());
        // PostName init
        String name = StringUtils.trimToNull(datagramObject.getPostName());
        // PostAUP init
        Boolean aup = NsiUtils.parseBoolean(datagramObject.getPostAUP());
        // PostPPS init
        Boolean pps = NsiUtils.parseBoolean(datagramObject.getPostPPS());
        String postCode = StringUtils.trimToNull(datagramObject.getPostPosition());
        String postKind = StringUtils.trimToNull(datagramObject.getPostKind());

        boolean isNew = false;
        boolean isChanged = false;
        PostBoundedWithQGandQL entity = null;
        CoreCollectionUtils.Pair<PostBoundedWithQGandQL, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if (pair != null)
            entity = pair.getX();
        if (entity == null)
            entity = findPostBoundedWithQGandQL(name);
        if (entity == null)
        {
            entity = new PostBoundedWithQGandQL();
            isNew = true;
            isChanged = true;
            entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(PostBoundedWithQGandQL.class));
            QualificationLevel level = getQualificationLevel(params);
            entity.setQualificationLevel(level);

            ProfQualificationGroup group = getProfQualificationGroup(params);
            entity.setProfQualificationGroup(group);
        }

        if (code != null && !code.equals(entity.getUserCode()))
        {
            entity.setUserCode(code);
        }
        else if (code == null || !NsiSyncManager.instance().reactorDAO().isPropertiesUnique(PostBoundedWithQGandQL.class, entity, PostBoundedWithQGandQL.P_USER_CODE))
        {
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(PostBoundedWithQGandQL.class, PostBoundedWithQGandQL.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }

        // PostName set
        if (null == name && isNew)
            throw new ProcessingDatagramObjectException("PostType object without PostName!");
        if (name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);

            @SuppressWarnings("unchecked")
            Post post = ((Map<String, Post>) reactorCache.get(TITLE_MAP)).get(name.toLowerCase());
            if (null == post)
            {
                post = getPost(postCode, aup != null ? aup : false, pps != null ? pps : false, params);
            }
            entity.setPost(post);
        }

        EtksLevels levels = additionalParams == null ? null : (EtksLevels) additionalParams.get(ETKS_LEVELS);
        if (levels != null && !levels.equals(entity.getEtksLevels()))
        {
            isChanged = true;
            entity.setEtksLevels(levels);
        }
        if (entity.getPost() == null)
        {
            Post post = new Post();
            EmployeeType employeeType = DataAccessServices.dao().getList(EmployeeType.class, EmployeeType.title(), postKind).get(0);
            post.setCode(postCode);
            post.setEmployeeType(employeeType);
            post.setTitle(name);
            params.put(POST, post);
            entity.setPost(post);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, null);
    }

    private static QualificationLevel getQualificationLevel(Map<String, Object> params)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(QualificationLevel.class, "q")
                .column("q").top(1);

        List<QualificationLevel> list = DataAccessServices.dao().getList(dql);
        if (!CollectionUtils.isEmpty(list))
            return list.get(0);
        QualificationLevel qualificationLevel = new QualificationLevel();
        qualificationLevel.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(QualificationLevel.class));
        qualificationLevel.setTitle("Квалификационный уровень");
        qualificationLevel.setShortTitle("tq");
        qualificationLevel.setUserCode("testCode");
        params.put(QUALIFICATION_LEVEL, qualificationLevel);
        return qualificationLevel;
    }

    private static ProfQualificationGroup getProfQualificationGroup(Map<String, Object> params)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ProfQualificationGroup.class, "q")
                .column("q").top(1);

        List<ProfQualificationGroup> list = DataAccessServices.dao().getList(dql);
        if (!CollectionUtils.isEmpty(list))
            return list.get(0);
        ProfQualificationGroup profQualificationGroup = new ProfQualificationGroup();
        profQualificationGroup.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ProfQualificationGroup.class));
        profQualificationGroup.setUserCode("testCode");
        profQualificationGroup.setTitle("Профессионально-квалификационная группа");
        profQualificationGroup.setShortTitle("tqg");
        profQualificationGroup.setUserCode("testCode");
        params.put(QUALIFICATION_GROUP, profQualificationGroup);
        return profQualificationGroup;
    }

    private static Post getPost(String postCode, boolean isAup, boolean isPps, Map<String, Object> params)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Post.class, "p")
                .column("p").top(1)
                .where(eqValue(property("p", Post.P_CODE), postCode));

        List<Post> list = DataAccessServices.dao().getList(dql);
        if (!CollectionUtils.isEmpty(list))
            return list.get(0);
        else
            return null;
    }

    @Override
    public void preUpdate(List<IDatagramObject> items, Map<String, Object> reactorCache) throws ProcessingException
    {
        super.preUpdate(items, reactorCache);
        @SuppressWarnings("unchecked")
        Map<String, Post> postTitleLowerToPostMap = (Map<String, Post>) reactorCache.get(TITLE_MAP);
        if (postTitleLowerToPostMap == null)
        {
            postTitleLowerToPostMap = new HashMap<>();
            List<Post> postList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Post.class, "p").column(property("p")));
            for (Post post : postList)
                postTitleLowerToPostMap.put(post.getTitle().toLowerCase(), post);
            reactorCache.put(TITLE_MAP, postTitleLowerToPostMap);
        }
    }

    @Override
    public void save(Session session, ChangedWrapper<PostBoundedWithQGandQL> w, PostType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            ProfQualificationGroup profQualificationGroup = (ProfQualificationGroup) params.get(QUALIFICATION_GROUP);
            if(profQualificationGroup != null)
                session.saveOrUpdate(profQualificationGroup);

            QualificationLevel qualificationLevel = (QualificationLevel) params.get(QUALIFICATION_LEVEL);
            if(qualificationLevel != null)
                session.saveOrUpdate(qualificationLevel);

            Post post = (Post) params.get(POST);
            if(post != null)
                session.saveOrUpdate(post);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<IDatagramObject> createTestObjects() {
        PostType entity = new PostType();
        entity.setID("cc5d937a-3202-3c23-8a11-postTypexxxX");
        entity.setPostID("99999");
        entity.setPostName("PostNameTest");
        entity.setPostAUP("1");
        entity.setPostPPS("0");
        List<IDatagramObject> datagrams = new ArrayList<>(1);
        datagrams.add(entity);
        return datagrams;
    }

    public static PostType getFirstPostTypeForEmbedded(boolean onlyGuid)
    {
        PostType entity = new PostType();
        entity.setID("cc5d937a-3202-3c23-8a11-postType1xxX");
        if (!onlyGuid)
        {
            entity.setPostID("8888881");
            entity.setPostName("PostNameTest1");
            entity.setPostAUP("1");
        }
        return entity;
    }

    public static PostType getSecondPostTypeForEmbedded(boolean onlyGuid)
    {
        PostType entity = new PostType();
        entity.setID("cc5d937a-3202-3c23-8a11-postType2xxX");
        if (!onlyGuid)
        {
            entity.setPostID("88888882");
            entity.setPostName("PostNameTest2");
            entity.setPostAUP("1");
        }
        return entity;
    }

    @Override
    public boolean isCorrect(PostBoundedWithQGandQL entity, PostType datagramObject, List<IDatagramObject> retrievedDatagram) {
        return ((datagramObject.getPostAUP().equals("1") && entity.getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.MANAGEMENT_STAFF))
                || (!datagramObject.getPostAUP().equals("1") && !entity.getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.MANAGEMENT_STAFF)))
                && ((datagramObject.getPostPPS().equals("1") && entity.getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.EDU_STAFF))
                || (!datagramObject.getPostPPS().equals("1") && !entity.getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.EDU_STAFF)))
                && datagramObject.getPostName().equals(entity.getTitle());
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(PostBoundedWithQGandQL.P_CODE);
        fields.add(PostBoundedWithQGandQL.L_SCIENCE_DEGREE_TYPE);
        fields.add(PostBoundedWithQGandQL.L_SCIENCE_STATUS_TYPE);
        fields.add(PostBoundedWithQGandQL.P_ORDER);

        return  fields;
    }
}
