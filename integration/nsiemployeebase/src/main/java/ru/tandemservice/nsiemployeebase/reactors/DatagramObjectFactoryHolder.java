/* $Id$ */
package ru.tandemservice.nsiemployeebase.reactors;

/**
 * @author Dmitry Seleznev
 * @since 14.07.2015
 */
public class DatagramObjectFactoryHolder
{
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
}