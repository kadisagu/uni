/* $Id$ */
package ru.tandemservice.nsiemployeebase.reactors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.EmploymentTypeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Avetisov
 * @since 22.01.2016
 */
public class EmploymentTypeTypeReactor  extends BaseEntityReactor<PostType, EmploymentTypeType>
{
    @Override
    public Class<PostType> getEntityClass()
    {
        return PostType.class;
    }

    @Override
    public EmploymentTypeType createDatagramObject(String guid)
    {
        EmploymentTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmploymentTypeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(PostType entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EmploymentTypeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmploymentTypeType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEmploymentTypeID(entity.getCode());
        datagramObject.setEmploymentTypeName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected PostType findPostType(String code, String name)
    {
        if(code != null)
        {
            List<PostType> postTypeList = findEntity(eq(property(ENTITY_ALIAS, PostType.code()), value(code)));
            if(!CollectionUtils.isEmpty(postTypeList) || postTypeList.size()==1)
                return postTypeList.get(0);
        }

        if(name != null)
        {
            List<PostType> postTypeList = findEntity(eq(property(ENTITY_ALIAS, PostType.title()), value(name)));
            if(!CollectionUtils.isEmpty(postTypeList) || postTypeList.size()==1)
                return postTypeList.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<PostType> processDatagramObject(@NotNull EmploymentTypeType datagramObject, Map<String, Object> commonCache,
                                                          Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;

        // GradeID init
        String code = StringUtils.trimToNull(datagramObject.getEmploymentTypeID());
        // GradeName init
        String name = StringUtils.trimToNull(datagramObject.getEmploymentTypeName());

        boolean isNew = false;
        boolean isChanged = false;
        PostType entity = null;

        CoreCollectionUtils.Pair<PostType, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findPostType(code, name);
        if(entity == null)
        {
            entity = new PostType();
            isNew = true;
            isChanged = true;
        }

        if(name != null && !name.equals(entity.getTitle()))
        {
            isChanged = true;
            entity.setTitle(name);
        }
        if(name!=null && entity.getShortTitle() == null)
        {
            isChanged = true;
            entity.setShortTitle(name);
        }

        if (entity.getTitle() == null || entity.getShortTitle() == null)
        {
            throw new ProcessingDatagramObjectException("EmploymentType object without name!");
        }


        //code
        StringBuilder warning = new StringBuilder();
        if (isNew)
        {
            entity.setCode(code);
            if (code == null || !DataAccessServices.dao().isUnique(PostType.class, entity, PostType.P_CODE))
            {
                if (code == null)
                    warning.append("[EmploymentType object without getEmploymentTypeID!");
                else
                    warning.append("getEmploymentTypeID is not unique!");

                code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(PostType.class);
                warning.append(" getEmploymentTypeID was set to ").append(code).append(".]");
                entity.setCode(code);
            }
            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(PostType.class, PostType.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        }
        else if (code != null && !code.equals(entity.getCode()))
            warning.append("[getEmploymentTypeID is immutable!]");

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(PostType.P_SHORT_TITLE);
        return  fields;
    }
}
