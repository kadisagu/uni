/* $Id: GradeTypeReactor.java 180 2015-07-14 13:49:04Z dseleznev $ */
package ru.tandemservice.nsiemployeebase.reactors;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import ru.tandemservice.nsiclient.base.bo.NsiSync.NsiSyncManager;
import ru.tandemservice.nsiclient.datagram.GradeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.ChangedWrapper;
import ru.tandemservice.nsiclient.reactor.support.ProcessingDatagramObjectException;
import ru.tandemservice.nsiclient.reactor.support.ResultListWrapper;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class GradeTypeReactor extends BaseEntityReactor<EtksLevels, GradeType>
{
    @Override
    public Class<EtksLevels> getEntityClass() {
        return EtksLevels.class;
    }

    @Override
    public GradeType createDatagramObject(String guid) {
        GradeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createGradeType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(EtksLevels entity, NsiEntity nsiEntity, Map<String, Object> commonCache) {
        GradeType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createGradeType();
        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setGradeID(getUserCodeForNSIChecked(entity.getUserCode()));
        datagramObject.setGradeName(entity.getTitle());

        return new ResultListWrapper(null, Collections.singletonList((IDatagramObject) datagramObject));
    }

    protected EtksLevels findEtksLevels(String code, String name)
    {
        if(code != null)
        {
            List<EtksLevels> levelsList = findEntity(eq(property(ENTITY_ALIAS, EtksLevels.userCode()), value(code)));
            if(levelsList != null && !levelsList.isEmpty())
                return levelsList.get(0);
        }
        if(name != null)
        {
            List<EtksLevels> levelsList = findEntity(eq(property(ENTITY_ALIAS, EtksLevels.title()), value(name)));
            if(levelsList != null && !levelsList.isEmpty())
                return levelsList.get(0);
        }
        return null;
    }

    @Override
    public ChangedWrapper<EtksLevels> processDatagramObject(GradeType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        assert datagramObject.getID() != null;

        // GradeID init
        String code = StringUtils.trimToNull(datagramObject.getGradeID());
        // GradeName init
        String name = StringUtils.trimToNull(datagramObject.getGradeName());

        boolean isNew = false;
        boolean isChanged = false;
        EtksLevels entity = null;
        CoreCollectionUtils.Pair<EtksLevels, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);
        if(pair != null)
            entity = pair.getX();
        if(entity == null)
            entity = findEtksLevels(code, name);
        if(entity == null)
        {
            entity = new EtksLevels();
            isNew = true;
            isChanged = true;
            entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EtksLevels.class));
        }

        // GradeID set
        StringBuilder warning = new StringBuilder();
        // Поле code immutable, поэтому оставляем как есть
        if(isNew)
        {
            if (code == null)
                throw new ProcessingDatagramObjectException("GradeType object without GradeID!");

            @SuppressWarnings("unchecked")
            String codeChecked = getUserCodeChecked((Map<Class<? extends IEntity>, Set<String>>) reactorCache.get(USED_CODES), code);

            if (!NsiSyncManager.instance().reactorDAO().isPropertyUnique(EtksLevels.class, EtksLevels.P_CODE, entity.getId(), codeChecked))
                throw new ProcessingDatagramObjectException("GradeID is not unique!");

            entity.setCode(codeChecked);

            String userCode = NsiSyncManager.instance().reactorDAO().findUniqueProperty(EtksLevels.class, EtksLevels.P_USER_CODE, entity.getId(), "nsi");
            entity.setUserCode(userCode);
        } else if(code != null && !code.equals(entity.getCode()))
            warning.append("[GradeID is immutable!]");

        // GradeName set
        if (null == name && isNew)
            throw new ProcessingDatagramObjectException("GradeType object without GradeName!");
        if(name != null)
        {
            if(!NsiSyncManager.instance().reactorDAO().isPropertyUnique(EtksLevels.class, EtksLevels.P_TITLE, entity.getId(), name))
                throw new ProcessingDatagramObjectException("GradeName is not unique!");
            if(!name.equals(entity.getTitle())) {
                isChanged = true;
                entity.setTitle(name);
            }
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, null, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public List<IDatagramObject> createTestObjects() {
        GradeType entity = new GradeType();
        entity.setID("cc5d937a-3202-3c23-8a11-gradeTypexxX");
        entity.setGradeID("99999");
        entity.setGradeName("gradeTypeNameTest");
        List<IDatagramObject> datagrams = new ArrayList<>();
        datagrams.add(entity);
        return datagrams;
    }

    @Override
    public GradeType getEmbedded(boolean onlyGuid)
    {
        GradeType entity = new GradeType();
        entity.setID("cc5d937a-3202-3c23-8a11-gradeTypexxX");
        if (!onlyGuid)
        {
            entity.setGradeID("99999");
            entity.setGradeName("gradeTypeNameTest");
        }
        return entity;
    }

    @Override
    public boolean isCorrect(EtksLevels entity, GradeType datagramObject, List<IDatagramObject> retrievedDatagram) {
        return datagramObject.getGradeName().equals(entity.getTitle());
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();
        fields.add(EtksLevels.P_CODE);
        fields.add(EtksLevels.P_SALARY);
        return  fields;
    }
}
