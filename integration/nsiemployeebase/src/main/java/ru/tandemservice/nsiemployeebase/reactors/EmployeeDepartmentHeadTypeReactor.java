/* $Id: EmployeeTypeReactor.java 221 2015-07-16 07:14:19Z dseleznev $ */
package ru.tandemservice.nsiemployeebase.reactors;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.DepartmentType;
import ru.tandemservice.nsiclient.datagram.EmployeeDepartmentHeadType;
import ru.tandemservice.nsiclient.datagram.EmployeeType;
import ru.tandemservice.nsiclient.datagram.IDatagramObject;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.entity.NsiPackage;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.INsiEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.nsiclient.utils.ReactorHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 05.05.2015
 */
public class EmployeeDepartmentHeadTypeReactor extends BaseEntityReactor<OrgUnit, EmployeeDepartmentHeadType>
{
    private static final String DEPARTMENT = "DepartmentID";
    private static final String EMPLOYEE = "EmployeeID";

    @Override
    public Class<OrgUnit> getEntityClass()
    {
        return OrgUnit.class;
    }

    @Override
    public EmployeeDepartmentHeadType createDatagramObject(String guid)
    {
        EmployeeDepartmentHeadType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeDepartmentHeadType();
        if (null != guid) datagramObject.setID(guid);
        return datagramObject;
    }

    @Override
    public ResultListWrapper createDatagramObject(OrgUnit entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EmployeeDepartmentHeadType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeDepartmentHeadType();

        ConditionalDatagramArrayList<IDatagramObject> result = new ConditionalDatagramArrayList<>(2);
        result.addWithoutConditions(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());

        EmployeePost employee = (EmployeePost) entity.getHead();
        StringBuilder warning = new StringBuilder();

        ProcessedDatagramObject<DepartmentType> departmentResult = createDatagramObject(DepartmentType.class, entity, commonCache, warning);
        EmployeeDepartmentHeadType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeDepartmentHeadTypeDepartmentID();
        departmentID.setDepartment(departmentResult.getObject());
        datagramObject.setDepartmentID(departmentID);

        if (departmentResult.getList() != null)
            result.addAll(departmentResult.getList());

        if (employee != null)
        {
            ProcessedDatagramObject<EmployeeType> employeeResult = createDatagramObject(EmployeeType.class, employee, commonCache, warning);
            EmployeeDepartmentHeadType.EmployeeID employeeID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEmployeeDepartmentHeadTypeEmployeeID();
            employeeID.setEmployee(employeeResult.getObject());
            datagramObject.setEmployeeID(employeeID);

            if (employeeResult.getList() != null)
                result.addAll(employeeResult.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public void collectUnknownDatagrams(EmployeeDepartmentHeadType datagramObject, IUnknownDatagramsCollector collector) {
        EmployeeDepartmentHeadType.EmployeeID employeeID = datagramObject.getEmployeeID();
        EmployeeType employeeType = employeeID == null ? null : employeeID.getEmployee();
        if (employeeType != null)
            collector.check(employeeType.getID(), EmployeeType.class);

        EmployeeDepartmentHeadType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if (departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);
    }

    @Override
    public ChangedWrapper<OrgUnit> processDatagramObject(EmployeeDepartmentHeadType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException
    {
        assert datagramObject.getID() != null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder warning = new StringBuilder();

        // EmployeeID init
        EmployeeDepartmentHeadType.EmployeeID employeeID = datagramObject.getEmployeeID();
        EmployeeType employeeType = employeeID == null ? null : employeeID.getEmployee();
        EmployeePost post = getOrCreate(EmployeeType.class, params, commonCache, employeeType, EMPLOYEE, null, warning);
        // DepartmentID init
        EmployeeDepartmentHeadType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        OrgUnit entity = getOrCreate(DepartmentType.class, params, commonCache, departmentType, DEPARTMENT, null, warning);

        boolean isNew = false;
        boolean isChanged = false;
        CoreCollectionUtils.Pair<OrgUnit, NsiEntity> pair = getFromCache(datagramObject.getID(), reactorCache);

        // EmployeeID set
        if (post != null && !post.equals(entity.getHead()))
        {
            isChanged = true;
            isNew = true;
            entity.setHead(post);
        }

        return new ChangedWrapper<>(isNew, isChanged, pair != null ? pair.getY() : null, entity, params, warning.length() > 0 ? warning.toString() : null);
    }

    @Override
    public void save(Session session, ChangedWrapper<OrgUnit> w, EmployeeDepartmentHeadType datagramObject, NsiPackage nsiPackage)
    {
        Map<String, Object> params = w.getParams();
        if (params != null)
        {
            saveChildEntity(EmployeeType.class, session, params, EMPLOYEE, nsiPackage);
            saveChildEntity(DepartmentType.class, session, params, DEPARTMENT, nsiPackage);
        }

        super.save(session, w, datagramObject, nsiPackage);
    }

    @Override
    public List<String> getNotValidatedFieldList()
    {
        List<String> fields = super.getNotValidatedFieldList();

        fields.add(OrgUnit.P_CORPS);
        fields.add(OrgUnit.L_POSTAL_ADDRESS);
        fields.add(OrgUnit.P_PHONE);
        fields.add(OrgUnit.P_INTERNAL_PHONE);
        fields.add(OrgUnit.P_ACCUSATIVE_CASE_TITLE);
        fields.add(OrgUnit.P_DATIVE_CASE_TITLE);
        fields.add(OrgUnit.P_GENITIVE_CASE_TITLE);
        fields.add(OrgUnit.P_NOMINATIVE_CASE_TITLE);
        fields.add(OrgUnit.P_PREPOSITIONAL_CASE_TITLE);
        fields.add(OrgUnit.P_DESCRIPTION);
        fields.add(OrgUnit.P_SHORT_TITLE);
        fields.add(OrgUnit.L_LEGAL_ADDRESS);
        fields.add(OrgUnit.P_RANK);
        fields.add(OrgUnit.P_WEBPAGE);
        fields.add(OrgUnit.P_TAG);
        fields.add(OrgUnit.P_FAX);
        fields.add(OrgUnit.P_TERRITORIAL_FULL_TITLE);
        fields.add(OrgUnit.P_TERRITORIAL_TITLE);
        fields.add(OrgUnit.P_TERRITORIAL_SHORT_TITLE);
        fields.add(OrgUnit.P_EMAIL);
        fields.add(OrgUnit.P_ARCHIVING_DATE);
        fields.add(OrgUnit.P_PERSONNEL_DEPARTMENT);
        fields.add(OrgUnit.P_FULL_TITLE);
        fields.add(OrgUnit.P_AUDIENCE);
        fields.add(OrgUnit.L_ADDRESS);
        fields.add(OrgUnit.P_ACCOUNTING_CODE);
        fields.add(OrgUnit.L_ORG_UNIT_TYPE);
        return  fields;
    }

}
