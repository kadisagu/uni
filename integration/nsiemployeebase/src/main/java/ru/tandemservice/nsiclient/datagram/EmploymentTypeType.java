/* $Id$ */


package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
/**
 * @author Andrey Avetisov
 * @since 22.01.2016
 */

/**
 * Элемент справочника Тип назначения на должность
 *
 * <p>Java class for EmploymentTypeType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EmploymentTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EmploymentTypeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmploymentTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmploymentTypeType", propOrder = {

})
public class EmploymentTypeType  implements IDatagramObject
{
    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EmploymentTypeID")
    protected String employmentTypeID;
    @XmlElement(name = "EmploymentTypeName")
    protected String employmentTypeName;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the employmentTypeID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmploymentTypeID() {
        return employmentTypeID;
    }

    /**
     * Sets the value of the employmentTypeD property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmploymentTypeID(String value) {
        this.employmentTypeID = value;
    }

    /**
     * Gets the value of the employmentTypeName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEmploymentTypeName() {
        return employmentTypeName;
    }

    /**
     * Sets the value of the employmentTypeName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEmploymentTypeName(String value) {
        this.employmentTypeName = value;
    }

    /**
     * Gets the value of the oid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     *
     * @return
     *     possible object is
     *     {@link java.math.BigInteger }
     *
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     *
     * @param value
     *     allowed object is
     *     {@link java.math.BigInteger }
     *
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     *
     * <p>
     * the map is keyed by the name of the attribute and
     * the value is the string value of the attribute.
     *
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     *
     *
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }
}
