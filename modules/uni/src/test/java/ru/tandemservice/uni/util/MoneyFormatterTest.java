package ru.tandemservice.uni.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;

/**
 * @author avedernikov
 * @since 01.07.2016.
 */
public class MoneyFormatterTest
{
	@DataProvider(name = "RuMoneyFormatter")
	Object[][] dataRuMoneyFormatter()
	{
		return new Object[][] {
				{0L, "0 руб. 00 коп."}, {1000L, "1 руб. 00 коп."}, {1L, "0 руб. 00 коп."}, {2048L, "2 руб. 05 коп."},
				{1024_120L, "1024 руб. 12 коп."}, {31_415_926L, "31415 руб. 93 коп."}, {-27_000L, "-27 руб. 00 коп."}, {-654_321L, "-654 руб. 32 коп."},
				{-123L, "-0 руб. 12 коп."}, {-1L, "0 руб. 00 коп."}, {-4L, "0 руб. 00 коп."}, {-5L, "-0 руб. 01 коп."}
		};
	}

	@Test(description = "форматтер сумм в рублях", dataProvider = "RuMoneyFormatter")
	public void testRuMoneyFormatter(long amount, String expectedString)
	{
		String formattedAmount = MoneyFormatter.ruMoneyFormatter(3).format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы в рублях.");
	}

	@DataProvider(name = "moneyFormatterAnotherCurrencies")
	Object[][] dataMoneyFormatterAnotherCurrencies()
	{
		MoneyFormatter byrMoneyFormatter = MoneyFormatter.byrMoneyFormatter(3);
		MoneyFormatter usdMoneyFormatter = MoneyFormatter.usdMoneyFormatter(3);
		MoneyFormatter eurMoneyFormatter = MoneyFormatter.eurMoneyFormatter(3);
		MoneyFormatter uahMoneyFormatter = MoneyFormatter.uahMoneyFormatter(3);
		MoneyFormatter chyMoneyFormatter = MoneyFormatter.cnyMoneyFormatter(3);
		return new Object[][] {
				{byrMoneyFormatter, 0L, "0 руб. 00 коп."}, {byrMoneyFormatter, 1024L, "1 руб. 02 коп."}, {byrMoneyFormatter, 123_456_789L, "123456 руб. 79 коп."},
				{byrMoneyFormatter, -3L, "0 руб. 00 коп."}, {byrMoneyFormatter, -987_654_321L, "-987654 руб. 32 коп."}, {byrMoneyFormatter, -241L, "-0 руб. 24 коп."},
				{usdMoneyFormatter, 0L, "0 дол. 00 ц."}, {usdMoneyFormatter, 1024L, "1 дол. 02 ц."}, {usdMoneyFormatter, 123_456_789L, "123456 дол. 79 ц."},
				{usdMoneyFormatter, -3L, "0 дол. 00 ц."}, {usdMoneyFormatter, -987_654_321L, "-987654 дол. 32 ц."}, {usdMoneyFormatter, -241L, "-0 дол. 24 ц."},
				{eurMoneyFormatter, 0L, "0 евр. 00 ц."}, {eurMoneyFormatter, 1024L, "1 евр. 02 ц."}, {eurMoneyFormatter, 123_456_789L, "123456 евр. 79 ц."},
				{eurMoneyFormatter, -3L, "0 евр. 00 ц."}, {eurMoneyFormatter, -987_654_321L, "-987654 евр. 32 ц."}, {eurMoneyFormatter, -241L, "-0 евр. 24 ц."},
				{uahMoneyFormatter, 0L, "0 грн. 00 коп."}, {uahMoneyFormatter, 1024L, "1 грн. 02 коп."}, {uahMoneyFormatter, 123_456_789L, "123456 грн. 79 коп."},
				{uahMoneyFormatter, -3L, "0 грн. 00 коп."}, {uahMoneyFormatter, -987_654_321L, "-987654 грн. 32 коп."}, {uahMoneyFormatter, -241L, "-0 грн. 24 коп."},
				{chyMoneyFormatter, 0L, "0 юань 00 фэн."}, {chyMoneyFormatter, 1024L, "1 юань 02 фэн."}, {chyMoneyFormatter, 123_456_789L, "123456 юань 79 фэн."},
				{chyMoneyFormatter, -3L, "0 юань 00 фэн."}, {chyMoneyFormatter, -987_654_321L, "-987654 юань 32 фэн."}, {chyMoneyFormatter, -241L, "-0 юань 24 фэн."}
		};
	}

	@Test(description = "форматтер суммы в белорусских рублях, долларах США, евро, гривнах и юанях", dataProvider = "moneyFormatterAnotherCurrencies")
	public void testMoneyFormatterAnotherCurrencies(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы.");
	}



	@DataProvider(name = "moneyFormatterDecimalDigits")
	Object[][] dataMoneyFormatterDecimalDigits()
	{
		return new Object[][] {
				{MoneyFormatter.ruMoneyFormatter(0), 1245L, "1245 руб. 00 коп."}, {MoneyFormatter.byrMoneyFormatter(1), 1245L, "124 руб. 50 коп."},
				{MoneyFormatter.usdMoneyFormatter(2), 1245L, "12 дол. 45 ц."}, {MoneyFormatter.eurMoneyFormatter(3), 1245L, "1 евр. 25 ц."},
				{MoneyFormatter.uahMoneyFormatter(4), 1245L, "0 грн. 12 коп."}, {MoneyFormatter.cnyMoneyFormatter(8), 1245L, "0 юань 00 фэн."},
				{MoneyFormatter.byrMoneyFormatter(0), 0L, "0 руб. 00 коп."}, {MoneyFormatter.uahMoneyFormatter(1), 0L, "0 грн. 00 коп."},
				{MoneyFormatter.cnyMoneyFormatter(2), 0L, "0 юань 00 фэн."}
		};
	}

	@Test(description = "число цифр после запятой в форматтере суммы", dataProvider = "moneyFormatterDecimalDigits")
	public void testMoneyFormatterDecimalDigits(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы, число цифр после запятой.");
	}



	@DataProvider(name = "moneyFormatterShortTitles")
	Object[][] dataMoneyFormatterShortTitles()
	{
		return new Object[][] {
				{ MoneyFormatter.ruMoneyFormatter(2), 314_15L, "314 руб. 15 коп."}, { MoneyFormatter.ruMoneyFormatter(2).shortTitles(false), 314_15L, "314 рублей 15 копеек"},
				{ MoneyFormatter.byrMoneyFormatter(4).shortTitles(true), 27_1848L, "27 руб. 18 коп."}, { MoneyFormatter.byrMoneyFormatter(4).shortTitles(false), 27_1848L, "27 рублей 18 копеек"},
				{ MoneyFormatter.usdMoneyFormatter(0), 1000_000L, "1000000 дол. 00 ц."}, { MoneyFormatter.usdMoneyFormatter(0).shortTitles(false), 1000_000L, "1000000 долларов 00 центов"},
				{ MoneyFormatter.eurMoneyFormatter(2), 42_42L, "42 евр. 42 ц."}, { MoneyFormatter.eurMoneyFormatter(2).shortTitles(false), 42_42L, "42 евро 42 цента"},
				{ MoneyFormatter.uahMoneyFormatter(3), 321_456L, "321 грн. 46 коп."}, { MoneyFormatter.uahMoneyFormatter(3).shortTitles(false), 321_456L, "321 гривна 46 копеек"},
				{ MoneyFormatter.cnyMoneyFormatter(0).shortTitles(true), 1L, "1 юань 00 фэн."}, { MoneyFormatter.cnyMoneyFormatter(0).shortTitles(false), 1L, "1 юань 00 фэней"},
		};
	}

	@Test(description = "форматтер суммы, сокращенные названия валют", dataProvider = "moneyFormatterShortTitles")
	public void testMoneyFormatterShortTitles(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы, сокращенные названия валют.");
	}



	@DataProvider(name = "moneyFormatterSpelledAmount")
	Object[][] dataMoneyFormatterSpelledAmount()
	{
		MoneyFormatter ruMoneyFormatter  = MoneyFormatter.ruMoneyFormatter(3).spellNumbers(true).shortTitles(false);
		MoneyFormatter byrMoneyFormatter = MoneyFormatter.byrMoneyFormatter(3).spellNumbers(true);
		MoneyFormatter usdMoneyFormatter = MoneyFormatter.usdMoneyFormatter(3).spellNumbers(true);
		MoneyFormatter eurMoneyFormatter = MoneyFormatter.eurMoneyFormatter(3).spellNumbers(true);
		MoneyFormatter uahMoneyFormatter = MoneyFormatter.uahMoneyFormatter(3).spellNumbers(true).shortTitles(false);
		MoneyFormatter chyMoneyFormatter = MoneyFormatter.cnyMoneyFormatter(3).spellNumbers(true);
		return new Object[][] {
				{ ruMoneyFormatter, 0L, "ноль рублей 00 копеек"}, { ruMoneyFormatter, 1000L, "один рубль 00 копеек"},
				{ ruMoneyFormatter, 1_234_567L, "одна тысяча двести тридцать четыре рубля 57 копеек"},
				{ ruMoneyFormatter, -2_000_303_404_210L, "минус два миллиарда триста три тысячи четыреста четыре рубля 21 копейка"},
				{ ruMoneyFormatter, -8_500L, "минус восемь рублей 50 копеек"}, { ruMoneyFormatter, -249_000_000L, "минус двести сорок девять тысяч рублей 00 копеек"},
				{ usdMoneyFormatter, 400_000L, "четыреста дол. 00 ц."}, { usdMoneyFormatter, 18_000_140L, "восемнадцать тысяч дол. 14 ц."},
				{ usdMoneyFormatter, -143L, "минус ноль дол. 14 ц."},
				{ byrMoneyFormatter, -42_244_224L, "минус сорок две тысячи двести сорок четыре руб. 22 коп."},
				{ eurMoneyFormatter, 223_322_223_322L, "двести двадцать три миллиона триста двадцать две тысячи двести двадцать три евр. 32 ц."},
				{ uahMoneyFormatter, -5_553_810L, "минус пять тысяч пятьсот пятьдесят три гривны 81 копейка"},
				{ uahMoneyFormatter, 321_456L, "триста двадцать одна гривна 46 копеек"}, { chyMoneyFormatter, 1_255L, "один юань 26 фэн."}
		};
	}

	@Test(description = "форматтер суммы, сумма прописью", dataProvider = "moneyFormatterSpelledAmount")
	public void testMoneyFormatterSpelledAmount(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы, сумма прописью.");
	}



	@DataProvider(name = "moneyFormatterShowPartUnits")
	Object[][] dataMoneyFormatterShowPartUnits()
	{
		return new Object[][] {
				{ MoneyFormatter.ruMoneyFormatter(3), 0L, "0 руб. 00 коп."}, { MoneyFormatter.ruMoneyFormatter(3).showPartUnits(false), 0L, "0 руб."},
				{ MoneyFormatter.byrMoneyFormatter(3).spellNumbers(true), 3L, "ноль руб. 00 коп."}, { MoneyFormatter.byrMoneyFormatter(3).showPartUnits(false).spellNumbers(true), 3L, "ноль руб."},
				{ MoneyFormatter.usdMoneyFormatter(3), 314L, "0 дол. 31 ц."}, { MoneyFormatter.usdMoneyFormatter(3).showPartUnits(false), 314L, "0 дол."},
				{ MoneyFormatter.eurMoneyFormatter(3).spellNumbers(true), 1024L, "один евр. 02 ц."}, { MoneyFormatter.eurMoneyFormatter(3).showPartUnits(false).spellNumbers(true), 1024L, "один евр."},
				{ MoneyFormatter.uahMoneyFormatter(3), 678_901L, "678 грн. 90 коп."}, { MoneyFormatter.uahMoneyFormatter(3).showPartUnits(false), 678_901L, "678 грн."},
				{ MoneyFormatter.cnyMoneyFormatter(3), 120_340_560L, "120340 юань 56 фэн."}, { MoneyFormatter.cnyMoneyFormatter(3).showPartUnits(false), 120_340_560L, "120340 юань"}
		};
	}

	@Test(description = "форматтер суммы, вывод без производных единиц (копеек/центов/...)", dataProvider = "moneyFormatterShowPartUnits")
	public void testMoneyFormatterShowPartUnits(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы без производных единиц (копеек/центов/...).");
	}



	@DataProvider(name = "moneyFormatterGroupTriads")
	Object[][] dataMoneyFormatterGroupTriads()
	{
		return new Object[][] {
				{ MoneyFormatter.ruMoneyFormatter(2).groupTriad(true), 1_234_56L, "1 234 руб. 56 коп."}, { MoneyFormatter.ruMoneyFormatter(2).groupTriad(false), 1234_56L, "1234 руб. 56 коп."},
				{ MoneyFormatter.ruMoneyFormatter(2).groupTriad(true).spellNumbers(true).shortTitles(false), 1234_56L, "одна тысяча двести тридцать четыре рубля 56 копеек"},
				{ MoneyFormatter.ruMoneyFormatter(2).spellNumbers(true).shortTitles(false), 1234_56L, "одна тысяча двести тридцать четыре рубля 56 копеек"},
				{ MoneyFormatter.usdMoneyFormatter(3).groupTriad(true).shortTitles(false), 1_000_000_000L, "1 000 000 долларов 00 центов"},
				{ MoneyFormatter.eurMoneyFormatter(0), 2_718_281_828_459L, "2718281828459 евр. 00 ц."},
				{ MoneyFormatter.eurMoneyFormatter(0).groupTriad(true), 2_718_281_828_459L, "2 718 281 828 459 евр. 00 ц."},
				{ MoneyFormatter.cnyMoneyFormatter(4).groupTriad(true), 1_719_243_5488L, "1 719 243 юань 55 фэн."}
		};
	}

	@Test(description = "форматтер суммы, группировка цифр в записи основной валюты (рубль/евро/...) тройками", dataProvider = "moneyFormatterGroupTriads")
	public void testMoneyFormatterGroupTriads(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы с группировкой цифр в записи основной валюты (рубль/евро/...) тройками.");
	}



	@DataProvider(name = "moneyFormatterComplex")
	Object[][] dataMoneyFormatterComplex()
	{
		return new Object[][]{
				{MoneyFormatter.ruMoneyFormatter(5).groupTriad(true).spellNumbers(true), 123_400_500_67890L, "сто двадцать три миллиона четыреста тысяч пятьсот руб. 68 коп."},
				{MoneyFormatter.ruMoneyFormatter(5).groupTriad(false).spellNumbers(true), 123_400_500_67890L, "сто двадцать три миллиона четыреста тысяч пятьсот руб. 68 коп."},
				{MoneyFormatter.cnyMoneyFormatter(3).showPartUnits(false).groupTriad(true).shortTitles(false), -2_016_001L, "-2 016 юаней"},
				{MoneyFormatter.cnyMoneyFormatter(3).showPartUnits(false).groupTriad(true).shortTitles(true).spellNumbers(true), -2_016_001L, "минус две тысячи шестнадцать юань"}
		};
	}

	@Test(description = "форматтер суммы, сложные случаи", dataProvider = "moneyFormatterComplex")
	public void testMoneyFormatterComplex(MoneyFormatter formatter, long amount, String expectedString)
	{
		String formattedAmount = formatter.format(amount);
		Assert.assertEquals(formattedAmount, expectedString, "Ошибка форматирования суммы.");
	}
}
