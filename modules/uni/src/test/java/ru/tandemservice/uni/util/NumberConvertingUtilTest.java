/* $Id$ */
package ru.tandemservice.uni.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * @author Nikolay Fedorovskih
 * @since 24.04.2015
 */
public class NumberConvertingUtilTest
{
    @DataProvider
    Object[][] testWriteSumData()
    {
        return new Object[][]{
                {"Восемь тысяч двести рублей 80 копеек", 8200_80L, 2},
                {"Восемь тысяч двести рублей 08 копеек", 8200_075L, 3},
                {"Ноль рублей 00 копеек", 0L, 2},
                {"Минус ноль рублей 10 копеек", -104_999L, 6},
                {"Минус один рубль 01 копейка", -1_01444L, 5},
        };
    }

    @Test(dataProvider = "testWriteSumData")
    public void testWriteSum(String expected, long value, int decimalDigits)
    {
        Assert.assertEquals(NumberConvertingUtil.writeSum(value, decimalDigits), expected);
    }
}