package uni.scripts
import org.hibernate.Session
import org.tandemframework.core.runtime.ApplicationRuntime
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfPictureUtil
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile
import org.tandemframework.shared.fias.base.entity.Address
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager
import org.tandemframework.shared.person.base.entity.PersonEduInstitution
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import ru.tandemservice.uni.component.student.StudentPersonCard.IPrintStudentEnrData
import ru.tandemservice.uni.entity.employee.OrderData
import ru.tandemservice.uni.entity.employee.Student
/**
 * @author rsizonenko
 * @since 11.12.2014
 */

/* $Id:$ */

return new StudentCardPrint(                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        student: session.get(Student.class, object) // объект печати
).print()


class StudentCardPrint{
    Session session
    byte[] template
    Student student
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        List<PersonEduInstitution> personEduInstitutionList = DataAccessServices.dao().getList(PersonEduInstitution.class, PersonEduInstitution.L_PERSON, student.getPerson(), PersonEduInstitution.P_ISSUANCE_DATE);
        List<PersonNextOfKin> personNextOfKinList = DataAccessServices.dao().getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, student.getPerson());
        OrderData orderData = DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, student)

        DatabaseFile photo = student.getPerson().getIdentityCard().getPhoto();

        def education;
        if (PersonEduDocumentManager.isShowLegacyEduDocuments())
            education = formatEducation(personEduInstitutionList.get(0))
        else
            education = student.getEduDocument()?.getTitleExtended()

        RtfDocument document = new RtfReader().read(template)
        if (null != photo && null != photo.getContent())
            RtfPictureUtil.insertPicture(document, photo.content, "photo", 2000L, 1500L)
        else
            im.put("photo", "");

        im
                .put("formativeOrgUnit", student.getEducationOrgUnit()?.getFormativeOrgUnit()?.getFullTitle())
                .put("bookNum", student.getPersonalFileNumber())
                .put("groupName", student.getGroup()?.getTitle())
                .put("lastName", student.getPerson().getIdentityCard().lastName)
                .put("firstName", student.getPerson().getIdentityCard().firstName)
                .put("middleName", student.getPerson().getIdentityCard().middleName)
                .put("seria", student.getPerson().getIdentityCard().seria)
                .put("number", student.getPerson().getIdentityCard().number)
                .put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getIssuanceDate()))
                .put("issuance", student.getPerson().getIdentityCard().getIssuancePlace())
                .put("educationOrgUnit", student.getEducationOrgUnit().title)
                .put("sex", student.getPerson().getIdentityCard().getSex().shortTitle)
                .put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getBirthDate()))
                .put("birthPlace", student.getPerson().getIdentityCard().birthPlace)
                .put("citizenship", student.getPerson().getIdentityCard().getCitizenship()?.title)
                .put("nationality", student.getPerson().getIdentityCard().getNationality()?.title)
                .put("addressRegistration", student.getPerson().getIdentityCard().getAddress()?.titleWithFlat)
                .put("maritalStatus", student.getPerson().getFamilyStatus()?.title)
                .put("addressTitle", student.getPerson().getAddress()?.titleWithFlat)
                .put("homePhone", student.getPerson().getContactData().getPhoneFact())
                .put("mobilePhone", student.getPerson().getContactData().getPhoneMobile())
                .put("fax", "")
                .put("email", student.getPerson().getContactData().getEmail())
                .put("icq", student.getPerson().getContactData().getOther())
                .put("enrollmentOrderNumber", orderData?.getEduEnrollmentOrderNumber())
                .put("enrollmentOrderDate", orderData == null? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(orderData.getEduEnrollmentOrderDate()))
                .put("education", education)
                .put("workPlace", student.getPerson().getWorkPlace())
                .put("workPlacePosition", student.getPerson().getWorkPlacePosition())
                .put("bookNumber", student.getBookNumber())
                .put("developForm", student.getEducationOrgUnit().getDevelopForm().getTitle())
                .modify(document)

        // Enrollment data
        injectEnrollmentExtractData(student.getId(), document);




        tm
                .put("educationType", personEduInstitutionList == null ? [[""]] : getPersonEducationData(personEduInstitutionList))
                .put("parentType", personNextOfKinList == null ? [[""]] :  getRelativesData(personNextOfKinList))
                .modify(document)


        return [document: RtfUtil.toByteArray(document),
                fileName: "Личная карточка студента ${student.fullFio}.rtf",
                rtf: document]
    }


    def injectEnrollmentExtractData(Long studentId, RtfDocument document)
    {
        RtfInjectModifier modifier = null;

        // Студент может быть зачислен или старым модулем или новым (есть клиенты, у которых оба модуля подключены)

        if (ApplicationRuntime.containsBean(IPrintStudentEnrData.UNIENR14_SPRING_BEAN_NAME))
            modifier = ApplicationRuntime.getBean(IPrintStudentEnrData.UNIENR14_SPRING_BEAN_NAME, IPrintStudentEnrData.class).getEnrPrintDataModifier(studentId);

        if (modifier == null && ApplicationRuntime.containsBean(IPrintStudentEnrData.UNIEC_SPRING_BEAN_NAME))
            modifier = ApplicationRuntime.getBean(IPrintStudentEnrData.UNIEC_SPRING_BEAN_NAME, IPrintStudentEnrData.class).getEnrPrintDataModifier(studentId);

        if (modifier == null)
        {
            // Возможно, лучше будет передавать модифаер в бины и там затирать метки...
            modifier = new RtfInjectModifier()
                    .put("orderNumber", "")
                    .put("orderDate", "")
                    .put("course", "")
                    .put("ball", "");
        }

        modifier.modify(document);
    }


    def String[][] getRelativesData(List<PersonNextOfKin> personNextOfKinList)
    {
        String[][] relativesData = new String[personNextOfKinList.size()][];
        for (int i = 0; i < personNextOfKinList.size(); i++)
        {
            PersonNextOfKin relative = personNextOfKinList.get(i);

            String relationDegree = relative.getRelationDegree() == null ? null : relative.getRelationDegree().getTitle();
            StringBuilder builder = new StringBuilder();
            builder.append(relative.getFullFio());
            if (relative.getBirthDate() != null)
                builder.append(" (").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(relative.getBirthDate())).append(")");
            if (relative.getEmploymentPlace() != null)
                builder.append(", место работы: ").append(relative.getEmploymentPlace());
            if (relative.getPost() != null)
                builder.append(" (").append(relative.getPost()).append(")");
            if (relative.getPhones() != null)
                builder.append(", тел. ").append(relative.getPhones());
            relativesData[i] = [relationDegree == null ? "" : relationDegree, builder.toString()];

        }
        return  relativesData;
    }

    def String[][] getPersonEducationData(List<PersonEduInstitution> personEduInstitutionList )
    {
        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            //[Вид образовательного учреждения] | [Адрес образовательного учреждения], [Тип документа об образовании]: [Серия] №[Номер], дата выдачи: [Дата окончания]
            List<List<String>> personEducationData = new ArrayList<>()
            for (int i = 0; i < personEduInstitutionList.size(); i++)
            {
                PersonEduInstitution personEduInstitution = personEduInstitutionList.get(i);

                String educationType = personEduInstitution.getEduInstitutionKind().getTitle();

                Address address = new Address();
                address.setSettlement(personEduInstitution.getAddressItem());
                address.setCountry(address.getSettlement().getCountry());

                StringBuilder buffer = new StringBuilder(address.getTitle() + ", " + personEduInstitution.getDocumentType().getTitle());
                if (personEduInstitution.getSeria() != null)
                    buffer.append(": ").append(personEduInstitution.getSeria());
                if (personEduInstitution.getNumber() != null)
                    buffer.append(" №").append(personEduInstitution.getNumber());
                buffer.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");

                personEducationData.add([educationType, buffer.toString()])
            }
            return personEducationData as String[][];
        } else {
            String eduLevel = student.getEduDocument() == null || student.getEduDocument().getEduLevel() == null ? "" : student.getEduDocument().getEduLevel().getTitle();
            String document = student.getEduDocument() == null ? "" : student.getEduDocument().getTitleExtended();
            return [[eduLevel, document]];
        }

    }

    def String formatEducation(PersonEduInstitution personEduInstitution)
    {
        StringBuilder result = new StringBuilder();
        if (personEduInstitution.getEduInstitutionKind() != null)
            result.append(personEduInstitution.getEduInstitutionKind().getShortTitle()).append(" ");
        if (personEduInstitution.getEduInstitution() != null)
            result.append(" ").append(personEduInstitution.getEduInstitution().getTitle());
        result.append(", ");
        if (personEduInstitution.getAddressItem() != null)
            result.append(personEduInstitution.getAddressItem().getTitleWithType()).append(", ");
        result.append(personEduInstitution.getDocumentType().getTitle());
        if (personEduInstitution.getSeria() != null)
            result.append(": ").append(personEduInstitution.getSeria());
        if (personEduInstitution.getNumber() != null)
            result.append(" №").append(personEduInstitution.getNumber());
        result.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }
}