package uni.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.component.documents.d1.Add.Model
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uni.util.formatters.PersonIofFormatter

/**
 * @author rsizonenko
 * @since 13.04.2015
 */

/* $Id:$ */

return new StudentIndeedPrint(
        session: session,
        template: template,
        model: object
).print();


class StudentIndeedPrint
{
    Session session
    byte[] template
    Model model

    RtfInjectModifier injectModifier = new RtfInjectModifier()


    def print() {
        boolean male = model.getStudent().getPerson().getIdentityCard().getSex().isMale()
        TopOrgUnit academy = TopOrgUnit.getInstance()
        Calendar c = Calendar.getInstance()
        c.setTime(model.getFormingDate())

        RtfDocument document = new RtfReader().read(template)

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_G", academy.getGenitiveCaseTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("birthDate", model.getStudent().getPerson().getBirthDateStr())
                .put("sexTitle", male ? "он" : "она")
                .put("studentCase", male ? "студентом" : "студенткой")
                .put("course", model.getCourse().getTitle())
                .put("departmentTitle", model.getDepartment().getId() == 0L ? "дневном" : "вечернем")
                .put("developFormTitle", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("orgUnitTitle", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle())
                .put("orgUnitTitleCase", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())
                .put("compensationTypeStr", model.getStudent().getCompensationType().isBudget() ? "бюджетной" : "договорной")
                .put("developPeriodStr", model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("term", model.getTerm() == null ? "" : model.getTerm().toString())
                .put("secretarTitle", model.getSecretarTitle())
                .put("phoneTitle", model.getPhone())
                .put("documentForTitle", model.getDocumentForTitle())
                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())
                .put("rectorAlt", academy.getHead() == null ? "" : PersonIofFormatter.INSTANCE.format((IdentityCard) academy.getHead().getEmployee().getPerson().getIdentityCard()));

        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "очной");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "заочной");
        else
            injectModifier.put("developFormTitle_G", "очно-заочной");


        injectModifier.modify(document);


        return [document: RtfUtil.toByteArray(document),
                fileName: "Справка «Действительно является студентом» ${model.getStudent().fullFio}.rtf",
                rtf: document]
    }
}