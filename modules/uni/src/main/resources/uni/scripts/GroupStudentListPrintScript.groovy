package uni.scripts

import org.hibernate.Session
import org.springframework.util.StringUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.shared.fias.base.entity.AddressBase
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.dao.UniDaoFacade
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.entity.orgstruct.Group

/**
 * @author rsizonenko
 * @since 21.04.2015
 */

/* $Id:$ */

return new GroupStudentListPrint(
        session: session,
        template: template,
        studentList : studentList,
        group : group
).print();

class GroupStudentListPrint{

    Session session
    byte[] template
    List<Student> studentList
    Group group

    def print()
    {

        RtfDocument document = RtfDocument.fromByteArray(template)

        new RtfInjectModifier()
                .put("groupTitle", group.getTitle())
                .put("currentDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                .put("educationLevel", group.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle())
                .put("developForm", group.getEducationOrgUnit().getDevelopForm().getTitle())
                .put("developPeriod", group.getEducationOrgUnit().getDevelopPeriod().getTitle())
                .modify(document);

        RtfTableModifier tableModifier = new RtfTableModifier();

        String[][] studentData = new String[studentList.size()][];

        List<Student> captainStudentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group)
        List<Integer> captainRows = new ArrayList<>()


        int i = 0;
        for (Student student : studentList)
        {
            String number = Integer.toString(i + 1);
            String fio = student.getPerson().getFullFio();
            IdentityCard identityCard = student.getPerson().getIdentityCard();

            String sex = identityCard.getSex().getShortTitle();
            String compensation = (student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET)) ? "Б" : "К";
            String status = student.getStatus().getTitle();
            String birthDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate());

            String passport = getIdentityCardTitle(identityCard);
            AddressBase address = identityCard.getAddress();
            AddressBase factAddress = student.getPerson().getAddress();

            String addressTitle = address == null ? "" : address.getTitleWithFlat();

            String factAddressTitle = "";

            if (captainStudentList.contains(student))  captainRows.add(i);


            if (null != factAddress)
                factAddressTitle = address != null && address.getTitleWithFlat().equals(factAddress.getTitleWithFlat()) ? "" : factAddress.getTitleWithFlat();

            studentData[i++] = [number, fio, sex, compensation, status, birthDate, passport, addressTitle, factAddressTitle] as String[];
        }

        tableModifier.put("T", studentData)
        tableModifier.put("T", new Intercepter(captainRows))

        tableModifier.modify(document)


        return [document : document.toByteArray(),
                fileName: "groupList.rtf",
                rtf : document];
    }

    static def getIdentityCardTitle(IdentityCard identityCard)
    {
        StringBuilder str = new StringBuilder();
        if (identityCard.getCardType() != null)
        {
            str.append(identityCard.getCardType().getShortTitle()).append(": ");
        }
        if (identityCard.getSeria() != null)
        {
            str.append(identityCard.getSeria()).append(" ");
        }
        if (identityCard.getNumber() != null)
        {
            str.append(identityCard.getNumber()).append(" ");
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(identityCard.getIssuancePlace()))
        {
            str.append("Выдан: ").append(identityCard.getIssuancePlace()).append(" ");
        }
        if (identityCard.getIssuanceDate() != null)
        {
            str.append("Дата выдачи: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()));
        }
        return str.toString();
    }

    class Intercepter extends RtfRowIntercepterBase
    {

        Intercepter(List<Integer> captainRows) {
            this.captainRows = captainRows
        }

        List<Integer> captainRows;

        @Override
        public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
        {
            if (captainRows.contains(rowIndex))
                return new RtfString().boldBegin().append(StringUtils.capitalize(value)).boldEnd().toList();

            return super.beforeInject(table, row, cell, rowIndex, colIndex, value);
        }
    }
}