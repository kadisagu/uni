package uni.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport

/**
 * @author rsizonenko
 * @since 23.04.2015
 */

/* $Id:$ */

return new StudentsAgeSexDistribution (
        session: session,
        template: template,
        students: students,
        report: report
).print();

class StudentsAgeSexDistribution{

    Session session
    byte[] template
    List<Student> students
    StudentsAgeSexDistributionReport report

    def print()
    {
        RtfDocument document = RtfDocument.fromByteArray(template);

        final RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate())).modify(document);

        // записать параметры
        new RtfTableModifier().put("P", getParameters(report)).modify(document);


        final int[][] result = new int[2][9];

        // для каждого студента, удовлетворяющего параметрам отчета
        for (final Student student : students)
        {
            final IdentityCard identityCard = student.getPerson().getIdentityCard();
            final boolean female = identityCard.getSex().getCode().equals(SexCodes.FEMALE);

            // посчитать итоговое количество
            result[0][0]++;

            // если девушка, то во второй строке отметить это отдельно
            if (female)
            {
                result[1][0]++;
            }

            final Date birthDate = identityCard.getBirthDate();
            if (birthDate == null)
            {
                continue;
            }

            // посчитать сколько лет студенту и в какую колонку занести эти данные
            final int age = CoreDateUtils.getFullYearsAge(birthDate);
            final int column = (age <= 17) ? 1 : (age >= 24) ? 8 : age - 16;

            result[0][column]++;

            // если девушка, то во второй строке отметить это отдельно
            if (female)
            {
                result[1][column]++;
            }
        }

        for (int i = 0; i < result.length; i++)
        {
            final row = result[i]
            for (int j = 0; j < row.length; j++)
            {
                injectModifier.put("T" + i + j, '' + row[j])
            }
        }
        injectModifier.modify(document);

        return [document: RtfUtil.toByteArray(document),
                fileName: "Отчет по составу студентов по возрасту и полу.rtf",
                rtf: document]
    }


    def getParameters(final StudentsAgeSexDistributionReport report)
    {
        final List<String[]> parameters = new ArrayList<String[]>();
        addParameterRow(parameters, "Формирующее подр.", report.getFormativeOrgUnit());
        addParameterRow(parameters, "Территориальное подр.", report.getTerritorialOrgUnit());
        addParameterRow(parameters, "Форма освоения", report.getDevelopForm());
        addParameterRow(parameters, "Условие освоения", report.getDevelopConditition());
        addParameterRow(parameters, "Технология освоения", report.getDevelopTech());
        addParameterRow(parameters, "Квалификация", report.getQualification());
        addParameterRow(parameters, "Категория обучаемого", report.getStudentCategory());
        addParameterRow(parameters, "Состояние", report.getStudentStatus());
        return parameters.toArray(new String[0][]);
    }

    def addParameterRow(final List<String[]> parameters, final String title, final String value)
    {
        if (!StringUtils.isEmpty(value))
        {
            parameters.add([title, value] as String[]);
        }
    }

}