package uni.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.component.documents.d4.Add.Model
import ru.tandemservice.uni.entity.catalog.EducationLevels
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes

import java.text.SimpleDateFormat

/**
 * @author rsizonenko
 * @since 13.04.2015
 */

/* $Id:$ */


return new MilitaryRegistrationPrint(
        session: session,
        template: template,
        model: object
).print();


;

class MilitaryRegistrationPrint
{
    Session session
    byte[] template
    Model model

    RtfInjectModifier injectModifier = new RtfInjectModifier()
    def print(){
        RtfDocument document = new RtfReader().read(template)


        TopOrgUnit academy = TopOrgUnit.getInstance();
        IdentityCard identityCard = model.getStudent().getPerson().getIdentityCard();
        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        boolean male = identityCard.getSex().isMale();
        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.DATIVE, male);
        String firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.DATIVE, male);
        String middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.DATIVE, male);
        Date birthDate = model.getStudent().getPerson().getIdentityCard().getBirthDate();
        EducationLevels educationLevel = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        StructureEducationLevels levelType = educationLevel.getLevelType();
        String levelTypeStr = "";
        String levelTypeStageStr = "";

        if (levelType.isMiddle())
        {
            if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = "по специальности";
                levelTypeStageStr = "Повышенный уровень СПО";
            }
            else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = "по специальности";
                levelTypeStageStr = "Базовый уровень СПО";
            }
        }
        else if (levelType.isSpecialty() || levelType.isSpecialization())
        {
            levelTypeStr = "по специальности";
            levelTypeStageStr = "Специалитет";
        }
        else if (levelType.isMaster())
        {
            levelTypeStr = "по направлению";
            levelTypeStageStr = "Магистратура";
        }
        else if (levelType.isBachelor())
        {
            levelTypeStr = "по направлению";
            levelTypeStageStr = "Бакалавриат";
        }

        SimpleDateFormat DD_DATE = new SimpleDateFormat("dd");
        SimpleDateFormat YY_DATE = new SimpleDateFormat("yyyy");

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_A", academy.getAccusativeCaseTitle() != null ? academy.getAccusativeCaseTitle().toLowerCase() : academy.getPrintTitle().toLowerCase())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()))
                .put("number", Integer.toString(model.getNumber()))
                .put("citizen", male ? "гражданину" : "гражданке")
                .put("lastName_D", lastName.toUpperCase())
                .put("firstName_D", firstName.toUpperCase())
                .put("middleName_D", middleName == null ? "" : middleName.toUpperCase())
                .put("birthDate", birthDate == null ? "    " : YY_DATE.format(birthDate))
                .put("gender", male ? "он" : "она")
                .put("entranceYear", Integer.toString(model.getEntranceYear()))
                .put("attempted", male ? "поступил" : "поступила")
                .put("educationLevelStage", model.getEducationLevelStage() == null ? "" : model.getEducationLevelStage())
                .put("uniTitleAtTimeAdmission", model.getUniTitleAtTimeAdmission())
                .put("enrollmentOrderNumber", model.getEnrollmentOrderNumber())
                .put("enrollmentOrderDate", new SimpleDateFormat("dd.MM.yy").format(model.getEnrollmentOrderDate()))
                .put("levelTypeStage", levelTypeStageStr)
                .put("course", model.getStudent().getCourse().getTitle())
                .put("developForm_G", model.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle())
                .put("formativeOrgUnit_P", formativeOrgUnit.getPrepositionalCaseTitle() != null ? formativeOrgUnit.getPrepositionalCaseTitle() : formativeOrgUnit.getPrintTitle())
                .put("levelType", levelTypeStr)
                .put("levelHighSchool", educationLevel.isProfileOrSpecialization() ? educationLevel.getParentLevel().getTitleCodePrefix() + " " + educationLevel.getParentLevel().getTitle() : educationLevel.getTitleCodePrefix() + " " + educationLevel.getTitle())
                .put("certificateSeria", model.getCertificateSeria())
                .put("certificateNumber", model.getCertificateNumber())
                .put("certificateRegNumber", model.getCertificateRegNumber())
                .put("certificateDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getCertificateDate()))
                .put("certificateExpiriationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getCertificateExpiriationDate()))
                .put("certificationAgency", model.getCertificationAgency())
                .put("licenceSeria", model.getLicenceSeria())
                .put("licenceNumber", model.getLicenceNumber())
                .put("licenceRegNumber", model.getLicenceRegNumber())
                .put("licenceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getLicenceDate()))
                .put("licensingAgency", model.getLicensingAgency())
                .put("plannedEndDay", DD_DATE.format(model.getPlannedEndDate()))
                .put("plannedEndMonth", RussianDateFormatUtils.getMonthName(model.getPlannedEndDate(), false))
                .put("plannedEndYear", YY_DATE.format(model.getPlannedEndDate()))
                .put("militaryOffice", model.getMilitaryOffice())
                .put("concluded", male ? "Заключил" : "Заключила")
                .put("initiated", male ? "приступил" : "приступила")
                .put("rectorAlt", model.getRectorAltStr())
                .put("programStartYear", model.isTrainingOfficersActive() ? model.getProgramStartYear() != null ? String.valueOf(model.getProgramStartYear()) : "____" : "")
                .put("programEndMonth", model.isTrainingOfficersActive() ? model.getMonth() != null ? " " + model.getMonth().getTitle() : "" : "")
                .put("programEndYear", model.isTrainingOfficersActive() ? model.getProgramEndYear() != null ? String.valueOf(model.getProgramEndYear()) : "____" : "")
                .put("managerMilitaryFIO", model.isTrainingOfficersActive() ? model.getManagerMilitaryDepartment() : "");

        if (model.isTrainingOfficersActive())
        {
            injectModifier.put("remove", "");
        }

        injectModifier.modify(document)


        return [document: RtfUtil.toByteArray(document),
                fileName: "Справка в военкомат ${model.getStudent().fullFio}.rtf",
                rtf: document]
    }
}