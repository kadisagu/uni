package uni.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.entity.orgstruct.Group

/**
 * @author rsizonenko
 * @since 21.04.2015
 */

/* $Id:$ */

return new StudentsGroupsListPrint(
        session : session,
        template : template,
        groupMap : groupMap,
        checkList : checkList,
        columnSizes : columnSizes,
        columnHeaders : columnHeaders
).print();

class StudentsGroupsListPrint{

    Session session
    byte[] template
    Map<Group, List<Student>> groupMap
    List<String> checkList
    int[] columnSizes
    String[] columnHeaders

    Boolean isSexReq
    Boolean isBirthDateReq
    Boolean isCompensationTypeReq
    Boolean isThriftyGroupReq
    Boolean isStudentStatusReq
    Boolean isPersonalNumberReq
    Boolean isBookNumberReq
    Boolean isEduLevelReq
    Boolean isDevelopFormReq
    Boolean isDevelopConditionReq
    Boolean isDevelopTechReq
    Boolean isDevelopPeriodReq
    Boolean isFormativeOrgUnitReq
    Boolean isTerritorialOrgUnitReq



    def print()
    {
        RtfDocument document = RtfDocument.fromByteArray(template)

        isSexReq = checkList.contains("isSexReq")
        isBirthDateReq = checkList.contains("isBirthDateReq")
        isCompensationTypeReq = checkList.contains("isCompensationTypeReq")
        isThriftyGroupReq = checkList.contains("isThriftyGroupReq")
        isStudentStatusReq = checkList.contains("isStudentStatusReq")
        isPersonalNumberReq = checkList.contains("isPersonalNumberReq")
        isBookNumberReq = checkList.contains("isBookNumberReq")
        isEduLevelReq = checkList.contains("isEduLevelReq")
        isDevelopFormReq = checkList.contains("isDevelopFormReq")
        isDevelopConditionReq = checkList.contains("isDevelopConditionReq")
        isDevelopTechReq = checkList.contains("isDevelopTechReq")
        isDevelopPeriodReq = checkList.contains("isDevelopPeriodReq")
        isFormativeOrgUnitReq = checkList.contains("isFormativeOrgUnitReq")
        isTerritorialOrgUnitReq = checkList.contains("isTerritorialOrgUnitReq")

        RtfDocument resultDoc = document.getClone();
        resultDoc.getElementList().clear();

        for (Map.Entry<Group, List<Student>> entry : groupMap.entrySet())
        {
            RtfDocument doc = document.getClone();
            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put("FILTERS", getFiltersTableData(entry.getKey()));
            tableModifier.put("TH", [["№", ""]] as String[][]);
            tableModifier.put("T", getStudentsTableData(entry.getValue()));

            tableModifier.put("TH", new Intercepter());

            tableModifier.modify(doc);
            resultDoc.getElementList().addAll(doc.getElementList());
        }

        return [document: resultDoc.toByteArray(),
        fileName: "studentsGroup.rtf",
        rtf: resultDoc]

    }

    def String[][] getFiltersTableData(Group group)
    {
        List<String[]> lines = new ArrayList<>();
        EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();
        lines.add(["Группа:", group.getTitle()] as String[]);

        if (isEduLevelReq)
            lines.add(["Направление подготовки (специальность):", educationOrgUnit.getEducationLevelHighSchool().getPrintTitle()] as String[]);

        if(isDevelopFormReq)
            lines.add(["Форма освоения:", educationOrgUnit.getDevelopForm().getTitle()] as String[]);

        if(isDevelopConditionReq)
            lines.add(["Условие освоения:", educationOrgUnit.getDevelopCondition().getTitle()] as String[]);

        if(isDevelopTechReq)
            lines.add(["Технология освоения:", educationOrgUnit.getDevelopTech().getTitle()] as String[]);

        if(isDevelopPeriodReq)
            lines.add(["Нормативный срок освоения:", educationOrgUnit.getDevelopPeriod().getTitle()] as String[]);

        if(isFormativeOrgUnitReq)
            lines.add(["Формирующее подр.:", null != educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() ? educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() : educationOrgUnit.getFormativeOrgUnit().getTitle()] as String[]);

        if(isTerritorialOrgUnitReq)
            lines.add(["Территориальное подр.:", null != educationOrgUnit.getTerritorialOrgUnit().getNominativeCaseTitle() ? educationOrgUnit.getTerritorialOrgUnit().getNominativeCaseTitle() : educationOrgUnit.getTerritorialOrgUnit().getTerritorialTitle()] as String[]);

        if(isTerritorialOrgUnitReq)
            lines.add(["Выпускающее подразделение:", null != group.getParent().getNominativeCaseTitle() ? group.getParent().getNominativeCaseTitle() : group.getParent().getTitle()] as String[]);

        return lines.toArray();
    }

    def String[][] getStudentsTableData(List<Student> students)
    {
        int i = 1;
        List<String[]> lines = new ArrayList<>();

        for(Student student : students)
        {
            List<String> cells = new ArrayList<>();

            cells.add(String.valueOf(i++));
            cells.add(student.getPerson().getFullFio());

            if(isSexReq) cells.add(student.getPerson().getIdentityCard().getSex().getShortTitle());
            if(isBirthDateReq) cells.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getBirthDate()));
            if(isCompensationTypeReq) cells.add(UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()) ? "Б" : "К");
            if(isThriftyGroupReq) cells.add(student.isTargetAdmission() ? "Ц" : "");
            if(isStudentStatusReq) cells.add(student.getStatus().getTitle());
            if(isPersonalNumberReq) cells.add(student.getPersonalNumber());
            if(isBookNumberReq) cells.add(student.getBookNumber());

            lines.add(cells.toArray(new String[cells.size()]));
        }

        return lines.toArray();
    }


    class Intercepter extends RtfRowIntercepterBase
    {
        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {
            RtfUtil.splitRow(table.getRowList().get(0), 1, new HeaderIntercepter(), columnSizes);

            RtfUtil.splitRow(table.getRowList().get(1), 1, new SplitIntercepter(), columnSizes);
        }
    }

    class SplitIntercepter implements IRtfRowSplitInterceptor
    {
        @Override
        public void intercept(RtfCell newCell, int index)
        {
            String content = (new String[columnSizes.length])[index];
            IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
            newCell.getElementList().add(text);
        }
    }

    class HeaderIntercepter implements IRtfRowSplitInterceptor
    {
        @Override
        public void intercept(RtfCell newCell, int index)
        {
            String content = columnHeaders[index];
            IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
            newCell.getElementList().add(text);
        }
    }
}