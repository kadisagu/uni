package uni.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.rtf.SharedRtfUtil
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uni.entity.catalog.Qualifications
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.entity.report.StudentsCoursesSpecialitiesAllocationReport
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject

/**
 * @author rsizonenko
 * @since 21.04.2015
 */

/* $Id:$ */

return new StudentsCoursesSpecialitiesAllocationPrint(
        session: session,
        template: template,
        map: map,
        report: report
).print();

class StudentsCoursesSpecialitiesAllocationPrint{

    Session session
    byte[] template
    Map<EducationLevelsHighSchool, List<Student>> map
    StudentsCoursesSpecialitiesAllocationReport report

    def print()
    {
        final RtfDocument document = RtfDocument.fromByteArray(template);

        // записать дату формирования
        new RtfInjectModifier().put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate())).modify(document);

        final RtfTableModifier tableModifier = new RtfTableModifier();

        // записать параметры
        tableModifier.put("P", getParameters(report));

        // записать таблицу
        tableModifier.put("T", getTable(map));
        tableModifier.put("T", new Intercepter());
        tableModifier.modify(document);

        return [document : document.toByteArray(),
                fileName: "Отчет «Распределение студентов по курсам и направлениям подготовки (специальностям) (ВО)».rtf",
                rtf: document];
    }

    def String[][] getParameters(final StudentsCoursesSpecialitiesAllocationReport report)
    {
        final List<String[]> parameters = new ArrayList<>();
        addParameterRow(parameters, "Формирующее подр.", report.getFormativeOrgUnit());
        addParameterRow(parameters, "Территориальное подр.", report.getTerritorialOrgUnit());
        addParameterRow(parameters, "Форма освоения", report.getDevelopForm());
        addParameterRow(parameters, "Условие освоения", report.getDevelopConditition());
        addParameterRow(parameters, "Технология освоения", report.getDevelopTech());
        addParameterRow(parameters, "Квалификация", report.getQualification());
        addParameterRow(parameters, "Категория обучаемого", report.getStudentCategory());
        addParameterRow(parameters, "Состояние", report.getStudentStatus());
        return parameters.toArray(new String[0][]);
    }

    def addParameterRow(final List<String[]> parameters, final String title, final String value)
    {
        if (!StringUtils.isEmpty(value))
        {
            parameters.add([title, value] as String[]);
        }
    }

    class Intercepter extends RtfRowIntercepterBase
    {
        @Override
        @SuppressWarnings(["deprecation"])
        public void afterModify(final RtfTable table, final List<RtfRow> newRowList, final int startIndex)
        {
            final RtfRow row = UniBaseUtils.getLast(newRowList);
            final List<RtfCell> cells = row.getCellList();

            UniRtfUtil.setRowBold(row);
            SharedRtfUtil.setCellAlignment(cells.get(0), IRtfData.QL, IRtfData.QR);
        }
    }


    def String[][] getTable(Map<EducationLevelsHighSchool, List<Student>> map)
    {
        final List<Row> rows = new ArrayList<>();
        final Row total = new Row("ИТОГО");
        // для каждого НПв
        for (final Map.Entry<EducationLevelsHighSchool, List<Student>> entry : map.entrySet())
        {
            final EducationLevelsHighSchool educationLevelHighSchool = entry.getKey();
            final Qualifications qualification = educationLevelHighSchool.getEducationLevel().getQualification();

            // создать строку
            final Row row = new Row(educationLevelHighSchool.getPrintTitle());

            // посчитать студентов на нем в соответствующих ячейках
            for (final Student student : entry.getValue())
            {
                def subject = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject()
                def kind = subject.getSubjectIndex().getProgramKind()

                final int course = Integer.parseInt(student.getCourse().getCode());

                def kindCode = kind.getCode()
                if (EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(kindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(kindCode))
                {
                    row.bachelors[course - 1]++;
                }
                else if (EduProgramKindCodes.PROGRAMMA_MAGISTRATURY.equals(kindCode))
                {
                    row.masters[(course % 2 == 1) ? 0 : 1]++;
                } else continue;
                row.total++;
                row.completeCompensation += (student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_CONTRACT)) ? 1 : 0;
                row.women += student.getPerson().getIdentityCard().getSex().getCode().equals(SexCodes.FEMALE) ? 1 : 0;
            }
            rows.add(row);

            // добавить количество студентов в "Итого"
            total.add(row);
        }
        Collections.sort(rows);
        rows.add(total);

        final List<String[]> result = new ArrayList<>();
        for (final Row row : rows)
        {
            result.add(row.toStringArray());
        }
        return result.toArray(new String[0][]);
    }

    class Row implements Comparable<Row>
    {
        String title; // наименование направления, специальности
        int[] bachelors = new int[6]; // подготовка специалистов, бакалавров
        int[] masters = new int[2]; // подготовка магистров
        int total; // итого обучается на всех курсах
        int completeCompensation; // из них с полным возмещением затрат на обучение
        int women; // из них женщины

        public Row(final String title)
        {
            this.title = title;
        }

        public String[] toStringArray()
        {
            final String[] result = new String[12];

            result[0] = title;
            final int[] array = new int[11];
            System.arraycopy(bachelors, 0, array, 0, 6);
            System.arraycopy(masters, 0, array, 6, 2);
            int index = 8;
            array[index++] = total;
            array[index++] = completeCompensation;
            array[index++] = women;
            for (int i = 0; i < array.length; i++)
            {
                result[i + 1] = Integer.toString(array[i]);
            }

            return result;
        }

        public void add(final Row row)
        {
            for (int i = 0; i < bachelors.length; i++)
            {
                bachelors[i] += row.bachelors[i];
            }
            for (int i = 0; i < masters.length; i++)
            {
                masters[i] += row.masters[i];
            }
            total += row.total;
            completeCompensation += row.completeCompensation;
            women += row.women;
        }

        @Override
        public int compareTo(final Row o)
        {
            return title.compareTo(o.title);
        }
    }
}