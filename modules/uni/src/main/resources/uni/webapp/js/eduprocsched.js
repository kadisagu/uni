var ONE_DAY_DURATION = 1000 * 60 * 60 * 24;
var ONE_WEEK_DURATION = ONE_DAY_DURATION * 7;
var dayAmountForMonth = new Array(0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

function mod(X, Y)
{
    var t = X % Y;
    return t < 0 ? t + Y : t;
}

function isNumber(data)
{
    var numStr = "0123456789";
    var thisChar;
    var counter = 0;
    for (var i = 0; i < data.length; i++)
    {
        thisChar = data.substring(i, i + 1);
        if (numStr.indexOf(thisChar) != -1) counter++;
    }
    if (counter == data.length) return true;
    return false;
}

function getDaysInMonth(month, year)
{
    if (month > dayAmountForMonth.length + 1) return 0;
    var dayAmount = dayAmountForMonth[month];
    if (month == 2 && mod(year, 4) != 0) dayAmount -= 1;
    return dayAmount;
}

function onClickApply(event)
{
    var target = (event.target) ? event.target : ((event.srcElement) ? event.srcElement : null);
    var parent_id = target.id.replace(/_ba/g, "");
    apply(parent_id);
}

function apply(parent_id)
{
    var parent = document.getElementById(parent_id);
    var textarea = document.getElementById(parent_id + '_ta');

    var div = document.getElementById(parent_id + '_d');
    div.style.visibility = 'hidden';

    if (!isDateStringValid(textarea))
    {
        div.style.visibility = 'visible';
        return;
    }

    document.getElementById(parent_id + '_h').value = textarea.value;
    document.getElementById(parent_id + '_yy').innerHTML = textarea.value;

    parent.removeChild(div);
}

function onClickCancel(event)
{
    var target = (event.target) ? event.target : ((event.srcElement) ? event.srcElement : null);
    var parent_id = target.id.replace(/_bc/g, "");
    cancel(parent_id);
}

function cancel(parent_id)
{
    var parent = document.getElementById(parent_id);
    var div = document.getElementById(parent_id + '_d');
    div.style.visibility = 'hidden';
    parent.removeChild(div);
}

function removePrevDiv(newDivId)
{
    var prev_div_hid = document.getElementById('hidden_inp');
    if (prev_div_hid)
    {
        var prev_div = document.getElementById(prev_div_hid.value);
        if (prev_div)
        {
            prev_div.style.visibility = 'hidden';
            prev_div.parentNode.removeChild(prev_div);
        }
    }
    else
    {
        prev_div_hid = document.createElement('input');
        prev_div_hid.id = 'hidden_inp';
        prev_div_hid.type = 'hidden';
        document.getElementById('rootElem').appendChild(prev_div_hid);
    }
    prev_div_hid.value = newDivId;
}

function isDateStringValid(parent)
{
    var prevErr = document.getElementById('err_div');
    if (prevErr) prevErr.parentNode.removeChild(prevErr);

    parent.value = parent.value.replace(/ /g, "").replace(/\t/g, "").replace(/\n/g, "").replace(/\r/g, "");
    var datesStr = parent.value.split(";");

    if (parent.value.length == 0)
    {
        var weekNumCell = document.getElementById(parent.id.replace(/_ta/g, "") + '_n');
        weekNumCell.innerHTML = "";
        return true;
    }
    var periods = new Array();
    for (var t = 0; t < datesStr.length; t++)
    {
        periods[t] = new Array();
        var period = datesStr[t].split("-");
        if (period.length != 2) return addError(parent, '1');

        var firstDate = null;
        for (var s = 0; s < period.length; s++)
        {
            var singdate = period[s].split(".");
            if (singdate.length != 3) return addError(parent, '1');

            if (singdate[0].length > 2 || singdate[1].length > 2 || singdate[2].length != 4) return addError(parent, '1');
            if (!(isNumber(singdate[0]) && isNumber(singdate[1]) && isNumber(singdate[2]))) return addError(parent, '4');
            if (singdate[0] > getDaysInMonth(parseInt(singdate[1], 10), parseInt(singdate[2], 10))) return addError(parent, '5');
            if (singdate[2] < 1900 || singdate[2] > 2500) return addError(parent, '6');

            var date = new Date(singdate[2], singdate[1] - 1, singdate[0]);
            periods[t][s] = date;
            if (!firstDate) firstDate = date.getTime();
            else if ((date.getTime() - firstDate) / ONE_DAY_DURATION <= 0) return addError(parent, '3');
        }
    }

    for (var i = 0; i < periods.length; i++)
        for (var j = i; j < periods.length; j++)
            if ((i != j) && ((periods[j][0] > periods[i][0] && periods[j][0] < periods[i][1]) || (periods[j][1] > periods[i][0] && periods[j][1] < periods[i][1])))
                return addError(parent, '7');

    var weekNums = "";
    for (var i = 0; i < periods.length; i++)
    {
        weekNums += Math.ceil((periods[i][1] - periods[i][0]) / ONE_WEEK_DURATION);
        if (i < periods.length - 1) weekNums += "; "
    }

    var weekNumCell = document.getElementById(parent.id.replace(/_ta/g, "") + '_n');
    weekNumCell.innerHTML = weekNums;

    parent.value = parent.value.replace(/;/g, "; ");
    return true;
}

function addError(parent, errnum)
{
    var errDiv = document.createElement('div');
    errDiv.id = "err_div";
    errDiv.style.color = "red";
    errDiv.position = "absolute";
    errDiv.width = "100%";
    errDiv.noWrap = "true";

    if (errnum > 0 || errnum < 4) errDiv.innerHTML = errs[errnum];

    parent.parentNode.appendChild(errDiv);

    return false;
}

function setDiv(event)
{
    var parent = (event.target) ? event.target : ((event.srcElement) ? event.srcElement : null);
    parent = document.getElementById(parent.id.replace(/_yy/g, ""));

    if (null == parent) return;
    var rootId = parent.id.replace(/_d/g, "").replace(/_n/g, "").replace(/_ta/g, "").replace(/_ba/g, "").replace(/_bc/g, "");
    if (rootId != parent.id) return;

    if (document.getElementById(parent.id + '_d')) return;

    var div = document.createElement('div');
    div.id = parent.id + '_d';
    div.style.border = "1px solid red";
    div.style.position = "absolute";
    div.style.backgroundColor = "#DDDDFF";
    div.style.width = "200px";
    //div.style.filter = "alpha(opacity=80)";

    removePrevDiv(div.id)

    var textarea = document.createElement('textarea');
    textarea.id = parent.id + "_ta";
    textarea.parentId = parent.id + "_yy";
    textarea.style.width = "100%";
    textarea.style.height = "50px";
    textarea.style.backgroundColor = "white";
    textarea.style.border = "1px solid gray";
    textarea.onkeydown = "smallEditKeyDown(event);";
    textarea.innerHTML = document.getElementById(parent.id + "_yy").innerHTML.replace(/ /g, "").replace(/\t/g, "").replace(/\r/g, "");

    var buttblock = "<table border=0 cellspacing=2 style=\"width: 200px; horizontal-align: center;\"><tr>";
    buttblock += "<td><div class=\"btn action\" style=\"width: 50px;\" id=\"" + parent.id + "_ba\" parentid=\"" + parent.id + "\" onclick=\"onClickApply(event);\">" + butts[0] + "</div></td>";
    buttblock += "<td><div class=\"btn action\" style=\"width: 50px;\" id=\"" + parent.id + "_bc\" parentid=\"" + parent.id + "\" onclick=\"onClickCancel(event);\">" + butts[1] + "</div></td>";
    buttblock += "</td></tr></table>";
    div.appendChild(textarea);
    div.innerHTML += buttblock;

    parent.appendChild(div);
    document.getElementById(textarea.id).focus();
}

function smallEditKeyDown(event)
{
    if (event == null) event = window.event;

    var target = (event.target) ? event.target : ((event.srcElement) ? event.srcElement : null);
    if (target != null && target.parentId != null)
        if (event.keyCode == 13)
        {
            event.cancelBubble = true;
            apply(target.id.replace(/_ta/g, ""));
        }
        else if (event.keyCode == 27)
        {
            event.cancelBubble = true;
            cancel(target.id.replace(/_ta/g, ""));
        }
}