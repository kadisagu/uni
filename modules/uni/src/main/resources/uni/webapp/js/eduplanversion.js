var formTimeout = 60000;

function initVariables()
{
    changedControlFormsMap = {};

    editableColumnsShift = 0;
    controlFormColumnsNumber = 0;
    hourColumnsNumber = 0;
    allocationColumnsNumber = 0;

    controlFormsIndexMap = {};
    hourIndexMap = {};
    allocationIndexMap = {};
    serializedMap = {};
}

initVariables();

function tableMouseDownWithOptions(target, opts)
{
    var div = document.getElementById(target.id + "_ddd");
    if (!div.options || !div.table)
    {
        var table = target;
        if (opts) div.options = opts;
        else div.options = options;
        div.table = table;
    }
    if (!div.filled) fillDivs(div, div.options);
    div.filled = true;
    div.style.visibility = 'visible';
}

function hideOptionsDiv(id)
{
    var tmp = document.getElementById(id);
    if (tmp) tmp.style.visibility = 'hidden';
}

function tableMouseOver(target)
{
    clearTimeout(target.timer);
}

function tableMouseOut(target)
{
    target.timer = setTimeout("hideOptionsDiv('" + target.id + "_ddd')", 400);
}

function fillDivs(div, options)
{
    for (var i = 0; i < options.length; i++)
    {
        var id = options[i][0];
        var title = options[i][1];

        var option = document.createElement('div');

        option.id = id;
        option.div = div.id;
        option.innerHTML = title;
        option.owner = div.table;
        option.style.padding="2px";

        option.onclick = function(event)
        {
            event = event ? event : window.event;
            event.cancelBubble = true;
            return false;
        }

        option.onmouseup = function(event)
        {
            var target = event ? event.target : window.event.srcElement;
            if (!target.owner.prevValue)
                target.owner.prevValue = target.owner.innerHTML.replace(/^\s+|\s+$/g, '').replace('&nbsp;', ''); // trim
            target.owner.innerHTML = target.innerHTML;
            if (!(target.owner.innerHTML === target.owner.prevValue))
                target.owner.style.color="red";
            else
                target.owner.style.color="";
            target.parentNode.style.visibility = 'hidden';
            document.getElementById(div.table.id + "_h").value = target.id;
        }

        option.onmouseover = function(event)
        {
            var target = event ? event.target : window.event.srcElement;
            clearTimeout(target.owner.timer);
            target.style.backgroundColor = '#F1F7FC';
        }

        option.onmouseout = function(event)
        {
            var target = event ? event.target : window.event.srcElement;
            target.owner.timer = setTimeout("hideOptionsDiv('" + target.div + "')", 400);
            target.style.backgroundColor = 'white';
        }

        div.appendChild(option);
    }
}

// Control forms for terms editing logic
function showEditControlFormsInTermWindow(event, discId)
{
	applyOpenedWindow(event);
	
	var map = parseControlActionsMap(discId);
	var div = createMainDiv(event, discId, function(){onClickApplyControlFormsInTerm();}, function(event) {controlFormsEditMouseOut();}, function(event) {controlFormsEditMouseOver();});
	if(div == null) return;
	
	var matrix = "<table border=0 cellspacing=1 cellpadding=0 style=\"width:1px\">";
	matrix += "<tr><td colspan=" + (termsAmount + 1) + " class=\"table-title\">Формы контроля по семестрам</td></tr>";
	matrix += "<tr><td class=\"table-header\"> </td>";
	for(var i = 1; i <= termsAmount; i++)
		matrix += "<td class=\"table-header\" style=\"text-align: center;\">" + i + "</td>";
	matrix += "</tr>";
	
	for(var i = 0; i < controlFormsList.length; i++)
	{
		matrix += "<tr><td class=\"table-header\">" + controlFormsList[i][1] + "</td>";
		for(var j = 1; j <= termsAmount; j++)
			matrix += "<td class=\"epv-table-cell\"><input id=\"ch_" + i + "_" + j + "\" type=\"checkbox\" " + (map[i][j-1] ? "checked" : "") + "/></td>";
		matrix += "</tr>";
	}
	
	matrix += "</table>";
	div.innerHTML = matrix;
	setDivPosition(div);
	clearTimeout(div.parentNode.timer);
}
function parseControlActionsMap(discId)
{
	if(changedControlFormsMap['controlActions'] && changedControlFormsMap['controlActions'][discId])
		return changedControlFormsMap['controlActions'][discId];
	
	var checkBoxMatrix = new Array();
	for(var i = 0; i < controlFormsList.length; i++)
	{
		checkBoxMatrix[i] = new Array();
		for(var j = 0; j < termsAmount; j++)
			checkBoxMatrix[i][j] = false;
	}
	
	var controlActions = controlFormAmountsMap[discId];
	for(var i = 0; i < controlFormsList.length; i++)
	{
		var val = controlActions['a' + controlFormsList[i][0]]; 
		if(val && val.length > 0)
		{
			var valPeriods = val.split(",");
			for(var j = 0; j < valPeriods.length; j++)
			{
				var valCourses = valPeriods[j].split("-");
				if(valCourses.length == 1)
					checkBoxMatrix[i][valCourses[0] - 1] = true;
				else
					for(var k = valCourses[0]; k <= valCourses[1]; k++)
						checkBoxMatrix[i][k - 1] = true;
			}
		}
	}
	
	return checkBoxMatrix;
}

function showEditHourAmountsWindow(event, discId)
{
	applyOpenedWindow(event);
	
	var totalsMap = {};
	if(changedControlFormsMap['totalHours'] && changedControlFormsMap['totalHours'][discId]) totalsMap = changedControlFormsMap['totalHours'][discId];
	else totalsMap = totalHoursMap[discId];
	
	var div = createMainDiv(event, discId, function(){onClickApplyTotalHours();}, function(event) {totalHoursEditMouseOut();}, function(event) {controlFormsEditMouseOver();});
	if(div == null) return;
	
	var matrix = "<table border=0 cellspacing=1 cellpadding=0 style=\"width:1px\">";
	matrix += "<tr><td colspan=2 class=\"table-title\">Общая аудиторная нагрузка</td></tr>";
	
	if(hoursList[hoursList.length - 1] == undefined) hoursList.length--;
	
	for(var i = 0; i < hoursList.length; i++)
	{
		matrix += "<tr><td class=\"table-header\">" + hoursList[i][1] + "</td>";
		matrix += "<td class=\"epv-table-cell\"><input id=\"ed_" + i + "\" type=\"text\" value=\"" + getFormFieldValue(totalsMap[hoursList[i][0]]) + "\" style=\"width:40px;\"/></td></tr>";
	}
	
	matrix += "</table>";
	div.innerHTML = matrix;
	setDivPosition(div);
	clearTimeout(div.parentNode.timer);
}

function showCourseLoadAllocationSimple(event, discId)
{
	applyOpenedWindow(event);
	
	var eduLoadMap = {};
	if(changedControlFormsMap['courseLoadAllocation'] && changedControlFormsMap['courseLoadAllocation'][discId]) eduLoadMap = changedControlFormsMap['courseLoadAllocation'][discId];
	else eduLoadMap = eduLoadAllocMap[discId];
	
	var div = createMainDiv(event, discId, function(){onClickApplyCourseLoadAllocation();}, function(event) {courseLoadAllocationEditMouseOut();}, function(event) {controlFormsEditMouseOver();});
	if(div == null) return;

	var matrix = "<table border=0 cellspacing=1 cellpadding=0 style=\"width:1px\">";
	matrix += "<tr><td colspan=" + (termsAmount + 1) + " class=\"table-title\">Распределение нагрузок по семестрам</td></tr>";
	matrix += "<tr><td class=\"table-header\"> </td>";
	for(var i = 1; i <= termsAmount; i++)
		matrix += "<td class=\"table-header\" style=\"text-align: center;\">" + i + "</td>";
	matrix += "</tr>";
	matrix += "<tr><td class=\"table-header\">Нагрузка</td>";
    for(var i = 1; i <= termsAmount; i++)
	{
		if(eduLoadMap["c"+ i] == undefined) eduLoadMap["c"+ i] = null;
		var value = eduLoadMap["c"+ i];
		if(value == null) value = "";
		matrix += "<td class=\"epv-table-cell\"><input id=\"inp_" + i + "\" type=\"text\" value=\"" + getFormFieldValue(value) + "\" style=\"width:25px;\"/></td>";
	}
	matrix += "</tr>";
	matrix += "</table>";
	div.innerHTML = matrix;

	setDivPosition(div);
	clearTimeout(div.parentNode.timer);
}

function showCourseLoadAllocationExtOutput(event, discId)
{
	applyOpenedWindow(event);
	
	var eduLoadMap = {};
	if(changedControlFormsMap['courseLoadAllocation'] && changedControlFormsMap['courseLoadAllocation'][discId]) eduLoadMap = changedControlFormsMap['courseLoadAllocation'][discId];
	else eduLoadMap = eduLoadAllocMap[discId];
	
	var div = createMainDiv(event, discId, function(){onClickApplyCourseLoadAllocationExt();}, function(event) {courseLoadAllocationExtEditMouseOut();}, function(event) {controlFormsEditMouseOver();});
	if(div == null) return;
	
		
	var matrix = "<table border=0 cellspacing=1 cellpadding=0 style=\"width:1px\">";
	matrix += "<tr><td colspan=" + (termsAmount + 1) + " class=\"table-title\">Распределение нагрузок по семестрам</td></tr>";
	matrix += "<tr><td class=\"table-header\"> </td>";
	for(var i = 1; i <= termsAmount; i++)
		matrix += "<td class=\"table-header\" style=\"text-align: center;\">" + i + "</td>";
	matrix += "</tr>";
	
	for(var i = 1; i <= eduLoadTypesList.length; i++)
	{
		matrix += "<tr><td class=\"table-header\">" + eduLoadTypesList[i-1][1] + "</td>";
		for(var j = 1; j <= termsAmount; j++)
		{
			var value = eduLoadMap["e" + eduLoadTypesList[i-1][0]]["c"+ j];
			if(value == null)
				value = "";
			matrix += "<td class=\"epv-table-cell\"><input id=\"inp_" + i + "_" + j + "\" type=\"text\" value=\"" + getFormFieldValue(value) + "\" style=\"width:25px;\"/></td>";
		}
		matrix += "</tr>";
	}
	
	matrix += "</table>";
	div.innerHTML = matrix;

	setDivPosition(div);
	clearTimeout(div.parentNode.timer);
}

function showCourseLoadAllocationExtInput(event, discId)
{
	showCourseLoadAllocationExtOutput(event, discId);
}

function applyOpenedWindow(event)
{
//	alert("apply to :" + event);
	if(getEditFormDiv() && event)
	{
		var formDiv = getEditFormDiv();
		var target;
		if (event.target) 
			target = event.target;
		else if (event.srcElement) 
			target = event.srcElement;
		if(target != formDiv && target != formDiv.previousSibling && target != formDiv.parentNode && (target.id=='close' || !isEventOnForm(event)))
		{
			clearTimeout(getEditFormDiv().timer);
			formDiv.applyMethod();
		}
	}
}

function isEventOnForm(event)
{
	var formPosition = getElementPosition(getEditFormDiv().firstChild);
	var scroll = getScroll();
	var eventX = event.clientX + scroll.x;
	var eventY = event.clientY + scroll.y;
//	alert("x=" + event.clientX + " scrollX=" + scroll.x + "\ny="+ event.clientY + " scrollY=" + scroll.y + "\nformX=" + formPosition.x + " formY=" + formPosition.y + "\nresult=" + (eventX > formPosition.x && eventX < formPosition.x + formPosition.width && eventY > formPosition.y && eventY < formPosition.y + formPosition.height));
	return (eventX > formPosition.x && eventX < formPosition.x + formPosition.width && eventY > formPosition.y && eventY < formPosition.y + formPosition.height);
}

function createMainDiv(event, discId, applyMethod, onmouseoutMethod, onmouseoverMethod)
{
	if(getEditFormDiv() != null) return null;
	
	var parent;
	if (event.target) parent = event.target;
	else if (event.srcElement) parent = event.srcElement;
	
	if("SPAN" == parent.nodeName) {
		parent = parent.parentNode;
	} else { 
		parent.innerHTML = ("<"+"span>" + parent.innerHTML + "<"+"/span>");
	}
	
	parent.className = parent.className.replace(/-over/g, "");
	
	fillColumnData(parent);
		
	var mainDiv = document.createElement('div');
	mainDiv.id = "edit_form";
	mainDiv.applyMethod = applyMethod;
	mainDiv.discId = discId;
	mainDiv.onClick = parent.onclick;
	mainDiv.style.zIndex = 100;
	mainDiv.style.position = "absolute";
	mainDiv.style.visibility='hidden';
	mainDiv.onmouseout = onmouseoutMethod;
	mainDiv.onmouseover = onmouseoverMethod;

    
    var div = document.createElement('div');
	div.style.border = "1px solid gray";
	div.style.position = "absolute";
	div.style.backgroundColor = "#AFC3D9";
	// div.style.horizontalalign = "center";
	// parent.onclick = function() {};

	mainDiv.appendChild(div);
	parent.appendChild(mainDiv);
	
	if(isInternetExplorer())
		document.onclick = function(){applyOpenedWindow(event)};
	else
		document.onclick = function(event){applyOpenedWindow(event)};
	return div; 
}

function getEditFormDiv()
{
	return document.getElementById('edit_form'); 
}

function getParentRow()
{
	return getEditFormDiv().parentNode.parentNode;
}

function removeEditForm(onclick)
{
//	alert("removing form");
	var div = getEditFormDiv();
	var parent = div.parentNode;
	var className = parent.className;
	parent.removeChild(div);
	parent.onclick = div.onClick;
	parent.className = className.replace(/-over/g, "");
	parent.parentNode.className = parent.parentNode.className.replace(/-over/g, "");
	parent.onMouseOver = "element_over(this);";
	parent.onMouseOut = "element_out(this);";
	document.onclick = null;
}

function getElementPosition(elem)
{
	var width = elem.offsetWidth;
    var height = elem.offsetHeight;
    var x = y = 0;
    while (elem)
    {
        x += elem.offsetLeft;
        y += elem.offsetTop;
        elem = elem.offsetParent;
    }
    return {x:x, y:y, width:width, height:height};
}

function getScroll() {
    var x = y = 0;
    x = (window.scrollX) ? window.scrollX : document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft;
    y = (window.scrollY) ? window.scrollY : document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
    return {x:x, y:y};
}

function getScreenSize() 
{
    var width = (window.innerWidth ? window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth));
    var height = (window.innerHeight ? window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.offsetHeight));
    return {width:width, height:height}; 
}

function setDivPosition(div)
{
	var mainDiv = div.parentNode;
	var divPosition = getElementPosition(div);
	var screenSize = getScreenSize();
	if(divPosition.width > screenSize.width - divPosition.x)
		mainDiv.style.left = (divPosition.x - divPosition.width) + "px";
	if(divPosition.height > screenSize.height - divPosition.y)
		mainDiv.style.top = (divPosition.y - divPosition.height) + "px";
	mainDiv.style.visibility = 'visible';
	
	var tmp = document.createElement('div');
    tmp.style.textAlign="center";
    tmp.style.paddingLeft="7px";
	tmp.innerHTML = "<div id=\"close\" type=\"cancel\" class=\"btn btn\" onclick=\"applyOpenedWindow(event)\">Закрыть</div>"; 
	div.appendChild(tmp);
}

function controlFormsEditMouseOut()
{
    document.getElementById('edit_form').timer = setTimeout("onClickApplyControlFormsInTerm()", formTimeout);
}

function totalHoursEditMouseOut()
{
    document.getElementById('edit_form').timer = setTimeout("onClickApplyTotalHours()", formTimeout);
}

function courseLoadAllocationEditMouseOut()
{
    document.getElementById('edit_form').timer = setTimeout("onClickApplyCourseLoadAllocation()", formTimeout);
}

function courseLoadAllocationExtEditMouseOut()
{
    document.getElementById('edit_form').timer = setTimeout("onClickApplyCourseLoadAllocationExt()", formTimeout);
}

function controlFormsEditMouseOver()
{
    clearTimeout(document.getElementById('edit_form').timer);
}

function isValidValue(value)
{
	var v = getCellValue(value);
	if(isNaN(v) || (v && v < 0))
		return false;
	return true;
}

function createSerializedMap()
{
	var result = "";
	for(key in serializedMap)
	{
		result += "#" + key + serializedMap[key];
	}
	return result;
}

function onClickApplyControlFormsInTerm()
{
//	alert("onClickApplyControlFormsInTerm");
	var modifiedForms = {};
	var anythingChanged = false;
	var modifiedFormsToSend = new Array();
	for(var i = 0; i < controlFormsList.length; i++)
	{
		var periodStr = "";
		var periodStart = 0;
		modifiedFormsToSend[i] = new Array();
		for(var j = 1; j <= termsAmount; j++)
		{
			var checkbox = document.getElementById('ch_' + i + '_' + j);
			modifiedFormsToSend[i][j - 1] = checkbox.checked;
			
			if(periodStart == 0 && checkbox.checked) periodStart = j;
			else if(periodStart > 0 && !checkbox.checked)
			{
				if(periodStr.length > 0) periodStr += ", ";
				if(j - periodStart == 1) periodStr += periodStart;
				else periodStr += periodStart + "-" + (j-1);
				periodStart = 0;
			}
		}
		
		if(periodStart > 0)
		{
			if(periodStr.length > 0) periodStr += ", ";
			if(periodStart == termsAmount) periodStr += periodStart;
			else periodStr += periodStart + "-" + termsAmount;
		}
		
		modifiedForms['a' + controlFormsList[i][0]] = periodStr;
	}
	
	var controlActions = controlFormAmountsMap[getEditFormDiv().discId];
	var childs = getParentRow().childNodes;
	
	var controlFormCodeByTitle = {};
	for(var i = 0; i < controlFormsList.length; i++)
	{
		controlFormCodeByTitle[controlFormsList[i][2]] = controlFormsList[i][0];
	}
	
	for(var item in controlFormsIndexMap)
	{
		index = controlFormsIndexMap[item];
		var currControlFormCode = controlFormCodeByTitle[item];
	
		if(controlActions['a' + currControlFormCode] == undefined) controlActions['a' + currControlFormCode] = "";
		if(controlActions['a' + currControlFormCode] != modifiedForms['a' + currControlFormCode])
		{
			anythingChanged = true;
			changeCellValue(childs[index], modifiedForms['a' + currControlFormCode]);
		}
		else
			recoverCellValue(childs[index], controlActions['a' + currControlFormCode]);
	}

	if(changedControlFormsMap['controlActions'] == undefined) changedControlFormsMap['controlActions'] = {};
	if(anythingChanged)
		changedControlFormsMap['controlActions'][getEditFormDiv().discId] = modifiedFormsToSend;
	else
		changedControlFormsMap['controlActions'][getEditFormDiv().discId] = null;
	
	var serialized ="{";
	for(var i in changedControlFormsMap['controlActions'])
	{
		var arr = changedControlFormsMap['controlActions'][i];
		if (arr) {
			serialized += i + ":";
			for ( var i = 0; i < arr.length; i++)
				for ( var j = 0; j < arr[i].length; j++) {
					if (arr[i][j])
						serialized += '1';
					else
						serialized += '0';
				}
			serialized += ";";
		}
	}
	serialized += "}";
	serializedMap['controlActions'] = serialized;
	document.getElementById('ChangedControlForms_h').value = createSerializedMap(); 
	removeEditForm();
}

function showErrorDiv()
{
	var editFormDiv = getEditFormDiv();
	var div = editFormDiv.firstChild;
	if(div.childNodes.length == 1)
	{
		var errorDiv = document.createElement('div');
		errorDiv.id = "errorContainer";
//		errorDiv.className = "sticky error";
		errorDiv.mctype = "mSticker";
		errorDiv.innerHTML= "Значение должно быть неотрицательным числом";
		errorDiv.style.borderColor = "red";
		errorDiv.style.borderStyle = "solid";
		errorDiv.style.borderWidth = "1px";
		errorDiv.style.backgroundColor = "#ffe2ef";
		errorDiv.style.color = "#004081";
		errorDiv.style.fontWeight = "bold";
		errorDiv.style.padding = "6px";
		var first = div.removeChild(div.firstChild);
		div.appendChild(errorDiv);
		div.appendChild(first);
	}
}

function showErrorsTotalHours(errors)
{
	var errors=[];
	for(var i = 0; i < hoursList.length; i++)
	{
		var textField = document.getElementById('ed_' + i);
		if(!isValidValue(textField.value))
			errors[i] = i;
	}
	if(errors.length > 0)
	{
		showErrorDiv();	
		var tbody = getEditFormDiv().firstChild.firstChild.firstChild;
		for(i in errors)
		{
			var tr = tbody.childNodes[Number(i) + 1];
			if(tr.childNodes && tr.childNodes.length > 1)
			{
				tr.childNodes[1].firstChild.style.backgroundColor="#ffe2ef";
				tr.childNodes[1].firstChild.style.border="1px solid red";
			}
		}
		return true;
	}
	return false;
}

function onClickApplyTotalHours()
{
//	alert("onClickApplyTotalHours");
	if(getEditFormDiv() == null) return;
	if(showErrorsTotalHours()) return;
	
	var modifiedForms = {};
	var modifiedFormsToSend = {};
	var anythingChanged = false;

	for(var i = 0; i < hoursList.length; i++)
	{
		var textField = document.getElementById('ed_' + i);
		modifiedForms[hoursList[i][0]] = textField.value ? getCellValue(textField.value) : 0;
		modifiedFormsToSend[hoursList[i][0]] = textField.value ? getToSendValue(textField.value) : 0;
	}
	
	var totalHours = totalHoursMap[getEditFormDiv().discId];
	var childs = getParentRow().childNodes;
	
	var hoursCodeByTitle = {};
	for(var i = 0; i < hoursList.length; i++)
	{
		hoursCodeByTitle[hoursList[i][2]] = hoursList[i][0];
	}
	
	for(var item in hourIndexMap)
	{
		var index = hourIndexMap[item];
		var currHoursCode = hoursCodeByTitle[item];
	
	    if(Number(totalHours[currHoursCode]) != Number(modifiedFormsToSend[currHoursCode]))
		{
			anythingChanged = true;
			changeCellValue(childs[index], modifiedForms[currHoursCode]);
		}
		else
			recoverCellValue(childs[index], getCellValue(totalHours[currHoursCode]));
	}
	
	if(changedControlFormsMap['totalHours'] == undefined) changedControlFormsMap['totalHours'] = {};
	if(anythingChanged)
		changedControlFormsMap['totalHours'][getEditFormDiv().discId] = modifiedFormsToSend;
	else
		changedControlFormsMap['totalHours'][getEditFormDiv().discId] = null;
	
	var serialized = "{";
	for(var i in changedControlFormsMap['totalHours'])
	{
		var arr = changedControlFormsMap['totalHours'][i];
		if (arr) {
			serialized += (serialized.length > 2 ? ", " : "") + i + ":{";
			for (var j = 0; j < hoursList.length; j++)
				serialized += (j > 0 ? ", " : "") + hoursList[j][0] +":'"+ arr[hoursList[j][0]] +"'";
			serialized += "}";
		}
	}
	serialized += "}";
	serializedMap['totalHours'] = serialized
	
	document.getElementById('ChangedControlForms_h').value = createSerializedMap(); 
	removeEditForm();
}

function showErrorsCourseLoadAllocation(errors)
{
//	alert("showErrorsCourseLoadAllocation");
	var errors=[];
	for(var i = 1; i <= termsAmount; i++)
	{
		var field = document.getElementById('inp_' + i);
		if(!isValidValue(field.value))
			errors[i] = i;
	}
	if(errors.length > 0)
	{
		showErrorDiv();	
		var tr = getEditFormDiv().firstChild.firstChild.firstChild.childNodes[2];
		for(i in errors)
		{
			var td = tr.childNodes[i];
			td.firstChild.style.backgroundColor="#ffe2ef";
			td.firstChild.style.border="1px solid red";
		}
		return true;
	}
	return false;
}

function onClickApplyCourseLoadAllocation()
{
//	alert("onClickApplyCourseLoadAllocation");
	if(showErrorsCourseLoadAllocation()) return;
	
	var modifiedForms = {};
	var modifiedFormsToSend = {};
	var anythingChanged = false;
	
    for(var i = 1; i <= termsAmount; i++)
	{
		var field = document.getElementById('inp_' + i);
		modifiedForms['c' + i] = getCellValue(field.value);
		modifiedFormsToSend['c' + i] = getToSendValue(field.value);
	}
    
	var eduLoads = eduLoadAllocMap[getEditFormDiv().discId];
	var childs = getParentRow().childNodes;
	for(var term in allocationIndexMap)
	{
		var index = allocationIndexMap[term];
		if(Number(eduLoads['c' + term]) != Number(modifiedFormsToSend['c' + term]))
		{
			anythingChanged = true;
			changeCellValue(childs[index], evaluateLoading(modifiedForms['c' + term], term));
		}
		else
			recoverCellValue(childs[index], getCellValue(evaluateLoading(eduLoads['c' + term], term)));
	}
	
	if(changedControlFormsMap['courseLoadAllocation'] == undefined) changedControlFormsMap['courseLoadAllocation'] = {};
	if(anythingChanged)
		changedControlFormsMap['courseLoadAllocation'][getEditFormDiv().discId] = modifiedFormsToSend;
	else
		changedControlFormsMap['courseLoadAllocation'][getEditFormDiv().discId] = null;
	
	var serialized = "{";
	for(var i in changedControlFormsMap['courseLoadAllocation'])
	{
		var arr = changedControlFormsMap['courseLoadAllocation'][i];
		if (arr) {
			serialized += (serialized.length > 2 ? ", " : "") + i + ":{";
			for ( var i = 1; i <= termsAmount; i++)
			{
				serialized += (i > 1 ? ", " : "") + "c" + i +":";
				if (arr['c' + i])
					serialized += arr['c' + i];
				else
					serialized += "null";
			}
			serialized += "}"
		}
	}
	serialized += "}";
	serializedMap['courseLoadAllocation'] = serialized;
	document.getElementById('ChangedControlForms_h').value = createSerializedMap(); 
	removeEditForm(); 
}

function showErrorsCourseLoadAllocationExt(errors)
{
	var errors=[];
	for(var i = 1; i <= eduLoadTypesList.length; i++)
	{
		for(var j = 1; j <= termsAmount; j++)
		{
			var field = document.getElementById('inp_' + i + '_' +j);
			if(!isValidValue(field.value))
			{
				if(errors[i-1] == undefined) errors[i-1] = [];
				errors[i-1][j-1] = 1;
			}
		}
	}
	if(errors.length > 0)
	{
		showErrorDiv();	
		var tbody = getEditFormDiv().firstChild.firstChild;
		for(i in errors)
		{
			for(j in errors[i])
			{
				var td = tbody.firstChild.childNodes[Number(i) + 2].childNodes[Number(j) + 1].firstChild;
				td.style.backgroundColor="#ffe2ef";
				td.style.border="1px solid red";
			}
		}
		return true;
	}
	return false;
}

function onClickApplyCourseLoadAllocationExt()
{
//	alert("onClickApplyCourseLoadAllocationExt");
	if(showErrorsCourseLoadAllocationExt()) return;
	
	var modifiedForms = {};
	var modifiedFormsToSend = {};
	var anythingChanged = false;
	for(var i = 1; i <= eduLoadTypesList.length; i++)
	{
		modifiedForms['e' + eduLoadTypesList[i-1][0]] = {};
		modifiedFormsToSend['e' + eduLoadTypesList[i-1][0]] = {};
		for(var j = 1; j <= termsAmount; j++)
		{
			var field = document.getElementById('inp_' + i + '_' +j);
			modifiedForms['e' + eduLoadTypesList[i-1][0]]['c' + j] = getCellValue(field.value);
			modifiedFormsToSend['e' + eduLoadTypesList[i-1][0]]['c' + j] = getToSendValue(field.value);
		}
	}
	
	var childs = getParentRow().childNodes;
	var key = 'showCourseLoadAllocationExtOutput(';
	for(var i = 0; i < childs.length; i++)
	{
		var onClickAttr = childs[i].getAttribute('onclick');
		if(onClickAttr && onClickAttr.toString().indexOf('showCourseLoadAllocationExtInput(') >= 0)
		{
			key = 'showCourseLoadAllocationExtInput(';
			break;
		}
	}
	
	var eduLoadCodeByTitle = {};
	for(var i = 0; i < eduLoadTypesList.length; i++)
	{
		eduLoadCodeByTitle[eduLoadTypesList[i][2]] = eduLoadTypesList[i][0];
	}
	
	var eduLoads = eduLoadAllocMap[getEditFormDiv().discId];
	if(key == 'showCourseLoadAllocationExtOutput(')
	{
		for(var item in allocationIndexMap)
		{
			var index = allocationIndexMap[item];
			
			var title = item.substring(0, item.indexOf('#'));
			var term = item.substr(item.indexOf('#') + 1);
			var eduLoadTypeCode = eduLoadCodeByTitle[title];
		
			if(Number(eduLoads['e' + eduLoadTypeCode]['c' + term]) != Number(modifiedFormsToSend['e' + eduLoadTypeCode]['c' + term]))
			{
				anythingChanged = true;
				changeCellValue(childs[index], evaluateLoading(modifiedForms['e' + eduLoadTypeCode]['c' + term], term));
			}
			else
			{
				recoverCellValue(childs[index], getCellValue(evaluateLoading(eduLoads['e' + eduLoadTypeCode]['c' + term], term)));
			}
		}
	}
	else if(key == 'showCourseLoadAllocationExtInput(')
	{
		for(var term in allocationIndexMap)
		{
			var index = allocationIndexMap[term];
			
			var anythingChangedInLoadType = false;
			var sumByLoadType = 0;
			var modifiedSumByLoadType = 0;
			for(var j = 0; j < eduLoadTypesList.length; j++)
			{
				if(Number(eduLoads['e' + eduLoadTypesList[j][0]]['c' + term]) != Number(modifiedFormsToSend['e' + eduLoadTypesList[j][0]]['c' + term]))
				{
					anythingChanged = true;
					anythingChangedInLoadType = true;
				}
				sumByLoadType += Number(evaluateLoading(eduLoads['e' + eduLoadTypesList[j][0]]['c' + term], term));
				modifiedSumByLoadType += Number(evaluateLoading(modifiedFormsToSend['e' + eduLoadTypesList[j][0]]['c' + term], term));
			}
			if(anythingChangedInLoadType)
				changeCellValue(childs[index], getCellValue(modifiedSumByLoadType));
			else
				recoverCellValue(childs[index], getCellValue(sumByLoadType));
		}
	}
		
	if(changedControlFormsMap['courseLoadAllocation'] == undefined) changedControlFormsMap['courseLoadAllocation'] = {};
	if(anythingChanged)
		changedControlFormsMap['courseLoadAllocation'][getEditFormDiv().discId] = modifiedFormsToSend;
	else
		changedControlFormsMap['courseLoadAllocation'][getEditFormDiv().discId] = null;
	
	var serialized = "{";
	for(var i in changedControlFormsMap['courseLoadAllocation'])
	{
		var arr = changedControlFormsMap['courseLoadAllocation'][i];
		if (arr) {
			serialized += (serialized.length > 2 ? ", " : "") + i + ":{";
			for ( var i = 1; i <= eduLoadTypesList.length; i++)
			{
				serialized += (i > 1 ? ", " : "") + "e" + eduLoadTypesList[i-1][0] +":{";
				for ( var j = 1; j <= termsAmount; j++)
				{
					serialized += (j > 1 ? ", " : "") + "c" + j +":";
					if (arr['e' + eduLoadTypesList[i-1][0]]['c' + j])
						serialized += arr['e' + eduLoadTypesList[i-1][0]]['c' + j];
					else
						serialized += "null";
				}
				serialized += "}"
			}
			serialized += "}"
		}
	}
	serialized += "}";
	serializedMap['courseLoadAllocation'] = serialized;
	document.getElementById('ChangedControlForms_h').value = createSerializedMap(); 
	removeEditForm();
}

function evaluateLoading(value, term)
{
	if(!value || term < 1 || term > termsAmount || weeksAmountByTermList.length == 0) 
		return value;
	return value * weeksAmountByTermList[term - 1];
}

function getCellValue(value)
{
	if(value == null || value == "") return "";
	if(String(value).indexOf(",") > 0 ) value = String(value).replace(",", ".");
	return Math.round(value);
}

function getFormFieldValue(value)
{
	if(value == null) return "";
	value = String(value);
	if(value.indexOf(",") > 0 ) value = value.replace(",", ".");
	if(value.indexOf(".0") == value.length-2) value = value.replace(".0", "");
	return Math.round(value*100)/100;
}

function getToSendValue(value)
{
	value = String(value);
	if(value == "") return null;
	if(value.indexOf(",") > 0 ) value = value.replace(",", ".");
	return Math.round(value*100)/100;
}

function getCellTextNode(cell) 
{
	var n = cell.firstChild;
	if (n) {
	 if (n.nodeName=='SPAN') { return n; }
	 cell.innerHTML = ("<"+"span>" + "<"+"/span>");
	} else {
 	 cell.innerHTML = ("<"+"span>" + cell.innerHTML + "<"+"/span>");
	}
	return cell.firstChild;
}

function changeCellValue(cell, newVal)
{
	var c =  getCellTextNode(cell);
	c.style.color = "#FF0000";
	c.innerHTML = (String(newVal) == "") ? "--" : newVal;
	document.getElementById('apply_changes_top').style.visibility="visible";
	document.getElementById('apply_changes_bottom').style.visibility="visible";
}

function recoverCellValue(cell, val)
{
	var c =  getCellTextNode(cell);
	c.style.color = null;
	c.innerHTML = val;
}

function onClickCancelEdit(event)
{
	removeEditForm();
}

function getVerticalColumnTitle(columnStr)
{
	var str = columnStr.substring(columnStr.indexOf('title=') + 6);
	if(str.indexOf('"') == 0)
	{
		var en = str.indexOf('"', 1);
		return str.substring(1, en);
	}
	else
	{
		var en = str.indexOf(' ');
		if(en < 0) en = str.indexOf('>');
		return str.substring(0, en);
	}
}

function fillColumnData(parent)
{
	var str = "";
	controlFormsIndexMap = {};
	var totalShift = 0;
	var tblContentNode = parent.parentNode.parentNode; 
	var childs1 = tblContentNode.rows[0].childNodes;
	var childs2 = tblContentNode.rows[1].childNodes;
	var childs3 = tblContentNode.rows[2].childNodes;
	var childs4 = tblContentNode.rows[3].childNodes;
	for(var i = 0; i < childs1.length; i++)
	{
		var c1html = childs1[i].innerHTML; 
		if(c1html.indexOf("Формы контроля по семестрам") >= 0)
		{
			totalShift = i;
			controlFormColumnsNumber = childs1[i].getAttribute('colspan'); 
			str += controlFormColumnsNumber + "(0): ";
			for(var j = 0; j < controlFormColumnsNumber; j++)
			{
				str += getVerticalColumnTitle(childs2[j].innerHTML) + "(" + (j + totalShift) + "), ";
				controlFormsIndexMap[getVerticalColumnTitle(childs2[j].innerHTML)] = Number(j) + Number(totalShift);
			}
			str += "\r\n";
		}
			
		if(c1html.indexOf("Часов")>=0)
		{
			var audShift = 0;
			hourColumnsNumber = childs1[i].getAttribute('colspan');
			str += hourColumnsNumber + "(" + hourColumnsNumber + "): ";
			
			var localShift = Number(controlFormColumnsNumber);
			var hours = hourColumnsNumber;
			for(var j = localShift; j <= localShift + hours; j++)
			{
				var childStr = childs2[j].innerHTML;
				if(childStr.indexOf("Аудиторных")>=0)
				{
					audShift = Number(childs2[j].getAttribute('colspan'));
					for(var k = 0; k < audShift; k++)
					{
						hourIndexMap[getVerticalColumnTitle(childs3[k].innerHTML)] = j + k + totalShift
						str += getVerticalColumnTitle(childs3[k].innerHTML) + "(" + (j+k+totalShift) + "), "; 
					}
					hours -= audShift;
				}
				else
				{
					var index = j > localShift ? j + audShift - 1 : j
					hourIndexMap[getVerticalColumnTitle(childs2[j].innerHTML)] = index + totalShift;
					str += getVerticalColumnTitle(childs2[j].innerHTML) + "(" + (index + totalShift) + "), "; 
				}
			}
			totalShift += Number(localShift) + Number(hourColumnsNumber);
			str += "\r\n";
		}
		
		if(c1html.indexOf("Распределение по курсам")>=0)
		{
			var index = 0; 
			allocationColumnsNumber = childs1[i].getAttribute('colspan') 
			str += allocationColumnsNumber + "(" + (Number(controlFormColumnsNumber) + Number(hourColumnsNumber)) + "): ";
			
			if(childs4.length > 0 && getVerticalColumnTitle(childs4[0].innerHTML) != "")
			{
				//TODO: extended scheme
				var shift = 0;
				for(var j = audShift; j < childs3.length; j++)//по семестрам
				{
					var termShift = childs3[j].getAttribute('colspan'); 
					for(var k = 0; k < termShift; k++)
					{
						var course = childs3[j].firstChild.innerHTML.substring(0, childs3[j].firstChild.innerHTML.indexOf(' '));
						allocationIndexMap[getVerticalColumnTitle(childs4[k + shift].innerHTML)+ "#" + course] = index + totalShift;
						str += getVerticalColumnTitle(childs4[k + shift].innerHTML) + "#" + course + "(" + (index + totalShift) + "), ";
						index++;
					}
					shift += Number(termShift);
				}
			}
			else
			{
				//TODO: simple scheme
				for(var j = audShift; j < childs3.length; j++)// Лекции, Лаб. зан-я 
				{
					var course = childs3[j].firstChild.innerHTML.substring(0, childs3[j].firstChild.innerHTML.indexOf(' '));
					allocationIndexMap[course] = index + totalShift;
					str += course + "(" + (index + totalShift) + "), ";
					index++;
				}
			}
			str += "\r\n";
		}
	}
//	alert(str);
}

function isInternetExplorer()
{
    var ua = navigator.userAgent.toLowerCase();
	if (ua.indexOf("msie") >= 0 && ua.indexOf("opera") < 0 && ua.indexOf("webtv") < 0) 
		return true;
	return false;
}

function isOpera()
{
	var ua = navigator.userAgent.toLowerCase();
	if (ua.indexOf("opera") >= 0) 
		return true;
	return false;
}