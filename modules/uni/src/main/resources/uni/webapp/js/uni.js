/**
 * Всем обездоленным посвящается. Сюда можно складывать скриптовые функции, которые трудно отнести к какой-либо
 * из уже существующих категорий, но слишком малы для создания обособленного файла. 
 */

function searchList_hierarchyCheckBoxColumn_clickEvent(event, checkbox, baseName, columnNumber, rowEntityId) 
{
	if ( checkbox.readonly ) 
	{
		checkbox.checked = true;
	} 
	else 
	{
		var checked = checkbox.checked;
    	var treeIterator = new SearchListTreeIterator(baseName, rowEntityId);
    	while( treeIterator.next(true) )  {
        	var childCheckbox = dojo.byId(baseName + "_" + columnNumber + "__" + treeIterator.childEntityId);
        	if( childCheckbox ) {
        		childCheckbox.readonly = checked; 
            	childCheckbox.checked = checked;
        	}
    	}
	}
};



/**
 * Could be used in thе scriptResolver as the frame, which shows some notification by the mose click.
 * It appears when you click on the searchlist cell, and disappears when mouse cursor move out from the notification frame. 
 */
//function showErrFrame(event, errMsg)
//{
//	var parent;
//	if (event.target) parent = event.target;
//	else if (event.srcElement) parent = event.srcElement;
//
//    alert(parent.parentNode.title);
//
//    parent.parentNode.parentNode.title="";
//
//	if(parent.id == "err_frame" || parent.parentNode.id == "err_frame" || parent.parentNode.parentNode.id == "err_frame") return;
//	killErrFrame();
//
//	var mainDiv = document.createElement('div');
//	mainDiv.id = "err_frame";
//	mainDiv.style.zIndex = 100;
//	mainDiv.style.position = "absolute";
//	mainDiv.onClick = function(){};
//	mainDiv.onmouseout = function(){errMsgMouseOut();};
//	mainDiv.onmouseover = function(){errMsgMouseOver();};
//	mainDiv.timer = setTimeout("killErrFrame()", 1500);
//
//	var div = document.createElement('div');
//	div.className = "tooltip-info";
//	div.style.position = "absolute";
//	div.style.width = "200px";
//	div.style.cursor = "text";
//	div.onClick = function(){};
//	div.innerHTML = "<div style=\"margin:5px;\">" + errMsg + "</div>";
//
//	mainDiv.appendChild(div);
//	parent.appendChild(mainDiv);
//}
//
//function killErrFrame()
//{
//	var div = document.getElementById('err_frame')
//	if(div) div.parentNode.removeChild(div);
//}
//
//function errMsgMouseOut()
//{
//    document.getElementById('err_frame').timer = setTimeout("killErrFrame()", 1500);
//}
//
//function errMsgMouseOver()
//{
//    clearTimeout(document.getElementById('err_frame').timer);
//}
