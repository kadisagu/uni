/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.EducationLevelViewObjectPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");

    static
    {
       // _orderSettings.setOrders(DevelopPeriod.title().s(), new OrderDescription("e", DevelopPeriod.priority().s()));
         _orderSettings.setOrders("developPeriod.title", new OrderDescription("e", "developPeriod.priority"));
    }

    @Override
    public void prepare(Model model)
    {
        model.setEducationLevelHighSchool(get(EducationLevelsHighSchool.class, model.getEducationLevelHighSchoolId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<EducationOrgUnit> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelHighSchool()));

         _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    @Override
    public void updateUsedOption(Long educationOrgUnitId)
    {
        EducationOrgUnit educationOrgUnit = getNotNull(EducationOrgUnit.class, educationOrgUnitId);
        educationOrgUnit.setUsed(!educationOrgUnit.isUsed());
        update(educationOrgUnit);
    }
}
