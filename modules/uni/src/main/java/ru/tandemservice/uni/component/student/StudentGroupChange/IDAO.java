/**
 * $Id$
 */
package ru.tandemservice.uni.component.student.StudentGroupChange;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 02.07.2008
 */
public interface IDAO extends IUniDao<Model>
{
}