package ru.tandemservice.uni.sec;

import org.tandemframework.core.auth.IAuthService;
import org.tandemframework.core.sec.IPrincipal;

/**
 * @author vdanilov
 * TODO: это копия SimpleAuthService
 */
public class UniDBAuthService implements IAuthService {

    @Override
    public boolean authenticate(final IPrincipal principal, final String password)
    {
        return principal.isPasswordEquals(password);
    }

}
