/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author vip_delete
 */
public class DatabaseDropAnalyzer
{
    public static void main(String[] args) throws Exception
    {
        Map<String, Set<String>> data = new HashMap<>();

        BufferedReader f = new BufferedReader(new InputStreamReader(new FileInputStream("c:/in.txt")));

        String line;
        String entity = null;
        while ((line = f.readLine()) != null)
        {
            line = line.trim();
            if (line.length() == 0)
                entity = null;
            else if (entity == null)
                entity = line;
            else
            {
                Set<String> list = data.get(entity);
                if (list == null)
                {
                    list = new HashSet<>();
                    data.put(entity, list);
                }
                list.add(line);
            }
        }

        for (Map.Entry<String, Set<String>> entry : data.entrySet())
        {
            Set<String> toExclude = new HashSet<>();
            for (String ref : entry.getValue())
                if (!data.containsKey(ref))
                    toExclude.add(ref);

            entry.getValue().removeAll(toExclude);
            entry.getValue().remove(entry.getKey());
        }

        Map<String, Set<String>> newData = new HashMap<>();
        for (Map.Entry<String, Set<String>> entry : data.entrySet())
        {
            for (String value : entry.getValue())
            {
                Set<String> set = newData.get(value);
                if (set == null)
                {
                    set = new HashSet<>();
                    newData.put(value, set);
                }
                set.add(entry.getKey());
            }
        }

        while (data.size() > 0)
        {
            Set<String> toExclude = new HashSet<>();
            for (Map.Entry<String, Set<String>> entry : data.entrySet())
            {
                if (!newData.containsKey(entry.getKey()))
                {
                    toExclude.add(entry.getKey());
                    System.out.println("tool.executeUpdate(\"delete from " + entry.getKey() + "_t\");");
                }
            }
            for (Map.Entry<String, Set<String>> entry : newData.entrySet())
            {
                entry.getValue().removeAll(toExclude);
            }
            for (String exclude : toExclude)
                data.remove(exclude);
            toExclude.clear();
            for (Map.Entry<String, Set<String>> entry : newData.entrySet())
            {
                if (entry.getValue().size() == 0)
                    toExclude.add(entry.getKey());
            }
            for (String exclude : toExclude)
                newData.remove(exclude);
        }
    }
}
