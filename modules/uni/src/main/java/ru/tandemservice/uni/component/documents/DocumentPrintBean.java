/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.documents;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;

import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

/**
 * @author vip_delete
 * @since 16.10.2009
 */
public abstract class DocumentPrintBean<Model extends DocumentAddBaseModel> implements IPrintFormCreator<Model>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Model model)
    {
        RtfDocument document = createTemplateDocument(template, model);

        createInjectModifier(model).modify(document);

        createTableModifier(model).modify(document);

        return document;
    }

    protected RtfDocument createTemplateDocument(byte[] template, Model model)
    {
        return new RtfReader().read(template);
    }

    protected RtfInjectModifier createInjectModifier(Model model)
    {
        return new RtfInjectModifier();
    }

    protected RtfTableModifier createTableModifier(Model model)
    {
        return new RtfTableModifier();
    }
}
