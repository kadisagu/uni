/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitKindSettings;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<OrgUnitKind> dataSource = model.getDataSource();

        List<OrgUnitKind> orgUnitKindList = getCatalogItemList(OrgUnitKind.class);

        dataSource.setCountRow(orgUnitKindList.size());
        dataSource.setTotalSize(orgUnitKindList.size());
        dataSource.createPage(orgUnitKindList);
    }

    @Override
    public void update(Model model)
    {
        OrgUnitKind orgUnitKind = get(OrgUnitKind.class, model.getOrgUnitKindId());
        orgUnitKind.setAllowStudents(!orgUnitKind.isAllowStudents());
        update(orgUnitKind);
    }

    @Override
    public void updateAllowGroups(Model model)
    {
        OrgUnitKind orgUnitKind = get(OrgUnitKind.class, model.getOrgUnitKindId());
        orgUnitKind.setAllowGroups(!orgUnitKind.isAllowGroups());
        update(orgUnitKind);
    }
}
