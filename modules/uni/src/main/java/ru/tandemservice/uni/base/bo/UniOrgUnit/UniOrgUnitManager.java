/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.uni.base.bo.UniOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.uni.UniDefines;

/**
 * @author Vasily Zhukov
 * @since 12.03.2012
 */
@Configuration
public class UniOrgUnitManager extends BusinessObjectManager
{
    public static UniOrgUnitManager instance()
    {
        return instance(UniOrgUnitManager.class);
    }

    @Bean
    @SuppressWarnings("deprecation")
    public IDefaultComboDataSourceHandler formativeOrgUnitComboDSHandler()
    {
        return new ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }

    @Bean
    @SuppressWarnings("deprecation")
    public IDefaultComboDataSourceHandler territorialOrgUnitComboDSHandler()
    {
        return new ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL);
    }

    @Bean
    @SuppressWarnings("deprecation")
    public IDefaultComboDataSourceHandler producingOrgUnitComboDSHandler()
    {
        return new ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING);
    }
}
