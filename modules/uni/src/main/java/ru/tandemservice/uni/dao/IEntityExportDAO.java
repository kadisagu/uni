/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import java.util.Iterator;

import org.tandemframework.core.entity.IEntity;

/**
 * @author agolubenko
 * @since May 11, 2010
 */
public interface IEntityExportDAO extends IUniBaseDao
{
    EntityExporter<IEntity> createEntityExporter(String className);

    /**
     * Интерфейс, для экспорта entity определенного типа из uni
     * @author agolubenko
     * @since May 11, 2010
     */
    public interface EntityExporter<T extends IEntity>
    {
        /**
         * @return итератор по элементам, доступным для экспорта
         */
        Iterator<T> iterate();
    }
}
