/* $Id: IDAO.java 25247 2012-12-12 09:25:55Z vdanilov $ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3s.EducationLevelHighGos3sItemPub;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3s;

/**
 * @author Vasily Zhukov
 * @since 18.11.2011
 */
public interface IDAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsItemPub.IDAO<EducationLevelHighGos3s, Model>
{
}
