package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Структура направлений подготовки (специальностей) Минобрнауки РФ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StructureEducationLevelsGen extends EntityBase
 implements INaturalIdentifiable<StructureEducationLevelsGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.StructureEducationLevels";
    public static final String ENTITY_NAME = "structureEducationLevels";
    public static final int VERSION_HASH = -649168608;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ALLOW_STUDENTS = "allowStudents";
    public static final String P_ALLOW_GROUPS = "allowGroups";
    public static final String P_BASIC = "basic";
    public static final String P_MIDDLE = "middle";
    public static final String P_HIGH = "high";
    public static final String P_HIGHER = "higher";
    public static final String P_ADDITIONAL = "additional";
    public static final String P_GOS2 = "gos2";
    public static final String P_GOS3 = "gos3";
    public static final String P_SPECIALTY = "specialty";
    public static final String P_SPECIALIZATION = "specialization";
    public static final String P_MASTER = "master";
    public static final String P_BACHELOR = "bachelor";
    public static final String P_PROFILE = "profile";
    public static final String P_QUALIFICATION_REQUIRED = "qualificationRequired";
    public static final String L_PARENT = "parent";
    public static final String P_PRIORITY = "priority";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _allowStudents;     // Обучение студентов осуществляется
    private boolean _allowGroups;     // Обучение групп осуществляется
    private boolean _basic;     // НПО
    private boolean _middle;     // СПО
    private boolean _high;     // ВПО
    private boolean _higher;     // Кадры высшей квалификации
    private boolean _additional;     // ДПО
    private boolean _gos2;     // ГОС2
    private boolean _gos3;     // ФГОС
    private boolean _specialty;     // Специальности
    private boolean _specialization;     // Специализации
    private boolean _master;     // Магистры
    private boolean _bachelor;     // Бакалавры
    private boolean _profile;     // Профили
    private boolean _qualificationRequired;     // Квалификация обязательна
    private StructureEducationLevels _parent;     // Структура направлений подготовки (специальностей) Минобрнауки РФ
    private int _priority;     // Приоритет
    private String _shortTitle;     // Сокращенное название
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Обучение студентов осуществляется. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowStudents()
    {
        return _allowStudents;
    }

    /**
     * @param allowStudents Обучение студентов осуществляется. Свойство не может быть null.
     */
    public void setAllowStudents(boolean allowStudents)
    {
        dirty(_allowStudents, allowStudents);
        _allowStudents = allowStudents;
    }

    /**
     * @return Обучение групп осуществляется. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowGroups()
    {
        initLazyForGet("allowGroups");
        return _allowGroups;
    }

    /**
     * @param allowGroups Обучение групп осуществляется. Свойство не может быть null.
     */
    public void setAllowGroups(boolean allowGroups)
    {
        initLazyForSet("allowGroups");
        dirty(_allowGroups, allowGroups);
        _allowGroups = allowGroups;
    }

    /**
     * @return НПО. Свойство не может быть null.
     */
    @NotNull
    public boolean isBasic()
    {
        return _basic;
    }

    /**
     * @param basic НПО. Свойство не может быть null.
     */
    public void setBasic(boolean basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return СПО. Свойство не может быть null.
     */
    @NotNull
    public boolean isMiddle()
    {
        return _middle;
    }

    /**
     * @param middle СПО. Свойство не может быть null.
     */
    public void setMiddle(boolean middle)
    {
        dirty(_middle, middle);
        _middle = middle;
    }

    /**
     * @return ВПО. Свойство не может быть null.
     */
    @NotNull
    public boolean isHigh()
    {
        return _high;
    }

    /**
     * @param high ВПО. Свойство не может быть null.
     */
    public void setHigh(boolean high)
    {
        dirty(_high, high);
        _high = high;
    }

    /**
     * @return Кадры высшей квалификации. Свойство не может быть null.
     */
    @NotNull
    public boolean isHigher()
    {
        return _higher;
    }

    /**
     * @param higher Кадры высшей квалификации. Свойство не может быть null.
     */
    public void setHigher(boolean higher)
    {
        dirty(_higher, higher);
        _higher = higher;
    }

    /**
     * @return ДПО. Свойство не может быть null.
     */
    @NotNull
    public boolean isAdditional()
    {
        return _additional;
    }

    /**
     * @param additional ДПО. Свойство не может быть null.
     */
    public void setAdditional(boolean additional)
    {
        dirty(_additional, additional);
        _additional = additional;
    }

    /**
     * @return ГОС2. Свойство не может быть null.
     */
    @NotNull
    public boolean isGos2()
    {
        return _gos2;
    }

    /**
     * @param gos2 ГОС2. Свойство не может быть null.
     */
    public void setGos2(boolean gos2)
    {
        dirty(_gos2, gos2);
        _gos2 = gos2;
    }

    /**
     * @return ФГОС. Свойство не может быть null.
     */
    @NotNull
    public boolean isGos3()
    {
        return _gos3;
    }

    /**
     * @param gos3 ФГОС. Свойство не может быть null.
     */
    public void setGos3(boolean gos3)
    {
        dirty(_gos3, gos3);
        _gos3 = gos3;
    }

    /**
     * @return Специальности. Свойство не может быть null.
     */
    @NotNull
    public boolean isSpecialty()
    {
        return _specialty;
    }

    /**
     * @param specialty Специальности. Свойство не может быть null.
     */
    public void setSpecialty(boolean specialty)
    {
        dirty(_specialty, specialty);
        _specialty = specialty;
    }

    /**
     * @return Специализации. Свойство не может быть null.
     */
    @NotNull
    public boolean isSpecialization()
    {
        return _specialization;
    }

    /**
     * @param specialization Специализации. Свойство не может быть null.
     */
    public void setSpecialization(boolean specialization)
    {
        dirty(_specialization, specialization);
        _specialization = specialization;
    }

    /**
     * @return Магистры. Свойство не может быть null.
     */
    @NotNull
    public boolean isMaster()
    {
        return _master;
    }

    /**
     * @param master Магистры. Свойство не может быть null.
     */
    public void setMaster(boolean master)
    {
        dirty(_master, master);
        _master = master;
    }

    /**
     * @return Бакалавры. Свойство не может быть null.
     */
    @NotNull
    public boolean isBachelor()
    {
        return _bachelor;
    }

    /**
     * @param bachelor Бакалавры. Свойство не может быть null.
     */
    public void setBachelor(boolean bachelor)
    {
        dirty(_bachelor, bachelor);
        _bachelor = bachelor;
    }

    /**
     * @return Профили. Свойство не может быть null.
     */
    @NotNull
    public boolean isProfile()
    {
        return _profile;
    }

    /**
     * @param profile Профили. Свойство не может быть null.
     */
    public void setProfile(boolean profile)
    {
        dirty(_profile, profile);
        _profile = profile;
    }

    /**
     * @return Квалификация обязательна. Свойство не может быть null.
     */
    @NotNull
    public boolean isQualificationRequired()
    {
        initLazyForGet("qualificationRequired");
        return _qualificationRequired;
    }

    /**
     * @param qualificationRequired Квалификация обязательна. Свойство не может быть null.
     */
    public void setQualificationRequired(boolean qualificationRequired)
    {
        initLazyForSet("qualificationRequired");
        dirty(_qualificationRequired, qualificationRequired);
        _qualificationRequired = qualificationRequired;
    }

    /**
     * @return Структура направлений подготовки (специальностей) Минобрнауки РФ.
     */
    public StructureEducationLevels getParent()
    {
        return _parent;
    }

    /**
     * @param parent Структура направлений подготовки (специальностей) Минобрнауки РФ.
     */
    public void setParent(StructureEducationLevels parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StructureEducationLevelsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StructureEducationLevels)another).getCode());
            }
            setAllowStudents(((StructureEducationLevels)another).isAllowStudents());
            setAllowGroups(((StructureEducationLevels)another).isAllowGroups());
            setBasic(((StructureEducationLevels)another).isBasic());
            setMiddle(((StructureEducationLevels)another).isMiddle());
            setHigh(((StructureEducationLevels)another).isHigh());
            setHigher(((StructureEducationLevels)another).isHigher());
            setAdditional(((StructureEducationLevels)another).isAdditional());
            setGos2(((StructureEducationLevels)another).isGos2());
            setGos3(((StructureEducationLevels)another).isGos3());
            setSpecialty(((StructureEducationLevels)another).isSpecialty());
            setSpecialization(((StructureEducationLevels)another).isSpecialization());
            setMaster(((StructureEducationLevels)another).isMaster());
            setBachelor(((StructureEducationLevels)another).isBachelor());
            setProfile(((StructureEducationLevels)another).isProfile());
            setQualificationRequired(((StructureEducationLevels)another).isQualificationRequired());
            setParent(((StructureEducationLevels)another).getParent());
            setPriority(((StructureEducationLevels)another).getPriority());
            setShortTitle(((StructureEducationLevels)another).getShortTitle());
            setTitle(((StructureEducationLevels)another).getTitle());
        }
    }

    public INaturalId<StructureEducationLevelsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StructureEducationLevelsGen>
    {
        private static final String PROXY_NAME = "StructureEducationLevelsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StructureEducationLevelsGen.NaturalId) ) return false;

            StructureEducationLevelsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StructureEducationLevelsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StructureEducationLevels.class;
        }

        public T newInstance()
        {
            return (T) new StructureEducationLevels();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "allowStudents":
                    return obj.isAllowStudents();
                case "allowGroups":
                    return obj.isAllowGroups();
                case "basic":
                    return obj.isBasic();
                case "middle":
                    return obj.isMiddle();
                case "high":
                    return obj.isHigh();
                case "higher":
                    return obj.isHigher();
                case "additional":
                    return obj.isAdditional();
                case "gos2":
                    return obj.isGos2();
                case "gos3":
                    return obj.isGos3();
                case "specialty":
                    return obj.isSpecialty();
                case "specialization":
                    return obj.isSpecialization();
                case "master":
                    return obj.isMaster();
                case "bachelor":
                    return obj.isBachelor();
                case "profile":
                    return obj.isProfile();
                case "qualificationRequired":
                    return obj.isQualificationRequired();
                case "parent":
                    return obj.getParent();
                case "priority":
                    return obj.getPriority();
                case "shortTitle":
                    return obj.getShortTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "allowStudents":
                    obj.setAllowStudents((Boolean) value);
                    return;
                case "allowGroups":
                    obj.setAllowGroups((Boolean) value);
                    return;
                case "basic":
                    obj.setBasic((Boolean) value);
                    return;
                case "middle":
                    obj.setMiddle((Boolean) value);
                    return;
                case "high":
                    obj.setHigh((Boolean) value);
                    return;
                case "higher":
                    obj.setHigher((Boolean) value);
                    return;
                case "additional":
                    obj.setAdditional((Boolean) value);
                    return;
                case "gos2":
                    obj.setGos2((Boolean) value);
                    return;
                case "gos3":
                    obj.setGos3((Boolean) value);
                    return;
                case "specialty":
                    obj.setSpecialty((Boolean) value);
                    return;
                case "specialization":
                    obj.setSpecialization((Boolean) value);
                    return;
                case "master":
                    obj.setMaster((Boolean) value);
                    return;
                case "bachelor":
                    obj.setBachelor((Boolean) value);
                    return;
                case "profile":
                    obj.setProfile((Boolean) value);
                    return;
                case "qualificationRequired":
                    obj.setQualificationRequired((Boolean) value);
                    return;
                case "parent":
                    obj.setParent((StructureEducationLevels) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "allowStudents":
                        return true;
                case "allowGroups":
                        return true;
                case "basic":
                        return true;
                case "middle":
                        return true;
                case "high":
                        return true;
                case "higher":
                        return true;
                case "additional":
                        return true;
                case "gos2":
                        return true;
                case "gos3":
                        return true;
                case "specialty":
                        return true;
                case "specialization":
                        return true;
                case "master":
                        return true;
                case "bachelor":
                        return true;
                case "profile":
                        return true;
                case "qualificationRequired":
                        return true;
                case "parent":
                        return true;
                case "priority":
                        return true;
                case "shortTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "allowStudents":
                    return true;
                case "allowGroups":
                    return true;
                case "basic":
                    return true;
                case "middle":
                    return true;
                case "high":
                    return true;
                case "higher":
                    return true;
                case "additional":
                    return true;
                case "gos2":
                    return true;
                case "gos3":
                    return true;
                case "specialty":
                    return true;
                case "specialization":
                    return true;
                case "master":
                    return true;
                case "bachelor":
                    return true;
                case "profile":
                    return true;
                case "qualificationRequired":
                    return true;
                case "parent":
                    return true;
                case "priority":
                    return true;
                case "shortTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "allowStudents":
                    return Boolean.class;
                case "allowGroups":
                    return Boolean.class;
                case "basic":
                    return Boolean.class;
                case "middle":
                    return Boolean.class;
                case "high":
                    return Boolean.class;
                case "higher":
                    return Boolean.class;
                case "additional":
                    return Boolean.class;
                case "gos2":
                    return Boolean.class;
                case "gos3":
                    return Boolean.class;
                case "specialty":
                    return Boolean.class;
                case "specialization":
                    return Boolean.class;
                case "master":
                    return Boolean.class;
                case "bachelor":
                    return Boolean.class;
                case "profile":
                    return Boolean.class;
                case "qualificationRequired":
                    return Boolean.class;
                case "parent":
                    return StructureEducationLevels.class;
                case "priority":
                    return Integer.class;
                case "shortTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StructureEducationLevels> _dslPath = new Path<StructureEducationLevels>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StructureEducationLevels");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Обучение студентов осуществляется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isAllowStudents()
     */
    public static PropertyPath<Boolean> allowStudents()
    {
        return _dslPath.allowStudents();
    }

    /**
     * @return Обучение групп осуществляется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isAllowGroups()
     */
    public static PropertyPath<Boolean> allowGroups()
    {
        return _dslPath.allowGroups();
    }

    /**
     * @return НПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isBasic()
     */
    public static PropertyPath<Boolean> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return СПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isMiddle()
     */
    public static PropertyPath<Boolean> middle()
    {
        return _dslPath.middle();
    }

    /**
     * @return ВПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isHigh()
     */
    public static PropertyPath<Boolean> high()
    {
        return _dslPath.high();
    }

    /**
     * @return Кадры высшей квалификации. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isHigher()
     */
    public static PropertyPath<Boolean> higher()
    {
        return _dslPath.higher();
    }

    /**
     * @return ДПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isAdditional()
     */
    public static PropertyPath<Boolean> additional()
    {
        return _dslPath.additional();
    }

    /**
     * @return ГОС2. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isGos2()
     */
    public static PropertyPath<Boolean> gos2()
    {
        return _dslPath.gos2();
    }

    /**
     * @return ФГОС. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isGos3()
     */
    public static PropertyPath<Boolean> gos3()
    {
        return _dslPath.gos3();
    }

    /**
     * @return Специальности. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isSpecialty()
     */
    public static PropertyPath<Boolean> specialty()
    {
        return _dslPath.specialty();
    }

    /**
     * @return Специализации. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isSpecialization()
     */
    public static PropertyPath<Boolean> specialization()
    {
        return _dslPath.specialization();
    }

    /**
     * @return Магистры. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isMaster()
     */
    public static PropertyPath<Boolean> master()
    {
        return _dslPath.master();
    }

    /**
     * @return Бакалавры. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isBachelor()
     */
    public static PropertyPath<Boolean> bachelor()
    {
        return _dslPath.bachelor();
    }

    /**
     * @return Профили. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isProfile()
     */
    public static PropertyPath<Boolean> profile()
    {
        return _dslPath.profile();
    }

    /**
     * @return Квалификация обязательна. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isQualificationRequired()
     */
    public static PropertyPath<Boolean> qualificationRequired()
    {
        return _dslPath.qualificationRequired();
    }

    /**
     * @return Структура направлений подготовки (специальностей) Минобрнауки РФ.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getParent()
     */
    public static StructureEducationLevels.Path<StructureEducationLevels> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StructureEducationLevels> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _allowStudents;
        private PropertyPath<Boolean> _allowGroups;
        private PropertyPath<Boolean> _basic;
        private PropertyPath<Boolean> _middle;
        private PropertyPath<Boolean> _high;
        private PropertyPath<Boolean> _higher;
        private PropertyPath<Boolean> _additional;
        private PropertyPath<Boolean> _gos2;
        private PropertyPath<Boolean> _gos3;
        private PropertyPath<Boolean> _specialty;
        private PropertyPath<Boolean> _specialization;
        private PropertyPath<Boolean> _master;
        private PropertyPath<Boolean> _bachelor;
        private PropertyPath<Boolean> _profile;
        private PropertyPath<Boolean> _qualificationRequired;
        private StructureEducationLevels.Path<StructureEducationLevels> _parent;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StructureEducationLevelsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Обучение студентов осуществляется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isAllowStudents()
     */
        public PropertyPath<Boolean> allowStudents()
        {
            if(_allowStudents == null )
                _allowStudents = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_ALLOW_STUDENTS, this);
            return _allowStudents;
        }

    /**
     * @return Обучение групп осуществляется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isAllowGroups()
     */
        public PropertyPath<Boolean> allowGroups()
        {
            if(_allowGroups == null )
                _allowGroups = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_ALLOW_GROUPS, this);
            return _allowGroups;
        }

    /**
     * @return НПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isBasic()
     */
        public PropertyPath<Boolean> basic()
        {
            if(_basic == null )
                _basic = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_BASIC, this);
            return _basic;
        }

    /**
     * @return СПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isMiddle()
     */
        public PropertyPath<Boolean> middle()
        {
            if(_middle == null )
                _middle = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_MIDDLE, this);
            return _middle;
        }

    /**
     * @return ВПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isHigh()
     */
        public PropertyPath<Boolean> high()
        {
            if(_high == null )
                _high = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_HIGH, this);
            return _high;
        }

    /**
     * @return Кадры высшей квалификации. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isHigher()
     */
        public PropertyPath<Boolean> higher()
        {
            if(_higher == null )
                _higher = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_HIGHER, this);
            return _higher;
        }

    /**
     * @return ДПО. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isAdditional()
     */
        public PropertyPath<Boolean> additional()
        {
            if(_additional == null )
                _additional = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_ADDITIONAL, this);
            return _additional;
        }

    /**
     * @return ГОС2. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isGos2()
     */
        public PropertyPath<Boolean> gos2()
        {
            if(_gos2 == null )
                _gos2 = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_GOS2, this);
            return _gos2;
        }

    /**
     * @return ФГОС. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isGos3()
     */
        public PropertyPath<Boolean> gos3()
        {
            if(_gos3 == null )
                _gos3 = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_GOS3, this);
            return _gos3;
        }

    /**
     * @return Специальности. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isSpecialty()
     */
        public PropertyPath<Boolean> specialty()
        {
            if(_specialty == null )
                _specialty = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_SPECIALTY, this);
            return _specialty;
        }

    /**
     * @return Специализации. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isSpecialization()
     */
        public PropertyPath<Boolean> specialization()
        {
            if(_specialization == null )
                _specialization = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_SPECIALIZATION, this);
            return _specialization;
        }

    /**
     * @return Магистры. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isMaster()
     */
        public PropertyPath<Boolean> master()
        {
            if(_master == null )
                _master = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_MASTER, this);
            return _master;
        }

    /**
     * @return Бакалавры. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isBachelor()
     */
        public PropertyPath<Boolean> bachelor()
        {
            if(_bachelor == null )
                _bachelor = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_BACHELOR, this);
            return _bachelor;
        }

    /**
     * @return Профили. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isProfile()
     */
        public PropertyPath<Boolean> profile()
        {
            if(_profile == null )
                _profile = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_PROFILE, this);
            return _profile;
        }

    /**
     * @return Квалификация обязательна. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#isQualificationRequired()
     */
        public PropertyPath<Boolean> qualificationRequired()
        {
            if(_qualificationRequired == null )
                _qualificationRequired = new PropertyPath<Boolean>(StructureEducationLevelsGen.P_QUALIFICATION_REQUIRED, this);
            return _qualificationRequired;
        }

    /**
     * @return Структура направлений подготовки (специальностей) Минобрнауки РФ.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getParent()
     */
        public StructureEducationLevels.Path<StructureEducationLevels> parent()
        {
            if(_parent == null )
                _parent = new StructureEducationLevels.Path<StructureEducationLevels>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StructureEducationLevelsGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(StructureEducationLevelsGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.StructureEducationLevels#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StructureEducationLevelsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StructureEducationLevels.class;
        }

        public String getEntityName()
        {
            return "structureEducationLevels";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
