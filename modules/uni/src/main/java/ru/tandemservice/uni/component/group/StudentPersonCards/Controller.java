/**
 * $Id$
 */
package ru.tandemservice.uni.component.group.StudentPersonCards;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author dseleznev
 * Created on: 08.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);
        final RtfDocument document = model.getPrintFactory().getData().getTemplate();
        RtfDocument resultDoc = (RtfDocument)document.getClone();
        resultDoc.getElementList().clear();

        for (Student student : model.getStudentsList())
        {
            model.setStudent(student);
            getDao().refreshStudent(model);
            RtfDocument doc = (RtfDocument)document.getClone();
            model.getPrintFactory().addCard(doc);
            resultDoc.getElementList().addAll(doc.getElementList());
            if (model.getStudentsList().indexOf(student) < model.getStudentsList().size() - 1)
                resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
        }

        resultDoc.setSettings(document.getSettings());
        resultDoc.setHeader(document.getHeader());

        deactivate(component);
        return new CommonBaseRenderer().rtf().fileName("StudentCard.rtf").document(resultDoc);
    }
}