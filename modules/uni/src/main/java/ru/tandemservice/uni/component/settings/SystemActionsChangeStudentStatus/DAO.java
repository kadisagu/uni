/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsChangeStudentStatus;

import org.hibernate.Query;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 20.10.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
    }

    @Override
    public void update(Model model)
    {
        StudentStatus fromStatus = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_POSSIBLE);
        StudentStatus toStatus = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);

        Map<String, List> mapParam = new HashMap<>();

        StringBuilder hql = new StringBuilder("update " + Student.ENTITY_CLASS + " set " + Student.L_STATUS + "=:toStatus where " + Student.L_STATUS + "=:fromStatus");

        if (model.isFormativeOrgUnitActive())
        {
            hql.append(" and " + Student.L_EDUCATION_ORG_UNIT + ".id in (select id from " + EducationOrgUnit.ENTITY_CLASS + " where " + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + " in (:formativeOrgUnit))");
            mapParam.put("formativeOrgUnit", model.getFormativeOrgUnitList());
        }

        if (model.isTerritorialOrgUnitActive())
        {
            hql.append(" and " + Student.L_EDUCATION_ORG_UNIT + ".id in (select id from " + EducationOrgUnit.ENTITY_CLASS + " where " + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + " in (:territorialOrgUnit))");
            mapParam.put("territorialOrgUnit", model.getTerritorialOrgUnitList());
        }

        if (model.isDevelopFormActive())
        {
            hql.append(" and " + Student.L_EDUCATION_ORG_UNIT + ".id in (select id from " + EducationOrgUnit.ENTITY_CLASS + " where " + EducationOrgUnit.L_DEVELOP_FORM + " in (:developForm))");
            mapParam.put("developForm", model.getDevelopFormList());
        }

        Query q = getSession().createQuery(hql.toString());

        q.setParameter("toStatus", toStatus);
        q.setParameter("fromStatus", fromStatus);
        for (Map.Entry<String, List> entry : mapParam.entrySet())
            q.setParameterList(entry.getKey(), entry.getValue());

        q.executeUpdate();
    }
}
