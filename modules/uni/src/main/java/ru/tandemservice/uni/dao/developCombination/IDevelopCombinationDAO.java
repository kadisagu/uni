package ru.tandemservice.uni.dao.developCombination;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uni.entity.education.IDevelopCombination;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IDevelopCombinationDAO {
    public static final SpringBeanCache<IDevelopCombinationDAO> instance = new SpringBeanCache<IDevelopCombinationDAO>(IDevelopCombinationDAO.class.getName());

    /**
     * @param developCombinationDefinition - определение комбинации ФУТС
     * @return объект из базы, комбинация ФУТС, если таковая есть в базе
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    DevelopCombination findSuitableCombination(IDevelopCombination developCombinationDefinition);

    /**
     * @param combinationList - набор комбинаций ФУТС
     * @return объекты из базы, комбинации ФУТС, если таковые есть в базе
     */
    List<DevelopCombination> findSuitableCombinations(Collection<IDevelopCombination> combinationList);
}
