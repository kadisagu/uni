// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.GroupStartEduYearEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;

import java.util.List;

/**
 * @author oleyba
 * @since 03.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setGroup(getNotNull(Group.class, model.getGroup().getId()));
        model.setYearModel(new EducationYearModel());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        if (ApplicationRuntime.containsBean(IGroupStartEduYearUpdateBlock.LIST_BEAN_NAME))
            for (IGroupStartEduYearUpdateBlock block : (List<IGroupStartEduYearUpdateBlock>) ApplicationRuntime.getBean(IGroupStartEduYearUpdateBlock.LIST_BEAN_NAME))
                if (block.isBlocked(model.getGroup()))
                    throw new ApplicationException(block.getErrorMessage());
        update(model.getGroup());
    }
}
