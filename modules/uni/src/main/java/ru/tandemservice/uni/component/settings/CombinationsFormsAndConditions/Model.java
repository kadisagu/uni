package ru.tandemservice.uni.component.settings.CombinationsFormsAndConditions;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * 
 * @author nkokorina
 * @since 01.03.2010
 */

public class Model
{
    private DynamicListDataSource<IEntity> _dataSource;

    public void setDataSource(DynamicListDataSource<IEntity> _dataSource)
    {
        this._dataSource = _dataSource;
    }

    public DynamicListDataSource<IEntity> getDataSource()
    {
        return _dataSource;
    }
}
