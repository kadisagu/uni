/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.ui.EduOrgUnitCorrection;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
public class UniReorganizationEduOrgUnitCorrectionUI extends UIPresenter {

    private String selectedTab;

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public Map<String, Object> getJournalParams()
    {
        Map<String, Object> params = new Hashtable<>();

        params.put("meta", EntityRuntime.getMeta(EducationOrgUnit.class));
        params.put("hideSelect", Boolean.TRUE);
        return params;
    }
}