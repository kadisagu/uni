/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/14
 */
public class UniEduProgramEduOuDSHandler extends DefaultSearchDataSourceHandler
{
    public UniEduProgramEduOuDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    @SuppressWarnings("deprecation")
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EducationOrgUnit.class, "e");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("e"));
        dql.joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("e"), "m");

        EduProgram program = context.get("eduProgram");
        
        if (program != null) {

            dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(program.getOwnerOrgUnit().getOrgUnit())))
                .where(eq(property("e", EducationOrgUnit.developForm().programForm()), value(program.getForm())))
                .where(eq(property("e", EducationOrgUnit.developPeriod().eduProgramDuration()), value(program.getDuration())))
                ;

//            if (program.getEduProgramTrait() == null) {
//                dql.where(isNull(property("e", EducationOrgUnit.developTech().programTrait())));
//            } else {
//                dql.where(eq(property("e", EducationOrgUnit.developTech().programTrait()), value(program.getEduProgramTrait())));
//            }
            if (program instanceof EduProgramProf) {
                dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()), value(((EduProgramProf) program).getProgramSubject())));
                dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().assignedQualification()), value(((EduProgramProf) program).getProgramQualification())));

                if (program instanceof EduProgramSecondaryProf) {
                    dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().code()), value(((EduProgramSecondaryProf) program).isInDepthStudy() ? QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O : QualificationsCodes.BAZOVYY_UROVEN_S_P_O)));
                } else if (program instanceof EduProgramHigherProf) {
                    EduProgramHigherProf prof = (EduProgramHigherProf) program;

                    dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization()), value(prof.getProgramSpecialization())));
                    dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().programOrientation()), value(prof.getProgramOrientation())));
                }
            }
        } else {
            StructureEducationLevels levelTop = context.get("levelTop");
            StructureEducationLevels levelType = context.get("levelType");
            String subjectCode = StringUtils.trimToNull(context.get("subjectCode"));
            String title = context.get("title");
            ICatalogItem qualification = context.get("qualification");
            OrgUnit orgUnit = context.get("orgUnit");

            FilterUtils.applySimpleLikeFilter(dql, "m", EducationLevelsHighSchool.title(), title);

            if (levelTop == null) {
                dql.where(isNull("m.id"));
            }

            if (levelType != null) {
                dql.where(eq(property("m", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE), value(levelType)));
            }
            else {
                Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap = prepareStructureMap();
                dql.where(in(property("m", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE), structureMap.get(levelTop)));
            }
            if (subjectCode != null) {
                Set<Long> ids = new HashSet<>();
                for (EducationLevelsHighSchool educationLevelsHighSchool : DataAccessServices.dao().getList(EducationLevelsHighSchool.class)) {
                    if (subjectCode.equals(educationLevelsHighSchool.getEducationLevel().getInheritedOkso())) {
                        ids.add(educationLevelsHighSchool.getId());
                    }

                    String titleCodePrefix = educationLevelsHighSchool.getEducationLevel().getTitleCodePrefix();
                    if ((null != titleCodePrefix) && titleCodePrefix.startsWith(subjectCode)) {
                        ids.add(educationLevelsHighSchool.getId());
                    }
                }

                dql.where(in(property("m", EducationLevelsHighSchool.P_ID), ids));
            }
            if (qualification != null) {
                Set<Long> ids = new HashSet<>();
                for (EducationLevelsHighSchool educationLevelsHighSchool : DataAccessServices.dao().getList(EducationLevelsHighSchool.class))
                    if (qualification.getCode().equals(educationLevelsHighSchool.getEducationLevel().getSafeQCode()))
                        ids.add(educationLevelsHighSchool.getId());

                dql.where(in(property("m", EducationLevelsHighSchool.P_ID), ids));
            }
            if (orgUnit != null) {
                dql.where(eq(property("m", EducationLevelsHighSchool.L_ORG_UNIT), value(orgUnit)));
            }
        }

        return DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(isPageable())
            .build();
    }

    private Map<StructureEducationLevels, Set<StructureEducationLevels>> prepareStructureMap()
    {
        List<StructureEducationLevels> baseList = DataAccessServices.dao().getList(StructureEducationLevels.class);
        Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap = new HashMap<>();
        for (StructureEducationLevels level : baseList)
            SafeMap.safeGet(structureMap, getRoot(level), HashSet.class).add(level);
        return structureMap;
    }

    private StructureEducationLevels getRoot(StructureEducationLevels item)
    {
        StructureEducationLevels root = item;
        while (root.getParent() != null)
            root = root.getParent();
        return root;
    }

}




