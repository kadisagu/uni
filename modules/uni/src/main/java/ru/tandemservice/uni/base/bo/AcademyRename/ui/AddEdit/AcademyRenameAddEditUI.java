/* $Id$ */
package ru.tandemservice.uni.base.bo.AcademyRename.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;

/**
 * @author Andrey Avetisov
 * @since 09.07.2014
 */
@Input({
    @Bind(key = AcademyRenameAddEditUI.ACADEMY_RENAME_ID, binding = "academyRename.id")
})
public class AcademyRenameAddEditUI extends UIPresenter
{
    private boolean _editForm;
    public static final String ACADEMY_RENAME_ID = "academyRenameId";

    AcademyRename _academyRename = new AcademyRename();

    @Override
    public void onComponentRefresh()
    {
        if (_academyRename.getId() != null)
        {
            _academyRename = DataAccessServices.dao().getNotNull(AcademyRename.class, _academyRename.getId());
            _editForm = true;
        }
        else _editForm = false;
    }

    public AcademyRename getAcademyRename()
    {
        return _academyRename;
    }

    public void setAcademyRename(AcademyRename academyRename)
    {
        _academyRename = academyRename;
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_academyRename);
        deactivate();
    }

    public boolean isEditForm()
    {
        return _editForm;
    }
}
