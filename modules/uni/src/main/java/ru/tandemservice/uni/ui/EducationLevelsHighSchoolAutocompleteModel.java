/**
 * $Id$
 */
package ru.tandemservice.uni.ui;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.UniStringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author dseleznev
 *         Created on: 10.10.2008
 * use EducationLevelsHighSchoolSelectModel
 */
public abstract class EducationLevelsHighSchoolAutocompleteModel extends UniSimpleAutocompleteModel
{
    @Deprecated
    public EducationLevelsHighSchoolAutocompleteModel()
    {
    }

    protected abstract List<EducationLevelsHighSchool> getFilteredList();

    @Override
    public ListResult findValues(String filter)
    {
        filter = (filter != null) ? UniStringUtils.escapePattern(CoreStringUtils.escapeLike(filter)).replaceAll("%", ".*") : "";
        List<EducationLevelsHighSchool> list = new ArrayList<>();
        Pattern pattern = Pattern.compile(filter);

        List<EducationLevelsHighSchool> filteredList = getFilteredList();
        for (EducationLevelsHighSchool educationLevel : filteredList)
            if (filter.isEmpty() || pattern.matcher(educationLevel.getFullTitle().toUpperCase()).find())
                list.add(educationLevel);

        Collections.sort(list, new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.P_FULL_TITLE, OrderDirection.asc)));
        return new ListResult<>(list.size() <= 50 ? list : list.subList(0, 50), list.size());
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        EducationLevelsHighSchool level = UniDaoFacade.getCoreDao().get(EducationLevelsHighSchool.class, (Long) primaryKey);
        if (getFilteredList().contains(level))
        {
            level.getFullTitle(); // unproxy
            getLabelFor(level, 0);
            return level;
        }
        return null;
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return ((EducationLevelsHighSchool) value).getFullTitle();
    }
}