package ru.tandemservice.uni.dao.pps;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class PpsEntryByEmployeePostSynchronizer extends UniBaseDao implements IPpsEntrySynchronizer
{
    public static final SpringBeanCache<IPpsEntrySynchronizer> instance = new SpringBeanCache<>(PpsEntryByEmployeePostSynchronizer.class.getName());

    public static final SyncDaemon DAEMON = new SyncDaemon(PpsEntryByEmployeePostSynchronizer.class.getName(), 120, IPpsEntrySynchronizer.LOCKER_NAME)
    {
        @Override
        protected void main()
        {
            try {
                instance.get().doSynchronize();
            } finally {
                if (this.logger.isInfoEnabled())
                {
                    this.logger.info(UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                        final int enabled = ((Number) session.createCriteria(PpsEntry.class).add(Restrictions.isNull(PpsEntry.P_REMOVAL_DATE)).setProjection(Projections.count("id")).uniqueResult()).intValue();
                        final int total = ((Number) session.createCriteria(PpsEntry.class).setProjection(Projections.count("id")).uniqueResult()).intValue();
                        return "pps=" + enabled + "/" + total;
                    }));
                }
            }
        }
    };

    @Override
    public void doSynchronize()
    {
        Debug.begin("PpsEntryByEmployeePostSynchronizer");
        try
        {
            // обновляем дату утраты актуальности
            updateRemovalDate();
            // обновляем, либо удаляем неиспользуемые записи ППС
            updateOrDeletePpsEntry();

            Map<TripletKey<Person, OrgUnit, PostBoundedWithQGandQL>, Long> key2PpsEntryMap = Maps.newHashMap();
            Map<Long, String> ppsEntry2EmployeeCodeMap = Maps.newHashMap();
            Map<Long, List<EmployeePost>> ppsEntry2EmployeePostMap = Maps.newHashMap();

            // поднимаем все записи ППС на сотрудника, включая сущности, без связей (которые нельзя удалить)
            new DQLSelectBuilder().fromEntity(PpsEntryByEmployeePost.class, "pps")
                    .joinEntity("pps", DQLJoinType.left, EmployeePost4PpsEntry.class, "ep4pe", eq(property("pps.id"), property("ep4pe", EmployeePost4PpsEntry.ppsEntry().id())))
                    .fetchPath(DQLJoinType.left, EmployeePost4PpsEntry.employeePost().fromAlias("ep4pe"), "ep")
                    .fetchPath(DQLJoinType.left, EmployeePost.employee().fromAlias("ep"), "e")
                    .order(property("pps.id"))
                    .order(property("e", Employee.employeeCode()))
                    .column(property("pps"))
                    .column(property("ep4pe"))
                    .createStatement(getSession()).<Object[]>list()
                    .forEach(item -> {
                        PpsEntryByEmployeePost ppsEntry = (PpsEntryByEmployeePost) item[0];
                        EmployeePost4PpsEntry ep4pe = (EmployeePost4PpsEntry) item[1];
                        TripletKey<Person, OrgUnit, PostBoundedWithQGandQL> key = TripletKey.create(ppsEntry.getPerson(), ppsEntry.getOrgUnit(), ppsEntry.getPost());

                        key2PpsEntryMap.put(key, ppsEntry.getId());
                        ppsEntry2EmployeeCodeMap.put(ppsEntry.getId(), ppsEntry.getEmployeeCode());
                        List<EmployeePost> employeePosts = SafeMap.safeGet(ppsEntry2EmployeePostMap, ppsEntry.getId(), LinkedList.class);
                        if (null != ep4pe)
                            employeePosts.add(ep4pe.getEmployeePost());
                    });

            // создаем записи ППС и соответстыующие с ними связи
            createPpsEntryAndRel(key2PpsEntryMap, ppsEntry2EmployeePostMap);
            // заполняем табельный номер
            fillEmployeeCodeByPpsEntry(ppsEntry2EmployeePostMap, ppsEntry2EmployeeCodeMap);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
        finally
        {
            Debug.end();
        }
    }

    /**
     * Отбираются "Запись в реестре ППС (на базе сотрудника)" и "Сотрудник для записи в реестре ППС" и в зависимости от условия:
     * - выставляется текущая дата утраты актуальности, если сотрудники не в актуальном состоянии;
     * - удаляется дата утраты актуальности, если сотрудники в актуальном состоянии.
     */
    private void updateRemovalDate()
    {
        // если clear = true => обнулить дату
        for (boolean clear : new boolean[]{false, true})
        {
            {
                DQLUpdateBuilder update = new DQLUpdateBuilder(PpsEntryByEmployeePost.class)
                        .fromDataSource(
                                new DQLSelectBuilder()
                                        .fromEntity(EmployeePost4PpsEntry.class, "pps")
                                        .column(property("pps", EmployeePost4PpsEntry.ppsEntry().id()), "entryId")
                                        .where(clear
                                                       ? and(
                                                               isNotNull(property("pps", EmployeePost4PpsEntry.ppsEntry().removalDate())),
                                                               in(property("pps", EmployeePost4PpsEntry.employeePost().id()), getEmployeePostNSelectBuilder("ep", true).column(property("ep", "id")).buildQuery()))
                                                       : and(
                                                               isNull(property("pps", EmployeePost4PpsEntry.ppsEntry().removalDate())),
                                                               notIn(property("pps", EmployeePost4PpsEntry.employeePost().id()), getEmployeePostNSelectBuilder("ep", true).column(property("ep", "id")).buildQuery()))
                                        )
                                        .buildQuery(), "x")
                        .where(eq(property(PpsEntryByEmployeePost.id()), property("x.entryId")));

                if (clear)
                    update.set(PpsEntryByEmployeePost.removalDate().s(), "cast(null, date)");
                else
                    update.set(PpsEntryByEmployeePost.removalDate().s(), value(new Date(), PropertyType.DATE));

                final int updateCount = executeAndClear(update);
                if (updateCount > 0)
                {
                    Debug.message("updated-inactive-rows-pps=" + updateCount);
                }
            }
            {
                DQLUpdateBuilder update = new DQLUpdateBuilder(EmployeePost4PpsEntry.class)
                        .fromDataSource(
                                new DQLSelectBuilder()
                                        .fromEntity(EmployeePost4PpsEntry.class, "ep4pe")
                                        .column(property("ep4pe.id"), "ep4peId")
                                        .where(clear
                                                       ? and(
                                                               isNotNull(property("ep4pe", EmployeePost4PpsEntry.removalDate())),
                                                               in(property("ep4pe", EmployeePost4PpsEntry.employeePost().id()), getEmployeePostNSelectBuilder("ep", true).column(property("ep", "id")).buildQuery()))
                                                       : and(
                                                               isNull(property("ep4pe", EmployeePost4PpsEntry.removalDate())),
                                                               notIn(property("ep4pe", EmployeePost4PpsEntry.employeePost().id()), getEmployeePostNSelectBuilder("ep", true).column(property("ep", "id")).buildQuery()))
                                        )
                                        .buildQuery(), "x")
                        .where(eq(property(EmployeePost4PpsEntry.id()), property("x.ep4peId")));

                if (clear)
                    update.set(EmployeePost4PpsEntry.removalDate().s(), "cast(null, date)");
                else
                    update.set(EmployeePost4PpsEntry.removalDate().s(), value(new Date(), PropertyType.DATE));

                final int updateCount = executeAndClear(update);
                if (updateCount > 0)
                {
                    Debug.message("updated-active-rows-ep4pe=" + updateCount);
                }
            }
        }
    }

    private void updateOrDeletePpsEntry()
    {
        Debug.begin("PpsEntryByEmployeePostSynchronizer.updateOrDelete-ppsEntry");
        try
        {
            // удаляем все EmployeePost4PpsEntry, для которых ключ свойств не соответствует связи. Такие записи не имею связей с другими сущностями, смело удаляем!
            {
                final int removeCount = new DQLDeleteBuilder(EmployeePost4PpsEntry.class)
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(EmployeePost4PpsEntry.class, "ep4pe")
                                        .where(exists(
                                                new DQLSelectBuilder().fromEntity(EmployeePost4PpsEntry.class, "rel")
                                                        .joinPath(DQLJoinType.inner, EmployeePost4PpsEntry.employeePost().fromAlias("rel"), "ep")
                                                        .joinPath(DQLJoinType.inner, EmployeePost4PpsEntry.ppsEntry().fromAlias("rel"), "pps")
                                                        .joinPath(DQLJoinType.left, EmployeePost.person().fromAlias("ep"), "epPerson")
                                                        .joinPath(DQLJoinType.left, EmployeePost.orgUnit().fromAlias("ep"), "epOu")
                                                        .joinPath(DQLJoinType.left, EmployeePost.postRelation().postBoundedWithQGandQL().fromAlias("ep"), "epPost")
                                                        .where(eq(property("ep4pe.id"), property("rel.id")))
                                                        .where(or(
                                                                ne(property("pps", PpsEntryByEmployeePost.person().id()), property("epPerson.id")),
                                                                ne(property("pps", PpsEntryByEmployeePost.orgUnit().id()), property("epOu.id")),
                                                                ne(property("pps", PpsEntryByEmployeePost.post().id()), property("epPost.id"))
                                                        ))
                                                        .column(property("rel.id"))
                                                        .buildQuery()
                                        ))
                                        .column(property("ep4pe.id"), "item_id")
                                        .buildQuery(), "x"
                        )
                        .where(eq(property(EmployeePost4PpsEntry.id()), property("x.item_id")))
                        .createStatement(getSession()).execute();
                if (removeCount > 0)
                {
                    Debug.message("removed-employeePost4PpsEntry-rows=" + removeCount);
                }
            }

            // для записей ППС, заблокированых входящими ссылками и не имеющих связей EmployeePost4PpsEntry ставим дату утраты актуальности
            {
                final int updateCount = new DQLUpdateBuilder(PpsEntryByEmployeePost.class)
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(PpsEntryByEmployeePost.class, "pps")
                                        .where(notExists(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().id().s(), property("pps.id")))
                                        .where(not(new DQLCanDeleteExpressionBuilder(PpsEntry.class, "pps.id").getExpression()))
                                        .column(property("pps.id"), "item_id")
                                        .buildQuery(), "x"
                        )
                        .where(eq(property(PpsEntryByEmployeePost.id()), property("x.item_id")))
                        .set(PpsEntryByEmployeePost.removalDate().s(), value(new Date(), PropertyType.DATE))
                        .createStatement(getSession()).execute();
                if (updateCount > 0)
                {
                    Debug.message("updated-ppsEntry-rows=" + updateCount);
                }
            }

            // удаляем для записей ППС, незаблокированые входящими ссылками и не имеющих связей EmployeePost4PpsEntry
            {
                final int removeCount = new DQLDeleteBuilder(PpsEntryByEmployeePost.class)
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(PpsEntryByEmployeePost.class, "pps")
                                        .where(notExists(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().id().s(), property("pps.id")))
                                        .where(new DQLCanDeleteExpressionBuilder(PpsEntry.class, "pps.id").getExpression())
                                        .column(property("pps.id"), "item_id")
                                        .buildQuery(), "x"
                        )
                        .where(eq(property(PpsEntryByEmployeePost.id()), property("x.item_id")))
                        .createStatement(getSession()).execute();

                if (removeCount > 0)
                {
                    Debug.message("removed-ppsEntry-rows=" + removeCount);
                }
            }
        }
        finally
        {
            Debug.end();
        }
    }

    private void createPpsEntryAndRel(Map<TripletKey<Person, OrgUnit, PostBoundedWithQGandQL>, Long> key2PpsEntryMap, Map<Long, List<EmployeePost>> ppsEntry2EmployeePostMap)
    {
        Debug.begin("PpsEntryByEmployeePostSynchronizer.create");
        try
        {
            Date now = new Date();
            final short ppsEntryEntityCode = EntityRuntime.getMeta(PpsEntryByEmployeePost.class).getEntityCode();
            final short employeePost4PEEntityCode = EntityRuntime.getMeta(EmployeePost4PpsEntry.class).getEntityCode();

            List<Object[]> rows = getEmployeePostNSelectBuilder("ep", true)
                    .joinPath(DQLJoinType.inner, EmployeePost.person().fromAlias("ep"), "person")
                    .joinPath(DQLJoinType.inner, EmployeePost.orgUnit().fromAlias("ep"), "ou")
                    .joinPath(DQLJoinType.inner, EmployeePost.postRelation().postBoundedWithQGandQL().fromAlias("ep"), "post")
                    .where(or(
                            notExists(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.employeePost().id().s(), property("ep", "id")),
                            notExists(
                                    new DQLSelectBuilder()
                                            .fromEntity(PpsEntryByEmployeePost.class, "pps")
                                            .where(eq(property("pps", PpsEntryByEmployeePost.person().id()), property("person.id")))
                                            .where(eq(property("pps", PpsEntryByEmployeePost.orgUnit().id()), property("ou.id")))
                                            .where(eq(property("pps", PpsEntryByEmployeePost.post().id()), property("post.id")))
                                            .buildQuery()
                            )
                    ))
                    .column(property("person"))
                    .column(property("ou"))
                    .column(property("post"))
                    .column(property("ep"))
                    .createStatement(getSession()).list();

            Iterables.partition(rows, DQL.MAX_VALUES_ROW_NUMBER).forEach(items -> {
                DQLInsertValuesBuilder ppsEntryBuilder = new DQLInsertValuesBuilder(PpsEntryByEmployeePost.class);
                DQLInsertValuesBuilder employeePost4PEBuilder = new DQLInsertValuesBuilder(EmployeePost4PpsEntry.class);

                boolean savePpsEntry = false;
                boolean saveEmployeePost4PE = false;

                for (Object[] item : items)
                {
                    Person person = (Person) item[0];
                    OrgUnit orgUnit = (OrgUnit) item[1];
                    PostBoundedWithQGandQL post = (PostBoundedWithQGandQL) item[2];
                    EmployeePost employeePost = (EmployeePost) item[3];
                    boolean active = employeePost.getPostStatus().isActive() && !employeePost.getEmployee().isArchival();

                    TripletKey<Person, OrgUnit, PostBoundedWithQGandQL> key = TripletKey.create(person, orgUnit, post);
                    final MutableBoolean hasRow = new MutableBoolean(false);

                    Long ppsEntryId = key2PpsEntryMap.get(key);
                    if (null == ppsEntryId)
                    {
                        ppsEntryId = EntityIDGenerator.generateNewId(ppsEntryEntityCode);

                        ppsEntryBuilder.valuesFrom(new PpsEntryByEmployeePost(person, orgUnit, post));
                        ppsEntryBuilder.value(PpsEntryByEmployeePost.P_ID, ppsEntryId);
                        ppsEntryBuilder.value(PpsEntryByEmployeePost.P_CREATION_DATE, now);
                        ppsEntryBuilder.value(PpsEntryByEmployeePost.P_REMOVAL_DATE, active ? null : now);
                        ppsEntryBuilder.addBatch();
                        savePpsEntry = true;

                        key2PpsEntryMap.put(key, ppsEntryId);
                    }
                    else
                    {
                        List<EmployeePost> employeePostRelations = ppsEntry2EmployeePostMap.getOrDefault(ppsEntryId, Lists.newArrayList());
                        employeePostRelations.stream().filter(rel -> rel.getId().equals(employeePost.getId())).forEach(rel -> hasRow.setValue(true));
                    }

                    if (hasRow.isFalse())
                    {
                        employeePost4PEBuilder.value(EmployeePost4PpsEntry.P_ID, EntityIDGenerator.generateNewId(employeePost4PEEntityCode));
                        employeePost4PEBuilder.value(EmployeePost4PpsEntry.L_PPS_ENTRY, ppsEntryId);
                        employeePost4PEBuilder.value(EmployeePost4PpsEntry.L_EMPLOYEE_POST, employeePost);
                        employeePost4PEBuilder.addBatch();
                        saveEmployeePost4PE = true;

                        SafeMap.safeGet(ppsEntry2EmployeePostMap, ppsEntryId, ArrayList.class).add(employeePost);
                    }
                }
                if (savePpsEntry) ppsEntryBuilder.createStatement(getSession()).execute();
                if (saveEmployeePost4PE) employeePost4PEBuilder.createStatement(getSession()).execute();
            });
        }
        finally
        {
            Debug.end();
        }
    }

    private void fillEmployeeCodeByPpsEntry(Map<Long, List<EmployeePost>> employeePostMap, Map<Long, String> employeeCodeMap)
    {
        Debug.begin("PpsEntryByEmployeePostSynchronizer.fill-employeeCode");
        try
        {
            IDQLStatement updatePpsEntryStatement = new DQLUpdateBuilder(PpsEntryByEmployeePost.class)
                    .set(PpsEntryByEmployeePost.P_EMPLOYEE_CODE, parameter("employeeCode", PropertyType.STRING))
                    .where(eq(property("id"), parameter("ppsEntryId", PropertyType.LONG)))
                    .createStatement(getSession());

            for (Map.Entry<Long, List<EmployeePost>> entry : employeePostMap.entrySet())
            {
                Long ppsEntryId = entry.getKey();
                List<String> employeeCodes = entry.getValue().stream().map(item -> item.getEmployee().getEmployeeCode()).distinct().collect(Collectors.toList());

                String employeeCodeNew = StringUtils.join(employeeCodes, ", ");
                String employeeCodeOld = employeeCodeMap.getOrDefault(entry.getKey(), "");

                if (!employeeCodeNew.equals(employeeCodeOld))
                {
                    updatePpsEntryStatement.setString("employeeCode", employeeCodeNew);
                    updatePpsEntryStatement.setLong("ppsEntryId", ppsEntryId);
                    updatePpsEntryStatement.execute();
                }
            }
        }
        finally
        {
            Debug.end();
        }
    }

    private DQLSelectBuilder getEmployeePostNSelectBuilder(String alias, boolean activeOnly)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EmployeePost.class, alias);
        dql.where(eq(property(alias, EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code()), value(EmployeeTypeCodes.EDU_STAFF)));
        if (activeOnly) {
             dql.where(eq(property(alias, EmployeePost.employee().archival()), value(Boolean.FALSE)));
             dql.where(eq(property(alias, EmployeePost.postStatus().actual()), value(Boolean.TRUE)));
        }
        return dql;
    }
}
