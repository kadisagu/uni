/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author nvankov
 * @since 3/25/13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "studentCustomStateId"),
    @Bind(key = "studentId", binding = "studentId")
})
public class UniStudentCustomStateAddEditUI extends UIPresenter
{
    private Long _studentId;
    private Long _studentCustomStateId;
    private StudentCustomState _studentCustomState;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Long getStudentCustomStateId()
    {
        return _studentCustomStateId;
    }

    public void setStudentCustomStateId(Long studentCustomStateId)
    {
        _studentCustomStateId = studentCustomStateId;
    }

    public StudentCustomState getStudentCustomState()
    {
        return _studentCustomState;
    }

    public void setStudentCustomState(StudentCustomState studentCustomState)
    {
        _studentCustomState = studentCustomState;
    }


    @Override
    public void onComponentRefresh()
    {
        if(null != _studentCustomStateId)
            _studentCustomState = DataAccessServices.dao().get(_studentCustomStateId);
        else
        {
            _studentCustomState = new StudentCustomState();
            _studentCustomState.setStudent(DataAccessServices.dao().<Student>get(_studentId));
        }
    }

    public boolean isAddMode()
    {
        return null == _studentCustomStateId;
    }

    public boolean isEditMode()
    {
        return !isAddMode();
    }

    public void onClickApply()
    {
        Date beginDate = _studentCustomState.getBeginDate();
        Date endDate = _studentCustomState.getEndDate();
        if(null != beginDate && null != endDate)
        {
            if(beginDate.after(endDate))
            {
                _uiSupport.error("«Дата начала действия статуса» должна быть не больше «Дата окончания действия статуса»", "beginDate", "endDate");
            }
        }
        if(!getUserContext().getErrorCollector().hasErrors())
        {
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(_studentCustomState);
            deactivate();
        }
    }
}
