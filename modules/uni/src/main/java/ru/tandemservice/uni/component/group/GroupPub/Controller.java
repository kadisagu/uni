/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.GroupPub;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniGroup.ui.StudentListAdd.UniGroupStudentListAdd;
import ru.tandemservice.uni.base.bo.UniStudent.daemon.UniStudentDevelopPeriodDaemonBean;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import org.tandemframework.core.entity.ViewWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    // список колонок, которые надо поместить раньше всех остальных
    public static final String PRIORITY_COLUMNS = "GroupStudentList.priorityColumns";

    @Override
    public void onRefreshComponent(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);

        model.setSettings(context.getSettings());

        getDao().prepare(model);

        model.setViewStudentListKey("orgUnit_viewStudentList_group");
        model.setDelKey("orgUnit_delete_group");
        model.setEditKey("orgUnit_edit_group");
        model.setArchiveKey("orgUnit_archive_group");
        model.setUnArchiveKey("orgUnit_unArchive_group");
        model.setEditArchivalKey("orgUnit_editArchival_group");

        prepareStudentDataSource(context);
    }

    @Override
    public void updateListDataSource(final IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    protected List<OrgUnitType> sortAllowedSubUnitType(final List<OrgUnitType> list)
    {
        return list;
    }

    // EVENT LISTENERS

    public void onClickEdit(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        model.setGroupId((Long) context.getListenerParameter());
        context.createChildRegion("groupTabPanel", new ComponentActivator(IUniComponents.GROUP_EDIT));
    }

    public void onClickDelete(final IBusinessComponent context)
    {
        UniDaoFacade.getCoreDao().delete(getModel(context).getGroupId());
        deactivate(context);
    }

    public void onClickArchive(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().setArchival(model, true, component.getUserContext().getErrorCollector());
        onRefreshComponent(component);
    }

    public void onClickUnarchive(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().setArchival(model, false, component.getUserContext().getErrorCollector());
        onRefreshComponent(component);
    }

    public void onClickEditArchivalAttributes(final IBusinessComponent context)
    {
        context.createChildRegion("groupTabPanel", new ComponentActivator("ru.tandemservice.uni.component.group.GroupArchivalEdit"));
    }

    public void onClickAssignCaptainStudent(final IBusinessComponent context)
    {
        context.createChildRegion("groupTabPanel", new ComponentActivator(IUniComponents.CAPTAIN_STUDENT_ASSIGN));
    }

    public void onClickAssignGroupHeader(final IBusinessComponent context)
    {
        context.createChildRegion("groupTabPanel", new ComponentActivator(IUniComponents.GROUP_HEADER_ASSIGN));
    }

    public void onClickAddStudent(final IBusinessComponent context)
    {
        context.createChildRegion("groupTabPanel", new ComponentActivator(IUniComponents.STUDENT_ADD));
    }

    public void onClickAddStudentList(final IBusinessComponent component)
    {
        component.createChildRegion("groupTabPanel", new ComponentActivator(UniGroupStudentListAdd.class.getSimpleName(), new ParametersMap().add("groupId", getModel(component).getGroupId())));
    }

    public void onClickEditStudent(final IBusinessComponent context)
    {
        getModel(context).setStudentId((Long) context.getListenerParameter());
        context.createChildRegion("groupTabPanel", new ComponentActivator(IUniComponents.STUDENT_EDIT));
    }

    public void onClickDeleteStudent(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        model.setStudentId((Long) context.getListenerParameter());
        getDao().deleteStudent(model);
        onRefreshComponent(context);
    }

    public void onPrintStudentsReport(final IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.GROUP_STUDENT_LIST, Collections.singletonMap("groupId", (Object) getModel(component).getGroupId())));
    }

    public void onClickPrintStudentPersonalCard(final IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled()) {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_PRINT_SCRIPT);
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, component.<Long>getListenerParameter());
        }
        else {
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARD, Collections.singletonMap("studentId", component.getListenerParameter())));
        }
    }

    public void onClickPrintGroupStudentPersonalCardsList(final IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled()) {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_PRINT_SCRIPT);
            final RtfDocument rtfDocument = new RtfDocument();
            rtfDocument.getElementList().clear();
            final List<Student> list = DataAccessServices.dao().getList(Student.class, Student.group(), getModel(component).getGroup());
            for (Student student : list) {
                final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, student.getId());
                final RtfDocument document = (RtfDocument) scriptResult.get("rtf");
                rtfDocument.addElement(document);
                //rtfDocument.getElementList().addAll(document.getElementList());
                if (list.indexOf(student) < list.size() - 1)
                    rtfDocument.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                else {
                    rtfDocument.setSettings(document.getSettings());
                    rtfDocument.setHeader(document.getHeader());
                }
            }
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("StudentCard.rtf").document(rtfDocument), true);
        }
        else
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARDS_GROUP_LIST, Collections.singletonMap("groupId", (Object) getModel(component).getGroup().getId())));
    }

    public void onClickStudentsEduOrgUnitChange(final IBusinessComponent context)
    {
        context.createChildRegion("groupTabPanel", new ComponentActivator(IUniComponents.STUDENTS_EDU_ORG_UNIT_CHANGE));
    }

    public void onClickStartEduYearEdit(final IBusinessComponent context)
    {
        context.createChildRegion("groupTabPanel", new ComponentActivator("ru.tandemservice.uni.component.group.GroupStartEduYearEdit", Collections.singletonMap("groupId", (Object) getModel(context).getGroup().getId())));
    }

    public void onClickDevelopPeriodRefresh(IBusinessComponent component)
    {
        UniStudentDevelopPeriodDaemonBean.DAEMON.wakeUpAndWaitDaemon(60 * 2);
        onRefreshComponent(component);
    }

    protected void prepareStudentDataSource(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        if (model.getStudentDataSource() != null) return;
        final DynamicListDataSource<Student> dataSource = new DynamicListDataSource<>(context, this, 20);

        List<AbstractColumn> columnList = new ArrayList<>();
        columnList.add(IndicatorColumn.createIconColumn("student", "Студент"));
        columnList.addAll(prepareColumnList(model));
        for (AbstractColumn column : columnList)
        {
            dataSource.addColumn(column);
        }

        model.setStudentDataSource(dataSource);
    }

    protected List<AbstractColumn> prepareColumnList(Model model)
    {
        List<AbstractColumn> columnList = Lists.newArrayList();

        columnList.add(new PublisherLinkColumn("ФИО", Student.FIO_KEY).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId()).add("selectedStudentTab", "studentTab");
            }
        }));
         //  студенты из архива перекрашиваются в серый цвет
        IStyleResolver styleResolverArch = new IStyleResolver()
        {
            @Override
            public String getStyle(IEntity iEntity)
            {
            Boolean archival =    ((Student)((ViewWrapper) iEntity).getEntity()).isArchival();
                if (!model.getGroup().isArchival() &&  archival)
                {
                    return "color:gray;";
                }
                return "color:black;";
            }
        };
        columnList.add(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Дата рождения", Student.person().identityCard().birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Паспорт", Student.PASSPORT_KEY).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Гражданство", Student.person().identityCard().citizenship().title().s()).setClickable(false).setStyleResolver(styleResolverArch));

        columnList.add(new SimpleColumn("Личный №", StudentGen.P_PERSONAL_NUMBER).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("№ зачетной книжки", StudentGen.P_BOOK_NUMBER).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Состояние", DAO.STATUS_KEY).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setStyleResolver(styleResolverArch));

        columnList.add(new SimpleColumn("Категория обучаемого", Student.studentCategory().title().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Целевой прием", Student.P_TARGET_ADMISSION, YesNoFormatter.INSTANCE).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Год приема", Student.entranceYear().s()).setClickable(false).setStyleResolver(styleResolverArch));

        columnList.add(new SimpleColumn("Формирующее подр.", Student.FORMATIVE_ORGUNIT_KEY).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Территориальное подр.", Student.TERRITORIAL_ORGUNIT_KEY).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Выпускающее подр.", Student.PRODUCTIVE_ORGUNIT_KEY).setClickable(false).setStyleResolver(styleResolverArch));

        columnList.add(new SimpleColumn("Направление подготовки (специальность)", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectWithCodeIndexAndGenTitle().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Направленность", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle().s()).setClickable(false).setOrderable(false).setStyleResolver(styleResolverArch));

        columnList.add(new SimpleColumn("Ориентация ОП", Student.educationOrgUnit().educationLevelHighSchool().programOrientation().shortTitle().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Присваиваемая квалификация", Student.educationOrgUnit().educationLevelHighSchool().assignedQualification().title().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));

        columnList.add(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Условие освоения", Student.educationOrgUnit().developCondition().title().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Нормативный срок", Student.educationOrgUnit().developPeriod().title().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));
        columnList.add(new SimpleColumn("Планируемый срок", Student.developPeriodAuto().title().s()).setOrderable(false).setClickable(false).setStyleResolver(styleResolverArch));

        final IndicatorColumn column = new IndicatorColumn("Печатать ЛК", null, "onClickPrintStudentPersonalCard");
        column.setOrderable(false);
        column.setPermissionKey("orgUnit_printStudentPersonalCard_group");
        column.setImageHeader(false);
        column.defaultIndicator(new IndicatorColumn.Item("printer", "Печатать ЛК"));
        columnList.add(column);

        columnList.add(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStudent").setPermissionKey("orgUnit_editStudent_group").setDisabledProperty(StudentGen.P_ARCHIVAL));
        columnList.add(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudent", "Удалить студента «{0}»?", Student.FIO_KEY).setPermissionKey("orgUnit_deleteStudent_group"));

        // все колонки, которые перечисленны в проперте - переездают в начало (в указанном порядке), остальные - следуют за ними в порядке объявления
        final String priorityColumnsProperty = StringUtils.trimToNull(ApplicationRuntime.getProperty(PRIORITY_COLUMNS));
        if (null != priorityColumnsProperty)
        {
            final List<String> list = new ArrayList<>();
            for (String columnCaption : priorityColumnsProperty.split(","))
            {
                columnCaption = StringUtils.trimToNull(columnCaption);
                list.add(columnCaption);
            }

            Collections.sort(columnList, (o1, o2) -> {
                int i1 = list.indexOf(o1.getCaption());
                if (i1 < 0)
                    i1 = Integer.MAX_VALUE;

                int i2 = list.indexOf(o2.getCaption());
                if (i2 < 0)
                    i2 = Integer.MAX_VALUE;

                return Integer.compare(i1, i2);
            });
        }

        return columnList;
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getStudentDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().set("status", null);
        component.getSettings().set("studentCustomStateCIs", null);
        component.getSettings().set("showArchivalStydent",null);
        onClickSearch(component);
    }
}
