// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelAdditional.EducationLevelAdditionalAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.entity.catalog.EducationLevelAdditional;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11.05.2010
 */
public class DAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit.DAO<EducationLevelAdditional, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        DQLSelectBuilder levelBuilder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "s")
                .where(eq(property("s", StructureEducationLevels.additional()), value(true)))
                .where(notExists(new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "ss")
                                         .where(eq(property("ss", StructureEducationLevels.additional()), value(true)))
                                        .where(eq(property("ss", StructureEducationLevels.parent()), property("s"))).buildQuery()));

        MQBuilder qualificationsBuilder = new MQBuilder(Qualifications.ENTITY_CLASS, "q")
                .add(MQExpression.in("q", Qualifications.P_CODE,
                                     QualificationsCodes.POVYSHENIE_KVALIFIKATSII,
                                     QualificationsCodes.PROFESSIONALNAYA_PEREPODGOTOVKA
        ));

        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(levelBuilder.createStatement(getSession()).list(), false));

        model.setQualificationList(qualificationsBuilder.<Qualifications>getResultList(getSession()));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
    }

    @Override
    protected String getOksoDuplicateErrorString()
    {
        return "Введенный код уже указан для другого направления подготовки (специальности)";
    }

    @Override
    public void update(Model model)
    {

        super.update(model);
    }
}
