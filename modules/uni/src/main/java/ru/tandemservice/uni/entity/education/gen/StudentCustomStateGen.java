package ru.tandemservice.uni.entity.education.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительный статус студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentCustomStateGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.education.StudentCustomState";
    public static final String ENTITY_NAME = "studentCustomState";
    public static final int VERSION_HASH = -1885751203;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_DESCRIPTION = "description";
    public static final String L_STUDENT = "student";
    public static final String L_CUSTOM_STATE = "customState";

    private Date _beginDate;     // Дата начала действия статуса
    private Date _endDate;     // Дата окончания действия статуса
    private String _description;     // Примечание
    private Student _student;     // Студент
    private StudentCustomStateCI _customState;     // Дополнительный статус студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала действия статуса.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала действия статуса.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания действия статуса.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания действия статуса.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Примечание.
     */
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Примечание.
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     */
    @NotNull
    public StudentCustomStateCI getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус студента. Свойство не может быть null.
     */
    public void setCustomState(StudentCustomStateCI customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentCustomStateGen)
        {
            setBeginDate(((StudentCustomState)another).getBeginDate());
            setEndDate(((StudentCustomState)another).getEndDate());
            setDescription(((StudentCustomState)another).getDescription());
            setStudent(((StudentCustomState)another).getStudent());
            setCustomState(((StudentCustomState)another).getCustomState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentCustomStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentCustomState.class;
        }

        public T newInstance()
        {
            return (T) new StudentCustomState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "description":
                    return obj.getDescription();
                case "student":
                    return obj.getStudent();
                case "customState":
                    return obj.getCustomState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "customState":
                    obj.setCustomState((StudentCustomStateCI) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "description":
                        return true;
                case "student":
                        return true;
                case "customState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "description":
                    return true;
                case "student":
                    return true;
                case "customState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "description":
                    return String.class;
                case "student":
                    return Student.class;
                case "customState":
                    return StudentCustomStateCI.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentCustomState> _dslPath = new Path<StudentCustomState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentCustomState");
    }
            

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getCustomState()
     */
    public static StudentCustomStateCI.Path<StudentCustomStateCI> customState()
    {
        return _dslPath.customState();
    }

    public static class Path<E extends StudentCustomState> extends EntityPath<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _description;
        private Student.Path<Student> _student;
        private StudentCustomStateCI.Path<StudentCustomStateCI> _customState;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(StudentCustomStateGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(StudentCustomStateGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(StudentCustomStateGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.StudentCustomState#getCustomState()
     */
        public StudentCustomStateCI.Path<StudentCustomStateCI> customState()
        {
            if(_customState == null )
                _customState = new StudentCustomStateCI.Path<StudentCustomStateCI>(L_CUSTOM_STATE, this);
            return _customState;
        }

        public Class getEntityClass()
        {
            return StudentCustomState.class;
        }

        public String getEntityName()
        {
            return "studentCustomState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
