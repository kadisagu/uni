/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.base.ext.OrgUnit.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.util.IDaoExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.IOrgUnitDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 26.03.2012
 */
public class DaoDoArchiveOrgUnit extends CommonDAO implements IDaoExtension
{
    @Override
    public void handle(String stateName, Map<String, Object> params)
    {
        OrgUnit orgUnit = (OrgUnit) params.get(IOrgUnitDao.PARAM_ORG_UNIT);

        @SuppressWarnings("unchecked")
        List<OrgUnit> orgUnitChildDeepList = (List<OrgUnit>) params.get(IOrgUnitDao.PARAM_ORG_UNIT_CHILD_DEEP_LIST);

        if (UniDaoFacade.getEmployeeDao().isAnyActiveStudentOnOrgUnit(orgUnit))
            throw new ApplicationException("Подразделение не может быть добавлено в архив, поскольку содержит активных студентов.");

        for (OrgUnit item : orgUnitChildDeepList)
            if (UniDaoFacade.getEmployeeDao().isAnyActiveStudentOnOrgUnit(item))
                throw new ApplicationException("Подразделение не может быть добавлено в архив, поскольку его дочернее подразделение «" + item.getFullTitle() + "» содержит активных студентов.");
    }
}
