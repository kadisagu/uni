/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.GroupStudentList;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;

import java.util.Map;

/**
 * @author vip_delete
 * @since 12.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }


    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);

        final UniScriptItem scriptItem = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.GROUP_STUDENT_LIST);
        final Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(scriptItem, "studentList", getDao().getStudentData(model) ,"group", model.getGroup());

        deactivate(component);
        return new CommonBaseRenderer().rtf().fileName((String)result.get("fileName")).document((RtfDocument)result.get("rtf"));
    }

}
