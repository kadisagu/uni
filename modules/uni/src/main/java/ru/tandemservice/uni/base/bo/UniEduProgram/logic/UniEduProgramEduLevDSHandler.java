/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/14
 */
public class UniEduProgramEduLevDSHandler extends DefaultSearchDataSourceHandler
{
    public UniEduProgramEduLevDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EducationLevels.class, "m");
        final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("m"));

        String code = context.get("code");
        String title = context.get("title");
        StructureEducationLevels levelType = context.get("levelType");

        FilterUtils.applySimpleLikeFilter(dql, "m", EducationLevels.inheritedOkso(), code);
        FilterUtils.applySimpleLikeFilter(dql, "m", EducationLevels.title(), title);

        if (null != levelType) {
            dql.where(eq(property("m", EducationLevels.levelType()), value(levelType)));
        }

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession())
            .order(orderDescriptionRegistry)
            .pageable(isPageable())
            .build();

//        for (DataWrapper wrapper: DataWrapper.wrap(output)) {
//
//        }

        return output;
    }
}



