/* $Id$ */
package ru.tandemservice.uni.catalog.bo.DevelopPeriod.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 11.09.2015
 */
public class DevelopPeriodDao extends CommonDAO implements IDevelopPeriodDao
{
    @Override
    public EduProgramDuration getEduProgramDuration(Integer numberOfYears, Integer numberOfMonths, Integer numberOfHours)
    {
        EduProgramDuration duration = getEduProgramDurationOrNull(numberOfYears, numberOfMonths, numberOfHours);

        if (duration == null)
        {
            duration = new EduProgramDuration();
            duration.setNumberOfYears(numberOfYears);
            duration.setNumberOfMonths(numberOfMonths);
            duration.setNumberOfHours(numberOfHours);
            duration.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EduProgramDuration.class));
            duration.setTitle(CommonBaseDateUtil.getPeriodWithNames(numberOfYears, numberOfMonths, null, numberOfHours, null));
            save(duration);
        }

        return duration;
    }

    @Override
    public EduProgramDuration getEduProgramDurationOrNull(Integer numberOfYears, Integer numberOfMonths, Integer numberOfHours)
    {
        return new DQLSelectBuilder()
                .fromEntity(EduProgramDuration.class, "d")
                .where(eq(property("d", EduProgramDuration.numberOfYears()), value(numberOfYears)))
                .where(eq(property("d", EduProgramDuration.numberOfMonths()), value(numberOfMonths)))
                .where(eq(property("d", EduProgramDuration.numberOfHours()), value(numberOfHours)))
                .createStatement(getSession()).uniqueResult();
    }
}
