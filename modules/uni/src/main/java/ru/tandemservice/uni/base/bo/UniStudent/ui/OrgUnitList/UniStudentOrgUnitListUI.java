/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList.OrgUnitStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 02.11.12
 */
@State(keys = {"orgUnitId", "archival"}, bindings = {"orgUnitId", "archival"})
public class UniStudentOrgUnitListUI extends AbstractUniStudentListUI
{
    public static final String PROP_ARCHIVAL = "archival";
    public static final String PROP_ORG_UNIT = "orgUnit";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private boolean _archival;
    private boolean _allowStudents;
    private boolean _allowAddStudents;
    private String _addStudentKey;
    private String _printStudentPersonCardsKey;

    @Override
    public void onComponentRefresh()
    {
        setShowDevelopTechFilter(true);
        setHideUnusedEducationOrgUnit(true);

        _orgUnit = DataAccessServices.dao().getNotNull(_orgUnitId);
        _allowStudents = UniDaoFacade.getOrgstructDao().isAllowStudents(_orgUnit);
        _allowAddStudents = UniDaoFacade.getOrgstructDao().isAllowAddStudents(_orgUnit) && !_archival;

        _addStudentKey = "orgUnit_addStudent_" + _orgUnit.getOrgUnitType().getCode();
        _printStudentPersonCardsKey = "orgUnit_printStudentPersonCards_" + ((_archival) ? "archival_" : "") + _orgUnit.getOrgUnitType().getCode();

        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
        {
            Collection<String> kindSet = UniDaoFacade.getOrgstructDao().getAllowStudentsOrgUnitKindCodes(_orgUnit);

            if (kindSet.isEmpty())
            {
                util.configWhere(EducationOrgUnit.id(), -1L, false);
            }
            else
            {
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                    util.configWhere(EducationOrgUnit.educationLevelHighSchool().orgUnit(), _orgUnit, true);

                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                    util.configWhere(EducationOrgUnit.formativeOrgUnit(), _orgUnit, true);

                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                    util.configWhere(EducationOrgUnit.territorialOrgUnit(), _orgUnit, true);
            }
        }

        super.onComponentRefresh();
    }

    protected IEducationOrgUnitContextHandler getContextHandler()
    {
        return new OrgUnitStudentListContextHandler(_archival, _orgUnit);
    }

    @Override
    public String getSettingsKey()
    {
        StringBuilder key = new StringBuilder();
        key.append("OrgUnitStudentList_").append(_orgUnit.getId());
        if (isArchival())
            key.append("_archival");
        key.append(".filter");
        return key.toString();
    }

    public void onClickAddStudent()
    {
        getActivationBuilder().asRegion(IUniComponents.STUDENT_ADD)
                .activate();
    }

    public void onClickPrintStudentPersonCards()
    {
        List<Student> studentList = getConfig().<BaseSearchListDataSource>getDataSource(UniStudentOrgUnitList.STUDENT_SEARCH_LIST_DS).getRecords();
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled()) {
            final IDocumentRenderer documentRenderer = UniStudentManger.instance().studentListPersonalCardPrintDao().getDocumentRenderer(UniBaseDao.ids(studentList));
            BusinessComponentUtils.downloadDocument(documentRenderer, true);
        }
        else
        {
            getActivationBuilder().asDesktopRoot(IUniComponents.STUDENT_PERSON_CARDS_ORG_UNIT_LIST)
                    .parameter("studentIds", UniBaseDao.ids(studentList))
                    .activate();
        }

    }

    public String getSearchListCaption()
    {
        return isArchival() ? "Список архивных студентов" : "Список студентов";
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        if (dataSource.getName().equals(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS) || dataSource.getName().equals(UniStudentManger.GROUP_DS))
        {
            dataSource.put(PROP_ARCHIVAL, _archival);
            dataSource.put(PROP_ORG_UNIT, _orgUnit);
        }
    }

    public void onClickEditStudent()
    {
        getActivationBuilder().asCurrent(IUniComponents.STUDENT_EDIT)
                .parameter("studentId", getListenerParameter())
                .activate();
    }

    public void onClickDeleteStudent()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        getConfig().<BaseSearchListDataSource>getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).getLegacyDataSource().refresh();
    }

    public void onClickPrintStudentPersonalCard()
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled()) {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_PRINT_SCRIPT);
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, getListenerParameterAsLong());
        }
        else {
            getActivationBuilder().asDesktopRoot(IUniComponents.STUDENT_PERSON_CARD)
                    .parameter("studentId", getListenerParameter())
                    .activate();
        }
    }

    // Calculate getters

    public String getEditStudentPermissionKey()
    {
        return "orgUnit_editStudent_" + _orgUnit.getOrgUnitType().getCode();
    }

    public String getDeleteStudentPermissionKey()
    {
        return "orgUnit_deleteStudent_" + _orgUnit.getOrgUnitType().getCode();
    }

    public String getPrintStudentPersonalCardPermissionKey()
    {
        return "orgUnit_printStudentPersonalCard_" + ((_archival) ? "archival_" : "") + _orgUnit.getOrgUnitType().getCode();
    }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public boolean isArchival()
    {
        return _archival;
    }

    public void setArchival(boolean archival)
    {
        _archival = archival;
    }

    public boolean isAllowStudents()
    {
        return _allowStudents;
    }

    public void setAllowStudents(boolean allowStudents)
    {
        _allowStudents = allowStudents;
    }

    public boolean isAllowAddStudents()
    {
        return _allowAddStudents;
    }

    public void setAllowAddStudents(boolean allowAddStudents)
    {
        _allowAddStudents = allowAddStudents;
    }

    public String getAddStudentKey()
    {
        return _addStudentKey;
    }

    public void setAddStudentKey(String addStudentKey)
    {
        _addStudentKey = addStudentKey;
    }

    public String getPrintStudentPersonCardsKey()
    {
        return _printStudentPersonCardsKey;
    }

    public void setPrintStudentPersonCardsKey(String printStudentPersonCardsKey)
    {
        _printStudentPersonCardsKey = printStudentPersonCardsKey;
    }
}
