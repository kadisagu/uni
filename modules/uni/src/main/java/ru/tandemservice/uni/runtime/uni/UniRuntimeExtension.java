/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.runtime.uni;

import org.tandemframework.common.catalog.runtime.CatalogGroupRuntime;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.IRuntimeExtension;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

/**
 * @author dbulychev
 */
public class UniRuntimeExtension implements IRuntimeExtension
{
    @Override
    public void init(Object object) {

        if (!IUniBaseDao.instance.get().existsEntity(AcademyData.class)) {
            IUniBaseDao.instance.get().save(new AcademyData());
        }

        if ("true".equals(ApplicationRuntime.getProperty("educationLevelBasic.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelBasic").setHidden(false);
        }
        if ("true".equals(ApplicationRuntime.getProperty("educationLevelHighGos2.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelHighGos2").setHidden(false);
        }
        if ("true".equals(ApplicationRuntime.getProperty("educationLevelHighGos3.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelHighGos3").setHidden(false);
        }
        if ("true".equals(ApplicationRuntime.getProperty("educationLevelHighGos3s.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelHighGos3s").setHidden(false);
        }
        if ("true".equals(ApplicationRuntime.getProperty("educationLevelMiddleGos2.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelMiddleGos2").setHidden(false);
        }
        if ("true".equals(ApplicationRuntime.getProperty("educationLevelMiddleGos3.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelMiddleGos3").setHidden(false);
        }
        if ("true".equals(ApplicationRuntime.getProperty("educationLevelMiddleGos3s.enabled"))) {
            CatalogGroupRuntime.getCatalog("educationLevelMiddleGos3s").setHidden(false);
        }
    }

    @Override
    public void destroy() {
    }
}
