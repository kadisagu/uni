/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3AddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

/**
 * @author oleyba
 * @since 5/8/11
 */
public class DAO  extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit.DAO<EducationLevelHighGos3, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        MQBuilder levelBuilder = new MQBuilder(StructureEducationLevels.ENTITY_CLASS, "s")
                .add(MQExpression.eq("s", StructureEducationLevels.P_HIGH, true))
                .add(MQExpression.eq("s", StructureEducationLevels.P_GOS3, true));

        MQBuilder qualificationsBuilder = new MQBuilder(Qualifications.ENTITY_CLASS, "q")
                .add(MQExpression.in("q", Qualifications.P_CODE,
                                     QualificationsCodes.BAKALAVR,
                                     QualificationsCodes.SPETSIALIST,
                                     QualificationsCodes.MAGISTR));

        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(levelBuilder.<StructureEducationLevels>getResultList(getSession()), true));

        model.setQualificationList(qualificationsBuilder.<Qualifications>getResultList(getSession()));
    }
}

