/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.mq;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.builder.expression.SimpleTextExpression;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Calendar;
import java.util.Date;

/**
 * @author vip_delete
 * @since 20.10.2008
 */
public class UniMQExpression
{
    private static AbstractExpression inYear(String alias, String property, int year, int firstMonth)
    {
        Calendar calendar = Calendar.getInstance();
        //noinspection MagicConstant
        calendar.set(year, firstMonth, 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        AbstractExpression e1 = MQExpression.greatOrEq(alias, property, calendar.getTime());

        calendar.add(Calendar.YEAR, 1);
        AbstractExpression e2 = MQExpression.less(alias, property, calendar.getTime());

        return MQExpression.and(e1, e2);
    }

    public static AbstractExpression eqEduYear(String alias, String property, EducationYear value)
    {
        return inYear(alias, property, value.getIntValue(), Calendar.SEPTEMBER);
    }

    public static AbstractExpression eqYear(String alias, String property, int year)
    {
        return inYear(alias, property, year, Calendar.JANUARY);
    }

    public static AbstractExpression eqDate(String alias, String property, Date date)
    {
        return betweenDate(alias, property, date, date);
    }

    public static AbstractExpression betweenDate(String alias, String property, Date dateFrom, Date dateTo)
    {
        return MQExpression.and(
                greatOrEq(alias, property, dateFrom),
                lessOrEq(alias, property, dateTo)
        );
    }

    public static AbstractExpression greatOrEq(String alias, String property, Date date)
    {
        return MQExpression.greatOrEq(alias, property, CoreDateUtils.getDayFirstTimeMoment(date));
    }

    public static AbstractExpression lessOrEq(String alias, String property, Date date)
    {
        return MQExpression.less(alias, property, CoreDateUtils.getNextDayFirstTimeMoment(date, 1));
    }

    public static AbstractExpression searchByFio(String idCardAlias, String filter)
    {
        filter = CoreStringUtils.escapeLike(filter);
        filter = filter.replace(" ", "%");
        final SimpleTextExpression searchWithoutMiddleNameExpression = new SimpleTextExpression(
                "upper(concat("+idCardAlias+"." + IdentityCardGen.P_LAST_NAME +
                ",concat("+idCardAlias+"." + IdentityCardGen.P_FIRST_NAME +
                "))) like '" + filter + "'");
        final SimpleTextExpression searchWithMiddleNameExpression = new SimpleTextExpression(
                "upper(concat("+idCardAlias+"." + IdentityCardGen.P_LAST_NAME +
                ",concat("+idCardAlias+"." + IdentityCardGen.P_FIRST_NAME +
                ",concat("+idCardAlias+"." + IdentityCardGen.P_MIDDLE_NAME +
                ")))) like '" + filter + "'");
        return MQExpression.or(
                MQExpression.and(
                        MQExpression.isNotNull(idCardAlias, IdentityCardGen.P_MIDDLE_NAME),
                        searchWithMiddleNameExpression),
                MQExpression.and(
                        MQExpression.isNull(idCardAlias, IdentityCardGen.P_MIDDLE_NAME),
                        searchWithoutMiddleNameExpression));
    }
}
