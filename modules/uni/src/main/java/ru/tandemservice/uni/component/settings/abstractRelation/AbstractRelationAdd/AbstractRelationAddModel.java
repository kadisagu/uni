/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
@Input(keys = "firstId", bindings = "firstId")
public class AbstractRelationAddModel
{
    private long _firstId;
    private IEntity _first;
    private IEntity _second;
    private List<IEntity> _secondList;
    private List<HSelectOption> _secondHierarchyList;
    private boolean _hierarchical;

    public long getFirstId()
    {
        return _firstId;
    }

    public void setFirstId(long firstId)
    {
        _firstId = firstId;
    }

    public IEntity getFirst()
    {
        return _first;
    }

    public void setFirst(IEntity first)
    {
        _first = first;
    }

    public IEntity getSecond()
    {
        return _second;
    }

    public void setSecond(IEntity second)
    {
        _second = second;
    }

    public List<IEntity> getSecondList()
    {
        return _secondList;
    }

    public void setSecondList(List<IEntity> secondList)
    {
        _secondList = secondList;
    }

    public List<HSelectOption> getSecondHierarchyList()
    {
        return _secondHierarchyList;
    }

    public void setSecondHierarchyList(List<HSelectOption> secondHierarchyList)
    {
        _secondHierarchyList = secondHierarchyList;
    }

    public boolean isHierarchical()
    {
        return _hierarchical;
    }

    public void setHierarchical(boolean hierarchical)
    {
        _hierarchical = hierarchical;
    }
}
