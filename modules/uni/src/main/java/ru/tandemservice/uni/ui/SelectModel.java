package ru.tandemservice.uni.ui;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.proxy.CGLIBLazyInitializer;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;

import java.util.*;

/**
 * @author vdanilov
 */
public class SelectModel<T> {

    public SelectModel() {}
    public SelectModel(final ISelectModel source) { this.setSource(source); }
    public <U extends T> SelectModel(final Collection<U> source) { this.setSource(source); }

    @SuppressWarnings("unchecked")
    public <U extends T> SelectModel(final U... source) { this.setSource(source); }


    private T value;
    public T getValue() { return this.value; }
    public void setValue(final T value) { this.value = value; }

    private ISelectModel source;
    public ISelectModel getSource() { return this.source; }
    public void setSource(final ISelectModel source) { this.source = source; }

    private boolean disabled = false;
    public boolean isDisabled() { return this.disabled; }
    public void setDisabled(final boolean disabled) { this.disabled = disabled; }

    public SelectModel<T> setSingletonValue(final T value, final boolean disabled) {
        this.setDisabled(disabled);
        return this.setSingletonValue(value);
    }

    public SelectModel<T> setSingletonValue(final T value) {
        this.setValue(value);
        return this.setSource((null == value) ? ImmutableList.of() : ImmutableList.of(value));
    }

    public <U extends T> SelectModel<T> setSource(final Collection<U> elements) {
        this.setSource(new LazySimpleSelectModel<>(elements));
        return this;
    }

    @SuppressWarnings("unchecked")
    public <U extends T> SelectModel<T> setSource(final U... elements) {
        return this.setSource(Arrays.asList(elements));
    }

    public Long getValueId() {
        // здесь может случится ClassCaseException - и это правильно
        return ((IIdentifiable)this.getValue()).getId();
    }

    @SuppressWarnings("unchecked")
    public void setValueId(final Long id) {
        // здесь может быть потенциальная подмена типов
        this.setValue(null == id ? null : (T) CGLIBLazyInitializer.getProxy(id));
    }


    public void clear() {
        this.setSource(new StaticSelectModel("id", "title", Collections.<T>emptyList()));
        this.setValue(null);
    }

    @SuppressWarnings("unchecked")
    public void setupFirstValue() {
        try {
            final ISelectModel source = this.getSource();
            final List objects = source.findValues(null).getObjects();
            final Object next = objects.iterator().next();
            final Object pKey = source.getPrimaryKey(next);

            if (source instanceof ISingleSelectModel) {
                this.setValue((T) ((ISingleSelectModel) source).getValue(pKey) );
            } else if (source instanceof IMultiSelectModel) {
                final List values = ((IMultiSelectModel) source).getValues(Collections.singleton(pKey));
                this.setValue((T) values.iterator().next());
            } else {
                this.setValue((T)next);
            }
        } catch (final Exception e) {
            Debug.exception(e);
            // do nothing, just init
        }
    }

}
