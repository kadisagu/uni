package ru.tandemservice.uni.catalog.ext.EduProgramSubjectIndex;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;

/**
 * @author vdanilov
 */
@Configuration
public class EduProgramSubjectIndexExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private EduProgramSubjectIndexManager _eduProgramSubjectIndexManager;

    @Bean
    public ItemListExtension<Runnable> cleanUpSubjectIndexDataCallbackListExtension()
    {
        return itemListExtension(_eduProgramSubjectIndexManager.beforeCleanUpSubjectIndexDataCallbackList())
                .add("uni.cleanup", UniEduProgramManager.instance().syncDao()::doCleanUpSubjectIndexDataCallback)
                .create();
    }

}
