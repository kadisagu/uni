/* $Id$ */
package ru.tandemservice.uni.dao;

import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.utils.SharedDQLUtils;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;
import static org.tandemframework.hibsupport.dql.DQLFunctions.isNumeric;
import static org.tandemframework.hibsupport.dql.DQLFunctions.substring;

/**
 * @author Nikolay Fedorovskih
 * @since 13.01.2014
 */
public class StudentDAO extends UniBaseDao implements IStudentDAO
{

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFullTitleExtended(Student student)
    {
        //noinspection StringBufferReplaceableByString
        return new StringBuilder("Студент: ")
                .append(student.getPerson().getFullFio())
                .append(" (").append(student.getStatus().getTitle()).append(", ")
                .append(student.getEducationOrgUnit().getDevelopForm().getTitle()).append(", ") // вид обучения
                .append(student.getCompensationType().getShortTitle()).append(")")              // вид возмещения затрат
                .append(" | Группа: ").append(((null != student.getGroup()) ? student.getGroup().getTitle() : "---"))
                .append(" | ").append(student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle())
                .append(" (").append(student.getEducationOrgUnit().getTerritorialOrgUnit().getShortTitle()).append(") ")
                .append(" | ").append(student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle())
                .toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFreePersonalNumber(String prefix, int min, int max, int capacity)
    {
        final Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "studentNumberGenerationSync" + prefix, 150);

        Preconditions.checkNotNull(prefix);
        Preconditions.checkArgument(min < max);
        Preconditions.checkArgument(min >= 0);
        Preconditions.checkArgument(capacity > 0);
        Preconditions.checkArgument(capacity > (int) Math.log10(max));

        final int prefixSize = prefix.length();
        final String minStr = prefix + StringUtils.leftPad(String.valueOf(min), capacity, '0');
        final String maxStr = prefix + StringUtils.leftPad(String.valueOf(max), capacity, '0');

        // Сначала ищем максимальный занятый номер в указанном диапазоне
        final String maxNumber = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .where(eq(DQLFunctions.length(property("s", Student.P_PERSONAL_NUMBER)), value(prefixSize + capacity)))
                .where(like(property("s", Student.P_PERSONAL_NUMBER), value(prefix + StringUtils.repeat("_", capacity))))
                .where(eq(isNumeric(
                        concat(
                           substring(property("s", Student.P_PERSONAL_NUMBER), value(prefixSize + 1), value(capacity)), // <-- Это числовая вариативная часть номера
                           value(".e0")) // <-- Это хак, чтобы isNumeric возвращал true только для целых чисел (см. http://www.tek-tips.com/faqs.cfm?fid=6423 ).
                ), value(true)))
                .column(property("s", Student.P_PERSONAL_NUMBER)).top(1)
                .order(property("s", Student.P_PERSONAL_NUMBER), OrderDirection.desc)
                .createStatement(session)
                .uniqueResult();

        // Если такого номера и минимальный номер диапазона не занят возвращаем минимальный номер
        if (maxNumber == null && !existsEntity(Student.class, Student.P_PERSONAL_NUMBER, minStr))
            return minStr;

        Integer free = null;
        if (maxNumber != null && !maxStr.equals(maxNumber))
        {
            // Максимальный номер из диапазона ещё не занят, пробелы в нумерации можно пока не искать
            // Берем правую числовую часть и увеличиваем её на единицу
            free = Ints.tryParse(StringUtils.right(maxNumber, capacity));
            if (free != null) {
                free++;
            }
        }

        if (free == null)
        {
            // Если максимально занятого нет или предыдущий запрос вернул максимальный номер дапазона, ищем пробел в нумерации
            free = SharedDQLUtils.getFirstStringNumberHole(Student.class, Student.P_PERSONAL_NUMBER, prefix, min, max, capacity)
                    .createStatement(session)
                    .uniqueResult();
        }

        if (free != null)
            return prefix + StringUtils.leftPad(String.valueOf(free), capacity, '0');

        // Свободных номеров в указанном диапазоне нет
        return null;
    }

    @Override
    public String generateNewPersonalNumber(Student student)
    {
        final String prefix = StringUtils.leftPad(String.valueOf(student.getEntranceYear() % 100), 2, '0');
        final int capacity = StudentNumberFormatter.getCapacity();
        final int minNumber = 1;
        final int maxNumber =  (int) Math.pow(10, capacity) - 1;
        return getFreePersonalNumber(prefix, minNumber, maxNumber, capacity);
    }

    @Override
    public boolean isStudentCardPrintLegacyVersionEnabled() {
        return ("true").equals(ApplicationRuntime.getProperty("uni.studentCardPrint.legacyVersion.enabled"));
    }
}