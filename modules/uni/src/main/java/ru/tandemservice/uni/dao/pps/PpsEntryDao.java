// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao.pps;

import com.google.common.collect.Maps;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 22.09.2010
 */
public class PpsEntryDao extends UniBaseDao implements IPpsEntryDao
{
    @Override
    public PpsEntry getCurrentPpsEntry(IPrincipalContext context)
    {
        if (!(context instanceof EmployeePost))
            throw new ApplicationException("Вы не зарегистрированы в реестре ППС.");

        return new DQLSelectBuilder().fromEntity(EmployeePost4PpsEntry.class, "e")
                .column(property("e", EmployeePost4PpsEntry.ppsEntry()))
                .where(eq(property("e", EmployeePost4PpsEntry.employeePost()), value(context)))
                .where(isNull(property("e", EmployeePost4PpsEntry.ppsEntry().removalDate())))
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public List<PpsEntry> getPpsList(IPrincipalContext principalContext)
    {
        if (principalContext == null)
            return Collections.emptyList();

        Person2PrincipalRelation rel = get(Person2PrincipalRelation.class, Person2PrincipalRelation.principal().s(), principalContext.getPrincipal());

        if (null == rel)
            return Collections.emptyList();

        Person person = rel.getPerson();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PpsEntry.class, "pps")
                .column("pps")
                .where(eq(property(PpsEntry.person().fromAlias("pps")), value(person)));

        return dql.createStatement(getSession()).list();
    }

    @Override
    public List<ScienceDegree> getScienceDegrees(PpsEntry pps)
    {
        DQLSelectBuilder subQueryBuilder = new DQLSelectBuilder()
                .fromEntity(PersonAcademicDegree.class, "pd")
                .column(PersonAcademicDegree.academicDegree().id().fromAlias("pd").s())
                .where(DQLExpressions.eq(DQLExpressions.property("pd", PersonAcademicDegree.person().id()), DQLExpressions.value(pps.getPerson().getId())));

        DQLSelectBuilder degreeQueryBuilder = new DQLSelectBuilder()
                .fromEntity(ScienceDegree.class, "degree")
                .column(DQLExpressions.property("degree"))
                .where(DQLExpressions.in(ScienceDegree.id().fromAlias("degree").s(), subQueryBuilder.buildQuery()))
                .order(DQLExpressions.property(ScienceDegree.type().level().fromAlias("degree")))
                .order(DQLExpressions.property(ScienceDegree.shortTitle().fromAlias("degree")));
        return degreeQueryBuilder.createStatement(getSession()).list();
    }

    @Override
    public List<ScienceStatus> getScienceStatuses(PpsEntry pps)
    {
        DQLSelectBuilder subQueryBuilder = new DQLSelectBuilder()
                .fromEntity(PersonAcademicStatus.class, "ps")
                .column(PersonAcademicStatus.academicStatus().id().fromAlias("ps").s())
                .where(DQLExpressions.eq(DQLExpressions.property("ps", PersonAcademicStatus.person().id()), DQLExpressions.value(pps.getPerson().getId())));

        DQLSelectBuilder statusQueryBuilder = new DQLSelectBuilder()
                .fromEntity(ScienceStatus.class, "status")
                .column(DQLExpressions.property("status"))
                .where(DQLExpressions.in(ScienceStatus.id().fromAlias("status").s(), subQueryBuilder.buildQuery()))
                .order(DQLExpressions.property(ScienceStatus.type().level().fromAlias("status")))
                .order(DQLExpressions.property(ScienceStatus.shortTitle().fromAlias("status")));
        return statusQueryBuilder.createStatement(getSession()).list();
    }

    @Override
    public Map<PpsEntry, List<IEntity>> getPpsEntry2TimeWorkerMap(List<Long> ppsEntryIds)
    {
        return Maps.newHashMap();
    }

    @Override
    public DQLSelectBuilder getPpsEntryByTimeworkerBuilder(String alias, OrgUnit orgUnit, EducationYear eduYear)
    {
        // вызывается только если подключен М Почасовики
        throw new IllegalStateException("don't use this method.");
    }
}
