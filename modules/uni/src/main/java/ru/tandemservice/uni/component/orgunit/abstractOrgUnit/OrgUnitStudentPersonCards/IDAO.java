/**
 * $Id$
 */
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards;

import org.tandemframework.core.document.IDocumentRenderer;
import ru.tandemservice.uni.entity.employee.Student;


/**
 * @author dseleznev
 * Created on: 09.10.2008
 */
public interface IDAO
{

    IDocumentRenderer getDocumentRenderer(Model model);

    void setDocumentTemplate(IOrgUnitStudentPersonCardPrintFactory factory, Student student);

}