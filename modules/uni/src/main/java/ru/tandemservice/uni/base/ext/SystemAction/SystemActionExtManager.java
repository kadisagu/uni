/**
 *$Id$
 */
package ru.tandemservice.uni.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uni.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        final IItemListExtensionBuilder<SystemActionDefinition> itemListExtensionBuilder = itemListExtension(_systemActionManager.buttonListExtPoint());

        return itemListExtensionBuilder
            .add("unisc_changeStudentStatus", new SystemActionDefinition("uni", "changeStudentStatus", "onClickChangeStudentStatus", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uni_correctIdentityCardFIO", new SystemActionDefinition("uni", "correctIdentityCardFIO", "onClickCorrectIdentityCardFIO", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uni_updateAddressItemsFullTitles", new SystemActionDefinition("uni", "updateAddressItemsFullTitles", "onClickUpdateAddressItemsFullTitles", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("unisc_actualiseGroupTitles", new SystemActionDefinition("uni", "actualiseGroupTitles", "onClickActualiseGroupTitles", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("unisc_getEduLevelsData", new SystemActionDefinition("uni", "getEduLevelsData", "onClickGetEduLevelsData", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uni_getPersonData", new SystemActionDefinition("uni", "getPersonData", "onClickGetPersonData", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uni_deleteCategorylessPersons", new SystemActionDefinition("uni", "deleteCategorylessPersons", "onClickDeleteCategorylessPersons", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uniorgunit_getOrdStructSchema", new SystemActionDefinition("uni", "getOrdStructSchema", "onClickGetOrdStructSchema", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uniemp_actualisePpsEntryByEmployeePost", new SystemActionDefinition("uni", "actualisePpsEntryByEmployeePost", "onClickUpdatePpsEntryByEmployeePost", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uniio_exportTemplate4Student", new SystemActionDefinition("uni", "exportTemplate4Student", "onClickExportTemplate4Student", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uniio_importStudent", new SystemActionDefinition("uni", "importStudent", "onClickImportStudents", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uni_exportEduData", new SystemActionDefinition("uni", "exportEduData", "onClickExportEduData", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .add("uni_exportExcludedStudentToXls", new SystemActionDefinition("uni", "exportExcludedStudentToXls", "onClickExportExcludedStudentToXls", SystemActionPubExt.UNI_SYSTEM_ACTION_PUB_ADDON_NAME))
            .create();
    }
}
