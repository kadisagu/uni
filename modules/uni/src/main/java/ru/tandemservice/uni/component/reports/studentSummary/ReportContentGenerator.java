/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentSummary;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.report.StudentSummaryReport;
import ru.tandemservice.uni.report.xls.CellFormatFactory;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author oleyba
 * @since 21.03.2009
 */
public class ReportContentGenerator
{
    private StudentSummaryReport _report;
    private ReportParams _params;
    private Session _session;
    private CellFormatFactory _cellFormatFactory = new CellFormatFactory();
    private ErrorCollector _errors;

    private boolean printSixCourseColumn = false;

    public ReportContentGenerator(StudentSummaryReport report, ReportParams params, Session session, ErrorCollector errors)
    {
        _report = report;
        _params = params;
        _session = session;
        _errors = errors;
    }

    public byte[] generateReportContent() throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        WritableSheet sheet = workbook.createSheet("Сводка", 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(100);
        sheet.getSettings().setBottomMargin(.3937);
        sheet.getSettings().setTopMargin(.3937);
        sheet.getSettings().setLeftMargin(.3937);
        sheet.getSettings().setRightMargin(.3937);

        sheet.setColumnView(0, 12);
        sheet.setColumnView(1, 4);
        sheet.setColumnView(2, 49);
        for (int i = 3; i <= 3 + 4 * 7; i++)
            sheet.setColumnView(i, 6);

        WritableFont font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableCellFormat header = new WritableCellFormat(font);
        _cellFormatFactory.registerFont(font);
        _cellFormatFactory.registerCellFormat("header", header);
        sheet.addCell(new Label(0, 0, "Контингент студентов", header));
        sheet.mergeCells(0, 0, 7, 0);
        sheet.addCell(new Label(0, 1, _params.getDevelopForm().getTitle() + " форма обучения на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()), header));
        sheet.mergeCells(0, 1, 7, 1);

        int row = 3;
        for (SettlementBlock block : prepareLines())
            row = printSettlement(sheet, block, row);
        sheet.addCell(new Label(2, row, "Итого по бюджету", getCellFormat(false, Bold.YES, Alignment.LEFT, Color.LIGHT_TURQUOISE)));
        printLine(sheet, row++, Bold.YES, Color.LIGHT_TURQUOISE, report_budget_total);
        sheet.addCell(new Label(2, row, "Итого по внебюджету", getCellFormat(false, Bold.YES, Alignment.LEFT, Color.IVORY)));
        printLine(sheet, row++, Bold.YES, Color.IVORY, report_contract_total);
        sheet.addCell(new Label(2, row, "Итого по всему вузу", getCellFormat(false, Bold.YES, Alignment.LEFT, Color.CORAL)));
        printLine(sheet, row, Bold.YES, Color.CORAL, report_total);

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    CourseSummary[] report_contract_total = new CourseSummary[8];
    CourseSummary[] report_budget_total = new CourseSummary[8];
    CourseSummary[] report_total = new CourseSummary[8];

    {
        for (int i = 1; i <= 7; i++)
        {
            report_contract_total[i] = new CourseSummary();
            report_budget_total[i] = new CourseSummary();
            report_total[i] = new CourseSummary();
        }
    }

    // для поиска строк, к которым можно было бы присоединить специализации на основании всех параметров ключа
    // ключ - id of settlement, id of formativeOrgUnit, code of compemsationType, displayableTitle (сюда входит оксо и квалификация).
    Map<MultiKey, EduLevelLine> allLinesMap = new HashMap<MultiKey, EduLevelLine>();
    Map<EduLevelLine, Object> revertedMap = new HashMap<EduLevelLine, Object>();

    // строит список строк отчета - объектов
    @SuppressWarnings("unchecked")
    private List<SettlementBlock> prepareLines()
    {
        // загружаем данные для построения отчета
        List<Object[]> queryList = new ArrayList<Object[]>();
        {
            String formOu = EducationOrgUnit.formativeOrgUnit().s();
            String terrOu = EducationOrgUnit.territorialOrgUnit().s();

            Criteria criteria = _session.createCriteria(Student.ENTITY_CLASS, "student");

            criteria.createAlias("student." + Student.educationOrgUnit().s(), "eduOu");
            criteria.createAlias("eduOu." + EducationOrgUnit.educationLevelHighSchool().s(), "levelhs");
            criteria.createAlias("levelhs." + EducationLevelsHighSchool.educationLevel().s(), "level");
            criteria.createAlias("eduOu." + formOu, "formOu");
            criteria.createAlias("eduOu." + terrOu, "terrOu", CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("student." + Student.course().s(), "course");
            criteria.createAlias("student." + Student.status().s(), "status");
            criteria.createAlias("student." + Student.compensationType().s(), "comp");

            criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("eduOu.id"))
                .add(Projections.groupProperty("course.code"))
                .add(Projections.groupProperty("status.code"))
                .add(Projections.groupProperty("comp.code"))
                .add(Projections.count("student.id")));

            criteria.add(Restrictions.eq("student." + Student.archival().s(), java.lang.Boolean.FALSE));

            if (_report.getOrgUnit() != null)
                criteria.add(Restrictions.or(Restrictions.eq("eduOu." + formOu, _report.getOrgUnit()), Restrictions.eq("eduOu." + terrOu, _report.getOrgUnit())));
            criteria.add(Restrictions.eq("eduOu." + EducationOrgUnit.developForm().s(), _params.getDevelopForm()));
            if (_params.isFormativeOrgUnitActive())
                addInRestricton("eduOu." + formOu, criteria, _params.getFormativeOrgUnitList());
            if (_params.isTerritorialOrgUnitActive())
                addInRestricton("eduOu." + terrOu, criteria, _params.getTerritorialOrgUnitList());
            if (_params.isDevelopConditionActive())
                addInRestricton("eduOu." + EducationOrgUnit.developCondition().s(), criteria, _params.getDevelopConditionList());
            if (_params.isDevelopTechActive())
                addInRestricton("eduOu." + EducationOrgUnit.developTech().s(), criteria, _params.getDevelopTechList());
            if (_params.isDevelopPeriodActive())
                addInRestricton("eduOu." + EducationOrgUnit.developPeriod().s(), criteria, _params.getDevelopPeriodList());
            if (_params.isQualificationActive())
                addInRestricton("level." + EducationLevels.qualification().s(), criteria, _params.getQualificationList());
            if (_params.isStudentCategoryActive())
                addInRestricton("student." + Student.studentCategory().s(), criteria, _params.getStudentCategoryList());
            //addInRestricton("student." + status, criteria, _params.getStudentStatusAllList());
            queryList.addAll(criteria.list());
        }

        // загружаем направления подготовки ОУ и ВПО
        Set<Long> eduOuIdList = new HashSet<Long>();
        for (Object[] row : queryList)
            eduOuIdList.add((Long) row[0]);
        Map<Long, EducationOrgUnit> eduOuMap = new HashMap<Long, EducationOrgUnit>();
        MQBuilder eduOuBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "e")
        .add(MQExpression.in("e", "id", eduOuIdList))
        .addLeftJoinFetch("e", EducationOrgUnit.formativeOrgUnit().s(), "f")
        .addLeftJoinFetch("e", EducationOrgUnit.territorialOrgUnit().s(), "t")
        .addLeftJoinFetch("e", EducationOrgUnit.educationLevelHighSchool().s(), "hs");
        for (EducationOrgUnit eduOu : eduOuBuilder.<EducationOrgUnit>getResultList(_session))
            eduOuMap.put(eduOu.getId(), eduOu);

        // загружаем состояния студентов
        Map<String, StudentStatus> statusMap = new HashMap<String, StudentStatus>();
        for (StudentStatus status : UniDaoFacade.getCoreDao().getCatalogItemList(StudentStatus.class))
            statusMap.put(status.getCode(), status);

        Map<AddressItem, SettlementBlock> blockMap = new HashMap<AddressItem, SettlementBlock>();

        // обрабатываем данные запроса - создаем строки для направлений ОУ
        for (Object[] row : queryList)
        {
            EducationOrgUnit eduOu = eduOuMap.get((Long) row[0]);
            int course = Integer.valueOf((String) row[1]);
            StudentStatus status = statusMap.get((String) row[2]);
            String comp = (String) row[3];
            int sum = ((java.lang.Number) row[4]).intValue();

            boolean budget = UniDefines.COMPENSATION_TYPE_BUDGET.equals(comp);
            if (course == 6) printSixCourseColumn = true;

            CourseSummary summary = new CourseSummary();

            if (_params.getStudentStatusAllList().contains(status))
                summary.total += sum;
            if (_params.getStudentStatusAcademList().contains(status))
                summary.academ += sum;
            if (_params.isPregActive() && _params.getStudentStatusPregList().contains(status))
                summary.preg += sum;
            if (_params.isChildActive() && _params.getStudentStatusChildList().contains(status))
                summary.child += sum;

            if (summary.total == 0 && summary.academ == 0 && summary.preg == 0 && summary.child == 0)
                continue;

            OrgUnit addressOwner = eduOu.getTerritorialOrgUnit().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) ? eduOu.getTerritorialOrgUnit() : eduOu.getFormativeOrgUnit();
            if (null == addressOwner.getAddress())
                _errors.add("Для подразделения «" + addressOwner.getTitle() + "» (" + addressOwner.getOrgUnitType().getTitle() + ") не указан фактический адрес.");
            else if (null == addressOwner.getAddress().getSettlement())
                _errors.add("Для подразделения «" + addressOwner.getTitle() + "» (" + addressOwner.getOrgUnitType().getTitle() + ") в фактическом адресе не указан населенный пункт.");

            if (_errors.hasErrors())
                break;

            AddressItem settlement = addressOwner.getAddress().getSettlement();
            SettlementBlock settlementBlock = blockMap.get(settlement);
            if (null == settlementBlock)
                blockMap.put(settlement, settlementBlock = new SettlementBlock(settlement));
            FormativeOrgUnitBlock orgUnitBlock = settlementBlock.orgUnits.get(eduOu.getFormativeOrgUnit());
            if (null == orgUnitBlock)
                settlementBlock.orgUnits.put(eduOu.getFormativeOrgUnit(), orgUnitBlock = new FormativeOrgUnitBlock(eduOu.getFormativeOrgUnit(), settlementBlock));
            Map<Object, EduLevelLine> lineMap = budget ? orgUnitBlock.budget_lines : orgUnitBlock.contract_lines;
            EducationLevelsHighSchool levelHS = eduOu.getEducationLevelHighSchool();
            EduLevelLine line;
            // если отчет с выделением специализаций - просто берем строку по напр. вуза
            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_params.getShowSpecializations()))
            {
                line = lineMap.get(levelHS);
                if (null == line)
                    lineMap.put(levelHS, line = new EduLevelLine(levelHS, orgUnitBlock));
            }
            // если не выделяем специализации, то химичим с подбором строки (см. #4231)
            else
            {
                // ключом для специализации будет родительское напр. ВПО, а для остальных типов - напр. вуза
                Object key = levelHS.isSpecialization() ? levelHS.getEducationLevel().getParentLevel() : levelHS;
                // название соответственно
                String title = levelHS.isSpecialization() ? levelHS.getEducationLevel().getParentLevel().getDisplayableTitle() : levelHS.getDisplayableTitle();
                // ищем такую строку в нашей таблице по этому ключу
                line = lineMap.get(key);
                if (null == line)
                {
                    // не нашли - ищем подходящую строку по общей таблице всех строк по сложному ключу
                    MultiKey multiKey = new MultiKey(settlement.getId(), eduOu.getFormativeOrgUnit().getId(), comp, title);
                    line = allLinesMap.get(multiKey);
                    // опять не нашли - создаем новую строку, кладем во все мапы
                    if (null == line)
                    {
                        line = new EduLevelLine(levelHS, orgUnitBlock);
                        lineMap.put(key, line);
                        allLinesMap.put(multiKey, line);
                        revertedMap.put(line, key);
                    }
                    // нашли, тогда если у нас не специализация - достаем строку из мапы нашего блока,
                    // и кладем ее по новому ключу - напр. вуза, а не впо,
                    // чтобы теперь в нее попадали только такие же направления вуза
                    else if (!levelHS.isSpecialization())
                    {
                        lineMap.remove(revertedMap.get(line));
                        lineMap.put(key, line);
                    }
                }
            }
            // так количества добавятся и в "7 курс" - итоговые столбцы
            line.addSummary(course, summary, budget);
        }

        if (_errors.hasErrors())
            return Collections.emptyList();

        final List<SettlementBlock> result = new ArrayList<SettlementBlock>();
        result.addAll(blockMap.values());
        // сортируем результаты
        TopOrgUnit academy = TopOrgUnit.getInstance();
        if (null == academy.getAddress())
            throw new ApplicationException("Для подразделения «" + academy.getTitle() + "» (" + academy.getOrgUnitType().getTitle() + ") не указан фактический адрес.");
        if (null == academy.getAddress().getSettlement())
            throw new ApplicationException("Для подразделения «" + academy.getTitle() + "» (" + academy.getOrgUnitType().getTitle() + ") в фактическом адресе не указан населенный пункт.");
        final AddressItem settlement = academy.getAddress().getSettlement();
        // по названию, но город вуза первым
        Collections.sort(result, new Comparator<SettlementBlock>()
            {
            @Override
            public int compare(SettlementBlock o1, SettlementBlock o2)
            {
                if (o1.settlement.equals(settlement))
                    return -1;
                if (o2.settlement.equals(settlement))
                    return 1;
                return o1.settlement.getTitle().compareTo(o2.settlement.getTitle());
            }
            });
        return result;
    }

    private <T extends IEntity> void addInRestricton(String property, Criteria criteria, Collection<T> entites)
    {
        List<Long> idList = new ArrayList<Long>();
        for (IEntity entity : entites)
            idList.add(entity.getId());
        criteria.add(Restrictions.in(property + ".id", idList));
    }

    // вспомогательный объект для построения отчета - блок города
    private class SettlementBlock
    {
        Map<OrgUnit, FormativeOrgUnitBlock> orgUnits = new HashMap<OrgUnit, FormativeOrgUnitBlock>();
        CourseSummary[] contract_total = new CourseSummary[8];
        CourseSummary[] budget_total = new CourseSummary[8];
        AddressItem settlement;

        SettlementBlock(AddressItem settlement)
        {
            this.settlement = settlement;
            for (int i = 1; i <= 7; i++)
            {
                contract_total[i] = new CourseSummary();
                budget_total[i] = new CourseSummary();
            }
        }
    }

    // вспомогательный объект для построения отчета - блок подразделения
    private class FormativeOrgUnitBlock
    {
        Map<Object, EduLevelLine> budget_lines = new HashMap<Object, EduLevelLine>();
        Map<Object, EduLevelLine> contract_lines = new HashMap<Object, EduLevelLine>();
        CourseSummary[] contract_total = new CourseSummary[8];
        CourseSummary[] budget_total = new CourseSummary[8];
        CourseSummary[] total = new CourseSummary[8];
        OrgUnit formative;
        SettlementBlock settlementBlock;

        FormativeOrgUnitBlock(OrgUnit formative, SettlementBlock settlementBlock)
        {
            this.formative = formative;
            this.settlementBlock = settlementBlock;
            for (int i = 1; i <= 7; i++)
            {
                contract_total[i] = new CourseSummary();
                budget_total[i] = new CourseSummary();
                total[i] = new CourseSummary();
            }
        }

        void addSummary(int course, CourseSummary summary, boolean budget)
        {
            if (budget)
                for (int i : new int[]{course, 7})
                {
                    budget_total[i].inc(summary);
                    total[i].inc(summary);
                    settlementBlock.budget_total[i].inc(summary);
                    report_budget_total[i].inc(summary);
                    report_total[i].inc(summary);
                }
            else
                for (int i : new int[]{course, 7})
                {
                    contract_total[i].inc(summary);
                    total[i].inc(summary);
                    settlementBlock.contract_total[i].inc(summary);
                    report_contract_total[i].inc(summary);
                    report_total[i].inc(summary);
                }
        }
    }

    // вспомогательный объект для построения отчета - строка отчета
    private class EduLevelLine implements Comparable
    {
        CourseSummary[] courseSummaries = new CourseSummary[8];
        EducationLevels level;
        FormativeOrgUnitBlock block;
        String title;
        //boolean titleFromSpecialization;

        EduLevelLine(EducationLevelsHighSchool levelHS, FormativeOrgUnitBlock block)
        {
            this.level = levelHS.getEducationLevel();
            this.block = block;
            for (int i = 1; i <= 7; i++)
                courseSummaries[i] = new CourseSummary();
            // названием строки будет любое название напр. вуза
            title = levelHS.getDisplayableTitle();
            // берем из напр. ВПО соотв. специальности, если это специализация
            if (!TwinComboDataSourceHandler.getSelectedValueNotNull(_params.getShowSpecializations()) && levelHS.isSpecialization())
                title = level.getParentLevel().getDisplayableTitle();
        }

        void addSummary(int course, CourseSummary summary, boolean budget)
        {
            courseSummaries[course].inc(summary);
            courseSummaries[7].inc(summary);
            block.addSummary(course, summary, budget);
            // заменяем название, если есть необходимость и возможность, на название не из специализации
            //if (titleFromSpecialization && !levelHS.isSpecialization())
            //    title = levelHS.getDisplayableTitle();
        }

        String title()
        {
            return title;
        }

        @Override
        public int compareTo(Object o)
        {
            EduLevelLine other = (EduLevelLine) o;

            int result;
            if (0 != (result = compare(level.getSafeQCode(), other.level.getSafeQCode()))) { return result; }
            if (0 != (result = compare(level.getTitleCodePrefix(), other.level.getTitleCodePrefix()))) { return result; }
            if (0 != (result = compare(title(), other.title()))) { return result; }
            return 0;
        }

        private int compare(String a, String b) {
            return StringUtils.trimToEmpty(a).compareTo(StringUtils.trimToEmpty(b));
        }

    }

    // вспомогательный объект для построения отчета -
    // количества студентов на курсе по направлению
    private class CourseSummary
    {
        int total = 0;
        int academ = 0;
        int preg = 0;
        int child = 0;

        public void inc(CourseSummary summary)
        {
            this.total += summary.total;
            this.academ += summary.academ;
            this.preg += summary.preg;
            this.child += summary.child;
        }
    }

    private int printSettlement(WritableSheet sheet, SettlementBlock block, int row) throws Exception
    {
        // заголовок таблицы

        sheet.addCell(new Label(0, row, block.settlement.getTitleWithType(), _cellFormatFactory.getFormat("header")));
        sheet.mergeCells(0, row, 2, row++);

        WritableCellFormat header = _cellFormatFactory.getFormat("tableHeader");
        if (null == header)
        {
            header = getCellFormat(false, Bold.YES, Alignment.CENTRE, Color.NONE);
            header.setVerticalAlignment(VerticalAlignment.CENTRE);
            _cellFormatFactory.registerCellFormat("tableHeader", header);
        }
        WritableCellFormat headerFlip = _cellFormatFactory.getFormat("tableHeaderFlip");
        if (null == headerFlip)
        {
            headerFlip = getCellFormat(false, true, Bold.YES, Alignment.CENTRE, Color.NONE);
            headerFlip.setVerticalAlignment(VerticalAlignment.CENTRE);
            _cellFormatFactory.registerCellFormat("tableHeaderFlip", headerFlip);
        }
        sheet.setRowView(row + 1, 3 * 230 + 115);
        sheet.addCell(new Label(0, row, "Факультет", header));
        sheet.mergeCells(0, row, 0, row + 1);
        sheet.addCell(new Label(1, row, "Направление подготовки (специальность)", header));
        sheet.mergeCells(1, row, 2, row + 1);
        int column = 2;
        int courseBlockWidth = 2;
        if (_params.isPregActive()) courseBlockWidth++;
        if (_params.isChildActive()) courseBlockWidth++;
        for (int course = 1; course <= (printSixCourseColumn ? 6 : 5); course++)
        {
            sheet.addCell(new Label(++column, row, course + " курс", header));
            sheet.mergeCells(column, row, column + courseBlockWidth - 1, row);
            sheet.addCell(new Label(column, row + 1, "Всего", headerFlip));
            sheet.addCell(new Label(++column, row + 1, "А/о", headerFlip));
            if (_params.isPregActive()) sheet.addCell(new Label(++column, row + 1, "От.б.р.", headerFlip));
            if (_params.isChildActive()) sheet.addCell(new Label(++column, row + 1, "От.у.р.", headerFlip));
        }
        sheet.addCell(new Label(++column, row, "Итого", header));
        sheet.mergeCells(column, row, column + courseBlockWidth - 1, row);
        sheet.addCell(new Label(column, row + 1, "Всего", headerFlip));
        sheet.addCell(new Label(++column, row + 1, "А/о", headerFlip));
        if (_params.isPregActive()) sheet.addCell(new Label(++column, row + 1, "От.б.р.", headerFlip));
        if (_params.isChildActive()) sheet.addCell(new Label(++column, row + 1, "От.у.р.", headerFlip));

        List<FormativeOrgUnitBlock> orgUnits = new ArrayList<FormativeOrgUnitBlock>();
        orgUnits.addAll(block.orgUnits.values());
        Collections.sort(orgUnits, new Comparator<FormativeOrgUnitBlock>()
            {
            @Override
            public int compare(FormativeOrgUnitBlock o1, FormativeOrgUnitBlock o2)
            {
                int result = o1.formative.getOrgUnitType().getPriority() - o2.formative.getOrgUnitType().getPriority();
                return (result != 0) ? result : o1.formative.getTitle().compareTo(o2.formative.getTitle());
            }
            });

        // таблица

        row = row + 2;
        for (FormativeOrgUnitBlock orgUnit : orgUnits)
            row = printFormative(sheet, orgUnit, row);

        // итоги

        sheet.addCell(new Label(2, row, "Итого по бюджету", getCellFormat(false, Bold.YES, Alignment.LEFT, Color.LIGHT_TURQUOISE)));
        printLine(sheet, row++, Bold.YES, Color.LIGHT_TURQUOISE, block.budget_total);
        sheet.addCell(new Label(2, row, "Итого по внебюджету", getCellFormat(false, Bold.YES, Alignment.LEFT, Color.IVORY)));
        printLine(sheet, row++, Bold.YES, Color.IVORY, block.contract_total);

        return row + 2;
    }

    private int printFormative(WritableSheet sheet, FormativeOrgUnitBlock block, int row) throws Exception
    {
        sheet.addCell(new Label(0, row, block.formative.getTitle(), getCellFormat(false, true, Bold.NO, Alignment.CENTRE, Color.NONE)));
        sheet.mergeCells(0, row, 0, row + block.budget_lines.size() + (block.budget_lines.size() == 0 ? 0 : 1) + block.contract_lines.size() + (block.contract_lines.size() == 0 ? 0 : 1));

        row = printCompensationType(sheet, row, block.budget_lines, block.budget_total, "бюджет", Color.LIGHT_TURQUOISE);
        row = printCompensationType(sheet, row, block.contract_lines, block.contract_total, "внебюджет", Color.IVORY);

        sheet.addCell(new Label(1, row, "Итого по " + block.formative.getShortTitle(), getCellFormat(false, false, Bold.NO, Alignment.CENTRE, Color.ICE_BLUE)));
        sheet.mergeCells(1, row, 2, row);
        printLine(sheet, row, Bold.YES, Color.ICE_BLUE, block.total);

        return ++row;
    }

    @SuppressWarnings("unchecked")
    private int printCompensationType(WritableSheet sheet, int row, Map<Object, EduLevelLine> levels, CourseSummary[] totals, String title, Color color) throws Exception
    {
        if (levels.isEmpty())
            return row;

        sheet.addCell(new Label(1, row, title, getCellFormat(false, true, Bold.NO, Alignment.CENTRE, Color.NONE)));
        sheet.mergeCells(1, row, 1, row + levels.size());

        List<EduLevelLine> lines = new ArrayList<EduLevelLine>();
        lines.addAll(levels.values());
        Collections.sort(lines);

        for (EduLevelLine line : lines)
        {
            sheet.addCell(new Label(2, row, line.title(), getCellFormat(false, false, Bold.NO, Alignment.LEFT, Color.NONE)));
            printLine(sheet, row++, Bold.NO, Color.NONE, line.courseSummaries);
        }

        sheet.addCell(new Label(2, row, "Итого по " + title + "у", getCellFormat(false, false, Bold.NO, Alignment.LEFT, color)));
        printLine(sheet, row, Bold.YES, color, totals);
        return ++row;
    }

    private void printLine(WritableSheet sheet, int row, Bold bold, Color color, CourseSummary[] summaries) throws Exception
    {
        int column = 2;
        for (int course = 1; course <= 7; course++)
        {
            if (!printSixCourseColumn && course == 6) continue;
            createCell(sheet, ++column, row, summaries[course].total, Bold.YES, color);
            createCell(sheet, ++column, row, summaries[course].academ, bold, color);
            if (_params.isPregActive())
                createCell(sheet, ++column, row, summaries[course].preg, bold, color);
            if (_params.isChildActive())
                createCell(sheet, ++column, row, summaries[course].child, bold, color);
        }
    }

    private void createCell(WritableSheet sheet, int column, int row, int value, Bold bold, Color color) throws Exception
    {
        sheet.addCell(new jxl.write.Number(column, row, value, getCellFormat((value == 0), bold, Alignment.RIGHT, color)));
    }

    private WritableCellFormat getCellFormat(boolean zero, Bold bold, Alignment alignment, Color backgroundColor) throws Exception
    {
        return getCellFormat(zero, false, bold, alignment, backgroundColor);
    }

    /**
     * Возвращает формат ячейки по заданным параметрам
     * Если еще не существует, то создает и регистрирует
     *
     * @param zero            в ячейке ноль или нет
     * @param flip            вертикально
     * @param bold            жирный
     * @param alignment       выравнивание
     * @param backgroundColor цвет фона
     * @return формат ячейки по заданным параметрам
     * @throws Exception если что-нибудь сломалось в jxl
     */
    private WritableCellFormat getCellFormat(boolean zero, boolean flip, Bold bold, Alignment alignment, Color backgroundColor) throws Exception
    {
        String cellFormatName = getCellFormatName(zero, flip, bold, alignment, backgroundColor);
        WritableCellFormat result = _cellFormatFactory.getFormat(cellFormatName);
        if (result == null)
        {
            WritableFont font = new WritableFont(_cellFormatFactory.getDefaultCellFormat().getFont());
            if (zero)
                font.setColour(Colour.GREY_25_PERCENT);
            font.setBoldStyle((bold == Bold.YES) ? WritableFont.BOLD : WritableFont.NO_BOLD);
            _cellFormatFactory.registerFont(font);

            result = new WritableCellFormat(font);
            result.setWrap(true);
            if (alignment != null)
                result.setAlignment(alignment);
            if (flip)
            {
                result.setOrientation(Orientation.PLUS_90);
                result.setVerticalAlignment(VerticalAlignment.CENTRE);
            }
            switch (backgroundColor)
            {
                case LIGHT_TURQUOISE:
                    result.setBackground(Colour.LIGHT_TURQUOISE);
                    break;
                case ICE_BLUE:
                    result.setBackground(Colour.ICE_BLUE);
                    break;
                case IVORY:
                    result.setBackground(Colour.IVORY);
                    break;
                case CORAL:
                    result.setBackground(Colour.CORAL);
                    break;

            }
            result.setBorder(Border.ALL, BorderLineStyle.THIN);
            _cellFormatFactory.registerCellFormat(getCellFormatName(zero, flip, bold, alignment, backgroundColor), result);
        }
        return result;
    }

    /**
     * @param zero            в ячейке ноль или нет
     * @param bold            жирный
     * @param alignment       выравнивание
     * @param backgroundColor цвет фона
     * @return название формата ячейки по заданным параметрам
     */
    private String getCellFormatName(boolean zero, boolean flip, Bold bold, Alignment alignment, Color backgroundColor)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(java.lang.Boolean.toString(zero));
        sb.append(java.lang.Boolean.toString(flip));
        sb.append(bold.toString());
        sb.append(alignment.getDescription());
        sb.append(backgroundColor.toString());
        return sb.toString();
    }

    private static enum Color
    {
        NONE, ICE_BLUE, LIGHT_TURQUOISE, IVORY, CORAL
    }

    private static enum Bold
    {
        YES, NO
    }
}
