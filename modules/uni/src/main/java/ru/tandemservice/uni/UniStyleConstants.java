/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni;

/**
 *
 * @author dbulychev
 */
public final class UniStyleConstants
{
    public static final String BOLD = "font-weight:bold;";
    public static final String ITALIC = "font-style:italic;";
    public static final String GREEN = "color:green;";
    public static final String RED = "color:red;";
}
