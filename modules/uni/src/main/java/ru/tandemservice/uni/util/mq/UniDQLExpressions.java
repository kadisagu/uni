/* $Id$ */
package ru.tandemservice.uni.util.mq;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Calendar;
import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 21.04.2011
 */
public class UniDQLExpressions
{
    public static IDQLExpression greatOrEq(String alias, String property, Date date)
    {
        return greatOrEq(property(alias, property), date);
    }

    public static IDQLExpression greatOrEq(IDQLExpression property, Date date)
    {
        return ge(property, commonValue(CoreDateUtils.getDayFirstTimeMoment(date)));
    }

    public static IDQLExpression lessOrEq(String alias, String property, Date date)
    {
        return lessOrEq(property(alias, property), date);
    }

    public static IDQLExpression lessOrEq(IDQLExpression property, Date date)
    {
        return lt(property, commonValue(CoreDateUtils.getNextDayFirstTimeMoment(date, 1)));
    }

    private static IDQLExpression inYear(String alias, String property, int year, int firstMonth)
    {
        Calendar calendar = Calendar.getInstance();
        //noinspection MagicConstant
        calendar.set(year, firstMonth, 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        IDQLExpression e1 = ge(property(alias, property), commonValue(calendar.getTime()));

        calendar.add(Calendar.YEAR, 1);
        IDQLExpression e2 = lt(property(alias, property), commonValue(calendar.getTime()));

        return and(e1, e2);
    }

    public static IDQLExpression eqEduYear(String alias, String property, EducationYear value)
    {
        return inYear(alias, property, value.getIntValue(), Calendar.SEPTEMBER);
    }

    public static IDQLExpression eqYear(String alias, String property, int year)
    {
        return inYear(alias, property, year, Calendar.JANUARY);
    }


    /**
     * Составляет like выражение в ФИО и группе студента: фамилия + ' ' + имя + (отчество ? ' ' + отчество : '') + (группа ? ' ' + группа : '')
     *
     * @param identityCardAlias алиас IdentityCard
     * @param groupAlias        алиас Group
     * @param filter            строка поиска
     * @return dql выражение
     */
    public static IDQLExpression likeFioAndGroupTitle(String identityCardAlias, String groupAlias, String filter)
    {
        IDQLExpression expr = DQLFunctions.concat(property(IdentityCard.lastName().fromAlias(identityCardAlias)),
            value(" "),
            property(IdentityCard.firstName().fromAlias(identityCardAlias)),
            new DQLCaseExpressionBuilder()
                .when(isNull(property(IdentityCard.middleName().fromAlias(identityCardAlias))), value(""))
                .otherwise(DQLFunctions.concat(value(" "), property(IdentityCard.middleName().fromAlias(identityCardAlias))))
                .build(),
            new DQLCaseExpressionBuilder()
                .when(isNull(property(Group.title().fromAlias(groupAlias))), value(""))
                .otherwise(DQLFunctions.concat(value(" "), property(Group.title().fromAlias(groupAlias))))
                .build()
//            caseExpr(
//                new IDQLExpression[]{isNull(property(IdentityCard.middleName().fromAlias(identityCardAlias)))},
//                new IDQLExpression[]{value("")},
//                DQLFunctions.concat(value(" "), property(IdentityCard.middleName().fromAlias(identityCardAlias)))),
//            caseExpr(
//                new IDQLExpression[]{isNull(property(Group.title().fromAlias(groupAlias)))},
//                new IDQLExpression[]{value("")},
//                DQLFunctions.concat(value(" "), property(Group.title().fromAlias(groupAlias))))
        );

        return likeUpper(expr, value(CoreStringUtils.escapeLike(filter, true)));
    }
}
