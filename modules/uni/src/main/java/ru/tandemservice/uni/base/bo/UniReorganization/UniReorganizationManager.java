/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization;

import org.springframework.context.annotation.Bean;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniReorganization.logic.IUniReorganizationDao;
import ru.tandemservice.uni.base.bo.UniReorganization.logic.UniReorganizationDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
@Configuration
public class UniReorganizationManager extends BusinessObjectManager {

    public static UniReorganizationManager instance() {
        return instance(UniReorganizationManager.class);
    }

    @Bean
    public IUniReorganizationDao dao()
    {
        return new UniReorganizationDao();
    }

    /**
     * Специальный хэндлер для реорганизации
     * */
    public static EntityComboDataSourceHandler formativeOrgUnitReorganizationCustomizedOrgUnitHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, OrgUnit.class)
                .titleProperty(OrgUnit.fullTitle().s())
                .order(OrgUnit.archival())
                .order(OrgUnit.fullTitle())
                .filter(OrgUnit.fullTitle())
                .filter(OrgUnit.orgUnitType().title())
                .customize((alias, dql, context, filter) ->
                        dql.where(exists(new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "kr")
                                .where(eq(property("kr", OrgUnitToKindRelation.orgUnit()), property(alias)))
                                .where(eq(property("kr", OrgUnitToKindRelation.orgUnitKind().code()), value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)))
                                .buildQuery())
                ));
    }

}
