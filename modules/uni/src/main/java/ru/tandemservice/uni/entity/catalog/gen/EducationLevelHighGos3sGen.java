package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelHigh;
import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3s;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки (специальность) ВО (2013)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelHighGos3sGen extends EducationLevelHigh
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3s";
    public static final String ENTITY_NAME = "educationLevelHighGos3s";
    public static final int VERSION_HASH = 503815910;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EducationLevelHighGos3sGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelHighGos3sGen> extends EducationLevelHigh.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevelHighGos3s.class;
        }

        public T newInstance()
        {
            return (T) new EducationLevelHighGos3s();
        }
    }
    private static final Path<EducationLevelHighGos3s> _dslPath = new Path<EducationLevelHighGos3s>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevelHighGos3s");
    }
            

    public static class Path<E extends EducationLevelHighGos3s> extends EducationLevelHigh.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EducationLevelHighGos3s.class;
        }

        public String getEntityName()
        {
            return "educationLevelHighGos3s";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
