/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.report.xls.lazy;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.write.WritableSheet;

import org.apache.commons.io.IOUtils;

import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 13.04.2009
 */
public class Test
{
   /**
     * Тестируем LazyReportGenerator
     *
     * @return содержимое отчета
     */
    public byte[] generateReportContent() throws Exception
    {
        InputStream in = UniBaseUtils.class.getClassLoader().getResourceAsStream("uniurgpu/templates/Test.xls");
        Workbook templateWorkbook = Workbook.getWorkbook(new ByteArrayInputStream(IOUtils.toByteArray(in)));
        LazyReportGenerator generator = new LazyReportGenerator(templateWorkbook);
        generator.setDefaultFormat("plain");
        generator.importFormat("table");
        generator.importFormat("header1");
        generator.importFormat("header2");
        generator.importFormat("header2text");
        generator.importFormat("tableHeader");

        //EducationOrgUnit educationOrgUnit = _report.getEntranceEducationOrgUnit().getEducationOrgUnit();
        String speciality = ""; //educationOrgUnit.getEducationLevelHighSchool().getTitle();
        String compensationType = ""; //_report.getCompensationType().getShortTitle();
        String branch = ""; //getBranch(educationOrgUnit.getDevelopForm());

        int sheetNumber = 0;

        List<String> discAtt = new ArrayList<String>();
        discAtt.add("мат.");
        discAtt.add("русс. яз.");
        discAtt.add("лит");

        List<String> discExam = new ArrayList<String>();

        final int markCols = discAtt.size() + discExam.size() == 0 ? 1 : discAtt.size() + discExam.size();

        int baseCols = 11;
        int totalCols = baseCols + markCols;
        int baseRows = 8;
        int totalRows = baseRows + getValuesRowCount();

        final LazyCell[][] cells = new LazyCell[totalRows][totalCols];

        cells[1][0] = new LazyCell("Протокол заседания приемной комиссии", "header1");
        cells[2][0] = new LazyCell("Уральского государственного педагогического университета", "header1");
        cells[3][0] = new LazyCell("__от  __.__.______ г", "header1");

        cells[5][0] = new LazyCell("Специальность (направление)", "header2");
        cells[5][2] = new LazyCell(speciality, "header2text");

        cells[5][5] = new LazyCell("Отделение", "header2");
        cells[5][6] = new LazyCell(branch, "header2text");

        cells[5][6 + markCols] = new LazyCell("Финансирование", "header2");
        cells[5][7 + markCols] = new LazyCell(compensationType, "header2text");

        cells[6][0] = new LazyCell("N п/п", "tableHeader");
        cells[6][1] = new LazyCell("Фамилия имя отчество", "tableHeader");
        cells[6][2] = new LazyCell("N экзам листа", "tableHeader");
        cells[6][3] = new LazyCell("Базовое образование", "tableHeader");
        cells[6][4] = new LazyCell("Место жительства", "tableHeader");
        cells[6][5] = new LazyCell("Иностр гражданство", "tableHeader");
        cells[6][6] = new LazyCell("Вид внеконкурс приема", "tableHeader");
        cells[6][7] = new LazyCell("Оценки", "tableHeader");
        for (int i = 0; i < markCols; i++)
        {
            cells[7][7 + i] = new LazyCell(discAtt.get(i), "tableHeader");
        }
        cells[6][7 + markCols] = new LazyCell("Стаж (мес)", "tableHeader");
        cells[6][8 + markCols] = new LazyCell("Доп преимущества", "tableHeader");
        cells[6][9 + markCols] = new LazyCell("Решение приемной комиссии", "tableHeader");

        appendValuesRows(baseRows, cells);

        generator.addSheet(sheetNumber, "отчет", cells, new ILazyModifier()
        {
            @Override
            public void apply(WritableSheet sheet) throws Exception
            {
                //sheet.mergeCells(2, 1, 2, 11 + markCols);
                //sheet.mergeCells(3, 1, 3, 11 + markCols);
                //sheet.mergeCells(4, 1, 4, 11 + markCols);

                sheet.mergeCells(0, 1, 9 + markCols, 1);
                sheet.mergeCells(0, 2, 9 + markCols, 2);
                sheet.mergeCells(0, 3, 9 + markCols, 3);

                sheet.mergeCells(0, 5, 1, 5);
                sheet.mergeCells(2, 5, 4, 5);
                sheet.mergeCells(6, 5, 6 + markCols, 5);
                //sheet.mergeCells(8 + markCols, 5, 9 + markCols, 5);

                sheet.mergeCells(7, 6, 6 + markCols, 6);

                for (int i = 0; i <= 6; i++)
                    sheet.mergeCells(i, 6, i, 7);
                for (int i = 7 + markCols; i <= 9 + markCols; i++)
                    sheet.mergeCells(i, 6, i, 7);

                sheet.setColumnView(0, 24 / 8);
                sheet.setColumnView(1, 240 / 8);
                sheet.setColumnView(2, 56 / 8);
                sheet.setColumnView(3, 72 / 8);
                sheet.setColumnView(4, 136 / 8);
                sheet.setColumnView(5, 72 / 8);
                sheet.setColumnView(6, 72 / 8);
                for (int i = 0; i < markCols; i++)
                    sheet.setColumnView(7 + i, 32 / 8);
                sheet.setColumnView(7 + markCols, 56 / 8);
                sheet.setColumnView(8 + markCols, 88 / 8);
                sheet.setColumnView(9 + markCols, 128 / 8);

                sheet.getRowView(8);
                sheet.setRowView(8, 256);
            }
        });

        return generator.generateContent();
    }

    private int getValuesRowCount()
    {
        return 1;
    }

    private void appendValuesRows(int baseRows, final LazyCell[][] cells)
    {
        cells[baseRows][0] = new LazyCell("1", "table");
        cells[baseRows][1] = new LazyCell("Иванов Иван Иванович", "table");
        cells[baseRows][2] = new LazyCell("7", "table");
        cells[baseRows][3] = new LazyCell("11 классов", "table");
        cells[baseRows][4] = new LazyCell("Саратов", "table");
        cells[baseRows][5] = new LazyCell("", "table");
        cells[baseRows][6] = new LazyCell("", "table");

        cells[baseRows][7] = new LazyCell("3", "table");
        cells[baseRows][8] = new LazyCell("4", "table");
        cells[baseRows][9] = new LazyCell("3", "table");

        cells[baseRows][7 + 3] = new LazyCell("0", "table");
        cells[baseRows][8 + 3] = new LazyCell("нет", "table");
        cells[baseRows][9 + 3] = new LazyCell("нафиг", "table");
    }   
}
