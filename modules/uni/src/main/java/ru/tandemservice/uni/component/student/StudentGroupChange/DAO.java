/**
 * $Id$
 */
package ru.tandemservice.uni.component.student.StudentGroupChange;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

/**
 * @author dseleznev
 * @since 02.07.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getStudentId())
        {
            model.setStudent(getNotNull(Student.class, model.getStudentId()));

            if (model.getStudent().getGroup() != null)
            {
                model.setCourse(model.getStudent().getGroup().getCourse());
                model.setFormativeOrgUnit(model.getStudent().getGroup().getEducationOrgUnit().getFormativeOrgUnit());
                model.setTerritorialOrgUnit(model.getStudent().getGroup().getEducationOrgUnit().getTerritorialOrgUnit());
                model.setGroup(model.getStudent().getGroup());
            } else
            {
                model.setCourse(model.getStudent().getCourse());
                model.setFormativeOrgUnit(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
                model.setTerritorialOrgUnit(model.getStudent().getEducationOrgUnit().getTerritorialOrgUnit());
            }

            model.setGroupsList(new BaseSingleSelectModel()
            {
                @Override
                @SuppressWarnings("unchecked")
                public ListResult findValues(String filter)
                {
                    if (model.getCourse() == null || model.getFormativeOrgUnit() == null)
                        return ListResult.getEmpty();

                    Criteria c = getSession().createCriteria(Group.class, "grp");
                    c.createAlias(Group.L_EDUCATION_ORG_UNIT, "eo");
                    c.add(Restrictions.eq(Group.L_COURSE, model.getCourse()));
                    c.add(Restrictions.eq(Group.P_ARCHIVAL, Boolean.FALSE));
                    c.add(Restrictions.eq("eo." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                    if (null != model.getTerritorialOrgUnit())
                        c.add(Restrictions.eq("eo." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                    else
                        c.add(Restrictions.isNull("eo." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT));
                    c.addOrder(Order.asc(Group.P_TITLE));
                    return new ListResult(c.list());
                }

                @Override
                public Object getValue(Object primaryKey)
                {
                    if (model.getCourse() == null || model.getFormativeOrgUnit() == null)
                        return null;

                    Group group = get(Group.class, (Long) primaryKey);
                    if (!group.getCourse().equals(model.getCourse()) || !group.getEducationOrgUnit().getFormativeOrgUnit().equals(model.getFormativeOrgUnit()))
                        return null;

                    return group.getEducationOrgUnit().getTerritorialOrgUnit().equals(model.getTerritorialOrgUnit()) ? group : null;
                }

                @Override
                public String getLabelFor(Object value, int columnIndex)
                {
                    Group group = (Group) value;
                    return group.getTitle() + "  ( " + group.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle() + " )";
                }
            });
        }

        model.setCoursesList(DevelopGridDAO.getCourseList());
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
    }

    @Override
    public void update(Model model)
    {
        model.getStudent().setGroup(model.getGroup());
        update(model.getStudent());
    }
}
