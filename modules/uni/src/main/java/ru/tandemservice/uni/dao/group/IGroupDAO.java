/**
 * $Id$
 */
package ru.tandemservice.uni.dao.group;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 31.03.2010
 */
public interface IGroupDAO extends IUniBaseDao
{
    final String BEAN_NAME = "groupDao";
    final SpringBeanCache<IGroupDAO> instance = new SpringBeanCache<IGroupDAO>(BEAN_NAME);

    boolean isNonArchivalGroupExists(Group group, String groupTitle);

    /**
     * Проверяет, является ли студент старостой группы.
     * @param group Группа.
     * @param student Студент.
     * @return true, если студент - староста группы, false в противном случае
     */
    boolean isCaptainStudent(Group group, Student student);

    /**
     * Возвращает список всех старост группы.
     * @param group Группа.
     * @return - Список старост.
     */
    List<Student> getCaptainStudentList(Group group);

    /**
     * Удаляет всех старост из группы.
     * @param group Группа.
     */
    void deleteCaptainStudentList(Group group);

    /**
     * Удаляет одного старосту из группы.
     * @param group Группа.
     * @param student Студент.
     */
    void deleteCaptainStudent(Group group, Student student);

    /**
     * Добавляет старосту в группу.
     * @param group Группа.
     */
    void addCaptainStudent(Group group, Student student);
}