/* $Id:$ */
package ru.tandemservice.uni.base.ext.PersonEduDocument.logic;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.PersonEduDocumentDao;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 5/3/14
 */
@SuppressWarnings("deprecation")
public class UniPersonEduDocumentDao extends PersonEduDocumentDao
{
    @Override
    public void doMigrate()
    {
        if (existsEntity(PersonEduDocument.class))
            throw new ApplicationException("Миграция уже была проведена, повторный запуск запрещен.");

        final Map<MultiKey, Long> documentKindIdMap = getDocumentKindMap();
        final Map<String, Long> eduLevelIdMap = getEduLevelMap();

        final List<Long> documentIds = new DQLSelectBuilder().fromEntity(org.tandemframework.shared.person.base.entity.PersonEduInstitution.class, "i").column("i.id").createStatement(getSession()).list();
        BatchUtils.execute(documentIds, 100, elements ->
            {
                List<org.tandemframework.shared.person.base.entity.PersonEduInstitution> eduInstitutionList = getList(org.tandemframework.shared.person.base.entity.PersonEduInstitution.class, "id", elements);

                for (org.tandemframework.shared.person.base.entity.PersonEduInstitution eduInstitution : eduInstitutionList) {
                    PersonEduDocument eduDocument = doCreateEduDocument(eduInstitution, documentKindIdMap, eduLevelIdMap);

                    if (eduInstitution.isMain()) {
                        for (Student student : getList(Student.class, Student.person(), eduInstitution.getPerson())) {
                            student.setEduDocument(eduDocument);
							student.getPerson().setMainEduDocument(eduDocument);
                            update(student);
                        }
                    }
                }

                getSession().flush();
                getSession().clear();
            });
    }

    @Override
    public void deleteDocument(Long documentId)
    {
        List<Student> students = getList(Student.class, Student.eduDocument().id(), documentId);
        if (!students.isEmpty()) {
            throw new ApplicationException("Невозможно удалить документ об образовании, так как он выбран в качестве основного для студента (" + students.get(0).getFullTitle() + ")");
        }
        delete(documentId);
    }
}
