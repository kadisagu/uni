/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentDocumentList;

import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.employee.StudentDocument;

/**
 * @author euroelessar
 * @since 23.03.2010
 */
@State(keys = {"orgUnitId", "archival"}, bindings = {"orgUnitId", "archival"})
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private DynamicListDataSource<StudentDocument> _dataSource;
    private IDataSettings _settings;
    private IPrincipalContext _principalContext;
    private boolean _archival;

    //filter
    private ISelectModel _courseListModel;
    private ISelectModel _compensationTypeListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _producingOrgUnitListModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developPeriodListModel;
    private ISelectModel _developConditionListModel;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public DynamicListDataSource<StudentDocument> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentDocument> dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public boolean isArchival()
    {
        return _archival;
    }

    public void setArchival(boolean archival)
    {
        _archival = archival;
    }

    public ISelectModel getCourseListModel()
    {
        return _courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel)
    {
        _courseListModel = courseListModel;
    }

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getProducingOrgUnitListModel()
    {
        return _producingOrgUnitListModel;
    }

    public void setProducingOrgUnitListModel(ISelectModel producingOrgUnitListModel)
    {
        _producingOrgUnitListModel = producingOrgUnitListModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }
}
