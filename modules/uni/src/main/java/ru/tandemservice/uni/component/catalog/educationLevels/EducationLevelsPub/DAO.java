/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.Collections;
import java.util.List;

/**
 * @author vip_delete
 */
public abstract class DAO<T extends EducationLevels, M extends Model<T>> extends DefaultCatalogPubDAO<T, M>
{
    @Override
    public void prepareListItemDataSource(M model)
    {
        MQBuilder builder = new MQBuilder(model.getItemClass().getName(), "ci");

        applyFilters(model, builder);

        DynamicListDataSource<T> dataSource = model.getDataSource();
        List<T> list;
        if (EducationLevels.P_FULL_TITLE.equals(dataSource.getEntityOrder().getKey()))
        {
            list = builder.getResultList(getSession());
            Collections.sort(list, new EntityComparator<EducationLevels>(dataSource.getEntityOrder()));
        }
        else
        {
            getOrderDescriptionRegistry().applyOrder(builder, model.getDataSource().getEntityOrder());
            list = builder.getResultList(getSession());
        }

        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    protected void applyFilters(M model, MQBuilder builder)
    {
        super.applyFilters(model, builder);

        StructureEducationLevels levelType = (StructureEducationLevels) model.getSettings().get("levelType");
        String okso = (String) model.getSettings().get("okso");
        ICatalogItem qualification = (ICatalogItem) model.getSettings().get("qualification");

        if (levelType != null)
            builder.add(MQExpression.eq("ci", EducationLevels.L_LEVEL_TYPE, levelType));

        if (StringUtils.isNotEmpty(okso))
            builder.add(MQExpression.eq("ci", EducationLevels.P_INHERITED_OKSO, okso));

        if (qualification != null)
            builder.add(MQExpression.eq("ci", EducationLevels.L_QUALIFICATION, qualification));
    }
}
