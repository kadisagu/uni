/* $Id$ */
package ru.tandemservice.uni.component.catalog.studentCustomStateCI.StudentCustomStateCIItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;

/**
 * @author nvankov
 * @since 3/27/13
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<StudentCustomStateCI, Model>
{
}
