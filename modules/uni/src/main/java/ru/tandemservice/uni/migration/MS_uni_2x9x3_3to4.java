/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Nikolay Fedorovskih
 * @since 14.03.2016
 */
@SuppressWarnings("unused")
public class MS_uni_2x9x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        // Коды элементов "Аспирантура" и "Стажировка" переименованы
        tool.executeUpdate("update qualifications_t set code_p=? where code_p=?", "Асп", "А"); // Аспирантура
        tool.executeUpdate("update qualifications_t set code_p=? where code_p=?", "Ст", "СТ"); // Стажировка
        tool.executeUpdate("update qualifications_t set shorttitle_p=? where shorttitle_p=?", null, ""); // string -> trimmedstring

        // Добавлены новые элементы
        final Short qualificationsCode = tool.entityCodes().get("qualifications");
        final MigrationUtils.BatchInsertBuilder ins = new MigrationUtils.BatchInsertBuilder(qualificationsCode, "code_p", "shorttitle_p", "order_p", "title_p");
        ins.addRow("Инт", null, 14, "Интернатура");
        ins.addRow("Орд", null, 15, "Ординатура");
        ins.addRow("Адн", null, 16, "Адъюнктура");
        ins.executeInsert(tool, "qualifications_t");
    }
}