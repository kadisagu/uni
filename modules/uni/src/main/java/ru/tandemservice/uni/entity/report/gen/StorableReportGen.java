package ru.tandemservice.uni.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сохраняемый отчет
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StorableReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.report.StorableReport";
    public static final String ENTITY_NAME = "storableReport";
    public static final int VERSION_HASH = -475066536;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_CONTENT = "content";

    private Date _formingDate;     // Дата формирования
    private DatabaseFile _content;     // Печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования.
     */
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Печатная форма.
     */
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StorableReportGen)
        {
            setFormingDate(((StorableReport)another).getFormingDate());
            setContent(((StorableReport)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StorableReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StorableReport.class;
        }

        public T newInstance()
        {
            return (T) new StorableReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formingDate":
                    return obj.getFormingDate();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formingDate":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formingDate":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formingDate":
                    return Date.class;
                case "content":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StorableReport> _dslPath = new Path<StorableReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StorableReport");
    }
            

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.uni.entity.report.StorableReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Печатная форма.
     * @see ru.tandemservice.uni.entity.report.StorableReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    public static class Path<E extends StorableReport> extends EntityPath<E>
    {
        private PropertyPath<Date> _formingDate;
        private DatabaseFile.Path<DatabaseFile> _content;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.uni.entity.report.StorableReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(StorableReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Печатная форма.
     * @see ru.tandemservice.uni.entity.report.StorableReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

        public Class getEntityClass()
        {
            return StorableReport.class;
        }

        public String getEntityName()
        {
            return "storableReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
