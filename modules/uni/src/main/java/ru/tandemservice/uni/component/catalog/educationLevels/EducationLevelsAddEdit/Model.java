/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;

/**
 * @author vip_delete
 */
public abstract class Model<T extends EducationLevels> extends DefaultCatalogAddEditModel<T>
{
    private List<HSelectOption> _levelTypeList;
    private ISelectModel _educationLevelListModel;
    private List<Qualifications> _qualificationList;

    public List<HSelectOption> getLevelTypeList()
    {
        return _levelTypeList;
    }

    public void setLevelTypeList(List<HSelectOption> levelTypeList)
    {
        _levelTypeList = levelTypeList;
    }

    public ISelectModel getEducationLevelListModel()
    {
        return _educationLevelListModel;
    }

    public void setEducationLevelListModel(ISelectModel educationLevelListModel)
    {
        _educationLevelListModel = educationLevelListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }
}
