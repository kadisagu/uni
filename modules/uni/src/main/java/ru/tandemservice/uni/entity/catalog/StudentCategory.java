package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.StudentCategoryGen;

/**
 * Категории обучаемых студентов
 */
public class StudentCategory extends StudentCategoryGen
{
    public static final String CATALOG_STUDENT_CATEGORIES = "StudentCategories";
    public static final String STUDENT_CATEGORY_STUDENT = "1";
    public static final String STUDENT_CATEGORY_LISTENER = "2";
    public static final String STUDENT_CATEGORY_SECOND_HIGH = "3";
    public static final String STUDENT_CATEGORY_DPP_LISTENER = "4";

    public boolean isStudent()
    {
        return STUDENT_CATEGORY_STUDENT.equals(getCode());
    }

    public boolean isListener()
    {
        return STUDENT_CATEGORY_LISTENER.equals(getCode());
    }

    public boolean isSecondHigh()
    {
        return STUDENT_CATEGORY_SECOND_HIGH.equals(getCode());
    }

    public boolean isDppListener()
    {
        return STUDENT_CATEGORY_DPP_LISTENER.equals(getCode());
    }
}