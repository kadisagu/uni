/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.customState;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.util.CustomStateUtil;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 3/25/13
 */
@Transactional
public class StudentCustomStateDAO extends SharedBaseDao implements IStudentCustomStateDAO
{
    @Override
    public void saveOrUpdateStudentCustomState(StudentCustomState studentCustomState)
    {
        if (hasMatchStudentCustomState(studentCustomState))
            throw new ApplicationException("Данный дополнительный статус «" + studentCustomState.getCustomState().getTitle() + "» пересекается по срокам действия с уже имеющимся аналогичным статусом студента.");

        save(studentCustomState);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean hasMatchStudentCustomState(StudentCustomState studentCustomState)
    {
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .where(eq(property("st", StudentCustomState.student()), value(studentCustomState.getStudent())))
                .where(eq(property("st", StudentCustomState.customState()), value(studentCustomState.getCustomState())));

        CustomStateUtil.applyInPeriodFilter(builder, "st", studentCustomState.getBeginDate(), studentCustomState.getEndDate());

        if (null != studentCustomState.getId())
            builder.where(ne(property("st"), value(studentCustomState)));

        return existsEntity(builder.buildQuery());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<StudentCustomState> getActiveStatesList(Long studentId)
    {
        DQLSelectBuilder dql =  new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .where(eq(property("st", StudentCustomState.student().id()), value(studentId)))
                .order(property("st", StudentCustomState.customState().title()));

        CustomStateUtil.applyActivePeriodFilter(dql, "st");

        return dql.createStatement(getSession()).list();
    }
}
