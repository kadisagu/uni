/**
 *$Id$
 */
package ru.tandemservice.uni.base.ext.ReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Zhebko
 * @since 26.03.2014
 */
@Configuration
public class ReportPersonExtManager extends BusinessObjectExtensionManager
{
}