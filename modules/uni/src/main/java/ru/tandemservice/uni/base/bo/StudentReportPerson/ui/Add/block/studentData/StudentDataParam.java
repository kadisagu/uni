/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentData;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 03.02.2011
 */
public class StudentDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _student = new ReportParam<>();
    private IReportParam<List<Course>> _course = new ReportParam<>();
    private IReportParam<List<StudentStatus>> _studentStatus = new ReportParam<>();
    private IReportParam<DataWrapper> _studentArchival = new ReportParam<>();
    private IReportParam<CompensationType> _compensationType = new ReportParam<>();
    private IReportParam<List<Qualifications>> _qualification = new ReportParam<>();
    private IReportParam<List<OrgUnit>> _formativeOrgUnit = new ReportParam<>();
    private IReportParam<List<OrgUnit>> _territorialOrgUnit = new ReportParam<>();
    private IReportParam<List<EducationLevelsHighSchool>> _eduLevel = new ReportParam<>();
    private IReportParam<List<DevelopForm>> _developForm = new ReportParam<>();
    private IReportParam<List<DevelopCondition>> _developCondition = new ReportParam<>();
    private IReportParam<List<DevelopTech>> _developTech = new ReportParam<>();
    private IReportParam<List<DevelopPeriod>> _developPeriod = new ReportParam<>();
    private IReportParam<List<DevelopPeriod>> _developPeriodAuto = new ReportParam<>();
    private IReportParam<List<Group>> _group = new ReportParam<>();
    private IReportParam<List<DataWrapper>> _entranceYear = new ReportParam<>();
    private IReportParam<Long> _finishYearFrom = new ReportParam<>();
    private IReportParam<Long> _finishYearTo = new ReportParam<>();
    private IReportParam<List<OrgUnit>> _producingOrgUnit = new ReportParam<>();
    private IReportParam<String> _personalNumber = new ReportParam<>();
    private IReportParam<String> _bookNumber = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем студента
        String studentAlias = dql.innerJoinEntity(Student.class, Student.person());

        // добавляем сортировку
        dql.order(studentAlias, Student.id());

        return studentAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        Long orgUnitId = printInfo.getSharedObject(ReportPersonAddUI.ORG_UNIT_ID);

        if (orgUnitId != null)
        {
            OrgUnit orgUnit = DataAccessServices.dao().getNotNull(orgUnitId);
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.orgUnit", orgUnit.getFullTitle());

            List<String> kindCodeList = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "e")
                    .column(property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("e")))
                    .where(eq(property(OrgUnitToKindRelation.orgUnit().fromAlias("e")), value(orgUnit)))
                    .where(eq(property(OrgUnitToKindRelation.orgUnitKind().allowStudents().fromAlias("e")), value(Boolean.TRUE))));

            List<IDQLExpression> expressions = new ArrayList<>();

            // для выпускающих подразделений – студенты, для которых текущее подразделение является выпускающим через связь с НПв.
            if (kindCodeList.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                expressions.add(eq(property(Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fromAlias(alias(dql))), value(orgUnit)));

            // для формирующих подразделений будет отображаться перечень студентов, для которых текущее подразделение является формирующим через связь с направлением подготовки (специальностью) подразделения.
            if (kindCodeList.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                expressions.add(eq(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias(alias(dql))), value(orgUnit)));

            // для территориальных подразделений – студенты, для которых текущее подразделение является территориальным подразделением через связь с направлением подготовки (специальностью) подразделения.
            if (kindCodeList.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                expressions.add(eq(property(Student.educationOrgUnit().territorialOrgUnit().fromAlias(alias(dql))), value(orgUnit)));

            if (expressions.isEmpty())
                dql.builder.where(DQLExpressions.nothing());
            else
                dql.builder.where(or(expressions.toArray(new IDQLExpression[expressions.size()])));
        }

        if (_student.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.student", _student.getData().getTitle());

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_student.getData()))
            {
                alias(dql);
            } else
            {
                String studentAlias = dql.nextAlias();

                dql.builder.where(DQLExpressions.notIn(property(dql.getFromEntityAlias()),
                        new DQLSelectBuilder().fromEntity(Student.class, studentAlias).column(property(Student.person().fromAlias(studentAlias))).buildQuery()
                ));
            }
        }

        if (_course.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.course", CommonBaseStringUtil.join(_course.getData(), Course.P_TITLE, ", "));

            dql.builder.where(in(property(Student.course().fromAlias(alias(dql))), _course.getData()));
        }

        if (_studentStatus.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.studentStatus", CommonBaseStringUtil.join(_studentStatus.getData(), StudentStatus.P_TITLE, ", "));

            dql.builder.where(in(property(Student.status().fromAlias(alias(dql))), _studentStatus.getData()));
        }

        if (_studentArchival.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.studentArchival", _studentArchival.getData().getTitle());

            dql.builder.where(eq(property(Student.archival().fromAlias(alias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_studentArchival.getData()))));
        }

        if (_compensationType.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.compensationType", _compensationType.getData().getTitle());

            dql.builder.where(eq(property(Student.compensationType().fromAlias(alias(dql))), value(_compensationType.getData())));
        }

        if (_qualification.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.qualification", CommonBaseStringUtil.join(_qualification.getData(), Qualifications.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias(alias(dql))), _qualification.getData()));
        }

        if (_formativeOrgUnit.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.formativeOrgUnit", CommonBaseStringUtil.join(_formativeOrgUnit.getData(), OrgUnit.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias(alias(dql))), _formativeOrgUnit.getData()));
        }

        if (_territorialOrgUnit.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.territorialOrgUnit", CommonBaseStringUtil.join(_territorialOrgUnit.getData(), OrgUnit.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().territorialOrgUnit().fromAlias(alias(dql))), _territorialOrgUnit.getData()));
        }

        if (_eduLevel.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.eduLevel", CommonBaseStringUtil.join(_eduLevel.getData(), EducationLevelsHighSchool.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().educationLevelHighSchool().fromAlias(alias(dql))), _eduLevel.getData()));
        }

        if (_developForm.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.developForm", CommonBaseStringUtil.join(_developForm.getData(), DevelopForm.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().developForm().fromAlias(alias(dql))), _developForm.getData()));
        }

        if (_developCondition.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.developCondition", CommonBaseStringUtil.join(_developCondition.getData(), DevelopCondition.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().developCondition().fromAlias(alias(dql))), _developCondition.getData()));
        }

        if (_developTech.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.developTech", CommonBaseStringUtil.join(_developTech.getData(), DevelopTech.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().developTech().fromAlias(alias(dql))), _developTech.getData()));
        }

        if (_developPeriod.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.developPeriod", CommonBaseStringUtil.join(_developPeriod.getData(), DevelopPeriod.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().developPeriod().fromAlias(alias(dql))), _developPeriod.getData()));
        }

        if (_developPeriodAuto.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.developPeriodAuto", CommonBaseStringUtil.join(_developPeriodAuto.getData(), DevelopPeriod.P_TITLE, ", "));

            dql.builder.where(in(property(Student.developPeriodAuto().fromAlias(alias(dql))), _developPeriodAuto.getData()));
        }

        if (_group.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.group", CommonBaseStringUtil.join(_group.getData(), Group.P_TITLE, ", "));

            if (_group.getData().isEmpty())
                dql.builder.where(DQLExpressions.isNull(property(Student.group().fromAlias(alias(dql)))));
            else
                dql.builder.where(in(property(Student.group().fromAlias(alias(dql))), _group.getData()));
        }

        if (_entranceYear.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.entranceYear", CommonBaseStringUtil.join(_entranceYear.getData(), "id", ", "));

            dql.builder.where(in(property(Student.entranceYear().fromAlias(alias(dql))), CommonBaseEntityUtil.getIdList(_entranceYear.getData())));
        }

        if (_finishYearFrom.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.finishYearFrom", _finishYearFrom.getData().toString());

            dql.builder.where(DQLExpressions.ge(property(Student.finishYear().fromAlias(alias(dql))), value(_finishYearFrom.getData().intValue())));
        }

        if (_finishYearTo.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.finishYearTo", _finishYearTo.getData().toString());

            dql.builder.where(DQLExpressions.le(property(Student.finishYear().fromAlias(alias(dql))), value(_finishYearTo.getData().intValue())));
        }

        if (_producingOrgUnit.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.producingOrgUnit", CommonBaseStringUtil.join(_producingOrgUnit.getData(), OrgUnit.P_TITLE, ", "));

            dql.builder.where(in(property(Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fromAlias(alias(dql))), _producingOrgUnit.getData()));
        }

        if (_personalNumber.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.personalNumber", _personalNumber.getData());

            dql.builder.where(eq(property(Student.personalNumber().fromAlias(alias(dql))), value(_personalNumber.getData())));
        }

        if (_bookNumber.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentData.bookNumber", _bookNumber.getData());

            if (StringUtils.isEmpty(_bookNumber.getData()))
                dql.builder.where(DQLExpressions.isNull(property(Student.bookNumber().fromAlias(alias(dql)))));
            else
                dql.builder.where(eq(property(Student.bookNumber().fromAlias(alias(dql))), value(_bookNumber.getData())));
        }
    }

    // Getters


    public IReportParam<DataWrapper> getStudent()
    {
        return _student;
    }

    public IReportParam<List<Course>> getCourse()
    {
        return _course;
    }

    public IReportParam<List<StudentStatus>> getStudentStatus()
    {
        return _studentStatus;
    }

    public IReportParam<DataWrapper> getStudentArchival()
    {
        return _studentArchival;
    }

    public IReportParam<CompensationType> getCompensationType()
    {
        return _compensationType;
    }

    public IReportParam<List<Qualifications>> getQualification()
    {
        return _qualification;
    }

    public IReportParam<List<OrgUnit>> getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public IReportParam<List<OrgUnit>> getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public IReportParam<List<EducationLevelsHighSchool>> getEduLevel()
    {
        return _eduLevel;
    }

    public IReportParam<List<DevelopForm>> getDevelopForm()
    {
        return _developForm;
    }

    public IReportParam<List<DevelopCondition>> getDevelopCondition()
    {
        return _developCondition;
    }

    public IReportParam<List<DevelopTech>> getDevelopTech()
    {
        return _developTech;
    }

    public IReportParam<List<DevelopPeriod>> getDevelopPeriod()
    {
        return _developPeriod;
    }

    public IReportParam<List<DevelopPeriod>> getDevelopPeriodAuto()
    {
        return _developPeriodAuto;
    }

    public IReportParam<List<Group>> getGroup()
    {
        return _group;
    }

    public IReportParam<List<DataWrapper>> getEntranceYear()
    {
        return _entranceYear;
    }

    public IReportParam<Long> getFinishYearFrom()
    {
        return _finishYearFrom;
    }

    public IReportParam<Long> getFinishYearTo()
    {
        return _finishYearTo;
    }

    public IReportParam<List<OrgUnit>> getProducingOrgUnit()
    {
        return _producingOrgUnit;
    }

    public IReportParam<String> getPersonalNumber()
    {
        return _personalNumber;
    }

    public IReportParam<String> getBookNumber()
    {
        return _bookNumber;
    }
}
