/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni;

import java.util.Date;

import org.tandemframework.core.entity.IEntity;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;

/**
 * @author vip_delete
 * @since 03.07.2009
 */
public interface IStorableReport extends IEntity
{
    Date getFormingDate();

    DatabaseFile getContent();
}
