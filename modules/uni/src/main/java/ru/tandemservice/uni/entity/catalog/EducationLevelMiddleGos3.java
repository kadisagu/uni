package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelMiddleGos3Gen;

/**
 * Направление подготовки (специальность) СПО (ФГОС)
 */
public class EducationLevelMiddleGos3 extends EducationLevelMiddleGos3Gen
{
    public EducationLevelMiddleGos3()
    {
        setCatalogCode("educationLevelMiddleGos3");
    }
}