package ru.tandemservice.uni.ws.phones;

/**
 * @author vdanilov
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import java.sql.SQLException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@WebService(serviceName="PhoneBookService")
public class PhoneBookService {

    @XmlType(name="post")
    public static class PhoneBookEmployeePost {
        @XmlAttribute public Long id;
        @XmlAttribute public String title;
        @XmlAttribute public String phoneInternal;
        @XmlAttribute public String phoneExternal;
        @XmlAttribute public String phoneMobile;
    }

    @XmlType(name="orgunit")
    public static class PhoneBookOrgUnit {
        @XmlAttribute public Long id;
        @XmlAttribute public String title;
        @XmlAttribute public String phoneInternal;
        @XmlAttribute public String phoneExternal;
        @XmlAttribute public String fax;
        @XmlAttribute public String email;
        @XmlAttribute public String webpage;
        @XmlAttribute public String addressPostal;
        @XmlAttribute public String addressLegal;
        @XmlAttribute public String addressReal;

        @XmlElements(@XmlElement(name="orgunit")) public PhoneBookOrgUnit[] children;
        @XmlElements(@XmlElement(name="post")) public PhoneBookEmployeePost[] posts;
    }


    @XmlType(name="PhoneBook")
    public static class PhoneBook {
        @XmlAttribute public Long id;
        @XmlAttribute public String title;
        @XmlAttribute public String date;

        @XmlElements(@XmlElement(name="orgunit")) public PhoneBookOrgUnit[] children;
    }

    @WebMethod
    public PhoneBook getPhoneBook(
            @WebParam(name="ids") final Long[] orgUnitIds,
            @WebParam(name="includeChild") final Boolean includeChild,
            @WebParam(name="includePost") final Boolean includePost
    ) {
        Debug.begin("PhoneBookService.getPhoneBook");
        try {

            return IUniBaseDao.instance.get().getCalculatedValue(new HibernateCallback<PhoneBook>() {
                @Override public PhoneBook doInHibernate(final Session session) throws HibernateException, SQLException {
                    final List<OrgUnit> list = new DQLSelectBuilder().fromEntity(OrgUnit.class, "o", true).column("o")
                            .where(eq(property("o", OrgUnit.P_ARCHIVAL), value(Boolean.FALSE)))
                            .order(property("o", OrgUnit.P_TITLE))
                            .createStatement(session).list();
                    final Map<Long, PhoneBookOrgUnit> resultMap = this.getPhoneBookMap(list);

                    final List<PhoneBookOrgUnit> result = new ArrayList<>();
                    for (final OrgUnit ou: list) {
                        if (null == ou.getParent()) { result.add(resultMap.get(ou.getId())); }
                    }

                    final TopOrgUnit academy = TopOrgUnit.getInstance();
                    final PhoneBook book = new PhoneBook();
                    book.id = academy.getId();
                    book.title = academy.getFullTitle();
                    book.date = DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date());
                    book.children = result.toArray(new PhoneBookOrgUnit[result.size()]);
                    return book;
                }


                @SuppressWarnings("unchecked")
                private Map<Long, PhoneBookOrgUnit> getPhoneBookMap(final List<OrgUnit> list) {
                    final Map<Long, Collection<Long>> orgUnitChildMap = this.getOrgUnitChildMap(list);
                    final Map<Long, PhoneBookOrgUnit> resultMap = new LinkedHashMap<>(orgUnitChildMap.size());
                    for (final OrgUnit ou: list) {
                        resultMap.put(ou.getId(), this.getPhoneBookOrgUnit(ou));
                    }
                    for (final Map.Entry<Long, Collection<Long>> e: orgUnitChildMap.entrySet()) {
                        resultMap.get(e.getKey()).children = ((Collection<PhoneBookOrgUnit>) CollectionUtils.collect(e.getValue(), resultMap::get))
                                .toArray(new PhoneBookOrgUnit[0]);
                    }
                    return resultMap;
                }

                private Map<Long, Collection<Long>> getOrgUnitChildMap(final List<OrgUnit> list) {
                    final Map<Long, Collection<Long>> orgUnitChildMap = new HashMap<>(list.size());
                    for (final OrgUnit ou: list) {
                        if (null != ou.getParent()) {
                            SafeMap.safeGet(orgUnitChildMap, ou.getParent().getId(), ArrayList.class).add(ou.getId());
                        }
                    }
                    return orgUnitChildMap;
                }


                protected PhoneBookOrgUnit getPhoneBookOrgUnit(final OrgUnit ou) {
                    final PhoneBookOrgUnit orgunit = new PhoneBookOrgUnit();
                    orgunit.id = ou.getId();
                    orgunit.phoneInternal = StringUtils.trimToNull(ou.getInternalPhone());
                    orgunit.phoneExternal = StringUtils.trimToNull(ou.getPhone());
                    orgunit.webpage = StringUtils.trimToNull(ou.getWebpage());
                    orgunit.email = StringUtils.trimToNull(ou.getEmail());
                    orgunit.fax = StringUtils.trimToNull(ou.getFax());
                    orgunit.addressPostal = (null == ou.getPostalAddress() ? null : ou.getPostalAddress().getTitleWithFlat());
                    orgunit.addressLegal = (null == ou.getLegalAddress() ? null : ou.getLegalAddress().getTitleWithFlat());
                    orgunit.addressReal = (null == ou.getAddress() ? null : ou.getAddress().getTitleWithFlat());
                    return orgunit;
                }
            });

        } finally {
            Logger.getLogger(this.getClass()).info(Debug.end().toString());


        }
    }

}
