package ru.tandemservice.uni.dao.pps;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author vdanilov
 */
public interface IPpsEntrySynchronizer {

    public static final String LOCKER_NAME = "ppsEntrySynchronizer.lock";

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSynchronize();
}
