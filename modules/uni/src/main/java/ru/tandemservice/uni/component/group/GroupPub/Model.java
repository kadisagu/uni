/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.GroupPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.base.bo.UniStudent.daemon.UniStudentDevelopPeriodDaemonBean;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "groupId"),
        @Bind(key = "selectedTabId", binding = "selectedTabId")
})
@Output(keys = { "groupId", "groupEditId", "archival", "studentId" }, bindings = { "groupId", "groupId", "group.archival", "studentId" })
public class Model implements IStudentListModel
{
    public static final String P_STUDENT_ACTIVE_CUSTOME_STATES = "studentActiveCustomStates";

    private DynamicListDataSource<Student> _studentDataSource;
    private Group _group;
    private Long _groupId;
    private Long _studentId;
    private String _selectedTabId;
    private IDataSettings _settings;
    private IMultiSelectModel _studentCustomStateCIModel;
    private List<Student> _captainStudentList;
    private String _speciality;
    private String _specialization;

    // secure
    private String _viewStudentListKey;
    private String _delKey;
    private String _editKey;
    private String _archiveKey;
    private String _unArchiveKey;
    private String _editArchivalKey;

    private boolean startEduYearEditDisabled;

    @Override
    public List<IdentifiableWrapper> getStudentStatusOptionList()
    {
        return STUDENT_STATUS_OPTION_LIST;
    }

    public DynamicListDataSource<Student> getStudentDataSource()
    {
        return _studentDataSource;
    }

    public void setStudentDataSource(DynamicListDataSource<Student> studentDataSource)
    {
        _studentDataSource = studentDataSource;
    }

    public String getSpeciality()
    {
        return _speciality;
    }

    public void setSpeciality(String speciality)
    {
        _speciality = speciality;
    }

    public String getSpecialization()
    {
        return _specialization;
    }

    public void setSpecialization(String specialization)
    {
        _specialization = specialization;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public IMultiSelectModel getStudentCustomStateCIModel()
    {
        return _studentCustomStateCIModel;
    }

    public void setStudentCustomStateCIModel(IMultiSelectModel studentCustomStateCIModel)
    {
        _studentCustomStateCIModel = studentCustomStateCIModel;
    }

    public List<Student> getCaptainStudentList()
    {
        return _captainStudentList;
    }

    public void setCaptainStudentList(List<Student> captainStudentList)
    {
        _captainStudentList = captainStudentList;
    }

    public String getCaptainStudentListStr()
    {
        StringBuilder builder = new StringBuilder();
        for (Student student : getCaptainStudentList())
        {
            builder.append("<div>");
            // DEV-4515
            builder.append(student == null ? "" : (student.getPerson().getFullFio()) + (student.getGroup() == null ? "" : (student.getGroup().getId().equals(getGroupId()) ? "" : ", Группа: " + student.getGroup().getTitle())));
            builder.append("</div>");
        }
        return builder.toString();
    }

    public String getDaemonDevelopPeriodStatus()
    {
        final Long date = UniStudentDevelopPeriodDaemonBean.DAEMON.getCompleteStatus();
        String status = date == null ? "обновляется" : DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        return "Планируемые сроки обновлены: " + "<b>" + status + "</b>";
    }

    public String getDaemonDevelopPeriodAlert()
    {
        return "Обновить планируемые сроки освоения?";
    }

    public String getViewStudentListKey()
    {
        return _viewStudentListKey;
    }

    public void setViewStudentListKey(String viewStudentListKey)
    {
        _viewStudentListKey = viewStudentListKey;
    }

    public String getDelKey()
    {
        return _delKey;
    }

    public void setDelKey(String delKey)
    {
        _delKey = delKey;
    }

    public String getEditKey()
    {
        return _editKey;
    }

    public void setEditKey(String editKey)
    {
        _editKey = editKey;
    }

    public String getArchiveKey()
    {
        return _archiveKey;
    }

    public void setArchiveKey(String archiveKey)
    {
        _archiveKey = archiveKey;
    }

    public String getUnArchiveKey()
    {
        return _unArchiveKey;
    }

    public void setUnArchiveKey(String unArchiveKey)
    {
        _unArchiveKey = unArchiveKey;
    }

    public String getEditArchivalKey()
    {
        return _editArchivalKey;
    }

    public void setEditArchivalKey(String editArchivalKey)
    {
        _editArchivalKey = editArchivalKey;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        _selectedTabId = selectedTabId;
    }

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public boolean isStartEduYearEditDisabled()
    {
        return startEduYearEditDisabled;
    }

    public void setStartEduYearEditDisabled(boolean startEduYearEditDisabled)
    {
        this.startEduYearEditDisabled = startEduYearEditDisabled;
    }

    public boolean isGroupNotArchival()
    {
        return !_group.isArchival();
    }


}