/* $Id$ */
package ru.tandemservice.uni.util;

import com.google.common.collect.Maps;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 31.10.2013
 */
public class CustomStateUtil
{
    public static boolean applyActivePeriodFilter(DQLSelectBuilder dql, String alias)
    {
        return FilterUtils.applyInPeriodFilterNullSafe(dql, alias, StudentCustomState.P_BEGIN_DATE, StudentCustomState.P_END_DATE, new Date());
    }

    public static boolean applyInPeriodFilter(DQLSelectBuilder dql, String alias, Date dateFrom, Date dateTo)
    {
        return FilterUtils.applyIntersectPeriodFilterNullSafe(dql, alias, StudentCustomState.P_BEGIN_DATE, StudentCustomState.P_END_DATE, dateFrom, dateTo);
    }

    public static Map<Long, Set<StudentCustomState>> getActiveCustomStatesMap(Collection<Long> studentIds)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .where(in(property("st", StudentCustomState.student().id()), studentIds));

        applyActivePeriodFilter(dql, "st");

        List<StudentCustomState> studentActiveCustomStateList = DataAccessServices.dao().getList(dql);

        Map<Long, Set<StudentCustomState>> studentActiveCustomStateMap = Maps.newHashMapWithExpectedSize(studentIds.size());
        for (StudentCustomState studentCustomState : studentActiveCustomStateList)
        {
            Set<StudentCustomState> states = studentActiveCustomStateMap.get(studentCustomState.getStudent().getId());
            if (states == null)
                studentActiveCustomStateMap.put(studentCustomState.getStudent().getId(), states = new HashSet<>());
            states.add(studentCustomState);
        }
        return studentActiveCustomStateMap;
    }

    public static void addCustomStatesFilter(MQBuilder builder, String studentAlias, Collection<StudentCustomStateCI> studentCustomStateCIList)
    {
        if (null != studentCustomStateCIList && !studentCustomStateCIList.isEmpty())
        {
            final MQBuilder studentCustomStateBuilder = new MQBuilder(StudentCustomState.ENTITY_CLASS, "st", new String[]{StudentCustomState.student().id().s()});
            studentCustomStateBuilder.add(MQExpression.in("st", StudentCustomState.customState().id(), UniBaseDao.ids(studentCustomStateCIList)));
            FilterUtils.applyInPeriodFilterNullSafe(studentCustomStateBuilder, "st", StudentCustomState.P_BEGIN_DATE, StudentCustomState.P_END_DATE, new Date());
            builder.add(MQExpression.in(studentAlias, Student.id(), studentCustomStateBuilder));
        }
    }

    public static void addCustomStatesFilter(DQLSelectBuilder builder, String studentAlias, Collection<StudentCustomStateCI> studentCustomStateCIList)
    {
        if (null != studentCustomStateCIList && !studentCustomStateCIList.isEmpty())
        {
            DQLSelectBuilder csDQL = new DQLSelectBuilder();
            csDQL.fromEntity(StudentCustomState.class, "st");
            csDQL.column(value(1));
            csDQL.where(eq(property("st", StudentCustomState.student()), property(studentAlias)));
            csDQL.where(in(property("st", StudentCustomState.customState()), studentCustomStateCIList));
            FilterUtils.applyInPeriodFilterNullSafe(csDQL, "st", StudentCustomState.P_BEGIN_DATE, StudentCustomState.P_END_DATE, new Date());
            builder.where(exists(csDQL.buildQuery()));
        }
    }

}