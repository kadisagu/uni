package ru.tandemservice.uni.dao.person;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;

/**
 * @author vdanilov
 */
public class PersonReleaseDao extends UniBaseDao implements IPersonReleaseDao {

    private static boolean disabled = true;

    public static final SyncDaemon DAEMON = new SyncDaemon(PersonReleaseDao.class.getName(), 2*60*24 /* 1 раз в 2 суток*/) {
        @Override protected void main() {
            if (disabled) { return; }
            IPersonReleaseDao.instance.get().doReleaseUnusedPersons();
        }
    };

    @Override
    public void doReleaseUnusedPersons()
    {

        final Set<Class<?>> skip = new HashSet<Class<?>>();
        skip.add(Person2PrincipalRelation.class);
        skip.add(IdentityCard.class);
        skip.add(PersonRole.class);

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Person.class, "p").column(DQLExpressions.property(Person.id().fromAlias("p")));
        dql.joinEntity("p", DQLJoinType.left, PersonRole.class, "pr", DQLExpressions.eq(
                DQLExpressions.property("p.id"),
                DQLExpressions.property(PersonRole.person().id().fromAlias("pr"))
        )).where(DQLExpressions.isNull(DQLExpressions.property("pr")));

        int i=0;
        for (final IRelationMeta relation: EntityRuntime.getMeta(Person.class).getIncomingRelations()) {
            if ("delete".equals(relation.getCascade().getStyle("delete", false))) { continue; }
            if (skip.contains(relation.getForeignEntity().getEntityClass())) { continue; }

            final String alias = "alias"+String.valueOf(i++);
            dql.joinEntity("p", DQLJoinType.left, relation.getForeignEntity().getEntityClass(), alias, DQLExpressions.eq(
                    DQLExpressions.property("p", relation.getPrimaryPropertyName()),
                    DQLExpressions.property(alias, relation.getForeignPropertyName())
            )).where(DQLExpressions.isNull(DQLExpressions.property(alias)));
        }

        final List<Long> ids = dql.createStatement(this.getSession()).list();
        for (final Long id: ids) {
            try {
                PersonManager.instance().dao().deletePerson(id);
                logger.info("person-id="+id+": released");
            } catch (Throwable t) {
                logger.fatal("person-id="+id+": "+t.getMessage(), t);
                throw CoreExceptionUtils.getRuntimeException(t);
            }
        }
    }

}
