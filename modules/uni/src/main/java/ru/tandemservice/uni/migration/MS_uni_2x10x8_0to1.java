package ru.tandemservice.uni.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uni_2x10x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность ppsEntry

		//  свойство orgUnit стало обязательным
		{
			// задать значение по умолчанию
			MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update pps_entry_base_t set orgunit_id= ? where id = ?", DBType.LONG, DBType.LONG);
			List<Object[]> rows = tool.executeQuery(
					processor(Long.class, Long.class),
					"select pps_ep.id, ep.orgunit_id from pps_entry_employeepost_t pps_ep " +
							"inner join employeepost_t ep on pps_ep.source_id = ep.id " +
							"inner join pps_entry_base_t ppsentry on pps_ep.id = ppsentry.id " +
							"where ppsentry.orgunit_id is null"
			);
			for (final Object[] row : rows)
			{
				Long ppsEpId = (Long) row[0];
				Long orgUnitId = (Long) row[1];
				updater.addBatch(orgUnitId, ppsEpId);
			}

			// правим сущность почасовика, если есть
			if (tool.tableExists("snpps_timeworker") && tool.tableExists("pps_entry_timeworker_t"))
			{
				List<Object[]> rows2 = tool.executeQuery(
						processor(Long.class, Long.class),
						"select pps_tw.id, tw.orgunit_id from pps_entry_timeworker_t pps_tw " +
								"inner join snpps_timeworker tw on pps_tw.source_id = tw.id " +
								"inner join pps_entry_base_t ppsentry on pps_tw.id = ppsentry.id " +
								"where ppsentry.orgunit_id is null"
				);
				for (final Object[] row : rows2)
				{
					Long ppsTwId = (Long) row[0];
					Long orgUnitId = (Long) row[1];
					updater.addBatch(orgUnitId, ppsTwId);
				}
			}
			updater.executeUpdate(tool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("pps_entry_base_t", "orgunit_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность ppsHourCost

		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("pps_hour_cost_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("ppsHourCost");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность employeePost4PpsEntry

		short entityCode;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("employee_post4pps_entry_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_employeepost4ppsentry"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("ppsentry_id", DBType.LONG).setNullable(false), 
				new DBColumn("employeepost_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCode = tool.entityCodes().ensure("employeePost4PpsEntry");
		}

		// создано обязательное свойство post
		{
			// создать колонку
			tool.createColumn("pps_entry_employeepost_t", new DBColumn("post_id", DBType.LONG));

			// задать значение по умолчанию
			MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update pps_entry_employeepost_t set post_id = ? where id = ?", DBType.LONG, DBType.LONG);
			List<Object[]> rows = tool.executeQuery(
					processor(Long.class, Long.class),
					"select pps_ep.id, pr.postboundedwithqgandql_id from pps_entry_employeepost_t pps_ep " +
							"inner join employeepost_t ep on pps_ep.source_id = ep.id " +
							"inner join orgunittypepostrelation_t pr on ep.postrelation_id = pr.id"
			);
			for (final Object[] row : rows)
			{
				Long ppsEpId = (Long) row[0];
				Long postId = (Long) row[1];
				updater.addBatch(postId, ppsEpId);
			}
			updater.executeUpdate(tool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("pps_entry_employeepost_t", "post_id", false);
		}

		// заполняем мап с табельным номером по ключу для "Запись в реестре ППС (на базе employeePost)"
		Map<TripletKey<Long, Long, Long>, List<String>> key2EmployeeCodeMap = Maps.newHashMap();
		{
			// свойство employeeCode перестало быть формулой
			{
				// создать колонку
				tool.createColumn("pps_entry_employeepost_t", new DBColumn("employeecode_p", DBType.createVarchar(255)));
			}

			List<Object[]> rows = tool.executeQuery(
					processor(Long.class, Long.class, Long.class, String.class),
					"select pr.person_id, pps_ep.post_id, ppsentry.orgunit_id, e.employeecode_p from pps_entry_employeepost_t pps_ep " +
							"inner join personrole_t pr on pps_ep.id = pr.id " +
							"inner join pps_entry_base_t ppsentry on pps_ep.id = ppsentry.id " +
							"inner join employeepost_t ep on pps_ep.source_id = ep.id " +
							"inner join employee_t e on ep.employee_id = e.id " +
							"group by pr.person_id, pps_ep.post_id, ppsentry.orgunit_id, e.employeecode_p " +
							"order by pr.person_id, pps_ep.post_id, ppsentry.orgunit_id, e.employeecode_p"
			);
			for (final Object[] row : rows)
			{
				Long personId = (Long) row[0];
				Long postId = (Long) row[1];
				Long orgUnitId = (Long) row[2];
				String employeeCode = (String) row[3];
				SafeMap.safeGet(key2EmployeeCodeMap, TripletKey.create(personId, postId, orgUnitId), LinkedList.class).add(employeeCode);
			}
		}

		////////////////////////////////////////////////////////////////////////////////
		// начинаем мердж

		mergePpsEntry(tool, entityCode, "pps_entry_employeepost_t", "post_id", "employee_post4pps_entry_t", "employeepost_id");

		////////////////////////////////////////////////////////////////////////////////
		// заполняем табельный номер для ключа, сущности "Запись в реестре ППС (на базе employeePost)"
		{
			MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update pps_entry_employeepost_t set employeecode_p = ? where id = ?", DBType.EMPTY_STRING, DBType.LONG);

			List<Object[]> rows = tool.executeQuery(
					processor(Long.class, Long.class, Long.class, Long.class),
					"select pps_e.id, pr.person_id, pps_e.post_id, ppsentry.orgunit_id from pps_entry_employeepost_t pps_e " +
							"inner join personrole_t pr on pps_e.id = pr.id " +
							"inner join pps_entry_base_t ppsentry on pps_e.id = ppsentry.id"
			);

			for (final Object[] row : rows)
			{
				Long id = (Long) row[0];
				Long personId = (Long) row[1];
				Long postId = (Long) row[2];
				Long orgUnitId = (Long) row[3];

				List<String> employeeCodes = key2EmployeeCodeMap.get(TripletKey.create(personId, postId, orgUnitId));
				updater.addBatch(StringUtils.join(employeeCodes, ", "), id);
			}
			updater.executeUpdate(tool);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность ppsEntryByEmployeePost

		// удалено свойство source
		{
			// удалить колонку
			tool.dropColumn("pps_entry_employeepost_t", "source_id");
		}
    }

	public static void mergePpsEntry(DBTool tool, short entityCode, String tableBySource, String columnBySource, String source4Table, String source4Column) throws Exception
	{
		// группируем по ключу
		Map<TripletKey<Long, Long, Long>, List<Long>> key2SourceMap = Maps.newHashMap();
		Map<TripletKey<Long, Long, Long>, List<Long>> key2PpsEntryIdMap = Maps.newHashMap();

		Statement stmt = tool.getConnection().createStatement();
		stmt.execute("select pps_e.id, pps_e.source_id, pr.person_id, ppsentry.orgunit_id, pps_e." + columnBySource + " from " + tableBySource + " pps_e " +
							 "inner join personrole_t pr on pps_e.id = pr.id " +
							 "inner join pps_entry_base_t ppsentry on pps_e.id = ppsentry.id");

		MigrationUtils.BatchInsertBuilder source4PpsEntryInsert = new MigrationUtils.BatchInsertBuilder(entityCode, "ppsentry_id", source4Column);

		ResultSet src = stmt.getResultSet();
		while (src.next())
		{
			Long id = src.getLong(1);
			Long sourceId = src.getLong(2);
			Long personId = src.getLong(3);
			Long orgUnitId = src.getLong(4);
			Long columnId = src.getLong(5);


			TripletKey<Long, Long, Long> key = TripletKey.create(personId, orgUnitId, columnId);
			SafeMap.safeGet(key2SourceMap, key, ArrayList.class).add(sourceId);
			SafeMap.safeGet(key2PpsEntryIdMap, key, LinkedList.class).add(id);
		}

		// связь старых id (которые схлопнуться) с уникальным id (который останется)
		Map<Long, Long> oldPpsEntryId2UniqueIdMap = Maps.newHashMap();

		for (Map.Entry<TripletKey<Long, Long, Long>, List<Long>> entry : key2SourceMap.entrySet())
		{
			TripletKey<Long, Long, Long> key = entry.getKey();
			List<Long> ppsEntryIds = key2PpsEntryIdMap.get(key);

			Long ppsEntryUniqueId = ppsEntryIds.get(0);

			if (ppsEntryIds.size() > 1)
			{
				ppsEntryIds.stream()
						.filter(ppsEntryOtherId -> !ppsEntryOtherId.equals(ppsEntryUniqueId))
						.forEach(ppsEntryOtherId -> oldPpsEntryId2UniqueIdMap.put(ppsEntryOtherId, ppsEntryUniqueId));
			}

			// создаем новые сущности "Сотрудник для записи в реестре ППС"
			for (Long sourceId : entry.getValue())
			{
				source4PpsEntryInsert.addRow(ppsEntryUniqueId, sourceId);
			}
		}
		source4PpsEntryInsert.executeInsert(tool, source4Table);

		// начинаем менять ссылки у связанных сущностей
		simpleChangePpsEntry(tool, oldPpsEntryId2UniqueIdMap);
		// теперь меняем ссылки у сложных сущностей, на которые много что завязано
		hardChangePpsEntry(tool, oldPpsEntryId2UniqueIdMap);

		// удаляем лишние ППС
		int count = MigrationUtils.massDeleteByIds(tool, tableBySource, oldPpsEntryId2UniqueIdMap.keySet());
		tool.info(count + " rows removed by " + tableBySource);

		String baseTable = "pps_entry_base_t";
		count = MigrationUtils.massDeleteByIds(tool, baseTable, oldPpsEntryId2UniqueIdMap.keySet());
		tool.info(count + " rows removed by " + baseTable);
	}

	private static void simpleChangePpsEntry(DBTool tool, Map<Long, Long> relMap) throws Exception
	{
		// PpsEntry
		updateOldOnNewEntityIds(tool, relMap, "student_t", "ppsentry_id");											// Student
		updateOldOnNewEntityIds(tool, relMap, "epl_pl_pps_t", "ppsentry_id");										// EplPlannedPps
		updateOldOnNewEntityIds(tool, relMap, "tr_journal", "responsible_id");										// TrJournal
		updateOldOnNewEntityIds(tool, relMap, "chngscntfcadvsrextrct_t", "scientificadvisername_id");				// ChangeScientificAdviserExtract
		updateOldOnNewEntityIds(tool, relMap, "chngdplmwrktpcstlstextrct_t", "scientificadvisername_id");			// ChangeDiplomaWorkTopicStuListExtract
		updateOldOnNewEntityIds(tool, relMap, "chngdplmwrktpcstlstextrct_t", "scientificadvisernameold_id");		// ChangeDiplomaWorkTopicStuListExtract
		updateOldOnNewEntityIds(tool, relMap, "stdplmwrktpcandscntfcadvsrst_t", "scientificadvisername_id");		// SetDiplomaWorkTopicAndScientificAdviserStuListExtract
		updateOldOnNewEntityIds(tool, relMap, "stdplmwrktpcandscntfcadvsrst_t", "scientificadvisernameold_id");		// SetDiplomaWorkTopicAndScientificAdviserStuListExtract
		updateOldOnNewEntityIds(tool, relMap, "session_project_theme_t", "supervisor_id");							// SessionProjectTheme
		updateOldOnNewEntityIds(tool, relMap, "loadrealppsentry_t", "pps_id");										// LoadRealPpsEntry
		updateOldOnNewEntityIds(tool, relMap, "pr_practice_assignment_t", "tutor_id");								// PrPracticeAssignment
		updateOldOnNewEntityIds(tool, relMap, "sportsection_t", "trainer_id");										// SportSection

		updateOldOnNewEntityIds(tool, relMap, "ulsunoneduload_t", "pps_id");										// UlsuNonEduLoad (uniulsu)
		updateOldOnNewEntityIds(tool, relMap, "ulsufirstcourseimedugroup_t", "pps_id");								// UlsuFirstCourseImEduGroup (uniulsu)

		// PpsEntryByEmployeePost
		updateOldOnNewEntityIds(tool, relMap, "session_doc_tp_t", "chairman_id");									// SessionTransferProtocolDocument
	}

	private static void updateOldOnNewEntityIds(DBTool tool, Map<Long, Long> relMap, String table, String column) throws Exception
	{
		if (!tool.tableExists(table) || !tool.columnExists(table, column)) return;

		MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update " + table + " set " + column + " = ? where " + column + " = ?", DBType.LONG, DBType.LONG);
		for (Map.Entry<Long, Long> entry : relMap.entrySet())
		{
			Long oldId = entry.getKey();
			Long newId = entry.getValue();
			updater.addBatch(newId, oldId);
		}
		updater.executeUpdate(tool);
	}

	private static void hardChangePpsEntry(DBTool tool, Map<Long, Long> relMap) throws Exception
	{
		// epp_pps_collection_t - EppPpsCollectionItem - "Элемент списка ППС"
		hardChangeEntity(tool, relMap, true, "epp_pps_collection_t", "pps_id", "list_id");
		// session_comission_pps_t - SessionComissionPps - "Преподаватель в комиссии"
		hardChangeEntity(tool, relMap, false, "session_comission_pps_t", "pps_id", "commission_id");
		// epl_ind_plan_t - EplIndividualPlan - "Индивидуальный план преподавателя (ИПП)"
		hardChangeEntity(tool, relMap, true, "epl_ind_plan_t", "pps_id", "eppyear_id", "orgunit_id");
		// sakaicourse2ppsentryrel_t - SakaiCourse2PpsEntryRel - "Связь ЭУК с преподавателем"
		hardChangeEntity(tool, relMap, true, "sakaicourse2ppsentryrel_t", "ppsentry_id", "sakaicourse_id", "trjournal_id");

		// bbcourse2ppsentryrel_t - BbCourse2PpsEntryRel (unifefu) - "Связь ЭУК с преподавателем"
		hardChangeEntity(tool, relMap, false, "bbcourse2ppsentryrel_t", "ppsentry_id", "bbcourse_id");
	}

	private static void hardChangeEntity(DBTool tool, Map<Long, Long> relMap, boolean needAdditionAction, String table, String columnMain, String... columnSecondaries) throws Exception
	{
		if (!tool.tableExists(table) || !tool.columnExists(table, columnMain)) return;

		// {уник. набор свойств} -> {id сущности}
		Map<MultiKey, Long> uniqueRowMap = Maps.newHashMap();
		// список строк (по констрейнту), в которых может произойти при потенциальном обновление строки
		List<MultiKey> changeRowMainIds = Lists.newArrayList();
		// мап связи по id сущности, старое значение заменяется новым
		Map<Long, Long> old2NewEntityRowIdRel = Maps.newHashMap();
		boolean update = false;
		boolean hasOneColumn = columnSecondaries.length == 1;

		MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update " + table + " set " + columnMain + " = ? where id = ?", DBType.LONG, DBType.LONG);

		// поднимаем все записи, чтобы определить, натолкнемся ли мы на констрейнт при обновлении
		List<Object[]> rows = hasOneColumn
				? tool.executeQuery(processor(Long.class, Long.class, Long.class), "select id, " + columnMain + ", " + columnSecondaries[0] + " from " + table)
				: tool.executeQuery(processor(Long.class, Long.class, Long.class, Long.class), "select id, " + columnMain + ", " + columnSecondaries[0] + ", " + columnSecondaries[1] + " from " + table);
		for (final Object[] row : rows)
		{
			Long id = (Long) row[0];
			Long mainId = (Long) row[1];

			MultiKey key;
			if (hasOneColumn)
			{
				Long columnId = (Long) row[2];
				key = new MultiKey(mainId, columnId);
			}
			else
			{
				Long column1Id = (Long) row[2];
				Long column2Id = (Long) row[3];
				key = new MultiKey(mainId, column1Id, column2Id);
			}
			uniqueRowMap.put(key, id);

			// если нашли изменившийся mainId - запоминаем
			if (relMap.containsKey(mainId))
			{
				changeRowMainIds.add(key);
			}
		}

		// идем по тем записям, которые будут изменены
		for (MultiKey key : changeRowMainIds)
		{
			Long mainIncorrectId = (Long) key.getKey(0);

			// значение, НА которое будем менять текущий mainId
			Long mainUniqueId = relMap.get(mainIncorrectId);
			// сформированный ключ (новый) по значению выше
			MultiKey uniqueKey;
			if (hasOneColumn)
			{
				Long columnId = (Long) key.getKey(1);
				uniqueKey = new MultiKey(mainUniqueId, columnId);
			}
			else
			{
				Long column1Id = (Long) key.getKey(1);
				Long column2Id = (Long) key.getKey(2);
				uniqueKey = new MultiKey(mainUniqueId, column1Id, column2Id);
			}

			// id сущности, по ключу текущего значения
			Long rowIncorrectId = uniqueRowMap.get(key);
			// id сущности, по новому ключу
			Long rowUniqueId = uniqueRowMap.get(uniqueKey);

			if (null != rowUniqueId)
			{
				// если ключ уже есть в базе, значит нарвались на констрейнт. rowIncorrectId - строка, которую нужно удалить, чтобы оставить одну уникальную строку
				old2NewEntityRowIdRel.put(rowIncorrectId, rowUniqueId);
			}
			else
			{
				// всё прошло гладко, заменяем старый mainId на новый
				updater.addBatch(mainUniqueId, rowIncorrectId);
				update = true;
				uniqueRowMap.put(uniqueKey, rowIncorrectId);
			}
		}

		if (update)
		{
			// обновляем то, что получилось обновить
			updater.executeUpdate(tool);
		}

		// всё-таки словили констрейнт? Удаляем эти строки!
		if (!old2NewEntityRowIdRel.isEmpty())
		{
			if (needAdditionAction)
			{
				// доп. причесывания в продуктовых/проектных слоях
				additionActionBeforeRemove(tool, table, old2NewEntityRowIdRel);
			}

			// удаляем лишние ссылки
			int count = MigrationUtils.massDeleteByIds(tool, table, old2NewEntityRowIdRel.keySet());
			tool.info(count + " rows removed by " + table);
		}
	}

	private static void additionActionBeforeRemove(DBTool tool, String table, Map<Long, Long> old2NewEntityRowIdRel) throws Exception
	{
		// переносим ссылки связанных объектов

		if (table.equals("epp_pps_collection_t"))
		{
			// УлГУ - UlsuEduGroupPpsDistr (Распределение часов УГС)
			String subTable = "ulsuedugroupppsdistr_t";
			String subColumn = "ppsgroup_id";
			if (tool.tableExists(subTable))
			{
				boolean update = false;
				Map<Long, Long> ppsGroup2IdRel = Maps.newHashMap();
				MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update " + subTable + " set " + subColumn + " = ? where " + subColumn + " = ?", DBType.LONG, DBType.LONG);
				List<Object[]> rows = tool.executeQuery(processor(Long.class, Long.class), "select id, " + subColumn + " from " + subTable);

				for (final Object[] row : rows)
				{
					Long id = (Long) row[0];
					Long ppsGroupId = (Long) row[1];
					ppsGroup2IdRel.put(ppsGroupId, id);
				}

				List<Long> removeIds = Lists.newArrayList();
				for (Map.Entry<Long, Long> entry : old2NewEntityRowIdRel.entrySet())
				{
					Long oldId = entry.getKey();
					Long newId = entry.getKey();
					Long id = ppsGroup2IdRel.get(oldId);

					if (ppsGroup2IdRel.keySet().contains(newId))
						removeIds.add(id);
					else
					{
						updater.addBatch(newId, oldId);
						update = true;
					}
				}

				if (update)
					updater.executeUpdate(tool);

				int count = MigrationUtils.massDeleteByIds(tool, subTable, removeIds);
				tool.info(count + " rows removed by " + subTable);
			}
		}
		else if (table.equals("epl_ind_plan_t"))
		{
			// Рассчитанные часы внеучебной нагрузки
			updateOldOnNewEntityIds(tool, old2NewEntityRowIdRel, "epl_time_item_non_edu_load_t", "indplan_id");

			// epl_ind_plan_2_ou_summary_t - EplIndPlan2OuSummaryRel - "Учебная нагрузка для ИПП"
			hardChangeEntity(tool, old2NewEntityRowIdRel, false, "epl_ind_plan_2_ou_summary_t", "indplan_id", "ousummary_id");
		}
		else if (table.equals("sakaicourse2ppsentryrel_t"))
		{
			updateOldOnNewEntityIds(tool, old2NewEntityRowIdRel, "sakaitrjournaleventrel_t", "courserelation_id");
		}
	}
}