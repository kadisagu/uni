package ru.tandemservice.uni.entity.report;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.entity.report.gen.StudentSummaryReportGen;

/**
 * Сводка контингента студентов (по направлениям подготовки)
 */
public class StudentSummaryReport extends StudentSummaryReportGen implements IStorableReport
{
    public static final String TEMPLATE_NAME = "studentSummary";
}