/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniStudent.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.uni.base.bo.UniStudent.logic.list.StudentSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
@Configuration
public class UniStudentList extends AbstractUniStudentList
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return  super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler())));
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
                .addColumn(actionColumn("printStudentPersonalCard", new Icon("printer"), "onClickPrintStudentPersonalCard").permissionKey("printStudentPersonalCard_List"))
                .addColumn(actionColumn("editStudent", new Icon("edit"), "onClickEditStudent").permissionKey("editStudentList"))
            .addColumn(actionColumn("deleteStudent", new Icon("delete"), "onClickDeleteStudent")
                .permissionKey("deleteStudentList")
                .alert(FormattedMessage.with().template("studentSearchListDS.deleteStudent.alert").parameter(Student.titleWithFio().s()).create()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new StudentSearchListDSHandler(getName());
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(columnList, GROUP_COLUMN);
        insertColumns(columnList, newGroupPublisherColumnBuilder().after(COMPENSATION_TYPE_COLUMN).create());
        return columnList;
    }
}
