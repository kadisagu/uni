/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.structureEducationLevels.StructureEducationLevelsPub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author vip_delete
 */
public class Model extends DefaultCatalogPubModel<StructureEducationLevels>
{
    private Map<StructureEducationLevels, List<StructureEducationLevels>> cache = new HashMap<StructureEducationLevels, List<StructureEducationLevels>>();

    public Map<StructureEducationLevels, List<StructureEducationLevels>> getCache()
    {
        return cache;
    }

    public void setCache(Map<StructureEducationLevels, List<StructureEducationLevels>> cache)
    {
        this.cache = cache;
    }
}
