/* $Id$ */
package ru.tandemservice.uni.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.ScriptItemResetToDefaultObject;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

/**
 * @author Nikolay Fedorovskih
 * @since 23.11.2014
 */
@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(_depersonalizationManager.updateBuilder(AcademyData.class)
                        .autoReplaceAllStringFields()
                        .autoCleanSomeFields())
                .add(new ScriptItemResetToDefaultObject(UniScriptItem.class))
                .build();
    }
}