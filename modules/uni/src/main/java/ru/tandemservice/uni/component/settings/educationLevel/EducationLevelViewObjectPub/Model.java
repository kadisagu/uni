/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.EducationLevelViewObjectPub;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author vip_delete
 */
@State(keys = {"educationLevelHighSchoolId"}, bindings = {"educationLevelHighSchoolId"})
@Output(keys = {"educationOrgUnitId", "educationLevelHighSchoolId"}, bindings = {"educationOrgUnitId", "educationLevelHighSchoolId"})
public class Model
{
    private Long _educationOrgUnitId;
    private Long _educationLevelHighSchoolId;

    private EducationLevelsHighSchool _educationLevelHighSchool;
    private DynamicListDataSource<EducationOrgUnit> _dataSource;

    public Long getEducationOrgUnitId()
    {
        return _educationOrgUnitId;
    }

    public void setEducationOrgUnitId(Long educationOrgUnitId)
    {
        _educationOrgUnitId = educationOrgUnitId;
    }

    public Long getEducationLevelHighSchoolId()
    {
        return _educationLevelHighSchoolId;
    }

    public void setEducationLevelHighSchoolId(Long educationLevelHighSchoolId)
    {
        _educationLevelHighSchoolId = educationLevelHighSchoolId;
    }

    public EducationLevelsHighSchool getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    public void setEducationLevelHighSchool(EducationLevelsHighSchool educationLevelHighSchool)
    {
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    public DynamicListDataSource<EducationOrgUnit> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EducationOrgUnit> dataSource)
    {
        _dataSource = dataSource;
    }
}
