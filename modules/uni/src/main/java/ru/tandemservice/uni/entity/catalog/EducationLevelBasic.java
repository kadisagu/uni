package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelBasicGen;

/**
 * Направление подготовки (специальности) НПО
 */
public class EducationLevelBasic extends EducationLevelBasicGen
{
    public EducationLevelBasic()
    {
        setCatalogCode("educationLevelBasic");
    }
}