/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsPrintPersonData;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 * @since 09.11.2009
 */
public class Model
{
    public static final Long STUDENT_TYPE = 0L;
    public static final Long EMPLOYEE_TYPE = 1L;

    private List<IdentifiableWrapper> _personRoleTypeList;
    private IdentifiableWrapper _personRoleType;

    // Студент

    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;

    private ISelectModel _territorialOrgUnitListModel;
    private List<OrgUnit> _territorialOrgUnitList;

    private ISelectModel _educationLevelHighSchoolListModel;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;

    private ISelectModel _developFormListModel;
    private List<DevelopForm> developFormList;

    private ISelectModel _courseListModel;
    private List<Course> _courseList;

    private ISelectModel _groupListModel;
    private List<Group> _groupList;

    private ISelectModel _qualificationListModel;
    private List<Qualifications> _qualificationList;

    // Сотрудник

    private ISelectModel _orgUnitTypeListModel;
    private List<OrgUnitType> _orgUnitTypeList;

    private ISelectModel _orgUnitListModel;
    private List<OrgUnit> _orgUnitList;

    private boolean _withRoles;

    // Getters & Setters

    public List<IdentifiableWrapper> getPersonRoleTypeList()
    {
        return _personRoleTypeList;
    }

    public void setPersonRoleTypeList(List<IdentifiableWrapper> personRoleTypeList)
    {
        _personRoleTypeList = personRoleTypeList;
    }

    public IdentifiableWrapper getPersonRoleType()
    {
        return _personRoleType;
    }

    public void setPersonRoleType(IdentifiableWrapper personRoleType)
    {
        _personRoleType = personRoleType;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        this.developFormList = developFormList;
    }

    public ISelectModel getCourseListModel()
    {
        return _courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel)
    {
        _courseListModel = courseListModel;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public ISelectModel getOrgUnitTypeListModel()
    {
        return _orgUnitTypeListModel;
    }

    public void setOrgUnitTypeListModel(ISelectModel orgUnitTypeListModel)
    {
        _orgUnitTypeListModel = orgUnitTypeListModel;
    }

    public List<OrgUnitType> getOrgUnitTypeList()
    {
        return _orgUnitTypeList;
    }

    public void setOrgUnitTypeList(List<OrgUnitType> orgUnitTypeList)
    {
        _orgUnitTypeList = orgUnitTypeList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public boolean isWithRoles()
    {
        return _withRoles;
    }

    public void setWithRoles(boolean withRoles)
    {
        _withRoles = withRoles;
    }
}
