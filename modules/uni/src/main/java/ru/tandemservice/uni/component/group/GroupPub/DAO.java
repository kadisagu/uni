/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.GroupPub;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.component.group.GroupStartEduYearEdit.IGroupStartEduYearUpdateBlock;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _studentOrderSettings = new OrderDescriptionRegistry("s");
    private static OrderDescription[] FIO_KEY = {new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME)};
    protected static String[] STATUS_KEY = new String[]{Student.L_STATUS, StudentStatus.P_TITLE};

    static
    {
        _studentOrderSettings.setOrders(Student.person().identityCard().sex().shortTitle(), (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("idCard", IdentityCard.sex().shortTitle())}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.person().identityCard().citizenship().title(), (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("idCard", IdentityCard.citizenship().title())}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.course().title(), (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("s", Student.course().title())}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.compensationType().shortTitle(), (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("s", Student.compensationType().shortTitle())}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.entranceYear().s(), (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("s", Student.entranceYear().s())}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.PASSPORT_KEY, new OrderDescription("idCard", IdentityCard.P_SERIA), new OrderDescription("idCard", IdentityCard.P_NUMBER));
        _studentOrderSettings.setOrders(Student.FORMATIVE_ORGUNIT_KEY, (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("s", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE})}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.TERRITORIAL_ORGUNIT_KEY, (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("s", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE})}, FIO_KEY));
        _studentOrderSettings.setOrders(Student.PRODUCTIVE_ORGUNIT_KEY, (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("s", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE})}, FIO_KEY));
        _studentOrderSettings.setOrders(STATUS_KEY, (OrderDescription[]) ArrayUtils.addAll(new OrderDescription[]{new OrderDescription("studentStatus", StudentStatus.P_PRIORITY)}, FIO_KEY));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(Model model)
    {
        model.setGroup(getNotNull(Group.class, model.getGroupId()));
        model.setCaptainStudentList(UniDaoFacade.getGroupDao().getCaptainStudentList(model.getGroup()));
        model.setStartEduYearEditDisabled(false);

        EducationLevelsHighSchool educationLevelHighSchool = model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
        model.setSpeciality(educationLevelHighSchool.getEducationLevel().getProgramSubjectWithCodeIndexAndGenTitle());
        model.setSpecialization(educationLevelHighSchool.getEducationLevel().getProgramSpecializationTitle());

        if (ApplicationRuntime.containsBean(IGroupStartEduYearUpdateBlock.LIST_BEAN_NAME))
            for (IGroupStartEduYearUpdateBlock block : (List<IGroupStartEduYearUpdateBlock>) ApplicationRuntime.getBean(IGroupStartEduYearUpdateBlock.LIST_BEAN_NAME))
                if (block.isBlocked(model.getGroup()))
                    model.setStartEduYearEditDisabled(true);

        model.setStudentCustomStateCIModel(new CommonMultiSelectModel(StudentCustomStateCI.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "st").column(property("st"))
                        .order(property(StudentCustomStateCI.title().fromAlias("st")));

                if (!StringUtils.isEmpty(filter))
                    builder.where(like(upper(property(StudentCustomStateCI.title().fromAlias("st"))), value(CoreStringUtils.escapeLike(filter, true))));

                if (set != null)
                    builder.where(in(property(StudentCustomStateCI.id().fromAlias("st")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        final DynamicListDataSource<Student> dataSource = model.getStudentDataSource();

        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", Student.L_GROUP, model.getGroup()));

        Boolean           isShowArchivalStydent = model.getSettings().get("showArchivalStydent");
        if(isShowArchivalStydent == null)
        {
            isShowArchivalStydent = false;
        }

      if (!model.getGroup().isArchival() && !isShowArchivalStydent )
      {
            builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
      }



        builder.addJoinFetch("s", Student.L_PERSON, "p");
        builder.addJoinFetch("p", Person.L_IDENTITY_CARD, "idCard");
        builder.addJoinFetch("s", Student.L_STATUS, "studentStatus");

        IdentifiableWrapper<IEntity> status = model.getSettings().get("status");
        if (status != null)
        {
            builder.add(MQExpression.eq("studentStatus", StudentStatus.P_ACTIVE, status.getId().equals(IStudentListModel.STUDENT_STATUS_ACTIVE)));
        }

        List<StudentCustomStateCI> studentCustomStateCIList = model.getSettings().get("studentCustomStateCIs");
        CustomStateUtil.addCustomStatesFilter(builder, "s", studentCustomStateCIList);

        if (!Student.FIO_KEY.equals(dataSource.getEntityOrder().getKey()))
            _studentOrderSettings.applyOrder(builder, dataSource.getEntityOrder());

        List<Student> list = builder.getResultList(getSession());
	    sortStudentList(dataSource, list);

        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);

        Map<Long, Set<StudentCustomState>> studentActiveCustomStateMap = CustomStateUtil.getActiveCustomStatesMap(UniBaseDao.ids(list));

        for (ViewWrapper<Student> viewWrapper : ViewWrapper.<Student>getPatchedList(dataSource))
        {
            viewWrapper.setViewProperty(Model.P_STUDENT_ACTIVE_CUSTOME_STATES, studentActiveCustomStateMap.get(viewWrapper.getEntity().getId()));
        }
    }

	protected void sortStudentList(final DynamicListDataSource<Student> dataSource, List<Student> list)
	{
		if (Student.FIO_KEY.equals(dataSource.getEntityOrder().getKey()))
		{
		    Collections.sort(list, (o1, o2) -> {
                final int ret = Student.FULL_FIO_AND_ID_COMPARATOR.compare(o1, o2);
                return dataSource.getEntityOrder().getDirection() == OrderDirection.asc ? ret : -ret;
            });
		}
	}

	@SuppressWarnings({"unchecked", "JpaQlInspection"})
    @Override
    public void setArchival(Model model, boolean archival, ErrorCollector errorCollector)
    {
        Session session = getSession();
        Group group = model.getGroup();
        if (archival)
        {
            Calendar calendar = Calendar.getInstance();
            EducationYear endingYear = get(EducationYear.class, EducationYear.current(), true);

            group.setEndingYear(endingYear);
            group.setArchivingDate(calendar.getTime());

            List<Student> groupNonArchivalStudents = session.createCriteria(Student.class).add(Restrictions.eq(Student.L_GROUP, group)).add(Restrictions.eq(Student.P_ARCHIVAL, false)).list();
            for (Student student : groupNonArchivalStudents)
            {
                if (student.getStatus().isActive())
                {
                    throw new ApplicationException("Группа " + group.getTitle() + " не может быть списана в архив, поскольку в группе есть студенты в активном состоянии.");
                }
            }
            for (Student student : groupNonArchivalStudents)
            {
                student.setArchival(true);
                student.setFinishYear(endingYear.getIntValue());
                student.setArchivingDate(calendar.getTime());
            }
        }
        else
        {
            Criteria c = getSession().createCriteria(Group.class);
            c.add(Restrictions.eq(Group.P_TITLE, group.getTitle()));
            c.add(Restrictions.eq(Group.P_ARCHIVAL, Boolean.FALSE));
            c.setProjection(Projections.rowCount());
            Number number = (Number) c.uniqueResult();
            int num = number == null ? 0 : number.intValue();
            if (num > 1)
            {
                throw new ApplicationException("Нельзя восстановить группу из архива, т.к. уже существует группа с данным названием.");
            }

            List<Long> groupStudentsIds = new ArrayList<>();
            List<Student> groupStudents = getList(Student.class, Student.L_GROUP, group);

            for (Student student : groupStudents)
            {
                student.setArchival(false);
                student.setPersonalArchivalFileNumber(null);
                student.setDescription(null);

                groupStudentsIds.add(student.getId());
            }

            // получить удостоверения личности тех студентов, номер зачетной книжки которых зарегистрирован у одного из неархивных студентов
            List<String> bookNumberConflictStudents = new ArrayList<>();
            if (!groupStudentsIds.isEmpty())
            {
                String queryString = "select student." + Student.L_PERSON + "." + Person.L_IDENTITY_CARD + " from " + Student.ENTITY_CLASS + " student where student.id in (:students) and exists (select s.id from " + Student.ENTITY_CLASS + " s where s." + Student.P_BOOK_NUMBER + " = student." + Student.P_BOOK_NUMBER + " and s." + Student.P_ARCHIVAL + " = :archival and s.id != student.id)";
                List<IdentityCard> identityCards = session.createQuery(queryString).setParameterList("students", groupStudentsIds).setBoolean("archival", false).list();
                for (IdentityCard identityCard : identityCards)
                    bookNumberConflictStudents.add(identityCard.getFullFio());
                if (!bookNumberConflictStudents.isEmpty())
                    errorCollector.add("Данные из архива восстановлены. Номер зачетной книжки студента(ов) " + StringUtils.join(bookNumberConflictStudents.iterator(), ", ") + " совпадает с одним из номеров обучаемых студентов.");
            }
        }
        model.getGroup().setArchival(archival);
        update(model.getGroup());
    }

    @Override
    public void deleteStudent(Model model)
    {
        PersonManager.instance().dao().deletePersonRole(model.getStudentId());
    }
}