/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentDocumentList;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author euroelessar
 * @since 23.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());

        getDao().prepare(model);

        prepareListDataSource(component);

        model.setSettings(component.getSettings());

        prepareListDataSource(component);
    }

    protected String getSettingsKey(Model model)
    {
        StringBuilder key = new StringBuilder();
        key.append("OrgUnitStudentDocumentList_").append(model.getOrgUnit().getId());
        if (model.isArchival())
            key.append("_archival");
        key.append(".filter");
        return key.toString();
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StudentDocument> dataSource = UniBaseUtils.createDataSource(component, getDao());
//        dataSource.addColumn(IndicatorColumn.createIconColumn("document", "Выданный студенту документ"));
        dataSource.addColumn(new PublisherLinkColumn("ФИО", new String[] { Student.L_PERSON, Person.P_FULLFIO}, new String[] { StudentDocument.L_STUDENT })
                .setResolver(new DefaultPublisherLinkResolver()
                {
                    @Override
                    public Object getParameters(IEntity entity)
                    {
                        return ParametersMap.createWith(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY, "documentTab")
                                .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                    }
                }).setOrderable(true).setClickable(true));
        dataSource.addColumn(new SimpleColumn("№ документа", StudentDocument.P_NUMBER)
        .setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата формирования", StudentDocument.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME)
        .setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип документа", StudentDocument.L_STUDENT_DOCUMENT_TYPE + "." + StudentDocumentType.P_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", StudentDocument.L_STUDENT + "." + Student.L_COURSE + "." + Course.P_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", StudentDocument.L_STUDENT + "." + Student.L_GROUP + "." + Group.P_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", StudentDocument.L_STUDENT + "." + Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_TERRITORIAL_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", StudentDocument.L_STUDENT + "." + Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE)
        .setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", StudentDocument.student().educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectTitleWithCode()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", StudentDocument.student().educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle()).setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Форма освоения", StudentDocument.L_STUDENT + "." + Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", StudentDocument.L_STUDENT + "." + Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Нормативный срок", StudentDocument.L_STUDENT + "." + Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD + "." + DevelopPeriod.P_TITLE)
        .setOrderable(false).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickDocumentPrint", "Печатать").setPermissionKey("orgUnit_printStudentDocument_" + model.getOrgUnit().getOrgUnitType().getCode()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDocumentDelete", "Удалить документ №{0}, выданный для студента «{1}»?", StudentDocument.number().s(), StudentDocument.L_STUDENT + "." + Student.L_PERSON + "." + Person.P_FULLFIO).setPermissionKey("orgUnit_deleteStudentDocument_" + model.getOrgUnit().getOrgUnitType().getCode()));
        model.setDataSource(dataSource);
    }

    public void onClickDocumentPrint(IBusinessComponent component)
    {
        StudentDocument studentDocument = getDao().getNotNull(StudentDocument.class, (Long) component.getListenerParameter());
        if (studentDocument.getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("StudentDocument.rtf").document(studentDocument.getContent()).rtf(), true);
    }

    public void onClickDocumentDelete(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
        getModel(component).getDataSource().refresh();
    }

    public void onClickSearch(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        onClickSearch(context);
    }
}