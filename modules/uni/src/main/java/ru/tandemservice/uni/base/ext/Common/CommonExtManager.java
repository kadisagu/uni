/* $Id:$ */
package ru.tandemservice.uni.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
        .add("uni", () -> {
            IUniBaseDao dao = IUniBaseDao.instance.get();

            final List<Object[]> activeEduProgKindDivList = dao.getList(new DQLSelectBuilder().fromEntity(Student.class, "s")
                .where(eq(property(Student.status().active().fromAlias("s")), value(true)))
                .column(property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().title().fromAlias("s")))
                .column(property(Student.educationOrgUnit().groupOrgUnit().title().fromAlias("s")))
                .column(count(property(Student.id().fromAlias("s"))))
                .group(property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().title().fromAlias("s")))
                .group(property(Student.educationOrgUnit().groupOrgUnit().title().fromAlias("s")))
                .order(property(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().title().fromAlias("s")))
                .order(property(Student.educationOrgUnit().groupOrgUnit().title().fromAlias("s")))

            );

            final List<Object[]> activeDevFormDivList = dao.getList(new DQLSelectBuilder().fromEntity(Student.class, "s")
                            .where(eq(property(Student.status().active().fromAlias("s")), value(true)))
                            .column(property(Student.educationOrgUnit().developForm().title().fromAlias("s")))
                            .column(count(property(Student.id().fromAlias("s"))))
                            .group(property(Student.educationOrgUnit().developForm().title().fromAlias("s")))
                            .order(property(Student.educationOrgUnit().developForm().title().fromAlias("s")))
            );

            final List<Object[]> specInSubjectCountList = dao.getList(new DQLSelectBuilder().fromEntity(EduProgramSpecializationChild.class, "s")
                            .column(property(EduProgramSpecializationChild.programSubject().title().fromAlias("s")))
                            .column(count(property(EduProgramSpecializationChild.id().fromAlias("s"))))
                            .group(property(EduProgramSpecializationChild.programSubject().title().fromAlias("s")))
                            .order(count(property(EduProgramSpecializationChild.id().fromAlias("s"))), OrderDirection.desc)
                            .top(3)
            );

            final List<Object[]> eduProgramsCountByEduYearList = dao.getList(new DQLSelectBuilder().fromEntity(EduProgram.class, "s")
                            .column(property(EduProgram.year().title().fromAlias("s")))
                            .column(count(property(EduProgramSubject.id().fromAlias("s"))))
                            .group(property(EduProgram.year().title().fromAlias("s")))
                            .order(property(EduProgram.year().title().fromAlias("s")))
            );


            final List<Object[]> eduOrgUnitWithActiveTerOUDivStList = dao.getList(new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "s")
                            .joinEntity("s", DQLJoinType.inner, Student.class, "e", eq(property(Student.educationOrgUnit().id().fromAlias("e")), property(EducationOrgUnit.id().fromAlias("s"))))
                            .column(property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("s")))
                            .column(count(DQLPredicateType.distinct, property(EducationOrgUnit.id().fromAlias("s"))))
                            .distinct()
                            .group(property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("s")))
                            .order(property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("s")))
            );

            final List<Object[]> eduOrgUnitWithActiveGrOUDivStList = dao.getList(new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "s")
                .joinEntity("s", DQLJoinType.inner, Student.class, "e", eq(property(Student.educationOrgUnit().id().fromAlias("e")), property(EducationOrgUnit.id().fromAlias("s"))))
                .column(property(EducationOrgUnit.groupOrgUnit().title().fromAlias("s")))
                .column(count(DQLPredicateType.distinct, property(EducationOrgUnit.id().fromAlias("s"))))
                .distinct()
                .group(property(EducationOrgUnit.groupOrgUnit().title().fromAlias("s")))
                .order(property(EducationOrgUnit.groupOrgUnit().title().fromAlias("s")))
            );


            final Collection<String> activeStudentsNppDiv = CollectionUtils.collect(activeEduProgKindDivList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);
            final Collection<String> activeStudentsDevFormDiv = CollectionUtils.collect(activeDevFormDivList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);
            final Collection<String> specInSubjectCount = CollectionUtils.collect(specInSubjectCountList, objects -> objects[1] + " по направлению " + objects[0]);
            final Collection<String> eduProgramsCountByYears = CollectionUtils.collect(eduProgramsCountByEduYearList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);
            final Collection<String> eduOrgUnitsWithActiveTerOUDivStudents = CollectionUtils.collect(eduOrgUnitWithActiveTerOUDivStList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);
            final Collection<String> eduOrgUnitsWithActiveGrOUDivStudents = CollectionUtils.collect(eduOrgUnitWithActiveGrOUDivStList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

            final List<String> strings = new ArrayList<>();
            strings.add("Всего студентов: " + dao.getCount(Student.class));
            strings.add("Всего активных студентов: " + dao.getCount(Student.class, Student.status().active().s(), Boolean.TRUE));
            strings.add("Активных студентов (по виду ОП, деканату): ");
            strings.addAll(activeStudentsNppDiv);
            strings.add("Активных студентов (по формам обучения): ");
            strings.addAll(activeStudentsDevFormDiv);
            strings.add("Всего направлений с НПП: " + dao.getCount(new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                .column(count(DQLPredicateType.distinct, property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().id())))));
            strings.add("Направленностей в рамках направления (максимально): ");
            strings.addAll(specInSubjectCount);
            strings.add("Число ОП по годам: ");
            strings.addAll(eduProgramsCountByYears);
            strings.add("Число НПП с активными студентами (по терр. подр.): ");
            strings.addAll(eduOrgUnitsWithActiveTerOUDivStudents);
            strings.add("Число НПП с активными студентами (по деканатам): ");
            strings.addAll(eduOrgUnitsWithActiveGrOUDivStudents);

            return strings;
        })
        .create();
    }
}
