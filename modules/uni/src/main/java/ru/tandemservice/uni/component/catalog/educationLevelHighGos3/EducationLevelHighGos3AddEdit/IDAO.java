/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3AddEdit;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;

/**
 * @author oleyba
 * @since 5/8/11
 */
public interface IDAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit.IDAO<EducationLevelHighGos3, Model>
{

}
