/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.GroupEdit;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.group.IAbstractGroupTitleDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setGroup(get(Group.class, model.getGroupId()));
        model.setGroupNumber(model.getGroup().getNumber());
        model.setGroupTitleAlgorithm(get(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_CURRENT, Boolean.TRUE));
        model.setTitleAlgorithmNotExists(model.getGroupTitleAlgorithm() == null);

        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setStartEducationYearList(new EducationYearModel());

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, model.getPrincipalContext()));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model)
        {
            @Override
            protected DQLSelectBuilder createEducationLevelsHighSchoolDQL(String alias)
            {
                return super.createEducationLevelsHighSchoolDQL(alias)
                        .where(eq(property(alias, EducationLevelsHighSchool.educationLevel().levelType().allowGroups()), value(true)));
            }
        });

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));

        model.setStartEducationYearList(new EducationYearModel());

        EducationOrgUnit educationOrgUnit = model.getGroup().getEducationOrgUnit();

        model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
        model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        model.setDevelopForm(educationOrgUnit.getDevelopForm());
        model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
        model.setDevelopTech(educationOrgUnit.getDevelopTech());
        model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
    }

    @Override
    public void update(Model model)
    {
        validate(model);

        Group group = model.getGroup();
        EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");
        group.setEducationOrgUnit(educationOrgUnit);

        update(model.getGroup());
    }

    public void validate(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (!model.isTitleAlgorithmNotExists() && model.getGroupNumber() != model.getGroup().getNumber())
        {
            IAbstractGroupTitleDAO generator = (IAbstractGroupTitleDAO) ApplicationRuntime.getBean(model.getGroupTitleAlgorithm().getDaoName());
            int maxNumber = generator.getMaxNumber();
            if (model.getGroupNumber() <= 0 || model.getGroupNumber() >= maxNumber)
            {
                errors.add("Номер группы должен быть от 1 до " + (maxNumber - 1), "gnumber");
                return;
            }
            String title = generator.getTitle(model.getGroup(), model.getGroupNumber());
            MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");
            builder.add(MQExpression.eq("g", Group.P_TITLE, title));
            builder.add(MQExpression.eq("g", Group.P_ARCHIVAL, Boolean.FALSE));
            if (builder.getResultCount(getSession()) > 0)
            {
                errors.add("Номер группы «" + model.getGroupNumber() + "» уже занят группой «" + title + "»", "gnumber");
                return;
            }
            model.getGroup().setNumber(model.getGroupNumber());
        }
    }
}