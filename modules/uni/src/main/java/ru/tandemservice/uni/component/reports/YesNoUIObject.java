/* $Id$ */
package ru.tandemservice.uni.component.reports;

import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2014
 * @deprecated use {@link TwinComboDataSourceHandler}
 */
@Deprecated
public class YesNoUIObject
{
    public static final int YES_CODE = 1;
    public static final int NO_CODE = 0;
    private int _id;
    private String _title;

    private YesNoUIObject(int id, String title)
    {
        _id = id;
        _title = title;
    }

    public int getId()
    {
        return _id;
    }

    public String getTitle()
    {
        return _title;
    }

    public static List<YesNoUIObject> createYesNoList()
    {
        List<YesNoUIObject> list = new ArrayList<YesNoUIObject>();
        list.add(getObject(true));
        list.add(getObject(false));
        return list;
    }

    public static YesNoUIObject getObject(boolean value) {
        return value ? new YesNoUIObject(YES_CODE, "да") : new YesNoUIObject(NO_CODE, "нет");
    }

    @SuppressWarnings("deprecation")
    public boolean isTrue()
    {
        return getId() == org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject.YES_CODE;
    }

	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof YesNoUIObject && this._id == ((YesNoUIObject) obj)._id;
	}

	@Override
	public int hashCode()
	{
		return getId();
    }
}