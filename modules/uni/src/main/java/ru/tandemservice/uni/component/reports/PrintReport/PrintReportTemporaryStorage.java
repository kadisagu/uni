/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.PrintReport;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author vip_delete
 * @since 21.01.2009
 */
public class PrintReportTemporaryStorage
{
    private static final Map<Integer, PrintFormDescription> _tempData = new WeakHashMap<Integer, PrintFormDescription>();

    public static Integer registerTemporaryPrintForm(byte[] content, String fileName)
    {
        final PrintFormDescription printFormDescription = new PrintFormDescription(content, fileName);

        // должен быть именно ноый объект, чтобы ссылка на него была потом была убита сборщиком мусора (в случае чего)
        final Integer key = System.identityHashCode(printFormDescription);
        synchronized(_tempData) {
            _tempData.put(key, printFormDescription);
        }
        return key;
    }

    public static PrintFormDescription getPrintForm(Integer id)
    {
        synchronized(_tempData) {
            return _tempData.remove(id);
        }
    }
}
