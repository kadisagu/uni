/* $Id$ */
package ru.tandemservice.uni.base.bo.UniGroup;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Alexey Lopatin
 * @since 15.11.2013
 */
@Configuration
public class UniGroupManager extends BusinessObjectManager
{
}
