package ru.tandemservice.uni.entity.employee.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись в реестре ППС (на базе сотрудника)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PpsEntryByEmployeePostGen extends PpsEntry
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost";
    public static final String ENTITY_NAME = "ppsEntryByEmployeePost";
    public static final int VERSION_HASH = -1814902789;
    private static IEntityMeta ENTITY_META;

    public static final String L_POST = "post";
    public static final String P_EMPLOYEE_CODE = "employeeCode";

    private PostBoundedWithQGandQL _post;     // Должность/профессия отнесенная к ПКГ и КУ
    private String _employeeCode;     // Табельный номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    /**
     * @param post Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPost(PostBoundedWithQGandQL post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Табельный номер.
     */
    @Length(max=255)
    public String getEmployeeCode()
    {
        return _employeeCode;
    }

    /**
     * @param employeeCode Табельный номер.
     */
    public void setEmployeeCode(String employeeCode)
    {
        dirty(_employeeCode, employeeCode);
        _employeeCode = employeeCode;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PpsEntryByEmployeePostGen)
        {
            setPost(((PpsEntryByEmployeePost)another).getPost());
            setEmployeeCode(((PpsEntryByEmployeePost)another).getEmployeeCode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PpsEntryByEmployeePostGen> extends PpsEntry.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PpsEntryByEmployeePost.class;
        }

        public T newInstance()
        {
            return (T) new PpsEntryByEmployeePost();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "post":
                    return obj.getPost();
                case "employeeCode":
                    return obj.getEmployeeCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "post":
                    obj.setPost((PostBoundedWithQGandQL) value);
                    return;
                case "employeeCode":
                    obj.setEmployeeCode((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "post":
                        return true;
                case "employeeCode":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "post":
                    return true;
                case "employeeCode":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "post":
                    return PostBoundedWithQGandQL.class;
                case "employeeCode":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PpsEntryByEmployeePost> _dslPath = new Path<PpsEntryByEmployeePost>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PpsEntryByEmployeePost");
    }
            

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost#getPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Табельный номер.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost#getEmployeeCode()
     */
    public static PropertyPath<String> employeeCode()
    {
        return _dslPath.employeeCode();
    }

    public static class Path<E extends PpsEntryByEmployeePost> extends PpsEntry.Path<E>
    {
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _post;
        private PropertyPath<String> _employeeCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost#getPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
        {
            if(_post == null )
                _post = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST, this);
            return _post;
        }

    /**
     * @return Табельный номер.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost#getEmployeeCode()
     */
        public PropertyPath<String> employeeCode()
        {
            if(_employeeCode == null )
                _employeeCode = new PropertyPath<String>(PpsEntryByEmployeePostGen.P_EMPLOYEE_CODE, this);
            return _employeeCode;
        }

        public Class getEntityClass()
        {
            return PpsEntryByEmployeePost.class;
        }

        public String getEntityName()
        {
            return "ppsEntryByEmployeePost";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
