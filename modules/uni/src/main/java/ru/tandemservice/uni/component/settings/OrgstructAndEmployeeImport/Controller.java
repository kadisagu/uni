/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.settings.OrgstructAndEmployeeImport;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ekachanova
 * @since 22.02.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        System.out.println("start " + df.format(new Date()));
        Debug.stopLogging();
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            Model model = getModel(component);
            getDao().update(model);
            deactivate(component);
        }
        finally
        {
            eventLock.release();
            Debug.resumeLogging();
        }
        System.out.println("end " + df.format(new Date()));
    }
}
