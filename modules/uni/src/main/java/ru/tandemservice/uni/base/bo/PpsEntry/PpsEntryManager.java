/* $Id:$ */
package ru.tandemservice.uni.base.bo.PpsEntry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.nothing;

/**
 * @author Vasily Zhukov
 * @since 14.02.2011
 */
@Configuration
public class PpsEntryManager extends BusinessObjectManager
{
    public static final String PARAM_TIME_WORKER_ONLY_OU = "twOnlyOu";
    public static final String PARAM_TIME_WORKER_ORG_UNIT_ID = "twOrgUnitId";
    public static final String PARAM_TIME_WORKER_EDU_YEAR_ID = "twEduYearId";

    public static PpsEntryManager instance()
    {
        return instance(PpsEntryManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler timeWorkerDSHandler()
    {
        return PpsEntry.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(nothing()));
    }
}
