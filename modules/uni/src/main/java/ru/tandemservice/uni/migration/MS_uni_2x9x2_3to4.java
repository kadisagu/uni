/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLDeleteQuery;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Nikolay Fedorovskih
 * @since 23.02.2016
 */
public class MS_uni_2x9x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        // Исправляем миграцию MS_uni_2x9x2_0to1, которая во второй своей редакции делала прикладного бакалавра разрешенным для всех направлений бакалавриата 2013

        final Long index_bachelor2013 = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_t", "2013.03");
        final long q_appliedBachelor = tool.getNumericResult("select id from edu_c_pr_qual_t where title_p=? and subjectindex_id=?", "Прикладной бакалавр", index_bachelor2013);
        if (q_appliedBachelor > 0)
        {
            final SQLDeleteQuery del = new SQLDeleteQuery("edu_c_pr_subject_qual_t", "sq");
            del.where("s.subjectindex_id = ?");
            del.where("sq.programqualification_id=?");
            del.where("s.code_p not in " + MS_uni_2x9x2_0to1.appliedBachelorSubjectCodes());
            del.where("sq.id in (" + MigrationUtils.getOrphanIdsSQL(tool, "eduProgramSubjectQualification", null, null) + ")");
            del.getDeletedTableFrom().innerJoin(SQLFrom.table("edu_c_pr_subject_t", "s"), "sq.programsubject_id=s.id");
            final int n = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(del), index_bachelor2013, q_appliedBachelor);
            tool.debug(n + " rows deleted from eduProgramSubjectQualification for applied bachelor");
        }

        // Еще раз делаем авто-фикс квалификаций
        MS_uni_2x9x2_0to1.legalAutoFix(tool);
    }
}