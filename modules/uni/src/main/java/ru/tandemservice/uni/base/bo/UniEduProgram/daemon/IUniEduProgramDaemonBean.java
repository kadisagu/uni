/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author oleyba
 * @since 10/28/14
 */
public interface IUniEduProgramDaemonBean
{
    final SpringBeanCache<IUniEduProgramDaemonBean> instance = new SpringBeanCache<IUniEduProgramDaemonBean>(IUniEduProgramDaemonBean.class.getName());

    /**
     * Обновляет кэш для сортировки НПм по направленности
     * @return true, если что-то было изменено
     */
    @Transactional(propagation= Propagation.REQUIRED)
    boolean doUpdateEduLevelSpecSortCache();
}
