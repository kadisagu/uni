/**
 * $Id$
 */
package ru.tandemservice.uni.component.reports.studentsGroupsList.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;

/**
 * @author dseleznev
 * Created on: 15.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
         activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
            .add("id", getDao().preparePrintReport(getModel(component)))
        ));
    }
}