/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLRestrictedBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author agolubenko
 * @since 04.06.2008
 */
public class EducationLevelDAO extends UniBaseDao implements IEducationLevelDAO
{
    public static final String EDU_LVL_HS_TITLE_PROPERTY;

    static
    {
        String workProperty = StringUtils.trimToNull(ApplicationRuntime.getProperty("ds.educationOrgUnit.educationLevelHighSchool.displayProperty"));
        EDU_LVL_HS_TITLE_PROPERTY = (null != workProperty) ? workProperty : EducationLevelsHighSchool.P_FULL_TITLE;
    }

    public static final Comparator<EducationLevels> EDU_LEVEL_OKSO_TITLE_COMPARATOR = new Comparator<EducationLevels>()
    {
        @Override
        public int compare(EducationLevels o1, EducationLevels o2)
        {
            int cmp = o1.getTitleCodePrefix().compareTo(o2.getTitleCodePrefix());
            if (cmp == 0) { return o1.getTitle().compareTo(o2.getTitle()); }
            return cmp;
        }
    };

    public static final Comparator<EducationLevelsHighSchool> EDU_LEVEL_HIGH_SCHOOL_OKSO_TITLE_COMPARATOR = new Comparator<EducationLevelsHighSchool>()
    {
        @Override
        public int compare(EducationLevelsHighSchool o1, EducationLevelsHighSchool o2)
        {
            int cmp = o1.getEducationLevel().getTitleCodePrefix().compareTo(o2.getEducationLevel().getTitleCodePrefix());
            if (cmp == 0) { return o1.getTitle().compareTo(o2.getTitle()); }
            return cmp;
        }
    };

    @Override
    public List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(List<OrgUnit> formativeOrgUnitList, List<OrgUnit> territorialOrgUnitList, String filter)
    {
        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "e", new String[]{EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL});

        if (null != formativeOrgUnitList && !formativeOrgUnitList.isEmpty())
        {
            builder.add(MQExpression.in("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnitList));
        }
        if (null != territorialOrgUnitList && !territorialOrgUnitList.isEmpty())
        {
            builder.add(MQExpression.in("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnitList));
        }
        builder.add(MQExpression.like("e", EducationOrgUnit.educationLevelHighSchool().displayableTitle().s(), "%" + filter));
        builder.setNeedDistinct(true);
        List<EducationLevelsHighSchool> list = builder.getResultList(getSession());
        Collections.sort(list, new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)));
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(IEducationLevelModel model, String filter)
    {
        Criteria c = getSession().createCriteria(EducationOrgUnit.class, "e");
        c.createAlias("e." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "h");

        if (null != model.getFormativeOrgUnit())
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        if (null != model.getTerritorialOrgUnit())
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        c.setProjection(Projections.distinct(Projections.property("h." + EducationLevelsHighSchool.P_ID)));

        List<Long> ids = c.list();

        if (ids.isEmpty())
            return Collections.emptyList();

        c = getSession().createCriteria(EducationLevelsHighSchool.class);
        c.add(Restrictions.in(EducationLevelsHighSchool.P_ID, ids));
        c.add(Restrictions.ilike(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "%" + filter + "%"));
        c.addOrder(Order.asc(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE));
        return c.list();
    }

    private void _applyFormativeOrgUnit(IEducationLevelModel model, Criteria c)
    {
        if (null != model.getFormativeOrgUnit())
        {
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        }
    }

    private void _applyTerritorialOrgUnit(IEducationLevelModel model, Criteria c)
    {
        if (null != model.getTerritorialOrgUnit())
        {
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        }
    }

    private void _applyEduHS(IEducationLevelModel model, Criteria c)
    {
        if (null != model.getEducationLevelsHighSchool())
        {
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        }
    }

    private void _applyDevelopForm(IEducationLevelModel model, Criteria c)
    {
        if (null != model.getDevelopForm())
        {
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        }
    }

    private void _applyDevelopCondition(IEducationLevelModel model, Criteria c)
    {
        if (null != model.getDevelopCondition())
        {
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        }
    }

    private void _applyDevelopTech(IEducationLevelModel model, Criteria c)
    {
        if (null != model.getDevelopTech())
        {
            c.add(Restrictions.eq("e." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));
        }
    }

    @Override
    @Deprecated
    public List<DevelopForm> getDevelopFormList(IEducationLevelModel model)
    {
        return getDevelopFormList(model, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DevelopForm> getDevelopFormList(IEducationLevelModel model, boolean nullIsIgnore)
    {
        if (!nullIsIgnore)
        {
            if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null)
                return Collections.emptyList();
        }

        Criteria c = getSession().createCriteria(EducationOrgUnit.class, "e");
        c.createAlias("e." + EducationOrgUnit.L_DEVELOP_FORM, "f");
        _applyFormativeOrgUnit(model, c);
        _applyTerritorialOrgUnit(model, c);
        _applyEduHS(model, c);

        c.setProjection(Projections.distinct(Projections.property("f." + DevelopForm.P_ID)));

        List<Long> developFormIds = c.list();

        if (developFormIds.isEmpty())
            return Collections.emptyList();

        c = getSession().createCriteria(DevelopForm.class);
        c.add(Restrictions.in(DevelopForm.P_ID, developFormIds));
        c.addOrder(Order.asc(DevelopForm.P_CODE));

        return c.list();
    }


    @Override
    @Deprecated
    public List<DevelopCondition> getDevelopConditionList(IEducationLevelModel model)
    {
        return getDevelopConditionList(model, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DevelopCondition> getDevelopConditionList(IEducationLevelModel model, boolean nullIsIgnore)
    {
        if (!nullIsIgnore)
        {
            if (model.getFormativeOrgUnit() == null || model.getTerritorialOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null)
                return Collections.emptyList();
        }

        Criteria c = getSession().createCriteria(EducationOrgUnit.class, "e");
        c.createAlias("e." + EducationOrgUnit.L_DEVELOP_CONDITION, "c");
        _applyFormativeOrgUnit(model, c);
        _applyTerritorialOrgUnit(model, c);
        _applyEduHS(model, c);
        _applyDevelopForm(model, c);
        c.setProjection(Projections.distinct(Projections.property("c." + DevelopCondition.P_ID)));

        List<Long> developConditionIds = c.list();

        if (developConditionIds.isEmpty())
            return Collections.emptyList();

        c = getSession().createCriteria(DevelopCondition.class);
        c.add(Restrictions.in(DevelopCondition.P_ID, developConditionIds));
        c.addOrder(Order.asc(DevelopCondition.P_CODE));

        return c.list();
    }


    @Override
    @Deprecated
    public List<DevelopTech> getDevelopTechList(IEducationLevelModel model)
    {
        return getDevelopTechList(model, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DevelopTech> getDevelopTechList(IEducationLevelModel model, boolean nullIsIgnore)
    {
        if (!nullIsIgnore)
        {
            if (model.getFormativeOrgUnit() == null || model.getTerritorialOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null)
                return Collections.emptyList();
        }

        Criteria c = getSession().createCriteria(EducationOrgUnit.class, "e");
        c.createAlias("e." + EducationOrgUnit.L_DEVELOP_TECH, "t");
        _applyFormativeOrgUnit(model, c);
        _applyTerritorialOrgUnit(model, c);
        _applyEduHS(model, c);
        _applyDevelopForm(model, c);
        _applyDevelopCondition(model, c);
        c.setProjection(Projections.distinct(Projections.property("t." + DevelopTech.P_ID)));

        List<Long> developTechIds = c.list();

        if (developTechIds.isEmpty())
            return Collections.emptyList();

        c = getSession().createCriteria(DevelopTech.class);
        c.add(Restrictions.in(DevelopTech.P_ID, developTechIds));
        c.addOrder(Order.asc(DevelopTech.P_CODE));

        return c.list();
    }


    @Override
    @Deprecated
    public List<DevelopPeriod> getDevelopPeriodList(IEducationLevelModel model)
    {
        return getDevelopPeriodList(model, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DevelopPeriod> getDevelopPeriodList(IEducationLevelModel model, boolean nullIsIgnore)
    {
        if (!nullIsIgnore)
        {
            if (model.getFormativeOrgUnit() == null || model.getTerritorialOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null)
                return Collections.emptyList();
        }
        Criteria c = getSession().createCriteria(EducationOrgUnit.class, "e");
        c.createAlias("e." + EducationOrgUnit.L_DEVELOP_PERIOD, "p");
        _applyFormativeOrgUnit(model, c);
        _applyTerritorialOrgUnit(model, c);
        _applyEduHS(model, c);
        _applyDevelopForm(model, c);
        _applyDevelopCondition(model, c);
        _applyDevelopTech(model, c);

        c.setProjection(Projections.distinct(Projections.property("p." + DevelopPeriod.P_ID)));

        List<Long> developPeriodIds = c.list();

        if (developPeriodIds.isEmpty())
            return Collections.emptyList();

        c = getSession().createCriteria(DevelopPeriod.class);
        c.add(Restrictions.in(DevelopPeriod.P_ID, developPeriodIds));
        c.addOrder(Order.asc(DevelopPeriod.P_TITLE));

        return c.list();
    }


    @Override
    public EducationOrgUnit getEducationOrgUnit(IEducationLevelModel model)
    {
        if (model.getFormativeOrgUnit() == null || model.getTerritorialOrgUnit() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null || model.getDevelopPeriod() == null)
            return null;

        Criteria c = getSession().createCriteria(EducationOrgUnit.class);
        c.add(Restrictions.eq(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        c.add(Restrictions.eq(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        c.add(Restrictions.eq(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        c.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        c.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
        c.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));
        c.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriod()));

        return (EducationOrgUnit) c.uniqueResult();
    }

    @Override
    public void applyUsedFilterForEducationOrgUnit(DQLRestrictedBuilder educationOrgUnitBuilder, String educationOrgUnitAlias)
    {
        educationOrgUnitBuilder.where(and(
            eq(property(educationOrgUnitAlias, EducationOrgUnit.used()), value(Boolean.TRUE)),
            eq(property(educationOrgUnitAlias, EducationOrgUnit.educationLevelHighSchool().allowStudents()), value(Boolean.TRUE))
        ));
    }
}