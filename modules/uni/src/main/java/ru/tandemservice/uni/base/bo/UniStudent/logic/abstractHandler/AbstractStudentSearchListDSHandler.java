/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.gen.ICitizenshipGen;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 02.11.12
 */
public abstract class AbstractStudentSearchListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    private String STUDENT_ACTIVE_CUSTOM_STATE_MAP = "studentActiveCustomStateMap";

    private static final OrderDescription LASTNAME_SORT = new OrderDescription("idCard", IdentityCard.P_LAST_NAME, OrderDirection.asc, false);
    private static final OrderDescription FIRSTNAME_SORT = new OrderDescription("idCard", IdentityCard.P_FIRST_NAME, OrderDirection.asc, false);
    private static final OrderDescription MIDDLENAME_SORT = new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME, OrderDirection.asc, false);

    protected DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(Student.class, "s");
    {
        _orderDescriptionRegistry.setOrders(Student.person().fullFio(), new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
        _orderDescriptionRegistry.setOrders(Student.person().identityCard().fullNumber(), new OrderDescription("idCard", IdentityCard.P_SERIA), new OrderDescription("idCard", IdentityCard.P_NUMBER), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.educationOrgUnit().formativeOrgUnit().fullTitle(), new OrderDescription("s", new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE }), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.educationOrgUnit().territorialOrgUnit().territorialFullTitle(), new OrderDescription("s", new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE }), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle(), new OrderDescription("s", new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE }), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.status().title(), new OrderDescription("studentStatus", StudentStatus.P_PRIORITY), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.group().title(), new OrderDescription("g", Group.P_TITLE), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.compensationType().shortTitle(), new OrderDescription("s", new String[]{Student.L_COMPENSATION_TYPE, CompensationType.P_SHORT_TITLE}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.course().title(), new OrderDescription("s", new String[]{Student.L_COURSE, Course.P_TITLE}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.entranceYear(), new OrderDescription("s", new String[]{Student.P_ENTRANCE_YEAR}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.bookNumber(), new OrderDescription("s", new String[]{Student.P_BOOK_NUMBER}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.personalNumber(), new OrderDescription("s", new String[]{Student.P_PERSONAL_NUMBER}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.person().identityCard().citizenship().title(), new OrderDescription("idCard", new String[]{IdentityCard.L_CITIZENSHIP, ICitizenshipGen.P_TITLE}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.person().identityCard().birthDate(), new OrderDescription("idCard", new String[]{IdentityCard.P_BIRTH_DATE}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
        _orderDescriptionRegistry.setOrders(Student.person().identityCard().sex().shortTitle(), new OrderDescription("idCard", new String[]{IdentityCard.L_SEX, Sex.P_SHORT_TITLE}), LASTNAME_SORT, FIRSTNAME_SORT, MIDDLENAME_SORT);
    }

    public AbstractStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId, Student.class);
    }

    public DQLSelectBuilder prepareDQL(ExecutionContext context)
    {
        IdentifiableWrapper statusOption = context.get(AbstractUniStudentListUI.PROP_STUDENT_STATUS_OPTION);
        String lastName = context.get(AbstractUniStudentListUI.PROP_LAST_NAME);
        String firstName = context.get(AbstractUniStudentListUI.PROP_FIRST_NAME);
        String middleName = context.get(AbstractUniStudentListUI.PROP_MIDDLE_NAME);
        String personalNumber = context.get(AbstractUniStudentListUI.PROP_PERSONAL_NUMBER);
        String personalFileNumber = context.get(AbstractUniStudentListUI.PROP_PERSONAL_FILE_NUMBER);
        String bookNumber = context.get(AbstractUniStudentListUI.PROP_BOOK_NUMBER);
        Number entranceYear = context.get(AbstractUniStudentListUI.PROP_ENTRANCE_YEAR);
        List<StudentStatus> studentStatusList = context.get(AbstractUniStudentListUI.PROP_STUDENT_STATUS_LIST);
        List<Course> courseList = context.get(AbstractUniStudentListUI.PROP_COURSE_LIST);
        List<Object> groupList = context.get(AbstractUniStudentListUI.PROP_GROUP_LIST);
        CompensationType compType = context.get(AbstractUniStudentListUI.PROP_COMPENSATION_TYPE);
        List<Qualifications> qualificationList = context.get(AbstractUniStudentListUI.PROP_QUALIFICATION_lIST);
        List<DevelopPeriod> developPeriodAutoList = context.get(AbstractUniStudentListUI.PROP_DEVELOP_PERIOD_AUTO_LIST);
        List<StudentCategory> studentCategoryList = context.get(AbstractUniStudentListUI.PROP_STUDENT_CATEGORY_LIST);
        DataWrapper specialPurposeRecruit = context.get(AbstractUniStudentListUI.PROP_SPECIAL_PURPOSE_RECRUIT);
        IdentifiableWrapper endingYear = context.get(AbstractUniStudentListUI.PROP_ENDING_YEAR);
        List<StudentCustomStateCI> studentCustomStateCIList = context.get(AbstractUniStudentListUI.PROP_STUDENT_CUSTOM_STATE_CI);
        AddressCountryVO countryVO = context.get(AbstractUniStudentListUI.PROP_CITIZENSHIP);
        String oldLastName = context.get(AbstractUniStudentListUI.PROP_OLD_LAST_NAME);
        List<String> groupTitles = null;

        UniEduProgramEducationOrgUnitAddon util = context.get(AbstractUniStudentListUI.EDU_PROGRAMM_UTIL);

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
        .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
        .joinPath(DQLJoinType.left, Student.status().fromAlias("s"), "studentStatus")
        .fetchPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("s"), "ou")
        .fetchPath(DQLJoinType.inner, Student.person().fromAlias("s"), "p")
        .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "idCard");

        if (groupList != null)
            groupTitles = groupList.stream()
                    .filter(e -> e instanceof DataWrapper)
                    .map(DataWrapper.class::cast)
                    .map(DataWrapper::getTitle)
                    .collect(Collectors.toList());

        if (statusOption != null)
        {
            builder.where(eq(
                property(StudentStatus.active().fromAlias("studentStatus")),
                value(IStudentListModel.STUDENT_STATUS_ACTIVE.equals(statusOption.getId()))
            ));
        }
        if (lastName != null)
        {
            FilterUtils.applySimpleLikeFilter(builder, "s", Student.person().identityCard().lastName(), lastName);
        }
        if (firstName != null)
        {
            FilterUtils.applySimpleLikeFilter(builder, "s", Student.person().identityCard().firstName(), firstName);
        }
        if (middleName != null)
        {
            FilterUtils.applySimpleLikeFilter(builder, "s", Student.person().identityCard().middleName(), middleName);
        }
        if (null != oldLastName)
        {
            builder.joinEntity("p", DQLJoinType.inner, IdentityCard.class, "ic", and(eq(property("p", Person.id()), property("ic", IdentityCard.person().id())),
                    not(eq(property("ic", IdentityCard.id()), property("p", Person.identityCard().id())))));
            FilterUtils.applySimpleLikeFilter(builder, "ic", IdentityCard.lastName(), oldLastName);
        }
        if (personalNumber != null)
        {
            builder.where(like(
                    property(Student.personalNumber().fromAlias("s")),
                    value(CoreStringUtils.escapeLike(personalNumber))
            ));
        }

        FilterUtils.applySelectFilter(builder, "s", Student.P_PERSONAL_FILE_NUMBER, personalFileNumber);
        FilterUtils.applySelectFilter(builder, "s", Student.P_BOOK_NUMBER, bookNumber);
        FilterUtils.applySelectFilter(builder, "s", Student.P_ENTRANCE_YEAR, entranceYear);
        FilterUtils.applySelectFilter(builder, "s", Student.L_STATUS, studentStatusList);
        FilterUtils.applySelectFilter(builder, "s", Student.L_COURSE, courseList);
        FilterUtils.applySelectFilter(builder, "s", Student.group().title(), groupTitles);
        FilterUtils.applySelectFilter(builder, "s", Student.L_COMPENSATION_TYPE, compType);
        FilterUtils.applySelectFilter(builder, "s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), qualificationList);
        FilterUtils.applySelectFilter(builder, "s", Student.L_DEVELOP_PERIOD_AUTO, developPeriodAutoList);
        FilterUtils.applySelectFilter(builder, "s", Student.L_STUDENT_CATEGORY, studentCategoryList);

        if (specialPurposeRecruit != null)
        {
            builder.where(eq(
                property(Student.targetAdmission().fromAlias("s")),
                value(TwinComboDataSourceHandler.getSelectedValueNotNull(specialPurposeRecruit))
            ));
        }
        if (endingYear != null)
        {
            builder.where(eq(
                property(Student.finishYear().fromAlias("s")),
                value(endingYear.getId().intValue())
            ));
        }

        if (util != null)// т.к. конечный список НПП получаемый из утили фильтров НПП может быть слишком большой, то что бы не юзать BatchUtils и не доставать всех студентов - применяем фильтры по НПП отдельно
            util.applyFilters(builder, "ou");

        //применяем фильтр  по доп.статусам
        if (null != studentCustomStateCIList && !studentCustomStateCIList.isEmpty())
        {
            applyCustomStateFilter(studentCustomStateCIList, builder, context);
        }

        if (null != countryVO)
        {
            if (countryVO.getId().equals(0L))
            {
                builder.where(ne(property("s", Student.person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
            }
            else
            {
                builder.where(eq(property("s", Student.person().identityCard().citizenship().id()), value(countryVO.getAddressCountry().getId())));
            }
        }



        builder.column(property("s"));

        return builder;
    }


    /**
     * Применение фильтра по доп.статусам студентов
     * @param studentCustomStateCIList - список доп.статусов, которые должны попасть в выборку
     * @param builder - билдер
     */
    public void applyCustomStateFilter(List<StudentCustomStateCI> studentCustomStateCIList, DQLSelectBuilder builder, ExecutionContext context)
    {
        DQLSelectBuilder csDQL = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .column(property("st", StudentCustomState.student().id()))
                .where(eq(property("s"), property("st", StudentCustomState.student())))
                .where(in(property("st", StudentCustomState.customState()), studentCustomStateCIList));

        CustomStateUtil.applyActivePeriodFilter(csDQL, "st");

        builder.where(exists(csDQL.buildQuery()));
    }



    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = prepareDQL(context);

        IEducationOrgUnitContextHandler contextHandler = getContextHandler(context);
        if (contextHandler != null)
            contextHandler.setStudentListContext(builder, "s", "ou");

        addAdditionalRestrictions(builder, "s", input, context);

        _orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());
        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();

        prepareWrappData(output.getRecordIds(), input, context);
        for (DataWrapper student : DataWrapper.wrap(output))
            wrap(student, context);

        return output;
    }

    /**
     * Подготалвиваются данные для врапера.
     * @param recordIds список id студентов выводимые в списке
     */
    protected void prepareWrappData(List<Long> recordIds, DSInput input, ExecutionContext context)
    {
        final Map<Long, List<StudentCustomState>> studentCustomStateMap = SafeMap.get(ArrayList.class);

        for (List<Long> elements : Lists.partition(recordIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            final DQLSelectBuilder customStateBuilder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "cs")
                    .column(property("cs"))
                    .where(in(property(StudentCustomState.student().id().fromAlias("cs")), elements))
                    .fetchPath(DQLJoinType.inner, StudentCustomState.customState().fromAlias("cs"), "customState")
                    .order(property(StudentCustomState.customState().title().fromAlias("cs")));

            CustomStateUtil.applyActivePeriodFilter(customStateBuilder, "cs");

            for (final StudentCustomState customState : customStateBuilder.createStatement(context.getSession()).<StudentCustomState>list())
            {
                studentCustomStateMap.get(customState.getStudent().getId()).add(customState);
            }
        }

        context.put(STUDENT_ACTIVE_CUSTOM_STATE_MAP, studentCustomStateMap);
    }

    /**
     * Добавляет кастомные условия в выборку студентов.
     */
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {

    }

    protected IEducationOrgUnitContextHandler getContextHandler(ExecutionContext context)
    {
        return null;
    }

    /**
     * Оборачивает студетнов в списке в DataWrapper.
     */
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        Map<Long, List<StudentCustomState>> studentCustomStateMap = context.get(STUDENT_ACTIVE_CUSTOM_STATE_MAP);

        wrapper.setProperty(AbstractUniStudentList.STUDENT_ACTIVE_CUSTOM_STATE_COLUMN, studentCustomStateMap.get(wrapper.<Student>getWrapped().getId()));
    }
}
