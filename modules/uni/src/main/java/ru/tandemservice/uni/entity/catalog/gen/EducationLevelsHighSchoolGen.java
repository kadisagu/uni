package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параметры выпуска студентов по направлению подготовки (НПв)
 *
 * Направление подготовки ОУ, НПв.
 * Показывает, на каких кафедрах происходит выпуск по указанному НПм.
 * Важно, что НПв полностью соответствует НПм: его нельзя использовать для конкретизации.
 * Например, запрещено создавать профили одного и того же НПм с помощью создания нескольких НПв.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelsHighSchoolGen extends EntityBase
 implements INaturalIdentifiable<EducationLevelsHighSchoolGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool";
    public static final String ENTITY_NAME = "educationLevelsHighSchool";
    public static final int VERSION_HASH = 95400083;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_EDUCATION_LEVEL = "educationLevel";
    public static final String L_ASSIGNED_QUALIFICATION = "assignedQualification";
    public static final String L_SUBJECT_QUALIFICATION = "subjectQualification";
    public static final String L_PROGRAM_ORIENTATION = "programOrientation";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_FULL_TITLE_EXTENDED = "fullTitleExtended";
    public static final String P_PRINT_TITLE_WITHOUT_ORG_UNIT = "printTitleWithoutOrgUnit";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String P_ALLOW_STUDENTS = "allowStudents";
    public static final String P_HIGH_SCHOOL_CODE = "highSchoolCode";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_OPEN_DATE = "openDate";
    public static final String P_CLOSE_DATE = "closeDate";
    public static final String P_TITLE = "title";
    public static final String P_ALLOW_STUDENTS_DISABLED = "allowStudentsDisabled";
    public static final String P_CONFIGURABLE_TITLE = "configurableTitle";
    public static final String P_DISPLAYABLE_SHORT_TITLE = "displayableShortTitle";
    public static final String P_ORG_UNIT_QUANTITY = "orgUnitQuantity";
    public static final String P_QUALIFICATION_TITLE_NULL_SAFE = "qualificationTitleNullSafe";
    public static final String P_TITLE_WITH_PROFILE = "titleWithProfile";

    private String _code;     // Системный код
    private EducationLevels _educationLevel;     // НПм
    private EduProgramQualification _assignedQualification;     // Присваиваемая квалификация
    private EduProgramSubjectQualification _subjectQualification;     // Квалификации направления подготовки (для констрейнта)
    private EduProgramOrientation _programOrientation;     // Ориентация ОП
    private String _shortTitle;     // Сокращенное название
    private String _displayableTitle;     // Название
    private String _fullTitle;     // Полное название
    private String _fullTitleExtended;     // Расширенное название
    private String _printTitleWithoutOrgUnit;     // Печатное название без выпускающего подразделения
    private String _printTitle;     // Печатное название
    private boolean _allowStudents;     // Набор студентов
    private String _highSchoolCode;     // Внутривузовский шифр
    private OrgUnit _orgUnit;     // Выпускающее подр.
    private Date _openDate;     // Дата открытия
    private Date _closeDate;     // Дата закрытия
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return НПм. Свойство не может быть null.
     */
    @NotNull
    public EducationLevels getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel НПм. Свойство не может быть null.
     */
    public void setEducationLevel(EducationLevels educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    /**
     * @return Присваиваемая квалификация.
     */
    public EduProgramQualification getAssignedQualification()
    {
        return _assignedQualification;
    }

    /**
     * @param assignedQualification Присваиваемая квалификация.
     */
    public void setAssignedQualification(EduProgramQualification assignedQualification)
    {
        dirty(_assignedQualification, assignedQualification);
        _assignedQualification = assignedQualification;
    }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     *
     * @return Квалификации направления подготовки (для констрейнта).
     */
    public EduProgramSubjectQualification getSubjectQualification()
    {
        return _subjectQualification;
    }

    /**
     * @param subjectQualification Квалификации направления подготовки (для констрейнта).
     */
    public void setSubjectQualification(EduProgramSubjectQualification subjectQualification)
    {
        dirty(_subjectQualification, subjectQualification);
        _subjectQualification = subjectQualification;
    }

    /**
     * @return Ориентация ОП.
     */
    public EduProgramOrientation getProgramOrientation()
    {
        return _programOrientation;
    }

    /**
     * @param programOrientation Ориентация ОП.
     */
    public void setProgramOrientation(EduProgramOrientation programOrientation)
    {
        dirty(_programOrientation, programOrientation);
        _programOrientation = programOrientation;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 3)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Название.
     */
    @Length(max=255)
    public String getDisplayableTitle()
    {
        return _displayableTitle;
    }

    /**
     * @param displayableTitle Название.
     */
    public void setDisplayableTitle(String displayableTitle)
    {
        dirty(_displayableTitle, displayableTitle);
        _displayableTitle = displayableTitle;
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 4)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Полное название.
     */
    @Length(max=255)
    public String getFullTitle()
    {
        return _fullTitle;
    }

    /**
     * @param fullTitle Полное название.
     */
    public void setFullTitle(String fullTitle)
    {
        dirty(_fullTitle, fullTitle);
        _fullTitle = fullTitle;
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 6)
     * Поскольку данное название формируется из нескольких полей, то в пределе длина сгенерированной строки может достигать 600 символов.
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Расширенное название.
     */
    @Length(max=600)
    public String getFullTitleExtended()
    {
        return _fullTitleExtended;
    }

    /**
     * @param fullTitleExtended Расширенное название.
     */
    public void setFullTitleExtended(String fullTitleExtended)
    {
        dirty(_fullTitleExtended, fullTitleExtended);
        _fullTitleExtended = fullTitleExtended;
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 8)
     * дублирует вариант 5 "printTitle"  , но исключая из себя  orgunit
     *
     * @return Печатное название без выпускающего подразделения.
     */
    @Length(max=255)
    public String getPrintTitleWithoutOrgUnit()
    {
        return _printTitleWithoutOrgUnit;
    }

    /**
     * @param printTitleWithoutOrgUnit Печатное название без выпускающего подразделения.
     */
    public void setPrintTitleWithoutOrgUnit(String printTitleWithoutOrgUnit)
    {
        dirty(_printTitleWithoutOrgUnit, printTitleWithoutOrgUnit);
        _printTitleWithoutOrgUnit = printTitleWithoutOrgUnit;
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 5)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Печатное название.
     */
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Набор студентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowStudents()
    {
        return _allowStudents;
    }

    /**
     * @param allowStudents Набор студентов. Свойство не может быть null.
     */
    public void setAllowStudents(boolean allowStudents)
    {
        dirty(_allowStudents, allowStudents);
        _allowStudents = allowStudents;
    }

    /**
     * @return Внутривузовский шифр.
     */
    @Length(max=255)
    public String getHighSchoolCode()
    {
        initLazyForGet("highSchoolCode");
        return _highSchoolCode;
    }

    /**
     * @param highSchoolCode Внутривузовский шифр.
     */
    public void setHighSchoolCode(String highSchoolCode)
    {
        initLazyForSet("highSchoolCode");
        dirty(_highSchoolCode, highSchoolCode);
        _highSchoolCode = highSchoolCode;
    }

    /**
     * @return Выпускающее подр.. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Выпускающее подр.. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Дата открытия.
     */
    public Date getOpenDate()
    {
        initLazyForGet("openDate");
        return _openDate;
    }

    /**
     * @param openDate Дата открытия.
     */
    public void setOpenDate(Date openDate)
    {
        initLazyForSet("openDate");
        dirty(_openDate, openDate);
        _openDate = openDate;
    }

    /**
     * @return Дата закрытия.
     */
    public Date getCloseDate()
    {
        initLazyForGet("closeDate");
        return _closeDate;
    }

    /**
     * @param closeDate Дата закрытия.
     */
    public void setCloseDate(Date closeDate)
    {
        initLazyForSet("closeDate");
        dirty(_closeDate, closeDate);
        _closeDate = closeDate;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationLevelsHighSchoolGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EducationLevelsHighSchool)another).getCode());
            }
            setEducationLevel(((EducationLevelsHighSchool)another).getEducationLevel());
            setAssignedQualification(((EducationLevelsHighSchool)another).getAssignedQualification());
            setSubjectQualification(((EducationLevelsHighSchool)another).getSubjectQualification());
            setProgramOrientation(((EducationLevelsHighSchool)another).getProgramOrientation());
            setShortTitle(((EducationLevelsHighSchool)another).getShortTitle());
            setDisplayableTitle(((EducationLevelsHighSchool)another).getDisplayableTitle());
            setFullTitle(((EducationLevelsHighSchool)another).getFullTitle());
            setFullTitleExtended(((EducationLevelsHighSchool)another).getFullTitleExtended());
            setPrintTitleWithoutOrgUnit(((EducationLevelsHighSchool)another).getPrintTitleWithoutOrgUnit());
            setPrintTitle(((EducationLevelsHighSchool)another).getPrintTitle());
            setAllowStudents(((EducationLevelsHighSchool)another).isAllowStudents());
            setHighSchoolCode(((EducationLevelsHighSchool)another).getHighSchoolCode());
            setOrgUnit(((EducationLevelsHighSchool)another).getOrgUnit());
            setOpenDate(((EducationLevelsHighSchool)another).getOpenDate());
            setCloseDate(((EducationLevelsHighSchool)another).getCloseDate());
            setTitle(((EducationLevelsHighSchool)another).getTitle());
        }
    }

    public INaturalId<EducationLevelsHighSchoolGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EducationLevelsHighSchoolGen>
    {
        private static final String PROXY_NAME = "EducationLevelsHighSchoolNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EducationLevelsHighSchoolGen.NaturalId) ) return false;

            EducationLevelsHighSchoolGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelsHighSchoolGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevelsHighSchool.class;
        }

        public T newInstance()
        {
            return (T) new EducationLevelsHighSchool();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "educationLevel":
                    return obj.getEducationLevel();
                case "assignedQualification":
                    return obj.getAssignedQualification();
                case "subjectQualification":
                    return obj.getSubjectQualification();
                case "programOrientation":
                    return obj.getProgramOrientation();
                case "shortTitle":
                    return obj.getShortTitle();
                case "displayableTitle":
                    return obj.getDisplayableTitle();
                case "fullTitle":
                    return obj.getFullTitle();
                case "fullTitleExtended":
                    return obj.getFullTitleExtended();
                case "printTitleWithoutOrgUnit":
                    return obj.getPrintTitleWithoutOrgUnit();
                case "printTitle":
                    return obj.getPrintTitle();
                case "allowStudents":
                    return obj.isAllowStudents();
                case "highSchoolCode":
                    return obj.getHighSchoolCode();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "openDate":
                    return obj.getOpenDate();
                case "closeDate":
                    return obj.getCloseDate();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((EducationLevels) value);
                    return;
                case "assignedQualification":
                    obj.setAssignedQualification((EduProgramQualification) value);
                    return;
                case "subjectQualification":
                    obj.setSubjectQualification((EduProgramSubjectQualification) value);
                    return;
                case "programOrientation":
                    obj.setProgramOrientation((EduProgramOrientation) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "displayableTitle":
                    obj.setDisplayableTitle((String) value);
                    return;
                case "fullTitle":
                    obj.setFullTitle((String) value);
                    return;
                case "fullTitleExtended":
                    obj.setFullTitleExtended((String) value);
                    return;
                case "printTitleWithoutOrgUnit":
                    obj.setPrintTitleWithoutOrgUnit((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "allowStudents":
                    obj.setAllowStudents((Boolean) value);
                    return;
                case "highSchoolCode":
                    obj.setHighSchoolCode((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "openDate":
                    obj.setOpenDate((Date) value);
                    return;
                case "closeDate":
                    obj.setCloseDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "educationLevel":
                        return true;
                case "assignedQualification":
                        return true;
                case "subjectQualification":
                        return true;
                case "programOrientation":
                        return true;
                case "shortTitle":
                        return true;
                case "displayableTitle":
                        return true;
                case "fullTitle":
                        return true;
                case "fullTitleExtended":
                        return true;
                case "printTitleWithoutOrgUnit":
                        return true;
                case "printTitle":
                        return true;
                case "allowStudents":
                        return true;
                case "highSchoolCode":
                        return true;
                case "orgUnit":
                        return true;
                case "openDate":
                        return true;
                case "closeDate":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "educationLevel":
                    return true;
                case "assignedQualification":
                    return true;
                case "subjectQualification":
                    return true;
                case "programOrientation":
                    return true;
                case "shortTitle":
                    return true;
                case "displayableTitle":
                    return true;
                case "fullTitle":
                    return true;
                case "fullTitleExtended":
                    return true;
                case "printTitleWithoutOrgUnit":
                    return true;
                case "printTitle":
                    return true;
                case "allowStudents":
                    return true;
                case "highSchoolCode":
                    return true;
                case "orgUnit":
                    return true;
                case "openDate":
                    return true;
                case "closeDate":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "educationLevel":
                    return EducationLevels.class;
                case "assignedQualification":
                    return EduProgramQualification.class;
                case "subjectQualification":
                    return EduProgramSubjectQualification.class;
                case "programOrientation":
                    return EduProgramOrientation.class;
                case "shortTitle":
                    return String.class;
                case "displayableTitle":
                    return String.class;
                case "fullTitle":
                    return String.class;
                case "fullTitleExtended":
                    return String.class;
                case "printTitleWithoutOrgUnit":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "allowStudents":
                    return Boolean.class;
                case "highSchoolCode":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "openDate":
                    return Date.class;
                case "closeDate":
                    return Date.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationLevelsHighSchool> _dslPath = new Path<EducationLevelsHighSchool>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevelsHighSchool");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return НПм. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getEducationLevel()
     */
    public static EducationLevels.Path<EducationLevels> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    /**
     * @return Присваиваемая квалификация.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getAssignedQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> assignedQualification()
    {
        return _dslPath.assignedQualification();
    }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     *
     * @return Квалификации направления подготовки (для констрейнта).
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getSubjectQualification()
     */
    public static EduProgramSubjectQualification.Path<EduProgramSubjectQualification> subjectQualification()
    {
        return _dslPath.subjectQualification();
    }

    /**
     * @return Ориентация ОП.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getProgramOrientation()
     */
    public static EduProgramOrientation.Path<EduProgramOrientation> programOrientation()
    {
        return _dslPath.programOrientation();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 3)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getDisplayableTitle()
     */
    public static PropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 4)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Полное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getFullTitle()
     */
    public static PropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 6)
     * Поскольку данное название формируется из нескольких полей, то в пределе длина сгенерированной строки может достигать 600 символов.
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Расширенное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getFullTitleExtended()
     */
    public static PropertyPath<String> fullTitleExtended()
    {
        return _dslPath.fullTitleExtended();
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 8)
     * дублирует вариант 5 "printTitle"  , но исключая из себя  orgunit
     *
     * @return Печатное название без выпускающего подразделения.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getPrintTitleWithoutOrgUnit()
     */
    public static PropertyPath<String> printTitleWithoutOrgUnit()
    {
        return _dslPath.printTitleWithoutOrgUnit();
    }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 5)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Печатное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Набор студентов. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#isAllowStudents()
     */
    public static PropertyPath<Boolean> allowStudents()
    {
        return _dslPath.allowStudents();
    }

    /**
     * @return Внутривузовский шифр.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getHighSchoolCode()
     */
    public static PropertyPath<String> highSchoolCode()
    {
        return _dslPath.highSchoolCode();
    }

    /**
     * @return Выпускающее подр.. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Дата открытия.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getOpenDate()
     */
    public static PropertyPath<Date> openDate()
    {
        return _dslPath.openDate();
    }

    /**
     * @return Дата закрытия.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getCloseDate()
     */
    public static PropertyPath<Date> closeDate()
    {
        return _dslPath.closeDate();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#isAllowStudentsDisabled()
     */
    public static SupportedPropertyPath<Boolean> allowStudentsDisabled()
    {
        return _dslPath.allowStudentsDisabled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getConfigurableTitle()
     */
    public static SupportedPropertyPath<String> configurableTitle()
    {
        return _dslPath.configurableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getDisplayableShortTitle()
     */
    public static SupportedPropertyPath<String> displayableShortTitle()
    {
        return _dslPath.displayableShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getOrgUnitQuantity()
     */
    public static SupportedPropertyPath<Integer> orgUnitQuantity()
    {
        return _dslPath.orgUnitQuantity();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getQualificationTitleNullSafe()
     */
    public static SupportedPropertyPath<String> qualificationTitleNullSafe()
    {
        return _dslPath.qualificationTitleNullSafe();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getTitleWithProfile()
     */
    public static SupportedPropertyPath<String> titleWithProfile()
    {
        return _dslPath.titleWithProfile();
    }

    public static class Path<E extends EducationLevelsHighSchool> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EducationLevels.Path<EducationLevels> _educationLevel;
        private EduProgramQualification.Path<EduProgramQualification> _assignedQualification;
        private EduProgramSubjectQualification.Path<EduProgramSubjectQualification> _subjectQualification;
        private EduProgramOrientation.Path<EduProgramOrientation> _programOrientation;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _displayableTitle;
        private PropertyPath<String> _fullTitle;
        private PropertyPath<String> _fullTitleExtended;
        private PropertyPath<String> _printTitleWithoutOrgUnit;
        private PropertyPath<String> _printTitle;
        private PropertyPath<Boolean> _allowStudents;
        private PropertyPath<String> _highSchoolCode;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _openDate;
        private PropertyPath<Date> _closeDate;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<Boolean> _allowStudentsDisabled;
        private SupportedPropertyPath<String> _configurableTitle;
        private SupportedPropertyPath<String> _displayableShortTitle;
        private SupportedPropertyPath<Integer> _orgUnitQuantity;
        private SupportedPropertyPath<String> _qualificationTitleNullSafe;
        private SupportedPropertyPath<String> _titleWithProfile;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_CODE, this);
            return _code;
        }

    /**
     * @return НПм. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getEducationLevel()
     */
        public EducationLevels.Path<EducationLevels> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new EducationLevels.Path<EducationLevels>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

    /**
     * @return Присваиваемая квалификация.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getAssignedQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> assignedQualification()
        {
            if(_assignedQualification == null )
                _assignedQualification = new EduProgramQualification.Path<EduProgramQualification>(L_ASSIGNED_QUALIFICATION, this);
            return _assignedQualification;
        }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     *
     * @return Квалификации направления подготовки (для констрейнта).
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getSubjectQualification()
     */
        public EduProgramSubjectQualification.Path<EduProgramSubjectQualification> subjectQualification()
        {
            if(_subjectQualification == null )
                _subjectQualification = new EduProgramSubjectQualification.Path<EduProgramSubjectQualification>(L_SUBJECT_QUALIFICATION, this);
            return _subjectQualification;
        }

    /**
     * @return Ориентация ОП.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getProgramOrientation()
     */
        public EduProgramOrientation.Path<EduProgramOrientation> programOrientation()
        {
            if(_programOrientation == null )
                _programOrientation = new EduProgramOrientation.Path<EduProgramOrientation>(L_PROGRAM_ORIENTATION, this);
            return _programOrientation;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 3)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getDisplayableTitle()
     */
        public PropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 4)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Полное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getFullTitle()
     */
        public PropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 6)
     * Поскольку данное название формируется из нескольких полей, то в пределе длина сгенерированной строки может достигать 600 символов.
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Расширенное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getFullTitleExtended()
     */
        public PropertyPath<String> fullTitleExtended()
        {
            if(_fullTitleExtended == null )
                _fullTitleExtended = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_FULL_TITLE_EXTENDED, this);
            return _fullTitleExtended;
        }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 8)
     * дублирует вариант 5 "printTitle"  , но исключая из себя  orgunit
     *
     * @return Печатное название без выпускающего подразделения.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getPrintTitleWithoutOrgUnit()
     */
        public PropertyPath<String> printTitleWithoutOrgUnit()
        {
            if(_printTitleWithoutOrgUnit == null )
                _printTitleWithoutOrgUnit = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_PRINT_TITLE_WITHOUT_ORG_UNIT, this);
            return _printTitleWithoutOrgUnit;
        }

    /**
     * @wiki http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1064219 (Вариант 5)
     * формируется в ru.tandemservice.uni.events.dset.EduLevelHSDisplayableTitleSync
     *
     * @return Печатное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Набор студентов. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#isAllowStudents()
     */
        public PropertyPath<Boolean> allowStudents()
        {
            if(_allowStudents == null )
                _allowStudents = new PropertyPath<Boolean>(EducationLevelsHighSchoolGen.P_ALLOW_STUDENTS, this);
            return _allowStudents;
        }

    /**
     * @return Внутривузовский шифр.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getHighSchoolCode()
     */
        public PropertyPath<String> highSchoolCode()
        {
            if(_highSchoolCode == null )
                _highSchoolCode = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_HIGH_SCHOOL_CODE, this);
            return _highSchoolCode;
        }

    /**
     * @return Выпускающее подр.. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Дата открытия.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getOpenDate()
     */
        public PropertyPath<Date> openDate()
        {
            if(_openDate == null )
                _openDate = new PropertyPath<Date>(EducationLevelsHighSchoolGen.P_OPEN_DATE, this);
            return _openDate;
        }

    /**
     * @return Дата закрытия.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getCloseDate()
     */
        public PropertyPath<Date> closeDate()
        {
            if(_closeDate == null )
                _closeDate = new PropertyPath<Date>(EducationLevelsHighSchoolGen.P_CLOSE_DATE, this);
            return _closeDate;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EducationLevelsHighSchoolGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#isAllowStudentsDisabled()
     */
        public SupportedPropertyPath<Boolean> allowStudentsDisabled()
        {
            if(_allowStudentsDisabled == null )
                _allowStudentsDisabled = new SupportedPropertyPath<Boolean>(EducationLevelsHighSchoolGen.P_ALLOW_STUDENTS_DISABLED, this);
            return _allowStudentsDisabled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getConfigurableTitle()
     */
        public SupportedPropertyPath<String> configurableTitle()
        {
            if(_configurableTitle == null )
                _configurableTitle = new SupportedPropertyPath<String>(EducationLevelsHighSchoolGen.P_CONFIGURABLE_TITLE, this);
            return _configurableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getDisplayableShortTitle()
     */
        public SupportedPropertyPath<String> displayableShortTitle()
        {
            if(_displayableShortTitle == null )
                _displayableShortTitle = new SupportedPropertyPath<String>(EducationLevelsHighSchoolGen.P_DISPLAYABLE_SHORT_TITLE, this);
            return _displayableShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getOrgUnitQuantity()
     */
        public SupportedPropertyPath<Integer> orgUnitQuantity()
        {
            if(_orgUnitQuantity == null )
                _orgUnitQuantity = new SupportedPropertyPath<Integer>(EducationLevelsHighSchoolGen.P_ORG_UNIT_QUANTITY, this);
            return _orgUnitQuantity;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getQualificationTitleNullSafe()
     */
        public SupportedPropertyPath<String> qualificationTitleNullSafe()
        {
            if(_qualificationTitleNullSafe == null )
                _qualificationTitleNullSafe = new SupportedPropertyPath<String>(EducationLevelsHighSchoolGen.P_QUALIFICATION_TITLE_NULL_SAFE, this);
            return _qualificationTitleNullSafe;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool#getTitleWithProfile()
     */
        public SupportedPropertyPath<String> titleWithProfile()
        {
            if(_titleWithProfile == null )
                _titleWithProfile = new SupportedPropertyPath<String>(EducationLevelsHighSchoolGen.P_TITLE_WITH_PROFILE, this);
            return _titleWithProfile;
        }

        public Class getEntityClass()
        {
            return EducationLevelsHighSchool.class;
        }

        public String getEntityName()
        {
            return "educationLevelsHighSchool";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isAllowStudentsDisabled();

    public abstract String getConfigurableTitle();

    public abstract String getDisplayableShortTitle();

    public abstract Integer getOrgUnitQuantity();

    public abstract String getQualificationTitleNullSafe();

    public abstract String getTitleWithProfile();
}
