/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.ui.EditEduOrgUnitOwner;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniReorganization.UniReorganizationManager;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */

@Input({
        @Bind(key = "singleValue", binding = "entity"),
        @Bind(key = "multiValue", binding = "entities")
})
public class UniReorganizationEditEduOrgUnitOwnerUI extends UIPresenter {

    private List<EducationOrgUnit> entities;
    private Long entity;
    private OrgUnit owner;

    @Override
    public void onComponentActivate() {
        super.onComponentActivate();
    }

    public void onClickApply()
    {
        List<EducationOrgUnit> output = new ArrayList<>();
        if (null != getEntities())
            output = getEntities();
        else if (null != getEntity())
            output.add(DataAccessServices.dao().get(getEntity()));
        UniReorganizationManager.instance().dao().doChangeEducationOrgUnitOwner(output, owner);
        deactivate();
    }

    public List<EducationOrgUnit> getEntities() {
        return entities;
    }

    public void setEntities(List<EducationOrgUnit> entities) {
        this.entities = entities;
    }

    public Long getEntity() {
        return entity;
    }

    public void setEntity(Long entity) {
        this.entity = entity;
    }

    public OrgUnit getOwner() {
        return owner;
    }

    public void setOwner(OrgUnit owner) {
        this.owner = owner;
    }
}