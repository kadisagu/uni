package ru.tandemservice.uni.entity.util.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.util.EducationOrgUnitSetting;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка «Параметры направления подготовки (специальности) с указанием ФУТС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationOrgUnitSettingGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.util.EducationOrgUnitSetting";
    public static final String ENTITY_NAME = "educationOrgUnitSetting";
    public static final int VERSION_HASH = -931169906;
    private static IEntityMeta ENTITY_META;

    public static final String P_USE_SHORT_TITLE = "useShortTitle";
    public static final String P_USE_INTERNAL_CODE = "useInternalCode";

    private boolean _useShortTitle;     // Использовать сокращенное название
    private boolean _useInternalCode;     // Использовать внутренний код

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Использовать сокращенное название. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseShortTitle()
    {
        return _useShortTitle;
    }

    /**
     * @param useShortTitle Использовать сокращенное название. Свойство не может быть null.
     */
    public void setUseShortTitle(boolean useShortTitle)
    {
        dirty(_useShortTitle, useShortTitle);
        _useShortTitle = useShortTitle;
    }

    /**
     * @return Использовать внутренний код. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseInternalCode()
    {
        return _useInternalCode;
    }

    /**
     * @param useInternalCode Использовать внутренний код. Свойство не может быть null.
     */
    public void setUseInternalCode(boolean useInternalCode)
    {
        dirty(_useInternalCode, useInternalCode);
        _useInternalCode = useInternalCode;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationOrgUnitSettingGen)
        {
            setUseShortTitle(((EducationOrgUnitSetting)another).isUseShortTitle());
            setUseInternalCode(((EducationOrgUnitSetting)another).isUseInternalCode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationOrgUnitSettingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationOrgUnitSetting.class;
        }

        public T newInstance()
        {
            return (T) new EducationOrgUnitSetting();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "useShortTitle":
                    return obj.isUseShortTitle();
                case "useInternalCode":
                    return obj.isUseInternalCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "useShortTitle":
                    obj.setUseShortTitle((Boolean) value);
                    return;
                case "useInternalCode":
                    obj.setUseInternalCode((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "useShortTitle":
                        return true;
                case "useInternalCode":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "useShortTitle":
                    return true;
                case "useInternalCode":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "useShortTitle":
                    return Boolean.class;
                case "useInternalCode":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationOrgUnitSetting> _dslPath = new Path<EducationOrgUnitSetting>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationOrgUnitSetting");
    }
            

    /**
     * @return Использовать сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.EducationOrgUnitSetting#isUseShortTitle()
     */
    public static PropertyPath<Boolean> useShortTitle()
    {
        return _dslPath.useShortTitle();
    }

    /**
     * @return Использовать внутренний код. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.EducationOrgUnitSetting#isUseInternalCode()
     */
    public static PropertyPath<Boolean> useInternalCode()
    {
        return _dslPath.useInternalCode();
    }

    public static class Path<E extends EducationOrgUnitSetting> extends EntityPath<E>
    {
        private PropertyPath<Boolean> _useShortTitle;
        private PropertyPath<Boolean> _useInternalCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Использовать сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.EducationOrgUnitSetting#isUseShortTitle()
     */
        public PropertyPath<Boolean> useShortTitle()
        {
            if(_useShortTitle == null )
                _useShortTitle = new PropertyPath<Boolean>(EducationOrgUnitSettingGen.P_USE_SHORT_TITLE, this);
            return _useShortTitle;
        }

    /**
     * @return Использовать внутренний код. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.EducationOrgUnitSetting#isUseInternalCode()
     */
        public PropertyPath<Boolean> useInternalCode()
        {
            if(_useInternalCode == null )
                _useInternalCode = new PropertyPath<Boolean>(EducationOrgUnitSettingGen.P_USE_INTERNAL_CODE, this);
            return _useInternalCode;
        }

        public Class getEntityClass()
        {
            return EducationOrgUnitSetting.class;
        }

        public String getEntityName()
        {
            return "educationOrgUnitSetting";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
