package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебная сетка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DevelopGridGen extends EntityBase
 implements INaturalIdentifiable<DevelopGridGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.DevelopGrid";
    public static final String ENTITY_NAME = "developGrid";
    public static final int VERSION_HASH = -930006419;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_DEVELOP_PERIOD = "developPeriod";
    public static final String P_APPROVED = "approved";
    public static final String P_META = "meta";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private DevelopPeriod _developPeriod;     // Срок освоения
    private boolean _approved;     // Зафиксирована
    private String _meta;     // Мета-описание
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения. Свойство не может быть null.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Зафиксирована. Свойство не может быть null.
     */
    @NotNull
    public boolean isApproved()
    {
        return _approved;
    }

    /**
     * @param approved Зафиксирована. Свойство не может быть null.
     */
    public void setApproved(boolean approved)
    {
        dirty(_approved, approved);
        _approved = approved;
    }

    /**
     * @return Мета-описание.
     */
    @Length(max=255)
    public String getMeta()
    {
        return _meta;
    }

    /**
     * @param meta Мета-описание.
     */
    public void setMeta(String meta)
    {
        dirty(_meta, meta);
        _meta = meta;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DevelopGridGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((DevelopGrid)another).getCode());
            }
            setDevelopPeriod(((DevelopGrid)another).getDevelopPeriod());
            setApproved(((DevelopGrid)another).isApproved());
            setMeta(((DevelopGrid)another).getMeta());
            setTitle(((DevelopGrid)another).getTitle());
        }
    }

    public INaturalId<DevelopGridGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<DevelopGridGen>
    {
        private static final String PROXY_NAME = "DevelopGridNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DevelopGridGen.NaturalId) ) return false;

            DevelopGridGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DevelopGridGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DevelopGrid.class;
        }

        public T newInstance()
        {
            return (T) new DevelopGrid();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "approved":
                    return obj.isApproved();
                case "meta":
                    return obj.getMeta();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
                case "approved":
                    obj.setApproved((Boolean) value);
                    return;
                case "meta":
                    obj.setMeta((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "developPeriod":
                        return true;
                case "approved":
                        return true;
                case "meta":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "developPeriod":
                    return true;
                case "approved":
                    return true;
                case "meta":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "developPeriod":
                    return DevelopPeriod.class;
                case "approved":
                    return Boolean.class;
                case "meta":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DevelopGrid> _dslPath = new Path<DevelopGrid>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DevelopGrid");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Зафиксирована. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#isApproved()
     */
    public static PropertyPath<Boolean> approved()
    {
        return _dslPath.approved();
    }

    /**
     * @return Мета-описание.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getMeta()
     */
    public static PropertyPath<String> meta()
    {
        return _dslPath.meta();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends DevelopGrid> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;
        private PropertyPath<Boolean> _approved;
        private PropertyPath<String> _meta;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(DevelopGridGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Зафиксирована. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#isApproved()
     */
        public PropertyPath<Boolean> approved()
        {
            if(_approved == null )
                _approved = new PropertyPath<Boolean>(DevelopGridGen.P_APPROVED, this);
            return _approved;
        }

    /**
     * @return Мета-описание.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getMeta()
     */
        public PropertyPath<String> meta()
        {
            if(_meta == null )
                _meta = new PropertyPath<String>(DevelopGridGen.P_META, this);
            return _meta;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.DevelopGrid#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(DevelopGridGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return DevelopGrid.class;
        }

        public String getEntityName()
        {
            return "developGrid";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
