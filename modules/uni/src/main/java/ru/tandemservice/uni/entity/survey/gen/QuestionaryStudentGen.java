package ru.tandemservice.uni.entity.survey.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.survey.base.entity.BaseQuestionary;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.survey.QuestionaryStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Анкета студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class QuestionaryStudentGen extends BaseQuestionary
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.survey.QuestionaryStudent";
    public static final String ENTITY_NAME = "questionaryStudent";
    public static final int VERSION_HASH = -839336304;
    private static IEntityMeta ENTITY_META;

    public static final String L_OBJECT = "object";

    private Student _object;     // Объект анкетирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Объект анкетирования.
     */
    public Student getObject()
    {
        return _object;
    }

    /**
     * @param object Объект анкетирования.
     */
    public void setObject(Student object)
    {
        dirty(_object, object);
        _object = object;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof QuestionaryStudentGen)
        {
            setObject(((QuestionaryStudent)another).getObject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends QuestionaryStudentGen> extends BaseQuestionary.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) QuestionaryStudent.class;
        }

        public T newInstance()
        {
            return (T) new QuestionaryStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "object":
                    return obj.getObject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "object":
                    obj.setObject((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "object":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "object":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "object":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<QuestionaryStudent> _dslPath = new Path<QuestionaryStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "QuestionaryStudent");
    }
            

    /**
     * @return Объект анкетирования.
     * @see ru.tandemservice.uni.entity.survey.QuestionaryStudent#getObject()
     */
    public static Student.Path<Student> object()
    {
        return _dslPath.object();
    }

    public static class Path<E extends QuestionaryStudent> extends BaseQuestionary.Path<E>
    {
        private Student.Path<Student> _object;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Объект анкетирования.
     * @see ru.tandemservice.uni.entity.survey.QuestionaryStudent#getObject()
     */
        public Student.Path<Student> object()
        {
            if(_object == null )
                _object = new Student.Path<Student>(L_OBJECT, this);
            return _object;
        }

        public Class getEntityClass()
        {
            return QuestionaryStudent.class;
        }

        public String getEntityName()
        {
            return "questionaryStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
