package ru.tandemservice.uni.component.settings.CombinationsFormsAndConditions;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.DevelopCombination;

/**
 * 
 * @author nkokorina
 * @since 01.03.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("dc");

    static
    {
        _orderSettings.setOrders(DevelopCombination.title(),
                                 new OrderDescription(DevelopCombination.developForm().shortTitle()),
                                 new OrderDescription(DevelopCombination.developCondition().shortTitle()),
                                 new OrderDescription(DevelopCombination.developTech().shortTitle()),
                                 new OrderDescription(DevelopCombination.developGrid().title())
        );
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(DevelopCombination.ENTITY_CLASS, "dc");
        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
