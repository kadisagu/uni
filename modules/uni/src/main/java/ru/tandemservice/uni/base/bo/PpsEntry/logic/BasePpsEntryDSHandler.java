/* $Id$ */
package ru.tandemservice.uni.base.bo.PpsEntry.logic;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

/**
 * @author Alexey Lopatin
 * @since 20.06.2016
 */
public class BasePpsEntryDSHandler extends EntityComboDataSourceHandler
{
    public BasePpsEntryDSHandler(String ownerId)
    {
        super(ownerId, PpsEntry.class);
        filter(PpsEntry.person().identityCard().fullFio());
        order(PpsEntry.person().identityCard().fullFio());
        order(PpsEntry.orgUnit().title());
        order(PpsEntry.id());
    }
}
