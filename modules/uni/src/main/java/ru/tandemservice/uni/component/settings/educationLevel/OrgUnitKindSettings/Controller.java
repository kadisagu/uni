/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitKindSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;

import ru.tandemservice.uni.entity.catalog.OrgUnitKind;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        prepareListDataSource(context);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<OrgUnitKind> dataSource = new DynamicListDataSource<>(component, this, 20);
        dataSource.addColumn(new SimpleColumn("Название", OrgUnitKind.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Отображать перечень студентов", OrgUnitKind.allowStudents().s()).setListener("onClickAllowStudents"));
        dataSource.addColumn(new ToggleColumn("Отображать перечень групп", OrgUnitKind.allowGroups().s()).setListener("onClickAllowGroups"));
        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    public void onClickAllowStudents(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.setOrgUnitKindId((Long)context.getListenerParameter());
        getDao().update(model);
    }

    public void onClickAllowGroups(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.setOrgUnitKindId((Long)context.getListenerParameter());
        getDao().updateAllowGroups(model);
    }
}
