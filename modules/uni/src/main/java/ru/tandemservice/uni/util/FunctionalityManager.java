/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author agolubenko
 * @since Jun 15, 2010
 */
public class FunctionalityManager
{
    public static final String PERSON_MILITARY_KEY = "person.military.enabled";

    public static boolean isPersonMilitaryEnabled()
    {
        return Boolean.parseBoolean(ApplicationRuntime.getProperty(PERSON_MILITARY_KEY));
    }
}
