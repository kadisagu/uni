package ru.tandemservice.uni.base.bo.UniOrgUnit.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 03.02.2011
 * @deprecated use UniOrgUnitManager.formativeOrgUnitComboDSHandler и UniOrgUnitManager.territorialOrgUnitComboDSHandler
 */
public class OrgUnitByKindComboDataSourceHandler extends DefaultComboDataSourceHandler
{
    private String _orgUnitKind;

    public OrgUnitByKindComboDataSourceHandler(String ownerId, String orgUnitKind)
    {
        super(ownerId, OrgUnit.class, orgUnitKind.equals(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL) ? OrgUnit.P_TERRITORIAL_FULL_TITLE : OrgUnit.P_FULL_TITLE);

        _orgUnitKind = orgUnitKind;
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);

        // Пока есть 2 разные настройки для выпускающих подразделений, делаем поиск в обеих.
        // Но одну из них всё равно надо выпиливать, чтобы не взрывать мозги бедным пользователям.
        IDQLExpression kindExpr = exists(OrgUnitToKindRelation.class,
                                         OrgUnitToKindRelation.orgUnit().s(), property("e"),
                                         OrgUnitToKindRelation.orgUnitKind().code().s(), _orgUnitKind);

        if (UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING.equals(_orgUnitKind)) {
            kindExpr = or(kindExpr, exists(EduOwnerOrgUnit.class, EduOwnerOrgUnit.orgUnit().s(), property("e")));
        }

        ep.dqlBuilder.where(eq(property("e", OrgUnit.archival()), value(false)));
        ep.dqlBuilder.where(kindExpr);
    }
}
