/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.entity.catalog;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.UniEduProgramRules;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsGen;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

/**
 * Направление подготовки министерского классификатора (НПм)
 */
public abstract class EducationLevels extends EducationLevelsGen implements ICatalogItem
{
    @Override
    @EntityDSLSupport(parts = P_TITLE)
    public String getFullTitleWithNewData() {
        StringBuilder b = new StringBuilder()
            .append(getFullTitle())
            .append(" [нов. напр-ие: ")
            .append(getEduProgramSubject() == null ? "-" : getEduProgramSubject().getTitleWithCode())
            .append(", нов. напр-ть: ")
            .append(getEduProgramSpecialization() == null ? "-" : getEduProgramSpecialization().getDisplayableTitle())
            .append("]");
        return b.toString();
    }

    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    public String getFullTitle()
    {
        return getDisplayableTitle() + " (" + getLevelType().getTitle() + ")";
    }

    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    public String getFullTitleWithRootLevel()
    {
        return getDisplayableTitle() + " (" + getLevelTitleWithRootLevel() + ")";
    }

    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    public String getDisplayableTitle()
    {
        return getTitleCodePrefix() + " " + getTitle();
    }

    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    public String getDisplayableShortTitle()
    {
        return getTitleCodePrefix() + (null != getShortTitle() ? " " + getShortTitle() : "");
    }

    public String getLevelTitleWithRootLevel() {

        if (getEduProgramSubject() != null)
            return getLevelType().getTitle() + ", " + getEduProgramSubject().getSubjectIndex().getEduLevelAndIndexNotation();

        return getLevelType().getTitle();
    }

    /**
     * @return Префикс с кодом перед названием (для старых элементов - это ОКСО.КВАЛИФИКАЦИЯ)
     */
    @Override
    @EntityDSLSupport
    @SuppressWarnings("deprecation")
    public String getTitleCodePrefix()
    {
        // для связанных направлений - используется код направления из связи
        EduProgramSubject eduProgramSubject = getEduProgramSubject();
        if (null != eduProgramSubject)
        {
            // если сопоставление указано, то пробуем получить код из сопоставления
            String titleCodePrefix = UniEduProgramRules.instance().getProgramSubjectTitleCodePrefix(this);
            if (null != titleCodePrefix) { return titleCodePrefix; }

            // иначе - по старинке
            Qualifications qualification = getQualification();
            if (null == qualification) { return eduProgramSubject.getSubjectCode(); }
            return eduProgramSubject.getSubjectCode() + "." + qualification.getCode();
        }

        // здесь она используется правильно
        return getInheritedOksoPrefix();
    }

    /**
     * Сортировать по формату нельзя - ДПО отвалится.
     * @return название направления подготовки с кодом. Для ДПО - просто название
     */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    @EntityDSLSupport
    public String getProgramSubjectTitleWithCode() {
        final EduProgramSubject subj = getEduProgramSubject();
        return subj != null ? subj.getTitleWithCode() : "?";
    }

    /**
     * Сортировать по формату нельзя - ДПО отвалится.
     * @return название направления подготовки с кодом, типом перечня и поколением. Для ДПО - просто название. Если нет направления проф. образования - "?"
     */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    @EntityDSLSupport
    public String getProgramSubjectWithCodeIndexAndGenTitle() {
        final EduProgramSubject subj = getEduProgramSubject();
        return subj != null ? subj.getTitleWithCodeIndexAndGen() : "?";
    }

    /**
     * @return название направленности
     */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    @EntityDSLSupport(parts = EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE)
    public String getProgramSpecializationTitle() {
        final EduProgramSpecialization specialization = getEduProgramSpecialization();
        return specialization != null ? specialization.getDisplayableTitle() : StringUtils.EMPTY;
    }

    /**
     * @return название направленности, если она не общая. Если общая или нет вообще - пустая строка.
     */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    @EntityDSLSupport(parts = EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE)
    public String getProgramSpecializationTitleIfChild() {
        final EduProgramSpecialization specialization = getEduProgramSpecialization();
        return specialization != null && !specialization.isRootSpecialization() ? specialization.getDisplayableTitle() : StringUtils.EMPTY;
    }

    /**
     * @deprecated use getTitleCodePrefix, заменить в рамках DEV-4281
     */
    @Deprecated
    @Override
    @EntityDSLSupport
    @SuppressWarnings("deprecation")
    public String getInheritedOksoPrefix()
    {
        String okso = getInheritedOkso();
        Qualifications qualification = getQualification();

        if (StringUtils.isEmpty(okso)) {
            return qualification == null ? "" : qualification.getCode();
        } else {
            return qualification == null ? okso : (okso + "." + qualification.getCode());
        }
    }

    /**
     * @deprecated use getInheritedOkso - там где нужно именно ОКСО / getTitleCodePrefix - там, где нужен код перед названием, заменить в рамках DEV-4281
     */
    @Deprecated
    @Override
    public String getOkso() {
        return super.getOkso();
    }

    /**
     * @deprecated заменить на getTitleCodePrefix где это возможно, заменить в рамках DEV-4281
     */
    @Deprecated
    @Override
    public String getInheritedOkso() {
        return super.getInheritedOkso();
    }

    /* используется только в UniSyncDao */
    @SuppressWarnings("deprecation")
    public String buildInheritedOksoNonPersistent()
    {
        String okso = getOkso();
        if (StringUtils.isNotEmpty(okso))
            return okso;
        else
            return getParentLevel() == null ? null : getParentLevel().buildInheritedOksoNonPersistent();
    }


    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2721317")
    public String getQualificationTitle()
    {
        Qualifications q = getQualification();
        return null == q ? "" : q.getTitle();
    }

    public String getSafeQCode()
    {
        return getQualification() == null ? null : getQualification().getCode();
    }

    public boolean isQualificationRequired()
    {
        return null != getLevelType() && getLevelType().isQualificationRequired();
    }

    public boolean isSpecialization()
    {
        return null != getLevelType() && getLevelType().isSpecialization();
    }

    public boolean isSpecialityPrintTitleMode()
    {
        return null != getLevelType() && getLevelType().isSpecialityPrintTitleMode();
    }

    public String getSpecializationNumber()
    {
        return null;
    }

    public boolean isProfileOrSpecialization() {
        return getEduProgramSpecialization() instanceof EduProgramSpecializationChild || getLevelType().isSpecialization() || getLevelType().isProfile();
    }

    /* ************** DEPRECATIONS ************* */

    /** @deprecated use getLevelType().getTitle() */
    @Deprecated
    public String getLevelTitleExtended() {
        return getLevelType().getTitle();
    }

    /** @deprecated use getLevelType().getTitle() */
    @Deprecated
    public String getLevelTitle() {
        return getLevelType().getTitle();
    }

    /** @deprecated use getProgramSubjectTitleWithCode() */
    @Deprecated
    public String getEduProgramSubjectTitle() {
        return getProgramSubjectTitleWithCode();
    }

    /** @deprecated use getProgramSpecializationTitle() */
    @Deprecated
    public String getEduProgramSpecializationDisplayableTitle() {
        return getProgramSpecializationTitle();
    }

    /** @deprecated use isProfileOrSpecialization() */
    @Deprecated
    public boolean isProfileOrSpecializationAndNotIndependentEducation() {
        return isProfileOrSpecialization();
    }

    @Deprecated
    public Qualifications getInheritedQualification()
    {
        return getQualification();
    }
}