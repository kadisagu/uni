/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.util.Collection;

/**
 * @author rsizonenko
 * @since 12.12.2014
 */
public interface IOrgUnitStudentListPersonalCardPrintDao extends INeedPersistenceSupport {
    IDocumentRenderer getDocumentRenderer(Collection<Long> ids);
}
