package ru.tandemservice.uni.migration;

import com.google.common.collect.Maps;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;
import java.util.Map;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uni_2x10x8_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // проверяем, миграция запускается на кривых данных?
        String ppsEntryEmployeePostSQL = tool.getDialect().getSQLTranslator().toSql(
                new SQLSelectQuery()
                        .from(SQLFrom.table("pps_entry_employeepost_t", "pps_ep"))
                        .top(1).column("pps_ep.post_id")
        );

        long firstPostId = tool.getNumericResult(ppsEntryEmployeePostSQL);
        boolean correct = tool.getNumericResult("select count(*) from postboundedwithqgandql_t where id = ?", firstPostId) > 0;

        // если должность выставлена неверно => меняем ссылки на должность ПКГ и КУ
        if (!correct)
        {
            tool.dropConstraint("pps_entry_employeepost_t", "fk_post_ppsentrybyemployeepost");

            // если у ключа есть связи
            {
                MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update pps_entry_employeepost_t set post_id = ? where id = ?", DBType.LONG, DBType.LONG);
                List<Object[]> rows = tool.executeQuery(
                        processor(Long.class, Long.class, Long.class),
                        "select ep4pe.ppsentry_id, pb.post_id, pr.postboundedwithqgandql_id from employee_post4pps_entry_t ep4pe " +
                                "inner join employeepost_t ep on ep4pe.employeepost_id = ep.id " +
                                "inner join orgunittypepostrelation_t pr on ep.postrelation_id = pr.id " +
                                "inner join postboundedwithqgandql_t pb on pr.postboundedwithqgandql_id = pb.id"
                );

                boolean update = false;
                Map<PairKey<Long, Long>, Long> oldPostId2NewIdMap = Maps.newHashMap();
                for (final Object[] row : rows)
                {
                    Long ppsEntryId = (Long) row[0];
                    Long postId = (Long) row[1];

                    PairKey<Long, Long> key = PairKey.create(ppsEntryId, postId);
                    Long postIdWithQGAndQLId = oldPostId2NewIdMap.get(key);

                    if (null == postIdWithQGAndQLId)
                    {
                        postIdWithQGAndQLId = (Long) row[2];
                        oldPostId2NewIdMap.put(key, postIdWithQGAndQLId);
                        updater.addBatch(postIdWithQGAndQLId, key.getFirst());
                        update = true;
                    }
                }
                if (update)
                    updater.executeUpdate(tool);
            }

            // если у ключа нет связей
            {
                List<Object[]> ppsEntryRows = tool.executeQuery(
                        processor(Long.class, Long.class),
                        "select pps_ep.id, pps_ep.post_id from pps_entry_employeepost_t pps_ep " +
                                "left join employee_post4pps_entry_t ep4pe on ep4pe.ppsentry_id = pps_ep.id " +
                                "where ep4pe.id is null"
                );

                Map<Long, Long> ppsEntryId2PostIdMap = Maps.newHashMap();
                for (final Object[] row : ppsEntryRows)
                {
                    Long id = (Long) row[0];
                    Long postId =(Long) row[1];
                    ppsEntryId2PostIdMap.put(id, postId);
                }

                MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update pps_entry_employeepost_t set post_id = ? where id = ?", DBType.LONG, DBType.LONG);
                List<Object[]> rows = tool.executeQuery(processor(Long.class, Long.class), "select id, post_id from postboundedwithqgandql_t order by post_id, id");

                Map<Long, Long> oldPostId2NewId = Maps.newHashMap();
                for (final Object[] row : rows)
                {
                    Long postId = (Long) row[1];
                    if (!oldPostId2NewId.containsKey(postId))
                        oldPostId2NewId.put(postId, (Long) row[0]);
                }

                boolean update = false;
                for (Map.Entry<Long, Long> entry : ppsEntryId2PostIdMap.entrySet())
                {
                    Long ppsEntryId = entry.getKey();
                    Long postId = entry.getValue();

                    updater.addBatch(oldPostId2NewId.get(postId), ppsEntryId);
                    update = true;
                }
                if (update)
                    updater.executeUpdate(tool);
            }
        }
    }
}