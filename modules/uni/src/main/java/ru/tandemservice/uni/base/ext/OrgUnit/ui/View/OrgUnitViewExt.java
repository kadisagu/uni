/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.archive.base.bo.Archive.ui.List.ArchiveList;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;

/**
 * @author Vasily Zhukov
 * @since 19.03.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    // addons
    public static final String ORG_UNIT_ADDON_NAME = "uniOrgUnitViewExtUI";

    // tabs
    public static final String ORG_UNIT_STUDENT_TAB = "orgUnitStudentTab";
    public static final String ORG_UNIT_REPORT_TAB = "orgUnitReportTab";

    // buttons
    public static final String ORG_UNIT_EDIT_ADDITIONAL_DATA = "orgUnitEditAdditionalData";

    // blocks
    public static final String ORG_UNIT_TOP_ADDITIONAL_BLOCK = "orgUnitTopAdditionalBlock";

    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_orgUnitView.presenterExtPoint())
                .addAddon(uiAddon(ORG_UNIT_ADDON_NAME, OrgUnitViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab(ORG_UNIT_STUDENT_TAB, ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentTab.Controller.class.getPackage().getName())
                        .parameters("mvel:['orgUnitId':presenter.orgUnit.id]")
                        .permissionKey("ui:secModel.orgUnit_viewStudentTabAll")
                        .before(org.tandemframework.shared.employeebase.base.ext.OrgUnit.ui.View.OrgUnitViewExt.ORG_UNIT_EMPLOYEE_TAB))

                .addTab(componentTab("archiveTab", ArchiveList.class, "mvel:['ownerId':presenter.orgUnit.id]")
                        .permissionKey("ui:secModel.viewArchiveList_orgUnit")
                        .visible("ognl:@org.tandemframework.shared.archive.base.bo.Archive.ArchiveManager@instance().archiveService().isVisibleArchiveListTab(presenter.orgUnit.id)"))
                .create();
    }

    @Bean
    public ButtonListExtension buttonListExtension()
    {
        return buttonListExtensionBuilder(_orgUnitView.orgUnitTabButtonListExtPoint())
                .addButton(submitButton(ORG_UNIT_EDIT_ADDITIONAL_DATA, ORG_UNIT_ADDON_NAME + ":onClickAdditionalEdit").visible("ui:orgUnit.top").after(OrgUnitView.ORG_UNIT_EDIT).permissionKey("orgUnit_editAdditionalAttrs_" + OrgUnitTypeCodes.TOP))
                .create();
    }

    @Bean
    public BlockListExtension blockListExtension()
    {
        return blockListExtensionBuilder(_orgUnitView.orgUnitTabBlockListExtPoint())
                .addBlock(htmlBlock(ORG_UNIT_TOP_ADDITIONAL_BLOCK, "OrgUnitTop_AdditionalBlock").visible("ui:orgUnit.top"))
                .create();
    }
}
