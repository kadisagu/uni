/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.StudentStatusSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author vip_delete
 */
public class Model
{
    private DynamicListDataSource<StudentStatus> _dataSource;
    private Long _studentStatusId;

    public DynamicListDataSource<StudentStatus> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentStatus> dataSource)
    {
        _dataSource = dataSource;
    }

    public Long getStudentStatusId()
    {
        return _studentStatusId;
    }

    public void setStudentStatusId(Long studentStatusId)
    {
        _studentStatusId = studentStatusId;
    }
}
