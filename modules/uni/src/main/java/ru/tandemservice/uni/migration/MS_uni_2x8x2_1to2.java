/* $Id:$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author oleyba
 * @since 6/25/15
 */
public class MS_uni_2x8x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update accessmatrix_t set permissionkey_p='addGlobalStoredReport' where permissionkey_p ='addUniContingentStorableReport'");
        tool.executeUpdate("update accessmatrix_t set permissionkey_p='printGlobalStoredReport' where permissionkey_p ='printUniContingentStorableReport'");
        tool.executeUpdate("update accessmatrix_t set permissionkey_p='deleteGlobalStoredReport' where permissionkey_p ='deletUniContingentStorableReport'");
    }
}