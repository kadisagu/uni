package ru.tandemservice.uni.migration;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x5_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность developCondition

		// создано свойство userCode
		{
			// создать колонку
			tool.createColumn("developcondition_t", new DBColumn("usercode_p", DBType.createVarchar(255)));
		}

		// создано свойство disabledDate
		{
			// создать колонку
			tool.createColumn("developcondition_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
		}

		//  свойство title стало обязательным
		{
			// сделать колонку NOT NULL
			tool.setColumnNullable("developcondition_t", "title_p", false);
		}

        // восстанавливаем недостающие элементы
        short developConditionEntityCode = tool.entityCodes().ensure("developCondition");

        Map<String, Object[]> missItemsMap = new HashMap<>(1);
        missItemsMap.put("1", new Object[]{ "Полный срок", "пс" });                         // DevelopConditionCodes.FULL_PERIOD
        missItemsMap.put("2", new Object[]{ "Сокращенная программа", "сп" });               // DevelopConditionCodes.REDUCED_PERIOD
        missItemsMap.put("3", new Object[]{ "Ускоренная программа", "уп" });                // DevelopConditionCodes.ACCELERATED_PERIOD
        missItemsMap.put("4", new Object[]{ "Сокращенная ускоренная программа", "суп" });   // DevelopConditionCodes.REDUCED_ACCELERATED_PERIOD

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select code_p from developcondition_t");
        ResultSet rs = stmt.getResultSet();
        Collection<String> itemCodes = new ArrayList<>();
        while (rs.next())
            itemCodes.add(rs.getString(1));

        Collection<String> missItemCodes = CollectionUtils.subtract(missItemsMap.keySet(), itemCodes);
        PreparedStatement insertMissItemsStmt = tool.getConnection().prepareStatement("insert into developcondition_t (id, discriminator, code_p, title_p, shorttitle_p) values (?, ?, ?, ?, ?)");
        insertMissItemsStmt.setShort(2, developConditionEntityCode);

        for (String missItemCode : missItemCodes)
        {
            Object[] values = missItemsMap.get(missItemCode);
            insertMissItemsStmt.setLong(1, EntityIDGenerator.generateNewId(developConditionEntityCode));
            insertMissItemsStmt.setString(3, missItemCode);
            insertMissItemsStmt.setString(4, (String) values[0]); // title_p
            insertMissItemsStmt.setString(5, (String) values[1]); // shorttitle_p

            insertMissItemsStmt.execute();
        }

        // устанавливаем дату запрещения для удаленных элементов
        PreparedStatement updateDisabledDate = tool.getConnection().prepareStatement("update developcondition_t set disableddate_p = ? where code_p = ?");
        updateDisabledDate.setDate(1, new java.sql.Date(new java.util.Date().getTime()));

        for (String missItemCode: missItemCodes)
        {
            updateDisabledDate.setString(2, missItemCode);
            updateDisabledDate.execute();
        }
    }
}