/**
 * $Id$
 */
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author dseleznev
 * Created on: 09.10.2008
 */
@Input( { @Bind(key = "studentIds", binding = "studentIds") })
public class Model
{

    private final SpringBeanCache<IOrgUnitStudentPersonCardPrintFactory> printFactory = new SpringBeanCache<IOrgUnitStudentPersonCardPrintFactory>(IOrgUnitStudentPersonCardPrintFactory.FACTORY_SPRING);
    public SpringBeanCache<IOrgUnitStudentPersonCardPrintFactory> getPrintFactory() { return this.printFactory; }

    private Collection<Long> studentIds = Collections.emptyList();
    public Collection<Long> getStudentIds() { return this.studentIds; }
    public void setStudentIds(Collection<Long> studentIds) { this.studentIds = studentIds; }

}