package ru.tandemservice.uni.ui;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.MQDomain;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.BaseFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.*;

/**
 * @author vdanilov
 */
public abstract class UniQueryFullCheckSelectModel extends BaseFullCheckSelectModel
{
    @Override
    public UniQueryFullCheckSelectModel setPageSize(final int size) {
        return (UniQueryFullCheckSelectModel)super.setPageSize(size);
    }

    public UniQueryFullCheckSelectModel(final String... labelProperties) {
        super(labelProperties);
    }

    protected abstract MQBuilder query(String alias, String filter);

    @Override public ListResult findValues(final String filter) {
        final MQBuilder builder = query("e", StringUtils.trimToNull(filter));
        if (null == builder) { return ListResult.getEmpty(); }
        return buildListResult(builder);
    }

    @SuppressWarnings("unchecked")
    protected ListResult buildListResult(final MQBuilder builder) {
        return UniDaoFacade.getCoreDao().<ListResult>getCalculatedValue(session -> {
            final int pageSize = getPageSize();
            final List list = wrap(list(builder, session, pageSize));
            if (pageSize > 0 && pageSize == list.size()) {
                final long count = builder.getResultCount(session);
                return new ListResult<>(list, count);
            }
            return new ListResult(list);
        });
    }

    @Override public Object getValue(final Object primaryKey) {
        if (null == primaryKey) { return null; }

        final MQBuilder builder = query("e", null);
        if (null == builder) { return null; }

        builder.add(MQExpression.eq("e", "id", primaryKey));
        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final List value = list(builder, session, 0);
            if ((null == value) || value.isEmpty()) { return null; }

            return value.iterator().next();
        });
    }

    @Override
    public List getValues(final Set primaryKeys) {
        final MQBuilder builder = query("e", null);
        if (null == builder) { return Collections.emptyList(); }

        builder.add(MQExpression.in("e", "id", primaryKeys));

        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final List value = list(builder, session, 0);
            if ((null == value) || value.isEmpty()) { return Collections.emptyList(); }
            return sort(value, primaryKeys);
        });
    }

    @SuppressWarnings("unchecked")
    private List<Object> list(final MQBuilder builder, final Session session, final int pageSize) {
        final MQDomain domain = builder.getDomain(builder.getSelectAliasList().iterator().next());
        if (domain.getMeta().isInterface() && (null == domain.getSelectedProperties()))
        {
            // если мы извлекаем интерфейс

            final Object[] selectedProperties = domain.getSelectedProperties();
            domain.setSelectedProperties(new String[] { "id" });

            final Collection<Long> ids = new LinkedHashSet<>((List) list(builder, session, pageSize));
            domain.setSelectedProperties(selectedProperties); // FIXME ?

            final Map<Long, Object> map = new HashMap<>(ids.size());
            final List<IEntity> list = IUniBaseDao.instance.get().<IEntity>getList(domain.getMeta().getEntityClass(), "id", ids);
            for (final IEntity e: list) {
                map.put(e.getId(), e);
            }

            final List<Object> result = new ArrayList<>(ids.size());
            for (final Long id: ids) {
                result.add(map.get(id));
            }

            return result;
        }

        return (pageSize > 0 ? builder.getResultList(session, 0, pageSize) : builder.getResultList(session));
    }

}
