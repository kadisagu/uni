package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelMiddle;
import ru.tandemservice.uni.entity.catalog.EducationLevelMiddleGos2;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки (специальность) СПО (ГОС2)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelMiddleGos2Gen extends EducationLevelMiddle
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.EducationLevelMiddleGos2";
    public static final String ENTITY_NAME = "educationLevelMiddleGos2";
    public static final int VERSION_HASH = -2075891098;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EducationLevelMiddleGos2Gen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelMiddleGos2Gen> extends EducationLevelMiddle.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevelMiddleGos2.class;
        }

        public T newInstance()
        {
            return (T) new EducationLevelMiddleGos2();
        }
    }
    private static final Path<EducationLevelMiddleGos2> _dslPath = new Path<EducationLevelMiddleGos2>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevelMiddleGos2");
    }
            

    public static class Path<E extends EducationLevelMiddleGos2> extends EducationLevelMiddle.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EducationLevelMiddleGos2.class;
        }

        public String getEntityName()
        {
            return "educationLevelMiddleGos2";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
