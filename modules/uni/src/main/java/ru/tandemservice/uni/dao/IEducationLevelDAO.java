/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.DQLRestrictedBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.List;

/**
 * @author agolubenko
 * @since 04.06.2008
 */
public interface IEducationLevelDAO
{
    final String BEAN_NAME = "educationLevelDao";
    final SpringBeanCache<IEducationLevelDAO> instance = new SpringBeanCache<>(BEAN_NAME);

    List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(List<OrgUnit> formativeOrgUnitList, List<OrgUnit> territorialOrgUnitList, String filter);

    List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList(IEducationLevelModel model, String filter);

    /**
     * Формирует список форм освоения на основании данных модели, на основании:
     *  формирующее подр., территориальное подр.
     * @param model модель
     * @param nullIsIgnore игнорировать параметры модели, значение которых null
     * @return список
     */
    List<DevelopForm> getDevelopFormList(IEducationLevelModel model, boolean nullIsIgnore);
    @Deprecated List<DevelopForm> getDevelopFormList(IEducationLevelModel model);

    /**
     * Формирует список условий освоения на основании данных модели, на основании:
     *  * формирующее подр., территориальное подр., форма
     * @param model модель
     * @param nullIsIgnore игнорировать параметры модели, значение которых null
     * @return список
     */
    List<DevelopCondition> getDevelopConditionList(IEducationLevelModel model, boolean nullIsIgnore);
    @Deprecated List<DevelopCondition> getDevelopConditionList(IEducationLevelModel model);

    /**
     * Формирует список технологий освоения на основании данных модели, на основании:
     *  * формирующее подр., территориальное подр., форма, условие
     * @param model модель
     * @param nullIsIgnore игнорировать параметры модели, значение которых null
     * @return список
     */
    List<DevelopTech> getDevelopTechList(IEducationLevelModel model, boolean nullIsIgnore);
    @Deprecated List<DevelopTech> getDevelopTechList(IEducationLevelModel model);

    /**
     * Формирует список периодов (сроки) освоения на основании данных модели, на основании:
     *  * формирующее подр., территориальное подр., форма, условие, технология
     * @param model модель
     * @param nullIsIgnore игнорировать параметры модели, значение которых null
     * @return список
     */
    List<DevelopPeriod> getDevelopPeriodList(IEducationLevelModel model, boolean nullIsIgnore);
    @Deprecated List<DevelopPeriod> getDevelopPeriodList(IEducationLevelModel model);

    EducationOrgUnit getEducationOrgUnit(IEducationLevelModel model);

    /**
     * Добавляем условие фильтрации для выбора только тех НПП (educationOrgUnit), у которых стоит флаг "Используется",
     * и НПВ которых имею флаг "Обучение студентов осуществляется".
     * Метод вынесен в дао для возможности переопределения - например, если кому-то не захочется скрывать неиспользуемые НПП в фильтрах.
     *
     * @param educationOrgUnitBuilder билдер
     * @param educationOrgUnitAlias алиас для EducationOrgUnit
     */
    void applyUsedFilterForEducationOrgUnit(DQLRestrictedBuilder educationOrgUnitBuilder, String educationOrgUnitAlias);
}
