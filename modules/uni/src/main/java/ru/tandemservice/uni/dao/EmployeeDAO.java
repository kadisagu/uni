/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import org.hibernate.Query;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.report.FiltersPreset;

import java.util.List;

/**
 * @author vip_delete
 * @since 24.10.2008
 */
public class EmployeeDAO extends UniBaseDao implements IEmployeeDAO
{
    @Override
    public List<FiltersPreset> getFilterPresetsList(String settingsKey)
    {
        MQBuilder builder = new MQBuilder(FiltersPreset.ENTITY_CLASS, "fp");
        builder.add(MQExpression.eq("fp", FiltersPreset.P_SETTINGS_KEY_PREFIX, settingsKey));
        builder.addOrder("fp", FiltersPreset.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public int getFreeFiltersPresetNumber(String settingsKey)
    {
        Query q = getSession().createQuery("select max(" + FiltersPreset.P_SETTINGS_NUMBER + ") from " + FiltersPreset.ENTITY_CLASS + " fp where fp." + FiltersPreset.P_SETTINGS_KEY_PREFIX + "=:settingsKey");
        q.setParameter("settingsKey", settingsKey);
        Number maxNumber = (Number) q.uniqueResult();
        return null != maxNumber ? maxNumber.intValue() + 1 : 1;
    }

    @Override
    public void updateFiltersPreset(FiltersPreset preset)
    {
        getSession().saveOrUpdate(preset);
    }

    @Override
    public void deleteFiltersPreset(FiltersPreset preset)
    {
        DataSettingsFacade.deleteSettings("general", preset.getSettingsKeyPrefix() + "." + preset.getSettingsNumber());
        getSession().delete(preset);
    }

    @Override
    public boolean isAnyActiveStudentOnOrgUnit(OrgUnit orgUnit)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
        AbstractExpression expr1 = MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, orgUnit);
        AbstractExpression expr2 = MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, orgUnit);
        AbstractExpression expr3 = MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_ORG_UNIT, orgUnit);
        builder.add(MQExpression.or(expr1, expr2, expr3));
        return builder.getResultCount(getSession()) > 0;
    }
}