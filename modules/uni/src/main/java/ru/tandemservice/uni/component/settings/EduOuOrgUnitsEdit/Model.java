// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit;

import java.util.List;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author oleyba
 * @since 03.12.2010
 */
@Input({@Bind(key = "ids", binding = "ids"),
        @Bind(key = "displayMode", binding = "displayMode")})
public class Model
{

    public static enum DisplayMode
    {
        OPERATION, GROUP, BOTH
    }

    private List<Long> ids;
    private DisplayMode displayMode;
    private OrgUnit operationOrgUnit;
    private OrgUnit groupOrgUnit;

    private ISelectModel ouModel;

    public String getTitle()
    {
        switch (getDisplayMode()) {
            case OPERATION:
                return "Задание ответственной диспетчерской для выбранных направлений подготовки (специальностей)";
            case GROUP:
                return "Задание ответственного деканата для выбранных направлений подготовки (специальностей)";
            case BOTH:
                return "Установка ответственных подразделений за направление подготовки (специальность) «" + IUniBaseDao.instance.get().get(EducationOrgUnit.class, ids.get(0)).getEducationLevelHighSchool().getDisplayableTitle() + "»";
        }
        return ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY);
    }

    public boolean isOperation()
    {
        return DisplayMode.OPERATION.equals(getDisplayMode());
    }

    public boolean isGroup()
    {
        return DisplayMode.GROUP.equals(getDisplayMode());
    }

    public boolean isBoth()
    {
        return DisplayMode.BOTH.equals(getDisplayMode());
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public DisplayMode getDisplayMode() {
        return displayMode;
    }

    public void setDisplayMode(DisplayMode displayMode) {
        this.displayMode = displayMode;
    }

    public OrgUnit getOperationOrgUnit() {
        return operationOrgUnit;
    }

    public void setOperationOrgUnit(OrgUnit operationOrgUnit) {
        this.operationOrgUnit = operationOrgUnit;
    }

    public OrgUnit getGroupOrgUnit() {
        return groupOrgUnit;
    }

    public void setGroupOrgUnit(OrgUnit groupOrgUnit) {
        this.groupOrgUnit = groupOrgUnit;
    }

    public ISelectModel getOuModel() {
        return ouModel;
    }

    public void setOuModel(ISelectModel ouModel) {
        this.ouModel = ouModel;
    }
}
