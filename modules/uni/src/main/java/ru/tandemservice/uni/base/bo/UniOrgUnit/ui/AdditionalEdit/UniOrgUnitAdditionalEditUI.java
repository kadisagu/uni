/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.base.bo.UniOrgUnit.ui.AdditionalEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

/**
 * @author Vasily Zhukov
 * @since 26.03.2012
 */
@Input({
        @Bind(key = UniOrgUnitAdditionalEditUI.ORG_UNIT_ID, binding = "orgUnit.id")
})
public class UniOrgUnitAdditionalEditUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";
    
    private TopOrgUnit _orgUnit = new TopOrgUnit();
    private AcademyData _academyData;

    @Override
    public void onComponentRefresh()
    {
        _orgUnit = DataAccessServices.dao().getNotNull(TopOrgUnit.class, _orgUnit.getId());
        _academyData = AcademyData.getInstance();
    }

    // Getters & Setters

    public TopOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(TopOrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public AcademyData getAcademyData()
    {
        return _academyData;
    }

    public void setAcademyData(AcademyData academyData)
    {
        _academyData = academyData;
    }

    // Listeners

    public void onClickApply()
    {
        DataAccessServices.dao().update(_academyData);
        
        deactivate();
    }
}
