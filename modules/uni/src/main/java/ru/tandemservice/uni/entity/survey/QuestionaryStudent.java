package ru.tandemservice.uni.entity.survey;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.survey.catalog.entity.codes.SurveyQuestionCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.survey.gen.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/** @see ru.tandemservice.uni.entity.survey.gen.QuestionaryStudentGen */
public class QuestionaryStudent extends QuestionaryStudentGen
{
    public static final String ALIAS = "s";
    @Override
    public String getObjectName()
    {
        return "Студент";
    }

    @Override
    public void setQuestionaryObject(Object object)
    {
        if (object instanceof Student)
            setObject((Student) object);
        else
            throw new ApplicationException("Объект анкетирования не является студентом.");
    }

    @Override
    public Object getQuestionaryObject()
    {
        return getObject();
    }

    @Override
    public String getQuestionaryObjectTitle()
    {
        return getObject() != null? getObject().getTitleWithFio() : null;
    }

    @Override
    public DQLSelectBuilder getDQLSearchObject()
    {
        return new DQLSelectBuilder().fromEntity(Student.class, ALIAS)
                .column(property(ALIAS, Student.id()))
                .order(property(ALIAS, Student.id()));
    }

    @Override
    public Map<PairKey<String,Boolean>, String> getDataSourceColumns()
    {
        Map<PairKey<String,Boolean>, String> resultMap = new LinkedHashMap<>();
        resultMap.put(new PairKey<>("Фамилия Имя Отчество", Boolean.TRUE), Student.person().fullFio().s());
        resultMap.put(new PairKey<>("Дата рождения", Boolean.FALSE), Student.person().identityCard().birthDate().s());
        resultMap.put(new PairKey<>("Курс", Boolean.FALSE), Student.course().title().s());
        resultMap.put(new PairKey<>("Группа", Boolean.FALSE), Student.group().title().s());
        resultMap.put(new PairKey<>("Направление подготовки", Boolean.FALSE), Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().titleWithCodeIndexAndGen().s());

        return resultMap;
    }

    @Override
    public Object getAnswerValue(String questionCode)
    {
        if (getObject() == null)
            return null;

        switch (questionCode)
        {
            case SurveyQuestionCodes.FIRST_NAME:
                return getObject().getPerson().getIdentityCard().getFirstName();
            case SurveyQuestionCodes.LAST_NAME:
                return getObject().getPerson().getIdentityCard().getLastName();
            case SurveyQuestionCodes.MIDDLE_NAME:
                return getObject().getPerson().getIdentityCard().getMiddleName();
            case SurveyQuestionCodes.BIRTHDATE:
                return getObject().getPerson().getIdentityCard().getBirthDate();
            case SurveyQuestionCodes.EDU_LEVELHIGHSCHOOL:
                return getObject().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getTitleWithCodeIndexAndGen();
            case SurveyQuestionCodes.CONTACT_PHONE:
                return  getObject().getPerson().getContactData().getContactPhones();
            case SurveyQuestionCodes.EMAIL:
                return  getObject().getPerson().getContactData().getEmail();
            case SurveyQuestionCodes.FORMATIVE_ORG_UNIT:
                return getObject().getEducationOrgUnit().getFormativeOrgUnit().getPrintTitle();
            case SurveyQuestionCodes.GROUP:
                return getObject().getGroup() != null? getObject().getGroup().getTitle() : "";

        }
        return null;
    }

    @Override
    public DQLSelectBuilder addWhereExpression(DQLSelectBuilder builder, String questionCode)
    {
        switch (questionCode)
        {
            case SurveyQuestionCodes.FIRST_NAME:
                return builder.column(property(ALIAS, Student.person().identityCard().firstName()));
            case SurveyQuestionCodes.LAST_NAME:
                return builder.column(property(ALIAS, Student.person().identityCard().lastName()));
            case SurveyQuestionCodes.MIDDLE_NAME:
                return builder.column(property(ALIAS, Student.person().identityCard().middleName()));
        }
        return builder;
    }

    @Override
    public DQLSelectBuilder getObjectList(Collection<Long> ids)
    {
        return new DQLSelectBuilder().fromEntity(Student.class, ALIAS)
                .where(in(property(ALIAS, Student.id()), ids));
    }


}