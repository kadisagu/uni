/**
 *$Id$
 */
package ru.tandemservice.uni.base.ext.SystemAction.ui.Pub;

import com.healthmarketscience.jackcess.Database;
import org.apache.commons.io.FileUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniSystemAction.UniSystemActionManager;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.dao.mdbio.IStudentIODao;
import ru.tandemservice.uni.dao.pps.PpsEntryByEmployeePostSynchronizer;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uni.util.schema.IOrgstructSchemaGenerator;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class UniSystemActionPubAddon extends UIAddon
{
    public UniSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickChangeStudentStatus()
    {
        getActivationBuilder().asCurrent("ru.tandemservice.uni.component.settings.SystemActionsChangeStudentStatus")
        .activate();
    }

    public void onClickCorrectIdentityCardFIO()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            UniSystemActionManager.instance().dao().correctIdentityCardFIOInNewTransaction();
        } finally {
            eventLock.release();
        }
    }

    public void onClickUpdateAddressItemsFullTitles()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            UniSystemActionManager.instance().dao().updateAddressItemsFullTitles();
        } finally {
            eventLock.release();
        }
    }

    public void onClickActualiseGroupTitles()
    {
        UniSystemActionManager.instance().dao().updateGroupTitles();
    }

    public void onClickGetEduLevelsData() throws Exception
    {
        // переходим на универсальный компонент рендера отчета
        getActivationBuilder().asCurrent(IUniComponents.PRINT_REPORT)
        .parameters(new ParametersMap().add("id", UniSystemActionManager.instance().dao().printEducationLevelsReport()))
        .activate();
    }

    public void onClickGetPersonData()
    {
        getActivationBuilder().asCurrent("ru.tandemservice.uni.component.settings.SystemActionsPrintPersonData")
        .activate();
    }

    public void onClickDeleteCategorylessPersons()
    {
        UniSystemActionManager.instance().dao().deleteCategorylessPersons();
    }

    public void onClickMergeCatalogItems()
    {
        getActivationBuilder().asCurrent("ru.tandemservice.uni.component.settings.SystemActionsMergeCatalogItems")
        .activate();
    }

    public void onClickGetOrdStructSchema()
    {
        // регистрируем его в универсальном компоненте рендера
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(IOrgstructSchemaGenerator.instance.get().getOrgstructSchema().getBytes(), "orgstruct.graphml");
        getActivationBuilder().asCurrent(IUniComponents.PRINT_REPORT)
        .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "graphml"))
        .activate();
    }

    public void onClickUpdateGroupTitlesWithNulls()
    {
        UniSystemActionManager.instance().dao().updateGroupTitlesWithNulls();
    }

    public void onClickUpdatePpsEntryByEmployeePost()
    {
        PpsEntryByEmployeePostSynchronizer.instance.get().doSynchronize();
    }

    public void onOrgstructAndEmployeeImport()
    {
        getActivationBuilder().asCurrent(ru.tandemservice.uni.component.settings.OrgstructAndEmployeeImport.Controller.class.getPackage().getName())
        .activate();
    }

    public void onClickExportTemplate4Student()
    {
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(getTemplate4Student(false), "students-"+ DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())+".mdb");
        getActivationBuilder().asDesktopRoot(IUniComponents.PRINT_REPORT)
        .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "mdb"))
        .activate();
    }

    public static byte[] getTemplate4Student(boolean includeArchiveStudents) {
        try {
            final File file = File.createTempFile("mdb-io-", "mdb");
            try {
                final Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                IStudentIODao.instance.get().export_StudentList(mdb, includeArchiveStudents);

                /*
                for (Table table: mdb) {
                    for (Column column: table.getColumns()) {
                        System.out.println(table.getName()+"."+column.getName()+" ["+column.getType()+"]");
                    }
                }
                 */

                mdb.close();
                return FileUtils.readFileToByteArray(file);
            } finally {
                file.delete();
            }
        } catch (final Exception t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickExportExcludedStudentToXls()
    {
        getActivationBuilder().asCurrent("ru.tandemservice.uni.component.settings.SystemActionsExportExcludedStudentsXls")
        .activate();
    }


    public void onClickImportStudents()
    {

        // пытаемся открыть файл
        final IBackgroundProcess process = new BackgroundProcessBase() {
            @Override public ProcessResult run(final ProcessState state) {

                // предварительно
                String path = ApplicationRuntime.getAppInstallPath();
                final File file = new File(path, "data/importStudents.mdb");
                if (!file.exists()) { throw new ApplicationException("Импорт данных провести не удалось: не найден файл «importStudents.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути."); }
                if (!file.canRead()) { throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/importStudents.mdb» не доступен для чтения. Проверьте доступ к файлу на сервере приложения."); }

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try {
                    BaseIODao.setupProcessState(state);
                    BaseIODao.doLoggedAction(() -> {
                        BaseIODao.getLog4jLogger().info("Import started.");
                        try {
                            // открываем файл
                            try (Database mdb = Database.open(file))
                            {
                                // запускаем импорт
                                final Map<String, Long> value = IStudentIODao.instance.get().doImport_StudentList(mdb);
                                BaseIODao.getLog4jLogger().info("Import finished. Done.");
                                return value;
                            }
                            // закрываем файл

                        } catch (Throwable t) {
                            BaseIODao.getLog4jLogger().info("Import finished. Error.");
                            throw CoreExceptionUtils.getRuntimeException(t);
                        }
                    }, "importStudents");

                } catch (final Exception t) {

                    if (t instanceof ApplicationException) {
                        // пробрасываем ApplicationException, как есть и добавляем ошибку в ErrorCollector для отображения сообщения о файле лога
                        UserContext.getInstance().getErrorCollector().add("Обратитесь к логу импорта (файл importStudents.log) на сервере приложения для получения детальной информации об ошибке.");
                        throw (ApplicationException) t;
                    }

                    Debug.exception(t.getMessage(), t);
                    throw new ApplicationException("Импорт данных провести не удалось: произошла ошибка при обращении к файлу импорта. Обратитесь к логу импорта (файл importStudents.log) на сервере приложения для получения детальной информации об ошибке.", t);

                } finally {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                }

                // если все ок, просто закрываем диалог
                return new ProcessResult("Импорт студентов завершен."); // закрываем диалог
            }
        };

        new BackgroundProcessHolder().start("Импорт данных студентов", process, ProcessDisplayMode.unknown);
    }

    public void onClickExportEduData()
    {
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(this.getEducationData(), "eduData-"+ DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())+".mdb");
        getActivationBuilder().asDesktopRoot(IUniComponents.PRINT_REPORT)
        .parameter("id", id)
        .parameter("zip", Boolean.FALSE)
        .parameter("extension", "mdb")
        .activate();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private byte[] getEducationData()
    {
        try
        {
            final File file = File.createTempFile("mdb-eduData-", "mdb");
            try
            {
                final Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                UniSystemActionManager.instance().dao().exportEduData(mdb);
                mdb.close();
                return FileUtils.readFileToByteArray(file);
            }
            finally
            {
                file.delete();
            }
        }
        catch (final IOException t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
