/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentAdditionalData;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.CustomStateUtil;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 07.02.2011
 */
public class StudentAdditionalDataParam implements IReportDQLModifier
{
    private IReportParam<List<StudentCategory>> _studentCategory = new ReportParam<>();
    private IReportParam<DataWrapper> _recruit = new ReportParam<>();
    private IReportParam<List<ExternalOrgUnit>> _targetAdmissionOrgUnit = new ReportParam<>();
    private IReportParam<DataWrapper> _individualEducation = new ReportParam<>();
    private IReportParam<String> _diplomTitle = new ReportParam<>();
    private IReportParam<List<PpsEntry>> _diplomAdvisor = new ReportParam<>();
    private IReportParam<String> _practicePlace = new ReportParam<>();
    private IReportParam<String> _employment = new ReportParam<>();
    private IReportParam<List<StudentCustomStateCI>> _studentCustomStateCI = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем студента
        String studentAlias = dql.innerJoinEntity(Student.class, Student.person());

        // добавляем сортировку
        dql.order(studentAlias, Student.id());

        return studentAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_studentCategory.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.studentCategory", CommonBaseStringUtil.join(_studentCategory.getData(), StudentCategory.P_TITLE, ", "));

            dql.builder.where(in(property(Student.studentCategory().fromAlias(alias(dql))), _studentCategory.getData()));
        }

        if (_recruit.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.recruit", _recruit.getData().getTitle());

            dql.builder.where(eq(property(Student.targetAdmission().fromAlias(alias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_recruit.getData()))));
        }
        if (_targetAdmissionOrgUnit.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.targetAdmissionOrgUnit", CommonBaseStringUtil.join(_targetAdmissionOrgUnit.getData(), ExternalOrgUnit.P_TITLE, ", "));

            dql.builder.where(in(property(Student.targetAdmissionOrgUnit().fromAlias(alias(dql))), _targetAdmissionOrgUnit.getData()));
        }

        if (_individualEducation.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.individualEducation", _individualEducation.getData().getTitle());

            dql.builder.where(eq(property(Student.personalEducation().fromAlias(alias(dql))), value(TwinComboDataSourceHandler.getSelectedValueNotNull(_individualEducation.getData()))));
        }

        if (_diplomTitle.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.diplomTitle", _diplomTitle.getData());

            if (StringUtils.isEmpty(_diplomTitle.getData()))
                dql.builder.where(isNull(property(Student.finalQualifyingWorkTheme().fromAlias(alias(dql)))));
            else
                dql.builder.where(likeUpper(property(Student.finalQualifyingWorkTheme().fromAlias(alias(dql))), value(CoreStringUtils.escapeLike(_diplomTitle.getData()))));
        }

        if (_diplomAdvisor.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.diplomAdvisor", CommonBaseStringUtil.join(_diplomAdvisor.getData(), PpsEntry.P_TITLE, ", "));

            if (_diplomAdvisor.getData().isEmpty()) {
                dql.builder.where(isNull(property(Student.ppsEntry().fromAlias(alias(dql)))));
            }
            else {
                dql.builder.where(in(property(Student.ppsEntry().fromAlias(alias(dql))), _diplomAdvisor.getData()));
            }
        }

        if (_practicePlace.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.practicePlace", _practicePlace.getData());

            if (StringUtils.isEmpty(_practicePlace.getData()))
                dql.builder.where(isNull(property(Student.practicePlace().fromAlias(alias(dql)))));
            else
                dql.builder.where(likeUpper(property(Student.practicePlace().fromAlias(alias(dql))), value(CoreStringUtils.escapeLike(_practicePlace.getData()))));
        }

        if (_employment.isActive())
        {
            printInfo.addPrintParam(StudentReportPersonAddUI.STUDENT_SCHEET, "studentAdditionalData.employment", _employment.getData());

            if (StringUtils.isEmpty(_employment.getData()))
                dql.builder.where(isNull(property(Student.employment().fromAlias(alias(dql)))));
            else
                dql.builder.where(likeUpper(property(Student.employment().fromAlias(alias(dql))), value(CoreStringUtils.escapeLike(_employment.getData()))));
        }

        if (_studentCustomStateCI.isActive() && null != _studentCustomStateCI.getData() && !_studentCustomStateCI.getData().isEmpty())
        {
            DQLSelectBuilder csDQL = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                    .column(property("st", StudentCustomState.student().id()))
                    .where(eq(property(alias(dql)), property("st", StudentCustomState.student())))
                    .where(in(property("st", StudentCustomState.customState()), _studentCustomStateCI.getData()));

            CustomStateUtil.applyActivePeriodFilter(csDQL, "st");

            dql.builder.where(exists(csDQL.buildQuery()));
        }
    }

    // Getters

    public IReportParam<List<StudentCategory>> getStudentCategory()
    {
        return _studentCategory;
    }

    public IReportParam<DataWrapper> getRecruit()
    {
        return _recruit;
    }

    public IReportParam<List<ExternalOrgUnit>> getTargetAdmissionOrgUnit()
    {
        return _targetAdmissionOrgUnit;
    }

    public IReportParam<DataWrapper> getIndividualEducation()
    {
        return _individualEducation;
    }

    public IReportParam<String> getDiplomTitle()
    {
        return _diplomTitle;
    }

    public IReportParam<List<PpsEntry>> getDiplomAdvisor()
    {
        return _diplomAdvisor;
    }

    public IReportParam<String> getPracticePlace()
    {
        return _practicePlace;
    }

    public IReportParam<String> getEmployment()
    {
        return _employment;
    }

    public IReportParam<List<StudentCustomStateCI>> getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(IReportParam<List<StudentCustomStateCI>> studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }
}
