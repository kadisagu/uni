package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelMiddleGos2Gen;

/**
 * Направление подготовки (специальность) СПО (ГОС2)
 */
public class EducationLevelMiddleGos2 extends EducationLevelMiddleGos2Gen
{
    public EducationLevelMiddleGos2()
    {
        setCatalogCode("educationLevelMiddleGos2");
    }
}