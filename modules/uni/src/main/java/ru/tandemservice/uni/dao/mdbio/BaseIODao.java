package ru.tandemservice.uni.dao.mdbio;

import com.google.common.collect.Lists;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.*;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.BatchUtils.Action;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * @author vdanilov
 */
public class BaseIODao extends UniBaseDao
{

    protected static final Logger log4j_logger = Logger.getLogger(BaseIODao.class);
    protected static final String[] DATE_PARSE_PATTERNS = new String[]{DateFormatter.PATTERN_DEFAULT};

    public static class IOFatalException extends RuntimeException
    {
        public IOFatalException(String message)
        {
            super(message);
            try {
                throw new IllegalStateException("generating stacktrace for the message: '" + message +"'");
            } catch (Exception e) {
                log4j_logger.fatal(message, e);
            }
        }

        public IOFatalException(String message, Throwable cause)
        {
            super(message, cause);
            log4j_logger.fatal(message, cause);
        }

        public IOFatalException(String context, String message)
        {
            this(context + ": " + message);
        }

        public IOFatalException(String context, String message, Throwable cause)
        {
            this(context + ": " + message, cause);
        }
    }

    public static Logger getLog4jLogger()
    {
        return log4j_logger;
    }

    public static synchronized <T> T doLoggedAction(Callable<T> c, String logFileName) throws Exception
    {
        // проверяем, есть ли уже appender
        Appender appender = log4j_logger.getAppender("IOLogAppender");

        if (null != appender)
        {
            return c.call(); // уже зарегистрирован - ничего делать не надо, просто выполняем требуемые действия
        }

        try
        {
            // добавляем, если нет
            appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), Paths.get(System.getProperty("catalina.base"), "logs", logFileName + ".log").toString());
            appender.setName("IOLogAppender");
            ((FileAppender) appender).setThreshold(Level.INFO);
            log4j_logger.addAppender(appender);
            log4j_logger.setLevel(Level.INFO);
            log4j_logger.setAdditivity(false);

            try
            {
                return c.call();
            }
            catch (Exception t)
            {
                throw new IOFatalException(t.getMessage(), t);
            }
        }
        finally
        {
            // и отцепляем, если добавляли
            if (null != appender)
            {
                log4j_logger.removeAppender(appender);
            }
        }
    }

    private static final ThreadLocal<Reference<ProcessState>> processStates = new ThreadLocal<>();

    public static void setupProcessState(ProcessState state)
    {
        processStates.set(new WeakReference<>(state));

        // теперь пару слов о том, почему не надо это убирать:
        // ProcessState всегда живет в рамках отдельного потока
        // если посмотреть на реализацию ThreadLocal - то она хранит данные не в себе, а в потоке, котороый умирает по завершению процесса
    }

    protected static void setProcessStateMessage(String message)
    {
        log4j_logger.debug(message);

        Reference<ProcessState> ref = processStates.get();
        if (null == ref)
        {
            return;
        }

        ProcessState state = ref.get();
        if (null == state)
        {
            return;
        }

        state.setMessage(message);
        Thread.yield();
    }


    protected static void execute(final Table elements, final int threshold, final String message, final Action<Map<String, Object>> action)
    {
        execute(elements, 0, elements.getRowCount(), threshold, message, action);
    }

    protected static int execute(final Iterable<Map<String, Object>> elements, final int startSize, final int allCount, final int threshold, final String message, final Action<Map<String, Object>> action)
    {
        final MutableInt counter = new MutableInt(startSize);
        BatchUtils.execute(elements, threshold, rows -> {
            counter.add(rows.size());
            setProcessStateMessage(message + ": " + counter + "/" + Math.max(counter.intValue(), allCount));
            action.execute(rows);
        });
        return counter.intValue();
    }


    public String id(Long id)
    {
        return (null == id ? "" : Long.toHexString(id));
    }

    public Long id(Map<String, Object> row, String name)
    {
        final String id = StringUtils.trimToNull((String) row.get(name));
        return (null == id ? null : Long.parseLong(id, 16));
    }

    public interface ICatalogIO<T extends ICatalogItem>
    {
        Map<T, String> export(Database mdb);

        Map<String, T> lookup(boolean require);
    }

    protected Integer integer(final Map<String, Object> row, final String field, final String context, boolean required) throws IllegalStateException
    {
        Object o = row.get(field);
        boolean isString = o instanceof String;
        if (isString)
        {
            o = StringUtils.trimToNull((String) o);
        }

        if (o == null)
        {
            if (required)
                throw new IOFatalException(context, "Field value for '" + field + "' must not be null.");

            return null;
        }

        try
        {
            if (isString)
                o = Integer.parseInt((String)o);

            return Math.max(0, ((Number) o).intValue());
        }
        catch (ClassCastException | NumberFormatException e)
        {
            throw new IOFatalException(context, "Field value for '" + field + "' is not valid format (should be integer): " + o + "'.", e);
        }
    }

    protected Boolean getBoolean(final Map<String, Object> row, final String field, final String context, boolean required) throws IllegalStateException
    {
        final String value = StringUtils.trimToNull((String) row.get(field));
        if (value == null)
        {
            if (required)
                throw new IOFatalException(context, "field value for '" + field + "' must not be null.");

            return null;
        }

        if ("1".equals(value)) return true;
        if ("0".equals(value)) return false;

        throw new IOFatalException(context, "field value for '" + field + "' has no valid format: " + value);
    }

    protected IFieldPropertyMeta getFieldProperty(Class<? extends IEntity> entityClass, String propertyName)
    {
        return (IFieldPropertyMeta) EntityRuntime.getMeta(entityClass).getProperty(propertyName);
    }

    protected String trimmedString(final Map<String, Object> row, final String field, final String context, final IFieldPropertyMeta propertyMeta)
    {
        return trimmedString(row, field, context, propertyMeta.getLength(), propertyMeta.isRequired());
    }

    protected String trimmedString(final Map<String, Object> row, final String field, final String context, int maxLength, boolean required)
    {
        String value = StringUtils.trimToNull((String) row.get(field));
        if (value == null)
        {
            if (required)
                throw new IOFatalException(context, "Field value for '" + field + "' must be specified.");

            return null;
        }

        if (value.length() > maxLength)
        {
            log4j_logger.error(String.format("%s: size of field value for '%s' is more then %d characters and was cut for this length. Source value: '%s'.",
                                             context, field, maxLength, value));
            return value.substring(0, maxLength);
        }
        return value;
    }

    protected Date date(final Map<String, Object> row, final String field, final String context, boolean required)
    {
        final String value = StringUtils.trimToNull((String) row.get(field));
        if (value == null)
        {
            if (required)
                throw new IOFatalException(context, "Field value for '" + field + "' must be specified.");

            return null;
        }

        try
        {
            return DateUtils.parseDate(value, DATE_PARSE_PATTERNS);
        }
        catch (ParseException e)
        {
            final String message = context + ": unable to parse value in the field '" + field + "', it is not a valid format: " + value;
            if (required)
                throw new IOFatalException(message);

            log4j_logger.error(message);
        }
        return null;
    }

    protected <T> T lookup(final Map<String, T> map, final Map<String, Object> row, final String field, final String context)
    {
        final String key = StringUtils.trimToNull((String) row.get(field));
        final T value = map.get(key);
        if ((null != key) && (null == value))
        {
            log4j_logger.error(context + ": field value not found in database for field '" + field + "' and value '" + key + "'.");
        }
        return value;
    }

    public List<List<Long>> getIdListPartition(Class<? extends IEntity> clazz) {
        final List<Long> ids = new DQLSelectBuilder().fromEntity(clazz, "e").column(DQLExpressions.property("e.id")).createStatement(getSession()).list();
        return Lists.partition(ids, DQL.MAX_VALUES_ROW_NUMBER);
    }

    public <T extends ICatalogItem> ICatalogIO<T> catalog(final Class<T> klass, final String titleProperty)
    {
        setProcessStateMessage("Загрузка справочника: " + EntityRuntime.getMeta(klass).getTitle());

        final String key = "CatalogIODao.catalog-" + klass.getSimpleName();
        final Map<Long, String> valueMap = Collections.unmodifiableMap(this.getCalculatedValue(session -> {
            Map<Long, String> map = DaoCache.get(key);
            if (null != map) {
                return map;
            }

            map = new LinkedHashMap<>();
            for (final T item : BaseIODao.this.getCatalogItemListOrderByCode(klass)) {
                map.put(item.getId(), (item.getCode() + " " + StringUtils.capitalize((String) item.getProperty(titleProperty))));
            }

            DaoCache.put(key, map);
            return map;
        }));

        return new ICatalogIO<T>()
        {
            private final String tableName = "catalog_" + StringUtils.uncapitalize(klass.getSimpleName());

            @SuppressWarnings("unchecked")
            @Override
            public Map<T, String> export(final Database mdb)
            {
                try
                {
                    if (null == mdb.getTable(this.tableName))
                    {
                        final Table table = new TableBuilder(this.tableName)
                                .addColumn(new ColumnBuilder("text", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                                .toTable(mdb);
                        for (final String value : valueMap.values())
                        {
                            table.addRow(value);
                        }
                    }
                }
                catch (final Exception t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                return SafeMap.get(new SafeMap.Callback() {
                    @Override public String resolve(Object key) {
                        if (key instanceof IEntity) {
                            key = ((IEntity) key).getId();
                        } if (null == key) {
                            return "";
                        }

                        if (key instanceof Long) {
                            final String value = valueMap.get(key);
                            if (null == value)
                                throw new IOFatalException("No value with id='" + key + "' found in catalog '" + klass.getSimpleName() + "'.");

                            return value;
                        }
                        throw new IllegalArgumentException("Unsupported argument class '" + key.getClass().getName() + "'.");
                    }
                });
            }

            @SuppressWarnings("unchecked")
            @Override
            public Map<String, T> lookup(final boolean require)
            {
                final Map<String, Long> result = new HashMap<>(valueMap.size());
                for (final Map.Entry<Long, String> e : valueMap.entrySet())
                {
                    final String text = e.getValue();
                    result.put(text.toLowerCase(), e.getKey());

                    final String code = StringUtils.substringBefore(text, " ");
                    if (!text.equals(code)) {
                        result.put(code.toLowerCase(), e.getKey());
                    }
                }

                final Session session = BaseIODao.this.getSession();
                return SafeMap.get((String k) -> {

                    String key = StringUtils.trimToNull(k);

                    if (null == key) {
                        if (!require)
                            return null;
                        // если поле обязательно, то просто null выбрасывать нельзя
                        throw new IOFatalException("Key for catalog '" + tableName + "' must not be null.");
                    }

                    key = key.toLowerCase();
                    Long value = result.get(key);
                    if (null == value) {
                        key = StringUtils.substringBefore(key, " ");
                        value = result.get(key);
                    }

                    if (null == value) {
                        if (require)
                            throw new IOFatalException("No value found: " + tableName + "[text='" + key + "'].");

                        log4j_logger.error("No value found: " + tableName + "[text='" + key + "'].");
                        return null;
                    }
                    return (T) session.load(klass, value);
                });
            }
        };
    }

    public static Long tryParseHexId(String hexId)
    {
        if (StringUtils.isEmpty(hexId))
            return null;

        try
        {
            return Long.parseLong(hexId, 16);
        }
        catch (NumberFormatException e)
        {
            return null;
        }
    }

    public static boolean isEmptyColumn(Object columnValue)
    {
        if(columnValue == null) return true;
        return columnValue instanceof String && StringUtils.isEmpty((String) columnValue);
    }
}
