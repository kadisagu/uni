/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

/**
 * @author vip_delete
 * @since 19.10.2009
 */
public class HtmlValidator
{
//    public static void main(String[] args) throws Exception
//    {
//        validateMenuPages();
//    }
//
//    private static Element[] findJwcid(Element owner, String name)
//    {
//        List<Element> elements = owner.elements();
//        for (Element item : elements)
//        {
//            String jwcid = item.attributeValue("jwcid");
//            if (jwcid != null && jwcid.endsWith(name))
//            {
//                return new Element[]{item, owner};
//            } else
//            {
//                Element[] s = findJwcid(item, name);
//                if (s != null)
//                    return s;
//            }
//        }
//        return null;
//    }
//
//    private static Element[] findJwcidNotDiv(Element owner, String name)
//    {
//        List<Element> elements = owner.elements();
//        for (Element item : elements)
//        {
//            String jwcid = item.attributeValue("jwcid");
//            if (jwcid != null && jwcid.endsWith(name) && !"div".equals(item.getName()))
//            {
//                return new Element[]{item, owner};
//            } else
//            {
//                Element[] s = findJwcidNotDiv(item, name);
//                if (s != null)
//                    return s;
//            }
//        }
//        return null;
//    }
//
//    private static Element[] findActionsWrapper(Element owner, String fileName)
//    {
//        List<Element> elements = owner.elements();
//        for (Element item : elements)
//        {
//            String clazz = item.attributeValue("class");
//            if ("actions-wrapper".equals(clazz))
//            {
//                if (!item.getName().equals("td"))
//                {
//                    throw new RuntimeException("invalid state: actions-wrapper not in <td>:" + fileName);
//                }
//                return new Element[]{item, owner};
//            } else
//            {
//                Element[] s = findActionsWrapper(item, fileName);
//                if (s != null)
//                    return s;
//            }
//        }
//        return null;
//    }
//
//    private static Element[] findEmptyDiv(Element owner)
//    {
//        List<Element> elements = owner.elements();
//        for (Element item : elements)
//        {
//            if ((item.getName().equals("div") || item.getName().equals("span")) && item.attributes().isEmpty())
//            {
//                return new Element[]{item, owner};
//            } else
//            {
//                Element[] s = findEmptyDiv(item);
//                if (s != null)
//                    return s;
//            }
//        }
//        return null;
//    }
//
//    private static Element[] findBr(Element owner)
//    {
//        List<Element> elements = owner.elements();
//        for (Element item : elements)
//        {
//            if (item.getName().equals("br"))
//            {
//                return new Element[]{item, owner};
//            } else
//            {
//                Element[] s = findBr(item);
//                if (s != null)
//                    return s;
//            }
//        }
//        return null;
//    }
//
//    private static Element[] findDivWrapper(Element owner)
//    {
//        List<Element> elements = owner.elements();
//        for (Element item : elements)
//        {
//            if (item.getName().equals("div") && "wrapper".equals(item.attributeValue("class")))
//            {
//                return new Element[]{item, owner};
//            } else
//            {
//                Element[] s = findDivWrapper(item);
//                if (s != null)
//                    return s;
//            }
//        }
//        return null;
//    }
//
//    private static boolean checkTR(Element root)
//    {
//        for (Element item : (List<Element>) root.elements())
//        {
//            if ("tr".equals(item.getName()))
//            {
//                for (Element e : (List<Element>) item.elements())
//                {
//                    if (!"td".equals(e.getName()))
//                        return true;
//                }
//            }
//            if (checkTR(item))
//                return true;
//        }
//        return false;
//    }
//
//    private static boolean checkTD(Element root)
//    {
//        for (Element item : (List<Element>) root.elements())
//        {
//            if ("td".equals(item.getName()))
//            {
//                if (!"tr".equals(item.getParent().getName()))
//                    return true;
//            }
//            if (checkTD(item))
//                return true;
//        }
//        return false;
//    }
//
//    private static void validateMenuPages() throws Exception
//    {
//        Set<String> nostickers = new TreeSet<String>();
//        Set<String> nosec = new TreeSet<String>();
//        Set<String> tabs = new TreeSet<String>();
//
//        // грузим все *.components.xml
//        // name -> set of main html files
//        Map<String, Set<File>> name2file = new HashMap<String, Set<File>>();
//
//        for (Object item : FileUtils.listFiles(new File("Z:/"), new String[]{"components.xml"}, true))
//        {
//            File f = (File) item;
//            String path = f.getPath();
//            if (path.contains("target") || path.contains("fckeditor") || path.contains("tgsa")) continue;
//
//            Document document = Dom4jUtil.readStream(new FileInputStream(f));
//            for (Element element : (List<Element>) document.getRootElement().elements())
//            {
//                if (element.getName().equals("component"))
//                {
//                    String n = element.attributeValue("name");
//                    String p = element.attributeValue("package");
//                    String d = element.attributeValue("default-page");
//
//                    if (p == null)
//                        throw new RuntimeException("package is null at: " + ((File) item).getAbsolutePath());
//                    if (n == null)
//                        n = p;
//
//                    Set<File> files = name2file.get(n);
//                    if (files == null)
//                        name2file.put(n, files = new HashSet<File>());
//                    if (d != null)
//                    {
//                        String[] parts = StringUtils.split(d, '.');
//                        String fileName = "Z:/" + (parts[0].equals("ru") ? "tu" : "tf") + "/" + parts[2] + "/src/main/java/" + d.replace('.', '/') + ".html";
//                        File file = new File(fileName);
//                        if (!file.exists())
//                            throw new RuntimeException("File for default-page not found:" + d);
//                        files.add(file);
//                    } else
//                    {
//                        // simple name
//                        String[] parts = StringUtils.split(p, '.');
//                        String fileName = "Z:/" + (parts[0].equals("ru") ? "tu" : "tf") + "/" + parts[2] + "/src/main/java/" + p.replace('.', '/') + "/" + parts[parts.length - 1] + ".html";
//                        File file = new File(fileName);
//                        if (file.exists())
//                        {
//                            files.add(file);
//                        } else
//                        {
//                            // caf name
//                            String componentSuffix = StringUtils.substringAfterLast(p, ".");
//                            String businessObjectPath = StringUtils.substringBeforeLast(StringUtils.substringBeforeLast(p, "."), ".");
//                            String businessObject = StringUtils.capitalize(StringUtils.substringAfterLast(businessObjectPath, "."));
//                            String page = p + "." + businessObject + componentSuffix;
//                            parts = StringUtils.split(page, '.');
//                            fileName = "Z:/" + (parts[0].equals("ru") ? "tu" : "tf") + "/" + parts[2] + "/src/main/java/" + p.replace('.', '/') + "/" + parts[parts.length - 1] + ".html";
//                            file = new File(fileName);
//                            if (file.exists())
//                            {
//                                files.add(file);
//                            } else
//                            {
//                                // possible there are no htmls
//                                File dir = new File("Z:/" + (parts[0].equals("ru") ? "tu" : "tf") + "/" + parts[2] + "/src/main/java/" + p.replace('.', '/'));
//                                if (!dir.exists())
//                                    throw new RuntimeException("No directory for component:" + p);
//
//                                Collection<File> list = FileUtils.listFiles(dir, new String[]{"html"}, false);
//                                if (!list.isEmpty())
//                                    //throw new RuntimeException("Possible invalid html in component: " + n);
//                                    System.out.println("Possible invalid html in component: " + n);
//                            }
//                        }
//                    }
//
//                    if (files.isEmpty())
//                    {
//                        name2file.remove(n);
//                        // возможно это компонент печати. возмем контроллер
//                        String[] parts = StringUtils.split(p, '.');
//                        String fileName = "Z:/" + (parts[0].equals("ru") ? "tu" : "tf") + "/" + parts[2] + "/src/main/java/" + p.replace('.', '/') + "/Controller.java";
//                        File cf = new File(fileName);
//                        if (!cf.exists())
//                            System.out.println("No controller for component: " + n);
//                        else
//                        {
//                            List<String> lines = FileUtils.readLines(cf, "UTF-8");
//                            int i = 0;
//                            while (i < lines.size() && !lines.get(i).contains("IDocumentRendererController")) i++;
//                            if (i == lines.size())
//                                System.out.println("No html files for component: " + n);
//                        }
//                    }
//                }
//            }
//        }
//
//        System.out.println("              WHERE ARE " + name2file.size() + " COMPONENTS");
//
//        // исключаем файлы-кастомайзеры
//        for (Map.Entry<String, Set<File>> entry : name2file.entrySet())
//        {
//            Set<File> files = entry.getValue();
//
//            if (files.size() != 1)
//            {
//                for (Iterator<File> i = files.iterator(); i.hasNext();)
//                {
//                    File f = i.next();
//                    String v = FileUtils.readFileToString(f, "UTF-8");
//                    if (v.contains("merge:action=\""))
//                        i.remove();
//                }
//            }
//
//            if (files.isEmpty())
//                throw new RuntimeException("All files has merge:action: " + entry.getKey());
//        }
//
//        int withTabs = 0;
//        for (Map.Entry<String, Set<File>> entry : name2file.entrySet())
//        {
//            for (File file : entry.getValue())
//            {
//                // перед нами компонент и его html файл.
//
//                if (file.getAbsolutePath().contains("tandemframework")) continue;
//
//                String fileString = FileUtils.readFileToString(file, "UTF-8");
//
//                String html = ("<root>" + fileString + "</root>").replace("&", "anraka6745tygdabra");
//                Document page = Dom4jUtil.readStream(new ByteArrayInputStream(html.getBytes("UTF-8")));
//                Element block = page.getRootElement();
//
//                // выкинем все mIf
//
//                for (String forDel : new String[]{"mIf", "mElse", "mFor", "@For"})
//                {
//                    Element[] ford;
//                    while ((ford = findJwcid(block, forDel)) != null)
//                    {
//                        List<Element> sub = ford[0].elements();
//                        if (!ford[1].remove(ford[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//                        for (Element s : sub)
//                        {
//                            s.setParent(null);
//                            ford[1].add(s);
//                        }
//                    }
//                }
//
//                // check all tables:
//                if (checkTR(page.getRootElement()))
//                    System.out.println("check TR failed: " + file.getAbsolutePath());
//
//                if (checkTD(page.getRootElement()))
//                    System.out.println("check TD failed: " + file.getAbsolutePath());
//
//                Element[] eTabPanel = findJwcid(block, "mTabPanel");
//
//                if (eTabPanel != null)
//                {
//                    // с табами ниче не делаем!
//                    tabs.add(file.getAbsolutePath());
//                    withTabs++;
//                } else
//                {
//                    // переднами простой компонент без табов. таких компонентов больше 1000
//                    // логично предложить, что такой компонент состоит только из одной html. сейчас мы ее разберем и
//                    // добавим в нее <div class="wrapper"> в нужное место. а заодно стике поставим в нужное место.
//
//                    // удаляем из документа блок, стикер и скоуп
//                    Element[] eBlock = findJwcid(block, "mBlock");
//                    String secObj = eBlock == null ? null : eBlock[0].attributeValue("securedObject");
//
//                    List<Element> blockColumns = new ArrayList<Element>();
//
//                    // вычленяем block column'ы
//                    Element[] blockColumn;
//                    while ((blockColumn = findJwcid(block, "BlockColumn@Block")) != null)
//                    {
//                        if (!blockColumn[1].remove(blockColumn[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//                        blockColumn[0].setParent(null);
//                        blockColumn[0].setName("div");
//                        blockColumns.add(blockColumn[0]);
//                    }
//
//                    if (eBlock != null)
//                    {
//                        if (secObj == null)
//                            throw new RuntimeException("secObj is null: " + file.getAbsolutePath());
//
//                        List<Element> sub = eBlock[0].elements();
//                        if (!eBlock[1].remove(eBlock[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//                        for (Element s : sub)
//                        {
//                            s.setParent(null);
//                            eBlock[1].add(s);
//                        }
//                    } else
//                    {
//                        if (!file.getAbsolutePath().endsWith("Add.html") && !file.getAbsolutePath().endsWith("Edit.html"))
//                        {
//                            nosec.add(file.getAbsolutePath());
//                        }
//                    }
//
//                    // do not modify page with 2 or more stickers
//                    if (findJwcid(block, "mBlock") != null)
//                    {
//                        System.out.println("2 sec block! " + file.getAbsolutePath());
//                        continue;
//                    }
//
//                    Element[] eSticker = findJwcid(block, "mSticker");
//
//                    if (eSticker == null)
//                    {
//                        nostickers.add(file.getAbsolutePath());
//                    }
//
//                    if (eSticker != null)
//                        if (!eSticker[1].remove(eSticker[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//
//
//                    // do not modify page with 2 or more stickers
//                    if (findJwcid(block, "mSticker") != null)
//                    {
//                        if (!file.getAbsolutePath().contains("defaultTheme"))
//                        {
//                            System.out.println("2 stickers! " + file.getAbsolutePath());
//                            continue;
//                        }
//                    }
//
//                    Element[] eScope = findJwcid(block, "mRenderComponentRegion");
//
//                    if (eScope != null)
//                    {
//                        List<Element> sub = eScope[0].elements();
//                        if (!eScope[1].remove(eScope[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//                        for (Element s : sub)
//                        {
//                            s.setParent(null);
//                            eScope[1].add(s);
//                        }
//                    }
//
//                    // do not modify page with 2 or more scopes
//                    if (findJwcid(block, "mRenderComponentRegion") != null)
//                    {
//                        System.out.println("2 scope! " + file.getAbsolutePath());
//                        continue;
//                    }
//
//                    // давайте еще удалим пустые <div> и <span>
//                    Element[] emptyDiv;
//                    while ((emptyDiv = findEmptyDiv(block)) != null)
//                    {
//                        List<Node> sub = emptyDiv[0].content();
//                        if (!emptyDiv[1].remove(emptyDiv[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//                        for (Node s : sub)
//                        {
//                            s.setParent(null);
//                            emptyDiv[1].add(s);
//                        }
//                    }
//
//                    // удаляем <div class="wrapper">
//                    Element[] divWrapper = findDivWrapper(block);
//                    if (divWrapper != null)
//                    {
//                        List<Element> sub = divWrapper[0].elements();
//                        if (!divWrapper[1].remove(divWrapper[0]))
//                            throw new RuntimeException("child is not removed:" + file.getAbsolutePath());
//                        for (Element s : sub)
//                        {
//                            s.setParent(null);
//                            divWrapper[1].add(s);
//                        }
//                    }
//
//                    // заменяем span на div у mInfoTable, mIf, mElse, mSearchList и тп
//                    for (String jwcid : new String[]{"mInfoTable", "mInfoProperty", "mSubmit", "mFor", "mTableRow", "mCheckbox", "mLabelField", "mSelect", "mTextField", "mIf", "mElse", "mSearchList", "mRenderPage"})
//                    {
//                        Element[] tag;
//                        while ((tag = findJwcidNotDiv(block, jwcid)) != null)
//                            tag[0].setName("div");
//                    }
//
//                    // находим actions-wrapper
//                    Element[] actionsWrapper = findActionsWrapper(block, file.getAbsolutePath());
//                    if (actionsWrapper != null)
//                    {
//                        Element table = actionsWrapper[0].getParent().getParent();
//                        table.addAttribute("width", "100%");
//                        table.addAttribute("border", "0");
//                        table.addAttribute("cellpadding", "0");
//                        table.addAttribute("cellspacing", "0");
//
//                        if (findBr(actionsWrapper[0]) != null)
//                            System.out.println("br in actions-wrapper! " + file.getAbsolutePath());
//
//                        Element[] mif = findJwcid(actionsWrapper[0], "mIf");
//                        if (mif == null)
//                        {
//                            if (actionsWrapper[0].elements().size() > 2)
//                            {
//                                List<Element> list = new ArrayList<Element>();
//                                for (Element sub : (List<Element>) actionsWrapper[0].elements())
//                                    list.add(sub);
//
//                                String preLastText = list.get(list.size() - 2).getText();
//                                String lastText = list.get(list.size() - 1).getText();
//
//                                if ("Сбросить".equals(lastText) && "Применить".equals(preLastText))
//                                {
//                                    // выбрасываем фильтр
//                                    boolean hasFilter = false;
//                                    for (Element sub : (List<Element>) actionsWrapper[0].elements())
//                                        if ("filter".equals(sub.attributeValue("class")))
//                                            hasFilter = true;
//                                    if (!hasFilter)
//                                        System.out.println("No Filter: " + file.getAbsolutePath());
//                                }
//                            }
//                        }
//                    }
//
////                    // создаем нормальный документ.
////                    // вначале идет блок, если он есть.
////
////                    StringBuilder b = new StringBuilder();
////                    if (eBlock != null)
////                    {
////                        b.append("<div jwcid=\"@tandem:mBlock\" securedObject=\"" + secObj + "\"");
////                        if (permissionKey != null)
////                            b.append(" permissionKey=\"" + permissionKey + "\"");
////                        b.append(">\n");
////                    }
////
////                    if (eScope != null)
////                        b.append("<div jwcid=\"@tandem:mRenderComponentRegion\">\n");
////
////                    if (eSticker != null)
////                        b.append(eSticker[0].asXML()).append("\n");
////
////                    b.append("<div class=\"wrapper\">\n    ");
////                    for (Element s : (List<Element>) block.elements())
////                        b.append(s.asXML()).append("\n");
////                    b.append("</div>\n");
////
////                    for (Element bc : blockColumns)
////                        b.append(bc.asXML()).append("\n");
////
////                    if (eScope != null)
////                        b.append("</div>\n");
////
////                    if (eBlock != null)
////                        b.append("</div>\n");
////
////                    String newHtml = b.toString();
////
////                    newHtml = newHtml.replace("anraka6745tygdabra", "&");
////                    List<String> lines = IOUtils.readLines(new ByteArrayInputStream(newHtml.getBytes("UTF-8")));
////
////                    for (Iterator<String> line = lines.iterator(); line.hasNext();)
////                    {
////                        String s = line.next();
////                        if (StringUtils.isWhitespace(s))
////                            line.remove();
////                    }
////
////                    FileOutputStream o = new FileOutputStream(file);
////                    for (String s : lines)
////                    {
////                        o.write(s.getBytes("UTF-8"));
////                        o.write('\n');
////                    }
////                    o.close();
//                }
//            }
//        }
//
//        System.out.println("              WHERE ARE " + withTabs + " COMPONENTS WITH mTabPanel");
//
//        String prev = null;
//        FileOutputStream nostickfile = new FileOutputStream("c:/nostickfile.txt");
//        for (String nostick : nostickers)
//        {
//            String module = StringUtils.split(nostick, '\\')[2];
//            if (prev == null || !prev.equals(module))
//            {
//                prev = module;
//                nostickfile.write('\n');
//                nostickfile.write(module.getBytes());
//                nostickfile.write('\n');
//            }
//            nostickfile.write(nostick.getBytes());
//            nostickfile.write('\n');
//        }
//        nostickfile.close();
//
//        FileOutputStream nosecfile = new FileOutputStream("c:/nosecfile.txt");
//        for (String nosecx : nosec)
//        {
//            String module = StringUtils.split(nosecx, '\\')[2];
//            if (prev == null || !prev.equals(module))
//            {
//                prev = module;
//                nosecfile.write('\n');
//                nosecfile.write(module.getBytes());
//                nosecfile.write('\n');
//            }
//            nosecfile.write(nosecx.getBytes());
//            nosecfile.write('\n');
//        }
//        nosecfile.close();
//
//        FileOutputStream tabfile = new FileOutputStream("c:/tabfile.txt");
//        for (String tab : tabs)
//        {
//            String module = StringUtils.split(tab, '\\')[2];
//            if (prev == null || !prev.equals(module))
//            {
//                prev = module;
//                tabfile.write('\n');
//                tabfile.write(module.getBytes());
//                tabfile.write('\n');
//            }
//            tabfile.write(tab.getBytes());
//            tabfile.write('\n');
//        }
//        tabfile.close();
//
//        System.exit(1);
//        // грузим все *.module.xml
//
//        for (Object item : FileUtils.listFiles(new File("Z:/tu"), new String[]{"module.xml"}, true))
//        {
//            File f = (File) item;
//            String path = f.getPath();
//            if (path.contains("target") || path.contains("fckeditor")) continue;
//
//            Document document = Dom4jUtil.readStream(new FileInputStream(f));
//            for (Element element : (List<Element>) document.getRootElement().elements())
//                analizeDeep(element, name2file);
//        }
//    }
//
//    private static void analizeDeep(Element element, Map<String, Set<File>> name2package) throws Exception
//    {
//        if (!"menu".equals(element.getName())) return;
//
//        String name = element.attributeValue("component-name");
//        if (name != null)
//        {
//            Set<File> set = name2package.get(name);
//            if (set == null)
//                throw new RuntimeException("Unknown component: " + name);
//
//            String key = element.attributeValue("permission-key");
//            if (key == null)
//                System.out.println("no permission-key on menu item for component: " + name);
//
//            for (File file : set)
//            {
//                try
//                {
//                    String html = new String(IOUtils.toByteArray(new FileInputStream(file)), "UTF-8").replace('&', ' ');
//                    Document page = Dom4jUtil.readStream(new ByteArrayInputStream(html.getBytes("UTF-8")));
//                    Element block = page.getRootElement();
//                    String keyOnPage = block.attributeValue("permissionKey");
//                    if (keyOnPage == null)
//                        System.out.println("no permission-key on menu-page: " + file.getAbsolutePath());
//                    else if (!keyOnPage.equals(key))
//                    {
//                        //if (!"ru.tandemservice.uni.component.orgunit.academy.AcademyPub".equals(component))
//                        System.out.println("permission-key on menu-page not equals permission-key on menu-item: " + file.getAbsolutePath());
//                    }
//                } catch (DocumentException e)
//                {
//                    System.out.println("no security on component: " + file.getAbsolutePath());
//                }
//            }
//        }
//
//        for (Element sub : (List<Element>) element.elements())
//            analizeDeep(sub, name2package);
//    }
}
