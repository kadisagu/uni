/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentPersonCard;

import org.tandemframework.rtf.document.RtfDocument;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author ekachanova
 */
public interface IData
{
    String FILENAME = "StudentCard.rtf";

    String getFileName();
    void setFileName(String fileName);
    RtfDocument getTemplate();
    void setTemplate(RtfDocument document);
    Student getStudent();
    String[][] getPersonEducationData();
    String[][] getRelativesData();
}
