/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni;

/**
 * Список компонентов модуля UNI на которые ссылаются бизнесс компоненты
 * <p/>
 * Плюс такого интерфейса в том, что компилятор делает текстовые "inline" вставки в код, вместо ссылок
 *
 * @author vip_delete
 */
public interface IUniComponents
{
    /** use BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer((Long) component.getListenerParameter()), true); */
    @Deprecated
    String DOWNLOAD_STORABLE_REPORT = "ru.tandemservice.uni.component.reports.DownloadStorableReport";

    /** use BusinessComponentUtils.downloadDocument(); */
    @Deprecated
    String PRINT_REPORT = "ru.tandemservice.uni.component.reports.PrintReport";

    // общее
    String DOCUMENT_ADD = "ru.tandemservice.uni.component.documents.DocumentAdd";
    String ORDER_DATA_EDIT = "ru.tandemservice.uni.component.student.OrderDataEdit";

    // направления подготовки
    String DEVELOP_COMBINATION_ADD_EDIT = "ru.tandemservice.uni.component.settings.CombinationsFormsAndConditionsAddEdit";
    String EDUCATION_LEVEL_VIEW_OBJECT_PUB = "ru.tandemservice.uni.component.settings.educationLevel.EducationLevelViewObjectPub";
    String EDUCATION_ORG_UNIT_ADD_EDIT = "ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitAddEdit";

    // студенты и группы
    String CAPTAIN_STUDENT_ASSIGN = "ru.tandemservice.uni.component.group.CaptainStudentAssign";
    String GROUP_ADD = "ru.tandemservice.uni.component.group.GroupAdd";
    String GROUP_EDIT = "ru.tandemservice.uni.component.group.GroupEdit";
    String GROUP_HEADER_ASSIGN = "ru.tandemservice.uni.component.group.GroupHeaderAssign";
    String GROUP_STUDENT_LIST = "ru.tandemservice.uni.component.group.GroupStudentList";
    String GROUP_TITLE_MANAGEMENT_EDIT = "ru.tandemservice.uni.component.settings.GroupTitleManagementEdit";
    String STUDENT_ADD = "ru.tandemservice.uni.component.student.StudentAdd";
    String STUDENT_PUB = "ru.tandemservice.uni.component.student.StudentPub";
    String STUDENT_ADDITIONAL_EDIT = "ru.tandemservice.uni.component.student.StudentAdditionalEdit";
    String STUDENT_ARCHIVAL_EDIT = "ru.tandemservice.uni.component.student.StudentArchivalEdit";
    String STUDENT_COMMENT_EDIT = "ru.tandemservice.uni.component.student.StudentCommentEdit";
    String STUDENT_EDIT = "ru.tandemservice.uni.component.student.StudentEdit";
    String STUDENT_ENTRANCE_YEAR_EDIT = "ru.tandemservice.uni.component.student.StudentEntranceYearEdit";
    String STUDENT_PERSON_CARD = "ru.tandemservice.uni.component.student.StudentPersonCard";
    String STUDENT_PERSON_CARDS_GROUP_LIST = "ru.tandemservice.uni.component.group.StudentPersonCards";
    String STUDENT_PERSON_CARDS_ORG_UNIT_LIST = "ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards";
    String STUDENT_GROUP_CHANGE = "ru.tandemservice.uni.component.student.StudentGroupChange";
    String STUDENTS_GROUPS_LIST_PRINT = "ru.tandemservice.uni.component.reports.studentsGroupsList.Add";
    String STUDENTS_EDU_ORG_UNIT_CHANGE = "ru.tandemservice.uni.component.group.StudentsEduOrgUnitChange";
    String ORGUNIT_STUDENT_LIST = "ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentList";

    // сотрудники
    String EMPLOYEE_POST_ADD_EDIT = "ru.tandemservice.uni.component.employee.EmployeePostAddEdit";

    // настройки
    String ORGUNIT_ADD = "ru.tandemservice.uni.component.settings.educationLevel.OrgUnitAdd";
    String ORGUNIT_TYPE_RESTRICTION_ADD = "ru.tandemservice.uni.component.settings.OrgUnitTypeRestrictionAdd";
}