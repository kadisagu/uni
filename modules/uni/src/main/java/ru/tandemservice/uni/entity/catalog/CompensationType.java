/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.entity.catalog;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.gen.CompensationTypeGen;

public class CompensationType extends CompensationTypeGen
{
    @Override
    @EntityDSLSupport
    public boolean isBudget()
    {
        return getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET);
    }

    /**
     * @return короткое название с большой буквы для использование в селектах (propertyView="capitalizedShortTitle")
     */
    public String getCapitalizedShortTitle()
    {
        return StringUtils.capitalize(getShortTitle());
    }
}