/* $Id$ */
package ru.tandemservice.uni.services;

import java.util.Collection;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 15.08.2012
 */
public interface IStudentEduContractDataService
{
    public static final String BEAN_NAME = "studentContractDataService";

    /**
     * Возвращает данные о договоре на обучение по идентификатору студента
     *
     * @param studentId - идентификатор студента
     * @return - данные о договоре на обучении (номер и дата)
     */
    ContractAttributes getStudentContractAttributes(Long studentId);

    /**
     * Возвращает мап данных о договорах на обучение по коллекции идентификаторов студентов
     *
     * @param studentIds - коллекция идентификаторов студентов
     * @return - мап данных о договорах на обучении (номер и дата)
     *         ключ в мапе - идентификатор студента, значение - данные о договоре на обучение
     */
    Map<Long, ContractAttributes> getStudentContractAttributes(Collection<Long> studentIds);
}