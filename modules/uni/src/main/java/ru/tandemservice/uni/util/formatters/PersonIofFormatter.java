/* $Id$ */
package ru.tandemservice.uni.util.formatters;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.person.base.entity.IdentityCard;

/**
 * @author Alexey Lopatin
 * @since 26.08.2013
 */
public class PersonIofFormatter implements IFormatter<IdentityCard>
{
    public static final PersonIofFormatter INSTANCE = new PersonIofFormatter();

    @Override
    public String format(IdentityCard identityCard)
    {
        String lastName = identityCard.getLastName();
        String firstName = identityCard.getFirstName();
        String middleName = identityCard.getMiddleName();
        StringBuilder str = new StringBuilder();

        str.append(firstName.substring(0, 1).toUpperCase()).append(".");
        if (StringUtils.isNotEmpty(middleName))
            str.append(middleName.substring(0, 1).toUpperCase()).append(".");
        str.append(lastName);

        return String.valueOf(str);
    }
}
