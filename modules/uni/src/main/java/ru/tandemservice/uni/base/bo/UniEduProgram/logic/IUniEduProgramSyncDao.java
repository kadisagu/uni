package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import java.util.Set;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SingleValueCache;

import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

/**
 * @author vdanilov
 */
public interface IUniEduProgramSyncDao extends INeedPersistenceSupport {

    SingleValueCache<IUniEduProgramSyncDao> instance = new SingleValueCache<IUniEduProgramSyncDao>() {
        @Override protected IUniEduProgramSyncDao resolve() {
            return UniEduProgramManager.instance().syncDao();
        }
    };

    /**
     * Сохарняем НПв
     * @param eduHs НПв
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveEducationLevelsHighSchool(EducationLevelsHighSchool eduHs);


    /**
     * Ищет НПм ВО для пары направление+направленность
     *
     * @param subject направление проф. образования
     * @param specialization направленность. null == общая направленность
     * @return НПм ВПО
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EducationLevels getEduLvl4High(EduProgramSubject subject, EduProgramSpecialization specialization);

    /**
     * Создает НПм ВО для пары направление+направленность
     *
     * @param subject направление проф. образования
     * @param specialization направленность
     * @return НПм ВПО
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EducationLevels createOrGetEduLvl4High(EduProgramSubject subject, EduProgramSpecialization specialization);

    /**
     * Создает НПм СПО, если подходящего нет. Если есть - возвращает его.
     *
     * @param subject направление проф. образования
     * @param advancedLevel повышенный уровень
     * @return НПм СПО
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EducationLevelMiddle createOrGetEduLvl4Middle(EduProgramSubject subject, boolean advancedLevel);

    /**
     * Ищет НПм СПО
     *
     * @param subject направление проф. образования
     * @param advancedLevel повышенный уровень
     * @return НПм СПО
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    EducationLevelMiddle getEduLvl4Middle(EduProgramSubject subject, boolean advancedLevel);

    /**
     * вызывается перед удалением неактуальных EduProgramSubject - удаляет все направления и направленности, ссылающиеся на такие EduProgramSubject,
     * если по ним не было других связанных объектов
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doCleanUpSubjectIndexDataCallback();


    /**
     * @param eduLvl
     * @return { eduProgramSubjectIndex.code } перечень допустимых кодов классификаторов направлений ОП для НПм
     */
    Set<String> getSubjectIndexCodeSet(EducationLevels eduLvl);

    /**
     * @param subjectIndexCode eduProgramSubjectIndex.code
     * @return класс НПм для кода перечня направлений ОП
     */
    Class<? extends EducationLevels> getEduLvlClass(String subjectIndexCode);

}
