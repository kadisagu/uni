/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSettings.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.entity.EduAccreditation;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.LicenseInOrgUnit;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;
import ru.tandemservice.uniedu.entity.gen.EduAccreditationGen;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgram;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 23.11.2016
 */
public class UniAccreditationDao extends UniBaseDao implements IUniAccreditationDao
{

    @Override
    public void saveOrUpdateAccreditation(DataWrapper row, EduInstitutionOrgUnit orgUnit, EduProgramSubjectIndex subjectIndex)
    {
       if (!(row.getWrapped() instanceof IPersistentAccreditationOwner))
           return;
        IPersistentAccreditationOwner owner = row.getWrapped();
        EduAccreditation accreditation = getByNaturalId(new EduAccreditationGen.NaturalId(owner, orgUnit, subjectIndex));
        StateAccreditationEnum state = (StateAccreditationEnum) row.getProperty(AccreditationSearchListDSHandler.PROPERTY_ACCR_STATE);
        if (state == null)
        {
            if (accreditation != null)
                delete(accreditation);
            return;
        }
        if (accreditation == null)
            accreditation = new EduAccreditation(owner, orgUnit, subjectIndex);

        accreditation.setStateAccreditation(state);
        saveOrUpdate(accreditation);
    }

    @Override
    public void updateProgramSubject(Collection<Long> programSubjectIds, boolean license, EduInstitutionOrgUnit orgUnit)
    {
        if (!license)
        {
            new DQLDeleteBuilder(LicenseInOrgUnit.class)
                    .where(eq(property(LicenseInOrgUnit.institutionOrgUnit()), value(orgUnit)))
                    .where(in(property(LicenseInOrgUnit.programSubject()), programSubjectIds)).createStatement(getSession()).execute();
        }
        else
        {
            List<Long> existingLicense = getList(new DQLSelectBuilder().fromEntity(LicenseInOrgUnit.class, "lic")
                                                    .column(property("lic", LicenseInOrgUnit.programSubject().id()))
                                                    .where(eq(property("lic", LicenseInOrgUnit.institutionOrgUnit()), value(orgUnit)))
                                                    .where(in(property("lic", LicenseInOrgUnit.programSubject().id()), programSubjectIds)));
            programSubjectIds.removeAll(existingLicense);

            for (List<Long> elements : Lists.partition(Lists.newArrayList(programSubjectIds), DQL.MAX_VALUES_ROW_NUMBER)) {
                final DQLInsertValuesBuilder insertDql = new DQLInsertValuesBuilder(LicenseInOrgUnit.class);
                insertDql.value(LicenseInOrgUnit.L_INSTITUTION_ORG_UNIT, orgUnit);
                for (Long row : elements)
                {
                    insertDql.value(LicenseInOrgUnit.L_PROGRAM_SUBJECT, row);
                    insertDql.addBatch();
                }
                insertDql.createStatement(getSession()).execute();
            }
        }
    }
}