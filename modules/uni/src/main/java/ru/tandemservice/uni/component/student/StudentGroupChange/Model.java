/**
 * $Id$
 */
package ru.tandemservice.uni.component.student.StudentGroupChange;

import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author dseleznev
 *         Created on: 02.07.2008
 */
@Input(keys = "studentId", bindings = "studentId")
public class Model
{
    private Long _studentId;
    private Student _student;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private ICatalogItem _course;
    private Group _group;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private List<Course> _coursesList;
    private ISelectModel _groupsList;

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public ICatalogItem getCourse()
    {
        return _course;
    }

    public void setCourse(ICatalogItem course)
    {
        _course = course;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public List<Course> getCoursesList()
    {
        return _coursesList;
    }

    public void setCoursesList(List<Course> coursesList)
    {
        _coursesList = coursesList;
    }

    public ISelectModel getGroupsList()
    {
        return _groupsList;
    }

    public void setGroupsList(ISelectModel groupsList)
    {
        _groupsList = groupsList;
    }
}