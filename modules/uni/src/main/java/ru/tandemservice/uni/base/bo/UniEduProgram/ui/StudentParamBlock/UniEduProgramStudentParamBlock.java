/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.StudentParamBlock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.UniEduProgramEduOuDSHandler;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author oleyba
 * @since 8/14/14
 */
@Configuration
public class UniEduProgramStudentParamBlock extends BusinessComponentManager
{
    public static final String EDU_OU_DS = "eduOuDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(searchListDS(EDU_OU_DS, eduOuDS(), eduOuDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eduOuDS()
    {
        return columnListExtPointBuilder(EDU_OU_DS)
            .addColumn(publisherColumn("title", EducationOrgUnit.title().s()).create())
            .addColumn(publisherColumn("eduHs", EducationOrgUnit.educationLevelHighSchool().shortTitle().s())
                .primaryKeyPath(EducationOrgUnit.educationLevelHighSchool().id().s())
                .create())
            .addColumn(textColumn("formativeOrgUnit", EducationOrgUnit.formativeOrgUnit().fullTitle().s()).order().create())
            .addColumn(textColumn("territorialOrgUnit", EducationOrgUnit.territorialOrgUnit().territorialFullTitle().s()).order().create())
            .addColumn(textColumn("developForm", EducationOrgUnit.developForm().title().s()).create())
            .addColumn(textColumn("developCondition", EducationOrgUnit.developCondition().title().s()).create())
            .addColumn(textColumn("developTech", EducationOrgUnit.developTech().title().s()).create())
            .addColumn(textColumn("developPeriod", EducationOrgUnit.developPeriod().title().s()).create())
            .addColumn(toggleColumn("used", EducationOrgUnit.used().s())
                .toggleOnListener("onClickChangeUsedEduOu")
                .toggleOffListener("onClickChangeUsedEduOu"))
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduOu"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduOu", new FormattedMessage("eduOuDS.deleteAlert", EducationOrgUnit.titleWithFormAndCondition())))
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduOuDSHandler()
    {
        return new UniEduProgramEduOuDSHandler(getName());
    }
}