/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.FiltersPreset;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.List;

/**
 * @author vip_delete
 * @since 24.10.2008
 */
public interface IEmployeeDAO
{
    final String EMPLOYEE_DAO_BEAN_NAME = "employeeDAO";
    final SpringBeanCache<IEmployeeDAO> instance = new SpringBeanCache<IEmployeeDAO>(EMPLOYEE_DAO_BEAN_NAME);

    // Получает глобальный список шаблонов фильтров для указанного ключа
    List<FiltersPreset> getFilterPresetsList(String settingsKey);

    // получает максимальный незанятый номер для указанного шаблона фильтров
    int getFreeFiltersPresetNumber(String settingsKey);

    // обновляет шаблон фильтров
    void updateFiltersPreset(FiltersPreset preset);

    // удаляет шаблон фильтров вместе с соответствующими настройками
    void deleteFiltersPreset(FiltersPreset preset);

    // возвращает True, если на указанном подразделении имеются активные студенты
    boolean isAnyActiveStudentOnOrgUnit(OrgUnit orgUnit);
}