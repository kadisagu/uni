package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelHighGos2Gen;

/**
 * Направление подготовки (специальность) ВПО (ГОС2)
 */
public class EducationLevelHighGos2 extends EducationLevelHighGos2Gen
{
    public EducationLevelHighGos2()
    {
        setCatalogCode("educationLevelHighGos2");
    }
}