/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.structureEducationLevels.StructureEducationLevelsPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author vip_delete
 */
public class Controller extends DefaultCatalogPubController<StructureEducationLevels, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        final DynamicListDataSource<StructureEducationLevels> dataSource = new DynamicListDataSource<StructureEducationLevels>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(getModel(context), "Название", StructureEducationLevels.P_TITLE).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", "shortTitle").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false).setDisabledProperty("upDisabled"));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false).setDisabledProperty("downDisabled"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        return dataSource;
    }

    public void onClickUp(final IBusinessComponent context)
    {
        this.getDao().updatePriorityUp(this.getModel(context), (Long) context.getListenerParameter());
    }

    public void onClickDown(final IBusinessComponent context)
    {
        this.getDao().updatePriorityDown(this.getModel(context), (Long) context.getListenerParameter());
    }
}
