package ru.tandemservice.uni.util.schema;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IOrgstructSchemaGenerator {

    public static final SpringBeanCache<IOrgstructSchemaGenerator> instance = new SpringBeanCache<IOrgstructSchemaGenerator>(IOrgstructSchemaGenerator.class.getName());

    /**
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=false)
    String getOrgstructSchema();

}
