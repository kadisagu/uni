// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.OrderDataEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author oleyba
 * @since 03.11.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setOrderData(get(OrderData.class, OrderData.L_STUDENT, model.getStudent()));
        if (model.getOrderData() == null)
        {
            model.setOrderData(new OrderData());
            model.getOrderData().setStudent(model.getStudent());
        }
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getOrderData());
    }
}
