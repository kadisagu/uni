/* $Id$ */
package ru.tandemservice.uni.migration;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;

import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil.joinWithSeparator;
import static org.tandemframework.shared.commonbase.utils.MergeBuilder.*;
import static org.tandemframework.shared.commonbase.utils.MigrationUtils.idsToStr;
import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Nikolay Fedorovskih
 * @since 21.01.2016
 */
public class MS_uni_2x9x3_2to3 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    public MergeRule getMainMergeRule(ISQLTranslator translator) throws SQLException {

        // Мерж НПм. Сортируем по убыванию по количеству студентов, групп, НПП, НПв, id и берем первый.
        final String eduLevelsMainSQL;
        {
            final SQLSelectQuery sel = new SQLSelectQuery();
            sel.top(1);
            sel.column("lv.id");
            sel.from(SQLFrom.table("educationlevels_t", "lv")
                             .leftJoin(SQLFrom.table("educationlevelshighschool_t", "npv"),"npv.educationlevel_id=lv.id")
                             .leftJoin(SQLFrom.table("educationorgunit_t", "npp"), "npp.educationlevelhighschool_id=npv.id")
                             .leftJoin(SQLFrom.table("group_t", "g"), "g.educationorgunit_id=npp.id")
                             .leftJoin(SQLFrom.table("student_t", "s"), "s.educationorgunit_id=npp.id")
            );
            sel.where("lv.id in (:ids)");

            sel.group("lv.id");

            sel.order("count(distinct s.id)", false);
            sel.order("count(distinct g.id)", false);
            sel.order("count(distinct npp.id)", false);
            sel.order("count(distinct npv.id)", false);
            sel.order("lv.id", false);

            eduLevelsMainSQL = translator.toSql(sel);
        }

        final Set<String> eduLevelUniqueKey = ImmutableSet.of("eduprogramsubject_id", "eduprogramspecialization_id", "leveltype_id");

        // Дополнительно проверяем, что мержатся НПм с одним кодом справочника, одного класса, уровня образования.
        final Set<String> eduLevelAdditionalColumns = ImmutableSet.of("catalogcode_p", "discriminator");

        return new MergeRule("educationlevels_t", eduLevelUniqueKey, eduLevelAdditionalColumns) {

            @Override protected Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {

                final Set<Object> catalogCodeCheck = new HashSet<>();
                final Set<Object> discriminatorCheck = new HashSet<>();
                for (final Object[] data : groupRows.values()) {
                    catalogCodeCheck.add(data[0]);
                    discriminatorCheck.add(data[1]);
                }
                final String ids = idsToStr(groupRows.keySet());
                if (catalogCodeCheck.size() > 1) {
                    throw new IllegalStateException("Not unique catalog code in grouping education levels: " + joinWithSeparator(", ", catalogCodeCheck) + ". Group ids: " + ids);
                }
                if (discriminatorCheck.size() > 1) {
                    throw new IllegalStateException("Not unique class in grouping education levels: " + joinWithSeparator(", ", discriminatorCheck) + ". Group ids: " + ids);
                }

                return tool.getNumericResult(StringUtils.replace(eduLevelsMainSQL, ":ids", ids));
            }

            @Override protected String getWhere() {
                return "eduprogramsubject_id is not null"; // Мержатся только НПм с направлением
            }
        };
    }

    public MergeRule getEduLvHSMergeRule(ISQLTranslator translator) throws SQLException {

        // Мерж НПв. Сортируем по убыванию по количеству студентов, групп, НПП, id и берем первый.
        final String eduLevelsHSMainSQL;
        {
            final SQLSelectQuery sel = new SQLSelectQuery();
            sel.top(1);
            sel.column("npv.id");
            sel.from(SQLFrom.table("educationlevelshighschool_t", "npv")
                             .leftJoin(SQLFrom.table("educationorgunit_t", "npp"), "npp.educationlevelhighschool_id=npv.id")
                             .leftJoin(SQLFrom.table("group_t", "g"), "g.educationorgunit_id=npp.id")
                             .leftJoin(SQLFrom.table("student_t", "s"), "s.educationorgunit_id=npp.id")
            );
            sel.where("npv.id in (:ids)");

            sel.group("npv.id");

            sel.order("count(distinct s.id)", false);
            sel.order("count(distinct g.id)", false);
            sel.order("count(distinct npp.id)", false);
            sel.order("npv.id", false);

            eduLevelsHSMainSQL = translator.toSql(sel);
        }

        final Set<String> key = ImmutableSet.of("educationlevel_id", "assignedqualification_id", "orgunit_id", "programorientation_id", "title_p");

        return new MergeRule("educationlevelshighschool_t", key, ImmutableSet.of("allowstudents_p")) {

            final Function<Object[], Boolean> hasFlagFunc = row -> Boolean.TRUE.equals(ConvertUtils.convert(row[0], Boolean.class));

            @Override protected Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {

                final String idsStr = idsToStr(groupRows.keySet());
                final long mainRowId = tool.getNumericResult(StringUtils.replace(eduLevelsHSMainSQL, ":ids", idsStr));

                if (!hasFlagFunc.apply(groupRows.get(mainRowId))) {

                    // Если по одному из дублей НПв было обучение студентов, а по оставшемуся - не было, то нужно сохранить этот флаг для оставлегося НПв
                    if (groupRows.values().stream().map(hasFlagFunc).anyMatch(flag -> flag)) {

                        tool.executeUpdate("update educationlevelshighschool_t set allowstudents_p=? where id=?", true, mainRowId);
                        tool.debug("set flag allowStudents for eduLevel " + mainRowId);
                    }
                }

                tool.info(tableName + " [" + idsStr + "] --> " + mainRowId + " (educationlevel_id=" + newLinkId + ")");
                return mainRowId;
            }
        };
    }

    public static String getBigGroupsSql(DBTool tool) throws SQLException  {

        final short dpoEntityCode = tool.entityCodes().get("educationLevelAdditional"); // НПв ДПО

        return "select lv.id from educationlevels_t lv " +
                " inner join structureeducationlevels_t lt on lt.id=lv.leveltype_id " +
                " where (lv.eduprogramsubject_id is null and lv.discriminator <> " + dpoEntityCode + ") or " +
                "       (lt.parent_id is null or lt.code_p in ('2', '12', '16', '19', '21', '31', '35'))";
    }

    private boolean checks(DBTool tool) throws SQLException {

        if (MS_uni_2x9x2_1to2.canAutoFix()) {
            return true;
        }

        final String prefix = tool.getDataSource().getSqlFunctionPrefix();
        final short rootSpecializationCode = tool.entityCodes().get("eduProgramSpecializationRoot");

        boolean hasInvalidData = hasWrongIds(
                tool,
                "select lv.id from educationlevels_t lv " +
                        " join structureeducationlevels_t lt on lt.id=lv.leveltype_id " +
                        " where lv.eduprogramspecialization_id is not null " +
                        " and (" +
                        "      (" + prefix + "getEntityCode(lv.eduprogramspecialization_id)=" + rootSpecializationCode + " and (lt.specialization_p=? or lt.profile_p=?))" +
                        "   or (" + prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode + " and not (lt.specialization_p=? or lt.profile_p=?))" +
                        " ) "
                ,
                "Has education levels with incorrect eduProgramSpecialization class (does not correspond to the level type of)",
                true, true, true, true);

        hasInvalidData |= hasWrongIds(
                tool,
                "select lv.id from educationlevels_t lv " +
                        " join structureeducationlevels_t lt on lt.id=lv.leveltype_id " +
                        " where (lv.eduprogramsubject_id is not null or lv.eduprogramspecialization_id is not null) " +
                        "   and (lt.specialization_p=? or lt.profile_p=?) and exists(select lv2.id from educationlevels_t lv2 where lv2.parentlevel_id=lv.id)"
                ,
                "Has education levels with for specialization or profile with child levels",
                true, true);

        if (hasInvalidData) {
            tool.error("=================================================");
            tool.error("");
            tool.error("Перед обновлением до версии 2.10.0 необходимо исправить ошибки в данных, отраженные в системном действии \"Квалификации профессионального образования в ОП, НПв и УП\" (доступно только в версии 2.9.2).");
            tool.error("");
            tool.error("=================================================");
        }

        return ! hasInvalidData;
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (!tool.columnExists("educationlevels_t", "independenteducation_p")) {
            return; // Миграция уже выполнялась?
        }

        final boolean hasRamecAspirant = tool.getNumericResult("select count(*) from educationlevels_t where catalogcode_p=?", "educationLevelScientific") > 0;

        // Выставляем признак профиля аспирантской направленности (если это не САФУ/ИжГТУ)
        if (!hasRamecAspirant) {
            tool.executeUpdate("update structureeducationlevels_t set profile_p=? where code_p=?", true, "39");
        }


        if ( ! checks(tool) | hasWrongIds(
                tool,
                "select e.id from educationlevels_t e where " +
                        " e.catalogcode_p <> 'educationLevelScientific' and exists (select 1 from educationlevels_t e2 where " +
                        " e2.eduprogramsubject_id = e.eduprogramsubject_id and " +
                        " e2.catalogcode_p <> e.catalogcode_p and e2.catalogcode_p <> 'educationLevelScientific')",
                "Wrong eduProgramSubject or class in some education levels. Fix by hands"
        ))
        {
            // Как это автоматически фиксить - хз
            // Исправлять базу руками, если это тестовый стенд
            throw new IllegalStateException("Invalid data. See log.");

            // Лечение для маленькой базы ДВФУ (которая еще старая, на 6-8 ГБ):
            /*
            delete from MDBVIEWSTUDENTADDITIONALDATA_T where MDBVIEWSTUDENT_ID in (select id from MDBVIEWSTUDENT_T where EDUOU_ID in (select id from EDUCATIONORGUNIT_T where EDUCATIONLEVELHIGHSCHOOL_ID in (select id from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357))));
            delete from MDBVIEWSTUDENT_T where EDUOU_ID in (select id from EDUCATIONORGUNIT_T where EDUCATIONLEVELHIGHSCHOOL_ID in (select id from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357)));
            delete from STUDENTFEFUEXT_T where STUDENT_ID in (select id from STUDENT_T where EDUCATIONORGUNIT_ID in (select id from EDUCATIONORGUNIT_T where EDUCATIONLEVELHIGHSCHOOL_ID in (select id from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357))));
            delete from STUDENT_T where EDUCATIONORGUNIT_ID in (select id from EDUCATIONORGUNIT_T where EDUCATIONLEVELHIGHSCHOOL_ID in (select id from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357)));
            delete from GROUP_T where EDUCATIONORGUNIT_ID in (select id from EDUCATIONORGUNIT_T where EDUCATIONLEVELHIGHSCHOOL_ID in (select id from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357)));
            delete from EDUCATIONORGUNIT_T where EDUCATIONLEVELHIGHSCHOOL_ID in (select id from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357));
            delete from EDUCATIONLEVELSHIGHSCHOOL_T where EDUCATIONLEVEL_ID in (1415491601967882951, 1419026566844261063, 1417959953733788357);
            delete from EDUCATIONLEVELHIGHGOS2_T where id in (1417959953733788357);
            delete from EDUCATIONLEVELS_T where id in (1415491601967882951, 1419026566844261063, 1417959953733788357);
            
            delete from FEFU_APE_PROGRAM_FOR_STUDENT_T;
            delete from FEFU_APE_PROGRAM_T;
            */
        }


        final ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        final String prefix = tool.getDataSource().getSqlFunctionPrefix();

        // Засекаем, сколько было объектов до миграции, чтобы в конце подвести итог
        final long npm_count_before = tool.getNumericResult("select count(*) from educationlevels_t");
        final long npv_count_before = tool.getNumericResult("select count(*) from educationlevelshighschool_t");
        final long npp_count_before = tool.getNumericResult("select count(*) from educationorgunit_t");

        // Удалена настройка "Отображение профилей и специализаций" - удаляем ключик прав, сеттинги и колонку у НПв
        {
            tool.executeUpdate("delete from accessmatrix_t where permissionkey_p=?", "menuUniStudentSettingsSpecializationShow");
            tool.executeUpdate("delete from settings_s where owner_p=?", "uniStudentSettings");
            tool.dropColumn("educationlevels_t", "independenteducation_p");
            tool.debug("Column independenteducation_p dropped from educationlevels_t");
        }

        // Удаляем НПм на урупненные группы
        {
            tool.table("educationlevels_t").indexes().clear();

            final String idsToRemoveSql = getBigGroupsSql(tool);

            // Чистим ссылки на парента (если этот парент будет удален)
            final int parentClean = tool.executeUpdate("update educationlevels_t set parentlevel_id=null where id not in (" + idsToRemoveSql + ") and parentlevel_id in (" + idsToRemoveSql + ")");
            tool.info(parentClean + " rows got parent NULL in educationlevels_t");

            // Чистим в educationLevelHighGos2, у которого есть своя таблица
            final int lv_gos2 = tool.executeUpdate("delete from educationlevelhighgos2_t where id in (" + idsToRemoveSql + ")");
            tool.info(lv_gos2 + " rows for grouping education levels removed from educationlevelhighgos2_t");

            // Удаляем сами НПм
            final int lv = tool.executeUpdate("delete from educationlevels_t where id in (" + idsToRemoveSql + ")");
            tool.info(lv + " rows for grouping education levels removed from educationlevels_t");

            // Проверяем, что укрупненных групп не осталось
            final List<Object[]> bigGroups = tool.executeQuery(processor(1), idsToRemoveSql);
            if (!bigGroups.isEmpty()) {
                throw new IllegalStateException("Education levels for big groups don't removed: " + bigGroups.stream().map(r->String.valueOf(r[0])).collect(Collectors.joining(", ", "[", "]")));
            }
        }

        final short rootSpecializationCode = tool.entityCodes().get("eduProgramSpecializationRoot");

        // Фиксим НПм с общей направленностью, но уровнем специализации/профиля - выставляем родителький уровень образования
        if (MS_uni_2x9x2_1to2.canAutoFix())
        {
            final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
            upd.set("leveltype_id", "lt.parent_id");
            upd.set("parentlevel_id", "null");
            upd.where("lt.specialization_p=? or lt.profile_p=?");
            upd.where(prefix + "getEntityCode(lv.eduprogramspecialization_id)=" + rootSpecializationCode);
            upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("structureeducationlevels_t", "lt"), "lt.id=lv.leveltype_id");

            final int n = tool.executeUpdate(translator.toSql(upd), true, true);
            tool.info(n + " education levels with root specialization and specialization/profile level type -> set parent level type");
        }

        /*
           Далее мержим НПм.

           В рамках одного направления проф.образования из перечня удаляются все НПм "без направленности" (до выбора направленности),
           т.о. в рамках него может существовать только одно НПм и оно будет "общей направленности".
           В рамках НПм "общей направленности" могут существовать другие НПм с конкретными направленностями
           (которые создаются пользователем в рамках направления проф.образования в справочнике).
           Получаем максимум двухуровневую структуру в НПм.
        */

        {
            // Если есть НПм с направленностью и без направления - лечим их, выставляя направление по направленности. Таких быть не должно! Но мало ли.
            {
                final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
                upd.set("eduprogramsubject_id", "sb.programsubject_id");
                upd.where("lv.eduprogramsubject_id is null");
                upd.where("lv.eduprogramspecialization_id is not null");
                upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("edu_specialization_base_t", "sb"), "sb.id=lv.eduprogramspecialization_id");

                final int n = tool.executeUpdate(translator.toSql(upd));
                if (n > 0) {
                    tool.info(n + " rows fixed program subject in educationlevels_t (!!!)");
                }
            }

            // Зануляем общую направленность (и parentLevel чтоб его!!!), чтобы смержить, т.к. для нас общая направленность равна без направленности
            {
                final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
                upd.set("eduprogramspecialization_id", "null");
                upd.set("parentlevel_id", "null");
                upd.where("lv.eduprogramsubject_id is not null");
                upd.where("lv.eduprogramspecialization_id is not null");

                if (MS_uni_2x9x2_1to2.canAutoFix()) {
                    upd.where("(" + prefix + "getEntityCode(lv.eduprogramspecialization_id)=" + rootSpecializationCode + ") " +
                                      // зануляем направленность для НПм с необщей направленностью, но с уровнем специальности/направления или ищеющем детей.
                                      " or (lt.specialty_p=?) " +
                                      " or (lt.profile_p=? and (lt.master_p=? or lt.bachelor_p=?)) " +
                                      " or exists(select lv2.id from educationlevels_t lv2 where lv2.parentlevel_id=lv.id)");
                } else {
                    upd.where("(" + prefix + "getEntityCode(lv.eduprogramspecialization_id)=" + rootSpecializationCode + ")");
                }
                upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("structureeducationlevels_t", "lt"), "lt.id=lv.leveltype_id");

                final int n;
                if (MS_uni_2x9x2_1to2.canAutoFix()) {
                    n = tool.executeUpdate(translator.toSql(upd), true, false, true, true);
                } else {
                    n = tool.executeUpdate(translator.toSql(upd));
                }
                tool.info(n + " rows set null to eduprogramspecialization_id and parentlevel_id for root specializations in educationlevels_t");
            }


            MigrationUtils.mergeBuilder(tool, getMainMergeRule(translator))

                    // uni
                    .addRule(getEduLvHSMergeRule(translator))
                    .addRule(MS_uni_2x9x2_1to2.canAutoFix()
                                     ? byLinkRule("educationlevelshighschool_t", 0, "educationlevel_id", "assignedqualification_id", "orgunit_id", "programorientation_id", "shorttitle_p").printMergedIdsToLog(true).build()
                                     : stopRule("educationlevelshighschool_t", "educationlevel_id", "assignedqualification_id", "orgunit_id", "programorientation_id", "shorttitle_p"))
                    .addRule(byLinkRule("educationorgunit_t", 0, "educationlevelhighschool_id", "formativeorgunit_id", "territorialorgunit_id", "developform_id", "developtech_id", "developcondition_id", "developperiod_id").printMergedIdsToLog(true))

                    //uniepp_load
                    .addRule(byLinkWithSumRule("epl_student_t", "count_p", 0, "educationorgunit_id", "compensationsource_id", "group_id"))
                    .addRule(stopRule("epl_student_t", "educationorgunit_id", "compensationsource_id", "group_id", "developgrid_id")) // Эта штука не сработает, т.к. есть более общий ключ
                    .addRule(byLinkWithSumRule("epl_student2workplan_t", "count_p", 0, "student_id", "workplan_id"))
                    .addRule(byLinkRule("epl_student_wp_slot_t", 0, "student_id", "yearpart_id", "term_id", "activityelementpart_id"))
                    .addRule(byLinkWithSumRule("epl_student_wp2gt_slot_t", "count_p", 0, "studentwpslot_id", "grouptype_id"))
                    .addRule(byLinkWithSumRule("epl_student_wp2gt_slot_t", "count_p", 0, "studentwpslot_id", "grouptype_id", "splitelement_id"))
                    .addRule(byLinkWithSumRule("epl_edu_group_row_t", "count_p", 0, "studentwp2gtypeslot_id", "group_id"))
                    .addRule(skipRule("epl_student_count_4se_t", "student2workplan_id", "regelement_id", "splitelement_id"))

                    // ctr
                    .addRule(byLinkRule("ctrpr_prel_period", 0, "element_id", "durationbegin_p"))
                    .addRule(byLinkRule("ctrpr_prel_cost", 0, "period_id", "paymentgrid_id", "category_id", "currency_id"))
                    .addRule(byLinkRule("ctrpr_prelcost_stage", 0, "cost_id", "stageuniquecode_p"))
                    .addRule(byLinkRule("ctrpr_prelcost_stage", 0, "cost_id", "number_p"))

                    // eductr
                    .addRule(byLinkRule("eductr_ou_config", 0, "educationorgunit_id", "cipher_p", "educationyear_id"))

                    // uniepp
                    .addRule(singleRule("epp_eduou_epv_rel_t", "educationorgunit_id"))

                    // unitutor
                    .addRule(byLinkRule("student2orientation_t", 0, "orientation_id", "student_id"))
                    .addRule(byLinkRule("orientation2eduplanversion_t", 0, "orientation_id", "eduplanversion_id", "educationyear_id"))

                    // unisc
                    .addRule(byLinkRule("sc_eduou_config", 0, "educationorgunit_id", "educationyear_id", "cipher_p"))
                    .addRule(singleRule("sc_eduou_config_relation", "config_id"))

                    // uniec
                    .addRule(byLinkRule("profileeducationorgunit_t", 0, "educationorgunit_id", "enrollmentdirection_id"))
                    .addRule(byLinkRule("priorityprofileeduou_t", 0, "profileeducationorgunit_id", "rqstdenrllmntdrctn_id"))
                    .addRule(byLinkRule("onlinepriorityprofileeduou_t", 0, "profileeducationorgunit_id", "nlnrqstdenrllmntdrctn_id"))
                    .addRule(stopRule("enrollmentdirection_t", "educationorgunit_id", "enrollmentcampaign_id")) // Будем надеяться, что до этого не дойдет...

                    // uniec_fis
                    .addRule(singleRule("ecf_conv_eduou_t", "educationorgunit_id"))

                    // accreditationrmc
                    .addRule(byLinkRule("accreditatedgroup_t", 0, "base_id", "formativeorgunit_id", "branch_id"))

                    // nsi
                    .addRule(skipRule("nsunedprgrmdscplnsttstcs_t", "developform_id", "course_id", "term_id", "qualification_id", "educationlevels_id", "formativeorgunit_id", "territorialorgunit_id"))

                    .merge("educationLevels");

            // Стираем парента, ссылающегося самого на себя
            {
                final int n = tool.executeUpdate("update educationlevels_t set parentlevel_id=null where parentlevel_id=id");
                if (n > 0) {
                    tool.info(n + " education levels set parentLevel=null for self linked");
                }
            }

            // Нужно создать общие направленности для НПм на ВО, для которых их нет
            {
                final List<Object[]> rows = tool.executeQuery(
                        processor(3),
                        "select distinct s.id, s.title_p, s.shorttitle_p from educationlevels_t lv " +
                                " inner join structureeducationlevels_t lt on lv.leveltype_id=lt.id and (lt.high_p=? or lt.higher_p=?) " +
                                " inner join edu_c_pr_subject_t s on s.id=lv.eduprogramsubject_id " +
                                " left join edu_specialization_base_t sb on sb.programsubject_id=s.id and sb.discriminator=?" +
                                " where sb.id is null",
                        true, true, rootSpecializationCode);

                if (!rows.isEmpty()) {

                    final BatchInsertBuilder insert = new BatchInsertBuilder("code_p", "programsubject_id", "title_p", "shorttitle_p");
                    insert.setEntityCode(tool, "eduProgramSpecializationRoot");
                    final BatchInsertBuilder insertChild = new BatchInsertBuilder();
                    for (final Object[] row : rows) {

                        final Long subjId = (Long) row[0];
                        final String subjTitle = (String) row[1];
                        final String subjShortTitle = (String) row[2];
                        final String code = "root." + Long.toString(subjId, 32);

                        final Long newId = insert.addRow(code, subjId, subjTitle, subjShortTitle);
                        insertChild.addRowWithId(newId);
                    }
                    insert.executeInsert(tool, "edu_specialization_base_t");
                    insertChild.executeInsert(tool, "edu_specialization_root_t");
                }
            }


            // Выставляем всем НПм на ВО без направленности общую направленность
            {
                final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
                upd.set("eduprogramspecialization_id", "sb.id");
                upd.where("lv.eduprogramspecialization_id is null");
                upd.where("sb.discriminator=" + rootSpecializationCode);
                upd.getUpdatedTableFrom()
                        .innerJoin(SQLFrom.table("structureeducationlevels_t", "lt"), "lv.leveltype_id=lt.id and (lt.high_p=? or lt.higher_p=?)")
                        .innerJoin(SQLFrom.table("edu_specialization_base_t", "sb"), "sb.programsubject_id=lv.eduprogramsubject_id");

                final int n = tool.executeUpdate(translator.toSql(upd), true, true);
                tool.info(n + " rows educationlevels_t set root specialization");
            }

            // Выставляем парента для НПм с необщей направленностью, если его нет
            {
                final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
                upd.set("parentlevel_id", "l2.id");
                upd.where("lv.parentlevel_id is null");
                upd.where("l2.eduprogramspecialization_id is not null and lv.eduprogramspecialization_id is not null");
                upd.where(prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode);
                upd.where(prefix + "getEntityCode(l2.eduprogramspecialization_id)=" + rootSpecializationCode);
                upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("educationlevels_t", "l2"), "l2.eduprogramsubject_id=lv.eduprogramsubject_id");

                final int n = tool.executeUpdate(translator.toSql(upd));
                tool.info(n + " rows educationlevels_t set parent with root specialization");
            }
        }

        // Удаляем неиспользуемые НПм

        final String orphanIdsSql = MigrationUtils.getOrphanIdsSQL(tool, "educationLevels", null, ImmutableSet.of("educationlevelhighgos2_t"));
        final Set<String> npmTables = ImmutableSet.of("educationlevelhighgos2_t", "educationlevels_t");
        if (MigrationUtils.deleteRows(tool, npmTables, orphanIdsSql) > 0) {

            // И еще раз удаляем (паренты в предыдущий заход могли удалиться)
            if (MigrationUtils.deleteRows(tool, npmTables, orphanIdsSql) > 0) {

                // А вот на третий заход уже не должно ничего удаляться - мы же избавились от укрупненных групп..
                if (MigrationUtils.deleteRows(tool, npmTables, orphanIdsSql) > 0) {
                    throw new IllegalStateException("Education levels removed so many times..");
                }
            }
        }

        // Для НПм с необщей направленностью и без парента надо создать парента на общей направленности
        {
            final List<Object[]> rows = tool.executeQuery(
                    processor(
                            Short.class,                               // 0
                            String.class,                              // 1
                            Long.class,                                // 2
                            Long.class,                                // 3
                            Long.class,                                // 4
                            Long.class,                                // 5
                            String.class,                              // 6
                            String.class,                              // 7
                            String.class,                              // 8
                            Long.class,                                // 9
                            Long.class                                 // 10
                    ),
                    "select   lv.discriminator, " +                    // 0
                            " lv.catalogcode_p, " +                    // 1
                            " lt.id, " +                               // 2
                            " lv.qualification_id, " +                 // 3
                            " lv.diplomaqualification_id, " +          // 4
                            " s.id, " +                                // 5
                            " s.subjectcode_p, " +                     // 6
                            " s.title_p, " +                           // 7
                            " s.shorttitle_p, " +                      // 8
                            " sb.id, " +                               // 9
                            " lv.id " +                                // 10
                            " from educationlevels_t lv " +
                            " inner join structureeducationlevels_t lt on lt.id=lv.leveltype_id " +
                            " inner join edu_c_pr_subject_t s on s.id=lv.eduprogramsubject_id " +
                            " left join edu_specialization_base_t sb on sb.programsubject_id=s.id and sb.discriminator=" + rootSpecializationCode +
                            " where lv.parentlevel_id is null and lv.eduprogramspecialization_id is not null and " +
                                    prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode
            );

            if (!rows.isEmpty()) {

                final BiMap<String, Long> levelTypeCodeToId = MigrationUtils.getCatalogCode2IdMap(tool, "structureeducationlevels_t");


                // [eduProgramSubject.id] -> [educationLevels.id] - НПм с общей направленностью для направления проф. образования
                final Map<Long, Long> subj2rootEduLevelMap = new HashMap<>();

                // Добавление НПм на общую направленность
                final BatchInsertBuilder insertEduLevels = new BatchInsertBuilder(
                        MigrationUtils.DISCRIMINATOR,
                        "code_p",
                        "catalogcode_p",
                        "title_p",
                        "eduprogramsubject_id",
                        "eduprogramspecialization_id",
                        "shorttitle_p",
                        "leveltype_id",
                        "qualification_id",
                        "diplomaqualification_id",
                        "okso_p",
                        "inheritedokso_p",
                        "inheritedqualification_id",
                        "nhrtddplmqlfctn_id"
                );
                insertEduLevels.setDefaultValue("parentlevel_id", null); // парента быть не должно
                insertEduLevels.setDefaultValue("dprgrmspclztnsrtngcch_p", "1"); // для всех общих направленностей ставится единица

                // Если будут ВПО (ГОС2) - их еще и в свою таблицу надо пихать
                final Short gos2vpoCode = tool.entityCodes().get("educationLevelHighGos2");
                final Short gos2spoCode = tool.entityCodes().get("educationLevelMiddleGos2");
                final BatchInsertBuilder insertGos2VPO = new BatchInsertBuilder(gos2vpoCode);
                insertGos2VPO.setDefaultValue("codelist_p", null); // Не знаю, какой сюда записывать код перечня
                insertGos2VPO.setDefaultValue("specializationnumber_p", null); // Тут ничего не нужно

                // Присваивание парента для осиротевших НПм
                final BatchUpdater updateEduLevels = new BatchUpdater("update educationlevels_t set parentlevel_id=? where id=?", DBType.LONG, DBType.LONG);
                final BatchUpdater updateEduLevelsLevelType = new BatchUpdater("update educationlevels_t set leveltype_id=? where id=?", DBType.LONG, DBType.LONG);

                for (final Object[] row : rows) {

                    final Short discriminator = (Short) row[0];
                    final String catalogCode = (String) row[1];
                    final Long levelTypeId = (Long) row[2];
                    final Long qualificationId = (Long) row[3];
                    final Long diplomaQualificationId = (Long) row[4];
                    final Long subjectId = (Long) row[5];
                    final String subjectCode = (String) row[6];
                    final String subjectTitle = (String) row[7];
                    final String subjectShortTitle = (String) row[8];
                    final Long specializationRootId = (Long) row[9];
                    final Long eduLevelId = (Long) row[10];

                    if (specializationRootId == null) {
                        tool.error("Internal error: not found root specialization for subject " + subjectId + " (" + subjectTitle + ") and edu level " + eduLevelId + ". Bug in migration.");
                        continue; // Упадет ниже на проверке
                    }

                    final String levelTypeCode = levelTypeCodeToId.inverse().get(levelTypeId);
                    String parentLevelCode = PARENT_LEVEL_CODE_MAP.get(levelTypeCode);
                    if (parentLevelCode == null) {
                        parentLevelCode = levelTypeCode;
                    }

                    Long baseEduLevelId = subj2rootEduLevelMap.get(subjectId);
                    if (null == baseEduLevelId) {
                        final String newEduLevelCode = "root." + Long.toString(subjectId, 32);
                        final String okso = (discriminator.equals(gos2vpoCode) || discriminator.equals(gos2spoCode)) ?
                                (subjectCode.indexOf('.') == subjectCode.lastIndexOf('.') ? StringUtils.substringBefore(subjectCode, ".") : subjectCode)
                                : null;

                        baseEduLevelId = insertEduLevels.addRow(
                                discriminator,
                                newEduLevelCode,
                                catalogCode,
                                subjectTitle,
                                subjectId,
                                specializationRootId,
                                subjectShortTitle,
                                levelTypeCodeToId.get(parentLevelCode),
                                qualificationId,
                                diplomaQualificationId,
                                okso,
                                okso,
                                qualificationId,
                                diplomaQualificationId
                        );
                        subj2rootEduLevelMap.put(subjectId, baseEduLevelId);

                        if (discriminator.equals(gos2vpoCode)) {
                            insertGos2VPO.addRowWithId(baseEduLevelId);
                        }
                    }

                    updateEduLevels.addBatch(baseEduLevelId, eduLevelId);
                    if (levelTypeCode.equals(parentLevelCode)) {
                        final List<Object[]> childRows = tool.executeQuery(processor(1), "select lt.id from structureeducationlevels_t lt where lt.parent_id=?", levelTypeId);
                        if (childRows.size() == 1) {
                            updateEduLevelsLevelType.addBatch(childRows.get(0)[0], eduLevelId);
                        }
                    }
                }

                insertEduLevels.executeInsert(tool, "educationlevels_t");
                insertGos2VPO.executeInsert(tool, "educationlevelhighgos2_t");
                updateEduLevels.executeUpdate(tool);
                updateEduLevelsLevelType.executeUpdate(tool);
            }
        }


        // Делаем разные проверки
        {
            // Проверяем, что в дочерней таблице есть все записи для educationLevelHighGos2
            final Short gos2vpoCode = tool.entityCodes().get("educationLevelHighGos2");
            boolean hasErrors = hasWrongIds(
                    tool,
                    "select lv.id from educationlevels_t lv where lv.discriminator=" + gos2vpoCode +
                            " and not exists(select 1 from educationlevelhighgos2_t g2 where g2.id=lv.id)",
                    "Missing child rows in educationlevelhighgos2_t"
            );

            // Проверяем, что не осталось НПм без направленности на ВО
            hasErrors |= hasWrongIds(
                    tool,
                    "select lv.id from educationlevels_t lv " +
                            " join structureeducationlevels_t lt on lv.leveltype_id=lt.id and (lt.high_p=? or lt.higher_p=?) " +
                            " where eduprogramspecialization_id is null",
                    "Has high or higher education level without specialization",
                    true, true
            );

            // Не должно остаться НПм без парента на не общей направленности
            hasErrors |= hasWrongIds(
                    tool,
                    "select id from educationlevels_t where parentlevel_id is null and eduprogramspecialization_id is not null and " + prefix + "getEntityCode(eduprogramspecialization_id)<>" + rootSpecializationCode,
                    "Has education level on eduProgramSpecializationChild without parent level"
            );

            // Проверяем, что нет НПм с направленностью, но без направления
            hasErrors |= hasWrongIds(
                    tool,
                    "select id from educationlevels_t where eduprogramspecialization_id is not null and eduprogramsubject_id is null",
                    "Has specialization without subject"
            );

            // Проверяем, что нет парента у НПм с общей направленностью
            hasErrors |= hasWrongIds(
                    tool,
                    "select id from educationlevels_t where parentlevel_id is not null and " + prefix + "getEntityCode(eduprogramspecialization_id)=" + rootSpecializationCode,
                    "Has parent with root specialization"
            );

            // Проверяем, что нет НПм на профиль/специализацию с общей направленностью
            hasErrors |= hasWrongIds(
                    tool,
                    "select lv.id from educationlevels_t lv join structureeducationlevels_t lt on lv.leveltype_id=lt.id " +
                            " where (lt.specialization_p=? or lt.profile_p=?) and " + prefix + "getEntityCode(lv.eduprogramspecialization_id)=" + rootSpecializationCode,
                    "Has education levels on specialization or profile with eduProgramSpecializationRoot",
                    true, true
            );

            // Проверяем соответствие класса направления проф. образования и поколения НПм
            final boolean invalidClasses = hasWrongIds(
                    tool,
                    "select lv.id from educationlevels_t lv join edu_c_pr_subject_t s on s.id=lv.eduprogramsubject_id " +
                            " where not(" +
                            "     (lv.catalogCode_p in ('educationLevelHighGos2', 'educationLevelMiddleGos2') and s.discriminator = (select code_id from entitycode_s where name_p='eduProgramSubjectOkso'))\n" +
                            "  or (lv.catalogCode_p in ('educationLevelHighGos3', 'educationLevelMiddleGos3') and s.discriminator = (select code_id from entitycode_s where name_p='eduProgramSubject2009'))\n" +
                            "  or (lv.catalogCode_p in ('educationLevelHighGos3s', 'educationLevelMiddleGos3s') and s.discriminator = (select code_id from entitycode_s where name_p='eduProgramSubject2013'))\n" +
                            "  or (lv.catalogCode_p not in ('educationLevelHighGos3s', 'educationLevelMiddleGos3s', 'educationLevelHighGos3', 'educationLevelMiddleGos3', 'educationLevelHighGos2', 'educationLevelMiddleGos2'))\n" +
                            ")",
                    "Has education levels with wrong class or wrong program subject"
            );
            if (invalidClasses) {
                if ( ! MS_uni_2x9x2_1to2.canAutoFix()) {
                    hasErrors = true;
                } else {
                    if (ApplicationRuntime.hasModule("unifefu")) {
                        if (1 != tool.executeUpdate("update educationlevelshighschool_t set educationlevel_id=? where id=?", 1431710741934121669L, 1415492469857460781L)
                                ||
                            1 != tool.executeUpdate("delete from educationlevels_t where id=?", 1415491601967882951L))
                        {
                            hasErrors = true;
                        }
                    } else {
                        hasErrors = true;
                    }
                }
            }

            // Проверяем, что нет НПм на специальность ВПО с необщей направленностью
            if (!hasRamecAspirant) {
                // САФУ/ИжГТУ
                hasErrors |= hasWrongIds(
                        tool,
                        "select lv.id from educationlevels_t lv join structureeducationlevels_t lt on lv.leveltype_id=lt.id " +
                                " where (lt.specialty_p=? " +
                                "        or (lt.profile_p=? and (lt.master_p=? or lt.bachelor_p=?))" +
                                "        or exists(select lv2.id from educationlevels_t lv2 where lv2.parentlevel_id=lv.id)) " +
                                " and eduprogramspecialization_id is not null and " + prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode,
                        "Has education levels with eduProgramSpecializationChild on speciality or with child rows",
                        true, false, true, true
                );
            } else {
                hasErrors |= hasWrongIds(
                        tool,
                        "select lv.id from educationlevels_t lv join structureeducationlevels_t lt on lv.leveltype_id=lt.id " +
                                " where ((lt.specialty_p=? and lt.high_p=?) " +
                                "        or (lt.profile_p=? and (lt.master_p=? or lt.bachelor_p=?))" +
                                "        or exists(select lv2.id from educationlevels_t lv2 where lv2.parentlevel_id=lv.id)) " +
                                " and eduprogramspecialization_id is not null and " + prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode,
                        "Has high education levels with eduProgramSpecializationChild on speciality or with child rows",
                        true, true, false, true, true
                );
            }

            // Проверяем, что нет двойной вложенности
            hasErrors |= hasWrongIds(
                    tool,
                    "select lv.id from educationlevels_t lv " +
                            " where lv.eduprogramspecialization_id is not null and " +
                            prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode +
                            " and exists (select 1 from educationlevels_t lv2 where lv2.parentlevel_id=lv.id)",
                    "Has education levels with eduProgramSpecializationChild and children levels"
            );

            // Проверяем, что направление соответствует направлению парента
            hasErrors |= hasWrongIds(
                    tool,
                    "select lv.id from educationlevels_t lv " +
                            " where lv.eduprogramspecialization_id is not null and " +
                            prefix + "getEntityCode(lv.eduprogramspecialization_id)<>" + rootSpecializationCode +
                            " and not exists (select 1 from educationlevels_t lv2 where lv.parentlevel_id=lv2.id and lv.eduprogramsubject_id=lv2.eduprogramsubject_id)",
                    "Has child education levels with eduProgramSubject not equals parent eduProgramSubject"
            );

            if (hasErrors) {
                // Ну что блин еще?
                tool.fatal("DB contains inconsistent data. See above.");
                throw new IllegalStateException("DB contains inconsistent data. See log for details");
            }
        }

        final long npm_count_after = tool.getNumericResult("select count(*) from educationlevels_t");
        final long npv_count_after = tool.getNumericResult("select count(*) from educationlevelshighschool_t");
        final long npp_count_after = tool.getNumericResult("select count(*) from educationorgunit_t");
        tool.debug(String.format(
                "Total: removed %d from %d EducationLevels,  %d from %d EducationLevelsHighSchool,  %d from %d EducationOrgUnit",
                (npm_count_before - npm_count_after), npm_count_before,
                (npv_count_before - npv_count_after), npv_count_before,
                (npp_count_before - npp_count_after), npp_count_before
        ));
    }

    private boolean hasWrongIds(DBTool tool, String sql, String message, Object... params) throws SQLException {

        final List<Object[]> ids = tool.executeQuery(processor(1), sql, params);
        if (CollectionUtils.isNotEmpty(ids)) {
            tool.error(message + ". Wrong IDs: " + ids.stream().map(r->String.valueOf((Long)r[0])).collect(Collectors.joining(", ", "[", "]")));
            return true;
        }
        return false;
    }

    private final Map<String, String> PARENT_LEVEL_CODE_MAP = ImmutableMap.<String, String>builder()
            .put("28", "27")
            .put("27", "27")
            .put("30", "29")
            .put("29", "29")
            .put("33", "32")
            .put("32", "32")
            .put("39", "38")
            .put("38", "38")
            .put("40", "40") // аспиранты рамека
            .put("41", "41") // аспиранты рамека
            .put("8", "4")
            .put("4", "4")
            .put("7", "3")
            .put("3", "3")
            .put("10", "6")
            .put("6", "6")
            .put("9", "5")
            .build();
}