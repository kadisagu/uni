/* $Id$ */
package ru.tandemservice.uni.util;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.debug.SectionDebugItem;

/**
 * @author oleyba
 * @since 11/7/11
 */
public class DebugUtils
{
    // todo remove it - http://wiki.tandemservice.ru/display/TFDevelop/1.6.10+-%3E+1.6.11 п.10 (когда заработает)

    public interface DebugSection
    {
        String name();
        void execute() throws Exception;
    }

    public static abstract class Section implements DebugSection
    {
        private String name;

        protected Section(String name)
        {
            this.name = name;
        }

        @Override
        public String name()
        {
            return name;
        }
    }

    public static void debug(DebugSection section) throws Exception
    {
        Debug.begin(section.name());
        try {
            section.execute();
        }
        finally {
            Debug.end();
        }
    }

    public static void debug(Log logger, DebugSection section) throws Exception
    {
        Debug.begin(section.name());
        try {
            section.execute();
        }
        finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());
        }
    }

    public static void debug(Logger logger, DebugSection section) throws Exception
    {
        Debug.begin(section.name());
        try {
            section.execute();
        }
        finally {
            final SectionDebugItem end = Debug.end();
            if (end != null && logger.isInfoEnabled())
                logger.info(end.toFullString());
        }
    }

    public static void debug(DebugSection ...sections) throws Exception
    {
        for (DebugSection section : sections)
            debug(section);
    }
}
