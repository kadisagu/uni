package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состояние студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentStatusGen extends EntityBase
 implements INaturalIdentifiable<StudentStatusGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.StudentStatus";
    public static final String ENTITY_NAME = "studentStatus";
    public static final int VERSION_HASH = -1759920702;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ACTIVE = "active";
    public static final String P_USED_IN_SYSTEM = "usedInSystem";
    public static final String P_PRIORITY = "priority";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private boolean _active;     // Активный
    private boolean _usedInSystem;     // Используется в системе
    private int _priority;     // Приоритет
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Активный. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Активный. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Используется в системе. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsedInSystem()
    {
        return _usedInSystem;
    }

    /**
     * @param usedInSystem Используется в системе. Свойство не может быть null.
     */
    public void setUsedInSystem(boolean usedInSystem)
    {
        dirty(_usedInSystem, usedInSystem);
        _usedInSystem = usedInSystem;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentStatusGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentStatus)another).getCode());
            }
            setShortTitle(((StudentStatus)another).getShortTitle());
            setActive(((StudentStatus)another).isActive());
            setUsedInSystem(((StudentStatus)another).isUsedInSystem());
            setPriority(((StudentStatus)another).getPriority());
            setTitle(((StudentStatus)another).getTitle());
        }
    }

    public INaturalId<StudentStatusGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentStatusGen>
    {
        private static final String PROXY_NAME = "StudentStatusNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentStatusGen.NaturalId) ) return false;

            StudentStatusGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentStatusGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentStatus.class;
        }

        public T newInstance()
        {
            return (T) new StudentStatus();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "active":
                    return obj.isActive();
                case "usedInSystem":
                    return obj.isUsedInSystem();
                case "priority":
                    return obj.getPriority();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "usedInSystem":
                    obj.setUsedInSystem((Boolean) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "active":
                        return true;
                case "usedInSystem":
                        return true;
                case "priority":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "active":
                    return true;
                case "usedInSystem":
                    return true;
                case "priority":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "active":
                    return Boolean.class;
                case "usedInSystem":
                    return Boolean.class;
                case "priority":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentStatus> _dslPath = new Path<StudentStatus>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentStatus");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Используется в системе. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#isUsedInSystem()
     */
    public static PropertyPath<Boolean> usedInSystem()
    {
        return _dslPath.usedInSystem();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StudentStatus> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Boolean> _active;
        private PropertyPath<Boolean> _usedInSystem;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentStatusGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(StudentStatusGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(StudentStatusGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Используется в системе. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#isUsedInSystem()
     */
        public PropertyPath<Boolean> usedInSystem()
        {
            if(_usedInSystem == null )
                _usedInSystem = new PropertyPath<Boolean>(StudentStatusGen.P_USED_IN_SYSTEM, this);
            return _usedInSystem;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StudentStatusGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.StudentStatus#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentStatusGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StudentStatus.class;
        }

        public String getEntityName()
        {
            return "studentStatus";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
