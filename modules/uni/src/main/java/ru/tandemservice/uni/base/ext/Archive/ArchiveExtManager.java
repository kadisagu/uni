/**
 *$Id$
 */
package ru.tandemservice.uni.base.ext.Archive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.archive.base.bo.Archive.ArchiveManager;
import org.tandemframework.shared.archive.base.bo.Archive.util.ArchiveOwnerSecDescription;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Alexander Zhebko
 * @since 25.02.2014
 */
@Configuration
public class ArchiveExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private ArchiveManager _archiveManager;

    @Bean
    public ItemListExtension<ArchiveOwnerSecDescription> archiveOwnerSecDescriptionListExtension()
    {
        ArchiveOwnerSecDescription.IPostfixResolver orgUnitPostfixResolver = new ArchiveOwnerSecDescription.IPostfixResolver()
        {
            @Override public String getPostfix(IEntity entity){ return OrgUnit.ENTITY_NAME + "_" + ((OrgUnit) entity).getOrgUnitType().getCode(); }
        };

        return itemListExtension(_archiveManager.archiveOwnerSecDescriptionListExtPoint())
                .add(OrgUnit.ENTITY_NAME, new ArchiveOwnerSecDescription(OrgUnit.ENTITY_NAME).postfixResolver(orgUnitPostfixResolver))
                .add(Student.ENTITY_NAME, new ArchiveOwnerSecDescription(Student.ENTITY_NAME).commonPermissionModifier("studentPG", "Объект «Студент»"))
                .create();
    }
}