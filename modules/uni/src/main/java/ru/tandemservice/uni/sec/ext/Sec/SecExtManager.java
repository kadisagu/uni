/* $Id:$ */
package ru.tandemservice.uni.sec.ext.Sec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.sec.bo.Sec.SecManager;
import org.tandemframework.shared.organization.sec.bo.Sec.util.DefaultRoleDefinition;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author oleyba
 * @since 5/7/15
 */
@Configuration
public class SecExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SecManager secManager;

    @Bean
    public ItemListExtension<DefaultRoleDefinition> defaultRoleListExtension()
    {
        return this.itemListExtension(secManager.defaultRoleListExtPoint())
            .add("student", new DefaultRoleDefinition<>(Student.class))
            .create();
    }
}
