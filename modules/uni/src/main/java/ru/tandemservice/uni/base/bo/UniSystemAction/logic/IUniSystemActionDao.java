/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniSystemAction.logic;

import com.healthmarketscience.jackcess.Database;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public interface IUniSystemActionDao extends INeedPersistenceSupport
{
    void correctIdentityCardFIOInNewTransaction();

    void updateAddressItemsFullTitles();

    void updateGroupTitles();

    Integer printEducationLevelsReport() throws Exception;

    void deleteCategorylessPersons();

    void updateGroupTitlesWithNulls();

    void exportEduData(final Database mdb) throws IOException;
}
