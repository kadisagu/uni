/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummary.Pub;

import com.google.common.base.Objects;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author oleyba
 * @since 23.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        final IStorableReport report = UniDaoFacade.getCoreDao().getNotNull(getModel(component).getReportId());
        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(CommonBaseUtil.getContentType(report.getContent(), DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL))
            .fileName(Objects.firstNonNull(report.getContent().getFilename(), "StudentSummaryReport.xls"))
            .document(content), true);
    }
}