package ru.tandemservice.uni.util.jxl;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class ListDataSourcePrinter {

    private final WritableSheet sheet;
    private final AbstractListDataSource<IEntity> source;

    public interface Column {
        AbstractColumn column();
        int size();
    }

    private final List<Column> printableColumns = new ArrayList<>();

    public ListDataSourcePrinter(final WritableSheet sheet, final AbstractListDataSource source) {
        this.sheet = sheet;
        this.source = source;
    }

    protected final WritableFont FONT_NORM = this.buildFontNorm();
    protected WritableFont buildFontNorm() {
        return new WritableFont(WritableFont.ARIAL, 10);
    }

    protected final WritableFont FONT_BOLD = this.buildFontBold();
    protected WritableFont buildFontBold() {
        try {
            final WritableFont font = this.buildFontNorm();
            font.setBoldStyle(WritableFont.BOLD);
            return font;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected final WritableCellFormat STYLE_TABLE_BODY = this.buildStyleTableNorm();
    protected WritableCellFormat buildStyleTableNorm() {
        try {
            final WritableCellFormat format = new WritableCellFormat();
            format.setVerticalAlignment(VerticalAlignment.TOP);
            format.setBorder(Border.ALL, BorderLineStyle.DOTTED);
            format.setFont(this.FONT_NORM);
            return format;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected final WritableCellFormat STYLE_TABLE_HEAD_NORM = this.buildStyleTableHeadNorm();
    protected WritableCellFormat buildStyleTableHeadNorm() {
        try {
            final WritableCellFormat format = this.buildStyleTableNorm();
            format.setVerticalAlignment(VerticalAlignment.CENTRE);
            format.setAlignment(Alignment.CENTRE);
            format.setFont(this.FONT_BOLD);
            return format;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected final WritableCellFormat STYLE_TABLE_HEAD_VERT = this.buildStyleTableHeadVert();
    protected WritableCellFormat buildStyleTableHeadVert() {
        try {
            final WritableCellFormat format = this.buildStyleTableHeadNorm();
            format.setVerticalAlignment(VerticalAlignment.CENTRE);
            format.setOrientation(Orientation.PLUS_90);
            format.setShrinkToFit(true); // хз как пока это впихнуть
            return format;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private final Map<WritableCellFormat, WritableCellFormat> normat2boldStyle = new IdentityHashMap<>();
    protected WritableCellFormat bold(WritableCellFormat style) {
        WritableCellFormat bold = normat2boldStyle.get(style);
        if (null == bold) {
            try {
                WritableFont f = new WritableFont(style.getFont());
                f.setBoldStyle(WritableFont.BOLD);
                bold = new WritableCellFormat(style);
                bold.setFont(f);
                normat2boldStyle.put(style, bold);
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
        }
        return bold;
    }


    protected WritableCellFormat getHeaderCellFormat(final AbstractColumn column) {
        return column.isVerticalHeader() ? this.STYLE_TABLE_HEAD_VERT : this.STYLE_TABLE_HEAD_NORM;
    }

    protected int getColumnWidth(final AbstractColumn column) {
        return 1;
    }

    protected WritableCellFormat getBodyCellFormat(final AbstractColumn column, final IEntity e, final String bodyCellContent) {
        return this.STYLE_TABLE_BODY;
    }

    protected boolean isColumnPrintable(final AbstractColumn column) {
        return (column instanceof FormatterColumn);
    }

    public int print(int top, final int left) {
        top = this.printHeader(top, left);
        top = this.printData(top, left);
        return top;
    }


    protected int printHeader(final int top, final int left) {
        try {
            this.printableColumns.clear();
            final int headerHeight = this.getHeaderHeight(this.source.getColumns());
            this.printHeaderProcessColumns(top, left, this.source.getColumns(), headerHeight);
            return (top+headerHeight+1);
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private int getHeaderHeight(final List<AbstractColumn> columns) {
        int height = 0;
        for (final AbstractColumn column : columns) {
            if (column instanceof HeadColumn)
            {
                height = Math.max(height, 1 + this.getHeaderHeight(((HeadColumn)column).getColumns()));
            }
            else if (this.isColumnPrintable(column))
            {
                height = Math.max(height, 1);
            }
        }
        return height;
    }

    private int printHeaderProcessColumns(final int top, int left, final List<AbstractColumn> columns, final int rows) throws WriteException
    {
        for (final AbstractColumn column : columns) {
            if (column instanceof HeadColumn)
            {
                final Label cell = new Label(left, top, getColumnCaption(column), this.getHeaderCellFormat(column));
                left = this.printHeaderProcessColumns(top+1, left, ((HeadColumn)column).getColumns(), rows-1);
                if (left > cell.getColumn()) {
                    this.sheet.addCell(cell);
                    this.sheet.mergeCells(cell.getColumn(), cell.getRow(), left-1, cell.getRow());
                } else {
                    // throw new IllegalStateException();
                }
            }
            else if (this.isColumnPrintable(column))
            {
                final Column c = new Column() {
                    @Override public AbstractColumn column() { return column; }

                    private final int size = Math.max(1, ListDataSourcePrinter.this.getColumnWidth(column));
                    @Override public int size() { return this.size; }
                };

                final Label cell = new Label(left, top, getColumnCaption(column), this.getHeaderCellFormat(column));
                this.sheet.addCell(cell);
                this.sheet.mergeCells(cell.getColumn(), cell.getRow(), cell.getColumn()+(c.size()-1), cell.getRow()+rows);
                this.printableColumns.add(c);
                left = left + c.size();
            }
            else
            {
                // throw new IllegalStateException("Unsupported column:"+column.getClass().getName());
            }
        }
        return left;
    }

    protected String getColumnCaption(final AbstractColumn column) {
        return StringUtils.trimToEmpty(column.getCaption()).replace('\n', ' ');
    }


    protected String getBodyCellContent(final AbstractColumn c, final IEntity e) {
        if (c instanceof MultiValuesColumn) {
            MultiValuesColumn mvc = (MultiValuesColumn)c;
            final StringBuilder sb = new StringBuilder();
            for (IEntity x: mvc.getEntityList(e)) {
                final String content = mvc.getContent(x);
                sb.append(mvc.isRawContent() ? CoreStringUtils.stripHtml(content) : content).append("\n");
            }
            return sb.toString();
        }

        if (c instanceof FormatterColumn) {
            final FormatterColumn fc = ((FormatterColumn)c);
            final String content = fc.getContent(e);
            if (fc.isRawContent()) {
                return CoreStringUtils.stripHtml(content);
            }
            return content;
        }

        // если колонка хз какого типа, то пытаемся взять его содержимое просто как строковое описание
        return String.valueOf(c.getContent(e));
    }

    protected int printData(int top, final int left) {
        try {
            List<IEntity> entityList = this.source.getEntityList();

            try {
                PlaneTree tree = new PlaneTree((List)entityList);
                entityList = (List)tree.getFlatTreeObjectsHierarchicalSorted((String)null);
            } catch (ClassCastException t) {
                // не иерархичная штука, выводим просто список
            }

            for (final IEntity e: entityList) {
                int l = left;
                for (final Column c: this.printableColumns) {
                    final Label cell = buildLabel(top, l, c.column(), e);
                    this.sheet.addCell(cell);
                    this.sheet.mergeCells(cell.getColumn(), cell.getRow(), cell.getColumn()+(c.size()-1), cell.getRow());
                    l = l + c.size();
                }
                top = top + 1;
            }

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        return top;
    }

    protected Label buildLabel(int top, int left, AbstractColumn column, IEntity e) {
        final String bodyCellContent = this.getBodyCellContent(column, e);
        final WritableCellFormat bodyCellFormat = this.getBodyCellFormat(column, e, bodyCellContent);
        return buildLabel(top, left, bodyCellContent, bodyCellFormat);
    }

    protected Label buildLabel(int top, int left, final String bodyCellContent, final WritableCellFormat bodyCellFormat) {
        return new Label(left, top, bodyCellContent, bodyCellFormat);
    }

}
