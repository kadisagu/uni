/* $Id$ */
package ru.tandemservice.uni.component.catalog.studentCustomStateCI.StudentCustomStateCIPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

/**
 * @author nvankov
 * @since 3/27/13
 */
public interface IDAO extends IDefaultCatalogPubDAO<StudentCustomStateCI, Model>
{
}
