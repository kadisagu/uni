/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao.group;

import org.apache.commons.lang.StringUtils;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class GroupTitleAlgorithmSample3 extends UniBaseDao implements IAbstractGroupTitleDAO
{
    /*
     * Формат - [ф][нп]-[к][№№]
     * Пример - ОЛД-101
     * Описание:
     *   ф - однобуквенное название формы освоения (поле "Группа" в справочнике);
     *   нп - сокращенное название направления подготовки (уровня ОУ), может быть несколько символов;
     *   к - номер курса;
     *   №№ - порядковый номер группы внутри курса, два символа, начинаем с 01.
     */
    @Override
    public String getTitle(Group group, int number)
    {
        EducationOrgUnit educationOrgUnit = get(group.getEducationOrgUnit());
        Course course = get(group.getCourse());

        return new StringBuilder()
        .append(StringUtils.trimToEmpty(educationOrgUnit.getDevelopForm().getGroup()))
        .append(StringUtils.trimToEmpty(educationOrgUnit.getEducationLevelHighSchool().getShortTitle()))
        .append('-')
        .append(course.getIntValue())
        .append(String.format("%02d", number % getMaxNumber()))
        .toString();
    }

    @Override
    public int getMaxNumber()
    {
        return 100;
    }
}