/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.documents.d1.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 16.10.2009
 */
public class Model extends DocumentAddBaseModel
{
    private Date _formingDate;
    private String _studentTitleStr;
    private List<Course> _courseList;
    private Course _course;
    private List<IdentifiableWrapper> _departmentList;
    private IdentifiableWrapper _department;
    private String _documentForTitle;
    private String _managerPostTitle;
    private String _managerFio;
    private String _secretarTitle;
    private Integer _term;
    private String _phone;

    // Getters & Setters

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<IdentifiableWrapper> getDepartmentList()
    {
        return _departmentList;
    }

    public void setDepartmentList(List<IdentifiableWrapper> departmentList)
    {
        _departmentList = departmentList;
    }

    public IdentifiableWrapper getDepartment()
    {
        return _department;
    }

    public void setDepartment(IdentifiableWrapper department)
    {
        _department = department;
    }

    public String getDocumentForTitle()
    {
        return _documentForTitle;
    }

    public void setDocumentForTitle(String documentForTitle)
    {
        _documentForTitle = documentForTitle;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }

    public String getSecretarTitle()
    {
        return _secretarTitle;
    }

    public void setSecretarTitle(String secretarTitle)
    {
        _secretarTitle = secretarTitle;
    }

    public Integer getTerm()
    {
        return _term;
    }

    public void setTerm(Integer term)
    {
        _term = term;
    }

    public String getPhone()
    {
        return _phone;
    }

    public void setPhone(String phone)
    {
        _phone = phone;
    }
}
