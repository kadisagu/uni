package ru.tandemservice.uni.entity.employee.pps;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.base.bo.PpsEntry.logic.BasePpsEntryDSHandler;
import ru.tandemservice.uni.dao.pps.IPpsEntryDao;
import ru.tandemservice.uni.entity.employee.pps.gen.PpsEntryGen;

import java.util.Arrays;

/**
 * Запись в реестре ППС
 */
public abstract class PpsEntry extends PpsEntryGen implements ITitled, IPpsEntry
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new BasePpsEntryDSHandler(ownerId);
    }

    public static final String P_TITLE = PpsEntryGen.P_TITLE;

    @Override
    @EntityDSLSupport(parts = L_PERSON + "." + Person.P_FULL_FIO)
    public abstract String getExtendedTitle();

    @Override
    @EntityDSLSupport(parts = L_PERSON + "." + Person.P_FULL_FIO)
    public abstract String getTitle();

    @Override
    @EntityDSLSupport(parts = L_PERSON + "." + Person.P_FULL_FIO)
    public abstract String getShortTitle();

    @Override
    @EntityDSLSupport
    public abstract String getTitleInfo();

    /**
     * Вариант 1
     *
     * @return информация о сотруднике в формете 1 из вики
     * [Фамилия И.О.] ([должность или информация почасовика], [сокр. название подразделения])
     */
    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=33065812")
    public String getTitleFioInfoOrgUnit()
    {
        return getFio() + " (" + getTitlePostOrTimeWorkerData() + ", " + getOrgUnit().getShortTitle() + ")";
    }

    /**
     * Вариант 3
     *
     * (метод обращается к БД, поэтому не желательно вызывать его много раз)
     * @return информация о сотруднике в формате 3 из вики
     * [titleFioInfoOrgUnit], [учёные степени], [учёные звания]
     */
    @Override
    @EntityDSLSupport(parts = L_PERSON + "." + Person.P_FULL_FIO)
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=33065812")
    public String getTitleFioInfoOrgUnitDegreesStatuses()
    {
        String degreeAndStat = getDegreesAndStatuses();
        return getTitleFioInfoOrgUnit() + (degreeAndStat.isEmpty() ? "" : ", " + degreeAndStat);
    }

    /**
     * (метод обращается к БД, поэтому не желательно вызывать его много раз)
     * @return возвращает [учёные степени], [учёные звания]
     */
    public String getDegreesAndStatuses()
    {
        String degrees = CommonBaseStringUtil.join(IPpsEntryDao.instance.get().getScienceDegrees(this), ScienceDegree.P_SHORT_TITLE, ", ");
        String statuses = CommonBaseStringUtil.join(IPpsEntryDao.instance.get().getScienceStatuses(this), ScienceStatus.P_SHORT_TITLE, ", ");
        return CommonBaseStringUtil.joinNotEmpty(Arrays.asList(degrees, statuses), ", ");
    }

    /**
     * @return строка с названием должности сотрудника или с данными почасовика
     */
    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=33065812")
    public abstract String getTitlePostOrTimeWorkerData();
}
