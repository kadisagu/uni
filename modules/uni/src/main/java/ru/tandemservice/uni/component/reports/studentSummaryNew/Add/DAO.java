/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentSummaryNew.Add;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.component.reports.studentSummaryNew.ReportContentGenerator;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.report.StudentSummaryNewReport;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * @since 14.03.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        if (model.getOrgUnitId() != null)
            model.setOrgUnit(getNotNull(model.getOrgUnitId()));
        else
            model.setOrgUnit(null);

        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechListModel(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopPeriodListModel(EducationCatalogsManager.getDevelopPeriodSelectModel());
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(CompensationType.class).setSortProperty(CompensationType.P_CODE));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setStudentStatusListModel(new LazySimpleSelectModel<>(getCatalogItemList(StudentStatus.class, StudentStatus.P_USED_IN_SYSTEM, Boolean.TRUE)));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(StudentCategory.class).setSortProperty(StudentCategory.P_CODE));
        model.getParams().setShowSpecializations(model.getYesNoList().get(1));

        // восстанавливаем параметры отчета, которые сохраняются в настройках
        {
            List<StudentStatus> all = (List<StudentStatus>) model.getSetting("allList");
            model.getParams().setStudentStatusAllList(all == null ? new ArrayList<>() : all);
            List<StudentStatus> academ = (List<StudentStatus>) model.getSetting("academList");
            model.getParams().setStudentStatusAcademList(academ);
            Boolean child = (Boolean) model.getSetting("child");
            model.getParams().setChildActive(child == null ? false : child);
            if (model.getParams().isChildActive())
            {
                List<StudentStatus> childList = (List<StudentStatus>) model.getSetting("childList");
                model.getParams().setStudentStatusChildList(childList);
            }
            Boolean preg = (Boolean) model.getSetting("preg");
            model.getParams().setPregActive(preg == null ? false : preg);
            if (model.getParams().isPregActive())
            {
                List<StudentStatus> pregList = (List<StudentStatus>) model.getSetting("pregList");
                model.getParams().setStudentStatusPregList(pregList);
            }
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        for (StudentStatus status : model.getParams().getStudentStatusAcademList())
        {
            if (model.getParams().getStudentStatusPregList().contains(status))
                errors.add("Состояние «" + status.getTitle() + "» не должно быть включено в более чем одну графу из трех последних.", "studentStatusAcadem", "studentStatusPreg");
            if (model.getParams().getStudentStatusChildList().contains(status))
                errors.add("Состояние «" + status.getTitle() + "» не должно быть включено в более чем одну графу из трех последних.", "studentStatusAcadem", "studentStatusChild");
        }
        for (StudentStatus status : model.getParams().getStudentStatusPregList())
        {
            if (model.getParams().getStudentStatusChildList().contains(status))
                errors.add("Состояние «" + status.getTitle() + "» не должно быть включено в более чем одну графу из трех последних.", "studentStatusPreg", "studentStatusChild");
        }
    }

    @Override
    public void update(Model model)
    {
        model.setSetting("allList", model.getParams().getStudentStatusAllList());
        model.setSetting("academList", model.getParams().getStudentStatusAcademList());
        model.setSetting("childList", model.getParams().getStudentStatusChildList());
        model.setSetting("pregList", model.getParams().getStudentStatusPregList());
        model.setSetting("child", model.getParams().isChildActive());
        model.setSetting("preg", model.getParams().isPregActive());
        DataSettingsFacade.saveSettings(model.getSettings());

        ErrorCollector errors = UserContext.getInstance().getErrorCollector();

        Session session = getSession();

        // заполняем параметры отчета
        StudentSummaryNewReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setOrgUnit(model.getOrgUnit());
        model.getParams().setParams(report);

        // формируем контент
        try
        {
            DatabaseFile content = new DatabaseFile();

            content.setContent(new ReportContentGenerator(report, model.getParams(), session, errors).generateReportContent());
            content.setFilename("StudentSummaryNewReport.xls");
            if (!errors.hasErrors())
            {
                session.save(content);
                report.setContent(content);
                session.save(report);
            }
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}