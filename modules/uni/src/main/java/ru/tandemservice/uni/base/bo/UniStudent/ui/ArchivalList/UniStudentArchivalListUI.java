/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniStudent.ui.ArchivalList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
public class UniStudentArchivalListUI extends AbstractUniStudentListUI
{
    public static final String PARAM_PERSONAL_ARCHIVAL_FILE_NUMBER = "personalArchivalFileNumber";

    @Override
    public void onComponentRefresh()
    {
        setShowDevelopTechFilter(true);
        setHideUnusedEducationOrgUnit(true);
        super.onComponentRefresh();
    }

    protected IEducationOrgUnitContextHandler getContextHandler()
    {
        return new BaseStudentListContextHandler(true);
    }

    @Override
    public String getSettingsKey()
    {
        return "ArchivalStudentList.filter";
    }

    public void onClickEditStudent()
    {
        getActivationBuilder().asRegion(IUniComponents.STUDENT_EDIT)
                .parameter("studentId", getListenerParameter())
                .activate();
    }

    public void onClickDeleteStudent()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        getConfig().<BaseSearchListDataSource>getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).getLegacyDataSource().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(UniStudentArchivalList.STUDENT_SEARCH_LIST_DS))
        {
            dataSource.put(PARAM_PERSONAL_ARCHIVAL_FILE_NUMBER, getSettings().get(PARAM_PERSONAL_ARCHIVAL_FILE_NUMBER));
        }
        if (dataSource.getName().equals(UniStudentManger.GROUP_DS))
        {
            dataSource.put("archival", true);
        }
    }
}
