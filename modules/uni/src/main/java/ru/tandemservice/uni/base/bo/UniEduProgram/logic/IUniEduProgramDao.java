/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgram;

import java.util.List;

/**
 * @author oleyba
 * @since 8/12/14
 */
public interface IUniEduProgramDao extends INeedPersistenceSupport
{
    void deleteUnusedEduLevels();

    void doChangeAllowStudents(Long eduHsId);

    void saveOrUpdateEduOu(EducationOrgUnit educationOrgUnit);

    void doChangeUsedEduOu(Long listenerParameterAsLong);

    List<EducationOrgUnit> getEduOuList(EduProgram p);

    EducationOrgUnit getWithSameKey(EducationOrgUnit educationOrgUnit);
}