/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentAdditionalData.StudentAdditionalDataBlock;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentAdditionalData.StudentAdditionalDataParam;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentData.StudentDataBlock;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentData.StudentDataParam;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.print.student.StudentPrintBlock;

/**
 * @author Alexander Zhebko
 * @since 26.03.2014
 */
public class StudentReportPersonAddUI extends UIPresenter implements IReportDQLModifierOwner
{
    public static final String STUDENT_SCHEET = "studentScheet";

    // tab flags
    private boolean _studentScheet;

    private String _selectedTab;

    // blocks
    private IReportDQLModifier _studentData = new StudentDataParam();
    private IReportDQLModifier _studentAdditionalData = new StudentAdditionalDataParam();

    // print blocks
    private IReportPrintBlock _studentPrintBlock = new StudentPrintBlock();


    @Override
    public void onComponentRefresh()
    {
        _studentScheet = ((ReportPersonAddUI) getSupport().getParentUI()).isScheetVisible(STUDENT_SCHEET);

        // по умолчанию ставим фильтр "является студентом - да"
        ((StudentDataParam) _studentData).getStudent().setActive(true);

        if(((StudentDataParam) _studentData).getStudent().isActive() && ((StudentDataParam) _studentData).getStudent().getData() == null)
        {
            ((StudentDataParam) _studentData).getStudent().setData(TwinComboDataSourceHandler.getYesOption());
        }

        // по умолчанию ставим фильтр "архивный студент - нет"
        ((StudentDataParam) _studentData).getStudentArchival().setActive(true);

        if(((StudentDataParam) _studentData).getStudentArchival().isActive() && ((StudentDataParam) _studentData).getStudentArchival().getData() == null)
        {
            ((StudentDataParam) _studentData).getStudentArchival().setData(TwinComboDataSourceHandler.getNoOption());
        }
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        // studentTab
        if (_studentScheet)
        {
            StudentDataBlock.onBeforeDataSourceFetch(dataSource, (StudentDataParam) _studentData);
            StudentAdditionalDataBlock.onBeforeDataSourceFetch(dataSource, (StudentAdditionalDataParam) _studentAdditionalData);
        }
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_studentScheet)
        {
            printInfo.addSheet(STUDENT_SCHEET);

            // фильтры модифицируют запросы
            _studentData.modify(dql, printInfo);
            _studentAdditionalData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
            _studentPrintBlock.modify(dql, printInfo);

            for (IUIPresenter presenter : _uiSupport.getChildrenUI().values())
                printInfo.registerBaseComponentMeta(presenter.getConfig().getBusinessComponentMeta());

            for (IUIPresenter presenter : _uiSupport.getChildrenUI().values())
                ((IReportDQLModifierOwner) presenter).modify(dql, printInfo);
        }
    }

    // Getters

    public IReportPrintBlock getStudentPrintBlock()
    {
        return _studentPrintBlock;
    }

    // Getters & Setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public boolean isStudentScheet()
    {
        return _studentScheet;
    }

    public void setStudentScheet(boolean studentScheet)
    {
        _studentScheet = studentScheet;
    }
}