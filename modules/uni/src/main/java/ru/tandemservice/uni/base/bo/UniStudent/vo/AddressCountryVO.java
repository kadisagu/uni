/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.fias.base.entity.AddressCountry;

import java.io.Serializable;

/**
 * @author nvankov
 * @since 4/26/13
 */
public class AddressCountryVO extends DataWrapper
{
    public static final String NOT_RUSSIAN_FEDERATION = "НЕ ГРАЖДАНИН РФ";

    private AddressCountry _addressCountry;

    public AddressCountryVO(Long id, String title)
    {
        super(id, title);
    }

    public AddressCountryVO(AddressCountry addressCountry)
    {
        super(addressCountry.getId(), addressCountry.getTitle());
        _addressCountry = addressCountry;
    }

    public AddressCountry getAddressCountry()
    {
        return _addressCountry;
    }
}
