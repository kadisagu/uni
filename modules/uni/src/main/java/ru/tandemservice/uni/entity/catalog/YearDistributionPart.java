package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.gen.YearDistributionPartGen;

import java.util.Arrays;
import java.util.Collection;

/**
 * Часть учебного года
 */
public class YearDistributionPart extends YearDistributionPartGen implements IHierarchyItem, IDeclinable, Comparable<YearDistributionPart>
{
    public static final String DECLINATION_PROPERTY_SESSION_SHORT_TITLE = "sessionShortTitle";
    public static final String DECLINATION_PROPERTY_SESSION_TITLE = "sessionTitle";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new EntityComboDataSourceHandler(ownerId, YearDistributionPart.class)
            .titleProperty(YearDistributionPart.title().s())
            .filter(YearDistributionPart.title())
            .order(YearDistributionPart.yearDistribution().code())
            .order(YearDistributionPart.number());
    }

    public static EntityComboDataSourceHandler useSelectDSHandler(String ownerId)
    {
        return defaultSelectDSHandler(ownerId)
                .where(YearDistributionPart.yearDistribution().inUse(), Boolean.TRUE);
    }

    @Override public int compareTo(YearDistributionPart o) {
        int i = this.getYearDistribution().compareTo(o.getYearDistribution());
        if (0 != i) { return i; }
        return Integer.compare(this.getNumber(), o.getNumber());
    }

    @Override public YearDistribution getHierarhyParent() {
        return getYearDistribution();
    }

    @Override
    @EntityDSLSupport(parts={P_TITLE, L_YEAR_DISTRIBUTION+"."+YearDistribution.P_TITLE})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1063816")
    public String getTitleWithParent() {
        return super.getTitle() +" ("+ getYearDistribution().getTitle() +")";
    }

    @Override
    @EntityDSLSupport(parts={P_NUMBER, L_YEAR_DISTRIBUTION+"."+YearDistribution.P_AMOUNT})
    public String getShortTitleWithParent() {
        return super.getNumber() +"/"+ getYearDistribution().getAmount();
    }

    @Override
    public Collection<String> getDeclinableProperties() {
        return Arrays.asList(P_TITLE, P_SHORT_TITLE, DECLINATION_PROPERTY_SESSION_TITLE, DECLINATION_PROPERTY_SESSION_SHORT_TITLE);
    }
}