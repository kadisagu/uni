/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.events.single;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vip_delete
 * @since 16.02.2009
 */
public class StudentSaveEventListener extends FilteredSingleEntityEventListener
{
    @Override
    public void onFilteredEvent(final ISingleEntityEvent event)
    {
        final Student student = (Student) event.getEntity();
        if (StringUtils.isNotBlank(student.getPersonalNumber()))
            return;

        final String freeNumber = IStudentDAO.instance.get().generateNewPersonalNumber(student);

        if (freeNumber == null) {
            throw new ApplicationException("Для " + student.getEntranceYear() + " года нет свободных персональных номеров.");
        }

        student.setPersonalNumber(freeNumber);
        final Session session = event.getSession();
        session.flush(); // сохраняем в базу немедленно
    }
}
