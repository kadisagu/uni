/* $Id$ */
package ru.tandemservice.uni.catalog.bo.StudentCatalogs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author oleyba
 * @since 2/7/12
 */
@Configuration
public class StudentCatalogsManager extends BusinessObjectManager
{
    public static final String DS_COMPENSATION_TYPE = "compensationTypeDS";

    public static final String PARAM_STUDENT_STATUS_ONLY_ACTIVE = "studentStatusOnlyActive";
    public static final String PARAM_COURSE = "course";

    public static StudentCatalogsManager instance()
    {
        return instance(StudentCatalogsManager.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentStatusDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), StudentStatus.class)
            .where(StudentStatus.active(), PARAM_STUDENT_STATUS_ONLY_ACTIVE)
            .filter(StudentStatus.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), StudentCategory.class)
            .order(StudentCategory.code())
            .filter(StudentCategory.title());
    }

    @Bean
    public UIDataSourceConfig compensationTypeDSConfig()
    {
        return SelectDSConfig.with(DS_COMPENSATION_TYPE, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.compensationTypeDSHandler())
            .addColumn(CompensationType.shortTitle().s())
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> compensationTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CompensationType.class)
            .filter(CompensationType.title());
    }
}
