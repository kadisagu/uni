/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsExportExcludedStudentsXls;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.*;
import jxl.write.Number;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.ByteArrayOutputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 04.03.2014
 */
public class DAO extends UniDao<Model> implements IDAO
{
    //private final static Transformer<IEntity, Long> ID_TRANSFORMER = IIdentifiable::getId;

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        model.setEducationLevelTypeListModel(new FullCheckSelectModel(StructureEducationLevels.P_SHORT_TITLE) {
            @Override
            public ListResult findValues(String filter) {
                Criteria c = getSession().createCriteria(StructureEducationLevels.class, "e");

//                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "level")
//                                                    .where(isNull(property("level", StructureEducationLevels.parent())))
//                                                    .order(property("level", StructureEducationLevels.priority()));
                c.add(Restrictions.isNull(StructureEducationLevels.L_PARENT));
                c.setProjection(Projections.distinct(Projections.property("e." + StructureEducationLevels.P_ID)));

                final List<Long> ids = c.list();

                if (ids.isEmpty()) {
                    return ListResult.getEmpty();
                }

                c = getSession().createCriteria(StructureEducationLevels.class);
                c.add(Restrictions.in(StructureEducationLevels.P_ID, ids));

                //c.addOrder(Order.asc(StructureEducationLevels.P_PRIORITY));

                return new ListResult<>(c.list());
            }
        });

    }

    private Set<Long> getAllChildLevels(List<StructureEducationLevels> rootLevels)
    {
        Set<Long> levelIds =  new HashSet<>();
        Map<Long, Set<Long>> levelParentMap = SafeMap.get(HashSet.class);
        DQLSelectBuilder levelParentBuilder = new DQLSelectBuilder()
                .fromEntity(StructureEducationLevels.class, "s")
                .column(property("s", StructureEducationLevels.id()))
                .column(property("s", StructureEducationLevels.parent().id()));

        for (Object[] row : DAO.this.<Object[]>getList(levelParentBuilder))
        {
            Long id = (Long) row[0];
            Long parentId = (Long) row[1];

            levelParentMap.get(parentId).add(id);
        }
        levelIds.addAll(UniBaseDao.ids(rootLevels));
                //levelIds.addAll(CollectionUtils.collect(rootLevels, ID_TRANSFORMER));
        for (StructureEducationLevels lev : rootLevels)
        {
            collectValues(levelParentMap, lev.getId(), levelIds);
        }
        return levelIds;
    }
    private void collectValues(Map<Long, Set<Long>> valueMap, Long currentValue, Set<Long> result)
        {
            Set<Long> values = valueMap.get(currentValue);
            result.addAll(values);
            for (Long value: values)
            {
                collectValues(valueMap, value, result);
            }
        }

    @Override
    public byte[] printExcludedStudentsReport(Model model) throws Exception
    {
        List<StructureEducationLevels> educationLevelsList = model.getEducationLevelTypeList();
        Date dateFrom = model.getDateFrom();
        Date dateTo = model.getDateTo();
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создать лист
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.getSettings().setCopies(1);

        // создаем форматы
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        WritableCellFormat titleFormat = new WritableCellFormat(arial8bold);
        titleFormat.setWrap(true);
        titleFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        titleFormat.setBackground(Colour.GRAY_25);

        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        headerFormat.setWrap(true);

        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);

        // рисуем шапку
        int column = 0;

        sheet.addCell(new Blank(column, 0, titleFormat));
        sheet.setColumnView(column, 5);
        column++;

        sheet.addCell(new Label(column, 0, "Фамилия", titleFormat));
        sheet.setColumnView(column, 28);
        column++;

        sheet.addCell(new Label(column, 0, "Имя", titleFormat));
        sheet.setColumnView(column, 28);
        column++;

        sheet.addCell(new Label(column, 0, "Отчество", titleFormat));
        sheet.setColumnView(column, 28);
        column++;

        sheet.addCell(new Label(column, 0, "Дата рождения", titleFormat));
        sheet.setColumnView(column, 14);
        column++;

        sheet.addCell(new Label(column, 0, "Формирующее подразделение", titleFormat));
        sheet.setColumnView(column, 40);
        column++;

        sheet.addCell(new Label(column, 0, "Академическая группа", titleFormat));
        sheet.setColumnView(column, 20);
        column++;

        sheet.addCell(new Label(column, 0, "Дата отчисления", titleFormat));
        sheet.setColumnView(column, 14);
        column++;

        sheet.addCell(new Label(column, 0, "Дата приказа об отчислении", titleFormat));
        sheet.setColumnView(column, 14);
        column++;

        sheet.addCell(new Label(column, 0, "Номер приказа об отчислении", titleFormat));
        sheet.setColumnView(column, 12);

//        DQLSelectBuilder eduLevelBuilder =  new DQLSelectBuilder().fromEntity(Student.class, "st");

        /*
        List<String> eduLevelTypeCodeList = new ArrayList<String>();

        for (StructureEducationLevels item : educationLevelsList)
        {
            eduLevelTypeCodeList.add(item.getCode());
        }

        Collection<Long> currentIds = CollectionUtils.collect(educationLevelsList, ID_TRANSFORMER);
        */
        Set<Long> eduLevelTypeIdSet = getAllChildLevels(educationLevelsList);
        // рисуем данные
        DQLSelectBuilder studentBuilder =  new DQLSelectBuilder().fromEntity(Student.class, "st");
        studentBuilder.joinEntity("st", DQLJoinType.left, OrderData.class, "od", eq(property(OrderData.student().fromAlias("od")), property("st")));
        studentBuilder.column(property("st"));
        studentBuilder.column(property("od"));
        if (dateFrom!=null && dateTo!=null)
        {
        studentBuilder.where(or(and(eq(property(Student.status().code().fromAlias("st")), value(UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA)),
                or(between(property(OrderData.graduateDiplomaOrderDate().fromAlias("od")), valueDate(dateFrom), valueDate(dateTo)),
                        between(property(OrderData.graduateSuccessDiplomaOrderDate().fromAlias("od")), valueDate(dateFrom), valueDate(dateTo)))),
                and(eq(property(Student.status().code().fromAlias("st")), value(UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED)),
                        between(property(OrderData.excludeOrderDate().fromAlias("od")), valueDate(dateFrom), valueDate(dateTo)))
        ));
        }
        else
        {
        studentBuilder.where(or(eq(property(Student.status().code().fromAlias("st")), value(UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA)),
               eq(property(Student.status().code().fromAlias("st")), value(UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED))
                ));
        }
        studentBuilder.where(in(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().id().fromAlias("st"), eduLevelTypeIdSet));
        studentBuilder.order(property(Student.person().identityCard().fullFio().fromAlias("st")));
        List<Object[]> studentList=  studentBuilder.createStatement(getSession()).list();

        int i = 1;
        int row = 1;

        for (Object[] item : studentList)
        {
            Student student = (Student)item[0];
            OrderData order = (OrderData)item[1];
            IdentityCard card = student.getPerson().getIdentityCard();
            //OrderData order = get(OrderData.class, OrderData.L_STUDENT, item);
            sheet.addCell(new Number(0, row, i++, rowFormat));
            sheet.addCell(new Label(1, row, card.getLastName(), rowFormat));
            sheet.addCell(new Label(2, row, card.getFirstName(), rowFormat));
            sheet.addCell(new Label(3, row, card.getMiddleName(), rowFormat));
            sheet.addCell(new Label(4, row, DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()), rowFormat));
            sheet.addCell(new Label(5, row, student.getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit().getFullTitle(), rowFormat));
            sheet.addCell(new Label(6, row, student.getGroup() == null ? null : student.getGroup().getTitle(), rowFormat));
            if (order!=null)
            {
               Date date = order.getExcludeOrderDate();
               String num = order.getExcludeOrderNumber();
               sheet.addCell(new Label(7, row, (date==null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(date)), rowFormat));
               sheet.addCell(new Label(8, row, (date==null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(date)), rowFormat));
               sheet.addCell(new Label(9, row, num, rowFormat));
            }

            //sheet.addCell(new Label(8, row, item.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getShortTitle(), rowFormat));

            row++;
//            sheet.addCell(new Label(2, row, item.getTerritorialOrgUnit().getTerritorialFullTitle(), rowFormat));
//            sheet.addCell(new Label(3, row, item.getDevelopForm().getTitle(), rowFormat));
//            sheet.addCell(new Label(4, row, item.getDevelopCondition().getTitle(), rowFormat));
//            sheet.addCell(new Label(5, row, item.getDevelopTech().getTitle(), rowFormat));
//            sheet.addCell(new Label(6, row, item.getDevelopPeriod().getTitle(), rowFormat));
//            sheet.addCell(new Label(7, row, item.getShortTitle(), rowFormat));
//            sheet.addCell(new Label(8, row, Integer.toString(item.getCode()), rowFormat));
        }





        workbook.write();
        workbook.close();

        // регистрируем его в универсальном компоненте рендера
        return out.toByteArray();
    }
}
