package ru.tandemservice.uni.util.sync;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;

/**
 * @author vdanilov
 */
public class SyncInTransactionCheckLocker {

	private final long timeout;
	public SyncInTransactionCheckLocker(long timeout) {
		this.timeout = timeout;
	}

	private final ReentrantLock lock = new ReentrantLock();
	private final TransactionCompleteAction<Void> sync = new TransactionCompleteAction<Void>() {
		@Override public synchronized void afterCompletion(Session session, int status, Void beforeCompleteResult) {
			lock.unlock();
		};
	};

	public final synchronized boolean waitForTransactionComplete(final Session session) {
		try {
			if (!lock.tryLock(timeout, TimeUnit.MILLISECONDS)) {
				return false;
			}

			try {
				sync.register(session);
			} catch (Throwable t) {
				lock.unlock();
				throw t;
			}
			return true;

		} catch (Throwable t) {
			throw CoreExceptionUtils.getRuntimeException(t);
		}
	}
}
