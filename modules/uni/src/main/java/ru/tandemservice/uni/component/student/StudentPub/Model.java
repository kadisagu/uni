/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import ru.tandemservice.uni.component.person.util.ISecureRoleContextOwner;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id"),
        @Bind(key = Model.SELECTED_TAB_BIND_KEY, binding = "selectedTab"),
        @Bind(key = "selectedDataTab", binding = "selectedDataTab")
})

@Output({
        @Bind(key = "studentId", binding = "student.id"),
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = ISecureRoleContextOwner.SECURE_ROLE_CONTEXT)
})
public class Model implements IStudentModel, ISecureRoleContextOwner
{
    public static final String SELECTED_TAB_BIND_KEY = "selectedStudentTab";
    private Student _student = new Student();
    private OrderData _orderData;

    private String _selectedTab;
    private String _selectedDataTab;
    private boolean _contractStudent;
    private String _speciality;
    private String _specialization;
    private IDataSettings _settings;
    private List<StudentDocumentType> _studentDocumentTypeList;
    private DynamicListDataSource<StudentDocument> _documentDataSource;
    private List<StudentCustomState> _studentCustomStates;
    private boolean _hasFormFilled = false;

    public boolean isShowEduDocumentBlock() {
        return PersonEduDocumentManager.isShowNewEduDocuments();
    }

    // IPersonRoleModel

    @Override
    public ISecureRoleContext getSecureRoleContext()
    {
        return SecureRoleContext.instance(_student).hasBirthDateCheck(true).accessible(isAccessible());
    }

    public boolean isAccessible()
    {
        return !_student.isArchival();
    }

    // Getters & Setters
    public Map<String, Object> getStudentParameters()
    {
        return ParametersMap.createWith("studentId", _student.getId());
    }

    public String getStudentCustomStates()
    {
        return new StudentCustomStateCollectionFormatter().format(_studentCustomStates);
    }

    public void setStudentCustomStates(List<StudentCustomState> studentCustomStates)
    {
        _studentCustomStates = studentCustomStates;
    }

    @Override
    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return _selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        _selectedDataTab = selectedDataTab;
    }

    public String getFUT() {
        final EducationOrgUnit eou = getStudent().getEducationOrgUnit();
        return CommonBaseStringUtil.joinWithSeparator(
                ", ",
                StringUtils.uncapitalize(eou.getDevelopForm().getTitle()),
                StringUtils.uncapitalize(eou.getDevelopCondition().getTitle()),
                StringUtils.uncapitalize(eou.getDevelopTech().getTitle())
        );
    }

    public boolean isContractStudent()
    {
        return _contractStudent;
    }

    public void setContractStudent(boolean contractStudent)
    {
        _contractStudent = contractStudent;
    }

    public String getSpeciality()
    {
        return _speciality;
    }

    public void setSpeciality(String speciality)
    {
        _speciality = speciality;
    }

    public String getSpecialization()
    {
        return _specialization;
    }

    public void setSpecialization(String specialization)
    {
        _specialization = specialization;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public List<StudentDocumentType> getStudentDocumentTypeList()
    {
        return _studentDocumentTypeList;
    }

    public void setStudentDocumentTypeList(List<StudentDocumentType> studentDocumentTypeList)
    {
        _studentDocumentTypeList = studentDocumentTypeList;
    }

    public DynamicListDataSource<StudentDocument> getDocumentDataSource()
    {
        return _documentDataSource;
    }

    public void setDocumentDataSource(DynamicListDataSource<StudentDocument> documentDataSource)
    {
        _documentDataSource = documentDataSource;
    }

    public void setOrderData(OrderData orderData)
    {
        _orderData = orderData;
    }

    private String getSimpleOrderData(Date date, String number)
    {
        StringBuilder result = new StringBuilder();
        result.append(null != date ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date) : "");
        if (null != number)
        {
            result.append(result.length() > 0 ? " " : "");
            result.append("№").append(number);
        }
        return result.toString();
    }

    public String getEduEnrollmentOrderData()
    {
        if (_orderData == null) return "";
        StringBuilder result = new StringBuilder();
        result.append(null != _orderData.getEduEnrollmentOrderDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getEduEnrollmentOrderDate()) : "");
        if (null != _orderData.getEduEnrollmentOrderNumber())
        {
            result.append(result.length() > 0 ? " " : "");
            result.append("№").append(_orderData.getEduEnrollmentOrderNumber());
        }
        if (null != _orderData.getEduEnrollmentOrderEnrDate())
        {
            result.append(" (зачислен с ");
            result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getEduEnrollmentOrderEnrDate()));
            result.append(")");
        }
        return result.toString();
    }

    public String getTransferOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getTransferOrderDate(), _orderData.getTransferOrderNumber());
    }

    public String getWeekendOutOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getWeekendOutOrderDate(), _orderData.getWeekendOutOrderNumber());
    }

    public String getRestorationOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getRestorationOrderDate(), _orderData.getRestorationOrderNumber());
    }

    public String getChangeFioOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getChangeFioOrderDate(), _orderData.getChangeFioOrderNumber());
    }

    public String getExcludeOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getExcludeOrderDate(), _orderData.getExcludeOrderNumber());
    }

    private String getWeekendOrderData(Date date, String number, Date from, Date to)
    {
        if (_orderData == null) return "";
        StringBuilder result = new StringBuilder();
        result.append(null != date ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date) : "");

        if (null != number)
        {
            result.append(result.length() > 0 ? " " : "");
            result.append("№").append(number);
        }
        if (null != from || null != to)
        {
            result.append(" (предоставлен");
            if (null != from) result.append(" с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(from));
            if (null != to) result.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(to));
            result.append(")");
        }
        return result.toString();
    }

    public String getWeekendOrderData()
    {
        if (_orderData == null) return "";
        return getWeekendOrderData(_orderData.getWeekendOrderDate(), _orderData.getWeekendOrderNumber(), _orderData.getWeekendDateFrom(), _orderData.getWeekendDateTo());
    }

    public String getWeekendPregnancyOrderData()
    {
        if (_orderData == null) return "";
        return getWeekendOrderData(_orderData.getWeekendPregnancyOrderDate(), _orderData.getWeekendPregnancyOrderNumber(), _orderData.getWeekendPregnancyDateFrom(), _orderData.getWeekendPregnancyDateTo());
    }

    public String getWeekendPregnancyOutOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getWeekendPregnancyOutOrderDate(), _orderData.getWeekendPregnancyOutOrderNumber());
    }

    public String getWeekendChildOrderData()
    {
        if (_orderData == null) return "";
        return getWeekendOrderData(_orderData.getWeekendChildOrderDate(), _orderData.getWeekendChildOrderNumber(), _orderData.getWeekendChildDateFrom(), _orderData.getWeekendChildDateTo());
    }

    public String getWeekendChildOutOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getWeekendChildOutOrderDate(), _orderData.getWeekendChildOutOrderNumber());
    }

    public String getSessionProlongOrderData()
    {
        if (_orderData == null) return "";
        String result = "";
        if (null != _orderData.getSessionProlongOrderDate())
            result = result + DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getSessionProlongOrderDate()) + " ";
        if (null != _orderData.getSessionProlongOrderNumber())
            result = result + "№" + _orderData.getSessionProlongOrderNumber();
        if (null != _orderData.getSessionProlongDateTo())
            result = result + " (продлена до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getSessionProlongDateTo()) + ")";
        return result;
    }

    public String getChangeCompensationTypeOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getChangeCompensationTypeOrderDate(), _orderData.getChangeCompensationTypeOrderNumber());
    }

    public String getHolidaysOrderData()
    {
        if (_orderData == null) return "";
        StringBuilder result = new StringBuilder();
        result.append(null != _orderData.getHolidaysOrderDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getHolidaysOrderDate()) : "");

        if (null != _orderData.getHolidaysOrderNumber())
        {
            result.append(result.length() > 0 ? " " : "");
            result.append("№").append(_orderData.getHolidaysOrderNumber());
        }
        if (null != _orderData.getHolidaysBeginDate() || null != _orderData.getHolidaysEndDate())
        {
            result.append(" (предоставлены");
            if (null != _orderData.getHolidaysBeginDate())
                result.append(" с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getHolidaysBeginDate()));
            if (null != _orderData.getHolidaysEndDate())
                result.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_orderData.getHolidaysEndDate()));
            result.append(")");

        }
        return result.toString();
    }

    public String getSplitToSpecializationOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getSplitToSpecializationOrderDate(), _orderData.getSplitToSpecializationOrderNumber());
    }

    public String getPublicOrderOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getPublicOrderOrderDate(), _orderData.getPublicOrderOrderNumber());
    }

    public String getGraduateDiplomaOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getGraduateDiplomaOrderDate(), _orderData.getGraduateDiplomaOrderNumber());
    }

    public String getGraduateSuccessDiplomaOrderData()
    {
        if (_orderData == null) return "";
        return getSimpleOrderData(_orderData.getGraduateSuccessDiplomaOrderDate(), _orderData.getGraduateSuccessDiplomaOrderNumber());
    }

    public boolean isHasFormFilled()
    {
        return _hasFormFilled;
    }

    public void setHasFormFilled(boolean hasFormFilled)
    {
        _hasFormFilled = hasFormFilled;
    }


    public String getEduProgramKind()
    {
        try
        {
            return _student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getShortTitle();
        } catch (NullPointerException e)
        {
            return "";
        }
    }

    public String getContractNumber()
    {
        if (null != _student.getContractDate() && null != _student.getContractNumber())
            return _student.getContractNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_student.getContractDate());
        if (null == _student.getContractDate())
            return _student.getContractNumber();
        if (null == _student.getContractNumber())
            return DateFormatter.DEFAULT_DATE_FORMATTER.format(_student.getContractDate());
        return "";
    }

}