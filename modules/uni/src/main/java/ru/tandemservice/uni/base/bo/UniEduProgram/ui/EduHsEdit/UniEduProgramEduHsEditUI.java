package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({
    @Bind(key=UniEduProgramEduHsEditUI.PUBLISHER_ID, binding="eduHsHolder.id", required=true),
    @Bind(key=UniEduProgramEduHsEditUI.BIND_DISABLED_OU_ORIENTATION, binding=UniEduProgramEduHsEditUI.BIND_DISABLED_OU_ORIENTATION)
})
public class UniEduProgramEduHsEditUI extends UIPresenter
{
    public static final String BIND_DISABLED_OU_ORIENTATION = "disabledOrgUnitAndOrientation";
    private Boolean _disabledOrgUnitAndOrientation;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (EduProgramOrientation.DS_ORIENTATION.equals(dataSource.getName())) {
            dataSource.put(EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX, getEduHs().getEducationLevel().getEduProgramSubject().getSubjectIndex());
        }
    }

    private final EntityHolder<EducationLevelsHighSchool> eduHsHolder = new EntityHolder<>();
    public EntityHolder<EducationLevelsHighSchool> getEduHsHolder() { return this.eduHsHolder; }
    public EducationLevelsHighSchool getEduHs() { return getEduHsHolder().getValue(); }

    private ISelectModel orgUnitListModel;
    public ISelectModel getOrgUnitListModel() { return this.orgUnitListModel; }
    protected void setOrgUnitListModel(ISelectModel orgUnitListModel) { this.orgUnitListModel = orgUnitListModel; }

    // присваиваемая квалификация
    private final ISelectModel assignedQualModel = new DQLFullCheckSelectModel(EduProgramSubjectQualification.programQualification().title()) {
        @Override protected DQLSelectBuilder query(String alias, String filter) {
            EduProgramSubject subject = getEduHs().getEducationLevel().getEduProgramSubject();
            if (null == subject) { return null; }

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramSubjectQualification.class, alias);
            dql.where(eq(property(EduProgramSubjectQualification.programSubject().fromAlias(alias)), value(subject)));
            if (null != filter) {
                dql.where(or(
                    like(EduProgramSubjectQualification.programQualification().title().fromAlias(alias), filter)
                ));
            }
            dql.order(property(EduProgramSubjectQualification.programQualification().title().fromAlias(alias)));
            return dql;
        }
    };
    public ISelectModel getAssignedQualModel() { return this.assignedQualModel; }
    public boolean isAssignedQualRequired() {return null != getEduHs().getEducationLevel().getEduProgramSubject(); }

    private EduProgramSubjectQualification assignedQual;
    public EduProgramSubjectQualification getAssignedQual() { return this.assignedQual; }
    public void setAssignedQual(EduProgramSubjectQualification assignedQual) { this.assignedQual = assignedQual; }

    public boolean isShowOrientation() {
        return isAssignedQualRequired() &&
                IUniBaseDao.instance.get().existsEntity(EduProgramOrientation.class, EduProgramOrientation.subjectIndex().s(), getEduHs().getEducationLevel().getEduProgramSubject().getSubjectIndex());
    }

    @Override
    public void onComponentRefresh()
    {
        getEduHsHolder().refresh(EducationLevelsHighSchool.class);
        setOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING) {
            @Override public ListResult<OrgUnit> findValues(String filter) {
                EducationLevelsHighSchool catalogItem = getEduHs();
                if (catalogItem.getEducationLevel() == null) return ListResult.getEmpty();
                return super.findValues(filter);
            }

            @Override public Object getValue(Object primaryKey) {
                EducationLevelsHighSchool catalogItem = getEduHs();
                if (catalogItem.getEducationLevel() == null) return null;
                return super.getValue(primaryKey);
            }
        });

        if (null == getAssignedQual() && null != getEduHs().getAssignedQualification() && null != getEduHs().getEducationLevel().getEduProgramSubject()) {
            setAssignedQual(DataAccessServices.dao().getByNaturalId(new EduProgramSubjectQualification.NaturalId(getEduHs().getEducationLevel().getEduProgramSubject(), getEduHs().getAssignedQualification())));
        }
    }

    public void onClickApply()
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        EducationLevelsHighSchool eduHs = getEduHs();

        if (eduHs.getCloseDate() != null && !eduHs.getCloseDate().after(eduHs.getOpenDate())) {
            assert errors != null;
            errors.add("Дата открытия должна быть раньше даты закрытия.", "openDate", "closeDate");
        }

        if (null != getAssignedQual()) {
            eduHs.setAssignedQualification(getAssignedQual().getProgramQualification());
        }

        UniEduProgramManager.instance().syncDao().doSaveEducationLevelsHighSchool(eduHs);
        deactivate();
    }

    public Boolean getDisabledOrgUnitAndOrientation() {
        return _disabledOrgUnitAndOrientation;
    }

    public void setDisabledOrgUnitAndOrientation(Boolean disabledOrgUnitAndOrientation) {
        _disabledOrgUnitAndOrientation = disabledOrgUnitAndOrientation;
    }

    public boolean isAllowStudentsShow()
    {
        return !getEduHs().isAllowStudentsDisabled();
    }
}
