/* $Id$ */
package ru.tandemservice.uni.component.catalog.studentCustomStateCI.StudentCustomStateCIPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.IColoredEntity;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

/**
 * @author nvankov
 * @since 3/27/13
 */
public class Controller extends DefaultCatalogPubController<StudentCustomStateCI, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<StudentCustomStateCI> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", StudentCustomStateCI.P_TITLE).setOrderable(false).setWidth(30));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", StudentCustomStateCI.P_SHORT_TITLE).setOrderable(false).setClickable(false).setWidth(10));
        dataSource.addColumn(new SimpleColumn("Цвет", StudentCustomStateCI.P_HTML_COLOR)
                .setStyleResolver(IColoredEntity.COLORED_STYLE_RESOLVER).setClickable(false).setOrderable(false).setWidth(10));
        dataSource.addColumn(new SimpleColumn("Описание", StudentCustomStateCI.P_DESCRIPTION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Код(xml)", StudentCustomStateCI.P_CODE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", Qualifications.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        dataSource.setOrder("code", OrderDirection.asc);
        return dataSource;
    }
}
