/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.list;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/26/13
 */
public class AddressCountryComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public AddressCountryComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressCountry.class, "o");
        builder.order(property("o", AddressCountry.title()));
        List<AddressCountry> addressCountries = createStatement(builder).list();

        List<AddressCountryVO> addressCountryVOs = Lists.newArrayList();

        addressCountryVOs.add(new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION));

        for(AddressCountry addressCountry : addressCountries)
        {
            addressCountryVOs.add(new AddressCountryVO(addressCountry));
        }

        context.put(UIDefines.COMBO_OBJECT_LIST, addressCountryVOs);
        return super.execute(input, context);
    }
}
