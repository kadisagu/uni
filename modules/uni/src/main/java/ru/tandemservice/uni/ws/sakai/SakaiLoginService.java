/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ws.sakai;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author agolubenko
 * @since 15.09.2009
 */
public interface SakaiLoginService extends Remote
{
    String login(String login, String password) throws RemoteException;

    boolean logout(String id) throws RemoteException;
}
