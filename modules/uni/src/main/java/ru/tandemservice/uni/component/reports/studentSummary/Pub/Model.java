/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummary.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.entity.report.StudentSummaryReport;

/**
 * @author oleyba
 * @since 23.03.2009
 */
@State(keys = {PublisherActivator.PUBLISHER_ID_KEY}, bindings = {"reportId"})
public class Model
{
    private StudentSummaryReport report;
    private Long reportId;
    private CommonPostfixPermissionModel _secModel;

    public String getViewKey()
    {
        return null == getReport().getOrgUnit() ? "studentSummaryReport" : getSecModel().getPermission("orgUnit_viewStudentSummaryReport");
    }

    public Object getSecuredObject()
    {
        return null == getReport().getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getReport().getOrgUnit();
    }

    public Long getReportId()
    {
        return reportId;
    }

    public void setReportId(Long reportId)
    {
        this.reportId = reportId;
    }

    public StudentSummaryReport getReport()
    {
        return report;
    }

    public void setReport(StudentSummaryReport report)
    {
        this.report = report;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
