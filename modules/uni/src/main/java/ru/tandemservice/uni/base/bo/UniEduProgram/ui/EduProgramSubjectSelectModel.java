package ru.tandemservice.uni.base.bo.UniEduProgram.ui;

import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;

import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

/**
 * @author vdanilov
 */
public abstract class EduProgramSubjectSelectModel extends DQLFullCheckSelectModel {

    public EduProgramSubjectSelectModel(final IPropertyPath... labelProperties) {
        super(labelProperties);
    }

    public EduProgramSubjectSelectModel() {
        this(EduProgramSubject.subjectCode(), EduProgramSubject.title());
    }

    @Override public String getFullLabel(Object value) {
        return ((EduProgramSubject)value).getTitleWithCode();
    }


}
