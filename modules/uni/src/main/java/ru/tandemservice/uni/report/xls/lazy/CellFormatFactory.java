/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.report.xls.lazy;

import java.util.HashMap;
import java.util.Map;

import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;

/**
 * Фабрика шрифтов и форматов ячеек для генератора отчетов.
 *
 * @author oleyba
 * @since 10.04.2009
 */
class CellFormatFactory
{
    WritableCellFormat defaultFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8));
    Map<String, WritableCellFormat> formats = new HashMap<String, WritableCellFormat>();
    Map<String, WritableFont> fonts = new HashMap<String, WritableFont>();

    void registerFont(String key, WritableFont font)
    {
        if (font == null || key == null)
            return;
        for (WritableFont registeredFont : fonts.values())
            if (registeredFont.equals(font))
            {
                fonts.put(key, registeredFont);
                return;
            }
        fonts.put(key, font);
    }

    void registerFormat(String key, WritableCellFormat format)
    {
        if (format == null || key == null)
            return;
        for (WritableCellFormat registeredFormat : formats.values())
            if (registeredFormat.equals(format))
            {
                formats.put(key, registeredFormat);
                return;
            }
        for (WritableFont registeredFont : fonts.values())
            if (registeredFont.equals(format.getFont()))
                format.setFont(registeredFont);
        formats.put(key, format);
    }

    WritableCellFormat getFormat(String key)
    {
        if (key == null)
            return defaultFormat;
        WritableCellFormat format = formats.get(key);
        return format != null ? format : defaultFormat;         
    }
}
