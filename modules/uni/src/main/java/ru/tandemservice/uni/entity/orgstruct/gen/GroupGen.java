package ru.tandemservice.uni.entity.orgstruct.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.orgstruct.Group";
    public static final String ENTITY_NAME = "group";
    public static final int VERSION_HASH = 144324499;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_ARCHIVING_DATE = "archivingDate";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_CODE = "code";
    public static final String P_NUMBER = "number";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_COURSE = "course";
    public static final String L_START_EDUCATION_YEAR = "startEducationYear";
    public static final String L_CURATOR = "curator";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String L_ENDING_YEAR = "endingYear";
    public static final String P_DESCRIPTION = "description";

    private OrgUnit _parent;     // Подразделение
    private boolean _archival;     // Архивная
    private Date _archivingDate;     // Дата архивации
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private String _code;     // Код
    private int _number;     // Номер
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)
    private Course _course;     // Курс
    private EducationYear _startEducationYear;     // Год начала обучения
    private EmployeePost _curator;     // Куратор
    private Date _creationDate;     // Дата создания
    private EducationYear _endingYear;     // Год окончания
    private String _description;     // Описание (комментарий)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getParent()
    {
        return _parent;
    }

    /**
     * @param parent Подразделение.
     */
    public void setParent(OrgUnit parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Архивная. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival Архивная. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Дата архивации.
     */
    public Date getArchivingDate()
    {
        initLazyForGet("archivingDate");
        return _archivingDate;
    }

    /**
     * @param archivingDate Дата архивации.
     */
    public void setArchivingDate(Date archivingDate)
    {
        initLazyForSet("archivingDate");
        dirty(_archivingDate, archivingDate);
        _archivingDate = archivingDate;
    }

    /**
     * @return Название.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Код.
     */
    @Length(max=255)
    public String getCode()
    {
        initLazyForGet("code");
        return _code;
    }

    /**
     * @param code Код.
     */
    public void setCode(String code)
    {
        initLazyForSet("code");
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     */
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП).
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Год начала обучения. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getStartEducationYear()
    {
        return _startEducationYear;
    }

    /**
     * @param startEducationYear Год начала обучения. Свойство не может быть null.
     */
    public void setStartEducationYear(EducationYear startEducationYear)
    {
        dirty(_startEducationYear, startEducationYear);
        _startEducationYear = startEducationYear;
    }

    /**
     * @return Куратор.
     */
    public EmployeePost getCurator()
    {
        initLazyForGet("curator");
        return _curator;
    }

    /**
     * @param curator Куратор.
     */
    public void setCurator(EmployeePost curator)
    {
        initLazyForSet("curator");
        dirty(_curator, curator);
        _curator = curator;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        initLazyForGet("creationDate");
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        initLazyForSet("creationDate");
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Год окончания.
     */
    public EducationYear getEndingYear()
    {
        initLazyForGet("endingYear");
        return _endingYear;
    }

    /**
     * @param endingYear Год окончания.
     */
    public void setEndingYear(EducationYear endingYear)
    {
        initLazyForSet("endingYear");
        dirty(_endingYear, endingYear);
        _endingYear = endingYear;
    }

    /**
     * @return Описание (комментарий).
     */
    @Length(max=4000)
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Описание (комментарий).
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupGen)
        {
            setParent(((Group)another).getParent());
            setArchival(((Group)another).isArchival());
            setArchivingDate(((Group)another).getArchivingDate());
            setTitle(((Group)another).getTitle());
            setShortTitle(((Group)another).getShortTitle());
            setCode(((Group)another).getCode());
            setNumber(((Group)another).getNumber());
            setEducationOrgUnit(((Group)another).getEducationOrgUnit());
            setCourse(((Group)another).getCourse());
            setStartEducationYear(((Group)another).getStartEducationYear());
            setCurator(((Group)another).getCurator());
            setCreationDate(((Group)another).getCreationDate());
            setEndingYear(((Group)another).getEndingYear());
            setDescription(((Group)another).getDescription());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Group.class;
        }

        public T newInstance()
        {
            return (T) new Group();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "parent":
                    return obj.getParent();
                case "archival":
                    return obj.isArchival();
                case "archivingDate":
                    return obj.getArchivingDate();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "code":
                    return obj.getCode();
                case "number":
                    return obj.getNumber();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "course":
                    return obj.getCourse();
                case "startEducationYear":
                    return obj.getStartEducationYear();
                case "curator":
                    return obj.getCurator();
                case "creationDate":
                    return obj.getCreationDate();
                case "endingYear":
                    return obj.getEndingYear();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "parent":
                    obj.setParent((OrgUnit) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "archivingDate":
                    obj.setArchivingDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "startEducationYear":
                    obj.setStartEducationYear((EducationYear) value);
                    return;
                case "curator":
                    obj.setCurator((EmployeePost) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "endingYear":
                    obj.setEndingYear((EducationYear) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "parent":
                        return true;
                case "archival":
                        return true;
                case "archivingDate":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "code":
                        return true;
                case "number":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "course":
                        return true;
                case "startEducationYear":
                        return true;
                case "curator":
                        return true;
                case "creationDate":
                        return true;
                case "endingYear":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "parent":
                    return true;
                case "archival":
                    return true;
                case "archivingDate":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "code":
                    return true;
                case "number":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "course":
                    return true;
                case "startEducationYear":
                    return true;
                case "curator":
                    return true;
                case "creationDate":
                    return true;
                case "endingYear":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "parent":
                    return OrgUnit.class;
                case "archival":
                    return Boolean.class;
                case "archivingDate":
                    return Date.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "code":
                    return String.class;
                case "number":
                    return Integer.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "course":
                    return Course.class;
                case "startEducationYear":
                    return EducationYear.class;
                case "curator":
                    return EmployeePost.class;
                case "creationDate":
                    return Date.class;
                case "endingYear":
                    return EducationYear.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Group> _dslPath = new Path<Group>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Group");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getParent()
     */
    public static OrgUnit.Path<OrgUnit> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Архивная. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Дата архивации.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getArchivingDate()
     */
    public static PropertyPath<Date> archivingDate()
    {
        return _dslPath.archivingDate();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Код.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Год начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getStartEducationYear()
     */
    public static EducationYear.Path<EducationYear> startEducationYear()
    {
        return _dslPath.startEducationYear();
    }

    /**
     * @return Куратор.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCurator()
     */
    public static EmployeePost.Path<EmployeePost> curator()
    {
        return _dslPath.curator();
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Год окончания.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getEndingYear()
     */
    public static EducationYear.Path<EducationYear> endingYear()
    {
        return _dslPath.endingYear();
    }

    /**
     * @return Описание (комментарий).
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    public static class Path<E extends Group> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _parent;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<Date> _archivingDate;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _number;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private Course.Path<Course> _course;
        private EducationYear.Path<EducationYear> _startEducationYear;
        private EmployeePost.Path<EmployeePost> _curator;
        private PropertyPath<Date> _creationDate;
        private EducationYear.Path<EducationYear> _endingYear;
        private PropertyPath<String> _description;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getParent()
     */
        public OrgUnit.Path<OrgUnit> parent()
        {
            if(_parent == null )
                _parent = new OrgUnit.Path<OrgUnit>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Архивная. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(GroupGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Дата архивации.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getArchivingDate()
     */
        public PropertyPath<Date> archivingDate()
        {
            if(_archivingDate == null )
                _archivingDate = new PropertyPath<Date>(GroupGen.P_ARCHIVING_DATE, this);
            return _archivingDate;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(GroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(GroupGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Код.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(GroupGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(GroupGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП).
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Год начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getStartEducationYear()
     */
        public EducationYear.Path<EducationYear> startEducationYear()
        {
            if(_startEducationYear == null )
                _startEducationYear = new EducationYear.Path<EducationYear>(L_START_EDUCATION_YEAR, this);
            return _startEducationYear;
        }

    /**
     * @return Куратор.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCurator()
     */
        public EmployeePost.Path<EmployeePost> curator()
        {
            if(_curator == null )
                _curator = new EmployeePost.Path<EmployeePost>(L_CURATOR, this);
            return _curator;
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(GroupGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Год окончания.
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getEndingYear()
     */
        public EducationYear.Path<EducationYear> endingYear()
        {
            if(_endingYear == null )
                _endingYear = new EducationYear.Path<EducationYear>(L_ENDING_YEAR, this);
            return _endingYear;
        }

    /**
     * @return Описание (комментарий).
     * @see ru.tandemservice.uni.entity.orgstruct.Group#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(GroupGen.P_DESCRIPTION, this);
            return _description;
        }

        public Class getEntityClass()
        {
            return Group.class;
        }

        public String getEntityName()
        {
            return "group";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
