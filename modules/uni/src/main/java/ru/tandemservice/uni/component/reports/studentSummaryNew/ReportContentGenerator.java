/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentSummaryNew;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.report.StudentSummaryNewReport;
import ru.tandemservice.uni.report.xls.CellFormatFactory;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author dseleznev
 * @since 14.03.2012
 */
public class ReportContentGenerator
{
    public static final Map<String, String> ORG_UNIT_DATIVE_TITLE_CASE_MAP = new HashMap<String, String>();

    static
    {
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.TOP, "ОУ");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.INSTITUTE, "институту");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.COLLEGE, "колледжу");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.BRANCH, "филиалу");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.REPRESENTATION, "представительству");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.FACULTY, "факультету");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.CATHEDRA, "кафедре");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.LABORATORY, "лаборатории");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.DEPARTMENT, "департаменту");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.DIVISION, "подразделению");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.DIVISION_CENTER, "центру");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.DIVISION_MANAGEMENT, "управлению");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.DIVISION_OFFICE, "отделению");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.DIVISION_SECTOR, "сектору");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.COMMITTEE, "комитету");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.HOTEL, "общежитию");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.LIBRARY, "библиотеке");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.MUSEUM, "музею");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.POLYCLINIC, "поликлинике");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.PRESS_OFFICE, "пресс-службе");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.SERVICE, "службе");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.TECHNICAL_SCHOOL, "техникуму");
        ORG_UNIT_DATIVE_TITLE_CASE_MAP.put(OrgUnitTypeCodes.VOCATIONAL_SCHOOL, "училищу");
    }

    private StudentSummaryNewReport _report;
    private ReportParams _params;
    private Session _session;
    private CellFormatFactory _cellFormatFactory = new CellFormatFactory();
    private ErrorCollector _errors;

    private int commonMaxCourse = 0;

    public ReportContentGenerator(StudentSummaryNewReport report, ReportParams params, Session session, ErrorCollector errors)
    {
        _report = report;
        _params = params;
        _session = session;
        _errors = errors;
    }

    public byte[] generateReportContent() throws Exception
    {
        registerCellFormats();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        WritableSheet sheet = workbook.createSheet("Сводка", 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(100);
        sheet.getSettings().setBottomMargin(.3937);
        sheet.getSettings().setTopMargin(.3937);
        sheet.getSettings().setLeftMargin(.3937);
        sheet.getSettings().setRightMargin(.3937);
        sheet.getSettings().setHorizontalFreeze(3);
        sheet.getSettings().setScaleFactor(50);

        // Высота строк и ширина столбцов
        sheet.setRowView(0, 420);
        sheet.setColumnView(0, 10);
        sheet.setColumnView(1, 6);
        sheet.setColumnView(2, 47);

        int oneCourseColumnsAmount = 4 + (_params.isPregActive() ? 1 : 0) + (_params.isChildActive() ? 1 : 0);
        for (int i = 3; i <= 3 + (oneCourseColumnsAmount * 7); i++) sheet.setColumnView(i, 7);

        sheet.addCell(new Label(0, 0, "С В О Д К А   К О Н Т И Н Г Е Н Т А   С Т У Д Е Н Т О В", _cellFormatFactory.getFormat("header")));
        sheet.mergeCells(0, 0, 3 + (oneCourseColumnsAmount * 7), 0);

        int row = 1;
        List<OrgUnitBlock> preparedLines = prepareLines();
        if (preparedLines.size() == 1) sheet.getSettings().setVerticalFreeze(4);

        for (OrgUnitBlock block : preparedLines)
            row = printOrgUnitTable(sheet, block, row);

        sheet.addCell(new Label(2, row, "Итого по всему вузу", getCellFormat(false, Bold.YES, Alignment.LEFT, Color.CORAL)));
        printLine(sheet, row, Bold.YES, Color.CORAL, report_total);

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    protected void registerCellFormats() throws Exception
    {
        // Шрифты отчета
        WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
        WritableFont headerFontItalic = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
        WritableFont tableHeaderFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
        WritableFont tableHeaderSmallFont = new WritableFont(WritableFont.ARIAL, 8, WritableFont.NO_BOLD);
        WritableFont levelHeaderFont = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
        WritableFont levelHeaderFontStat = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont eduLevelHeaderFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
        WritableFont eduLevelHeaderFontStat = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
        WritableFont eduLevelHeaderFontStatBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        _cellFormatFactory.registerFont(eduLevelHeaderFontStatBold);
        _cellFormatFactory.registerFont(eduLevelHeaderFontStat);
        _cellFormatFactory.registerFont(eduLevelHeaderFont);
        _cellFormatFactory.registerFont(tableHeaderSmallFont);
        _cellFormatFactory.registerFont(levelHeaderFontStat);
        _cellFormatFactory.registerFont(tableHeaderFont);
        _cellFormatFactory.registerFont(levelHeaderFont);
        _cellFormatFactory.registerFont(headerFontItalic);
        _cellFormatFactory.registerFont(headerFont);

        // Заголовок отчета
        createWratableCellFormat("header", headerFont, Alignment.CENTRE, VerticalAlignment.CENTRE, null, null, null);

        // Заголовок над таблицей (наименование подразделения)
        createWratableCellFormat("headerItalic", headerFontItalic, Alignment.CENTRE, VerticalAlignment.CENTRE, null, null, null);

        // Заголовок таблицы
        createWratableCellFormat("tableHeader", tableHeaderFont, Alignment.CENTRE, VerticalAlignment.CENTRE, null, BorderLineStyle.MEDIUM, null); // Основной
        createWratableCellFormat("tableHeaderSmall", tableHeaderSmallFont, Alignment.CENTRE, VerticalAlignment.CENTRE, null, BorderLineStyle.THIN, new BorderLineStyle[]{null, null, null, BorderLineStyle.MEDIUM}); // Разбивка по семестрам
        createWratableCellFormat("tableHeaderSmallFirst", tableHeaderSmallFont, Alignment.CENTRE, VerticalAlignment.CENTRE, null, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, BorderLineStyle.MEDIUM}); // Разбивка по семестрам (первая колонка)
        createWratableCellFormat("tableHeaderSmallLast", tableHeaderSmallFont, Alignment.CENTRE, VerticalAlignment.CENTRE, null, BorderLineStyle.THIN, new BorderLineStyle[]{null, BorderLineStyle.MEDIUM, null, BorderLineStyle.MEDIUM}); // Разбивка по семестрам (последняя колонка)

        // Заголовок уровня направлений подготовки в таблице
        createWratableCellFormat("levelHeader", levelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, Colour.VERY_LIGHT_YELLOW, null, new BorderLineStyle[]{BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM, BorderLineStyle.THIN}); // Основной
        createWratableCellFormat("levelHeaderStat", levelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.VERY_LIGHT_YELLOW, BorderLineStyle.THIN, new BorderLineStyle[]{null, null, BorderLineStyle.MEDIUM, null}); // Разбивка по семестрам
        createWratableCellFormat("levelHeaderStatFirst", levelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.VERY_LIGHT_YELLOW, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, BorderLineStyle.MEDIUM, null}); // Разбивка по семестрам (первая колонка)
        createWratableCellFormat("levelHeaderStatLast", levelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.VERY_LIGHT_YELLOW, BorderLineStyle.THIN, new BorderLineStyle[]{null, BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM, null}); // Разбивка по семестрам (ппоследняя колонка)

        // Заголовок направления подготовки в таблице
        createWratableCellFormat("eduLevelHeader", eduLevelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, Colour.ICE_BLUE, BorderLineStyle.THIN, null); // Основной
        createWratableCellFormat("eduLevelHeaderFirst", eduLevelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, Colour.ICE_BLUE, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, null}); // Основной (первая колонка)
        createWratableCellFormat("eduLevelHeaderStat", eduLevelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.ICE_BLUE, BorderLineStyle.THIN, null); // Разбивка по семестрам
        createWratableCellFormat("eduLevelHeaderStatFirst", eduLevelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.ICE_BLUE, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, null}); // Разбивка по семестрам (первая колонка)
        createWratableCellFormat("eduLevelHeaderStatLast", eduLevelHeaderFontStatBold, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.ICE_BLUE, BorderLineStyle.THIN, new BorderLineStyle[]{null, BorderLineStyle.MEDIUM, null, null}); // Разбивка по семестрам (ппоследняя колонка)

        // Заголовок специальности в таблице
        createWratableCellFormat("specialityHeader", eduLevelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, Colour.LIGHT_TURQUOISE, BorderLineStyle.THIN, null); // Основной
        createWratableCellFormat("specialityHeaderFirst", eduLevelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, Colour.LIGHT_TURQUOISE, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, null}); // Основной (первая колонка)
        createWratableCellFormat("specialityHeaderStat", eduLevelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.LIGHT_TURQUOISE, BorderLineStyle.THIN, null); // Разбивка по семестрам
        createWratableCellFormat("specialityHeaderStatFirst", eduLevelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.LIGHT_TURQUOISE, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, null}); // Разбивка по семестрам (первая колонка)
        createWratableCellFormat("specialityHeaderStatLast", eduLevelHeaderFontStatBold, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.LIGHT_TURQUOISE, BorderLineStyle.THIN, new BorderLineStyle[]{null, BorderLineStyle.MEDIUM, null, null}); // Разбивка по семестрам (ппоследняя колонка)

        // основной текст в таблице
        createWratableCellFormat("tableCell", eduLevelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, null, BorderLineStyle.THIN, null); // Основной
        createWratableCellFormat("tableCellFirst", eduLevelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, null, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, null}); // Основной (первая колонка)
        createWratableCellFormat("tableCellStat", eduLevelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, null, BorderLineStyle.THIN, null); // Разбивка по семестрам
        createWratableCellFormat("tableCellStatFirst", eduLevelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, null, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, null, null}); // Разбивка по семестрам (первая колонка)
        createWratableCellFormat("tableCellStatLast", eduLevelHeaderFontStatBold, Alignment.RIGHT, VerticalAlignment.BOTTOM, null, BorderLineStyle.THIN, new BorderLineStyle[]{null, BorderLineStyle.MEDIUM, null, null}); // Разбивка по семестрам (ппоследняя колонка)

        // Строка "Всего по подразделению"
        createWratableCellFormat("totalFooter", levelHeaderFont, Alignment.LEFT, VerticalAlignment.BOTTOM, Colour.ROSE, BorderLineStyle.MEDIUM, null); // Основной
        createWratableCellFormat("totalFooterStat", levelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.ROSE, BorderLineStyle.THIN, new BorderLineStyle[]{null, null, BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM}); // Разбивка по семестрам
        createWratableCellFormat("totalFooterStatFirst", levelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.ROSE, BorderLineStyle.THIN, new BorderLineStyle[]{BorderLineStyle.MEDIUM, null, BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM}); // Разбивка по семестрам (первая колонка)
        createWratableCellFormat("totalFooterStatLast", levelHeaderFontStat, Alignment.RIGHT, VerticalAlignment.BOTTOM, Colour.ROSE, BorderLineStyle.THIN, new BorderLineStyle[]{null, BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM, BorderLineStyle.MEDIUM}); // Разбивка по семестрам (ппоследняя колонка)
    }

    private WritableCellFormat createWratableCellFormat(String name, WritableFont font, Alignment horizontalAlignment, VerticalAlignment verticalAlignment, Colour backColour, BorderLineStyle allBordersStyle, BorderLineStyle[] borderStyles) throws Exception
    {
        WritableCellFormat cellFormat = new WritableCellFormat(font);
        cellFormat.setVerticalAlignment(verticalAlignment);
        cellFormat.setAlignment(horizontalAlignment);
        cellFormat.setWrap(true);

        if (null != backColour) cellFormat.setBackground(backColour);
        if (null != allBordersStyle) cellFormat.setBorder(Border.ALL, allBordersStyle);
        if (null != borderStyles)
        {
            if (null != borderStyles[0]) cellFormat.setBorder(Border.LEFT, borderStyles[0]);
            if (null != borderStyles[1]) cellFormat.setBorder(Border.RIGHT, borderStyles[1]);
            if (null != borderStyles[2]) cellFormat.setBorder(Border.TOP, borderStyles[2]);
            if (null != borderStyles[3]) cellFormat.setBorder(Border.BOTTOM, borderStyles[3]);
        }

        _cellFormatFactory.registerCellFormat(name, cellFormat);
        return cellFormat;
    }

    CourseSummary[] report_contract_total = new CourseSummary[8];
    CourseSummary[] report_budget_total = new CourseSummary[8];
    CourseSummary[] report_total = new CourseSummary[8];

    {
        for (int i = 1; i <= 7; i++)
        {
            report_contract_total[i] = new CourseSummary();
            report_budget_total[i] = new CourseSummary();
            report_total[i] = new CourseSummary();
        }
    }

    // строит список строк отчета - объектов
    @SuppressWarnings("unchecked")
    private List<OrgUnitBlock> prepareLines()
    {
        // загружаем данные для построения отчета
        List<Object[]> queryList = new ArrayList<Object[]>();
        {
            String formOu = EducationOrgUnit.formativeOrgUnit().s();
            String terrOu = EducationOrgUnit.territorialOrgUnit().s();

            Criteria criteria = _session.createCriteria(Student.ENTITY_CLASS, "student");

            criteria.createAlias("student." + Student.educationOrgUnit().s(), "eduOu");
            criteria.createAlias("eduOu." + EducationOrgUnit.educationLevelHighSchool().s(), "levelhs");
            criteria.createAlias("levelhs." + EducationLevelsHighSchool.educationLevel().s(), "level");
            criteria.createAlias("eduOu." + formOu, "formOu");
            criteria.createAlias("eduOu." + terrOu, "terrOu", CriteriaSpecification.LEFT_JOIN);
            criteria.createAlias("student." + Student.course().s(), "course");
            criteria.createAlias("student." + Student.status().s(), "status");
            criteria.createAlias("student." + Student.compensationType().s(), "comp");

            criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("eduOu.id"))
                .add(Projections.groupProperty("course.code"))
                .add(Projections.groupProperty("status.code"))
                .add(Projections.groupProperty("comp.code"))
                .add(Projections.count("student.id")));

            criteria.add(Restrictions.eq("student." + Student.archival().s(), java.lang.Boolean.FALSE));

            if (_report.getOrgUnit() != null)
                criteria.add(Restrictions.or(Restrictions.eq("eduOu." + formOu, _report.getOrgUnit()), Restrictions.eq("eduOu." + terrOu, _report.getOrgUnit())));
            criteria.add(Restrictions.eq("eduOu." + EducationOrgUnit.developForm().s(), _params.getDevelopForm()));
            if (_params.isFormativeOrgUnitActive())
                addInRestricton("eduOu." + formOu, criteria, _params.getFormativeOrgUnitList());
            if (_params.isTerritorialOrgUnitActive())
                addInRestricton("eduOu." + terrOu, criteria, _params.getTerritorialOrgUnitList());
            if (_params.isDevelopConditionActive())
                addInRestricton("eduOu." + EducationOrgUnit.developCondition().s(), criteria, _params.getDevelopConditionList());
            if (_params.isDevelopTechActive())
                addInRestricton("eduOu." + EducationOrgUnit.developTech().s(), criteria, _params.getDevelopTechList());
            if (_params.isDevelopPeriodActive())
                addInRestricton("eduOu." + EducationOrgUnit.developPeriod().s(), criteria, _params.getDevelopPeriodList());
            if (_params.isQualificationActive())
                addInRestricton("level." + EducationLevels.qualification().s(), criteria, _params.getQualificationList());
            if (_params.isStudentCategoryActive())
                addInRestricton("student." + Student.studentCategory().s(), criteria, _params.getStudentCategoryList());
            //addInRestricton("student." + status, criteria, _params.getStudentStatusAllList());
            queryList.addAll(criteria.list());
        }

        // загружаем направления подготовки ОУ и ВПО
        Set<Long> eduOuIdList = new HashSet<Long>();
        for (Object[] row : queryList)
            eduOuIdList.add((Long) row[0]);
        Map<Long, EducationOrgUnit> eduOuMap = new HashMap<Long, EducationOrgUnit>();
        MQBuilder eduOuBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "e")
        .add(MQExpression.in("e", "id", eduOuIdList))
        .addLeftJoinFetch("e", EducationOrgUnit.formativeOrgUnit().s(), "f")
        .addLeftJoinFetch("e", EducationOrgUnit.territorialOrgUnit().s(), "t")
        .addLeftJoinFetch("e", EducationOrgUnit.educationLevelHighSchool().s(), "hs");
        for (EducationOrgUnit eduOu : eduOuBuilder.<EducationOrgUnit>getResultList(_session))
            eduOuMap.put(eduOu.getId(), eduOu);

        // загружаем состояния студентов
        Map<String, StudentStatus> statusMap = new HashMap<String, StudentStatus>();
        for (StudentStatus status : UniDaoFacade.getCoreDao().getCatalogItemList(StudentStatus.class))
            statusMap.put(status.getCode(), status);

        TopOrgUnit academy = TopOrgUnit.getInstance();
        if (null == academy.getAddress())
            throw new ApplicationException("Для подразделения «" + academy.getTitle() + "» (" + academy.getOrgUnitType().getTitle() + ") не указан фактический адрес.");
        if (null == academy.getAddress().getSettlement())
            throw new ApplicationException("Для подразделения «" + academy.getTitle() + "» (" + academy.getOrgUnitType().getTitle() + ") в фактическом адресе не указан населенный пункт.");
        final AddressItem academySettlement = academy.getAddress().getSettlement();

        Map<OrgUnit, OrgUnitBlock> blockMap = new HashMap<OrgUnit, OrgUnitBlock>();

        // обрабатываем данные запроса - создаем строки для направлений ОУ
        for (Object[] row : queryList)
        {
            EducationOrgUnit eduOu = eduOuMap.get((Long) row[0]);
            int course = Integer.valueOf((String) row[1]);
            StudentStatus status = statusMap.get((String) row[2]);
            String comp = (String) row[3];
            int sum = ((java.lang.Number) row[4]).intValue();

            // Вычисляем категорию группы направлений подготовки
            EducationLevelsHighSchool levelHS = eduOu.getEducationLevelHighSchool();

            boolean budget = UniDefines.COMPENSATION_TYPE_BUDGET.equals(comp);

            CourseSummary summary = new CourseSummary();

            if (_params.getStudentStatusAllList().contains(status))
                summary.total += sum;
            if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(comp))
                summary.budget += sum;
            else
                summary.contract += sum;

            if (_params.getStudentStatusAcademList().contains(status))
                summary.academ += sum;
            if (_params.isPregActive() && _params.getStudentStatusPregList().contains(status))
                summary.preg += sum;
            if (_params.isChildActive() && _params.getStudentStatusChildList().contains(status))
                summary.child += sum;

            if (summary.total == 0 && summary.academ == 0 && summary.preg == 0 && summary.child == 0)
                continue;

            OrgUnit addressOwner = eduOu.getTerritorialOrgUnit().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) ? eduOu.getTerritorialOrgUnit() : eduOu.getFormativeOrgUnit();
            if (null == addressOwner.getAddress())
                _errors.add("Для подразделения «" + addressOwner.getTitle() + "» (" + addressOwner.getOrgUnitType().getTitle() + ") не указан фактический адрес.");
            else if (null == addressOwner.getAddress().getSettlement())
                _errors.add("Для подразделения «" + addressOwner.getTitle() + "» (" + addressOwner.getOrgUnitType().getTitle() + ") в фактическом адресе не указан населенный пункт.");

            if (_errors.hasErrors())
                break;

            AddressItem settlement = addressOwner.getAddress().getSettlement();
            if (academySettlement.equals(settlement)) settlement = null;
            OrgUnitBlock orgUnitBlock = blockMap.get(addressOwner);
            if (null == orgUnitBlock)
                blockMap.put(addressOwner, orgUnitBlock = new OrgUnitBlock(addressOwner, settlement));

            String categoryTitle;
            int rank = eduOu.getDevelopPeriod().getLastCourse();

            if (null == levelHS.getEducationLevel().getQualification())
            {
                categoryTitle = "ПРОЧИЕ";
            } else if (QualificationsCodes.BAKALAVR.equals(levelHS.getEducationLevel().getQualification().getCode()))
            {
                rank += 50;
                categoryTitle = "БАКАЛАВРИАТ";
            } else if (QualificationsCodes.SPETSIALIST.equals(levelHS.getEducationLevel().getQualification().getCode()))
            {
                rank += 40;
                categoryTitle = "СПЕЦИАЛИТЕТ";
            } else if (QualificationsCodes.MAGISTR.equals(levelHS.getEducationLevel().getQualification().getCode()))
            {
                rank += 30;
                categoryTitle = "МАГИСТРАТУРА";
            } else if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(levelHS.getEducationLevel().getQualification().getCode()))
            {
                rank += 20;
                categoryTitle = "ПОВЫШЕННЫЙ УРОВЕНЬ СПО";
            } else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(levelHS.getEducationLevel().getQualification().getCode()))
            {
                rank += 10;
                categoryTitle = "БАЗОВЫЙ УРОВЕНЬ СПО";
            } else
            {
                categoryTitle = "ДРУГОЕ";
            }

            Long categoryId = (long) rank;
            categoryTitle += " (" + eduOu.getDevelopPeriod().getTitle() + ")";
            CategoryBlock categoryBlock = orgUnitBlock.categoryBlocks.get(categoryId);
            if (null == categoryBlock)
                orgUnitBlock.categoryBlocks.put(categoryId, categoryBlock = new CategoryBlock(rank, categoryTitle, orgUnitBlock));

            EducationLevels currentLevel = levelHS.getEducationLevel();
            List<EducationLevels> eduLevelsTree = new ArrayList<EducationLevels>();
            while (null != currentLevel)
            {
                eduLevelsTree.add(0, currentLevel);
                currentLevel = currentLevel.getParentLevel();
            }

            EduLevelBlock parentEduLevelBlock = null;
            for (EducationLevels eduLevel : eduLevelsTree)
            {
                int level = eduLevelsTree.indexOf(eduLevel);
                boolean leaf = eduLevel.equals(levelHS.getEducationLevel());
                EduLevelBlock eduLevelBlock = null != parentEduLevelBlock ? parentEduLevelBlock.eduLevelBlocks.get(eduLevel.getId()) : null;
                if (null == eduLevelBlock && level == 0)
                    eduLevelBlock = categoryBlock.eduLevelBlocks.get(eduLevel.getId());
                EduLevelType levelType = level == 2 ? EduLevelType.SPECIALIZATION : (level == 1 ? EduLevelType.SPECIALITY : ReportContentGenerator.EduLevelType.EDU_LEVEL);

                if (null == eduLevelBlock)
                {
                    if (leaf)
                        eduLevelBlock = new EduLevelBlock(eduLevel.getTitleCodePrefix(), levelHS.getShortTitle(), levelHS.getTitle(), levelType, null == parentEduLevelBlock ? categoryBlock : null, parentEduLevelBlock);
                    else
                        eduLevelBlock = new EduLevelBlock(eduLevel.getTitleCodePrefix(), null, eduLevel.getTitle(), levelType, null == parentEduLevelBlock ? categoryBlock : null, parentEduLevelBlock);
                    if (null == parentEduLevelBlock) categoryBlock.eduLevelBlocks.put(eduLevel.getId(), eduLevelBlock);
                    else parentEduLevelBlock.eduLevelBlocks.put(eduLevel.getId(), eduLevelBlock);
                }
                if (leaf) eduLevelBlock.addSummary(course, summary, budget);
                parentEduLevelBlock = eduLevelBlock;
            }
        }

        if (_errors.hasErrors())
            return Collections.emptyList();

        final List<OrgUnitBlock> result = new ArrayList<OrgUnitBlock>();
        result.addAll(blockMap.values());
        // сортируем результаты

        // по названию, но город вуза первым
        Collections.sort(result, new Comparator<OrgUnitBlock>()
            {
            @Override
            public int compare(OrgUnitBlock o1, OrgUnitBlock o2)
            {
                if ((null == o1.settlement && null != o2.settlement) || (null != o1.settlement && o1.settlement.equals(academySettlement)))
                    return -1;
                if ((null != o1.settlement && null == o2.settlement) || (null != o2.settlement && o2.settlement.equals(academySettlement)))
                    return 1;
                if (null == o1.settlement && null == o2.settlement) return 0;
                return o1.settlement.getTitle().compareTo(o2.settlement.getTitle());
            }
            });
        return result;
    }

    private <T extends IEntity> void addInRestricton(String property, Criteria criteria, Collection<T> entites)
    {
        List<Long> idList = new ArrayList<Long>();
        for (IEntity entity : entites)
            idList.add(entity.getId());
        criteria.add(Restrictions.in(property + ".id", idList));
    }

    private class OrgUnitBlock
    {
        int maxCourse = 1;

        String title;
        OrgUnit orgUnit;
        AddressItem settlement;
        CourseSummary[] statistics = new CourseSummary[8];
        Map<Long, CategoryBlock> categoryBlocks = new LinkedHashMap<Long, CategoryBlock>();

        OrgUnitBlock(OrgUnit orgUnit, AddressItem settlement)
        {
            this.orgUnit = orgUnit;
            this.settlement = settlement;
            this.title = null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getFullTitle();

            for (int i = 0; i < statistics.length; i++)
            {
                statistics[i] = new CourseSummary();
            }
        }
    }

    private class CategoryBlock implements Comparable
    {
        int rank;
        String title;
        OrgUnitBlock orgUnitBlock;
        CourseSummary[] statistics = new CourseSummary[8];
        Map<Long, EduLevelBlock> eduLevelBlocks = new LinkedHashMap<Long, EduLevelBlock>();

        CategoryBlock(int rank, String title, OrgUnitBlock orgUnitBlock)
        {
            this.rank = rank;
            this.title = title;
            this.orgUnitBlock = orgUnitBlock;

            for (int i = 0; i < statistics.length; i++)
            {
                statistics[i] = new CourseSummary();
            }
        }

        void addSummary(int course, CourseSummary summary, boolean budget)
        {
            if (course > commonMaxCourse) commonMaxCourse = course;
            if (course > orgUnitBlock.maxCourse) orgUnitBlock.maxCourse = course;

            for (int i : new int[]{course, 7})
            {
                statistics[i].inc(summary);
                report_total[i].inc(summary);
                orgUnitBlock.statistics[i].inc(summary);
                if (budget) report_budget_total[i].inc(summary);
                else report_contract_total[i].inc(summary);
            }
        }

        @Override
        public int compareTo(Object o)
        {
            return rank > ((CategoryBlock) o).rank ? -1 : 1;
        }
    }

    private class EduLevelBlock implements Comparable
    {
        String code;
        String title;
        String shortTitle;
        EduLevelType levelType;
        CategoryBlock parentCategory;
        EduLevelBlock parentEduLevelBlock;
        CourseSummary[] statistics = new CourseSummary[8];
        Map<Long, EduLevelBlock> eduLevelBlocks = new LinkedHashMap<Long, EduLevelBlock>();

        EduLevelBlock(String code, String shortTitle, String title, EduLevelType levelType, CategoryBlock parentCategory, EduLevelBlock parentEduLevelBlock)
        {
            this.code = code;
            this.title = title;
            this.shortTitle = shortTitle;
            this.levelType = levelType;
            this.parentCategory = parentCategory;
            this.parentEduLevelBlock = parentEduLevelBlock;

            for (int i = 0; i < statistics.length; i++)
            {
                statistics[i] = new CourseSummary();
            }
        }

        void addSummary(int course, CourseSummary summary, boolean budget)
        {
            for (int i : new int[]{course, 7})
            {
                statistics[i].inc(summary);
            }
            if (null != parentCategory) parentCategory.addSummary(course, summary, budget);
            else parentEduLevelBlock.addSummary(course, summary, budget);
        }

        @Override
        public int compareTo(Object o)
        {
            int result = 0;
            EduLevelBlock otherBlock = (EduLevelBlock) o;

            if (null != code && otherBlock.code == null) result = 1;
            else if (null == code && otherBlock.code != null) result = -1;
            else if (null != code && otherBlock.code != null) result = code.compareTo(otherBlock.code);

            if (0 == result)
            {
                if (null != shortTitle && otherBlock.shortTitle == null) result = 1;
                else if (null == shortTitle && otherBlock.shortTitle != null) result = -1;
                else if (null != shortTitle && otherBlock.shortTitle != null)
                    result = shortTitle.compareTo(otherBlock.shortTitle);
            }
            if (0 == result) result = title.compareTo(otherBlock.title);

            return result;
        }
    }

    private static enum EduLevelType
    {
        EDU_LEVEL, SPECIALITY, SPECIALIZATION
    }


    // вспомогательный объект для построения отчета -
    // количества студентов на курсе по направлению
    private class CourseSummary
    {
        int total = 0;
        int budget = 0;
        int contract = 0;
        int academ = 0;
        int preg = 0;
        int child = 0;

        public void inc(CourseSummary summary)
        {
            this.total += summary.total;
            this.budget += summary.budget;
            this.contract += summary.contract;
            this.academ += summary.academ;
            this.preg += summary.preg;
            this.child += summary.child;
        }
    }

    private int printOrgUnitTable(WritableSheet sheet, OrgUnitBlock block, int row) throws Exception
    {
        // заголовок таблицы
        sheet.setRowView(row, 465);
        sheet.setRowView(row + 1, 465);
        sheet.setRowView(row + 2, 1230);
        int oneCourseColumnsAmount = 4 + (_params.isPregActive() ? 1 : 0) + (_params.isChildActive() ? 1 : 0);
        String orgUnitTitle = block.title + (null != block.settlement ? (" (" + block.settlement.getTitleWithType() + ")") : "");
        sheet.addCell(new Label(0, row, orgUnitTitle, _cellFormatFactory.getFormat("headerItalic")));
        sheet.mergeCells(0, row, 3 + (oneCourseColumnsAmount * 7), row++);
        WritableCellFormat header = _cellFormatFactory.getFormat("tableHeader");
        WritableCellFormat headerSmall = _cellFormatFactory.getFormat("tableHeaderSmall");
        WritableCellFormat headerSmallFirst = _cellFormatFactory.getFormat("tableHeaderSmallFirst");
        WritableCellFormat headerSmallLast = _cellFormatFactory.getFormat("tableHeaderSmallLast");

        //sheet.setRowView(row + 1, 3 * 230 + 115);
        sheet.addCell(new Label(0, row, "код", header));
        sheet.mergeCells(0, row, 0, row + 1);
        sheet.addCell(new Label(1, row, "Наименование специальностей и направлений подготовки", header));
        sheet.mergeCells(1, row, 2, row + 1);
        int column = 2;
        int courseBlockWidth = 4;
        if (_params.isPregActive()) courseBlockWidth++;
        if (_params.isChildActive()) courseBlockWidth++;

        //TODO: определять количество курсов
        for (int course = 1; course <= (block.maxCourse); course++)
        {
            sheet.addCell(new Label(++column, row, course + " курс", header));
            sheet.mergeCells(column, row, column + courseBlockWidth - 1, row);
            sheet.addCell(new Label(column, row + 1, "итого", headerSmallFirst));
            sheet.addCell(new Label(++column, row + 1, "бюджет", headerSmall));
            sheet.addCell(new Label(++column, row + 1, "договор", headerSmall));
            sheet.addCell(new Label(++column, row + 1, "а/о", headerSmall));
            if (_params.isPregActive()) sheet.addCell(new Label(++column, row + 1, "От.б.р.", headerSmall));
            if (_params.isChildActive()) sheet.addCell(new Label(++column, row + 1, "От.у.р.", headerSmall));
        }
        sheet.addCell(new Label(++column, row, "ВСЕГО", header));
        sheet.mergeCells(column, row, column + courseBlockWidth - 1, row);
        sheet.addCell(new Label(column, row + 1, "итого", headerSmallFirst));
        sheet.addCell(new Label(++column, row + 1, "бюджет", headerSmall));
        sheet.addCell(new Label(++column, row + 1, "договор", headerSmall));
        if (!_params.isPregActive() && !_params.isChildActive())
            sheet.addCell(new Label(++column, row + 1, "а/о", headerSmallLast));
        else
            sheet.addCell(new Label(++column, row + 1, "а/о", headerSmall));
        if (_params.isPregActive())
            sheet.addCell(new Label(++column, row + 1, "От.б.р.", _params.isChildActive() ? headerSmall : headerSmallLast));
        if (_params.isChildActive()) sheet.addCell(new Label(++column, row + 1, "От.у.р.", headerSmallLast));

        List<CategoryBlock> categories = new ArrayList<CategoryBlock>();
        categories.addAll(block.categoryBlocks.values());
        Collections.sort(categories);

        // таблица

        row = row + 2;
        for (CategoryBlock category : categories)
            row = printCategory(sheet, category, row);

        // итоги

        String orgUnitTypeDative = ORG_UNIT_DATIVE_TITLE_CASE_MAP.get(block.orgUnit.getOrgUnitType().getCode());
        if (null == orgUnitTypeDative) orgUnitTypeDative = "подразделению";

        sheet.addCell(new Label(0, row, "ВСЕГО ПО " + orgUnitTypeDative.toUpperCase(), _cellFormatFactory.getFormat("totalFooter")));
        printLine(sheet, row, block.statistics, "totalFooter", block.maxCourse, true);
        sheet.mergeCells(0, row, 2, row++);

        return row + 2;
    }

    private int printCategory(WritableSheet sheet, CategoryBlock block, int row) throws Exception
    {
        sheet.setRowView(row, 420);
        sheet.addCell(new Label(0, row, block.title, _cellFormatFactory.getFormat("levelHeader")));
        printLine(sheet, row, block.statistics, "levelHeader", block.orgUnitBlock.maxCourse, true);
        sheet.mergeCells(0, row, 2, row);

        for (EduLevelBlock eduLevelBlock : block.eduLevelBlocks.values())
            row = printEduLevel(sheet, eduLevelBlock, ++row);

        return ++row;
    }

    private int printEduLevel(WritableSheet sheet, EduLevelBlock block, int row) throws Exception
    {
        /*int rowHeight = 300;
        if(EduLevelType.EDU_LEVEL.equals(block.levelType)) rowHeight = 375;
        if(EduLevelType.SPECIALITY.equals(block.levelType)) rowHeight = 360;
        sheet.setRowView(row, rowHeight);*/

        Color backColor = Color.NONE;
        String styleSuffix = "tableCell";

        if (EduLevelType.EDU_LEVEL.equals(block.levelType))
        {
            backColor = Color.ICE_BLUE;
            styleSuffix = "eduLevelHeader";
        }
        if (EduLevelType.SPECIALITY.equals(block.levelType))
        {
            backColor = Color.LIGHT_TURQUOISE;
            styleSuffix = "specialityHeader";
        }

        if (null != block.code)
            sheet.addCell(new Label(0, row, block.code, _cellFormatFactory.getFormat(styleSuffix + "First")));
        else sheet.addCell(new Blank(0, row, _cellFormatFactory.getFormat(styleSuffix)));
        if (null != block.shortTitle)
            sheet.addCell(new Label(1, row, block.shortTitle, _cellFormatFactory.getFormat(styleSuffix)));
        else sheet.addCell(new Blank(1, row, _cellFormatFactory.getFormat(styleSuffix)));

        sheet.addCell(new Label(2, row, block.title, _cellFormatFactory.getFormat(styleSuffix)));

        EduLevelBlock parentBlock = block;
        while (null == parentBlock.parentCategory)
            parentBlock = parentBlock.parentEduLevelBlock;
        int orgUnitMaxCourse = parentBlock.parentCategory.orgUnitBlock.maxCourse;
        printLine(sheet, row, block.statistics, styleSuffix, orgUnitMaxCourse, false);

        for (EduLevelBlock eduLevelBlock : block.eduLevelBlocks.values())
            row = printEduLevel(sheet, eduLevelBlock, ++row);

        return row;
    }

    private void printLine(WritableSheet sheet, int row, CourseSummary[] summaries, String styleSuffix, int orgUnitMaxCourse, boolean printZeroes) throws Exception
    {
        //First,Stat,StatFirst,StatLast
        int column = 2;
        int[] printCells = new int[orgUnitMaxCourse + 1];
        for (int i = 1; i <= orgUnitMaxCourse; i++) printCells[i - 1] = i;
        printCells[orgUnitMaxCourse] = 7;

        for (int course : printCells)
        {
            if (course == 7) printZeroes = true;
            sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].total, _cellFormatFactory.getFormat(styleSuffix + "StatFirst"), printZeroes));
            sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].budget, _cellFormatFactory.getFormat(styleSuffix + "Stat"), printZeroes));
            sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].contract, _cellFormatFactory.getFormat(styleSuffix + "Stat"), printZeroes));
            if (!_params.isPregActive() && !_params.isChildActive())
                sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].academ, _cellFormatFactory.getFormat(styleSuffix + "StatLast"), printZeroes));
            else
                sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].academ, _cellFormatFactory.getFormat(styleSuffix + "Stat"), printZeroes));
            if (_params.isPregActive())
                sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].preg, _cellFormatFactory.getFormat(styleSuffix + (_params.isChildActive() ? "Stat" : "StatLast")), printZeroes));
            if (_params.isChildActive())
                sheet.addCell(getCellZeroProcessed(++column, row, summaries[course].child, _cellFormatFactory.getFormat(styleSuffix + "StatLast"), printZeroes));
        }
    }

    private void printLine(WritableSheet sheet, int row, Bold bold, Color color, CourseSummary[] summaries) throws Exception
    {
        int column = 2;
        int[] printCells = new int[commonMaxCourse + 1];
        for (int i = 1; i <= commonMaxCourse; i++) printCells[i - 1] = i;
        printCells[commonMaxCourse] = 7;

        for (int course : printCells)
        {
            createCell(sheet, ++column, row, summaries[course].total, bold, color);
            createCell(sheet, ++column, row, summaries[course].budget, bold, color);
            createCell(sheet, ++column, row, summaries[course].contract, bold, color);
            createCell(sheet, ++column, row, summaries[course].academ, Bold.YES, color);
            if (_params.isPregActive())
                createCell(sheet, ++column, row, summaries[course].preg, Bold.YES, color);
            if (_params.isChildActive())
                createCell(sheet, ++column, row, summaries[course].child, Bold.YES, color);
        }
    }

    private WritableCell getCellZeroProcessed(int column, int row, int value, WritableCellFormat cellFormat, boolean printZeroes)
    {
        if (printZeroes || 0 != value)
            return new jxl.write.Number(column, row, value, cellFormat);
        else
            return new jxl.write.Label(column, row, "", cellFormat);
    }

    private void createCell(WritableSheet sheet, int column, int row, int value, Bold bold, Color color) throws Exception
    {
        sheet.addCell(new jxl.write.Number(column, row, value, getCellFormat((value == 0), bold, Alignment.RIGHT, color)));
    }

    private WritableCellFormat getCellFormat(boolean zero, Bold bold, Alignment alignment, Color backgroundColor) throws Exception
    {
        return getCellFormat(zero, false, bold, alignment, backgroundColor);
    }

    /**
     * Возвращает формат ячейки по заданным параметрам
     * Если еще не существует, то создает и регистрирует
     *
     * @param zero            в ячейке ноль или нет
     * @param flip            вертикально
     * @param bold            жирный
     * @param alignment       выравнивание
     * @param backgroundColor цвет фона
     * @return формат ячейки по заданным параметрам
     * @throws Exception если что-нибудь сломалось в jxl
     */
    private WritableCellFormat getCellFormat(boolean zero, boolean flip, Bold bold, Alignment alignment, Color backgroundColor) throws Exception
    {
        String cellFormatName = getCellFormatName(zero, flip, bold, alignment, backgroundColor);
        WritableCellFormat result = _cellFormatFactory.getFormat(cellFormatName);
        if (result == null)
        {
            WritableFont font = new WritableFont(_cellFormatFactory.getDefaultCellFormat().getFont());
            if (zero)
                font.setColour(Colour.GREY_25_PERCENT);
            font.setBoldStyle((bold == Bold.YES) ? WritableFont.BOLD : WritableFont.NO_BOLD);
            _cellFormatFactory.registerFont(font);

            result = new WritableCellFormat(font);
            result.setWrap(true);
            if (alignment != null)
                result.setAlignment(alignment);
            if (flip)
            {
                result.setOrientation(Orientation.PLUS_90);
                result.setVerticalAlignment(VerticalAlignment.CENTRE);
            }
            switch (backgroundColor)
            {
                case VERY_LIGHT_YELLOW: result.setBackground(Colour.VERY_LIGHT_YELLOW); break;
                case ICE_BLUE: result.setBackground(Colour.ICE_BLUE); break;
                case LIGHT_TURQUOISE: result.setBackground(Colour.LIGHT_TURQUOISE); break;
                case ROSE: result.setBackground(Colour.ROSE); break;
                case IVORY: result.setBackground(Colour.IVORY); break;
                case CORAL: result.setBackground(Colour.CORAL); break;
            }
            result.setBorder(Border.ALL, BorderLineStyle.THIN);
            _cellFormatFactory.registerCellFormat(getCellFormatName(zero, flip, bold, alignment, backgroundColor), result);
        }
        return result;
    }

    /**
     * @param zero            в ячейке ноль или нет
     * @param bold            жирный
     * @param alignment       выравнивание
     * @param backgroundColor цвет фона
     * @return название формата ячейки по заданным параметрам
     */
    private String getCellFormatName(boolean zero, boolean flip, Bold bold, Alignment alignment, Color backgroundColor)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(java.lang.Boolean.toString(zero));
        sb.append(java.lang.Boolean.toString(flip));
        sb.append(bold.toString());
        sb.append(alignment.getDescription());
        sb.append(backgroundColor.toString());
        return sb.toString();
    }

    private static enum Color
    {
        NONE, VERY_LIGHT_YELLOW, ICE_BLUE, LIGHT_TURQUOISE, ROSE, IVORY, CORAL
    }

    private static enum Bold
    {
        YES, NO
    }
}