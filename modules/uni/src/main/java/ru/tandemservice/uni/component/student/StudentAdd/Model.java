/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentAdd;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.IPersonSimilarAddValidator;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
@Input(keys = { "groupId" }, bindings = { "groupId" })
@Output(keys = { "similarPersons", "validator", "personString", "infoString" }, bindings = { "similarPersons", "validator", "personString", "infoString" })
public class Model extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Model<Student> implements IEducationLevelModel
{
    private Long _groupId;
    private Group _group;
    private Student _student = new Student();
    private IPrincipalContext _principalContext;

    private ISelectModel _nationalityModel;
    private List<Course> _courseList;
    private List<CompensationType> _compensationTypesList;
    private List<StudentCategory> _studentCategoryList;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;

    public IPersonSimilarAddValidator getValidator()
    {
        return new IPersonSimilarAddValidator()
        {
            @Override
            public void validate(Person person)
            {
                for (Student role : person.getRoles(Student.class))
                {
                    if (null != role.getGroup() && null != _group && _group.equals(role.getGroup()))
                    {
                        throw new ApplicationException("Данная персона уже является студентом текущей группы.");
                    }
                }
            }
        };
    }

    @Override
    protected Student getNewInstance()
    {
        return getStudent();
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public ISelectModel getNationalityModel()
    {
        return _nationalityModel;
    }

    public void setNationalityModel(ISelectModel nationalityModel)
    {
        _nationalityModel = nationalityModel;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public List<CompensationType> getCompensationTypesList()
    {
        return _compensationTypesList;
    }

    public void setCompensationTypesList(List<CompensationType> compensationTypesList)
    {
        _compensationTypesList = compensationTypesList;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    @Override
    public String getPersonString()
    {
        return "Добавление студента: " + super.getPersonString();
    }

    @Override
    public String getInfoString()
    {
        return "В процессе регистрации студента найдены персоны с похожими личными данными. Вы можете зарегистрировать студента на основе одной из существующих персон, или создать новую.<br/>Для того, чтобы зарегистрировать студента на основе одной из существующих персон, выберите её в списке.";
    }

    @Override
    protected String getNewPersonButtonName()
    {
        return "Добавить студента, создав новую персону";
    }

    @Override
    protected String getOnBasisPersonButtonName()
    {
        return "Добавить студента на основе существующей персоны";
    }
}