/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.StudentsEduOrgUnitChange;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.List;

/**
 * @author oleyba
 * @since 09.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setGroup(getNotNull(Group.class, model.getGroup().getId()));

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, model.getPrincipalContext()));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model));

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));

        EducationOrgUnit educationOrgUnit = model.getGroup().getEducationOrgUnit();

        model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
        model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        model.setDevelopForm(educationOrgUnit.getDevelopForm());
        model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
        model.setDevelopTech(educationOrgUnit.getDevelopTech());
        model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
        builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
        builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
        builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME);
        List<Student> studentList = builder.getResultList(getSession());
        model.getDataSource().setCountRow(studentList.size());
        UniBaseUtils.createPage(model.getDataSource(), studentList);
    }

    @Override
    public void update(Model model)
    {
        EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);
        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("selected")).getSelectedObjects())
        {
            Student student = (Student) entity;
            student.setEducationOrgUnit(educationOrgUnit);
            update(student);
        }
    }
}
