package ru.tandemservice.uni.dao.mdbio;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uni.dao.mdbio.BaseIODao.ICatalogIO;
import ru.tandemservice.uni.entity.catalog.*;
import org.tandemframework.shared.person.catalog.entity.EducationDocumentType;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.core.util.cache.SpringBeanCache;

import com.healthmarketscience.jackcess.Database;

import java.io.IOException;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface ICatalogIODao {
    public static final SpringBeanCache<ICatalogIODao> instance = new SpringBeanCache<ICatalogIODao>(ICatalogIODao.class.getName());

    ICatalogIO<Sex> catalogSex();
    ICatalogIO<FamilyStatus> catalogFamilyStatus();
    ICatalogIO<PensionType> catalogPensionType();
    ICatalogIO<FlatPresence> catalogFlatPresence();
    ICatalogIO<EduDocumentKind> catalogEduDocumentKind();
    ICatalogIO<EduLevel> catalogEduLevel();
    ICatalogIO<EducationalInstitutionTypeKind> catalogEducationalInstitutionTypeKind();
    ICatalogIO<EducationDocumentType> catalogEducationDocumentType();
    ICatalogIO<EmployeeSpeciality> catalogEmployeeSpeciality();
    ICatalogIO<DiplomaQualifications> catalogDiplomaQualifications();
    ICatalogIO<GraduationHonour> catalogGraduationHonour();
    ICatalogIO<EducationLevelStage> catalogEducationLevelStage();
    ICatalogIO<SportType> catalogSportType();
    ICatalogIO<SportRank> catalogSportRank();
    ICatalogIO<RelationDegree> catalogRelationDegree();
    ICatalogIO<ForeignLanguage> catalogForeignLanguage();
    ICatalogIO<ForeignLanguageSkill> catalogForeignLanguageSkill();
    ICatalogIO<CompensationType> catalogCompensationType();
    ICatalogIO<StudentCategory> catalogStudentCategory();
    ICatalogIO<StudentStatus> catalogStudentStatusCategory();
    ICatalogIO<Benefit> catalogBenefitCategory();
    ICatalogIO<IdentityCardType> catalogIdentityCardType();

    ICatalogIO<DevelopForm> catalogDevelopForm();
    ICatalogIO<DevelopCondition> catalogDevelopCondition();
    ICatalogIO<DevelopTech> catalogDevelopTech();
    ICatalogIO<DevelopGrid> catalogDevelopGrid();


    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<EduInstitution, String> export_catalogEducationalInstitution(Database mdb) throws IOException;
    @Transactional
    Map<String, EduInstitution> import_catalogEducationalInstitution(Database mdb) throws IOException;

}
