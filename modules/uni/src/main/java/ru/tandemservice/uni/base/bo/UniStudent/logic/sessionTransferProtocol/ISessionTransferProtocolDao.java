/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
public interface ISessionTransferProtocolDao  extends INeedPersistenceSupport
{

    /**
     * @param student Студент
     * @return ISingleSelectModel протоколов (SessionTransferProtocolDocument) студента
     */
    ISingleSelectModel getSessionTransferProtocolModel(Student student);

    /**
     * Возвращает документ об образовании из протокола перезачтения студента
     *
     * @param protocolNumber Номер протокола
     * @param protocolDate   Дата протокола
     * @param student        Студент
     * @return Документ об образовании из протокола перезачтения студента
     */
    PersonEduDocument getEduDocumentFromProtocol(String protocolNumber, Date protocolDate, Student student);

    /**
     * @param protocolNumber Номер протокола
     * @param protocolDate   Дата протокола
     * @param student        Студент
     * @return Протокол(SessionTransferProtocolDocument) студента
     */
    ISessionTransferProtocolProperty getSessionTransferProtocolDocument(String protocolNumber, Date protocolDate, Student student);
}
