/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MergeBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static org.tandemframework.shared.commonbase.utils.MergeBuilder.byLinkRule;
import static org.tandemframework.shared.commonbase.utils.MergeBuilder.skipRule;

/**
 * @author Nikolay Fedorovskih
 * @since 11.12.2015
 */
public class MS_uni_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    public static String appliedBachelorSubjectCodes() {
        return "('2013.04.03.01','2013.05.03.03','2013.05.03.05','2013.05.03.06','2013.07.03.01','2013.07.03.03','2013.07.03.04','2013.08.03.01','2013.09.03.01','2013.09.03.02','2013.09.03.03','2013.09.03.04','2013.11.03.01','2013.11.03.02','2013.11.03.03','2013.11.03.04','2013.12.03.01','2013.12.03.02','2013.12.03.03','2013.12.03.04','2013.12.03.05','2013.13.03.01','2013.13.03.02','2013.13.03.03','2013.14.03.01','2013.14.03.02','2013.15.03.01','2013.15.03.02','2013.15.03.03','2013.15.03.04','2013.15.03.05','2013.15.03.06','2013.16.03.03','2013.17.03.01','2013.18.03.01','2013.19.03.02','2013.19.03.03','2013.19.03.04','2013.20.03.01','2013.20.03.02','2013.21.03.01','2013.21.03.02','2013.21.03.03','2013.22.03.01','2013.22.03.02','2013.23.03.01','2013.23.03.02','2013.23.03.03','2013.24.03.01','2013.24.03.02','2013.24.03.03','2013.24.03.04','2013.24.03.05','2013.25.03.01','2013.25.03.02','2013.26.03.02','2013.27.03.02','2013.27.03.03','2013.27.03.04','2013.27.03.05','2013.28.03.01','2013.28.03.02','2013.29.03.01','2013.29.03.02','2013.29.03.03','2013.29.03.05','2013.35.03.01','2013.35.03.02','2013.35.03.03','2013.35.03.04','2013.35.03.05','2013.35.03.06','2013.35.03.07','2013.35.03.08','2013.36.03.01','2013.36.03.02','2013.38.03.01','2013.38.03.02','2013.38.03.03','2013.38.03.04','2013.38.03.06','2013.38.03.07','2013.39.03.01','2013.39.03.02','2013.39.03.03','2013.40.03.01','2013.41.03.06','2013.42.03.01','2013.42.03.03','2013.42.03.05','2013.43.03.01','2013.43.03.02','2013.43.03.03','2013.44.03.01','2013.44.03.02','2013.44.03.03','2013.44.03.04','2013.44.03.05','2013.46.03.02','2013.51.03.01','2013.52.03.01','2013.52.03.02','2013.52.03.03','2013.54.03.01','2013.54.03.02','2013.54.03.03','2013.54.03.04')";
    }

    public static void legalAutoFix(DBTool tool) throws SQLException {

        if (tool.columnExists("educationlevelshighschool_t", "programorientation_id")) {
            // Уже не актуально
            return;
        }

        // Всем ОП, УП и НПв, у которых квалификация не указана или указана неверная (неразрешенная для направления),
        // автоматически присваивать квалификацию разрешенную для направления, если она такая одна.

        final String relsSql = "select programsubject_id s, min(programqualification_id) q from edu_c_pr_subject_qual_t group by programsubject_id having count(*) = 1";

        // НПв
        {
            final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevelshighschool_t", "hs");
            upd.set("assignedqualification_id", "rel.q");
            upd.where("hs.assignedqualification_id is null or hs.assignedqualification_id <> rel.q");
            upd.getUpdatedTableFrom()
                    .innerJoin(SQLFrom.table("educationlevels_t", "l"), "l.id = hs.educationlevel_id")
                    .innerJoin(SQLFrom.select(relsSql, "rel"), "rel.s=l.eduprogramsubject_id");

            final int n = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
            if (n > 0) {
                tool.info(n + " rows auto fixed qualification in educationlevelshighschool_t");
            }
        }

        // ОП
        for (final String table : new String[] {"edu_program_prof_higher_t", "edu_program_prof_secondary_t"}) {

            if (!tool.columnExists(table, "programqualification_id")) {
                continue;
            }
            final SQLUpdateQuery upd = new SQLUpdateQuery(table, "t");
            upd.set("programqualification_id", "rel.q");
            upd.where("t.programqualification_id is null or t.programqualification_id <> rel.q");
            upd.getUpdatedTableFrom()
                    .innerJoin(SQLFrom.table("edu_program_prof_t", "p"), "p.id=t.id")
                    .innerJoin(SQLFrom.select(relsSql, "rel"), "rel.s=p.programsubject_id");

            final int n = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
            if (n > 0) {
                tool.info(n + " rows auto fixed qualification in " + table);
            }
        }

        // УП
        if (tool.columnExists("epp_eduplan_prof_t", "programqualification_id"))
        {
            final SQLUpdateQuery upd = new SQLUpdateQuery("epp_eduplan_prof_t", "t");
            upd.set("programqualification_id", "rel.q");
            upd.where("t.programqualification_id is null or t.programqualification_id <> rel.q");
            upd.getUpdatedTableFrom().innerJoin(SQLFrom.select(relsSql, "rel"), "rel.s=t.programsubject_id");

            final int n = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
            if (n > 0 && Debug.isEnabled()) {
                tool.info(n + " rows auto fixed qualification in epp_eduplan_prof_t");
            }
        }
    }

    @Override
    public void run(DBTool tool) throws Exception {

        // Мержим квалификации с одинаковым названием в рамках перечня и кода уровня
        final MergeBuilder.MergeRule mainRule = new MergeBuilder.MergeRuleBuilder("edu_c_pr_qual_t")
                .key("subjectindex_id", "title_p", "qualificationlevelcode_p")
                .order(tool.getDialect().getLengthFunction("code_p"), OrderDirection.desc)
                .build();

        MigrationUtils.mergeBuilder(tool, mainRule)
                .addRule(byLinkRule("eduprogramqualificationtofis_t", 0, "qualifications_id", "eceducationlevelfis_id"))
                .addRule(byLinkRule("edu_c_pr_subject_qual_t", 0, "programqualification_id", "programsubject_id"))
                .addRule(byLinkRule("edulevelshs2qualification_t", 0, "programqualification_id", "educationlevelshighschool_id"))
                .addRule(skipRule("educationlevelshighschool_t", "educationlevel_id", "orgunit_id", "assignedqualification_id", "programorientation_id", "title_p"))
                .addRule(skipRule("educationlevelshighschool_t", "educationlevel_id", "orgunit_id", "assignedqualification_id", "programorientation_id", "shorttitle_p"))
                .addRule(skipRule("nsunedprgrmdscplnsttstcs_t", "developform_id", "course_id", "term_id", "qualification_id", "educationlevels_id", "formativeorgunit_id", "territorialorgunit_id"))
                .merge("eduProgramQualification");

        // Сначала восстановим связи с прикладным бакалавром
        final Map<String, Long> subjectIndexMap = MigrationUtils.getCatalogCode2IdMap(tool, "edu_c_pr_subject_index_t");
        final Long index_bachelor2013 = subjectIndexMap.get("2013.03");
        final long q_appliedBachelor = tool.getNumericResult("select id from edu_c_pr_qual_t where title_p=? and subjectindex_id=?", "Прикладной бакалавр", index_bachelor2013);
        if (q_appliedBachelor != 0) {
            final short relCode = tool.entityCodes().ensure("eduProgramSubjectQualification");
            final MigrationUtils.BatchInsertBuilder ins = new MigrationUtils.BatchInsertBuilder(relCode, "programsubject_id", "programqualification_id");
            final List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(1), "select id from edu_c_pr_subject_t where subjectindex_id=? and code_p in " + appliedBachelorSubjectCodes(), index_bachelor2013);
            for (final Object[] row : rows) {
                final Long subjId = (Long) row[0];
                if (!tool.hasResultRows("select id from edu_c_pr_subject_qual_t where programsubject_id=? and programqualification_id=?", subjId, q_appliedBachelor)) {
                    ins.addRow(subjId, q_appliedBachelor);
                }
            }
            ins.executeInsert(tool, "edu_c_pr_subject_qual_t");
        }

        legalAutoFix(tool);
    }
}