/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author agolubenko
 * @since 18.05.2009
 */
public class DAO extends UniDao<Model>
{
    @Override
    public void prepare(final Model model)
    {
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechListModel(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(StudentCategory.class));
        model.setStudentStatusListModel(new LazySimpleSelectModel<>(getCatalogItemList(StudentStatus.class, StudentStatus.P_USED_IN_SYSTEM, true)));
    }

    @Override
    public void update(final Model model)
    {
        final IFormatter<Object> formatter = CollectionFormatter.COLLECTION_FORMATTER;

        final StudentsAgeSexDistributionReport report = model.getReport();
        report.setFormingDate(new Date());

        if(model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(StringUtils.trimToNull(formatter.format(model.getFormativeOrgUnitList())));
        else
            report.setFormativeOrgUnit(null);

        report.setTerritorialOrgUnit((model.isTerritorialOrgUnitActive() && model.getTerritorialOrgUnitList().isEmpty()) ? "не указано" : StringUtils.trimToNull(formatter.format(model.getTerritorialOrgUnitList())));

        if(model.isDevelopFormActive())
            report.setDevelopForm(StringUtils.trimToNull(formatter.format(model.getDevelopFormList())));
        else
            report.setDevelopForm(null);

        if(model.isDevelopConditionActive())
            report.setDevelopConditition(StringUtils.trimToNull(formatter.format(model.getDevelopConditionList())));
        else
            report.setDevelopConditition(null);

        if(model.isDevelopTechActive())
            report.setDevelopTech(StringUtils.trimToNull(formatter.format(model.getDevelopTechList())));
        else
            report.setDevelopTech(null);

        if(model.isQualificationActive())
            report.setQualification(StringUtils.trimToNull(formatter.format(model.getQualificationList())));
        else
            report.setQualification(null);

        if(model.isStudentCategoryActive())
            report.setStudentCategory(StringUtils.trimToNull(formatter.format(model.getStudentCategoryList())));
        else
            report.setStudentCategory(null);

        if(model.isStudentStatusActive())
            report.setStudentStatus(StringUtils.trimToNull(formatter.format(model.getStudentStatusList())));
        else
            report.setStudentStatus(null);

        final DatabaseFile databaseFile = new DatabaseFile();

        final UniScriptItem scriptItem = getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENTS_AGE_SEX_DISTRIBUTION_REPORT);

        final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                "students", getReportData(model),
                "report", model.getReport());

        databaseFile.setContent((byte[])scriptResult.get(IScriptExecutor.DOCUMENT));
        databaseFile.setFilename((String) scriptResult.get(IScriptExecutor.FILE_NAME));

        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }


    /**
     * @param model модель
     * @return студенты, удовлетворяющие параметрам отчета
     */
    @SuppressWarnings("unchecked")
    private List<Student> getReportData(final Model model)
    {
        final Criteria criteria = getSession().createCriteria(Student.class);
        criteria.createAlias(Student.L_PERSON, "person");
        criteria.createAlias("person." + Person.L_IDENTITY_CARD, "identityCard");
        criteria.createAlias(Student.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.createAlias("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelsHighSchool");
        criteria.createAlias("educationLevelsHighSchool." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "educationLevel");
        criteria.setFetchMode("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, FetchMode.JOIN);
        criteria.setFetchMode("person.identityCard", FetchMode.JOIN);

        if (model.isFormativeOrgUnitActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
        }
        if (model.isTerritorialOrgUnitActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
        }
        if (model.isDevelopFormActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopFormList()));
        }
        if (model.isDevelopTechActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTechList()));
        }
        if (model.isDevelopConditionActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionList()));
        }
        if (model.isQualificationActive())
        {
            criteria.add(Restrictions.in("educationLevel." + EducationLevels.L_QUALIFICATION, model.getQualificationList()));
        }
        if (model.isStudentCategoryActive())
        {
            criteria.add(Restrictions.in(Student.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        }
        if (model.isStudentStatusActive())
        {
            criteria.add(Restrictions.in(Student.L_STATUS, model.getStudentStatusList()));
        }
        criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));

        return criteria.list();
    }
}
