package ru.tandemservice.uni.component.person.util;

import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;

/**
 * @author Vasily Zhukov
 * @since 24.03.2011
 */
public interface ISecureRoleContextOwner
{
    String SECURE_ROLE_CONTEXT = "secureRoleContext";

    ISecureRoleContext getSecureRoleContext();
}
