/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentAdd;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;

import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardEditInline.PersonIdentityCardEditInline;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class Controller extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.Controller<IDAO, Model, Student>
{

    public static final String IDENTITY_CARD_REGION = "identityCardRegion";

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getModel(context).setPrincipalContext(context.getUserContext().getPrincipalContext());
        super.onRefreshComponent(context);
        context.createChildRegion(IDENTITY_CARD_REGION, new ComponentActivator(PersonIdentityCardEditInline.class.getSimpleName(),
                new ParametersMap()
                        .add(PersonIdentityCardEditInline.PERSON, getModel(context).getPerson())));
    }

}
