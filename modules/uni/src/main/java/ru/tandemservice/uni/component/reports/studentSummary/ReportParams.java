/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummary;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.report.StudentSummaryReport;
import ru.tandemservice.uni.util.UniStringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 26.03.2009
 */
public class ReportParams
{
    private boolean _formativeOrgUnitActive;
    private List<OrgUnit> _formativeOrgUnitList;

    private boolean _territorialOrgUnitActive;
    private List<OrgUnit> _territorialOrgUnitList;

    private DevelopForm _developForm;

    private boolean _developTechActive;
    private List<DevelopTech> _developTechList;

    private boolean _developPeriodActive;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _developConditionActive;
    private List<DevelopCondition> _developConditionList;


    private boolean _qualificationActive;
    private List<Qualifications> _qualificationList;

    private boolean _studentCategoryActive;
    private List<StudentCategory> _studentCategoryList;

    private DataWrapper showSpecializations;

    private List<StudentStatus> _studentStatusAllList = new ArrayList<StudentStatus>();
    private List<StudentStatus> _studentStatusAcademList = new ArrayList<StudentStatus>();

    private boolean pregActive;
    private List<StudentStatus> _studentStatusPregList = new ArrayList<StudentStatus>();

    private boolean childActive;
    private List<StudentStatus> _studentStatusChildList = new ArrayList<StudentStatus>();

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<StudentStatus> getStudentStatusAllList()
    {
        return _studentStatusAllList;
    }

    public void setStudentStatusAllList(List<StudentStatus> studentStatusAllList)
    {
        _studentStatusAllList = studentStatusAllList;
    }

    public List<StudentStatus> getStudentStatusAcademList()
    {
        return _studentStatusAcademList;
    }

    public void setStudentStatusAcademList(List<StudentStatus> studentStatusAcademList)
    {
        _studentStatusAcademList = studentStatusAcademList;
    }

    public boolean isPregActive()
    {
        return pregActive;
    }

    public void setPregActive(boolean pregActive)
    {
        this.pregActive = pregActive;
    }

    public List<StudentStatus> getStudentStatusPregList()
    {
        return _studentStatusPregList;
    }

    public void setStudentStatusPregList(List<StudentStatus> studentStatusPregList)
    {
        _studentStatusPregList = studentStatusPregList;
    }

    public boolean isChildActive()
    {
        return childActive;
    }

    public void setChildActive(boolean childActive)
    {
        this.childActive = childActive;
    }

    public List<StudentStatus> getStudentStatusChildList()
    {
        return _studentStatusChildList;
    }

    public void setStudentStatusChildList(List<StudentStatus> studentStatusChildList)
    {
        _studentStatusChildList = studentStatusChildList;
    }

    public DataWrapper getShowSpecializations()
    {
        return showSpecializations;
    }

    public void setShowSpecializations(DataWrapper showSpecializations)
    {
        this.showSpecializations = showSpecializations;
    }

    /**
     * Копирует параметры в отчет в нужном формате.
     * @param report отчет
     */
    public void setParams(StudentSummaryReport report)
    {
        report.setDevelopForm(getDevelopForm().getTitle());
        if (isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(getFormativeOrgUnitList(), OrgUnit.P_TITLE, "; "));
        if (isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(UniStringUtils.join(getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_TITLE, "; "));
        if (isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
        if (isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(getDevelopTechList(), DevelopTech.P_TITLE, "; "));
        if (isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(getDevelopPeriodList(), DevelopPeriod.P_TITLE, "; "));
        if (isStudentCategoryActive())
            report.setStudentCategory(UniStringUtils.join(getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        if (isQualificationActive())
            report.setQualification(UniStringUtils.join(getQualificationList(), Qualifications.P_TITLE, "; "));
        report.setShowSpecializations(getShowSpecializations().getTitle());
        report.setStudentStatusAll(UniStringUtils.join(getStudentStatusAllList(), StudentStatus.P_TITLE, "; "));
        report.setStudentStatusAcadem(UniStringUtils.join(getStudentStatusAcademList(), StudentStatus.P_TITLE, "; "));
        if (isPregActive())
            report.setStudentStatusPreg(UniStringUtils.join(getStudentStatusPregList(), StudentStatus.P_TITLE, "; "));
        if (isChildActive())
            report.setStudentStatusChild(UniStringUtils.join(getStudentStatusChildList(), StudentStatus.P_TITLE, "; "));
    }
}
