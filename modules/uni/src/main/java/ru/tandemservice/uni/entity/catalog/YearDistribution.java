package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uni.entity.catalog.gen.YearDistributionGen;

/**
 * Разбиение учебного года
 */
public class YearDistribution extends YearDistributionGen implements IHierarchyItem, Comparable<YearDistribution>, ITitled
{
    @Override
    public int compareTo(YearDistribution o) {
        return getCode().compareTo(o.getCode());
    }

    @Override public IHierarchyItem getHierarhyParent() {
        return null;
    }
}