/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.AbstractGroupList;

import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 */
public abstract class DAO<M extends Model> extends UniDao<M> implements IDAO<M>
{
    private static final OrderDescriptionRegistry _groupOrderSettings = new OrderDescriptionRegistry("g");

    static
    {
        _groupOrderSettings.setOrders(Group.COURSE_KEY, new OrderDescription("g", new String[]{Group.L_COURSE, ICatalogItem.CATALOG_ITEM_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
        _groupOrderSettings.setOrders(Group.FORMATIVE_ORGUNIT_KEY, new OrderDescription("g", new String[]{Group.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
        _groupOrderSettings.setOrders(Group.TERRITORIAL_ORGUNIT_KEY, new OrderDescription("g", new String[]{Group.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
        _groupOrderSettings.setOrders(Group.PRODUCTIVE_ORGUNIT_KEY, new OrderDescription("g", new String[]{Group.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
    }

    @Override
    public void prepare(final M model)
    {
        if (model.getOrgUnitId() != null)
            model.setOrgUnit(getNotNull(model.getOrgUnitId()));

        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitListModel(new LazySimpleSelectModel<>(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)));
        model.setEducationLevelsModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<OrgUnit> formativeOrgUnitList = model.getSettings().get("formativeOrgUnitList");
                List<OrgUnit> territorialOrgUnitList = model.getSettings().get("territorialOrgUnitList");
                List<EducationLevelsHighSchool> list = UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(formativeOrgUnitList, territorialOrgUnitList, filter);
                return new ListResult<>(list);
            }
        });
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopPeriodListModel(EducationCatalogsManager.getDevelopPeriodSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setEndingYearListModel(new LazySimpleSelectModel<>(EducationYear.class));
    }

    @Override
    public final void prepareListDataSource(M model)
    {

        DynamicListDataSource dataSource = model.getDataSource();
        MQBuilder builder = getFinalGroupsListBuilder(model);

        boolean countOrdering = model.getDataSource().getEntityOrder().getKeyString().equals(Group.P_ACTIVE_STUDENT_COUNT);

        DQLSelectBuilder countBuilder = getDqlSelectBuilder(model);

        if (!countOrdering)
        {
            _groupOrderSettings.applyOrder(builder, dataSource.getEntityOrder());
            UniBaseUtils.createPage(dataSource, builder, getSession());

            countBuilder.where(in(property("g"), dataSource.getEntityList()));
            List<Object[]> items = countBuilder.createStatement(getSession()).list();
            Map<Long, Long> mapTemp = Maps.newLinkedHashMap();
            for (Object[] item : items)
            {
                mapTemp.put((Long)item[0], (Long)item[1]);
            }

            // inject view properties
            for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
            {
                Long value = mapTemp.get(wrapper.getId());
                wrapper.setViewProperty(Group.P_ACTIVE_STUDENT_COUNT, value);
            }
        }

        else {

            countBuilder.order(property("count"), dataSource.getEntityOrder().getDirection())
                    .order(property("g", Group.title()))
                    .order(property("g.id"));

            DSInput input = new DSInput();
            input.setStartRecord((int)model.getDataSource().getActualStartRow());
            input.setCountRecord((int)model.getDataSource().getCountRow());

            DSOutput output = DQLSelectOutputBuilder.get(input, countBuilder, getSession()).pageable(true).build();
            List<Object[]> items = output.getRecordList();
            Map<Long, Long> mapTemp = Maps.newLinkedHashMap();
            for (Object[] item : items)
            {
                mapTemp.put((Long)item[0], (Long)item[1]);
            }

            final List<Long> idList = new ArrayList<>(mapTemp.keySet());
            builder.add(MQExpression.in("g", "id", mapTemp.keySet()));

            dataSource.setTotalSize(output.getTotalSize());

            List list = getList(builder);
            dataSource.createPage(list);

            Collections.sort(dataSource.getEntityList(), new Comparator<Group>()
            {
                public int compare(Group o1, Group o2)
                {
                    return idList.indexOf(o1.getId()) - idList.indexOf(o2.getId());
                }
            });

            // inject view properties
            for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
            {
                Long value = mapTemp.get(wrapper.getId());
                wrapper.setViewProperty(Group.P_ACTIVE_STUDENT_COUNT, value);
            }

        }
        patchGroupList(dataSource);

    }


    //TODO Убрать копипаст, переписав основной запрос на DQL

    public DQLSelectBuilder getDqlSelectBuilder(Model model)
    {

        IDataSettings settings = model.getSettings();

        Object title = settings.get("title");
        Object courseList = settings.get("courseList");
        Object formativeOrgUnitList = settings.get("formativeOrgUnitList");
        Object territorialOrgUnitList = settings.get("territorialOrgUnitList");
        Object producingOrgUnitList = settings.get("producingOrgUnitList");
        Object eduLevelHighSchool = settings.get("eduLevelHighSchool");
        Object qualification = settings.get("qualification");
        Object developFormList = settings.get("developFormList");
        Object developConditionList = settings.get("developConditionList");
        Object developPeriodList = settings.get("developPeriodList");
        Object endingYear = model.getSettings().get("endingYear");
        List<Employee> curatorList = model.getSettings().get("curatorList");

        DQLSelectBuilder countBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
        countBuilder.column(property("g", Group.id()), "groupId")
                .column(DQLFunctions.coalesce(property("count_t.count_p"), value(0)), "count");
        countBuilder.joinDataSource("g", DQLJoinType.left,
                                    new DQLSelectBuilder().fromEntity(Student.class, "s")
                                            .column(property("s", Student.group().id()), "group_id")
                                            .column(DQLFunctions.count(property(Student.id().fromAlias("s"))), "count_p")
                                            .where(eq(property("s", Student.archival()), value(false)))
                                            .where(eq(property("s", Student.status().code()), value(UniDefines.CATALOG_STUDENT_STATUS_ACTIVE)))
                                            .group(property("s", Student.group())).buildQuery(),
                                    "count_t",
                                    eq("g.id", "count_t.group_id"));


        if (model.getOrgUnitId() != null) {


            Collection<String> kindSet = UniDaoFacade.getOrgstructDao().getAllowGroupsOrgUnitKindCodes(model.getOrgUnit());

            IDQLExpression condition = DQLExpressions.eq(property("g", Group.id()), value(0L)); // false

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)) {
                condition = DQLExpressions.or(condition, DQLExpressions.eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit()), value(model.getOrgUnit())));
            }

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)) {
                condition = DQLExpressions.or(condition, DQLExpressions.eq(property("g", Group.educationOrgUnit().formativeOrgUnit()), value(model.getOrgUnit())));
            }

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)) {
                condition = DQLExpressions.or(condition, DQLExpressions.eq(property("g", Group.educationOrgUnit().territorialOrgUnit()), value(model.getOrgUnit())));
            }

            countBuilder.where(condition);
        }

        if(model.isArchival() != null)
            countBuilder.where(eq(property("g", Group.P_ARCHIVAL), value(model.isArchival())));

        if (title != null)
            countBuilder.where(DQLExpressions.likeUpper(property("g", Group.title()), value(CoreStringUtils.escapeLike((String)title))));
        if (courseList != null && !((List) courseList).isEmpty())
            countBuilder.where(in(property("g", Group.course()), (Collection) courseList));
        if (formativeOrgUnitList != null && !((List) formativeOrgUnitList).isEmpty())
            countBuilder.where(in(property("g", Group.educationOrgUnit().formativeOrgUnit()), (Collection) formativeOrgUnitList));
        if (territorialOrgUnitList != null && !((List) territorialOrgUnitList).isEmpty())
            countBuilder.where(in(property("g", Group.educationOrgUnit().territorialOrgUnit().s()), (Collection) territorialOrgUnitList));
        if (producingOrgUnitList != null && !((List) producingOrgUnitList).isEmpty())
            countBuilder.where(in(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit()), (Collection) producingOrgUnitList));
        if (eduLevelHighSchool != null)
            countBuilder.where(eq(property("g", Group.educationOrgUnit().educationLevelHighSchool()), commonValue(eduLevelHighSchool)));
        if (qualification != null)
            countBuilder.where(eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification()), commonValue(qualification)));
        if (developFormList != null && !((List) developFormList).isEmpty())
            countBuilder.where(in(property("g", Group.educationOrgUnit().developForm()), (Collection) developFormList));
        if (developConditionList != null && !((List) developConditionList).isEmpty())
            countBuilder.where(in(property("g", Group.educationOrgUnit().developCondition()), (Collection) developConditionList));
        if (developPeriodList != null && !((List) developPeriodList).isEmpty())
            countBuilder.where(in(property("g", Group.educationOrgUnit().developPeriod()), (Collection) developPeriodList));
        if (endingYear != null)
            countBuilder.where(eq(property("g", Group.L_ENDING_YEAR), commonValue(endingYear)));
        if (CollectionUtils.isNotEmpty(curatorList))
            countBuilder.where(in(property("g", Group.curator().employee()), curatorList));

        return countBuilder;
    }


    @Override
    public List<Long> getFinalGroupIdsList(M model)
    {
        MQBuilder builder = getFinalGroupsListBuilder(model);
        builder.addSelect("g", new String[]{Group.P_ID});
        return builder.getResultList(getSession());
    }

    @SuppressWarnings("unchecked")
    protected MQBuilder getFinalGroupsListBuilder(M model)
    {
        IDataSettings settings = model.getSettings();

        Object title = settings.get("title");
        Object courseList = settings.get("courseList");
        Object formativeOrgUnitList = settings.get("formativeOrgUnitList");
        Object territorialOrgUnitList = settings.get("territorialOrgUnitList");
        Object producingOrgUnitList = settings.get("producingOrgUnitList");
        Object eduLevelHighSchool = settings.get("eduLevelHighSchool");
        Object qualification = settings.get("qualification");
        Object developFormList = settings.get("developFormList");
        Object developConditionList = settings.get("developConditionList");
        Object developPeriodList = settings.get("developPeriodList");
        Object endingYear = model.getSettings().get("endingYear");
        List<Employee> curatorList = model.getSettings().get("curatorList");

        MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");

        if (model.getOrgUnitId() != null) {


            Collection<String> kindSet = UniDaoFacade.getOrgstructDao().getAllowGroupsOrgUnitKindCodes(model.getOrgUnit());

            AbstractExpression condition = MQExpression.eq("g", "id", 0L); // false

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)) {
                condition = MQExpression.or(condition, MQExpression.eq("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit(), model.getOrgUnit()));
            }

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)) {
                condition = MQExpression.or(condition, MQExpression.eq("g", Group.educationOrgUnit().formativeOrgUnit(), model.getOrgUnit()));
            }

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)) {
                condition = MQExpression.or(condition, MQExpression.eq("g", Group.educationOrgUnit().territorialOrgUnit(), model.getOrgUnit()));
            }

            builder.add(condition);
        }

	    if(model.isArchival() != null)
            builder.add(MQExpression.eq("g", Group.P_ARCHIVAL, model.isArchival()));

        if (title != null)
            builder.add(MQExpression.like("g", Group.title().s(), "%" + title));
        if (courseList != null && !((List) courseList).isEmpty())
            builder.add(MQExpression.in("g", Group.course().s(), (Collection) courseList));
        if (formativeOrgUnitList != null && !((List) formativeOrgUnitList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().formativeOrgUnit().s(), (Collection) formativeOrgUnitList));
        if (territorialOrgUnitList != null && !((List) territorialOrgUnitList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().territorialOrgUnit().s(), (Collection) territorialOrgUnitList));
        if (producingOrgUnitList != null && !((List) producingOrgUnitList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit().s(), (Collection) producingOrgUnitList));
        if (eduLevelHighSchool != null)
            builder.add(MQExpression.eq("g", Group.educationOrgUnit().educationLevelHighSchool().s(), eduLevelHighSchool));
        if (qualification != null)
            builder.add(MQExpression.eq("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), qualification));
        if (developFormList != null && !((List) developFormList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().developForm().s(), (Collection) developFormList));
        if (developConditionList != null && !((List) developConditionList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().developCondition().s(), (Collection) developConditionList));
        if (developPeriodList != null && !((List) developPeriodList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().developPeriod().s(), (Collection) developPeriodList));
        if (endingYear != null)
            builder.add(MQExpression.eq("g", Group.L_ENDING_YEAR, endingYear));
        if (CollectionUtils.isNotEmpty(curatorList))
            builder.add(MQExpression.in("g", Group.curator().employee().s(), curatorList));

        return builder;
    }

    //    @SuppressWarnings("unchecked")
    //    private Set<Long> getGroupIds(M model)
    //    {
    //        Set<String> kindSet = new HashSet<String>();
    //        for (OrgUnitKind orgUnitKind : UniDaoFacade.getOrgstructDao().getOrgUnitKindList(model.getOrgUnit()))
    //            if (orgUnitKind.isAllowGroups())
    //                kindSet.add(orgUnitKind.getCode());
    //
    //        //сводный список групп (их id-шники)
    //        Set<Long> dataList = new HashSet<Long>();
    //
    //        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
    //        {
    //            //Для выпускающих подразделений – группы, для которых текущее подразделение является выпускающим через связь с НПв.
    //            Criteria c = getSession().createCriteria(Group.class, "gr");
    //            c.createAlias("gr." + Group.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
    //            c.createAlias("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "highSchool");
    //            c.add(Restrictions.eq("highSchool." + EducationLevelsHighSchool.L_ORG_UNIT, model.getOrgUnit()));
    //            c.setProjection(Projections.property(Group.P_ID));
    //            dataList.addAll(c.list());
    //        }
    //
    //        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
    //        {
    //            //Для формирующих подразделений будет отображаться перечень групп, для которых текущее подразделение является формирующим через связь с направления подготовки (специальностью) подразделения.
    //            Criteria c = getSession().createCriteria(Group.class, "gr");
    //            c.createAlias("gr." + Group.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
    //            c.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit()));
    //            c.setProjection(Projections.property(Group.P_ID));
    //            dataList.addAll(c.list());
    //        }
    //
    //        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
    //        {
    //            //Для территориальных подразделений – группы, для которых текущее подразделение является территориальным подразделением через связь с направлением подготовки (специальностью) подразделения.
    //            Criteria c = getSession().createCriteria(Group.class, "gr");
    //            c.createAlias("gr." + Group.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
    //            c.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit()));
    //            c.setProjection(Projections.property(Group.P_ID));
    //            dataList.addAll(c.list());
    //        }
    //        return dataList;
    //    }

    @SuppressWarnings("unchecked")
    protected void patchGroupList(DynamicListDataSource dataSource)
    {
    }
}
