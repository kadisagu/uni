/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.ui;

import java.util.Arrays;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;

/**
 * @author agolubenko
 * @since 05.05.2009
 */
public interface IStudentListModel
{
    Long STUDENT_STATUS_ACTIVE = 0L;
    Long STUDENT_STATUS_NON_ACTIVE = 1L;
    List<IdentifiableWrapper> STUDENT_STATUS_OPTION_LIST = Arrays.asList(new IdentifiableWrapper(STUDENT_STATUS_ACTIVE, "Да"), new IdentifiableWrapper(STUDENT_STATUS_NON_ACTIVE, "Нет"));

    List<IdentifiableWrapper> getStudentStatusOptionList();
}
