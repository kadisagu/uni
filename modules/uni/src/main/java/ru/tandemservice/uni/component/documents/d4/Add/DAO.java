/* $Id$ */
package ru.tandemservice.uni.component.documents.d4.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uni.util.formatters.PersonIofFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 22.08.2013
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        TopOrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();
        Calendar currentYear = Calendar.getInstance();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());

        model.setCurrentYear(currentYear.get(Calendar.YEAR));
        model.setFormingDate(new Date());

        model.setCertificateSeria(academyData.getCertificateSeria());
        model.setCertificateNumber(academyData.getCertificateNumber());
        model.setCertificateRegNumber(academyData.getCertificateRegNumber());
        model.setCertificateDate(academyData.getCertificateDate());
        model.setCertificateExpiriationDate(academyData.getCertificateExpiriationDate());
        model.setCertificationAgency(academyData.getCertificationAgency());

        model.setLicenceSeria(academyData.getLicenceSeria());
        model.setLicenceNumber(academyData.getLicenceNumber());
        model.setLicenceRegNumber(academyData.getLicenceRegNumber());
        model.setLicenceDate(academyData.getLicenceDate());
        model.setLicensingAgency(academyData.getLicensingAgency());

        model.setUniTitleAtTimeAdmission(academy.getTitle());
        model.setEntranceYear(model.getStudent().getEntranceYear());
        model.setEducationLevelStageHint("Сведения об образовании призывника до поступления в данное образовательное учреждение заполняются только образовательными учреждениями среднего профессионального образования и начального профессионального образования.");

        model.setEducationLevelStage(PersonEduDocumentManager.instance().dao().getHighestEduLevel(model.getStudent().getPerson().getId()));

        if (orderData != null)
        {
            model.setEnrollmentOrderNumber(orderData.getEduEnrollmentOrderNumber());
            model.setEnrollmentOrderDate(orderData.getEduEnrollmentOrderDate());
        }

        List<PersonMilitaryStatus> list = getList(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, model.getStudent().getPerson());

        if (list.size() > 0)
        {
            PersonMilitaryStatus militaryStatus = (PersonMilitaryStatus)list.get(0);
            if (militaryStatus.getMilitaryOffice() != null)
            {
                model.setMilitaryOffice(militaryStatus.getMilitaryOffice().getTitle());
            }
        }

        if (academy.getHead() != null)
        {
            model.setRectorAltStr(PersonIofFormatter.INSTANCE.format((IdentityCard) academy.getHead().getEmployee().getPerson().getIdentityCard()));
        }

        List<IdentifiableWrapper> monthList = new ArrayList<>(12);
        for (int i = 1; i <= 12; i++)
            monthList.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));

        model.setMonthList(monthList);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        SimpleDateFormat YY_DATE = new SimpleDateFormat("yyyy");

        if (model.getEntranceYear() != null)
        {
            int plannedEndYear = Integer.parseInt(YY_DATE.format(model.getPlannedEndDate()));
            if (plannedEndYear < 1990)
                errors.add("Планируемый год окончания должен быть не меньше 1990", "plannedEndDate");

            if (plannedEndYear < model.getCurrentYear())
                errors.add("Планируемый год окончания должен быть больше текущего " + model.getCurrentYear(), "plannedEndDate");

            if (plannedEndYear < model.getEntranceYear())
                errors.add("Планируемый год окончания должен быть позже года поступления.", "entranceYear", "plannedEndDate");
        }
        if (!model.isTrainingOfficersActive())
        {
            model.setProgramStartYear(null);
            model.setMonth(null);
            model.setProgramEndYear(null);
            model.setManagerMilitaryDepartment(null);
        } else {
            if (model.getProgramStartYear() != null && model.getProgramEndYear() != null && model.getProgramStartYear() >= model.getProgramEndYear())
                errors.add("Год окончания должен быть позже года вступления.", "programStartYear", "programEndYear");
        }
    }
}
