/* $Id: Controller.java 11362 2010-02-09 09:59:26Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;

/**
 * @author AutoGenerator
 * @since 16.06.2008
 */
public class Controller extends DefaultCatalogPubController<DevelopGrid, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<DevelopGrid> dataSource = new DynamicListDataSource<DevelopGrid>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", DevelopGrid.P_TITLE));
        dataSource.addColumn(new BlockColumn("grid", "Сетка").setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", DevelopGrid.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisabledProperty("deleteDisabled"));
        return dataSource;
    }
}
