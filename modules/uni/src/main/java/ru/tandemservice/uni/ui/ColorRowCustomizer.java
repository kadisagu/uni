package ru.tandemservice.uni.ui;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import ru.tandemservice.uni.entity.catalog.IColoredEntity;

/**
 * @author vdanilov
 */
public class ColorRowCustomizer<T extends IEntity> implements IRowCustomizer<T> {

    private String _coloredProperty;

    public ColorRowCustomizer<T> coloredProperty(String property)
    {
        _coloredProperty = property;
        return this;
    }

    @Override
    public String getHeadRowStyle() {
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getRowStyle(T entity) {
        if(!StringUtils.isEmpty(_coloredProperty)) {
            entity = (T) entity.getProperty(_coloredProperty);
        }

        if (entity instanceof IColoredEntity) {
            return "color:" + ((IColoredEntity) entity).getHtmlColor();
        }

        return "";
    }

    @Override
    public boolean isClickable(AbstractColumn<?> column, T rowEntity) {
        return false;
    }

    @Override
    public boolean isClickable(IPublisherLinkColumn column, T rowEntity, IEntity itemEntity) {
        return true; // иначе ни одна IPublisherLinkColumn во всем списке кликабельна не будет
    }
}
