/**
 *$Id$
 */
package ru.tandemservice.uni.dao.student;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * Фасад для настроек размеров стипендий по умолчанию
 *
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public interface IAcademicGrantSizeDAO
{
    public static final SpringBeanCache<IAcademicGrantSizeDAO> instance = new SpringBeanCache<>(IAcademicGrantSizeDAO.class.getName());

    /**
     * @return размер академической стипендии по умолчанию
     */
    public Integer getDefaultGrantSize();

    /**
     * @return размер надбавки старостам по умолчанию
     */
    public Integer getDefaultGroupManagerBonusSize();

    /**
     * @return размер социальной стипендии по умолчанию
     */
    public Integer getDefaultSocialGrantSize();
}