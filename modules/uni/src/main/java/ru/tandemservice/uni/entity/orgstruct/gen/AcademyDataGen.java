package ru.tandemservice.uni.entity.orgstruct.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительные данные ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcademyDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.orgstruct.AcademyData";
    public static final String ENTITY_NAME = "academyData";
    public static final int VERSION_HASH = 1234616630;
    private static IEntityMeta ENTITY_META;

    public static final String P_LICENCE_SERIA = "licenceSeria";
    public static final String P_LICENCE_NUMBER = "licenceNumber";
    public static final String P_LICENCE_REG_NUMBER = "licenceRegNumber";
    public static final String P_LICENCE_DATE = "licenceDate";
    public static final String P_LICENCE_EXPIRIATION_DATE = "licenceExpiriationDate";
    public static final String P_LICENSING_AGENCY = "licensingAgency";
    public static final String P_CERTIFICATE_SERIA = "certificateSeria";
    public static final String P_CERTIFICATE_NUMBER = "certificateNumber";
    public static final String P_CERTIFICATE_REG_NUMBER = "certificateRegNumber";
    public static final String P_CERTIFICATE_DATE = "certificateDate";
    public static final String P_CERTIFICATE_EXPIRIATION_DATE = "certificateExpiriationDate";
    public static final String P_CERTIFICATION_AGENCY = "certificationAgency";
    public static final String P_CERTIFICATE_TITLE = "certificateTitle";
    public static final String P_LICENCE_TITLE = "licenceTitle";

    private String _licenceSeria;     // Серия лицензии
    private String _licenceNumber;     // Номер лицензии
    private String _licenceRegNumber;     // Регистрационный номер лицензии
    private Date _licenceDate;     // Дата регистрации лицензии
    private Date _licenceExpiriationDate;     // Срок действия лицензии до
    private String _licensingAgency;     // Наименование лицензирующего органа
    private String _certificateSeria;     // Серия свидетельства
    private String _certificateNumber;     // Номер свидетельства
    private String _certificateRegNumber;     // Регистрационный номер свидетельства
    private Date _certificateDate;     // Дата регистрации свидетельства
    private Date _certificateExpiriationDate;     // Срок действия свидетельства до
    private String _certificationAgency;     // Наименование уполномоченного органа исполнительной власти, выдавшего свидетельство

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Серия лицензии.
     */
    @Length(max=255)
    public String getLicenceSeria()
    {
        return _licenceSeria;
    }

    /**
     * @param licenceSeria Серия лицензии.
     */
    public void setLicenceSeria(String licenceSeria)
    {
        dirty(_licenceSeria, licenceSeria);
        _licenceSeria = licenceSeria;
    }

    /**
     * @return Номер лицензии.
     */
    @Length(max=255)
    public String getLicenceNumber()
    {
        return _licenceNumber;
    }

    /**
     * @param licenceNumber Номер лицензии.
     */
    public void setLicenceNumber(String licenceNumber)
    {
        dirty(_licenceNumber, licenceNumber);
        _licenceNumber = licenceNumber;
    }

    /**
     * @return Регистрационный номер лицензии.
     */
    @Length(max=255)
    public String getLicenceRegNumber()
    {
        return _licenceRegNumber;
    }

    /**
     * @param licenceRegNumber Регистрационный номер лицензии.
     */
    public void setLicenceRegNumber(String licenceRegNumber)
    {
        dirty(_licenceRegNumber, licenceRegNumber);
        _licenceRegNumber = licenceRegNumber;
    }

    /**
     * @return Дата регистрации лицензии.
     */
    public Date getLicenceDate()
    {
        return _licenceDate;
    }

    /**
     * @param licenceDate Дата регистрации лицензии.
     */
    public void setLicenceDate(Date licenceDate)
    {
        dirty(_licenceDate, licenceDate);
        _licenceDate = licenceDate;
    }

    /**
     * @return Срок действия лицензии до.
     */
    public Date getLicenceExpiriationDate()
    {
        return _licenceExpiriationDate;
    }

    /**
     * @param licenceExpiriationDate Срок действия лицензии до.
     */
    public void setLicenceExpiriationDate(Date licenceExpiriationDate)
    {
        dirty(_licenceExpiriationDate, licenceExpiriationDate);
        _licenceExpiriationDate = licenceExpiriationDate;
    }

    /**
     * @return Наименование лицензирующего органа.
     */
    @Length(max=255)
    public String getLicensingAgency()
    {
        return _licensingAgency;
    }

    /**
     * @param licensingAgency Наименование лицензирующего органа.
     */
    public void setLicensingAgency(String licensingAgency)
    {
        dirty(_licensingAgency, licensingAgency);
        _licensingAgency = licensingAgency;
    }

    /**
     * @return Серия свидетельства.
     */
    @Length(max=255)
    public String getCertificateSeria()
    {
        return _certificateSeria;
    }

    /**
     * @param certificateSeria Серия свидетельства.
     */
    public void setCertificateSeria(String certificateSeria)
    {
        dirty(_certificateSeria, certificateSeria);
        _certificateSeria = certificateSeria;
    }

    /**
     * @return Номер свидетельства.
     */
    @Length(max=255)
    public String getCertificateNumber()
    {
        return _certificateNumber;
    }

    /**
     * @param certificateNumber Номер свидетельства.
     */
    public void setCertificateNumber(String certificateNumber)
    {
        dirty(_certificateNumber, certificateNumber);
        _certificateNumber = certificateNumber;
    }

    /**
     * @return Регистрационный номер свидетельства.
     */
    @Length(max=255)
    public String getCertificateRegNumber()
    {
        return _certificateRegNumber;
    }

    /**
     * @param certificateRegNumber Регистрационный номер свидетельства.
     */
    public void setCertificateRegNumber(String certificateRegNumber)
    {
        dirty(_certificateRegNumber, certificateRegNumber);
        _certificateRegNumber = certificateRegNumber;
    }

    /**
     * @return Дата регистрации свидетельства.
     */
    public Date getCertificateDate()
    {
        return _certificateDate;
    }

    /**
     * @param certificateDate Дата регистрации свидетельства.
     */
    public void setCertificateDate(Date certificateDate)
    {
        dirty(_certificateDate, certificateDate);
        _certificateDate = certificateDate;
    }

    /**
     * @return Срок действия свидетельства до.
     */
    public Date getCertificateExpiriationDate()
    {
        return _certificateExpiriationDate;
    }

    /**
     * @param certificateExpiriationDate Срок действия свидетельства до.
     */
    public void setCertificateExpiriationDate(Date certificateExpiriationDate)
    {
        dirty(_certificateExpiriationDate, certificateExpiriationDate);
        _certificateExpiriationDate = certificateExpiriationDate;
    }

    /**
     * @return Наименование уполномоченного органа исполнительной власти, выдавшего свидетельство.
     */
    @Length(max=255)
    public String getCertificationAgency()
    {
        return _certificationAgency;
    }

    /**
     * @param certificationAgency Наименование уполномоченного органа исполнительной власти, выдавшего свидетельство.
     */
    public void setCertificationAgency(String certificationAgency)
    {
        dirty(_certificationAgency, certificationAgency);
        _certificationAgency = certificationAgency;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AcademyDataGen)
        {
            setLicenceSeria(((AcademyData)another).getLicenceSeria());
            setLicenceNumber(((AcademyData)another).getLicenceNumber());
            setLicenceRegNumber(((AcademyData)another).getLicenceRegNumber());
            setLicenceDate(((AcademyData)another).getLicenceDate());
            setLicenceExpiriationDate(((AcademyData)another).getLicenceExpiriationDate());
            setLicensingAgency(((AcademyData)another).getLicensingAgency());
            setCertificateSeria(((AcademyData)another).getCertificateSeria());
            setCertificateNumber(((AcademyData)another).getCertificateNumber());
            setCertificateRegNumber(((AcademyData)another).getCertificateRegNumber());
            setCertificateDate(((AcademyData)another).getCertificateDate());
            setCertificateExpiriationDate(((AcademyData)another).getCertificateExpiriationDate());
            setCertificationAgency(((AcademyData)another).getCertificationAgency());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcademyDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcademyData.class;
        }

        public T newInstance()
        {
            return (T) new AcademyData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "licenceSeria":
                    return obj.getLicenceSeria();
                case "licenceNumber":
                    return obj.getLicenceNumber();
                case "licenceRegNumber":
                    return obj.getLicenceRegNumber();
                case "licenceDate":
                    return obj.getLicenceDate();
                case "licenceExpiriationDate":
                    return obj.getLicenceExpiriationDate();
                case "licensingAgency":
                    return obj.getLicensingAgency();
                case "certificateSeria":
                    return obj.getCertificateSeria();
                case "certificateNumber":
                    return obj.getCertificateNumber();
                case "certificateRegNumber":
                    return obj.getCertificateRegNumber();
                case "certificateDate":
                    return obj.getCertificateDate();
                case "certificateExpiriationDate":
                    return obj.getCertificateExpiriationDate();
                case "certificationAgency":
                    return obj.getCertificationAgency();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "licenceSeria":
                    obj.setLicenceSeria((String) value);
                    return;
                case "licenceNumber":
                    obj.setLicenceNumber((String) value);
                    return;
                case "licenceRegNumber":
                    obj.setLicenceRegNumber((String) value);
                    return;
                case "licenceDate":
                    obj.setLicenceDate((Date) value);
                    return;
                case "licenceExpiriationDate":
                    obj.setLicenceExpiriationDate((Date) value);
                    return;
                case "licensingAgency":
                    obj.setLicensingAgency((String) value);
                    return;
                case "certificateSeria":
                    obj.setCertificateSeria((String) value);
                    return;
                case "certificateNumber":
                    obj.setCertificateNumber((String) value);
                    return;
                case "certificateRegNumber":
                    obj.setCertificateRegNumber((String) value);
                    return;
                case "certificateDate":
                    obj.setCertificateDate((Date) value);
                    return;
                case "certificateExpiriationDate":
                    obj.setCertificateExpiriationDate((Date) value);
                    return;
                case "certificationAgency":
                    obj.setCertificationAgency((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "licenceSeria":
                        return true;
                case "licenceNumber":
                        return true;
                case "licenceRegNumber":
                        return true;
                case "licenceDate":
                        return true;
                case "licenceExpiriationDate":
                        return true;
                case "licensingAgency":
                        return true;
                case "certificateSeria":
                        return true;
                case "certificateNumber":
                        return true;
                case "certificateRegNumber":
                        return true;
                case "certificateDate":
                        return true;
                case "certificateExpiriationDate":
                        return true;
                case "certificationAgency":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "licenceSeria":
                    return true;
                case "licenceNumber":
                    return true;
                case "licenceRegNumber":
                    return true;
                case "licenceDate":
                    return true;
                case "licenceExpiriationDate":
                    return true;
                case "licensingAgency":
                    return true;
                case "certificateSeria":
                    return true;
                case "certificateNumber":
                    return true;
                case "certificateRegNumber":
                    return true;
                case "certificateDate":
                    return true;
                case "certificateExpiriationDate":
                    return true;
                case "certificationAgency":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "licenceSeria":
                    return String.class;
                case "licenceNumber":
                    return String.class;
                case "licenceRegNumber":
                    return String.class;
                case "licenceDate":
                    return Date.class;
                case "licenceExpiriationDate":
                    return Date.class;
                case "licensingAgency":
                    return String.class;
                case "certificateSeria":
                    return String.class;
                case "certificateNumber":
                    return String.class;
                case "certificateRegNumber":
                    return String.class;
                case "certificateDate":
                    return Date.class;
                case "certificateExpiriationDate":
                    return Date.class;
                case "certificationAgency":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcademyData> _dslPath = new Path<AcademyData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcademyData");
    }
            

    /**
     * @return Серия лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceSeria()
     */
    public static PropertyPath<String> licenceSeria()
    {
        return _dslPath.licenceSeria();
    }

    /**
     * @return Номер лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceNumber()
     */
    public static PropertyPath<String> licenceNumber()
    {
        return _dslPath.licenceNumber();
    }

    /**
     * @return Регистрационный номер лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceRegNumber()
     */
    public static PropertyPath<String> licenceRegNumber()
    {
        return _dslPath.licenceRegNumber();
    }

    /**
     * @return Дата регистрации лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceDate()
     */
    public static PropertyPath<Date> licenceDate()
    {
        return _dslPath.licenceDate();
    }

    /**
     * @return Срок действия лицензии до.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceExpiriationDate()
     */
    public static PropertyPath<Date> licenceExpiriationDate()
    {
        return _dslPath.licenceExpiriationDate();
    }

    /**
     * @return Наименование лицензирующего органа.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicensingAgency()
     */
    public static PropertyPath<String> licensingAgency()
    {
        return _dslPath.licensingAgency();
    }

    /**
     * @return Серия свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateSeria()
     */
    public static PropertyPath<String> certificateSeria()
    {
        return _dslPath.certificateSeria();
    }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateNumber()
     */
    public static PropertyPath<String> certificateNumber()
    {
        return _dslPath.certificateNumber();
    }

    /**
     * @return Регистрационный номер свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateRegNumber()
     */
    public static PropertyPath<String> certificateRegNumber()
    {
        return _dslPath.certificateRegNumber();
    }

    /**
     * @return Дата регистрации свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateDate()
     */
    public static PropertyPath<Date> certificateDate()
    {
        return _dslPath.certificateDate();
    }

    /**
     * @return Срок действия свидетельства до.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateExpiriationDate()
     */
    public static PropertyPath<Date> certificateExpiriationDate()
    {
        return _dslPath.certificateExpiriationDate();
    }

    /**
     * @return Наименование уполномоченного органа исполнительной власти, выдавшего свидетельство.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificationAgency()
     */
    public static PropertyPath<String> certificationAgency()
    {
        return _dslPath.certificationAgency();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateTitle()
     */
    public static SupportedPropertyPath<String> certificateTitle()
    {
        return _dslPath.certificateTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceTitle()
     */
    public static SupportedPropertyPath<String> licenceTitle()
    {
        return _dslPath.licenceTitle();
    }

    public static class Path<E extends AcademyData> extends EntityPath<E>
    {
        private PropertyPath<String> _licenceSeria;
        private PropertyPath<String> _licenceNumber;
        private PropertyPath<String> _licenceRegNumber;
        private PropertyPath<Date> _licenceDate;
        private PropertyPath<Date> _licenceExpiriationDate;
        private PropertyPath<String> _licensingAgency;
        private PropertyPath<String> _certificateSeria;
        private PropertyPath<String> _certificateNumber;
        private PropertyPath<String> _certificateRegNumber;
        private PropertyPath<Date> _certificateDate;
        private PropertyPath<Date> _certificateExpiriationDate;
        private PropertyPath<String> _certificationAgency;
        private SupportedPropertyPath<String> _certificateTitle;
        private SupportedPropertyPath<String> _licenceTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Серия лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceSeria()
     */
        public PropertyPath<String> licenceSeria()
        {
            if(_licenceSeria == null )
                _licenceSeria = new PropertyPath<String>(AcademyDataGen.P_LICENCE_SERIA, this);
            return _licenceSeria;
        }

    /**
     * @return Номер лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceNumber()
     */
        public PropertyPath<String> licenceNumber()
        {
            if(_licenceNumber == null )
                _licenceNumber = new PropertyPath<String>(AcademyDataGen.P_LICENCE_NUMBER, this);
            return _licenceNumber;
        }

    /**
     * @return Регистрационный номер лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceRegNumber()
     */
        public PropertyPath<String> licenceRegNumber()
        {
            if(_licenceRegNumber == null )
                _licenceRegNumber = new PropertyPath<String>(AcademyDataGen.P_LICENCE_REG_NUMBER, this);
            return _licenceRegNumber;
        }

    /**
     * @return Дата регистрации лицензии.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceDate()
     */
        public PropertyPath<Date> licenceDate()
        {
            if(_licenceDate == null )
                _licenceDate = new PropertyPath<Date>(AcademyDataGen.P_LICENCE_DATE, this);
            return _licenceDate;
        }

    /**
     * @return Срок действия лицензии до.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceExpiriationDate()
     */
        public PropertyPath<Date> licenceExpiriationDate()
        {
            if(_licenceExpiriationDate == null )
                _licenceExpiriationDate = new PropertyPath<Date>(AcademyDataGen.P_LICENCE_EXPIRIATION_DATE, this);
            return _licenceExpiriationDate;
        }

    /**
     * @return Наименование лицензирующего органа.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicensingAgency()
     */
        public PropertyPath<String> licensingAgency()
        {
            if(_licensingAgency == null )
                _licensingAgency = new PropertyPath<String>(AcademyDataGen.P_LICENSING_AGENCY, this);
            return _licensingAgency;
        }

    /**
     * @return Серия свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateSeria()
     */
        public PropertyPath<String> certificateSeria()
        {
            if(_certificateSeria == null )
                _certificateSeria = new PropertyPath<String>(AcademyDataGen.P_CERTIFICATE_SERIA, this);
            return _certificateSeria;
        }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateNumber()
     */
        public PropertyPath<String> certificateNumber()
        {
            if(_certificateNumber == null )
                _certificateNumber = new PropertyPath<String>(AcademyDataGen.P_CERTIFICATE_NUMBER, this);
            return _certificateNumber;
        }

    /**
     * @return Регистрационный номер свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateRegNumber()
     */
        public PropertyPath<String> certificateRegNumber()
        {
            if(_certificateRegNumber == null )
                _certificateRegNumber = new PropertyPath<String>(AcademyDataGen.P_CERTIFICATE_REG_NUMBER, this);
            return _certificateRegNumber;
        }

    /**
     * @return Дата регистрации свидетельства.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateDate()
     */
        public PropertyPath<Date> certificateDate()
        {
            if(_certificateDate == null )
                _certificateDate = new PropertyPath<Date>(AcademyDataGen.P_CERTIFICATE_DATE, this);
            return _certificateDate;
        }

    /**
     * @return Срок действия свидетельства до.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateExpiriationDate()
     */
        public PropertyPath<Date> certificateExpiriationDate()
        {
            if(_certificateExpiriationDate == null )
                _certificateExpiriationDate = new PropertyPath<Date>(AcademyDataGen.P_CERTIFICATE_EXPIRIATION_DATE, this);
            return _certificateExpiriationDate;
        }

    /**
     * @return Наименование уполномоченного органа исполнительной власти, выдавшего свидетельство.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificationAgency()
     */
        public PropertyPath<String> certificationAgency()
        {
            if(_certificationAgency == null )
                _certificationAgency = new PropertyPath<String>(AcademyDataGen.P_CERTIFICATION_AGENCY, this);
            return _certificationAgency;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getCertificateTitle()
     */
        public SupportedPropertyPath<String> certificateTitle()
        {
            if(_certificateTitle == null )
                _certificateTitle = new SupportedPropertyPath<String>(AcademyDataGen.P_CERTIFICATE_TITLE, this);
            return _certificateTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyData#getLicenceTitle()
     */
        public SupportedPropertyPath<String> licenceTitle()
        {
            if(_licenceTitle == null )
                _licenceTitle = new SupportedPropertyPath<String>(AcademyDataGen.P_LICENCE_TITLE, this);
            return _licenceTitle;
        }

        public Class getEntityClass()
        {
            return AcademyData.class;
        }

        public String getEntityName()
        {
            return "academyData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getCertificateTitle();

    public abstract String getLicenceTitle();
}
