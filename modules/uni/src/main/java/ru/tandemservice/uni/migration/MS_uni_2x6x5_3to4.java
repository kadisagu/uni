package ru.tandemservice.uni.migration;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x5_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность developPeriod

		// создано свойство userCode
		{
			// создать колонку
			tool.createColumn("developperiod_t", new DBColumn("usercode_p", DBType.createVarchar(255)));
		}

		// создано свойство disabledDate
		{
			// создать колонку
			tool.createColumn("developperiod_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
		}

		//  свойство title стало обязательным
		{
			// сделать колонку NOT NULL
			tool.setColumnNullable("developperiod_t", "title_p", false);
		}

        {
            // Исправляем коды справочника "Продолжительность обучения" на новый формат     {Число лет}.{Число месяцев}
            Statement statement = tool.getConnection().createStatement();
            statement.execute("select numberofyears_p, numberofmonths_p from edu_c_rp_duration_t");
            ResultSet resultSet = statement.getResultSet();

            PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update edu_c_rp_duration_t set code_p = ? where numberofyears_p = ? and numberofmonths_p = ?");
            while (resultSet.next())
            {
                preparedStatement.setString(1, String.valueOf(resultSet.getInt(1)) + "." + String.valueOf(resultSet.getInt(2)));
                preparedStatement.setInt(2, resultSet.getInt(1));
                preparedStatement.setInt(3, resultSet.getInt(2));

                preparedStatement.execute();
            }
        }

        // выбираем из существующих продолжительностей те, которые нам подходят (по числу лет и месяцев), обновляем код
        Map<Integer, Map<Integer, String>> durationMap = SafeMap.get(HashMap.class);
        durationMap.putAll(new ImmutableMap.Builder<Integer, Map<Integer, String>>()
            .put(1, new HashMap<Integer, String>(of(0, "1.0")))
            .put(2, new HashMap<Integer, String>(of(0, "2.0", 6, "2.6")))
            .put(3, new HashMap<Integer, String>(of(0, "3.0", 6, "3.6")))
            .put(4, new HashMap<Integer, String>(of(0, "4.0", 6, "4.6")))
            .put(5, new HashMap<Integer, String>(of(0, "5.0", 6, "5.6")))
            .put(6, new HashMap<Integer, String>(of(0, "6.0")))
            .build());

        Statement statement = tool.getConnection().createStatement();
        statement.execute("select code_p, numberofyears_p, numberofmonths_p from edu_c_rp_duration_t");
        ResultSet resultSet = statement.getResultSet();
        while (resultSet.next())
        {
            String code = resultSet.getString(1);
            Integer numberOfYears = resultSet.getInt(2);
            Integer numberOfMonths = resultSet.getInt(3);

            String s = durationMap.get(numberOfYears).remove(numberOfMonths);
            if (s != null && !s.equals(code))
                tool.executeUpdate("update edu_c_rp_duration_t set code_p = ? where code_p = ?", s, code);
        }

        Statement titleStatement = tool.getConnection().createStatement();
        titleStatement.execute("select title_p from edu_c_rp_duration_t");
        ResultSet titleResutSet = titleStatement.getResultSet();
        Collection<String> titles = new HashSet<>();
        while (titleResutSet.next())
            titles.add(titleResutSet.getString(1));

        short programDurationEntityCode = tool.entityCodes().ensure("eduProgramDuration");
        PreparedStatement insertProgramDurationPreparedStatement = tool.getConnection().prepareStatement("insert into edu_c_rp_duration_t (id, discriminator, code_p, numberofyears_p, numberofmonths_p, title_p) values (?, ?, ?, ?, ?, ?)");
        insertProgramDurationPreparedStatement.setShort(2, programDurationEntityCode);

        for (Map.Entry<Integer, Map<Integer, String>> numberOfYearsEntry: durationMap.entrySet())
        {
            Integer numberOfYears = numberOfYearsEntry.getKey();
            for (Map.Entry<Integer, String> numberOfMonthsEntry: numberOfYearsEntry.getValue().entrySet())
            {
                insertProgramDurationPreparedStatement.setLong(1, EntityIDGenerator.generateNewId(programDurationEntityCode));
                insertProgramDurationPreparedStatement.setString(3, numberOfMonthsEntry.getValue());
                insertProgramDurationPreparedStatement.setInt(4, numberOfYears);
                insertProgramDurationPreparedStatement.setInt(5, numberOfMonthsEntry.getKey());

                String durationTitle = CommonBaseDateUtil.getDatePeriodWithNames(numberOfYears, numberOfMonthsEntry.getKey(), null);
                if (titles.contains(durationTitle))
                    durationTitle = durationTitle + " (нов)";
                insertProgramDurationPreparedStatement.setString(6, durationTitle);

                insertProgramDurationPreparedStatement.execute();
            }
        }

        // проставляем связь между develop period и program duration
        Map<String, String> period2durationCodeMap = new HashMap<>(10);
        period2durationCodeMap.put("0", "1.0");
        period2durationCodeMap.put("1", "2.0");
        period2durationCodeMap.put("10", "2.6");
        period2durationCodeMap.put("2", "3.0");
        period2durationCodeMap.put("7", "3.6");
        period2durationCodeMap.put("3", "4.0");
        period2durationCodeMap.put("8", "4.6");
        period2durationCodeMap.put("4", "5.0");
        period2durationCodeMap.put("5", "5.6");
        period2durationCodeMap.put("6", "6.0");

        PreparedStatement updateDevelopPeriodPS = tool.getConnection().prepareStatement("update developperiod_t set eduprogramduration_id = (select id from edu_c_rp_duration_t where code_p = ?) where code_p = ?");
        for (Map.Entry<String, String> periodEntry: period2durationCodeMap.entrySet())
        {
            updateDevelopPeriodPS.setString(1, periodEntry.getValue());
            updateDevelopPeriodPS.setString(2, periodEntry.getKey());
            updateDevelopPeriodPS.execute();
        }
    }
}