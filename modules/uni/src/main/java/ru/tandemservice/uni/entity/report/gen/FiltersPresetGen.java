package ru.tandemservice.uni.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.FiltersPreset;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблон фильтров
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FiltersPresetGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.report.FiltersPreset";
    public static final String ENTITY_NAME = "filtersPreset";
    public static final int VERSION_HASH = 1920068310;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_SETTINGS_KEY_PREFIX = "settingsKeyPrefix";
    public static final String P_SETTINGS_NUMBER = "settingsNumber";

    private String _title;     // Название
    private String _settingsKeyPrefix;     // Префикс фильтров
    private int _settingsNumber;     // Номер шаблона

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Префикс фильтров. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSettingsKeyPrefix()
    {
        return _settingsKeyPrefix;
    }

    /**
     * @param settingsKeyPrefix Префикс фильтров. Свойство не может быть null.
     */
    public void setSettingsKeyPrefix(String settingsKeyPrefix)
    {
        dirty(_settingsKeyPrefix, settingsKeyPrefix);
        _settingsKeyPrefix = settingsKeyPrefix;
    }

    /**
     * @return Номер шаблона. Свойство не может быть null.
     */
    @NotNull
    public int getSettingsNumber()
    {
        return _settingsNumber;
    }

    /**
     * @param settingsNumber Номер шаблона. Свойство не может быть null.
     */
    public void setSettingsNumber(int settingsNumber)
    {
        dirty(_settingsNumber, settingsNumber);
        _settingsNumber = settingsNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FiltersPresetGen)
        {
            setTitle(((FiltersPreset)another).getTitle());
            setSettingsKeyPrefix(((FiltersPreset)another).getSettingsKeyPrefix());
            setSettingsNumber(((FiltersPreset)another).getSettingsNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FiltersPresetGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FiltersPreset.class;
        }

        public T newInstance()
        {
            return (T) new FiltersPreset();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "settingsKeyPrefix":
                    return obj.getSettingsKeyPrefix();
                case "settingsNumber":
                    return obj.getSettingsNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "settingsKeyPrefix":
                    obj.setSettingsKeyPrefix((String) value);
                    return;
                case "settingsNumber":
                    obj.setSettingsNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "settingsKeyPrefix":
                        return true;
                case "settingsNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "settingsKeyPrefix":
                    return true;
                case "settingsNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "settingsKeyPrefix":
                    return String.class;
                case "settingsNumber":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FiltersPreset> _dslPath = new Path<FiltersPreset>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FiltersPreset");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.report.FiltersPreset#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Префикс фильтров. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.report.FiltersPreset#getSettingsKeyPrefix()
     */
    public static PropertyPath<String> settingsKeyPrefix()
    {
        return _dslPath.settingsKeyPrefix();
    }

    /**
     * @return Номер шаблона. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.report.FiltersPreset#getSettingsNumber()
     */
    public static PropertyPath<Integer> settingsNumber()
    {
        return _dslPath.settingsNumber();
    }

    public static class Path<E extends FiltersPreset> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<String> _settingsKeyPrefix;
        private PropertyPath<Integer> _settingsNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.report.FiltersPreset#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FiltersPresetGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Префикс фильтров. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.report.FiltersPreset#getSettingsKeyPrefix()
     */
        public PropertyPath<String> settingsKeyPrefix()
        {
            if(_settingsKeyPrefix == null )
                _settingsKeyPrefix = new PropertyPath<String>(FiltersPresetGen.P_SETTINGS_KEY_PREFIX, this);
            return _settingsKeyPrefix;
        }

    /**
     * @return Номер шаблона. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.report.FiltersPreset#getSettingsNumber()
     */
        public PropertyPath<Integer> settingsNumber()
        {
            if(_settingsNumber == null )
                _settingsNumber = new PropertyPath<Integer>(FiltersPresetGen.P_SETTINGS_NUMBER, this);
            return _settingsNumber;
        }

        public Class getEntityClass()
        {
            return FiltersPreset.class;
        }

        public String getEntityName()
        {
            return "filtersPreset";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
