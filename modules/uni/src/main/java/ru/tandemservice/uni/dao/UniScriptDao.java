/* $Id$ */
package ru.tandemservice.uni.dao;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import groovy.lang.GroovyCodeSource;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.control.CompilationFailedException;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.IScriptOwner;
import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.uni.util.groovy.UniScriptLoader;

import java.io.InputStream;

/**
 * @author Nikolay Fedorovskih
 * @since 16.07.2015
 */
public class UniScriptDao extends UniBaseDao implements IUniScriptDao
{
    @Override
    public void doSaveUserScript(IEntity scriptOwner)
    {
        try {
            this.save(scriptOwner);
        }
        finally {
            UniScriptLoader.reset(); // сбрасываем loader
        }
    }


    public static String findScriptPath(String pathWithoutModuleName)
    {
        final ClassLoader classLoader = UniScriptDao.class.getClassLoader();
        return CommonBaseUtil.findPath(classLoader, pathWithoutModuleName);
    }

    public static String getScriptText(String path) {
        try {
            final ClassLoader classLoader = UniScriptDao.class.getClassLoader();
            final InputStream stream = classLoader.getResourceAsStream(path);
            return IOUtils.toString(stream);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public <S extends IScriptInstance, O extends IScriptOwner> IScriptWrapper<S, O> buildScriptWrapper(final O scriptOwner, final Class<S> klass, String configKey, final String defaultScriptPath, String scriptTitle)
    {
        final String configScript = (null == scriptOwner ? "" : StringUtils.trimToEmpty(scriptOwner.getUserScript()));

        // формируем исходные данные (название скрипла - запись в кэше)
        final GroovyCodeSource source = new GroovyCodeSource(configScript, configKey, "/groovy/script") {
            @Override public String getScriptText()
            {
                // грузим сначала из объекта (если он есть)
                final String userScript = StringUtils.trimToNull(super.getScriptText());
                if (null != userScript) { return userScript; }

                // если там нет - то из файловой системы
                return UniScriptDao.getScriptText(scriptOwner != null ? scriptOwner.getScriptPath() : defaultScriptPath);
            }
        };

        // грузим (скрипт возвращает экземпляр класса-обработчика)
        IScriptInstance instance;
        try {
            instance = UniScriptLoader.current().getScriptInstance(source);
        }
        catch (CompilationFailedException e) {
            CoreExceptionUtils.logException("Script compilation failed, settings id=" + (scriptOwner == null ? "null" : scriptOwner.getId()) + " .", e, null);
            throw new ApplicationException("Произошла ошибка компиляции скрипта, пожалуйста, исправьте текст. Информация об ошибке доступна в логе.", e);
        }

        if (klass.isInstance(instance)) {
            return this.wrap(scriptOwner, (S) instance, scriptTitle);
        }

        // классы не совпали
        final String interfaces = StringUtils.join(
                CollectionUtils.collect(
                        ClassUtils.getAllInterfaces(instance.getClass()),
                        new Transformer<Class, String>()
                        {
                            @Override
                            public String transform(final Class input)
                            {
                                return input.getName();
                            }
                        }
                ),
                ", "
        );

        throw new ClassCastException(
                "script instance «" + source.getName() + "» (class=" + instance.getClass().getName() + ", interfaces=" + interfaces + ", settings id=" + (scriptOwner == null ? "null" : scriptOwner.getId()) + ") is not instance of " + klass.getSimpleName()
        );
    }

    protected  <S extends IScriptInstance, O extends IScriptOwner> IScriptWrapper<S, O> wrap(final O owner, final S script, final String title)
    {
        return new IScriptWrapper<S, O>() {

            @Override public O getScriptOwner() { return owner; }
            @Override public S getScript() { return script; }
            @Override public String toString() { return title; }

            @Override public int hashCode() {
                final IScriptOwner scriptOwner = this.getScriptOwner();
                return (null == scriptOwner ? 0 : scriptOwner.hashCode());
            }

            @Override public boolean equals(final Object obj) {
                if (obj == this) { return true; }
                if (! (obj instanceof IScriptWrapper)) { return false;}

                final IScriptOwner owner1 = getScriptOwner();
                final IScriptOwner owner2 = ((IScriptWrapper) obj).getScriptOwner();
                return CoreUtils.safeEqual(owner1, owner2);
            }
        };
    }

    @Override
    public <S extends IScriptInstance> S buildScriptItemInstance(IScriptItem scriptItem, Class<S> klass)
    {
        final String hash = scriptItem.getUserScript() != null ? Hashing.crc32().newHasher().putString(scriptItem.getUserScript(), Charsets.UTF_8).hash().toString() : "null";
        final String configKey = scriptItem.getClass().getSimpleName() + "_" + scriptItem.getCode() + "_" + hash;
        final IScriptWrapper<S, IScriptItem> scriptWrapper = buildScriptWrapper(scriptItem, klass, configKey, scriptItem.getScriptPath(), "script");

        return scriptWrapper.getScript();
    }
}