/* $Id$ */
package ru.tandemservice.uni.util;

import ru.tandemservice.uni.entity.catalog.EducationLevels;

/**
 * @author Nikolay Fedorovskih
 * @since 25.10.2013
 */
public interface IExtEducationLevelModel
{
    EducationLevels getParentEduLevel();
}