package ru.tandemservice.uni.util;

import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 * TODO: использовать вместо copy-and-paste в DAO
 */
@SuppressWarnings("unchecked")
public class FilterUtils extends CommonBaseFilterUtil
{
    public static IDQLExpression getEducationOrgUnitFilter(String eduOuAlias, OrgUnit ou)
    {
        Collection<String> kindSet = UniDaoFacade.getOrgstructDao().getAllowStudentsOrgUnitKindCodes(ou);

        IDQLExpression expression = null;

        //Для выпускающих подразделений – студенты, для которых текущее подразделение является выпускающим через связь с НПв.
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
            expression = eq(
                    property(EducationOrgUnit.educationLevelHighSchool().orgUnit().fromAlias(eduOuAlias)),
                    value(ou)
            );

        //Для формирующих подразделений будет отображаться перечень студентов, для которых текущее подразделение является формирующим через связь с направлением подготовки (специальностью) подразделения.
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
            expression = or(expression, eq(
                    property(EducationOrgUnit.formativeOrgUnit().fromAlias(eduOuAlias)),
                    value(ou)
            ));

        //Для территориальных подразделений – студенты, для которых текущее подразделение является территориальным подразделением через связь с направлением подготовки (специальностью) подразделения.
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
            expression = or(expression, eq(
                    property(EducationOrgUnit.territorialOrgUnit().fromAlias(eduOuAlias)),
                    value(ou)
            ));

        return expression;
    }
}
