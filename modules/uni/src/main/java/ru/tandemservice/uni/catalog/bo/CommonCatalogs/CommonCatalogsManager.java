/* $Id$ */
package ru.tandemservice.uni.catalog.bo.CommonCatalogs;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 2/7/12
 */
@Configuration
public class CommonCatalogsManager extends BusinessObjectManager
{
    public static CommonCatalogsManager instance()
    {
        return instance(CommonCatalogsManager.class);
    }

}
