/* $Id: DAO.java 5483 2008-11-28 12:06:00Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;
import java.util.Map;

/**
 * @author AutoGenerator
 * Created on 16.06.2008
 */
public class DAO extends DefaultCatalogPubDAO<DevelopGrid, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setDevelopPeriodListModel(EducationCatalogsManager.getDevelopPeriodSelectModel());
        model.setTermListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getTermMap().values()));
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void applyFilters(Model model, MQBuilder builder)
    {
        String title = model.getSettings().get("title");
        if (StringUtils.isNotEmpty(title))
            builder.add(MQExpression.like("ci", ICatalogItem.CATALOG_ITEM_TITLE, CoreStringUtils.escapeLike(title)));

        List<Term> terms = model.getSettings().get("term");
        List<Integer> nums = CommonBaseEntityUtil.getPropertiesList(terms, Term.intValue().s());
        if (nums.size() > 0)
        {
            String havingStr = "";
            for (Integer num : nums)
                havingStr += "count(term.id) = " + num + " or ";
            havingStr = havingStr.substring(0, havingStr.length() - 4);

            DQLSelectBuilder termDQL = new DQLSelectBuilder()
            .fromEntity(DevelopGridTerm.class, "term")
            .joinPath(DQLJoinType.inner, DevelopGridTerm.developGrid().fromAlias("term"), "grid")
            .column(DQLExpressions.property("grid.id"))
            .group(DQLExpressions.property("grid.id"))
            .having(havingStr);
            builder.add(MQExpression.in("ci", "id", termDQL.createStatement(getSession()).list()));
        }
        FilterUtils.applySelectFilter(builder, "ci", "developPeriod", model.getSettings().get("developPeriod"));
    }

    @Override
    protected void prepareListItemDataSource(Model model)
    {
        super.prepareListItemDataSource(model);
        List<ViewWrapper<IEntity>> wrapperList = ViewWrapper.getPatchedList(model.getDataSource());
        Map<Long, Map<Integer, DevelopGridTerm>> map = IDevelopGridDAO.instance.get().getDevelopGridDistributionMap(ids(wrapperList));
        for (ViewWrapper<IEntity> wrapper : wrapperList)
        {
            wrapper.setViewProperty("terms", map.get(wrapper.getId()));

            String code = ((ICatalogItem) get(wrapper.getId())).getCode();
            final ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy("developGrid").getItemPolicy(code);
            wrapper.setViewProperty("deleteDisabled", itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize()));

        }
    }
}
