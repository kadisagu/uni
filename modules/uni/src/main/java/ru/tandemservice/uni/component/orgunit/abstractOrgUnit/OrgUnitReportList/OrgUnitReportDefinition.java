/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;

/**
 * @author vip_delete
 * @since 26.01.2009
 */
@Deprecated
public class OrgUnitReportDefinition extends IdentifiableWrapper implements ITitled
{
    private static final long serialVersionUID = -2238764244955780114L;

    public static final String P_TITLE = "title";

    private IPublisherLinkResolver _linkResolver;

    public OrgUnitReportDefinition(Long id, IOrgUnitReportDefinition definition)
    {
        super(id, definition.getReportTitle());
        _linkResolver = definition.getLinkResolver();
    }

    public IPublisherLinkResolver getLinkResolver()
    {
        return _linkResolver;
    }
}
