/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public abstract class AbstractRelationPubController<IDAO extends IAbstractRelationPubDAO<Model>, Model extends AbstractRelationPubModel> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    protected void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", new String[]{IEntityRelation.L_SECOND, "title"}).setClickable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteRelation", "Удалить «{0}»?", new Object[]{new String[]{IEntityRelation.L_SECOND, "title"}}).setPermissionKey(getDeleteRowPermissionKey()));

        model.setDataSource(dataSource);
    }

    protected abstract String getRelationAddingComponentName();

    protected abstract String getDeleteRowPermissionKey();

    public void onClickAddSecond(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(getRelationAddingComponentName(), new ParametersMap()
                .add("firstId", getModel(component).getFirst().getId())
        ));
    }

    public void onClickDeleteRelation(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
