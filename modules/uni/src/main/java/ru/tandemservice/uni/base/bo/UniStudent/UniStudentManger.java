/**
 * $Id:$
 */
package ru.tandemservice.uni.base.bo.UniStudent;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.IndividualEduPlan.IIndividualEduPlanDao;
import ru.tandemservice.uni.base.bo.UniStudent.logic.IndividualEduPlan.IndividualEduPlanDao;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.customState.IStudentCustomStateDAO;
import ru.tandemservice.uni.base.bo.UniStudent.logic.customState.StudentCustomStateDAO;
import ru.tandemservice.uni.base.bo.UniStudent.logic.list.AddressCountryComboDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList.IOrgUnitStudentListPersonalCardPrintDao;
import ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList.OrgUnitStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList.OrgUnitStudentListPersonalCardPrintDao;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolDao;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.SessionTransferProtocolDao;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.11.12
 */
@Configuration
public class UniStudentManger extends BusinessObjectManager
{
    public static final String STUDENT_STATUS_OPTION_DS = "studentStatusOptionDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";
    public static final String COURSE_DS = "courseDS";
    public static final String GROUP_DS = "groupDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String ENDING_YEAR_DS = "endingYearDS";
    public static final String STUDENT_CUSTOM_STATE_CI_DS = "studentCustomStateCIDS";
    public static final String CITIZENSHIP_DS = "citizenshipDS";

    public static UniStudentManger instance()
    {
        return instance(UniStudentManger.class);
    }

    // studentStatusOptionDS
    @Bean
    public UIDataSourceConfig studentStatusOptionDSConfig()
    {
        return SelectDSConfig.with(STUDENT_STATUS_OPTION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(studentStatusOptionDSHandler())
                .addColumn("title")
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentStatusOptionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(IStudentListModel.STUDENT_STATUS_OPTION_LIST);
    }

    // studentStatusDS
    @Bean
    public UIDataSourceConfig studentStatusDSConfig()
    {
        return SelectDSConfig.with(STUDENT_STATUS_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(StudentCatalogsManager.instance().studentStatusDSHandler())
                .addColumn(StudentStatus.title().s())
                .create();
    }

    // courseDS
    @Bean
    public UIDataSourceConfig courseDSConfig()
    {
        return CommonBaseStaticSelectDataSource.selectDS(COURSE_DS, getName(), Course.defaultSelectDSHandler(getName())).create();
    }

    // groupDS
    @Bean
    public UIDataSourceConfig groupDSConfig()
    {
        return SelectDSConfig.with(GROUP_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(groupDS())
                .addColumn("title")
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDS()
    {
        return new SimpleTitledComboDataSourceWithPopupSizeHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final List<Course> courseList = context.get(AbstractUniStudentListUI.PROP_COURSE_LIST);
                final OrgUnit orgUnit = context.get("orgUnit");
                final Boolean archival = context.get("archival");

                DQLSelectBuilder groupBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .where(exists(Student.class, Student.group().s(), property("g")));
                if (archival != null) {
                    groupBuilder.where(exists(Student.class,
                            Student.archival().s(), value(archival),
                            Student.group().s(), property("g")));
                }
                if (CollectionUtils.isNotEmpty(courseList)) {
                    groupBuilder.where(in(property("g", Group.course()), courseList));
                }
                if (orgUnit != null) {
                    groupBuilder.where(or(
                            eq(property("g", Group.educationOrgUnit().territorialOrgUnit().id()), value(orgUnit.getId())),
                            eq(property("g", Group.educationOrgUnit().formativeOrgUnit().id()), value(orgUnit.getId()))
                    ));
                }
                groupBuilder.column(property("g", Group.title())).order(property("g", Group.title())).distinct();

                // Поиск групп происходит по названию, чтобы учитывать студентов из разных групп с одинаковым
                // названием. Для корректного восстановления выбранного значения фильтра из настроек,
                // используется враппер c хешем тайтла в качестве id.
                // Шанс коллизии хешей учтен и проигнорирован, так как не является значительным.
                List<DataWrapper> groupWrappers = groupBuilder.createStatement(getSession()).<String>list().stream()
                        .map(groupTitle -> new DataWrapper(Long.valueOf(groupTitle.hashCode()), groupTitle))
                        .collect(Collectors.toList());

                context.put(UIDefines.COMBO_OBJECT_LIST, groupWrappers);
                return super.execute(input, context);
            }

        }.setFilterByProperty("title")
                .filtered(true);
    }

    // compensationTypeDS
    @Bean
    public UIDataSourceConfig compensationTypeDSConfig()
    {
        return StudentCatalogsManager.instance().compensationTypeDSConfig();
    }

    // qualificationDS
    @Bean
    public UIDataSourceConfig qualificationDSConfig()
    {
        return SelectDSConfig.with(QUALIFICATION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(qualificationDSHandler())
                .addColumn(Qualifications.title().s())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> qualificationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Qualifications.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                IDQLSelectableQuery ex = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "ex").column(property(EducationLevelsHighSchool.id().fromAlias("ex")))
                        .where(eq(property(Qualifications.id().fromAlias(alias)), property(EducationLevelsHighSchool.educationLevel().qualification().id().fromAlias("ex"))))
                        .buildQuery();

                dql.where(exists(ex));
            }
        }
                .filter(Qualifications.title())
                .order(Qualifications.order());
    }

    // studentCategoryDS
    @Bean
    public UIDataSourceConfig studentCategoryDSConfig()
    {
        return SelectDSConfig.with(STUDENT_CATEGORY_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(StudentCatalogsManager.instance().studentCategoryDSHandler())
                .addColumn(StudentCategory.title().s())
                .create();
    }

    // endingYearDS
    @Bean
    public UIDataSourceConfig endingYearDSConfig()
    {
        return SelectDSConfig.with(ENDING_YEAR_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(endingYearDSHandler())
                .addColumn("title")
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> endingYearDSHandler()
    {
        int currentYear = UniBaseUtils.getCurrentYear();
        List<IdentifiableWrapper> endingYears = new ArrayList<>();
        for (int i = currentYear - 9; i <= currentYear; i++)
        {
            endingYears.add(new IdentifiableWrapper((long) i, Integer.toString(i)));
        }

        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(endingYears);
    }

    @Bean
    public UIDataSourceConfig studentCustomStateCIDSConfig()
    {
        return CommonBaseStaticSelectDataSource.selectDS(STUDENT_CUSTOM_STATE_CI_DS, getName(), StudentCustomStateCI.defaultSelectDSHandler(getName())).create();
    }

    @Bean
    public UIDataSourceConfig countryDSConfig()
    {
        return SelectDSConfig.with(CITIZENSHIP_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(countryComboDSHandler())
                .addColumn("title")
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler countryComboDSHandler()
    {
        return new AddressCountryComboDSHandler(getName()).filtered(true);
    }

    @Bean
    public IStudentCustomStateDAO studentCustomStateDAO()
    {
        return new StudentCustomStateDAO();
    }

    @Bean
    public IOrgUnitStudentListPersonalCardPrintDao studentListPersonalCardPrintDao()
    {
        return new OrgUnitStudentListPersonalCardPrintDao();
    }

    @Bean
    public IIndividualEduPlanDao individualEduPlanDao()
    {
        return new IndividualEduPlanDao();
    }

    @Bean
    public ISessionTransferProtocolDao sessionTransferProtocolDao()
    {
        return new SessionTransferProtocolDao();
    }
}
