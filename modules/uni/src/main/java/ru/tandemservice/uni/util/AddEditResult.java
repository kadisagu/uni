package ru.tandemservice.uni.util;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;

import java.util.Collections;
import java.util.Map;


/**
 * @author vdanilov
 */
public class AddEditResult implements IIdentifiable {

    public static final String PARAM = "addEditResult";

    private final boolean newObject;
    public boolean isNewObject() { return this.newObject; }

    private final Long id;
    @Override public Long getId() { return id; }

    public AddEditResult(boolean newObject, Long id) {
        this.newObject = newObject;
        this.id = id;
    }

    public static boolean processReturned(Map<String, Object> returnedData)
    {
        // если с формы пришла инфа, что создался объект - переходим на его карточку
        Object val = returnedData.get(AddEditResult.PARAM);
        if (val instanceof AddEditResult) {
            AddEditResult res = (AddEditResult)val;
            if (res.isNewObject()) {
                Long id = res.getId();
                Class entityClass = EntityRuntime.getMeta(id).getEntityClass();
                String publisher = BusinessComponentRuntime.getInstance().getPublisher(entityClass);
                ContextLocal.createDesktop(new ComponentActivator(publisher, Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, id)), null);
                return true;
            }
        }

        return false;
    }


    public static void saveAndDeactivate(IBusinessComponent component, IEntity element, Runnable saveAction)
    {
        boolean newObject = (null == element.getId());
        saveAction.run();
        if (UserContext.getInstance().getErrorCollector().hasErrors()) { return; }
        if (component.getParentRegion().getActiveComponent() != component) { return; }

        component.getParentRegion().deactivateComponent(true, new ParametersMap().add(PARAM, new AddEditResult(newObject, element.getId())));
    }

}
