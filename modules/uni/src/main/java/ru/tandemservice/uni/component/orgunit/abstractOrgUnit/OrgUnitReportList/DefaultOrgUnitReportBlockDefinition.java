/* $Id$ */
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.util.*;

/**
 * @author oleyba
 * @deprecated
 * @see org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition
 */

@Deprecated
public class DefaultOrgUnitReportBlockDefinition implements IOrgUnitReportBlockDefinition
{
    private String _blockTitle;
    private String _permissionPrefix;
    private IOrgUnitReportVisibleResolver _visibleResolver;

    @Override
    public boolean isVisible(OrgUnit orgUnit)
    {
        return _visibleResolver.isVisible(orgUnit);
    }

    @SuppressWarnings("unchecked")
    public static List<OrgUnitReportBlock> prepareBlockList(OrgUnit orgUnit, CommonPostfixPermissionModel secModel, IPrincipalContext context)
    {
        if (!ApplicationRuntime.containsBean(IOrgUnitReportDefinition.LIST_BEAN_NAME))
            return Collections.EMPTY_LIST;

        Map<IOrgUnitReportBlockDefinition, OrgUnitReportBlock> blockMap = new HashMap<>();

        long id = 0;
        for (IOrgUnitReportDefinition definition : (List<IOrgUnitReportDefinition>) ApplicationRuntime.getBean(IOrgUnitReportDefinition.LIST_BEAN_NAME))
        {
            if (definition.isVisible(orgUnit))
            {
                final IOrgUnitReportBlockDefinition blockDef = definition.getBlock();
                OrgUnitReportBlock block = blockMap.get(blockDef);
                if (block == null) {
                    if (!blockDef.isVisible(orgUnit)) {
                        continue;
                    }
                    if (StringUtils.isNotBlank(blockDef.getPermissionPrefix())) {
                        if (!CoreServices.securityService().check(orgUnit, context, secModel.getPermission(blockDef.getPermissionPrefix()))) {
                            continue;
                        }
                    }
                    blockMap.put(blockDef, block = new OrgUnitReportBlock(blockDef));
                }
                if (StringUtils.isNotBlank(definition.getPermissionPrefix()))
                    if (!CoreServices.securityService().check(orgUnit, context, secModel.getPermission(definition.getPermissionPrefix())))
                        continue;
                block.getReportList().add(new OrgUnitReportDefinition(id++, definition));
            }
        }

        List<OrgUnitReportBlock> blockList = new ArrayList<>();
        for (OrgUnitReportBlock block : blockMap.values())
            if (!block.getReportList().isEmpty())
                blockList.add(block);
        Collections.sort(blockList, ITitled.TITLED_COMPARATOR);
        return blockList;
    }

    @SuppressWarnings("unchecked")
    public static boolean isReportTabVisible(OrgUnit orgUnit, CommonPostfixPermissionModel secModel, IPrincipalContext context)
    {
        if (!ApplicationRuntime.containsBean(IOrgUnitReportDefinition.LIST_BEAN_NAME))
            return false;

        Map<IOrgUnitReportBlockDefinition, OrgUnitReportBlock> blockMap = new HashMap<>();

        for (IOrgUnitReportDefinition definition : (List<IOrgUnitReportDefinition>) ApplicationRuntime.getBean(IOrgUnitReportDefinition.LIST_BEAN_NAME))
        {
            if (definition.isVisible(orgUnit))
            {
                final IOrgUnitReportBlockDefinition blockDef = definition.getBlock();
                OrgUnitReportBlock block = blockMap.get(blockDef);
                if (block == null) {
                    if (!blockDef.isVisible(orgUnit)) {
                        continue;
                    }
                    if (StringUtils.isNotBlank(blockDef.getPermissionPrefix())) {
                        if (!CoreServices.securityService().check(orgUnit, context, secModel.getPermission(blockDef.getPermissionPrefix()))) {
                            continue;
                        }
                    }
                    blockMap.put(blockDef, new OrgUnitReportBlock(blockDef));
                }
                if (StringUtils.isNotBlank(definition.getPermissionPrefix()))
                    if (!CoreServices.securityService().check(orgUnit, context, secModel.getPermission(definition.getPermissionPrefix())))
                        continue;

                return true;
            }
        }
        return false;
    }

    // getters and setters

    @Override
    public String getBlockTitle()
    {
        return _blockTitle;
    }

    public void setBlockTitle(String blockTitle)
    {
        _blockTitle = blockTitle;
    }

    @Override
    public String getPermissionPrefix()
    {
        return _permissionPrefix;
    }

    public void setPermissionPrefix(String permissionPrefix)
    {
        _permissionPrefix = permissionPrefix;
    }

    public IOrgUnitReportVisibleResolver getVisibleResolver()
    {
        return _visibleResolver;
    }

    public void setVisibleResolver(IOrgUnitReportVisibleResolver visibleResolver)
    {
        _visibleResolver = visibleResolver;
    }
}
