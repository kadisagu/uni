/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.StudentDocumentTypeUsed;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;

/**
 * @author vip_delete
 * @since 14.10.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void updateUsed(Long studentDocumentTypeId)
    {
        StudentDocumentType studentDocumentType = getNotNull(StudentDocumentType.class, studentDocumentTypeId);
        studentDocumentType.setActive(!studentDocumentType.isActive());
        getSession().update(studentDocumentType);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentDocumentType.ENTITY_CLASS, "s");
        builder.addOrder("s", StudentDocumentType.P_TITLE);

        List<StudentDocumentType> result = builder.getResultList(getSession());
        UniBaseUtils.createPage(model.getDataSource(), result);
    }
}
