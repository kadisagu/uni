package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Категория обучаемого"
 * Имя сущности : studentCategory
 * Файл data.xml : catalogs.data.xml
 */
public interface StudentCategoryCodes
{
    /** Константа кода (code) элемента : Студент (title) */
    String STUDENT_CATEGORY_STUDENT = "1";
    /** Константа кода (code) элемента : Слушатель (title) */
    String STUDENT_CATEGORY_LISTENER = "2";
    /** Константа кода (code) элемента : Второе высшее (title) */
    String STUDENT_CATEGORY_SECONDARY = "3";
    /** Константа кода (code) элемента : Слушатель ДПО (title) */
    String STUDENT_CATEGORY_DPP = "4";

    Set<String> CODES = ImmutableSet.of(STUDENT_CATEGORY_STUDENT, STUDENT_CATEGORY_LISTENER, STUDENT_CATEGORY_SECONDARY, STUDENT_CATEGORY_DPP);
}
