/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.uni.base.ext.OrgUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.util.IDaoExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import ru.tandemservice.uni.base.ext.OrgUnit.logic.DaoDoArchiveOrgUnit;
import ru.tandemservice.uni.base.ext.OrgUnit.logic.DaoUpdateOrgUnitChangeType;

/**
 * @author Vasily Zhukov
 * @since 12.03.2012
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private OrgUnitManager _orgUnitManager;

    @Bean
    public IDaoExtension methodUpdateOrgUnitChangeTypeDao()
    {
        return new DaoUpdateOrgUnitChangeType();
    }

    @Bean
    public IDaoExtension methodDoArchiveOrgUnitDao()
    {
        return new DaoDoArchiveOrgUnit();
    }

    @Bean
    public ItemListExtension<IDaoExtension> methodUpdateOrgUnitChangeType()
    {
        return itemListExtension(_orgUnitManager.methodUpdateOrgUnitChangeType())
                .add("validateOrgUnitKindSetting", methodUpdateOrgUnitChangeTypeDao())
                .create();
    }

    @Bean
    public ItemListExtension<IDaoExtension> methodDoArchiveOrgUnit()
    {
        return itemListExtension(_orgUnitManager.methodDoArchiveOrgUnit())
                .add("validateOrgUnitStudentList", methodDoArchiveOrgUnitDao())
                .create();
    }
}
