/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.GroupHeaderAssign;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.util.OrgUnitByTypeAutocompleteModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setGroup(get(Group.class, model.getGroup().getId()));
        model.setOrgUnitTypeListModel(new LazySimpleSelectModel<>(OrgUnitManager.instance().dao().getOrgUnitActiveTypeList()));
        model.setOrgUnitListModel(new OrgUnitByTypeAutocompleteModel(model));
        model.setEmployeePostListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                if (model.getOrgUnit() == null)
                {
                    return ListResult.getEmpty();
                }
                final MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
                builder.add(MQExpression.eq("ep", EmployeePost.L_EMPLOYEE + "." + Employee.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.eq("ep", EmployeePost.postStatus().active().s(), Boolean.TRUE));
                builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT, model.getOrgUnit()));
                if (StringUtils.isNotEmpty(filter))
                    builder.add(MQExpression.like("ep", EmployeePost.person().identityCard().fullFio().s(), CoreStringUtils.escapeLike(filter, true)));
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME);
                builder.addOrder("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public String getLabelFor(final Object value, int columnIndex)
            {
                return ((EmployeePost) value).getTitleWithOrgUnit();
            }

            @Override
            public Object getValue(final Object primaryKey)
            {
                final EmployeePost employeePost = get(EmployeePost.class, (Long) primaryKey);
                return employeePost.getOrgUnit().equals(model.getOrgUnit()) ? employeePost : null;
            }
        });
    }

    @Override
    public void update(final Model model)
    {
        update(model.getGroup());
    }
}