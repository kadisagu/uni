/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uni.entity.catalog.gen.StructureEducationLevelsGen;

import java.util.*;

import static ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes.*;

/**
 * Структура направлений подготовки (специальностей) Минобрнауки РФ
 */
public class StructureEducationLevels extends StructureEducationLevelsGen implements IHierarchyItem
{
    public String getDativeCaseSimpleShortTitle()
    {
        return isSpecialityPrintTitleMode() ? "специальности" : "направлению";
    }

    public String getGenitiveCaseSimpleShortTitle()
    {
        return isSpecialityPrintTitleMode() ? "специальности" : "направления";
    }

    public String getDativeCaseShortTitle()
    {
        return isSpecialityPrintTitleMode() ? "специальности" : "направлению подготовки";
    }

    public String getNominativeCaseSimpleShortTitle()
    {
        return isSpecialityPrintTitleMode() ? "специальность" : "направление";
    }

    public String getAccusativeCaseSimpleShortTitle()
    {
        if (isSpecialization())
            return "специализацию";
        if (isMaster() && isProfile())
            return "магистерский профиль (программу)";
        if (isBachelor() && isProfile())
            return "бакалаврский профиль (программу)";
        if (isMaster())
            return "направление магистров";
        if (isBachelor())
            return "направление бакалавров";
        if (isSpecialityPrintTitleMode())
            return "специальность";
        return "направление";
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }

    public static final Set<String> GROUPING_LEVEL_CODES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
            HIGH_GOS2_GROUP, HIGH_GOS2_GROUP_MASTER_FIELD, HIGH_GOS2_GROUP_BACHELOR_FIELD, HIGH_GOS2_GROUP_SPECIALTY_FIELD, HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY, HIGH_GOS2_GROUP_SPECIALTY,
            HIGH_GOS3_GROUP, HIGH_GOS3_GROUP_MASTER_FIELD, HIGH_GOS3_GROUP_BACHELOR_FIELD, HIGH_GOS3_GROUP_SPECIALTY_FIELD,
            MIDDLE_GOS2_GROUP, MIDDLE_GOS2_GROUP_SPECIALTY_FIELD,
            MIDDLE_GOS3_GROUP, MIDDLE_GOS3_GROUP_SPECIALTY_FIELD,
            BASIC_GROUP, BASIC_GROUP_PROFESSION_FIELD,
            ADDITIONAL_GROUP, ADDITIONAL_GROUP_ADDITIONAL, ADDITIONAL_GROUP_TRAINING_FIELD, ADDITIONAL_GROUP_RETRAINING_FIELD, ADDITIONAL_GROUP_PROBATION,
            HIGHER_SPECIALITY_POSTGRADUATE
    )));
    public boolean isGroupingLevel()
    {
        return GROUPING_LEVEL_CODES.contains(getCode());
    }

    public boolean isSpecialityPrintTitleMode()
    {
        return isSpecialty() || isSpecialization();
    }

    public boolean isHighGos2()
    {
        return isHigh() && isGos2();
    }

    public boolean isHighGos3()
    {
        return isHigh() && isGos3();
    }

    public boolean isMiddleGos2()
    {
        return isMiddle() && isGos2();
    }

    public boolean isMiddleGos3()
    {
        return isMiddle() && isGos3();
    }

    public List<Integer> getPriorityTrail()
    {
        if (null == this.getParent())
        {
            return Collections.singletonList(this.getPriority());
        }
        final List<Integer> list = new ArrayList<>(this.getParent().getPriorityTrail());
        list.add(this.getPriority());
        return list;
    }

    public static Comparator<StructureEducationLevels> COMPARATOR = new Comparator<StructureEducationLevels>()
    {
        @Override
        public int compare(final StructureEducationLevels o1, final StructureEducationLevels o2)
        {
            int compare = this.compare(o1.getPriorityTrail().iterator(), o2.getPriorityTrail().iterator());
            if (compare == 0)
                compare = o1.getId().compareTo(o2.getId());
            return compare;
        }

        private <T extends Comparable<T>> int compare(final Iterator<T> i1, final Iterator<T> i2)
        {
            while (i1.hasNext() && i2.hasNext())
            {
                final int result = i1.next().compareTo(i2.next());
                if (result != 0)
                {
                    return result;
                }
            }

            if (i1.hasNext())
            {
                return 1;
            }
            if (i2.hasNext())
            {
                return -1;
            }
            return 0;
        }
    };

    public StructureEducationLevels getRoot() {
        StructureEducationLevels p = this;
        while (null != p.getParent()) { p = p.getParent(); }
        return p;
    }

}
