/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;

/**
 * @author agolubenko
 * @since 29.12.2008
 */
public class UniStringUtils extends CommonBaseStringUtil
{
    public static <T extends IEntity, V> String getAndJoin(final Class<T> clazz, final MetaDSLPath<V> property, final V value, final MetaDSLPath<String> selectProperty, final String separator) {
        return StringUtils.join(IUniBaseDao.instance.get().getPropertiesList(clazz, property, value, true, selectProperty), separator);
    }
}
