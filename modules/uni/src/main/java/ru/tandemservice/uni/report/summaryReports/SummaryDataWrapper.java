/**
 *$Id:$
 */
package ru.tandemservice.uni.report.summaryReports;

import com.google.common.base.Function;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Враппер для количественных отчетав.
 * По заданой логике инкреминтирует значение для проперти (колонке) и сохраняет id сущьности которая попала в инкремент. Список id в колонке можно получить.
 *
 * @author Alexander Shaburov
 * @since 30.10.12
 */
public class SummaryDataWrapper<E extends IEntity> extends DataWrapper
{
    protected Map<String,Function<E,Boolean>> _incrementsMap;
    protected Map<String,Set<Long>> _idsMap;
    protected String _incProperty;

    public SummaryDataWrapper()
    {
        _incrementsMap = new HashMap<String, Function<E, Boolean>>();
        _idsMap = new HashMap<String, Set<Long>>();
        _incProperty = null;
    }

    public SummaryDataWrapper<E> addIncrement(String name, Function<E,Boolean> func)
    {
        _incrementsMap.put(name, func);
        _idsMap.put(name, new HashSet<Long>());
        setProperty(name, 0);
        return this;
    }

    public void inc(E e)
    {
        for (Map.Entry<String, Function<E,Boolean>> entry : _incrementsMap.entrySet())
        {
            if (entry.getValue().apply(e))
            {
                int val = getInteger(entry.getKey());
                Number increment = _incProperty == null ? 1 : (Number)e.getProperty(_incProperty);
                setProperty(entry.getKey(), val + increment.intValue());
                _idsMap.get(entry.getKey()).add(e.getId());
            }
        }
    }

    public Set<Long> getMatchedIds(String name)
    {
        return _idsMap.get(name);
    }

    public void setIncProperty(String incProperty)
    {
        _incProperty = incProperty;
    }
}
