/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummaryNew.Pub;

import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.report.StudentSummaryNewReport;

/**
 * @author dseleznev
 * @since 14.03.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(get(StudentSummaryNewReport.class, model.getReportId()));
        if (null != model.getReport().getOrgUnit())
            model.setSecModel(new OrgUnitSecModel(model.getReport().getOrgUnit()));
    }
}