/**
 * $Id$
 */
package ru.tandemservice.uni.component.reports.studentsGroupsList.Add;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 15.06.2009
 */
@Input(keys = "groupIdsList", bindings = "groupIdsList")
public class Model
{
    // Значения фильтров, взятые с формы со списком групп 
    private List<Long> _groupIdsList;

    private List<StudentStatus> _statusesList;
    private IMultiSelectModel _statusesListModel;


    // О Т О Б Р А Ж А Е М Ы Е   К О Л О Н К И

    // Атрибуты студента
    private boolean _sexReq;
    private boolean _birthDateReq;
    private boolean _compensationTypeReq;
    private boolean _thriftyGroupReq;
    private boolean _studentStatusReq;
    private boolean _personalNumberReq;
    private boolean _bookNumberReq;

    // Атрибуты группы
    private boolean _eduLevelReq;
    private boolean _developFormReq;
    private boolean _developConditionReq;
    private boolean _developTechReq;
    private boolean _developPeriodReq;
    private boolean _formativeOrgUnitReq;
    private boolean _territorialOrgUnitReq;
    private boolean _producingOrgUnitReq;

    // Р Е З У Л Ь Т А Т Ы   В Ы Б О Р К И
    private List<Group> _groupsList;
    private Map<Group, List<Student>> _studentsMap;

    // О С Т А Л Ь Н Ы Е   Д А Н Н Ы Е
    private String[] _columnHeaders;
    private int[] _columnSizes;

    public List<Long> getGroupIdsList()
    {
        return _groupIdsList;
    }

    public void setGroupIdsList(List<Long> groupIdsList)
    {
        this._groupIdsList = groupIdsList;
    }

    public List<StudentStatus> getStatusesList()
    {
        return _statusesList;
    }

    public void setStatusesList(List<StudentStatus> statusesList)
    {
        this._statusesList = statusesList;
    }

    public IMultiSelectModel getStatusesListModel()
    {
        return _statusesListModel;
    }

    public void setStatusesListModel(IMultiSelectModel statusesListModel)
    {
        this._statusesListModel = statusesListModel;
    }

    public boolean isSexReq()
    {
        return _sexReq;
    }

    public void setSexReq(boolean sexReq)
    {
        this._sexReq = sexReq;
    }

    public boolean isBirthDateReq()
    {
        return _birthDateReq;
    }

    public void setBirthDateReq(boolean birthDateReq)
    {
        this._birthDateReq = birthDateReq;
    }

    public boolean isCompensationTypeReq()
    {
        return _compensationTypeReq;
    }

    public void setCompensationTypeReq(boolean compensationTypeReq)
    {
        this._compensationTypeReq = compensationTypeReq;
    }

    public boolean isThriftyGroupReq()
    {
        return _thriftyGroupReq;
    }

    public void setThriftyGroupReq(boolean thriftyGroupReq)
    {
        this._thriftyGroupReq = thriftyGroupReq;
    }

    public boolean isStudentStatusReq()
    {
        return _studentStatusReq;
    }

    public void setStudentStatusReq(boolean studentStatusReq)
    {
        this._studentStatusReq = studentStatusReq;
    }

    public boolean isPersonalNumberReq()
    {
        return _personalNumberReq;
    }

    public void setPersonalNumberReq(boolean personalNumberReq)
    {
        this._personalNumberReq = personalNumberReq;
    }

    public boolean isEduLevelReq()
    {
        return _eduLevelReq;
    }

    public void setEduLevelReq(boolean eduLevelReq)
    {
        this._eduLevelReq = eduLevelReq;
    }

    public boolean isDevelopFormReq()
    {
        return _developFormReq;
    }

    public void setDevelopFormReq(boolean developFormReq)
    {
        this._developFormReq = developFormReq;
    }

    public boolean isDevelopConditionReq()
    {
        return _developConditionReq;
    }

    public void setDevelopConditionReq(boolean developConditionReq)
    {
        this._developConditionReq = developConditionReq;
    }

    public boolean isDevelopTechReq()
    {
        return _developTechReq;
    }

    public void setDevelopTechReq(boolean developTechReq)
    {
        this._developTechReq = developTechReq;
    }

    public boolean isDevelopPeriodReq()
    {
        return _developPeriodReq;
    }

    public void setDevelopPeriodReq(boolean developPeriodReq)
    {
        this._developPeriodReq = developPeriodReq;
    }

    public boolean isFormativeOrgUnitReq()
    {
        return _formativeOrgUnitReq;
    }

    public void setFormativeOrgUnitReq(boolean formativeOrgUnitReq)
    {
        this._formativeOrgUnitReq = formativeOrgUnitReq;
    }

    public boolean isTerritorialOrgUnitReq()
    {
        return _territorialOrgUnitReq;
    }

    public void setTerritorialOrgUnitReq(boolean territorialOrgUnitReq)
    {
        this._territorialOrgUnitReq = territorialOrgUnitReq;
    }

    public boolean isProducingOrgUnitReq()
    {
        return _producingOrgUnitReq;
    }

    public void setProducingOrgUnitReq(boolean producingOrgUnitReq)
    {
        this._producingOrgUnitReq = producingOrgUnitReq;
    }

    public List<Group> getGroupsList()
    {
        return _groupsList;
    }

    public void setGroupsList(List<Group> groupsList)
    {
        this._groupsList = groupsList;
    }

    public Map<Group, List<Student>> getStudentsMap()
    {
        return _studentsMap;
    }

    public void setStudentsMap(Map<Group, List<Student>> studentsMap)
    {
        this._studentsMap = studentsMap;
    }

    public String[] getColumnHeaders()
    {
        return _columnHeaders;
    }

    public void setColumnHeaders(String[] columnHeaders)
    {
        this._columnHeaders = columnHeaders;
    }

    public int[] getColumnSizes()
    {
        return _columnSizes;
    }

    public void setColumnSizes(int[] columnSizes)
    {
        this._columnSizes = columnSizes;
    }

    public boolean isBookNumberReq() {
        return _bookNumberReq;
    }

    public void setBookNumberReq(boolean bookNumber) {
        _bookNumberReq = bookNumber;
    }
}