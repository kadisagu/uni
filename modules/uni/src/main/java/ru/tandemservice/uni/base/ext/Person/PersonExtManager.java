/* $Id$ */
package ru.tandemservice.uni.base.ext.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.util.IPersonDivideDuplicateDoc;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 23.10.2015
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private PersonManager _personManager;

    @Bean
    public ItemListExtension<IPersonDivideDuplicateDoc> _personDivideExtPoint()
    {
        return itemListExtension(_personManager.personDivideExtPoint())
                .add("uni", new IPersonDivideDuplicateDoc()
                {

                    @Override
                    public List<PersonEduDocument> getDuplicateDocuments(final Person template, final Person duplicate, Collection<PersonRole> personRoles)
                    {
                        List<PersonEduDocument> resultDocumentList = new ArrayList<>();
                        List<PersonEduDocument> templateDocumentList = DataAccessServices.dao().getList(PersonEduDocument.class, PersonEduDocument.L_PERSON, template);
                        for (PersonEduDocument templateDocument : templateDocumentList)
                        {
                            //создаем для новой персоны копии документов об образовании
                            final PersonEduDocument personEduDocument = new PersonEduDocument();
                            personEduDocument.update(templateDocument);
                            personEduDocument.setPerson(duplicate);
                            if (template.getMainEduDocument() != null && templateDocument.equals(template.getMainEduDocument()))
                                duplicate.setMainEduDocument(personEduDocument);

                            for (PersonRole role : personRoles)
                            {
                                if (role instanceof Student)
                                {
                                    //Перелинковываем студента на копию документа об образовании
                                    Student student = (Student) role;
                                    student.setEduDocument(personEduDocument);
                                }
                            }
                            resultDocumentList.add(personEduDocument);
                        }


                        return resultDocumentList;
                    }
                }).create();
    }

}
