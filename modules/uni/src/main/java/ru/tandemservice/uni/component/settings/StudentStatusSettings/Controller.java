/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.StudentStatusSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        prepareDataSource(context);
    }

    private void prepareDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);
        DynamicListDataSource<StudentStatus> dataSource = UniBaseUtils.createDataSource(context, getDao());
        dataSource.addColumn(new SimpleColumn("Название", StudentStatus.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Использовать", StudentStatus.P_USED_IN_SYSTEM).setListener("onClickUsedInSystem"));
        dataSource.addColumn(new ToggleColumn("Активное состояние", StudentStatus.P_ACTIVE, "Состояние считается активным", "Состояние считается не активным").setListener("onClickActive"));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickActive(IBusinessComponent context)
    {
        getDao().doToggleActive((Long) context.getListenerParameter());
    }

    public void onClickUsedInSystem(IBusinessComponent context)
    {
        getDao().doToggleUsedInSystem((Long) context.getListenerParameter());
    }
    
    public void onClickUp(IBusinessComponent context)
    {
        getDao().updatePriorityUp((Long) context.getListenerParameter());
    }

    public void onClickDown(IBusinessComponent context)
    {
        getDao().updatePriorityDown((Long) context.getListenerParameter());
    }
}
