/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitList;

import java.util.List;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

/**
 * @author vip_delete
 */
@State(keys = {"kindCode"}, bindings = {"kindCode"})
@Output(keys = {"kindCode"}, bindings = {"kindCode"})
public class Model
{
    private DynamicListDataSource<OrgUnitToKindRelation> _dataSource;
    private Long _orgUnitToKindRelationId;
    private String _kindCode;
    private String _title;
    private String _orgUnitTitle;
    private OrgUnitType _orgUnitType;
    private List<OrgUnitType> _orgUnitTypeList;

    public DynamicListDataSource<OrgUnitToKindRelation> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<OrgUnitToKindRelation> dataSource)
    {
        _dataSource = dataSource;
    }

    public Long getOrgUnitToKindRelationId()
    {
        return _orgUnitToKindRelationId;
    }

    public void setOrgUnitToKindRelationId(Long orgUnitToKindRelationId)
    {
        _orgUnitToKindRelationId = orgUnitToKindRelationId;
    }

    public String getKindCode()
    {
        return _kindCode;
    }

    public void setKindCode(String kindCode)
    {
        _kindCode = kindCode;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        _orgUnitType = orgUnitType;
    }

    public List<OrgUnitType> getOrgUnitTypeList()
    {
        return _orgUnitTypeList;
    }

    public void setOrgUnitTypeList(List<OrgUnitType> orgUnitTypeList)
    {
        _orgUnitTypeList = orgUnitTypeList;
    }
}
