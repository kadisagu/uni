package ru.tandemservice.uni.component.settings.CombinationsFormsAndConditionsAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;

/**
 * 
 * @author nkokorina
 * @since 01.03.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechList(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopGridList(new LazySimpleSelectModel<>(getCatalogItemList(DevelopGrid.class)));

        if (null != model.getDevelopCombinationId())
        {
            model.setDevelopCombination(getNotNull(DevelopCombination.class, model.getDevelopCombinationId()));
        }
        else
        {
            model.setDevelopCombination(new DevelopCombination());
        }
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getDevelopCombination());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        MQBuilder builder = new MQBuilder(DevelopCombination.ENTITY_CLASS, "dc");
        builder.add(MQExpression.and(MQExpression.eq("dc", DevelopCombination.developForm().id().s(), model.getDevelopCombination().getDevelopForm().getId()), MQExpression.eq("dc", DevelopCombination.developTech().id().s(), model.getDevelopCombination().getDevelopTech().getId()), MQExpression.eq("dc", DevelopCombination.developCondition().id().s(), model.getDevelopCombination().getDevelopCondition().getId()), MQExpression.eq("dc", DevelopCombination.developGrid().id().s(), model.getDevelopCombination().getDevelopGrid().getId())));
        if (model.getDevelopCombination().getId() != null)
        {
            builder.add(MQExpression.notEq("dc", DevelopCombination.P_ID, model.getDevelopCombination().getId()));
        }
        if (builder.getResultCount(getSession()) > 0)
        {
            errors.add("Такая комбинация уже существует.");
        }
    }
}
