/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentAdd;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class DAO extends org.tandemframework.shared.person.base.bo.Person.util.AbstractPersonRoleAdd.DAO<Student, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        Student student = model.getStudent();
        student.setStatus(getByCode(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_POSSIBLE));
        if (model.getGroupId() != null)
            model.setGroup(get(Group.class, model.getGroupId()));

        model.setCompensationTypesList(CatalogManager.instance().dao().getCatalogItemList(CompensationType.class));
        model.setNationalityModel(new LazySimpleSelectModel<>(Nationality.class).setRecordLimit(50));
        model.setCourseList(DevelopGridDAO.getCourseList());

        int currentYear = UniBaseUtils.getCurrentYear();
        student.setEntranceYear(currentYear);
        model.setStudentCategoryList(CatalogManager.instance().dao().getCatalogItemList(StudentCategory.class));

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, model.getPrincipalContext()));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model));

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));

        if (model.getGroup() != null)
        {
            Group group = model.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();
            model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
            model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
            model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
            model.setDevelopForm(educationOrgUnit.getDevelopForm());
            model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
            model.setDevelopTech(educationOrgUnit.getDevelopTech());
            model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
            model.getStudent().setCourse(group.getCourse());
        }
    }

    @Override
    public void create(Model model)
    {
        Session session = getSession();
        Person person = model.getPerson();
        IdentityCard identityCard = person.getIdentityCard();

        PersonManager.instance().dao().checkPersonUnique(person);

        DatabaseFile photo = new DatabaseFile();
        session.save(photo);

        identityCard.setPhoto(photo);
        session.save(identityCard);

        session.save(person.getContactData());

        session.save(person);

        identityCard.setPerson(person);
        update(identityCard);

        session.save(person);

        createOnBasis(model, person);
    }

    private void createOnBasis(Model model, Person person)
    {
        Student student = model.getStudent();
        student.setGroup(model.getGroup());
        student.setPerson(person);

        EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");
        student.setEducationOrgUnit(educationOrgUnit);
        student.setDevelopPeriodAuto(educationOrgUnit.getDevelopPeriod());
//        student.setPersonalNumber(generateUniqueStudentNumber(student.getEntranceYear(), getSession()));

        save(student);
    }

    @Override
    public void createOnBasis(Model model)
    {
        createOnBasis(model, getBasisPerson(model));
    }

//    public static int generateUniqueStudentNumber(int entranceYear, Session session)
//    {
//        MQBuilder builder = new MQBuilder("select max(s." + Student.P_PERSONAL_NUMBER + ") from " + Student.ENTITY_CLASS + " s");
//        builder.add(MQExpression.eq("s", "entranceYear", entranceYear));
//        Number maxNumber = (Number) builder.uniqueResult(session);
//        int max = (maxNumber == null) ? 0 : maxNumber.intValue();
//
//        return (max != 0) ? max + 1 : (entranceYear % 100) * 10000 + 1;
//    }

    public Student getNewInstance(Model model)
    {
        return model.getStudent();
    }

    private List<Integer> getEntranceYearList(int educationYear)
    {
        List<Integer> result = new ArrayList<Integer>();
        for (Integer year = educationYear - 9; year <= educationYear; year++)
            result.add(year);
        return result;
    }

    public boolean checkStudentBookNumber(Student student)
    {
        Criteria criteria = getSession().createCriteria(Student.class);
        criteria.add(Restrictions.eq(Student.P_BOOK_NUMBER, student.getBookNumber()));
        criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));
        criteria.setProjection(Projections.rowCount());
        return ((Number) criteria.uniqueResult()).intValue() == 0;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        IdentityCard identityCard = model.getPerson().getIdentityCard();
        int amount = -13;
        if (identityCard.getBirthDate() != null && identityCard.getBirthDate().after(CoreDateUtils.add(CoreDateUtils.getDayFirstTimeMoment(new Date()), Calendar.YEAR, amount)))
            errors.add("Студент должен быть старше 13 лет.", "birthDate");

        PersonManager.instance().dao().checkIdentityCard(model.getPerson().getIdentityCard(), errors);
        
        if (!checkStudentBookNumber(model.getStudent()))
            errors.add("Указанный номер зачетной книжки уже закреплен за другим студентом.", "bookNumber");

        int entranceYear = model.getStudent().getEntranceYear();

        if (entranceYear < 1900)
            errors.add("Значение поля «Год приема» должно быть не меньше, чем 1900.", "entranceYear");

        int currentYear = UniBaseUtils.getCurrentYear();

        if (entranceYear > currentYear)
            errors.add("Значение поля «Год приема» должно быть не больше, чем " + String.valueOf(currentYear) + ".", "entranceYear");
    }
}
