package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Конфигурация для скриптовой печати модуля «Юни»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniScriptItemGen extends ScriptItem
 implements org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.UniScriptItem";
    public static final String ENTITY_NAME = "uniScriptItem";
    public static final int VERSION_HASH = -2087752485;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniScriptItemGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniScriptItemGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniScriptItem.class;
        }

        public T newInstance()
        {
            return (T) new UniScriptItem();
        }
    }
    private static final Path<UniScriptItem> _dslPath = new Path<UniScriptItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniScriptItem");
    }
            

    public static class Path<E extends UniScriptItem> extends ScriptItem.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UniScriptItem.class;
        }

        public String getEntityName()
        {
            return "uniScriptItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
