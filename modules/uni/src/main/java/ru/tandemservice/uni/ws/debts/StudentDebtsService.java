package ru.tandemservice.uni.ws.debts;

/**
 * @author vdanilov
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.tandemframework.core.util.cache.SafeMap;

import org.tandemframework.core.util.cache.SpringBeanCache;

@WebService(serviceName="StudentDebtsService")
public class StudentDebtsService {

    @XmlTransient
    private static final SpringBeanCache<List<IStudentDebtResolver>> studentDebtResolverList = new SpringBeanCache<List<IStudentDebtResolver>>("studentDebtResolverList");

    @XmlType
    public static class StudentDebtInfo {
        @XmlAttribute public String module;
        @XmlAttribute public String key;
        @XmlAttribute public String keyTitle;
        @XmlAttribute public String value;
        @XmlAttribute public String valueTitle;
    }

    @XmlType
    public static class StudentInfo {
        @XmlAttribute public Long id;
        @XmlElements(@XmlElement) public StudentDebtInfo[] debts;
    }

    @WebMethod
    public StudentInfo[] getStudentDebts(@WebParam(name="ids") final Long[] studentIds) {
        // TODO: cache it (24 часа?)


        final List<IStudentDebtResolver> resolverList = StudentDebtsService.studentDebtResolverList.get();
        final List<StudentInfo> result = new ArrayList<StudentInfo>();
        if ((null != studentIds) && (studentIds.length > 0)) {
            final Collection<Long> ids = Arrays.asList(studentIds);
            final Map<Long, Collection<StudentDebtInfo>> globalDebtMap = SafeMap.get(ArrayList.class);
            for (final IStudentDebtResolver resolver: resolverList) {
                final Map<Long, Collection<StudentDebtInfo>> debtMap = resolver.getDebtMap(ids);
                for (final Map.Entry<Long, Collection<StudentDebtInfo>> e: debtMap.entrySet()) {
                    globalDebtMap.get(e.getKey()).addAll(e.getValue());
                }
            }

            for (final Map.Entry<Long, Collection<StudentDebtInfo>> e: globalDebtMap.entrySet()) {
                final StudentInfo studentInfo = new StudentInfo();
                studentInfo.id=e.getKey();
                studentInfo.debts=e.getValue().toArray(new StudentDebtInfo[e.getValue().size()]);
                result.add(studentInfo);
            }
        }
        return result.toArray(new StudentInfo[result.size()]);
    }

}
