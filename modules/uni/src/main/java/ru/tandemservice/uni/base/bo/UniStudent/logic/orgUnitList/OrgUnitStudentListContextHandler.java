/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 24.10.2013
 */
public class OrgUnitStudentListContextHandler extends BaseStudentListContextHandler
{
    private OrgUnit _orgUnit;

    public OrgUnitStudentListContextHandler(Boolean archival, OrgUnit orgUnit)
    {
        super(archival);
        _orgUnit = orgUnit;
    }

    @Override
    public void setStudentListContext(DQLSelectBuilder builder, String studentAlias, String educationOrgUnitAlias)
    {
        super.setStudentListContext(builder, studentAlias, educationOrgUnitAlias);

        // условие на НПП
        IDQLExpression expression = FilterUtils.getEducationOrgUnitFilter(educationOrgUnitAlias, _orgUnit);
        builder.where(null == expression ? eq(property(studentAlias, Student.id()), value(-1L)) : expression);
    }

}