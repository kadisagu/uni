/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;

import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.shared.person.base.entity.NotUsedEduInstitutionTypeKind;

/**
 * @author agolubenko
 * @since May 11, 2010
 */
public class EntityExportDAO extends UniBaseDao implements IEntityExportDAO
{
    private static final Map<String, String> _className2Query = new HashMap<String, String>();
    static
    {
        _className2Query.put(EducationalInstitutionTypeKind.ENTITY_CLASS, "from " + EducationalInstitutionTypeKind.ENTITY_CLASS + " where id not in (select settings." + NotUsedEduInstitutionTypeKind.educationalInstitutionTypeKind().s() + " from " + NotUsedEduInstitutionTypeKind.ENTITY_CLASS + " settings where settings." + NotUsedEduInstitutionTypeKind.personRoleName().s() + " = 'Entrant') order by id");
        _className2Query.put(EduInstitution.ENTITY_CLASS, "from " + EduInstitution.ENTITY_CLASS + " where " + EduInstitution.address().settlement().s() + " is not null and " + EduInstitution.eduInstitutionKind().s() + " in (select eitk.id from " + EducationalInstitutionTypeKind.ENTITY_CLASS + " eitk where eitk.id not in (select settings." + NotUsedEduInstitutionTypeKind.educationalInstitutionTypeKind().s() + " from " + NotUsedEduInstitutionTypeKind.ENTITY_CLASS + " settings where settings." + NotUsedEduInstitutionTypeKind.personRoleName().s() + " = 'Entrant')) order by id");
    }

    @Override
    public EntityExporter<IEntity> createEntityExporter(final String className)
    {
        return new EntityExporter<IEntity>()
        {
            @SuppressWarnings("unchecked")
            @Override
            public Iterator<IEntity> iterate()
            {
                return getSession().createQuery(_className2Query.get(className)).list().iterator();
            }
        };
    }

}
