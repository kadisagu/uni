/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.UniDefines;

/**
 * @author vip_delete
 * @since 21.01.2009
 * @deprecated use OrgUnitKindAutocompleteModel
 */
@Deprecated
public class OrgUnitKindMultiSelectModel extends FullCheckSelectModel
{
    private OrgUnitKindAutocompleteModel _autocompleteModel;

    public OrgUnitKindMultiSelectModel(String kindCode, IPrincipalContext principalContext)
    {
        super(kindCode.equals(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL) ? OrgUnit.P_TERRITORIAL_TITLE : OrgUnit.P_TITLE);
        _autocompleteModel = new OrgUnitKindAutocompleteModel(kindCode, principalContext);
    }

    @Override
    public ListResult findValues(String filter)
    {
        return _autocompleteModel.findValues(filter);
    }
}