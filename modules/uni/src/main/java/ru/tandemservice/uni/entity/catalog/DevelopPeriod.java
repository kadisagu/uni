/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.entity.catalog;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uni.catalog.bo.DevelopPeriod.ui.AddEdit.DevelopPeriodAddEdit;
import ru.tandemservice.uni.entity.catalog.gen.DevelopPeriodGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

import java.util.Collection;
import java.util.Set;

/**
 * Срок освоения
 */
public class DevelopPeriod extends DevelopPeriodGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, DevelopPeriod.class)
                .titleProperty(DevelopPeriod.title().s())
                .where(DevelopPeriod.enabled(), Boolean.TRUE)
                .filter(DevelopPeriod.title())
                .order(DevelopPeriod.priority());
    }

    private static final Set<String> HIDDEN_FIELDS = ImmutableSet.of(P_LAST_COURSE, L_EDU_PROGRAM_DURATION, P_USER_CODE);
    private static final Set<String> HIDDEN_FIELDS_ITEM_PUB = ImmutableSet.of(P_USER_CODE);

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_FIELDS; }
            @Override public Collection<String> getHiddenFieldsItemPub() { return HIDDEN_FIELDS_ITEM_PUB; }
            @Override public Collection<String> getHiddenFieldsAddEditForm() { return ImmutableSet.of(L_EDU_PROGRAM_DURATION, P_USER_CODE); }
            @Override public String getAddEditComponentName() { return DevelopPeriodAddEdit.class.getSimpleName(); }
            @Override public void addAdditionalColumns(DynamicListDataSource<ICatalogItem> dataSource) {
                dataSource.addColumn(new SimpleColumn(P_LAST_COURSE, DevelopPeriod.lastCourse().s(), "Последний курс").setOrderable(false).setWidth("1px"));
                dataSource.addColumn(new SimpleColumn(EduProgramDuration.P_NUMBER_OF_YEARS, DevelopPeriod.eduProgramDuration().numberOfYears().s(), "Число полных лет обучения").setOrderable(false).setWidth("1px"));
                dataSource.addColumn(new SimpleColumn(EduProgramDuration.P_NUMBER_OF_MONTHS, DevelopPeriod.eduProgramDuration().numberOfMonths().s(), "Число полных месяцев обучения").setOrderable(false).setWidth("1px"));
                dataSource.addColumn(new SimpleColumn(EduProgramDuration.P_NUMBER_OF_HOURS, DevelopPeriod.eduProgramDuration().numberOfHours().s(), "Число часов обучения").setOrderable(false).setWidth("1px"));
            }
        };
    }
}