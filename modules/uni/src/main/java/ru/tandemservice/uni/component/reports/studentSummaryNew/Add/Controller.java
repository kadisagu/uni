/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummaryNew.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.sec.runtime.SecurityRuntime;

/**
 * @author dseleznev
 * @since 14.03.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(DataSettingsFacade.getSettings(model.getSettingsOwner()));
        getDao().prepare(model);
        String key = model.getOrgUnit() == null ? "editStudentSummaryReportColumns" : "orgUnit_editStudentSummaryReportColumns_" + StringUtils.uncapitalize(model.getOrgUnit().getOrgUnitType().getCode());
        ISecured securedObject = (ISecured) (model.getOrgUnit() == null ? (ISecured) SecurityRuntime.getInstance().getCommonSecurityObject() : model.getOrgUnit());
        model.setStatusEditingPermitted(!CoreServices.securityService().check(securedObject, component.getUserContext().getPrincipalContext(), key));
    }

    public void onClickApply(IBusinessComponent component)
    {
        /*Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(getDao().getReport(getModel(component)), "Report.xls");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));*/

        Model model = getModel(component);
        getDao().validate(model, component.getUserContext().getErrorCollector());
        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }
}