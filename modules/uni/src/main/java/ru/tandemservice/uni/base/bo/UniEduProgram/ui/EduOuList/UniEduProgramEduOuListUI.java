/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit.UniEduProgramEduOuAddEdit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.*;

/**
 * @author oleyba
 * @since 8/12/14
 */
public class UniEduProgramEduOuListUI extends UIPresenter
{
    private List<HSelectOption> _levelTypeList;
    private List<StructureEducationLevels> levelTopList;
    private ISelectModel _orgUnitListModel;
    private List<Qualifications> _qualificationList;

    private Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap;

    @Override
    public void onComponentRefresh()
    {
        setLevelTopList(DataAccessServices.dao().getList(StructureEducationLevels.class, StructureEducationLevels.parent(), (StructureEducationLevels) null, StructureEducationLevels.priority().s()));
        prepareLevelTypeModel();
        setOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        setQualificationList(DataAccessServices.dao().getList(Qualifications.class, Qualifications.P_CODE));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAsMap("subjectCode", "title", "levelType", "levelTop", "orgUnit"));
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        getSettings().set("levelTop", IUniBaseDao.instance.get().getCatalogItem(StructureEducationLevels.class, StructureEducationLevelsCodes.HIGH_GOS3_GROUP));
        prepareLevelTypeModel();
        onClickSearch();
    }

    public void onChangeLevelTop() {
        prepareLevelTypeModel();
    }

    public void onClickAdd() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class).activate();
    }

    public void onClickEdit() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickDelete() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickChangeUsedEduOu() {
        UniEduProgramManager.instance().dao().doChangeUsedEduOu(getListenerParameterAsLong());
    }

    // utils

    private void prepareLevelTypeModel()
    {
        if (null == getSettings())
        {
            setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(Collections.<StructureEducationLevels>emptyList(), true));
            return;
        }
        StructureEducationLevels levelTop = (StructureEducationLevels) getSettings().get("levelTop");
        if (null == levelTop)
        {
            setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(Collections.<StructureEducationLevels>emptyList(), true));
            return;
        }
        if (null == getStructureMap())
            prepareStructureMap();
        ArrayList<StructureEducationLevels> list = new ArrayList<StructureEducationLevels>(getStructureMap().get(levelTop));
        Collections.sort(list, StructureEducationLevels.COMPARATOR);
        setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(list, true));
    }

    private void prepareStructureMap()
    {
        List<StructureEducationLevels> baseList = DataAccessServices.dao().getList(StructureEducationLevels.class);
        Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap = new HashMap<StructureEducationLevels, Set<StructureEducationLevels>>();
        for (StructureEducationLevels level : baseList)
            SafeMap.safeGet(structureMap, getRoot(level), HashSet.class).add(level);
        setStructureMap(structureMap);
    }

    private StructureEducationLevels getRoot(StructureEducationLevels item)
    {
        StructureEducationLevels root = item;
        while (root.getParent() != null)
            root = root.getParent();
        return root;
    }

    // getters and setters

    public List<HSelectOption> getLevelTypeList()
    {
        return _levelTypeList;
    }

    public void setLevelTypeList(List<HSelectOption> levelTypeList)
    {
        _levelTypeList = levelTypeList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<StructureEducationLevels> getLevelTopList()
    {
        return levelTopList;
    }

    public void setLevelTopList(List<StructureEducationLevels> levelTopList)
    {
        this.levelTopList = levelTopList;
    }

    public Map<StructureEducationLevels, Set<StructureEducationLevels>> getStructureMap()
    {
        return structureMap;
    }

    public void setStructureMap(Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap)
    {
        this.structureMap = structureMap;
    }
}