/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao.group;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class GroupTitleAlgorithmSample2 extends UniBaseDao implements IAbstractGroupTitleDAO
{
    /*
     * Формат - СНУОВ(xx)-(yy)
     * Пример - ПИЭ07-01
     * Описание:
     *   СНУОВ - сокращенное название уровня образования ОУ;
     *   (xx) - две цифры года поступления (начального учебного года для группы);
     *   (yy) - номер группы на уровне ОУ.
     */
    @Override
    public String getTitle(Group group, int number)
    {
        EducationOrgUnit educationOrgUnit = get(group.getEducationOrgUnit());
        EducationYear startEducationYear = get(group.getStartEducationYear());

        return new StringBuilder()
        .append(educationOrgUnit.getEducationLevelHighSchool().getShortTitle())
        .append(String.format("%02d", startEducationYear.getIntValue() % 100))
        .append('-')
        .append(String.format("%02d", number % getMaxNumber()))
        .toString();
    }

    @Override
    public int getMaxNumber()
    {
        return 100;
    }
}