package ru.tandemservice.uni.ui;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;

/**
 * @author vdanilov
 */
public class ProxyRowCustomizer<T extends IEntity> implements IRowCustomizer<T> {

    private final IRowCustomizer<T> base;
    public IRowCustomizer<T> getBase() { return this.base; }

    public ProxyRowCustomizer(IRowCustomizer<T> base) {
        this.base = base;
    }

    @Override
    public String getHeadRowStyle() {
        return getBase().getHeadRowStyle();
    }

    @Override
    public String getRowStyle(T entity) {
        return getBase().getRowStyle(entity);
    }

    @Override
    public boolean isClickable(AbstractColumn<?> column, T rowEntity) {
        return getBase().isClickable(column, rowEntity);
    }

    @Override
    public boolean isClickable(IPublisherLinkColumn column, T rowEntity, IEntity itemEntity) {
        return getBase().isClickable(column, rowEntity, itemEntity);
    }
}
