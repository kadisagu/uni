package ru.tandemservice.uni.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выданная студенту справка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.employee.StudentDocument";
    public static final String ENTITY_NAME = "studentDocument";
    public static final int VERSION_HASH = 589012061;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_NUMBER = "number";
    public static final String L_STUDENT_DOCUMENT_TYPE = "studentDocumentType";
    public static final String L_STUDENT = "student";
    public static final String L_CONTENT = "content";

    private Date _formingDate;     // Дата создания в системе
    private int _number;     // Номер справки
    private StudentDocumentType _studentDocumentType;     // Тип документа, выдаваемого студенту
    private Student _student;     // Студент
    private DatabaseFile _content;     // Печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата создания в системе. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Номер справки. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер справки. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Тип документа, выдаваемого студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentDocumentType getStudentDocumentType()
    {
        return _studentDocumentType;
    }

    /**
     * @param studentDocumentType Тип документа, выдаваемого студенту. Свойство не может быть null.
     */
    public void setStudentDocumentType(StudentDocumentType studentDocumentType)
    {
        dirty(_studentDocumentType, studentDocumentType);
        _studentDocumentType = studentDocumentType;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentDocumentGen)
        {
            setFormingDate(((StudentDocument)another).getFormingDate());
            setNumber(((StudentDocument)another).getNumber());
            setStudentDocumentType(((StudentDocument)another).getStudentDocumentType());
            setStudent(((StudentDocument)another).getStudent());
            setContent(((StudentDocument)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentDocument.class;
        }

        public T newInstance()
        {
            return (T) new StudentDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formingDate":
                    return obj.getFormingDate();
                case "number":
                    return obj.getNumber();
                case "studentDocumentType":
                    return obj.getStudentDocumentType();
                case "student":
                    return obj.getStudent();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "studentDocumentType":
                    obj.setStudentDocumentType((StudentDocumentType) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formingDate":
                        return true;
                case "number":
                        return true;
                case "studentDocumentType":
                        return true;
                case "student":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formingDate":
                    return true;
                case "number":
                    return true;
                case "studentDocumentType":
                    return true;
                case "student":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formingDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "studentDocumentType":
                    return StudentDocumentType.class;
                case "student":
                    return Student.class;
                case "content":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentDocument> _dslPath = new Path<StudentDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentDocument");
    }
            

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Номер справки. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Тип документа, выдаваемого студенту. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getStudentDocumentType()
     */
    public static StudentDocumentType.Path<StudentDocumentType> studentDocumentType()
    {
        return _dslPath.studentDocumentType();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    public static class Path<E extends StudentDocument> extends EntityPath<E>
    {
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Integer> _number;
        private StudentDocumentType.Path<StudentDocumentType> _studentDocumentType;
        private Student.Path<Student> _student;
        private DatabaseFile.Path<DatabaseFile> _content;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(StudentDocumentGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Номер справки. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(StudentDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Тип документа, выдаваемого студенту. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getStudentDocumentType()
     */
        public StudentDocumentType.Path<StudentDocumentType> studentDocumentType()
        {
            if(_studentDocumentType == null )
                _studentDocumentType = new StudentDocumentType.Path<StudentDocumentType>(L_STUDENT_DOCUMENT_TYPE, this);
            return _studentDocumentType;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.StudentDocument#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

        public Class getEntityClass()
        {
            return StudentDocument.class;
        }

        public String getEntityName()
        {
            return "studentDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
