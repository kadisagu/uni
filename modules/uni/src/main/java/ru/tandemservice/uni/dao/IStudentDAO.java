/* $Id$ */
package ru.tandemservice.uni.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Nikolay Fedorovskih
 * @since 13.01.2014
 */
public interface IStudentDAO
{
    final SpringBeanCache<IStudentDAO> instance = new SpringBeanCache<>(IStudentDAO.class.getName());

    /**
     * Расширенный стикер для студента:
     * <p/>
     * Студент: <ФИО> (<состояние>, <Форма освоения>, <вид возм. затрат>) | Группа: <Название группы> | <Сокр название подразделения [(сокр название филиала)]> | <Название направления подготовки>",
     *
     * @return расширенный стикер для студента
     */
    String getFullTitleExtended(Student student);

    /**
     * Получения незанятого персонального номера студента. Сначала ищется максимальный занятый номер в указанном диапазоне.
     * Если он не найден, то возвращается min (т.е. в данном диапазоне ещё нет занятых номеров).
     * Если он меньше верхей границы, то возвращается найденный номер + 1.
     * Иначе ищется первый пробел в нумерации. Если пробела не найдено, возвращается {@code null}.
     * Метод должен вызываться только в рамках транзакции (т.к. вешается именная блокировка).
     *
     * @param prefix   префикс для номера
     * @param min      минимальный номер диапазона
     * @param max      максимальный номер диапазона
     * @param capacity порядок (длина) правой числовой части номера
     * @return незанятый номер в указанном диапазоне. Либо {@code null}, если свободных номеров нет.
     */
    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    String getFreePersonalNumber(String prefix, int min, int max, int capacity);

    /**
     * Генерирование нового персонального номера студента на основе данных переданного студента (в продуктовой реализации - это год только приема).
     * У переданного студента не должно быть своего ЛН (не должно быть в базе!). Иначе какой смысл ему новый номер искать?
     * Ситуацию, когда у студенту перегенерируется ЛН нужно разруливать отдельно, чтобы не получилось так, что номер сменится без смены префикса.
     * Т.е. в данном методе передаваемый студент служит как источник данных, на основе которых генерируется <b>новый</b> номер.
     *
     * Номер генерится в формате YYXXXX, где YY - последние две цифры года поступления, XXXX - порядковый номер в рамках года постапления
     * студента ({@link ru.tandemservice.uni.entity.employee.Student#personalNumber()}).
     * Количество разрядов XXXX зависит от проперти "student.personalNumberCapacity" (см. {@link ru.tandemservice.uni.ui.formatters.StudentNumberFormatter#getCapacity()}).
     * Сначала ищется максимальный занятый номер в рамках года. Если он не найден, то возвращается YY0001.
     * Если найденный номер меньше верхей границы (YY9999), то возвращается найденный номер + 1.
     * Иначе ищется первый пробел в нумерации. Если пробел не найден, возвращается {@code null} - свободных номеров в рамках указанного года нет.
     * Метод должен вызываться только в рамках транзакции (т.к. вешается именная блокировка).
     *
     * @param student студента
     * @return незанятый номер в рамках года поступления. Либо {@code null}, если свободных номеров в рамках года нет.
     */
    @Transactional(propagation = Propagation.MANDATORY, readOnly = true)
    String generateNewPersonalNumber(Student student);

    boolean isStudentCardPrintLegacyVersionEnabled();
}