/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSettings.ui.AccreditationList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.ui.AdditionalEdit.UniOrgUnitAdditionalEdit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.ui.AdditionalEdit.UniOrgUnitAdditionalEditUI;
import ru.tandemservice.uni.base.bo.UniSettings.UniSettingsManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uni.base.bo.UniSettings.logic.AccreditationSearchListDSHandler;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author Ekaterina Zvereva
 * @since 17.11.2016
 */
public class UniSettingsAccreditationListUI extends UIPresenter
{
    Comparator<DataWrapper> _comparator;
    private AcademyData _academyData;

    private IUIDataSource accreditationRecordDS;

    public IUIDataSource getAccreditationRecordDS()
    {
        return accreditationRecordDS;
    }

    public void setAccreditationRecordDS(IUIDataSource accreditationRecordDS)
    {
        this.accreditationRecordDS = accreditationRecordDS;
    }

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        setAccreditationRecordDS(this.getConfig().getDataSource(UniSettingsAccreditationList.ACCREDITATIONS_SEARCH_LIST_DS));
        _academyData = AcademyData.getInstance();

        _comparator = new Comparator<DataWrapper>()
        {
            @Override
            public int compare(final DataWrapper o1, final DataWrapper o2)
            {
                int result=0;
                if (o1.getWrapped() instanceof IPersistentAccreditationOwner && o2.getWrapped() instanceof IPersistentAccreditationOwner)
                    result =  ((IPersistentAccreditationOwner) o1.getWrapped()).getDisplayableTitle().compareTo(((IPersistentAccreditationOwner) o2.getWrapped()).getDisplayableTitle());
                else if(o1.getWrapped() instanceof EduProgramSubject && o2.getWrapped() instanceof EduProgramSubject)
                    result=((EduProgramSubject) o1.getWrapped()).getTitleWithCode().compareTo(((EduProgramSubject) o2.getWrapped()).getTitleWithCode());
                return result;
            }
        };
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), "eduProgramKind", "eduProgramSubjectIndex", "institutionOrgUnit");
        onClickSearch();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(UniSettingsAccreditationList.PROGRAM_KIND, getSettings().get(UniSettingsAccreditationList.PROGRAM_KIND));
        dataSource.put(UniSettingsAccreditationList.PROGRAM_SUBJECT_INDEX, getSettings().get(UniSettingsAccreditationList.PROGRAM_SUBJECT_INDEX));
        dataSource.put(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT, getSettings().get(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT));
        dataSource.put("typeID", getSettings().get("typeID"));
        dataSource.put("accreditation", getSettings().get("accreditation"));
        dataSource.put("subjectTitle", getSettings().get("subjectTitle"));

    }

    public boolean isNothingSelected()
    {
        return getSettings().get(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT) == null ||
                getSettings().get(UniSettingsAccreditationList.PROGRAM_KIND) == null ||
                getSettings().get(UniSettingsAccreditationList.PROGRAM_SUBJECT_INDEX) == null;
    }

    public Comparator<DataWrapper> getComparator()
    {
       return _comparator;
    }

    public void onToggleLicenseOnOff()
    {
        UniSettingsManager.instance().accreditationDao()
                .updateProgramSubject(Collections.singletonList(getListenerParameterAsLong()), !((Boolean) ((DataWrapper)getAccreditationRecordDS().getRecordById(getListenerParameterAsLong())).getProperty("licenseField")),
                                      getSettings().get(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT));
        getSupport().setRefreshScheduled(true);
    }

    public void onClickLicenseOn()
    {
       onClickChangeLicense(true);
    }

    public void onClickLicenseOff()
    {
        onClickChangeLicense(false);
    }

    private void onClickChangeLicense(boolean activate)
    {
        Collection<IEntity> records = ((CheckboxColumn) ((BaseSearchListDataSource)getAccreditationRecordDS()).getLegacyDataSource().getColumn("checked")).getSelectedObjects();
        if (records.isEmpty())
        {
            ErrorCollector collector = UserContext.getInstance().getErrorCollector();
            collector.add("Не выбраны направления для редактирования лицензии.");
            return;
        }
        final Collection<Long> ids = UniBaseDao.ids(records);
        UniSettingsManager.instance().accreditationDao().updateProgramSubject(ids, activate, getSettings().get(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT));
        ((CheckboxColumn) ((BaseSearchListDataSource)getAccreditationRecordDS()).getLegacyDataSource().getColumn("checked")).reset();
        this.getSupport().setRefreshScheduled(true);
    }

    public void onChangeSelectors()
    {
        saveSettings();
    }

    public DataWrapper getCurrentRow() {
        return getAccreditationRecordDS() == null ? null : (DataWrapper) this.getAccreditationRecordDS().getCurrent();
    }

    public Long getCurrentRowId() {
        final DataWrapper currentRow = this.getCurrentRow();
        return (null == currentRow ? null : currentRow.getId());
    }

    public StateAccreditationEnum getCurrentRowValue() {
        final DataWrapper currentRow = this.getCurrentRow();
        return (null == currentRow ? null : (StateAccreditationEnum)currentRow.get(AccreditationSearchListDSHandler.PROPERTY_ACCR_STATE));
    }

    private DataWrapper currentEditRow;
    public DataWrapper getCurrentEditRow() { return this.currentEditRow; }
    protected void setCurrentEditRow(final DataWrapper currentEditRow) { this.currentEditRow = currentEditRow; }

    public boolean isComponentInEditMode() {
        return null != this.getCurrentEditRow();
    }

    public StateAccreditationEnum getCurrentEditRowValue() {
        if (!this.isComponentInEditMode()) { throw new IllegalStateException(); }
        return this.getCurrentEditRow().get(AccreditationSearchListDSHandler.PROPERTY_ACCR_STATE);
    }

    public void setCurrentEditRowValue(final StateAccreditationEnum value) {
        if (!this.isComponentInEditMode()) { throw new IllegalStateException(); }
        this.getCurrentEditRow().put(AccreditationSearchListDSHandler.PROPERTY_ACCR_STATE, value);
    }

    public boolean isCurrentRowEditable()
    {
        final DataWrapper currentRow = this.getCurrentRow();
        if (currentRow.getWrapped() instanceof EduProgramSubject)
            return false;
        if (getAccreditationRecordDS().getRecordById(currentRow.getId()) == null)
            return false;
        return true;
    }



    public boolean isCurrentRowInEditMode()
    {
        final DataWrapper editRow = this.getCurrentEditRow();
        if (null == editRow)
        {
            return false;
        }

        final DataWrapper currentRow = this.getCurrentRow();
        return !(null == currentRow || currentRow.getHierarhyParent() != null) && editRow.getId().equals(currentRow.getId());


    }

    public void onClickEditRow()
    {
        final Long id = this.getListenerParameterAsLong();
        if (null == id) { return; }

        final IUIDataSource ds = this.getAccreditationRecordDS();

        final DataWrapper current = ds.getRecordById(id);
        if (null == current) { return; }

        final DataWrapper wrapper = this.buildEditRowDataWrapper(current);
        if (null == wrapper) { return; }

        this.setCurrentEditRow(wrapper);
    }

    public void onClickSaveRow()
    {
        final Long id = this.getListenerParameterAsLong();
        if (null == id) { return; }

        final DataWrapper editRow = this.getCurrentEditRow();
        if (null == editRow) { return; }

        if (!id.equals(editRow.getId())) { return; }

        try {
            if (this.doSaveRow(editRow)) {
                this.setCurrentEditRow(null);
            }
        } catch (final Throwable t) {
            this.getSupport().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        ContextLocal.getInfoCollector().clear();
    }

    public void onClickCancelEdit()
    {
        this.setCurrentEditRow(null);
        getSupport().setRefreshScheduled(true);
    }

    protected DataWrapper buildEditRowDataWrapper(final DataWrapper current) {
        final DataWrapper editWrapper = new DataWrapper(current.getWrapped());
        editWrapper.put(AccreditationSearchListDSHandler.PROPERTY_ACCR_STATE, current.getProperty(AccreditationSearchListDSHandler.PROPERTY_ACCR_STATE));
        return editWrapper;
    }

    @SuppressWarnings("unchecked")
    protected boolean doSaveRow(final DataWrapper editRow) {
        UniSettingsManager.instance().accreditationDao().saveOrUpdateAccreditation(editRow, getSettings().get(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT),  getSettings().get(UniSettingsAccreditationList.PROGRAM_SUBJECT_INDEX));
        return true;
    }

    public void onClickEditAcademyData()
    {
        _uiActivation.asRegion(UniOrgUnitAdditionalEdit.class).parameter(UniOrgUnitAdditionalEditUI.ORG_UNIT_ID, TopOrgUnit.getInstance().getId()).activate();
    }
}