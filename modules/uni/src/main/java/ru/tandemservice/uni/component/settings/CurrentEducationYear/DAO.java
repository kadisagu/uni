/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.CurrentEducationYear;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(new EducationYearModel());
        List<EducationYear> list = getList(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE);
        if (null == list || list.isEmpty()) {
            model.setEducationYear(null);
        } else if (1 == list.size()) {
            model.setEducationYear(list.iterator().next());
        } else {
            ContextLocal.getInfoCollector().add("Учебный год указан некорректно. Необходимо выбрать текущий учебный год заново");
            model.setEducationYear(null);
        }
    }

    @Override
    public void update(Model model)
    {
        lock(EducationYear.class.getName());

        // сначала сбрасываем текущий год (иначе будет падать constraint)
        executeAndClear(
            new DQLUpdateBuilder(EducationYear.class)
            .set(EducationYear.P_CURRENT, value(Boolean.FALSE))
            .where(eq(property(EducationYear.P_CURRENT), value(Boolean.TRUE)))
        );

        // затем проставляем флаг (причем еще раз сбрасываем значения, если они там появились)
        executeAndClear(
            new DQLUpdateBuilder(EducationYear.class)
                .set(EducationYear.P_CURRENT, new DQLCaseExpressionBuilder()
                        .when(eq(property(EducationYear.P_ID), value(model.getEducationYear().getId())), value(Boolean.TRUE))
                        .otherwise(value(Boolean.FALSE))
                        .build()
                ));
    }
}
