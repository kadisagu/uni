/* $Id: Controller.java 20463 2011-10-25 05:43:26Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelMiddleGos3s.EducationLevelMiddleGos3sPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.EducationLevelMiddle;

/**
 * @author vip_delete
 * @since 12.11.2009
 */
public class Controller extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.Controller<EducationLevelMiddle, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EducationLevelMiddle> dataSource = new DynamicListDataSource<EducationLevelMiddle>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelMiddle.P_FULL_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EducationLevelMiddle.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подчинено направлению подготовки (специальности)", new String[]{EducationLevelMiddle.L_PARENT_LEVEL, EducationLevelMiddle.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        return dataSource;
    }
}
