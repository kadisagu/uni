package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Технология освоения"
 * Имя сущности : developTech
 * Файл data.xml : uni.education.data.xml
 */
public interface DevelopTechCodes
{
    /** Константа кода (code) элемента : Обычная (title) */
    String GENERAL = "1";
    /** Константа кода (code) элемента : Дистанционная (title) */
    String DISTANCE = "2";
    /** Константа кода (code) элемента : Дистанционная электронная (title) */
    String ELECTRONIC_DISTANCE = "3";
    /** Константа кода (code) элемента : Группа выходного дня (title) */
    String WEEKEND_GROUP = "4";

    Set<String> CODES = ImmutableSet.of(GENERAL, DISTANCE, ELECTRONIC_DISTANCE, WEEKEND_GROUP);
}
