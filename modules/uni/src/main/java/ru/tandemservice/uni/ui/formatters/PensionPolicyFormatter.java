/**
 * $Id$
 */
package ru.tandemservice.uni.ui.formatters;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author dseleznev
 * Created on: 28.10.2010
 */
public class PensionPolicyFormatter implements IFormatter
{
    @Override
    public String format(Object source)
    {
        if (null == source) return null;

        String src = (String)source;
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < src.length(); i++)
        {
            char ch = src.charAt(i);
            if (StringUtils.isNumeric(String.valueOf(ch))) result.append(ch);
            if (result.length() == 3 || result.length() == 7 || result.length() == 11) result.append("-");
        }

        return result.toString();
    }
}