/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.ui.EducationLevelsAutocompleteModel;

/**
 * @author vip_delete
 */
public abstract class DAO<T extends EducationLevels, M extends Model<T>> extends DefaultCatalogAddEditDAO<T, M>
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final M model)
    {
        super.prepare(model);

        model.setEducationLevelListModel(new EducationLevelsAutocompleteModel()
        {
            @SuppressWarnings("unchecked")
            @Override
            protected List<EducationLevels> getFilteredList()
            {
                if (model.getCatalogItem().getLevelType() == null) return Collections.emptyList();

                Criteria c = getSession().createCriteria(EducationLevels.ENTITY_CLASS);
                c.add(Restrictions.eq(EducationLevels.L_LEVEL_TYPE, model.getCatalogItem().getLevelType().getParent()));
                return c.list();
            }

            @Override
            @SuppressWarnings("unchecked")
            protected boolean checkLevel(Pattern pattern, EducationLevels educationLevel)
            {
                return (
                pattern.matcher(StringUtils.trimToEmpty(educationLevel.getDisplayableTitle()).toUpperCase()).find() ||
                pattern.matcher(StringUtils.trimToEmpty(educationLevel.getTitleCodePrefix()).toUpperCase()).find() ||
                pattern.matcher(StringUtils.trimToEmpty(educationLevel.getInheritedOkso()).toUpperCase()).find()
                );
            }
        });
    }

    protected final Criteria getUniqueCheckingCriteria(EducationLevels level)
    {
        Criteria crit = getSession().createCriteria(EducationLevels.class);
        crit.add(Restrictions.eq(EducationLevels.L_LEVEL_TYPE, level.getLevelType()));
        crit.add(Restrictions.eq(EducationLevels.P_CATALOG_CODE, level.getCatalogCode()));
        if (null != level.getId())
            crit.add(Restrictions.ne(EducationLevels.P_ID, level.getId()));
        return crit;
    }

    @Override
    @SuppressWarnings("deprecation")
    public void validate(M model, ErrorCollector errors)
    {
        super.validate(model, errors);

        String okso = model.getCatalogItem().getOkso(); // здесь проверка на само поле ОКСО - это правильно
        if (null != okso) {
            Criteria crit = getUniqueCheckingCriteria(model.getCatalogItem());
            crit.add(Restrictions.eq(EducationLevels.P_OKSO, okso));
            if (!crit.list().isEmpty())
                errors.add(getOksoDuplicateErrorString(), "okso");
        }
    }

    protected String getOksoDuplicateErrorString()
    {
        return "Введенный ОКСО уже указан для другого направления подготовки (специальности)";
    }
}