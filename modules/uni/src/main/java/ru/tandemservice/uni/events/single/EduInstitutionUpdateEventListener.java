/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.events.single;

import org.hibernate.Query;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateUpdateListener;
import org.tandemframework.hibsupport.event.single.type.HibernateUpdateEvent;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;

/**
 * @author vip_delete
 * @since 12.04.2009
 */
public class EduInstitutionUpdateEventListener extends FilteredSingleEntityEventListener<HibernateUpdateEvent> implements IHibernateUpdateListener
{
    @SuppressWarnings("deprecation")
    @Override
    public void onFilteredEvent(HibernateUpdateEvent event)
    {
        EduInstitution eduInstitution = (EduInstitution) event.getEntity();

        Query query = event.getSession().createQuery(
            " update " +  org.tandemframework.shared.person.base.entity.PersonEduInstitution.ENTITY_CLASS + " " +
            " set " +
                org.tandemframework.shared.person.base.entity.PersonEduInstitution.L_ADDRESS_ITEM + "=:addressItem, " +
                org.tandemframework.shared.person.base.entity.PersonEduInstitution.L_EDU_INSTITUTION_KIND + "=:eduInstitutionKind " +
            " where " +  org.tandemframework.shared.person.base.entity.PersonEduInstitution.L_EDU_INSTITUTION + "=:eduInstitution");
        query.setParameter("addressItem", eduInstitution.getAddress().getSettlement());
        query.setParameter("eduInstitutionKind", eduInstitution.getEduInstitutionKind());
        query.setParameter("eduInstitution", eduInstitution);
        query.executeUpdate();
    }
}
