package ru.tandemservice.uni.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.IOException;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface IPersonIODao {

    SpringBeanCache<IPersonIODao> instance = new SpringBeanCache<>(IPersonIODao.class.getName());

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_PersonList(Database mdb, boolean full) throws IOException;

    /** @return { mdb.person_id -> person.id } */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<String, Long> doImport_PersonList(Database mdb) throws IOException;

    /** @return { mdb.personEduDocument_id -> personEduDocument.id } */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<String, Long> doImport_PersonEduDocument(final Database mdb, final Map<String, Long> personIdMap) throws IOException;
}
