/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub;

import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
@State(keys = "firstId", bindings = "firstId")
public class AbstractRelationPubModel
{
    private long _firstId;
    private IEntity _first;

    @SuppressWarnings({"unchecked"})
    private DynamicListDataSource _dataSource;

    public long getFirstId()
    {
        return _firstId;
    }

    public void setFirstId(long firstId)
    {
        _firstId = firstId;
    }

    public IEntity getFirst()
    {
        return _first;
    }

    public void setFirst(IEntity first)
    {
        _first = first;
    }

    @SuppressWarnings({"unchecked"})
    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    @SuppressWarnings({"unchecked"})
    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }
}
