/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.EntityDebug.CommonEntityDebug;
import ru.tandemservice.uni.base.bo.UniSystemAction.logic.BaseQualificationCheckDataSourceResolver;
import ru.tandemservice.uni.base.bo.UniSystemAction.logic.IUniSystemActionDao;
import ru.tandemservice.uni.base.bo.UniSystemAction.logic.UniSystemActionDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class UniSystemActionManager extends BusinessObjectManager
{
    public static UniSystemActionManager instance()
    {
        return instance(UniSystemActionManager.class);
    }

    @Bean
    public IUniSystemActionDao dao()
    {
        return new UniSystemActionDao();
    }
}
