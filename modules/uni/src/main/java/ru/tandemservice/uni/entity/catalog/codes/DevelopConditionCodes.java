package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Условие освоения"
 * Имя сущности : developCondition
 * Файл data.xml : uni.education.data.xml
 */
public interface DevelopConditionCodes
{
    /** Константа кода (code) элемента : Полный срок (title) */
    String FULL_PERIOD = "1";
    /** Константа кода (code) элемента : Сокращенная программа (title) */
    String REDUCED_PERIOD = "2";
    /** Константа кода (code) элемента : Ускоренная программа (title) */
    String ACCELERATED_PERIOD = "3";
    /** Константа кода (code) элемента : Сокращенная ускоренная программа (title) */
    String REDUCED_ACCELERATED_PERIOD = "4";

    Set<String> CODES = ImmutableSet.of(FULL_PERIOD, REDUCED_PERIOD, ACCELERATED_PERIOD, REDUCED_ACCELERATED_PERIOD);
}
