/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.CaptainStudentAssign;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;

import java.util.List;
import java.util.Set;

/**
 * @author dseleznev
 * @since : 22.01.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setGroup(get(Group.class, model.getGroup().getId()));
        model.setCaptainStudentList(UniDaoFacade.getGroupDao().getCaptainStudentList(model.getGroup()));
        model.setCaptainStudentOldList(model.getCaptainStudentList());

        model.setStudentListModel(new BaseMultiSelectModel()
        {
            private final String STUDENT_IDENTITY_CARD = Student.L_PERSON + "." + Person.L_IDENTITY_CARD;
            private final String STUDENT_LAST_NAME = STUDENT_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME;
            private final String STUDENT_FIRST_NAME = STUDENT_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME;
            private final String STUDENT_MIDDLE_NAME = STUDENT_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME;
            private final String STUDENT_GROUP_TITLE = Student.L_GROUP + "." + Group.P_TITLE;

            private final String[] FILTER_PROPERTIES = new String[]{STUDENT_LAST_NAME, STUDENT_FIRST_NAME, STUDENT_MIDDLE_NAME, STUDENT_GROUP_TITLE};

            @Override
            public ListResult findValues(String filter)
            {
                String[] filterParts = StringUtils.split(filter);

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, "s")
                        .where(DQLExpressions.eq(DQLExpressions.property(Student.archival().fromAlias("s")), DQLExpressions.value(Boolean.FALSE)))
                        .where(DQLExpressions.eq(DQLExpressions.property(Student.status().active().fromAlias("s")), DQLExpressions.value(Boolean.TRUE)));

                if (model.isAnyGroupStudent())
                {

                    builder
                            .where(DQLExpressions.isNotNull(DQLExpressions.property(Student.group().fromAlias("s"))))
                            .where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("s")), DQLExpressions.value(model.getGroup().getEducationOrgUnit().getFormativeOrgUnit())))
                            .where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().territorialOrgUnit().fromAlias("s")), DQLExpressions.value(model.getGroup().getEducationOrgUnit().getTerritorialOrgUnit())));

                } else
                {
                    builder.where(DQLExpressions.eq(DQLExpressions.property(Student.group().fromAlias("s")), DQLExpressions.value(model.getGroup())));
                }

                // фильтрация
                for (int i = 0; i < Math.min(filterParts.length, FILTER_PROPERTIES.length); i++)
                {
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(("s." + FILTER_PROPERTIES[i]))), DQLExpressions.value(CoreStringUtils.escapeLike(filterParts[i]))));
                }

                // упорядочивание
                for (String property : FILTER_PROPERTIES)
                {
                    builder.order(DQLExpressions.property("s", property));
                }

                int rowsNumber = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (rowsNumber > UniDao.MAX_ROWS)
                {
                    builder.top(MAX_ROWS);
                }

                return new ListResult<>(builder.createStatement(getSession()).<Student>list(), rowsNumber);
            }

            @SuppressWarnings("unchecked")
            @Override
            public List getValues(Set primaryKeys)
            {
                return getList(Student.class, primaryKeys);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                Student student = (Student) value;
                // DEV-4515
                return student.getPerson().getIdentityCard().getFullFio() + (student.getGroup() == null ? "" : (!student.getGroup().getId().equals(model.getGroup().getId()) ? " (Группа: " + student.getGroup().getTitle() + ")" : ""));
            }
        });
    }

    public void countCaptainStudentValidate(Model model)
    {
        // DEV-4545
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(model.getGroup());
        if (model.getCaptainStudentOldList().size() != captainList.size())
            throw new ApplicationException("Староста уже назначен другим пользователем.");

        if (model.getCaptainStudentList().size() > 1)
            throw new ApplicationException("В группе может быть только один староста.");
    }

    @Override
    public void update(Model model)
    {
        countCaptainStudentValidate(model);

        for (Student student : model.getCaptainStudentOldList())
        {
            if (!model.getCaptainStudentList().contains(student))
            {
                UniDaoFacade.getGroupDao().deleteCaptainStudent(model.getGroup(), student);
            }
        }

        if (!model.getCaptainStudentList().isEmpty())
        {
            for(Student student : model.getCaptainStudentList())
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupCaptainStudent.class, "s")
                        .where(DQLExpressions.eq(DQLExpressions.property("s", GroupCaptainStudent.student()), DQLExpressions.value(student)))
                        .where(DQLExpressions.eq(DQLExpressions.property("s", GroupCaptainStudent.group()), DQLExpressions.value(model.getGroup())));

                GroupCaptainStudent groupCaptainStudent = builder.createStatement(getSession()).uniqueResult();

                if (null == groupCaptainStudent) groupCaptainStudent = new GroupCaptainStudent();

                groupCaptainStudent.setGroup(model.getGroup());
                groupCaptainStudent.setStudent(student);
                saveOrUpdate(groupCaptainStudent);
            }
        }
    }
}
