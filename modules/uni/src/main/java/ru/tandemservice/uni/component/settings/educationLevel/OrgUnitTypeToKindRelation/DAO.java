/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitTypeToKindRelation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        //orgUnitType.id -> List of orgUnitKind.code
        Map<Long, Set<String>> map = new HashMap<Long, Set<String>>();
        for (OrgUnitTypeToKindRelation relation : getList(OrgUnitTypeToKindRelation.class))
        {
            Set<String> arr = map.get(relation.getOrgUnitType().getId());
            if (arr == null)
            {
                arr = new HashSet<String>();
                map.put(relation.getOrgUnitType().getId(), arr);
            }
            arr.add(relation.getOrgUnitKind().getCode());
        }

        List<OrgUnitType> list = new ArrayList<OrgUnitType>();
        for (OrgUnitType orgUnitType : getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            if (orgUnitType.isEnabled())
            {
                Set<String> set = map.get(orgUnitType.getId());

                list.add(new OrgUnitTypeViewObject(orgUnitType, set != null && set.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING), set != null && set.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING), set != null && set.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)));
            }
        }

        DynamicListDataSource<OrgUnitType> dataSource = model.getDataSource();
        dataSource.setTotalSize(list.size());
        dataSource.setCountRow(list.size());
        dataSource.createPage(list);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void updateRelationByToggle(String kindCode, Long orgUnitTypeId)
    {
        //get current value
        OrgUnitType orgUnitType = get(OrgUnitType.class, orgUnitTypeId);

        final Criteria c = getSession().createCriteria(OrgUnitTypeToKindRelation.class);
        c.add(Restrictions.eq(OrgUnitTypeToKindRelation.L_ORG_UNIT_TYPE, orgUnitType));
        List<OrgUnitTypeToKindRelation> relationList = c.list();

        int i = 0;
        while (i < relationList.size() && !relationList.get(i).getOrgUnitKind().getCode().equals(kindCode)) i++;
        if (i < relationList.size())
        {
            //связь существует, ее надо удалить
            delete(relationList.get(i));
        } else
        {
            //связи нет, ее надо создать
            OrgUnitTypeToKindRelation relation = new OrgUnitTypeToKindRelation();
            relation.setOrgUnitKind(getCatalogItem(OrgUnitKind.class, kindCode));
            relation.setOrgUnitType(orgUnitType);
            save(relation);
        }
    }
}
