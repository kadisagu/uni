/**
 * $Id$
 */
package ru.tandemservice.uni.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Collection;

/**
 * @author dseleznev
 * Created on: 18.03.2010
 *
 * @deprecated
 */
public class EntityUtil
{
    /**
     * Проверяет наличие объектов, ссылающиеся на указанный объект, которые не удаляются каскадно.
     * Есть возможность формирования списка объектов, которые не нужно учитывать (на случай, 
     * когда эти объекты обрабатываются совместно с основным). <p/>
     * @param entityId - Id объекта типа Long, для которого необходимо проверить наличие ссылок на него.
     * @param excludeOjectIds - коллекция Id объектов, ссылки с которых на указанный объект не нужно учитывать.
     * @return true, если на указанный объект не ссылаются какие-либо объекты (кроме указанных в параметре excludeObjectIds), 
     * не помеченные, как удаляемые каскадно. False, если такие объекты найдены.
     * @deprecated use DQLCanDeleteExpressionBuilder
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    @Zlo
    public static boolean isEntityCanBeDeleted(Long entityId, Collection<Long> excludeOjectIds)
    {
        IEntity entity = UniDaoFacade.getCoreDao().get(entityId);
        for(IRelationMeta relMeta : EntityRuntime.getMeta(entityId).getIncomingRelations())
        {
            if(!"delete".equals(relMeta.getCascade().getStyle("delete", false)))
            {
                for(Object refObject : UniDaoFacade.getCoreDao().getList(relMeta.getForeignEntity().getEntityClass(), relMeta.getForeignPropertyName(), entity))
                {
                    IEntity refEntity = (IEntity) refObject;
                    if(!excludeOjectIds.contains(refEntity.getId())) return false;
                }
            }
        }
        
        return true;
    }
}