package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import ru.tandemservice.uni.entity.catalog.YearDistribution;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть учебного года
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class YearDistributionPartGen extends EntityBase
 implements IDeclinable, INaturalIdentifiable<YearDistributionPartGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.YearDistributionPart";
    public static final String ENTITY_NAME = "yearDistributionPart";
    public static final int VERSION_HASH = -835079428;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_YEAR_DISTRIBUTION = "yearDistribution";
    public static final String P_NUMBER = "number";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE_WITH_PARENT = "shortTitleWithParent";
    public static final String P_TITLE_WITH_PARENT = "titleWithParent";

    private String _code;     // Системный код
    private YearDistribution _yearDistribution;     // Разбиение
    private int _number;     // Номер
    private String _shortTitle;     // Сокращенное название
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Разбиение. Свойство не может быть null.
     */
    @NotNull
    public YearDistribution getYearDistribution()
    {
        return _yearDistribution;
    }

    /**
     * @param yearDistribution Разбиение. Свойство не может быть null.
     */
    public void setYearDistribution(YearDistribution yearDistribution)
    {
        dirty(_yearDistribution, yearDistribution);
        _yearDistribution = yearDistribution;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof YearDistributionPartGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((YearDistributionPart)another).getCode());
            }
            setYearDistribution(((YearDistributionPart)another).getYearDistribution());
            setNumber(((YearDistributionPart)another).getNumber());
            setShortTitle(((YearDistributionPart)another).getShortTitle());
            setTitle(((YearDistributionPart)another).getTitle());
        }
    }

    public INaturalId<YearDistributionPartGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<YearDistributionPartGen>
    {
        private static final String PROXY_NAME = "YearDistributionPartNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof YearDistributionPartGen.NaturalId) ) return false;

            YearDistributionPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends YearDistributionPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) YearDistributionPart.class;
        }

        public T newInstance()
        {
            return (T) new YearDistributionPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "yearDistribution":
                    return obj.getYearDistribution();
                case "number":
                    return obj.getNumber();
                case "shortTitle":
                    return obj.getShortTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "yearDistribution":
                    obj.setYearDistribution((YearDistribution) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "yearDistribution":
                        return true;
                case "number":
                        return true;
                case "shortTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "yearDistribution":
                    return true;
                case "number":
                    return true;
                case "shortTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "yearDistribution":
                    return YearDistribution.class;
                case "number":
                    return Integer.class;
                case "shortTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<YearDistributionPart> _dslPath = new Path<YearDistributionPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "YearDistributionPart");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Разбиение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getYearDistribution()
     */
    public static YearDistribution.Path<YearDistribution> yearDistribution()
    {
        return _dslPath.yearDistribution();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getShortTitleWithParent()
     */
    public static SupportedPropertyPath<String> shortTitleWithParent()
    {
        return _dslPath.shortTitleWithParent();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getTitleWithParent()
     */
    public static SupportedPropertyPath<String> titleWithParent()
    {
        return _dslPath.titleWithParent();
    }

    public static class Path<E extends YearDistributionPart> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private YearDistribution.Path<YearDistribution> _yearDistribution;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _shortTitleWithParent;
        private SupportedPropertyPath<String> _titleWithParent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(YearDistributionPartGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Разбиение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getYearDistribution()
     */
        public YearDistribution.Path<YearDistribution> yearDistribution()
        {
            if(_yearDistribution == null )
                _yearDistribution = new YearDistribution.Path<YearDistribution>(L_YEAR_DISTRIBUTION, this);
            return _yearDistribution;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(YearDistributionPartGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(YearDistributionPartGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(YearDistributionPartGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getShortTitleWithParent()
     */
        public SupportedPropertyPath<String> shortTitleWithParent()
        {
            if(_shortTitleWithParent == null )
                _shortTitleWithParent = new SupportedPropertyPath<String>(YearDistributionPartGen.P_SHORT_TITLE_WITH_PARENT, this);
            return _shortTitleWithParent;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.YearDistributionPart#getTitleWithParent()
     */
        public SupportedPropertyPath<String> titleWithParent()
        {
            if(_titleWithParent == null )
                _titleWithParent = new SupportedPropertyPath<String>(YearDistributionPartGen.P_TITLE_WITH_PARENT, this);
            return _titleWithParent;
        }

        public Class getEntityClass()
        {
            return YearDistributionPart.class;
        }

        public String getEntityName()
        {
            return "yearDistributionPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getShortTitleWithParent();

    public abstract String getTitleWithParent();
}
