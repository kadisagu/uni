package ru.tandemservice.uni.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Квалификация по диплому"
 * Имя сущности : diplomaQualifications
 * Файл data.xml : uni.education.data.xml
 */
public interface DiplomaQualificationsCodes
{
    /** Константа кода (code) элемента : Академический бакалавр (title) */
    String ACADEMIC_BACHELOR = "academ_bach";
    /** Константа кода (code) элемента : Прикладной бакалавр (title) */
    String APPLIED_BACHELOR = "app_bach";

    Set<String> CODES = ImmutableSet.of(ACADEMIC_BACHELOR, APPLIED_BACHELOR);
}
