/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.entity.employee;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class Student extends StudentGen implements ITitled, ISecLocalEntityOwner
{
    public static final Comparator<Student> FULL_FIO_AND_ID_COMPARATOR = CommonCollator.comparing(Student::getFullFio, true);

    public static final Object FIO_KEY = new String[]{Student.L_PERSON, Person.P_FULLFIO};
    public static final Object PASSPORT_KEY = new String[]{Student.L_PERSON, Person.P_FULL_IDCARD_NUMBER};
    public static final Object FORMATIVE_ORGUNIT_KEY = new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE};
    public static final Object TERRITORIAL_ORGUNIT_KEY = new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE};
    public static final Object PRODUCTIVE_ORGUNIT_KEY = new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_FULL_TITLE};
    public static final Object STATUS_KEY = new String[]{Student.L_STATUS, StudentStatus.P_TITLE};
    public static final String STUDENT_LAST_NAME = Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME;
    public static final String STUDENT_FIRST_NAME = Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME;
    public static final String STUDENT_MIDDLE_NAME = Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME;

    public static final String P_FULL_FIO_WITH_BIRTH_YEAR_GROUP_AND_STATUS = "studentFullFioWithBirthYearGroupAndStatus";

    @Override
    public String getLocalRoleContextFullTitle()
    {
        return getTitle();
    }

    public String getPerNumber()
    {
        return getPersonalNumber();
    }

    @Override
    public String getTitle()
    {
        if (getPerson() == null) {
            return this.getClass().getSimpleName();
        }
        return getPerson().getFullFio() + " : студент";
    }

    /**
     * [ФИО студента] ([состояние], гр. [академ. группа студента], [курс студента] курс, [название направления НПП студента])
     *
     * @return формат студента с ФИО
     */
    @Override
    @EntityDSLSupport
    public String getTitleWithFio()
    {
        return getPerson().getIdentityCard().getFullFio() + " (" + getTitleWithoutFio() + ")";
    }

    /**
     * [состояние], гр. [академ. группа студента], [курс студента] курс, [название направления НПП студента]
     *
     * @return формат студента без ФИО
     */
    @Override
    @EntityDSLSupport
    public String getTitleWithoutFio()
    {
        return getStatus().getTitle() + (getGroup() == null ? "" : ", гр. " + getGroup().getTitle()) + ", " + getCourse().getTitle() + " курс, " + getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle();
    }

    @Override
    public String getFullTitle()
    {
        StringBuilder str = new StringBuilder("Студент: ");
        str.append(getPerson().getFullFio());
        if (getStatus() != null)
            str.append(" (").append(getStatus().getTitle()).append(")");
        if (null != getGroup())
            str.append(" | Группа: ").append(getGroup().getTitle());
        str.append(" | ").append(getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        return str.toString();
    }

    @Override
    public String getContextTitle()
    {
        StringBuilder str = new StringBuilder("Студент: ");
        if (getStatus() != null)
            str.append(" (").append(getStatus().getTitle()).append(")");
        if (null != getGroup())
            str.append(" | Группа: ").append(getGroup().getTitle());
        str.append(" | ").append(getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        return str.toString();
    }

    /**
     * Студент: <ФИО> (<состояние>, <Форма освоения>, <вид возм. затрат>) | Группа: <Название группы> | <Сокр название подразделения [(сокр название филиала)]> | <Название направления подготовки>",
     *
     * @return расширенный стикер для студента
     */
    public String getFullTitleExtended()
    {
        return IStudentDAO.instance.get().getFullTitleExtended(this);
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        final List<IEntity> result = new ArrayList<>();
        result.add(getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit());
        result.add(getEducationOrgUnit().getFormativeOrgUnit());
        result.add(getEducationOrgUnit().getTerritorialOrgUnit());
        return result;
    }

    @EntityDSLSupport
    public String getStudentFullFioWithBirthYearGroupAndStatus()
    {
        StringBuilder builder = new StringBuilder(getPerson().getFullFio());
        if (null != getPerson().getIdentityCard().getBirthDate()) {
            builder.append(" ");
            builder.append(RussianDateFormatUtils.getYearString(getPerson().getIdentityCard().getBirthDate(), false));
            builder.append(" г.р.");
        }

        if (null != getGroup()) builder.append(", гр. ").append(getGroup().getTitle());

        builder.append(" (").append(getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
        builder.append(", ").append(getStatus().getTitle().toLowerCase()).append(")");

        return builder.toString();
    }
}