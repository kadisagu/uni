package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

@State( { @Bind(key = "orgUnitId", binding = "orgUnitId"), @Bind(key = "orgUnitStudentSelectedTab", binding = "orgUnitStudentSelectedTab") })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private String _orgUnitStudentSelectedTab;
    private CommonPostfixPermissionModel _secModel;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public String getOrgUnitStudentSelectedTab()
    {
        return _orgUnitStudentSelectedTab;
    }

    public void setOrgUnitStudentSelectedTab(String orgUnitStudentSelectedTab)
    {
        _orgUnitStudentSelectedTab = orgUnitStudentSelectedTab;
    }

    public boolean isAllowGroups()
    {
        return UniDaoFacade.getOrgstructDao().isAllowGroups(getOrgUnit());
    }

    public boolean isAllowStudents()
    {
        return UniDaoFacade.getOrgstructDao().isAllowStudents(getOrgUnit());
    }

    public String getOrgUnitViewStudentTab()
    {
        return "orgUnit_viewStudentTab_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getOrgUnitViewStudentDocumentTab()
    {
        return "orgUnit_viewStudentDocumentTab_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getOrgUnitViewArchivalStudentTab()
    {
        return "orgUnit_viewArchivalStudentTab_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getOrgUnitViewGroupTab()
    {
        return "orgUnit_viewTab_" + getOrgUnit().getOrgUnitType().getCode() + "_group";
    }

    public String getOrgUnitViewArchivalGroupTab()
    {
        return "orgUnit_viewArchivalTab_" + getOrgUnit().getOrgUnitType().getCode() + "_group";
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}