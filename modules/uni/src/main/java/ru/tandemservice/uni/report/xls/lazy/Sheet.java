/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.report.xls.lazy;

import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;

/**
 * @author oleyba
 * @since 10.04.2009
 */
class Sheet
{
    WritableSheet _writableSheet;
    LazyCell[][] _cells;
    ILazyModifier _modifier;

    Sheet(WritableSheet writableSheet, LazyCell[][] cells)
    {
        _writableSheet = writableSheet;
        _cells = cells;
    }

    void printAll(CellFormatFactory factory) throws Exception
    {
        for (int row = 0; row < _cells.length; row++)
        {
            LazyCell[] rowCells = _cells[row];
            for (int col = 0; col < rowCells.length; col++)
            {
                LazyCell cell = rowCells[col];
                if (cell != null)
                {
                    WritableCellFormat format = factory.getFormat(cell._format);
                    Label label = new Label(col, row, cell._content, format);
                    _writableSheet.addCell(label);
                }
                else
                {
                    _writableSheet.addCell(new Blank(col, row, factory.defaultFormat));
                }
            }
        }
        if (_modifier != null)
        {
            _modifier.apply(_writableSheet);
        }
    }

}
