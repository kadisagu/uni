/* $Id:$ */
package ru.tandemservice.uni.migration;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.IResultSetExt;
import org.tandemframework.dbsupport.ddl.ResultRowProcessor;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

/**
 * @author rsizonenko
 * @since 18.06.2015
 */
public class MS_uni_2x8x1_1to2  extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        final List<Object[]> objects = tool.executeQuery(
                new ResultRowProcessor<List<Object[]>>(new ArrayList<Object[]>()) {
                    @Override
                    protected void processRow(IDBConnect ctool, ResultSet rs, List<Object[]> result) throws SQLException {
                        IResultSetExt extRS = (IResultSetExt) rs;
                        Blob b = extRS.getBlob(1);
                        String code = extRS.getString(2);

                        if (b != null)
                            result.add(new Object[]{b.getBytes(1, (int) b.length()), code});
                        else
                            result.add(new Object[]{null, code});
                    }
                },
                "select document_p, code_p from templatedocument_t where code_p in (select s.code_p from uni_c_script_t as u inner join scriptitem_t as s on s.id = u.id)");

        Map<String, byte[]> map = new HashMap<>();
        for (Object[] object : objects) {
            byte[] bytes = (byte[])object[0];
            if (bytes == null) continue;
            ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(bytes));
            zip.getNextEntry();
            map.put((String)object[1], IOUtils.toByteArray(zip));

        }


        final PreparedStatement preparedStatement = tool.prepareStatement("update scriptitem_t set usertemplate_p = ? where code_p = ?");

        for (Map.Entry<String, byte[]> stringEntry : map.entrySet()) {

            if (stringEntry.getKey().equals("studentPersonCard"))
                continue;

            preparedStatement.clearParameters();

            final byte[] bytes = stringEntry.getValue();

            preparedStatement.setBlob(1, new Blob() {
                @Override
                public long length() throws SQLException {
                    return bytes.length;
                }

                @Override
                public byte[] getBytes(long pos, int length) throws SQLException {
                    return ArrayUtils.subarray(bytes, length, (int) (length + pos));
                }

                @Override
                public InputStream getBinaryStream() throws SQLException {
                    return new ByteArrayInputStream(bytes);
                }

                @Override
                public long position(byte[] pattern, long start) throws SQLException {
                    return 0;
                }

                @Override
                public long position(Blob pattern, long start) throws SQLException {
                    return 0;
                }

                @Override
                public int setBytes(long pos, byte[] bytes) throws SQLException {
                    return 0;
                }

                @Override
                public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
                    return 0;
                }

                @Override
                public OutputStream setBinaryStream(long pos) throws SQLException {
                    return null;
                }

                @Override
                public void truncate(long len) throws SQLException {
                }

                @Override
                public void free() throws SQLException {
                }

                @Override
                public InputStream getBinaryStream(long pos, long length) throws SQLException {
                    return new ByteArrayInputStream(bytes, (int) pos, (int) length);
                }
            });

            preparedStatement.setString(2, stringEntry.getKey());
            preparedStatement.executeUpdate();

        }

        tool.executeUpdate("delete from templatedocument_t where code_p in (select code_p from uni_c_script_t as u inner join scriptitem_t as s on s.id = u.id) and code_p != 'studentPersonCard'");
    }
}
