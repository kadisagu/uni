package ru.tandemservice.uni.base.ui;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.sec.OrgUnitHolder;

/**
 * @author vdanilov
 */
@State({
    @Bind(key=OrgUnitUIPresenter.INPUT_PARAM_ORG_UNIT_ID, binding="orgUnitHolder.id", required=false)
})
public abstract class OrgUnitUIPresenter extends UIPresenter
{
    public static final String INPUT_PARAM_ORG_UNIT_ID = "orgUnitId";

    @Override
    public String getSettingsKey()
    {
        final OrgUnit orgUnit = getOrgUnit();
        if (orgUnit != null)
            return getConfig().getName() + "." + String.valueOf(FastBeanUtils.getValue(this, "orgUnitHolder.id"));

        return super.getSettingsKey() + ".";
    }

    @Override public ISecured getSecuredObject() {
        ISecured securedObject = this.getOrgUnit();
        if (securedObject == null) {
            securedObject = super.getSecuredObject();
        }
        return securedObject;
    }

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder() {
        @Override protected String getSecModelPostfix() {
            return OrgUnitUIPresenter.this.getSecModelPostfix();
        }
    };

    protected String getSecModelPostfix() {
        final OrgUnit orgUnit = getOrgUnit();
        if (null == orgUnit) { return null; }
        return OrgUnitSecModel.getPostfix(orgUnit);
    }

    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }

    public CommonPostfixPermissionModelBase getSec() { return this.getOrgUnitHolder().getSecModel(); }
}
