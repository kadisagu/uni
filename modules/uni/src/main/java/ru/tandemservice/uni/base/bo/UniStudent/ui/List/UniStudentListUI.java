/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniStudent.ui.List;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
public class UniStudentListUI extends AbstractUniStudentListUI
{
    @Override
    public void onComponentRefresh()
    {
        setShowDevelopTechFilter(true);
        setHideUnusedEducationOrgUnit(true);
        super.onComponentRefresh();
    }

    protected IEducationOrgUnitContextHandler getContextHandler()
    {
        return new BaseStudentListContextHandler(false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(UniStudentManger.GROUP_DS))
        {
            dataSource.put("archival", false);
        }
    }

    @Override
    public String getSettingsKey()
    {
        return "StudentList.filter";
    }

    public void onClickEditStudent()
    {
        getActivationBuilder().asCurrent(IUniComponents.STUDENT_EDIT)
                .parameter("studentId", getListenerParameter())
                .activate();
    }

    public void onClickDeleteStudent()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        getConfig().<BaseSearchListDataSource>getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).getLegacyDataSource().refresh();
    }

    public void onClickPrintStudentPersonalCard()
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled()) {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_PRINT_SCRIPT);
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, getListenerParameterAsLong());
        }
        else {
            getActivationBuilder().asDesktopRoot(IUniComponents.STUDENT_PERSON_CARD)
                    .parameter("studentId", getListenerParameter())
                    .activate();
        }
    }
}
