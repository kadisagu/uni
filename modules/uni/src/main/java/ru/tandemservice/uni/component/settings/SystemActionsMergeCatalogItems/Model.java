// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsMergeCatalogItems;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.List;

/**
 * @author oleyba
 * @since 23.05.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "todo.id")
public class Model
{
    public static final Long SPORT_TYPE_CODE = 0L;
    public static final Long SPORT_RANK_CODE = 1L;
    public static final Long ADDRESS_COUNTRY_CODE = 2L;
    public static final Long COUNTRY_CODE = 2L;

    // «Виды спорта», «Спортивные разряды», «Страны»
    private List<IdentifiableWrapper> catalogList = ImmutableList.of(new IdentifiableWrapper(SPORT_TYPE_CODE, "Виды спорта"),
                                                                     new IdentifiableWrapper(SPORT_RANK_CODE, "Спортивные разряды"),
                                                                     new IdentifiableWrapper(ADDRESS_COUNTRY_CODE, "Страны"));
    private IdentifiableWrapper catalog;

    private ISelectModel sportTypeModel;
    private ISelectModel sportRankModel;
    private ISelectModel addressCountryModel;

    private DynamicListDataSource<IEntity> dataSource;
    private BlockColumn<IEntity> newItemColumn;

    public ISelectModel getCatalogItemModel()
    {
        if (null == getCatalog()) return new LazySimpleSelectModel<>(ImmutableList.of());
        if (getCatalog().getId().equals(SPORT_TYPE_CODE))
            return getSportTypeModel();
        if (getCatalog().getId().equals(SPORT_RANK_CODE))
            return getSportRankModel();
        if (getCatalog().getId().equals(ADDRESS_COUNTRY_CODE))
            return getAddressCountryModel();
        return new LazySimpleSelectModel<>(ImmutableList.of());
    }


    public List<IdentifiableWrapper> getCatalogList()
    {
        return catalogList;
    }

    public void setCatalogList(List<IdentifiableWrapper> catalogList)
    {
        this.catalogList = catalogList;
    }

    public IdentifiableWrapper getCatalog()
    {
        return catalog;
    }

    public void setCatalog(IdentifiableWrapper catalog)
    {
        this.catalog = catalog;
    }

    public ISelectModel getSportTypeModel()
    {
        return sportTypeModel;
    }

    public void setSportTypeModel(ISelectModel sportTypeModel)
    {
        this.sportTypeModel = sportTypeModel;
    }

    public ISelectModel getSportRankModel()
    {
        return sportRankModel;
    }

    public void setSportRankModel(ISelectModel sportRankModel)
    {
        this.sportRankModel = sportRankModel;
    }

    public ISelectModel getAddressCountryModel()
    {
        return addressCountryModel;
    }

    public void setAddressCountryModel(ISelectModel addressCountryModel)
    {
        this.addressCountryModel = addressCountryModel;
    }

    public DynamicListDataSource<IEntity> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<IEntity> dataSource)
    {
        this.dataSource = dataSource;
    }

    public BlockColumn<IEntity> getNewItemColumn()
    {
        return newItemColumn;
    }

    public void setNewItemColumn(BlockColumn<IEntity> newItemColumn)
    {
        this.newItemColumn = newItemColumn;
    }
}
