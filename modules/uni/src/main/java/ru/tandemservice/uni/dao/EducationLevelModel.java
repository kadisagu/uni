package ru.tandemservice.uni.dao;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;

import java.util.Collections;
import java.util.Map;

public class EducationLevelModel implements IEducationLevelModel {

    /**
     * поскольку в силу исторических причин название одного из свойств на одну букву отличается от аналогичного свойства для объекта
     * пришлось ввести такой справочник для преобразования имен (пропертя объекта -> пропертя модели)
     **/
    public static final Map<String, String> TRANSLATIONS = ImmutableMap.<String, String>builder()
            .put(EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT, "formativeOrgUnit")
            .put(EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT, "territorialOrgUnit")
            .put(EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelsHighSchool") // здесь есть лишняя 's'
            .put(EducationOrgUnitGen.L_DEVELOP_FORM, "developForm")
            .put(EducationOrgUnitGen.L_DEVELOP_CONDITION, "developCondition")
            .put(EducationOrgUnitGen.L_DEVELOP_TECH, "developTech")
            .put(EducationOrgUnitGen.L_DEVELOP_PERIOD, "developPeriod")
            .build();

    public void fromEducationOrgUnit(final EducationOrgUnit edu, final boolean clearSelectionModel) {
        this.setFormativeOrgUnit(edu.getFormativeOrgUnit());
        this.setTerritorialOrgUnit(edu.getTerritorialOrgUnit());
        this.setEducationLevelsHighSchool(edu.getEducationLevelHighSchool());
        this.setDevelopForm(edu.getDevelopForm());
        this.setDevelopCondition(edu.getDevelopCondition());
        this.setDevelopTech(edu.getDevelopTech());
        this.setDevelopPeriod(edu.getDevelopPeriod());

        if (clearSelectionModel) {
            this.setFormativeOrgUnitModel(new StaticSelectModel("id", OrgUnit.P_TITLE, Collections.singletonList(this.getFormativeOrgUnit())));
            this.setTerritorialOrgUnitModel(new StaticSelectModel("id", OrgUnit.P_TERRITORIAL_TITLE, Collections.singletonList(this.getTerritorialOrgUnit())));
            this.setEducationLevelsHighSchoolModel(new StaticSelectModel("id", "title", Collections.singletonList(this.getEducationLevelsHighSchool())));
            this.setDevelopFormModel(new StaticSelectModel("id", "title", Collections.singletonList(this.getDevelopForm())));
            this.setDevelopConditionModel(new StaticSelectModel("id", "title", Collections.singletonList(this.getDevelopCondition())));
            this.setDevelopTechModel(new StaticSelectModel("id", "title", Collections.singletonList(this.getDevelopTech())));
            this.setDevelopPeriodModel(new StaticSelectModel("id", "title", Collections.singletonList(this.getDevelopPeriod())));
        }
    }

    private ISelectModel _formativeOrgUnitModel;
    public ISelectModel getFormativeOrgUnitModel() { return _formativeOrgUnitModel; }
    public void setFormativeOrgUnitModel(final ISelectModel formativeOrgUnitModel) { _formativeOrgUnitModel = formativeOrgUnitModel; }

    private ISelectModel _territorialOrgUnitModel;
    public ISelectModel getTerritorialOrgUnitModel() { return _territorialOrgUnitModel; }
    public void setTerritorialOrgUnitModel(final ISelectModel territorialOrgUnitModel) { _territorialOrgUnitModel = territorialOrgUnitModel; }

    private ISelectModel _educationLevelsHighSchoolModel;
    public ISelectModel getEducationLevelsHighSchoolModel() { return _educationLevelsHighSchoolModel; }
    public void setEducationLevelsHighSchoolModel(final ISelectModel educationLevelsHighSchoolModel) { _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel; }

    private ISelectModel _developFormModel;
    public ISelectModel getDevelopFormModel() { return _developFormModel; }
    public void setDevelopFormModel(final ISelectModel developFormModel) { _developFormModel = developFormModel; }

    private ISelectModel _developConditionModel;
    public ISelectModel getDevelopConditionModel() { return _developConditionModel; }
    public void setDevelopConditionModel(final ISelectModel developConditionModel) { _developConditionModel = developConditionModel; }

    private ISelectModel _developTechModel;
    public ISelectModel getDevelopTechModel() { return _developTechModel; }
    public void setDevelopTechModel(final ISelectModel developTechModel) { _developTechModel = developTechModel; }

    private ISelectModel _developPeriodModel;
    public ISelectModel getDevelopPeriodModel() { return _developPeriodModel; }
    public void setDevelopPeriodModel(final ISelectModel developPeriodModel) { _developPeriodModel = developPeriodModel; }


    public boolean isSelectorsFilled() {
        return (
                (null != getFormativeOrgUnitModel()) &&
                (null != getTerritorialOrgUnitModel()) &&
                (null != getEducationLevelsHighSchoolModel()) &&
                (null != getDevelopFormModel()) &&
                (null != getDevelopConditionModel()) &&
                (null != getDevelopTechModel()) &&
                (null != getDevelopPeriodModel())
        );
    }

    public boolean isFilled() {
        return (
                (null != getFormativeOrgUnit()) &&
                (null != getTerritorialOrgUnit()) &&
                (null != getEducationLevelsHighSchool()) &&
                (null != getDevelopForm()) &&
                (null != getDevelopCondition()) &&
                (null != getDevelopTech()) &&
                (null != getDevelopPeriod())
        );
    }

    public void clear() {
        setFormativeOrgUnit(null);
        setTerritorialOrgUnit(null);
        setEducationLevelsHighSchool(null);
        setDevelopForm(null);
        setDevelopCondition(null);
        setDevelopTech(null);
        setDevelopPeriod(null);
    }

    private OrgUnit formativeOrgUnit;
    @Override public OrgUnit getFormativeOrgUnit() { return formativeOrgUnit; }
    public void setFormativeOrgUnit(final OrgUnit formativeOrgUnit) { this.formativeOrgUnit = formativeOrgUnit; }

    private OrgUnit territorialOrgUnit;
    @Override public OrgUnit getTerritorialOrgUnit() { return territorialOrgUnit; }
    public void setTerritorialOrgUnit(final OrgUnit territorialOrgUnit) { this.territorialOrgUnit = territorialOrgUnit; 	}

    private EducationLevelsHighSchool educationLevelsHighSchool;
    @Override public EducationLevelsHighSchool getEducationLevelsHighSchool() { return educationLevelsHighSchool; }
    public void setEducationLevelsHighSchool(final EducationLevelsHighSchool educationLevelsHighSchool) { this.educationLevelsHighSchool = educationLevelsHighSchool; }

    private DevelopForm developForm;
    @Override public DevelopForm getDevelopForm() { return developForm; }
    public void setDevelopForm(final DevelopForm developForm) { this.developForm = developForm; }

    private DevelopCondition developCondition;
    @Override public DevelopCondition getDevelopCondition() { return developCondition; }
    public void setDevelopCondition(final DevelopCondition developCondition) { this.developCondition = developCondition; }

    private DevelopTech developTech;
    @Override public DevelopTech getDevelopTech() { return developTech; }
    public void setDevelopTech(final DevelopTech developTech) { this.developTech = developTech; }

    private DevelopPeriod developPeriod;
    @Override public DevelopPeriod getDevelopPeriod() { return developPeriod; }
    public void setDevelopPeriod(final DevelopPeriod developPeriod) { this.developPeriod = developPeriod; }
}
