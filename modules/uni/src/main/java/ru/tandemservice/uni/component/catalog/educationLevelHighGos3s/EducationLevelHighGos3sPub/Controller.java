/* $Id: Controller.java 20463 2011-10-25 05:43:26Z vdanilov $ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3s.EducationLevelHighGos3sPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.EducationLevelHigh;
import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3s;

/**
 * @author oleyba
 * @since 5/11/11
 */
public class Controller extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.Controller<EducationLevelHighGos3s, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EducationLevelHigh> dataSource = new DynamicListDataSource<EducationLevelHigh>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelHigh.P_FULL_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EducationLevelHigh.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подчинено направлению подготовки (специальности)", new String[]{EducationLevelHigh.L_PARENT_LEVEL, EducationLevelHigh.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        return dataSource;
    }
}