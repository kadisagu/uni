package ru.tandemservice.uni.ui;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vdanilov
 */
public class FilterSelectModel<T> extends SelectModel<T> {

    public static interface IDataSettingsProvider {
        public IDataSettings getSettings();
    }

    private final IDataSettingsProvider provider;
    public IDataSettingsProvider getProvider() { return this.provider; }

    private final String name;
    public String getName() { return this.name; }

    public FilterSelectModel(IDataSettingsProvider provider, String name) {
        super();
        this.provider = provider;
        this.name = name;
    }

    public FilterSelectModel(IDataSettingsProvider provider, String name, ISelectModel source) {
        super(source);
        this.provider = provider;
        this.name = name;
    }

    public <U extends T> FilterSelectModel(IDataSettingsProvider provider, String name, List<U> source) {
        super(source);
        this.provider = provider;
        this.name = name;
    }

    public <U extends T> FilterSelectModel(IDataSettingsProvider provider, String name, U... source) {
        super(source);
        this.provider = provider;
        this.name = name;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getValue() {
        return (T)provider.getSettings().getEntry(getName()).getValue();
    }

    @Override
    public void setValue(T value) {
        provider.getSettings().getEntry(getName()).setValue(value);
    };



}
