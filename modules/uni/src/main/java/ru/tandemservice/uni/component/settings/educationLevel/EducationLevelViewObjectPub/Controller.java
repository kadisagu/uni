/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.settings.educationLevel.EducationLevelViewObjectPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        Model model = getModel(context);

        getDao().prepare(model);

        prepareDataSource(context);
    }

    protected void refresh(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    protected void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EducationOrgUnit> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Форма освоения", new String[]{EducationOrgUnit.L_DEVELOP_FORM, DevelopForm.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Условие освоения", new String[]{EducationOrgUnit.L_DEVELOP_CONDITION, DevelopCondition.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Технология освоения", new String[]{EducationOrgUnit.L_DEVELOP_TECH, DevelopTech.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Нормативный срок", new String[]{EducationOrgUnit.L_DEVELOP_PERIOD, DevelopPeriod.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Сокр. наз.", EducationOrgUnit.P_SHORT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вн. код", EducationOrgUnit.P_CODE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Используется", EducationOrgUnit.used().s()).setListener("onClickUsed"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить направление подготовки (специальность) подразделения?"));
        model.setDataSource(dataSource);
    }

    public void onClickUsed(IBusinessComponent context)
    {
        getDao().updateUsedOption((Long) context.getListenerParameter());
    }

    public void onClickAdd(IBusinessComponent context)
    {
        getModel(context).setEducationOrgUnitId(null);
        context.createDefaultChildRegion(new ComponentActivator(IUniComponents.EDUCATION_ORG_UNIT_ADD_EDIT));
    }

    public void onClickEdit(IBusinessComponent context)
    {
        getModel(context).setEducationOrgUnitId((Long) context.getListenerParameter());
        context.createDefaultChildRegion(new ComponentActivator(IUniComponents.EDUCATION_ORG_UNIT_ADD_EDIT));
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().delete((Long) context.getListenerParameter());
        refresh(context);
    }
}
