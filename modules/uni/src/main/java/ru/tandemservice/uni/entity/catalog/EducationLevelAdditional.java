package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelAdditionalGen;

/**
 * Направления подготовки ДПО
 */
public class EducationLevelAdditional extends EducationLevelAdditionalGen
{
    public EducationLevelAdditional()
    {
        setCatalogCode("educationLevelAdditional");
    }

    @Override
    public String getProgramSubjectTitleWithCode() {
        return getTitle(); // Для ДПО просто название
    }

    @Override
    public String getProgramSubjectWithCodeIndexAndGenTitle() {
        return getTitle(); // Для ДПО просто название
    }
}