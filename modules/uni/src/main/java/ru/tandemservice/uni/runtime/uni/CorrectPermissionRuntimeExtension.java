package ru.tandemservice.uni.runtime.uni;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.sec.entity.AccessMatrix;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.sec.meta.GroupMeta;
import org.tandemframework.sec.meta.GroupRelationMeta;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.PermissionMeta;
import org.tandemframework.sec.runtime.SecurityRuntime;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author vdanilov
 */
public class CorrectPermissionRuntimeExtension extends UniBaseDao implements IRuntimeExtension {

    private void collect(final Map<String, String> permissionKey2UpperTabPermissionMap, final PermissionGroupMeta pgMeta, final String parentViewKey) {
        final String localViewKey = getLocalViewKey(pgMeta);

        if (null == localViewKey) {
            // если мы не сумели найти ключ для текущего таба - прицепляем все к вышестоящему
            for (final PermissionMeta pMeta: pgMeta.getPermissionList()) {
                final String oldViewKey = permissionKey2UpperTabPermissionMap.put(pMeta.getName(), parentViewKey);
                if ((null != oldViewKey) && !oldViewKey.equals(parentViewKey)) {
                    logger.warn("Duplicate parent-view-key references for permission `"+pMeta.getName()+"' (`"+oldViewKey+"' -> `"+parentViewKey+"')");
                }
            }
        } else  {
            // если смогли найти ключ
            for (final PermissionMeta pMeta: pgMeta.getPermissionList()) {
                if (localViewKey.equals(pMeta.getName())) {
                    // если это как раз ключ просмотра текущего таба - прицепляем его к вышестоящей вкладке
                    final String oldViewKey = permissionKey2UpperTabPermissionMap.put(pMeta.getName(), parentViewKey);
                    if ((null != oldViewKey) && !oldViewKey.equals(parentViewKey)) {
                        logger.warn("Duplicate parent-view-key references for permission `"+pMeta.getName()+"' (`"+oldViewKey+"' -> `"+parentViewKey+"')");
                    }
                } else {
                    // иначе - к текущему табу (который нашли)
                    final String oldViewKey = permissionKey2UpperTabPermissionMap.put(pMeta.getName(), localViewKey);
                    if ((null != oldViewKey) && !oldViewKey.equals(localViewKey)) {
                        logger.warn("Duplicate parent-view-key references for permission `"+pMeta.getName()+"' (`"+oldViewKey+"' -> `"+localViewKey+"')");
                    }
                }
            }

        }

        // обрабатываем детей
        for (final PermissionGroupMeta child: pgMeta.getPermissionGroupList()) {
            collect(permissionKey2UpperTabPermissionMap, child, localViewKey);
        }
    }

    private String getLocalViewKey(final PermissionGroupMeta pgMeta) {
        String localViewKey = null;
        for (final PermissionMeta pMeta: pgMeta.getPermissionList()) {
            if ("просмотр".equals(StringUtils.lowerCase(pMeta.getTitle()))) {
                if (null != localViewKey) {
                    logger.warn("Duplicate local-view-key references for permission group `"+pgMeta.getName()+"' (`"+localViewKey+"' -> `"+pMeta.getName()+"')");
                }
                localViewKey = pMeta.getName();
            }
        }
        return localViewKey;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void init(final Object object) {
        final IDataSettings settings = DataSettingsFacade.getSettings("correctPermissionRE");
        if (Boolean.TRUE.equals(settings.get("INIT"))) { return; }


        // obtain orgUnit's group names
        final Set<String> groupNames = new HashSet<String>();
        for (final GroupMeta group: SecurityRuntime.getInstance().getGroupMap().values()) {
            final String entityClass = StringUtils.trimToNull(group.getEntityClass());
            try {
                if ((null != entityClass) && OrgUnit.class.isAssignableFrom(Class.forName(entityClass))) {
                    groupNames.add(group.getName());
                }
            } catch (final ClassNotFoundException e)  {
                logger.error(e);
            }
        }

        // { permissionKey -> parentTabViewPermissionKey }
        final Map<String, String> permissionKey2UpperTabPermissionMap = new HashMap<String, String>();
        for (final GroupRelationMeta relation: SecurityRuntime.getInstance().getGroupRelationList()) {
            if (groupNames.contains(relation.getGroupName())) {
                collect(permissionKey2UpperTabPermissionMap, SecurityRuntime.getInstance().getPermissionGroupMap().get(relation.getPermissionGroupName()), null);
            }
        }

        final Session session = getSession();

        final Map<Long, Set<String>> roleId2PKeySetMap = SafeMap.get(HashSet.class);
        final MQBuilder b = new MQBuilder(AccessMatrix.ENTITY_CLASS, "am", new String[] { AccessMatrix.L_ROLE+".id", AccessMatrix.P_PERMISSION_KEY });
        b.addOrder("am", AccessMatrix.L_ROLE+".id");
        for (final Object[] row: b.<Object[]>getResultList(session)) {
            roleId2PKeySetMap.get(row[0]).add((String)row[1]);
        }

        for (final Map.Entry<Long, Set<String>> e: roleId2PKeySetMap.entrySet()) {
            final Role role = (Role)session.load(Role.class, e.getKey());
            final Set<String> set = e.getValue();
            for (String pKey: new HashSet<String>(set) /* clone it here (avoid self customization)*/) {
                while (null != pKey) {
                    if (set.add(pKey)) {
                        final AccessMatrix row = new AccessMatrix();
                        row.setPermissionKey(pKey);
                        row.setRole(role);
                        session.save(row);
                        logger.info(row.getRole() + " -> " + row.getPermissionKey());
                    }
                    pKey = permissionKey2UpperTabPermissionMap.get(pKey);
                }
            }
        }

        session.flush();

        settings.set("INIT", Boolean.TRUE);
        DataSettingsFacade.saveSettings(settings);
    }

    @Override
    public void destroy() {}

}
