/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/14
 */
@Input({
    @Bind(key = UniEduProgramEduOuAddEditUI.EDU_HS_ID, binding = "eduHs.id"),
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "educationOrgUnit.id"),
    @Bind(key = "programId", binding = "programHolder.id")
})
public class UniEduProgramEduOuAddEditUI extends UIPresenter
{
    public static final String EDU_HS_ID = "eduHsId";

    private EducationOrgUnit educationOrgUnit = new EducationOrgUnit();
    private EducationLevelsHighSchool eduHs = new EducationLevelsHighSchool();
    private EntityHolder<EduProgram> programHolder = new EntityHolder<>();

    private ISelectModel eduHsModel;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _developFormList;
    private ISelectModel _developTechList;
    private ISelectModel _developConditionList;
    private ISelectModel _developPeriodList;

    @Override
    public void onComponentRefresh()
    {
        if (getEducationOrgUnit().getId() != null) {
            setEducationOrgUnit(DataAccessServices.dao().getNotNull(EducationOrgUnit.class, getEducationOrgUnit().getId()));
        }
        if (getEduHs().getId() != null) {
            setEduHs(DataAccessServices.dao().getNotNull(EducationLevelsHighSchool.class, getEduHs().getId()));
            if (getEducationOrgUnit().getEducationLevelHighSchool() == null) {
                getEducationOrgUnit().setEducationLevelHighSchool(getEduHs());
            }
        }

        setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));

        DQLSelectBuilder lhsBuilder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "e")
                .where(eq(property("e", EducationLevelsHighSchool.allowStudents()), value(Boolean.TRUE)));

        getProgramHolder().refresh();
        if (getProgramHolder().getId() == null) {
            setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
            setDevelopTechList(EducationCatalogsManager.getDevelopTechSelectModel());
            setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
            setDevelopPeriodList(EducationCatalogsManager.getDevelopPeriodSelectModel());
        } else {
            EduProgram p = getProgramHolder().getValue();

            if (p instanceof EduProgramHigherProf) {
                EduProgramHigherProf prof = (EduProgramHigherProf) p;
                EducationLevels educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4High(prof.getProgramSubject(), prof.getProgramSpecialization());

                lhsBuilder.where(eq(property("e", EducationLevelsHighSchool.educationLevel()), value(educationLevel)));
                lhsBuilder.where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(p.getOwnerOrgUnit().getOrgUnit())));
                lhsBuilder.where(eq(property("e", EducationLevelsHighSchool.assignedQualification()), value(prof.getProgramQualification())));
                lhsBuilder.where(eq(property("e", EducationLevelsHighSchool.programOrientation()), value(prof.getProgramOrientation())));
            } else if (p instanceof EduProgramSecondaryProf) {
                EduProgramSecondaryProf prof = (EduProgramSecondaryProf) p;
                EducationLevelMiddle educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4Middle(prof.getProgramSubject(), prof.isInDepthStudy());

                lhsBuilder.where(eq(property("e", EducationLevelsHighSchool.educationLevel()), value(educationLevel)));
                lhsBuilder.where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(p.getOwnerOrgUnit().getOrgUnit())));
            } else {
                throw new IllegalStateException();
            }

            getEducationOrgUnit().setDevelopCondition(DataAccessServices.dao().get(DevelopCondition.class, DevelopCondition.code(), DevelopConditionCodes.FULL_PERIOD));
            setDevelopFormList(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getList(DevelopForm.class, DevelopForm.programForm(), getProgramHolder().getValue().getForm(), DevelopForm.code().s())));
            setDevelopTechList(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getList(DevelopTech.class, DevelopTech.programTrait(), getProgramHolder().getValue().getEduProgramTrait(), DevelopTech.code().s())));
            setDevelopConditionList(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getList(DevelopCondition.class, DevelopCondition.code().s())));
            setDevelopPeriodList(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getList(DevelopPeriod.class, DevelopPeriod.eduProgramDuration(), getProgramHolder().getValue().getDuration(), DevelopPeriod.P_PRIORITY)));
        }

        List<EducationLevelsHighSchool> eduHsList = lhsBuilder.createStatement(_uiSupport.getSession()).list();
        setEduHsModel(new LazySimpleSelectModel<>(eduHsList, EducationLevelsHighSchool.displayableTitle().s())
                .setSortProperty(EducationLevelsHighSchool.displayableTitle().s())
                .setSearchProperty(EducationLevelsHighSchool.displayableTitle().s())
                .setSearchFromStart(false));
    }

    public void onClickApply() {
        UniEduProgramManager.instance().dao().saveOrUpdateEduOu(getEducationOrgUnit());
        deactivate();
    }

    public boolean isEduHsDisabled() {
        return getEduHs().getId() != null || getEducationOrgUnit().getId() != null;
    }

    public boolean isEditForm() {
        return getEducationOrgUnit().getId() != null;
    }

    public boolean isCreateFromProgram() {
        return getProgramHolder().getId() != null;
    }

    // getters and setters

    public EducationOrgUnit getEducationOrgUnit()
    {
        return educationOrgUnit;
    }

    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        this.educationOrgUnit = educationOrgUnit;
    }

    public EducationLevelsHighSchool getEduHs()
    {
        return eduHs;
    }

    public void setEduHs(EducationLevelsHighSchool eduHs)
    {
        this.eduHs = eduHs;
    }

    public ISelectModel getDevelopConditionList() { return _developConditionList; }
    public void setDevelopConditionList(ISelectModel developConditionList) { _developConditionList = developConditionList; }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public ISelectModel getDevelopPeriodList() { return _developPeriodList; }
    public void setDevelopPeriodList(ISelectModel developPeriodList) { _developPeriodList = developPeriodList; }

    public ISelectModel getDevelopTechList() { return _developTechList; }
    public void setDevelopTechList(ISelectModel developTechList) { _developTechList = developTechList; }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEduHsModel()
    {
        return eduHsModel;
    }

    public void setEduHsModel(ISelectModel eduHsModel)
    {
        this.eduHsModel = eduHsModel;
    }

    public EntityHolder<EduProgram> getProgramHolder()
    {
        return programHolder;
    }
}