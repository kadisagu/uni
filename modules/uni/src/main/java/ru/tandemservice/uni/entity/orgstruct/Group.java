/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.entity.orgstruct;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.gen.GroupGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Group extends GroupGen implements ISecLocalEntityOwner, ITitled
{
    public static final Object COURSE_KEY = new String[] { Group.L_COURSE, ICatalogItem.CATALOG_ITEM_TITLE };
    public static final Object FORMATIVE_ORGUNIT_KEY = new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE };
    public static final Object TERRITORIAL_ORGUNIT_KEY = new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE };
    public static final Object PRODUCTIVE_ORGUNIT_KEY = new String[] { Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_FULL_TITLE };

    public static final String P_ACTIVE_STUDENT_COUNT = "activeStudentCount";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, Group.class)
                .titleProperty(Group.title().s())
                .filter(Group.title())
                .order(Group.title());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        final List<IEntity> result = new ArrayList<>(3);
        final EducationOrgUnit educationOrgUnit = getEducationOrgUnit();
        if (null != educationOrgUnit) {
            result.add(educationOrgUnit.getEducationLevelHighSchool().getOrgUnit());
            result.add(educationOrgUnit.getFormativeOrgUnit());
            result.add(educationOrgUnit.getTerritorialOrgUnit());
        }
        return result;
    }

    public String getFullTitle()
    {
        StringBuilder str = new StringBuilder("Группа студентов: ");
        str.append(getTitle() + " | " );
        str.append(getCourse().getTitle() + " курс ");
        str.append("  " + getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        return str.toString();
    }
}