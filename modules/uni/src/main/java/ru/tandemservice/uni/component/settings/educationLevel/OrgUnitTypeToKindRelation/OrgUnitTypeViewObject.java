/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitTypeToKindRelation;

import org.tandemframework.core.bean.IFastBean;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.catalog.entity.gen.OrgUnitTypeGen;

/**
 * @author vip_delete
 */
public class OrgUnitTypeViewObject extends OrgUnitType
{
    private boolean _1;
    private boolean _2;
    private boolean _3;

    public OrgUnitTypeViewObject(OrgUnitType orgUnitType, boolean a1, boolean a2, boolean a3)
    {
        update(orgUnitType);
        setId(orgUnitType.getId());
        _1 = a1;
        _2 = a2;
        _3 = a3;
    }

    //TODO!!!!!!!!!!!HACK!!!!!!!!!!!!!!!!!!
    public boolean is1()
    {
        //выпускающее?
        return _1;
    }

    public boolean is2()
    {
        //формирующее?
        return _2;
    }

    public boolean is3()
    {
        //территориальное?
        return _3;
    }

    public static final FastBean<OrgUnitTypeViewObject> FAST_BEAN = new FastBean<OrgUnitTypeViewObject>();

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitTypeGen> extends OrgUnitType.FastBean<T>
    {
        @SuppressWarnings("unchecked")
        @Override
        public Class<T> getBeanClass()
        {
            return (Class<T>)OrgUnitTypeViewObject.class;
        }
    }
}
