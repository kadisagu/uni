package ru.tandemservice.uni.entity.education;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.dao.grid.IGridTerm;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistribution;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.gen.DevelopGridTermGen;

/**
 * Семестр учебной сетки
 */
public class DevelopGridTerm extends DevelopGridTermGen implements IGridTerm, IEntityDebugTitled
{
    @Override
    @EntityDSLSupport(parts=L_COURSE+"."+Course.P_INT_VALUE)
    public int getCourseNumber() { return getCourse().getIntValue(); }

    @Override
    @EntityDSLSupport(parts=L_TERM+"."+Term.P_INT_VALUE)
    public int getTermNumber() { return getTerm().getIntValue(); }

    @Override
    @EntityDSLSupport(parts=L_PART+"."+YearDistributionPart.P_NUMBER)
    public int getPartNumber() { return getPart().getNumber(); }

    @Override
    @EntityDSLSupport(parts=L_PART+"."+YearDistributionPart.L_YEAR_DISTRIBUTION+"."+YearDistribution.P_AMOUNT)
    public int getPartAmount() { return getPart().getYearDistribution().getAmount(); }

    @Override
    public String getEntityDebugTitle()
    {
        return getCourse().getTitle() + ", " + getTerm().getTitle() + ", " + getPart().getShortTitle();
    }
}