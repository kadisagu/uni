package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final IDAO dao = this.getDao();
        dao.prepare(model);
    }

    public void onTabChange(final IBusinessComponent component)
    {
        component.refresh();
    }

}
