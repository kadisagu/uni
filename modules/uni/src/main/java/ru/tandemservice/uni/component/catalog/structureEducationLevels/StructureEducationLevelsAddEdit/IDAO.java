/* $Id$ */
package ru.tandemservice.uni.component.catalog.structureEducationLevels.StructureEducationLevelsAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author Vasily Zhukov
 * @since 30.06.2011
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<StructureEducationLevels, Model>
{
}
