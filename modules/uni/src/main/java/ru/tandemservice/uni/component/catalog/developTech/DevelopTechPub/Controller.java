/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developTech.DevelopTechPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.DevelopTech;

/**
 * @author AutoGenerator
 * @since 16.06.2008
 */
public class Controller extends DefaultCatalogPubController<DevelopTech, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<DevelopTech> dataSource = new DynamicListDataSource<DevelopTech>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", DevelopTech.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокр. название", DevelopTech.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сист. название", DevelopTech.programTrait().defaultTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Префикс группы", DevelopTech.P_GROUP).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Используется", DevelopTech.P_ENABLED).setListener("onClickChangeCatalogItemActivity"));

        if (Debug.isDisplay()) {
            dataSource.addColumn(new SimpleColumn("Системный код", "code").setOrderable(false).setWidth(1).setClickable(false));
        }

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));

        return dataSource;
    }

    public void onClickChangeCatalogItemActivity(IBusinessComponent component)
    {
        CatalogManager.instance().modifyDao().doChangeCatalogItemActivity(component.<Long>getListenerParameter());
    }
}
