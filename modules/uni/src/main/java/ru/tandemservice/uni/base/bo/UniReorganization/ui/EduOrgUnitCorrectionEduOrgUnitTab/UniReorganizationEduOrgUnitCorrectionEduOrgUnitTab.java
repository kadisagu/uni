/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.ui.EduOrgUnitCorrectionEduOrgUnitTab;

import org.apache.tools.ant.taskdefs.condition.Or;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.util.NonActiveSelectValueStyle;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniReorganization.UniReorganizationManager;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
@Configuration
public class UniReorganizationEduOrgUnitCorrectionEduOrgUnitTab extends BusinessComponentManager {

    public static final String OWNER_DS = "ownerDS";
    public static final String OWNER = "owner";
    public static final String EDU_ORG_UNIT_DS = "eduOrgUnitDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .addDataSource(CommonBaseStaticSelectDataSource
                        .selectDS(OWNER_DS, getName(), UniReorganizationManager.formativeOrgUnitReorganizationCustomizedOrgUnitHandler(getName())
                        .customize((alias, dql, context, filter) -> dql.where(exists(EducationOrgUnit.class,
                                EducationOrgUnit.formativeOrgUnit().s(), property(alias)))))
                        .valueStyleSource(value -> {
                            if (value instanceof OrgUnit && ((OrgUnit) value).isArchival()) return NonActiveSelectValueStyle.INSTANCE;
                            return null;
                        }))
                .addDataSource(searchListDS(EDU_ORG_UNIT_DS, eduOrgUnitDSColumnListExtPoint(), eduOrgUnitDSHandler()))
                .create();
    }


    @Bean
    public EntityComboDataSourceHandler registryStructureDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationOrgUnit.class);
    }


    @Bean
    public ColumnListExtPoint eduOrgUnitDSColumnListExtPoint()
    {
        return columnListExtPointBuilder(EDU_ORG_UNIT_DS)
                .addColumn(checkboxColumn("checkboxColumn").number("0"))
                .addColumn(publisherColumn("titleColumn", EducationOrgUnit.title()).order())
                .addColumn(textColumn("educationLevelHighSchoolOrgUnitTitle", EducationOrgUnit.educationLevelHighSchool().orgUnit().fullTitle()).order())
                .addColumn(textColumn("territorialOrgUnitTitle", EducationOrgUnit.territorialOrgUnit().territorialFullTitle()).order())
                .addColumn(textColumn("developForm", EducationOrgUnit.developForm().title()).order())
                .addColumn(textColumn("developCondition", EducationOrgUnit.developCondition().title()).order())
                .addColumn(textColumn("developTech", EducationOrgUnit.developTech().title()).order())
                .addColumn(textColumn("developPeriod", EducationOrgUnit.developPeriod().title()).order())
                .addColumn(booleanColumn("used", EducationOrgUnit.used()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduOrgUnitDSHandler() {
        return new DefaultSearchDataSourceHandler(getName(), EducationOrgUnit.class){
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e");
                UniEduProgramEducationOrgUnitAddon util = context.get("util");
                util.applyFilters(builder, "e");
                final OrgUnit owner = context.get(OWNER);
                builder.where(eq(property("e", EducationOrgUnit.formativeOrgUnit().id()), value(owner.getId())));
                final EntityOrder entityOrder = input.getEntityOrder();
                if (entityOrder != null) {
                    if (entityOrder.getKeyString().equals("title"))
                        builder.order(property("e", EducationOrgUnit.educationLevelHighSchool().fullTitle()), entityOrder.getDirection());
                    else
                        builder.order(property("e", entityOrder.getKeyString()), entityOrder.getDirection());
                }
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
            }
        };
    }

}
