/**
 * $Id$
 */
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards;

import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author dseleznev
 * Created on: 09.10.2008
 */
@SuppressWarnings("deprecation")
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public IDocumentRenderer getDocumentRenderer(Model model)
    {
        final Session session = getSession();
        final Long NULL = (long) 0;

        final Map<Long, Collection<Long>> group2studentIdsMap = new HashMap<>();
        BatchUtils.execute(model.getStudentIds(), 200, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> ids) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(Student.class, "s");
                dql.column(DQLExpressions.property(Student.id().fromAlias("s")));
                dql.column(DQLExpressions.property(Student.group().id().fromAlias("s")));
                dql.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), ids));
                for (Object[] row : dql.createStatement(session).<Object[]>list()) {
                    SafeMap.safeGet(group2studentIdsMap, (null == row[1] ? NULL : (Long)row[1]), ArrayList.class).add((Long)row[0]);
                }
            }
        });

        if (group2studentIdsMap.isEmpty()) {
            throw new ApplicationException("Список студентов пуст.");
        }

        final IOrgUnitStudentPersonCardPrintFactory printFactory = model.getPrintFactory().get();

        try {
            int i = 1;
            ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
            BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

            ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
            zipOut.setLevel(Deflater.BEST_COMPRESSION);

            for (Map.Entry<Long, Collection<Long>> entry: group2studentIdsMap.entrySet()) {

                final Group group = (NULL.equals(entry.getKey()) ? null : (Group)session.get(Group.class, entry.getKey()));

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
                builder.add(MQExpression.in("s", "id", entry.getValue()));
                builder.addLeftJoinFetch("s", Student.L_GROUP, "g");
                builder.addJoinFetch("s", Student.L_PERSON, "p");
                builder.addJoinFetch("p", Person.L_IDENTITY_CARD, "idCard");
                builder.addJoinFetch("s", Student.L_STATUS, "studentStatus");
                builder.addJoinFetch("s", Student.L_EDUCATION_ORG_UNIT, "ou");
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                builder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME);

                final List<Student> studentList = builder.getResultList(session);

                final Map<Long, List<org.tandemframework.shared.person.base.entity.PersonEduInstitution>> personEduInstitutionMap = new HashMap<>();
                final Map<Long, List<PersonNextOfKin>> personNextOfKinMap = new HashMap<>();
                {
                    MQBuilder personIdsBuilder = new MQBuilder(Student.ENTITY_CLASS, "s", new String[] { Student.person().id().s() });
                    personIdsBuilder.add(MQExpression.in("s", "id", entry.getValue()));
                    {
                        MQBuilder eduInstitutionsBuilder = new MQBuilder(org.tandemframework.shared.person.base.entity.PersonEduInstitution.ENTITY_CLASS, "ei");
                        eduInstitutionsBuilder.add(MQExpression.in("ei", org.tandemframework.shared.person.base.entity.PersonEduInstitution.person().id().s(), personIdsBuilder));
                        for(org.tandemframework.shared.person.base.entity.PersonEduInstitution eduInstitution : eduInstitutionsBuilder.<org.tandemframework.shared.person.base.entity.PersonEduInstitution>getResultList(getSession())) {
                            SafeMap.safeGet(personEduInstitutionMap, eduInstitution.getPerson().getId(), ArrayList.class).add(eduInstitution);
                        }
                    }
                    {
                        MQBuilder nextOfKinBuilder = new MQBuilder(PersonNextOfKin.ENTITY_CLASS, "nok");
                        nextOfKinBuilder.add(MQExpression.in("nok", PersonNextOfKin.person().id().s(), personIdsBuilder));
                        for(PersonNextOfKin kextOfKin : nextOfKinBuilder.<PersonNextOfKin>getResultList(getSession())) {
                            SafeMap.safeGet(personNextOfKinMap, kextOfKin.getPerson().getId(), ArrayList.class).add(kextOfKin);
                        }
                    }
                }

                setDocumentTemplate(printFactory, studentList.isEmpty()? null : studentList.get(0));

                final RtfDocument template = printFactory.getData().getTemplate();
                final RtfDocument resultDoc = template.getClone();
                resultDoc.getElementList().clear();

                for (Student student: studentList) {

                    if (!resultDoc.getElementList().isEmpty()) {
//                        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
//                        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
//                        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
//                        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                    }

                    Long personId = student.getPerson().getId();
                    final List<org.tandemframework.shared.person.base.entity.PersonEduInstitution> personEduInstitutionList = SafeMap.safeGet(personEduInstitutionMap, personId, ArrayList.class);
                    final List<PersonNextOfKin> personNextOfKinList =  SafeMap.safeGet(personNextOfKinMap, personId, ArrayList.class);
                    printFactory.initPrintData(student, personEduInstitutionList, personNextOfKinList, session);

                    initStudentPrintData(student, printFactory);

                    final RtfDocument doc = template.getClone();
                    printFactory.addCard(doc);
                    resultDoc.getElementList().addAll(doc.getElementList());
                }

                // resultDoc.setSettings(template.getSettings());
                // resultDoc.setHeader(template.getHeader());

                final String fileName = "student-cards-"+StudentNumberFormatter.INSTANCE.format(i++)+"-"+ CoreStringUtils.transliterate((null == group ? "no-group" : group.getTitle()) + ".rtf");
                if (1 == group2studentIdsMap.size()) {
                    // если группа одна - то возвращаем rtf
                    return new CommonBaseRenderer().rtf().fileName(fileName).document(resultDoc);
                }

                final ByteArrayOutputStream groupStream = new ByteArrayOutputStream(1024);
                final BufferedOutputStream groupBuffer = new BufferedOutputStream(groupStream, 1024);
                new CommonBaseRenderer().rtf().fileName(fileName).document(resultDoc).render(groupBuffer);
                groupBuffer.close();
                groupStream.close();

                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.write(groupStream.toByteArray());
                zipOut.closeEntry();
                session.clear();
            }

            zipOut.close();
            zipBuffer.close();

            return new CommonBaseRenderer().zip().fileName("student-cards.zip").document(zipStream);
        } catch(Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    @Override
    public void setDocumentTemplate(IOrgUnitStudentPersonCardPrintFactory factory, Student student)
    {
        factory.getData().setTemplate(new RtfReader().read(getCatalogItem(TemplateDocument.class, UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT).getContent()));
    }

    protected void initStudentPrintData(Student student, IOrgUnitStudentPersonCardPrintFactory printFactory)
    {
    }
}