/* $Id: GroupTitleAlgorithmSample1.java 5483 2008-11-28 12:06:00Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao.group;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class GroupTitleAlgorithmSample4 extends UniBaseDao implements IAbstractGroupTitleDAO
{

    private static final Set<String> TYPES = new HashSet<String>(Arrays.asList(OrgUnitTypeCodes.BRANCH, OrgUnitTypeCodes.REPRESENTATION));

    /*
     * Формат - (ф)(СНУОВ)-(гг)-(н)-(у)-(ФП)
     * Пример - зПИЭ-07-1-с-КУ
     * Описание:
     *   ф - поле "группа" для формы освоения из справочника;
     *   СНУОВ - сокращенное название уровня образования ОУ;
     *   (гг) - две цифры года поступления (начального учебного года для группы);
     *   (н) - порядковый номер группы в рамках: уровня образования ОУ + формы освоения + условия освоения + факультетов(институтов)/филиала/представительства;
     *   (у) - поле "группа" для условия освоения из справочника;
     *   (ФП) - сокращенное название территориального подразделения (филиала/представительства)
     */
    @Override
    public String getTitle(final Group group, final int number)
    {

        EducationOrgUnit educationOrgUnit = get(group.getEducationOrgUnit());
        EducationYear startEducationYear = get(group.getStartEducationYear());

        final StringBuilder buffer = new StringBuilder()
        .append(StringUtils.trimToEmpty(educationOrgUnit.getDevelopForm().getGroup()))
        .append(educationOrgUnit.getEducationLevelHighSchool().getShortTitle())
        .append('-').append(String.format("%02d", startEducationYear.getIntValue() % 100))
        .append('-').append(number % getMaxNumber());

        final String c = StringUtils.trimToNull(educationOrgUnit.getDevelopCondition().getGroup());
        if (null != c) {
            buffer.append('-').append(c);
        }

        final OrgUnit f = educationOrgUnit.getFormativeOrgUnit();
        if (TYPES.contains(f.getOrgUnitType().getCode())) {
            return buffer.append('-').append(StringUtils.trimToEmpty(f.getShortTitle())).toString();
        }

        final OrgUnit t = educationOrgUnit.getTerritorialOrgUnit();
        if (TYPES.contains(t.getOrgUnitType().getCode())) {
            return buffer.append('-').append(StringUtils.trimToEmpty(t.getShortTitle())).toString();
        }

        return buffer.toString();
    }

    @Override
    public int getMaxNumber()
    {
        return 10;
    }
}
