package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.gen.*;

/**
 * Дополнительный статус студента (справочник)
 */
public class StudentCustomStateCI extends StudentCustomStateCIGen implements IColoredEntity, ITitled
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new EntityComboDataSourceHandler(ownerId, StudentCustomStateCI.class)
                .filter(StudentCustomStateCI.title())
                .order(StudentCustomStateCI.title());
    }
}