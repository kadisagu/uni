/**
 * $Id$
 */
package ru.tandemservice.uni.ui;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.util.UniStringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author dseleznev
 *         Created on: 09.10.2008
 */
public abstract class EducationLevelsAutocompleteModel extends FullCheckSelectModel
{
    protected abstract List<EducationLevels> getFilteredList();

    @Deprecated
    protected EducationLevelsAutocompleteModel()
    {
    }

    @Override
    public ListResult findValues(String filter)
    {
        filter = (filter != null) ? UniStringUtils.escapePattern(CoreStringUtils.escapeLike(filter)).replaceAll("%", ".*") : "";
        Pattern pattern = Pattern.compile(filter);

        List<EducationLevels> result = new ArrayList<>();
        for (EducationLevels educationLevel : getFilteredList())
        {
            if (filter.isEmpty() || checkLevel(pattern, educationLevel))
                result.add(educationLevel);
        }

        Collections.sort(result, EducationLevelDAO.EDU_LEVEL_OKSO_TITLE_COMPARATOR);
        int count = result.size();
        if (count > 100)
            result = result.subList(0, 100);
        return new ListResult<>(result, count);
    }

    protected boolean checkLevel(Pattern pattern, EducationLevels educationLevel)
    {
        return pattern.matcher(educationLevel.getDisplayableTitle().toUpperCase()).find();
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        EducationLevels level = UniDaoFacade.getCoreDao().get(EducationLevels.class, (Long) primaryKey);
        if (getFilteredList().contains(level))
        {
            level.getDisplayableTitle(); // unproxy
            return level;
        }
        return null;
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return ((EducationLevels) value).getDisplayableTitle();
    }

}