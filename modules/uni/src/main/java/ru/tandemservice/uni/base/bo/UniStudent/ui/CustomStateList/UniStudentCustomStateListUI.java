/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateAddEdit.UniStudentCustomStateAddEdit;

import java.util.Date;

/**
 * @author nvankov
 * @since 3/25/13
 */
@Input({
        @Bind(key = "studentId", binding = "studentId", required = true),
})
public class UniStudentCustomStateListUI extends UIPresenter
{
    private Long _studentId;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(dataSource.getName().equals(UniStudentCustomStateList.CUSTOM_STATE_LIST_DS))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "studentCustomStateCI", "activeDateFrom", "activeDateTo"));
            dataSource.put("studentId", _studentId);
        }
    }

    public void onClickAddCustomState()
    {
        _uiActivation.asRegionDialog(UniStudentCustomStateAddEdit.class).
                parameter("studentId", _studentId).
                activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(UniStudentCustomStateAddEdit.class).
                parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).
                parameter("studentId", _studentId).
                activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickApply()
    {
        Date activeDateFrom = _uiSettings.get("activeDateFrom");
        Date activeDateTo = _uiSettings.get("activeDateTo");
        if(null != activeDateFrom && null != activeDateTo)
        {
            if(activeDateFrom.after(activeDateTo))
            {
                _uiSupport.error("Дата активности статуса «с» должна быть не больше даты активности «по»", "activeDateFrom", "activeDateTo");
            }
        }
        if(!getUserContext().getErrorCollector().hasErrors())
        {
            saveSettings();
        }
    }
}
