/* $Id$ */
package ru.tandemservice.uni.services;

import org.tandemframework.core.CoreCollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public interface IEntrantEduContractDataService
{
    public static final String BEAN_NAME = "entrantContractDataService";

    /**
     * Возвращает данные о договоре на обучение по идентификатору студента
     *
     * @param ecgpEntrantRecommendedId  - идентификатор рекомендованного к зачислению студента предзачисления
     * @param studentId                      - "страховочный" идентификатор студента. Если договор не найдётся для студента,
     *                                       будет предпринята попытка найти договор для стдудента, соответствующего абитуриенту
     * @return - данные о договоре на обучении (номер и дата)
     */
    ContractAttributes getEntrantContractAttributes(Long ecgpEntrantRecommendedId, Long studentId);

    /**
     * Возвращает мап данных о договорах на обучение по коллекции идентификаторов студентов
     *
     * @param studentIds - коллекция пар идентификаторов.
     *                   Первый идентификатор рекомендованного к зачислению студента предзачисления
     *                   второй идентификатор идентификатор студента
     * @return - мап данных о договорах на обучении (номер и дата)
     *         ключ в мапе - идентификатор студента, значение - данные о договоре на обучение
     */
    Map<Long, ContractAttributes> getEntrantContractAttributes(List<CoreCollectionUtils.Pair<Long, Long>> studentIds);
}