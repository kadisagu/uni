package ru.tandemservice.uni.entity.education;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.entity.education.gen.DevelopCombinationGen;
import ru.tandemservice.uni.util.DevelopUtil;

/**
 * ФУТС (возможные варианты, по умолчанию)
 */
public class DevelopCombination extends DevelopCombinationGen implements ITitled, IDevelopCombination
{
    private String _title_cache;

    @EntityDSLSupport
    @Override
    public String getTitle()
    {
        if (null != _title_cache) { return _title_cache; }
        if (getDevelopForm() == null) {
            return this.getClass().getSimpleName();
        }
        return (_title_cache = DevelopUtil.getTitle(this));
    }

}