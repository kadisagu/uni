/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni;

import org.tandemframework.shared.employeebase.catalog.entity.codes.*;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

/**
 * @author lefay
 */
public interface UniDefines
{
    @Deprecated int MAX_SELECT_ITEMS_COUNT = 50;

    @Deprecated String CATALOG_PROFESSION = "Profession";

    @Deprecated String CATALOG_DIPLOMA_QUALIFICATIONS = "DiplomaQualifications";
    @Deprecated String CATALOG_QUALIFICATIONS = "Qualifications";
    @Deprecated String QUALIFICATIONS_BASIC_2 = QualificationsCodes.N_P_O_2;
    @Deprecated String QUALIFICATIONS_BASIC_3 = QualificationsCodes.N_P_O_3;
    @Deprecated String QUALIFICATIONS_BASIC_4 = QualificationsCodes.N_P_O_4;
    @Deprecated String QUALIFICATIONS_MIDDLE_BASE = QualificationsCodes.BAZOVYY_UROVEN_S_P_O;
    @Deprecated String QUALIFICATIONS_MIDDLE_HIGH = QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O;
    @Deprecated String QUALIFICATIONS_BACHELOR = QualificationsCodes.BAKALAVR;
    @Deprecated String QUALIFICATIONS_ENGINEER = QualificationsCodes.SPETSIALIST;
    @Deprecated String QUALIFICATIONS_MASTER = QualificationsCodes.MAGISTR;
    @Deprecated String QUALIFICATIONS_INCREASE = QualificationsCodes.POVYSHENIE_KVALIFIKATSII;
    @Deprecated String QUALIFICATIONS_RETRAINING = QualificationsCodes.PROFESSIONALNAYA_PEREPODGOTOVKA;
    String CATALOG_EMPLOYEE_SPECIALITY = "EmployeeSpeciality";

    @Deprecated String CATALOG_DEVELOP_FORM = "DevelopForm";
    @Deprecated String CATALOG_DEVELOP_FORM_INTERNAL = DevelopFormCodes.FULL_TIME_FORM;
    @Deprecated String CATALOG_DEVELOP_FORM_CORRESPONDENCE = DevelopFormCodes.CORESP_FORM;
    @Deprecated String CATALOG_DEVELOP_FORM_INTERNAL_CORRESPONDENCE = DevelopFormCodes.PART_TIME_FORM;
    @Deprecated String CATALOG_DEVELOP_FORM_EXTERNAT = DevelopFormCodes.EXTERNAL_FORM;
    @Deprecated String CATALOG_DEVELOP_FORM_INDEPENDENT = DevelopFormCodes.APPLICANT_FORM;

    @Deprecated String CATALOG_DEVELOP_TECH = "DevelopTech";
    String DEVELOP_TECH_SIMPLE = DevelopTechCodes.GENERAL;
    String DEVELOP_TECH_REMOTE = DevelopTechCodes.DISTANCE;

    @Deprecated String CATALOG_DEVELOP_CONDITION = "DevelopCondition";
    String DEVELOP_CONDITION_FULL_TIME = "1";
    String DEVELOP_CONDITION_SHORT = "2";
    String DEVELOP_CONDITION_FAST = "3";
    String DEVELOP_CONDITION_SHORT_FAST = "4";

    String CATALOG_EMPLOEE_POST_STATUS = "EmployeePostStatus";

    @Deprecated
    String EMPLOYEE_POST_STATUS_ACTIVE = EmployeePostStatusCodes.STATUS_ACTIVE; // Активный
    @Deprecated
    String EMPLOYEE_POST_STATUS_POSSIBLE = EmployeePostStatusCodes.STATUS_POSSIBLE; // Возможный
    @Deprecated
    String EMPLOYEE_POST_STATUS_WEEKEND = EmployeePostStatusCodes.STATUS_LEAVE_REGULAR; // Отпуск
    @Deprecated
    String EMPLOYEE_POST_STATUS_WEEKEND_WO_SALARY = EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR; // Отпуск, без сохранения заработной платы
    @Deprecated
    String EMPLOYEE_POST_STATUS_WEEKEND_PREGNANCY = EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY; // Отпуск по беременности и родам
    @Deprecated
    String EMPLOYEE_POST_STATUS_WEEKEND_CHILDCARE = EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE; // Отпуск по уходу за ребенком в возрасте до 1.5 лет
    @Deprecated
    String EMPLOYEE_POST_STATUS_MISSION = EmployeePostStatusCodes.STATUS_TRIP; // Командирован
    @Deprecated
    String EMPLOYEE_POST_STATUS_FIRED = EmployeePostStatusCodes.STATUS_FIRED; // Уволен
    @Deprecated
    String EMPLOYEE_POST_STATUS_TRANSFERRED = EmployeePostStatusCodes.STATUS_TRANSFERED; // Переведен

    String CATALOG_POST_TYPE = "PostType";
    String POST_TYPE_MAIN_JOB = PostTypeCodes.MAIN_JOB;
    String POST_TYPE_SECOND_JOB_INNER = PostTypeCodes.SECOND_JOB_INNER;
    String POST_TYPE_INSTEAD_ABSENT_EMPLOYEE = PostTypeCodes.INSTEAD_ABSENT_EMPLOYEE;
    String POST_TYPE_ASSISTANT = PostTypeCodes.ASSISTANT;
    String POST_TYPE_EXECUTIVE = PostTypeCodes.EXECUTIVE;
    String POST_TYPE_TEMPORARILY_EXECUTIVE = PostTypeCodes.TEMPORARILY_EXECUTIVE;
    String POST_TYPE_SECOND_JOB_OUTER = PostTypeCodes.SECOND_JOB_OUTER;
    String POST_TYPE_SECOND_JOB = PostTypeCodes.SECOND_JOB;

    String CATALOG_SPORT_RANK = "SportRank";

    String CATALOG_FOREIGN_LANGUAGE = "ForeignLanguage";
    String CATALOG_FOREIGN_LANGUAGE_ENGLISH = "12";
    String CATALOG_FOREIGN_LANGUAGE_SKILL = "ForeignLanguageSkill";

    String CATALOG_COMPENSATION_TYPE = "CompensationType";
    String COMPENSATION_TYPE_BUDGET = "1";
    String COMPENSATION_TYPE_CONTRACT = "2";

    String CATALOG_COURSE = "Course";
    //String CATALOG_COURSE_FIRST = "1"; use CourseCodes


    //    String CATALOG_CHAIR_TYPE = "ChairType";
    //    String CATALOG_CHAIR_TYPE_PRODUCTIVE = "1";
    //    String CATALOG_CHAIR_TYPE_NON_PRODUCTIVE = "2";
    //String CATALOG_ITEM_ORGUNIT_TYPE_MAIN = "10";

    String CATALOG_STUDENT_STATUSES = "StudentStatus";
    String CATALOG_STUDENT_STATUS_ACTIVE = "1";
    String CATALOG_STUDENT_STATUS_POSSIBLE = "2";
    String CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA = "3";
    String CATALOG_STUDENT_STATUS_ACADEM = "5";
    String CATALOG_STUDENT_STATUS_EXCLUDED = "8";
    String CATALOG_STUDENT_STATUS_PREGNANCY = "20";
    String CATALOG_STUDENT_STATUS_CHILD = "21";
    String CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE = "22";
    String CATALOG_STUDENT_STATUS_CHILD_WITHOUT_ATTENDANCE = "23";

    String CATALOG_IDENTITYCARD_TYPE = "IdentityCardType";
    String CATALOG_IDENTITYCARD_TYPE_PASSPORT = "1";
    String CATALOG_IDENTITYCARD_TYPE_PASSPORT_NOT_RF = "2";
    String CATALOG_IDENTITYCARD_TYPE_RESIDENCE = "6";

    @Deprecated
    String CATALOG_SEX_MALE = SexCodes.MALE;

    @Deprecated
    String CATALOG_SEX_FEMALE = SexCodes.FEMALE;

    @Deprecated String CATALOG_EDU_INSTITUTION_TYPE_KIND_BASE  = "5"; //Общеобразовательные учреждения
    @Deprecated String CATALOG_EDU_INSTITUTION_TYPE_KIND_BASE_EVENING  = "8"; //Вечерние (сменные) общеобразовательные учреждения

    @Deprecated String CATALOG_EDU_INSTITUTION_KIND_UNIVERSITY = "17";

    @Deprecated String CATALOG_ORGUNIT_KIND = "OrgUnitKinds";
    String CATALOG_ORGUNIT_KIND_PRODUCING = "1";
    String CATALOG_ORGUNIT_KIND_FORMING = "2";
    String CATALOG_ORGUNIT_KIND_TERRITORIAL = "3";

    String ICO_EDIT_CASE = "edit_case";

    @Deprecated String CATALOG_TEMPLATE_DOCUMENTS = "TemplateDocuments";
    String TEMPLATE_STUDENT_PERSON_CARD_REPORT = "studentPersonCard";
    String TEMPLATE_EMPLOYEE_POST_REFERENCE = "employeePostReference";
    String TEMPLATE_EMPLOYEE_POST_GLOB_PERMISSIONS_PAR = "employeePostGlobPermissionsPar";
    String TEMPLATE_GROUP_STUDENT_LIST = "groupStudentList";
    String TEMPLATE_STUDENT_SAMPLE = "studentSample";
    String TEMPLATE_STUDENTS_COURSES_SPECIALITIES_ALLOCATION = "studentsCoursesSpecialitiesAllocationReport";
    String TEMPLATE_STUDENTS_AGE_SEX_DISTRIBUTION = "studentsAgeSexDistributionReport";
    String TEMPLATE_STUDENTS_GROUPS_LIST = "studentsGroupsList";
    String TEMPLATE_EMPLOYMENT_CONTRACT_TERMINATION_NOTICE = "employmentContractsTerminationNotice";


    @Deprecated String CATALOG_EMPLOYEE_ROSTER_TEMPLATE_DOCUMENTS = "EmployeeRosterTemplateDocuments";


    @Deprecated
    String EMPLOYEE_TYPE_PPS_CODE = EmployeeTypeCodes.EDU_STAFF;
    @Deprecated
    String EMPLOYEE_TYPE_WS_CODE = EmployeeTypeCodes.WORK_STAFF;
    @Deprecated
    String EMPLOYEE_TYPE_WS_AUP_CODE = EmployeeTypeCodes.MANAGEMENT_STAFF;
    @Deprecated
    String EMPLOYEE_TYPE_WS_UVP_CODE = EmployeeTypeCodes.SUPPORT_STAFF;

    @Deprecated
    String EMPLOYEE_WEEK_WORK_LOAD_40 = EmployeeWeekWorkLoadCodes.TITLE_40;
    @Deprecated
    String EMPLOYEE_WEEK_WORK_LOAD_36 = EmployeeWeekWorkLoadCodes.TITLE_36;

    @Deprecated String CATALOG_EMPLOYEE_WORK_WEEK_DURATION = "EmployeeWorkWeekDuration";

    @Deprecated
    String EMPLOYEE_WORK_WEEK_DURATION_5 = EmployeeWorkWeekDurationCodes.WEEK_5;
    @Deprecated
    String EMPLOYEE_WORK_WEEK_DURATION_6 = EmployeeWorkWeekDurationCodes.WEEK_6;

    @Deprecated String CATALOG_COMPETITION_ASSIGNMENT_TYPE = "CompetitionAssignmentType";
    String COMPETITION_ASSIGNMENT_TYPE_BY_COMPETITION_RESULTS = "1";
    String COMPETITION_ASSIGNMENT_TYPE_ELECTION_PASSED = "2";
    String COMPETITION_ASSIGNMENT_TYPE_BEFORE_COMPETITION = "3";
    String COMPETITION_ASSIGNMENT_TYPE_BEFORE_ELECTION = "4";

    @Deprecated String CATALOG_STUDENT_CATEGORIES = StudentCategory.CATALOG_STUDENT_CATEGORIES;
    String STUDENT_CATEGORY_STUDENT = StudentCategory.STUDENT_CATEGORY_STUDENT;
    String STUDENT_CATEGORY_LISTENER = StudentCategory.STUDENT_CATEGORY_LISTENER;
    String STUDENT_CATEGORY_SECOND_HIGH = StudentCategory.STUDENT_CATEGORY_SECOND_HIGH;

    @Deprecated String CATALOG_EDUCATION_LEVEL_STAGE = "EducationLevelStage";
    String EDUCATION_LEVEL_STAGE_PRESCHOOL_EDUCATION = "10"; // Дошкольное образование
    String EDUCATION_LEVEL_STAGE_BEGIN_EDUCATION = "20"; // Начальное общее образование
    String EDUCATION_LEVEL_STAGE_BASE_EDUCATION = "30"; // Основное общее образование
    String EDUCATION_LEVEL_STAGE_FULL_EDUCATION = "40"; // Среднее (полное) общее образование
    String EDUCATION_LEVEL_STAGE_BEGIN_PROFILE = "50"; // Начальное профессиональное
    String EDUCATION_LEVEL_STAGE_FULL_PROFILE = "60"; // Среднее профессиональное
    String EDUCATION_LEVEL_STAGE_VOCATIONAL_TRAINING = "70"; // Профессиональная подготовка
    String EDUCATION_LEVEL_STAGE_HIGH_PROFILE = "80"; // Высшее профессиональное
    String EDUCATION_LEVEL_STAGE_HALF_HIGH = "81"; // Неполное высшее
    String EDUCATION_LEVEL_STAGE_BACHELORS_DEGREE = "82"; // Бакалавриат
    String EDUCATION_LEVEL_STAGE_CERTIFICATED_SPEICALISTS_TRAINING = "83"; // Подготовка дипломированных специалистов
    String EDUCATION_LEVEL_STAGE_MASTERS_DEGREE = "84"; // Магистратура
    String EDUCATION_LEVEL_STAGE_ADDITIONAL_EDUCATION = "90"; // Дополнительное образование
    String EDUCATION_LEVEL_STAGE_GRADUATE_PROFESSIONAL_EDUCATION = "100"; // Послевузовское профессиональное образование

    @Deprecated String CATALOG_SESSION_TYPE = "SessionType";
    String SESSION_TYPE_WINTER = "1";
    String SESSION_TYPE_SUMMER = "2";
    String SESSION_TYPE_SPRING = "3";

    @Deprecated String CATALOG_SALARY_RAISING_COEFFICIENT = "SalaryRaisingCoefficient";
    @Deprecated String SALARY_RAISING_COEFFICIENT_DEFAULT = "0";

    @Deprecated String CATALOG_SCIENCE_DEGREE_TYPE = "ScienceDegreeType";
    String SCIENCE_DEGREE_TYPE_DOCTOR = "1";
    String SCIENCE_DEGREE_TYPE_MASTER = "2";

    @Deprecated String CATALOG_SCIENCE_STATUS_TYPE = "ScienceStatusType";
    String SCIENCE_STATUS_TYPE_PROFESSOR = "3";
    String SCIENCE_STATUS_TYPE_DOCENT = "4";

    @Deprecated String CATALOG_SCIENCE_DEGREE_CODE = "ScienceDegree";
    @Deprecated String CATALOG_SCIENCE_STATUS_CODE = "ScienceStatus";

    @Deprecated String CATALOG_YEAR_DISTRIBUTION = "YearDistribution";
    String YEAR_DISTRIBUTION_SEMESTERS = "2";
    String YEAR_DISTRIBUTION_WINTER_TERM = "21";
    String YEAR_DISTRIBUTION_SUMMER_TERM = "22";

    //владелец настроек модуля uniemp
    @Deprecated String UNI_MODULE_OWNER = "uniModuleOwner";

    //имя настройки в которой хранится признак используемости отчета
    @Deprecated String IS_REPORT_USED = "isReportUsed";

    String COLOR_ERROR = "#ffe8f3";
    String COLOR_GREEN = "#c5f4bd"; // # 7435 #
    String COLOR_CYAN = "#aadce1";  // # 7435 #
    String COLOR_BLUE = "#d8dee8";  // # 7435 #
    String COLOR_RED = COLOR_ERROR; // "#fab5b0";   // # 7435 #
    String COLOR_YELLOW = "#ffffcc";

    String COLOR_CELL_ERROR = "#DB0000";
    String COLOR_CELL_WARNING = "#000000"; // DEV-2681: "#531b00";

    Integer CITIZENSHIP_NO = -1;
}