/* $Id:$ */
package ru.tandemservice.uni.base.bo.PpsEntry.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/17/12
 */
public class PpsEntrySelectBlockData
{
    private static final IdentifiableWrapper<IEntity> SOURCE_OWNER_ORGUNIT = new IdentifiableWrapper<IEntity>(1L, "С читающих подразделений");
    private static final IdentifiableWrapper<IEntity> SOURCE_ALL_ORGUNIT = new IdentifiableWrapper<IEntity>(2L, "Со всех подразделений");
    private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(
        PpsEntrySelectBlockData.SOURCE_OWNER_ORGUNIT,
        PpsEntrySelectBlockData.SOURCE_ALL_ORGUNIT
    );

    public PpsEntrySelectBlockData(Collection<Long> orgUnitIds) {
        setOrgUnitIds(orgUnitIds);
        initFieldTitles();
        setPpsModel(new PpsEntryModel()
        {
            @Override
            protected DQLSelectBuilder filter(String alias, DQLSelectBuilder ppsDQL)
            {
                ppsDQL.where(isNull(property(PpsEntry.removalDate().fromAlias(alias))));
                if (SOURCE_OWNER_ORGUNIT.equals(getSourceFilter())) {
                    ppsDQL.where(or(
                        in(property(PpsEntry.orgUnit().id().fromAlias(alias)), getOrgUnitIds()),
                        in(property(alias), getInitiallySelectedPps())));
                }
                return ppsDQL;
            }
        });
    }

    public PpsEntrySelectBlockData(Long orgUnitId)
    {
        this(Collections.singletonList(orgUnitId));
    }

    private Collection<Long> orgUnitIds;
    private boolean multiSelect;

    private String ppsSourceFilterFieldTitle;
    private String ppsFieldTitle;

    private Object selectedPps;
    private List<PpsEntry> initiallySelectedPps;

    private ISelectModel ppsModel;

    public IdentifiableWrapper<IEntity> sourceFilter = PpsEntrySelectBlockData.SOURCE_OWNER_ORGUNIT;

    private void initFieldTitles()
    {
        setPpsFieldTitle(isMultiSelect() ? "Преподаватели" : "Преподаватель");
        setPpsSourceFilterFieldTitle("Выбор преподавателей");
    }

    public PpsEntry getSingleSelectedPps() {
        if (isMultiSelect())
            throw new IllegalStateException();
        return (PpsEntry) getSelectedPps();
    }

    @SuppressWarnings("unchecked")
    public List<PpsEntry> getSelectedPpsList() {
        if (!isMultiSelect())
            throw new IllegalStateException();
        return (List<PpsEntry>) getSelectedPps();
    }

    public List<IdentifiableWrapper<IEntity>> getSourceFilterList() {
        return PpsEntrySelectBlockData.SOURCE_FILTER_LIST;
    }

    public boolean isMultiSelect()
    {
        return multiSelect;
    }

    public PpsEntrySelectBlockData setMultiSelect(boolean multiSelect)
    {
        this.multiSelect = multiSelect;
        return this;
    }

    public Object getSelectedPps()
    {
        return selectedPps;
    }

    public void setSelectedPps(Object selectedPps)
    {
        this.selectedPps = selectedPps;
    }

    public ISelectModel getPpsModel()
    {
        return ppsModel;
    }

    public void setPpsModel(ISelectModel ppsModel)
    {
        this.ppsModel = ppsModel;
    }

    public IdentifiableWrapper<IEntity> getSourceFilter()
    {
        return sourceFilter;
    }

    public void setSourceFilter(IdentifiableWrapper<IEntity> sourceFilter)
    {
        this.sourceFilter = sourceFilter;
    }

    public String getPpsSourceFilterFieldTitle()
    {
        return ppsSourceFilterFieldTitle;
    }

    public void setPpsSourceFilterFieldTitle(String ppsSourceFilterFieldTitle)
    {
        this.ppsSourceFilterFieldTitle = ppsSourceFilterFieldTitle;
    }

    public String getPpsFieldTitle()
    {
        return ppsFieldTitle;
    }

    public void setPpsFieldTitle(String ppsFieldTitle)
    {
        this.ppsFieldTitle = ppsFieldTitle;
    }

    public Collection<Long> getOrgUnitIds() {
        return orgUnitIds;
    }

    public PpsEntrySelectBlockData setOrgUnitIds(Collection<Long> orgUnitIds) {
        this.orgUnitIds = orgUnitIds;
        return this;
    }

    public List<PpsEntry> getInitiallySelectedPps()
    {
        return initiallySelectedPps;
    }

    public PpsEntrySelectBlockData setInitiallySelectedPps(List<PpsEntry> initiallySelectedPps)
    {
        this.initiallySelectedPps = initiallySelectedPps;
        setSelectedPps(initiallySelectedPps);

        if (null != initiallySelectedPps && null != getOrgUnitIds()) {
            for (PpsEntry entry : initiallySelectedPps) {
                if (entry.getOrgUnit() == null || !getOrgUnitIds().contains(entry.getOrgUnit().getId()))
                    setSourceFilter(SOURCE_ALL_ORGUNIT);
            }
        }
        return this;
    }

    public PpsEntrySelectBlockData setInitiallySelectedPps(PpsEntry initiallySelectedPps)
    {
        setInitiallySelectedPps(initiallySelectedPps == null ? null : Collections.singletonList(initiallySelectedPps));
        setSelectedPps(initiallySelectedPps);
        return this;
    }

}

