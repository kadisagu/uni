/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;



/**
 * @author vip_delete
 */
public class Test
{
    public static void main(String[] args) throws Exception
    {
        //ScriptEngineManager factory = new ScriptEngineManager();
        //ScriptEngine engine = factory.getEngineByName("groovy");
        //
        //// basic example
        //System.out.println(engine.eval("(1..10).sum()"));
        //
        //// example showing scripting variables
        //engine.put("first", "HELLO");
        //engine.put("second", "world");
        //Class a = (Class)engine.eval("" +
        //        "class A {def a;\ndef b;A(def a, def b){this.a=a;this.b=b;print(a);print(b);println('!');}\ndef printx(){print(a.toLowerCase() + b.toUpperCase())}}" +
        //        "");
        //
        //a.getMethod("printx").invoke(a.getConstructor(Object.class, Object.class).newInstance("aaaaaaaaaaaa", "bb"));

        //System.out.println(new SelectFormat("{один}").format("один"));
        //NumberFormat format = NumberFormat.getCurrencyInstance(new ULocale("ru_RU"));
        //System.out.println(format.format(new CurrencyAmount(21.22, Currency.getInstance("RUB"))));
    }

    //public static void main(String[] args) throws Exception
    //{
    //    List<Connection> connList = Arrays.asList(
    //            DriverManager.getConnection("jdbc:postgresql://192.168.201.251:5432/product_uni_trunk", "postgres", "postgres"),
    //            DriverManager.getConnection("jdbc:postgresql://192.168.201.251:5432/demo", "postgres", "postgres"),
    //            DriverManager.getConnection("jdbc:postgresql://192.168.201.233:5433/demo_trunk", "postgres", ""),
    //            DriverManager.getConnection("jdbc:postgresql://192.168.201.233:5433/analytics_uni", "postgres", "")
    //            //DriverManager.getConnection("jdbc:postgresql://localhost:5432/product_uni", "postgres", "postgres")
    //    );
    //
    //    try
    //    {
    //        for (Connection conn : connList)
    //        {
    //            conn.setAutoCommit(false);
    //
    //            // сбрасываем все логины
    //            Statement s = conn.createStatement();
    //            s.executeUpdate("update principal_t set login_p = id");
    //            s.close();
    //
    //            Set<String> usedLoginSet = new HashSet<String>();
    //            Map<Long, String> id2login = new HashMap<Long, String>();
    //
    //            s = conn.createStatement();
    //            ResultSet r = s.executeQuery("select i.lastName_p, i.firstName_p, i.middleName_p, pr.id from person_t p inner join identityCard_t i on p.identityCard_id = i.id inner join person2principalrelation_t r on p.id=r.person_id inner join principal_t pr on r.principal_id = pr.id order by i.lastName_p, i.firstName_p, i.middleName_p, i.id");
    //            while (r.next())
    //            {
    //                String lastName = r.getString(1);
    //                String firstName = r.getString(2);
    //                String middleName = r.getString(3);
    //                long principalId = r.getLong(4);
    //
    //                final String generatedLogin = UniSecurityUtils.generateLogin(firstName, lastName, middleName);
    //
    //                String uniqueLogin = generatedLogin;
    //                int postfix = 1;
    //                while (!usedLoginSet.add(uniqueLogin))
    //                    uniqueLogin = generatedLogin + postfix++;
    //
    //                id2login.put(principalId, uniqueLogin);
    //            }
    //            s.close();
    //
    //            PreparedStatement ps = conn.prepareStatement("update principal_t set login_p = ? where id = ?");
    //            int i = 0;
    //            for (Map.Entry<Long, String> entry : id2login.entrySet())
    //            {
    //                ps.setString(1, entry.getValue());
    //                ps.setLong(2, entry.getKey());
    //                ps.addBatch();
    //                i++;
    //                if ((i & 0x3FF) == 0)
    //                {
    //                    System.out.print(".");
    //                    ps.executeBatch();
    //                }
    //            }
    //            System.out.print(".");
    //            ps.executeBatch();
    //            ps.close();
    //
    //            conn.commit();
    //        }
    //    } finally
    //    {
    //        for (Connection conn : connList)
    //            conn.close();
    //    }
    //}

    //public static void main(String[] args) throws Exception
    //{
    //    List<File> list = CoreFileUtils.listAllFiles(new File("Z:/tu"), new ArrayList<File>(), new FileFilter()
    //    {
    //        @Override
    //        public boolean accept(File file)
    //        {
    //            String path = file.getAbsolutePath();
    //            if (!path.endsWith("Model.java")) return false;
    //            if (!path.contains("\\src\\main\\java\\ru\\tandemservice\\")) return false;
    //
    //            int j = path.lastIndexOf('\\');
    //            int i = path.lastIndexOf('\\', j - 1);
    //            String componentName = path.substring(i + 1, j);
    //
    //            return componentName.contains("Add") || componentName.contains("Edit");
    //        }
    //    });
    //
    //    String from1 = "import org.tandemframework.core.component.State;";
    //    String to1 = "import org.tandemframework.core.component.Input;";
    //
    //    String from2 = "@State";
    //    String to2 = "@Input";
    //
    //    for (File file : list)
    //    {
    //        String data = IOUtils.toString(new FileInputStream(file));
    //        data = data.replace(from1, to1);
    //
    //        data = data.replace(from2, to2);
    //        IOUtils.write(data, new FileOutputStream(file));
    //    }
    //}
    //
    //@SuppressWarnings("unchecked")
    //public static void main(String[] args) throws Exception
    //{
    //    EntityRuntime.getInstance();
    //    List<File> list = CoreFileUtils.listAllFiles(new File("Z:/tu"), new ArrayList<File>(), new FileFilter()
    //    {
    //        @Override
    //        public boolean accept(File file)
    //        {
    //            String path = file.getAbsolutePath();
    //            if (!path.endsWith("Controller.java")) return false;
    //            if (!path.contains("\\src\\main\\java\\ru\\tandemservice\\")) return false;
    //
    //            //if (!path.contains("ru\\tandemservice\\unitj\\component\\orgunit\\CathedraStatusJournalList")) return false;
    //            return true;
    //        }
    //    });
    //
    //    List<String> rows;
    //    List<String> newRows;
    //    for (File file : list)
    //    {
    //        boolean hasChanges = false;
    //        rows = IOUtils.readLines(new FileInputStream(file));
    //
    //        newRows = new ArrayList<String>(rows.size());
    //        for (String row : rows)
    //        {
    //            if (row.contains(".addColumn("))
    //            {
    //
    //                int i = row.indexOf("new String");
    //
    //                if (i > 0)
    //                {
    //                    int j = row.indexOf('{', i + 1);
    //                    int k = row.indexOf('}', j + 1);
    //
    //                    String dsl = getDSL(row.substring(j + 1, k));
    //                    if (dsl != null)
    //                    {
    //                        hasChanges = true;
    //                        newRows.add(row.substring(0, i) + dsl + row.substring(k + 1, row.length()));
    //                    } else
    //                    {
    //                        newRows.add(row);
    //                    }
    //                } else
    //                {
    //                    newRows.add(row);
    //                }
    //            } else
    //            {
    //                newRows.add(row);
    //            }
    //        }
    //
    //        if (hasChanges)
    //        {
    //            FileUtils.writeLines(file, "UTF-8", newRows);
    //        }
    //    }
    //}
    //
    //private static String getDSL(String arr)
    //{
    //    String[] split = StringUtils.split(arr, ",");
    //
    //    StringBuilder result = new StringBuilder();
    //
    //    IPropertyMeta lastPropertyMeta = null;
    //    String lastPropertyName = null;
    //    for (int h = 0; h < split.length; h++)
    //    {
    //        String s = split[h];
    //        int i = s.indexOf('.');
    //        if (i < 0) return null;
    //
    //        String entityName = s.substring(0, i).trim();
    //
    //        if (entityName.equals("ICatalogDefines"))
    //            entityName = lastPropertyMeta.getSimpleValueClassName();
    //
    //        if (entityName.endsWith("Gen"))
    //            entityName = entityName.substring(0, entityName.length() - 3);
    //
    //        IEntityMeta m = EntityRuntime.getMeta(entityName);
    //        if (m == null) return null;
    //
    //        if (h == 0)
    //            result.append(m.getSimpleClassName());
    //
    //        if (lastPropertyMeta != null && !m.getSimpleClassName().equals(lastPropertyMeta.getSimpleValueClassName()))
    //            throw new RuntimeException("Meta is not eq: " + arr + "; need=" + lastPropertyMeta.getSimpleValueClassName() + "; found=" + m.getSimpleClassName());
    //
    //        String propertyUpper = s.substring(i + 1, s.length()).trim();
    //        if (propertyUpper.startsWith("L_"))
    //            propertyUpper = propertyUpper.substring(10, propertyUpper.length());
    //        else if (propertyUpper.startsWith("P_"))
    //            propertyUpper = propertyUpper.substring(9, propertyUpper.length());
    //
    //        String propertyName = lowerStyleString(propertyUpper);
    //        IPropertyMeta propertyMeta = m.getProperty(propertyName);
    //        if (propertyMeta == null) return null;
    //        lastPropertyMeta = propertyMeta;
    //        lastPropertyName = propertyName;
    //
    //        result.append("." + lastPropertyName);
    //
    //        if (h + 1 == split.length)
    //            result.append("().s()");
    //        else
    //            result.append("()");
    //    }
    //
    //    return result.toString();
    //}
    //
    //private static String lowerStyleString(String source)
    //{
    //    StringBuilder str = new StringBuilder();
    //    for (int i = 0; i < source.length(); i++)
    //    {
    //        char current = source.charAt(i);
    //        if (current == '_')
    //        {
    //            i++;
    //            str.append(Character.toUpperCase(source.charAt(i)));
    //        } else
    //        {
    //            str.append(Character.toLowerCase(current));
    //        }
    //    }
    //    return str.toString();
    //}
}
