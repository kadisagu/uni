/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;

import ru.tandemservice.uni.dao.IUniBaseDao;

/**
 * @author agolubenko
 * @since 20.03.2008
 */
public abstract class UniSelectModel<T extends IEntity, IDAO extends IUniBaseDao, Model> extends BaseSingleSelectModel
{
    protected final Model _model;
    protected final IDAO _dao;

    public UniSelectModel(Model model, IDAO dao)
    {
        _model = model;
        _dao = dao;
    }

    public UniSelectModel(Model model, IDAO dao, String labelProperty)
    {
        super(labelProperty);
        _model = model;
        _dao = dao;
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        T value = _dao.<T> get((Long) primaryKey);
        return (findValues("").getObjects().contains(value)) ? value : null;
    }
}
