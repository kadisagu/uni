/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.ui.EduOrgUnitCorrection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationJournal.OrgUnitReorganizationJournal;
import ru.tandemservice.uni.base.bo.UniReorganization.ui.EduOrgUnitCorrectionEduOrgUnitTab.UniReorganizationEduOrgUnitCorrectionEduOrgUnitTab;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
@Configuration
public class UniReorganizationEduOrgUnitCorrection extends BusinessComponentManager {

    public static final String TAB_PANEL = "eduOrgUnitCorrectionTabPanel";


    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab("eduOrgTab", UniReorganizationEduOrgUnitCorrectionEduOrgUnitTab.class))
                .addTab(componentTab("journalTab", OrgUnitReorganizationJournal.class).permissionKey("orgUnitReorganizationJournal").after("eduOrgTab").parameters("ui:journalParams"))
                .create();
    }

}
