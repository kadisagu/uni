package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlFixPage;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

public class UniEduProgramEduLvlFixPageUI extends UIPresenter
{
    private final boolean hasEc = (null != EntityRuntime.getMeta("enrollmentDirection"));

    // для каждого НПм нужно уметь менять ссылку на направленность/направление
    // для каждого НПв нужно уметь менять ссылку на НПм


    /** key -> { eduLvl.id }, в идеале все наборы eduLvl.id по ключам должны быть уникальны */
    private final Map<String, Set<Long>> subjectKey2EduLvlIdsMap = new HashMap<>();
    private final Map<String, String> subjectKey2TitleMap = new HashMap<>();


    public static class EduOuRow
    {
        public EduOuRow(final EducationOrgUnit eduOu) {
            super();
            this.eduOu = eduOu;
        }

        private final EducationOrgUnit eduOu;
        public EducationOrgUnit getEduOu() { return this.eduOu; }

        private int activeStudentCount;
        public int getActiveStudentCount() { return this.activeStudentCount; }
        public void setActiveStudentCount(final int activeStudentCount) { this.activeStudentCount = activeStudentCount; }

        private int archiveStudentCount;
        public int getArchiveStudentCount() { return this.archiveStudentCount; }
        public void setArchiveStudentCount(final int archiveStudentCount) { this.archiveStudentCount = archiveStudentCount; }

    }


    // группа ФУТС (НПП)
    public static class EduOuFCTPRow
    {
        public EduOuFCTPRow(final String title) {
            this.title = title;
        }

        private final String title;
        public String getTitle() { return this.title; }

        private final Map<EducationOrgUnit, EduOuRow> eduOuMap = new LinkedHashMap<>(4);
        public Map<EducationOrgUnit, EduOuRow> getEduOuMap() { return this.eduOuMap; }
        public Collection<Map.Entry<EducationOrgUnit, EduOuRow>> getEduOuEntries() { return this.getEduOuMap().entrySet(); }

        public String getDevelopOrgUnitShortTitle() {
            final StringBuilder sb = new StringBuilder();
            for (final EducationOrgUnit ou: getEduOuMap().keySet()) {
                if (sb.length() > 0) { sb.append(", "); }
                sb.append(ou.getFormativeOrgUnit().getShortTitle());
                if (!(ou.getTerritorialOrgUnit() instanceof TopOrgUnit)) {
                    sb.append(" (").append(ou.getTerritorialOrgUnit().getTerritorialShortTitle()).append(")");
                }
            }
            return sb.toString();
        }

        private int _cached_active_count = -1;
        public int getActiveStudentCount() {
            if (_cached_active_count < 0) {
                int count = 0;
                for (final EduOuRow r: getEduOuMap().values()) {
                    count += r.getActiveStudentCount();
                }
                return _cached_active_count = count;
            }
            return _cached_active_count;
        }

        private int _cached_archive_count = -1;
        public int getArchiveStudentCount() {
            if (_cached_archive_count < 0) {
                int count = 0;
                for (final EduOuRow r: getEduOuMap().values()) {
                    count += r.getArchiveStudentCount();
                }
                return _cached_archive_count = count;
            }
            return _cached_archive_count;
        }

        public boolean isEmpty() {
            return getActiveStudentCount() <= 0 && getArchiveStudentCount() <= 0;
        }

    }


    // НПв
    public static class EduHsRow
    {
        public EduHsRow(final EducationLevelsHighSchool eduHs) {
            this.eduHs = eduHs;
            //this.setTargetEduLvl(eduHs.getEducationLevel());
        }

        private final EducationLevelsHighSchool eduHs;
        public EducationLevelsHighSchool getEduHs() { return this.eduHs; }

        //private EducationLevels targetEduLvl;
        //public EducationLevels getTargetEduLvl() { return this.targetEduLvl; }
        //public void setTargetEduLvl(EducationLevels targetEduLvl) { this.targetEduLvl = targetEduLvl; }

        private final Map<String, EduOuFCTPRow> eduOuFCTPMap = new LinkedHashMap<>(4);
        public Map<String, EduOuFCTPRow> getEduOuFCTPMap() { return this.eduOuFCTPMap; }
        public Collection<Map.Entry<String, EduOuFCTPRow>> getEduOuFCTPEntries() { return this.getEduOuFCTPMap().entrySet(); }

        // приемная кампания -> (что-то)
        private final Map<String, Integer> ecMap = new LinkedHashMap<>(2);
        public Map<String, Integer> getEcMap() { return this.ecMap; }
        public String getEcNameList() { return StringUtils.join(getEcMap().keySet(), ", "); }

        // используется для фильтрации (чтобы не перегружать весь список из базы заново)
        private boolean hidden = false;
        public boolean isHidden() { return this.hidden; }
        public void setHidden(final boolean hidden) { this.hidden = hidden; }

        private Boolean _cached_empty;
        public boolean isEmpty() {
            if (null == _cached_empty) {
                boolean empty = true;
                for (final EduOuFCTPRow eduOuFCTFRow: getEduOuFCTPMap().values()) {
                    if (!eduOuFCTFRow.isEmpty()) {
                        empty = false;
                        break;
                    }
                }
                if (ecMap.size() > 0) {
                    empty = false;
                }
                return _cached_empty = empty;
            }
            return _cached_empty;
        }

    }

    // НПм
    public static class EduLvlRow
    {
        public EduLvlRow(final EducationLevels eduLvl) {
            this.eduLvl = eduLvl;
            //this.setTargetSubject(eduLvl.getEduProgramSubject());
            //this.setTargetSpecialization(eduLvl.getEduProgramSpecialization());
        }

        private final EducationLevels eduLvl;
        public EducationLevels getEduLvl() { return this.eduLvl; }


        public String getEduProgramSubjectTitleWithSpecialization()
        {
            EduProgramSubject subject = getEduLvl().getEduProgramSubject();
            EduProgramSpecialization specialization = getEduLvl().getEduProgramSpecialization();
            if (null == subject) { return ""; }

            // напрввление
            StringBuilder sb = new StringBuilder(subject.getTitleWithCode());

            // направленность если есть
            if (null != specialization) {
                if (specialization.isRootSpecialization()) {
                    sb.append(" (общая направленность)");
                } else {
                    sb.append(" (направленность: ").append(specialization.getTitle()).append(")");
                }
            }

            // перечень
            sb.append(", ").append(subject.getSubjectIndex().getShortTitle());

            return sb.toString();
        }

        //private EduProgramSubject targetSubject;
        //public EduProgramSubject getTargetSubject() { return this.targetSubject; }
        //public void setTargetSubject(EduProgramSubject targetSubject) { this.targetSubject = targetSubject; }

        //private EduProgramSpecialization targetSpecialization;
        //public EduProgramSpecialization getTargetSpecialization() { return this.targetSpecialization; }
        //public void setTargetSpecialization(EduProgramSpecialization targetSpecialization) { this.targetSpecialization = targetSpecialization; }

        private final Map<EducationLevelsHighSchool, EduHsRow> eduHsMap = new LinkedHashMap<>(4);
        public Map<EducationLevelsHighSchool, EduHsRow> getEduHsMap() { return this.eduHsMap; }
        public Collection<Map.Entry<EducationLevelsHighSchool, EduHsRow>> getEduHsEntries() { return getEduHsMap().entrySet(); }

        // используется для фильтрации (чтобы не перегружать весь список из базы заново)
        private boolean hidden = false;
        public boolean isHidden() { return this.hidden; }
        public void setHidden(final boolean hidden) { this.hidden = hidden; }


        private String _cached_subjectUniqKey;
        public String getSubjectUniqKey()
        {
            if (null == this._cached_subjectUniqKey) {
                final StringBuilder keyBuilder = new StringBuilder();
                if (null != eduLvl.getEduProgramSubject()) {
                    final StringBuilder titleBuilder = new StringBuilder();
                    keyBuilder.append(Long.toString(eduLvl.getEduProgramSubject().getId(), 32));
                    titleBuilder.append(eduLvl.getEduProgramSubject().getTitleWithCode());
                    if (null != eduLvl.getEduProgramSpecialization()) {
                        keyBuilder.append("-").append(Long.toString(eduLvl.getEduProgramSpecialization().getId(), 32));
                        if (eduLvl.getEduProgramSpecialization() instanceof EduProgramSpecializationChild) {
                            titleBuilder.append(", спец. ").append((eduLvl.getEduProgramSpecialization().getTitle()));
                        }
                    }
                    if (eduLvl instanceof EducationLevelMiddle) {
                        // для СПО - добавляем квалификацию (базовый\расширенный)
                        keyBuilder.append("-").append(eduLvl.getQualification().getCode());
                        titleBuilder.append(" (").append(eduLvl.getQualification().getTitle()).append(")");
                    }

                    keyBuilder.append("\n").append(titleBuilder);
                }
                return (this._cached_subjectUniqKey = keyBuilder.toString());
            }
            return this._cached_subjectUniqKey;
        }

        public String getSubjectUniqKeyTitle() {
            if (null == eduLvl.getEduProgramSubject()) { return null; }

            final StringBuilder titleBuilder = new StringBuilder();
            titleBuilder.append(eduLvl.getEduProgramSubject().getTitleWithCode());
            if (null != eduLvl.getEduProgramSpecialization()) {
                if (eduLvl.getEduProgramSpecialization() instanceof EduProgramSpecializationChild) {
                    titleBuilder.append(", спец. ").append((eduLvl.getEduProgramSpecialization().getTitle()));
                }
            }
            if (eduLvl instanceof EducationLevelMiddle) {
                // для СПО - добавляем квалификацию (базовый\расширенный)
                titleBuilder.append(" (").append(eduLvl.getQualification().getTitle()).append(")");
            }

            return titleBuilder.toString();
        }


        private Boolean _cached_empty;
        public boolean isEmpty() {
            if (null == _cached_empty) {
                boolean empty = true;
                for (final EduHsRow eduHsRow: getEduHsMap().values()) {
                    if (!eduHsRow.isEmpty()) {
                        empty = false;
                        break;
                    }
                }
                return _cached_empty = empty;
            }
            return _cached_empty;
        }

    }

    private final Map<EducationLevels, EduLvlRow> eduLvlMap = new LinkedHashMap<>();
    public Map<EducationLevels, EduLvlRow> getEduLvlMap() { return this.eduLvlMap; }
    public Collection<Map.Entry<EducationLevels, EduLvlRow>> getEduLvlEntries() { return this.getEduLvlMap().entrySet(); }

    private Map.Entry<EducationLevels, EduLvlRow> eduLvlEntry;
    public Map.Entry<EducationLevels, EduLvlRow> getEduLvlEntry() { return this.eduLvlEntry; }
    public void setEduLvlEntry(final Map.Entry<EducationLevels, EduLvlRow> eduLvlEntry) { this.eduLvlEntry = eduLvlEntry; }
    public EduLvlRow getEduLvlRow() { return getEduLvlEntry().getValue(); }
    public Collection<Map.Entry<EducationLevelsHighSchool, EduHsRow>> getEduHsEntries() { return getEduLvlRow().getEduHsEntries(); }

    public Set<Long> getEduLvl2SubjectUniqSet() {
        return subjectKey2EduLvlIdsMap.get(getEduLvlRow().getSubjectUniqKey());
    }

    private Map.Entry<EducationLevelsHighSchool, EduHsRow> eduHsEntry;
    public Map.Entry<EducationLevelsHighSchool, EduHsRow> getEduHsEntry() { return this.eduHsEntry; }
    public void setEduHsEntry(final Map.Entry<EducationLevelsHighSchool, EduHsRow> eduHsEntry) { this.eduHsEntry = eduHsEntry; }
    public EduHsRow getEduHsRow() { return getEduHsEntry().getValue(); }
    public Collection<Map.Entry<String, EduOuFCTPRow>> getEduOuFCTPEntries() { return this.getEduHsRow().getEduOuFCTPEntries(); }

    private Map.Entry<String, EduOuFCTPRow> eduOuFCTPEntry;
    public Map.Entry<String, EduOuFCTPRow> getEduOuFCTPEntry() { return this.eduOuFCTPEntry; }
    public void setEduOuFCTPEntry(final Map.Entry<String, EduOuFCTPRow> eduOuFCTPEntry) { this.eduOuFCTPEntry = eduOuFCTPEntry; }
    public EduOuFCTPRow getEduOuFCTPRow() { return getEduOuFCTPEntry().getValue(); }
    public Collection<Map.Entry<EducationOrgUnit, EduOuRow>> getEduOuEntries() { return this.getEduOuFCTPRow().getEduOuEntries(); }

    private Map.Entry<EducationOrgUnit, EduOuRow> eduOuEntry;
    public Map.Entry<EducationOrgUnit, EduOuRow> getEduOuEntry() { return this.eduOuEntry; }
    public void setEduOuEntry(final Map.Entry<EducationOrgUnit, EduOuRow> eduOuEntry) { this.eduOuEntry = eduOuEntry; }
    public EduOuRow getEduOuRow() { return this.getEduOuEntry().getValue(); }
    public EducationOrgUnit getEduOu() { return getEduOuRow().getEduOu(); }

    private ISelectModel eduGroupModel;
    public ISelectModel getEduGroupModel() { return this.eduGroupModel; }
    public void setEduGroupModel(final ISelectModel eduGroupModel) { this.eduGroupModel = eduGroupModel; }

    @SuppressWarnings("unchecked")
    protected Set<String> getEduGroupSet() {
        final Object group = this.getSettings().get("eduGroupList");
        if ((group instanceof Collection) && (((Collection)group).size() > 0)) { return new HashSet<>((Collection<String>)group); }
        return null;
    }

    // грузит все за один раз, фильтрация для показа - не в этом методе
    @SuppressWarnings("unused")
    private void reinit()
    {
        // очищаем все!
        eduLvlMap.clear();
        subjectKey2EduLvlIdsMap.clear();

        final IUniBaseDao dao = IUniBaseDao.instance.get();

        // preload
        Debug.begin("reinit-preload");
        dao.getList(DevelopForm.class);
        dao.getList(DevelopCondition.class);
        dao.getList(DevelopTech.class);
        dao.getList(DevelopPeriod.class);
        Debug.end();


        final Set<String> eduGroupNameSet = new TreeSet<>();
        eduGroupModel = UniBaseUtils.getTitleSelectModel(new SingleValueCache<List<String>>() {
            @Override protected List<String> resolve() {
                return new ArrayList<>(eduGroupNameSet);
            }
        });

        final Map<EducationLevels, EduLvlRow> eduLvlMap = this.eduLvlMap;

        Debug.begin("reinit-eduhs-load");
        final List<EducationLevelsHighSchool> eduHsList = dao.getList(
            new DQLSelectBuilder()
            .fromEntity(EducationLevelsHighSchool.class, "eduHs")
            .fetchPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("eduHs"), "eduLvl")
            .fetchPath(DQLJoinType.left, EducationLevelsHighSchool.educationLevel().eduProgramSubject().fromAlias("eduHs"), "eduLvlSubj")
            .fetchPath(DQLJoinType.left, EducationLevelsHighSchool.educationLevel().eduProgramSpecialization().fromAlias("eduHs"), "eduLvlSpec")
            .column(property("eduHs"))
            .order(property(EducationLevelsHighSchool.educationLevel().inheritedOkso().fromAlias("eduHs")))
            .order(property(EducationLevelsHighSchool.educationLevel().title().fromAlias("eduHs")))
            .order(property(EducationLevelsHighSchool.displayableTitle().fromAlias("eduHs")))
            .where(eq(property(EducationLevelsHighSchool.educationLevel().levelType().additional().fromAlias("eduHs")), value(Boolean.FALSE))) // ДПО игнорим
            .where(notInstanceOf("eduLvl", EducationLevelHighGos3s.class)) // 2013 игнорим
            .where(notInstanceOf("eduLvl", EducationLevelMiddleGos3s.class)) // 2013 игнорим
        );
        Debug.end();

        // TODO: отсортировать их в коде

        Debug.begin("reinit-eduhs-process");
        for (final EducationLevelsHighSchool eduHs: eduHsList)
        {
            // edu-lvl
            final EducationLevels eduLvl = eduHs.getEducationLevel();
            final EduLvlRow eduLvlRow = getEduLvlRow(eduLvlMap, eduLvl);

            // edu-hs
            final EduHsRow eduHsRow = getEduHsRow(eduLvlRow.getEduHsMap(), eduHs);

            // edu-lvl-root
            final String rootTitle = getEduLvlRootTitle(eduLvl);
            eduGroupNameSet.add(rootTitle);

            // uniq
            final String key = eduLvlRow.getSubjectUniqKey();
            Set<Long> set = subjectKey2EduLvlIdsMap.get(key);
            if (null == set) {
                subjectKey2EduLvlIdsMap.put(key, set = new HashSet<>(4));
                subjectKey2TitleMap.put(key, eduLvlRow.getSubjectUniqKeyTitle());
            }
            set.add(eduLvl.getId());


        }
        Debug.end();


        Debug.begin("reinit-eduou-load");
        final List<EducationOrgUnit> eduOuList = dao.getList(
            new DQLSelectBuilder()
            .fromEntity(EducationOrgUnit.class, "eduOu").column(property("eduOu"))
            .order(property(EducationOrgUnit.developForm().code().fromAlias("eduOu")))
            .order(property(EducationOrgUnit.developCondition().code().fromAlias("eduOu")))
            .order(property(EducationOrgUnit.developTech().code().fromAlias("eduOu")))
            .order(property(EducationOrgUnit.developPeriod().code().fromAlias("eduOu")))
            .order(property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eduOu")))
            .order(property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("eduOu")))
        );
        Debug.end();

        Debug.begin("reinit-eduou-student-load");
        final Map<Long, Object[]> eduOuActiveStudentRowMap = UniBaseDao.mapRows(dao.<Object[]>getList(
            new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
                .column(property(Student.educationOrgUnit().id().fromAlias("s")))
                .column(count(property(Student.id().fromAlias("s"))))
                .group(property(Student.educationOrgUnit().id().fromAlias("s")))
        ), 0);
        final Map<Long, Object[]> eduOuArchiveStudentRowMap = UniBaseDao.mapRows(dao.<Object[]>getList(
            new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.TRUE)))
                .column(property(Student.educationOrgUnit().id().fromAlias("s")))
                .column(count(property(Student.id().fromAlias("s"))))
                .group(property(Student.educationOrgUnit().id().fromAlias("s")))
        ), 0);
        Debug.end();

        Debug.begin("reinit-eduou-process");
        for (final EducationOrgUnit eduOu: eduOuList)
        {
            final EduHsRow eduHsRow = getEduHsRow(eduOu.getEducationLevelHighSchool());
            if (null == eduHsRow) { continue; /*пропустили выше*/ }

            final String fctpTitle = eduOu.getDevelopCombinationTitle();
            EduOuFCTPRow fctpRow = eduHsRow.getEduOuFCTPMap().get(fctpTitle);
            if (null == fctpRow) {
                eduHsRow.getEduOuFCTPMap().put(fctpTitle, fctpRow = new EduOuFCTPRow(fctpTitle));
            }

            EduOuRow eduOuRow = fctpRow.getEduOuMap().get(eduOu);
            if (null == eduOuRow) {
                fctpRow.getEduOuMap().put(eduOu, eduOuRow = new EduOuRow(eduOu));
            }

            final Object[] eduOuActiveStudentRow = eduOuActiveStudentRowMap.get(eduOu.getId());
            eduOuRow.setActiveStudentCount(null == eduOuActiveStudentRow ? 0 : ((Number)eduOuActiveStudentRow[1]).intValue());

            final Object[] eduOuArchiveStudentRow = eduOuArchiveStudentRowMap.get(eduOu.getId());
            eduOuRow.setArchiveStudentCount(null == eduOuArchiveStudentRow ? 0 : ((Number)eduOuArchiveStudentRow[1]).intValue());
        }
        Debug.end();

        // абитуриент (здесь нельзя использовать классы - так что делаем все константами)
        if (hasEc) {
            Debug.begin("reinit-eduou-ec");

            // направления
            final List<Object[]> rowsDir = dao.getList(
                    new DQLSelectBuilder()
                            .fromEntity("EnrollmentDirection", "ed")
                            .predicate(DQLPredicateType.distinct)
                            .column(property("ed.educationOrgUnit.educationLevelHighSchool.id"))
                            .column(property("ed.enrollmentCampaign.title"))
                            .column(property("ed.enrollmentCampaign.educationYear.intValue"))
                            .order(property("ed.enrollmentCampaign.educationYear.intValue"))
                            .order(property("ed.enrollmentCampaign.title"))
            );
            for (final Object[] row: rowsDir) {
                final EducationLevelsHighSchool eduHs = dao.get(EducationLevelsHighSchool.class, (Long)row[0]);
                final EduHsRow eduHsRow = getEduHsRow(eduHs);
                if (null == eduHsRow) { continue; /*пропустили выше*/ }

                final String ecTitle = (String)row[1];
                eduHsRow.getEcMap().put(ecTitle, null);
            }

            // профили
            final List<Object[]> rowsProf = dao.getList(
                    new DQLSelectBuilder()
                            .fromEntity("ProfileEducationOrgUnit", "peduou")
                            .joinPath(DQLJoinType.inner, "peduou.enrollmentDirection", "ed")
                            .predicate(DQLPredicateType.distinct)
                            .column(property("peduou.educationOrgUnit.educationLevelHighSchool.id"))
                            .column(property("ed.enrollmentCampaign.title"))
                            .column(property("ed.enrollmentCampaign.educationYear.intValue"))
                            .order(property("ed.enrollmentCampaign.educationYear.intValue"))
                            .order(property("ed.enrollmentCampaign.title"))
            );
            for (final Object[] row: rowsProf) {
                final EducationLevelsHighSchool eduHs = dao.get(EducationLevelsHighSchool.class, (Long)row[0]);
                final EduHsRow eduHsRow = getEduHsRow(eduHs);
                if (null == eduHsRow) { continue; /*пропустили выше*/ }

                final String ecTitle = (String)row[1];
                eduHsRow.getEcMap().put(ecTitle, null);
            }

            Debug.end();
        }

    }


    protected String getEduLvlRootTitle(final EducationLevels eduLvl) {
        EducationLevels root = eduLvl;
        while (root.getParentLevel() != null) {
            root = root.getParentLevel();
        }
        return StringUtils.trimToEmpty(root.getTitle()).toUpperCase();
    }

    @SuppressWarnings("deprecation")
    public boolean isTitleCodePrefixCorrect() {
        EducationLevels lvl = getEduLvlRow().eduLvl;
        String s1 = StringUtils.trimToEmpty(lvl.getInheritedOksoPrefix()); // здесь проверяется именно оно
        String s2 = StringUtils.trimToEmpty(lvl.getTitleCodePrefix());
        return s1.equals(s2);
    }

    private EduHsRow getEduHsRow(final EducationLevelsHighSchool eduHs)
    {
        final EducationLevels eduLvl = eduHs.getEducationLevel();
        final EduLvlRow eduLvlRow = eduLvlMap.get(eduLvl);
        if (null == eduLvlRow) { return null; }

        final EduHsRow eduHsRow = eduLvlRow.getEduHsMap().get(eduHs);
        if (null == eduHsRow) { return null; }

        return eduHsRow;
    }

    private EduHsRow getEduHsRow(final Map<EducationLevelsHighSchool, EduHsRow> eduHsMap, final EducationLevelsHighSchool eduHs) {
        EduHsRow eduHsRow = eduHsMap.get(eduHs);
        if (null == eduHsRow) {
            eduHsMap.put(eduHs, eduHsRow = new EduHsRow(eduHs));
        }
        return eduHsRow;
    }

    private EduLvlRow getEduLvlRow(final Map<EducationLevels, EduLvlRow> eduLvlMap, final EducationLevels eduLvl) {
        EduLvlRow eduLvlRow = eduLvlMap.get(eduLvl);
        if (null == eduLvlRow) {
            eduLvlMap.put(eduLvl, eduLvlRow = new EduLvlRow(eduLvl));
        }
        return eduLvlRow;
    }

    @Override
    public void onComponentRefresh()
    {
        // инициализацию делаем внутри dao-метода (чтобы коннекшен был один)
        IUniBaseDao.instance.get().getCalculatedValue(session -> {
            reinit();
            return null;
        });

        // и да, фльтруем
        onClickFilter();
    }

    public void onClickFilter()
    {
        // только расставляем hidden на нужные элементы

        // фльтрация по группе
        final Set<String> selectedGroupTitleSet = getEduGroupSet();
        for (final EduLvlRow lvlRow: getEduLvlMap().values()) {
            lvlRow.setHidden((null != selectedGroupTitleSet)  && !selectedGroupTitleSet.contains(getEduLvlRootTitle(lvlRow.getEduLvl())));
        }

        // если отфильтровать получилось - сохраняем
        getSettings().save();
    }

//    public void onClickSetSubject() {
//        Компонент удален, т.к. работает неправильно в новой сруктуре.
//        Если понадобиться - доставайте из истории svn ru.tandemservice.uni.base.bo.UniEduProgram.ui.SetEduLvlSubject.UniEduProgramSetEduLvlSubject
//        и реализуйте правильные методы ДАО (смена НПм у НПв, если меняется общая-необщая и т.д.).
//
//        getActivationBuilder()
//        .asRegion(UniEduProgramSetEduLvlSubject.class, "fixDialog")
//        .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
//        .activate();
//    }


    // удаление НПм = удаление всех его НПв, само НПм не удаляется
    public void onClickDeleteEmptyEduLvl()
    {
        getSupport().setRefreshScheduled(true); // всегда обновляем
        try {
            IUniBaseDao.instance.get().doInTransaction(session -> {
                final List<EducationLevelsHighSchool> eduHsList = IUniBaseDao.instance.get().getList(EducationLevelsHighSchool.class, EducationLevelsHighSchool.educationLevel().id(), getListenerParameterAsLong());
                for (final EducationLevelsHighSchool eduHs: eduHsList) {
                    IUniBaseDao.instance.get().delete(eduHs);
                }
                return null;
            });
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    // удаляем одно НПв
    public void onClickDeleteEmptyEduHs()
    {
        getSupport().setRefreshScheduled(true); // всегда обновляем
        try {
            IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public int getEmptyEduLvlCount() {
        return CollectionUtils.countMatches(getEduLvlMap().values(), EduLvlRow::isEmpty);
    }

    public int getEmptyEduHsCount() {
        int count = 0;
        for (final EduLvlRow lvlRow: getEduLvlMap().values()) {
            count += CollectionUtils.countMatches(lvlRow.getEduHsMap().values(), EduHsRow::isEmpty);
        }
        return count;
    }

    public int getEduLvlCount() {
        return getEduLvlMap().size();
    }

    public int getEduHsCount() {
        int i = 0;
        for (final EduLvlRow lvlRow: getEduLvlMap().values()) {
            i += lvlRow.getEduHsMap().size();
        }
        return i;
    }

    public int getEduLvl2EmptySubjectSize() {
        final Set<Long> set = subjectKey2EduLvlIdsMap.get("");
        return (null == set ? 0 : set.size());
    }

    public int getEduLvl2SubjectNonEmptySize() {
        return CollectionUtils.countMatches(subjectKey2EduLvlIdsMap.entrySet(), object -> !StringUtils.isEmpty(object.getKey()));
    }

    public int getEduLvl2SubjectNonUniqSize() {
        return CollectionUtils.countMatches(subjectKey2EduLvlIdsMap.entrySet(), object -> !StringUtils.isEmpty(object.getKey()) && (object.getValue().size() > 1));
    }

    public String getEduLvl2SubjectNonUniqRawString() {
        final List<Map.Entry<String, Set<Long>>> nonUniqList = new ArrayList<>(CollectionUtils.select(
            subjectKey2EduLvlIdsMap.entrySet(), object -> !StringUtils.isEmpty(object.getKey()) && (object.getValue().size() > 1)
        ));

        if (nonUniqList.isEmpty()) {
            return "нет";
        }

        Collections.sort(nonUniqList, (o1, o2) -> Integer.compare(o2.getValue().size(), o1.getValue().size()));


        int threshold = 3;
        final StringBuilder x = new StringBuilder();
        while (threshold-- > 0) {
            if (nonUniqList.isEmpty()) {
                break;
            }

            final Entry<String, Set<Long>> e = nonUniqList.remove(0);
            final String key = e.getKey();
            final String title = subjectKey2TitleMap.get(key);
            if (x.length() > 0) { x.append("</div><div>"); }
            x.append(title).append(" - ").append(e.getValue().size()).append(" НПм");
        }

        final StringBuilder sb = new StringBuilder();
        sb.append(nonUniqList.size()+threshold);
        sb.append("<div>");
        sb.append(x);
        sb.append("</div>");
        return sb.toString();
    }


}


