/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.DownloadStorableReport;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.component.reports.PrintReport.PrintFormDescription;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportControllerBase;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportModelBase;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.text.SimpleDateFormat;

/**
 * @author agolubenko
 * @since 14.10.2008
 */
@SuppressWarnings("unchecked")
public class Controller extends PrintReportControllerBase
{


    @Override
    protected PrintFormDescription getPrintFormDescription(final PrintReportModelBase model) {
        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                final IStorableReport report = UniDaoFacade.getCoreDao().getNotNull(((Model)model).getReportId());
                if (report.getContent().getContent() == null)
                    throw new ApplicationException("Файл отчета пуст.");
                final String fileName = "Report_" + new SimpleDateFormat("dd_MM_yyyy").format(report.getFormingDate());
                return new PrintFormDescription(report.getContent().getContent(), fileName);
            });
    }
}
