/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3Pub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.EducationLevelHigh;
import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

/**
 * @author oleyba
 * @since 5/11/11
 */
public class Controller extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.Controller<EducationLevelHighGos3, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EducationLevelHigh> dataSource = new DynamicListDataSource<EducationLevelHigh>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelHigh.P_FULL_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EducationLevelHigh.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подчинено направлению подготовки (специальности)", new String[]{EducationLevelHigh.L_PARENT_LEVEL, EducationLevelHigh.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", EducationLevelHigh.P_QUALIFICATION_TITLE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Направление класссификатора", EducationLevels.programSubjectWithCodeIndexAndGenTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", EducationLevels.programSpecializationTitle()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}