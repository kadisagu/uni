/* $Id$ */
package ru.tandemservice.uni.ws.sakai;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.UniProperties;

/**
 * @author Vasily Zhukov
 * @since 15.11.2011
 */
public class SakaiWSDLResolverBean
{
    public String getLoginWsdlDocumentUrl()
    {
        return getSakaiHost() + "/sakai-axis/SakaiLogin.jws?wsdl";
    }

    public String getLoginNamespaceUri()
    {
        return getSakaiHost() + "/sakai-axis/SakaiLogin.jws";
    }

    public String getScriptWsdlDocumentUrl()
    {
        return getSakaiHost() + "/sakai-axis/SakaiScript.jws?wsdl";
    }

    public String getScriptNamespaceUri()
    {
        return getSakaiHost() + "/sakai-axis/SakaiScript.jws";
    }

    private String getSakaiHost()
    {
        String sakaiHost = ApplicationRuntime.getProperty(UniProperties.SAKAI_HOST);

        if (StringUtils.isEmpty(sakaiHost))
            throw new RuntimeException("No property 'sakai.host' found at runtime.");

        if (!sakaiHost.startsWith("http://"))
            sakaiHost = "http://" + sakaiHost;

        if (sakaiHost.endsWith("/"))
            sakaiHost = sakaiHost.substring(0, sakaiHost.length() - 1);

        return sakaiHost;
    }
}
