/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.daemon;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/28/14
 */
public class UniEduProgramDaemonBean extends UniBaseDao implements IUniEduProgramDaemonBean
{
    public static final SyncDaemon DAEMON = new SyncDaemon(UniEduProgramDaemonBean.class.getName(), 120) {
        @Override protected void main()
        {
            try { IUniEduProgramDaemonBean.instance.get().doUpdateEduLevelSpecSortCache(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }
        }
    };

    @Override
    protected void initDao()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EduProgramSpecializationChild.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EducationLevels.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationLevels.class, DAEMON.getAfterCompleteWakeUpListener());
    }

    @Override
    public boolean doUpdateEduLevelSpecSortCache()
    {
        int updates = 0;
        Debug.begin("UniEduProgramDaemonBean.doUpdateEduLevelSpecSortCache");
        try {
            final Session session = lock(EducationLevels.class.getName());

            updates = updates + executeAndClear(
                new DQLUpdateBuilder(EducationLevels.class)
                .fromDataSource(
                    new DQLSelectBuilder()
                    .fromEntity(EduProgramSpecializationChild.class, "c")
                    .column(property(EduProgramSpecializationChild.id().fromAlias("c")), "c_id")
                    .column(property(EduProgramSpecializationChild.title().fromAlias("c")), "c_title")
                    .buildQuery(),
                    "ds"
                )
                .where(eq(property(EducationLevels.L_EDU_PROGRAM_SPECIALIZATION), property("ds.c_id")))
                .set(EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE, property("ds.c_title"))
                .where(ne(property(EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE), property("ds.c_title"))),
                session
            );

            updates = updates + executeAndClear(
                new DQLUpdateBuilder(EducationLevels.class)
                .where(isNull(property(EducationLevels.L_EDU_PROGRAM_SPECIALIZATION)))
                .set(EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE, value("0"))
                .where(ne(property(EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE), value("0"))),
                session
            );

            updates = updates + executeAndClear(
                new DQLUpdateBuilder(EducationLevels.class)
                .where(instanceOf(EducationLevels.L_EDU_PROGRAM_SPECIALIZATION, EduProgramSpecializationRoot.class))
                .set(EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE, value("1"))
                .where(ne(property(EducationLevels.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE), value("1"))),
                session
            );


        } finally {
            Debug.end();
        }

        return updates > 0;
    }
}
