/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.GroupTitleManagement;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<GroupTitleAlgorithm> list = getList(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_FORMAT);
        DynamicListDataSource<GroupTitleAlgorithm> dataSource = model.getDataSource();

        dataSource.setTotalSize(list.size());
        dataSource.createPage(list);
        dataSource.setCountRow(list.size());
    }

    @Override
    public void updateCurrent(Long algorithmId)
    {
        GroupTitleAlgorithm algorithm = get(GroupTitleAlgorithm.class, algorithmId);
        if (algorithm.isCurrent())
        {
            algorithm.setCurrent(false);
        } else
        {
            for(GroupTitleAlgorithm item : getList(GroupTitleAlgorithm.class))
            {
                item.setCurrent(false);
                update(item);
            }
            algorithm.setCurrent(true);
        }
        update(algorithm);
    }
}
