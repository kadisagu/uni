package ru.tandemservice.uni.dao.mdbio;

import com.healthmarketscience.jackcess.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.io.IOException;
import java.util.Map;

/**
 * @author vdanilov
 */
public class OrgStructIODao extends BaseIODao implements IOrgStructIODao {

    public ICatalogIO<OrgUnitType> catalogOrgUnitType() { return this.catalog(OrgUnitType.class, "title"); }

    @Override
    public void export_OrgStructureTable(final Database mdb) throws IOException
    {

        if (null != mdb.getTable("orgunit_t")) { return; }

        final Map<OrgUnitType, String> typeMap = this.catalogOrgUnitType().export(mdb);

        final Table orgunit_t = new TableBuilder("orgunit_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("parent_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("type_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("title_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        for (final OrgUnit ou: this.getList(OrgUnit.class)) {
            orgunit_t.addRow(
                    Long.toHexString(ou.getId()),
                    (null == ou.getParent() ? null : Long.toHexString(ou.getParent().getId())),
                    typeMap.get(ou.getOrgUnitType()),
                    ou.getFullTitle()
            );
        }
    }

}
