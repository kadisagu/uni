/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3AddEdit;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;

/**
 * @author oleyba
 * @since 5/8/11
 */
public class Model  extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit.Model<EducationLevelHighGos3>
{
    public boolean isParentRequired()
    {
        return getCatalogItem().getLevelType() != null && !getCatalogItem().getLevelType().getCode().equals(StructureEducationLevelsCodes.HIGH_GOS3_GROUP);
    }
}
