/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util;

/**
 * @author vip_delete
 */
@SuppressWarnings("unchecked")
public class UniDDLTool
{
// TODO: надо актуализировать все методы
//    private DBTool _tool;
//
//    public UniDDLTool(DBTool tool)
//    {
//        _tool = tool;
//    }
//
//    @Deprecated
//    public long getCatalogId(String catalogCode) throws SQLException
//    {
//        Statement s = _tool.getConnection().createStatement();
//        ResultSet r = s.executeQuery("select id from catalog_t where code_p='" + catalogCode + "'");
//        long result = r.next() ? r.getLong(1) : 0;
//        s.close();
//        return result;
//    }
//
//    @Deprecated
//    public long getCatalogItemId(String catalogCode, String itemCode) throws SQLException
//    {
//        Statement s = _tool.getConnection().createStatement();
//        ResultSet r = s.executeQuery("select id from catalogItem_t where catalog_id=(select id from catalog_t where code_p='" + catalogCode + "') and code_p='" + itemCode + "'");
//        long result = r.next() ? r.getLong(1) : 0;
//        s.close();
//        return result;
//    }
//
//    @Deprecated
//    public long getCatalogItemId(long catalogId, String itemCode) throws SQLException
//    {
//        Statement s = _tool.getConnection().createStatement();
//        ResultSet r = s.executeQuery("select id from catalogItem_t where catalog_id=" + catalogId + " and code_p='" + itemCode + "'");
//        long result = r.next() ? r.getLong(1) : 0;
//        s.close();
//        return result;
//    }
//
//    public void deleteRow(long rowId, String... tables) throws SQLException
//    {
//        for (String table : tables)
//            _tool.executeUpdate("delete from " + table + " where id=" + rowId);
//    }
//
//    @Deprecated
//    public void deleteCatalogItem(String catalogCode, String itemCode, String tableName) throws SQLException
//    {
//        deleteRow(getCatalogItemId(catalogCode, itemCode), tableName, "catalogItem_t");
//    }
//
//    @Deprecated
//    public void deleteCatalogItem(String catalogCode, String itemCode) throws SQLException
//    {
//        deleteRow(getCatalogItemId(catalogCode, itemCode), "catalogItem_t");
//    }
//
//    @Deprecated
//    public String getCatalogItemNewCode(String catalogCode) throws Exception
//    {
//        int count = 1;
//
//        while (getCatalogItemId(catalogCode, Integer.toString(count)) != 0) count++;
//
//        return String.valueOf(count);
//    }
//
//
//    public long getId(String tableName, String property, String value) throws Exception
//    {
//        Statement s = _tool.getConnection().createStatement();
//        ResultSet r = s.executeQuery("select id from " + tableName + " where " + property + " = " + value);
//        r.next();
//        return r.getLong(1);
//    }
//
//    public short getEntityCode(String simpleClassName) throws Exception
//    {
//        Statement s = _tool.getConnection().createStatement();
//        ResultSet r = s.executeQuery("select code_id from entitycode_t where name_p = '" + simpleClassName + "'");
//        short result = r.next() ? r.getShort(1) : -1;
//        s.close();
//        return result;
//    }
//
//    @Deprecated
//    public Long createCatalog(String code, String title) throws Exception
//    {
//        throw new RuntimeException("This code cannot be executed. Your version is too old!");
//        //        short catalogDiscriminator = EntityRuntime.getMeta(Catalog.class).getEntityCode();
//        //        Long catalogId = EntityIDGenerator.generateNewId(catalogDiscriminator);
//        //        _tool.executeUpdate("insert into catalogitemfolder_t (id, title_p, discriminator) values(" + catalogId + ", '" + title + "', " + catalogDiscriminator + ")");
//        //        _tool.executeUpdate("insert into catalog_t (id, code_p) values(" + catalogId + ", '" + code + "')");
//        //        return catalogId;
//    }
//
//    @Deprecated
//    public Long createCatalogItem(Long catalogId, String code, String title, Class catalogItemClass) throws Exception
//    {
//        short catalogItemDiscriminator = EntityRuntime.getMeta(catalogItemClass).getEntityCode();
//        Long catalogItemId = EntityIDGenerator.generateNewId(catalogItemDiscriminator);
//        _tool.executeUpdate("insert into catalogitem_t (id, title_p, code_p, catalog_id, discriminator) values(" + catalogItemId + ", '" + title + "', '" + code + "', " + catalogId + ", " + catalogItemDiscriminator + ")");
//        return catalogItemId;
//    }
//
//    /**
//     * Creates hierarchical simple catalog item
//     */
//    @Deprecated
//    public Long createHierarchicalCatalogItem(Long catalogId, String code, String title, String parentCatalogItemCode, Class catalogItemClass) throws Exception
//    {
//        Long parentItemId = getCatalogItemId(catalogId, parentCatalogItemCode);
//        short catalogItemDiscriminator = EntityRuntime.getMeta(catalogItemClass).getEntityCode();
//        Long catalogItemId = EntityIDGenerator.generateNewId(catalogItemDiscriminator);
//        _tool.executeUpdate("insert into catalogitem_t (id, title_p, code_p, catalog_id, parent_id, discriminator) values(" + catalogItemId + ", '" + title + "', '" + code + "', " + catalogId + ", " + parentItemId + ", " + catalogItemDiscriminator + ")");
//        return catalogItemId;
//    }
//
//    /**
//     * Creates any type of catalog item.
//     * Map of itemValues must contain items with key name as it is in the database.
//     * For example, we have to add items into some catalog, which has two extra fields:
//     * field1 and field2. They titled as field1_p and field2_id in the database, so
//     * we should add these two fields into map as follows: map.put("field1_p", value1),
//     * map.put("field2_id", value2).
//     */
//    @Deprecated
//    public Long createCatalogItem(String catalogCode, String code, String title, String parentCatalogItemCode, Class catalogItemClass, Map<String, Object> itemValues) throws Exception
//    {
//        Long catalogId = getCatalogId(catalogCode);
//        Long catalogItemId = null != parentCatalogItemCode ? createHierarchicalCatalogItem(catalogId, code, title, parentCatalogItemCode, catalogItemClass) : createCatalogItem(catalogId, code, title, catalogItemClass);
//
//        Set<String> fieldNames = itemValues.keySet();
//        Object[] values = new Object[fieldNames.size() + 1];
//        StringBuilder queryBuilder = new StringBuilder("insert into ");
//        queryBuilder.append(PropertySupport.buildArtifactName(EntityRuntime.getMeta(catalogItemClass).getName(), "_t")).append(" (id");
//        StringBuilder valuesAreaBuilder = new StringBuilder(") values(?");
//
//        int i = 0;
//        values[i++] = catalogItemId;
//        for (String field : fieldNames)
//        {
//            queryBuilder.append(",").append(field);
//            values[i++] = itemValues.get(field);
//            valuesAreaBuilder.append(",?");
//        }
//
//        queryBuilder.append(valuesAreaBuilder).append(")");
//        _tool.executeUpdate(queryBuilder.toString(), values);
//
//        return catalogItemId;
//    }
//
//    @Deprecated
//    public void dropCatalog(String catalogCode) throws Exception
//    {
//        Long catalogId = getCatalogId(catalogCode);
//        String tableName = PropertySupport.buildArtifactName(catalogCode, "_t");
//        boolean tableExists = _tool.tableExists(tableName);
//
//        if (tableExists)
//        {
//            _tool.executeUpdate("delete from " + tableName);
//        }
//
//        _tool.executeUpdate("delete from catalogitem_t where catalog_id = ?", catalogId);
//        _tool.executeUpdate("delete from catalog_t where id = ?", catalogId);
//        _tool.executeUpdate("delete from entitycode_t where name_p = ?", catalogCode);
//
//        if (tableExists)
//        {
//            _tool.dropTable(tableName);
//        }
//    }
}