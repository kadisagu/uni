/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.DownloadStorableReport;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uni.component.reports.PrintReport.PrintReportModelBase;

/**
 * @author agolubenko
 * @since 14.10.2008
 */
@Input( @Bind(key = "reportId", binding = "reportId") )
public class Model extends PrintReportModelBase
{
    private Long _reportId;
    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

}
