/* $Id$ */
package ru.tandemservice.uni.component.catalog.developCondition.DevelopConditionPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;

/**
 * @author azhebko
 * @since 15.08.2014
 */
public interface IDAO extends IDefaultCatalogPubDAO<DevelopCondition, Model>
{
}