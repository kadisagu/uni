/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util;

import org.apache.commons.lang.StringUtils;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.IDevelopCombination;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DevelopUtil
{
    private static void append(StringBuilder builder, String title) {
        if (null != title) {
            if (builder.length() > 0) {
                builder.append(", ").append(title);
            } else {
                builder.append(StringUtils.capitalize(title));
            }
        }
    }

    private static StringBuilder getTitle(DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech)
    {
        final StringBuilder builder = new StringBuilder();
        append(builder, (null == developForm ? null : StringUtils.trimToNull(developForm.getShortTitle())));
        append(builder, (null == developCondition ? null : StringUtils.trimToNull(developCondition.getShortTitle())));
        append(builder, (null == developTech ? null : StringUtils.trimToNull(developTech.getShortTitle())));
        return builder;
    }

    public static String getTitle(DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopGrid developGrid)
    {
        StringBuilder builder = getTitle(developForm, developCondition, developTech);
        append(builder, (null == developGrid ? null : StringUtils.trimToNull(developGrid.getTitle())));
        return builder.toString();
    }

    public static String getTitle(IDevelopCombination developCombination)
    {
        return getTitle(developCombination.getDevelopForm(), developCombination.getDevelopCondition(), developCombination.getDevelopTech(), developCombination.getDevelopGrid());
    }

    public static String getTitle(DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod)
    {
        StringBuilder builder = new StringBuilder();

        append(builder, (null == developForm ? null : StringUtils.trimToNull(developForm.getShortTitle())));

        if (developCondition != null && !UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(developCondition.getCode()))
            append(builder, StringUtils.trimToNull(developCondition.getShortTitle()));

        if (developTech != null && !UniDefines.DEVELOP_TECH_SIMPLE.equals(developTech.getCode()))
            append(builder, StringUtils.trimToNull(developTech.getShortTitle()));

        append(builder, (null == developPeriod ? null : StringUtils.trimToNull(developPeriod.getTitle())));

        return builder.toString();
    }
    
    public static String getTitle(EducationOrgUnit eduOu)
    {
        return getTitle(eduOu.getDevelopForm(), eduOu.getDevelopCondition(), eduOu.getDevelopTech(), eduOu.getDevelopPeriod());
    }
}
