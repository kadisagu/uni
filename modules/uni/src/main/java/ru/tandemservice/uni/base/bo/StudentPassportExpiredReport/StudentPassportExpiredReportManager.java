/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentPassportExpiredReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Denis Katkov
 * @since 10.06.2016
 */
@Configuration
public class StudentPassportExpiredReportManager extends BusinessObjectManager
{
}