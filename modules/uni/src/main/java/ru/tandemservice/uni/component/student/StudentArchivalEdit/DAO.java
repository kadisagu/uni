/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentArchivalEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author agolubenko
 * @since 11.02.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));

        int currentYear = UniBaseUtils.getCurrentYear();
        List<IdentifiableWrapper> endingYears = new ArrayList<IdentifiableWrapper>();
        for(int i = currentYear - 9; i <= currentYear ; i++)
        {
            endingYears.add(new IdentifiableWrapper((long) i, String.valueOf(i)));
            if(null != model.getStudent().getFinishYear() && i == model.getStudent().getFinishYear())
                model.setEndingYear(endingYears.get(endingYears.size() - 1));
        }
        model.setEndingYearList(endingYears);
    }

    @Override
    public void update(Model model)
    {
        model.getStudent().setFinishYear(model.getEndingYear().getId().intValue());
        update(model.getStudent());
    }
}