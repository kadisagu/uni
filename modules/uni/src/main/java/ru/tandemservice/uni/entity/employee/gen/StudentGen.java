package ru.tandemservice.uni.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.sec.entity.ILocalRoleContext;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentGen extends PersonRole
 implements ILocalRoleContext, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.employee.Student";
    public static final String ENTITY_NAME = "student";
    public static final int VERSION_HASH = 190834409;
    private static IEntityMeta ENTITY_META;

    public static final String P_PERSONAL_NUMBER = "personalNumber";
    public static final String P_BOOK_NUMBER = "bookNumber";
    public static final String P_ENTRANCE_YEAR = "entranceYear";
    public static final String L_GROUP = "group";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_STATUS = "status";
    public static final String L_COURSE = "course";
    public static final String L_DEVELOP_PERIOD_AUTO = "developPeriodAuto";
    public static final String L_DEVELOP_PERIOD = "developPeriod";
    public static final String P_COMMENT = "comment";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String P_EDU_DOCUMENT_ORIGINAL_HANDED_IN = "eduDocumentOriginalHandedIn";
    public static final String P_PERSONAL_EDUCATION = "personalEducation";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String L_TARGET_ADMISSION_ORG_UNIT = "targetAdmissionOrgUnit";
    public static final String P_THRIFTY_GROUP = "thriftyGroup";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_FINAL_QUALIFYING_WORK_THEME = "finalQualifyingWorkTheme";
    public static final String L_PPS_ENTRY = "ppsEntry";
    public static final String P_FINAL_QUALIFYING_WORK_ADVISOR = "finalQualifyingWorkAdvisor";
    public static final String P_PRACTICE_PLACE = "practicePlace";
    public static final String P_EMPLOYMENT = "employment";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String L_OFFICE_POSTAL_ADDRESS = "officePostalAddress";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_FINISH_YEAR = "finishYear";
    public static final String P_PERSONAL_FILE_NUMBER = "personalFileNumber";
    public static final String P_PERSONAL_ARCHIVAL_FILE_NUMBER = "personalArchivalFileNumber";
    public static final String P_DESCRIPTION = "description";
    public static final String P_ARCHIVING_DATE = "archivingDate";
    public static final String P_LOCAL_ROLE_CONTEXT_TITLE = "localRoleContextTitle";
    public static final String P_STUDENT_FULL_FIO_WITH_BIRTH_YEAR_GROUP_AND_STATUS = "studentFullFioWithBirthYearGroupAndStatus";
    public static final String P_TITLE_WITH_FIO = "titleWithFio";
    public static final String P_TITLE_WITHOUT_FIO = "titleWithoutFio";

    private String _personalNumber;     // Личный номер
    private String _bookNumber;     // Номер зачетной книжки
    private int _entranceYear;     // Год приема
    private Group _group;     // Группа
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)
    private CompensationType _compensationType;     // Вид возмещения затрат
    private StudentStatus _status;     // Состояние студента
    private Course _course;     // Курс
    private DevelopPeriod _developPeriodAuto;     // Срок освоения (автообновляемое)
    private DevelopPeriod _developPeriod;     // Срок освоения
    private String _comment;     // Примечание
    private PersonEduDocument _eduDocument;     // Документ об образовании
    private boolean _eduDocumentOriginalHandedIn;     // В личном деле хранится оригинал док. об образ.
    private boolean _personalEducation;     // Индивидуальное образование
    private boolean _targetAdmission;     // Целевой прием
    private ExternalOrgUnit _targetAdmissionOrgUnit;     // Организация ЦП
    private boolean _thriftyGroup;     // Хоз. группа
    private StudentCategory _studentCategory;     // Категория обучаемого
    private String _contractNumber;     // Номер договора
    private String _finalQualifyingWorkTheme;     // Тема ВКР
    private PpsEntry _ppsEntry;     // Руководитель ВКР
    private String _finalQualifyingWorkAdvisor;     // Руководитель ВКР (текстом)
    private String _practicePlace;     // Место прохождения практики
    private String _employment;     // Трудоустройство
    private Date _contractDate;     // Дата договора
    private AddressBase _officePostalAddress;     // Адрес (базовый)
    private boolean _archival;     // Архивный
    private Integer _finishYear;     // Год выпуска
    private String _personalFileNumber;     // Номер личного дела
    private String _personalArchivalFileNumber;     // Номер личного дела в архиве
    private String _description;     // Описание (комментарий)
    private Date _archivingDate;     // Дата добавления в архив
    private String _localRoleContextTitle; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Личный номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getPersonalNumber()
    {
        return _personalNumber;
    }

    /**
     * @param personalNumber Личный номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setPersonalNumber(String personalNumber)
    {
        dirty(_personalNumber, personalNumber);
        _personalNumber = personalNumber;
    }

    /**
     * @return Номер зачетной книжки.
     */
    @Length(max=255)
    public String getBookNumber()
    {
        return _bookNumber;
    }

    /**
     * @param bookNumber Номер зачетной книжки.
     */
    public void setBookNumber(String bookNumber)
    {
        dirty(_bookNumber, bookNumber);
        _bookNumber = bookNumber;
    }

    /**
     * @return Год приема. Свойство не может быть null.
     */
    @NotNull
    public int getEntranceYear()
    {
        return _entranceYear;
    }

    /**
     * @param entranceYear Год приема. Свойство не может быть null.
     */
    public void setEntranceYear(int entranceYear)
    {
        dirty(_entranceYear, entranceYear);
        _entranceYear = entranceYear;
    }

    /**
     * @return Группа.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStatus()
    {
        return _status;
    }

    /**
     * @param status Состояние студента. Свойство не может быть null.
     */
    public void setStatus(StudentStatus status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Срок освоения (автообновляемое). Свойство не может быть null.
     */
    @NotNull
    public DevelopPeriod getDevelopPeriodAuto()
    {
        return _developPeriodAuto;
    }

    /**
     * @param developPeriodAuto Срок освоения (автообновляемое). Свойство не может быть null.
     */
    public void setDevelopPeriodAuto(DevelopPeriod developPeriodAuto)
    {
        dirty(_developPeriodAuto, developPeriodAuto);
        _developPeriodAuto = developPeriodAuto;
    }

    /**
     * @return Срок освоения.
     */
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Примечание.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Ссылка на документ, на основании которого студент поступил, и который хранится в личном деле.
     *
     * @return Документ об образовании.
     */
    public PersonEduDocument getEduDocument()
    {
        initLazyForGet("eduDocument");
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        initLazyForSet("eduDocument");
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return В личном деле хранится оригинал док. об образ.. Свойство не может быть null.
     */
    @NotNull
    public boolean isEduDocumentOriginalHandedIn()
    {
        initLazyForGet("eduDocumentOriginalHandedIn");
        return _eduDocumentOriginalHandedIn;
    }

    /**
     * @param eduDocumentOriginalHandedIn В личном деле хранится оригинал док. об образ.. Свойство не может быть null.
     */
    public void setEduDocumentOriginalHandedIn(boolean eduDocumentOriginalHandedIn)
    {
        initLazyForSet("eduDocumentOriginalHandedIn");
        dirty(_eduDocumentOriginalHandedIn, eduDocumentOriginalHandedIn);
        _eduDocumentOriginalHandedIn = eduDocumentOriginalHandedIn;
    }

    /**
     * @return Индивидуальное образование. Свойство не может быть null.
     */
    @NotNull
    public boolean isPersonalEducation()
    {
        initLazyForGet("personalEducation");
        return _personalEducation;
    }

    /**
     * @param personalEducation Индивидуальное образование. Свойство не может быть null.
     */
    public void setPersonalEducation(boolean personalEducation)
    {
        initLazyForSet("personalEducation");
        dirty(_personalEducation, personalEducation);
        _personalEducation = personalEducation;
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием. Свойство не может быть null.
     */
    public void setTargetAdmission(boolean targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    /**
     * @return Организация ЦП.
     */
    public ExternalOrgUnit getTargetAdmissionOrgUnit()
    {
        return _targetAdmissionOrgUnit;
    }

    /**
     * @param targetAdmissionOrgUnit Организация ЦП.
     */
    public void setTargetAdmissionOrgUnit(ExternalOrgUnit targetAdmissionOrgUnit)
    {
        dirty(_targetAdmissionOrgUnit, targetAdmissionOrgUnit);
        _targetAdmissionOrgUnit = targetAdmissionOrgUnit;
    }

    /**
     * @return Хоз. группа. Свойство не может быть null.
     */
    @NotNull
    public boolean isThriftyGroup()
    {
        initLazyForGet("thriftyGroup");
        return _thriftyGroup;
    }

    /**
     * @param thriftyGroup Хоз. группа. Свойство не может быть null.
     */
    public void setThriftyGroup(boolean thriftyGroup)
    {
        initLazyForSet("thriftyGroup");
        dirty(_thriftyGroup, thriftyGroup);
        _thriftyGroup = thriftyGroup;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Номер договора.
     */
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Тема ВКР.
     */
    @Length(max=1024)
    public String getFinalQualifyingWorkTheme()
    {
        initLazyForGet("finalQualifyingWorkTheme");
        return _finalQualifyingWorkTheme;
    }

    /**
     * @param finalQualifyingWorkTheme Тема ВКР.
     */
    public void setFinalQualifyingWorkTheme(String finalQualifyingWorkTheme)
    {
        initLazyForSet("finalQualifyingWorkTheme");
        dirty(_finalQualifyingWorkTheme, finalQualifyingWorkTheme);
        _finalQualifyingWorkTheme = finalQualifyingWorkTheme;
    }

    /**
     * @return Руководитель ВКР.
     */
    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    /**
     * @param ppsEntry Руководитель ВКР.
     */
    public void setPpsEntry(PpsEntry ppsEntry)
    {
        dirty(_ppsEntry, ppsEntry);
        _ppsEntry = ppsEntry;
    }

    /**
     * @return Руководитель ВКР (текстом).
     */
    @Length(max=255)
    public String getFinalQualifyingWorkAdvisor()
    {
        initLazyForGet("finalQualifyingWorkAdvisor");
        return _finalQualifyingWorkAdvisor;
    }

    /**
     * @param finalQualifyingWorkAdvisor Руководитель ВКР (текстом).
     */
    public void setFinalQualifyingWorkAdvisor(String finalQualifyingWorkAdvisor)
    {
        initLazyForSet("finalQualifyingWorkAdvisor");
        dirty(_finalQualifyingWorkAdvisor, finalQualifyingWorkAdvisor);
        _finalQualifyingWorkAdvisor = finalQualifyingWorkAdvisor;
    }

    /**
     * @return Место прохождения практики.
     */
    @Length(max=255)
    public String getPracticePlace()
    {
        initLazyForGet("practicePlace");
        return _practicePlace;
    }

    /**
     * @param practicePlace Место прохождения практики.
     */
    public void setPracticePlace(String practicePlace)
    {
        initLazyForSet("practicePlace");
        dirty(_practicePlace, practicePlace);
        _practicePlace = practicePlace;
    }

    /**
     * @return Трудоустройство.
     */
    @Length(max=255)
    public String getEmployment()
    {
        initLazyForGet("employment");
        return _employment;
    }

    /**
     * @param employment Трудоустройство.
     */
    public void setEmployment(String employment)
    {
        initLazyForSet("employment");
        dirty(_employment, employment);
        _employment = employment;
    }

    /**
     * @return Дата договора.
     */
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата договора.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Адрес (базовый).
     */
    public AddressBase getOfficePostalAddress()
    {
        initLazyForGet("officePostalAddress");
        return _officePostalAddress;
    }

    /**
     * @param officePostalAddress Адрес (базовый).
     */
    public void setOfficePostalAddress(AddressBase officePostalAddress)
    {
        initLazyForSet("officePostalAddress");
        dirty(_officePostalAddress, officePostalAddress);
        _officePostalAddress = officePostalAddress;
    }

    /**
     * @return Архивный. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival Архивный. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Год выпуска.
     */
    public Integer getFinishYear()
    {
        initLazyForGet("finishYear");
        return _finishYear;
    }

    /**
     * @param finishYear Год выпуска.
     */
    public void setFinishYear(Integer finishYear)
    {
        initLazyForSet("finishYear");
        dirty(_finishYear, finishYear);
        _finishYear = finishYear;
    }

    /**
     * @return Номер личного дела.
     */
    @Length(max=255)
    public String getPersonalFileNumber()
    {
        return _personalFileNumber;
    }

    /**
     * @param personalFileNumber Номер личного дела.
     */
    public void setPersonalFileNumber(String personalFileNumber)
    {
        dirty(_personalFileNumber, personalFileNumber);
        _personalFileNumber = personalFileNumber;
    }

    /**
     * @return Номер личного дела в архиве.
     */
    @Length(max=255)
    public String getPersonalArchivalFileNumber()
    {
        initLazyForGet("personalArchivalFileNumber");
        return _personalArchivalFileNumber;
    }

    /**
     * @param personalArchivalFileNumber Номер личного дела в архиве.
     */
    public void setPersonalArchivalFileNumber(String personalArchivalFileNumber)
    {
        initLazyForSet("personalArchivalFileNumber");
        dirty(_personalArchivalFileNumber, personalArchivalFileNumber);
        _personalArchivalFileNumber = personalArchivalFileNumber;
    }

    /**
     * @return Описание (комментарий).
     */
    @Length(max=4000)
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Описание (комментарий).
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Дата добавления в архив.
     */
    public Date getArchivingDate()
    {
        initLazyForGet("archivingDate");
        return _archivingDate;
    }

    /**
     * @param archivingDate Дата добавления в архив.
     */
    public void setArchivingDate(Date archivingDate)
    {
        initLazyForSet("archivingDate");
        dirty(_archivingDate, archivingDate);
        _archivingDate = archivingDate;
    }

    /**
     * @return 
     *
     * Это формула "person.identityCard.lastName".
     */
    // @Length(max=255)
    public String getLocalRoleContextTitle()
    {
        initLazyForGet("localRoleContextTitle");
        return _localRoleContextTitle;
    }

    /**
     * @param localRoleContextTitle 
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setLocalRoleContextTitle(String localRoleContextTitle)
    {
        initLazyForSet("localRoleContextTitle");
        dirty(_localRoleContextTitle, localRoleContextTitle);
        _localRoleContextTitle = localRoleContextTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentGen)
        {
            setPersonalNumber(((Student)another).getPersonalNumber());
            setBookNumber(((Student)another).getBookNumber());
            setEntranceYear(((Student)another).getEntranceYear());
            setGroup(((Student)another).getGroup());
            setEducationOrgUnit(((Student)another).getEducationOrgUnit());
            setCompensationType(((Student)another).getCompensationType());
            setStatus(((Student)another).getStatus());
            setCourse(((Student)another).getCourse());
            setDevelopPeriodAuto(((Student)another).getDevelopPeriodAuto());
            setDevelopPeriod(((Student)another).getDevelopPeriod());
            setComment(((Student)another).getComment());
            setEduDocument(((Student)another).getEduDocument());
            setEduDocumentOriginalHandedIn(((Student)another).isEduDocumentOriginalHandedIn());
            setPersonalEducation(((Student)another).isPersonalEducation());
            setTargetAdmission(((Student)another).isTargetAdmission());
            setTargetAdmissionOrgUnit(((Student)another).getTargetAdmissionOrgUnit());
            setThriftyGroup(((Student)another).isThriftyGroup());
            setStudentCategory(((Student)another).getStudentCategory());
            setContractNumber(((Student)another).getContractNumber());
            setFinalQualifyingWorkTheme(((Student)another).getFinalQualifyingWorkTheme());
            setPpsEntry(((Student)another).getPpsEntry());
            setFinalQualifyingWorkAdvisor(((Student)another).getFinalQualifyingWorkAdvisor());
            setPracticePlace(((Student)another).getPracticePlace());
            setEmployment(((Student)another).getEmployment());
            setContractDate(((Student)another).getContractDate());
            setOfficePostalAddress(((Student)another).getOfficePostalAddress());
            setArchival(((Student)another).isArchival());
            setFinishYear(((Student)another).getFinishYear());
            setPersonalFileNumber(((Student)another).getPersonalFileNumber());
            setPersonalArchivalFileNumber(((Student)another).getPersonalArchivalFileNumber());
            setDescription(((Student)another).getDescription());
            setArchivingDate(((Student)another).getArchivingDate());
            setLocalRoleContextTitle(((Student)another).getLocalRoleContextTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Student.class;
        }

        public T newInstance()
        {
            return (T) new Student();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "personalNumber":
                    return obj.getPersonalNumber();
                case "bookNumber":
                    return obj.getBookNumber();
                case "entranceYear":
                    return obj.getEntranceYear();
                case "group":
                    return obj.getGroup();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "compensationType":
                    return obj.getCompensationType();
                case "status":
                    return obj.getStatus();
                case "course":
                    return obj.getCourse();
                case "developPeriodAuto":
                    return obj.getDevelopPeriodAuto();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "comment":
                    return obj.getComment();
                case "eduDocument":
                    return obj.getEduDocument();
                case "eduDocumentOriginalHandedIn":
                    return obj.isEduDocumentOriginalHandedIn();
                case "personalEducation":
                    return obj.isPersonalEducation();
                case "targetAdmission":
                    return obj.isTargetAdmission();
                case "targetAdmissionOrgUnit":
                    return obj.getTargetAdmissionOrgUnit();
                case "thriftyGroup":
                    return obj.isThriftyGroup();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "contractNumber":
                    return obj.getContractNumber();
                case "finalQualifyingWorkTheme":
                    return obj.getFinalQualifyingWorkTheme();
                case "ppsEntry":
                    return obj.getPpsEntry();
                case "finalQualifyingWorkAdvisor":
                    return obj.getFinalQualifyingWorkAdvisor();
                case "practicePlace":
                    return obj.getPracticePlace();
                case "employment":
                    return obj.getEmployment();
                case "contractDate":
                    return obj.getContractDate();
                case "officePostalAddress":
                    return obj.getOfficePostalAddress();
                case "archival":
                    return obj.isArchival();
                case "finishYear":
                    return obj.getFinishYear();
                case "personalFileNumber":
                    return obj.getPersonalFileNumber();
                case "personalArchivalFileNumber":
                    return obj.getPersonalArchivalFileNumber();
                case "description":
                    return obj.getDescription();
                case "archivingDate":
                    return obj.getArchivingDate();
                case "localRoleContextTitle":
                    return obj.getLocalRoleContextTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "personalNumber":
                    obj.setPersonalNumber((String) value);
                    return;
                case "bookNumber":
                    obj.setBookNumber((String) value);
                    return;
                case "entranceYear":
                    obj.setEntranceYear((Integer) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "status":
                    obj.setStatus((StudentStatus) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "developPeriodAuto":
                    obj.setDevelopPeriodAuto((DevelopPeriod) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "eduDocumentOriginalHandedIn":
                    obj.setEduDocumentOriginalHandedIn((Boolean) value);
                    return;
                case "personalEducation":
                    obj.setPersonalEducation((Boolean) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((Boolean) value);
                    return;
                case "targetAdmissionOrgUnit":
                    obj.setTargetAdmissionOrgUnit((ExternalOrgUnit) value);
                    return;
                case "thriftyGroup":
                    obj.setThriftyGroup((Boolean) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "finalQualifyingWorkTheme":
                    obj.setFinalQualifyingWorkTheme((String) value);
                    return;
                case "ppsEntry":
                    obj.setPpsEntry((PpsEntry) value);
                    return;
                case "finalQualifyingWorkAdvisor":
                    obj.setFinalQualifyingWorkAdvisor((String) value);
                    return;
                case "practicePlace":
                    obj.setPracticePlace((String) value);
                    return;
                case "employment":
                    obj.setEmployment((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "officePostalAddress":
                    obj.setOfficePostalAddress((AddressBase) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "finishYear":
                    obj.setFinishYear((Integer) value);
                    return;
                case "personalFileNumber":
                    obj.setPersonalFileNumber((String) value);
                    return;
                case "personalArchivalFileNumber":
                    obj.setPersonalArchivalFileNumber((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "archivingDate":
                    obj.setArchivingDate((Date) value);
                    return;
                case "localRoleContextTitle":
                    obj.setLocalRoleContextTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "personalNumber":
                        return true;
                case "bookNumber":
                        return true;
                case "entranceYear":
                        return true;
                case "group":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "compensationType":
                        return true;
                case "status":
                        return true;
                case "course":
                        return true;
                case "developPeriodAuto":
                        return true;
                case "developPeriod":
                        return true;
                case "comment":
                        return true;
                case "eduDocument":
                        return true;
                case "eduDocumentOriginalHandedIn":
                        return true;
                case "personalEducation":
                        return true;
                case "targetAdmission":
                        return true;
                case "targetAdmissionOrgUnit":
                        return true;
                case "thriftyGroup":
                        return true;
                case "studentCategory":
                        return true;
                case "contractNumber":
                        return true;
                case "finalQualifyingWorkTheme":
                        return true;
                case "ppsEntry":
                        return true;
                case "finalQualifyingWorkAdvisor":
                        return true;
                case "practicePlace":
                        return true;
                case "employment":
                        return true;
                case "contractDate":
                        return true;
                case "officePostalAddress":
                        return true;
                case "archival":
                        return true;
                case "finishYear":
                        return true;
                case "personalFileNumber":
                        return true;
                case "personalArchivalFileNumber":
                        return true;
                case "description":
                        return true;
                case "archivingDate":
                        return true;
                case "localRoleContextTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "personalNumber":
                    return true;
                case "bookNumber":
                    return true;
                case "entranceYear":
                    return true;
                case "group":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "compensationType":
                    return true;
                case "status":
                    return true;
                case "course":
                    return true;
                case "developPeriodAuto":
                    return true;
                case "developPeriod":
                    return true;
                case "comment":
                    return true;
                case "eduDocument":
                    return true;
                case "eduDocumentOriginalHandedIn":
                    return true;
                case "personalEducation":
                    return true;
                case "targetAdmission":
                    return true;
                case "targetAdmissionOrgUnit":
                    return true;
                case "thriftyGroup":
                    return true;
                case "studentCategory":
                    return true;
                case "contractNumber":
                    return true;
                case "finalQualifyingWorkTheme":
                    return true;
                case "ppsEntry":
                    return true;
                case "finalQualifyingWorkAdvisor":
                    return true;
                case "practicePlace":
                    return true;
                case "employment":
                    return true;
                case "contractDate":
                    return true;
                case "officePostalAddress":
                    return true;
                case "archival":
                    return true;
                case "finishYear":
                    return true;
                case "personalFileNumber":
                    return true;
                case "personalArchivalFileNumber":
                    return true;
                case "description":
                    return true;
                case "archivingDate":
                    return true;
                case "localRoleContextTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "personalNumber":
                    return String.class;
                case "bookNumber":
                    return String.class;
                case "entranceYear":
                    return Integer.class;
                case "group":
                    return Group.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "compensationType":
                    return CompensationType.class;
                case "status":
                    return StudentStatus.class;
                case "course":
                    return Course.class;
                case "developPeriodAuto":
                    return DevelopPeriod.class;
                case "developPeriod":
                    return DevelopPeriod.class;
                case "comment":
                    return String.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "eduDocumentOriginalHandedIn":
                    return Boolean.class;
                case "personalEducation":
                    return Boolean.class;
                case "targetAdmission":
                    return Boolean.class;
                case "targetAdmissionOrgUnit":
                    return ExternalOrgUnit.class;
                case "thriftyGroup":
                    return Boolean.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "contractNumber":
                    return String.class;
                case "finalQualifyingWorkTheme":
                    return String.class;
                case "ppsEntry":
                    return PpsEntry.class;
                case "finalQualifyingWorkAdvisor":
                    return String.class;
                case "practicePlace":
                    return String.class;
                case "employment":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "officePostalAddress":
                    return AddressBase.class;
                case "archival":
                    return Boolean.class;
                case "finishYear":
                    return Integer.class;
                case "personalFileNumber":
                    return String.class;
                case "personalArchivalFileNumber":
                    return String.class;
                case "description":
                    return String.class;
                case "archivingDate":
                    return Date.class;
                case "localRoleContextTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Student> _dslPath = new Path<Student>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Student");
    }
            

    /**
     * @return Личный номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.employee.Student#getPersonalNumber()
     */
    public static PropertyPath<String> personalNumber()
    {
        return _dslPath.personalNumber();
    }

    /**
     * @return Номер зачетной книжки.
     * @see ru.tandemservice.uni.entity.employee.Student#getBookNumber()
     */
    public static PropertyPath<String> bookNumber()
    {
        return _dslPath.bookNumber();
    }

    /**
     * @return Год приема. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getEntranceYear()
     */
    public static PropertyPath<Integer> entranceYear()
    {
        return _dslPath.entranceYear();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.uni.entity.employee.Student#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getStatus()
     */
    public static StudentStatus.Path<StudentStatus> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Срок освоения (автообновляемое). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getDevelopPeriodAuto()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriodAuto()
    {
        return _dslPath.developPeriodAuto();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uni.entity.employee.Student#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uni.entity.employee.Student#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Ссылка на документ, на основании которого студент поступил, и который хранится в личном деле.
     *
     * @return Документ об образовании.
     * @see ru.tandemservice.uni.entity.employee.Student#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return В личном деле хранится оригинал док. об образ.. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isEduDocumentOriginalHandedIn()
     */
    public static PropertyPath<Boolean> eduDocumentOriginalHandedIn()
    {
        return _dslPath.eduDocumentOriginalHandedIn();
    }

    /**
     * @return Индивидуальное образование. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isPersonalEducation()
     */
    public static PropertyPath<Boolean> personalEducation()
    {
        return _dslPath.personalEducation();
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isTargetAdmission()
     */
    public static PropertyPath<Boolean> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @return Организация ЦП.
     * @see ru.tandemservice.uni.entity.employee.Student#getTargetAdmissionOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> targetAdmissionOrgUnit()
    {
        return _dslPath.targetAdmissionOrgUnit();
    }

    /**
     * @return Хоз. группа. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isThriftyGroup()
     */
    public static PropertyPath<Boolean> thriftyGroup()
    {
        return _dslPath.thriftyGroup();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.uni.entity.employee.Student#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.uni.entity.employee.Student#getFinalQualifyingWorkTheme()
     */
    public static PropertyPath<String> finalQualifyingWorkTheme()
    {
        return _dslPath.finalQualifyingWorkTheme();
    }

    /**
     * @return Руководитель ВКР.
     * @see ru.tandemservice.uni.entity.employee.Student#getPpsEntry()
     */
    public static PpsEntry.Path<PpsEntry> ppsEntry()
    {
        return _dslPath.ppsEntry();
    }

    /**
     * @return Руководитель ВКР (текстом).
     * @see ru.tandemservice.uni.entity.employee.Student#getFinalQualifyingWorkAdvisor()
     */
    public static PropertyPath<String> finalQualifyingWorkAdvisor()
    {
        return _dslPath.finalQualifyingWorkAdvisor();
    }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.uni.entity.employee.Student#getPracticePlace()
     */
    public static PropertyPath<String> practicePlace()
    {
        return _dslPath.practicePlace();
    }

    /**
     * @return Трудоустройство.
     * @see ru.tandemservice.uni.entity.employee.Student#getEmployment()
     */
    public static PropertyPath<String> employment()
    {
        return _dslPath.employment();
    }

    /**
     * @return Дата договора.
     * @see ru.tandemservice.uni.entity.employee.Student#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Адрес (базовый).
     * @see ru.tandemservice.uni.entity.employee.Student#getOfficePostalAddress()
     */
    public static AddressBase.Path<AddressBase> officePostalAddress()
    {
        return _dslPath.officePostalAddress();
    }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Год выпуска.
     * @see ru.tandemservice.uni.entity.employee.Student#getFinishYear()
     */
    public static PropertyPath<Integer> finishYear()
    {
        return _dslPath.finishYear();
    }

    /**
     * @return Номер личного дела.
     * @see ru.tandemservice.uni.entity.employee.Student#getPersonalFileNumber()
     */
    public static PropertyPath<String> personalFileNumber()
    {
        return _dslPath.personalFileNumber();
    }

    /**
     * @return Номер личного дела в архиве.
     * @see ru.tandemservice.uni.entity.employee.Student#getPersonalArchivalFileNumber()
     */
    public static PropertyPath<String> personalArchivalFileNumber()
    {
        return _dslPath.personalArchivalFileNumber();
    }

    /**
     * @return Описание (комментарий).
     * @see ru.tandemservice.uni.entity.employee.Student#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Дата добавления в архив.
     * @see ru.tandemservice.uni.entity.employee.Student#getArchivingDate()
     */
    public static PropertyPath<Date> archivingDate()
    {
        return _dslPath.archivingDate();
    }

    /**
     * @return 
     *
     * Это формула "person.identityCard.lastName".
     * @see ru.tandemservice.uni.entity.employee.Student#getLocalRoleContextTitle()
     */
    public static PropertyPath<String> localRoleContextTitle()
    {
        return _dslPath.localRoleContextTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.Student#getStudentFullFioWithBirthYearGroupAndStatus()
     */
    public static SupportedPropertyPath<String> studentFullFioWithBirthYearGroupAndStatus()
    {
        return _dslPath.studentFullFioWithBirthYearGroupAndStatus();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.Student#getTitleWithFio()
     */
    public static SupportedPropertyPath<String> titleWithFio()
    {
        return _dslPath.titleWithFio();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.Student#getTitleWithoutFio()
     */
    public static SupportedPropertyPath<String> titleWithoutFio()
    {
        return _dslPath.titleWithoutFio();
    }

    public static class Path<E extends Student> extends PersonRole.Path<E>
    {
        private PropertyPath<String> _personalNumber;
        private PropertyPath<String> _bookNumber;
        private PropertyPath<Integer> _entranceYear;
        private Group.Path<Group> _group;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private CompensationType.Path<CompensationType> _compensationType;
        private StudentStatus.Path<StudentStatus> _status;
        private Course.Path<Course> _course;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriodAuto;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;
        private PropertyPath<String> _comment;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private PropertyPath<Boolean> _eduDocumentOriginalHandedIn;
        private PropertyPath<Boolean> _personalEducation;
        private PropertyPath<Boolean> _targetAdmission;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _targetAdmissionOrgUnit;
        private PropertyPath<Boolean> _thriftyGroup;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<String> _finalQualifyingWorkTheme;
        private PpsEntry.Path<PpsEntry> _ppsEntry;
        private PropertyPath<String> _finalQualifyingWorkAdvisor;
        private PropertyPath<String> _practicePlace;
        private PropertyPath<String> _employment;
        private PropertyPath<Date> _contractDate;
        private AddressBase.Path<AddressBase> _officePostalAddress;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<Integer> _finishYear;
        private PropertyPath<String> _personalFileNumber;
        private PropertyPath<String> _personalArchivalFileNumber;
        private PropertyPath<String> _description;
        private PropertyPath<Date> _archivingDate;
        private PropertyPath<String> _localRoleContextTitle;
        private SupportedPropertyPath<String> _studentFullFioWithBirthYearGroupAndStatus;
        private SupportedPropertyPath<String> _titleWithFio;
        private SupportedPropertyPath<String> _titleWithoutFio;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Личный номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.employee.Student#getPersonalNumber()
     */
        public PropertyPath<String> personalNumber()
        {
            if(_personalNumber == null )
                _personalNumber = new PropertyPath<String>(StudentGen.P_PERSONAL_NUMBER, this);
            return _personalNumber;
        }

    /**
     * @return Номер зачетной книжки.
     * @see ru.tandemservice.uni.entity.employee.Student#getBookNumber()
     */
        public PropertyPath<String> bookNumber()
        {
            if(_bookNumber == null )
                _bookNumber = new PropertyPath<String>(StudentGen.P_BOOK_NUMBER, this);
            return _bookNumber;
        }

    /**
     * @return Год приема. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getEntranceYear()
     */
        public PropertyPath<Integer> entranceYear()
        {
            if(_entranceYear == null )
                _entranceYear = new PropertyPath<Integer>(StudentGen.P_ENTRANCE_YEAR, this);
            return _entranceYear;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.uni.entity.employee.Student#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getStatus()
     */
        public StudentStatus.Path<StudentStatus> status()
        {
            if(_status == null )
                _status = new StudentStatus.Path<StudentStatus>(L_STATUS, this);
            return _status;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Срок освоения (автообновляемое). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getDevelopPeriodAuto()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriodAuto()
        {
            if(_developPeriodAuto == null )
                _developPeriodAuto = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD_AUTO, this);
            return _developPeriodAuto;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uni.entity.employee.Student#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uni.entity.employee.Student#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(StudentGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Ссылка на документ, на основании которого студент поступил, и который хранится в личном деле.
     *
     * @return Документ об образовании.
     * @see ru.tandemservice.uni.entity.employee.Student#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return В личном деле хранится оригинал док. об образ.. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isEduDocumentOriginalHandedIn()
     */
        public PropertyPath<Boolean> eduDocumentOriginalHandedIn()
        {
            if(_eduDocumentOriginalHandedIn == null )
                _eduDocumentOriginalHandedIn = new PropertyPath<Boolean>(StudentGen.P_EDU_DOCUMENT_ORIGINAL_HANDED_IN, this);
            return _eduDocumentOriginalHandedIn;
        }

    /**
     * @return Индивидуальное образование. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isPersonalEducation()
     */
        public PropertyPath<Boolean> personalEducation()
        {
            if(_personalEducation == null )
                _personalEducation = new PropertyPath<Boolean>(StudentGen.P_PERSONAL_EDUCATION, this);
            return _personalEducation;
        }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isTargetAdmission()
     */
        public PropertyPath<Boolean> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<Boolean>(StudentGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @return Организация ЦП.
     * @see ru.tandemservice.uni.entity.employee.Student#getTargetAdmissionOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> targetAdmissionOrgUnit()
        {
            if(_targetAdmissionOrgUnit == null )
                _targetAdmissionOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_TARGET_ADMISSION_ORG_UNIT, this);
            return _targetAdmissionOrgUnit;
        }

    /**
     * @return Хоз. группа. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isThriftyGroup()
     */
        public PropertyPath<Boolean> thriftyGroup()
        {
            if(_thriftyGroup == null )
                _thriftyGroup = new PropertyPath<Boolean>(StudentGen.P_THRIFTY_GROUP, this);
            return _thriftyGroup;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.uni.entity.employee.Student#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(StudentGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Тема ВКР.
     * @see ru.tandemservice.uni.entity.employee.Student#getFinalQualifyingWorkTheme()
     */
        public PropertyPath<String> finalQualifyingWorkTheme()
        {
            if(_finalQualifyingWorkTheme == null )
                _finalQualifyingWorkTheme = new PropertyPath<String>(StudentGen.P_FINAL_QUALIFYING_WORK_THEME, this);
            return _finalQualifyingWorkTheme;
        }

    /**
     * @return Руководитель ВКР.
     * @see ru.tandemservice.uni.entity.employee.Student#getPpsEntry()
     */
        public PpsEntry.Path<PpsEntry> ppsEntry()
        {
            if(_ppsEntry == null )
                _ppsEntry = new PpsEntry.Path<PpsEntry>(L_PPS_ENTRY, this);
            return _ppsEntry;
        }

    /**
     * @return Руководитель ВКР (текстом).
     * @see ru.tandemservice.uni.entity.employee.Student#getFinalQualifyingWorkAdvisor()
     */
        public PropertyPath<String> finalQualifyingWorkAdvisor()
        {
            if(_finalQualifyingWorkAdvisor == null )
                _finalQualifyingWorkAdvisor = new PropertyPath<String>(StudentGen.P_FINAL_QUALIFYING_WORK_ADVISOR, this);
            return _finalQualifyingWorkAdvisor;
        }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.uni.entity.employee.Student#getPracticePlace()
     */
        public PropertyPath<String> practicePlace()
        {
            if(_practicePlace == null )
                _practicePlace = new PropertyPath<String>(StudentGen.P_PRACTICE_PLACE, this);
            return _practicePlace;
        }

    /**
     * @return Трудоустройство.
     * @see ru.tandemservice.uni.entity.employee.Student#getEmployment()
     */
        public PropertyPath<String> employment()
        {
            if(_employment == null )
                _employment = new PropertyPath<String>(StudentGen.P_EMPLOYMENT, this);
            return _employment;
        }

    /**
     * @return Дата договора.
     * @see ru.tandemservice.uni.entity.employee.Student#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(StudentGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Адрес (базовый).
     * @see ru.tandemservice.uni.entity.employee.Student#getOfficePostalAddress()
     */
        public AddressBase.Path<AddressBase> officePostalAddress()
        {
            if(_officePostalAddress == null )
                _officePostalAddress = new AddressBase.Path<AddressBase>(L_OFFICE_POSTAL_ADDRESS, this);
            return _officePostalAddress;
        }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.Student#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(StudentGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Год выпуска.
     * @see ru.tandemservice.uni.entity.employee.Student#getFinishYear()
     */
        public PropertyPath<Integer> finishYear()
        {
            if(_finishYear == null )
                _finishYear = new PropertyPath<Integer>(StudentGen.P_FINISH_YEAR, this);
            return _finishYear;
        }

    /**
     * @return Номер личного дела.
     * @see ru.tandemservice.uni.entity.employee.Student#getPersonalFileNumber()
     */
        public PropertyPath<String> personalFileNumber()
        {
            if(_personalFileNumber == null )
                _personalFileNumber = new PropertyPath<String>(StudentGen.P_PERSONAL_FILE_NUMBER, this);
            return _personalFileNumber;
        }

    /**
     * @return Номер личного дела в архиве.
     * @see ru.tandemservice.uni.entity.employee.Student#getPersonalArchivalFileNumber()
     */
        public PropertyPath<String> personalArchivalFileNumber()
        {
            if(_personalArchivalFileNumber == null )
                _personalArchivalFileNumber = new PropertyPath<String>(StudentGen.P_PERSONAL_ARCHIVAL_FILE_NUMBER, this);
            return _personalArchivalFileNumber;
        }

    /**
     * @return Описание (комментарий).
     * @see ru.tandemservice.uni.entity.employee.Student#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(StudentGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Дата добавления в архив.
     * @see ru.tandemservice.uni.entity.employee.Student#getArchivingDate()
     */
        public PropertyPath<Date> archivingDate()
        {
            if(_archivingDate == null )
                _archivingDate = new PropertyPath<Date>(StudentGen.P_ARCHIVING_DATE, this);
            return _archivingDate;
        }

    /**
     * @return 
     *
     * Это формула "person.identityCard.lastName".
     * @see ru.tandemservice.uni.entity.employee.Student#getLocalRoleContextTitle()
     */
        public PropertyPath<String> localRoleContextTitle()
        {
            if(_localRoleContextTitle == null )
                _localRoleContextTitle = new PropertyPath<String>(StudentGen.P_LOCAL_ROLE_CONTEXT_TITLE, this);
            return _localRoleContextTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.Student#getStudentFullFioWithBirthYearGroupAndStatus()
     */
        public SupportedPropertyPath<String> studentFullFioWithBirthYearGroupAndStatus()
        {
            if(_studentFullFioWithBirthYearGroupAndStatus == null )
                _studentFullFioWithBirthYearGroupAndStatus = new SupportedPropertyPath<String>(StudentGen.P_STUDENT_FULL_FIO_WITH_BIRTH_YEAR_GROUP_AND_STATUS, this);
            return _studentFullFioWithBirthYearGroupAndStatus;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.Student#getTitleWithFio()
     */
        public SupportedPropertyPath<String> titleWithFio()
        {
            if(_titleWithFio == null )
                _titleWithFio = new SupportedPropertyPath<String>(StudentGen.P_TITLE_WITH_FIO, this);
            return _titleWithFio;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.Student#getTitleWithoutFio()
     */
        public SupportedPropertyPath<String> titleWithoutFio()
        {
            if(_titleWithoutFio == null )
                _titleWithoutFio = new SupportedPropertyPath<String>(StudentGen.P_TITLE_WITHOUT_FIO, this);
            return _titleWithoutFio;
        }

        public Class getEntityClass()
        {
            return Student.class;
        }

        public String getEntityName()
        {
            return "student";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getStudentFullFioWithBirthYearGroupAndStatus();

    public abstract String getTitleWithFio();

    public abstract String getTitleWithoutFio();
}
