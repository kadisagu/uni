/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.FinishYearEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Ekaterina Zvereva
 * @since 10.01.2017
 */
@Input({@Bind(key = "studentId", binding = "student.id", required = true)})
public class UniStudentFinishYearEditUI extends UIPresenter
{
    private Student _student = new Student();

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(Student.class, _student.getId());
    }

    public void onClickApply()
    {
        ErrorCollector errors = UserContext.getInstance().getErrorCollector();
        if (getStudent().getFinishYear() != null)
        {
            if (getStudent().getFinishYear() < 1900)
                errors.add("Значение поля «Год выпуска» не может быть меньше 1900", "finishYear");

            if (getStudent().getFinishYear() < getStudent().getEntranceYear())
                errors.add("Значение поля «Год выпуска» должно быть не меньше, чем «Год приема».", "finishYear");
        }

        if (!errors.hasErrors())
        {
            DataAccessServices.dao().saveOrUpdate(_student);
            deactivate();
        }
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}