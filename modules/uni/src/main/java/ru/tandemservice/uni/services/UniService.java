/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.services;

import com.google.common.base.Preconditions;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author vip_delete
 */
@Zlo
public abstract class UniService implements IUniService
{
    @Override
    public final void execute()
    {
        //do not call directly from inherit classes
        if (getClass().isAssignableFrom(IUniService.class))
            throw new RuntimeException("You cannot call IUniService directly from inherite class:" + getClass().getName());

        IUniBaseDao.instance.get().doInTransaction(session -> {

            NamedSyncInTransactionCheckLocker.register(session, "globalUniServiceExecuteLock"); // см. реализацию любого UniService (там данные передаются через поля singleton-объектов)

            UniService.this.beforeExecute();
            UniService.this.doValidate();

            try {
                UniService.this.doExecute();
            } catch (Exception e) {
                throw CoreExceptionUtils.getRuntimeException(e);
            }

            UniService.this.afterExecute();

            return null;
        });
    }

    protected void beforeExecute()
    {
    }

    protected abstract void doValidate();

    protected abstract void doExecute() throws Exception;

    protected void afterExecute()
    {
    }

    protected IUniBaseDao getCoreDao()
    {
        return UniDaoFacade.getCoreDao();
    }

    protected class DoubleInitializeServiceError extends RuntimeException {
        private final UniService servie;

        public DoubleInitializeServiceError(UniService servie) {
            Preconditions.checkNotNull(servie);
            this.servie = servie;
        }

        @Override
        public String getMessage() {
            return "Uni service " + this.servie.getClass().getName() + " double initialized.";
        }
    }
}
