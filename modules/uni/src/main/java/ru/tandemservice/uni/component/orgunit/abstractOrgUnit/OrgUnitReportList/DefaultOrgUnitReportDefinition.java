/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.springframework.beans.factory.InitializingBean;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author vip_delete
 * @deprecated
 * @see org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition
 */
@Deprecated
public class DefaultOrgUnitReportDefinition implements IOrgUnitReportDefinition, InitializingBean
{
    private IOrgUnitReportBlockDefinition _block;
    private String _reportTitle;
    private String _componentName;
    private Object _parameters;
    private String _permissionPrefix;
    private IOrgUnitReportVisibleResolver _visibleResolver;

    @Override
    public void afterPropertiesSet() throws Exception
    {
        if (null == getBlock())
            throw new IllegalStateException();
    }

    // Getters & Setters

    @Override
    public IOrgUnitReportBlockDefinition getBlock()
    {
        return _block;
    }

    public void setBlock(IOrgUnitReportBlockDefinition block)
    {
        _block = block;
    }

    @Override
    public String getReportTitle()
    {
        return _reportTitle;
    }

    public void setReportTitle(String reportTitle)
    {
        _reportTitle = reportTitle;
    }

    public String getComponentName()
    {
        return _componentName;
    }

    public void setComponentName(String componentName)
    {
        _componentName = componentName;
    }

    public Object getParameters()
    {
        return _parameters;
    }

    public void setParameters(Object parameters)
    {
        _parameters = parameters;
    }

    @Override
    public String getPermissionPrefix()
    {
        return _permissionPrefix;
    }

    public void setPermissionPrefix(String permissionPrefix)
    {
        _permissionPrefix = permissionPrefix;
    }

    public IOrgUnitReportVisibleResolver getVisibleResolver()
    {
        return _visibleResolver;
    }

    public void setVisibleResolver(IOrgUnitReportVisibleResolver visibleResolver)
    {
        _visibleResolver = visibleResolver;
    }

    // Util

    @Override
    public boolean isVisible(OrgUnit orgUnit)
    {
        return _visibleResolver == null || _visibleResolver.isVisible(orgUnit);
    }

    @Override
    public IPublisherLinkResolver getLinkResolver()
    {
        return new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return _parameters;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return _componentName;
            }
        };
    }
}
