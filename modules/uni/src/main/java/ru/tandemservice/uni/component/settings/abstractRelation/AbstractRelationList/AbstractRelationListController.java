/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList;

import java.util.HashMap;
import java.util.Map;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public abstract class AbstractRelationListController<IDAO extends IAbstractRelationListDAO<Model>, Model extends AbstractRelationListModel> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        PublisherLinkColumn column = new PublisherLinkColumn("Название", "title");
        IPublisherLinkResolver resolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("firstId", entity.getId());
                return params;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return getRelationPubComponent();
            }
        };
        column.setResolver(resolver);
        dataSource.addColumn(column);
        model.setDataSource(dataSource);
    }

    protected abstract String getRelationPubComponent();
}
