/* $Id$ */
package ru.tandemservice.uni.component.catalog.studentCustomStateCI.StudentCustomStateCIAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

/**
 * @author nvankov
 * @since 3/27/13
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<StudentCustomStateCI, Model>
{
}
