package ru.tandemservice.uni.migration;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x5_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность developForm

		// создано свойство programForm
		{
			// создать колонку
			tool.createColumn("developform_t", new DBColumn("programform_id", DBType.LONG));
		}

		// создано свойство userCode
		{
			// создать колонку
			tool.createColumn("developform_t", new DBColumn("usercode_p", DBType.createVarchar(255)));
		}

		// создано свойство disabledDate
		{
			// создать колонку
			tool.createColumn("developform_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
		}

		//  свойство title стало обязательным
		{
			// сделать колонку NOT NULL
			tool.setColumnNullable("developform_t", "title_p", false);
		}


        // восстанавливаем недостающие элементы
        short developFormEntityCode = tool.entityCodes().ensure("developForm");

        Map<String, Object[]> missItemsMap = new HashMap<>(1);
        missItemsMap.put("1", new Object[] { "Очная", "очн", "" });                                                 // DevelopFormCodes.FULL_TIME_FORM
        missItemsMap.put("2", new Object[] { "Заочная", "заочн", "З" });                                            // DevelopFormCodes.CORESP_FORM
        missItemsMap.put("3", new Object[] { "Очно-заочная", "очн-заочн", "В" });                                   // DevelopFormCodes.PART_TIME_FORM
        missItemsMap.put("4", new Object[] { "Экстернат", "экст", "Э" });                                           // DevelopFormCodes.EXTERNAL_FORM
        missItemsMap.put("5", new Object[] { "Самостоятельное обучение и итоговая аттестация", "с изуч", "И" });    // DevelopFormCodes.APPLICANT_FORM

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select code_p from developform_t");
        ResultSet rs = stmt.getResultSet();
        Collection<String> itemCodes = new ArrayList<>();
        while (rs.next())
            itemCodes.add(rs.getString(1));

        Collection<String> missItemCodes = CollectionUtils.subtract(missItemsMap.keySet(), itemCodes);
        PreparedStatement insertMissItemsStmt = tool.getConnection().prepareStatement("insert into developform_t (id, discriminator, code_p, title_p, shorttitle_p, group_p) values (?, ?, ?, ?, ?, ?)");
        insertMissItemsStmt.setShort(2, developFormEntityCode);

        for (String missItemCode : missItemCodes)
        {
            Object[] values = missItemsMap.get(missItemCode);
            insertMissItemsStmt.setLong(1, EntityIDGenerator.generateNewId(developFormEntityCode));
            insertMissItemsStmt.setString(3, missItemCode);
            insertMissItemsStmt.setString(4, (String) values[0]); // title_p
            insertMissItemsStmt.setString(5, (String) values[1]); // shorttitle_p
            insertMissItemsStmt.setString(6, (String) values[2]); // group_p

            insertMissItemsStmt.execute();
        }

        // проставляем связь между develop form и program form
        Map<String, String> devFormCode2programFormCodeMap = new HashMap<>(2);
        {
            devFormCode2programFormCodeMap.put("1", "1");   // DevelopFormCodes.FULL_TIME_FORM
            devFormCode2programFormCodeMap.put("2", "2");   // DevelopFormCodes.CORESP_FORM
            devFormCode2programFormCodeMap.put("3", "3");   // DevelopFormCodes.PART_TIME_FORM
            devFormCode2programFormCodeMap.put("4", null);  // DevelopFormCodes.EXTERNAL_FORM
            devFormCode2programFormCodeMap.put("5", null);  // DevelopFormCodes.APPLICANT_FORM
        }

        PreparedStatement updateProgramFormCodeStmt = tool.getConnection().prepareStatement("update developform_t set programform_id = (select id from edu_c_pr_form_t where code_p = ?) where code_p = ?");
        for (Map.Entry<String, String> formEntry: devFormCode2programFormCodeMap.entrySet())
        {
            updateProgramFormCodeStmt.setString(1, formEntry.getValue());
            updateProgramFormCodeStmt.setString(2, formEntry.getKey());

            updateProgramFormCodeStmt.execute();
        }

        // устанавливаем дату запрещения для удаленных элементов
        PreparedStatement updateDisabledDate = tool.getConnection().prepareStatement("update developform_t set disableddate_p = ? where code_p = ?");
        updateDisabledDate.setDate(1, new java.sql.Date(new java.util.Date().getTime()));

        for (String missItemCode: missItemCodes)
        {
            updateDisabledDate.setString(2, missItemCode);
            updateDisabledDate.execute();
        }

        // создано обязательное свойство defaultTitle
		{
			// создать колонку
			tool.createColumn("developform_t", new DBColumn("defaulttitle_p", DBType.createVarchar(255)));

            Map<String, String> devFormCode2defaultTitleMap = new HashMap<>(2);
            {
                devFormCode2defaultTitleMap.put("1", "Очная");                                              // DevelopFormCodes.FULL_TIME_FORM
                devFormCode2defaultTitleMap.put("2", "Заочная");                                            // DevelopFormCodes.CORESP_FORM
                devFormCode2defaultTitleMap.put("3", "Очно-заочная");                                       // DevelopFormCodes.PART_TIME_FORM
                devFormCode2defaultTitleMap.put("4", "Экстернат");                                          // DevelopFormCodes.EXTERNAL_FORM
                devFormCode2defaultTitleMap.put("5", "Самостоятельное обучение и итоговая аттестация");     // DevelopFormCodes.APPLICANT_FORM
            }

            PreparedStatement updateDevelopFormDefaultTitlePS = tool.getConnection().prepareStatement("update developform_t set defaulttitle_p = ? where code_p = ?");
            for (Map.Entry<String, String> devFormEntry: devFormCode2defaultTitleMap.entrySet())
            {
                updateDevelopFormDefaultTitlePS.setString(1, devFormEntry.getValue());
                updateDevelopFormDefaultTitlePS.setString(2, devFormEntry.getKey());

                updateDevelopFormDefaultTitlePS.execute();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("developform_t", "defaulttitle_p", false);
		}
    }
}