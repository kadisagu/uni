package ru.tandemservice.uni.dao.mdbio;

import com.google.common.collect.Lists;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade.CacheEntry;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.base.entity.gen.AddressCountryGen;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class AddressIODao extends UniBaseDao implements IAddressIODao {

    public static final String ADDRESS_COUNTRY_T = "address_country_t";
    public static final String COLUMN_TEXT = "text";

    public static final String ADDRESS_SETTLEMENT_T = "address_settlement_t";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PARENT_ID = "parent_id";
    public static final String COLUMN_COUNTRY = "country";

    @Override
    public void export_addressCountry(final Database mdb) throws IOException
    {
        if (null != mdb.getTable(ADDRESS_COUNTRY_T)) { return; }

        final Table address_country_t = new TableBuilder(ADDRESS_COUNTRY_T)
        .addColumn(new ColumnBuilder(COLUMN_TEXT, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .toTable(mdb);

        for (final AddressCountry item: this.getList(AddressCountry.class, AddressCountryGen.P_CODE)) {
            address_country_t.addRow(AddressIODao.getCountryString(item));
        }
    }

    @Override
    public void export_addressSettlement(final Database mdb) throws IOException {
        if (null != mdb.getTable(ADDRESS_SETTLEMENT_T)) { return; }

        this.export_addressCountry(mdb);

        final Table address_settlement_t = new TableBuilder(ADDRESS_SETTLEMENT_T)
        .addColumn(new ColumnBuilder(COLUMN_TEXT, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder(COLUMN_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder(COLUMN_PARENT_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder(COLUMN_COUNTRY, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .toTable(mdb);

        final Session session = this.getSession();
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AddressItem.class, "e0").column(property("e0.id"));
        dql.joinPath(DQLJoinType.left, "e0.parent", "e1");
        dql.joinPath(DQLJoinType.left, "e1.parent", "e2");
        dql.joinPath(DQLJoinType.left, "e2.parent", "e3");
        dql.order(property("e3.id"));
        dql.order(property("e2.id"));
        dql.order(property("e1.id"));
        dql.order(property("e0.id"));

        for (final List<Long> idsPart : Lists.partition(dql.createStatement(session).<Long>list(), DQL.MAX_VALUES_ROW_NUMBER)) {
            for (final AddressItem item: AddressIODao.this.getList(AddressItem.class, idsPart)) {
                address_settlement_t.addRow(
                        AddressIODao.getSettlementString(item),
                        Long.toHexString(item.getId()),
                        (null == item.getParent() ? null : Long.toHexString(item.getParent().getId())),
                        AddressIODao.getCountryString(item.getCountry())
                );
            }
            session.clear();
        }
    }


    public static String getCountryString(final AddressCountry country) {
        if (null == country) { return null; }
        return (country.getCode() + " " + country.getTitle());
    }

    public static String getCitizenshipString(final ICitizenship citizenship) {
        if (null == citizenship) { return null; }
        return (citizenship.getCode() + " " + citizenship.getTitle());
    }

    public static String getCountryString(final Address address) {
        if (null == address) { return null; }
        return AddressIODao.getCountryString(address.getCountry());
    }

    public static String getSettlementString(final AddressItem settlement) {
        if (null == settlement) { return null; }

        String result = null;

        AddressItem item = settlement;
        while (null != item)
        {
            final StringBuilder sb = new StringBuilder();
            final String postcode = StringUtils.trimToNull(item.getInheritedPostCode());
            if (null != postcode) {
                sb.append(postcode).append("# ");
            }

            sb.append(item.getTitleWithType());
            item = item.getParent();

            final String s = sb.toString().replace(';', ' ');
            result = ((null == result) ? s : (s+";\n"+result));
        }

        return StringUtils.trimToNull(result);
    }

    public static String getSettlementString(final Address address) {
        if (null == address) { return null; }
        return AddressIODao.getSettlementString(address.getSettlement());
    }

    public static String getPostcode(final AddressStreet street) {
        if (null == street) { return null; }

        final String postcode = StringUtils.trimToNull(street.getPostcode());
        if (null != postcode) { return postcode+"#"; }

        return AddressIODao.getPostcode(street.getParent());
    }

    public static String getPostcode(AddressItem item) {
        while (null != item) {
            final String postcode = StringUtils.trimToNull(item.getInheritedPostCode());
            if (null != postcode) { return postcode+"#"; }
            item = item.getParent();
        }
        return null;
    }

    public static String getStreetString(final AddressStreet street) {
        if (null == street) { return null; }
        return StringUtils.trimToNull(StringUtils.trimToEmpty(AddressIODao.getPostcode(street)) + " " + street.getTitle());
    }

    public static String getStreetString(final Address address) {
        if (null == address) { return null; }
        return AddressIODao.getStreetString(address.getStreet());
    }



    ////////////////////////////////////////////////////////////////////////////////////////

    private final DaoCacheFacade.CacheEntryDefinition<String, Long> CACHE_ADDRESS_COUNTRY = new DaoCacheFacade.CacheEntryDefinition<String, Long>(
    AddressIODao.class.getSimpleName()+"-"+AddressCountry.class.getSimpleName(), 1
    ) {
        @Override public void fill(final Map<String, Long> cache, final Collection<String> ids) {
            for (final String id: ids) { cache.put(id, this.get(id)); }
        }

        private Long get(final String id) {
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AddressCountry.class, "e").column(property("e.id"));

            final int i = id.indexOf(' ');
            if (i < 0)
            {
                // одно единственное значение - код
                final Integer code = Integer.valueOf(id);
                dql.where(eq(
                        property("e.code"),
                        value(code)
                ));
            }
            else
            {
                // код + название - юзаем код и название
                final Integer code = Integer.valueOf(id.substring(0, i));
                dql.where(eq(
                        property("e.code"),
                        value(code)
                ));

                final String title = StringUtils.trimToNull(id.substring(i));
                if (null != title) {
                    dql.where(eq(
                            DQLFunctions.lower(property("e.title")),
                            value(title)
                    ));
                }
            }

            final Long uniqueResult = dql.createStatement(AddressIODao.this.getSession()).setMaxResults(1).<Long>uniqueResult();
            if (null == uniqueResult) {
                UserContext.getInstance().getErrorCollector().add("Отсутствует запись для страны: «"+id+"»");
            }
            return uniqueResult;
        }
    };

    @Override
    public AddressCountry findCountry(String countryString)
    {
        countryString = StringUtils.trimToNull(countryString);
        if (null == countryString) { return null; }
        countryString = countryString.toLowerCase();

        final CacheEntry<String, Long> cache = DaoCacheFacade.getEntry(CACHE_ADDRESS_COUNTRY);
        final Long id = cache.getRecords(Collections.singleton(countryString)).get(countryString);
        if (null == id) {
            throw new IllegalStateException("No entry in database for the country: «"+countryString+"»");
        }

        return (AddressCountry)this.getSession().load(AddressCountry.class, id);
    }

    private final DaoCacheFacade.CacheEntryDefinition<String, Long> CACHE_I_CITIZENSHIP = new DaoCacheFacade.CacheEntryDefinition<String, Long>(
            AddressIODao.class.getSimpleName()+"-"+AddressCountry.class.getSimpleName(), 1
    ) {
        @Override public void fill(final Map<String, Long> cache, final Collection<String> ids) {
            for (final String id: ids) { cache.put(id, this.get(id)); }
        }

        private Long get(final String id) {
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ICitizenship.class, "e").column(property("e.id"));

            final int i = id.indexOf(' ');
            if (i < 0)
            {
                // одно единственное значение - код
                final Integer code = Integer.valueOf(id);
                dql.where(eq(
                        property("e.code"),
                        value(code)
                ));
            }
            else
            {
                // код + название - юзаем код и название
                final Integer code = Integer.valueOf(id.substring(0, i));
                dql.where(eq(
                        property("e.code"),
                        value(code)
                ));

                final String title = StringUtils.trimToNull(id.substring(i));
                if (null != title) {
                    dql.where(eq(
                            DQLFunctions.lower(property("e.title")),
                            value(title)
                    ));
                }
            }

            final Long uniqueResult = dql.createStatement(AddressIODao.this.getSession()).setMaxResults(1).<Long>uniqueResult();
            if (null == uniqueResult) {
                UserContext.getInstance().getErrorCollector().add("Отсутствует запись для гражданства: «"+id+"»");
            }
            return uniqueResult;
        }
    };

    @Override
    public ICitizenship findCitizenship(String citizenshipString)
    {
        citizenshipString = StringUtils.trimToNull(citizenshipString);
        if (null == citizenshipString) { return null; }
        citizenshipString = citizenshipString.toLowerCase();

        final CacheEntry<String, Long> cache = DaoCacheFacade.getEntry(CACHE_I_CITIZENSHIP);
        final Long id = cache.getRecords(Collections.singleton(citizenshipString)).get(citizenshipString);
        if (null == id) {
            throw new IllegalStateException("No entry in database for the citizenship: «"+citizenshipString+"»");
        }

        return (ICitizenship)this.getSession().load(ICitizenship.class, id);
    }



    ////////////////////////////////////////////////////////////////////////////////////////

    private final DaoCacheFacade.CacheEntryDefinition<MultiKey, Long> CACHE_ADDRESS_SETTLEMENT = new DaoCacheFacade.CacheEntryDefinition<MultiKey, Long>(
    AddressIODao.class.getSimpleName()+"-"+AddressItem.class.getSimpleName(), 1
    ){
        @Override public void fill(final Map<MultiKey, Long> cache, final Collection<MultiKey> ids) {
            for (final MultiKey id: ids) { cache.put(id, this.get(id)); }
        }

        private Long get(final MultiKey key) {
            final Long countryId = (Long)key.getKey(0);
            final String title = (String)key.getKey(1);

            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AddressItem.class, "e").column(property("e.id"));
            dql.where(eq(property("e.country.id"), value(countryId)));

            String alias = null;
            for (String part: StringUtils.split(title, ';'))
            {
                part = StringUtils.trimToNull(part);
                if (null == part) { continue; }

                part = part.replaceAll("[ ]+", " ");

                if (null == alias) {
                    alias = "p";
                    dql.fromEntity(AddressItem.class, alias);
                } else {
                    final String new_alias = (alias+"_");
                    dql.fromEntity(AddressItem.class, new_alias);
                    dql.where(eq(property(alias), property(new_alias, "parent")));
                    alias = new_alias;
                }

                final int i = part.indexOf('#');
                if (i > 0)
                {
                    // есть почтовый код
                    final String postcode = StringUtils.trimToNull(part.substring(0, i));
                    if (null != postcode) {
                        dql.where(eq(
                                property(alias + "." + AddressItem.P_INHERITED_POST_CODE),
                                value(postcode)
                        ));
                    }
                    part = part.substring(1+i);
                }

                // оставшаяся строка - это название (или название + префикс типа)
                part = StringUtils.trimToNull(part);
                if (null == part) { continue; }

                dql.where(or(
                        eq(
                                DQLFunctions.lower(property(alias, AddressItem.title())),
                                value(part)
                        ),
                        eq(
                                DQLFunctions.lower(DQLFunctions.concat(
                                        property(alias, AddressItem.addressType().shortTitle()), value(" "), property(alias, AddressItem.title())
                                )),
                                value(part)
                        )
                ));
            }

            if (null != alias) {
                dql.where(eq(property("e"), property(alias)));
            }

            return dql.createStatement(AddressIODao.this.getSession()).setMaxResults(1).<Long>uniqueResult();
        }
    };

    @Override
    public AddressItem findSettlement(final AddressCountry country, String settlementString, final boolean require)
    {
        settlementString = StringUtils.trimToNull(settlementString);
        if (null == settlementString) { return null; }


        if (null == country) {
            this.logger.error("AddressItem["+settlementString.replace('\n', ' ')+"]: country is null");
            if (require) {
                throw new IllegalStateException("Country is not specified but there is the settlement");
            }
            return null;
        }

        final CacheEntry<MultiKey, Long> cache = DaoCacheFacade.getEntry(CACHE_ADDRESS_SETTLEMENT);
        final MultiKey key = new MultiKey(country.getId(), settlementString.toLowerCase());
        final Long id = cache.getRecords(Collections.singleton(key)).get(key);
        if (null == id) {
            settlementString = settlementString.replace('\n', ' ');
            AddressIODao.this.logger.error("AddressItem["+settlementString+", country="+country.getTitle()+"]: no database value");
            if (require) {

                throw new IllegalStateException("No entry in database for the settlement: «"+settlementString+"» (country "+country.getTitle()+")");
            }
            return null;
        }

        return (AddressItem)this.getSession().load(AddressItem.class, id);
    }







    ////////////////////////////////////////////////////////////////////////////////////////

    private final DaoCacheFacade.CacheEntryDefinition<MultiKey, Long> CACHE_ADDRESS_STREET = new DaoCacheFacade.CacheEntryDefinition<MultiKey, Long>(
    AddressIODao.class.getSimpleName()+"-"+AddressStreet.class.getSimpleName(), 1
    ){
        @Override public void fill(final Map<MultiKey, Long> cache, final Collection<MultiKey> ids) {
            for (final MultiKey id: ids) { cache.put(id, this.get(id)); }
        }

        private Long get(final Long settlementId, final String postcode, final String title) {

            if (null == title) { return null; }

            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AddressStreet.class, "e").column(property("e.id"));
            dql.where(eq(property("e.parent.id"), value(settlementId)));

            if (null != postcode) {
                final DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(AddressItem.class, "e0").column(property("e0.id"));
                subDql.joinPath(DQLJoinType.left, "e0.parent", "e1");
                subDql.where(or(
                        eq(
                                property("e0.postcode"),
                                value(postcode)
                        ),
                        eq(
                                property("e1.postcode"),
                                value(postcode)
                        )
                ));

                dql.where(
                    or(
                            eq(
                                    property("e.postcode"),
                                    value(postcode)
                            ),
                            DQLExpressions.in(
                                    property("e.parent.id"),
                                    subDql.buildQuery()
                            )
                    )
                );
            }

            dql.where(
                eq(
                        DQLFunctions.lower(property("e", AddressItem.title())),
                        value(title)
                )
            );

            return dql.createStatement(AddressIODao.this.getSession()).setMaxResults(1).<Long>uniqueResult();
        }

        private Long get(MultiKey key)
        {
            final Long settlementId = (Long)key.getKey(0);
            final String title = (String)key.getKey(1);
            final int i = title.indexOf('#');
            if (i > 0)
            {
                // есть почтовый код
                final String postcode = StringUtils.trimToNull(title.substring(0, i));
                if (null != postcode) {
                    final Long uniqueResult = this.get(settlementId, postcode, StringUtils.trimToNull(title.substring(1+i)));
                    if (null != uniqueResult) { return uniqueResult; }
                }
            }

            return this.get(settlementId, null, title);
        }
    };


    @Override
    public AddressStreet findStreet(final AddressItem settlement, String streetString, final boolean require)
    {
        streetString = StringUtils.trimToNull(streetString);
        if (null == streetString) { return null; }

        if (null == settlement) {
            this.logger.error("AddressStreet["+streetString.replace('\n', ' ')+"]: settlement is null");
            if (require) {
                throw new IllegalStateException("The settlement is not specified but there is the street");
            }
            return null;
        }

        streetString = streetString.toLowerCase();
        final CacheEntry<MultiKey, Long> cache = DaoCacheFacade.getEntry(CACHE_ADDRESS_STREET);
        final MultiKey key = new MultiKey(settlement.getId(), streetString);
        final Long id = cache.getRecords(Collections.singleton(key)).get(key);
        if (null == id) {
            AddressIODao.this.logger.error("AddressStreet["+streetString.replace('\n', ' ')+", settlement="+settlement.getTitle()+"]: no database value");
            if (require) {
                throw new IllegalStateException("No entry in database for the street: «"+streetString.replace('\n', ' ')+"» (settlement "+settlement.getTitle()+")");
            }
            return null;
        }

        return (AddressStreet)this.getSession().load(AddressStreet.class, id);
    }




}
