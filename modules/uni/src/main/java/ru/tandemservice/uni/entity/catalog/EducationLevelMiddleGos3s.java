package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelMiddleGos3sGen;

/**
 * Направление подготовки (специальность) СПО (2013)
 */
public class EducationLevelMiddleGos3s extends EducationLevelMiddleGos3sGen
{
    public EducationLevelMiddleGos3s()
    {
        setCatalogCode("educationLevelMiddleGos3s");
    }
}