/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentPassportExpiredReport.ui.Add;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.PageOrientation;
import jxl.read.biff.BiffException;
import jxl.write.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.process.*;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.gen.StudentStatusGen;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 10.06.2016
 */
@State({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class StudentPassportExpiredReportAddUI extends UIPresenter
{
    private Long orgUnitId;
    private byte[] report = null;

    public static final String DATE = "date";
    public static final String STUDENT_STATUS = "studentStatus";

    @Override
    public void onComponentRefresh()
    {
        UniEduProgramEducationOrgUnitAddon addon = (UniEduProgramEducationOrgUnitAddon) this.getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        addon.configDoubleWidthFilters(Boolean.TRUE);
        Date date = getSettings().get(DATE);
        if (date == null) {
            getSettings().set(DATE, new Date());
        }
        List<StudentStatus> status = getSettings().get(STUDENT_STATUS);
        if (CollectionUtils.isEmpty(status)) {
            String alias = "alias";
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentStatus.class, alias)
                    .where(eq(property(StudentStatus.usedInSystem().fromAlias(alias)), value(Boolean.TRUE)))
                    .where(eq(property(alias, StudentStatus.active()), value(true)));
            List<StudentStatus> studentStatusList = dql.createStatement(getSupport().getSession()).list();
            getSettings().set(STUDENT_STATUS, studentStatusList);
        }
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (report != null) {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName("Сведения о просроченных паспортах.xls").document(report), true);
            report = null;
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
    }

    public void filter(DQLSelectBuilder dql, String alias, UniEduProgramEducationOrgUnitAddon addon)
    {
        dql.where(eq(property(alias, Student.person().identityCard().cardType().code()), value(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII)));
        dql.where(eq(property(alias, Student.archival()), value(Boolean.FALSE)));

        Date date = getSettings().get(DATE);
        if (date != null) {
            dql.where(
                    or(
                            and(
                                    le(DQLFunctions.addyears(property(alias, Student.person().identityCard().birthDate()), value(20)), value(date, PropertyType.DATE)),
                                    lt(property(alias, Student.person().identityCard().issuanceDate()), DQLFunctions.addyears(property(alias, Student.person().identityCard().birthDate()), value(20)))
                            ),
                            and(
                                    le(DQLFunctions.addyears(property(alias, Student.person().identityCard().birthDate()), value(45)), value(date, PropertyType.DATE)),
                                    lt(property(alias, Student.person().identityCard().issuanceDate()), DQLFunctions.addyears(property(alias, Student.person().identityCard().birthDate()), value(45)))
                            )
                    )
            );
        }

        if (orgUnitId != null) {
            dql.where(or(
                    eq(property(alias, Student.educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)),
                    eq(property(alias, Student.educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId))
            ));
        }

        if (addon != null) {
            addon.applyFilters(dql, Student.educationOrgUnit().fromAlias(alias).s());
        }

        List<StudentStatus> status = getSettings().get(STUDENT_STATUS);
        if (CollectionUtils.isNotEmpty(status)) {
            dql.where(in(property(alias, Student.status()), status));
        }
        dql.order(property(alias, Student.person().identityCard().fullFio()));
    }

    public void onClickPrint()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState processState)
            {
                final UniEduProgramEducationOrgUnitAddon addon = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
                String alias = "student";
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, alias);
                filter(dql, alias, addon);
                List<Student> studentList = dql.createStatement(getSupport().getSession()).list();
                if (CollectionUtils.isEmpty(studentList)) {
                    throw new ApplicationException("Невозможно построить отчет, т.к. для выбранных параметров не найдены студенты.");
                }

                try {
                    report = buildReport(studentList, addon);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new ApplicationException("Возникла ошибка при печати отчета.");
                }
                return null;
            }
        };
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", process, ProcessDisplayMode.unknown));

    }

    public byte[] buildReport(List<Student> studentList, UniEduProgramEducationOrgUnitAddon addon) throws IOException, BiffException, WriteException
    {

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем шрифты
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // шрифт для названий фильтров
        WritableCellFormat filterFormat = new WritableCellFormat(arial8bold);
        filterFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        filterFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        filterFormat.setWrap(true);

        // шрифт для шапки отчета
        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setBackground(jxl.format.Colour.GRAY_25);
        headerFormat.setWrap(false);

        // шрифт для строк отчета
        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);


        WritableSheet sheet = workbook.createSheet("Сведения о просроченных паспортах", workbook.getSheets().length);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);

        //setting up filter info
        int indent = 0;
        Date date = getSettings().get(DATE);
        if (date != null) {
            sheet.addCell(new Label(1, indent, "На дату", filterFormat));
            sheet.addCell(new Label(2, indent, DateFormatter.DEFAULT_DATE_FORMATTER.format(date), rowFormat));
            indent++;
        }

        List<StudentStatus> statusList = getSettings().get(STUDENT_STATUS);
        if (CollectionUtils.isNotEmpty(statusList)) {
            sheet.addCell(new Label(1, indent, "Статус студента", filterFormat));
            sheet.addCell(new Label(2, indent, statusList.stream().map(StudentStatusGen::getTitle).collect(Collectors.joining(System.lineSeparator())), rowFormat));
            indent++;
        }

        TreeMap<Integer, PairKey> filterTitleMap = new TreeMap<>();
        addon.getValuesMap().forEach((filters, o) -> {
            if (CollectionUtils.isNotEmpty((Collection) o)) {
                int position = ((ArrayList) UniEduProgramEducationOrgUnitAddon.Filters.BASE_FILTER_SET).indexOf(filters);
                String selection = StringUtils.join((ArrayList) o, System.lineSeparator());
                filterTitleMap.put(position, new PairKey<>(filters.getFieldName(), selection));
            }
        });

        for (Map.Entry<Integer, PairKey> filter : filterTitleMap.entrySet()) {
            sheet.addCell(new Label(1, indent, (String) filter.getValue().getFirst(), filterFormat));
            sheet.addCell(new Label(2, indent, (String) filter.getValue().getSecond(), rowFormat));
            indent++;
        }

        indent++;

        // setting up headers
        String[] headers = {
                "№",
                "Фамилия имя отчество",
                "Группа",
                "Формирующее подразделение (Институт)",
                "Дата рождения",
                "Дата выдачи паспорта",
                "E-mail",
                "Контактный телефон",
                "Мобильный телефон",
                "Рабочий телефон",
                "Телефон по месту временной регистрации",
                "Телефон по месту постоянной регистрации",
                "Телефон по месту фактического проживания"
        };

        int rowPointer = 0;
        int columnPointer = 0;
        for (String title : headers) {
            sheet.addCell(new Label(columnPointer, rowPointer = indent, title, headerFormat));

            CellView cell = sheet.getColumnView(columnPointer);
            if (columnPointer == 2) {
                cell.setSize(6000); // special for filter columns
            } else {
                cell.setSize(title.length() * 300 + 500);// cell.autosize works wrong
            }

            sheet.setColumnView(columnPointer++, cell);
        }

        for (Student student : studentList) {
            columnPointer = 0;
            rowPointer++;
            sheet.addCell(new Label(columnPointer++, rowPointer, String.valueOf(rowPointer - indent), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getFullFio(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getGroup() != null ? student.getGroup().getTitle() : "", rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getEducationOrgUnit().getFormativeOrgUnit().getPrintTitle(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getBirthDateStr(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getIssuanceDate()), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getEmail(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getContactData().getPhoneDefault(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getContactData().getPhoneMobile(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getContactData().getPhoneWork(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getContactData().getPhoneRegTemp(), rowFormat));
            sheet.addCell(new Label(columnPointer++, rowPointer, student.getPerson().getContactData().getPhoneReg(), rowFormat));
            sheet.addCell(new Label(columnPointer, rowPointer, student.getPerson().getContactData().getPhoneFact(), rowFormat));
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    public void onClickClear()
    {
        getSettings().clear();
        UniEduProgramEducationOrgUnitAddon addon = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (addon != null) {
            addon.clearFilters();
        }
    }

    public String getViewKey()
    {
        return getOrgUnitId() == null ? "studentPassportExpiredReport" : "orgUnit_viewStudentPassportExpiredReport_" + StringUtils.uncapitalize(DataAccessServices.dao().get(OrgUnit.class, orgUnitId).getOrgUnitType().getCode());
    }

    @Override
    public ISecured getSecuredObject()
    {
        return orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, orgUnitId) : super.getSecuredObject();
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }
}