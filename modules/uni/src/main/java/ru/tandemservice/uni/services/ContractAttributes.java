/* $Id$ */
package ru.tandemservice.uni.services;

import java.util.Date;

/**
 * Данные о договоре на обучение студента/абитуриента
 *
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class ContractAttributes
{
    private String _number;
    private Date _date;

    public ContractAttributes(String number, Date date)
    {
        _number = number;
        _date = date;
    }

    public String getNumber()
    {
        return _number;
    }

    public Date getDate()
    {
        return _date;
    }
}