/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.tandemframework.core.velocity.VelocityManager;
import org.tandemframework.core.view.formatter.DateFormatter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;

/**
 * TODO: Требует рефакторинга
 *
 * @author vip_delete
 */
@Deprecated
public class CatalogComponentsGenerator
{
    private static String _catalogEntityName;
    private static String _domain;
    private static String _company;
    private static String _module;
    private static String _entityName;
    private static String _simpleName;
    private static String _moduleDir;
    private static boolean _printCatalog;

    public static void main(String[] args) throws Exception
    {
        if (args.length != 1 && args.length != 2)
        {
            System.out.println("Usage: " + CatalogComponentsGenerator.class.getSimpleName() + " <catalogClassName>");
            System.exit(1);
        }
        if (args.length == 2)
        {
            if (args[1].equals("print-catalog"))
                _printCatalog = true;
            else
            {
                System.out.println("Second argument must be 'print-catalog' or empty");
                System.exit(1);
            }
        }
        _catalogEntityName = args[0];
        String[] items = StringUtils.split(_catalogEntityName, '.');
        _domain = items[0];
        _company = items[1];
        _module = items[2];
        _simpleName = items[items.length - 1];
        _entityName = StringUtils.uncapitalize(_simpleName);

        File moduleDir = getModulesDir();
        _moduleDir = moduleDir.getAbsolutePath();
        String componentPath = _moduleDir + "/" + _module + "/src/main/java/" + _domain + "/" + _company + "/" + _module + "/component/catalog/" + _entityName;

        String[] postfixList = new String[]{"Pub", "ItemPub", "AddEdit"};
        String[] pathList = new String[postfixList.length];

        for (int i = 0; i < postfixList.length; i++)
            pathList[i] = componentPath + "/" + _simpleName + postfixList[i];

        initVelocity();

        for (int i = 0; i < pathList.length; i++)
        {
            String path = pathList[i];
            if (!new File(path).exists())
            {
                FileUtils.forceMkdir(new File(path));
                createComponent(path, postfixList[i]);
            }
        }
    }

    private static void createComponent(String path, String postfix) throws Exception
    {
        FileUtils.forceMkdir(new File(path));
        createComponentClass(path + "/Model.java", "model" + postfix);
        createComponentClass(path + "/IDAO.java", "idao" + postfix);
        createComponentClass(path + "/DAO.java", "dao" + postfix);
        createComponentClass(path + "/Controller.java", "controller" + postfix);
        createHTMLTemplate(path, postfix);
    }

    private static void createComponentClass(String path, String templateName) throws Exception
    {
        VelocityContext context = new VelocityContext();
        context.put("domain", _domain);
        context.put("company", _company);
        context.put("module", _module);
        context.put("entityName", _entityName);
        context.put("simpleName", _simpleName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", _catalogEntityName);

        Template template;
        if (_printCatalog)
            template = Velocity.getTemplate("ru/tandemservice/uni/util/system/templates/printcatalog/" + templateName + ".vm");
        else
            template = Velocity.getTemplate("ru/tandemservice/uni/util/system/templates/catalog/" + templateName + ".vm");

        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(path)), "UTF-8");
        //BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        template.merge(context, writer);
        writer.flush();
        writer.close();
    }

    private static void createHTMLTemplate(String path, String postfix) throws Exception
    {
        File srcFile;
        if (_printCatalog)
            srcFile = new File(_moduleDir + "/uni/src/main/java/ru/tandemservice/uni/util/system/templates/html/AbstractPrintCatalog" + postfix + ".html");
        else
            srcFile = new File(_moduleDir + "/uni/src/main/java/ru/tandemservice/uni/util/system/templates/catalog/DefaultCatalog" + postfix + ".html");
        File destFile = new File(path + "/" + _simpleName + postfix + ".html");
        FileUtils.copyFile(srcFile, destFile);
    }

    private static void initVelocity() throws Exception
    {
        VelocityManager.init();

        //        Velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "class");
        //        Velocity.setProperty("class.resource.loader.class", TemplateLoader.class.getCanonicalName());
        //        Velocity.setProperty(RuntimeConstants.ENCODING_DEFAULT, "UTF-8");
        //        Velocity.setProperty(RuntimeConstants.INPUT_ENCODING, "UTF-8");
        //        Velocity.setProperty(RuntimeConstants.OUTPUT_ENCODING, "UTF-8");
        //        Velocity.init();
    }

    private static File getModulesDir()
    {
        String modules = System.getProperty("modules");
        if (StringUtils.isEmpty(modules))
            throw new RuntimeException("System property 'modules' is not specified");
        File file = new File(modules);
        if (!file.exists())
            throw new RuntimeException("Path '" + modules + "' is not exist");
        if (!file.isDirectory())
            throw new RuntimeException("Path '" + modules + "' is not a directory");
        return file;
    }
}
