/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentPersonCard;

import java.util.List;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.io.RtfReader;

import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vip_delete
 * @since 18.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("deprecation")
    @Override
    public void prepare(Model model)
    {
        model.setStudent(get(Student.class, model.getStudent().getId()));
        List<org.tandemframework.shared.person.base.entity.PersonEduInstitution> personEduInstitutionList = getList(org.tandemframework.shared.person.base.entity.PersonEduInstitution.class, org.tandemframework.shared.person.base.entity.PersonEduInstitution.L_PERSON, model.getStudent().getPerson(), org.tandemframework.shared.person.base.entity.PersonEduInstitution.P_ISSUANCE_DATE);
        List<PersonNextOfKin> personNextOfKinList = getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, model.getStudent().getPerson());

         model.setPrintFactory((IStudentPersonCardPrintFactory) ApplicationRuntime.getApplicationContext().getBean(IStudentPersonCardPrintFactory.FACTORY_SPRING));
         IData data = model.getPrintFactory().getData();
         data.setTemplate(new RtfReader().read(getCatalogItem(TemplateDocument.class, UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT).getContent()));
         data.setFileName(IData.FILENAME);

         // CREATE PRINT DATA
         model.getPrintFactory().initPrintData(model.getStudent(), personEduInstitutionList, personNextOfKinList, getSession());
    }
}
