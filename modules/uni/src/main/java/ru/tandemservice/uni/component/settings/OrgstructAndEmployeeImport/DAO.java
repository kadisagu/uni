/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.settings.OrgstructAndEmployeeImport;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.IOrgUnitDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnitTypeRestriction;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author ekachanova
 * @since 22.02.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final String U4_FacultyInstitute = "U4_Факультет_институт";
    private static final String U5_CathedraLab = "U5_Кафедры_лаборатории";
    private static final String C15_2ndLevelOrgUnitPrefix = "C15_Префиксф_подр_2_уровня";
    private static final String C5_Post = "C5_Должность";
    private static final String M7_EmployeePost = "M7_Должности_по_штатному";
    private static final String U20_Person = "U20_Личности";
    private static final String C17_Name = "C17_ИМЕНА";
    private static final String U6_OrgUnitHead = "U6_Руководители_подразделений";

    @Override
    public void update(Model model)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), IOrgUnitDao.ORG_UNIT_HIERARCHY_LOCK);

        //todo
        try
        {
            //инициализация
            List<OrgUnitType> orgUnitTypes = getCatalogItemList(OrgUnitType.class);
            OrgUnitType facultyUnitType = getCatalogItem(OrgUnitType.class, OrgUnitTypeCodes.FACULTY);
            QualificationLevel qualificationLevel = getDefaultQualificationLevel();
            ProfQualificationGroup profQualificationGroup = getDefaultProfQualificationGroup();
            AddressCountry russia = get(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE);
            PostType postType = getCatalogItem(PostType.class, UniDefines.POST_TYPE_MAIN_JOB);
            EmployeePostStatus postStatus = getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE);
            Sex maleSex = getCatalogItem(Sex.class, SexCodes.MALE);
            Sex femaleSex = getCatalogItem(Sex.class, SexCodes.FEMALE);
            IdentityCardType identityCardType = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT);
            EmployeeType employeeType = getCatalogItem(EmployeeType.class, EmployeeTypeCodes.WORK_STAFF);

            Calendar minDateOfBirthCal = GregorianCalendar.getInstance();
            minDateOfBirthCal.set(Calendar.YEAR, 1900);
            Date minDateOfBirth = minDateOfBirthCal.getTime();

            Map<String, Post> postByTitle = new HashMap<String, Post>();
            for(Post post : getCatalogItemList(Post.class))
            {
                postByTitle.put(post.getTitle().toLowerCase(), post);
            }
            Map<String, PostBoundedWithQGandQL> postBoundedWithQGandQLByPostCode = new HashMap<String, PostBoundedWithQGandQL>();
            for(PostBoundedWithQGandQL p : getCatalogItemList(PostBoundedWithQGandQL.class))
            {
                postBoundedWithQGandQLByPostCode.put(p.getPost().getCode(), p);
            }
            Map<MultiKey, OrgUnitTypePostRelation> orgUnitTypePostRelations = new HashMap<MultiKey, OrgUnitTypePostRelation>();
            {
                for(OrgUnitTypePostRelation orgUnitTypePostRelation : new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel").<OrgUnitTypePostRelation>getResultList(getSession()))
                {
                   orgUnitTypePostRelations.put(new MultiKey(orgUnitTypePostRelation.getPostBoundedWithQGandQL().getPost().getCode(), orgUnitTypePostRelation.getOrgUnitType().getCode()), orgUnitTypePostRelation);
                }
            }

            //подразделения 1-го уровня
            Database db = Database.open(new File(model.getFilePath()));
            Table facultyInstituteTable = db.getTable(U4_FacultyInstitute);
            Cursor facultyInstituteCursor = new CursorBuilder(facultyInstituteTable).toCursor();
            Iterator<Map<String,Object>> facultyInstituteIterator = facultyInstituteCursor.iterator();
            Map<String, OrgUnit> _1stLevelOrgUnitCode2OrgUnit = new HashMap<String, OrgUnit>();
            for(;facultyInstituteIterator.hasNext();)
            {
                Map<String, Object> values = facultyInstituteIterator.next();
                String codeCol = "Код_факультета";
                String titleCol = "Полное_наименование_факультета";
                String titleGenitiveCol = "Полное_наименование_факультета_в_родительном_падеже";
                String shortTitleCol = "Краткое_наименование_факультета";
                String divisionCodeCol = "Номер_подразделения";
                String code = String.valueOf(values.get(codeCol));
                String title = values.get(titleCol) != null ? String.valueOf(values.get(titleCol)) : null;
                String titleGenitive = values.get(titleGenitiveCol) != null ? String.valueOf(values.get(titleGenitiveCol)) : null;
                String shortTitle = values.get(shortTitleCol) != null ? String.valueOf(values.get(shortTitleCol)) : null;
                String divisionCode = values.get(divisionCodeCol) != null ? String.valueOf(values.get(divisionCodeCol)) : null;

                if(StringUtils.isEmpty(title))
                {
                    continue;
                }
                OrgUnitType orgUnitType = getOrgUnitType(title, orgUnitTypes);
                if(orgUnitType == null)
                {
                   orgUnitType = facultyUnitType;
                }
                OrgUnit orgUnit = getOrgUnit(title, orgUnitType);
                if(orgUnit != null)
                {
                    _1stLevelOrgUnitCode2OrgUnit.put(code, orgUnit);
                    continue;
                }
                _1stLevelOrgUnitCode2OrgUnit.put(code, createOrgUnit(TopOrgUnit.getInstance(), orgUnitType, title, shortTitle, titleGenitive, divisionCode));
            }

            //подразделения 2-го уровня
            Table _2ndLevelPrefixTable = db.getTable(C15_2ndLevelOrgUnitPrefix);
            Cursor _2ndLevelPrefixCursor = new CursorBuilder(_2ndLevelPrefixTable).toCursor();
            Iterator<Map<String,Object>> _2ndLevelPrefixIterator = _2ndLevelPrefixCursor.iterator();
            Map<String, String> _2ndLevelCode2Title = new HashMap<String, String>();
            for(;_2ndLevelPrefixIterator.hasNext();)
            {
                Map<String, Object> values = _2ndLevelPrefixIterator.next();
                Object code = values.get("Id_префикса");
                Object value = values.get("Префикс_в_именит");
                if(!CoreUtils.isNull(code) && !CoreUtils.isNull(value))
                {
                    _2ndLevelCode2Title.put(String.valueOf(code), String.valueOf(value));
                }
            }
            Table cathedraLabTable = db.getTable(U5_CathedraLab);
            Cursor cathedraLabCursor = new CursorBuilder(cathedraLabTable).toCursor();
            Iterator<Map<String,Object>> cathedraLabIterator = cathedraLabCursor.iterator();
            Map<String, OrgUnit> _2ndLevelOrgUnitCode2OrgUnit = new HashMap<String, OrgUnit>();
            for(;cathedraLabIterator.hasNext();)
            {
                Map<String, Object> values = cathedraLabIterator.next();
                String _1stLevelOrgUnitCodeCol = "Код_факультета";
                String titleCol = "Полное_название_кафедры";
                String _2ndLevelCodeCol = "Id_префикса";
                String codeCol = "Код_кафедры_лаборатории";

                String code = String.valueOf(values.get(codeCol));
                String _2ndLevelCode = values.get(_2ndLevelCodeCol) != null ? String.valueOf(values.get(_2ndLevelCodeCol)) : null;

                String orgUnitTypeTitle = _2ndLevelCode != null ? _2ndLevelCode2Title.get(_2ndLevelCode) : null;
                OrgUnitType orgUnitType = getOrgUnitType(orgUnitTypeTitle, orgUnitTypes);

                String title = values.get(titleCol) != null ? String.valueOf(values.get(titleCol)) : null;
                OrgUnit orgUnit = getOrgUnit(title, orgUnitType);
                if(orgUnit != null)
                {
                    _2ndLevelOrgUnitCode2OrgUnit.put(code, orgUnit);
                    continue;
                }
                String _1stLevelOrgUnitCode = values.get(_1stLevelOrgUnitCodeCol) != null ? String.valueOf(values.get(_1stLevelOrgUnitCodeCol)) : null;
                if(StringUtils.isEmpty(_1stLevelOrgUnitCode))
                {
                   continue;
                }
                OrgUnit parent = _1stLevelOrgUnitCode2OrgUnit.get(_1stLevelOrgUnitCode);
                if(parent == null)
                {
                    continue;
                }
                if(orgUnitType != null && !StringUtils.isEmpty(title))
                {
                    updateOrgUnitTypeRestriction(parent.getOrgUnitType(), orgUnitType);
                    _2ndLevelOrgUnitCode2OrgUnit.put(code, createOrgUnit(parent, orgUnitType, title, null, null, null));
                }
                else if(orgUnitType == null)
                {
                    _2ndLevelOrgUnitCode2OrgUnit.put(code, parent);
                }
            }

            //Должности
            Table postTable = db.getTable(C5_Post);
            Cursor postCursor = new CursorBuilder(postTable).toCursor();
            Iterator<Map<String,Object>> postIterator = postCursor.iterator();
            Map<String, String> postTitleByCode = new HashMap<String, String>();
            for(;postIterator.hasNext();)
            {
                Map<String, Object> values = postIterator.next();
                if(!CoreUtils.isNull(values.get("Наименование_должности")))
                {
                    String postTitle = String.valueOf(values.get("Наименование_должности")).toLowerCase();
                    if(postTitle.startsWith("заведующий"))
                    {
                        postTitle = postTitle.replace("заведующий", "зав.");
                    }
                    postTitleByCode.put(String.valueOf(values.get("Код_должности")), postTitle);
                }
            }
            Map<String, Post> postByCode = new HashMap<String, Post>();

            Table orgUnitTypePostTable = db.getTable(M7_EmployeePost);
            Cursor orgUnitTypePostCursor = new CursorBuilder(orgUnitTypePostTable).toCursor();
            Iterator<Map<String,Object>> orgUnitTypePostIterator = orgUnitTypePostCursor.iterator();
            for(;orgUnitTypePostIterator.hasNext();)
            {
                Map<String, Object> values = orgUnitTypePostIterator.next();
                String codeCol = "Код_должности_в_штате";
                String postCodeCol = "Код_должности";
                String _2ndLevelCodeCol = "Код_кафедры_лаборатории";

                ((Number)values.get(codeCol)).intValue();
                String postCode = values.get(postCodeCol) != null ? String.valueOf(values.get(postCodeCol)) : null;
                String _2ndLevelCode = values.get(_2ndLevelCodeCol) != null ? String.valueOf(values.get(_2ndLevelCodeCol)) : null;
                processOrgUnitTypePostRelation(postCode, postByCode, postByTitle, postTitleByCode, _2ndLevelCode,
                        _2ndLevelOrgUnitCode2OrgUnit, postBoundedWithQGandQLByPostCode, employeeType,
                        qualificationLevel, profQualificationGroup, orgUnitTypePostRelations);

            }

            //имена персон
            Map<Integer, Name> nameById = new HashMap<Integer, Name>();
            Table nameTable = db.getTable(C17_Name);
            Cursor nameCursor = new CursorBuilder(nameTable).toCursor();
            Iterator<Map<String,Object>> nameIterator = nameCursor.iterator();
            for(;nameIterator.hasNext();)
            {
                Map<String, Object> values = nameIterator.next();
                String idCol = "Id_имени";
                String nameCol = "Имя";
                String middleNameMaleNameCol = "Отчество_МУЖ_им";
                String middleNameFemaleNameCol = "Отчество_ЖЕН_им";

                Integer id = ((Number)values.get(idCol)).intValue();
                String name = values.get(nameCol) == null ? null : String.valueOf(values.get(nameCol));
                if(StringUtils.isEmpty(name))
                {
                    continue;
                }
                nameById.put(id, new Name(id, name,
                        values.get(middleNameMaleNameCol) == null ? null : String.valueOf(values.get(middleNameMaleNameCol)),
                        values.get(middleNameFemaleNameCol) == null ? null : String.valueOf(values.get(middleNameFemaleNameCol))));
            }

            //персоны, сотрудники
            Table personTable = db.getTable(U20_Person);
            Cursor personCursor = new CursorBuilder(personTable).toCursor();
            Iterator<Map<String,Object>> personIterator = personCursor.iterator();
            Map<Integer, EmployeePost> employeePostById = new HashMap<Integer, EmployeePost>();
            for(;personIterator.hasNext();)
            {
                Map<String, Object> values = personIterator.next();
                String idCol = "Person_ID";
                String maleCol = "Мужчина";
                String lastNameCol = "Фамилия";
                String firstNameCol = "Имя";
                String middleNameCol = "Отчество";
                String dateOfBirthCol = "Дата_рождения";
                String postCol = "Должность1";
                String _2ndLevelCodeCol = "Кафедра";

                Integer personId = ((Number)values.get(idCol)).intValue();
                Boolean male = values.get(maleCol) instanceof Boolean ? (Boolean)values.get(maleCol) : true;
                String lastName = values.get(lastNameCol) == null ? null : String.valueOf(values.get(lastNameCol));
                if(StringUtils.isEmpty(lastName))
                {
                    continue;
                }
                Integer firstNameId = values.get(firstNameCol) == null ? null : ((Number)values.get(firstNameCol)).intValue();
                Name firstName = nameById.get(firstNameId);
                if(firstName == null || StringUtils.isEmpty(firstName._name))
                {
                    continue;
                }
                Integer middleNameId = values.get(middleNameCol) == null ? null : ((Number)values.get(middleNameCol)).intValue();
                Name middleName = nameById.get(middleNameId);
                Date dateOfBirth = values.get(dateOfBirthCol) instanceof Date ? (Date)values.get(dateOfBirthCol) : null;
                String postCode = values.get(postCol) == null ? null : String.valueOf(values.get(postCol));
                String _2ndLevelCode = values.get(_2ndLevelCodeCol) == null ? null : String.valueOf(values.get(_2ndLevelCodeCol));
                if(postCode == null || _2ndLevelCode == null)
                {
                    continue;
                }
                processOrgUnitTypePostRelation(postCode, postByCode, postByTitle, postTitleByCode, _2ndLevelCode,
                        _2ndLevelOrgUnitCode2OrgUnit, postBoundedWithQGandQLByPostCode, employeeType,
                        qualificationLevel, profQualificationGroup, orgUnitTypePostRelations);
                Post post = postByCode.get(postCode);
                OrgUnit orgUnit = _2ndLevelOrgUnitCode2OrgUnit.get(_2ndLevelCode);
                if(post == null || orgUnit == null)
                {
                    continue;
                }
                OrgUnitTypePostRelation postRelation = orgUnitTypePostRelations.get(new MultiKey(post.getCode(), orgUnit.getOrgUnitType().getCode()));
                if(postRelation == null)
                {
                    continue;
                }

                IdentityCard identityCard = new IdentityCard();
                identityCard.setLastName(lastName);
                identityCard.setFirstName(firstName._name);
                if(middleName != null)
                {
                    identityCard.setMiddleName(male ? middleName._middleNameMaleName : middleName._middleNameFemaleName);
                }
                identityCard.setCitizenship(russia);
                identityCard.setSex(male ? maleSex : femaleSex);
                if(dateOfBirth != null && minDateOfBirth.before(dateOfBirth))
                {
                    identityCard.setBirthDate(dateOfBirth);
                }
                identityCard.setCardType(identityCardType);

                ErrorCollector errors = ContextLocal.getErrorCollector();
                PersonManager.instance().dao().checkIdentityCard(identityCard, errors);
                if (errors.hasErrors())
                {
                    continue;
                }

                PersonContactData contactData = new PersonContactData();

                Person person = new Person();
                person.setIdentityCard(identityCard);
                person.setContactData(contactData);

                Employee employee = getEmployee(person);
                if(employee == null)
                {
                    getSession().save(contactData);
                    getSession().save(identityCard);
                    getSession().save(person);

                    employee = new Employee();
                    employee.setPerson(person);
                    employee.setEmployeeCode(EmployeeManager.instance().dao().getUniqueEmployeeCode());
                    getSession().save(employee);
                }

                List<EmployeePost> employeePosts = getEmployeePosts(employee, postRelation);
                EmployeePost employeePost;
                if(employeePosts.isEmpty())
                {
                    employeePost = new EmployeePost();
                    employeePost.setEmployee(employee);
                    employeePost.setOrgUnit(orgUnit);
                    employeePost.setPostRelation(postRelation);
                    employeePost.setPostType(postType);
                    employeePost.setPostStatus(postStatus);
                    getSession().save(employeePost);
                }
                else
                {
                    employeePost = employeePosts.get(0);
                }
                employeePostById.put(personId, employeePost);
            }

            //руководители подразделений
            Table orgUnitHeadTable = db.getTable(U6_OrgUnitHead);
            Cursor orgUnitHeadCursor = new CursorBuilder(orgUnitHeadTable).toCursor();
            Iterator<Map<String,Object>> orgUnitHeadIterator = orgUnitHeadCursor.iterator();
            for(;orgUnitHeadIterator.hasNext();)
            {
                Map<String, Object> values = orgUnitHeadIterator.next();
                String idCol = "Id_руководитель";
                String personIdCol = "Person_ID";
                String _1stLevelOrgUnitIdCol = "Код_факультета";

                ((Number)values.get(idCol)).intValue();
                Integer personId = values.get(personIdCol) == null ? null : ((Number)values.get(personIdCol)).intValue();
                String _1stLevelOrgUnitCode = values.get(_1stLevelOrgUnitIdCol) == null ? null : String.valueOf(values.get(_1stLevelOrgUnitIdCol));
                if(personId == null || _1stLevelOrgUnitCode == null
                        || !_1stLevelOrgUnitCode2OrgUnit.containsKey(_1stLevelOrgUnitCode)
                        || !employeePostById.containsKey(personId))
                {
                    continue;
                }
                OrgUnit orgUnit = _1stLevelOrgUnitCode2OrgUnit.get(_1stLevelOrgUnitCode);
                orgUnit.setHead(employeePostById.get(personId));
                getSession().update(orgUnit);
            }
        }
        catch(IOException e)
        {
            throw new ApplicationException("Ошибка чтения данных из файла, загрузка данных невозможна.");
        }
    }

    private void processOrgUnitTypePostRelation(String postCode, Map<String, Post> postByCode,
                                                Map<String, Post> postByTitle,
                                                Map<String, String> postTitleByCode,
                                                String _2ndLevelCode, Map<String, OrgUnit> _2ndLevelOrgUnitCode2OrgUnit,
                                                Map<String, PostBoundedWithQGandQL> postBoundedWithQGandQLByPostCode,
                                                EmployeeType employeeType, QualificationLevel qualificationLevel,
                                                ProfQualificationGroup profQualificationGroup,
                                                Map<MultiKey, OrgUnitTypePostRelation> orgUnitTypePostRelations)
    {
        if(postCode == null)
        {
            return;
        }
        OrgUnit _2ndLevelOrgUnit = _2ndLevelCode != null ? _2ndLevelOrgUnitCode2OrgUnit.get(_2ndLevelCode) : null;
        if(_2ndLevelOrgUnit == null)
        {
            return;
        }

        //поиск, создание Должности/профессии сотрудников ОУ
        String postTitle = postTitleByCode.get(postCode);
        if(postTitle == null)
        {
            return;
        }
        Post post = postByTitle.get(postTitle);
        if(post == null)
        {
            post = new Post();
            post.setCode(getNewCatalogItemCode(Post.class));
            post.setEmployeeType(employeeType);
            post.setTitle(StringUtils.capitalize(postTitle));
            getSession().save(post);
            postByTitle.put(postTitle.toLowerCase(), post);
        }
        postByCode.put(postCode, post);

        //поиск, создание Должности и профессии, отнесенные к ПКГ и КУ
        PostBoundedWithQGandQL postBoundedWithQGandQL = postBoundedWithQGandQLByPostCode.get(post.getCode());
        if(postBoundedWithQGandQL == null)
        {
            postBoundedWithQGandQL = new PostBoundedWithQGandQL();
            postBoundedWithQGandQL.setCode(getNewCatalogItemCode(PostBoundedWithQGandQL.class));
            postBoundedWithQGandQL.setQualificationLevel(qualificationLevel);
            postBoundedWithQGandQL.setProfQualificationGroup(profQualificationGroup);
            postBoundedWithQGandQL.setPost(post);
            postBoundedWithQGandQL.setTitle(post.getTitle());
            getSession().save(postBoundedWithQGandQL);
            postBoundedWithQGandQLByPostCode.put(post.getCode(), postBoundedWithQGandQL);
        }
        //поиск, создание OrgUnitTypePostRelation
        MultiKey orgUnitTypePostRelationKey = new MultiKey(postBoundedWithQGandQL.getPost().getCode(), _2ndLevelOrgUnit.getOrgUnitType().getCode());
        OrgUnitTypePostRelation orgUnitTypePostRelation = orgUnitTypePostRelations.get(orgUnitTypePostRelationKey);
        if(orgUnitTypePostRelation == null)
        {
            orgUnitTypePostRelation = new OrgUnitTypePostRelation();
            orgUnitTypePostRelation.setOrgUnitType(_2ndLevelOrgUnit.getOrgUnitType());
            orgUnitTypePostRelation.setPostBoundedWithQGandQL(postBoundedWithQGandQL);
            //orgUnitTypePostRelation.setHeaderPost(true);
            orgUnitTypePostRelation.setMultiPost(true);
            getSession().save(orgUnitTypePostRelation);
            orgUnitTypePostRelations.put(new MultiKey(postBoundedWithQGandQL.getPost().getCode(), _2ndLevelOrgUnit.getOrgUnitType().getCode()), orgUnitTypePostRelation);
        }
    }

    private OrgUnitType getOrgUnitType(String orgUnitTitle, List<OrgUnitType> orgUnitTypes)
    {
        if(StringUtils.isEmpty(orgUnitTitle))
        {
            return null;
        }
        for(OrgUnitType orgUnitType : orgUnitTypes)
        {
            if(StringUtils.containsIgnoreCase(orgUnitTitle, orgUnitType.getTitle()))
            {
                return orgUnitType;
            }
        }
        return null;
    }

    private OrgUnit getOrgUnit(String title, OrgUnitType orgUnitType)
    {
        if(orgUnitType == null)
        {
            return null;
        }
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.orgUnitType(), orgUnitType));
        builder.add(MQExpression.like("ou", OrgUnit.title(), title));
        List<OrgUnit> list = builder.getResultList(getSession());
        return list.isEmpty() ? null : list.get(0);
    }

    private void updateOrgUnitTypeRestriction(OrgUnitType parent, OrgUnitType child)
    {
        MQBuilder builder = new MQBuilder(OrgUnitTypeRestriction.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", OrgUnitTypeRestriction.parent(), parent));
        builder.add(MQExpression.eq("r", OrgUnitTypeRestriction.child(), child));
        if(builder.getResultList(getSession()).isEmpty())
        {
            OrgUnitTypeRestriction orgUnitTypeRestriction = new OrgUnitTypeRestriction();
            orgUnitTypeRestriction.setParent(parent);
            orgUnitTypeRestriction.setChild(child);
            getSession().save(orgUnitTypeRestriction);
        }
    }

    private OrgUnit createOrgUnit(OrgUnit parent, OrgUnitType orgUnitType, String orgUnitTitle,
                                  String shortTitle, String titleGenitive, String divisionCode) throws RuntimeException
    {
        OrgUnit orgUnit = new OrgUnit();
        orgUnit.setParent(parent);
        orgUnit.setOrgUnitType(orgUnitType);
        orgUnit.setTitle(orgUnitTitle);
        orgUnit.setShortTitle(StringUtils.trimToEmpty(shortTitle));
        orgUnit.setGenitiveCaseTitle(titleGenitive);
        orgUnit.setDivisionCode(divisionCode);

        getSession().save(orgUnit);

        return orgUnit;
    }

    private String getNewCatalogItemCode(Class<? extends ICatalogItem> catalogItemClass)
    {
        int count = getCount(catalogItemClass);
        while (getCatalogItem(catalogItemClass, Integer.toString(count)) != null)
        {
            count++;
        }
        return String.valueOf(count);
    }

    private QualificationLevel getDefaultQualificationLevel()
    {
        List<QualificationLevel> items = getCatalogItemList(QualificationLevel.class);
        if(!items.isEmpty())
        {
            return items.get(0);
        }
        QualificationLevel qualificationLevel = new QualificationLevel();
        qualificationLevel.setCode(getNewCatalogItemCode(QualificationLevel.class));
        qualificationLevel.setTitle("1");
        qualificationLevel.setShortTitle("1");
        getSession().save(qualificationLevel);
        return qualificationLevel;
    }

    private ProfQualificationGroup getDefaultProfQualificationGroup()
    {
        List<ProfQualificationGroup> items = getCatalogItemList(ProfQualificationGroup.class);
        if(!items.isEmpty())
        {
            return items.get(0);
        }
        ProfQualificationGroup profQualificationGroup = new ProfQualificationGroup();
        profQualificationGroup.setCode(getNewCatalogItemCode(ProfQualificationGroup.class));
        profQualificationGroup.setTitle("1");
        profQualificationGroup.setShortTitle("1");
        getSession().save(profQualificationGroup);
        return profQualificationGroup;
    }

    private Employee getEmployee(Person person)
    {
        MQBuilder builder = new MQBuilder(Employee.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", Employee.person(), "p");
        builder.addJoinFetch("p", Person.identityCard(), "i");
        builder.add(MQExpression.eq("i", IdentityCard.lastName(), person.getIdentityCard().getLastName()));
        builder.add(MQExpression.eq("i", IdentityCard.firstName(), person.getIdentityCard().getFirstName()));
        if(person.getIdentityCard().getBirthDate() != null)
        {
            builder.add(MQExpression.eq("i", IdentityCard.birthDate(), person.getIdentityCard().getBirthDate()));
        }
        if (person.getId() != null)
        {
            builder.add(MQExpression.notEq("p", Person.P_ID, person.getId()));
        }
        List<Employee> list = builder.getResultList(getSession());
        return list.isEmpty() ? null : list.get(0);
    }

    private List<EmployeePost> getEmployeePosts(Employee employee, OrgUnitTypePostRelation postRelation)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.eq("ep", EmployeePost.employee(), employee));
        builder.add(MQExpression.eq("ep", EmployeePost.postRelation(), postRelation));
        return builder.getResultList(getSession());
    }

    private class Name
    {
        String _name;
        String _middleNameMaleName;
        String _middleNameFemaleName;

        Name(Integer id, String name, String middleNameMaleName, String middleNameFemaleName)
        {
            _name = name;
            _middleNameMaleName = middleNameMaleName;
            _middleNameFemaleName = middleNameFemaleName;
        }
    }

}
