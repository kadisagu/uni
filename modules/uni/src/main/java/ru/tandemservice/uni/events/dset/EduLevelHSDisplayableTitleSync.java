/* $Id$ */
package ru.tandemservice.uni.events.dset;

import org.hibernate.engine.SessionImplementor;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniSyncDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

/**
 * Обновляет у EducationLevelsHighSchool поля displayableTitle, fullTitle и fullTitleExtended.
 * Запускается только при условии изменения полей объектов, влияющих на эти поля
 * <p/>
 * Изменения применяются в конце транзакции
 * <p/>
 * При каждом изменении алгоритма формирования этих полей требуется увеличивать
 * версию алгоритма в методе init, чтобы при следующем старте эти поля
 * автоматически обновили свои значения в соответствии с новым алгоритмом
 *
 * @author Vasily Zhukov
 * @since 29.06.2011
 */
public class EduLevelHSDisplayableTitleSync implements IDSetEventListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EducationLevelsHighSchool.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationLevelsHighSchool.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EducationLevelsHighSchool.class, this);

        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationLevels.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, StructureEducationLevels.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, OrgUnit.class, this);

        // если изменился алгоритм формирования названий, то при старте нужно обновить эти поля

        final String SETTING_NAME = "version";
        final Integer ALGORITHM_VERSION = 6;

        IDataSettings setting = DataSettingsFacade.getSettings(EduLevelHSDisplayableTitleSync.class.getSimpleName());

        if (!ALGORITHM_VERSION.equals(setting.get(SETTING_NAME)))
        {
            IEventServiceLock eventLock = CoreServices.eventService().lock();
            try
            {
                IUniSyncDao.instance.get().doUpdateEduHSTitles();
            } finally
            {
                eventLock.release();
            }
            setting.set(SETTING_NAME, ALGORITHM_VERSION);
            DataSettingsFacade.saveSettings(setting);
        }
    }

    private static final ThreadLocal<Sync> syncs = new ThreadLocal<>();

    @Override
    public void onEvent(DSetEvent event)
    {
        if (event.getMultitude().isSingular())
        {
            onEvent(event, event.getMultitude().getSingularEntity());
        } else
        {
            for (Long id : new DQLSelectBuilder()
                    .fromDataSource(event.getMultitude().getSource(), "s").column("s.id")
                    .createStatement(event.getContext()).<Long>list())
            {
                // пересчет уже запланирован
                if (syncs.get() != null) break;

                onEvent(event, DataAccessServices.dao().get(id));
            }
        }
    }

    private void onEvent(DSetEvent event, IEntity entity)
    {
        Sync sync = syncs.get();

        // пересчет уже запланирован
        if (sync != null) return;

        // получаем изменившиеся свойства
        Set propertySet = event.getMultitude().getAffectedProperties();

        // true, если нужно планировать пересчет
        boolean need = false;

        if (entity instanceof EducationLevelsHighSchool)
        {
            // нужно пересчитывать если выполнено одно из следующих:
            // 1. добавился или удалился объект
            // 2. изменилось НПОУ
            // 3. изменилось название
            // 4. изменилось выпускающее подр.
            // 5. изменилась присваиваемая квалификация
            // 6. изменилась ориентация ОП
            need = event.getEventType().equals(DSetEventType.afterInsert) || event.getEventType().equals(DSetEventType.afterDelete) ||
                    !Collections.disjoint(propertySet, Arrays.asList(EducationLevelsHighSchool.L_EDUCATION_LEVEL,
                                                                     EducationLevelsHighSchool.P_TITLE,
                                                                     EducationLevelsHighSchool.L_ORG_UNIT,
                                                                     EducationLevelsHighSchool.L_ASSIGNED_QUALIFICATION,
                                                                     EducationLevelsHighSchool.L_PROGRAM_ORIENTATION));
        } else if (entity instanceof EducationLevels)
        {
            // нужно пересчитывать если выполнено одно из следующих:
            // 1. изменилось ОКСО
            // 2. изменился парент
            // 3. изменилась ступень образования (в интерфейсе невозможно, но на всякий случай)
            need = propertySet.contains(EducationLevels.P_OKSO) || propertySet.contains(EducationLevels.L_PARENT_LEVEL) || propertySet.contains(EducationLevels.L_LEVEL_TYPE);
        } else if (entity instanceof StructureEducationLevels)
        {
            // нужно пересчитывать если выполнено одно из следующих:
            // 1. изменилось сокр.название у рутового элемента
            // 2. изменилось название
            // 3. изменился парент (в интерфейсе не возможно, но на всякий случай)
            need = (((StructureEducationLevels) entity).getParent() == null && propertySet.contains(StructureEducationLevels.P_SHORT_TITLE)) || propertySet.contains(StructureEducationLevels.P_TITLE) || propertySet.contains(StructureEducationLevels.L_PARENT);
        } else if (entity instanceof OrgUnit)
        {
            // нужно пересчитывать если выполнено одно из следующих:
            // 1. изменилось сокр. название
            need = propertySet.contains(OrgUnit.P_SHORT_TITLE);
        }

        if (need)
        {
            syncs.set(sync = new Sync(event.getContext().getSessionImplementor()));

            event.getContext().getTransaction().registerSynchronization(sync);
        }
    }

    private static final class Sync implements Synchronization
    {
        private SessionImplementor _session;

        public Sync(SessionImplementor session)
        {
            _session = session;
        }

        @Override
        public void beforeCompletion()
        {
            IEventServiceLock eventLock = CoreServices.eventService().lock();

            try
            {
                boolean hasUpdate = IUniSyncDao.instance.get().doUpdateEduHSTitles();

                if (hasUpdate)
                    _session.flush();
            } finally
            {
                eventLock.release();
            }
        }

        @Override
        public void afterCompletion(int i)
        {
            syncs.remove();
        }
    }
}
