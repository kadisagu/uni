/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author vip_delete
 */
public interface IUniDao<Model> extends IUniBaseDao, IUpdateable<Model>, IListDataSourceDao<Model>
{
    String UNI_DAO_BEAN_NAME = "uniDao";

    @Override
    void prepare(Model model);

    @Override
    void update(Model model);

    @Override
    void validate(Model model, ErrorCollector errors);

    @Override
    void deleteRow(IBusinessComponent component);

    boolean isUnique(Class<? extends IEntity> clazz, Long id, String property, Object value);
}
