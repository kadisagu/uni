/* $Id$ */
package ru.tandemservice.uni.base.bo.UniGroup.ui.StudentListAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 15.11.2013
 */
@Configuration
public class UniGroupStudentListAdd extends BusinessComponentManager
{
    public static final String STUDENT_DS = "studentDS";
    public static final String GROUP = "group";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().addDataSource(searchListDS(STUDENT_DS, getStudentsDS(), studentsDSHandler())).create();
    }

    @Bean
    public ColumnListExtPoint getStudentsDS()
    {
        return columnListExtPointBuilder(STUDENT_DS)
                .addColumn(checkboxColumn("selected"))
                .addColumn(textColumn(Student.P_FULL_FIO, Student.P_FULL_FIO))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentsDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Group group = context.get(GROUP);
                EducationOrgUnit eou = group.getEducationOrgUnit();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                        .where(eq(property(Student.status().code().fromAlias("s")), value(UniDefines.CATALOG_STUDENT_STATUS_ACTIVE)))
                        .where(isNull(property(Student.group().fromAlias("s"))))
                        .where(eq(property(Student.course().fromAlias("s")), value(group.getCourse())))
                        .where(eq(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("s")), value(eou.getFormativeOrgUnit())))
                        .where(eq(property(Student.educationOrgUnit().developCondition().fromAlias("s")), value(eou.getDevelopCondition())))
                        .where(eq(property(Student.educationOrgUnit().developForm().fromAlias("s")), value(eou.getDevelopForm())))
                        .where(eq(property(Student.educationOrgUnit().developPeriod().fromAlias("s")), value(eou.getDevelopPeriod())))
                        .where(eq(property(Student.educationOrgUnit().developTech().fromAlias("s")), value(eou.getDevelopTech())))
                        .order(property(Student.person().identityCard().fullFio().fromAlias("s")));

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
            }
        };
    }
}
