/**
 * $Id$
 */
package ru.tandemservice.uni.ui;

import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;

/**
 * @author dseleznev
 * Created on: 16.04.2010
 */
public class OrgUnitExtendedAutocompleteModel extends UniSimpleAutocompleteModel
{
    boolean _ommitRootOrgUnit = false;

    public OrgUnitExtendedAutocompleteModel()
    {
        super();
    }

    public OrgUnitExtendedAutocompleteModel(boolean ommitRootOrgUnit)
    {
        super();
        _ommitRootOrgUnit = ommitRootOrgUnit;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ListResult findValues(String filter)
    {
        return OrgUnitManager.instance().dao().getOrgUnitList(filter, 50, true);
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return ((OrgUnit) value).getFullTitle();
    }
}