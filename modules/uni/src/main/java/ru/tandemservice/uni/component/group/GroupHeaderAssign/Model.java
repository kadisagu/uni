/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.GroupHeaderAssign;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IOrgUnitTypeModel;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
@Input(keys = "groupId", bindings = "group.id")
public class Model implements IOrgUnitTypeModel
{
    private Group _group = new Group();
    private OrgUnitType _orgUnitType;
    private OrgUnit _orgUnit;
    private ISelectModel _orgUnitTypeListModel;
    private ISelectModel _orgUnitListModel;
    private ISelectModel _employeePostListModel;

    @Override
    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        _orgUnitType = orgUnitType;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public ISelectModel getOrgUnitTypeListModel()
    {
        return _orgUnitTypeListModel;
    }

    public void setOrgUnitTypeListModel(ISelectModel orgUnitTypeListModel)
    {
        _orgUnitTypeListModel = orgUnitTypeListModel;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public ISelectModel getEmployeePostListModel()
    {
        return _employeePostListModel;
    }

    public void setEmployeePostListModel(ISelectModel employeePostListModel)
    {
        _employeePostListModel = employeePostListModel;
    }
}