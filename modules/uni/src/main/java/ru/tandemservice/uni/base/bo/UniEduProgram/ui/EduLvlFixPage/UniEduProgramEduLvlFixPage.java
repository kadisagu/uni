package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlFixPage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

@Configuration
public class UniEduProgramEduLvlFixPage extends BusinessComponentManager 
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}
