/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.uni.base.ext.OrgUnit.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.IDaoExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.IOrgUnitDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation;

import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 12.03.2012
 */
public class DaoUpdateOrgUnitChangeType extends UniBaseDao implements IDaoExtension
{
    @Override
    public void handle(String stateName, Map<String, Object> params)
    {
        // перед изменением типа нужно проверить настройку видов подразделений
        if (IDaoExtension.BEFORE_VALIDATE.equals(stateName))
        {
            OrgUnit orgUnit = (OrgUnit) params.get(IOrgUnitDao.PARAM_ORG_UNIT);
            OrgUnitType orgUnitType = (OrgUnitType) params.get(IOrgUnitDao.PARAM_ORG_UNIT_TYPE);

            List<OrgUnitKind> kindOldList = getPropertiesList(OrgUnitTypeToKindRelation.class, OrgUnitTypeToKindRelation.orgUnitType(), orgUnit.getOrgUnitType(), false, OrgUnitTypeToKindRelation.orgUnitKind().id());
            List<OrgUnitKind> kindNewList = getPropertiesList(OrgUnitTypeToKindRelation.class, OrgUnitTypeToKindRelation.orgUnitType(), orgUnitType, false, OrgUnitTypeToKindRelation.orgUnitKind().id());

            if (!kindNewList.containsAll(kindOldList)) {
                throw new ApplicationException("Невозможно изменить тип подразделения, так как для текущего и выбранного типа подразделения не совпадает " +
                                                       "настройка «Типы подразделений, разрешенные в качестве выпускающих, формирующих, территориальных (для совместимости)».");
            }
        }
    }
}
