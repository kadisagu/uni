/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSettings.ui.AccreditationList;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniSettings.UniSettingsManager;
import ru.tandemservice.uni.base.bo.UniSettings.logic.AccreditationSearchListDSHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 17.11.2016
 */
@Configuration
public class UniSettingsAccreditationList extends BusinessComponentManager
{
    public static final Long PROGRAM_SUBJECT_ID = 0L;
    public static final Long PROGRAM_SUBJECT_GROUPS_ID = 1L;

    public static final Long ACCREDITATION_YES_ID = 0L;
    public static final Long ACCREDITATION_NO_ID = 1L;

    public static final String PROGRAM_KIND = "eduProgramKind";
    public static final String PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
    public static final String INSTITUTION_ORG_UNIT = "institutionOrgUnit";

    public static final String ACCREDITATIONS_DS = "accreditationDS";
    public static final String TYPE_WRAPPER_DS = "typeDS";
    public static final String ACCREDITATIONS_SEARCH_LIST_DS = "accreditationSearchListDS";
    public static final String EDU_PROGRAM_KIND_DS = "eduProgramKindDS";
    public static final String PROGRAM_SUBJECT_INDEX_DS = "eduProgramSubjectIndexDS";
    public static final String ACCREDITATION_STATE_DS = "accreditationStateDS";
    public static final String INSTITUTION_ORG_UNIT_DS = "institutionDS";




    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ACCREDITATIONS_SEARCH_LIST_DS, accreditationColumnsDS(), UniSettingsManager.instance().accreditationSearchListDSHandler()))
                .addDataSource(selectDS(TYPE_WRAPPER_DS, typesDSHandler()))
                .addDataSource(selectDS(ACCREDITATIONS_DS, accreditationDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_KIND_DS, eduProgramKindDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_INDEX_DS, programSubjectIndexDSHandler()))
                .addDataSource(selectDS(ACCREDITATION_STATE_DS, accreditationStateDSHandler()))
                .addDataSource(selectDS(INSTITUTION_ORG_UNIT_DS, institutionOrgUnitDS()))
                .create();
    }

    @Bean
    public ColumnListExtPoint accreditationColumnsDS()
    {
        return columnListExtPointBuilder(ACCREDITATIONS_SEARCH_LIST_DS)
                .addColumn(checkboxColumn("checked").disableHandler(e->((DataWrapper)e).getWrapped() instanceof IPersistentAccreditationOwner))
                .addColumn(textColumn("title", AccreditationSearchListDSHandler.TITLE_FIELD).treeable(true).headerAlign("center")
                                   .styleResolver(rowEntity -> ((DataWrapper)rowEntity).getWrapped() instanceof IPersistentAccreditationOwner? "font-weight:bold;" : ""))
                .addColumn(toggleColumn("license", AccreditationSearchListDSHandler.LICENSE).align("center").headerAlign("center").width("150px")
                                   .toggleOnListener("onToggleLicenseOnOff").toggleOnLabel("license.on")
                                   .toggleOffListener("onToggleLicenseOnOff").toggleOffLabel("license.off").permissionKey("accreditationSettings_editLicense"))
                .addColumn(blockColumn("active", "valueBlock").align("center").headerAlign("center").width("150px"))
                .addColumn(blockColumn("action", "actionBlock").width("1px").hasBlockHeader(true))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler typesDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        new IdentifiableWrapper(PROGRAM_SUBJECT_ID, "только направления"),
                        new IdentifiableWrapper(PROGRAM_SUBJECT_GROUPS_ID, "только УГНС")
                ))
                .filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler accreditationDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        new IdentifiableWrapper(ACCREDITATION_YES_ID, "есть"),
                        new IdentifiableWrapper(ACCREDITATION_NO_ID, "нет")
                ))
                .filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramKindDSHandler()
    {
        return EduProgramKind.defaultSelectDSHandler(getName());
    }


    @Bean
    public IDefaultComboDataSourceHandler programSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .customize((alias, dql, context, filter) ->
                           dql.where(eq(property(alias, EduProgramSubjectIndex.programKind()), value((EduProgramKind)context.get(PROGRAM_KIND)))));
    }

    @Bean
    public IDefaultComboDataSourceHandler accreditationStateDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Lists.newArrayList(StateAccreditationEnum.values()));
    }

    @Bean
    public IDefaultComboDataSourceHandler institutionOrgUnitDS()
    {
        return EduInstitutionOrgUnit.defaultSelectDSHandler(getName());
    }

}