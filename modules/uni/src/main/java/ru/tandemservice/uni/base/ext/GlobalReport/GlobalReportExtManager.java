/* $Id:$ */
package ru.tandemservice.uni.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.uni.base.bo.StudentPassportExpiredReport.ui.Add.StudentPassportExpiredReportAdd;
import ru.tandemservice.uni.base.ext.GlobalReport.ui.List.UniGlobalReportListAddon;

/**
 * @author oleyba
 * @since 12/11/13
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("studentSampleReport", new GlobalReportDefinition("student", "studentSample", ReportPersonAdd.class.getSimpleName(), "studentSampleParamMap", UniGlobalReportListAddon.NAME))
                .add("studentsCoursesSpecialitiesAllocationReport", new GlobalReportDefinition("student", "studentsCoursesSpecialitiesAllocation", ru.tandemservice.uni.component.reports.studentsCoursesSpecialitiesAllocationReport.List.Model.class.getPackage().getName()))
                .add("studentsAgeSexDistributionReport", new GlobalReportDefinition("student", "studentsAgeSexDistribution", ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.List.Model.class.getPackage().getName()))
                .add("studentSummaryReport", new GlobalReportDefinition("student", "studentSummary", ru.tandemservice.uni.component.reports.studentSummary.List.Model.class.getPackage().getName()))
                .add("studentSummaryNewReport", new GlobalReportDefinition("student", "studentSummaryNew", ru.tandemservice.uni.component.reports.studentSummaryNew.List.Model.class.getPackage().getName()))
                .add("studentPassportExpiredReport", new GlobalReportDefinition("student", "studentPassportExpired", StudentPassportExpiredReportAdd.class.getSimpleName()))
                .create();
    }
}