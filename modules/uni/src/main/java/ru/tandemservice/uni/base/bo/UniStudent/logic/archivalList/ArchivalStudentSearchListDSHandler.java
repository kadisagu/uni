/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniStudent.logic.archivalList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.ArchivalList.UniStudentArchivalListUI;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
public class ArchivalStudentSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public ArchivalStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        // номер в архиве
        String personalArchivalFileNumber = context.get(UniStudentArchivalListUI.PARAM_PERSONAL_ARCHIVAL_FILE_NUMBER);
        FilterUtils.applySimpleLikeFilter(builder, alias, Student.personalArchivalFileNumber(), personalArchivalFileNumber);
    }

    @Override
    public IEducationOrgUnitContextHandler getContextHandler(ExecutionContext context)
    {
        return new BaseStudentListContextHandler(true);
    }
}
