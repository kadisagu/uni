/* $Id$ */
package ru.tandemservice.uni.component.catalog.studentCustomStateCI.StudentCustomStateCIItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;

/**
 * @author nvankov
 * @since 3/27/13
 */
public class Model extends DefaultCatalogItemPubModel<StudentCustomStateCI>
{
}
