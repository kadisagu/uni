/* $Id$ */
package ru.tandemservice.uni.events.dset;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;

import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * Обновляет у EducationOrgUnit поле shortTitle
 * Запускается только при условии, что изменилось свойство shortTitle у объекта EducationOrgUnit.educationLevelHighSchool
 * <p/>
 * Изменения применяются в конце транзакции
 *
 * @author Vasily Zhukov
 * @since 02.06.2011
 */
public class EduOrgUnitShortTitleSync extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationLevelsHighSchool.class, this);
    }


    @Override
    public void onEvent(final DSetEvent event)
    {
        if (event.getMultitude().getAffectedProperties().contains(EducationLevelsHighSchool.P_SHORT_TITLE)) {
            super.onEvent(event);
        }
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        BatchUtils.execute(params, 128, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> ids) {
                new DQLUpdateBuilder(EducationOrgUnit.class)
                .fromEntity(EducationLevelsHighSchool.class, "eduHs").where(eq(property("eduHs"), property(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL)))
                .set(EducationOrgUnit.P_SHORT_TITLE, property("eduHs", EducationLevelsHighSchool.P_SHORT_TITLE))
                .where(ne(property(EducationOrgUnit.P_SHORT_TITLE), property("eduHs", EducationLevelsHighSchool.P_SHORT_TITLE)))
                .where(in(property("eduHs.id"), ids))
                .createStatement(session).execute();
            }
        });
        return Boolean.TRUE;
    }
}
