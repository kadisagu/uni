/** *$Id: OrgSystemActionPubAddon.java 4443 2014-05-21 08:10:28Z nvankov $*/
package ru.tandemservice.uni.base.ext.DataCorrection.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.uni.base.bo.UniReorganization.ui.EduOrgUnitCorrection.UniReorganizationEduOrgUnitCorrection;

/**
 * @author rsizonenko
 * @since 30/03/16
 */
public class OrgDataCorrectionPubAddon extends UIAddon
{
    public OrgDataCorrectionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickShowEducationOrgUnitCorrection()
    {
        getActivationBuilder().asCurrent(UniReorganizationEduOrgUnitCorrection.class).activate();
    }
}
