package ru.tandemservice.uni.dao.grid;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.mvel2.MVEL;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.shared.commonbase.utils.SharedDQLUtils;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.catalog.gen.DevelopGridGen;
import ru.tandemservice.uni.entity.catalog.gen.YearDistributionGen;
import ru.tandemservice.uni.entity.catalog.gen.YearDistributionPartGen;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.gen.DevelopGridTermGen;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DevelopGridDAO extends UniBaseDao implements IDevelopGridDAO {

    private final DaoCacheFacade.CacheEntryDefinition<Long, Map<Integer, DevelopGridTerm>> CACHE_DISTRIB_MAP =
            new DaoCacheFacade.CacheEntryDefinition<Long, Map<Integer, DevelopGridTerm>>(DevelopGridDAO.class.getSimpleName()+".distributionMap", 64) {
                @Override public void fill(Map<Long, Map<Integer, DevelopGridTerm>> cache, Collection<Long> ids) {
                    cache.putAll(DevelopGridDAO.this.getDevelopGridDistributionMapInternal(ids));
                }
            };

    @Override
    public Map<Long, Map<Integer, DevelopGridTerm>> getDevelopGridDistributionMap(final Collection<Long> gridIds) {
        if (null == gridIds) {
            return getDevelopGridDistributionMap(getPropertiesList(DevelopGrid.class, false, DevelopGrid.id()));
        }
        return DaoCacheFacade.getEntry(CACHE_DISTRIB_MAP).getRecords(gridIds);
    }

    @Override
    public Map<Integer, DevelopGridTerm> getDevelopGridMap(Long gridId) {
        return getDevelopGridDistributionMap(ImmutableList.of(gridId)).get(gridId);
    }

    @Override
    public List<DevelopGridTerm> getDevelopGridTermList(DevelopGrid grid)
    {
        return new ArrayList<>(getDevelopGridMap(grid.getId()).values());
    }

    protected Map<Long, Map<Integer, DevelopGridTerm>> getDevelopGridDistributionMapInternal(final Collection<Long> gridIds) {
        final Map<Long, Map<Integer, DevelopGridTerm>> result = new HashMap<>(gridIds != null ? gridIds.size() : 16);
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "gt");
        dql.column(property("gt"));
        dql.fetchPath(DQLJoinType.inner, DevelopGridTerm.term().fromAlias("gt"), "t");
        dql.fetchPath(DQLJoinType.inner, DevelopGridTerm.course().fromAlias("gt"), "c");
        if (null != gridIds) {
            dql.where(in(property("gt", DevelopGridTerm.developGrid().id()), gridIds));
        }
        for (final DevelopGridTerm gridTerm : dql.createStatement(getSession()).<DevelopGridTerm>list()) {
            SafeMap.safeGet(result, gridTerm.getDevelopGrid().getId(), key -> new TreeMap<>()).put(gridTerm.getTerm().getIntValue(), gridTerm);
        }
        return result;
    }


    protected void validate(final Map<Long, Map<Integer, DevelopGridTerm>> educationGridDistributionMap) {
        for (final Map.Entry<Long, Map<Integer, DevelopGridTerm>> gridEntry: educationGridDistributionMap.entrySet()) {
            for (final Map.Entry<Integer, DevelopGridTerm> termEntry: gridEntry.getValue().entrySet()) {
                final DevelopGridTerm gridTerm = termEntry.getValue();

                final int term = gridTerm.getTerm().getIntValue();
                if (!termEntry.getKey().equals(term)) { throw new IllegalArgumentException("DevelopGridTerm("+gridTerm.getId()+").term("+term+") != "+termEntry.getKey()); }

                final Long grid = gridTerm.getDevelopGrid().getId();
                if (!gridEntry.getKey().equals(grid)) { throw new IllegalArgumentException("DevelopGridTerm("+gridTerm.getId()+").grid("+grid+") != "+gridEntry.getKey()); }
            }
        }
    }

    @Override
    public void updateDevelopGrids(final Map<Long, Map<Integer, DevelopGridTerm>> newMap) {
        this.validate(newMap);
        final Session session = lock(DevelopGrid.class.getSimpleName());

        final Map<Long, Map<Integer, DevelopGridTerm>> currentMap = this.getDevelopGridDistributionMapInternal(newMap.keySet());

        // обновляем существующие
        for (final Map.Entry<Long, Map<Integer, DevelopGridTerm>> currentGridEntry: currentMap.entrySet()) {
            final Map<Integer, DevelopGridTerm> currentTermMap = currentGridEntry.getValue();
            final Map<Integer, DevelopGridTerm> newTermMap = newMap.remove(currentGridEntry.getKey());

            for (final Map.Entry<Integer, DevelopGridTerm> newTermEntry: newTermMap.entrySet()) {
                DevelopGridTerm gridTerm = currentTermMap.remove(newTermEntry.getKey());
                if (null == gridTerm) { gridTerm = new DevelopGridTerm(); }
                gridTerm.update(newTermEntry.getValue());
                session.saveOrUpdate(gridTerm);
            }

            for (final Map.Entry<Integer, DevelopGridTerm> currentTermEntry: currentTermMap.entrySet()) {
                session.delete(currentTermEntry.getValue());
            }
        }

        // создаем новые
        for (final Map.Entry<Long, Map<Integer, DevelopGridTerm>> newGridEntry: newMap.entrySet()) {
            final Map<Integer, DevelopGridTerm> newTermMap = newGridEntry.getValue();
            for (final Map.Entry<Integer, DevelopGridTerm> newTermEntry: newTermMap.entrySet()) {
                final DevelopGridTerm gridTerm = new DevelopGridTerm();
                gridTerm.update(newTermEntry.getValue());
                session.saveOrUpdate(gridTerm);
            }
        }
        session.flush();
    }

    public static <T> List<Map.Entry<Integer, T>> getSortedIntMapEntryList(final Map<Integer, T> term2valueMap) {
        return term2valueMap.entrySet().stream()
                .sorted((o1, o2) -> Integer.compare(o1.getKey(), o2.getKey()))
                .collect(Collectors.toList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void doGenerateGridsFromDescription() {
        final Session session = lock(DevelopGrid.class.getSimpleName());

        final Map<Integer, Course> courseMap = getCourseMap();
        final Map<Integer, Term> termMap = getTermMap();

        final MQBuilder builder = new MQBuilder(DevelopGridGen.ENTITY_CLASS, "g");
        builder.add(MQExpression.notIn("g", "id", new MQBuilder(DevelopGridTermGen.ENTITY_CLASS, "t", new String[] {DevelopGridTermGen.developGrid().id().s()})));
        final List<DevelopGrid> gridList = builder.getResultList(session);
        final Map<Long, Map<Integer, DevelopGridTerm>> gridMap = this.getDevelopGridDistributionMapInternal(UniBaseDao.ids(gridList));

        for(final DevelopGrid grid: gridList) {
            if (StringUtils.isEmpty(grid.getMeta())) { continue; }

            final Map<Integer, DevelopGridTerm> gridTermMap = SafeMap.safeGet(gridMap, grid.getId(), (key) -> new HashMap<>());
            if (gridTermMap.size() > 0) { continue; }

            final Map<Integer, Integer> term2courseMap = (Map) MVEL.eval(grid.getMeta(), new HashMap(), Map.class);
            if (term2courseMap.isEmpty()) { continue; }

            final Map<Integer, MutableInt> course2termAmountMap = SafeMap.get(MutableInt.class);
            for (final Integer course: term2courseMap.values()) { course2termAmountMap.get(course).increment(); }
            final int distribution = Collections.max(course2termAmountMap.values()).intValue();
            final YearDistributionPart[] parts = (YearDistributionPart[]) this.getSession()
            .createCriteria(YearDistributionPart.class)
            .addOrder(Order.asc(YearDistributionPartGen.number().toString()))
            .createCriteria(YearDistributionPartGen.yearDistribution().toString())
            .add(Restrictions.eq(YearDistributionGen.amount().toString(), distribution))
            .list().toArray(new YearDistributionPart[distribution]);

            final List<Map.Entry<Integer, Integer>> entries = DevelopGridDAO.getSortedIntMapEntryList(term2courseMap);
            for (final Map.Entry<Integer, Integer> e: entries) {
                final Integer termNumber = e.getKey();
                final Integer termCourse = e.getValue();

                final DevelopGridTerm value = new DevelopGridTerm();
                value.setDevelopGrid(grid);
                value.setCourse(courseMap.get(termCourse));
                value.setTerm(termMap.get(termNumber));
                value.setPart(parts[(value.getTerm().getIntValue()-1) % parts.length]);
                gridTermMap.put(termNumber, value);
            }
        }

        this.updateDevelopGrids(gridMap);
    }

    @Override
    public Map<Course, Integer[]> getDevelopGridDetail(final DevelopGrid developGrid)
    {
        final Map<Course, Integer[]> map = new TreeMap<>(Course.COURSE_COMPARATOR);

        for (final DevelopGridTerm term : getDevelopGridMap(developGrid.getId()).values())
        {
            Integer[] value = map.get(term.getCourse());
            if (value == null) {
                map.put(term.getCourse(), value = new Integer[term.getPartAmount()]);
            }
            if (value.length != term.getPartAmount()) {
                throw new RuntimeException("Invalid DevelopGrid (PartAmount): " + developGrid.getId());
            }
            value[term.getPartNumber() - 1] = term.getTermNumber();
        }
        return map;
    }

    @Override
    public Set<Course> getDevelopGridCourseSet(DevelopGrid developGrid)
    {
        return getDevelopGridMap(developGrid.getId()).values().stream()
                .map(DevelopGridTermGen::getCourse)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private static final String CACHE_KEY_COURSE_MAP = DevelopGridDAO.class.getSimpleName() + ".course-map";
    public final static HibernateCallback<Map<Integer, Course>> COURSE_MAP_CALLBACK = session -> {
        Map<Integer, Course> courseMap = DaoCache.get(CACHE_KEY_COURSE_MAP);
        if (null == courseMap) {
            final ImmutableMap.Builder<Integer, Course> builder = ImmutableMap.builder();
            SharedDQLUtils.builder(Course.class, "t", property("t"))
                    .createStatement(session).<Course>list().stream()
                    .sorted((c1, c2) -> Integer.compare(c1.getIntValue(), c2.getIntValue()))
                    .forEachOrdered(course -> builder.put(course.getIntValue(), course));

            DaoCache.put(CACHE_KEY_COURSE_MAP, courseMap = builder.build());
        }
        return courseMap;
    };

    public static List<Course> getCourseList() {
        return new ArrayList<>(getCourseMap().values());
    }

    public static Map<Integer, Course> getCourseMap() {
        return UniDaoFacade.getCoreDao().getCalculatedValue(COURSE_MAP_CALLBACK);
    }

    @Override
    public int getDevelopGridSize(DevelopGrid developGrid)
    {
        return new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "dgt")
                .column(DQLFunctions.max(property("dgt", DevelopGridTerm.term().intValue())))
                .where(eq(property("dgt", DevelopGridTerm.developGrid()), value(developGrid)))
                .createStatement(getSession()).uniqueResult();
    }

    private static final String CACHE_KEY_TERM_MAP = DevelopGridDAO.class.getSimpleName() + ".term-map";
    public final static HibernateCallback<Map<Integer, Term>> TERM_MAP_CALLBACK = session -> {
        Map<Integer, Term> termMap = DaoCache.get(CACHE_KEY_TERM_MAP);
        if (null == termMap) {
            final ImmutableMap.Builder<Integer, Term> builder = ImmutableMap.builder();
            SharedDQLUtils.builder(Term.class, "t", property("t"))
                    .createStatement(session).<Term>list().stream()
                    .sorted((t1, t2) -> t1.getIntValue() - t2.getIntValue())
                    .forEachOrdered(term -> builder.put(term.getIntValue(), term));

            DaoCache.put(CACHE_KEY_TERM_MAP, termMap = builder.build());
        }
        return termMap;
    };

    // семестры по int-значениям
    public static Map<Integer, Term> getTermMap() {
        return UniDaoFacade.getCoreDao().getCalculatedValue(TERM_MAP_CALLBACK);
    }

    private static final String CACHE_KEY_ID_2_TERM_NUMBER_MAP = DevelopGridDAO.class.getSimpleName() + ".id-2-term-number-map";
    // id семестра -> номер семестра
    public static Map<Long, Integer> getIdToTermNumberMap() {
        Map<Long, Integer> idMap = DaoCache.get(CACHE_KEY_ID_2_TERM_NUMBER_MAP);
        if (null == idMap) {
            final ImmutableMap.Builder<Long, Integer> builder = ImmutableMap.builder();
            getTermMap().forEach((k, v) -> builder.put(v.getId(), k));
            DaoCache.put(CACHE_KEY_ID_2_TERM_NUMBER_MAP, idMap = builder.build());
        }
        return idMap;
    }

}
