/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummaryNew.Add;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.reports.studentSummaryNew.ReportParams;
import ru.tandemservice.uni.entity.report.StudentSummaryNewReport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author dseleznev
 * @since 14.03.2012
 */
@Input(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private StudentSummaryNewReport _report = new StudentSummaryNewReport();
    private ReportParams _params = new ReportParams();

    private ISelectModel _developFormListModel;
    private IMultiSelectModel _formativeOrgUnitListModel;
    private IMultiSelectModel _territorialOrgUnitListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developPeriodListModel;
    private IMultiSelectModel _compensationTypeListModel;
    private IMultiSelectModel _qualificationListModel;
    private IMultiSelectModel _studentStatusListModel;
    private IMultiSelectModel _studentCategoryModel;
    private List<DataWrapper> _yesNoList = TwinComboDataSourceHandler.getDefaultOptionList();

    private IDataSettings settings;

    private boolean statusEditingPermitted;

    public String getSettingsOwner()
    {
        return "system"; //getOrgUnitId() == null ? "system" : String.valueOf(getOrgUnitId());
    }

    private String getSettingsKeyPrefix()
    {
        return "report.studentSummaryNew.";
    }

    public Object getSetting(String key)
    {
        return getSettings().get(getSettingsKeyPrefix() + key);
    }

    public void setSetting(String key, Object value)
    {
        getSettings().set(getSettingsKeyPrefix() + key, value);
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public IMultiSelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(IMultiSelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public IMultiSelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(IMultiSelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public StudentSummaryNewReport getReport()
    {
        return _report;
    }

    public void setReport(StudentSummaryNewReport report)
    {
        _report = report;
    }

    public ReportParams getParams()
    {
        return _params;
    }

    public void setParams(ReportParams params)
    {
        _params = params;
    }

    public ISelectModel getDevelopConditionListModel() { return _developConditionListModel; }
    public void setDevelopConditionListModel(ISelectModel developConditionListModel) { _developConditionListModel = developConditionListModel; }

    public IMultiSelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(IMultiSelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public IMultiSelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(IMultiSelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public IMultiSelectModel getStudentStatusListModel()
    {
        return _studentStatusListModel;
    }

    public void setStudentStatusListModel(IMultiSelectModel studentStatusListModel)
    {
        _studentStatusListModel = studentStatusListModel;
    }

    public IMultiSelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(IMultiSelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public List<DataWrapper> getYesNoList()
    {
        return _yesNoList;
    }

    public void setYesNoList(List<DataWrapper> yesNoList)
    {
        _yesNoList = yesNoList;
    }

    public boolean isStatusEditingPermitted()
    {
        return statusEditingPermitted;
    }

    public void setStatusEditingPermitted(boolean statusEditingPermitted)
    {
        this.statusEditingPermitted = statusEditingPermitted;
    }

    public IDataSettings getSettings()
    {
        return settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this.settings = settings;
    }

    public ISelectModel getDevelopTechListModel() { return _developTechListModel; }
    public void setDevelopTechListModel(ISelectModel developTechListModel) { _developTechListModel = developTechListModel; }

    public ISelectModel getDevelopPeriodListModel() { return _developPeriodListModel; }
    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel) { _developPeriodListModel = developPeriodListModel; }

    public List<Validator> getValidators(Boolean use)
    {
        if (use)
            return Collections.singletonList((Validator) new Required());
        return new ArrayList<Validator>();
    }
}