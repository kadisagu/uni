/**
 *$Id$
 */
package ru.tandemservice.uni.base.ext.ReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAdd;

/**
 * @author Alexander Zhebko
 * @since 26.03.2014
 */
@Configuration
public class ReportPersonAddExt extends BusinessComponentExtensionManager
{
    // first level tabs
    public static final String STUDENT_TAB = "studentTab";

    @Autowired
    private ReportPersonAdd _reportPersonAdd;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_reportPersonAdd.tabPanelExtPoint())
                .addTab(componentTab(STUDENT_TAB, StudentReportPersonAdd.class).visible("mvel:presenter.isTabVisible('" + STUDENT_TAB + "')"))
                .create();
    }
}