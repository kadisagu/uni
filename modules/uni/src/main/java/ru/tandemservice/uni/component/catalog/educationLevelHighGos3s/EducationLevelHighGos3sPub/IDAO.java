/* $Id: IDAO.java 18135 2011-05-26 04:14:54Z vdanilov $ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3s.EducationLevelHighGos3sPub;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3s;

/**
 * @author oleyba
 * @since 5/11/11
 */
public interface IDAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.IDAO<EducationLevelHighGos3s, Model>
{
}
