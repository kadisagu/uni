package ru.tandemservice.uni.entity.education.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ФУТС (возможные варианты, по умолчанию)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DevelopCombinationGen extends EntityBase
 implements INaturalIdentifiable<DevelopCombinationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.education.DevelopCombination";
    public static final String ENTITY_NAME = "developCombination";
    public static final int VERSION_HASH = -1002198256;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String P_TITLE = "title";

    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopGrid _developGrid;     // Учебная сетка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Учебная сетка. Свойство не может быть null.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DevelopCombinationGen)
        {
            if (withNaturalIdProperties)
            {
                setDevelopForm(((DevelopCombination)another).getDevelopForm());
                setDevelopCondition(((DevelopCombination)another).getDevelopCondition());
                setDevelopTech(((DevelopCombination)another).getDevelopTech());
                setDevelopGrid(((DevelopCombination)another).getDevelopGrid());
            }
        }
    }

    public INaturalId<DevelopCombinationGen> getNaturalId()
    {
        return new NaturalId(getDevelopForm(), getDevelopCondition(), getDevelopTech(), getDevelopGrid());
    }

    public static class NaturalId extends NaturalIdBase<DevelopCombinationGen>
    {
        private static final String PROXY_NAME = "DevelopCombinationNaturalProxy";

        private Long _developForm;
        private Long _developCondition;
        private Long _developTech;
        private Long _developGrid;

        public NaturalId()
        {}

        public NaturalId(DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopGrid developGrid)
        {
            _developForm = ((IEntity) developForm).getId();
            _developCondition = ((IEntity) developCondition).getId();
            _developTech = ((IEntity) developTech).getId();
            _developGrid = ((IEntity) developGrid).getId();
        }

        public Long getDevelopForm()
        {
            return _developForm;
        }

        public void setDevelopForm(Long developForm)
        {
            _developForm = developForm;
        }

        public Long getDevelopCondition()
        {
            return _developCondition;
        }

        public void setDevelopCondition(Long developCondition)
        {
            _developCondition = developCondition;
        }

        public Long getDevelopTech()
        {
            return _developTech;
        }

        public void setDevelopTech(Long developTech)
        {
            _developTech = developTech;
        }

        public Long getDevelopGrid()
        {
            return _developGrid;
        }

        public void setDevelopGrid(Long developGrid)
        {
            _developGrid = developGrid;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DevelopCombinationGen.NaturalId) ) return false;

            DevelopCombinationGen.NaturalId that = (NaturalId) o;

            if( !equals(getDevelopForm(), that.getDevelopForm()) ) return false;
            if( !equals(getDevelopCondition(), that.getDevelopCondition()) ) return false;
            if( !equals(getDevelopTech(), that.getDevelopTech()) ) return false;
            if( !equals(getDevelopGrid(), that.getDevelopGrid()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDevelopForm());
            result = hashCode(result, getDevelopCondition());
            result = hashCode(result, getDevelopTech());
            result = hashCode(result, getDevelopGrid());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDevelopForm());
            sb.append("/");
            sb.append(getDevelopCondition());
            sb.append("/");
            sb.append(getDevelopTech());
            sb.append("/");
            sb.append(getDevelopGrid());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DevelopCombinationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DevelopCombination.class;
        }

        public T newInstance()
        {
            return (T) new DevelopCombination();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developGrid":
                    return obj.getDevelopGrid();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developGrid":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developGrid":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developTech":
                    return DevelopTech.class;
                case "developGrid":
                    return DevelopGrid.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DevelopCombination> _dslPath = new Path<DevelopCombination>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DevelopCombination");
    }
            

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends DevelopCombination> extends EntityPath<E>
    {
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopCombination#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(DevelopCombinationGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return DevelopCombination.class;
        }

        public String getEntityName()
        {
            return "developCombination";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
