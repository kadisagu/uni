/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author vip_delete
 */
public class UniDao<Model> extends UniBaseDao implements IUniDao<Model>
{
    public static final int MAX_ROWS = 50;

    @Override
    public void prepare(Model model)
    {
        //do not call super
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        //do not call super
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        //do not call super
    }

    @Override
    public void update(Model model)
    {
        //do not call super
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        delete((Long) component.getListenerParameter());
    }

    @Override
    public final boolean isUnique(Class<? extends IEntity> clazz, Long id, String property, Object value)
    {
        Criteria criteria = getSession().createCriteria(clazz);
        criteria.add(Restrictions.eq(property, value));
        if (id != null)
        {
            criteria.add(Restrictions.ne(IEntity.P_ID, id));
        }
        criteria.setProjection(Projections.rowCount());
        return (((Number) criteria.uniqueResult()).intValue() == 0);
    }
}
