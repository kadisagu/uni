/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.StatusEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Alexey Lopatin
 * @since 15.04.2015
 */
@Input({@Bind(key = "studentId", binding = "student.id", required = true)})
public class UniStudentStatusEditUI extends UIPresenter
{
    private Student _student = new Student();

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(Student.class, _student.getId());
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_student);
        deactivate();
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}
