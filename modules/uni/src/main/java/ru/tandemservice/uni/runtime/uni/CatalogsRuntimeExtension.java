/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.runtime.uni;

import org.tandemframework.common.catalog.runtime.CatalogGroupRuntime;
import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.uni.util.FunctionalityManager;

/**
 * @author agolubenko
 * @since Jun 15, 2010
 */
public class CatalogsRuntimeExtension implements IRuntimeExtension
{
    @Override
    public void init(Object object)
    {
        CatalogGroupRuntime.getGroup("militaryReg").setHidden(!FunctionalityManager.isPersonMilitaryEnabled());
    }

    @Override
    public void destroy()
    {

    }
}
