/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsPrintPersonData;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.sec.entity.RoleAssignmentCache;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 * @since 09.11.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        model.setPersonRoleTypeList(Arrays.asList(
                new IdentifiableWrapper(Model.STUDENT_TYPE, "Студент"),
                new IdentifiableWrapper(Model.EMPLOYEE_TYPE, "Сотрудник")
        ));

        // Студент

        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelHighSchoolListModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                Criteria c = getSession().createCriteria(EducationOrgUnit.class, "e");
                c.createAlias("e." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "h");
                c.createAlias("h." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "lev");

                if ((model.getQualificationList() != null) && !model.getQualificationList().isEmpty())
                {
                    c.add(Restrictions.in("lev." + EducationLevels.L_QUALIFICATION, model.getQualificationList()));
                }
                if ((model.getFormativeOrgUnitList() != null) && !model.getFormativeOrgUnitList().isEmpty())
                {
                    c.add(Restrictions.in("e." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
                }
                if ((model.getTerritorialOrgUnitList() != null) && !model.getTerritorialOrgUnitList().isEmpty())
                {
                    c.add(Restrictions.in("e." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
                }

                c.setProjection(Projections.distinct(Projections.property("h." + EducationLevelsHighSchool.P_ID)));

                final List<Long> ids = c.list();

                if (ids.isEmpty())
                {
                    return ListResult.getEmpty();
                }

                if (filter != null)
                {
                    filter = filter.trim();
                }

                c = getSession().createCriteria(EducationLevelsHighSchool.class);
                c.add(Restrictions.in(EducationLevelsHighSchool.P_ID, ids));

                if (StringUtils.isNotEmpty(filter))
                {
                    c.add(Restrictions.ilike(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "%" + filter + "%"));
                }

                c.addOrder(Order.asc(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE));

                return new ListResult<>(c.list());
            }
        });
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setGroupListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                final Criteria c = getSession().createCriteria(Group.class, "g");
                c.createAlias("g." + Group.L_EDUCATION_ORG_UNIT, "e");
                c.createAlias("e." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "h");
                c.createAlias("h." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "lev");

                c.add(Restrictions.eq("g." + Group.P_ARCHIVAL, Boolean.FALSE));

                if ((model.getQualificationList() != null) && !model.getQualificationList().isEmpty())
                {
                    c.add(Restrictions.in("lev." + EducationLevels.L_QUALIFICATION, model.getQualificationList()));
                }
                if ((model.getFormativeOrgUnitList() != null) && !model.getFormativeOrgUnitList().isEmpty())
                {
                    c.add(Restrictions.in("e." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
                }
                if ((model.getTerritorialOrgUnitList() != null) && !model.getTerritorialOrgUnitList().isEmpty())
                {
                    c.add(Restrictions.in("e." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
                }
                if ((model.getEducationLevelHighSchoolList() != null) && !model.getEducationLevelHighSchoolList().isEmpty())
                {
                    c.add(Restrictions.in("e." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelHighSchoolList()));
                }
                if ((model.getDevelopFormList() != null) && !model.getDevelopFormList().isEmpty())
                {
                    c.add(Restrictions.in("e." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopFormList()));
                }
                if ((model.getCourseList() != null) && !model.getCourseList().isEmpty())
                {
                    c.add(Restrictions.in("g." + Group.L_COURSE, model.getCourseList()));
                }

                if (filter != null)
                {
                    filter = filter.trim();
                }

                if (StringUtils.isNotEmpty(filter))
                {
                    c.add(Restrictions.ilike("g." + Group.P_TITLE, "%" + filter + "%"));
                }

                c.addOrder(Order.asc("g." + Group.P_TITLE));

                return new ListResult<>(c.list());
            }
        });

        // Сотрудник

        model.setOrgUnitTypeListModel(new LazySimpleSelectModel<>(OrgUnitManager.instance().dao().getOrgUnitActiveTypeList()));
        model.setOrgUnitListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                final Criteria c = getSession().createCriteria(OrgUnit.class);
                c.add(Restrictions.eq(OrgUnit.P_ARCHIVAL, Boolean.FALSE));

                if ((model.getOrgUnitTypeList() != null) && !model.getOrgUnitTypeList().isEmpty())
                {
                    c.add(Restrictions.in(OrgUnit.L_ORG_UNIT_TYPE, model.getOrgUnitTypeList()));
                }

                if (filter != null)
                {
                    filter = filter.trim();
                }

                if (StringUtils.isNotEmpty(filter))
                {
                    c.add(Restrictions.ilike(OrgUnit.P_TITLE, "%" + filter + "%"));
                }

                return new ListResult<>(c.list());
            }
        });
    }

    @Override
    public Integer printPersonData(final Model model) throws Exception
    {
        if (model.getPersonRoleType().getId() == Model.STUDENT_TYPE)
        {
            final MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
            builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));

            if ((model.getQualificationList() != null) && !model.getQualificationList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));
            }

            if ((model.getFormativeOrgUnitList() != null) && !model.getFormativeOrgUnitList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.educationOrgUnit().formativeOrgUnit().s(), model.getFormativeOrgUnitList()));
            }

            if ((model.getTerritorialOrgUnitList() != null) && !model.getTerritorialOrgUnitList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.educationOrgUnit().territorialOrgUnit().s(), model.getTerritorialOrgUnitList()));
            }

            if ((model.getEducationLevelHighSchoolList() != null) && !model.getEducationLevelHighSchoolList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.educationOrgUnit().educationLevelHighSchool().s(), model.getEducationLevelHighSchoolList()));
            }

            if ((model.getDevelopFormList() != null) && !model.getDevelopFormList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.educationOrgUnit().developForm().s(), model.getDevelopFormList()));
            }

            if ((model.getCourseList() != null) && !model.getCourseList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.course().s(), model.getCourseList()));
            }

            if ((model.getGroupList() != null) && !model.getGroupList().isEmpty())
            {
                builder.add(MQExpression.in("s", Student.group().s(), model.getGroupList()));
            }

            builder.addJoin("s", Student.L_PERSON, "person");
            builder.addLeftJoin("person", Person.L_ADDRESS, "address");
            builder.addLeftJoin("person", Person.L_CONTACT_DATA, "contactData");

            builder.addOrder("s", Student.person().identityCard().lastName().s());
            builder.addOrder("s", Student.person().identityCard().firstName().s());
            builder.addOrder("s", Student.person().identityCard().middleName().s());

            builder.getSelectAliasList().clear();
            builder.addSelect("s", new Object[]{
                    Student.person().identityCard().lastName().s(),
                    Student.person().identityCard().firstName().s(),
                    Student.person().identityCard().middleName().s(),
                    Student.group().title().s(),
                    Student.educationOrgUnit().formativeOrgUnit().title().s(),
                    Student.principal().login().s()
            });
            builder.addSelect("contactData", new Object[]{
                    PersonContactData.email().s(),
                    PersonContactData.phoneMobile().s()
            });

            final List<Object[]> data = builder.getResultList(getSession());

            final ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
            final OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");

            int j = 1;
            for (int i = 0; i < data.size(); i++)
            {
                final Object[] row = data.get(i);
                final String lastName = (String) row[0];
                final String firstName = (String) row[1];
                final String middleName = (String) row[2];
                final String group = (String) row[3];
                final String orgUnit = (String) row[4];
                final String login = (String) row[5];
                final String email = (String) row[6];
                final String phone = (String) row[7];

                // Нумерация
                writer.write(Integer.toString(j++));
                writer.write(",");
                // Фамилия
                writer.write("\"" + lastName + "\"");
                writer.write(",");
                // Имя и Отчество
                if (StringUtils.isEmpty(middleName))
                {
                    writer.write("\"" + firstName + "\"");
                }
                else
                {
                    writer.write("\"" + firstName + " " + middleName + "\"");
                }
                writer.write(",");
                // Логин
                writer.write("\"" + login + "\"");
                writer.write(",");
                // Пустая колонка зарезервированная под пароль
                writer.write(",");
                // Название группы
                writer.write("\"" + group + "\"");
                writer.write(",");
                // Название формирующего подразделения
                writer.write("\"" + orgUnit + "\"");
                writer.write(",");
                // e-mail
                if (email != null)
                {
                    writer.write("\"" + email + "\"");
                }
                writer.write(",");
                // мобильный телефон
                if (phone != null)
                {
                    writer.write("\"" + phone + "\"");
                }
                writer.write('\n');
            }

            writer.close();

            return PrintReportTemporaryStorage.registerTemporaryPrintForm(out.toByteArray(), "Students.csv");
        }

        if (model.getPersonRoleType().getId() == Model.EMPLOYEE_TYPE)
        {
            final MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "e");

            builder.add(MQExpression.eq("e", EmployeePost.employee().archival().s(), Boolean.FALSE));
            if ((model.getOrgUnitList() != null) && !model.getOrgUnitList().isEmpty())
            {
                builder.add(MQExpression.in("e", EmployeePost.L_ORG_UNIT, model.getOrgUnitList()));
            }

            if (model.isWithRoles())
            {
                builder.addDomain("role", RoleAssignmentCache.ENTITY_CLASS);
                builder.add(MQExpression.eqProperty("role", RoleAssignmentCache.L_PRINCIPAL_CONTEXT + ".id", "e", "id"));
            }

            builder.addJoin("e", EmployeePost.L_EMPLOYEE, "emp");
            builder.addJoin("emp", Employee.L_PERSON, "person");
            builder.addLeftJoin("person", Person.L_ADDRESS, "address");
            builder.addLeftJoin("person", Person.L_CONTACT_DATA, "contactData");

            builder.addOrder("e", EmployeePost.employee().person().identityCard().lastName().s());
            builder.addOrder("e", EmployeePost.employee().person().identityCard().firstName().s());
            builder.addOrder("e", EmployeePost.employee().person().identityCard().middleName().s());

            builder.getSelectAliasList().clear();
            builder.addSelect("e", new Object[]{
                    EmployeePost.employee().id().s(),
                    EmployeePost.employee().person().identityCard().lastName().s(),
                    EmployeePost.employee().person().identityCard().firstName().s(),
                    EmployeePost.employee().person().identityCard().middleName().s(),
                    EmployeePost.principal().login().s()
            });
            builder.addSelect("contactData", new Object[]{
                    PersonContactData.email().s(),
                    PersonContactData.phoneMobile().s()
            });

            final List<Object[]> data = builder.getResultList(getSession());

            final ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
            final OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");

            final Set<Long> used = new HashSet<>();
            int j = 1;
            for (int i = 0; i < data.size(); i++)
            {
                final Object[] row = data.get(i);
                final Long id = (Long) row[0];

                if (!used.add(id))
                {
                    continue;
                }

                final String lastName = (String) row[1];
                final String firstName = (String) row[2];
                final String middleName = (String) row[3];
                final String login = (String) row[4];
                final String email = (String) row[5];
                final String phone = (String) row[6];

                // Нумерация
                writer.write(Integer.toString(j++));
                writer.write(",");
                // Фамилия
                writer.write("\"" + lastName + "\"");
                writer.write(",");
                // Имя и Отчество
                if (StringUtils.isEmpty(middleName))
                {
                    writer.write("\"" + firstName + "\"");
                }
                else
                {
                    writer.write("\"" + firstName + " " + middleName + "\"");
                }
                writer.write(",");
                // Логин
                writer.write("\"" + login + "\"");
                writer.write(",");
                // Пустая колонка зарезервированная под пароль
                writer.write(",");
                // Название группы
                writer.write(",");
                // Название формирующего подразделения
                writer.write(",");
                // e-mail
                if (email != null)
                {
                    writer.write("\"" + email + "\"");
                }
                writer.write(",");
                // мобильный телефон
                if (phone != null)
                {
                    writer.write("\"" + phone + "\"");
                }
                writer.write('\n');
            }

            writer.close();

            return PrintReportTemporaryStorage.registerTemporaryPrintForm(out.toByteArray(), "Employees.csv");
        }

        return null;
    }
}
