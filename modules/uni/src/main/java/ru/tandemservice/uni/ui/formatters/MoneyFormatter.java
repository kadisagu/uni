package ru.tandemservice.uni.ui.formatters;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;

import java.util.HashMap;
import java.util.Map;

/**
 * Отображает денежные суммы, хранимые в Long со смещением, в виде текстовой строки.
 * @author avedernikov
 * @since 01.07.2016.
 */
public class MoneyFormatter implements IFormatter<Long>
{
	private final CurrencyInfo currencyInfo;
	private final int tenInDegreeScale;		// 10 в степени scale: 100, если храним с точность до копейки/цента/..., 10000 - если с точностью до сотой, и т.п.
	private boolean spellNumbers = false;	// Отображать число основной единицы прописью ("десять тысяч руб. 99 коп.", если true; "10000 руб. 99 коп.", если false)
	private boolean shortTitles = true;		// Краткие названия основной и производной единиц ("10000 руб. 99 коп.", если true; "10000 рублей 99 копеек", если false)
	private boolean showPartUnits = true;	// Отображать производные единицы ("10000 руб. 99 коп.", если true; "10000 руб.", если false)
	private boolean groupTriad = false;		// Группировать цифры в основной единице тройками ("10 000 руб. 99 коп.", если true; "10000 руб. 99 коп.", если false). Не влияет на запись чисел прописью.

	/**
	 * Получить новый экземпляр форматтера.
	 * @param currencyCode Код валюты из {@link org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes}. Валюта определяет названия денежных единиц (рубли, копейки, евро, центы, и т.п.)
	 * @param scale Число десятичных цифр, которые отводятся на хранение производных единиц (копеек, центов и т.п.). Должно быть неотрицательным.
	 *              Если равно 0, то сумма хранится с точностью до основных единиц (рублей, долларов и т.п.). Если 1 - то с точностью до десятков производных единиц (копеек, центов и т.п.). И т.д.
	 *              Если больше двух, то хранится с точностью до долей производных единиц. При этом отображение производится с точностью до производных единиц с округлением до ближайшего целого.
	 * @return Новый экземпляр форматтера для указанной валюты, хранимой в типе long, где на производные денежные единицы отводится scale десятичных цифр.
	 * @throws ApplicationException Если указан неподдерживаемый код валюты.
	 */
	public static MoneyFormatter formatter(String currencyCode, int scale)
	{
		Preconditions.checkArgument(scale >= 0, "Число десятичных знаков после запятой должно быть неотрицательным.");
		if (! currencyCode2Info.containsKey(currencyCode))
			throw new ApplicationException("Невозможно создать форматтер денежных сумм. Указан неизвестный код валюты.");
		return new MoneyFormatter(currencyCode, scale);
	}

	/**
	 * Отображать число основной денежной единицы прописью (по умолчанию false). Число производной денежной единицы в любом случае отображается цифрами.
	 * @param spellNumbers Если true - будет "десять тысяч руб. 99 коп.", если false - будет "10000 руб. 99 коп."
	 */
	public MoneyFormatter spellNumbers(boolean spellNumbers)
	{
		this.spellNumbers = spellNumbers;
		return this;
	}

	/**
	 * Отображать сокращенные названия денежных единиц. По умолчанию true.
	 * @param shortTitles Если true - будет "10000 руб. 99 коп.", если false - будет "10000 рублей 99 копеек".
	 */
	public MoneyFormatter shortTitles(boolean shortTitles)
	{
		this.shortTitles = shortTitles;
		return this;
	}

	/**
	 * Отображать производные денежные единицы (копейки, центы и т.п.). По умолчанию true.
	 * @param showPartUnits Если true - будет "10000 руб. 99 коп.", если false - будет "10000 руб."
	 */
	public MoneyFormatter showPartUnits(boolean showPartUnits)
	{
		this.showPartUnits = showPartUnits;
		return this;
	}

	/**
	 * Группировать цифры в числовой записи основной денежной единицы (рубль, евро и т.п.) тройками. Если число отображается прописью, данная настройка игнорируется. По умолчанию false.
	 * @param groupTriad Если true - будет "10 000 руб. 00 коп.", если false - будет "10000 руб. 00 коп."
	 */
	public MoneyFormatter groupTriad(boolean groupTriad)
	{
		this.groupTriad = groupTriad;
		return this;
	}

	// Код валюты из CurrencyCodes -> информация о валюте. Если коды валют изменятся - необходимо будет изменить эту мапу.
	private static Map<String, CurrencyInfo> currencyCode2Info = new HashMap<>();
	static
	{
		currencyCode2Info.put(CurrencyCodes.BYR, new CurrencyInfo(true, "руб.", "коп.", new String[]{"рубль", "рубля", "рублей"}, new String[]{"копейка", "копейки", "копеек"}));
		currencyCode2Info.put(CurrencyCodes.RUB, new CurrencyInfo(true, "руб.", "коп.", new String[]{"рубль", "рубля", "рублей"}, new String[]{"копейка", "копейки", "копеек"}));
		currencyCode2Info.put(CurrencyCodes.USD, new CurrencyInfo(true, "дол.", "ц.", new String[]{"доллар", "доллара", "долларов"}, new String[]{"цент", "цента", "центов"}));
		currencyCode2Info.put(CurrencyCodes.EUR, new CurrencyInfo(true, "евр.", "ц.", new String[]{"евро", "евро", "евро"}, new String[]{"цент", "цента", "центов"}));
		currencyCode2Info.put(CurrencyCodes.UAH, new CurrencyInfo(false, "грн.", "коп.", new String[]{"гривна", "гривны", "гривен"}, new String[]{"копейка", "копейки", "копеек"}));
		currencyCode2Info.put(CurrencyCodes.CNY, new CurrencyInfo(true, "юань", "фэн.", new String[]{"юань", "юаня", "юаней"}, new String[]{"фэнь", "фэня", "фэней"}));
	}

	/**
	 * Создать новый форматтер для рублей.
	 * @param scale Число десятичных цифр, которые отводятся на хранение копеек (см. {@link #formatter})
	 * @return Новый экземпляр форматтера для суммы в рублях, хранимой в типе long, где на копейки отводится scale десятичных цифр.
	 */
	public static MoneyFormatter ruMoneyFormatter(int scale)
	{
		return formatter(CurrencyCodes.RUB, scale);
	}
	/**
	 * Создать новый форматтер для белорусских рублей.
	 * @param scale Число десятичных цифр, которые отводятся на хранение копеек (см. {@link #formatter})
	 * @return Новый экземпляр форматтера для суммы в белорусских рублях, хранимой в типе long, где на копейки отводится scale десятичных цифр.
	 */
	public static MoneyFormatter byrMoneyFormatter(int scale)
	{
		return formatter(CurrencyCodes.BYR, scale);
	}
	/**
	 * Создать новый форматтер для долларов США.
	 * @param scale Число десятичных цифр, которые отводятся на хранение центов (см. {@link #formatter})
	 * @return Новый экземпляр форматтера для суммы в долларах США, хранимой в типе long, где на центы отводится scale десятичных цифр.
	 */
	public static MoneyFormatter usdMoneyFormatter(int scale)
	{
		return formatter(CurrencyCodes.USD, scale);
	}
	/**
	 * Создать новый форматтер для евро.
	 * @param scale Число десятичных цифр, которые отводятся на хранение центов (см. {@link #formatter})
	 * @return Новый экземпляр форматтера для суммы в евро, хранимой в типе long, где на центы отводится scale десятичных цифр.
	 */
	public static MoneyFormatter eurMoneyFormatter(int scale)
	{
		return formatter(CurrencyCodes.EUR, scale);
	}
	/**
	 * Создать новый форматтер для гривен.
	 * @param scale Число десятичных цифр, которые отводятся на хранение копеек (см. {@link #formatter})
	 * @return Новый экземпляр форматтера для суммы в гривнах, хранимой в типе long, где на копейки отводится scale десятичных цифр.
	 */
	public static MoneyFormatter uahMoneyFormatter(int scale)
	{
		return formatter(CurrencyCodes.UAH, scale);
	}
	/**
	 * Создать новый форматтер для китайских юаней.
	 * @param scale Число десятичных цифр, которые отводятся на хранение фэней (см. {@link #formatter})
	 * @return Новый экземпляр форматтера для суммы в юанях, хранимой в типе long, где на фэни отводится scale десятичных цифр.
	 */
	public static MoneyFormatter cnyMoneyFormatter(int scale)
	{
		return formatter(CurrencyCodes.CNY, scale);
	}

	private MoneyFormatter(String currencyCode, int scale)
	{
		this.currencyInfo = currencyCode2Info.get(currencyCode);

		int tenInDegree = 1;
		for (int i = 0; i < scale; i++)
			tenInDegree *= 10;
		this.tenInDegreeScale = tenInDegree;
	}

	/**
	 * Является ли указанная сумма отрицательной с учетом округления. Т.е. если сумма равна -0.1 копейке, то это число округляется до нуля копеек, и будет уже неотрицательным.
	 * -0.5 копеек уже округляется до минус одной копейки.
	 * @param totalAmount Сумма со смещением
	 * @return false, если число totalAmount не меньше нуля; либо отрицательно, но округляется до 0 копеек/центов/... true, если в пересчете на целые копейки/центы/... сумма -1 копейка/цент или меньше.
	 */
	private boolean isNegativeRoundingWise(long totalAmount)
	{
		if (tenInDegreeScale <= 100)
			return totalAmount < 0;
		return totalAmount <= -(tenInDegreeScale / 200);
	}

	/**
	 * Знак числа в виде строки, с учетом записи числа прописью.
	 * @param negative Является ли число отрицательным с точки зрения {@link #isNegativeRoundingWise(long)}
	 * @return Пустая строка, если число неотрицательно; в противном случае - "-" или "минус" в зависимости от того, нужна ли запись числа прописью.
	 */
	private String signAsString(boolean negative)
	{
		if (! negative)
			return "";
		return spellNumbers ? "минус " : "-";
	}

	/**
	 * Последняя тройка цифр в записи числа в виде строки. Состоит из трех символов (если нужно - с нулями слева), если только само число имеет в записи не менее трех цифр.
	 * @param number Число, последние три цифры которого нужно получить.
	 * @return Если запись числа состоит из одной или двух цифр - строка, содержащая эти одну или две цифры.
	 * В противном случае - строка, содержащая цифры из трех младших разрядов числа (в т.ч. нули).
	 */
	private String lastTriadString(long number)
	{
		String rawTriadString = String.valueOf(number % 1000L);
		if (number < 1000)
			return rawTriadString;
		return StringUtils.leftPad(rawTriadString, 3, '0');
	}

	/**
	 * Строковая запись числа основной единицы (рубль/доллар/...), если запись прописью не требуется. Может быть группировка цифр тройками.
	 * @param mainUnitAmount Число основной денежной единицы.
	 * @return "10000" или "10 000" при mainUnitAmount == 10000L в зависимости от значения {@link #groupTriad}
	 */
	private String mainUnitAmountNotSpelled(long mainUnitAmount)
	{
		if (! groupTriad)
			return String.valueOf(mainUnitAmount);
		if (mainUnitAmount == 0)
			return "0";

		long n = mainUnitAmount;
		StringBuilder builder = new StringBuilder();
		while (n != 0)
		{
			builder.insert(0, lastTriadString(n) + " ");
			n /= 1000L;
		}
		builder.deleteCharAt(builder.length() - 1);
		return builder.toString();
	}

	/**
	 * Строковая запись числа основной единицы (рубль/доллар/...). Учитывается группировка цифр тройками и запись числа прописью.
	 * @param mainUnitAmount Число основной денежной единицы.
	 * @return "десять тысяч", "10000" или "10 000" при mainUnitAmount == 10000L в зависимости от значений {@link #groupTriad} и {@link #spellNumbers}
	 */
	private String mainUnitAmountAsString(long mainUnitAmount)
	{
		if (!spellNumbers)
			return mainUnitAmountNotSpelled(mainUnitAmount);
		return currencyInfo.titleIsMasculineGender ? NumberSpellingUtil.spellNumberMasculineGender(mainUnitAmount) : NumberSpellingUtil.spellNumberFeminineGender(mainUnitAmount);
	}

	/**
	 * Полное название денежной единицы в правильной форме в зависимости от количества.
	 * @param amount Количество денежной единицы.
	 * @param titleForms Возможные формы названия для данной единицы: {"рубль", "рубля", "рублей"} для рубля, {"копейка", "копейки", "копеек"} для копеек и т.п.
	 * @return Название единицы в правильной форме.
	 */
	private String correctUnitFullTitleForm(long amount, String[] titleForms)
	{
		if ((amount % 100) >= 10 && (amount % 100) < 20)
			return titleForms[CurrencyInfo.titleFormAnother];
		switch ((int)(amount % 10))
		{
			case 1: return titleForms[CurrencyInfo.titleForm1];
			case 2: case 3: case 4: return titleForms[CurrencyInfo.titleForm2To4];
			default: return titleForms[CurrencyInfo.titleFormAnother];
		}
	}

	/**
	 * Название основной денежной единицы (рубль/юань/...) - сокращенное или полное в правильной форме, в зависимости от настроек.
	 * @param mainCurrencyAmount Количество основной денежной единицы.
	 * @return "руб.", "рубль", "рубля", "рублей" для рубля, и т.п.
	 */
	private String mainUnitTitle(long mainCurrencyAmount)
	{
		if (shortTitles)
			return currencyInfo.mainUnitShortTitle;
		return correctUnitFullTitleForm(mainCurrencyAmount, currencyInfo.mainUnitFullTitles);
	}

	/**
	 * Название производной денежной единицы (копейка/фэнь/...) - сокращенное или полное в правильной форме, в зависимости от настроек.
	 * @param partUnitAmount Количество производной денежной единицы.
	 * @return "коп.", "копейка", "копейки", "копеек" для копейки, и т.п.
	 */
	private String partUnitTitle(long partUnitAmount)
	{
		if (shortTitles)
			return currencyInfo.partUnitShortTitle;
		return correctUnitFullTitleForm(partUnitAmount, currencyInfo.partUnitFullTitles);
	}

	/**
	 * Число производных денежных, хранимых в числе Long с учетом смещения. Учитывается округление до ближайшего целого.
	 * @param totalAmount Число, хранимое в long.
	 * @return Если totalAmount == 3, то при scale == 0 будет 0 (все цифры отводятся под основную единицу); при scale == 1 будет 30, при scale == 2 будет 3; при scale > 2 будет 0.
	 */
	private long partUnitsAmount(long totalAmount)
	{
		// Если все цифры после запятой - значимые для производных единиц (0, 1 или 2 цифры после запятой)
		if (tenInDegreeScale <= 100)
			return (totalAmount % tenInDegreeScale) * (100 / tenInDegreeScale);

		// Берем только две первые цифры после запятой, округляем до ближайшего целого
		int partUnitParts = tenInDegreeScale / 100;
		return ((totalAmount + (partUnitParts / 2)) / partUnitParts) % 100;
	}

	/**
	 * Отобразить число Long в виде строки как денежную сумму с учетом заданных настроек форматирования.
	 * @param amount Число, интерпретируемое как денежная сумма со смещением.
	 * @return Строковое представление денежной суммы.
	 */
	@Override
	public String format(Long amount)
	{
		if (null == amount)
			return null;

		long absAmount = Math.abs(amount);
		long mainUnitAmount = absAmount / tenInDegreeScale;
		long partUnitAmount = partUnitsAmount(absAmount);
		String partUnitAmountAsString = (partUnitAmount < 10) ? ("0" + partUnitAmount) : String.valueOf(partUnitAmount);

		String mainUnitString = signAsString(isNegativeRoundingWise(amount)) + mainUnitAmountAsString(mainUnitAmount) + " " + mainUnitTitle(mainUnitAmount);
		return showPartUnits ? (mainUnitString + " " + partUnitAmountAsString + " " + partUnitTitle(partUnitAmount)) : mainUnitString;
	}

	/**
	 * Информация, необходимая для корректного текстового отображения валюты.
	 */
	private static class CurrencyInfo
	{
		public final boolean titleIsMasculineGender;
		public final String mainUnitShortTitle;
		public final String partUnitShortTitle;
		public final String[] mainUnitFullTitles;
		public final String[] partUnitFullTitles;

		// Индексы форм полного названия валюты в массиве.
		/** Индекс в массиве формы названия для чисел 1, 21, 31 и т.п */
		public static final int titleForm1 = 0;
		/** Индекс в массиве формы названия для чисел 2, 3, 4, 22, 23, 24 и т.п */
		public static final int titleForm2To4 = 1;
		/** Индекс в массиве формы названия для чисел 5-20 (в т.ч. 11-14), 25-30, 35-40 и т.п */
		public static final int titleFormAnother = 2;

		/**
		 * Информация, необходимая для корректного текстового отображения валюты.
		 * @param titleIsMasculineGender Является ли название основной денежной единицы (рубль, доллар и т.п.) валюты словом мужского рода (учитывается при записи суммы прописью;
		 *                               производные единицы - копейки, центы и т.п. при этом в любом случае записываются цифрами)
		 * @param mainUnitShortTitle Короткое название основной денежной единицы.
		 * @param partUnitShortTitle Короткое название производной денежной единицы.
		 * @param mainUnitFullTitles Три формы полного названия основной денежной единицы. См. {@link #titleForm1}, {@link #titleForm2To4} и {@link #titleFormAnother}.
		 * @param partUnitFullTitles Три формы полного названия производной денежной единицы. См. {@link #titleForm1}, {@link #titleForm2To4} и {@link #titleFormAnother}.
		 */
		public CurrencyInfo(final boolean titleIsMasculineGender, final String mainUnitShortTitle, final String partUnitShortTitle,
							final String[] mainUnitFullTitles, final String[] partUnitFullTitles)
		{
			this.titleIsMasculineGender = titleIsMasculineGender;
			this.mainUnitShortTitle = mainUnitShortTitle;
			this.partUnitShortTitle = partUnitShortTitle;
			this.mainUnitFullTitles = mainUnitFullTitles;
			this.partUnitFullTitles = partUnitFullTitles;
		}
	}
}
