/**
 * $Id$
 */
package ru.tandemservice.uni.dao.importer;

/**
 * @author dseleznev
 * Created on: 16.06.2008
 */
public class ImportingOrgunit
{
    private Long _id;
    private String _innerCode;
    private String _code;
    private String _typeCode;
    private String _title;
    private String _shortTitle;
    private boolean _productive;
    private String _parentCode;

    public ImportingOrgunit(String innerCode, String code, String typeCode, String title, String shortTitle, boolean productive, String parentCode)
    {
        _innerCode = innerCode;
        _code = code.trim().length() > 0 ? code : null;
        _typeCode = typeCode;
        _title = title.trim();
        _shortTitle = shortTitle.trim();
        _productive = productive;
        _parentCode = parentCode;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public String getInnerCode()
    {
        return _innerCode;
    }

    public void setInnerCode(String innerCode)
    {
        this._innerCode = innerCode;
    }

    public String getTypeCode()
    {
        return _typeCode;
    }

    public void setTypeCode(String typeCode)
    {
        this._typeCode = typeCode;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        this._title = title;
    }

    public String getParentCode()
    {
        return _parentCode;
    }

    public void setParentCode(String parentCode)
    {
        this._parentCode = parentCode;
    }

    public String getShortTitle()
    {
        return _shortTitle;
    }

    public void setShortTitle(String shortTitle)
    {
        this._shortTitle = shortTitle;
    }

    public boolean isProductive()
    {
        return _productive;
    }

    public void setProductive(boolean productive)
    {
        this._productive = productive;
    }

    public String getCode()
    {
        return _code;
    }

    public void setCode(String code)
    {
        this._code = code;
    }

}