/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.gen.DevelopFormGen;

public class DevelopForm extends DevelopFormGen implements ITitled
{
    @Override
    @EntityDSLSupport(parts = DevelopFormGen.P_TITLE)
    public String getGenCaseTitle()
    {
        if (DevelopFormCodes.FULL_TIME_FORM.equals(getCode()))
            return "очной";
        if (DevelopFormCodes.CORESP_FORM.equals(getCode()))
            return "заочной";
        if (DevelopFormCodes.PART_TIME_FORM.equals(getCode()))
            return "очно-заочной";
        if (DevelopFormCodes.EXTERNAL_FORM.equals(getCode()))
            return "экстерната";
        if (DevelopFormCodes.APPLICANT_FORM.equals(getCode()))
            return "самостоятельного обучения";
        throw new IllegalStateException(); // ну извините
    }

    @Override
    @EntityDSLSupport
    public String getAccCaseTitle()
    {
        if (DevelopFormCodes.FULL_TIME_FORM.equals(getCode()))
            return "очную";
        if (DevelopFormCodes.CORESP_FORM.equals(getCode()))
            return "заочную";
        if (DevelopFormCodes.PART_TIME_FORM.equals(getCode()))
            return "очно-заочную";
        if (DevelopFormCodes.EXTERNAL_FORM.equals(getCode()))
            return "экстернат";
        if (DevelopFormCodes.APPLICANT_FORM.equals(getCode()))
            return "самостоятельного обучения";
        throw new IllegalStateException(); // ну извините
    }
}