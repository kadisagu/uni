/* $Id$ */
package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.view.list.column.IStyleResolver;

/**
 * @author nvankov
 * @since 3/28/13
 */
@FunctionalInterface
public interface IColoredEntity
{
    String getHtmlColor();

    IStyleResolver COLORED_STYLE_RESOLVER = row -> row instanceof IColoredEntity ? ("color:" + ((IColoredEntity) row).getHtmlColor()) : ";";
}
