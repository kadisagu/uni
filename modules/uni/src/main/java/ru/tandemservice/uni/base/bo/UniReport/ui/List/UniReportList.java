package ru.tandemservice.uni.base.bo.UniReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniReport.logic.UniReportDSHandler;

/**
 * @author Vasily Zhukov
 * @since 02.02.2011
 */
@Configuration
public class UniReportList extends BusinessComponentManager
{
    public static String REPORT_DS = "reportDS";

    @Bean
    public ColumnListExtPoint reportDS()
    {
        return columnListExtPointBuilder(REPORT_DS)
                .addColumn(actionColumn("title", "title", "onClickView"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportDSHandler()
    {
        return new UniReportDSHandler(getName());
    }
}
