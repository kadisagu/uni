/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 8/12/14
 */
@Configuration
public class UniEduProgramEduOuAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }
}