/* $Id:$ */
package ru.tandemservice.uni.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.base.ext.ReportPerson.ui.Add.ReportPersonAddExt;

/**
 * @author oleyba
 * @since 12/11/13
 */
public class UniGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniGlobalReportListAddon";

    public UniGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public ParametersMap getStudentSampleParamMap()
    {
        return new ParametersMap()
                .add(ReportPersonAddUI.TAB_LIST_VISIBLE, ReportPersonAddExt.STUDENT_TAB)
                .add(ReportPersonAddUI.SCHEET_LIST_VISIBLE, StudentReportPersonAddUI.STUDENT_SCHEET)
                .add(ReportPersonAddUI.COMPONENT_TITLE, "Форма добавления отчета «Выборка студентов»");
    }
}
