/* $Id:$ */
package ru.tandemservice.uni.base.ext.OrgUnitReport;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.base.bo.StudentPassportExpiredReport.ui.Add.StudentPassportExpiredReportAdd;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.base.ext.ReportPerson.ui.Add.ReportPersonAddExt;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.DefaultOrgUnitReportDefinition;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportBlockDefinition;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author rsizonenko
 * @since 05.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    /** Код блока отчетов на подразделении "Отчеты модуля «Студенты»" **/
    public static final String UNI_ORG_UNIT_STUDENT_REPORT_BLOCK = "uniOrgUnitStudentReportBlock";

    private List<OrgUnitReportDefinition> legacyReports = new ArrayList<>();
    private List<OrgUnitReportBlockDefinition> legacyBlocks = new ArrayList<>();

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        initLegacyReports();

        final IItemListExtensionBuilder<OrgUnitReportBlockDefinition> builder = itemListExtension(_orgUnitReportManager.blockListExtPoint());

        for (OrgUnitReportBlockDefinition legacyBlock : legacyBlocks) {
            builder.add(legacyBlock.getName(), legacyBlock);
        }

        return builder
                .add(UNI_ORG_UNIT_STUDENT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Студенты»", UNI_ORG_UNIT_STUDENT_REPORT_BLOCK, new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(_orgUnitReportManager.reportListExtPoint());
        int i = 0;
        for (OrgUnitReportDefinition legacyReport : legacyReports) {
            itemList.add(legacyReport.getComponent() + i++, legacyReport);
        }

        return itemList
                .add("studentSummary", new OrgUnitReportDefinition("Сводка контингента студентов по направлениям подготовки (специальностям)", "studentSummary", UNI_ORG_UNIT_STUDENT_REPORT_BLOCK, "ru.tandemservice.uni.component.reports.studentSummary.List", "orgUnit_viewStudentSummaryReport"))
                .add("studentSummaryNew", new OrgUnitReportDefinition("Сводка контингента студентов по направлениям подготовки (специальностям, форма 2)", "studentSummaryNew", UNI_ORG_UNIT_STUDENT_REPORT_BLOCK, "ru.tandemservice.uni.component.reports.studentSummaryNew.List", "orgUnit_viewStudentSummaryNewReport"))
                .add("reportPersonAdd", new OrgUnitReportDefinition("Выборка студентов", "reportPersonAdd", UNI_ORG_UNIT_STUDENT_REPORT_BLOCK, "ReportPersonAdd", ImmutableMap.<String, Object>builder().put("tabListVisible", ReportPersonAddExt.STUDENT_TAB).put("scheetListVisible", StudentReportPersonAddUI.STUDENT_SCHEET).build(), "orgUnit_viewStudentSampleReport"))
                .add("studentPassportExpired", new OrgUnitReportDefinition("Сведения о просроченных паспортах", "studentPassportExpired", UNI_ORG_UNIT_STUDENT_REPORT_BLOCK, StudentPassportExpiredReportAdd.class.getSimpleName(), "orgUnit_viewStudentPassportExpiredReport"))

                .create();
    }

    /**
     * Legacy
     * поддерживает расширение перенесенной в шаред точки расширения с отчетами на подразделении старым способом.
     * Если кто-то из клиентов не перенес отчеты в новую точку расширения, они будут отображаться с помощью данного метода.
    * */
    @SuppressWarnings("unchecked")
    private void initLegacyReports()
    {

        if (!ApplicationRuntime.containsBean(IOrgUnitReportDefinition.LIST_BEAN_NAME))
            return;

        final List<IOrgUnitReportDefinition> reportsToInitiate = (List<IOrgUnitReportDefinition>) ApplicationRuntime.getBean(IOrgUnitReportDefinition.LIST_BEAN_NAME);

        Map<IOrgUnitReportBlockDefinition, OrgUnitReportBlockDefinition> blockMap = new HashMap<>();

        reportsToInitiate.forEach(report -> blockMap.put(report.getBlock(),
                new OrgUnitReportBlockDefinition(report.getBlock().getBlockTitle(), report.getBlock().getBlockTitle(), report.getBlock().getPermissionPrefix(), orgUnit -> report.getBlock().isVisible(orgUnit))));

        for (IOrgUnitReportDefinition definition : reportsToInitiate)
        {

            Map params = null;
            if (definition instanceof DefaultOrgUnitReportDefinition && ((DefaultOrgUnitReportDefinition) definition).getParameters() instanceof Map)
            {
                params = ((Map) ((DefaultOrgUnitReportDefinition) definition).getParameters());
            }

            final OrgUnitReportBlockDefinition blockDefinition = blockMap.get(definition.getBlock());

            final String componentName = definition.getLinkResolver().getComponentName(null);
            final OrgUnitReportDefinition orgUnitReportDefinition = new OrgUnitReportDefinition(
                    definition.getReportTitle(), componentName, blockDefinition.getName(),
                    componentName, params,
                    definition.getPermissionPrefix(), definition::isVisible

            );

            if (!legacyBlocks.contains(blockDefinition))
                legacyBlocks.add(blockDefinition);
            legacyReports.add(orgUnitReportDefinition);

        }
    }


}
