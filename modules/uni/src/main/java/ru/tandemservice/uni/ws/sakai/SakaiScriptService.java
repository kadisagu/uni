/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ws.sakai;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author agolubenko
 * @since 16.09.2009
 */
public interface SakaiScriptService extends Remote
{
    String addNewSite(String sessionId, String siteId, String title, String description, String shortDescription, String iconUrl, String infoUrl, boolean joinable, String joinedRole, boolean published, boolean publicView, String skin, String type) throws RemoteException;

    boolean checkForSite(String sessionId, String siteId) throws RemoteException;

    String addNewRoleToAuthzGroup(String sessionId, String authzGroupid, String roleId, String Description) throws RemoteException;

    String addMemberToSiteWithRole(String sessionId, String siteId, String eid, String roleId) throws RemoteException;

    String addNewToolToPage(String sessionId, String siteId, String pageTitle, String toolTitle, String toolId, String layoutHints) throws RemoteException;

    String addNewPageToSite(String sessionId, String siteId, String pageTitle, int pageLayout) throws RemoteException;

    String copySite(String sessionId, String siteIdToCopy, String newSiteId, String title, String description, String shortDesc, String iconUrl, String infoUrl, boolean joinable, String joinedRole, boolean published, boolean publicView, String skin, String type) throws RemoteException;

    String addNewAuthzGroup(String sessionId, String groupId) throws RemoteException;

    boolean checkForAuthzGroup(String sessionId, String authzGroupId) throws RemoteException;

    boolean checkForRoleInAuthzGroup(String sessionId, String authzGroupId, String roleId) throws RemoteException;

    String getUsersInAuthzGroupWithRole(String sessionId, String authzGroupId, String authzGroupRoles) throws RemoteException;

    String removeMemberFromSite(String sessionId, String siteId, String eid) throws RemoteException;
}
