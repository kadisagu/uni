/* $Id$ */
package ru.tandemservice.uni.component.catalog.developCondition.DevelopConditionPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;

/**
 * @author azhebko
 * @since 15.08.2014
 */
public class Controller extends DefaultCatalogPubController<DevelopCondition, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<DevelopCondition> createListDataSource(IBusinessComponent context)
    {
        final Model model = getModel(context);

        final DynamicListDataSource<DevelopCondition> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", ICatalogItem.CATALOG_ITEM_TITLE));
        dataSource.addColumn(new ToggleColumn("Используется", DevelopCondition.P_ENABLED).setListener("onClickChangeCatalogItemActivity"));

        if (Debug.isDisplay()) {
            dataSource.addColumn(new SimpleColumn("Системный код", "code").setOrderable(false).setWidth(1).setClickable(false));
        }
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));

        return dataSource;
    }


    public void onClickChangeCatalogItemActivity(IBusinessComponent component)
    {
        CatalogManager.instance().modifyDao().doChangeCatalogItemActivity(component.<Long>getListenerParameter());
    }
}