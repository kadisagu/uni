/**
 * $Id$
 */
package ru.tandemservice.uni.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;

import java.util.*;

import static org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil.numberPostfixCase;

/**
 * Содержит матоды для преобразования чисел
 * 
 * @author dseleznev
 * Created on: 05.06.2009
 */
public class NumberConvertingUtil
{
    public static final int NOMINATIVE_SINGLE_CASE = 1; // Единственное число, именительный падеж
    public static final int GENETIVE_SINGLE_CASE = 2; // Единственное число, родительный падеж
    public static final int GENETIVE_PLURAL_CASE = 3; // Множественное число, родительный падеж

    public static final int NOMINATIVE_CASE = 1; // Именительный падеж
    public static final int GENETIVE_CASE = 2; // Родительный падеж
    public static final int DATIVE_CASE = 3; // Дательный падеж
    public static final int ACCUSATIVE_CASE = 4; // Винительный падеж

    private static final String[] ROMAN_CODES_ARRAY = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
    private static final int[] DECIMAL_EQUIVALENTS_ARRAY = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

    private static final String[][] NUMERIC_MALE_DIGIT_TITLE = 
    { 
        { "нулевой", "первый", "второй", "третий", "четвертый", "пятый", "шестой", "седьмой", "восьмой", "девятый" },
        { "нулевого", "первого", "второго", "третьего", "четвертого", "пятого", "шестого", "седьмого", "восьмого", "девятого" },
        { "нулевому", "первому", "второму", "третьему", "четвертому", "пятому", "шестому", "седьмому", "восьмому", "девятому" },
        { "нулевой", "первый", "второй", "третий", "четвертый", "пятый", "шестой", "седьмой", "восьмой", "девятый" }
    };

    private static final String[][] NUMERIC_FEMALE_DIGIT_TITLE = 
    { 
        { "нулевая", "первая", "вторая", "третья", "четвертая", "пятая", "шестая", "седьмая", "восьмая", "девятая" },
        { "нулевой", "первой", "второй", "третьей", "четвертой", "пятой", "шестой", "седьмой", "восьмой", "девятой" },
        { "нулевой", "первой", "второй", "третьей", "четвертой", "пятой", "шестой", "седьмой", "восьмой", "девятой" },
        { "нулевую", "первую", "вторую", "третью", "четвертую", "пятую", "шестую", "седьмую", "восьмую", "девятую" }
    };

    public static final Map<String, String [][]> CURRENCY_TITLES_MAP = new HashMap<>();
    static
    {
        CURRENCY_TITLES_MAP.put(CurrencyCodes.RUB, new String[][]{{"рубль", "рубля", "рублей"}, {"копейка", "копейки", "копеек"}});
        CURRENCY_TITLES_MAP.put(CurrencyCodes.USD, new String[][]{{"доллар", "доллара", "долларов"}, {"цент", "цента", "центов"}});
        CURRENCY_TITLES_MAP.put(CurrencyCodes.EUR, new String[][]{{"евро", "евро", "евро"}, {"цент", "цента", "центов"}});
        CURRENCY_TITLES_MAP.put(CurrencyCodes.UAH, new String[][]{{"гривна", "гривны", "гривен"}, {"копейка", "копейки", "копеек"}});
        CURRENCY_TITLES_MAP.put(CurrencyCodes.CNY, new String[][]{{"юань", "юаня", "юаней"}, {"фэнь", "фэня", "фэней"}});
        CURRENCY_TITLES_MAP.put(CurrencyCodes.BYR, new String[][]{{"рубль", "рубля", "рублей"}, {"копейка", "копейки", "копеек"}});
    }

    /**
     * Метод для тестирования предоставляемых методов преобразования чисел 
     */
    public static void main(String[] args)
    {
        System.out.println("Десятичное\tРимское");
        for (int i = 1; i < 4000; i++)
            System.out.println(i + "\t=\t" + convertDecimal2Roman(i));

        System.out.println("\n\nДесятичное\t  \tМужской род");
        for(int i = -9; i < 10; i++)
        {
            System.out.print(i + "\t=\t");
            for(int j = 1; j < 5; j++) System.out.print(getSpelledNumeric(i, false, j) + " - ");
            System.out.println();
        }

        System.out.println("\n\nДесятичное\t  \tЖенский род");
        for(int i = -9; i < 10; i++)
        {
            System.out.print(i + "\t=\t");
            for(int j = 1; j < 5; j++) System.out.print(getSpelledNumeric(i, true, j) + " - ");
            System.out.println();
        }
    }

    /**
     * Преобразует десятичные целые числа в их римское представление.<p/>
     * @param number - целое десятичное число.
     * @return число в римском представлении.<p/>
     * Оперирует с целыми числами от 1 до 3999 включительно.
     */
    public static String convertDecimal2Roman(int number)
    {
        if (number <= 0 || number >= 4000)
            throw new NumberFormatException("Given number is out of roman numeral range.");

        StringBuilder romanNumber = new StringBuilder();

        for (int i = 0; i < ROMAN_CODES_ARRAY.length; i++)
        {
            while (number >= DECIMAL_EQUIVALENTS_ARRAY[i])
            {
                number -= DECIMAL_EQUIVALENTS_ARRAY[i];
                romanNumber.append(ROMAN_CODES_ARRAY[i]);
            }
        }

        return romanNumber.toString();
    }

    /**
     * Преобразует целое число в число прописью на русском языке
     * @param number - число
     * @param femaleCase - указывает, что следующее за числом слово женского рода, например
     * "триста двадцать две копейки". Слово "копейка" женского рода.
     * @return число прописью. <p/>
     * Пока оперирует с числами от -999'999'999'999'999 до 999'999'999'999'999
	 *
	 * @deprecated Используйте {@link NumberSpellingUtil#spellNumberMasculineGender(long)} и {@link NumberSpellingUtil#spellNumberFeminineGender(long)}.
     * Эти методы работают корректно и поддерживают весь диапазон значений типа Long.
     */
	@Deprecated
    public static String getSpelledNumber(long number, boolean femaleCase)
    {
        return femaleCase ? NumberSpellingUtil.spellNumberFeminineGender(number) : NumberSpellingUtil.spellNumberMasculineGender(number);
    }

    /**
     * Преобразует целое число в число прописью на русском языке (для курсов)
     * @param number - число
     * @param femaleCase - указывает, что следующее за числом слово женского рода, например
     * "девятая рота". Слово "рота" женского рода.
     * @return число прописью. <p/>
     * Пока оперирует с числами от -9 до 9
     */
    public static String getSpelledNumeric(int number, boolean femaleCase, int titleCase)
    {
        String numberStr = String.valueOf(Math.abs(number));

        if (numberStr.length() > 1)
            throw new NumberFormatException("Given number is too large.");

        StringBuilder convertedNumber = new StringBuilder(number < 0 ? "минус " : " ");

        if(!femaleCase) convertedNumber.append(NUMERIC_MALE_DIGIT_TITLE[titleCase - 1][Math.abs(number)]);
        else convertedNumber.append(NUMERIC_FEMALE_DIGIT_TITLE[titleCase - 1][Math.abs(number)]);

        return convertedNumber.toString().trim();
    }

    /**
     * Возвращает код склонения, в котором должено стоять слово после числа,
     * например, должно быть 1 - "человек", 2-4 - "человека", 5 - 20 "человек", и т.д.
     * @param number - целое число
     * @return код склонения
     * @deprecated use CommonBaseStringUtil.numberPostfixCase()
     */
    @Deprecated
    public static int getNumberPostfixCase(long number)
    {
        number = Math.abs(number);
        long n = number % 10;
        if (n > 4 || n == 0 || (number % 100 == n + 10)) {
            return GENETIVE_PLURAL_CASE;
        }
        return n > 1 ? GENETIVE_SINGLE_CASE : NOMINATIVE_SINGLE_CASE;
    }

    /**
     * Сумма прописью, в формате "Пятьсот двадцать четыре рубля 00 копеек"
     */
    public static String writeSum(long value, int decimalDigits)
    {
		return writeSum(value, decimalDigits, CurrencyCodes.RUB);
    }

    /**
     * Сумма прописью, в формате "Пятьсот двадцать четыре рубля 00 копеек"
     *
     * @param paymentCost сумма
     * @param currencyCode код валюты
     */
    public static String writeSum(long paymentCost, int decimalDigits, String currencyCode)
    {
		MoneyFormatter formatter = MoneyFormatter.formatter(currencyCode, decimalDigits).spellNumbers(true).shortTitles(false);
        return StringUtils.capitalize(formatter.format(paymentCost));
    }

    /**
     * Возвращает стоку "календарных дней" склоненную относительно days
     *
     * @param days - кол-во дней
     * @return "календарных дней"
     * @deprecated use {@link ru.tandemservice.uni.util.NumberConvertingUtil#getCalendarDaysWithName(double)} or {@link ru.tandemservice.uni.util.NumberConvertingUtil#getCalendarDaysWithName(int)
     */
    @Deprecated
    public static String writeCalendarDays(Double days)
    {
        return numberPostfixCase((long) Math.floor(days), "календарный день", "календарных дня", "календарных дней");
    }

    /**
     * Возвращает стоку "10 календарных дней" склоненную относительно days.
     *
     * @param days - кол-во дней
     */
    public static String getCalendarDaysWithName(int days)
    {
        return CommonBaseStringUtil.numberWithPostfixCase(days, "календарный день", "календарных дня", "календарных дней");
    }

    /**
     * Возвращает стоку "10 календарных дней" склоненную относительно days.
     *
     * @param days - кол-во дней
     */
    public static String getCalendarDaysWithName(double days)
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(days) + numberPostfixCase((long) Math.floor(days), " календарный день", " календарных дня", " календарных дней");
    }

    /**
     * Возвращает стоку постфикса для числительных по передаваемому числу и массиву возможных склонений постфикса.
     * Например, есть число и набор постфиксов ("минута", "минуты", "минут"). 
     * Если передаваемое число равно 1, 21, 31 и т.д., то метод вернет "минута".
     * Если передаваемое число равно 2 - 4, 22 - 24, 32 - 34 и т.д., то метод вернет "минуты".
     * В остальных сучаях вернется "минуты". 
     * @param number - целое положительное число.
     * @param postfixCasesArray - массив склонений. Ожидаемое число склнений - 3.  
     * @return постфикс для числительных.
     * @deprecated use CommonBaseStringUtil.numberPostfixCase()
     */
    public static String getNumberPostfixTitleCaseNormal(long number, String[] postfixCasesArray)
    {
        if(null == postfixCasesArray || postfixCasesArray.length != 3)
            throw new NumberFormatException("Given postfix cases array does not correspond to the requierements.");

        return numberPostfixCase(
                number, postfixCasesArray[NOMINATIVE_SINGLE_CASE], postfixCasesArray[GENETIVE_SINGLE_CASE], postfixCasesArray[GENETIVE_PLURAL_CASE]);
    }
}