package ru.tandemservice.uni.base.bo.UniReport.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;

import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportMeta;
import ru.tandemservice.uni.base.bo.UniReport.UniReportManager;

/**
 * @author Vasily Zhukov
 * @since 02.02.2011
 */
public class UniReportDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public UniReportDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Map<IReportMeta, Long> map = UniReportManager.instance().uniReportListExtPoint().<Long>getUniqueCodeMappingSource().getItem2UniqueCodeMap();

        List<IReportMeta> list = new ArrayList<IReportMeta>(map.keySet());

        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        List<IIdentifiableWrapper> result = new ArrayList<IIdentifiableWrapper>();

        for (IReportMeta meta : list)
            result.add(new IdentifiableWrapper(map.get(meta), meta.getTitle()));

        DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build();
        output.setCountRecord(list.size());
        return output;
    }
}
