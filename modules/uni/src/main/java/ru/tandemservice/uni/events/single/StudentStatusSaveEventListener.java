/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.events.single;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;

import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author agolubenko
 * @since 04.05.2009
 */
public class StudentStatusSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @Override
    public void onFilteredEvent(HibernateSaveEvent event)
    {
        Criteria criteria = event.getSession().createCriteria(StudentStatus.class);
        criteria.setProjection(Projections.max(StudentStatus.P_PRIORITY));
        Number maxPriority = (Number) criteria.uniqueResult();

        ((StudentStatus) event.getEntity()).setPriority((maxPriority != null) ? maxPriority.intValue() + 1 : 1);
    }
}
