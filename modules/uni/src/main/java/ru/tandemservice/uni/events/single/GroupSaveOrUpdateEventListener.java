package ru.tandemservice.uni.events.single;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.transaction.DaoCache;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.group.IAbstractGroupTitleDAO;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;

/**
 * @author vdanilov
 */
public class GroupSaveOrUpdateEventListener extends FilteredSingleEntityEventListener
{
    private static final String KEY = IAbstractGroupTitleDAO.class.getSimpleName()+"@"+GroupSaveOrUpdateEventListener.class.getSimpleName();
    private static final ThreadLocal<Object> lock = new ThreadLocal<Object>();

    private IAbstractGroupTitleDAO getGroupTitleDAO()
    {
        Object value = DaoCache.get(KEY);

        if (null == value) {

            // берем алгоритм
            GroupTitleAlgorithm algorithm = UniDaoFacade.getCoreDao().get(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_CURRENT, Boolean.TRUE);
            if (null != algorithm) {
                value = (IAbstractGroupTitleDAO) ApplicationRuntime.getBean(algorithm.getDaoName());
            }

            // если не осилили - заполняем заглушкой
            if(null == value) {
                value = new Object(); // placeholder (no group title dao)
            }

            DaoCache.put(KEY, value);
        }

        // возвращаем результат
        if (value instanceof IAbstractGroupTitleDAO) { return (IAbstractGroupTitleDAO)value; }
        return null;
    }

    @Override
    public void onFilteredEvent(ISingleEntityEvent event) {

        // листенер не должен работать сам из под себя
        if (null != lock.get()) { return; }

        // берем генератор из настройки (кэшируется)
        final IAbstractGroupTitleDAO generator = getGroupTitleDAO();
        if (null == generator) { return; }

        // поехали
        lock.set(this);
        try {

            Group group = (Group) event.getEntity();
            Session session = event.getSession();

            int number = Math.max(group.getNumber(), 1);
            String title = generator.getTitle(group, number);

            if (hasOtherGroup(session, title, group)) {
                number = 1; // начинаем нумерацию с 1
                while (hasOtherGroup(session, title = generator.getTitle(group, number), group)) {
                    number++;
                    if (number >= generator.getMaxNumber()) {
                        throw new IllegalStateException("too many groups with identical parameters");
                    }
                }
            }

            // выставляем номер и название группы
            group.setNumber(number);
            group.setTitle(title);

            // затем проихойдет сохранение

        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            lock.set(null);
        }
    }

    protected boolean hasOtherGroup(Session session, String title, Group group)
    {
        Criteria c = session.createCriteria(Group.class, "g");
        c.add(Restrictions.eq("g." + Group.P_TITLE, title));
        c.add(Restrictions.eq("g." + Group.P_ARCHIVAL, Boolean.FALSE));
        c.add(Restrictions.ne("g." + Group.P_ID, (null == group.getId() ? Long.valueOf(0L) : group.getId())));
        c.setProjection(Projections.rowCount());
        Number number = (Number) c.uniqueResult();
        return number != null && number.intValue() > 0;
    }

}
