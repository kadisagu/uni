/* $Id$ */
package ru.tandemservice.uni.util;

import org.tandemframework.tapsupport.component.selection.BaseSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;

import java.util.*;

/**
 * Вынес HSelect.SimpleHSelectModel в публичный доступ
 * Если еще кому-нибудь понадобится, то перенести в uni
 *
 * @author Vasily Zhukov
 * @since 11.04.2011
 */
@SuppressWarnings("deprecation")
public abstract class HBaseSelectModel extends BaseSelectModel implements IHSelectModel, ISingleSelectModel, IMultiSelectModel
{
    @Override
    public int getLevel(Object value)
    {
        return ((HSelectOption) value).getLevel();
    }

    @Override
    public boolean isSelectable(Object value)
    {
        return ((HSelectOption) value).isCanBeSelected();
    }

    @Override
    public Object getRawValue(Object value)
    {
        return value == null ? null : ((HSelectOption) value).getObject();
    }

    @Override
    public Object getPackedValue(Object value)
    {
        return new HSelectOption(value, 0, true);
    }

    @Override
    public Object getPrimaryKey(Object object)
    {
        return super.getPrimaryKey(getRawValue(object));
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return super.getLabelFor(getRawValue(value), columnIndex);
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        ListResult list = findValues(null);
        List objects = list.getObjects();
        if (objects.size() != list.getMaxCount())
            throw new RuntimeException("HBaseSelectModel need all elements!");

        Map<Object, Object> id2obj = new HashMap<>();
        for (Object obj : objects)
            if (id2obj.put(getPrimaryKey(obj), obj) != null)
                throw new RuntimeException("HBaseSelectModel has duplicated id!");

        List<Object> result = new ArrayList<>();
        for (Object key : primaryKeys)
        {
            Object value = id2obj.get(key);
            if (value != null)
                result.add(value);
        }
        return result;
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        ListResult list = findValues(null);
        List objects = list.getObjects();
        if (objects.size() != list.getMaxCount())
            throw new RuntimeException("HBaseSelectModel need all elements!");

        for (Object obj : objects)
            if (getPrimaryKey(obj).equals(primaryKey))
                return obj;

        return null;
    }
}
