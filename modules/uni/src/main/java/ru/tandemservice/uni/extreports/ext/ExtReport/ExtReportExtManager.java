package ru.tandemservice.uni.extreports.ext.ExtReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.ExtReportManager;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.IExtReportContextDefinition;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.OrgUnitExtReportContextDefinition;
import ru.tandemservice.uni.extreports.entity.codes.ExtReportContextCodes;

/**
 * @author vdanilov
 */
@Configuration
public class ExtReportExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private ExtReportManager _extReportManager;

    @Bean
    public ItemListExtension<IExtReportContextDefinition> contextDefinitionListExtension()
    {
        IItemListExtensionBuilder<IExtReportContextDefinition> ext = this.itemListExtension(_extReportManager.contextDefinitionList());

        // Головное подразделение
        ext.add(ExtReportContextCodes.TOP_ORG_UNIT_CONTEXT, new OrgUnitExtReportContextDefinition(ExtReportContextCodes.TOP_ORG_UNIT_CONTEXT) {
            @Override public boolean isExtReportContext(IEntity entity) {
                return TopOrgUnit.class.isInstance(entity);
            }
        });

        return ext.create();
    }


}
