/* $Id: Controller.java 13876 2010-07-23 19:46:04Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.PrintReport;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author vip_delete
 * @since 21.01.2009
 */
@SuppressWarnings("unchecked")
public abstract class PrintReportControllerBase extends AbstractBusinessController
{

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    protected abstract PrintFormDescription getPrintFormDescription(PrintReportModelBase model);

    public IDocumentRenderer buildDocumentRenderer(final IBusinessComponent component)
    {
        final PrintReportModelBase model = (PrintReportModelBase) this.getModel(component);
        final PrintFormDescription printForm = getPrintFormDescription(model);

        final String internalFileName = PrintReportControllerBase.getFileName(
                StringUtils.trimToEmpty(printForm.getFileName()),
                StringUtils.trimToNull(model.getExtension())
        );
        final byte[] internalFileContent = printForm.getContent();

        // если ничего не упало - то возвращаем рендерер и закрываем компонент
        try {
            if (Boolean.TRUE.equals(model.isZip())) {
                // зипуем
                return new CommonBaseRenderer() {
                    @Override public void render(final OutputStream outputStream) throws IOException
                    {
                        final ZipOutputStream zipOut = new ZipOutputStream(outputStream);
                        zipOut.putNextEntry(new ZipEntry(CoreStringUtils.transliterate(internalFileName)));
                        zipOut.write(internalFileContent);
                        zipOut.closeEntry();
                        zipOut.close();
                    }
                }.contentType(DatabaseFile.CONTENT_TYPE_APPLICATION_ZIP).fileName(internalFileName+".zip");

            }

            // не зипуем
            return new CommonBaseRenderer() {
                @Override public void render(final OutputStream outputStream) throws IOException
                {
                    outputStream.write(internalFileContent);
                }
            }.contentType(PrintReportControllerBase.getFileContentType(internalFileName)).fileName(internalFileName);

        } finally {
            this.deactivate(component);
        }
    }



    private static String getFileContentType(final String internalFileName) {
        final String internalFileExtension = FilenameUtils.getExtension(internalFileName);
        if (null == internalFileExtension) { return "application/data"; }

        if ("rtf".equals(internalFileExtension)) { return DatabaseFile.CONTENT_TYPE_APPLICATION_RTF; }
        if ("xls".equals(internalFileExtension)) { return DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL; }
        if ("xml".equals(internalFileExtension)) { return DatabaseFile.CONTENT_TYPE_APPLICATION_XML; }

        return URLConnection.getFileNameMap().getContentTypeFor(internalFileName);
    }

    private static String getFileName(final String printFormFileName, final String modelExtension) {
        if (null == modelExtension) {
            return printFormFileName;
        }

        // есть какое-то расширение у модели

        if (printFormFileName.endsWith("."+modelExtension)) {
            // в имени файла такое же расширение - так его и выводим
            return printFormFileName;
        }

        // иначе к имени нужно дописать расширение
        return printFormFileName+"."+modelExtension;
    }


}
