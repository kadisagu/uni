package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма освоения"
 * Имя сущности : developForm
 * Файл data.xml : uni.education.data.xml
 */
public interface DevelopFormCodes
{
    /** Константа кода (code) элемента : Очная (title) */
    String FULL_TIME_FORM = "1";
    /** Константа кода (code) элемента : Заочная (title) */
    String CORESP_FORM = "2";
    /** Константа кода (code) элемента : Очно-заочная (title) */
    String PART_TIME_FORM = "3";
    /** Константа кода (code) элемента : Экстернат (title) */
    String EXTERNAL_FORM = "4";
    /** Константа кода (code) элемента : Самостоятельное обучение и итоговая аттестация (title) */
    String APPLICANT_FORM = "5";

    Set<String> CODES = ImmutableSet.of(FULL_TIME_FORM, CORESP_FORM, PART_TIME_FORM, EXTERNAL_FORM, APPLICANT_FORM);
}
