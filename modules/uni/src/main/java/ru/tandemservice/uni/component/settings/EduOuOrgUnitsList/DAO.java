// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.EduOuOrgUnitsList;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;

/**
 * @author oleyba
 * @since 03.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setDisplayOptionList(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(0L, "без диспетчерской"),
                new IdentifiableWrapper(1L, "без деканата"))));
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitListModel(new LazySimpleSelectModel<>(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)));
        model.setEducationLevelsModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<OrgUnit> formativeOrgUnitList = model.getSettings().get("formativeOrgUnitList");
                List<OrgUnit> territorialOrgUnitList = model.getSettings().get("territorialOrgUnitList");
                List<EducationLevelsHighSchool> list = UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(formativeOrgUnitList, territorialOrgUnitList, filter);
                return new ListResult<>(list);
            }
        });
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopPeriodListModel(EducationCatalogsManager.getDevelopPeriodSelectModel());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        IDataSettings settings = model.getSettings();

        IdentifiableWrapper displayOption = settings.get("displayOption");
        Object formativeOrgUnitList = settings.get("formativeOrgUnitList");
        Object territorialOrgUnitList = settings.get("territorialOrgUnitList");
        Object producingOrgUnitList = settings.get("producingOrgUnitList");
        Object eduLevelHighSchool = settings.get("eduLevelHighSchool");
        Object qualificationList = settings.get("qualification");
        Object developFormList = settings.get("developFormList");
        Object developConditionList = settings.get("developConditionList");
        Object developPeriodList = settings.get("developPeriodList");

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "ou");

        if (null != displayOption)
            builder.add(MQExpression.isNull("ou", 0L == displayOption.getId() ? EducationOrgUnit.operationOrgUnit() : EducationOrgUnit.groupOrgUnit()));
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.formativeOrgUnit().s(), formativeOrgUnitList);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.territorialOrgUnit().s(), territorialOrgUnitList);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.educationLevelHighSchool().orgUnit().s(), producingOrgUnitList);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.educationLevelHighSchool().s(), eduLevelHighSchool);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), qualificationList);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.developForm().s(), developFormList);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.developCondition().s(), developConditionList);
        FilterUtils.applySelectFilter(builder, "ou", EducationOrgUnit.developPeriod().s(), developPeriodList);

        builder.applyOrder(model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void setDefaultOperationOrgUnit(List<Long> selectedIds) {
        for (Long id : selectedIds)
        {
            EducationOrgUnit eduOu = get(EducationOrgUnit.class, id);
            if (null == eduOu.getOperationOrgUnit())
            {
                eduOu.setOperationOrgUnit(eduOu.getFormativeOrgUnit());
                update(eduOu);
            }
        }
    }

    @Override
    public void setDefaultGroupOrgUnit(List<Long> selectedIds) {
        for (Long id : selectedIds)
        {
            EducationOrgUnit eduOu = get(EducationOrgUnit.class, id);
            if (null == eduOu.getGroupOrgUnit())
            {
                eduOu.setGroupOrgUnit(eduOu.getFormativeOrgUnit());
                update(eduOu);
            }
        }
    }
}
