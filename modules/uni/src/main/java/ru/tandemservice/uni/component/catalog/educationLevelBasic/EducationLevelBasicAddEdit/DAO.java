// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelBasic.EducationLevelBasicAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.entity.catalog.EducationLevelBasic;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

/**
 * @author oleyba
 * @since 11.05.2010
 */
public class DAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit.DAO<EducationLevelBasic, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        MQBuilder levelBuilder = new MQBuilder(StructureEducationLevels.ENTITY_CLASS, "s")
                .add(MQExpression.eq("s", StructureEducationLevels.P_BASIC, true));

        MQBuilder qualificationsBuilder = new MQBuilder(Qualifications.ENTITY_CLASS, "q").add(
                MQExpression.in("q", Qualifications.P_CODE,
                                QualificationsCodes.N_P_O_2,
                                QualificationsCodes.N_P_O_3,
                                QualificationsCodes.N_P_O_4));

        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(levelBuilder.<StructureEducationLevels>getResultList(getSession()), true));

        model.setQualificationList(qualificationsBuilder.<Qualifications>getResultList(getSession()));
    }

    @Override
    protected String getOksoDuplicateErrorString()
    {
        return "Введенный код уже указан для другого направления подготовки (специальности)";
    }
}
