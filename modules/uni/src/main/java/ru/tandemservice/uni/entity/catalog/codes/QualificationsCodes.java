package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Квалификация"
 * Имя сущности : qualifications
 * Файл data.xml : uni.education.data.xml
 */
public interface QualificationsCodes
{
    /** Константа кода (code) элемента : НПО 2 (title) */
    String N_P_O_2 = "42";
    /** Константа кода (code) элемента : НПО 3 (title) */
    String N_P_O_3 = "43";
    /** Константа кода (code) элемента : НПО 4 (title) */
    String N_P_O_4 = "44";
    /** Константа кода (code) элемента : Базовый уровень СПО (title) */
    String BAZOVYY_UROVEN_S_P_O = "51";
    /** Константа кода (code) элемента : Повышенный уровень СПО (title) */
    String POVYSHENNYY_UROVEN_S_P_O = "52";
    /** Константа кода (code) элемента : Бакалавр (title) */
    String BAKALAVR = "62";
    /** Константа кода (code) элемента : Специалист (title) */
    String SPETSIALIST = "65";
    /** Константа кода (code) элемента : Магистр (title) */
    String MAGISTR = "68";
    /** Константа кода (code) элемента : Повышение квалификации (title) */
    String POVYSHENIE_KVALIFIKATSII = "ПК";
    /** Константа кода (code) элемента : Профессиональная переподготовка (title) */
    String PROFESSIONALNAYA_PEREPODGOTOVKA = "ПП";
    /** Константа кода (code) элемента : Стажировка (title) */
    String STAJIROVKA = "Ст";
    /** Константа кода (code) элемента : Дополнительные профессиональные услуги (title) */
    String DOPOLNITELNYE_PROFESSIONALNYE_USLUGI = "ДО";
    /** Константа кода (code) элемента : Аспирантура (title) */
    String ASPIRANTURA = "Асп";
    /** Константа кода (code) элемента : Интернатура (title) */
    String INTERNATURA = "Инт";
    /** Константа кода (code) элемента : Ординатура (title) */
    String ORDINATURA = "Орд";
    /** Константа кода (code) элемента : Адъюнктура (title) */
    String ADYUNKTURA = "Адн";
    /** Константа кода (code) элемента : Кандидат наук (title) */
    String KANDIDAT_NAUK = "КН";

    Set<String> CODES = ImmutableSet.of(N_P_O_2, N_P_O_3, N_P_O_4, BAZOVYY_UROVEN_S_P_O, POVYSHENNYY_UROVEN_S_P_O, BAKALAVR, SPETSIALIST, MAGISTR, POVYSHENIE_KVALIFIKATSII, PROFESSIONALNAYA_PEREPODGOTOVKA, STAJIROVKA, DOPOLNITELNYE_PROFESSIONALNYE_USLUGI, ASPIRANTURA, INTERNATURA, ORDINATURA, ADYUNKTURA, KANDIDAT_NAUK);
}
