/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author vip_delete
 */
@Input(keys = {"educationOrgUnitId", "educationLevelHighSchoolId"}, bindings = {"educationOrgUnitId", "educationLevelHighSchoolId"})
public class Model
{
    private Long _educationOrgUnitId;
    private Long _educationLevelHighSchoolId;

    private EducationOrgUnit _educationOrgUnit;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _developFormList;
    private ISelectModel _developTechList;
    private ISelectModel _developConditionList;
    private ISelectModel _developPeriodList;

    private boolean _editForm;
    private boolean _shortTitleDisabled;
    private boolean _internalCodeDisabled;

    // Getters & Setters

    public Long getEducationOrgUnitId()
    {
        return _educationOrgUnitId;
    }

    public void setEducationOrgUnitId(Long educationOrgUnitId)
    {
        _educationOrgUnitId = educationOrgUnitId;
    }

    public Long getEducationLevelHighSchoolId()
    {
        return _educationLevelHighSchoolId;
    }

    public void setEducationLevelHighSchoolId(Long educationLevelHighSchoolId)
    {
        _educationLevelHighSchoolId = educationLevelHighSchoolId;
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        _educationOrgUnit = educationOrgUnit;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public ISelectModel getDevelopTechList() { return _developTechList; }
    public void setDevelopTechList(ISelectModel developTechList) { _developTechList = developTechList; }

    public ISelectModel getDevelopConditionList() { return _developConditionList; }
    public void setDevelopConditionList(ISelectModel developConditionList) { _developConditionList = developConditionList; }

    public ISelectModel getDevelopPeriodList() { return _developPeriodList; }
    public void setDevelopPeriodList(ISelectModel developPeriodList) { _developPeriodList = developPeriodList; }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public boolean isShortTitleDisabled()
    {
        return _shortTitleDisabled;
    }

    public void setShortTitleDisabled(boolean shortTitleDisabled)
    {
        _shortTitleDisabled = shortTitleDisabled;
    }

    public boolean isInternalCodeDisabled()
    {
        return _internalCodeDisabled;
    }

    public void setInternalCodeDisabled(boolean internalCodeDisabled)
    {
        _internalCodeDisabled = internalCodeDisabled;
    }
}
