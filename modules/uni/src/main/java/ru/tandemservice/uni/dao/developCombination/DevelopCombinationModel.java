package ru.tandemservice.uni.dao.developCombination;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uni.util.DevelopUtil;

import java.util.*;

/**
 * @author vdanilov
 */
public class DevelopCombinationModel implements IDevelopCombination {

    @Override public String getTitle() { return DevelopUtil.getTitle(this); }

    private final SelectModel<DevelopCombination> developCombinationModel = new SelectModel<>();
    public SelectModel<DevelopCombination> getDevelopCombinationModel() { return developCombinationModel; }

    private final SelectModel<DevelopForm> developFormModel = new SelectModel<>();
    public SelectModel<DevelopForm> getDevelopFormModel() { return developFormModel; }
    @Override public DevelopForm getDevelopForm() { return getDevelopFormModel().getValue(); }

    private final SelectModel<DevelopCondition> developConditionModel = new SelectModel<>();
    public SelectModel<DevelopCondition> getDevelopConditionModel() { return developConditionModel; }
    @Override public DevelopCondition getDevelopCondition() { return getDevelopConditionModel().getValue(); }

    private final SelectModel<DevelopTech> developTechModel = new SelectModel<>();
    public SelectModel<DevelopTech> getDevelopTechModel() { return developTechModel; }
    @Override public DevelopTech getDevelopTech() { return getDevelopTechModel().getValue(); }

    private final SelectModel<DevelopGrid> developGridModel = new SelectModel<>();
    public SelectModel<DevelopGrid> getDevelopGridModel() { return developGridModel; }
    @Override public DevelopGrid getDevelopGrid() { return getDevelopGridModel().getValue(); }


    /** сбрасывает все значения и делает пустые модели */
    public void clear() {
        getDevelopCombinationModel().clear();
        getDevelopFormModel().clear();
        getDevelopConditionModel().clear();
        getDevelopTechModel().clear();
        getDevelopGridModel().clear();
    }

    /** возвращает комбинацию, соответствующую модели, если все поля заполены */
    public IDevelopCombination getFilledDevelopCombination() {
        if (null == getDevelopForm()) { return null; }
        if (null == getDevelopCondition()) { return null; }
        if (null == getDevelopTech()) { return null; }
        if (null == getDevelopGrid()) { return null; }

        // если все поля заполнены, то возвращаем себя :)
        return this;
    }

    /** вызывать, если требуется значения полей Ф, У, Т, С на базе значения выбранной комбинации */
    public void onChangeDevelopCombination() {
        setDevelopCombination(getDevelopCombinationModel().getValue());
    }

    /** вызывать, если требуется значения полей Ф, У, Т, С на базе значения выбранной комбинации */
    public void setDevelopCombination(final IDevelopCombination value) {
        if (null != value) {

            getDevelopFormModel().setValue(value.getDevelopForm());
            getDevelopConditionModel().setValue(value.getDevelopCondition());
            getDevelopTechModel().setValue(value.getDevelopTech());
            getDevelopGridModel().setValue(value.getDevelopGrid());

            if (value instanceof DevelopCombination) {
                getDevelopCombinationModel().setValue((DevelopCombination) value);
            } else {
                getDevelopCombinationModel().setValue(IDevelopCombinationDAO.instance.get().findSuitableCombination(this));
            }
        }
    }


    private interface DevelopCombinationSelector {
        Object select(IDevelopCombination combination);
    }

    protected ISelectModel getSelectModel(final List<IDevelopCombination> combinationList, final DevelopCombinationSelector selector) {
        return new FullCheckSelectModel() {
            @Override public Object getPrimaryKey(final Object value) { return ((IEntity)value).getId(); }
            @Override public String getLabelFor(final Object value, int columnIndex) { return ((ITitled)value).getTitle(); }

            @Override public ListResult findValues(final String filter) {
                final LinkedHashSet<Object> set = new LinkedHashSet<>(combinationList.size());
                for (final IDevelopCombination c: combinationList) {
                    final Object selection = selector.select(c);
                    if (null != selection) { set.add(selection); }
                }
                return new ListResult<>(set);
            }
        };
    }

    /** создает модели для выбора комбинаций */
    public void setSource(final Collection<IDevelopCombination> source) {
        final List<IDevelopCombination> combinationList = new ArrayList<>(source);
        Collections.sort(combinationList, IDevelopCombination.COMPARATOR);

        getDevelopCombinationModel().setSource(new FullCheckSelectModel() {
            final List<DevelopCombination> combinations = IDevelopCombinationDAO.instance.get().findSuitableCombinations(combinationList);

            @Override public ListResult findValues(final String filter) {
                return new ListResult<>(combinations);
            }
        });

        getDevelopFormModel().setSource(getSelectModel(combinationList, new DevelopCombinationSelector() {
            @Override public Object select(final IDevelopCombination combination) {
                return combination.getDevelopForm();
            }
        }));

        getDevelopConditionModel().setSource(getSelectModel(combinationList, new DevelopCombinationSelector() {
            @Override public Object select(final IDevelopCombination combination) {
                return (
                        (combination.getDevelopForm().equals(getDevelopForm()))
                ) ? combination.getDevelopCondition() : null;
            }
        }));

        getDevelopTechModel().setSource(getSelectModel(combinationList, new DevelopCombinationSelector() {
            @Override public Object select(final IDevelopCombination combination) {
                return (
                        (combination.getDevelopForm().equals(getDevelopForm())) &&
                        (combination.getDevelopCondition().equals(getDevelopCondition()))
                ) ? combination.getDevelopTech() : null;
            }
        }));

        getDevelopGridModel().setSource(getSelectModel(combinationList, new DevelopCombinationSelector() {
            @Override public Object select(final IDevelopCombination combination) {
                return (
                        (combination.getDevelopForm().equals(getDevelopForm())) &&
                        (combination.getDevelopCondition().equals(getDevelopCondition())) &&
                        (combination.getDevelopTech().equals(getDevelopTech()))
                ) ? combination.getDevelopGrid() : null;
            }
        }));
    }



}
