/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitAdd;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrgUnitListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder subBuilder = new MQBuilder(OrgUnitTypeToKindRelation.ENTITY_CLASS, "r", new String[] { OrgUnitTypeToKindRelation.L_ORG_UNIT_TYPE });
                subBuilder.add(MQExpression.eq("r", OrgUnitTypeToKindRelation.L_ORG_UNIT_KIND + "." + OrgUnitKind.P_CODE, model.getKindCode()));
                MQBuilder notSubBuilder = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "rel", new String[] { OrgUnitToKindRelation.L_ORG_UNIT + ".id" });
                notSubBuilder.add(MQExpression.eq("rel", OrgUnitToKindRelation.L_ORG_UNIT_KIND + "." + OrgUnitKind.P_CODE, model.getKindCode()));
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "o");
                builder.add(MQExpression.in("o", OrgUnit.L_ORG_UNIT_TYPE, subBuilder));
                builder.add(MQExpression.like("o", OrgUnit.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.add(MQExpression.notIn("o", OrgUnit.P_ID, notSubBuilder));
                builder.add(MQExpression.eq("o", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                builder.addOrder("o", OrgUnit.P_TITLE);
                return new ListResult<>(builder.<OrgUnit>getResultList(getSession(), 0, 100), builder.getResultCount(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                OrgUnit orgUnit = (OrgUnit) value;
                return orgUnit.getTitle() + " (" + orgUnit.getOrgUnitType().getTitle().toLowerCase() + ")";
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit orgUnit = get(OrgUnit.class, (Long) primaryKey);
                if (null != orgUnit && findValues(orgUnit.getTitle()).getObjects().contains(orgUnit))
                    return orgUnit;
                return null;
            }
        });

        if (UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING.equals(model.getKindCode()))
        {
            model.setTitle("выпускающего");
        }
        else if (UniDefines.CATALOG_ORGUNIT_KIND_FORMING.equals(model.getKindCode()))
        {
            model.setTitle("формирующего");
        }
        else if (UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL.equals(model.getKindCode()))
        {
            model.setTitle("территориального");
        }
        else
        {
            throw new ApplicationException("Неизвестный вид подразделения: " + model.getKindCode()+".");
        }

        if (!model.getKindCode().equals(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
        {
            model.setAllowAddGroup(true);
        }
    }

    @Override
    public void update(Model model)
    {
        if (!UniDaoFacade.getOrgstructDao().isOrgUnitHasKind(model.getOrgUnit(), model.getKindCode()))
        {
            OrgUnitToKindRelation rel = new OrgUnitToKindRelation();
            rel.setAllowAddGroup(model.isAllowAddGroup());
            rel.setAllowAddStudent(model.isAllowAddStudent());
            rel.setOrgUnit(model.getOrgUnit());
            rel.setOrgUnitKind(getCatalogItem(OrgUnitKind.class, model.getKindCode()));
            save(rel);
        }
    }
}
