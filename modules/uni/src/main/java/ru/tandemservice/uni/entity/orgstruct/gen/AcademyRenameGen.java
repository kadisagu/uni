package ru.tandemservice.uni.entity.orgstruct.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Переименование образовательной организации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AcademyRenameGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.orgstruct.AcademyRename";
    public static final String ENTITY_NAME = "academyRename";
    public static final int VERSION_HASH = 1256174155;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String P_PREVIOUS_FULL_TITLE = "previousFullTitle";
    public static final String P_PREVIOUS_SHORT_TITLE = "previousShortTitle";

    private Date _date;     // Дата переименования
    private String _previousFullTitle;     // Полное наименование до переименования
    private String _previousShortTitle;     // Краткое наименование до переименования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата переименования. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата переименования. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Полное наименование до переименования. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPreviousFullTitle()
    {
        return _previousFullTitle;
    }

    /**
     * @param previousFullTitle Полное наименование до переименования. Свойство не может быть null.
     */
    public void setPreviousFullTitle(String previousFullTitle)
    {
        dirty(_previousFullTitle, previousFullTitle);
        _previousFullTitle = previousFullTitle;
    }

    /**
     * @return Краткое наименование до переименования. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPreviousShortTitle()
    {
        return _previousShortTitle;
    }

    /**
     * @param previousShortTitle Краткое наименование до переименования. Свойство не может быть null.
     */
    public void setPreviousShortTitle(String previousShortTitle)
    {
        dirty(_previousShortTitle, previousShortTitle);
        _previousShortTitle = previousShortTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AcademyRenameGen)
        {
            setDate(((AcademyRename)another).getDate());
            setPreviousFullTitle(((AcademyRename)another).getPreviousFullTitle());
            setPreviousShortTitle(((AcademyRename)another).getPreviousShortTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AcademyRenameGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AcademyRename.class;
        }

        public T newInstance()
        {
            return (T) new AcademyRename();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "previousFullTitle":
                    return obj.getPreviousFullTitle();
                case "previousShortTitle":
                    return obj.getPreviousShortTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "previousFullTitle":
                    obj.setPreviousFullTitle((String) value);
                    return;
                case "previousShortTitle":
                    obj.setPreviousShortTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "previousFullTitle":
                        return true;
                case "previousShortTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "previousFullTitle":
                    return true;
                case "previousShortTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "previousFullTitle":
                    return String.class;
                case "previousShortTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AcademyRename> _dslPath = new Path<AcademyRename>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AcademyRename");
    }
            

    /**
     * @return Дата переименования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyRename#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Полное наименование до переименования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyRename#getPreviousFullTitle()
     */
    public static PropertyPath<String> previousFullTitle()
    {
        return _dslPath.previousFullTitle();
    }

    /**
     * @return Краткое наименование до переименования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyRename#getPreviousShortTitle()
     */
    public static PropertyPath<String> previousShortTitle()
    {
        return _dslPath.previousShortTitle();
    }

    public static class Path<E extends AcademyRename> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private PropertyPath<String> _previousFullTitle;
        private PropertyPath<String> _previousShortTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата переименования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyRename#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(AcademyRenameGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Полное наименование до переименования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyRename#getPreviousFullTitle()
     */
        public PropertyPath<String> previousFullTitle()
        {
            if(_previousFullTitle == null )
                _previousFullTitle = new PropertyPath<String>(AcademyRenameGen.P_PREVIOUS_FULL_TITLE, this);
            return _previousFullTitle;
        }

    /**
     * @return Краткое наименование до переименования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.AcademyRename#getPreviousShortTitle()
     */
        public PropertyPath<String> previousShortTitle()
        {
            if(_previousShortTitle == null )
                _previousShortTitle = new PropertyPath<String>(AcademyRenameGen.P_PREVIOUS_SHORT_TITLE, this);
            return _previousShortTitle;
        }

        public Class getEntityClass()
        {
            return AcademyRename.class;
        }

        public String getEntityName()
        {
            return "academyRename";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
