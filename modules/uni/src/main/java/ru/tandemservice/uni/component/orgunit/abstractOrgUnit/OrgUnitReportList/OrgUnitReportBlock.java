/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vip_delete
 * @since 26.01.2009
 */
@Deprecated
public class OrgUnitReportBlock implements ITitled
{
    private IOrgUnitReportBlockDefinition _definition;
    private List<OrgUnitReportDefinition> _reportList = new ArrayList<OrgUnitReportDefinition>();
    private AbstractListDataSource<OrgUnitReportDefinition> _dataSource;

    public OrgUnitReportBlock(IOrgUnitReportBlockDefinition definition)
    {
        _definition = definition;
    }

    @Override
    public String getTitle()
    {
        return getDefinition().getBlockTitle();
    }

    public List<OrgUnitReportDefinition> getReportList()
    {
        return _reportList;
    }

    public void setReportList(List<OrgUnitReportDefinition> reportList)
    {
        _reportList = reportList;
    }

    public AbstractListDataSource<OrgUnitReportDefinition> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(AbstractListDataSource<OrgUnitReportDefinition> dataSource)
    {
        _dataSource = dataSource;
    }

    public IOrgUnitReportBlockDefinition getDefinition()
    {
        return _definition;
    }

    public void setDefinition(IOrgUnitReportBlockDefinition definition)
    {
        _definition = definition;
    }
}
