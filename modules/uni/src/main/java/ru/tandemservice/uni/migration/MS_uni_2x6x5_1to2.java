/* $Id:$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author oleyba
 * @since 8/14/14
 */
public class MS_uni_2x6x5_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		tool.executeUpdate("update diplomaqualifications_t set code_p=? where title_p=?", "academ_bach", "Академический бакалавр");
        tool.executeUpdate("update diplomaqualifications_t set code_p=? where title_p=?", "app_bach", "Прикладной бакалавр");
    }
}
