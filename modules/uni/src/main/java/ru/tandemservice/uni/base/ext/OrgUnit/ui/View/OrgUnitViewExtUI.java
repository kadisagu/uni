/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.base.ext.OrgUnit.ui.View;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.ui.ContextList.ExtReportContextListUI;
import org.tandemframework.shared.commonbase.extreports.entity.ExtReport;
import org.tandemframework.shared.commonbase.extreports.entity.ExtReportContext;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;

import ru.tandemservice.uni.base.bo.UniOrgUnit.ui.AdditionalEdit.UniOrgUnitAdditionalEdit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.ui.AdditionalEdit.UniOrgUnitAdditionalEditUI;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.DefaultOrgUnitReportBlockDefinition;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.OrgUnitReportBlock;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

/**
 * @author Vasily Zhukov
 * @since 26.03.2012
 */
public class OrgUnitViewExtUI extends UIAddon
{
    public OrgUnitViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
    private AcademyData _academyData;

    @Override
    public void onComponentRefresh()
    {
        _academyData = AcademyData.getInstance();
    }

    public void onClickAdditionalEdit()
    {
        OrgUnitViewUI presenter = (OrgUnitViewUI) getPresenter();

        getActivationBuilder().asRegion(UniOrgUnitAdditionalEdit.class, presenter.getRegionName())
        .parameter(UniOrgUnitAdditionalEditUI.ORG_UNIT_ID, presenter.getOrgUnit().getId())
        .activate();
    }

    public AcademyData getAcademyData()
    {
        return _academyData;
    }
}
