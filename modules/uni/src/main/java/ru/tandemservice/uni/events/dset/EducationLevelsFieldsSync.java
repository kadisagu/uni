/* $Id$ */
package ru.tandemservice.uni.events.dset;

import org.hibernate.engine.SessionImplementor;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.uni.dao.IUniSyncDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

import javax.transaction.Synchronization;
import java.util.HashSet;
import java.util.Set;

/**
 * Обновляет у EducationLevels поля inheritedOkso, inheritedDiplomaQualification
 * <p/>
 * Изменения применяются в конце транзакции
 *
 * @author Vasily Zhukov
 * @since 17.11.2011
 */
public class EducationLevelsFieldsSync implements IDSetEventListener
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EducationLevels.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationLevels.class, this);

        // если изменился алгоритм полей, то при старте нужно обновить эти поля

        final String SETTING_NAME = "version";
        final Integer ALGORITHM_VERSION = 2;

        IDataSettings setting = DataSettingsFacade.getSettings(EducationLevelsFieldsSync.class.getSimpleName());

        if (!ALGORITHM_VERSION.equals(setting.get(SETTING_NAME)))
        {
            IUniSyncDao.instance.get().doUpdateEduLevelsFieldsFull();
            setting.set(SETTING_NAME, ALGORITHM_VERSION);
            DataSettingsFacade.saveSettings(setting);
        }
    }

    private static final ThreadLocal<Sync> syncs = new ThreadLocal<>();

    @Override
    public void onEvent(DSetEvent event)
    {
        @SuppressWarnings("unchecked")
        Set<String> affectedProperties = event.getMultitude().getAffectedProperties();

        if (!affectedProperties.contains(EducationLevels.P_OKSO)
                && !affectedProperties.contains(EducationLevels.L_QUALIFICATION)
                && !affectedProperties.contains(EducationLevels.L_PARENT_LEVEL))
            return;

        Sync sync = getSyncSafe(event.getContext());

        if (event.getMultitude().isSingular())
            sync.ids.add(event.getMultitude().getSingularEntity().getId());
        else
            sync.ids.addAll(new DQLSelectBuilder()
                    .fromDataSource(event.getMultitude().getSource(), "s").column("s.id")
                    .createStatement(event.getContext()).<Long>list());
    }

    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();

        if (sync == null)
        {
            syncs.set(sync = new Sync(context.getSessionImplementor()));

            context.getTransaction().registerSynchronization(sync);
        }

        return sync;
    }

    private static class Sync implements Synchronization
    {
        private SessionImplementor sessionImplementor;
        private Set<Long> ids = new HashSet<>();

        private Sync(SessionImplementor sessionImplementor)
        {
            this.sessionImplementor = sessionImplementor;
        }

        @Override
        public void beforeCompletion()
        {
            boolean hasUpdate = IUniSyncDao.instance.get().doUpdateEduLevelsFields(ids);

            if (hasUpdate)
                sessionImplementor.flush();
        }

        @Override
        public void afterCompletion(int status)
        {
            syncs.remove();
        }
    }
}
