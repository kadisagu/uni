/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.util;

import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.document.CacheInfo;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.exception.GeneralPlatformException;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfWriter;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import org.tandemframework.shared.image.stamp.*;

import java.io.*;
import java.util.List;

/**
 * @author vip_delete
 * @since 12.09.2008
 */
public class ReportRenderer implements IDocumentRenderer
{
    private String _fileName;
    private RtfDocument _document;
    private byte[] _data;
    private boolean _convertToPdf = false;
    private String _barcode;
    private boolean _addNumOfPages = false;

    @Deprecated
    public ReportRenderer(String fileName, RtfDocument document)
    {
        _fileName = fileName;
        _document = document;
        RtfUtil.optimizeFontAndColorTable(_document);
    }

    @Deprecated
    public ReportRenderer(String fileName, byte[] data)
    {
        _fileName = fileName;
        _data = data;
    }

    public ReportRenderer(String fileName, RtfDocument document, boolean convertToPdf)
    {
        _fileName = fileName;
        _document = document;
        _convertToPdf = convertToPdf;
        RtfUtil.optimizeFontAndColorTable(_document);
    }

    public ReportRenderer(String fileName, RtfDocument document, boolean convertToPdf, String barcode, boolean addNumOfPages)
    {
        _fileName = fileName;
        _document = document;
        _convertToPdf = convertToPdf;
        _barcode = barcode;
        _addNumOfPages = addNumOfPages;
        RtfUtil.optimizeFontAndColorTable(_document);
    }

    public ReportRenderer(String fileName, byte[] data, boolean convertToPdf)
    {
        _convertToPdf = convertToPdf;
        _fileName = fileName;
        _data = data;
    }

    public ReportRenderer(String fileName, byte[] data, boolean convertToPdf, String barcode, boolean addNumOfPages)
    {
        _convertToPdf = convertToPdf;
        _fileName = fileName;
        _data = data;
        _barcode = barcode;
        _addNumOfPages = addNumOfPages;
    }

    @Override
    public String getContentType()
    {
        return _convertToPdf ? DatabaseFile.CONTENT_TYPE_APPLICATION_PDF : DatabaseFile.CONTENT_TYPE_APPLICATION_RTF;
    }

    @Override
    public String getFilename()
    {
        return _fileName;
    }

    @Override
    public void render(OutputStream outputStream) throws IOException
    {
        if (_data != null)
        {
            if (_convertToPdf)
            {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                OpenOfficeServiceUtil.convertRtfToPdf(new ByteArrayInputStream(_data), baos);
                writePdfToOutputStream(baos, outputStream);
            }
            else outputStream.write(_data);
        } else
        {
            if (_convertToPdf)
            {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                OpenOfficeServiceUtil.convertRtfToPdf(new ByteArrayInputStream(RtfUtil.toByteArray(_document)), baos);
                writePdfToOutputStream(baos, outputStream);
            }
            else RtfWriter.write(_document, outputStream);
        }
    }

    private void writePdfToOutputStream(ByteArrayOutputStream baos, OutputStream outputStream)
    {
        if(!StringUtils.isEmpty(_barcode))
        {
            InputStream ins = new ByteArrayInputStream(baos.toByteArray());

            PdfReader prd;
            PdfStamper pst;
            try
            {
                prd = new PdfReader(ins);
                pst = new PdfStamper(prd, baos);
            }
            catch (IOException e)
            {

                if("PdfReader not opened with owner password".equals(e.getLocalizedMessage()))
                    throw new ApplicationException("Файл защищен от записи паролем. Невозможно поставить штамп.");
                else
                {
                    throw new GeneralPlatformException(e);
                }
            }
            catch (DocumentException e)
            {
                throw new GeneralPlatformException(e);
            }

            int numberOfPages = prd.getNumberOfPages();

            Stamp stamp = createStamp(numberOfPages);

            for(int i = 0; i < numberOfPages; i++)
            {

                stamp.draw(pst, i+1, null, 300, 300);
            }

            try
            {
                pst.close();
                outputStream.write(baos.toByteArray());
            }
            catch (DocumentException | IOException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            try
            {
                outputStream.write(baos.toByteArray());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private Stamp createStamp(int numberOfPages)
    {
        Stamp stmp = new Stamp();
        stmp.setRightMargin(new SizeHolder(150, SizeUnit.MM));
        stmp.setBottomMargin(new SizeHolder(5, SizeUnit.MM));

        StampVerticalLayout mainLayout = new StampVerticalLayout();
        List<IStampRenderer> mainLayoutRenderers = Lists.newArrayList();
        mainLayout.setRenderers(mainLayoutRenderers);

        mainLayout.setBorder(false);
        mainLayout.setWidth(new SizeHolder(50, SizeUnit.MM));
        mainLayout.setHeight(new SizeHolder(9, SizeUnit.MM));
        mainLayout.setAlign(StampAlign.middle);

        StampHorizontalLayout imageHorizonatlLayout = new StampHorizontalLayout();
        List<IStampRenderer> imageHorizonatlLayoutRenderers = Lists.newArrayList();
        mainLayoutRenderers.add(imageHorizonatlLayout);

        imageHorizonatlLayout.setBorder(false);
//        imageHorizonatlLayout.setVerticalAlign(StampVerticalAlign.top);
        imageHorizonatlLayout.setAlign(StampAlign.left);
        imageHorizonatlLayout.setRenderers(imageHorizonatlLayoutRenderers);

        StampImageCode imageCode = new StampImageCode();
        imageHorizonatlLayoutRenderers.add(imageCode);

        imageCode.setWidth(new SizeHolder(50, SizeUnit.MM));
        imageCode.setHeight(new SizeHolder(6, SizeUnit.MM));

        String errorCorrLvl = ErrorCorrectionLevel.Q.name();
        imageCode.setErrorCorrection(errorCorrLvl);

        String barcode = _barcode + (_addNumOfPages ? String.format("%03d", numberOfPages) : "");

        imageCode.setText(barcode);
        imageCode.setColor("0,0,0");
        imageCode.setFormat(BarcodeFormat.CODE_128.name());

        StampHorizontalLayout valueHorizonatlLayout = new StampHorizontalLayout();

        List<IStampRenderer> valueHorizonatlLayoutRenderers = Lists.newArrayList();
        valueHorizonatlLayout.setBorder(false);
        valueHorizonatlLayout.setAlign(StampAlign.middle);
        valueHorizonatlLayout.setVerticalAlign(StampVerticalAlign.top);
        valueHorizonatlLayout.setRenderers(valueHorizonatlLayoutRenderers);
//
        mainLayoutRenderers.add(valueHorizonatlLayout);

        StampText valueText = new StampText();

        valueText.setAlign(StampAlign.right);
        valueText.setText(barcode);
        valueText.setFont("Times New Roman,B,7");

        valueHorizonatlLayoutRenderers.add(valueText);


//        textExample.setColor(colorR + "," + colorG + "," + colorB);
//        textExample.setText(i <= strings.size()-1 ? strings.get(i) : "");
//        textExample.setFont(fontFamily + "," + fontBoldItalic + fontSize);

        stmp.setRenderer(mainLayout);
        return stmp;
    }

    @Override
    public Integer getContentSize()
    {
        return null;
    }

    @Override
    public CacheInfo getCacheInfo()
    {
        return null;
    }
}