package ru.tandemservice.uni.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные о последних приказах
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrderDataGen extends EntityBase
 implements INaturalIdentifiable<OrderDataGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.employee.OrderData";
    public static final String ENTITY_NAME = "orderData";
    public static final int VERSION_HASH = 636859987;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_EDU_ENROLLMENT_ORDER_DATE = "eduEnrollmentOrderDate";
    public static final String P_EDU_ENROLLMENT_ORDER_NUMBER = "eduEnrollmentOrderNumber";
    public static final String P_EDU_ENROLLMENT_ORDER_ENR_DATE = "eduEnrollmentOrderEnrDate";
    public static final String P_TRANSFER_ORDER_DATE = "transferOrderDate";
    public static final String P_TRANSFER_ORDER_NUMBER = "transferOrderNumber";
    public static final String P_SESSION_PROLONG_ORDER_DATE = "sessionProlongOrderDate";
    public static final String P_SESSION_PROLONG_ORDER_NUMBER = "sessionProlongOrderNumber";
    public static final String P_SESSION_PROLONG_DATE_TO = "sessionProlongDateTo";
    public static final String P_WEEKEND_ORDER_DATE = "weekendOrderDate";
    public static final String P_WEEKEND_ORDER_NUMBER = "weekendOrderNumber";
    public static final String P_WEEKEND_DATE_FROM = "weekendDateFrom";
    public static final String P_WEEKEND_DATE_TO = "weekendDateTo";
    public static final String P_WEEKEND_OUT_ORDER_DATE = "weekendOutOrderDate";
    public static final String P_WEEKEND_OUT_ORDER_NUMBER = "weekendOutOrderNumber";
    public static final String P_WEEKEND_PREGNANCY_ORDER_DATE = "weekendPregnancyOrderDate";
    public static final String P_WEEKEND_PREGNANCY_ORDER_NUMBER = "weekendPregnancyOrderNumber";
    public static final String P_WEEKEND_PREGNANCY_DATE_FROM = "weekendPregnancyDateFrom";
    public static final String P_WEEKEND_PREGNANCY_DATE_TO = "weekendPregnancyDateTo";
    public static final String P_WEEKEND_PREGNANCY_OUT_ORDER_DATE = "weekendPregnancyOutOrderDate";
    public static final String P_WEEKEND_PREGNANCY_OUT_ORDER_NUMBER = "weekendPregnancyOutOrderNumber";
    public static final String P_WEEKEND_CHILD_ORDER_DATE = "weekendChildOrderDate";
    public static final String P_WEEKEND_CHILD_ORDER_NUMBER = "weekendChildOrderNumber";
    public static final String P_WEEKEND_CHILD_DATE_FROM = "weekendChildDateFrom";
    public static final String P_WEEKEND_CHILD_DATE_TO = "weekendChildDateTo";
    public static final String P_WEEKEND_CHILD_OUT_ORDER_DATE = "weekendChildOutOrderDate";
    public static final String P_WEEKEND_CHILD_OUT_ORDER_NUMBER = "weekendChildOutOrderNumber";
    public static final String P_RESTORATION_ORDER_DATE = "restorationOrderDate";
    public static final String P_RESTORATION_ORDER_NUMBER = "restorationOrderNumber";
    public static final String P_CHANGE_FIO_ORDER_DATE = "changeFioOrderDate";
    public static final String P_CHANGE_FIO_ORDER_NUMBER = "changeFioOrderNumber";
    public static final String P_EXCLUDE_ORDER_DATE = "excludeOrderDate";
    public static final String P_EXCLUDE_ORDER_NUMBER = "excludeOrderNumber";
    public static final String P_CHANGE_COMPENSATION_TYPE_ORDER_DATE = "changeCompensationTypeOrderDate";
    public static final String P_CHANGE_COMPENSATION_TYPE_ORDER_NUMBER = "changeCompensationTypeOrderNumber";
    public static final String P_GRADUATE_DIPLOMA_ORDER_DATE = "graduateDiplomaOrderDate";
    public static final String P_GRADUATE_DIPLOMA_ORDER_NUMBER = "graduateDiplomaOrderNumber";
    public static final String P_GRADUATE_SUCCESS_DIPLOMA_ORDER_DATE = "graduateSuccessDiplomaOrderDate";
    public static final String P_GRADUATE_SUCCESS_DIPLOMA_ORDER_NUMBER = "graduateSuccessDiplomaOrderNumber";
    public static final String P_HOLIDAYS_ORDER_DATE = "holidaysOrderDate";
    public static final String P_HOLIDAYS_ORDER_NUMBER = "holidaysOrderNumber";
    public static final String P_HOLIDAYS_BEGIN_DATE = "holidaysBeginDate";
    public static final String P_HOLIDAYS_END_DATE = "holidaysEndDate";
    public static final String P_SPLIT_TO_SPECIALIZATION_ORDER_DATE = "splitToSpecializationOrderDate";
    public static final String P_SPLIT_TO_SPECIALIZATION_ORDER_NUMBER = "splitToSpecializationOrderNumber";
    public static final String P_PUBLIC_ORDER_ORDER_DATE = "publicOrderOrderDate";
    public static final String P_PUBLIC_ORDER_ORDER_NUMBER = "publicOrderOrderNumber";
    public static final String P_ACAD_GRANT_ASSIGNMENT_ORDER_DATE = "acadGrantAssignmentOrderDate";
    public static final String P_ACAD_GRANT_ASSIGNMENT_ORDER_NUMBER = "acadGrantAssignmentOrderNumber";
    public static final String P_ACAD_GRANT_PAYMENT_DATE_FROM = "acadGrantPaymentDateFrom";
    public static final String P_ACAD_GRANT_PAYMENT_DATE_TO = "acadGrantPaymentDateTo";
    public static final String P_SOC_GRANT_ASSIGNMENT_ORDER_DATE = "socGrantAssignmentOrderDate";
    public static final String P_SOC_GRANT_ASSIGNMENT_ORDER_NUMBER = "socGrantAssignmentOrderNumber";
    public static final String P_SOC_GRANT_PAYMENT_DATE_FROM = "socGrantPaymentDateFrom";
    public static final String P_SOC_GRANT_PAYMENT_DATE_TO = "socGrantPaymentDateTo";
    public static final String P_ACAD_GRANT_BONUS_ASSIGNMENT_ORDER_DATE = "acadGrantBonusAssignmentOrderDate";
    public static final String P_ACAD_GRANT_BONUS_ASSIGNMENT_ORDER_NUMBER = "acadGrantBonusAssignmentOrderNumber";
    public static final String P_ACAD_GRANT_BONUS_PAYMENT_DATE_FROM = "acadGrantBonusPaymentDateFrom";
    public static final String P_ACAD_GRANT_BONUS_PAYMENT_DATE_TO = "acadGrantBonusPaymentDateTo";
    public static final String P_SEND_PRACTICE_ORDER_DATE = "sendPracticeOrderDate";
    public static final String P_SEND_PRACTICE_ORDER_NUMBER = "sendPracticeOrderNumber";

    private Student _student;     // Студент
    private Date _eduEnrollmentOrderDate;     // Приказ о зачислении - дата
    private String _eduEnrollmentOrderNumber;     // Приказ о зачислении - номер
    private Date _eduEnrollmentOrderEnrDate;     // Приказ о зачислении - зачислен с
    private Date _transferOrderDate;     // Приказ о переводе - дата
    private String _transferOrderNumber;     // Приказ о переводе - номер
    private Date _sessionProlongOrderDate;     // Приказ о продлении экзаменационной сессии - дата
    private String _sessionProlongOrderNumber;     // Приказ о продлении экзаменационной сессии - номер
    private Date _sessionProlongDateTo;     // Сессия продлена до
    private Date _weekendOrderDate;     // Приказ о предоставлении академического отпуска - дата
    private String _weekendOrderNumber;     // Приказ о предоставлении академического отпуска - номер
    private Date _weekendDateFrom;     // Дата начала отпуска
    private Date _weekendDateTo;     // Дата окончания отпуска
    private Date _weekendOutOrderDate;     // Приказ о выходе из академического отпуска - дата
    private String _weekendOutOrderNumber;     // Приказ о выходе из академического отпуска - номер
    private Date _weekendPregnancyOrderDate;     // Приказ о предоставлении отпуска по беременности и родам - дата
    private String _weekendPregnancyOrderNumber;     // Приказ о предоставлении отпуска по беременности и родам - номер
    private Date _weekendPregnancyDateFrom;     // Дата начала отпуска
    private Date _weekendPregnancyDateTo;     // Дата окончания отпуска
    private Date _weekendPregnancyOutOrderDate;     // Приказ о выходе из отпуска по беременности и родам - дата
    private String _weekendPregnancyOutOrderNumber;     // Приказ о выходе из отпуска по беременности и родам - номер
    private Date _weekendChildOrderDate;     // Приказ о предоставлении отпуска по уходу за ребенком - дата
    private String _weekendChildOrderNumber;     // Приказ о предоставлении отпуска по уходу за ребенком - номер
    private Date _weekendChildDateFrom;     // Дата начала отпуска
    private Date _weekendChildDateTo;     // Дата окончания отпуска
    private Date _weekendChildOutOrderDate;     // Приказ о выходе из отпуска по уходу за ребенком - дата
    private String _weekendChildOutOrderNumber;     // Приказ о выходе из отпуска по уходу за ребенком - номер
    private Date _restorationOrderDate;     // Приказ о восстановлении - дата
    private String _restorationOrderNumber;     // Приказ о восстановлении - номер
    private Date _changeFioOrderDate;     // Приказ о смене фамилии (имени, отчества) - дата
    private String _changeFioOrderNumber;     // Приказ о смене фамилии (имени, отчества) - номер
    private Date _excludeOrderDate;     // Приказ об отчислении - дата
    private String _excludeOrderNumber;     // Приказ об отчислении - номер
    private Date _changeCompensationTypeOrderDate;     // Приказ об изменении основы оплаты обучения - дата
    private String _changeCompensationTypeOrderNumber;     // Приказ об изменении основы оплаты обучения - номер
    private Date _graduateDiplomaOrderDate;     // Приказ о выпуске (диплом) - дата
    private String _graduateDiplomaOrderNumber;     // Приказ о выпуске (диплом) - номер
    private Date _graduateSuccessDiplomaOrderDate;     // Приказ о выпуске (диплом с отличием) - дата
    private String _graduateSuccessDiplomaOrderNumber;     // Приказ о выпуске (диплом с отличием) - номер
    private Date _holidaysOrderDate;     // Приказ о предоставлении каникул - дата
    private String _holidaysOrderNumber;     // Приказ о предоставлении каникул - номер
    private Date _holidaysBeginDate;     // Приказ о предоставлении каникул - дата начала
    private Date _holidaysEndDate;     // Приказ о предоставлении каникул - дата окончания
    private Date _splitToSpecializationOrderDate;     // Приказ о распределении по профилям/специализациям - дата
    private String _splitToSpecializationOrderNumber;     // Приказ о распределении по профилям/специализациям - номер
    private Date _publicOrderOrderDate;     // Приказ об общественном поручении - дата
    private String _publicOrderOrderNumber;     // Приказ об общественном поручении - номер
    private Date _acadGrantAssignmentOrderDate;     // Приказ о назначении академической стипендии - дата
    private String _acadGrantAssignmentOrderNumber;     // Приказ о назначении академической стипендии - номер
    private Date _acadGrantPaymentDateFrom;     // Дата начала выплаты академической стипендии
    private Date _acadGrantPaymentDateTo;     // Дата окончания выплаты академической стипендии
    private Date _socGrantAssignmentOrderDate;     // Приказ о назначении социальной стипендии - дата
    private String _socGrantAssignmentOrderNumber;     // Приказ о назначении социальной стипендии - номер
    private Date _socGrantPaymentDateFrom;     // Дата начала выплаты социальной стипендии
    private Date _socGrantPaymentDateTo;     // Дата окончания выплаты социальной стипендии
    private Date _acadGrantBonusAssignmentOrderDate;     // Приказ о назначении надбавки к академической стипендии - дата
    private String _acadGrantBonusAssignmentOrderNumber;     // Приказ о назначении надбавки к академической стипендии - номер
    private Date _acadGrantBonusPaymentDateFrom;     // Дата начала выплаты надбавки к академической стипендии
    private Date _acadGrantBonusPaymentDateTo;     // Дата окончания выплаты надбавки к академической стипендии
    private Date _sendPracticeOrderDate;     // Приказ о направлении на практику - дата
    private String _sendPracticeOrderNumber;     // Приказ о направлении на практику - номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Приказ о зачислении - дата.
     */
    public Date getEduEnrollmentOrderDate()
    {
        return _eduEnrollmentOrderDate;
    }

    /**
     * @param eduEnrollmentOrderDate Приказ о зачислении - дата.
     */
    public void setEduEnrollmentOrderDate(Date eduEnrollmentOrderDate)
    {
        dirty(_eduEnrollmentOrderDate, eduEnrollmentOrderDate);
        _eduEnrollmentOrderDate = eduEnrollmentOrderDate;
    }

    /**
     * @return Приказ о зачислении - номер.
     */
    @Length(max=255)
    public String getEduEnrollmentOrderNumber()
    {
        return _eduEnrollmentOrderNumber;
    }

    /**
     * @param eduEnrollmentOrderNumber Приказ о зачислении - номер.
     */
    public void setEduEnrollmentOrderNumber(String eduEnrollmentOrderNumber)
    {
        dirty(_eduEnrollmentOrderNumber, eduEnrollmentOrderNumber);
        _eduEnrollmentOrderNumber = eduEnrollmentOrderNumber;
    }

    /**
     * @return Приказ о зачислении - зачислен с.
     */
    public Date getEduEnrollmentOrderEnrDate()
    {
        return _eduEnrollmentOrderEnrDate;
    }

    /**
     * @param eduEnrollmentOrderEnrDate Приказ о зачислении - зачислен с.
     */
    public void setEduEnrollmentOrderEnrDate(Date eduEnrollmentOrderEnrDate)
    {
        dirty(_eduEnrollmentOrderEnrDate, eduEnrollmentOrderEnrDate);
        _eduEnrollmentOrderEnrDate = eduEnrollmentOrderEnrDate;
    }

    /**
     * @return Приказ о переводе - дата.
     */
    public Date getTransferOrderDate()
    {
        return _transferOrderDate;
    }

    /**
     * @param transferOrderDate Приказ о переводе - дата.
     */
    public void setTransferOrderDate(Date transferOrderDate)
    {
        dirty(_transferOrderDate, transferOrderDate);
        _transferOrderDate = transferOrderDate;
    }

    /**
     * @return Приказ о переводе - номер.
     */
    @Length(max=255)
    public String getTransferOrderNumber()
    {
        return _transferOrderNumber;
    }

    /**
     * @param transferOrderNumber Приказ о переводе - номер.
     */
    public void setTransferOrderNumber(String transferOrderNumber)
    {
        dirty(_transferOrderNumber, transferOrderNumber);
        _transferOrderNumber = transferOrderNumber;
    }

    /**
     * @return Приказ о продлении экзаменационной сессии - дата.
     */
    public Date getSessionProlongOrderDate()
    {
        return _sessionProlongOrderDate;
    }

    /**
     * @param sessionProlongOrderDate Приказ о продлении экзаменационной сессии - дата.
     */
    public void setSessionProlongOrderDate(Date sessionProlongOrderDate)
    {
        dirty(_sessionProlongOrderDate, sessionProlongOrderDate);
        _sessionProlongOrderDate = sessionProlongOrderDate;
    }

    /**
     * @return Приказ о продлении экзаменационной сессии - номер.
     */
    @Length(max=255)
    public String getSessionProlongOrderNumber()
    {
        return _sessionProlongOrderNumber;
    }

    /**
     * @param sessionProlongOrderNumber Приказ о продлении экзаменационной сессии - номер.
     */
    public void setSessionProlongOrderNumber(String sessionProlongOrderNumber)
    {
        dirty(_sessionProlongOrderNumber, sessionProlongOrderNumber);
        _sessionProlongOrderNumber = sessionProlongOrderNumber;
    }

    /**
     * @return Сессия продлена до.
     */
    public Date getSessionProlongDateTo()
    {
        return _sessionProlongDateTo;
    }

    /**
     * @param sessionProlongDateTo Сессия продлена до.
     */
    public void setSessionProlongDateTo(Date sessionProlongDateTo)
    {
        dirty(_sessionProlongDateTo, sessionProlongDateTo);
        _sessionProlongDateTo = sessionProlongDateTo;
    }

    /**
     * @return Приказ о предоставлении академического отпуска - дата.
     */
    public Date getWeekendOrderDate()
    {
        return _weekendOrderDate;
    }

    /**
     * @param weekendOrderDate Приказ о предоставлении академического отпуска - дата.
     */
    public void setWeekendOrderDate(Date weekendOrderDate)
    {
        dirty(_weekendOrderDate, weekendOrderDate);
        _weekendOrderDate = weekendOrderDate;
    }

    /**
     * @return Приказ о предоставлении академического отпуска - номер.
     */
    @Length(max=255)
    public String getWeekendOrderNumber()
    {
        return _weekendOrderNumber;
    }

    /**
     * @param weekendOrderNumber Приказ о предоставлении академического отпуска - номер.
     */
    public void setWeekendOrderNumber(String weekendOrderNumber)
    {
        dirty(_weekendOrderNumber, weekendOrderNumber);
        _weekendOrderNumber = weekendOrderNumber;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getWeekendDateFrom()
    {
        return _weekendDateFrom;
    }

    /**
     * @param weekendDateFrom Дата начала отпуска.
     */
    public void setWeekendDateFrom(Date weekendDateFrom)
    {
        dirty(_weekendDateFrom, weekendDateFrom);
        _weekendDateFrom = weekendDateFrom;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getWeekendDateTo()
    {
        return _weekendDateTo;
    }

    /**
     * @param weekendDateTo Дата окончания отпуска.
     */
    public void setWeekendDateTo(Date weekendDateTo)
    {
        dirty(_weekendDateTo, weekendDateTo);
        _weekendDateTo = weekendDateTo;
    }

    /**
     * @return Приказ о выходе из академического отпуска - дата.
     */
    public Date getWeekendOutOrderDate()
    {
        return _weekendOutOrderDate;
    }

    /**
     * @param weekendOutOrderDate Приказ о выходе из академического отпуска - дата.
     */
    public void setWeekendOutOrderDate(Date weekendOutOrderDate)
    {
        dirty(_weekendOutOrderDate, weekendOutOrderDate);
        _weekendOutOrderDate = weekendOutOrderDate;
    }

    /**
     * @return Приказ о выходе из академического отпуска - номер.
     */
    @Length(max=255)
    public String getWeekendOutOrderNumber()
    {
        return _weekendOutOrderNumber;
    }

    /**
     * @param weekendOutOrderNumber Приказ о выходе из академического отпуска - номер.
     */
    public void setWeekendOutOrderNumber(String weekendOutOrderNumber)
    {
        dirty(_weekendOutOrderNumber, weekendOutOrderNumber);
        _weekendOutOrderNumber = weekendOutOrderNumber;
    }

    /**
     * @return Приказ о предоставлении отпуска по беременности и родам - дата.
     */
    public Date getWeekendPregnancyOrderDate()
    {
        return _weekendPregnancyOrderDate;
    }

    /**
     * @param weekendPregnancyOrderDate Приказ о предоставлении отпуска по беременности и родам - дата.
     */
    public void setWeekendPregnancyOrderDate(Date weekendPregnancyOrderDate)
    {
        dirty(_weekendPregnancyOrderDate, weekendPregnancyOrderDate);
        _weekendPregnancyOrderDate = weekendPregnancyOrderDate;
    }

    /**
     * @return Приказ о предоставлении отпуска по беременности и родам - номер.
     */
    @Length(max=255)
    public String getWeekendPregnancyOrderNumber()
    {
        return _weekendPregnancyOrderNumber;
    }

    /**
     * @param weekendPregnancyOrderNumber Приказ о предоставлении отпуска по беременности и родам - номер.
     */
    public void setWeekendPregnancyOrderNumber(String weekendPregnancyOrderNumber)
    {
        dirty(_weekendPregnancyOrderNumber, weekendPregnancyOrderNumber);
        _weekendPregnancyOrderNumber = weekendPregnancyOrderNumber;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getWeekendPregnancyDateFrom()
    {
        return _weekendPregnancyDateFrom;
    }

    /**
     * @param weekendPregnancyDateFrom Дата начала отпуска.
     */
    public void setWeekendPregnancyDateFrom(Date weekendPregnancyDateFrom)
    {
        dirty(_weekendPregnancyDateFrom, weekendPregnancyDateFrom);
        _weekendPregnancyDateFrom = weekendPregnancyDateFrom;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getWeekendPregnancyDateTo()
    {
        return _weekendPregnancyDateTo;
    }

    /**
     * @param weekendPregnancyDateTo Дата окончания отпуска.
     */
    public void setWeekendPregnancyDateTo(Date weekendPregnancyDateTo)
    {
        dirty(_weekendPregnancyDateTo, weekendPregnancyDateTo);
        _weekendPregnancyDateTo = weekendPregnancyDateTo;
    }

    /**
     * @return Приказ о выходе из отпуска по беременности и родам - дата.
     */
    public Date getWeekendPregnancyOutOrderDate()
    {
        return _weekendPregnancyOutOrderDate;
    }

    /**
     * @param weekendPregnancyOutOrderDate Приказ о выходе из отпуска по беременности и родам - дата.
     */
    public void setWeekendPregnancyOutOrderDate(Date weekendPregnancyOutOrderDate)
    {
        dirty(_weekendPregnancyOutOrderDate, weekendPregnancyOutOrderDate);
        _weekendPregnancyOutOrderDate = weekendPregnancyOutOrderDate;
    }

    /**
     * @return Приказ о выходе из отпуска по беременности и родам - номер.
     */
    @Length(max=255)
    public String getWeekendPregnancyOutOrderNumber()
    {
        return _weekendPregnancyOutOrderNumber;
    }

    /**
     * @param weekendPregnancyOutOrderNumber Приказ о выходе из отпуска по беременности и родам - номер.
     */
    public void setWeekendPregnancyOutOrderNumber(String weekendPregnancyOutOrderNumber)
    {
        dirty(_weekendPregnancyOutOrderNumber, weekendPregnancyOutOrderNumber);
        _weekendPregnancyOutOrderNumber = weekendPregnancyOutOrderNumber;
    }

    /**
     * @return Приказ о предоставлении отпуска по уходу за ребенком - дата.
     */
    public Date getWeekendChildOrderDate()
    {
        return _weekendChildOrderDate;
    }

    /**
     * @param weekendChildOrderDate Приказ о предоставлении отпуска по уходу за ребенком - дата.
     */
    public void setWeekendChildOrderDate(Date weekendChildOrderDate)
    {
        dirty(_weekendChildOrderDate, weekendChildOrderDate);
        _weekendChildOrderDate = weekendChildOrderDate;
    }

    /**
     * @return Приказ о предоставлении отпуска по уходу за ребенком - номер.
     */
    @Length(max=255)
    public String getWeekendChildOrderNumber()
    {
        return _weekendChildOrderNumber;
    }

    /**
     * @param weekendChildOrderNumber Приказ о предоставлении отпуска по уходу за ребенком - номер.
     */
    public void setWeekendChildOrderNumber(String weekendChildOrderNumber)
    {
        dirty(_weekendChildOrderNumber, weekendChildOrderNumber);
        _weekendChildOrderNumber = weekendChildOrderNumber;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getWeekendChildDateFrom()
    {
        return _weekendChildDateFrom;
    }

    /**
     * @param weekendChildDateFrom Дата начала отпуска.
     */
    public void setWeekendChildDateFrom(Date weekendChildDateFrom)
    {
        dirty(_weekendChildDateFrom, weekendChildDateFrom);
        _weekendChildDateFrom = weekendChildDateFrom;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getWeekendChildDateTo()
    {
        return _weekendChildDateTo;
    }

    /**
     * @param weekendChildDateTo Дата окончания отпуска.
     */
    public void setWeekendChildDateTo(Date weekendChildDateTo)
    {
        dirty(_weekendChildDateTo, weekendChildDateTo);
        _weekendChildDateTo = weekendChildDateTo;
    }

    /**
     * @return Приказ о выходе из отпуска по уходу за ребенком - дата.
     */
    public Date getWeekendChildOutOrderDate()
    {
        return _weekendChildOutOrderDate;
    }

    /**
     * @param weekendChildOutOrderDate Приказ о выходе из отпуска по уходу за ребенком - дата.
     */
    public void setWeekendChildOutOrderDate(Date weekendChildOutOrderDate)
    {
        dirty(_weekendChildOutOrderDate, weekendChildOutOrderDate);
        _weekendChildOutOrderDate = weekendChildOutOrderDate;
    }

    /**
     * @return Приказ о выходе из отпуска по уходу за ребенком - номер.
     */
    @Length(max=255)
    public String getWeekendChildOutOrderNumber()
    {
        return _weekendChildOutOrderNumber;
    }

    /**
     * @param weekendChildOutOrderNumber Приказ о выходе из отпуска по уходу за ребенком - номер.
     */
    public void setWeekendChildOutOrderNumber(String weekendChildOutOrderNumber)
    {
        dirty(_weekendChildOutOrderNumber, weekendChildOutOrderNumber);
        _weekendChildOutOrderNumber = weekendChildOutOrderNumber;
    }

    /**
     * @return Приказ о восстановлении - дата.
     */
    public Date getRestorationOrderDate()
    {
        return _restorationOrderDate;
    }

    /**
     * @param restorationOrderDate Приказ о восстановлении - дата.
     */
    public void setRestorationOrderDate(Date restorationOrderDate)
    {
        dirty(_restorationOrderDate, restorationOrderDate);
        _restorationOrderDate = restorationOrderDate;
    }

    /**
     * @return Приказ о восстановлении - номер.
     */
    @Length(max=255)
    public String getRestorationOrderNumber()
    {
        return _restorationOrderNumber;
    }

    /**
     * @param restorationOrderNumber Приказ о восстановлении - номер.
     */
    public void setRestorationOrderNumber(String restorationOrderNumber)
    {
        dirty(_restorationOrderNumber, restorationOrderNumber);
        _restorationOrderNumber = restorationOrderNumber;
    }

    /**
     * @return Приказ о смене фамилии (имени, отчества) - дата.
     */
    public Date getChangeFioOrderDate()
    {
        return _changeFioOrderDate;
    }

    /**
     * @param changeFioOrderDate Приказ о смене фамилии (имени, отчества) - дата.
     */
    public void setChangeFioOrderDate(Date changeFioOrderDate)
    {
        dirty(_changeFioOrderDate, changeFioOrderDate);
        _changeFioOrderDate = changeFioOrderDate;
    }

    /**
     * @return Приказ о смене фамилии (имени, отчества) - номер.
     */
    @Length(max=255)
    public String getChangeFioOrderNumber()
    {
        return _changeFioOrderNumber;
    }

    /**
     * @param changeFioOrderNumber Приказ о смене фамилии (имени, отчества) - номер.
     */
    public void setChangeFioOrderNumber(String changeFioOrderNumber)
    {
        dirty(_changeFioOrderNumber, changeFioOrderNumber);
        _changeFioOrderNumber = changeFioOrderNumber;
    }

    /**
     * @return Приказ об отчислении - дата.
     */
    public Date getExcludeOrderDate()
    {
        return _excludeOrderDate;
    }

    /**
     * @param excludeOrderDate Приказ об отчислении - дата.
     */
    public void setExcludeOrderDate(Date excludeOrderDate)
    {
        dirty(_excludeOrderDate, excludeOrderDate);
        _excludeOrderDate = excludeOrderDate;
    }

    /**
     * @return Приказ об отчислении - номер.
     */
    @Length(max=255)
    public String getExcludeOrderNumber()
    {
        return _excludeOrderNumber;
    }

    /**
     * @param excludeOrderNumber Приказ об отчислении - номер.
     */
    public void setExcludeOrderNumber(String excludeOrderNumber)
    {
        dirty(_excludeOrderNumber, excludeOrderNumber);
        _excludeOrderNumber = excludeOrderNumber;
    }

    /**
     * @return Приказ об изменении основы оплаты обучения - дата.
     */
    public Date getChangeCompensationTypeOrderDate()
    {
        return _changeCompensationTypeOrderDate;
    }

    /**
     * @param changeCompensationTypeOrderDate Приказ об изменении основы оплаты обучения - дата.
     */
    public void setChangeCompensationTypeOrderDate(Date changeCompensationTypeOrderDate)
    {
        dirty(_changeCompensationTypeOrderDate, changeCompensationTypeOrderDate);
        _changeCompensationTypeOrderDate = changeCompensationTypeOrderDate;
    }

    /**
     * @return Приказ об изменении основы оплаты обучения - номер.
     */
    @Length(max=255)
    public String getChangeCompensationTypeOrderNumber()
    {
        return _changeCompensationTypeOrderNumber;
    }

    /**
     * @param changeCompensationTypeOrderNumber Приказ об изменении основы оплаты обучения - номер.
     */
    public void setChangeCompensationTypeOrderNumber(String changeCompensationTypeOrderNumber)
    {
        dirty(_changeCompensationTypeOrderNumber, changeCompensationTypeOrderNumber);
        _changeCompensationTypeOrderNumber = changeCompensationTypeOrderNumber;
    }

    /**
     * @return Приказ о выпуске (диплом) - дата.
     */
    public Date getGraduateDiplomaOrderDate()
    {
        return _graduateDiplomaOrderDate;
    }

    /**
     * @param graduateDiplomaOrderDate Приказ о выпуске (диплом) - дата.
     */
    public void setGraduateDiplomaOrderDate(Date graduateDiplomaOrderDate)
    {
        dirty(_graduateDiplomaOrderDate, graduateDiplomaOrderDate);
        _graduateDiplomaOrderDate = graduateDiplomaOrderDate;
    }

    /**
     * @return Приказ о выпуске (диплом) - номер.
     */
    @Length(max=255)
    public String getGraduateDiplomaOrderNumber()
    {
        return _graduateDiplomaOrderNumber;
    }

    /**
     * @param graduateDiplomaOrderNumber Приказ о выпуске (диплом) - номер.
     */
    public void setGraduateDiplomaOrderNumber(String graduateDiplomaOrderNumber)
    {
        dirty(_graduateDiplomaOrderNumber, graduateDiplomaOrderNumber);
        _graduateDiplomaOrderNumber = graduateDiplomaOrderNumber;
    }

    /**
     * @return Приказ о выпуске (диплом с отличием) - дата.
     */
    public Date getGraduateSuccessDiplomaOrderDate()
    {
        return _graduateSuccessDiplomaOrderDate;
    }

    /**
     * @param graduateSuccessDiplomaOrderDate Приказ о выпуске (диплом с отличием) - дата.
     */
    public void setGraduateSuccessDiplomaOrderDate(Date graduateSuccessDiplomaOrderDate)
    {
        dirty(_graduateSuccessDiplomaOrderDate, graduateSuccessDiplomaOrderDate);
        _graduateSuccessDiplomaOrderDate = graduateSuccessDiplomaOrderDate;
    }

    /**
     * @return Приказ о выпуске (диплом с отличием) - номер.
     */
    @Length(max=255)
    public String getGraduateSuccessDiplomaOrderNumber()
    {
        return _graduateSuccessDiplomaOrderNumber;
    }

    /**
     * @param graduateSuccessDiplomaOrderNumber Приказ о выпуске (диплом с отличием) - номер.
     */
    public void setGraduateSuccessDiplomaOrderNumber(String graduateSuccessDiplomaOrderNumber)
    {
        dirty(_graduateSuccessDiplomaOrderNumber, graduateSuccessDiplomaOrderNumber);
        _graduateSuccessDiplomaOrderNumber = graduateSuccessDiplomaOrderNumber;
    }

    /**
     * @return Приказ о предоставлении каникул - дата.
     */
    public Date getHolidaysOrderDate()
    {
        return _holidaysOrderDate;
    }

    /**
     * @param holidaysOrderDate Приказ о предоставлении каникул - дата.
     */
    public void setHolidaysOrderDate(Date holidaysOrderDate)
    {
        dirty(_holidaysOrderDate, holidaysOrderDate);
        _holidaysOrderDate = holidaysOrderDate;
    }

    /**
     * @return Приказ о предоставлении каникул - номер.
     */
    @Length(max=255)
    public String getHolidaysOrderNumber()
    {
        return _holidaysOrderNumber;
    }

    /**
     * @param holidaysOrderNumber Приказ о предоставлении каникул - номер.
     */
    public void setHolidaysOrderNumber(String holidaysOrderNumber)
    {
        dirty(_holidaysOrderNumber, holidaysOrderNumber);
        _holidaysOrderNumber = holidaysOrderNumber;
    }

    /**
     * @return Приказ о предоставлении каникул - дата начала.
     */
    public Date getHolidaysBeginDate()
    {
        return _holidaysBeginDate;
    }

    /**
     * @param holidaysBeginDate Приказ о предоставлении каникул - дата начала.
     */
    public void setHolidaysBeginDate(Date holidaysBeginDate)
    {
        dirty(_holidaysBeginDate, holidaysBeginDate);
        _holidaysBeginDate = holidaysBeginDate;
    }

    /**
     * @return Приказ о предоставлении каникул - дата окончания.
     */
    public Date getHolidaysEndDate()
    {
        return _holidaysEndDate;
    }

    /**
     * @param holidaysEndDate Приказ о предоставлении каникул - дата окончания.
     */
    public void setHolidaysEndDate(Date holidaysEndDate)
    {
        dirty(_holidaysEndDate, holidaysEndDate);
        _holidaysEndDate = holidaysEndDate;
    }

    /**
     * @return Приказ о распределении по профилям/специализациям - дата.
     */
    public Date getSplitToSpecializationOrderDate()
    {
        return _splitToSpecializationOrderDate;
    }

    /**
     * @param splitToSpecializationOrderDate Приказ о распределении по профилям/специализациям - дата.
     */
    public void setSplitToSpecializationOrderDate(Date splitToSpecializationOrderDate)
    {
        dirty(_splitToSpecializationOrderDate, splitToSpecializationOrderDate);
        _splitToSpecializationOrderDate = splitToSpecializationOrderDate;
    }

    /**
     * @return Приказ о распределении по профилям/специализациям - номер.
     */
    @Length(max=255)
    public String getSplitToSpecializationOrderNumber()
    {
        return _splitToSpecializationOrderNumber;
    }

    /**
     * @param splitToSpecializationOrderNumber Приказ о распределении по профилям/специализациям - номер.
     */
    public void setSplitToSpecializationOrderNumber(String splitToSpecializationOrderNumber)
    {
        dirty(_splitToSpecializationOrderNumber, splitToSpecializationOrderNumber);
        _splitToSpecializationOrderNumber = splitToSpecializationOrderNumber;
    }

    /**
     * @return Приказ об общественном поручении - дата.
     */
    public Date getPublicOrderOrderDate()
    {
        return _publicOrderOrderDate;
    }

    /**
     * @param publicOrderOrderDate Приказ об общественном поручении - дата.
     */
    public void setPublicOrderOrderDate(Date publicOrderOrderDate)
    {
        dirty(_publicOrderOrderDate, publicOrderOrderDate);
        _publicOrderOrderDate = publicOrderOrderDate;
    }

    /**
     * @return Приказ об общественном поручении - номер.
     */
    @Length(max=255)
    public String getPublicOrderOrderNumber()
    {
        return _publicOrderOrderNumber;
    }

    /**
     * @param publicOrderOrderNumber Приказ об общественном поручении - номер.
     */
    public void setPublicOrderOrderNumber(String publicOrderOrderNumber)
    {
        dirty(_publicOrderOrderNumber, publicOrderOrderNumber);
        _publicOrderOrderNumber = publicOrderOrderNumber;
    }

    /**
     * @return Приказ о назначении академической стипендии - дата.
     */
    public Date getAcadGrantAssignmentOrderDate()
    {
        return _acadGrantAssignmentOrderDate;
    }

    /**
     * @param acadGrantAssignmentOrderDate Приказ о назначении академической стипендии - дата.
     */
    public void setAcadGrantAssignmentOrderDate(Date acadGrantAssignmentOrderDate)
    {
        dirty(_acadGrantAssignmentOrderDate, acadGrantAssignmentOrderDate);
        _acadGrantAssignmentOrderDate = acadGrantAssignmentOrderDate;
    }

    /**
     * @return Приказ о назначении академической стипендии - номер.
     */
    @Length(max=255)
    public String getAcadGrantAssignmentOrderNumber()
    {
        return _acadGrantAssignmentOrderNumber;
    }

    /**
     * @param acadGrantAssignmentOrderNumber Приказ о назначении академической стипендии - номер.
     */
    public void setAcadGrantAssignmentOrderNumber(String acadGrantAssignmentOrderNumber)
    {
        dirty(_acadGrantAssignmentOrderNumber, acadGrantAssignmentOrderNumber);
        _acadGrantAssignmentOrderNumber = acadGrantAssignmentOrderNumber;
    }

    /**
     * @return Дата начала выплаты академической стипендии.
     */
    public Date getAcadGrantPaymentDateFrom()
    {
        return _acadGrantPaymentDateFrom;
    }

    /**
     * @param acadGrantPaymentDateFrom Дата начала выплаты академической стипендии.
     */
    public void setAcadGrantPaymentDateFrom(Date acadGrantPaymentDateFrom)
    {
        dirty(_acadGrantPaymentDateFrom, acadGrantPaymentDateFrom);
        _acadGrantPaymentDateFrom = acadGrantPaymentDateFrom;
    }

    /**
     * @return Дата окончания выплаты академической стипендии.
     */
    public Date getAcadGrantPaymentDateTo()
    {
        return _acadGrantPaymentDateTo;
    }

    /**
     * @param acadGrantPaymentDateTo Дата окончания выплаты академической стипендии.
     */
    public void setAcadGrantPaymentDateTo(Date acadGrantPaymentDateTo)
    {
        dirty(_acadGrantPaymentDateTo, acadGrantPaymentDateTo);
        _acadGrantPaymentDateTo = acadGrantPaymentDateTo;
    }

    /**
     * @return Приказ о назначении социальной стипендии - дата.
     */
    public Date getSocGrantAssignmentOrderDate()
    {
        return _socGrantAssignmentOrderDate;
    }

    /**
     * @param socGrantAssignmentOrderDate Приказ о назначении социальной стипендии - дата.
     */
    public void setSocGrantAssignmentOrderDate(Date socGrantAssignmentOrderDate)
    {
        dirty(_socGrantAssignmentOrderDate, socGrantAssignmentOrderDate);
        _socGrantAssignmentOrderDate = socGrantAssignmentOrderDate;
    }

    /**
     * @return Приказ о назначении социальной стипендии - номер.
     */
    @Length(max=255)
    public String getSocGrantAssignmentOrderNumber()
    {
        return _socGrantAssignmentOrderNumber;
    }

    /**
     * @param socGrantAssignmentOrderNumber Приказ о назначении социальной стипендии - номер.
     */
    public void setSocGrantAssignmentOrderNumber(String socGrantAssignmentOrderNumber)
    {
        dirty(_socGrantAssignmentOrderNumber, socGrantAssignmentOrderNumber);
        _socGrantAssignmentOrderNumber = socGrantAssignmentOrderNumber;
    }

    /**
     * @return Дата начала выплаты социальной стипендии.
     */
    public Date getSocGrantPaymentDateFrom()
    {
        return _socGrantPaymentDateFrom;
    }

    /**
     * @param socGrantPaymentDateFrom Дата начала выплаты социальной стипендии.
     */
    public void setSocGrantPaymentDateFrom(Date socGrantPaymentDateFrom)
    {
        dirty(_socGrantPaymentDateFrom, socGrantPaymentDateFrom);
        _socGrantPaymentDateFrom = socGrantPaymentDateFrom;
    }

    /**
     * @return Дата окончания выплаты социальной стипендии.
     */
    public Date getSocGrantPaymentDateTo()
    {
        return _socGrantPaymentDateTo;
    }

    /**
     * @param socGrantPaymentDateTo Дата окончания выплаты социальной стипендии.
     */
    public void setSocGrantPaymentDateTo(Date socGrantPaymentDateTo)
    {
        dirty(_socGrantPaymentDateTo, socGrantPaymentDateTo);
        _socGrantPaymentDateTo = socGrantPaymentDateTo;
    }

    /**
     * @return Приказ о назначении надбавки к академической стипендии - дата.
     */
    public Date getAcadGrantBonusAssignmentOrderDate()
    {
        return _acadGrantBonusAssignmentOrderDate;
    }

    /**
     * @param acadGrantBonusAssignmentOrderDate Приказ о назначении надбавки к академической стипендии - дата.
     */
    public void setAcadGrantBonusAssignmentOrderDate(Date acadGrantBonusAssignmentOrderDate)
    {
        dirty(_acadGrantBonusAssignmentOrderDate, acadGrantBonusAssignmentOrderDate);
        _acadGrantBonusAssignmentOrderDate = acadGrantBonusAssignmentOrderDate;
    }

    /**
     * @return Приказ о назначении надбавки к академической стипендии - номер.
     */
    @Length(max=255)
    public String getAcadGrantBonusAssignmentOrderNumber()
    {
        return _acadGrantBonusAssignmentOrderNumber;
    }

    /**
     * @param acadGrantBonusAssignmentOrderNumber Приказ о назначении надбавки к академической стипендии - номер.
     */
    public void setAcadGrantBonusAssignmentOrderNumber(String acadGrantBonusAssignmentOrderNumber)
    {
        dirty(_acadGrantBonusAssignmentOrderNumber, acadGrantBonusAssignmentOrderNumber);
        _acadGrantBonusAssignmentOrderNumber = acadGrantBonusAssignmentOrderNumber;
    }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии.
     */
    public Date getAcadGrantBonusPaymentDateFrom()
    {
        return _acadGrantBonusPaymentDateFrom;
    }

    /**
     * @param acadGrantBonusPaymentDateFrom Дата начала выплаты надбавки к академической стипендии.
     */
    public void setAcadGrantBonusPaymentDateFrom(Date acadGrantBonusPaymentDateFrom)
    {
        dirty(_acadGrantBonusPaymentDateFrom, acadGrantBonusPaymentDateFrom);
        _acadGrantBonusPaymentDateFrom = acadGrantBonusPaymentDateFrom;
    }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии.
     */
    public Date getAcadGrantBonusPaymentDateTo()
    {
        return _acadGrantBonusPaymentDateTo;
    }

    /**
     * @param acadGrantBonusPaymentDateTo Дата окончания выплаты надбавки к академической стипендии.
     */
    public void setAcadGrantBonusPaymentDateTo(Date acadGrantBonusPaymentDateTo)
    {
        dirty(_acadGrantBonusPaymentDateTo, acadGrantBonusPaymentDateTo);
        _acadGrantBonusPaymentDateTo = acadGrantBonusPaymentDateTo;
    }

    /**
     * @return Приказ о направлении на практику - дата.
     */
    public Date getSendPracticeOrderDate()
    {
        return _sendPracticeOrderDate;
    }

    /**
     * @param sendPracticeOrderDate Приказ о направлении на практику - дата.
     */
    public void setSendPracticeOrderDate(Date sendPracticeOrderDate)
    {
        dirty(_sendPracticeOrderDate, sendPracticeOrderDate);
        _sendPracticeOrderDate = sendPracticeOrderDate;
    }

    /**
     * @return Приказ о направлении на практику - номер.
     */
    @Length(max=255)
    public String getSendPracticeOrderNumber()
    {
        return _sendPracticeOrderNumber;
    }

    /**
     * @param sendPracticeOrderNumber Приказ о направлении на практику - номер.
     */
    public void setSendPracticeOrderNumber(String sendPracticeOrderNumber)
    {
        dirty(_sendPracticeOrderNumber, sendPracticeOrderNumber);
        _sendPracticeOrderNumber = sendPracticeOrderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrderDataGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent(((OrderData)another).getStudent());
            }
            setEduEnrollmentOrderDate(((OrderData)another).getEduEnrollmentOrderDate());
            setEduEnrollmentOrderNumber(((OrderData)another).getEduEnrollmentOrderNumber());
            setEduEnrollmentOrderEnrDate(((OrderData)another).getEduEnrollmentOrderEnrDate());
            setTransferOrderDate(((OrderData)another).getTransferOrderDate());
            setTransferOrderNumber(((OrderData)another).getTransferOrderNumber());
            setSessionProlongOrderDate(((OrderData)another).getSessionProlongOrderDate());
            setSessionProlongOrderNumber(((OrderData)another).getSessionProlongOrderNumber());
            setSessionProlongDateTo(((OrderData)another).getSessionProlongDateTo());
            setWeekendOrderDate(((OrderData)another).getWeekendOrderDate());
            setWeekendOrderNumber(((OrderData)another).getWeekendOrderNumber());
            setWeekendDateFrom(((OrderData)another).getWeekendDateFrom());
            setWeekendDateTo(((OrderData)another).getWeekendDateTo());
            setWeekendOutOrderDate(((OrderData)another).getWeekendOutOrderDate());
            setWeekendOutOrderNumber(((OrderData)another).getWeekendOutOrderNumber());
            setWeekendPregnancyOrderDate(((OrderData)another).getWeekendPregnancyOrderDate());
            setWeekendPregnancyOrderNumber(((OrderData)another).getWeekendPregnancyOrderNumber());
            setWeekendPregnancyDateFrom(((OrderData)another).getWeekendPregnancyDateFrom());
            setWeekendPregnancyDateTo(((OrderData)another).getWeekendPregnancyDateTo());
            setWeekendPregnancyOutOrderDate(((OrderData)another).getWeekendPregnancyOutOrderDate());
            setWeekendPregnancyOutOrderNumber(((OrderData)another).getWeekendPregnancyOutOrderNumber());
            setWeekendChildOrderDate(((OrderData)another).getWeekendChildOrderDate());
            setWeekendChildOrderNumber(((OrderData)another).getWeekendChildOrderNumber());
            setWeekendChildDateFrom(((OrderData)another).getWeekendChildDateFrom());
            setWeekendChildDateTo(((OrderData)another).getWeekendChildDateTo());
            setWeekendChildOutOrderDate(((OrderData)another).getWeekendChildOutOrderDate());
            setWeekendChildOutOrderNumber(((OrderData)another).getWeekendChildOutOrderNumber());
            setRestorationOrderDate(((OrderData)another).getRestorationOrderDate());
            setRestorationOrderNumber(((OrderData)another).getRestorationOrderNumber());
            setChangeFioOrderDate(((OrderData)another).getChangeFioOrderDate());
            setChangeFioOrderNumber(((OrderData)another).getChangeFioOrderNumber());
            setExcludeOrderDate(((OrderData)another).getExcludeOrderDate());
            setExcludeOrderNumber(((OrderData)another).getExcludeOrderNumber());
            setChangeCompensationTypeOrderDate(((OrderData)another).getChangeCompensationTypeOrderDate());
            setChangeCompensationTypeOrderNumber(((OrderData)another).getChangeCompensationTypeOrderNumber());
            setGraduateDiplomaOrderDate(((OrderData)another).getGraduateDiplomaOrderDate());
            setGraduateDiplomaOrderNumber(((OrderData)another).getGraduateDiplomaOrderNumber());
            setGraduateSuccessDiplomaOrderDate(((OrderData)another).getGraduateSuccessDiplomaOrderDate());
            setGraduateSuccessDiplomaOrderNumber(((OrderData)another).getGraduateSuccessDiplomaOrderNumber());
            setHolidaysOrderDate(((OrderData)another).getHolidaysOrderDate());
            setHolidaysOrderNumber(((OrderData)another).getHolidaysOrderNumber());
            setHolidaysBeginDate(((OrderData)another).getHolidaysBeginDate());
            setHolidaysEndDate(((OrderData)another).getHolidaysEndDate());
            setSplitToSpecializationOrderDate(((OrderData)another).getSplitToSpecializationOrderDate());
            setSplitToSpecializationOrderNumber(((OrderData)another).getSplitToSpecializationOrderNumber());
            setPublicOrderOrderDate(((OrderData)another).getPublicOrderOrderDate());
            setPublicOrderOrderNumber(((OrderData)another).getPublicOrderOrderNumber());
            setAcadGrantAssignmentOrderDate(((OrderData)another).getAcadGrantAssignmentOrderDate());
            setAcadGrantAssignmentOrderNumber(((OrderData)another).getAcadGrantAssignmentOrderNumber());
            setAcadGrantPaymentDateFrom(((OrderData)another).getAcadGrantPaymentDateFrom());
            setAcadGrantPaymentDateTo(((OrderData)another).getAcadGrantPaymentDateTo());
            setSocGrantAssignmentOrderDate(((OrderData)another).getSocGrantAssignmentOrderDate());
            setSocGrantAssignmentOrderNumber(((OrderData)another).getSocGrantAssignmentOrderNumber());
            setSocGrantPaymentDateFrom(((OrderData)another).getSocGrantPaymentDateFrom());
            setSocGrantPaymentDateTo(((OrderData)another).getSocGrantPaymentDateTo());
            setAcadGrantBonusAssignmentOrderDate(((OrderData)another).getAcadGrantBonusAssignmentOrderDate());
            setAcadGrantBonusAssignmentOrderNumber(((OrderData)another).getAcadGrantBonusAssignmentOrderNumber());
            setAcadGrantBonusPaymentDateFrom(((OrderData)another).getAcadGrantBonusPaymentDateFrom());
            setAcadGrantBonusPaymentDateTo(((OrderData)another).getAcadGrantBonusPaymentDateTo());
            setSendPracticeOrderDate(((OrderData)another).getSendPracticeOrderDate());
            setSendPracticeOrderNumber(((OrderData)another).getSendPracticeOrderNumber());
        }
    }

    public INaturalId<OrderDataGen> getNaturalId()
    {
        return new NaturalId(getStudent());
    }

    public static class NaturalId extends NaturalIdBase<OrderDataGen>
    {
        private static final String PROXY_NAME = "OrderDataNaturalProxy";

        private Long _student;

        public NaturalId()
        {}

        public NaturalId(Student student)
        {
            _student = ((IEntity) student).getId();
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OrderDataGen.NaturalId) ) return false;

            OrderDataGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrderDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrderData.class;
        }

        public T newInstance()
        {
            return (T) new OrderData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "eduEnrollmentOrderDate":
                    return obj.getEduEnrollmentOrderDate();
                case "eduEnrollmentOrderNumber":
                    return obj.getEduEnrollmentOrderNumber();
                case "eduEnrollmentOrderEnrDate":
                    return obj.getEduEnrollmentOrderEnrDate();
                case "transferOrderDate":
                    return obj.getTransferOrderDate();
                case "transferOrderNumber":
                    return obj.getTransferOrderNumber();
                case "sessionProlongOrderDate":
                    return obj.getSessionProlongOrderDate();
                case "sessionProlongOrderNumber":
                    return obj.getSessionProlongOrderNumber();
                case "sessionProlongDateTo":
                    return obj.getSessionProlongDateTo();
                case "weekendOrderDate":
                    return obj.getWeekendOrderDate();
                case "weekendOrderNumber":
                    return obj.getWeekendOrderNumber();
                case "weekendDateFrom":
                    return obj.getWeekendDateFrom();
                case "weekendDateTo":
                    return obj.getWeekendDateTo();
                case "weekendOutOrderDate":
                    return obj.getWeekendOutOrderDate();
                case "weekendOutOrderNumber":
                    return obj.getWeekendOutOrderNumber();
                case "weekendPregnancyOrderDate":
                    return obj.getWeekendPregnancyOrderDate();
                case "weekendPregnancyOrderNumber":
                    return obj.getWeekendPregnancyOrderNumber();
                case "weekendPregnancyDateFrom":
                    return obj.getWeekendPregnancyDateFrom();
                case "weekendPregnancyDateTo":
                    return obj.getWeekendPregnancyDateTo();
                case "weekendPregnancyOutOrderDate":
                    return obj.getWeekendPregnancyOutOrderDate();
                case "weekendPregnancyOutOrderNumber":
                    return obj.getWeekendPregnancyOutOrderNumber();
                case "weekendChildOrderDate":
                    return obj.getWeekendChildOrderDate();
                case "weekendChildOrderNumber":
                    return obj.getWeekendChildOrderNumber();
                case "weekendChildDateFrom":
                    return obj.getWeekendChildDateFrom();
                case "weekendChildDateTo":
                    return obj.getWeekendChildDateTo();
                case "weekendChildOutOrderDate":
                    return obj.getWeekendChildOutOrderDate();
                case "weekendChildOutOrderNumber":
                    return obj.getWeekendChildOutOrderNumber();
                case "restorationOrderDate":
                    return obj.getRestorationOrderDate();
                case "restorationOrderNumber":
                    return obj.getRestorationOrderNumber();
                case "changeFioOrderDate":
                    return obj.getChangeFioOrderDate();
                case "changeFioOrderNumber":
                    return obj.getChangeFioOrderNumber();
                case "excludeOrderDate":
                    return obj.getExcludeOrderDate();
                case "excludeOrderNumber":
                    return obj.getExcludeOrderNumber();
                case "changeCompensationTypeOrderDate":
                    return obj.getChangeCompensationTypeOrderDate();
                case "changeCompensationTypeOrderNumber":
                    return obj.getChangeCompensationTypeOrderNumber();
                case "graduateDiplomaOrderDate":
                    return obj.getGraduateDiplomaOrderDate();
                case "graduateDiplomaOrderNumber":
                    return obj.getGraduateDiplomaOrderNumber();
                case "graduateSuccessDiplomaOrderDate":
                    return obj.getGraduateSuccessDiplomaOrderDate();
                case "graduateSuccessDiplomaOrderNumber":
                    return obj.getGraduateSuccessDiplomaOrderNumber();
                case "holidaysOrderDate":
                    return obj.getHolidaysOrderDate();
                case "holidaysOrderNumber":
                    return obj.getHolidaysOrderNumber();
                case "holidaysBeginDate":
                    return obj.getHolidaysBeginDate();
                case "holidaysEndDate":
                    return obj.getHolidaysEndDate();
                case "splitToSpecializationOrderDate":
                    return obj.getSplitToSpecializationOrderDate();
                case "splitToSpecializationOrderNumber":
                    return obj.getSplitToSpecializationOrderNumber();
                case "publicOrderOrderDate":
                    return obj.getPublicOrderOrderDate();
                case "publicOrderOrderNumber":
                    return obj.getPublicOrderOrderNumber();
                case "acadGrantAssignmentOrderDate":
                    return obj.getAcadGrantAssignmentOrderDate();
                case "acadGrantAssignmentOrderNumber":
                    return obj.getAcadGrantAssignmentOrderNumber();
                case "acadGrantPaymentDateFrom":
                    return obj.getAcadGrantPaymentDateFrom();
                case "acadGrantPaymentDateTo":
                    return obj.getAcadGrantPaymentDateTo();
                case "socGrantAssignmentOrderDate":
                    return obj.getSocGrantAssignmentOrderDate();
                case "socGrantAssignmentOrderNumber":
                    return obj.getSocGrantAssignmentOrderNumber();
                case "socGrantPaymentDateFrom":
                    return obj.getSocGrantPaymentDateFrom();
                case "socGrantPaymentDateTo":
                    return obj.getSocGrantPaymentDateTo();
                case "acadGrantBonusAssignmentOrderDate":
                    return obj.getAcadGrantBonusAssignmentOrderDate();
                case "acadGrantBonusAssignmentOrderNumber":
                    return obj.getAcadGrantBonusAssignmentOrderNumber();
                case "acadGrantBonusPaymentDateFrom":
                    return obj.getAcadGrantBonusPaymentDateFrom();
                case "acadGrantBonusPaymentDateTo":
                    return obj.getAcadGrantBonusPaymentDateTo();
                case "sendPracticeOrderDate":
                    return obj.getSendPracticeOrderDate();
                case "sendPracticeOrderNumber":
                    return obj.getSendPracticeOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "eduEnrollmentOrderDate":
                    obj.setEduEnrollmentOrderDate((Date) value);
                    return;
                case "eduEnrollmentOrderNumber":
                    obj.setEduEnrollmentOrderNumber((String) value);
                    return;
                case "eduEnrollmentOrderEnrDate":
                    obj.setEduEnrollmentOrderEnrDate((Date) value);
                    return;
                case "transferOrderDate":
                    obj.setTransferOrderDate((Date) value);
                    return;
                case "transferOrderNumber":
                    obj.setTransferOrderNumber((String) value);
                    return;
                case "sessionProlongOrderDate":
                    obj.setSessionProlongOrderDate((Date) value);
                    return;
                case "sessionProlongOrderNumber":
                    obj.setSessionProlongOrderNumber((String) value);
                    return;
                case "sessionProlongDateTo":
                    obj.setSessionProlongDateTo((Date) value);
                    return;
                case "weekendOrderDate":
                    obj.setWeekendOrderDate((Date) value);
                    return;
                case "weekendOrderNumber":
                    obj.setWeekendOrderNumber((String) value);
                    return;
                case "weekendDateFrom":
                    obj.setWeekendDateFrom((Date) value);
                    return;
                case "weekendDateTo":
                    obj.setWeekendDateTo((Date) value);
                    return;
                case "weekendOutOrderDate":
                    obj.setWeekendOutOrderDate((Date) value);
                    return;
                case "weekendOutOrderNumber":
                    obj.setWeekendOutOrderNumber((String) value);
                    return;
                case "weekendPregnancyOrderDate":
                    obj.setWeekendPregnancyOrderDate((Date) value);
                    return;
                case "weekendPregnancyOrderNumber":
                    obj.setWeekendPregnancyOrderNumber((String) value);
                    return;
                case "weekendPregnancyDateFrom":
                    obj.setWeekendPregnancyDateFrom((Date) value);
                    return;
                case "weekendPregnancyDateTo":
                    obj.setWeekendPregnancyDateTo((Date) value);
                    return;
                case "weekendPregnancyOutOrderDate":
                    obj.setWeekendPregnancyOutOrderDate((Date) value);
                    return;
                case "weekendPregnancyOutOrderNumber":
                    obj.setWeekendPregnancyOutOrderNumber((String) value);
                    return;
                case "weekendChildOrderDate":
                    obj.setWeekendChildOrderDate((Date) value);
                    return;
                case "weekendChildOrderNumber":
                    obj.setWeekendChildOrderNumber((String) value);
                    return;
                case "weekendChildDateFrom":
                    obj.setWeekendChildDateFrom((Date) value);
                    return;
                case "weekendChildDateTo":
                    obj.setWeekendChildDateTo((Date) value);
                    return;
                case "weekendChildOutOrderDate":
                    obj.setWeekendChildOutOrderDate((Date) value);
                    return;
                case "weekendChildOutOrderNumber":
                    obj.setWeekendChildOutOrderNumber((String) value);
                    return;
                case "restorationOrderDate":
                    obj.setRestorationOrderDate((Date) value);
                    return;
                case "restorationOrderNumber":
                    obj.setRestorationOrderNumber((String) value);
                    return;
                case "changeFioOrderDate":
                    obj.setChangeFioOrderDate((Date) value);
                    return;
                case "changeFioOrderNumber":
                    obj.setChangeFioOrderNumber((String) value);
                    return;
                case "excludeOrderDate":
                    obj.setExcludeOrderDate((Date) value);
                    return;
                case "excludeOrderNumber":
                    obj.setExcludeOrderNumber((String) value);
                    return;
                case "changeCompensationTypeOrderDate":
                    obj.setChangeCompensationTypeOrderDate((Date) value);
                    return;
                case "changeCompensationTypeOrderNumber":
                    obj.setChangeCompensationTypeOrderNumber((String) value);
                    return;
                case "graduateDiplomaOrderDate":
                    obj.setGraduateDiplomaOrderDate((Date) value);
                    return;
                case "graduateDiplomaOrderNumber":
                    obj.setGraduateDiplomaOrderNumber((String) value);
                    return;
                case "graduateSuccessDiplomaOrderDate":
                    obj.setGraduateSuccessDiplomaOrderDate((Date) value);
                    return;
                case "graduateSuccessDiplomaOrderNumber":
                    obj.setGraduateSuccessDiplomaOrderNumber((String) value);
                    return;
                case "holidaysOrderDate":
                    obj.setHolidaysOrderDate((Date) value);
                    return;
                case "holidaysOrderNumber":
                    obj.setHolidaysOrderNumber((String) value);
                    return;
                case "holidaysBeginDate":
                    obj.setHolidaysBeginDate((Date) value);
                    return;
                case "holidaysEndDate":
                    obj.setHolidaysEndDate((Date) value);
                    return;
                case "splitToSpecializationOrderDate":
                    obj.setSplitToSpecializationOrderDate((Date) value);
                    return;
                case "splitToSpecializationOrderNumber":
                    obj.setSplitToSpecializationOrderNumber((String) value);
                    return;
                case "publicOrderOrderDate":
                    obj.setPublicOrderOrderDate((Date) value);
                    return;
                case "publicOrderOrderNumber":
                    obj.setPublicOrderOrderNumber((String) value);
                    return;
                case "acadGrantAssignmentOrderDate":
                    obj.setAcadGrantAssignmentOrderDate((Date) value);
                    return;
                case "acadGrantAssignmentOrderNumber":
                    obj.setAcadGrantAssignmentOrderNumber((String) value);
                    return;
                case "acadGrantPaymentDateFrom":
                    obj.setAcadGrantPaymentDateFrom((Date) value);
                    return;
                case "acadGrantPaymentDateTo":
                    obj.setAcadGrantPaymentDateTo((Date) value);
                    return;
                case "socGrantAssignmentOrderDate":
                    obj.setSocGrantAssignmentOrderDate((Date) value);
                    return;
                case "socGrantAssignmentOrderNumber":
                    obj.setSocGrantAssignmentOrderNumber((String) value);
                    return;
                case "socGrantPaymentDateFrom":
                    obj.setSocGrantPaymentDateFrom((Date) value);
                    return;
                case "socGrantPaymentDateTo":
                    obj.setSocGrantPaymentDateTo((Date) value);
                    return;
                case "acadGrantBonusAssignmentOrderDate":
                    obj.setAcadGrantBonusAssignmentOrderDate((Date) value);
                    return;
                case "acadGrantBonusAssignmentOrderNumber":
                    obj.setAcadGrantBonusAssignmentOrderNumber((String) value);
                    return;
                case "acadGrantBonusPaymentDateFrom":
                    obj.setAcadGrantBonusPaymentDateFrom((Date) value);
                    return;
                case "acadGrantBonusPaymentDateTo":
                    obj.setAcadGrantBonusPaymentDateTo((Date) value);
                    return;
                case "sendPracticeOrderDate":
                    obj.setSendPracticeOrderDate((Date) value);
                    return;
                case "sendPracticeOrderNumber":
                    obj.setSendPracticeOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "eduEnrollmentOrderDate":
                        return true;
                case "eduEnrollmentOrderNumber":
                        return true;
                case "eduEnrollmentOrderEnrDate":
                        return true;
                case "transferOrderDate":
                        return true;
                case "transferOrderNumber":
                        return true;
                case "sessionProlongOrderDate":
                        return true;
                case "sessionProlongOrderNumber":
                        return true;
                case "sessionProlongDateTo":
                        return true;
                case "weekendOrderDate":
                        return true;
                case "weekendOrderNumber":
                        return true;
                case "weekendDateFrom":
                        return true;
                case "weekendDateTo":
                        return true;
                case "weekendOutOrderDate":
                        return true;
                case "weekendOutOrderNumber":
                        return true;
                case "weekendPregnancyOrderDate":
                        return true;
                case "weekendPregnancyOrderNumber":
                        return true;
                case "weekendPregnancyDateFrom":
                        return true;
                case "weekendPregnancyDateTo":
                        return true;
                case "weekendPregnancyOutOrderDate":
                        return true;
                case "weekendPregnancyOutOrderNumber":
                        return true;
                case "weekendChildOrderDate":
                        return true;
                case "weekendChildOrderNumber":
                        return true;
                case "weekendChildDateFrom":
                        return true;
                case "weekendChildDateTo":
                        return true;
                case "weekendChildOutOrderDate":
                        return true;
                case "weekendChildOutOrderNumber":
                        return true;
                case "restorationOrderDate":
                        return true;
                case "restorationOrderNumber":
                        return true;
                case "changeFioOrderDate":
                        return true;
                case "changeFioOrderNumber":
                        return true;
                case "excludeOrderDate":
                        return true;
                case "excludeOrderNumber":
                        return true;
                case "changeCompensationTypeOrderDate":
                        return true;
                case "changeCompensationTypeOrderNumber":
                        return true;
                case "graduateDiplomaOrderDate":
                        return true;
                case "graduateDiplomaOrderNumber":
                        return true;
                case "graduateSuccessDiplomaOrderDate":
                        return true;
                case "graduateSuccessDiplomaOrderNumber":
                        return true;
                case "holidaysOrderDate":
                        return true;
                case "holidaysOrderNumber":
                        return true;
                case "holidaysBeginDate":
                        return true;
                case "holidaysEndDate":
                        return true;
                case "splitToSpecializationOrderDate":
                        return true;
                case "splitToSpecializationOrderNumber":
                        return true;
                case "publicOrderOrderDate":
                        return true;
                case "publicOrderOrderNumber":
                        return true;
                case "acadGrantAssignmentOrderDate":
                        return true;
                case "acadGrantAssignmentOrderNumber":
                        return true;
                case "acadGrantPaymentDateFrom":
                        return true;
                case "acadGrantPaymentDateTo":
                        return true;
                case "socGrantAssignmentOrderDate":
                        return true;
                case "socGrantAssignmentOrderNumber":
                        return true;
                case "socGrantPaymentDateFrom":
                        return true;
                case "socGrantPaymentDateTo":
                        return true;
                case "acadGrantBonusAssignmentOrderDate":
                        return true;
                case "acadGrantBonusAssignmentOrderNumber":
                        return true;
                case "acadGrantBonusPaymentDateFrom":
                        return true;
                case "acadGrantBonusPaymentDateTo":
                        return true;
                case "sendPracticeOrderDate":
                        return true;
                case "sendPracticeOrderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "eduEnrollmentOrderDate":
                    return true;
                case "eduEnrollmentOrderNumber":
                    return true;
                case "eduEnrollmentOrderEnrDate":
                    return true;
                case "transferOrderDate":
                    return true;
                case "transferOrderNumber":
                    return true;
                case "sessionProlongOrderDate":
                    return true;
                case "sessionProlongOrderNumber":
                    return true;
                case "sessionProlongDateTo":
                    return true;
                case "weekendOrderDate":
                    return true;
                case "weekendOrderNumber":
                    return true;
                case "weekendDateFrom":
                    return true;
                case "weekendDateTo":
                    return true;
                case "weekendOutOrderDate":
                    return true;
                case "weekendOutOrderNumber":
                    return true;
                case "weekendPregnancyOrderDate":
                    return true;
                case "weekendPregnancyOrderNumber":
                    return true;
                case "weekendPregnancyDateFrom":
                    return true;
                case "weekendPregnancyDateTo":
                    return true;
                case "weekendPregnancyOutOrderDate":
                    return true;
                case "weekendPregnancyOutOrderNumber":
                    return true;
                case "weekendChildOrderDate":
                    return true;
                case "weekendChildOrderNumber":
                    return true;
                case "weekendChildDateFrom":
                    return true;
                case "weekendChildDateTo":
                    return true;
                case "weekendChildOutOrderDate":
                    return true;
                case "weekendChildOutOrderNumber":
                    return true;
                case "restorationOrderDate":
                    return true;
                case "restorationOrderNumber":
                    return true;
                case "changeFioOrderDate":
                    return true;
                case "changeFioOrderNumber":
                    return true;
                case "excludeOrderDate":
                    return true;
                case "excludeOrderNumber":
                    return true;
                case "changeCompensationTypeOrderDate":
                    return true;
                case "changeCompensationTypeOrderNumber":
                    return true;
                case "graduateDiplomaOrderDate":
                    return true;
                case "graduateDiplomaOrderNumber":
                    return true;
                case "graduateSuccessDiplomaOrderDate":
                    return true;
                case "graduateSuccessDiplomaOrderNumber":
                    return true;
                case "holidaysOrderDate":
                    return true;
                case "holidaysOrderNumber":
                    return true;
                case "holidaysBeginDate":
                    return true;
                case "holidaysEndDate":
                    return true;
                case "splitToSpecializationOrderDate":
                    return true;
                case "splitToSpecializationOrderNumber":
                    return true;
                case "publicOrderOrderDate":
                    return true;
                case "publicOrderOrderNumber":
                    return true;
                case "acadGrantAssignmentOrderDate":
                    return true;
                case "acadGrantAssignmentOrderNumber":
                    return true;
                case "acadGrantPaymentDateFrom":
                    return true;
                case "acadGrantPaymentDateTo":
                    return true;
                case "socGrantAssignmentOrderDate":
                    return true;
                case "socGrantAssignmentOrderNumber":
                    return true;
                case "socGrantPaymentDateFrom":
                    return true;
                case "socGrantPaymentDateTo":
                    return true;
                case "acadGrantBonusAssignmentOrderDate":
                    return true;
                case "acadGrantBonusAssignmentOrderNumber":
                    return true;
                case "acadGrantBonusPaymentDateFrom":
                    return true;
                case "acadGrantBonusPaymentDateTo":
                    return true;
                case "sendPracticeOrderDate":
                    return true;
                case "sendPracticeOrderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "eduEnrollmentOrderDate":
                    return Date.class;
                case "eduEnrollmentOrderNumber":
                    return String.class;
                case "eduEnrollmentOrderEnrDate":
                    return Date.class;
                case "transferOrderDate":
                    return Date.class;
                case "transferOrderNumber":
                    return String.class;
                case "sessionProlongOrderDate":
                    return Date.class;
                case "sessionProlongOrderNumber":
                    return String.class;
                case "sessionProlongDateTo":
                    return Date.class;
                case "weekendOrderDate":
                    return Date.class;
                case "weekendOrderNumber":
                    return String.class;
                case "weekendDateFrom":
                    return Date.class;
                case "weekendDateTo":
                    return Date.class;
                case "weekendOutOrderDate":
                    return Date.class;
                case "weekendOutOrderNumber":
                    return String.class;
                case "weekendPregnancyOrderDate":
                    return Date.class;
                case "weekendPregnancyOrderNumber":
                    return String.class;
                case "weekendPregnancyDateFrom":
                    return Date.class;
                case "weekendPregnancyDateTo":
                    return Date.class;
                case "weekendPregnancyOutOrderDate":
                    return Date.class;
                case "weekendPregnancyOutOrderNumber":
                    return String.class;
                case "weekendChildOrderDate":
                    return Date.class;
                case "weekendChildOrderNumber":
                    return String.class;
                case "weekendChildDateFrom":
                    return Date.class;
                case "weekendChildDateTo":
                    return Date.class;
                case "weekendChildOutOrderDate":
                    return Date.class;
                case "weekendChildOutOrderNumber":
                    return String.class;
                case "restorationOrderDate":
                    return Date.class;
                case "restorationOrderNumber":
                    return String.class;
                case "changeFioOrderDate":
                    return Date.class;
                case "changeFioOrderNumber":
                    return String.class;
                case "excludeOrderDate":
                    return Date.class;
                case "excludeOrderNumber":
                    return String.class;
                case "changeCompensationTypeOrderDate":
                    return Date.class;
                case "changeCompensationTypeOrderNumber":
                    return String.class;
                case "graduateDiplomaOrderDate":
                    return Date.class;
                case "graduateDiplomaOrderNumber":
                    return String.class;
                case "graduateSuccessDiplomaOrderDate":
                    return Date.class;
                case "graduateSuccessDiplomaOrderNumber":
                    return String.class;
                case "holidaysOrderDate":
                    return Date.class;
                case "holidaysOrderNumber":
                    return String.class;
                case "holidaysBeginDate":
                    return Date.class;
                case "holidaysEndDate":
                    return Date.class;
                case "splitToSpecializationOrderDate":
                    return Date.class;
                case "splitToSpecializationOrderNumber":
                    return String.class;
                case "publicOrderOrderDate":
                    return Date.class;
                case "publicOrderOrderNumber":
                    return String.class;
                case "acadGrantAssignmentOrderDate":
                    return Date.class;
                case "acadGrantAssignmentOrderNumber":
                    return String.class;
                case "acadGrantPaymentDateFrom":
                    return Date.class;
                case "acadGrantPaymentDateTo":
                    return Date.class;
                case "socGrantAssignmentOrderDate":
                    return Date.class;
                case "socGrantAssignmentOrderNumber":
                    return String.class;
                case "socGrantPaymentDateFrom":
                    return Date.class;
                case "socGrantPaymentDateTo":
                    return Date.class;
                case "acadGrantBonusAssignmentOrderDate":
                    return Date.class;
                case "acadGrantBonusAssignmentOrderNumber":
                    return String.class;
                case "acadGrantBonusPaymentDateFrom":
                    return Date.class;
                case "acadGrantBonusPaymentDateTo":
                    return Date.class;
                case "sendPracticeOrderDate":
                    return Date.class;
                case "sendPracticeOrderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrderData> _dslPath = new Path<OrderData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrderData");
    }
            

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Приказ о зачислении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getEduEnrollmentOrderDate()
     */
    public static PropertyPath<Date> eduEnrollmentOrderDate()
    {
        return _dslPath.eduEnrollmentOrderDate();
    }

    /**
     * @return Приказ о зачислении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getEduEnrollmentOrderNumber()
     */
    public static PropertyPath<String> eduEnrollmentOrderNumber()
    {
        return _dslPath.eduEnrollmentOrderNumber();
    }

    /**
     * @return Приказ о зачислении - зачислен с.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getEduEnrollmentOrderEnrDate()
     */
    public static PropertyPath<Date> eduEnrollmentOrderEnrDate()
    {
        return _dslPath.eduEnrollmentOrderEnrDate();
    }

    /**
     * @return Приказ о переводе - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getTransferOrderDate()
     */
    public static PropertyPath<Date> transferOrderDate()
    {
        return _dslPath.transferOrderDate();
    }

    /**
     * @return Приказ о переводе - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getTransferOrderNumber()
     */
    public static PropertyPath<String> transferOrderNumber()
    {
        return _dslPath.transferOrderNumber();
    }

    /**
     * @return Приказ о продлении экзаменационной сессии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSessionProlongOrderDate()
     */
    public static PropertyPath<Date> sessionProlongOrderDate()
    {
        return _dslPath.sessionProlongOrderDate();
    }

    /**
     * @return Приказ о продлении экзаменационной сессии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSessionProlongOrderNumber()
     */
    public static PropertyPath<String> sessionProlongOrderNumber()
    {
        return _dslPath.sessionProlongOrderNumber();
    }

    /**
     * @return Сессия продлена до.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSessionProlongDateTo()
     */
    public static PropertyPath<Date> sessionProlongDateTo()
    {
        return _dslPath.sessionProlongDateTo();
    }

    /**
     * @return Приказ о предоставлении академического отпуска - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOrderDate()
     */
    public static PropertyPath<Date> weekendOrderDate()
    {
        return _dslPath.weekendOrderDate();
    }

    /**
     * @return Приказ о предоставлении академического отпуска - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOrderNumber()
     */
    public static PropertyPath<String> weekendOrderNumber()
    {
        return _dslPath.weekendOrderNumber();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendDateFrom()
     */
    public static PropertyPath<Date> weekendDateFrom()
    {
        return _dslPath.weekendDateFrom();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendDateTo()
     */
    public static PropertyPath<Date> weekendDateTo()
    {
        return _dslPath.weekendDateTo();
    }

    /**
     * @return Приказ о выходе из академического отпуска - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOutOrderDate()
     */
    public static PropertyPath<Date> weekendOutOrderDate()
    {
        return _dslPath.weekendOutOrderDate();
    }

    /**
     * @return Приказ о выходе из академического отпуска - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOutOrderNumber()
     */
    public static PropertyPath<String> weekendOutOrderNumber()
    {
        return _dslPath.weekendOutOrderNumber();
    }

    /**
     * @return Приказ о предоставлении отпуска по беременности и родам - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOrderDate()
     */
    public static PropertyPath<Date> weekendPregnancyOrderDate()
    {
        return _dslPath.weekendPregnancyOrderDate();
    }

    /**
     * @return Приказ о предоставлении отпуска по беременности и родам - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOrderNumber()
     */
    public static PropertyPath<String> weekendPregnancyOrderNumber()
    {
        return _dslPath.weekendPregnancyOrderNumber();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyDateFrom()
     */
    public static PropertyPath<Date> weekendPregnancyDateFrom()
    {
        return _dslPath.weekendPregnancyDateFrom();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyDateTo()
     */
    public static PropertyPath<Date> weekendPregnancyDateTo()
    {
        return _dslPath.weekendPregnancyDateTo();
    }

    /**
     * @return Приказ о выходе из отпуска по беременности и родам - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOutOrderDate()
     */
    public static PropertyPath<Date> weekendPregnancyOutOrderDate()
    {
        return _dslPath.weekendPregnancyOutOrderDate();
    }

    /**
     * @return Приказ о выходе из отпуска по беременности и родам - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOutOrderNumber()
     */
    public static PropertyPath<String> weekendPregnancyOutOrderNumber()
    {
        return _dslPath.weekendPregnancyOutOrderNumber();
    }

    /**
     * @return Приказ о предоставлении отпуска по уходу за ребенком - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOrderDate()
     */
    public static PropertyPath<Date> weekendChildOrderDate()
    {
        return _dslPath.weekendChildOrderDate();
    }

    /**
     * @return Приказ о предоставлении отпуска по уходу за ребенком - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOrderNumber()
     */
    public static PropertyPath<String> weekendChildOrderNumber()
    {
        return _dslPath.weekendChildOrderNumber();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildDateFrom()
     */
    public static PropertyPath<Date> weekendChildDateFrom()
    {
        return _dslPath.weekendChildDateFrom();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildDateTo()
     */
    public static PropertyPath<Date> weekendChildDateTo()
    {
        return _dslPath.weekendChildDateTo();
    }

    /**
     * @return Приказ о выходе из отпуска по уходу за ребенком - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOutOrderDate()
     */
    public static PropertyPath<Date> weekendChildOutOrderDate()
    {
        return _dslPath.weekendChildOutOrderDate();
    }

    /**
     * @return Приказ о выходе из отпуска по уходу за ребенком - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOutOrderNumber()
     */
    public static PropertyPath<String> weekendChildOutOrderNumber()
    {
        return _dslPath.weekendChildOutOrderNumber();
    }

    /**
     * @return Приказ о восстановлении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getRestorationOrderDate()
     */
    public static PropertyPath<Date> restorationOrderDate()
    {
        return _dslPath.restorationOrderDate();
    }

    /**
     * @return Приказ о восстановлении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getRestorationOrderNumber()
     */
    public static PropertyPath<String> restorationOrderNumber()
    {
        return _dslPath.restorationOrderNumber();
    }

    /**
     * @return Приказ о смене фамилии (имени, отчества) - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeFioOrderDate()
     */
    public static PropertyPath<Date> changeFioOrderDate()
    {
        return _dslPath.changeFioOrderDate();
    }

    /**
     * @return Приказ о смене фамилии (имени, отчества) - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeFioOrderNumber()
     */
    public static PropertyPath<String> changeFioOrderNumber()
    {
        return _dslPath.changeFioOrderNumber();
    }

    /**
     * @return Приказ об отчислении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getExcludeOrderDate()
     */
    public static PropertyPath<Date> excludeOrderDate()
    {
        return _dslPath.excludeOrderDate();
    }

    /**
     * @return Приказ об отчислении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getExcludeOrderNumber()
     */
    public static PropertyPath<String> excludeOrderNumber()
    {
        return _dslPath.excludeOrderNumber();
    }

    /**
     * @return Приказ об изменении основы оплаты обучения - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeCompensationTypeOrderDate()
     */
    public static PropertyPath<Date> changeCompensationTypeOrderDate()
    {
        return _dslPath.changeCompensationTypeOrderDate();
    }

    /**
     * @return Приказ об изменении основы оплаты обучения - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeCompensationTypeOrderNumber()
     */
    public static PropertyPath<String> changeCompensationTypeOrderNumber()
    {
        return _dslPath.changeCompensationTypeOrderNumber();
    }

    /**
     * @return Приказ о выпуске (диплом) - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateDiplomaOrderDate()
     */
    public static PropertyPath<Date> graduateDiplomaOrderDate()
    {
        return _dslPath.graduateDiplomaOrderDate();
    }

    /**
     * @return Приказ о выпуске (диплом) - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateDiplomaOrderNumber()
     */
    public static PropertyPath<String> graduateDiplomaOrderNumber()
    {
        return _dslPath.graduateDiplomaOrderNumber();
    }

    /**
     * @return Приказ о выпуске (диплом с отличием) - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateSuccessDiplomaOrderDate()
     */
    public static PropertyPath<Date> graduateSuccessDiplomaOrderDate()
    {
        return _dslPath.graduateSuccessDiplomaOrderDate();
    }

    /**
     * @return Приказ о выпуске (диплом с отличием) - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateSuccessDiplomaOrderNumber()
     */
    public static PropertyPath<String> graduateSuccessDiplomaOrderNumber()
    {
        return _dslPath.graduateSuccessDiplomaOrderNumber();
    }

    /**
     * @return Приказ о предоставлении каникул - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysOrderDate()
     */
    public static PropertyPath<Date> holidaysOrderDate()
    {
        return _dslPath.holidaysOrderDate();
    }

    /**
     * @return Приказ о предоставлении каникул - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysOrderNumber()
     */
    public static PropertyPath<String> holidaysOrderNumber()
    {
        return _dslPath.holidaysOrderNumber();
    }

    /**
     * @return Приказ о предоставлении каникул - дата начала.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysBeginDate()
     */
    public static PropertyPath<Date> holidaysBeginDate()
    {
        return _dslPath.holidaysBeginDate();
    }

    /**
     * @return Приказ о предоставлении каникул - дата окончания.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysEndDate()
     */
    public static PropertyPath<Date> holidaysEndDate()
    {
        return _dslPath.holidaysEndDate();
    }

    /**
     * @return Приказ о распределении по профилям/специализациям - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSplitToSpecializationOrderDate()
     */
    public static PropertyPath<Date> splitToSpecializationOrderDate()
    {
        return _dslPath.splitToSpecializationOrderDate();
    }

    /**
     * @return Приказ о распределении по профилям/специализациям - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSplitToSpecializationOrderNumber()
     */
    public static PropertyPath<String> splitToSpecializationOrderNumber()
    {
        return _dslPath.splitToSpecializationOrderNumber();
    }

    /**
     * @return Приказ об общественном поручении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getPublicOrderOrderDate()
     */
    public static PropertyPath<Date> publicOrderOrderDate()
    {
        return _dslPath.publicOrderOrderDate();
    }

    /**
     * @return Приказ об общественном поручении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getPublicOrderOrderNumber()
     */
    public static PropertyPath<String> publicOrderOrderNumber()
    {
        return _dslPath.publicOrderOrderNumber();
    }

    /**
     * @return Приказ о назначении академической стипендии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantAssignmentOrderDate()
     */
    public static PropertyPath<Date> acadGrantAssignmentOrderDate()
    {
        return _dslPath.acadGrantAssignmentOrderDate();
    }

    /**
     * @return Приказ о назначении академической стипендии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantAssignmentOrderNumber()
     */
    public static PropertyPath<String> acadGrantAssignmentOrderNumber()
    {
        return _dslPath.acadGrantAssignmentOrderNumber();
    }

    /**
     * @return Дата начала выплаты академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantPaymentDateFrom()
     */
    public static PropertyPath<Date> acadGrantPaymentDateFrom()
    {
        return _dslPath.acadGrantPaymentDateFrom();
    }

    /**
     * @return Дата окончания выплаты академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantPaymentDateTo()
     */
    public static PropertyPath<Date> acadGrantPaymentDateTo()
    {
        return _dslPath.acadGrantPaymentDateTo();
    }

    /**
     * @return Приказ о назначении социальной стипендии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantAssignmentOrderDate()
     */
    public static PropertyPath<Date> socGrantAssignmentOrderDate()
    {
        return _dslPath.socGrantAssignmentOrderDate();
    }

    /**
     * @return Приказ о назначении социальной стипендии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantAssignmentOrderNumber()
     */
    public static PropertyPath<String> socGrantAssignmentOrderNumber()
    {
        return _dslPath.socGrantAssignmentOrderNumber();
    }

    /**
     * @return Дата начала выплаты социальной стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantPaymentDateFrom()
     */
    public static PropertyPath<Date> socGrantPaymentDateFrom()
    {
        return _dslPath.socGrantPaymentDateFrom();
    }

    /**
     * @return Дата окончания выплаты социальной стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantPaymentDateTo()
     */
    public static PropertyPath<Date> socGrantPaymentDateTo()
    {
        return _dslPath.socGrantPaymentDateTo();
    }

    /**
     * @return Приказ о назначении надбавки к академической стипендии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusAssignmentOrderDate()
     */
    public static PropertyPath<Date> acadGrantBonusAssignmentOrderDate()
    {
        return _dslPath.acadGrantBonusAssignmentOrderDate();
    }

    /**
     * @return Приказ о назначении надбавки к академической стипендии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusAssignmentOrderNumber()
     */
    public static PropertyPath<String> acadGrantBonusAssignmentOrderNumber()
    {
        return _dslPath.acadGrantBonusAssignmentOrderNumber();
    }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusPaymentDateFrom()
     */
    public static PropertyPath<Date> acadGrantBonusPaymentDateFrom()
    {
        return _dslPath.acadGrantBonusPaymentDateFrom();
    }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusPaymentDateTo()
     */
    public static PropertyPath<Date> acadGrantBonusPaymentDateTo()
    {
        return _dslPath.acadGrantBonusPaymentDateTo();
    }

    /**
     * @return Приказ о направлении на практику - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSendPracticeOrderDate()
     */
    public static PropertyPath<Date> sendPracticeOrderDate()
    {
        return _dslPath.sendPracticeOrderDate();
    }

    /**
     * @return Приказ о направлении на практику - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSendPracticeOrderNumber()
     */
    public static PropertyPath<String> sendPracticeOrderNumber()
    {
        return _dslPath.sendPracticeOrderNumber();
    }

    public static class Path<E extends OrderData> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<Date> _eduEnrollmentOrderDate;
        private PropertyPath<String> _eduEnrollmentOrderNumber;
        private PropertyPath<Date> _eduEnrollmentOrderEnrDate;
        private PropertyPath<Date> _transferOrderDate;
        private PropertyPath<String> _transferOrderNumber;
        private PropertyPath<Date> _sessionProlongOrderDate;
        private PropertyPath<String> _sessionProlongOrderNumber;
        private PropertyPath<Date> _sessionProlongDateTo;
        private PropertyPath<Date> _weekendOrderDate;
        private PropertyPath<String> _weekendOrderNumber;
        private PropertyPath<Date> _weekendDateFrom;
        private PropertyPath<Date> _weekendDateTo;
        private PropertyPath<Date> _weekendOutOrderDate;
        private PropertyPath<String> _weekendOutOrderNumber;
        private PropertyPath<Date> _weekendPregnancyOrderDate;
        private PropertyPath<String> _weekendPregnancyOrderNumber;
        private PropertyPath<Date> _weekendPregnancyDateFrom;
        private PropertyPath<Date> _weekendPregnancyDateTo;
        private PropertyPath<Date> _weekendPregnancyOutOrderDate;
        private PropertyPath<String> _weekendPregnancyOutOrderNumber;
        private PropertyPath<Date> _weekendChildOrderDate;
        private PropertyPath<String> _weekendChildOrderNumber;
        private PropertyPath<Date> _weekendChildDateFrom;
        private PropertyPath<Date> _weekendChildDateTo;
        private PropertyPath<Date> _weekendChildOutOrderDate;
        private PropertyPath<String> _weekendChildOutOrderNumber;
        private PropertyPath<Date> _restorationOrderDate;
        private PropertyPath<String> _restorationOrderNumber;
        private PropertyPath<Date> _changeFioOrderDate;
        private PropertyPath<String> _changeFioOrderNumber;
        private PropertyPath<Date> _excludeOrderDate;
        private PropertyPath<String> _excludeOrderNumber;
        private PropertyPath<Date> _changeCompensationTypeOrderDate;
        private PropertyPath<String> _changeCompensationTypeOrderNumber;
        private PropertyPath<Date> _graduateDiplomaOrderDate;
        private PropertyPath<String> _graduateDiplomaOrderNumber;
        private PropertyPath<Date> _graduateSuccessDiplomaOrderDate;
        private PropertyPath<String> _graduateSuccessDiplomaOrderNumber;
        private PropertyPath<Date> _holidaysOrderDate;
        private PropertyPath<String> _holidaysOrderNumber;
        private PropertyPath<Date> _holidaysBeginDate;
        private PropertyPath<Date> _holidaysEndDate;
        private PropertyPath<Date> _splitToSpecializationOrderDate;
        private PropertyPath<String> _splitToSpecializationOrderNumber;
        private PropertyPath<Date> _publicOrderOrderDate;
        private PropertyPath<String> _publicOrderOrderNumber;
        private PropertyPath<Date> _acadGrantAssignmentOrderDate;
        private PropertyPath<String> _acadGrantAssignmentOrderNumber;
        private PropertyPath<Date> _acadGrantPaymentDateFrom;
        private PropertyPath<Date> _acadGrantPaymentDateTo;
        private PropertyPath<Date> _socGrantAssignmentOrderDate;
        private PropertyPath<String> _socGrantAssignmentOrderNumber;
        private PropertyPath<Date> _socGrantPaymentDateFrom;
        private PropertyPath<Date> _socGrantPaymentDateTo;
        private PropertyPath<Date> _acadGrantBonusAssignmentOrderDate;
        private PropertyPath<String> _acadGrantBonusAssignmentOrderNumber;
        private PropertyPath<Date> _acadGrantBonusPaymentDateFrom;
        private PropertyPath<Date> _acadGrantBonusPaymentDateTo;
        private PropertyPath<Date> _sendPracticeOrderDate;
        private PropertyPath<String> _sendPracticeOrderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Приказ о зачислении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getEduEnrollmentOrderDate()
     */
        public PropertyPath<Date> eduEnrollmentOrderDate()
        {
            if(_eduEnrollmentOrderDate == null )
                _eduEnrollmentOrderDate = new PropertyPath<Date>(OrderDataGen.P_EDU_ENROLLMENT_ORDER_DATE, this);
            return _eduEnrollmentOrderDate;
        }

    /**
     * @return Приказ о зачислении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getEduEnrollmentOrderNumber()
     */
        public PropertyPath<String> eduEnrollmentOrderNumber()
        {
            if(_eduEnrollmentOrderNumber == null )
                _eduEnrollmentOrderNumber = new PropertyPath<String>(OrderDataGen.P_EDU_ENROLLMENT_ORDER_NUMBER, this);
            return _eduEnrollmentOrderNumber;
        }

    /**
     * @return Приказ о зачислении - зачислен с.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getEduEnrollmentOrderEnrDate()
     */
        public PropertyPath<Date> eduEnrollmentOrderEnrDate()
        {
            if(_eduEnrollmentOrderEnrDate == null )
                _eduEnrollmentOrderEnrDate = new PropertyPath<Date>(OrderDataGen.P_EDU_ENROLLMENT_ORDER_ENR_DATE, this);
            return _eduEnrollmentOrderEnrDate;
        }

    /**
     * @return Приказ о переводе - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getTransferOrderDate()
     */
        public PropertyPath<Date> transferOrderDate()
        {
            if(_transferOrderDate == null )
                _transferOrderDate = new PropertyPath<Date>(OrderDataGen.P_TRANSFER_ORDER_DATE, this);
            return _transferOrderDate;
        }

    /**
     * @return Приказ о переводе - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getTransferOrderNumber()
     */
        public PropertyPath<String> transferOrderNumber()
        {
            if(_transferOrderNumber == null )
                _transferOrderNumber = new PropertyPath<String>(OrderDataGen.P_TRANSFER_ORDER_NUMBER, this);
            return _transferOrderNumber;
        }

    /**
     * @return Приказ о продлении экзаменационной сессии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSessionProlongOrderDate()
     */
        public PropertyPath<Date> sessionProlongOrderDate()
        {
            if(_sessionProlongOrderDate == null )
                _sessionProlongOrderDate = new PropertyPath<Date>(OrderDataGen.P_SESSION_PROLONG_ORDER_DATE, this);
            return _sessionProlongOrderDate;
        }

    /**
     * @return Приказ о продлении экзаменационной сессии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSessionProlongOrderNumber()
     */
        public PropertyPath<String> sessionProlongOrderNumber()
        {
            if(_sessionProlongOrderNumber == null )
                _sessionProlongOrderNumber = new PropertyPath<String>(OrderDataGen.P_SESSION_PROLONG_ORDER_NUMBER, this);
            return _sessionProlongOrderNumber;
        }

    /**
     * @return Сессия продлена до.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSessionProlongDateTo()
     */
        public PropertyPath<Date> sessionProlongDateTo()
        {
            if(_sessionProlongDateTo == null )
                _sessionProlongDateTo = new PropertyPath<Date>(OrderDataGen.P_SESSION_PROLONG_DATE_TO, this);
            return _sessionProlongDateTo;
        }

    /**
     * @return Приказ о предоставлении академического отпуска - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOrderDate()
     */
        public PropertyPath<Date> weekendOrderDate()
        {
            if(_weekendOrderDate == null )
                _weekendOrderDate = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_ORDER_DATE, this);
            return _weekendOrderDate;
        }

    /**
     * @return Приказ о предоставлении академического отпуска - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOrderNumber()
     */
        public PropertyPath<String> weekendOrderNumber()
        {
            if(_weekendOrderNumber == null )
                _weekendOrderNumber = new PropertyPath<String>(OrderDataGen.P_WEEKEND_ORDER_NUMBER, this);
            return _weekendOrderNumber;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendDateFrom()
     */
        public PropertyPath<Date> weekendDateFrom()
        {
            if(_weekendDateFrom == null )
                _weekendDateFrom = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_DATE_FROM, this);
            return _weekendDateFrom;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendDateTo()
     */
        public PropertyPath<Date> weekendDateTo()
        {
            if(_weekendDateTo == null )
                _weekendDateTo = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_DATE_TO, this);
            return _weekendDateTo;
        }

    /**
     * @return Приказ о выходе из академического отпуска - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOutOrderDate()
     */
        public PropertyPath<Date> weekendOutOrderDate()
        {
            if(_weekendOutOrderDate == null )
                _weekendOutOrderDate = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_OUT_ORDER_DATE, this);
            return _weekendOutOrderDate;
        }

    /**
     * @return Приказ о выходе из академического отпуска - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendOutOrderNumber()
     */
        public PropertyPath<String> weekendOutOrderNumber()
        {
            if(_weekendOutOrderNumber == null )
                _weekendOutOrderNumber = new PropertyPath<String>(OrderDataGen.P_WEEKEND_OUT_ORDER_NUMBER, this);
            return _weekendOutOrderNumber;
        }

    /**
     * @return Приказ о предоставлении отпуска по беременности и родам - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOrderDate()
     */
        public PropertyPath<Date> weekendPregnancyOrderDate()
        {
            if(_weekendPregnancyOrderDate == null )
                _weekendPregnancyOrderDate = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_PREGNANCY_ORDER_DATE, this);
            return _weekendPregnancyOrderDate;
        }

    /**
     * @return Приказ о предоставлении отпуска по беременности и родам - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOrderNumber()
     */
        public PropertyPath<String> weekendPregnancyOrderNumber()
        {
            if(_weekendPregnancyOrderNumber == null )
                _weekendPregnancyOrderNumber = new PropertyPath<String>(OrderDataGen.P_WEEKEND_PREGNANCY_ORDER_NUMBER, this);
            return _weekendPregnancyOrderNumber;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyDateFrom()
     */
        public PropertyPath<Date> weekendPregnancyDateFrom()
        {
            if(_weekendPregnancyDateFrom == null )
                _weekendPregnancyDateFrom = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_PREGNANCY_DATE_FROM, this);
            return _weekendPregnancyDateFrom;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyDateTo()
     */
        public PropertyPath<Date> weekendPregnancyDateTo()
        {
            if(_weekendPregnancyDateTo == null )
                _weekendPregnancyDateTo = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_PREGNANCY_DATE_TO, this);
            return _weekendPregnancyDateTo;
        }

    /**
     * @return Приказ о выходе из отпуска по беременности и родам - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOutOrderDate()
     */
        public PropertyPath<Date> weekendPregnancyOutOrderDate()
        {
            if(_weekendPregnancyOutOrderDate == null )
                _weekendPregnancyOutOrderDate = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_PREGNANCY_OUT_ORDER_DATE, this);
            return _weekendPregnancyOutOrderDate;
        }

    /**
     * @return Приказ о выходе из отпуска по беременности и родам - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendPregnancyOutOrderNumber()
     */
        public PropertyPath<String> weekendPregnancyOutOrderNumber()
        {
            if(_weekendPregnancyOutOrderNumber == null )
                _weekendPregnancyOutOrderNumber = new PropertyPath<String>(OrderDataGen.P_WEEKEND_PREGNANCY_OUT_ORDER_NUMBER, this);
            return _weekendPregnancyOutOrderNumber;
        }

    /**
     * @return Приказ о предоставлении отпуска по уходу за ребенком - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOrderDate()
     */
        public PropertyPath<Date> weekendChildOrderDate()
        {
            if(_weekendChildOrderDate == null )
                _weekendChildOrderDate = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_CHILD_ORDER_DATE, this);
            return _weekendChildOrderDate;
        }

    /**
     * @return Приказ о предоставлении отпуска по уходу за ребенком - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOrderNumber()
     */
        public PropertyPath<String> weekendChildOrderNumber()
        {
            if(_weekendChildOrderNumber == null )
                _weekendChildOrderNumber = new PropertyPath<String>(OrderDataGen.P_WEEKEND_CHILD_ORDER_NUMBER, this);
            return _weekendChildOrderNumber;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildDateFrom()
     */
        public PropertyPath<Date> weekendChildDateFrom()
        {
            if(_weekendChildDateFrom == null )
                _weekendChildDateFrom = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_CHILD_DATE_FROM, this);
            return _weekendChildDateFrom;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildDateTo()
     */
        public PropertyPath<Date> weekendChildDateTo()
        {
            if(_weekendChildDateTo == null )
                _weekendChildDateTo = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_CHILD_DATE_TO, this);
            return _weekendChildDateTo;
        }

    /**
     * @return Приказ о выходе из отпуска по уходу за ребенком - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOutOrderDate()
     */
        public PropertyPath<Date> weekendChildOutOrderDate()
        {
            if(_weekendChildOutOrderDate == null )
                _weekendChildOutOrderDate = new PropertyPath<Date>(OrderDataGen.P_WEEKEND_CHILD_OUT_ORDER_DATE, this);
            return _weekendChildOutOrderDate;
        }

    /**
     * @return Приказ о выходе из отпуска по уходу за ребенком - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getWeekendChildOutOrderNumber()
     */
        public PropertyPath<String> weekendChildOutOrderNumber()
        {
            if(_weekendChildOutOrderNumber == null )
                _weekendChildOutOrderNumber = new PropertyPath<String>(OrderDataGen.P_WEEKEND_CHILD_OUT_ORDER_NUMBER, this);
            return _weekendChildOutOrderNumber;
        }

    /**
     * @return Приказ о восстановлении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getRestorationOrderDate()
     */
        public PropertyPath<Date> restorationOrderDate()
        {
            if(_restorationOrderDate == null )
                _restorationOrderDate = new PropertyPath<Date>(OrderDataGen.P_RESTORATION_ORDER_DATE, this);
            return _restorationOrderDate;
        }

    /**
     * @return Приказ о восстановлении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getRestorationOrderNumber()
     */
        public PropertyPath<String> restorationOrderNumber()
        {
            if(_restorationOrderNumber == null )
                _restorationOrderNumber = new PropertyPath<String>(OrderDataGen.P_RESTORATION_ORDER_NUMBER, this);
            return _restorationOrderNumber;
        }

    /**
     * @return Приказ о смене фамилии (имени, отчества) - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeFioOrderDate()
     */
        public PropertyPath<Date> changeFioOrderDate()
        {
            if(_changeFioOrderDate == null )
                _changeFioOrderDate = new PropertyPath<Date>(OrderDataGen.P_CHANGE_FIO_ORDER_DATE, this);
            return _changeFioOrderDate;
        }

    /**
     * @return Приказ о смене фамилии (имени, отчества) - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeFioOrderNumber()
     */
        public PropertyPath<String> changeFioOrderNumber()
        {
            if(_changeFioOrderNumber == null )
                _changeFioOrderNumber = new PropertyPath<String>(OrderDataGen.P_CHANGE_FIO_ORDER_NUMBER, this);
            return _changeFioOrderNumber;
        }

    /**
     * @return Приказ об отчислении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getExcludeOrderDate()
     */
        public PropertyPath<Date> excludeOrderDate()
        {
            if(_excludeOrderDate == null )
                _excludeOrderDate = new PropertyPath<Date>(OrderDataGen.P_EXCLUDE_ORDER_DATE, this);
            return _excludeOrderDate;
        }

    /**
     * @return Приказ об отчислении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getExcludeOrderNumber()
     */
        public PropertyPath<String> excludeOrderNumber()
        {
            if(_excludeOrderNumber == null )
                _excludeOrderNumber = new PropertyPath<String>(OrderDataGen.P_EXCLUDE_ORDER_NUMBER, this);
            return _excludeOrderNumber;
        }

    /**
     * @return Приказ об изменении основы оплаты обучения - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeCompensationTypeOrderDate()
     */
        public PropertyPath<Date> changeCompensationTypeOrderDate()
        {
            if(_changeCompensationTypeOrderDate == null )
                _changeCompensationTypeOrderDate = new PropertyPath<Date>(OrderDataGen.P_CHANGE_COMPENSATION_TYPE_ORDER_DATE, this);
            return _changeCompensationTypeOrderDate;
        }

    /**
     * @return Приказ об изменении основы оплаты обучения - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getChangeCompensationTypeOrderNumber()
     */
        public PropertyPath<String> changeCompensationTypeOrderNumber()
        {
            if(_changeCompensationTypeOrderNumber == null )
                _changeCompensationTypeOrderNumber = new PropertyPath<String>(OrderDataGen.P_CHANGE_COMPENSATION_TYPE_ORDER_NUMBER, this);
            return _changeCompensationTypeOrderNumber;
        }

    /**
     * @return Приказ о выпуске (диплом) - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateDiplomaOrderDate()
     */
        public PropertyPath<Date> graduateDiplomaOrderDate()
        {
            if(_graduateDiplomaOrderDate == null )
                _graduateDiplomaOrderDate = new PropertyPath<Date>(OrderDataGen.P_GRADUATE_DIPLOMA_ORDER_DATE, this);
            return _graduateDiplomaOrderDate;
        }

    /**
     * @return Приказ о выпуске (диплом) - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateDiplomaOrderNumber()
     */
        public PropertyPath<String> graduateDiplomaOrderNumber()
        {
            if(_graduateDiplomaOrderNumber == null )
                _graduateDiplomaOrderNumber = new PropertyPath<String>(OrderDataGen.P_GRADUATE_DIPLOMA_ORDER_NUMBER, this);
            return _graduateDiplomaOrderNumber;
        }

    /**
     * @return Приказ о выпуске (диплом с отличием) - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateSuccessDiplomaOrderDate()
     */
        public PropertyPath<Date> graduateSuccessDiplomaOrderDate()
        {
            if(_graduateSuccessDiplomaOrderDate == null )
                _graduateSuccessDiplomaOrderDate = new PropertyPath<Date>(OrderDataGen.P_GRADUATE_SUCCESS_DIPLOMA_ORDER_DATE, this);
            return _graduateSuccessDiplomaOrderDate;
        }

    /**
     * @return Приказ о выпуске (диплом с отличием) - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getGraduateSuccessDiplomaOrderNumber()
     */
        public PropertyPath<String> graduateSuccessDiplomaOrderNumber()
        {
            if(_graduateSuccessDiplomaOrderNumber == null )
                _graduateSuccessDiplomaOrderNumber = new PropertyPath<String>(OrderDataGen.P_GRADUATE_SUCCESS_DIPLOMA_ORDER_NUMBER, this);
            return _graduateSuccessDiplomaOrderNumber;
        }

    /**
     * @return Приказ о предоставлении каникул - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysOrderDate()
     */
        public PropertyPath<Date> holidaysOrderDate()
        {
            if(_holidaysOrderDate == null )
                _holidaysOrderDate = new PropertyPath<Date>(OrderDataGen.P_HOLIDAYS_ORDER_DATE, this);
            return _holidaysOrderDate;
        }

    /**
     * @return Приказ о предоставлении каникул - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysOrderNumber()
     */
        public PropertyPath<String> holidaysOrderNumber()
        {
            if(_holidaysOrderNumber == null )
                _holidaysOrderNumber = new PropertyPath<String>(OrderDataGen.P_HOLIDAYS_ORDER_NUMBER, this);
            return _holidaysOrderNumber;
        }

    /**
     * @return Приказ о предоставлении каникул - дата начала.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysBeginDate()
     */
        public PropertyPath<Date> holidaysBeginDate()
        {
            if(_holidaysBeginDate == null )
                _holidaysBeginDate = new PropertyPath<Date>(OrderDataGen.P_HOLIDAYS_BEGIN_DATE, this);
            return _holidaysBeginDate;
        }

    /**
     * @return Приказ о предоставлении каникул - дата окончания.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getHolidaysEndDate()
     */
        public PropertyPath<Date> holidaysEndDate()
        {
            if(_holidaysEndDate == null )
                _holidaysEndDate = new PropertyPath<Date>(OrderDataGen.P_HOLIDAYS_END_DATE, this);
            return _holidaysEndDate;
        }

    /**
     * @return Приказ о распределении по профилям/специализациям - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSplitToSpecializationOrderDate()
     */
        public PropertyPath<Date> splitToSpecializationOrderDate()
        {
            if(_splitToSpecializationOrderDate == null )
                _splitToSpecializationOrderDate = new PropertyPath<Date>(OrderDataGen.P_SPLIT_TO_SPECIALIZATION_ORDER_DATE, this);
            return _splitToSpecializationOrderDate;
        }

    /**
     * @return Приказ о распределении по профилям/специализациям - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSplitToSpecializationOrderNumber()
     */
        public PropertyPath<String> splitToSpecializationOrderNumber()
        {
            if(_splitToSpecializationOrderNumber == null )
                _splitToSpecializationOrderNumber = new PropertyPath<String>(OrderDataGen.P_SPLIT_TO_SPECIALIZATION_ORDER_NUMBER, this);
            return _splitToSpecializationOrderNumber;
        }

    /**
     * @return Приказ об общественном поручении - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getPublicOrderOrderDate()
     */
        public PropertyPath<Date> publicOrderOrderDate()
        {
            if(_publicOrderOrderDate == null )
                _publicOrderOrderDate = new PropertyPath<Date>(OrderDataGen.P_PUBLIC_ORDER_ORDER_DATE, this);
            return _publicOrderOrderDate;
        }

    /**
     * @return Приказ об общественном поручении - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getPublicOrderOrderNumber()
     */
        public PropertyPath<String> publicOrderOrderNumber()
        {
            if(_publicOrderOrderNumber == null )
                _publicOrderOrderNumber = new PropertyPath<String>(OrderDataGen.P_PUBLIC_ORDER_ORDER_NUMBER, this);
            return _publicOrderOrderNumber;
        }

    /**
     * @return Приказ о назначении академической стипендии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantAssignmentOrderDate()
     */
        public PropertyPath<Date> acadGrantAssignmentOrderDate()
        {
            if(_acadGrantAssignmentOrderDate == null )
                _acadGrantAssignmentOrderDate = new PropertyPath<Date>(OrderDataGen.P_ACAD_GRANT_ASSIGNMENT_ORDER_DATE, this);
            return _acadGrantAssignmentOrderDate;
        }

    /**
     * @return Приказ о назначении академической стипендии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantAssignmentOrderNumber()
     */
        public PropertyPath<String> acadGrantAssignmentOrderNumber()
        {
            if(_acadGrantAssignmentOrderNumber == null )
                _acadGrantAssignmentOrderNumber = new PropertyPath<String>(OrderDataGen.P_ACAD_GRANT_ASSIGNMENT_ORDER_NUMBER, this);
            return _acadGrantAssignmentOrderNumber;
        }

    /**
     * @return Дата начала выплаты академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantPaymentDateFrom()
     */
        public PropertyPath<Date> acadGrantPaymentDateFrom()
        {
            if(_acadGrantPaymentDateFrom == null )
                _acadGrantPaymentDateFrom = new PropertyPath<Date>(OrderDataGen.P_ACAD_GRANT_PAYMENT_DATE_FROM, this);
            return _acadGrantPaymentDateFrom;
        }

    /**
     * @return Дата окончания выплаты академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantPaymentDateTo()
     */
        public PropertyPath<Date> acadGrantPaymentDateTo()
        {
            if(_acadGrantPaymentDateTo == null )
                _acadGrantPaymentDateTo = new PropertyPath<Date>(OrderDataGen.P_ACAD_GRANT_PAYMENT_DATE_TO, this);
            return _acadGrantPaymentDateTo;
        }

    /**
     * @return Приказ о назначении социальной стипендии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantAssignmentOrderDate()
     */
        public PropertyPath<Date> socGrantAssignmentOrderDate()
        {
            if(_socGrantAssignmentOrderDate == null )
                _socGrantAssignmentOrderDate = new PropertyPath<Date>(OrderDataGen.P_SOC_GRANT_ASSIGNMENT_ORDER_DATE, this);
            return _socGrantAssignmentOrderDate;
        }

    /**
     * @return Приказ о назначении социальной стипендии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantAssignmentOrderNumber()
     */
        public PropertyPath<String> socGrantAssignmentOrderNumber()
        {
            if(_socGrantAssignmentOrderNumber == null )
                _socGrantAssignmentOrderNumber = new PropertyPath<String>(OrderDataGen.P_SOC_GRANT_ASSIGNMENT_ORDER_NUMBER, this);
            return _socGrantAssignmentOrderNumber;
        }

    /**
     * @return Дата начала выплаты социальной стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantPaymentDateFrom()
     */
        public PropertyPath<Date> socGrantPaymentDateFrom()
        {
            if(_socGrantPaymentDateFrom == null )
                _socGrantPaymentDateFrom = new PropertyPath<Date>(OrderDataGen.P_SOC_GRANT_PAYMENT_DATE_FROM, this);
            return _socGrantPaymentDateFrom;
        }

    /**
     * @return Дата окончания выплаты социальной стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSocGrantPaymentDateTo()
     */
        public PropertyPath<Date> socGrantPaymentDateTo()
        {
            if(_socGrantPaymentDateTo == null )
                _socGrantPaymentDateTo = new PropertyPath<Date>(OrderDataGen.P_SOC_GRANT_PAYMENT_DATE_TO, this);
            return _socGrantPaymentDateTo;
        }

    /**
     * @return Приказ о назначении надбавки к академической стипендии - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusAssignmentOrderDate()
     */
        public PropertyPath<Date> acadGrantBonusAssignmentOrderDate()
        {
            if(_acadGrantBonusAssignmentOrderDate == null )
                _acadGrantBonusAssignmentOrderDate = new PropertyPath<Date>(OrderDataGen.P_ACAD_GRANT_BONUS_ASSIGNMENT_ORDER_DATE, this);
            return _acadGrantBonusAssignmentOrderDate;
        }

    /**
     * @return Приказ о назначении надбавки к академической стипендии - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusAssignmentOrderNumber()
     */
        public PropertyPath<String> acadGrantBonusAssignmentOrderNumber()
        {
            if(_acadGrantBonusAssignmentOrderNumber == null )
                _acadGrantBonusAssignmentOrderNumber = new PropertyPath<String>(OrderDataGen.P_ACAD_GRANT_BONUS_ASSIGNMENT_ORDER_NUMBER, this);
            return _acadGrantBonusAssignmentOrderNumber;
        }

    /**
     * @return Дата начала выплаты надбавки к академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusPaymentDateFrom()
     */
        public PropertyPath<Date> acadGrantBonusPaymentDateFrom()
        {
            if(_acadGrantBonusPaymentDateFrom == null )
                _acadGrantBonusPaymentDateFrom = new PropertyPath<Date>(OrderDataGen.P_ACAD_GRANT_BONUS_PAYMENT_DATE_FROM, this);
            return _acadGrantBonusPaymentDateFrom;
        }

    /**
     * @return Дата окончания выплаты надбавки к академической стипендии.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getAcadGrantBonusPaymentDateTo()
     */
        public PropertyPath<Date> acadGrantBonusPaymentDateTo()
        {
            if(_acadGrantBonusPaymentDateTo == null )
                _acadGrantBonusPaymentDateTo = new PropertyPath<Date>(OrderDataGen.P_ACAD_GRANT_BONUS_PAYMENT_DATE_TO, this);
            return _acadGrantBonusPaymentDateTo;
        }

    /**
     * @return Приказ о направлении на практику - дата.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSendPracticeOrderDate()
     */
        public PropertyPath<Date> sendPracticeOrderDate()
        {
            if(_sendPracticeOrderDate == null )
                _sendPracticeOrderDate = new PropertyPath<Date>(OrderDataGen.P_SEND_PRACTICE_ORDER_DATE, this);
            return _sendPracticeOrderDate;
        }

    /**
     * @return Приказ о направлении на практику - номер.
     * @see ru.tandemservice.uni.entity.employee.OrderData#getSendPracticeOrderNumber()
     */
        public PropertyPath<String> sendPracticeOrderNumber()
        {
            if(_sendPracticeOrderNumber == null )
                _sendPracticeOrderNumber = new PropertyPath<String>(OrderDataGen.P_SEND_PRACTICE_ORDER_NUMBER, this);
            return _sendPracticeOrderNumber;
        }

        public Class getEntityClass()
        {
            return OrderData.class;
        }

        public String getEntityName()
        {
            return "orderData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
