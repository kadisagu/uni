/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.util;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.EducationLevelModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolAutocompleteModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.Collections;
import java.util.List;

/**
 * @author agolubenko
 * @since 04.06.2008
 */
@SuppressWarnings("deprecation")
public class EducationLevelUtil
{
    @Deprecated
    public static ISelectModel createEducationLevelsHighSchoolModel(final IEducationLevelModel model)
    {
        return new EducationLevelsHighSchoolAutocompleteModel()
        {
            @Override
            protected List<EducationLevelsHighSchool> getFilteredList()
            {
                return UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(model, "");
            }
        };
    }


    @Deprecated
    public static ISelectModel createDevelopFormModel(final IEducationLevelModel model) {
        return createDevelopFormModel(model, false);
    }
    @Deprecated
    public static ISelectModel createDevelopFormModel(final IEducationLevelModel model, final boolean nullIsIgnore)
    {
        return new FullCheckSelectModel()
        {
            @Override public ListResult findValues(String filter)
            {
                List<DevelopForm> list = UniDaoFacade.getEducationLevelDao().getDevelopFormList(model, nullIsIgnore);
                return new ListResult<>(list, list.size());
            }
        };
    }


    @Deprecated
    public static ISelectModel createDevelopConditionModel(final IEducationLevelModel model) {
        return createDevelopConditionModel(model, false);
    }
    @Deprecated
    public static ISelectModel createDevelopConditionModel(final IEducationLevelModel model, final boolean nullIsIgnore)
    {
        return new FullCheckSelectModel()
        {
            @Override public ListResult findValues(String filter)
            {
                List<DevelopCondition> list = UniDaoFacade.getEducationLevelDao().getDevelopConditionList(model, nullIsIgnore);
                return new ListResult<>(list, list.size());
            }
        };
    }


    @Deprecated
    public static ISelectModel createDevelopTechModel(final IEducationLevelModel model) {
        return createDevelopTechModel(model, false);
    }
    @Deprecated
    public static ISelectModel createDevelopTechModel(final IEducationLevelModel model, final boolean nullIsIgnore)
    {
        return new FullCheckSelectModel()
        {
            @Override public ListResult findValues(String filter)
            {
                List<DevelopTech> list = UniDaoFacade.getEducationLevelDao().getDevelopTechList(model, nullIsIgnore);
                return new ListResult<>(list, list.size());
            }
        };
    }

    @Deprecated
    public static ISelectModel createDevelopPeriodModel(final IEducationLevelModel model) {
        return createDevelopPeriodModel(model, false);
    }
    @Deprecated
    public static ISelectModel createDevelopPeriodModel(final IEducationLevelModel model, final boolean nullIsIgnore)
    {
        return new FullCheckSelectModel()
        {
            @Override public ListResult findValues(String filter)
            {
                List<DevelopPeriod> list = UniDaoFacade.getEducationLevelDao().getDevelopPeriodList(model, nullIsIgnore);
                Collections.sort(list, new EntityComparator<DevelopPeriod>(new EntityOrder(DevelopPeriod.P_PRIORITY)));
                return new ListResult<>(list, list.size());
            }
        };
    }

    @Deprecated
    public static EducationLevelModel getEducationLevelModel(boolean nullIsIgnore) {
        return prepareEducationLevelModel(new EducationLevelModel(), nullIsIgnore);
    }

    @Deprecated
    public static EducationLevelModel prepareEducationLevelModel(EducationLevelModel model, boolean nullIsIgnore) {
        model.setFormativeOrgUnitModel(createFormativeOrgUnitAutocompleteModel());
        model.setTerritorialOrgUnitModel(createTerritorialOrgUnitAutocompleteModel());
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        model.setEducationLevelsHighSchoolModel(createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(createDevelopFormModel(model, nullIsIgnore));
        model.setDevelopConditionModel(createDevelopConditionModel(model, nullIsIgnore));
        model.setDevelopTechModel(createDevelopTechModel(model, nullIsIgnore));
        model.setDevelopPeriodModel(createDevelopPeriodModel(model, nullIsIgnore));
        return model;
    }

    @Deprecated
    public static OrgUnitKindAutocompleteModel createTerritorialOrgUnitAutocompleteModel() {
        return new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL);
    }

    @Deprecated
    public static OrgUnitKindAutocompleteModel createFormativeOrgUnitAutocompleteModel() {
        return new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }
}
