/**
 * $Id$
 */
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author dseleznev
 * Created on: 09.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (null != model.getStudentIds() && !model.getStudentIds().isEmpty()) {
            try { BusinessComponentUtils.downloadDocument(getDao().getDocumentRenderer(model), true); }
            catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
        }
        deactivate(component);
    }
}
