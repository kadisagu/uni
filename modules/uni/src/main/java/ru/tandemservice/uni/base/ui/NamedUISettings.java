package ru.tandemservice.uni.base.ui;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.support.impl.UISettings;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

/**
 * Работает с биндингом <code>mfilter</code>.
 *
 * @author vdanilov
 */
public class NamedUISettings  {

/*
    protected String _presenterPath;

    public NamedUISettings(IUIPresenter presenter, final String presenterPath) {
        super(presenter);
        this._presenterPath = presenterPath;
    }

    protected String getSettingsKey() {
        return (_presenter.getConfig().getName() + "." + String.valueOf(FastBeanUtils.getValue(_presenter, _presenterPath)));
    }

    @Override
    protected IDataSettings createSettings() {
        return DataSettingsFacade.getSettings(UserContext.getInstance().getPrincipal().getId().toString(), getSettingsKey());
    }

    @Override
    public void save() {
        if (null != _settings) {
            DataSettingsFacade.saveSettings(_settings);
        }
    }

    @Override
    public void clear() {
        if (null != _settings) {
            _settings.clear();
            save();
        }
    }
*/
}
