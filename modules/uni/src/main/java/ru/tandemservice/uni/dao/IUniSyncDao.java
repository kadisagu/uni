/* $Id$ */
package ru.tandemservice.uni.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.Collection;

/**
 * Дао содержит разные методы перевычисления полей объектов.
 * Для системного использования в DSet событиях.
 * Вызывать напрямую из бизнес логики не требуется.
 *
 * @author Vasily Zhukov
 * @since 29.06.2011
 */
public interface IUniSyncDao
{
    SpringBeanCache<IUniSyncDao> instance = new SpringBeanCache<>(IUniSyncDao.class.getName());

    /**
     * Обновляет displayableTitle и fullTitle у всех объектов EducationLevelsHighSchool
     * Вызывать в ручную не требуется, обновление происходит автоматически в EduLevelHSDisplayableTitleSync
     * <p/>
     * В методе поднимаются все объекты EducationLevelsHighSchool, так как по изменившимся объектам, влияющим на
     * эти поля, нельзя точно установить какие EducationLevelsHighSchool должны измениться. Потенциально могут
     * измениться все, так как логика вычисления этих полей зависит от того, чему равны эти поля у других объектов
     * <p/>
     * Для системного использования в EduLevelHSDisplayableTitleSync
     *
     * @return true, если было хотя бы одно реальное обновление
     */
    boolean doUpdateEduHSTitles();

    /**
     * Обновляет поля inheritedOkso, inheritedDiplomaQualification у абсолютно всех EducationLevels
     * <p/>
     * Для системного использования в EducationLevelsFieldsSync
     */
    void doUpdateEduLevelsFieldsFull();

    /**
     * Обновляет inheritedOkso, inheritedDiplomaQualification у указанных EducationLevels и их чилдов
     * <p/>
     * Для системного использования в EducationLevelsFieldsSync
     *
     * @param ids идентификаторы EducationLevels
     * @return true, если было обновление
     */
    boolean doUpdateEduLevelsFields(Collection<Long> ids);
}