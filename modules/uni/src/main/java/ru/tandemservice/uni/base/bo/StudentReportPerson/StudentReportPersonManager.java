/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.StudentReportPerson;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentAdditionalData.StudentAdditionalDataBlock;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 26.03.2014
 */
@Configuration
public class StudentReportPersonManager extends BusinessObjectManager
{
    public static StudentReportPersonManager instance() {
        return instance(StudentReportPersonManager.class);
    }

    @Bean
    public UIDataSourceConfig advisorsDS() {
        return SelectDSConfig.with(StudentAdditionalDataBlock.STUDENT_ADVISOR_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(PpsEntry.person().fio().s())
                .addColumn(PpsEntry.orgUnit().shortTitle().s())
                .addColumn(PpsEntry.titlePostOrTimeWorkerData().s())
                .handler(this.advisorsDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler advisorsDSHandler() {
        return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
                .order(PpsEntry.person().identityCard().fullFio())
                .order(PpsEntry.orgUnit().id())
                .order(PpsEntry.id())
                .filter(PpsEntry.person().identityCard().fullFio())
                .customize((alias, dql, context, filter) -> {
                    dql.where(in(property(alias, PpsEntry.id()),
                                 new DQLSelectBuilder()
                                         .fromEntity(Student.class, "s")
                                         .column(property("s", Student.ppsEntry().id())).distinct().buildQuery()));
                    return dql;
                });
    }
}
