package ru.tandemservice.uni.base.bo.UniReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;

import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportMeta;
import ru.tandemservice.uni.base.bo.UniReport.UniReportManager;

/**
 * @author Vasily Zhukov
 * @since 02.02.2011
 */
public class UniReportListUI extends UIPresenter
{
    public void onClickView()
    {
        IReportMeta meta = UniReportManager.instance().uniReportListExtPoint().<Long>getUniqueCodeMappingSource().getUniqueCode2ItemMap().get(_uiSupport.getListenerParameterAsLong());

        if (meta.getParameters() != null)
            _uiActivation.asRegion(meta.getComponentClass()).parameters(meta.getParameters()).activate();
        else
            _uiActivation.asRegion(meta.getComponentClass()).activate();
    }
}
