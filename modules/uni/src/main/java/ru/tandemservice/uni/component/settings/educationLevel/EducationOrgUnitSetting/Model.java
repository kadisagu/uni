/* $Id$ */
package ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitSetting;

import java.util.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

/**
 * @author Vasily Zhukov
 * @since 02.06.2011
 */
public class Model
{
    private List<DataWrapper> _yesNoList;
    private DataWrapper _useShortTitle;
    private DataWrapper _useInternalCode;

    // Getters & Setters

    public List<DataWrapper> getYesNoList()
    {
        return _yesNoList;
    }

    public void setYesNoList(List<DataWrapper> yesNoList)
    {
        _yesNoList = yesNoList;
    }

    public DataWrapper getUseShortTitle()
    {
        return _useShortTitle;
    }

    public void setUseShortTitle(DataWrapper useShortTitle)
    {
        _useShortTitle = useShortTitle;
    }

    public DataWrapper getUseInternalCode()
    {
        return _useInternalCode;
    }

    public void setUseInternalCode(DataWrapper useInternalCode)
    {
        _useInternalCode = useInternalCode;
    }
}
