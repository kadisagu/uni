/* $Id$ */
package ru.tandemservice.uni.component.catalog.developCondition.DevelopConditionAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;

/**
 * @author azhebko
 * @since 26.08.2014
 */
public class Controller extends DefaultCatalogAddEditController<DevelopCondition, Model, IDAO>
{
}