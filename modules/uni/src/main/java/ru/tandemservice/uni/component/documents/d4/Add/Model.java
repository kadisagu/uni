/* $Id$ */
package ru.tandemservice.uni.component.documents.d4.Add;

import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 22.08.2013
 */
public class Model extends DocumentAddBaseModel
{
    private Integer _currentYear;
    private Date _formingDate;
    private String _certificateSeria;
    private String _certificateNumber;
    private String _certificateRegNumber;
    private Date _certificateDate;
    private Date _certificateExpiriationDate;
    private String _certificationAgency;
    private String _licenceSeria;
    private String _licenceNumber;
    private String _licenceRegNumber;
    private Date _licenceDate;
    private String _licensingAgency;
    private String _uniTitleAtTimeAdmission;
    private String _educationLevelStage;
    private String _educationLevelStageHint;
    private String _enrollmentOrderNumber;
    private Date _enrollmentOrderDate;
    private Integer _entranceYear;
    private Date _plannedEndDate;
    private String _militaryOffice;
    private String _rectorAltStr;
    private boolean _trainingOfficersActive;
    private Integer _programStartYear;
    private IdentifiableWrapper _month;
    private List<IdentifiableWrapper> _monthList;
    private Integer _programEndYear;
    private String _managerMilitaryDepartment;

    public Integer getCurrentYear()
    {
        return _currentYear;
    }

    public void setCurrentYear(Integer currentYear)
    {
        _currentYear = currentYear;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getCertificateSeria()
    {
        return _certificateSeria;
    }

    public void setCertificateSeria(String certificateSeria)
    {
        _certificateSeria = certificateSeria;
    }

    public String getCertificateNumber()
    {
        return _certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber)
    {
        _certificateNumber = certificateNumber;
    }

    public String getCertificateRegNumber()
    {
        return _certificateRegNumber;
    }

    public void setCertificateRegNumber(String certificateRegNumber)
    {
        _certificateRegNumber = certificateRegNumber;
    }

    public Date getCertificateDate()
    {
        return _certificateDate;
    }

    public void setCertificateDate(Date certificateDate)
    {
        _certificateDate = certificateDate;
    }

    public Date getCertificateExpiriationDate()
    {
        return _certificateExpiriationDate;
    }

    public void setCertificateExpiriationDate(Date certificateExpiriationDate)
    {
        _certificateExpiriationDate = certificateExpiriationDate;
    }

    public String getCertificationAgency()
    {
        return _certificationAgency;
    }

    public void setCertificationAgency(String certificationAgency)
    {
        _certificationAgency = certificationAgency;
    }

    public String getLicenceSeria()
    {
        return _licenceSeria;
    }

    public void setLicenceSeria(String licenceSeria)
    {
        _licenceSeria = licenceSeria;
    }

    public String getLicenceNumber()
    {
        return _licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber)
    {
        _licenceNumber = licenceNumber;
    }

    public String getLicenceRegNumber()
    {
        return _licenceRegNumber;
    }

    public void setLicenceRegNumber(String licenceRegNumber)
    {
        _licenceRegNumber = licenceRegNumber;
    }

    public Date getLicenceDate()
    {
        return _licenceDate;
    }

    public void setLicenceDate(Date licenceDate)
    {
        _licenceDate = licenceDate;
    }

    public String getLicensingAgency()
    {
        return _licensingAgency;
    }

    public void setLicensingAgency(String licensingAgency)
    {
        _licensingAgency = licensingAgency;
    }

    public String getUniTitleAtTimeAdmission()
    {
        return _uniTitleAtTimeAdmission;
    }

    public void setUniTitleAtTimeAdmission(String uniTitleAtTimeAdmission)
    {
        _uniTitleAtTimeAdmission = uniTitleAtTimeAdmission;
    }

    public String getEducationLevelStage()
    {
        return _educationLevelStage;
    }

    public void setEducationLevelStage(String educationLevelStage)
    {
        _educationLevelStage = educationLevelStage;
    }

    public String getEducationLevelStageHint()
    {
        return _educationLevelStageHint;
    }

    public void setEducationLevelStageHint(String educationLevelStageHint)
    {
        _educationLevelStageHint = educationLevelStageHint;
    }

    public String getEnrollmentOrderNumber()
    {
        return _enrollmentOrderNumber;
    }

    public void setEnrollmentOrderNumber(String enrollmentOrderNumber)
    {
        _enrollmentOrderNumber = enrollmentOrderNumber;
    }

    public Date getEnrollmentOrderDate()
    {
        return _enrollmentOrderDate;
    }

    public void setEnrollmentOrderDate(Date enrollmentOrderDate)
    {
        _enrollmentOrderDate = enrollmentOrderDate;
    }

    public Integer getEntranceYear()
    {
        return _entranceYear;
    }

    public void setEntranceYear(Integer entranceYear)
    {
        _entranceYear = entranceYear;
    }

    public Date getPlannedEndDate()
    {
        return _plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate)
    {
        _plannedEndDate = plannedEndDate;
    }

    public String getMilitaryOffice()
    {
        return _militaryOffice;
    }

    public void setMilitaryOffice(String militaryOffice)
    {
        _militaryOffice = militaryOffice;
    }

    public String getRectorAltStr()
    {
        return _rectorAltStr;
    }

    public void setRectorAltStr(String rectorAltStr)
    {
        _rectorAltStr = rectorAltStr;
    }

    public boolean isTrainingOfficersActive()
    {
        return _trainingOfficersActive;
    }

    public void setTrainingOfficersActive(boolean trainingOfficersActive)
    {
        _trainingOfficersActive = trainingOfficersActive;
    }

    public Integer getProgramStartYear()
    {
        return _programStartYear;
    }

    public void setProgramStartYear(Integer programStartYear)
    {
        _programStartYear = programStartYear;
    }

    public IdentifiableWrapper getMonth()
    {
        return _month;
    }

    public void setMonth(IdentifiableWrapper month)
    {
        _month = month;
    }

    public List<IdentifiableWrapper> getMonthList()
    {
        return _monthList;
    }

    public void setMonthList(List<IdentifiableWrapper> monthList)
    {
        _monthList = monthList;
    }

    public Integer getProgramEndYear()
    {
        return _programEndYear;
    }

    public void setProgramEndYear(Integer programEndYear)
    {
        _programEndYear = programEndYear;
    }

    public String getManagerMilitaryDepartment()
    {
        return _managerMilitaryDepartment;
    }

    public void setManagerMilitaryDepartment(String managerMilitaryDepartment)
    {
        _managerMilitaryDepartment = managerMilitaryDepartment;
    }

    public List<Validator> getValidatorYear()
    {
        List<Validator> list = new ArrayList<Validator>();
        list.add(new Required());
        list.add(new Min("min=1990"));
        list.add(new Max("max=" + getCurrentYear()));
        return list;
    }


}
