/**
 * $Id$
 */
package ru.tandemservice.uni.component.group.StudentPersonCards;

import java.util.List;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.io.RtfReader;

import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.student.StudentPersonCard.IData;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author dseleznev
 * Created on: 08.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setGroup(get(Group.class, model.getGroup().getId()));
        model.setStudentsList(getStudentsList(model.getGroup()));

        //подготовка данных для печати в PrintFactory
        model.setPrintFactory((IGrouptStudentPersonCardPrintFactory) ApplicationRuntime.getApplicationContext().getBean(IGrouptStudentPersonCardPrintFactory.FACTORY_SPRING));
        IData data = model.getPrintFactory().getData();
        data.setTemplate(new RtfReader().read(getCatalogItem(TemplateDocument.class, UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT).getContent()));
        data.setFileName(IData.FILENAME);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void refreshStudent(Model model)
    {
        List<PersonEduInstitution> personEduInstitutionList = getList(PersonEduInstitution.class, PersonEduInstitution.L_PERSON, model.getStudent().getPerson(), PersonEduInstitution.P_ISSUANCE_DATE);
        List<PersonNextOfKin> personNextOfKinList = getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, model.getStudent().getPerson());
        // CREATE PRINT DATA
        model.getPrintFactory().initPrintData(model.getStudent(), personEduInstitutionList, personNextOfKinList, getSession());
        initStudentPrintDAO(model.getPrintFactory(), model.getStudent());
    }

    protected void initStudentPrintDAO(IGrouptStudentPersonCardPrintFactory printFactory, Student student)
    {
    }

    private List<Student> getStudentsList(Group group)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", Student.group().s(), group));
        builder.add(MQExpression.eq("s", Student.archival().s(), Boolean.FALSE));
        builder.addOrder("s", Student.person().identityCard().lastName().s());
        builder.addOrder("s", Student.person().identityCard().firstName().s());
        builder.addOrder("s", Student.person().identityCard().middleName().s());
        return builder.getResultList(getSession());
    }
}