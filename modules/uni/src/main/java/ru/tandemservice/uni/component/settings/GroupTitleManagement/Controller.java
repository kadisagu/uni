/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.GroupTitleManagement;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));

        prepareDataSource(context);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<GroupTitleAlgorithm> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Формат", GroupTitleAlgorithm.P_FORMAT, NoWrapFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пример", GroupTitleAlgorithm.P_SAMPLE, NoWrapFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", GroupTitleAlgorithm.P_DESCRIPTION, RawFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Текущий", GroupTitleAlgorithm.P_CURRENT).setListener("onClickCurrent"));
        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent context)
    {
        getModel(context).setAlgorithmId((Long) context.getListenerParameter());
        context.createDefaultChildRegion(new ComponentActivator(IUniComponents.GROUP_TITLE_MANAGEMENT_EDIT));
    }

    public void onClickCurrent(IBusinessComponent context)
    {
        getDao().updateCurrent((Long) context.getListenerParameter());
    }
}
