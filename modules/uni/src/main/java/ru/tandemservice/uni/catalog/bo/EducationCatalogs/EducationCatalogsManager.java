/* $Id$ */
package ru.tandemservice.uni.catalog.bo.EducationCatalogs;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.ISelectValueStyleSource;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.common.catalog.entity.IActiveCatalogItem;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.ISelectableByEducationYear;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/7/12
 */
@Configuration
public class EducationCatalogsManager extends BusinessObjectManager
{
    public static final String DS_EDU_YEAR = "eduYearDS";
    public static final String DS_YEAR_PART = "yearPartDS";

    public static final String PARAM_EDU_HS = "eduHS";
    public static final String PARAM_FORMATIVE_ORGUNIT = "formativeOrgUnit";
    public static final String PARAM_TERRITORIAL_ORGUNIT = "territorialOrgUnit";

    // параметры модели "Часть года"
    public static final String PARAM_DEVELOP_GRID = "developGrid";
    public static final String PARAM_COURSE = "course";

    public static EducationCatalogsManager instance()
    {
        return instance(EducationCatalogsManager.class);
    }

    /** @return Хандлер стандартного селекта Направления (специальности) */
    public static IDefaultComboDataSourceHandler createEduHsDSHandler(String ownerName, final String formativeOuSettingsName, final String territorialOuSettingsName)
    {
        final EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(ownerName, EducationLevelsHighSchool.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                DQLSelectBuilder eduOuDql = new DQLSelectBuilder()
                        .fromEntity(EducationOrgUnit.class, "eduOu")
                        .where(eq(property(alias), property("eduOu", EducationOrgUnit.educationLevelHighSchool())));

                if (StringUtils.isNotEmpty(formativeOuSettingsName))
                    FilterUtils.applySelectFilter(eduOuDql, "eduOu", EducationOrgUnit.formativeOrgUnit(), context.get(formativeOuSettingsName));

                if (StringUtils.isNotEmpty(territorialOuSettingsName))
                    FilterUtils.applySelectFilter(eduOuDql, "eduOu", EducationOrgUnit.territorialOrgUnit(), context.get(territorialOuSettingsName));

                dql.where(exists(eduOuDql.buildQuery()));
            }
        };

        handler.order(EducationLevelsHighSchool.fullTitleExtended());
        handler.filter(EducationLevelsHighSchool.fullTitleExtended());
        handler.pageable(true);

        return handler;
    }

    @Bean
    public IDefaultComboDataSourceHandler eduHsDSHandler()
    {
        return createEduHsDSHandler(getName(), PARAM_FORMATIVE_ORGUNIT, PARAM_TERRITORIAL_ORGUNIT);
    }

    @Bean
    public UIDataSourceConfig eduYearDSConfig()
    {
        return SelectDSConfig.with(DS_EDU_YEAR, getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(eduYearDSHandler())
        .valueStyleSource(getEduYearValueStyleSource())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }


    public static ISelectValueStyleSource getEduYearValueStyleSource()
    {
        return value -> {
            final EducationYear eduYear = (value instanceof ISelectableByEducationYear ? ((ISelectableByEducationYear) value).getEducationYear() : null);
            return new ISelectValueStyle()
            {
                @Override public boolean isDisabled() { return false; }
                @Override public boolean isSelectable() { return true; }
                @Override public boolean isRemovable() { return false; }
                @Override public String getControlColumnStyle() { return null; }
                @Override public String getColumnStyle(int columnIndex) { return null; }

                @Override
                public String getRowStyle() {
                    if (null == eduYear)
                        return null;
                    return Boolean.TRUE.equals(eduYear.getCurrent()) ? EducationYearModel.CURRENT_YEAR_STYLE : null;
                }
            };
        };
    }

    @Bean
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1063816")
    public UIDataSourceConfig yearPartDSConfig()
    {
        return SelectDSConfig.with(DS_YEAR_PART, getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(yearPartDSHandler())
        .treeable(true)
        .valueStyleSource(getYearPartValueStyleSource())
        .create();
    }

    @Bean
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1063816")
    public IReadAggregateHandler<DSInput, DSOutput> yearPartDSHandler()
    {
        return new AbstractReadAggregateHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(YearDistributionPart.class, "p").column("p")
                    .order(property(YearDistributionPart.yearDistribution().code().fromAlias("p")))
                    .order(property(YearDistributionPart.number().fromAlias("p")));

                if (!CollectionUtils.isEmpty(dsInput.getPrimaryKeys()))
                    return ListOutputBuilder.get(dsInput, dql.where(in("p.id", dsInput.getPrimaryKeys())).createStatement(context.getSession()).list()).build();

                dql.where(eq(property(YearDistributionPart.yearDistribution().inUse().fromAlias("p")), value(Boolean.TRUE)));

                DevelopGrid developGrid = context.get(PARAM_DEVELOP_GRID);
                Course course = context.get(PARAM_COURSE);

                if (developGrid != null)
                    dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(DevelopGridTerm.class, "t")
                        .where(eq(property("t", DevelopGridTerm.part()), property("p")))
                        .where(eq(property("t", DevelopGridTerm.developGrid()), value(developGrid)))
                        .where(course == null ? null : eq(property("t", DevelopGridTerm.course()), value(course)))
                        .buildQuery()));

                final Set<YearDistributionPart> parts = new LinkedHashSet<>(dql.createStatement(context.getSession()).<YearDistributionPart>list());
                final List options = HierarchyUtil.listHierarchyItemsWithParents(new ArrayList<>(parts), new Comparator()
                {
                    @Override
                    public int compare(Object o1, Object o2)
                    {
                        if(o1 instanceof YearDistribution && o2 instanceof YearDistribution)
                        {
                            return ((YearDistribution) o1).getCode().compareTo(((YearDistribution) o2).getCode());
                        }

                        if(o1 instanceof YearDistributionPart && o2 instanceof YearDistributionPart) return ((YearDistributionPart) o1).getNumber() - ((YearDistributionPart) o2).getNumber();

                        if(o1 instanceof YearDistributionPart) return -1;

                        return 1;
                    }
                });

                return ListOutputBuilder.get(dsInput, options).build();
            }
        };
    }


    public static ISelectValueStyle getDefaultValueStyleSource(boolean disabled)
    {
        return new ISelectValueStyle()
        {
            @Override public boolean isDisabled() { return disabled; }
            @Override public String getRowStyle() { return isDisabled() ? "color:gray;" : null; }
            @Override public boolean isSelectable() { return !isDisabled(); }
            @Override public boolean isRemovable() { return true; }
            @Override public String getControlColumnStyle() { return null; }
            @Override public String getColumnStyle(int columnIndex) { return null; }
        };
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1063816")
    public static ISelectValueStyleSource getYearPartValueStyleSource()
    {
        return value -> {
            final boolean disabled = !(value instanceof YearDistributionPart);
            return getDefaultValueStyleSource(disabled);
        };
    }

    public static ISingleSelectModel getYearDistributionPartModel()
    {
        return getYearDistributionPartModel(null);
    }

    public static ISingleSelectModel getYearDistributionPartModel(final Collection<YearDistributionPart> partList)
    {
        CommonSingleSelectModel commonSingleSelectModel = new CommonSingleSelectModel()
        {
            @Override
            public IViewSelectValueStyle getValueStyle(Object value)
            {
                final boolean disabled = !(value instanceof YearDistributionPart);
                return getDefaultValueStyleSource(disabled);
            }

            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                List options;
                if (partList != null)
                    options = HierarchyUtil.listHierarchyItemsWithParents(new ArrayList<>(partList));
                else
                    options = HierarchyUtil.listHierarchyItemsWithParents(IUniBaseDao.instance.get().getCatalogItemList(YearDistributionPart.class, YearDistributionPart.yearDistribution().inUse().s(), true));

                if (o != null) {
                    options.removeIf(item -> !((IIdentifiable) item).getId().equals(o));
                }

                return new SimpleListResultBuilder<>(options);
            }
        };
        commonSingleSelectModel.setHierarchical(true);
        return commonSingleSelectModel;
    }

    public static ISingleSelectModel getEducationYearModel()
    {
        return new EducationYearModel();
    }


//                  ФФ           УУ          УУ     ТТТТТТТТТТТТТТТТ       СССССССС
//              ФФФФФФФФФФ        УУ        УУ             ТТ            СС        СС
//            ФФ    ФФ    ФФ       УУ      УУ              ТТ          СС
//           ФФ     ФФ     ФФ       УУ    УУ               ТТ         СС
//           ФФ     ФФ     ФФ        УУ  УУ                ТТ         СС
//            ФФ    ФФ    ФФ          УУУУ                 ТТ         СС
//              ФФ  ФФ  ФФ             УУ                  ТТ          СС
//                ФФФФФФ              УУ                   ТТ           СС
//                  ФФ               УУ                    ТТ            СС        СС
//                  ФФ              УУ                     ТТ              СССССССС

    /** @return  Модель селекта формы обучения. */
    public static ISelectModel getDevelopFormSelectModel() { return new ActiveCatalogSelectModel<>(DevelopForm.class); }

    /** @return  Модель селекта технологии обучения. */
    public static ISelectModel getDevelopTechSelectModel() { return new ActiveCatalogSelectModel<>(DevelopTech.class); }

    /** @return  Модель селекта условия освоения. */
    public static ISelectModel getDevelopConditionSelectModel() { return new ActiveCatalogSelectModel<>(DevelopCondition.class); }

    /** @return  Модель селекта срока освоения. */
    public static ISelectModel getDevelopPeriodSelectModel() { return new ActiveCatalogSelectModel<>(DevelopPeriod.class, DevelopPeriod.P_PRIORITY); }


    /** Модель селекта элементов активного справочникаю. */
    private static class ActiveCatalogSelectModel<T extends IActiveCatalogItem> extends CommonFilterSelectModel
    {
        private final Class<T> _entityClass;        // класс активного справочника
        private final String[] _orderProperties;    // свойства сортировки

        private ActiveCatalogSelectModel(Class<T> entityClass, String ... orderProperties)
        {
            _entityClass = entityClass;
            _orderProperties = orderProperties;
        }

        @Override
        protected IListResultBuilder createBuilder(String filter, Object o)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(_entityClass, "alias")
                .where(eq(property("alias", "enabled"), value(Boolean.TRUE)));

            FilterUtils.applySelectFilter(builder, "alias", IEntity.P_ID, o);

            if (StringUtils.isNotEmpty(filter)) {
                builder.where(likeUpper(property("alias", ICatalogItem.CATALOG_ITEM_TITLE), value(CoreStringUtils.escapeLike(filter))));
            }

            for (String orderProperty: _orderProperties) {
                builder.order(property("alias", orderProperty));
            }

            builder.order(property("alias", ICatalogItem.CATALOG_ITEM_CODE));

            return new DQLListResultBuilder(builder);
        }
    }


    public static final String DS_DEVELOP_FORM = "developFormDS";               // Формы обучения
    public static final String DS_DEVELOP_TECH = "developTechDS";               // Технология обучения
    public static final String DS_DEVELOP_PERIOD = "developPeriodDS";           // Срок освоения
    public static final String DS_DEVELOP_GRID = "developGridDS";               // Учебная сетка
    public static final String DS_DEVELOP_CONDITION = "developConditionDS";     // Условие освоения
    public static final String DS_PROGRAM_TRAIT = "programTraitDS";             // Особенности обучения
    public static final String DS_PROGRAM_FORM = "programFormDS";               // Форма обучения
    public static final String DS_DEVELOP_COMBINATION = "developCombinationDS"; // ФУТС


    public static final String PARAM_DEVELOP_DORM = "developForm";

    /** @return Конфиг стандартного селекта Формы обучения */
    @Bean
    public UIDataSourceConfig developFormDSConfig()
    {
        return SelectDSConfig.with(DS_DEVELOP_FORM, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(developFormDSHandler())
            .create();
    }

    /** @return Конфиг стандартного селекта Технологии обучения */
    @Bean
    public UIDataSourceConfig developTechDSConfig()
    {
        return SelectDSConfig.with(DS_DEVELOP_TECH, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(developTechDSHandler())
            .create();
    }

    /** @return Конфиг стандартного селекта Срока освоения */
    @Bean
    public UIDataSourceConfig developPeriodDSConfig()
    {
        return SelectDSConfig.with(DS_DEVELOP_PERIOD, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(developPeriodDSHandler())
            .create();
    }

    /** @return Конфиг стандартного селекта Учебная сетка (срок освоения) */
    @Bean
    public UIDataSourceConfig developGridDSConfig()
    {
        return SelectDSConfig.with(DS_DEVELOP_GRID, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(developGridDSHandler())
                .create();
    }

    /** @return Конфиг стандартного селекта Условия освоения */
    @Bean
    public UIDataSourceConfig developConditionDSConfig()
    {
        return SelectDSConfig.with(DS_DEVELOP_CONDITION, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(developConditionDSHandler())
                .create();
    }

    /** @return Конфиг стандартного селекта Особенности ОП */
    @Bean
    public UIDataSourceConfig programTraitDSConfig()
    {
        return SelectDSConfig.with(DS_PROGRAM_TRAIT, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(programTraitDSHandler())
                .create();
    }

    /** @return Конфиг стандартного селекта Особенности ОП */
    @Bean
    public UIDataSourceConfig programFormDSConfig()
    {
        return SelectDSConfig.with(DS_PROGRAM_FORM, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(programFormDSHandler())
                .create();
    }

    /** @return Конфиг стандартного селекта ФУТС */
    @Bean
    public UIDataSourceConfig developCombinationDSConfig()
    {
        return SelectDSConfig.with(DS_DEVELOP_COMBINATION, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(developCombinationDSHandler())
                .create();
    }

    /** @return Хандлер стандартного селекта Формы обучения */
    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopForm.class)
                .where(DevelopForm.enabled(), Boolean.TRUE)
                .filter(DevelopForm.title())
                .order(DevelopForm.code())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта Технологии обучения */
    @Bean
    public IDefaultComboDataSourceHandler developTechDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopTech.class)
                .where(DevelopTech.enabled(), Boolean.TRUE)
                .filter(DevelopTech.title())
                .order(DevelopTech.code())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта Срока освоения */
    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopPeriod.class)
                .where(DevelopPeriod.enabled(), Boolean.TRUE)
                .filter(DevelopPeriod.title())
                .order(DevelopPeriod.priority())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта Условия освоения */
    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopCondition.class)
                .where(DevelopCondition.enabled(), Boolean.TRUE)
                .filter(DevelopCondition.title())
                .order(DevelopCondition.code())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта Учебной сетки (срок освоения) */
    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopGrid.class)
                .filter(DevelopGrid.title())
                .order(DevelopGrid.developPeriod().priority())
                .order(DevelopGrid.title())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта Формы обучения */
    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramForm.class)
                .filter(EduProgramForm.title())
                .order(EduProgramForm.code())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта Особенности ОП */
    @Bean
    public IDefaultComboDataSourceHandler programTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramTrait.class)
                .filter(EduProgramTrait.title())
                .order(EduProgramTrait.code())
                .pageable(false);
    }

    /** @return Хандлер стандартного селекта ФУТС */
    @Bean
    public IDefaultComboDataSourceHandler developCombinationDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), DevelopCombination.class)
            .filter(DevelopCombination.developForm().title())
            .filter(DevelopCombination.developCondition().title())
            .filter(DevelopCombination.developTech().title())
            .filter(DevelopCombination.developGrid().title())
            .order(DevelopCombination.developForm().code())
            .order(DevelopCombination.developCondition().code())
            .order(DevelopCombination.developTech().code())
            .order(DevelopCombination.developGrid().developPeriod().priority())
            ;
    }
}
