package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 22.02.11
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
public abstract class RowWrapper {
    public RowWrapper(final Course course, final YearDistributionPart part) {
            if (null != course && part != null)
            {
                this.setCurrentCourse(course);
                this.setCurrentPart(part);
            }
        }

        public abstract ISelectModel getCourseListModel();
        public abstract IHSelectModel getPartListModel();

        private Course currentCourse;
        public Course getCurrentCourse() { return this.currentCourse; }
        public void setCurrentCourse(final Course currentCourse) { this.currentCourse = currentCourse; }

        private YearDistributionPart currentPart;
        public YearDistributionPart getCurrentPart() { return this.currentPart; }
        public void setCurrentPart(final YearDistributionPart currentPart) { this.currentPart = currentPart; }

}
