/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.base.bo.UniStudent.daemon.UniStudentDevelopPeriodDaemonBean;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.setPrincipalContext(context.getUserContext().getPrincipalContext());
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent context)
    {
        ErrorCollector errors = context.getUserContext().getErrorCollector();
        getDao().validate(getModel(context), errors);
        if (errors.hasErrors()) return;
        
        getDao().update(getModel(context));
        UniStudentDevelopPeriodDaemonBean.DAEMON.wakeUpAndWaitDaemon(120);
        deactivate(context);
    }
}
