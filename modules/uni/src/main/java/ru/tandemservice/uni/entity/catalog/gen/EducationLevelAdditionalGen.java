package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelAdditional;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направления подготовки ДПО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelAdditionalGen extends EducationLevels
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.EducationLevelAdditional";
    public static final String ENTITY_NAME = "educationLevelAdditional";
    public static final int VERSION_HASH = -867813219;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EducationLevelAdditionalGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelAdditionalGen> extends EducationLevels.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevelAdditional.class;
        }

        public T newInstance()
        {
            return (T) new EducationLevelAdditional();
        }
    }
    private static final Path<EducationLevelAdditional> _dslPath = new Path<EducationLevelAdditional>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevelAdditional");
    }
            

    public static class Path<E extends EducationLevelAdditional> extends EducationLevels.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EducationLevelAdditional.class;
        }

        public String getEntityName()
        {
            return "educationLevelAdditional";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
