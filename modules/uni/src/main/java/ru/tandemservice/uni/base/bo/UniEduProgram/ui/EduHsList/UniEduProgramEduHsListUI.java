/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsAdd.UniEduProgramEduHsAdd;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEdit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEditUI;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsPub.UniEduProgramEduHsPub;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlPub.UniEduProgramEduLvlPub;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/14
 */
public class UniEduProgramEduHsListUI extends UIPresenter
{

    private List<HSelectOption> _levelTypeList;
    private List<StructureEducationLevels> levelTopList;
    private ISelectModel _orgUnitListModel;
    private List<Qualifications> _qualificationList;

    private Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap;
    
    private DynamicListDataSource<EducationLevelsHighSchool> _dataSource;

    @Override
    public void onComponentRefresh()
    {
        prepareStructureMap();
        setLevelTopList(DataAccessServices.dao().getList(StructureEducationLevels.class, StructureEducationLevels.parent(), (StructureEducationLevels) null, StructureEducationLevels.priority().s()));
        prepareLevelTypeModel();
        setOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        setQualificationList(DataAccessServices.dao().getList(Qualifications.class, Qualifications.P_CODE));
        createListDataSource();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAsMap("code", "title", "levelType"));
    }

    // actions

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        getSettings().set("levelTop", IUniBaseDao.instance.get().getCatalogItem(StructureEducationLevels.class, StructureEducationLevelsCodes.HIGH_GOS3_GROUP));
        prepareLevelTypeModel();
        onClickSearch();
    }
    
    public void onChangeLevelTop() {
        prepareLevelTypeModel();
    }

    public void onClickAddEduItem() {
        _uiActivation.asRegionDialog(UniEduProgramEduHsAdd.class).activate();
    }

    public void onClickAddOldItem() {
        _uiActivation.asRegionDialog(ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolAddEdit.Model.class.getPackage().getName()).activate();
    }

    public void onClickEditItem() {
        _uiActivation.asRegion(UniEduProgramEduHsEdit.class).
            parameter(UniEduProgramEduHsEditUI.PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }

    public void onClickDeleteItem() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickAllowStudents()
    {
        _uiSupport.setRefreshScheduled(true);
        UniEduProgramManager.instance().dao().doChangeAllowStudents(getListenerParameterAsLong());
    }
    
    // utils
    
    private void prepareLevelTypeModel()
    {
        if (null == getSettings()) {
            setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(Collections.<StructureEducationLevels>emptyList(), CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR, true));
            return;
        }

        StructureEducationLevels levelTop = getSettings().get("levelTop");
        if (null == levelTop) {
            setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(Collections.<StructureEducationLevels>emptyList(), true));
            return;
        }

        if (null == getStructureMap())
            prepareStructureMap();

        ArrayList<StructureEducationLevels> list = new ArrayList<>(getStructureMap().get(levelTop));
        setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(list, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR, true));
    }
    
    private void prepareStructureMap()
    {
        List<StructureEducationLevels> baseList = DataAccessServices.dao().getList(StructureEducationLevels.class);
        Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap = new HashMap<>();
        for (StructureEducationLevels level : baseList)
            SafeMap.safeGet(structureMap, getRoot(level), HashSet.class).add(level);
        setStructureMap(structureMap);
    }

    private StructureEducationLevels getRoot(StructureEducationLevels item)
    {
        StructureEducationLevels root = item;
        while (root.getParent() != null)
            root = root.getParent();
        return root;
    }

    @SuppressWarnings("deprecation")
    private void createListDataSource()
    {
        DynamicListDataSource<EducationLevelsHighSchool> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {
            final DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(EducationLevelsHighSchool.class, "m");
            final DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("m"));

            StructureEducationLevels levelTop = (StructureEducationLevels) getSettings().get("levelTop");
            StructureEducationLevels levelType = (StructureEducationLevels) getSettings().get("levelType");
            String subjectCode = StringUtils.trimToNull((String) getSettings().get("subjectCode"));
            String title = getSettings().get("title");
            ICatalogItem qualification = (ICatalogItem) getSettings().get("qualification");
            OrgUnit orgUnit = (OrgUnit) getSettings().get("orgUnit");

            FilterUtils.applySimpleLikeFilter(dql, "m", EducationLevelsHighSchool.title(), title);

            if (levelTop == null)
            {
                dql.where(isNull("m.id"));
            }

            if (levelType != null)
            {
                dql.where(eq(property("m", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE), value(levelType)));
            }
            else
            {
                if (null == getStructureMap()) prepareStructureMap();
                dql.where(in(property("m", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE), getStructureMap().get(levelTop)));
            }
            if (subjectCode != null)
            {
                Set<Long> ids = new HashSet<>();
                for (EducationLevelsHighSchool educationLevelsHighSchool : DataAccessServices.dao().getList(EducationLevelsHighSchool.class))
                {
                    if (subjectCode.equals(educationLevelsHighSchool.getEducationLevel().getInheritedOkso()))
                    {
                        ids.add(educationLevelsHighSchool.getId());
                    }

                    String titleCodePrefix = educationLevelsHighSchool.getEducationLevel().getTitleCodePrefix();
                    if ((null != titleCodePrefix) && titleCodePrefix.startsWith(subjectCode))
                    {
                        ids.add(educationLevelsHighSchool.getId());
                    }
                }

                dql.where(in(property("m", EducationLevelsHighSchool.P_ID), ids));
            }
            if (qualification != null)
            {
                Set<Long> ids = new HashSet<>();
                for (EducationLevelsHighSchool educationLevelsHighSchool : DataAccessServices.dao().getList(EducationLevelsHighSchool.class))
                    if (qualification.getCode().equals(educationLevelsHighSchool.getEducationLevel().getSafeQCode()))
                        ids.add(educationLevelsHighSchool.getId());

                dql.where(in(property("m", EducationLevelsHighSchool.P_ID), ids));
            }
            if (orgUnit != null)
            {
                dql.where(eq(property("m", EducationLevelsHighSchool.L_ORG_UNIT), value(orgUnit)));
            }

            orderDescriptionRegistry.applyOrderWithLeftJoins(dql, getDataSource().getEntityOrder());
            UniBaseUtils.createPage(getDataSource(), dql, _uiSupport.getSession());
        });

        // dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelsHighSchool.P_FULL_TITLE));
        dataSource.addColumn(new PublisherLinkColumn("Название", EducationLevelsHighSchool.P_FULL_TITLE).setResolver(new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) { return entity.getId(); }
            @Override public String getComponentName(final IEntity entity) { return UniEduProgramEduHsPub.class.getSimpleName(); }
        }));


        dataSource.addColumn(new SimpleColumn("Сокр. название", EducationLevelsHighSchool.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("НПм", EducationLevelsHighSchool.educationLevel().fullTitle().s()).setResolver(new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) { return ((EducationLevelsHighSchool)entity).getEducationLevel().getId(); }
            @Override public String getComponentName(final IEntity entity) { return UniEduProgramEduLvlPub.class.getSimpleName(); }
        }).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Направление ОП", EducationLevelsHighSchool.educationLevel().programSubjectWithCodeIndexAndGenTitle().s())
                                     .setResolver(new SimplePublisherLinkResolver(EducationLevelsHighSchool.educationLevel().eduProgramSubject().id()))
                                     .setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", EducationLevelsHighSchool.educationLevel().programSpecializationTitle().s()).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Квалификация", EducationLevelsHighSchool.assignedQualification().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ориентация", EducationLevelsHighSchool.programOrientation().abbreviation().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", EducationLevelsHighSchool.orgUnit().fullTitle().s()).setClickable(false));

        dataSource.addColumn(new ToggleColumn("Обучение студентов осуществляется", EducationLevelsHighSchool.allowStudents().s()).setListener("onClickAllowStudents").setDisabledProperty(EducationLevelsHighSchool.P_ALLOW_STUDENTS_DISABLED));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE));

        setDataSource(dataSource);
    }

    // getters and setters

    public List<HSelectOption> getLevelTypeList()
    {
        return _levelTypeList;
    }

    public void setLevelTypeList(List<HSelectOption> levelTypeList)
    {
        _levelTypeList = levelTypeList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<StructureEducationLevels> getLevelTopList()
    {
        return levelTopList;
    }

    public void setLevelTopList(List<StructureEducationLevels> levelTopList)
    {
        this.levelTopList = levelTopList;
    }

    public Map<StructureEducationLevels, Set<StructureEducationLevels>> getStructureMap()
    {
        return structureMap;
    }

    public void setStructureMap(Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap)
    {
        this.structureMap = structureMap;
    }

    public DynamicListDataSource<EducationLevelsHighSchool> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EducationLevelsHighSchool> dataSource)
    {
        _dataSource = dataSource;
    }
}