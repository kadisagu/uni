package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelHighGos3sGen;

/**
 * Направление подготовки (специальность) ВПО (2013)
 */
public class EducationLevelHighGos3s extends EducationLevelHighGos3sGen
{
    public EducationLevelHighGos3s() {
        setCatalogCode("educationLevelHighGos3s");
    }
}