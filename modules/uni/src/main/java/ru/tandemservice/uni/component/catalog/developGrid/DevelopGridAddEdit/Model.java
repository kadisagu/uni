/* $Id: modelAddEdit.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridAddEdit;

import java.util.*;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;

/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public class Model extends DefaultCatalogAddEditModel<DevelopGrid>
{
    private String _title;
    private ISelectModel _developPeriodList;
    private Collection<Term> _termList;
    private Term _term;
    private DevelopPeriod _developPeriod;

    private List<Term> _currentTermList = new ArrayList<Term>();
    public List<Term> getCurrentTermList() { return this._currentTermList;  }
    public void setCurrentTermList(final List<Term> currentTermList) { this._currentTermList = currentTermList; }

    private Term _currentTerm;
    public Term getCurrentTerm() { return this._currentTerm; }
    public void setCurrentTerm(final Term currentTerm) { this._currentTerm = currentTerm; }
    public String _currentCourseId, _currentPartId;

    public String getCurrentCourseId()
    {
        return "id_course_" + String.valueOf(getCurrentTerm().getIntValue());
    }

    public String getCurrentPartId()
    {
         return "id_part_" + String.valueOf(getCurrentTerm().getIntValue());
    }

    public void setCurrentCourseId(String currentCourseId) {
        _currentCourseId = currentCourseId;
    }

    public void setCurrentPartId(String currentPartId) {
        _currentPartId = currentPartId;
    }

    public boolean isTableVisible()
    {
        return rowTermMap.size()>0;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public ISelectModel getDevelopPeriodList() { return _developPeriodList; }
    public void setDevelopPeriodList(ISelectModel developPeriodList) { _developPeriodList = developPeriodList; }

    public Collection<Term> getTermList()
    {
        return _termList;
    }

    public void setTermList(Collection<Term> termList)
    {
        _termList = termList;
    }

    public Term getTerm()
    {
        return _term;
    }

    public void setTerm(Term term)
    {
        _term = term;
    }

    private Map<Integer, RowWrapper> rowTermMap = new HashMap<Integer, RowWrapper>();
    public Map<Integer, RowWrapper> getRowTermMap() { return this.rowTermMap; }
    public void setRowTermMap(final Map<Integer, RowWrapper> rowTermMap) { this.rowTermMap = rowTermMap; }

    protected RowWrapper getCurrentRow()
    {
        return this.getRowTermMap().get(this.getCurrentTerm().getIntValue());
    }

    public ISelectModel getCourseListModel()
    {
        return this.getCurrentRow().getCourseListModel();
    }

    public IHSelectModel getPartListModel()
    {
        return this.getCurrentRow().getPartListModel();
    }

    public Course getCurrentCourse()
    {
        return this.getCurrentRow().getCurrentCourse();
    }

    public void setCurrentPart(final ICatalogItem part)
    {
        if (part instanceof YearDistributionPart)
            this.getCurrentRow().setCurrentPart((YearDistributionPart)part);
        else
            this.getCurrentRow().setCurrentPart(null);
    }

    public ICatalogItem getCurrentPart()
    {
        return this.getCurrentRow().getCurrentPart();
    }

    public void setCurrentCourse(final Course course)
    {
        this.getCurrentRow().setCurrentCourse(course);
    }


}
