/* $Id$ */
package ru.tandemservice.uni.component.catalog.developCondition.DevelopConditionPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;

/**
 * @author azhebko
 * @since 15.08.2014
 */
public class Model extends DefaultCatalogPubModel<DevelopCondition>
{
}