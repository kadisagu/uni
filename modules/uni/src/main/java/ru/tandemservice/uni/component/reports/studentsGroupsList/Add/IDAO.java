/**
 * $Id$
 */
package ru.tandemservice.uni.component.reports.studentsGroupsList.Add;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 15.06.2009
 */
public interface IDAO extends IUniDao<Model>
{
    Integer preparePrintReport(Model model);
}