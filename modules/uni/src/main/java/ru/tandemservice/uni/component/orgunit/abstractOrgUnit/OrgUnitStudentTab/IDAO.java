package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentTab;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

public interface IDAO
{
    void prepare(Model model);
}
