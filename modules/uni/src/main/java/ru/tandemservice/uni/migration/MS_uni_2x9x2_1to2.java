/* $Id$ */
package ru.tandemservice.uni.migration;

import com.google.common.collect.HashBiMap;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Nikolay Fedorovskih
 * @since 11.12.2015
 */
public class MS_uni_2x9x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    public static boolean canAutoFix() {
        return "true".equals(ApplicationRuntime.getProperty("qualifications-auto-fixer.DO.NOT.USE.ON.PRODUCTION")) && Debug.isEnabled();
    }

    public static boolean autoFix(DBTool tool) throws Exception {

        if ( ! canAutoFix() ) {
            return false;
        }

        // Грузим сами квалификации
        // { [index.id] -> [[q.title] -> [q.id]] }
        final Map<Long, Map<String, Long>> qMap = SafeMap.get(key -> HashBiMap.create());
        {
            final List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, String.class, Long.class),
                    "select id, title_p, subjectindex_id from edu_c_pr_qual_t"
            );
            for (final Object[] row : rows) {
                final Long qId = (Long) row[0];
                final String qTitle = (String) row[1];
                final Long indexId = (Long) row[2];
                qMap.get(indexId).put(qTitle, qId);
            }
        }

        // Грузим связи с направлениями
        // { [subj.id] -> [[q.id]] }
        final Map<Long, Set<Long>> relMap = SafeMap.get(key -> new HashSet<>());
        {
            final List<Object[]> rows= tool.executeQuery(
                    processor(Long.class, Long.class),
                    "select programsubject_id, programqualification_id from edu_c_pr_subject_qual_t"
            );
            for (final Object[] row : rows) {
                final Long subjId = (Long) row[0];
                final Long qId = (Long) row[1];
                relMap.get(subjId).add(qId);
            }
        }

        // Грузим квалификации по диплому из НПв
        // { [eduHS.id] -> [diplomaQualification.title] }
        final Map<Long, String> diplomaQualificationMap = new HashMap<>();
        {
            final List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, String.class),
                    "select hs.id, d.title_p from educationlevelshighschool_t hs join educationlevels_t l on l.id=hs.educationlevel_id join diplomaqualifications_t d on d.id = l.diplomaqualification_id"
            );
            for (final Object[] row : rows) {
                final Long hsId = (Long) row[0];
                final String diplomaQualificationTitle = (String) row[1];
                diplomaQualificationMap.put(hsId, diplomaQualificationTitle);
            }
        }

        // Грузим сокращенные названия перечней
        final Map<Long, String> indexShortTitleMap = new HashMap<>();
        {
            final List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, String.class),
                    "select id, shorttitle_p from edu_c_pr_subject_index_t"
            );
            for (final Object[] row : rows) {
                final Long indexId = (Long) row[0];
                final String indexShortTitle = (String) row[1];
                indexShortTitleMap.put(indexId, indexShortTitle);
            }
        }

        final BatchInsertBuilder relsIns = new BatchInsertBuilder(tool.entityCodes().ensure("eduProgramSubjectQualification"), "programsubject_id", "programqualification_id");
        final BiConsumer<Long, Long> addRelConsumer = (subjId, qId) -> {
            final Set<Long> qSet = relMap.get(subjId);
            if (!qSet.contains(qId)) {
                relsIns.addRow(subjId, qId);
                qSet.add(qId);
            }
        };

        final BatchInsertBuilder qualIns = new BatchInsertBuilder(tool.entityCodes().ensure("eduProgramQualification"), "code_p", "subjectindex_id", "title_p", "qualificationlevelcode_p");
        final BiFunction<Long, String, Long> addQualificationFunction = (indexId, title) -> {
            if (title == null) {
                title = "Квалификация. " + indexShortTitleMap.get(indexId);
            }
            final Map<String, Long> qTitleMap = qMap.get(indexId);
            Long q_id = qTitleMap.get(title);
            if (q_id == null) {
                // С таким названием в перечне еще нет. Создаем.
                final String q_code = Long.toHexString(indexId) + "." + Integer.toHexString(title.hashCode()) + "." + Long.toHexString(System.currentTimeMillis());
                q_id = qualIns.addRow(q_code, indexId, title, null);
                qTitleMap.put(title, q_id);
            }
            return q_id;
        };

        // Блок 1. Уникальность названия квалификации в рамках перечня и кода уровня квалификации
        // Исправлено в MS_uni_2x9x2_0to1.

        final List<BatchUpdater> updaters = new ArrayList<>();

        // Блок 5. Для параметров выпуска студентов по направлению подготовки (НПв) не указана квалификация.
        // Блок 6. Для параметров выпуска студентов по направлению подготовки (НПв) квалификация указана неверно.
        {
            final BatchUpdater upd = new BatchUpdater("update educationlevelshighschool_t set assignedqualification_id=? where id=?", DBType.LONG, DBType.LONG);
            final List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class),
                    "select hs.id, l.eduprogramsubject_id, s.subjectindex_id from educationlevelshighschool_t hs " +
                            " join educationlevels_t l on l.id=hs.educationlevel_id " +
                            " join edu_c_pr_subject_t s on s.id=l.eduprogramsubject_id" +
                            " where hs.assignedqualification_id is null " +
                            "       or not exists(select r.id from edu_c_pr_subject_qual_t r where r.programsubject_id=l.eduprogramsubject_id and r.programqualification_id=hs.assignedqualification_id)"
            );
            for (final Object[] row : rows) {
                final Long hsId = (Long) row[0];
                final Long subjId = (Long) row[1];
                final Long indexId = (Long) row[2];

                final Long qId;
                final Set<Long> subjQualifications = relMap.get(subjId);
                if (!subjQualifications.isEmpty()) {
                    // Берем первую попавшуюся связь направления с квалификацией, если такие есть для направления
                    qId = subjQualifications.iterator().next();
                } else {
                    // Берем квалификацию по диплому, ищем такую же по названию. Если не нашли, создаем фэйк в рамках перечня.
                    qId = addQualificationFunction.apply(indexId, diplomaQualificationMap.get(hsId));
                    // Создаем связь с направлением, если еще нет
                    addRelConsumer.accept(subjId, qId);
                }

                upd.addBatch(qId, hsId);
            }
            updaters.add(upd);
        }

        // Блок 2. В учебных планах не указаны квалификации.
        // Блок 4. Квалификация УП отсутствует в списке разрешенных квалификаций направления подготовки УП.
        if (tool.tableExists("epp_eduplan_prof_t"))
        {
            final BatchUpdater upd = new BatchUpdater("update epp_eduplan_prof_t set programqualification_id=? where id=?", DBType.LONG, DBType.LONG);
            final List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class),
                    "select p.id, p.programsubject_id, s.subjectindex_id from epp_eduplan_prof_t p " +
                            " join edu_c_pr_subject_t s on s.id=p.programsubject_id" +
                            " where p.programqualification_id is null " +
                            "       or not exists(select r.id from edu_c_pr_subject_qual_t r where r.programsubject_id=p.programsubject_id and r.programqualification_id=p.programqualification_id)"
            );
            for (final Object[] row : rows) {
                final Long planId = (Long) row[0];
                final Long subjId = (Long) row[1];
                final Long indexId = (Long) row[2];

                final Long qId;
                final Set<Long> subjQualifications = relMap.get(subjId);
                if (!subjQualifications.isEmpty()) {
                    qId = subjQualifications.iterator().next();
                } else {
                    qId = addQualificationFunction.apply(indexId, null);
                    addRelConsumer.accept(subjId, qId);
                }
                upd.addBatch(qId, planId);
            }
            updaters.add(upd);
        }

        // Блок 3. Квалификация ОП отсутствует в списке разрешенных квалификаций направления подготовки ОП.
        for (final String prog_table : new String[]{"edu_program_prof_higher_t", "edu_program_prof_secondary_t"})
        {
            final BatchUpdater upd = new BatchUpdater("update "+prog_table+" set programqualification_id=? where id=?", DBType.LONG, DBType.LONG);
            final List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class),
                    "select t.id, p.programsubject_id, s.subjectindex_id from "+prog_table+" t " +
                            " join edu_program_prof_t p on p.id=t.id" +
                            " join edu_c_pr_subject_t s on s.id=p.programsubject_id" +
                            " where t.programqualification_id is null " +
                            "       or not exists(select r.id from edu_c_pr_subject_qual_t r where r.programsubject_id=p.programsubject_id and r.programqualification_id=t.programqualification_id)"
            );
            for (final Object[] row : rows) {
                final Long progId = (Long) row[0];
                final Long subjId = (Long) row[1];
                final Long indexId = (Long) row[2];

                final Long qId;
                final Set<Long> subjQualifications = relMap.get(subjId);
                if (!subjQualifications.isEmpty()) {
                    qId = subjQualifications.iterator().next();
                } else {
                    qId = addQualificationFunction.apply(indexId, null);
                    addRelConsumer.accept(subjId, qId);
                }
                upd.addBatch(qId, progId);
            }
            updaters.add(upd);
        }

        qualIns.executeInsert(tool, "edu_c_pr_qual_t");
        relsIns.executeInsert(tool, "edu_c_pr_subject_qual_t");

        for (final BatchUpdater updater : updaters) {
            updater.executeUpdate(tool);
        }



        // Блок 7. Для параметров выпуска студентов по направлению подготовки (НПв) задано не уникальное название.
        // Блок 8. Для параметров выпуска студентов по направлению подготовки (НПв) задано не уникальное сокращенное название.
        for (final String column : new String[]{"title_p", "shorttitle_p"}) {

            final String sql = "select hs.id, " + column + " from educationlevelshighschool_t hs " +
                    " join (select t.id rid, count(*) over(partition by educationlevel_id, assignedqualification_id, orgunit_id, "+column+") cnt from educationlevelshighschool_t t) g on g.rid=hs.id" +
                    " where g.cnt > 1";

            List<Object[]> rows;
            do {
                rows = tool.executeQuery(processor(Long.class, String.class), sql);
                if (!rows.isEmpty()) {
                    final BatchUpdater upd = new BatchUpdater("update educationlevelshighschool_t set " + column + "=? where id=?", DBType.EMPTY_STRING, DBType.LONG);
                    for (int i = 0; i < rows.size(); i++) {
                        final Object[] row = rows.get(i);
                        final Long hsId = (Long) row[0];
                        final String value = (String) row[1];
                        upd.addBatch(value + "_" + (i+1), hsId);
                    }
                    upd.executeUpdate(tool);
                }
            } while (!rows.isEmpty());
        }

        return true;
    }

    @Override
    public void run(DBTool tool) throws Exception {
        if (canAutoFix()) {
            System.out.println("============= Start auto fix qualifications ==============");
            autoFix(tool);
            System.out.println("============= Finish auto fix qualifications ==============");
        }
    }
}