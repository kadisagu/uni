/* $Id$ */
package ru.tandemservice.uni.catalog.bo.DevelopPeriod.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

/**
 * @author Alexey Lopatin
 * @since 11.09.2015
 */
public interface IDevelopPeriodDao extends INeedPersistenceSupport
{
    EduProgramDuration getEduProgramDuration(Integer numberOfYears, Integer numberOfMonths, Integer numberOfHours);

    EduProgramDuration getEduProgramDurationOrNull(Integer numberOfYears, Integer numberOfMonths, Integer numberOfHours);

}
