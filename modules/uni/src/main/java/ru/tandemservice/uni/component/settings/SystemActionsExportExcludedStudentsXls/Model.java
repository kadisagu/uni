/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsExportExcludedStudentsXls;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.Date;
import java.util.List;

/**
 * @author vnekrasov
 * @since 04.03.2014
 */
public class Model
{


    private ISelectModel _educationLevelTypeListModel;
    private List<StructureEducationLevels> _educationLevelTypeList;

    public Date getDateFrom() {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        _dateFrom = dateFrom;
    }

    private Date _dateFrom;

    public Date getDateTo() {
        return _dateTo;
    }

    public void setDateTo(Date dateTo) {
        _dateTo = dateTo;
    }

    private Date _dateTo;

        public ISelectModel getEducationLevelTypeListModel()
        {
            return _educationLevelTypeListModel;
        }

        public void setEducationLevelTypeListModel(ISelectModel educationLevelTypeListModel)
        {
            _educationLevelTypeListModel = educationLevelTypeListModel;
        }

        public List<StructureEducationLevels> getEducationLevelTypeList()
        {
            return _educationLevelTypeList;
        }

        public void setEducationLevelTypeList(List<StructureEducationLevels> educationLevelTypeList)
        {
            _educationLevelTypeList = educationLevelTypeList;
        }
}
