package ru.tandemservice.uni.migration;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x5_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность developTech

		// создано свойство programTrait
		{
			// создать колонку
			tool.createColumn("developtech_t", new DBColumn("programtrait_id", DBType.LONG));
		}

		// создано свойство userCode
		{
			// создать колонку
			tool.createColumn("developtech_t", new DBColumn("usercode_p", DBType.createVarchar(255)));
		}

		// создано свойство disabledDate
		{
			// создать колонку
			tool.createColumn("developtech_t", new DBColumn("disableddate_p", DBType.TIMESTAMP));
		}

		//  свойство title стало обязательным
		{
			// сделать колонку NOT NULL
			tool.setColumnNullable("developtech_t", "title_p", false);
		}

        // восстанавливаем недостающие элементы
        short developTechEntityCode = tool.entityCodes().ensure("developTech");

        Map<String, Object[]> missItemsMap = new HashMap<>(1);
        missItemsMap.put("2", new Object[]{ "Дистанционная", "дист", "д" });                    // DevelopTechCodes.DISTANCE
        missItemsMap.put("3", new Object[]{ "Дистанционная электронная", "дист.эл.", "э" });    // DevelopTechCodes.ELECTRONIC_DISTANCE

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select code_p from developtech_t");
        ResultSet rs = stmt.getResultSet();
        Collection<String> itemCodes = new ArrayList<>();
        while (rs.next())
            itemCodes.add(rs.getString(1));

        Collection<String> missItemCodes = CollectionUtils.subtract(missItemsMap.keySet(), itemCodes);
        PreparedStatement insertMissItemsStmt = tool.getConnection().prepareStatement("insert into developtech_t (id, discriminator, code_p, title_p, shorttitle_p, group_p) values (?, ?, ?, ?, ?, ?)");
        insertMissItemsStmt.setShort(2, developTechEntityCode);

        for (String missItemCode : missItemCodes)
        {
            Object[] values = missItemsMap.get(missItemCode);
            insertMissItemsStmt.setLong(1, EntityIDGenerator.generateNewId(developTechEntityCode));
            insertMissItemsStmt.setString(3, missItemCode);
            insertMissItemsStmt.setString(4, (String) values[0]); // title_p
            insertMissItemsStmt.setString(5, (String) values[1]); // shorttitle_p
            insertMissItemsStmt.setString(6, (String) values[2]); // group_p

            insertMissItemsStmt.execute();
        }

        // проставляем связь между develop tech и program trait
        Map<String, String> techCode2traitCodeMap = new HashMap<>(2);
        {
            techCode2traitCodeMap.put("1", null);   // DevelopTechCodes.GENERAL
            techCode2traitCodeMap.put("2", "1");    // DevelopTechCodes.DISTANCE
            techCode2traitCodeMap.put("3", "2");    // DevelopTechCodes.ELECTRONIC_DISTANCE
        }

        PreparedStatement updateTraitCodeStmt = tool.getConnection().prepareStatement("update developtech_t set programtrait_id = (select id from eduprogramtrait_t where code_p = ?) where code_p = ?");
        for (Map.Entry<String, String> techEntry: techCode2traitCodeMap.entrySet())
        {
            updateTraitCodeStmt.setString(1, techEntry.getValue());
            updateTraitCodeStmt.setString(2, techEntry.getKey());

            updateTraitCodeStmt.execute();
        }

        // устанавливаем дату запрещения для удаленных элементов
        PreparedStatement updateDisabledDate = tool.getConnection().prepareStatement("update developtech_t set disableddate_p = ? where code_p = ?");
        updateDisabledDate.setDate(1, new java.sql.Date(new java.util.Date().getTime()));

        for (String missItemCode: missItemCodes)
        {
            updateDisabledDate.setString(2, missItemCode);
            updateDisabledDate.execute();
        }
    }
}