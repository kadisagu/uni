/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 29.01.2016
 */
public interface ISessionTransferProtocolProperty
{
    /**
     * @return Номер протокола.
     */
    String getNumber();

    /**
     * @return Дата протокола.
     */
    Date getProtocolDate();
}
