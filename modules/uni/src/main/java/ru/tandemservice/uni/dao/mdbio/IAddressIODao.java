package ru.tandemservice.uni.dao.mdbio;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressStreet;

import org.tandemframework.core.util.cache.SpringBeanCache;

import com.healthmarketscience.jackcess.Database;
import org.tandemframework.shared.fias.base.entity.ICitizenship;

import java.io.IOException;

/**
 * @author vdanilov
 */
public interface IAddressIODao {
    SpringBeanCache<IAddressIODao> instance = new SpringBeanCache<>(IAddressIODao.class.getName());

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_addressCountry(Database mdb) throws IOException;

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_addressSettlement(Database mdb) throws IOException;


    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    AddressCountry findCountry(String countryString);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    ICitizenship findCitizenship(String citizenshipString);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    AddressItem findSettlement(AddressCountry country, String settlementString, boolean require);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    AddressStreet findStreet(AddressItem settlement, String streetString, boolean require);

}
