/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Collection;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
public interface IUniReorganizationDao extends INeedPersistenceSupport {

    public void doChangeEducationOrgUnitOwner(Collection<EducationOrgUnit> input, OrgUnit owner);

}
