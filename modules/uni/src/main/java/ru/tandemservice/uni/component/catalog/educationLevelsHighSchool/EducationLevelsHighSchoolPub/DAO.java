/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.*;

/**
 * @author vip_delete
 */
public class DAO extends DefaultCatalogPubDAO<EducationLevelsHighSchool, Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderRegistry = new OrderDescriptionRegistry("ci");

    static
    {
        _orderRegistry.setOrders(EducationLevelsHighSchool.ORGUNIT_KEY, new OrderDescription("ci", new String[]{EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription("ci", new String[]{EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.L_ORG_UNIT_TYPE, OrgUnitType.P_TITLE}));
        _orderRegistry.setOrders(EducationLevelsHighSchool.EDUCATION_LEVELS_KEY, new OrderDescription("ci", new String[]{EducationLevelsHighSchool.L_EDUCATION_LEVEL, EducationLevels.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        prepareStructureMap(model);
        model.setLevelTopList(this.<StructureEducationLevels, StructureEducationLevels>getList(StructureEducationLevels.class, StructureEducationLevels.parent(), (StructureEducationLevels) null, StructureEducationLevels.priority().s()));
        prepareLevelTypeModel(model);
        model.setOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        model.setQualificationList(getList(Qualifications.class, Qualifications.P_CODE));
    }

    @Override
    public void prepareLevelTypeModel(Model model)
    {
        if (null == model.getSettings())
        {
            model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(Collections.<StructureEducationLevels>emptyList(), true));
            return;
        }
        StructureEducationLevels levelTop = (StructureEducationLevels) model.getSettings().get("levelTop");
        if (null == levelTop)
        {
            model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(Collections.<StructureEducationLevels>emptyList(), true));
            return;
        }
        if (null == model.getStructureMap())
            prepareStructureMap(model);
        ArrayList<StructureEducationLevels> list = new ArrayList<StructureEducationLevels>(model.getStructureMap().get(levelTop));
        Collections.sort(list, StructureEducationLevels.COMPARATOR);
        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(list, true));
    }

    private void prepareStructureMap(Model model)
    {
        List<StructureEducationLevels> baseList = getList(StructureEducationLevels.class);
        Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap = new HashMap<StructureEducationLevels, Set<StructureEducationLevels>>();
        for (StructureEducationLevels level : baseList)
            SafeMap.safeGet(structureMap, getRoot(level), HashSet.class).add(level);
        model.setStructureMap(structureMap);
    }

    private StructureEducationLevels getRoot(StructureEducationLevels item)
    {
        StructureEducationLevels root = item;
        while (root.getParent() != null)
            root = root.getParent();
        return root;
    }

    @Override
    public void prepareListItemDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(model.getItemClass().getName(), "ci");

        applyFilters(model, builder);

        getOrderDescriptionRegistry().applyOrder(builder, model.getDataSource().getEntityOrder());

        List<EducationLevelsHighSchool> list = builder.getResultList(getSession());

        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    protected OrderDescriptionRegistry getOrderDescriptionRegistry()
    {
        return _orderRegistry;
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void applyFilters(Model model, MQBuilder builder)
    {
        super.applyFilters(model, builder);

        StructureEducationLevels levelTop = (StructureEducationLevels) model.getSettings().get("levelTop");
        StructureEducationLevels levelType = (StructureEducationLevels) model.getSettings().get("levelType");
        String subjectCode = StringUtils.trimToNull((String) model.getSettings().get("subjectCode"));
        ICatalogItem qualification = (ICatalogItem) model.getSettings().get("qualification");
        OrgUnit orgUnit = (OrgUnit) model.getSettings().get("orgUnit");

        if (levelTop == null)
        {
            builder.add(MQExpression.eq("ci", "id", -1L));
            return;
        }

        if (levelType != null)
            builder.add(MQExpression.eq("ci", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE, levelType));
        else
        {
            if (null == model.getStructureMap())
                prepareStructureMap(model);
            builder.add(MQExpression.in("ci", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE, model.getStructureMap().get(levelTop)));
        }
        if (subjectCode != null)
        {
            Set<Long> ids = new HashSet<Long>();
            for (EducationLevelsHighSchool educationLevelsHighSchool : getList(EducationLevelsHighSchool.class)) {
                if (subjectCode.equals(educationLevelsHighSchool.getEducationLevel().getInheritedOkso())) {
                    ids.add(educationLevelsHighSchool.getId());
                }

                String titleCodePrefix = educationLevelsHighSchool.getEducationLevel().getTitleCodePrefix();
                if ((null != titleCodePrefix) && titleCodePrefix.startsWith(subjectCode)) {
                    ids.add(educationLevelsHighSchool.getId());
                }
            }

            builder.add(MQExpression.in("ci", EducationLevelsHighSchool.P_ID, ids));
        }
        if (qualification != null)
        {
            Set<Long> ids = new HashSet<Long>();
            for (EducationLevelsHighSchool educationLevelsHighSchool : getList(EducationLevelsHighSchool.class))
                if (qualification.getCode().equals(educationLevelsHighSchool.getEducationLevel().getSafeQCode()))
                    ids.add(educationLevelsHighSchool.getId());

            builder.add(MQExpression.in("ci", EducationLevelsHighSchool.P_ID, ids));
        }
        if (orgUnit != null)
            builder.add(MQExpression.eq("ci", EducationLevelsHighSchool.L_ORG_UNIT, orgUnit));
    }
}