/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.GroupTitleManagement;

import org.tandemframework.core.component.Output;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;

/**
 * @author vip_delete
 */
@Output(keys = {"algorithmId"}, bindings = {"algorithmId"})
public class Model
{
    private DynamicListDataSource<GroupTitleAlgorithm> _dataSource;
    private Long _algorithmId;

    public DynamicListDataSource<GroupTitleAlgorithm> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<GroupTitleAlgorithm> dataSource)
    {
        _dataSource = dataSource;
    }

    public Long getAlgorithmId()
    {
        return _algorithmId;
    }

    public void setAlgorithmId(Long algorithmId)
    {
        _algorithmId = algorithmId;
    }
}
