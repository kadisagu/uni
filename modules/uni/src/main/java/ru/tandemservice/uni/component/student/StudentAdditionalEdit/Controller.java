/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentAdditionalEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);

        if (!model.getStudent().isTargetAdmission())
            model.getStudent().setTargetAdmissionOrgUnit(null);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();
        dao.validate(model, errorCollector);
        
        if (!errorCollector.hasErrors())
        {
            dao.update(model);
            deactivate(component);
        }
    }

    public void onChangeSelect(IBusinessComponent context)
    {
        Model model = getModel(context);
        getDao().prepare(model);
    }

    public void onChangeAdvisor(IBusinessComponent context) {
        Model model = getModel(context);
        // При выборе другого руководителя поле "Руководитель ВКР (текстом)" заполнится исходя из поля "Руководитель ВКР"
        PpsEntry advisor = model.getStudent().getPpsEntry();
        model.getStudent().setFinalQualifyingWorkAdvisor(advisor == null ? "" : advisor.getTitleFioInfoOrgUnitDegreesStatuses());
    }
}
