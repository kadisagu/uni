/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.List;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport;
import ru.tandemservice.unibase.UniBaseUtils;
//import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;

/**
 * @author agolubenko
 * @since 18.05.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setSettings(component.getSettings());
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<StudentsAgeSexDistributionReport> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", StudentsAgeSexDistributionReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", StudentsAgeSexDistributionReport.P_FORMATIVE_ORG_UNIT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", StudentsAgeSexDistributionReport.P_TERRITORIAL_ORG_UNIT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", StudentsAgeSexDistributionReport.P_DEVELOP_FORM).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", StudentsAgeSexDistributionReport.P_DEVELOP_CONDITITION).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", StudentsAgeSexDistributionReport.P_DEVELOP_TECH).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", StudentsAgeSexDistributionReport.P_QUALIFICATION).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Категория обучаемого", StudentsAgeSexDistributionReport.P_STUDENT_CATEGORY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", StudentsAgeSexDistributionReport.P_STUDENT_STATUS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", StudentsAgeSexDistributionReport.P_FORMING_DATE).setPermissionKey("deleteGlobalStoredReport"));
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.Add"));
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer((Long) component.getListenerParameter()), true);
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().clear();
        onClickSearch(component);
    }
}
