/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.report.xls.lazy;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.format.CellFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * Класс для генерации сложных файлов в формате xls.
 * Шаблон при генерации не используется. Возможен только импорт стилей из него.
 * Все таблицы печатаются в новой книге,
 * на соответствующих листах от верхнего левого угла.
 * Модификаторы листов предназначены для финальной обработки - merge,
 * задание ширины колонок и высоты рядов, и тому подобное
 * форматирование.
 *
 * @author oleyba
 * @since 10.04.2009
 */
public class LazyReportGenerator
{
    private CellFormatFactory factory = new CellFormatFactory();

    ByteArrayOutputStream _outputStream = new ByteArrayOutputStream();
    private WritableWorkbook workbook;
    private Workbook template;

    private List<Sheet> sheets = new ArrayList<Sheet>();

    public LazyReportGenerator() throws Exception
    {
        workbook = Workbook.createWorkbook(_outputStream);
    }

    public LazyReportGenerator(Workbook template) throws Exception
    {
        workbook = Workbook.createWorkbook(_outputStream);
        this.template = template;
    }

    /**
     * Добавляет лист.
     * @param number - порядковый номер
     * @param name - название
     * @param cells - массив ячеек
     */
    public void addSheet(int number, String name, LazyCell[][] cells)
    {
        WritableSheet writableSheet = workbook.createSheet(name, number);
        sheets.add(new Sheet(writableSheet, cells));
    }

    /**
     * Добавляет лист.
     * @param number - порядковый номер
     * @param name - название
     * @param cells - массив ячеек
     * @param modifier - модификатор, метод apply запускается после заполнения листа по массиву ячеек
     */
    public void addSheet(int number, String name, LazyCell[][] cells, ILazyModifier modifier)
    {
        WritableSheet writableSheet = workbook.createSheet(name, number);
        final Sheet sheet = new Sheet(writableSheet, cells);
        sheet._modifier = modifier;
        sheets.add(sheet);
    }

    /**
     * Добавляет формат для использования в ячейках листов.
     * @param key - строковый ключ формата
     * @param format - формат
     */
    public void addFormat(String key, WritableCellFormat format)
    {
        factory.registerFormat(key, format);
    }

    /**
     * Импортирует формат из книги-шаблона, переданной в конструкторе.
     * Ключ - название ячейки в книге-шаблоне.
     * Ключ импортированного формата можно использовать в ячейках листов.
     * @param key - строковый ключ формата
     * @throws Exception - если книга-шаблон в генераторе не инициализирована.
     */
    public void importFormat(String key) throws Exception
    {
        if (template == null)
            throw new Exception("Template workbook wasn't initialized.");
        CellFormat format = template.findCellByName(key).getCellFormat();
        if (format != null)
            factory.registerFormat(key, new WritableCellFormat(format));
        else
            factory.registerFormat(key, new WritableCellFormat());
    }

    /**
     * Устанавливает формат по умолчанию,
     * импортируя его из книги-шаблона по ключу (названию ячейки).
     * Этот формат будет использован
     * для тех ячеек листов, где формат не указан,
     * и для ячеек на позициях, для которых в массиве не заполнено значение.
     * @param key - строковый ключ формата
     * @throws Exception - если книга-шаблон в генераторе не инициализирована, или ячейка не найдена.
     */
    public void setDefaultFormat(String key) throws Exception
    {
        if (template == null)
            throw new Exception("Template workbook wasn't initialized.");
        CellFormat format = template.findCellByName(key).getCellFormat();
        if (format != null)
            factory.defaultFormat = new WritableCellFormat(format);
    }

    /**
     * Устанавливает формат по умолчанию.
     * Этот формат будет использован
     * для тех ячеек листов, где формат не указан,
     * и для ячеек на позициях, для которых в массиве не заполнено значение.
     * @param format - формат
     * @throws Exception - если книга-шаблон в генераторе не инициализирована.
     */
    public void setDefaultFormat(WritableCellFormat format) throws Exception
    {
        if (format == null)
            return;
        factory.defaultFormat = format;
    }

    /**
     * Добавляет шрифт для использования в ячейках листов.
     * @param key - строковый ключ шрифта
     * @param font - шрифт
     */
    public void addFont(String key, WritableFont font)
    {
        factory.registerFont(key, font);
    }

    /**
     * Импортирует шрифт из книги-шаблона, переданной в конструкторе.
     * Ключ - название ячейки в книге-шаблоне.
     * Ключ импортированного шрифта можно использовать в ячейках листов.
     * @param key - строковый ключ шрифта
     * @throws Exception - если книга-шаблон в генераторе не инициализирована, или ячейка не найдена.
     */
    public void importFont(String key) throws Exception
    {
        if (template == null)
            throw new Exception("Template workbook wasn't initialized.");
        factory.registerFont(key, new WritableFont(template.findCellByName(key).getCellFormat().getFont()));
    }

    /**
     * Генерирует файл.
     * @return файл
     * @throws Exception - ошибки из библиотеки напрямую.
     */
    public byte[] generateContent() throws Exception
    {
        for (Sheet sheet : sheets)
            sheet.printAll(factory);
        workbook.write();
        workbook.close();
        return _outputStream.toByteArray();
    }
}
