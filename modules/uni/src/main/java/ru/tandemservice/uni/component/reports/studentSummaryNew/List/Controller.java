/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentSummaryNew.List;

import com.google.common.base.Objects;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.report.StudentSummaryNewReport;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author dseleznev
 * @since 14.03.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<StudentSummaryNewReport> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата формирования", StudentSummaryNewReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Форма освоения", StudentSummaryNewReport.P_DEVELOP_FORM).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", StudentSummaryNewReport.P_DEVELOP_CONDITION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", StudentSummaryNewReport.P_QUALIFICATION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Категория обучаемого", StudentSummaryNewReport.P_STUDENT_CATEGORY).setOrderable(false).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", StudentSummaryNewReport.P_FORMING_DATE).setPermissionKey(model.getDeleteKey()));
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uni.component.reports.studentSummaryNew.Add.Model.class.getPackage().getName(), new ParametersMap().add("orgUnitId", getModel(component).getOrgUnitId())));
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        final IStorableReport report = UniDaoFacade.getCoreDao().getNotNull((Long) component.getListenerParameter());
        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(CommonBaseUtil.getContentType(report.getContent(), DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL))
            .fileName(Objects.firstNonNull(report.getContent().getFilename(), "StudentSummaryNewReport.xls"))
            .document(content), true);
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}