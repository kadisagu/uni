/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentPassportExpiredReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author Denis Katkov
 * @since 10.06.2016
 */
@Configuration
public class StudentPassportExpiredReportAdd extends BusinessComponentManager
{
    public static final String STUDENT_STATUS_DS = "studentStatusDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(STUDENT_STATUS_DS, getName(), StudentStatus.defaultSelectDSHandler(getName())))
                .create();
    }


}
