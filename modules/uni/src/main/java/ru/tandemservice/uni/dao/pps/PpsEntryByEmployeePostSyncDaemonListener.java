package ru.tandemservice.uni.dao.pps;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;

import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;

/**
 * @author vdanilov
 */
public class PpsEntryByEmployeePostSyncDaemonListener implements IHibernateEventListener<ISingleEntityEvent>
{

    private static final TransactionCompleteAction<Void> action = new TransactionCompleteAction<Void>() {
        @Override public void afterCompletion(Session session, int status, Void beforeCompleteResult) {
            // именно по зваершению транзакции нужно все пересчитать
            PpsEntryByEmployeePostSynchronizer.DAEMON.wakeUpDaemon();
        };
    };

    @Override
    public void onEvent(ISingleEntityEvent event) {
        if (check(event)) {
            action.register(event.getSession());
        }
    }

    private boolean check(ISingleEntityEvent event) {
        final IEntity entity = event.getEntity();
        return ((entity instanceof EmployeePost) || (entity instanceof Employee) || (entity instanceof PostBoundedWithQGandQL));
    }




}
