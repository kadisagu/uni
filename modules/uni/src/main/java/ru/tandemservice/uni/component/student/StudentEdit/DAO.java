/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentEdit;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setPerson(model.getStudent().getPerson());
        model.setCompensationTypesList(getCatalogItemList(CompensationType.class));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, model.getPrincipalContext()));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model));

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));
        model.setDefaultDevelopPeriodModel(new LazySimpleSelectModel<>(getList(DevelopPeriod.class, DevelopPeriod.enabled(), Boolean.TRUE, DevelopPeriod.priority().s())));

        EducationOrgUnit educationOrgUnit = model.getStudent().getEducationOrgUnit();

        model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
        model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
        model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        model.setDevelopForm(educationOrgUnit.getDevelopForm());
        model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
        model.setDevelopTech(educationOrgUnit.getDevelopTech());
        model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());

        List<PersonEduDocument> personEduDocuments = getList(PersonEduDocument.class, PersonEduDocument.person(), model.getStudent().getPerson(), PersonEduDocument.issuanceDate().s(), PersonEduDocument.creationDate().s());
        Collections.reverse(personEduDocuments);
        model.setEduDocumentModel(new LazySimpleSelectModel<>(personEduDocuments));
    }

    @Override
    public void update(Model model)
    {
        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);
        student.setEducationOrgUnit(educationOrgUnit);
        update(student);
    }
    
    public static boolean checkStudentBookNumber(Student student, Session session)
    {
        Criteria criteria = session.createCriteria(Student.class);
        criteria.add(Restrictions.eq(Student.P_BOOK_NUMBER, student.getBookNumber()));
        criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));
        criteria.add(Restrictions.ne(Student.P_ID, student.getId()));
        criteria.setProjection(Projections.rowCount());
        return ((Number) criteria.uniqueResult()).intValue() == 0;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (!checkStudentBookNumber(model.getStudent(), getSession()))
            errors.add("Указанный номер зачетной книжки уже закреплен за другим студентом", "bookNumber");


        Student otherStudent = new DQLSelectBuilder().fromEntity(Student.class, "s").column("s")
            .where(eq(property("s", Student.P_PERSONAL_NUMBER), value(model.getStudent().getPersonalNumber())))
            .where(ne(property("s"), value(model.getStudent().getId())))
            .createStatement(getSession()).uniqueResult();

        if (otherStudent != null)
        {
            errors.add("Указанный личный номер уже занят студентом «" + otherStudent.getFullFio() + "».", "personalNumber");
        }

    }
}
