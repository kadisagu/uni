/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Alexey Lopatin
 * @since 23.09.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uni_2x10x8_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
		        new ScriptDependency("org.tandemframework", "1.6.18"),
		        new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        deleteMissPrincipalContext(tool, "ppsEntry", "pps_entry_base_t");
        deleteMissPrincipalContext(tool, "ppsEntryByEmployeePost", "pps_entry_employeepost_t");

        if (tool.tableExists("pps_entry_timeworker_t"))
        {
            deleteMissPrincipalContext(tool, "ppsEntryByTimeworker", "pps_entry_timeworker_t");
        }
    }

    private void deleteMissPrincipalContext(DBTool tool, String entityName, String tableName) throws Exception
    {
        short entityCode = tool.entityCodes().ensure(entityName);

        List<Object[]> rows = tool.executeQuery(
                processor(Long.class),
                "select pr.id, pps.id from personrole_t pr " +
                        "left join " + tableName + " pps on pps.id = pr.id " +
                        "where pr.discriminator = ? and pps.id is null", entityCode
        );
        List<Long> deleteIds = rows.stream().map(row -> (Long) row[0]).collect(Collectors.toList());
        int count = MigrationUtils.massDeleteByIds(tool, "personrole_t", deleteIds);
        tool.info(count + " rows (" + entityName + ") removed by personrole_t");
    }
}
