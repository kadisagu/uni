/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3Pub;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;

/**
 * @author oleyba
 * @since 5/11/11
 */
public interface IDAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.IDAO<EducationLevelHighGos3, Model>
{
}
