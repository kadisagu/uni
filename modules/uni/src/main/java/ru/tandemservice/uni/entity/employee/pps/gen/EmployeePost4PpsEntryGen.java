package ru.tandemservice.uni.entity.employee.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сотрудник для записи в реестре ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeePost4PpsEntryGen extends EntityBase
 implements INaturalIdentifiable<EmployeePost4PpsEntryGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry";
    public static final String ENTITY_NAME = "employeePost4PpsEntry";
    public static final int VERSION_HASH = -1410758604;
    private static IEntityMeta ENTITY_META;

    public static final String L_PPS_ENTRY = "ppsEntry";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_REMOVAL_DATE = "removalDate";

    private PpsEntryByEmployeePost _ppsEntry;     // Запись в реестре ППС (на базе сотрудника)
    private EmployeePost _employeePost;     // Сотрудник
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись в реестре ППС (на базе сотрудника). Свойство не может быть null.
     */
    @NotNull
    public PpsEntryByEmployeePost getPpsEntry()
    {
        return _ppsEntry;
    }

    /**
     * @param ppsEntry Запись в реестре ППС (на базе сотрудника). Свойство не может быть null.
     */
    public void setPpsEntry(PpsEntryByEmployeePost ppsEntry)
    {
        dirty(_ppsEntry, ppsEntry);
        _ppsEntry = ppsEntry;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeePost4PpsEntryGen)
        {
            if (withNaturalIdProperties)
            {
                setPpsEntry(((EmployeePost4PpsEntry)another).getPpsEntry());
                setEmployeePost(((EmployeePost4PpsEntry)another).getEmployeePost());
            }
            setRemovalDate(((EmployeePost4PpsEntry)another).getRemovalDate());
        }
    }

    public INaturalId<EmployeePost4PpsEntryGen> getNaturalId()
    {
        return new NaturalId(getPpsEntry(), getEmployeePost());
    }

    public static class NaturalId extends NaturalIdBase<EmployeePost4PpsEntryGen>
    {
        private static final String PROXY_NAME = "EmployeePost4PpsEntryNaturalProxy";

        private Long _ppsEntry;
        private Long _employeePost;

        public NaturalId()
        {}

        public NaturalId(PpsEntryByEmployeePost ppsEntry, EmployeePost employeePost)
        {
            _ppsEntry = ((IEntity) ppsEntry).getId();
            _employeePost = ((IEntity) employeePost).getId();
        }

        public Long getPpsEntry()
        {
            return _ppsEntry;
        }

        public void setPpsEntry(Long ppsEntry)
        {
            _ppsEntry = ppsEntry;
        }

        public Long getEmployeePost()
        {
            return _employeePost;
        }

        public void setEmployeePost(Long employeePost)
        {
            _employeePost = employeePost;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EmployeePost4PpsEntryGen.NaturalId) ) return false;

            EmployeePost4PpsEntryGen.NaturalId that = (NaturalId) o;

            if( !equals(getPpsEntry(), that.getPpsEntry()) ) return false;
            if( !equals(getEmployeePost(), that.getEmployeePost()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPpsEntry());
            result = hashCode(result, getEmployeePost());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPpsEntry());
            sb.append("/");
            sb.append(getEmployeePost());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeePost4PpsEntryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeePost4PpsEntry.class;
        }

        public T newInstance()
        {
            return (T) new EmployeePost4PpsEntry();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ppsEntry":
                    return obj.getPpsEntry();
                case "employeePost":
                    return obj.getEmployeePost();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ppsEntry":
                    obj.setPpsEntry((PpsEntryByEmployeePost) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ppsEntry":
                        return true;
                case "employeePost":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ppsEntry":
                    return true;
                case "employeePost":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ppsEntry":
                    return PpsEntryByEmployeePost.class;
                case "employeePost":
                    return EmployeePost.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeePost4PpsEntry> _dslPath = new Path<EmployeePost4PpsEntry>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeePost4PpsEntry");
    }
            

    /**
     * @return Запись в реестре ППС (на базе сотрудника). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry#getPpsEntry()
     */
    public static PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost> ppsEntry()
    {
        return _dslPath.ppsEntry();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends EmployeePost4PpsEntry> extends EntityPath<E>
    {
        private PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost> _ppsEntry;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись в реестре ППС (на базе сотрудника). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry#getPpsEntry()
     */
        public PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost> ppsEntry()
        {
            if(_ppsEntry == null )
                _ppsEntry = new PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost>(L_PPS_ENTRY, this);
            return _ppsEntry;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EmployeePost4PpsEntryGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return EmployeePost4PpsEntry.class;
        }

        public String getEntityName()
        {
            return "employeePost4PpsEntry";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
