package ru.tandemservice.uni.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состав студентов по возрасту и полу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentsAgeSexDistributionReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport";
    public static final String ENTITY_NAME = "studentsAgeSexDistributionReport";
    public static final int VERSION_HASH = 54729109;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITITION = "developConditition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_STUDENT_STATUS = "studentStatus";

    private String _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _developForm;     // Форма освоения
    private String _developConditition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _qualification;     // Квалификация
    private String _studentCategory;     // Категория обучаемого
    private String _studentStatus;     // Состояние

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditition()
    {
        return _developConditition;
    }

    /**
     * @param developConditition Условие освоения.
     */
    public void setDevelopConditition(String developConditition)
    {
        dirty(_developConditition, developConditition);
        _developConditition = developConditition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Категория обучаемого.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Состояние.
     */
    @Length(max=255)
    public String getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Состояние.
     */
    public void setStudentStatus(String studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentsAgeSexDistributionReportGen)
        {
            setFormativeOrgUnit(((StudentsAgeSexDistributionReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((StudentsAgeSexDistributionReport)another).getTerritorialOrgUnit());
            setDevelopForm(((StudentsAgeSexDistributionReport)another).getDevelopForm());
            setDevelopConditition(((StudentsAgeSexDistributionReport)another).getDevelopConditition());
            setDevelopTech(((StudentsAgeSexDistributionReport)another).getDevelopTech());
            setQualification(((StudentsAgeSexDistributionReport)another).getQualification());
            setStudentCategory(((StudentsAgeSexDistributionReport)another).getStudentCategory());
            setStudentStatus(((StudentsAgeSexDistributionReport)another).getStudentStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentsAgeSexDistributionReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentsAgeSexDistributionReport.class;
        }

        public T newInstance()
        {
            return (T) new StudentsAgeSexDistributionReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "developForm":
                    return obj.getDevelopForm();
                case "developConditition":
                    return obj.getDevelopConditition();
                case "developTech":
                    return obj.getDevelopTech();
                case "qualification":
                    return obj.getQualification();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "studentStatus":
                    return obj.getStudentStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developConditition":
                    obj.setDevelopConditition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "developForm":
                        return true;
                case "developConditition":
                        return true;
                case "developTech":
                        return true;
                case "qualification":
                        return true;
                case "studentCategory":
                        return true;
                case "studentStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "developForm":
                    return true;
                case "developConditition":
                    return true;
                case "developTech":
                    return true;
                case "qualification":
                    return true;
                case "studentCategory":
                    return true;
                case "studentStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developConditition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "qualification":
                    return String.class;
                case "studentCategory":
                    return String.class;
                case "studentStatus":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentsAgeSexDistributionReport> _dslPath = new Path<StudentsAgeSexDistributionReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentsAgeSexDistributionReport");
    }
            

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getDevelopConditition()
     */
    public static PropertyPath<String> developConditition()
    {
        return _dslPath.developConditition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Категория обучаемого.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Состояние.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getStudentStatus()
     */
    public static PropertyPath<String> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    public static class Path<E extends StudentsAgeSexDistributionReport> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developConditition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<String> _studentStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getDevelopConditition()
     */
        public PropertyPath<String> developConditition()
        {
            if(_developConditition == null )
                _developConditition = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_DEVELOP_CONDITITION, this);
            return _developConditition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Категория обучаемого.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Состояние.
     * @see ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport#getStudentStatus()
     */
        public PropertyPath<String> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new PropertyPath<String>(StudentsAgeSexDistributionReportGen.P_STUDENT_STATUS, this);
            return _studentStatus;
        }

        public Class getEntityClass()
        {
            return StudentsAgeSexDistributionReport.class;
        }

        public String getEntityName()
        {
            return "studentsAgeSexDistributionReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
