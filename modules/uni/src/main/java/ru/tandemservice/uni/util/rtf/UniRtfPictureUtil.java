/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.util.rtf;

import java.io.IOException;

import org.tandemframework.rtf.document.text.picture.RtfPicture;
import org.tandemframework.rtf.util.RtfPictureUtil;

/**
 * @author agolubenko
 * @since 15.01.2009
 */
public class UniRtfPictureUtil
{
    public static RtfPicture createRtfPicture(String text, double height, int dpi) throws IOException {
        return RtfPictureUtil.createRtfBarcodePicture(text, height, dpi);
    }
}
