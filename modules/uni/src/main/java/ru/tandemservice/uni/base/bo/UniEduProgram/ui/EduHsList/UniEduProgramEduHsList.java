/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.UniEduProgramEduLevDSHandler;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

/**
 * @author oleyba
 * @since 8/12/14
 */
@Configuration
public class UniEduProgramEduHsList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .create();
    }
}