/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;

/**
 * @author nvankov
 * @since 3/25/13
 */
@Configuration
public class UniStudentCustomStateAddEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
                .create();
    }
}
