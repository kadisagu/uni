/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * @author Nikolay Fedorovskih
 * @since 15.03.2016
 */
@SuppressWarnings("unused")
public class MS_uni_2x9x3_4to5 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        final ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        final String prefix = tool.getDataSource().getSqlFunctionPrefix();
        final short rootSpecializationCode = tool.entityCodes().get("eduProgramSpecializationRoot");

        // Присваиваем название и сокращенное название НПм - для общей направленности берем из направления. Для не общей - из направленности.

        // СПО и общие направленности
        {
            final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
            upd.set("title_p", "s.title_p");
            upd.set("shorttitle_p", "s.shorttitle_p");
            upd.where("lv.eduprogramspecialization_id is null or (" + prefix + "getEntityCode(lv.eduprogramspecialization_id)=" + rootSpecializationCode + ")");
            upd.where("lv.title_p <> s.title_p or lv.shorttitle_p <> s.shorttitle_p");
            upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("edu_c_pr_subject_t", "s"), "s.id=lv.eduprogramsubject_id");
            final int n = tool.executeUpdate(translator.toSql(upd));
            if (n > 0) {
                tool.info(n + " rows in educationlevels_t update title and short title from edu_c_pr_subject_t");
            }
        }

        // необщие направленнсти
        {
            final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevels_t", "lv");
            upd.set("title_p", "sb.title_p");
            upd.set("shorttitle_p", "sb.shorttitle_p");
            upd.where("sb.discriminator<>" + rootSpecializationCode);
            upd.where("lv.title_p <> sb.title_p or lv.shorttitle_p <> sb.shorttitle_p");
            upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("edu_specialization_base_t", "sb"), "sb.id=lv.eduprogramspecialization_id");
            final int n = tool.executeUpdate(translator.toSql(upd));
            if (n > 0) {
                tool.info(n + " rows in educationlevels_t update title and short title from edu_specialization_base_t");
            }
        }
    }
}