/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsAdd.UniEduProgramEduHsAdd;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEdit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEditUI;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsPub.UniEduProgramEduHsPub;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;

/**
 * @author vip_delete
 */
public class Controller extends DefaultCatalogPubController<EducationLevelsHighSchool, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EducationLevelsHighSchool> dataSource = new DynamicListDataSource<>(context, this);

        // dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelsHighSchool.P_FULL_TITLE));
        dataSource.addColumn(new PublisherLinkColumn("Название", EducationLevelsHighSchool.P_FULL_TITLE).setResolver(new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) { return entity.getId(); }
            @Override public String getComponentName(final IEntity entity) { return UniEduProgramEduHsPub.class.getSimpleName(); }
        }));

        dataSource.addColumn(new SimpleColumn("Сокр. название", EducationLevelsHighSchool.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата открытия", EducationLevelsHighSchool.P_OPEN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата закрытия", EducationLevelsHighSchool.P_CLOSE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направления подготовки (специальности) Минобрнауки РФ", EducationLevelsHighSchool.EDUCATION_LEVELS_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", EducationLevelsHighSchool.ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", EducationLevelsHighSchool.QUALIFICATION_TITLE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Присваиваемая квалификация", EducationLevelsHighSchool.assignedQualification().title().s()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Направление ОП", EducationLevelsHighSchool.educationLevel().programSubjectWithCodeIndexAndGenTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", EducationLevelsHighSchool.educationLevel().programSpecializationTitle()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }

    public void onChangeLevelTop(IBusinessComponent context) {
        getDao().prepareLevelTypeModel(getModel(context));
    }

    @Override
    public void onClickClear(IBusinessComponent context) {
        final IDataSettings settings = getModel(context).getSettings();
        settings.clear();
        settings.set("levelTop", IUniBaseDao.instance.get().getCatalogItem(StructureEducationLevels.class, StructureEducationLevelsCodes.HIGH_GOS3_GROUP));
        onClickSearch(context);
    }

    public void onClickAddEduItem(IBusinessComponent context) {
        // создание на базе направлений ОП
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(UniEduProgramEduHsAdd.class.getSimpleName()));
    }

    public void onClickAddOldItem(IBusinessComponent context) {
        // создание на базе НПм - старые НПО, ДПО и прочие неклассифицируемые прелести
        context.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolAddEdit.Model.class.getPackage().getName()
        ));
    }

    @Override
    public void onClickEditItem(IBusinessComponent context) {
        context.createDefaultChildRegion(new ComponentActivator(
                UniEduProgramEduHsEdit.class.getSimpleName(),
                new ParametersMap().add(UniEduProgramEduHsEditUI.PUBLISHER_ID, context.getListenerParameter())
        ));
    }

}
