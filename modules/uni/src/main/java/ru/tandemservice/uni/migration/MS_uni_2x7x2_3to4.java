/* $Id:$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/24/15
 */
public class MS_uni_2x7x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // убираем по задаче DEV-7133
//        short entityCode = tool.entityCodes().ensure("diplomaQualifications");
//
//        PreparedStatement insert = tool.prepareStatement("insert into diplomaqualifications_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)");
//        Statement stmt = tool.getConnection().createStatement();
//        ResultSet rs = stmt.executeQuery("select q.title_p from qualifications_t q where " +
//            " exists(select id from educationlevels_t l where q.id=l.qualification_id and l.diplomaqualification_id is null and l.qualification_id is not null)" +
//            " and not exists(select id from diplomaqualifications_t dq where dq.title_p=q.title_p)");
//        while (rs.next()) {
//            String title = rs.getString(1);
//
//            insert.clearParameters();
//            insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
//            insert.setShort(2, entityCode);
//            insert.setString(3, title);
//            insert.setString(4, title);
//            insert.execute();
//            System.out.println(title);
//        }
//
//		tool.executeUpdate("update educationlevels_t set diplomaqualification_id = (select dq.id from diplomaqualifications_t dq join qualifications_t q on dq.title_p=q.title_p where q.id=educationlevels_t.qualification_id) where diplomaqualification_id is null");
    }
}

