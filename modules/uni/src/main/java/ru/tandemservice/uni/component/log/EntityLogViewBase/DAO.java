/* $Id$ */
package ru.tandemservice.uni.component.log.EntityLogViewBase;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.logging.entity.LogEvent;
import org.tandemframework.common.logging.entity.LogEventProperty;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.ui.View.RowItem;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/19/11
 */
public abstract class DAO extends UniBaseDao implements IDAO
{
    private static final Map<String, String> _orderMap = ImmutableMap.<String, String>builder()
    .put("transactionNumber", "e.transactionNumber")
    .put("eventType", "e.type")
    .put("date", "e.date")
    .put("message", "e.message")
    .put("principalContextTitle", "e.principalContextTitle")
    .put("entityTitle", "e.entityTitle")
    .put("propertyTitle", "p.propertyTitle")
    .put("prevValue", "p.prevValue")
    .put("newValue", "p.newValue")
    .build();

    protected abstract List<String> getEntityClassNames();

    @Override
    public void prepare(final Model model)
    {
        if (CollectionUtils.isEmpty(model.getEntityTypeList()))
        {
            model.setEntityTypeList(new ArrayList<>());
            for (final IEntityMeta entityType : EntityRuntime.getInstance().getEntities())
            {
                if (getEntityClassNames().contains(entityType.getClassName()))
                {
                    model.getEntityTypeList().add(entityType);
                }
            }
            Collections.sort(model.getEntityTypeList(), (o1, o2) -> o1.getTitle().compareToIgnoreCase(o2.getTitle()));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource<RowItem> dataSource = model.getDataSource();

        final DQLExecutionContext context = new DQLExecutionContext(this.getSession());

        final DQLSelectBuilder sel = this.createSelect(model);
        sel.column(property("e"));
        sel.column(property("p"));
        final EntityOrder order = dataSource.getEntityOrder();
        if(order != null)
        {
            final String orderProperty = DAO._orderMap.get((String)order.getKey());
            if(orderProperty != null)
            {
                sel.order(property(orderProperty),
                        order.getDirection()==org.tandemframework.core.entity.OrderDirection.asc ? OrderDirection.asc : OrderDirection.desc);
            }
        }

        final Number cnt = sel.createCountStatement(context).uniqueResult();
        dataSource.setTotalSize(cnt==null ? 0L : cnt.intValue());

        final IDQLStatement stmt = sel.createStatement(context);
        stmt.setFirstResult((int) dataSource.getStartRow());
        stmt.setMaxResults((int) dataSource.getCountRow());
        final List rows = stmt.list();
        dataSource.createPage(this.createWrapperList(rows));
    }

    private DQLSelectBuilder createSelect(final Model model)
    {
        final IDataSettings settings = model.getSettings();

        final String typeName = settings.get("eventTypeName");
        final String entityName = settings.get("entityName");
        final String principalContextTitle = settings.get("principalContextTitle");
        final String message = settings.get("message");
        final String entityTitle = settings.get("entityTitle");
        final String property = settings.get("property");
        final String prevValue = settings.get("prevValue");
        final String newValue = settings.get("newValue");
        final Date fromDate = settings.get("fromDate");
        final Date toDate = settings.get("toDate");
        final String ipAddress = settings.get("ipAddress");

        final DQLSelectBuilder sel = new DQLSelectBuilder();
        sel.fromEntity(LogEvent.class, "e");
        sel.joinEntity("e", DQLJoinType.left, LogEventProperty.class, "p", eq(property("p.event"), property("e")));

        sel.where(DQLExpressions.or(
                in(property("e.entityId"), getEntityIds(model)),
                in(property("e.ownerId"), getEntityIds(model))));

        if(StringUtils.isNotEmpty(typeName)) {
            sel.where(eq(property("e.type"), value(typeName)));
        }
        if(StringUtils.isNotEmpty(entityName)) {
            sel.where(eq(property("e.entityName"), value(entityName)));
        }
        if(StringUtils.isNotEmpty(message)) {
            sel.where(likeUpper(property("e.message"), value(wrapLikeValue(message))));
        }
        if(StringUtils.isNotEmpty(entityTitle)) {
            sel.where(likeUpper(property("e.entityTitle"), value(wrapLikeValue(entityTitle))));
        }
        if(StringUtils.isNotEmpty(principalContextTitle)) {
            sel.where(likeUpper(property("e.principalContextTitle"), value(wrapLikeValue(principalContextTitle))));
        }
        if(StringUtils.isNotEmpty(property)) {
            sel.where(likeUpper(property("p.propertyTitle"), value(wrapLikeValue(property))));
        }
        if(StringUtils.isNotEmpty(prevValue)) {
            sel.where(likeUpper(property("p.prevValueTitle"), value(wrapLikeValue(prevValue))));
        }
        if(StringUtils.isNotEmpty(newValue)) {
            sel.where(likeUpper(property("p.newValueTitle"), value(wrapLikeValue(newValue))));
        }

        if(fromDate != null) {
            sel.where(DQLExpressions.ge(property("e.date"), valueTimestamp(fromDate)));
        }
        if(toDate != null) {
            sel.where(DQLExpressions.le(property("e.date"), valueTimestamp(toDate)));
        }

        if(StringUtils.isNotEmpty(ipAddress)) {
            sel.where(like(property("e.ipAddress"), value("%" + ipAddress + "%")));
        }

        return sel;
    }

    protected Collection<Long> getEntityIds(Model model)
    {
        return Collections.singletonList(model.getEntityId());
    }

    private static String wrapLikeValue(final String likeValue)
    {
        return CoreStringUtils.escapeLike(likeValue).replaceAll(" ", "%");
    }

    private List<RowItem> createWrapperList(final List<Object[]> rowset)
    {
        final List<RowItem> result = new ArrayList<>(rowset.size());
        for( final Object[] row : rowset )
        {
            final LogEvent e = (LogEvent) row[0];
            final LogEventProperty p = (LogEventProperty) row[1];

            result.add(new RowItem(e, p));
        }
        return result;
    }
}
