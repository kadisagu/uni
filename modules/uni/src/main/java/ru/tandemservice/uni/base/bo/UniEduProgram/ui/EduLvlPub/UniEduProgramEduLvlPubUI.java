package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.catalog.runtime.CatalogGroupRuntime;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.catalog.CatalogMeta;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.CatalogUtils;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsPub.UniEduProgramEduHsPub;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@State({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="eduLvlHolder.id", required=true)
})
public class UniEduProgramEduLvlPubUI extends UIPresenter
{
    private final EntityHolder<EducationLevels> eduLvlHolder = new EntityHolder<>();
    public EntityHolder<EducationLevels> getEduLvlHolder() { return this.eduLvlHolder; }
    public EducationLevels getEduLvl() { return this.getEduLvlHolder().getValue(); }

    private String lvlCatalogTitle;
    public String getLvlCatalogTitle() { return this.lvlCatalogTitle; }
    public void setLvlCatalogTitle(String lvlCatalogTitle) { this.lvlCatalogTitle = lvlCatalogTitle; }

    private final StaticListDataSource<EducationLevelsHighSchool> eduHsDs = new StaticListDataSource<>();
    public StaticListDataSource<EducationLevelsHighSchool> getEduHsDs() { return this.eduHsDs; }

    @Override
    public void onComponentRefresh()
    {
        final EducationLevels lvl = getEduLvlHolder().refresh(EducationLevels.class);
        final IEntityMeta lvlEntityMeta = EntityRuntime.getMeta(lvl);
        final CatalogMeta lvlCatalogMeta = CatalogGroupRuntime.getCatalog(CatalogUtils.getCatalogCode(lvl));

        if ((null == getLvlCatalogTitle()) && (null != lvlCatalogMeta)) {
            setLvlCatalogTitle(lvlCatalogMeta.getTitle());
        }
        if ((null == getLvlCatalogTitle()) && (null != lvlEntityMeta)) {
            setLvlCatalogTitle(lvlEntityMeta.getTitle());
        }

        StaticListDataSource<EducationLevelsHighSchool> eduHsDs = getEduHsDs();
        eduHsDs.getColumns().clear();
        eduHsDs.addColumn(
            new PublisherLinkColumn("Название", EducationLevelsHighSchool.displayableTitle())
            .setResolver(
                new SimplePublisherLinkResolver(EducationLevelsHighSchool.id())
                .setComponentName(UniEduProgramEduHsPub.class.getSimpleName())
            )
        );
        eduHsDs.addColumn(new SimpleColumn("Выпускающее подр.", EducationLevelsHighSchool.orgUnit().fullTitle()));

        eduHsDs.setupRows(
            IUniBaseDao.instance.get().<EducationLevelsHighSchool>getList(
                new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "e")
                .column(property("e"))
                .where(eq(property(EducationLevelsHighSchool.educationLevel().fromAlias("e")), value(lvl)))
                .order(property(EducationLevelsHighSchool.displayableTitle().fromAlias("e")))
            )
        );
    }

//    public void onClickSetSubject() {
//        Компонент удален, т.к. работает неправильно в новой сруктуре.
//        Если понадобиться - доставайте из истории svn ru.tandemservice.uni.base.bo.UniEduProgram.ui.SetEduLvlSubject.UniEduProgramSetEduLvlSubject
//        и реализуйте правильные методы ДАО (смена НПм у НПв, если меняется общая-необщая и т.д.).
//
//        getActivationBuilder()
//        .asRegionDialog(UniEduProgramSetEduLvlSubject.class)
//        .parameter(UIPresenter.PUBLISHER_ID, getEduLvlHolder().getId())
//        .activate();
//    }

}
