// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import java.util.List;
import java.util.Set;

/**
 * @author oleyba
 * @since 17.06.2010
 */
public class QualificationModel extends BaseMultiSelectModel implements ISingleSelectModel
{
    private Session session;

    public static List<Qualifications> getList(Session session)
    {
        return builder().getResultList(session);
    }

    public QualificationModel(Session session)
    {
        super("id", new String[]{"title"});
        this.session = session;
    }

    private static MQBuilder builder()
    {
        MQBuilder builder = new MQBuilder(Qualifications.ENTITY_CLASS, "q");
        builder.add(MQExpression.exists(new MQBuilder(EducationLevelsHighSchool.ENTITY_CLASS, "hs").add(MQExpression.eqProperty("q", "id", "hs", EducationLevelsHighSchool.educationLevel().qualification().id().s()))));
        builder.addOrder("q", Qualifications.P_ORDER);
        return builder;
    }

    @Override
    public ListResult findValues(String filter)
    {
        MQBuilder builder = builder();
        filter = StringUtils.trimToNull(filter);
        if (null != filter)
            builder.add(MQExpression.like("q", Qualifications.title().s(), filter + "%"));
        return new ListResult<Qualifications>(builder.getResultList(session));
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        MQBuilder builder = builder();
        builder.add(MQExpression.in("q", "id", primaryKeys));
        return builder.getResultList(session);
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        MQBuilder builder = builder();
        builder.add(MQExpression.eq("q", "id", primaryKey));
        return builder.uniqueResult(session);
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return ((Qualifications) value).getTitle();
    }

}
