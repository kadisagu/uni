/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentArchivalEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author agolubenko
 * @since 11.02.2008
 */
@Input(keys = "studentId", bindings = "student.id")
public class Model
{
    private Student _student = new Student();
    private List<IdentifiableWrapper> _endingYearList;
    private IdentifiableWrapper _endingYear;

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public List<IdentifiableWrapper> getEndingYearList()
    {
        return _endingYearList;
    }

    public void setEndingYearList(List<IdentifiableWrapper> endingYearList)
    {
        _endingYearList = endingYearList;
    }

    public IdentifiableWrapper getEndingYear()
    {
        return _endingYear;
    }

    public void setEndingYear(IdentifiableWrapper endingYear)
    {
        this._endingYear = endingYear;
    }
}