/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 03.12.2007
 */
public class OrgstructDAO extends UniBaseDao implements IOrgstructDAO
{
    @Override
    public ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode)
    {
        return getOrgUnitList(filter, kindCode, null);
    }

    @Override
    public ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode, IPrincipalContext context)
    {
        return getOrgUnitList(filter, kindCode, context, 50);
    }

    @Override
    public ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode, IPrincipalContext context, int maxResult)
    {
        return getOrgUnitList(filter, kindCode, context, maxResult, null);
    }

    @Override
    public ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode, IPrincipalContext context, int maxResult, Object primaryKeys)
    {
        final PropertyPath titleProperty = kindCode.equals(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)
                ? OrgUnit.territorialFullTitle().fromAlias("e")
                : OrgUnit.fullTitle().fromAlias("e");

        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(OrgUnit.class, "e")
                .column(property("e"))
                .where(eq(property(OrgUnit.archival().fromAlias("e")), value(Boolean.FALSE)))
                .order(property(titleProperty));

        CommonBaseFilterUtil.applySelectFilter(b, "e", "id", primaryKeys);

        // Пока есть 2 разные настройки для выпускающих подразделений, делаем поиск в обеих.
        // Но одну из них всё равно надо выпиливать, чтобы не взрывать мозги бедным пользователям.
        IDQLExpression kindExpr = exists(OrgUnitToKindRelation.class,
                                         OrgUnitToKindRelation.orgUnit().s(), property("e"),
                                         OrgUnitToKindRelation.orgUnitKind().code().s(), kindCode);

        if (UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING.equals(kindCode)) {
            kindExpr = or(kindExpr, exists(EduOwnerOrgUnit.class, EduOwnerOrgUnit.orgUnit().s(), property("e")));
        }
        b.where(kindExpr);

        CommonBaseFilterUtil.applyLikeFilter(b, filter, titleProperty);

        if (context == null || context.isAdmin()) {
            return new DQLListResultBuilder<OrgUnit>(b, maxResult).findOptions();
        }

        b.fetchPath(DQLJoinType.inner, OrgUnit.orgUnitType().fromAlias("e"), "t");

        final List<OrgUnit> result = b.createStatement(new DQLExecutionContext(getSession()))
                .<OrgUnit>list().stream().filter(o -> CoreServices.securityService().check(o, context, "orgUnit_viewPub_" + o.getOrgUnitType().getCode()))
                .collect(Collectors.toList());

        final int allCount = result.size();
        return new ListResult<>(allCount > maxResult ? result.subList(0, maxResult) : result, allCount);
    }

    @Override
    public boolean isOrgUnitFormingOrTerritorial(final OrgUnit orgUnit)
    {
        return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                OrgUnitToKindRelation.orgUnitKind().code().s(), Arrays.asList(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)
        );
    }

    @Override
    public List<OrgUnit> getOrgUnitList(final String kindCode)
    {
        final MQBuilder b = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "r", new String[]{OrgUnitToKindRelation.orgUnit().s()});
        b.add(MQExpression.eq("r", OrgUnitToKindRelation.orgUnit().archival().s(), Boolean.FALSE));
        b.add(MQExpression.eq("r", OrgUnitToKindRelation.orgUnitKind().code().s(), kindCode));
        return b.getResultList(getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<OrgUnitKind> getOrgUnitKindList(final OrgUnit orgUnit)
    {
        final Criteria c = getSession().createCriteria(OrgUnitToKindRelation.class);
        c.add(Restrictions.eq(OrgUnitToKindRelation.L_ORG_UNIT, orgUnit));
        c.setProjection(Projections.property(OrgUnitToKindRelation.L_ORG_UNIT_KIND));
        return c.list();
    }

    @Override
    public Collection<String> getAllowStudentsOrgUnitKindCodes(OrgUnit orgUnit)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnitKind.class, "k")
                .column(property("k", OrgUnitKind.code()))
                .where(eq(property("k", OrgUnitKind.allowStudents()), value(Boolean.TRUE)))
                .where(exists(OrgUnitToKindRelation.class,
                              OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                              OrgUnitToKindRelation.L_ORG_UNIT_KIND, property("k.id")
                ));
        return dql.createStatement(getSession()).list();
    }

    @Override
    public Collection<String> getAllowGroupsOrgUnitKindCodes(OrgUnit orgUnit)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnitKind.class, "k")
                .column(property("k", OrgUnitKind.code()))
                .where(eq(property("k", OrgUnitKind.allowGroups()), value(Boolean.TRUE)))
                .where(exists(OrgUnitToKindRelation.class,
                              OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                              OrgUnitToKindRelation.L_ORG_UNIT_KIND, property("k.id")
                ));
        return dql.createStatement(getSession()).list();
    }

    @Override
    public boolean isOrgUnitHasKind(final OrgUnit orgUnit, final String kindCode)
    {
        return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                OrgUnitToKindRelation.orgUnitKind().code().s(), kindCode
        );
    }

    @Override
    public boolean isAllowStudents(final OrgUnit orgUnit)
    {
        return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                OrgUnitToKindRelation.orgUnitKind().allowStudents().s(), Boolean.TRUE
        );
    }

    @Override
    public boolean isAllowAddStudents(final OrgUnit orgUnit)
    {
        return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                OrgUnitToKindRelation.P_ALLOW_ADD_STUDENT, Boolean.TRUE
        );
    }

    @Override
    public boolean isAllowGroups(final OrgUnit orgUnit)
    {
        return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                OrgUnitToKindRelation.orgUnitKind().allowGroups().s(), Boolean.TRUE
        );
    }

    @Override
    public boolean isAllowAddGroups(final OrgUnit orgUnit)
    {
        return existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                OrgUnitToKindRelation.P_ALLOW_ADD_GROUP, Boolean.TRUE
        );
    }

    @Override
    public boolean hidePermissionsBasedOnFormingAndTerritorialKind(OrgUnitType orgUnitType)
    {
        Set<OrgUnitType> types = OrgUnitManager.instance().dao().getOrgUnitTypeChildrenFullList(orgUnitType);
        types.add(orgUnitType);

        return !existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.orgUnitKind().code().s(), Arrays.asList(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL),
                OrgUnitToKindRelation.orgUnit().orgUnitType().s(), types
        );
    }
}