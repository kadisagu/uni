/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vip_delete
 */
@Input(keys = {"kindCode"}, bindings = {"kindCode"})
public class Model
{
    private String _kindCode;
    private OrgUnit _orgUnit;
    private ISelectModel _orgUnitListModel;
    private boolean _allowAddStudent;
    private boolean _allowAddGroup;
    private String _title;

    public String getKindCode()
    {
        return _kindCode;
    }

    public void setKindCode(String kindCode)
    {
        _kindCode = kindCode;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public boolean isAllowAddStudent()
    {
        return _allowAddStudent;
    }

    public void setAllowAddStudent(boolean allowAddStudent)
    {
        _allowAddStudent = allowAddStudent;
    }

    public boolean isAllowAddGroup()
    {
        return _allowAddGroup;
    }

    public void setAllowAddGroup(boolean allowAddGroup)
    {
        _allowAddGroup = allowAddGroup;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }
}
