/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentsCoursesSpecialitiesAllocationReport.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.report.StudentsCoursesSpecialitiesAllocationReport;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since 13.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechListModel(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(StudentCategory.class));
        model.setStudentStatusListModel(new LazySimpleSelectModel<>(getCatalogItemList(StudentStatus.class, StudentStatus.P_USED_IN_SYSTEM, true)));
    }

    @Override
    public void update(final Model model)
    {
        final IFormatter<Object> formatter = CollectionFormatter.COLLECTION_FORMATTER;

        final StudentsCoursesSpecialitiesAllocationReport report = model.getReport();
        report.setFormingDate(new Date());

        if(model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(StringUtils.trimToNull(formatter.format(model.getFormativeOrgUnitList())));
        else
            report.setFormativeOrgUnit(null);

        if(model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(StringUtils.trimToNull(formatter.format(model.getTerritorialOrgUnitList())));
        else
            report.setTerritorialOrgUnit(null);

        if(model.isDevelopFormActive())
            report.setDevelopForm(StringUtils.trimToNull(formatter.format(model.getDevelopFormList())));
        else
            report.setDevelopForm(null);

        if(model.isDevelopConditionActive())
            report.setDevelopConditition(StringUtils.trimToNull(formatter.format(model.getDevelopConditionList())));
        else
            report.setDevelopConditition(null);

        if(model.isDevelopTechActive())
            report.setDevelopTech(StringUtils.trimToNull(formatter.format(model.getDevelopTechList())));
        else
            report.setDevelopTech(null);

        if(model.isQualificationActive())
            report.setQualification(StringUtils.trimToNull(formatter.format(model.getQualificationList())));
        else
            report.setQualification(null);

        if(model.isStudentCategoryActive())
            report.setStudentCategory(StringUtils.trimToNull(formatter.format(model.getStudentCategoryList())));
        else
            report.setStudentCategory(null);

        if(model.isStudentStatusActive())
            report.setStudentStatus(StringUtils.trimToNull(formatter.format(model.getStudentStatusList())));
        else
            report.setStudentStatus(null);
        final DatabaseFile databaseFile = new DatabaseFile();

        final UniScriptItem scriptItem = getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENTS_COURSES_SPECIALITIES_ALLOCATION_REPORT);

        final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, "map", getReportData(model), "report", model.getReport());

        databaseFile.setContent((byte[])scriptResult.get(IScriptExecutor.DOCUMENT));
        databaseFile.setFilename((String) scriptResult.get(IScriptExecutor.FILE_NAME));
        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }



    /**
     * @param model модель
     * @return студенты, удовлетворяющие параметрам отчета, сгруппированные по НПв
     */
    @SuppressWarnings("unchecked")
    private Map<EducationLevelsHighSchool, List<Student>> getReportData(final Model model)
    {

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "e");

        builder.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("e"), "eou");
        builder.joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("eou"), "elhs");
        builder.joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("elhs"), "npm");
        builder.joinPath(DQLJoinType.inner, EducationLevels.eduProgramSubject().fromAlias("npm"), "eps");
        builder.joinPath(DQLJoinType.inner, EduProgramSubject.subjectIndex().fromAlias("eps"), "si");
        builder.joinPath(DQLJoinType.inner, EduProgramSubjectIndex.programKind().fromAlias("si"), "pk");

        if (model.isFormativeOrgUnitActive())
        {
            builder.where(in(property("eou", EducationOrgUnit.formativeOrgUnit()), model.getFormativeOrgUnitList()));
        }
        if (model.isTerritorialOrgUnitActive())
        {
            builder.where(in(property("eou", EducationOrgUnit.territorialOrgUnit()), model.getTerritorialOrgUnitList()));
        }
        if (model.isDevelopFormActive())
        {
            builder.where(in(property("eou", EducationOrgUnit.developForm()), model.getDevelopFormList()));
        }
        if (model.isDevelopTechActive())
        {
            builder.where(in(property("eou", EducationOrgUnit.developTech()), model.getDevelopTechList()));
        }
        if (model.isQualificationActive())
        {
            builder.where(in(property("npm", EducationLevels.qualification()), model.getQualificationList()));
        }
        else
        {
            builder.where(in(property("npm", EducationLevels.qualification()), model.getQualificationListModel().findValues("").getObjects()));
        }
        if (model.isDevelopConditionActive())
        {
            builder.where(in(property("eou", EducationOrgUnit.developCondition()), model.getDevelopConditionList()));
        }
        if (model.isStudentCategoryActive())
        {
            builder.where(in(property("e", Student.studentCategory()), model.getStudentCategoryList()));
        }
        if (model.isStudentStatusActive())
        {
            builder.where(in(property("e", Student.status()), model.getStudentStatusList()));
        }

        builder.where(eq(property("e", Student.archival()), value(false)));

        final List<String> acceptedKindCodes = Arrays.asList(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY,
                EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV,
                EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA);

        builder.where(isNotNull(property("eps")));
        builder.where(in(property("pk", EduProgramKind.code()), acceptedKindCodes));
        builder.column(property("e"));

        final Map<EducationLevelsHighSchool, List<Student>> result = SafeMap.get(ArrayList.class);
        for (final Student student : DataAccessServices.dao().<Student>getList(builder))
        {
            result.get(student.getEducationOrgUnit().getEducationLevelHighSchool()).add(student);
        }
        return result;
    }
}
