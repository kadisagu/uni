/* $Id: daoAddEdit.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridAddEdit;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.util.selectModel.YearDistributionPartModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public class DAO extends DefaultCatalogAddEditDAO<DevelopGrid, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isEditForm())
        {
            model.setTitle(model.getCatalogItem().getTitle());
            return;
        }

        model.setTermList(DevelopGridDAO.getTermMap().values());
        model.setDevelopPeriodList(EducationCatalogsManager.getDevelopPeriodSelectModel());

        final HashMap<Integer, RowWrapper> rowTermMap = new HashMap<>();

        if (model.getTerm() !=null )
        {
            final List<Term> termList = model.getTermList().stream()
                    .filter(t -> t.getIntValue() <= model.getTerm().getIntValue())
                    .collect(Collectors.toList());

            model.setCurrentTermList(termList);

            for (final Term term : termList)
            {
                final int currentTerm = term.getIntValue();

                final ISelectModel courseListModel = new FullCheckSelectModel()
                {
                    @Override
                    public ListResult findValues(final String filter)
                    {
                        int maxCourseHigher = -1;
                        for (int i = 1; i < currentTerm; i++)
                            maxCourseHigher = rowTermMap.get(i).getCurrentCourse() != null && rowTermMap.get(i).getCurrentCourse().getIntValue() > maxCourseHigher ? rowTermMap.get(i).getCurrentCourse().getIntValue() : maxCourseHigher;
                        List<Course> currentTermCourseList = new ArrayList<>();
                        for (Course course : DevelopGridDAO.getCourseList())
                            if (course.getIntValue() >= maxCourseHigher) currentTermCourseList.add(course);
                        return new ListResult<>(currentTermCourseList);
                    }
                };

                final YearDistributionPartModel partListModel = new YearDistributionPartModel() {
                    @Override public Map<EducationYear, Set<YearDistributionPart>> getValueMap() { return null; }
                    @Override public ListResult findValues(String filter) {

                        List yearDistributionList = UniDaoFacade.getCoreDao().getCatalogItemList(YearDistribution.class);
                        List yearDistributionPartList = UniDaoFacade.getCoreDao().getCatalogItemList(YearDistributionPart.class);
                        ArrayList<ICatalogItem> itemList = new ArrayList<>();

                        int maxPartHigher = -1, maxCourseHigher = -1;
                        Long yearDistribution = 0L;
                        for (int i = 1; i < currentTerm; i++)
                        {
                            maxCourseHigher = rowTermMap.get(i).getCurrentCourse() != null && rowTermMap.get(i).getCurrentCourse().getIntValue() > maxCourseHigher ? rowTermMap.get(i).getCurrentCourse().getIntValue() : maxCourseHigher;
                            maxPartHigher = rowTermMap.get(i).getCurrentCourse() != null && rowTermMap.get(i).getCurrentPart() != null && (rowTermMap.get(i).getCurrentPart().getNumber() > maxPartHigher || (i > 0 && rowTermMap.containsKey(i - 1) && rowTermMap.get(i - 1).getCurrentCourse() != rowTermMap.get(i).getCurrentCourse())) ? rowTermMap.get(i).getCurrentPart().getNumber() : -1;
                            if (rowTermMap.get(i).getCurrentCourse() != null && rowTermMap.get(currentTerm).getCurrentCourse() != null && rowTermMap.get(i).getCurrentCourse().getId() == rowTermMap.get(currentTerm).getCurrentCourse().getId() && rowTermMap.get(i).getCurrentPart() != null)
                                yearDistribution = rowTermMap.get(i).getCurrentPart().getYearDistribution().getId();
                        }
                        if (rowTermMap.get(currentTerm).getCurrentCourse() == null || (rowTermMap.get(currentTerm).getCurrentCourse() != null && maxCourseHigher < rowTermMap.get(currentTerm).getCurrentCourse().getIntValue()))
                            maxPartHigher = -1;
                        for (Object year : yearDistributionList)
                        {
                            YearDistribution yearD = (YearDistribution) year;
                            if (!yearD.getId().equals(yearDistribution) && yearDistribution != 0L) continue;
                            itemList.add(yearD);
                            for (Object part : yearDistributionPartList)
                                if (((YearDistributionPart) part).getYearDistribution().equals(yearD) && ((YearDistributionPart) part).getNumber() > maxPartHigher)
                                    itemList.add((YearDistributionPart) part);
                        }
                        return new ListResult<>(itemList);
                    }

                    @Override
                    public Object getPrimaryKey(Object value)
                    {
                        return (value instanceof IEntity) ? ((IEntity) value).getId() : null;
                    }
                };

                rowTermMap.put(currentTerm, new RowWrapper(null, null) {
                    @Override public ISelectModel getCourseListModel()
                    {
                        return courseListModel;
                    }
                    @Override public IHSelectModel getPartListModel()
                    {
                        return partListModel;
                    }
                });
            }
        }
        model.setRowTermMap(rowTermMap);
    }
}
