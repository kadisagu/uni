package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.catalog.runtime.CatalogGroupRuntime;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.meta.catalog.CatalogMeta;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.CatalogUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEdit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit.UniEduProgramEduOuAddEdit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit.UniEduProgramEduOuAddEditUI;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;

@State({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="eduHsHolder.id", required=true)
})
public class UniEduProgramEduHsPubUI extends UIPresenter
{
    private final EntityHolder<EducationLevelsHighSchool> eduHsHolder = new EntityHolder<>();

    private String lvlCatalogTitle;

    private DynamicListDataSource<EducationOrgUnit> _dataSource;

    @Override
    public void onComponentRefresh() {
        getEduHsHolder().refresh();

        final IEntityMeta lvlEntityMeta = EntityRuntime.getMeta(getEduLvl());
        final CatalogMeta lvlCatalogMeta = CatalogGroupRuntime.getCatalog(CatalogUtils.getCatalogCode(getEduLvl()));

        if ((null == getLvlCatalogTitle()) && (null != lvlCatalogMeta)) {
            setLvlCatalogTitle(lvlCatalogMeta.getTitle());
        }
        if ((null == getLvlCatalogTitle()) && (null != lvlEntityMeta)) {
            setLvlCatalogTitle(lvlEntityMeta.getTitle());
        }

        prepareDataSource();
    }

    public void onClickEdit() {
        getActivationBuilder().asRegion(UniEduProgramEduHsEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getEduHs().getId())
            .activate();
    }

    public void onClickDelete() {
        try {
            IUniBaseDao.instance.get().delete(getEduHs());
        } catch (Throwable t) {
            getSupport().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        deactivate();
    }



    public void onClickAddEduOu() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class)
            .parameter(UniEduProgramEduOuAddEditUI.EDU_HS_ID, getEduHs().getId())
            .activate();
    }

    public void onClickEditEduOu() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }

    public void onClickDeleteEduOu() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickChangeUsedEduOu() {
        UniEduProgramManager.instance().dao().doChangeUsedEduOu(getListenerParameterAsLong());
    }


    private void prepareDataSource()
    {
        if (getDataSource() != null) return;

        DynamicListDataSource<EducationOrgUnit> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {
            DynamicListDataSource<EducationOrgUnit> dataSource1 = getDataSource();

            MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, getEduHs()));

            OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");
            _orderSettings.setOrders("developPeriod.title", new OrderDescription("e", "developPeriod.priority"));
            _orderSettings.applyOrder(builder, getDataSource().getEntityOrder());

            UniBaseUtils.createPage(dataSource1, builder, _uiSupport.getSession());
        });

        dataSource.addColumn(new SimpleColumn("Название", EducationOrgUnit.title().s()).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Форма освоения", new String[]{EducationOrgUnit.L_DEVELOP_FORM, DevelopForm.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Условие освоения", new String[]{EducationOrgUnit.L_DEVELOP_CONDITION, DevelopCondition.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Технология освоения", new String[]{EducationOrgUnit.L_DEVELOP_TECH, DevelopTech.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Нормативный срок", new String[]{EducationOrgUnit.L_DEVELOP_PERIOD, DevelopPeriod.P_TITLE}).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Сокр. наз.", EducationOrgUnit.P_SHORT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вн. код", EducationOrgUnit.P_CODE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Используется", EducationOrgUnit.used().s()).setListener("onClickChangeUsedEduOu"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEduOu"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEduOu", "Удалить набор параметров «{0}, {1}»?", EducationOrgUnit.developOrgUnitShortTitle(), EducationOrgUnit.developCombinationTitle()));
        setDataSource(dataSource);
    }

    // getters and setters

    public EntityHolder<EducationLevelsHighSchool> getEduHsHolder() { return this.eduHsHolder; }
    public EducationLevelsHighSchool getEduHs() { return getEduHsHolder().getValue(); }
    public String getLvlCatalogTitle() { return this.lvlCatalogTitle; }
    public void setLvlCatalogTitle(String lvlCatalogTitle) { this.lvlCatalogTitle = lvlCatalogTitle; }

    public DynamicListDataSource<EducationOrgUnit> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EducationOrgUnit> dataSource)
    {
        _dataSource = dataSource;
    }

    public EducationLevels getEduLvl() {
        return getEduHs().getEducationLevel();
    }
}
