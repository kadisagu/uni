package ru.tandemservice.uni.runtime.uni;

import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;

/**
 * @author vdanilov
 */
public class EducationGridRuntimeExtension implements IRuntimeExtension {

    private IDevelopGridDAO dao;
    public IDevelopGridDAO getDao() { return this.dao; }
    public void setDao(IDevelopGridDAO dao) { this.dao = dao; }


    @Override
    public void init(Object object) {
        dao.doGenerateGridsFromDescription();
    }

    @Override
    public void destroy() {}


}
