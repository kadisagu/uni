/* $Id: EduLevelHSDisplayableTitleSync.java 25247 2012-12-12 09:25:55Z vdanilov $ */
package ru.tandemservice.uni.events.dset;

import java.util.Set;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

/**
 * Запрещает редактирование НПм для НПВ
 */
public class EduLevelHSEditDisabler implements IDSetEventListener
{
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationLevelsHighSchool.class, this);
    }

    public static final EventListenerLocker<EduLevelHSEditDisabler> LOCKER = new EventListenerLocker<>();

    @Override
    public void onEvent(DSetEvent event) {

        final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
        final Set<String> properties = event.getMultitude().getAffectedProperties();
        if (properties.contains(EducationLevelsHighSchool.L_EDUCATION_LEVEL)) {
            if (event.getMultitude().isSingular() && event.getMultitude().getSingularEntityOldProperty(EducationLevelsHighSchool.L_EDUCATION_LEVEL) == null) {
                return;
            }
            final EventListenerLocker.Handler<EduLevelHSEditDisabler> handler = LOCKER.handler();
            if (!handler.handle(this, entityMeta, properties, Integer.MAX_VALUE, event)) {
                // если ошибка не была обработкна - кидуаем исключение (в данном случае это системная ошибка, а не пользовательская - т.к. это безусловная ошибка и поля на формах должны быть задизаблены ВСЕГДА)
                throw new IllegalStateException("try-to-change: educationLevelsHighSchool.educationLevel");
            }
        }
    }
}
