package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelMiddleGen;

/**
 * Направление подготовки (специальности) СПО
 */
public abstract class EducationLevelMiddle extends EducationLevelMiddleGen
{
}