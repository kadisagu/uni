/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;

import java.util.List;
import java.util.Map;

/**
 * Расширяет стандартные права на подразделении
 *
 * @author Vasily Zhukov
 * @see org.tandemframework.shared.organization.sec.OrganizationPermissionModifier
 * @since 14.03.2012
 */
public class UniOrganizationPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uni");
        config.setName("uni-organization-sec-config");
        config.setTitle("");
        ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        List<OrgUnitType> orgUnitTypeList = DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE);
        for (OrgUnitType description : orgUnitTypeList)
        {
            String code = description.getCode();

            ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            if(code.equals(OrgUnitTypeCodes.TOP))
            {
                PermissionGroupMeta permissionGroupPublisher = PermissionMetaUtil.createPermissionGroup(config, code + "PublisherPermissionGroup", "Вкладка «" + description.getTitle() + "»");
                PermissionMetaUtil.createPermission(permissionGroupPublisher, "orgUnit_editAdditionalAttrs_" + code, "Редактирование дополнительных данных");
            }

            // Вкладка «Архив»
            PermissionGroupMeta archiveTab = PermissionMetaUtil.createPermissionGroup(config, code + "ArchiveOrgUnitTabPermissionGroup", "Вкладка «Архив»");
            PermissionMetaUtil.createGroupRelation(config, archiveTab.getName(), globalClassGroup.getName());
            PermissionMetaUtil.createGroupRelation(config, archiveTab.getName(), localClassGroup.getName());
            PermissionMetaUtil.createPermission(archiveTab, "viewArchiveList_orgUnit_" + code, "Просмотр");

            // Вкладка «Студенты» на подразделении
            PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");
            PermissionMetaUtil.createPermission(permissionGroupStudentsTab, "orgUnit_viewStudentTabAll_" + code, "Просмотр");
            addTabPermission(config, globalClassGroup, localClassGroup, permissionGroupStudentsTab);

            // Вкладка «Группы» на табе «Студенты»
            PermissionGroupMeta permissionGroupGroup = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "GroupPermissionGroup", "Вкладка «Группы»");
            PermissionMetaUtil.createPermission(permissionGroupGroup, "orgUnit_viewTab_" + code + "_group", "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupGroup, "orgUnit_printStudentsGroupsList_" + code + "_group", "Печать списочного состава групп");
            PermissionMetaUtil.createPermission(permissionGroupGroup, "orgUnit_add_" + code + "_group", "Добавление группы");
            PermissionMetaUtil.createPermission(permissionGroupGroup, "orgUnit_delete_" + code + "_group", "Удаление группы");
            PermissionMetaUtil.createPermission(permissionGroupGroup, "orgUnit_edit_" + code + "_group", "Редактирование группы");

            // Вкладка «Архив групп» на табе «Студенты»
            PermissionGroupMeta permissionGroupGroupArchival = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "ArchivalGroupPermissionGroup", "Вкладка «Архив групп»");
            PermissionMetaUtil.createPermission(permissionGroupGroupArchival, "orgUnit_viewArchivalTab_" + code + "_group", "Просмотр");

            // Вкладка «Студенты» на табе «Студенты»
            PermissionGroupMeta permissionGroupStudent = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "StudentsTabPermissionGroup", "Вкладка «Студенты»");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_viewStudentTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_addStudent_" + code, "Добавление студента");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_editStudent_" + code, "Редактирование студента");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_printStudentPersonalCard_" + code, "Печатать ЛК из списка");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_deleteStudent_" + code, "Удаление студента");
            PermissionMetaUtil.createPermission(permissionGroupStudent, "orgUnit_printStudentPersonCards_" + code, "Печатать всех ЛК студентов");

            // Вкладка «Архив студентов» на табе «Студенты»
            PermissionGroupMeta permissionGroupStudentArchival = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "ArchivalStudentsTabPermissionGroup", "Вкладка «Архив студентов»");
            PermissionMetaUtil.createPermission(permissionGroupStudentArchival, "orgUnit_viewArchivalStudentTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupStudentArchival, "orgUnit_printStudentPersonalCard_archival_" + code, "Печатать ЛК из списка");
            PermissionMetaUtil.createPermission(permissionGroupStudentArchival, "orgUnit_printStudentPersonCards_archival_" + code, "Печатать всех ЛК студентов");

            // Вкладка «Документы, выданные студентам» на табе «Студенты»
            PermissionGroupMeta permissionGroupStudentDocument = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "StudentDocumentsTabPermissionGroup", "Вкладка «Документы, выданные студентам»");
            PermissionMetaUtil.createPermission(permissionGroupStudentDocument, "orgUnit_viewStudentDocumentTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupStudentDocument, "orgUnit_printStudentDocument_" + code, "Печатать документ");
            PermissionMetaUtil.createPermission(permissionGroupStudentDocument, "orgUnit_deleteStudentDocument_" + code, "Удаление документа");


        }
        securityConfigMetaMap.put(config.getName(), config);
    }

    private void addTabPermission(SecurityConfigMeta config, ClassGroupMeta globalClassGroup, ClassGroupMeta localClassGroup, PermissionGroupMeta tab)
    {
        PermissionMetaUtil.createGroupRelation(config, tab.getName(), globalClassGroup.getName());
        PermissionMetaUtil.createGroupRelation(config, tab.getName(), localClassGroup.getName());
    }
}
