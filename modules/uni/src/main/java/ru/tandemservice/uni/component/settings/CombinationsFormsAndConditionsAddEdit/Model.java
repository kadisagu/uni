package ru.tandemservice.uni.component.settings.CombinationsFormsAndConditionsAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.DevelopCombination;

/**
 * 
 * @author nkokorina
 * @since 01.03.2010
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "developCombinationId") })
public class Model
{
    private Long _developCombinationId;
    private DevelopCombination _developCombination;

    private ISelectModel _developFormList;
    private ISelectModel _developConditionList;
    private ISelectModel _developTechList;
    private ISelectModel _developGridList;

    public Long getDevelopCombinationId()
    {
        return _developCombinationId;
    }

    public void setDevelopCombinationId(Long developCombinationId)
    {
        _developCombinationId = developCombinationId;
    }

    public DevelopCombination getDevelopCombination()
    {
        return _developCombination;
    }

    public void setDevelopCombination(DevelopCombination developCombination)
    {
        _developCombination = developCombination;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public ISelectModel getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(ISelectModel developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public ISelectModel getDevelopTechList() { return _developTechList; }
    public void setDevelopTechList(ISelectModel developTechList) { _developTechList = developTechList; }

    public ISelectModel getDevelopGridList()
    {
        return _developGridList;
    }

    public void setDevelopGridList(ISelectModel developGridList)
    {
        _developGridList = developGridList;
    }

}
