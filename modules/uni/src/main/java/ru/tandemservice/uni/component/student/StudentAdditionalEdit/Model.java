/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentAdditionalEdit;

import org.tandemframework.core.component.Input;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
@Input(keys = "studentId", bindings = "student.id")
public class Model
{
    private Student _student = new Student();
    private boolean _contractStudent;

    private ISelectModel _externalOrgUnits;
    private ISelectModel _ppsListModel;

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public boolean isContractStudent()
    {
        return _contractStudent;
    }

    public void setContractStudent(boolean contractStudent)
    {
        _contractStudent = contractStudent;
    }

    public ISelectModel getExternalOrgUnits() { return _externalOrgUnits; }
    public void setExternalOrgUnits(ISelectModel externalOrgUnits) { _externalOrgUnits = externalOrgUnits; }

    public ISelectModel getPpsListModel() { return _ppsListModel; }
    public void setPpsListModel(ISelectModel ppsListModel) { _ppsListModel = ppsListModel; }
}
