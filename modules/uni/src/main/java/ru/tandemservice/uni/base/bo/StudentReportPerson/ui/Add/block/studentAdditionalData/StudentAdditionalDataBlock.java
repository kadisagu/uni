/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentAdditionalData;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Vasily Zhukov
 * @since 07.02.2011
 */
public class StudentAdditionalDataBlock
{
    // names
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
//    public static final String RECRUIT_DS = "recruitDS";
//    public static final String INDIVIDUAL_EDUCATION_DS = "individualEducationDS";
    public static final String TARGET_ADMISSION_ORG_UNIT_DS = "targetAdmissionOrgUnitDS";
    public static final String STUDENT_ADVISOR_DS = "studentAdvisorDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, StudentAdditionalDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createStudentCategoryDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, StudentCategory.class);
    }

    public static IDefaultComboDataSourceHandler createTargetAdmissionOrgUnitDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, ExternalOrgUnit.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.exists(Student.class, Student.L_TARGET_ADMISSION_ORG_UNIT, (Object) property("e")));
            }
        };
    }
}
