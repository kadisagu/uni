/* $Id$ */
package ru.tandemservice.uni.component.log.EntityLogViewBase;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 9/19/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void prepareListDataSource(Model model);
}
