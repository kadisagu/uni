/* $Id$ */
package ru.tandemservice.uni.migration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Nikolay Fedorovskih
 * @since 02.12.2015
 */
@SuppressWarnings({"SpellCheckingInspection", "unused"})
public class MS_uni_2x9x3_0to1 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Сначала необходимо выполнить комплекс проверок из показателя корректности для квалификаций (DEV-8837)
        qualificationIndexCheck(tool, true);

        // ---------------------------------------------------------------------------------------------------
        // Используемые в миграции элементы справочников

        // Перечни
        final Map<String, Long> subjectIndexMap = MigrationUtils.getCatalogCode2IdMap(tool, "edu_c_pr_subject_index_t");
        final Long index_bachelor2013 = subjectIndexMap.get("2013.03");
        final Long index_master2013 = subjectIndexMap.get("2013.04");

        // Квалификации проф. образования
        final long q_simpleBachelor = tool.getNumericResult("select id from edu_c_pr_qual_t where code_p=?", "2013.03.9ac370aa611a7839fa6d35c145f724dd88942b02");
        // прикладного ищем по названию, потому что в перечень он попадал 2 раза (см. DEV-8837)
        final long q_appliedBachelor = tool.getNumericResult("select id from edu_c_pr_qual_t where title_p=? and subjectindex_id=?", "Прикладной бакалавр", index_bachelor2013);

        if (0 != q_appliedBachelor && 0 == q_simpleBachelor) {
            // Проверим на всякий случай, что если есть прикладной бакалавр,
            // то обычный должен быть обязательно (была миграция, которая академического в обычного переделывала).
            throw new IllegalStateException("Simple bachelor row (code=2013.03.9ac370aa611a7839fa6d35c145f724dd88942b02) is missing fro edu_c_pr_qual_t");
        }

        // Ориентации ОП
        final Long orientation_academicBachelor;
        final Long orientation_appliedBachelor;
        final Long orientation_academicMaster;
        final Long orientation_appliedMaster;

        // Новый справочник - ориентация ОП
        {
            final DBTable dbt = new DBTable("eduprogramorientation_t");
            dbt.addColumn(new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eduprogramorientation"));
            dbt.addColumn(new DBColumn("discriminator", DBType.SHORT).setNullable(false));
            dbt.addColumn(new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false));
            dbt.addColumn(new DBColumn("title_p", DBType.createVarchar(1200)));
            dbt.addColumn(new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false));
            dbt.addColumn(new DBColumn("abbreviation_p", DBType.createVarchar(255)).setNullable(false));
            dbt.addColumn(new DBColumn("printtitle_p", DBType.createVarchar(255)).setNullable(false));
            dbt.addColumn(new DBColumn("subjectindex_id", DBType.LONG).setNullable(false));
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            final short entityCode = tool.entityCodes().ensure("eduProgramOrientation");

            // Заполняем справочник системными элементами
            final BatchInsertBuilder ins = new BatchInsertBuilder(entityCode, "code_p", "title_p", "shorttitle_p", "abbreviation_p", "printtitle_p", "subjectindex_id");
            {
                orientation_academicBachelor = ins.addRow("2013.03.academicBachelor", "Академический бакалавриат", "акад. бак.", "АБ", "программа академического бакалавриата", index_bachelor2013);
                orientation_appliedBachelor = ins.addRow("2013.03.appliedBachelor", "Прикладной бакалавриат", "прик. бак.", "ПБ", "программа прикладного бакалавриата", index_bachelor2013);
                orientation_academicMaster = ins.addRow("2013.04.academicMaster", "Академическая магистратура", "акад. маг.", "АМ", "программа академической магистратуры", index_master2013);
                //noinspection UnusedAssignment
                orientation_appliedMaster = ins.addRow("2013.04.appliedMaster", "Прикладная магистратура", "прик. маг.", "ПМ", "программа прикладной магистратуры", index_master2013);
            }
            ins.executeInsert(tool, dbt.getName());
        }

        // --------------------------------------------------------------------------

        // Мигрируем НПв, ОП ВО и УП ВО на ориентацию ОП.
        // Удаляем ссылающиеся записи на квалификацию "Прикладной бакалавр" в проектах.
        // Окончательно удаляем квалификацию проф. образования "Прикладной бакалавр" для перечня 2013 года.

        final ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        {
            // case-выражение, подставляющее нужную ориентацию на основе квалификации,
            // где q - алиас приджойненнной таблицы квалификаций (eduProgramQualification)
            final String caseExpr = "(case " +
                    " when q.id="+q_appliedBachelor+" then "+orientation_appliedBachelor+ // Если это квалификация "Прикладной бакалавр", то ориентация "Прикладной бакалавриат"
                    " else (case q.subjectindex_id "+ // Иначе выбираем по индексу
                    " when "+index_bachelor2013+" then "+orientation_academicBachelor+ // Если это бакалавриат 2013, то ориентация "Академический бакалавриат"
                    " when "+index_master2013+" then "+orientation_academicMaster+ // Если это магистратура 2013, то ориентация "Академическая магистратура"
                    " else null end) " + // Иначе нет никакой ориентации
                    " end)";

            // В НПв и ОП ВО просто добавляем колонку и заполняем
            // { [table name] -> [all updated rows] }
            final Map<String, Long> tables4check = new HashMap<>(3);
            final Map<String, String> tables4update = new HashMap<>(3);
            final Map<String, Long> appliedCount = SafeMap.get(key -> 0L);
            {
                // { [table] -> [qualification column] }
                final Map<String, String> table2column = ImmutableMap.of(
                        "educationlevelshighschool_t", "assignedqualification_id", // НПв
                        "edu_program_prof_higher_t", "programqualification_id" // ОП ВО (квалификация пока ещё в этой таблице есть)
                );

                for (final Map.Entry<String, String> entry : table2column.entrySet()) {

                    final String tableName = entry.getKey();
                    final String q_column = entry.getValue();

                    // Создаем колонку "Ориентация ОП"
                    tool.createColumn(tableName, new DBColumn("programorientation_id", DBType.LONG));

                    // Заполняем колонку на основе квалификаций
                    final SQLUpdateQuery upd = new SQLUpdateQuery(tableName, "t");
                    upd.set("programorientation_id", caseExpr);
                    upd.getUpdatedTableFrom().leftJoin(SQLFrom.table("edu_c_pr_qual_t", "q"), "t." + q_column + "=q.id");
                    final long n = tool.executeUpdate(translator.toSql(upd));
                    tables4check.put(tableName, n);

                    tables4update.put(tableName, q_column);
                }
            }

            if (tool.tableExists("epp_eduplan_prof_t")) {
                // Миграция учебных планов не в своём модуле, поэтому сначала надо проверить наличие родительской таблицы

                // свойство programQualification стало обязательным (проверки на заполненность есть в показателе)
                tool.setColumnNullable("epp_eduplan_prof_t", "programqualification_id", false);

                if (tool.tableExists("epp_eduplan_prof_higher_t")) {
                    // На базе САФУ таблица почему-то не удалена была, хотя MS_uniepp_2x9x0_1to2 должна была это сделать.
                    // Загадка..
                    tool.dropTable("epp_eduplan_prof_higher_t", false);
                }

                // УП ВПО теперь имеет свою таблицу (для хранения ориентаций)
                tool.createTable(new DBTable(
                        "epp_eduplan_prof_higher_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppeduplanhigherprof"),
                        new DBColumn("programorientation_id", DBType.LONG)
                ));

                // Заполняем новую таблицу
                final short entityCode = tool.entityCodes().get("eppEduPlanHigherProf");
                final long n = tool.executeUpdate(
                        "insert into epp_eduplan_prof_higher_t(id, programorientation_id) " +
                                " select p.id, " + caseExpr +
                                " from epp_eduplan_prof_t p " +
                                " inner join epp_eduplan_t b on b.id=p.id " +
                                " left join edu_c_pr_qual_t q on p.programqualification_id=q.id " +
                                " where b.discriminator=" + entityCode
                );

                appliedCount.put("epp_eduplan_prof_higher_t", tool.getNumericResult("select count(*) from epp_eduplan_prof_t where programqualification_id=?", q_appliedBachelor));
                tables4check.put("epp_eduplan_prof_higher_t", n);
                tables4update.put("epp_eduplan_prof_t", "programqualification_id");
            }

            // Заменяем прикладного бакалавра на обычного
            if (q_appliedBachelor != 0) {
                for (Map.Entry<String, String> entry : tables4update.entrySet()) {
                    final String table = entry.getKey();
                    final String column = entry.getValue();
                    final long n = tool.executeUpdate("update " + table + " set " + column + "=? where " + column + "=?", q_simpleBachelor, q_appliedBachelor);
                    appliedCount.put(table, n);
                    tool.debug(n + " rows migrated from applied bachelor to simple bachelor in " + table);
                }
            }

            // На всякий случай делаем проверки, что всё сходится во всех таблицах, куда вы добавили ориентацию
            for (final Map.Entry<String, Long> entry : tables4check.entrySet()) {

                final String tableName = entry.getKey();
                final Long total = entry.getValue();

                final Map<Long, Long> map = SafeMap.get(key -> 0L);
                tool.executeQuery(processor(Long.class, Long.class), "select programorientation_id, count(*) from " + tableName + " group by programorientation_id").stream()
                        .forEach(row -> map.put((Long) row[0], (Long) row[1]));

                final Long a = map.get(orientation_academicBachelor);
                final Long b = map.get(orientation_appliedBachelor);
                final Long c = map.get(orientation_academicMaster);
                final Long d = map.get(null);
                if ((a + b + c + d) != total) {
                    throw new IllegalStateException("Invalid sum check for " + tableName);
                }
                if (!Objects.equals(appliedCount.get(tableName), b)) {
                    throw new IllegalStateException("Invalid applied bachelor count check for " + tableName);
                }
                tool.debug(String.format(tableName + " [total %d rows] : %d - academic bachelor, %d - applied bachelor, %d - academic master, %d - null", total, a, b, c, d));
            }

            if (0 != q_appliedBachelor) {
                // Если прикладной бакалавр есть в базе..

                // Удаляем зависимости от прикладного бакалавра в проектах.
                final Map<String, String> table2column = ImmutableMap.of(
                        "ffeppsttedstndrdlbrblcks_t", "qualification_id", // В проекте ДВФУ. Удалить все записи из fefuEppStateEduStandardLaborBlocks (Трудоемкость ГОС по блокам (ДВФУ)), которые ссылаются на "Прикладного бакалавра"
                        "nsunedprgrmdscplnsttstcs_t", "qualification_id", // В проекте ПГУПС. Удалить все записи из nsiUniEduProgramDisciplineStatistics (Статистика по студентам для выгрузки), которые ссылаются на "Прикладного бакалавра"
                        "eduprogramqualificationtofis_t", "qualifications_id", // В модуле fisrmc. Удалить все записи из eduProgramQualificationToFis (Квалификации НП к ФИС), которые ссылаются на "Прикладного бакалавра"
                        "inspectioneduplanversion_t", "eduprogramqualification_id" // В модуле uniplancheckrmc. Удалить все записи из inspectionEduPlanVersion (Проверка УП(в)), которые ссылаются на "Прикладного бакалавра"
                );
                for (final Map.Entry<String, String> entry : table2column.entrySet()) {
                    if (tool.tableExists(entry.getKey())) {
                        final int n = tool.executeUpdate("delete from " + entry.getKey() + " where " + entry.getValue() + "=?", q_appliedBachelor);
                        tool.debug(n + " rows linked to applied bachelor qualification removed from " + entry.getKey());
                    }
                }


                // Удаляем с концами эту квалификацию вместе со связями с напрвлениями
                tool.executeUpdate("delete from edu_c_pr_subject_qual_t where programqualification_id=?", q_appliedBachelor);
                if (1 != tool.executeUpdate("delete from edu_c_pr_qual_t where id=?", q_appliedBachelor)) {
                    throw new IllegalStateException("Delete unknown trouble...");
                }
            }

        }

        // --------------------------------------------------------------------------

        // Переносим колонку с квалификацией в ОП проф образования из ОП ВО и ОП СПО
        {
            tool.createColumn("edu_program_prof_t", new DBColumn("programqualification_id", DBType.LONG));

            final SQLUpdateQuery upd = new SQLUpdateQuery("edu_program_prof_t", "p");
            upd.set("programqualification_id", "coalesce(h.programqualification_id, s.programqualification_id)");
            upd.getUpdatedTableFrom().leftJoin(SQLFrom.table("edu_program_prof_higher_t", "h"), "p.id=h.id");
            upd.getUpdatedTableFrom().leftJoin(SQLFrom.table("edu_program_prof_secondary_t", "s"), "p.id=s.id");
            final int n = tool.executeUpdate(translator.toSql(upd));
            tool.debug(n + " rows in edu_program_prof_t filled qualification from child tables");

            // Отныне квалификация обязательна (показатель позаботился)
            tool.setColumnNullable("edu_program_prof_t", "programqualification_id", false);

            tool.dropColumn("edu_program_prof_higher_t", "programqualification_id");
            tool.dropColumn("edu_program_prof_secondary_t", "programqualification_id");
        }

        // --------------------------------------------------------------------------

        // Теперь надо насоздавать ссылки на связи направления и квалификации в НПв, ОП и УП
        {
            // НПв
            {
                tool.createColumn("educationlevelshighschool_t", new DBColumn("subjectqualification_id", DBType.LONG));

                final SQLUpdateQuery upd = new SQLUpdateQuery("educationlevelshighschool_t", "hs");
                upd.set("subjectqualification_id", "rel.id");
                upd.where("rel.id is not null");
                upd.getUpdatedTableFrom()
                        .innerJoin(SQLFrom.table("educationlevels_t", "l"), "l.id = hs.educationlevel_id")
                        .innerJoin(SQLFrom.table("edu_c_pr_subject_qual_t", "rel"), "rel.programsubject_id = l.eduprogramsubject_id and rel.programqualification_id = hs.assignedqualification_id");
                final int n = tool.executeUpdate(translator.toSql(upd));
                tool.debug(n + " rows filled subjectqualification_id in educationlevelshighschool_t");

                if (tool.hasResultRows("select id from educationlevelshighschool_t where subjectqualification_id is null and assignedqualification_id is not null")) {
                    // В миграциях проверки лишними не бывают
                    throw new IllegalStateException("educationlevelshighschool_t: subjectqualification_id=null and assignedqualification_id<>null");
                }
            }

            // ОП / УП. Все нужные названия колонок у них совпадают.
            for (final String table : Arrays.asList("edu_program_prof_t", "epp_eduplan_prof_t")) {

                if (!tool.tableExists(table)) {
                    continue;
                }
                tool.createColumn(table, new DBColumn("subjectqualification_id", DBType.LONG));

                final SQLUpdateQuery upd = new SQLUpdateQuery(table, "t");
                upd.set("subjectqualification_id", "rel.id");
                upd.getUpdatedTableFrom().innerJoin(
                        SQLFrom.table("edu_c_pr_subject_qual_t", "rel"),
                        "rel.programsubject_id = t.programsubject_id and rel.programqualification_id = t.programqualification_id"
                );
                final int n = tool.executeUpdate(translator.toSql(upd));
                tool.debug(n + " rows filled subjectqualification_id in " + table);

                tool.setColumnNullable(table, "subjectqualification_id", false);
            }
        }


        // --------------------------------------------------------------------------

        // До кучи, удаляем ненужные пока таблицы некоторых видов образовательных программ
        {
            // сущность eduProgramVocational. у сущности больше нет своей таблицы
            tool.dropTable("edu_program_vocational_t", true);
            // сущность eduProgramPrimary. у сущности больше нет своей таблицы
            tool.dropTable("edu_program_primary_t", true);
            // сущность eduProgramAdditional. у сущности больше нет своей таблицы
            tool.dropTable("edu_program_additional_t", true);
        }
    }

    private void qualificationIndexCheck(DBTool tool, boolean autoFix) throws Exception {

        final List<String> errors = new ArrayList<>();

        // Блок 1. Уникальность названия квалификации в рамках перечня и кода уровня квалификации.
        // Блок 7. Для параметров выпуска студентов по направлению подготовки (НПв) задано не уникальное название.
        // Блок 8. Для параметров выпуска студентов по направлению подготовки (НПв) задано не уникальное сокращенное название.
        final List<PairKey<String, Set<String>>> keys = ImmutableList.of(
                new PairKey<>("edu_c_pr_qual_t", ImmutableSet.of("title_p", "subjectindex_id", "qualificationlevelcode_p")),
                new PairKey<>("educationlevelshighschool_t", ImmutableSet.of("educationlevel_id", "orgunit_id", "assignedqualification_id", "title_p")),
                new PairKey<>("educationlevelshighschool_t", ImmutableSet.of("educationlevel_id", "orgunit_id", "assignedqualification_id", "shorttitle_p"))
        );
        for (final PairKey<String, Set<String>> entry : keys) {
            if (!MigrationUtils.checkUniqueKey(tool, entry.getFirst(), entry.getSecond())) {
                errors.add(entry.getFirst() + " contain not unique rows for key " + entry.getSecond().stream().collect(Collectors.joining(", ", "[", "]")));
            }
        }

        // Блок 2. В учебных планах не указаны квалификации.
        if (tool.tableExists("epp_eduplan_prof_t") && tool.hasResultRows("select id from epp_eduplan_prof_t where programqualification_id is null")) {
            errors.add("epp_eduplan_prof_t contain rows without programQualification");
        }

        // Блок 3. Квалификация ОП отсутствует в списке разрешенных квалификаций направления подготовки ОП.
        for (String table : Arrays.asList("edu_program_prof_higher_t", "edu_program_prof_secondary_t")) {
            if (tool.hasResultRows("select t.id from " + table + " t join edu_program_prof_t p on t.id=p.id where not exists(select id from edu_c_pr_subject_qual_t rel where rel.programsubject_id=p.programsubject_id and rel.programqualification_id=t.programqualification_id)")) {
                errors.add(table + " contain rows without relation in EduProgramSubjectQualification");
            }
        }

        // Блок 4. Квалификация УП отсутствует в списке разрешенных квалификаций направления подготовки УП.
        if (tool.tableExists("epp_eduplan_prof_t") && tool.hasResultRows("select p.id from epp_eduplan_prof_t p where not exists(select id from edu_c_pr_subject_qual_t rel where rel.programsubject_id=p.programsubject_id and rel.programqualification_id=p.programqualification_id)")) {
            errors.add("epp_eduplan_prof_t contain rows without relation in EduProgramSubjectQualification");
        }

        // Блок 5. Для параметров выпуска студентов по направлению подготовки (НПв) не указана квалификация.
        if (tool.hasResultRows("select hs.id from educationlevelshighschool_t hs join educationlevels_t l on l.id = hs.educationlevel_id where hs.assignedqualification_id is null and l.eduprogramsubject_id is not null")) {
            errors.add("educationlevelshighschool_t contain rows without assignedQualification and with eduProgramSubject");
        }

        // Блок 6. Для параметров выпуска студентов по направлению подготовки (НПв) квалификация указана неверно.
        if (tool.hasResultRows("select hs.id from educationlevelshighschool_t hs join educationlevels_t l on l.id = hs.educationlevel_id where l.eduprogramsubject_id is not null and not exists(select id from edu_c_pr_subject_qual_t rel where rel.programsubject_id=l.eduprogramsubject_id and rel.programqualification_id=hs.assignedqualification_id)")) {
            errors.add("educationlevelshighschool_t contain rows without relation in EduProgramSubjectQualification");
        }

        // Блок 9. Для параметров выпуска студентов по направлению подготовки (НПв) не задано направление профессионального образования.
        // Блок 10. Параметры выпуска студентов по направлению подготовки (НПв) не должны быть созданы на базе укрупненной группы направлений.
        {
            final short additionalEduLevelsEntityCode = tool.entityCodes().get("educationLevelAdditional");
            final String invalidEduLevelCondition =
                    " (lev.eduprogramsubject_id is null and lev.discriminator <> " + additionalEduLevelsEntityCode + ") " +
                 " or (lt.parent_id is null or lt.code_p in ('2', '12', '16', '19', '21', '31', '35'))";
            final String fromSql = "from educationlevelshighschool_t hs " +
                    " inner join educationlevels_t lev on lev.id=hs.educationlevel_id " +
                    " left join structureeducationlevels_t lt on lt.id=lev.leveltype_id " +
                    " where " + invalidEduLevelCondition;

            if (tool.hasResultRows("select hs.id " + fromSql)) {
                errors.add("educationlevelshighschool_t contain rows linked to educatioLevels without eduProgramSubject or from big group");
            }
        }


        if (!errors.isEmpty()) {
            if (autoFix && MS_uni_2x9x2_1to2.autoFix(tool)) {
                qualificationIndexCheck(tool, false);
            } else {
                throw new IllegalStateException(
                        "Qualifications index check faild. You need fix it in 2.9.2 (see special system action). " +
                                "\nПеред обновлением до версии 2.10.0 необходимо исправить ошибки в данных, отраженные в системном действии \"Квалификации профессионального образования в ОП, НПв и УП\" (доступно только в версии 2.9.2)." +
                                "\n - " + errors.stream().collect(Collectors.joining("\n - "))
                );
            }
        }
    }
}