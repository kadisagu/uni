/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.documents;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;

import java.util.Map;

/**
 * @author vip_delete
 * @since 16.10.2009
 */
public class DocumentStudentUtils
{
    public static final String UNI_COMPONENTS_MAP_NAME = "uniComponentsMap";
    public static final String COMPONENT_ADD_POSTFIX = "Add";
    public static final String PARENT_PACKAGE_NAME = "ru.tandemservice.uni.component.documents.d";

    @SuppressWarnings("unchecked")
    public static String getDocumentAddComponent(StudentDocumentType documentType)
    {
        if (null != documentType.getBusinessObjectName())
        {
            if (ApplicationRuntime.containsBean(UNI_COMPONENTS_MAP_NAME))
            {
                Map<String, String> componentsMap = (Map<String, String>) ApplicationRuntime.getBean(UNI_COMPONENTS_MAP_NAME);
                String compName = componentsMap.get(documentType.getBusinessObjectName() + COMPONENT_ADD_POSTFIX);
                if (null != compName) return compName;
            }
        }

        return PARENT_PACKAGE_NAME + documentType.getIndex() + "." + COMPONENT_ADD_POSTFIX;
    }

    @Deprecated
    public static String getDocumentAddComponent(int index)
    {
        return "ru.tandemservice.uni.component.documents.d" + index + ".Add";
    }

    public static String getPrintBeanName(int index)
    {
        return "d" + index + "_studentDocumentPrint";
    }
}
