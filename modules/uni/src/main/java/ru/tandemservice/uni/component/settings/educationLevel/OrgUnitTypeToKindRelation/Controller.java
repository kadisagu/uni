/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitTypeToKindRelation;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));

        prepareDataSource(context);
    }

    protected void refresh(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<OrgUnitType> dataSource = new DynamicListDataSource<>(component, this, 20);

        dataSource.addColumn(new SimpleColumn("Название", OrgUnitType.title().s()).setClickable(false).setOrderable(false));
        for (OrgUnitKind orgUnitKind : UniDaoFacade.getCoreDao().getList(OrgUnitKind.class))
            dataSource.addColumn(new ToggleColumn(orgUnitKind.getTitle(), new String[]{orgUnitKind.getCode()}).setListener("onClick" + orgUnitKind.getCode()));
        model.setDataSource(dataSource);
    }

    //TODO:!!!!!!!!!!!!!!HACK!!!!!!!!!!!
    public void onClick1(IBusinessComponent context)
    {
        getDao().updateRelationByToggle(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING, (Long) context.getListenerParameter());
        refresh(context);
    }

    public void onClick2(IBusinessComponent context)
    {
        getDao().updateRelationByToggle(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, (Long) context.getListenerParameter());
        refresh(context);
    }

    public void onClick3(IBusinessComponent context)
    {
        getDao().updateRelationByToggle(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL, (Long) context.getListenerParameter());
        refresh(context);
    }
}
