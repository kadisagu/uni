package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид возмещения затрат"
 * Имя сущности : compensationType
 * Файл data.xml : catalogs.data.xml
 */
public interface CompensationTypeCodes
{
    /** Константа кода (code) элемента : Госбюджетное место (title) */
    String COMPENSATION_TYPE_BUDGET = "1";
    /** Константа кода (code) элемента : Сверхплановое место (title) */
    String COMPENSATION_TYPE_CONTRACT = "2";

    Set<String> CODES = ImmutableSet.of(COMPENSATION_TYPE_BUDGET, COMPENSATION_TYPE_CONTRACT);
}
