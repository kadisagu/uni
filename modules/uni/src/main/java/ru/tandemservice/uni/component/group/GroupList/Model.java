/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.GroupList;

import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author vip_delete
 */
@State(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model extends ru.tandemservice.uni.component.group.AbstractGroupList.Model
{
    private boolean _allowAddGroups;
    private ISelectModel _curatorListModel;

    public ISelectModel getCuratorListModel()
    {
        return _curatorListModel;
    }

    public void setCuratorListModel(ISelectModel curatorListModel)
    {
        this._curatorListModel = curatorListModel;
    }

    public boolean isAllowAddGroups()
    {
        return _allowAddGroups;
    }

    public void setAllowAddGroups(boolean allowAddGroups)
    {
        _allowAddGroups = allowAddGroups;
    }
}
