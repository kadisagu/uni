package ru.tandemservice.uni.util;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.Zlo;

@Zlo
@DoNotUseMe(comment = "Глотать исключения - зло")
public class SafeSimpleColumn extends SimpleColumn {
    public SafeSimpleColumn(final String caption, final Object key) { super(caption, key); }
    @Override public String getContent(final IEntity entity) {
        try { return super.getContent(entity); }
        catch (final Exception t) { return null; }
    }
}