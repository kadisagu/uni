package ru.tandemservice.uni.dao.mdbio;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.core.util.cache.SpringBeanCache;

import com.healthmarketscience.jackcess.Database;

import java.io.IOException;

/**
 * @author vdanilov
 */
public interface IOrgStructIODao {

    SpringBeanCache<IOrgStructIODao> instance = new SpringBeanCache<IOrgStructIODao>(IOrgStructIODao.class.getName());

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_OrgStructureTable(Database mdb) throws IOException;

}
