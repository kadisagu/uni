package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import java.util.List;

import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.ExtReportContextDefinition;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.logic.ExtReportPermissionUtil;
import org.tandemframework.shared.commonbase.extreports.entity.ExtReport;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

/**
 * @author vdanilov
 */
@Deprecated
public abstract class OrgUnitExtReportContextDefinition extends ExtReportContextDefinition {

    public OrgUnitExtReportContextDefinition(String code) {
        super(code);
    }

    @Override
    public String getPermissionPosftix(ISecured securedObject) {
        if (!OrgUnit.class.isInstance(securedObject)) { return null; }
        return ((OrgUnit)securedObject).getOrgUnitType().getCode();
    }

    @Override
    public void refreshPermissions(List<ExtReport> reportList) {
        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class)) {

            PermissionGroupMeta permissionGroup = new PermissionGroupMeta();
            permissionGroup.setName("ext_report_root_"+getCode()+"_"+description.getCode());
            permissionGroup.setTitle(ExtReportPermissionUtil.getContextBlockTitle(getTitle()));
            permissionGroup.setComment(getComment());

            final PermissionGroupMeta permissionGroupReportsAll = OrgUnitReportUtils.getRuntimeOrgUnitReportTabPermissionGroup(description.getCode());
            permissionGroupReportsAll.getPermissionGroupList().remove(permissionGroup); // replace
            PermissionMetaUtil.addMeta(permissionGroupReportsAll, permissionGroup);
            SecurityRuntime.getInstance().putPermissionGroup(permissionGroup.getName(), permissionGroup);

            ExtReportPermissionUtil.fillPermissions(permissionGroup, reportList, description.getCode());
        }
    }



}
