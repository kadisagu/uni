package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.EduProgramQualificationManager;

@Configuration
public class UniEduProgramEduHsEdit extends BusinessComponentManager 
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(EduProgramQualificationManager.instance().orientationDSConfig())
                .create();
    }

}
