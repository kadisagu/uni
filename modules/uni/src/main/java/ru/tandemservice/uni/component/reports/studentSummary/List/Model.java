/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummary.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.StudentSummaryReport;

/**
 * @author oleyba
 * @since 23.03.2009
 */
@State(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private DynamicListDataSource<StudentSummaryReport> _dataSource;
    private CommonPostfixPermissionModel _secModel;

    public String getViewKey()
    {
        return null == getOrgUnit() ? "studentSummaryReport" : getSecModel().getPermission("orgUnit_viewStudentSummaryReport");
    }

    public Object getSecuredObject()
    {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getAddKey()
    {
        return null == getOrgUnit() ? "addGlobalStoredReport" : getSecModel().getPermission("addStorableReport");
    }

    public String getDeleteKey()
    {
        return null == getOrgUnit() ? "deleteGlobalStoredReport" : getSecModel().getPermission("deleteStorableReport");
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public DynamicListDataSource<StudentSummaryReport> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentSummaryReport> dataSource)
    {
        _dataSource = dataSource;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
