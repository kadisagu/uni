package ru.tandemservice.uni.entity.education.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.IReorganizationObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параметры обучения студентов по направлению подготовки (НПП)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationOrgUnitGen extends EntityBase
 implements IReorganizationObject, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.education.EducationOrgUnit";
    public static final String ENTITY_NAME = "educationOrgUnit";
    public static final int VERSION_HASH = 51446245;
    private static IEntityMeta ENTITY_META;

    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_PERIOD = "developPeriod";
    public static final String L_OPERATION_ORG_UNIT = "operationOrgUnit";
    public static final String L_GROUP_ORG_UNIT = "groupOrgUnit";
    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_USED = "used";
    public static final String P_DEVELOP_COMBINATION_FULL_TITLE = "developCombinationFullTitle";
    public static final String P_DEVELOP_COMBINATION_TITLE = "developCombinationTitle";
    public static final String P_DEVELOP_ORG_UNIT_SHORT_TITLE = "developOrgUnitShortTitle";
    public static final String P_PROGRAM_SUBJECT_SHORT_EXTENDED_TITLE = "programSubjectShortExtendedTitle";
    public static final String P_PROGRAM_SUBJECT_SHORT_EXTENDED_TITLE_WITH_DEVELOP_PERIOD = "programSubjectShortExtendedTitleWithDevelopPeriod";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_FORM_AND_CONDITION = "titleWithFormAndCondition";

    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private EducationLevelsHighSchool _educationLevelHighSchool;     // НПв
    private DevelopForm _developForm;     // Форма освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopPeriod _developPeriod;     // Срок освоения
    private OrgUnit _operationOrgUnit;     // Диспетчерская
    private OrgUnit _groupOrgUnit;     // Деканат
    private int _code;     // Код
    private String _shortTitle;     // Сокращенное название
    private boolean _used = true;     // Используется

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return НПв. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool НПв. Свойство не может быть null.
     */
    public void setEducationLevelHighSchool(EducationLevelsHighSchool educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения. Свойство не может быть null.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Диспетчерская.
     */
    public OrgUnit getOperationOrgUnit()
    {
        return _operationOrgUnit;
    }

    /**
     * @param operationOrgUnit Диспетчерская.
     */
    public void setOperationOrgUnit(OrgUnit operationOrgUnit)
    {
        dirty(_operationOrgUnit, operationOrgUnit);
        _operationOrgUnit = operationOrgUnit;
    }

    /**
     * @return Деканат.
     */
    public OrgUnit getGroupOrgUnit()
    {
        return _groupOrgUnit;
    }

    /**
     * @param groupOrgUnit Деканат.
     */
    public void setGroupOrgUnit(OrgUnit groupOrgUnit)
    {
        dirty(_groupOrgUnit, groupOrgUnit);
        _groupOrgUnit = groupOrgUnit;
    }

    /**
     * @return Код. Свойство не может быть null.
     */
    @NotNull
    public int getCode()
    {
        return _code;
    }

    /**
     * @param code Код. Свойство не может быть null.
     */
    public void setCode(int code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        initLazyForGet("used");
        return _used;
    }

    /**
     * @param used Используется. Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        initLazyForSet("used");
        dirty(_used, used);
        _used = used;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationOrgUnitGen)
        {
            setFormativeOrgUnit(((EducationOrgUnit)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((EducationOrgUnit)another).getTerritorialOrgUnit());
            setEducationLevelHighSchool(((EducationOrgUnit)another).getEducationLevelHighSchool());
            setDevelopForm(((EducationOrgUnit)another).getDevelopForm());
            setDevelopTech(((EducationOrgUnit)another).getDevelopTech());
            setDevelopCondition(((EducationOrgUnit)another).getDevelopCondition());
            setDevelopPeriod(((EducationOrgUnit)another).getDevelopPeriod());
            setOperationOrgUnit(((EducationOrgUnit)another).getOperationOrgUnit());
            setGroupOrgUnit(((EducationOrgUnit)another).getGroupOrgUnit());
            setCode(((EducationOrgUnit)another).getCode());
            setShortTitle(((EducationOrgUnit)another).getShortTitle());
            setUsed(((EducationOrgUnit)another).isUsed());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EducationOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developTech":
                    return obj.getDevelopTech();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "operationOrgUnit":
                    return obj.getOperationOrgUnit();
                case "groupOrgUnit":
                    return obj.getGroupOrgUnit();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "used":
                    return obj.isUsed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
                case "operationOrgUnit":
                    obj.setOperationOrgUnit((OrgUnit) value);
                    return;
                case "groupOrgUnit":
                    obj.setGroupOrgUnit((OrgUnit) value);
                    return;
                case "code":
                    obj.setCode((Integer) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developTech":
                        return true;
                case "developCondition":
                        return true;
                case "developPeriod":
                        return true;
                case "operationOrgUnit":
                        return true;
                case "groupOrgUnit":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "used":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developTech":
                    return true;
                case "developCondition":
                    return true;
                case "developPeriod":
                    return true;
                case "operationOrgUnit":
                    return true;
                case "groupOrgUnit":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "used":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "educationLevelHighSchool":
                    return EducationLevelsHighSchool.class;
                case "developForm":
                    return DevelopForm.class;
                case "developTech":
                    return DevelopTech.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developPeriod":
                    return DevelopPeriod.class;
                case "operationOrgUnit":
                    return OrgUnit.class;
                case "groupOrgUnit":
                    return OrgUnit.class;
                case "code":
                    return Integer.class;
                case "shortTitle":
                    return String.class;
                case "used":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationOrgUnit> _dslPath = new Path<EducationOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationOrgUnit");
    }
            

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return НПв. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getEducationLevelHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Диспетчерская.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getOperationOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> operationOrgUnit()
    {
        return _dslPath.operationOrgUnit();
    }

    /**
     * @return Деканат.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getGroupOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> groupOrgUnit()
    {
        return _dslPath.groupOrgUnit();
    }

    /**
     * @return Код. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getCode()
     */
    public static PropertyPath<Integer> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopCombinationFullTitle()
     */
    public static SupportedPropertyPath<String> developCombinationFullTitle()
    {
        return _dslPath.developCombinationFullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopCombinationTitle()
     */
    public static SupportedPropertyPath<String> developCombinationTitle()
    {
        return _dslPath.developCombinationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopOrgUnitShortTitle()
     */
    public static SupportedPropertyPath<String> developOrgUnitShortTitle()
    {
        return _dslPath.developOrgUnitShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getProgramSubjectShortExtendedTitle()
     */
    public static SupportedPropertyPath<String> programSubjectShortExtendedTitle()
    {
        return _dslPath.programSubjectShortExtendedTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getProgramSubjectShortExtendedTitleWithDevelopPeriod()
     */
    public static SupportedPropertyPath<String> programSubjectShortExtendedTitleWithDevelopPeriod()
    {
        return _dslPath.programSubjectShortExtendedTitleWithDevelopPeriod();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getTitleWithFormAndCondition()
     */
    public static SupportedPropertyPath<String> titleWithFormAndCondition()
    {
        return _dslPath.titleWithFormAndCondition();
    }

    public static class Path<E extends EducationOrgUnit> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelHighSchool;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;
        private OrgUnit.Path<OrgUnit> _operationOrgUnit;
        private OrgUnit.Path<OrgUnit> _groupOrgUnit;
        private PropertyPath<Integer> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Boolean> _used;
        private SupportedPropertyPath<String> _developCombinationFullTitle;
        private SupportedPropertyPath<String> _developCombinationTitle;
        private SupportedPropertyPath<String> _developOrgUnitShortTitle;
        private SupportedPropertyPath<String> _programSubjectShortExtendedTitle;
        private SupportedPropertyPath<String> _programSubjectShortExtendedTitleWithDevelopPeriod;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithFormAndCondition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return НПв. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getEducationLevelHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Диспетчерская.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getOperationOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> operationOrgUnit()
        {
            if(_operationOrgUnit == null )
                _operationOrgUnit = new OrgUnit.Path<OrgUnit>(L_OPERATION_ORG_UNIT, this);
            return _operationOrgUnit;
        }

    /**
     * @return Деканат.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getGroupOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> groupOrgUnit()
        {
            if(_groupOrgUnit == null )
                _groupOrgUnit = new OrgUnit.Path<OrgUnit>(L_GROUP_ORG_UNIT, this);
            return _groupOrgUnit;
        }

    /**
     * @return Код. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getCode()
     */
        public PropertyPath<Integer> code()
        {
            if(_code == null )
                _code = new PropertyPath<Integer>(EducationOrgUnitGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EducationOrgUnitGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(EducationOrgUnitGen.P_USED, this);
            return _used;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopCombinationFullTitle()
     */
        public SupportedPropertyPath<String> developCombinationFullTitle()
        {
            if(_developCombinationFullTitle == null )
                _developCombinationFullTitle = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_DEVELOP_COMBINATION_FULL_TITLE, this);
            return _developCombinationFullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopCombinationTitle()
     */
        public SupportedPropertyPath<String> developCombinationTitle()
        {
            if(_developCombinationTitle == null )
                _developCombinationTitle = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_DEVELOP_COMBINATION_TITLE, this);
            return _developCombinationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getDevelopOrgUnitShortTitle()
     */
        public SupportedPropertyPath<String> developOrgUnitShortTitle()
        {
            if(_developOrgUnitShortTitle == null )
                _developOrgUnitShortTitle = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_DEVELOP_ORG_UNIT_SHORT_TITLE, this);
            return _developOrgUnitShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getProgramSubjectShortExtendedTitle()
     */
        public SupportedPropertyPath<String> programSubjectShortExtendedTitle()
        {
            if(_programSubjectShortExtendedTitle == null )
                _programSubjectShortExtendedTitle = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_PROGRAM_SUBJECT_SHORT_EXTENDED_TITLE, this);
            return _programSubjectShortExtendedTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getProgramSubjectShortExtendedTitleWithDevelopPeriod()
     */
        public SupportedPropertyPath<String> programSubjectShortExtendedTitleWithDevelopPeriod()
        {
            if(_programSubjectShortExtendedTitleWithDevelopPeriod == null )
                _programSubjectShortExtendedTitleWithDevelopPeriod = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_PROGRAM_SUBJECT_SHORT_EXTENDED_TITLE_WITH_DEVELOP_PERIOD, this);
            return _programSubjectShortExtendedTitleWithDevelopPeriod;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.EducationOrgUnit#getTitleWithFormAndCondition()
     */
        public SupportedPropertyPath<String> titleWithFormAndCondition()
        {
            if(_titleWithFormAndCondition == null )
                _titleWithFormAndCondition = new SupportedPropertyPath<String>(EducationOrgUnitGen.P_TITLE_WITH_FORM_AND_CONDITION, this);
            return _titleWithFormAndCondition;
        }

        public Class getEntityClass()
        {
            return EducationOrgUnit.class;
        }

        public String getEntityName()
        {
            return "educationOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDevelopCombinationFullTitle();

    public abstract String getDevelopCombinationTitle();

    public abstract String getDevelopOrgUnitShortTitle();

    public abstract String getProgramSubjectShortExtendedTitle();

    public abstract String getProgramSubjectShortExtendedTitleWithDevelopPeriod();

    public abstract String getTitle();

    public abstract String getTitleWithFormAndCondition();
}
