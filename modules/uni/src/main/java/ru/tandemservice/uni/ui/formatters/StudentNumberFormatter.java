/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui.formatters;

import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author vip_delete
 */
public class StudentNumberFormatter implements IFormatter
{
    public static final StudentNumberFormatter INSTANCE = new StudentNumberFormatter();

    private static final int MIN_CAPACITY = 4;
    private static final int MAX_CAPACITY = 8;
    private static final Integer CAPACITY;
    static
    {
        final String property = ApplicationRuntime.getProperty("student.personalNumberCapacity");
        if (property == null)
        {
            CAPACITY = 4;
        }
        else
        {
            final Integer capacity = Ints.tryParse(property);
            CAPACITY = (capacity != null && capacity >= MIN_CAPACITY && capacity <= MAX_CAPACITY) ? capacity : null;
        }
    }

    public static int getCapacity()
    {
        if (CAPACITY == null)
        {
            throw new ApplicationException("Неверное значение параметра «student.personalNumberCapacity» в properties-файле. " +
                                                   "Оно должно быть целым числом от " + MIN_CAPACITY + " до " + MAX_CAPACITY + ".");
        }
        return CAPACITY;
    }


    @Override
    public String format(Object source)
    {
        return source != null ? StringUtils.leftPad(source.toString(), getCapacity() + 2, '0') : "";
    }
}
