package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x9_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.9")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность developPeriod

        //  свойство eduProgramDuration стало обязательным
        {

//
// Решили в этой итерации не делать
//

//            tool.getStatement().execute("select count(*) from developperiod_t where eduprogramduration_id is null");
//            if (tool.getStatement().getResultSet().next() && tool.getStatement().getResultSet().getLong(1) > 0)
//            {
//                throw new IllegalStateException("Всем срокам освоения должна быть указана продолжительность (см. показатель корректности данных учебного процесса для 2.6.7). DEV-5406");
//            }
//
//            // сделать колонку NOT NULL
//            tool.setColumnNullable("developperiod_t", "eduprogramduration_id", false);

        }


    }
}