/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentDocumentList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author euroelessar
 * @since 23.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrgUnit(getNotNull(OrgUnit.class, model.getOrgUnitId()));
        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(CompensationType.class, CompensationType.P_SHORT_TITLE));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopPeriodListModel(EducationCatalogsManager.getDevelopPeriodSelectModel());
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        MQBuilder studentBuilder = new MQBuilder(Student.ENTITY_CLASS, "s");
        addAdditionalRestrictions(model, studentBuilder);
        MQBuilder docBuilder = new MQBuilder(StudentDocument.ENTITY_CLASS, "sd");
        applyFilters(model, docBuilder, studentBuilder);
        docBuilder.add(MQExpression.in("sd", StudentDocument.L_STUDENT, studentBuilder));

        docBuilder.addJoin("sd", StudentDocument.student(), "s_o");
        docBuilder.addJoin("s_o", Student.person(), "p_o");
        docBuilder.addJoin("p_o", Person.identityCard(), "idc_o");
        OrderDescriptionRegistry reg = new OrderDescriptionRegistry("sd");
        reg.setOrders("person.fullFio", new OrderDescription("idc_o", IdentityCard.lastName()), new OrderDescription("idc_o", IdentityCard.firstName()), new OrderDescription("idc_o", IdentityCard.middleName()));
        reg.applyOrder(docBuilder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), docBuilder, getSession());
    }
    
    @SuppressWarnings("unchecked")
    protected void applyFilters(Model model, MQBuilder docBuilder, MQBuilder studentBuilder)
    {
        IDataSettings settings = model.getSettings();

        Object personLastName = settings.get("lastName");
        Object personFirstName = settings.get("firstName");
        Object documentNumber = settings.get("documentNumber");
        Object registeredFromFilter = settings.get("registeredFromFilter");
        Object registeredToFilter = settings.get("registeredToFilter");
        Object courseList = settings.get("courseList");
        Object compensationType = settings.get("compensationType");
        Object territorialOrgUnitList = settings.get("territorialOrgUnitList");
        Object producingOrgUnitList = settings.get("producingOrgUnitList");
        Object qualificationList = settings.get("qualification");
        Object developFormList = settings.get("developFormList");
        Object developConditionList = settings.get("developConditionList");
        Object developPeriodList = settings.get("developPeriodList");

        studentBuilder.addJoin("s", Student.educationOrgUnit(), "eduOu");

        FilterUtils.applySimpleLikeFilter(studentBuilder, "s", Student.person().identityCard().lastName().s(), (String) personLastName);
        FilterUtils.applySimpleLikeFilter(studentBuilder, "s", Student.person().identityCard().firstName().s(), (String) personFirstName);
        if (null != documentNumber)
            FilterUtils.applySelectFilter(docBuilder, "sd", StudentDocument.number().s(), ((Long) documentNumber).intValue());
        if (registeredFromFilter != null)
            docBuilder.add(MQExpression.greatOrEq("sd", StudentDocument.formingDate().s(), registeredFromFilter));
        if (registeredToFilter != null)
            docBuilder.add(MQExpression.lessOrEq("sd", StudentDocument.formingDate().s(), registeredToFilter));
        FilterUtils.applySelectFilter(studentBuilder, "s", Student.course().s(), courseList);
        FilterUtils.applySelectFilter(studentBuilder, "s", Student.compensationType().s(), compensationType);
        FilterUtils.applySelectFilter(studentBuilder, "eduOu", EducationOrgUnit.territorialOrgUnit().s(), territorialOrgUnitList);
        FilterUtils.applySelectFilter(studentBuilder, "eduOu", EducationOrgUnit.educationLevelHighSchool().orgUnit().s(), producingOrgUnitList);
        FilterUtils.applySelectFilter(studentBuilder, "eduOu", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), qualificationList);
        FilterUtils.applySelectFilter(studentBuilder, "eduOu", EducationOrgUnit.developForm().s(), developFormList);
        FilterUtils.applySelectFilter(studentBuilder, "eduOu", EducationOrgUnit.developCondition().s(), developConditionList);
        FilterUtils.applySelectFilter(studentBuilder, "eduOu", EducationOrgUnit.developPeriod().s(), developPeriodList);
    }

    protected void addAdditionalRestrictions(Model model, MQBuilder builder)
    {
        Collection<String> kindSet = UniDaoFacade.getOrgstructDao().getAllowStudentsOrgUnitKindCodes(model.getOrgUnit());
        List<AbstractExpression> expressions = new ArrayList<>();

        //Для выпускающих подразделений – студенты, для которых текущее подразделение является выпускающим через связь с НПв.
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
            expressions.add(MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_ORG_UNIT, model.getOrgUnit()));

        //Для формирующих подразделений будет отображаться перечень студентов, для которых текущее подразделение является формирующим через связь с направлением подготовки (специальностью) подразделения.
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
            expressions.add(MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit()));

        //Для территориальных подразделений – студенты, для которых текущее подразделение является территориальным подразделением через связь с направлением подготовки (специальностью) подразделения.
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
            expressions.add(MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit()));

        builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
        builder.add(expressions.isEmpty() ? MQExpression.eq("s", Student.P_ID, -1) : MQExpression.or(expressions.toArray(new AbstractExpression[expressions.size()])));
    }
}