/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uni.dao.IUniDao;

import java.lang.reflect.InvocationTargetException;

/**
 * @author vip_delete
 * не используйте этот класс. наследуйдесь от AbstractBusinessController
 * этот класс использует Deprecated сущности
 */
@Deprecated
@SuppressWarnings("deprecation")
public abstract class UniListController<IDAO extends IUniDao<Model>, Model> extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    protected static final String ADD_ROW_METHOD = "addRow";
    protected static final String EDIT_ROW_METHOD = "editRow";
    protected static final String DELETE_ROW_METHOD = "deleteRow";

    protected abstract void prepareDataSource(IBusinessComponent context);


    @Override
    public void onRefreshComponent(final IBusinessComponent context)
    {
        super.onRefreshComponent(context);
        getDao().prepare(getModel(context));
        try
        {
            prepareDataSource(context);
            getModel(context).getClass().getMethod("setSelectedPage", String.class).invoke(getModel(context), new Object[] { null });
        }
        catch (final Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public void addRow(final IBusinessComponent context) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        getModel(context).getClass().getMethod("setRowId", Long.class).invoke(getModel(context), new Object[] { null });
        context.createDefaultChildRegion(new ComponentActivator(getAddRowComponent(context)));
    }

    protected String getAddRowComponent(final IBusinessComponent context)
    {
        return getAddEditRowComponent(context);
    }

    protected String getAddEditRowComponent(final IBusinessComponent context)
    {
        throw new UnsupportedOperationException("getAddEditRowComponent");
    }

    protected String getEditRowComponent(final IBusinessComponent context)
    {
        return getAddEditRowComponent(context);
    }

    public void editRow(final IBusinessComponent context) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        getModel(context).getClass().getMethod("setRowId", Long.class).invoke(getModel(context), (Long) context.getListenerParameter());
        context.createDefaultChildRegion(new ComponentActivator(getEditRowComponent(context)));
    }

    public void deleteRow(final IBusinessComponent context)
    {
        getDao().deleteRow(context);
        refresh(context);
    }

    protected void refresh(final IBusinessComponent context)
    {
        try
        {
            ((AbstractListDataSource<?>) getModel(context).getClass().getMethod("getDataSource").invoke(getModel(context))).refresh();
        }
        catch (final Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateListDataSource(final IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    public static IndicatorColumn getIcoColumn(final String ico, final String title)
    {
        final IndicatorColumn icoColumn = new IndicatorColumn("Иконка", null);
        icoColumn.setOrderable(false);
        icoColumn.setClickable(false);
        icoColumn.defaultIndicator(new IndicatorColumn.Item(ico, title));
        return icoColumn;
    }

    protected static ActionColumn getEditColumn()
    {
        return new ActionColumn("Редактировать", ActionColumn.EDIT, EDIT_ROW_METHOD);
    }

    @SuppressWarnings("deprecation")
    protected static ActionColumn getDeleteColumn(final String alert, final Object... params)
    {
        return new ActionColumn("Удалить", ActionColumn.DELETE, DELETE_ROW_METHOD, alert, params);
    }
}
