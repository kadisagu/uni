/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniStudent.logic.list;

import org.tandemframework.caf.logic.ExecutionContext;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
public class StudentSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public StudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public IEducationOrgUnitContextHandler getContextHandler(ExecutionContext context)
    {
        return new BaseStudentListContextHandler(false);
    }
}
