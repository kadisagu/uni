/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.compensationType.CompensationTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.CompensationType;

/**
 * @author AutoGenerator
 * @since 16.06.2008
 */
public class Controller extends DefaultCatalogPubController<CompensationType, Model, IDAO>
{
    @Override
    @SuppressWarnings("unchecked")
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<CompensationType> dataSource = new DynamicListDataSource<CompensationType>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", CompensationType.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", CompensationType.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", CompensationType.P_GROUP).setClickable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", CompensationType.P_TITLE).setPermissionKey(model.getCatalogItemDelete()));
        return dataSource;
    }
}
