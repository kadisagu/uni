/* $Id: DAO.java 22487 2012-04-04 13:16:00Z vzhukov $ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3s.EducationLevelHighGos3sPub;

import java.util.Collections;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3s;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author oleyba
 * @since 5/11/11
 */
public class DAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.DAO<EducationLevelHighGos3s, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        MQBuilder levelBuilder = new MQBuilder(StructureEducationLevels.ENTITY_CLASS, "s")
        .add(MQExpression.eq("s", StructureEducationLevels.P_HIGH, true))
        .add(MQExpression.eq("s", StructureEducationLevels.P_GOS3, true));

        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(levelBuilder.<StructureEducationLevels>getResultList(getSession()), true));

        model.setQualificationList(Collections.<Qualifications>emptyList());
    }
}