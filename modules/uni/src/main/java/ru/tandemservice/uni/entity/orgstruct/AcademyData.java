package ru.tandemservice.uni.entity.orgstruct;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.gen.*;

/**
 * Дополнительные данные ОУ
 */
public class AcademyData extends AcademyDataGen
{
    public static AcademyData getInstance()
    {
        return DataAccessServices.dao().getList(AcademyData.class).get(0);
    }
    
    @Override
    @EntityDSLSupport
    public String getLicenceTitle()
    {
        StringBuilder result = new StringBuilder();

        if (null != getLicenceSeria())
        {
            result.append(getLicenceSeria());
        }

        if (null != getLicenceNumber())
        {
            result.append(result.length() > 0 ? " №" : "№").append(getLicenceNumber());
        }

        if (null != getLicenceRegNumber())
        {
            result.append(result.length() > 0 ? " (рег. № " : "(рег. № ");
            result.append(getLicenceRegNumber()).append(")");
        }

        if (null != getLicenceDate())
        {
            result.append(result.length() > 0 ? " от " : "от ");
            result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getLicenceDate()));
        }

        if (null != getLicensingAgency())
        {
            result.append(result.length() > 0 ? ", выдана органом \"" : "выдана органом \"");
            result.append(getLicensingAgency()).append("\"");
        }

        if (null != getLicenceExpiriationDate())
        {
            result.append(result.length() > 0 ? ", действительна до " : "действительна до ");
            result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getLicenceExpiriationDate()));
        }

        return result.toString();
    }

    @Override
    @EntityDSLSupport
    public String getCertificateTitle()
    {
        StringBuilder result = new StringBuilder();

        if (null != getCertificateSeria())
        {
            result.append(getCertificateSeria());
        }

        if (null != getCertificateNumber())
        {
            result.append(result.length() > 0 ? " №" : "№").append(getCertificateNumber());
        }

        if (null != getCertificateRegNumber())
        {
            result.append(result.length() > 0 ? " (рег. № " : "(рег. № ");
            result.append(getCertificateRegNumber()).append(")");
        }

        if (null != getCertificateDate())
        {
            result.append(result.length() > 0 ? " от " : "от ");
            result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCertificateDate()));
        }

        if (null != getCertificationAgency())
        {
            result.append(result.length() > 0 ? ", выдано органом \"" : "выдано органом \"");
            result.append(getCertificationAgency()).append("\"");
        }

        if (null != getCertificateExpiriationDate())
        {
            result.append(result.length() > 0 ? ", действительно до " : "действительно до ");
            result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCertificateExpiriationDate()));
        }

        return result.toString();
    }
}