/* $Id$ */
package ru.tandemservice.uni.dao;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.IScriptOwner;
import ru.tandemservice.uni.util.groovy.IScriptInstance;

/**
 * @author Nikolay Fedorovskih
 * @since 16.07.2015
 */
public interface IUniScriptDao extends INeedPersistenceSupport
{
    SpringBeanCache<IUniScriptDao> instance = new SpringBeanCache<>(IUniScriptDao.class.getName());

    void doSaveUserScript(IEntity scriptOwner);

    interface IScriptWrapper<S extends IScriptInstance, O extends IScriptOwner>
    {
        S getScript();
        O getScriptOwner();
    }

    <S extends IScriptInstance, O extends IScriptOwner> IScriptWrapper<S, O> buildScriptWrapper(final O scriptOwner, final Class<S> klass, String configKey, final String defaultScriptPath, String scriptTitle);

    <S extends IScriptInstance> S buildScriptItemInstance(final IScriptItem scriptItem, final Class<S> klass);
}