package ru.tandemservice.uni.component.settings.CombinationsFormsAndConditionsAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * 
 * @author nkokorina
 * @since 01.03.2010
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(getModel(component), errors);
        if (errors.hasErrors())
        {
            return;
        }

        getDao().update(getModel(component));
        deactivate(component);
    }
}
