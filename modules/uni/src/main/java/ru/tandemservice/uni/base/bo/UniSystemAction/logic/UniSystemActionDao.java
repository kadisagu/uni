/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.UniSystemAction.logic;

import com.healthmarketscience.jackcess.*;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.utils.AddressItemTitleAutoupdateUtils;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.group.IAbstractGroupTitleDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class UniSystemActionDao extends UniBaseDao implements IUniSystemActionDao
{
    @Override
    @SuppressWarnings("unchecked")
    public void correctIdentityCardFIOInNewTransaction()
    {
        Session session = getSession();
        int counter = 0;
        for (Iterator<IdentityCard> iterator = session.createQuery("from " + IdentityCard.ENTITY_CLASS).iterate(); iterator.hasNext();)
        {
            IdentityCard idCard = iterator.next();

            idCard.setFirstName(StringUtils.capitalize(idCard.getFirstName().toLowerCase()));
            idCard.setLastName(StringUtils.capitalize(idCard.getLastName().toLowerCase()));
            if (!CoreUtils.isNull(idCard.getMiddleName()))
            {
                idCard.setMiddleName(StringUtils.capitalize(idCard.getMiddleName().toLowerCase()));
            }
            getSession().update(idCard);

            counter++;
            if (counter % 4000 == 0)
            {
                counter = 0;
                session.flush();
                session.clear();
            }
        }
        session.flush();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void updateAddressItemsFullTitles()
    {
        Session session = getSession();
        int counter = 0;
        for (Iterator<AddressItem> iterator = session.createQuery("from " + AddressItem.ENTITY_CLASS).iterate(); iterator.hasNext();)
        {
            AddressItem item = iterator.next();
	        AddressItemTitleAutoupdateUtils.updateFullTitleProperties(item);

            counter++;
            if (counter % 4000 == 0)
            {
                counter = 0;
                session.flush();
                session.clear();
            }
        }
        session.flush();
    }

    @Override
    public void updateGroupTitles()
    {
        MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");
        builder.add(MQExpression.notEq("g", Group.P_ARCHIVAL, Boolean.TRUE));

        GroupTitleAlgorithm algorithm = UniDaoFacade.getCoreDao().get(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_CURRENT, Boolean.TRUE);
        if (algorithm == null) return;
        IAbstractGroupTitleDAO generator = (IAbstractGroupTitleDAO) ApplicationRuntime.getBean(algorithm.getDaoName());

        System.out.println("-------------- Алгоритм коррекции названий групп запущен --------------");
        System.out.println("Старое название\t -> \tНовое название");

        for (Group group : builder.<Group>getResultList(getSession()))
        {
            String oldTitle = group.getTitle();
            String title = generator.getTitle(group, group.getNumber());

            if (!title.equals(oldTitle))
            {
                update(group);
                System.out.println("     " + oldTitle + "\t -> \t" + group.getTitle());
            }
        }
        System.out.println("------------------ Коррекция названий групп завершена -----------------");
    }

    @Override
    public Integer printEducationLevelsReport() throws Exception
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создать лист
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.getSettings().setCopies(1);

        // создаем форматы
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        WritableCellFormat titleFormat = new WritableCellFormat(arial8bold);
        titleFormat.setWrap(true);
        titleFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        titleFormat.setBackground(Colour.GRAY_25);

        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        headerFormat.setWrap(true);

        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);

        // рисуем шапку
        int column = 0;

        sheet.addCell(new Blank(column, 0, titleFormat));
        sheet.setColumnView(column, 5);
        column++;

        sheet.addCell(new Label(column, 0, "Формирующее подр.", titleFormat));
        sheet.setColumnView(column, 40);
        column++;

        sheet.addCell(new Label(column, 0, "Территориальное подр.", titleFormat));
        sheet.setColumnView(column, 35);
        column++;

        sheet.addCell(new Label(column, 0, "Форма обучения", titleFormat));
        sheet.setColumnView(column, 15);
        column++;

        sheet.addCell(new Label(column, 0, "Условие", titleFormat));
        sheet.setColumnView(column, 23);
        column++;

        sheet.addCell(new Label(column, 0, "Технология", titleFormat));
        sheet.setColumnView(column, 14);
        column++;

        sheet.addCell(new Label(column, 0, "Срок", titleFormat));
        sheet.setColumnView(column, 8);
        column++;

        sheet.addCell(new Label(column, 0, "Сокр.назв.", titleFormat));
        sheet.setColumnView(column, 10);
        column++;

        sheet.addCell(new Label(column, 0, "Код", titleFormat));
        sheet.setColumnView(column, 6);

        // рисуем данные
        MQBuilder eduLevelsBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        eduLevelsBuilder.addOrder("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_TITLE);
        eduLevelsBuilder.addOrder("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        eduLevelsBuilder.addOrder("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE);

        Map<Long, List<EducationOrgUnit>> eduOrgUnitsListsMap = new HashMap<>();
        for (EducationOrgUnit item : eduLevelsBuilder.<EducationOrgUnit>getResultList(getSession()))
        {
            Long id = item.getEducationLevelHighSchool().getId();
            List<EducationOrgUnit> eduOrgUnitList = eduOrgUnitsListsMap.get(id);
            if (null == eduOrgUnitList) eduOrgUnitList = new ArrayList<>();
            eduOrgUnitsListsMap.put(id, eduOrgUnitList);
            eduOrgUnitList.add(item);
        }

        MQBuilder eduLevelHSBuilder = new MQBuilder(EducationLevelsHighSchool.ENTITY_CLASS, "el");
        eduLevelHSBuilder.addOrder("el", EducationLevelsHighSchool.L_ORG_UNIT + "." + OrgUnit.P_ID);

        Map<Long, List<EducationLevelsHighSchool>> elhsListsMap = new HashMap<>();
        for (EducationLevelsHighSchool eduLevel : eduLevelHSBuilder.<EducationLevelsHighSchool>getResultList(getSession()))
        {
            List<EducationLevelsHighSchool> elhsList = elhsListsMap.get(eduLevel.getOrgUnit().getId());
            if (null == elhsList) elhsList = new ArrayList<>();
            elhsListsMap.put(eduLevel.getOrgUnit().getId(), elhsList);
            elhsList.add(eduLevel);
        }

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.addOrder("ou", OrgUnit.P_TITLE);

        int i = 1;
        int row = 2;
        for (OrgUnit orgUnit : builder.<OrgUnit>getResultList(getSession()))
        {
            if (elhsListsMap.containsKey(orgUnit.getId()))
            {
                List<EducationLevelsHighSchool> elhsList = elhsListsMap.get(orgUnit.getId());
                Collections.sort(elhsList, EducationLevelDAO.EDU_LEVEL_HIGH_SCHOOL_OKSO_TITLE_COMPARATOR);

                for (EducationLevelsHighSchool level : elhsList)
                {
                    if (eduOrgUnitsListsMap.containsKey(level.getId()))
                    {
                        sheet.addCell(new Number(0, row, i++, rowFormat));
                        sheet.addCell(new Label(1, row, level.getPrintTitle(), headerFormat));
                        sheet.addCell(new Label(2, row, level.getOrgUnit().getFullTitle(), headerFormat));
                        row++;

                        for (EducationOrgUnit item : eduOrgUnitsListsMap.get(level.getId()))
                        {
                            sheet.addCell(new Label(1, row, item.getFormativeOrgUnit().getFullTitle(), rowFormat));
                            sheet.addCell(new Label(2, row, item.getTerritorialOrgUnit().getTerritorialFullTitle(), rowFormat));
                            sheet.addCell(new Label(3, row, item.getDevelopForm().getTitle(), rowFormat));
                            sheet.addCell(new Label(4, row, item.getDevelopCondition().getTitle(), rowFormat));
                            sheet.addCell(new Label(5, row, item.getDevelopTech().getTitle(), rowFormat));
                            sheet.addCell(new Label(6, row, item.getDevelopPeriod().getTitle(), rowFormat));
                            sheet.addCell(new Label(7, row, item.getShortTitle(), rowFormat));
                            sheet.addCell(new Label(8, row, Integer.toString(item.getCode()), rowFormat));
                            row++;
                        }
                    }
                }
            }
        }

        workbook.write();
        workbook.close();

        // регистрируем его в универсальном компоненте рендера
        return PrintReportTemporaryStorage.registerTemporaryPrintForm(out.toByteArray(), "EducationLevels.xls");
    }

    @Override
    public void deleteCategorylessPersons()
    {
        MQBuilder builder = new MQBuilder(Person.ENTITY_CLASS, "p");
        MQBuilder subBuilder = new MQBuilder(PersonRole.ENTITY_CLASS, "pr", new String[]{PersonRole.L_PERSON + ".id"});
        builder.add(MQExpression.notIn("p", Person.P_ID, subBuilder));
        List<Person> personList = builder.getResultList(getSession());
        for (Person person : personList)
        {
            PersonManager.instance().dao().deletePerson(person.getId());
        }
    }

    @Override
    public void updateGroupTitlesWithNulls()
    {
        MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");
        builder.add(MQExpression.like("g", Group.P_TITLE, CoreStringUtils.escapeLike("null")));

        GroupTitleAlgorithm algorithm = UniDaoFacade.getCoreDao().get(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_CURRENT, Boolean.TRUE);
        if (algorithm == null) return;
        IAbstractGroupTitleDAO generator = (IAbstractGroupTitleDAO) ApplicationRuntime.getBean(algorithm.getDaoName());

        System.out.println("-------------- Алгоритм коррекции названий групп запущен --------------");
        System.out.println("Старое название\t -> \tНовое название");

        for (Group group : builder.<Group>getResultList(getSession()))
        {
            String oldTitle = group.getTitle();
            String title = generator.getTitle(group, group.getNumber());

            if (!title.equals(oldTitle))
            {
                update(group);
                System.out.println("     " + oldTitle + "\t -> \t" + group.getTitle());
            }
        }
        System.out.println("------------------ Коррекция названий групп завершена -----------------");

    }

    @Override
    public void exportEduData(Database mdb) throws IOException
    {
        if (null != mdb.getTable("edudata_t"))
            return;

        final Table eduData_t = new TableBuilder("edudata_t")
                .addColumn(new ColumnBuilder("acid", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("lvlid", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("lvlpid", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("lvltitle", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("lvltp", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("okso", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("qcode", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("allowstudents", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("eduhsid", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("eduhstitle", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("form", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("cond", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("tech", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("period", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("cat", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("compensation", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("course", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("status", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("sid", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("sarch", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        final IDQLStatement statement = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                .joinEntity("e", DQLJoinType.left, Student.class, "s", eq(property(EducationOrgUnit.id().fromAlias("e")), property(Student.educationOrgUnit().id().fromAlias("s"))))
                .column(property("e"))
                .column(property("s"))
                .createStatement(getSession());

        final long topOrgUnitId = OrgUnitManager.instance().dao().getTopOrgUnitId();
        for (final Object[] row : scrollRows(statement))
        {
            final EducationOrgUnit eduOrgUnit = (EducationOrgUnit) row[0];
            final Student student = (Student) row[1];

            eduData_t.addRow(
                    topOrgUnitId,
                    eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getId().toString(),
                    eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel() != null ? eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getId().toString() : null,
                    eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitle(),
                    eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode(),
                    eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix(),
                    eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification() != null ? eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getQualification().getCode() : null,
                    eduOrgUnit.getEducationLevelHighSchool().isAllowStudents() ? "true" : "false",
                    eduOrgUnit.getEducationLevelHighSchool().getId().toString(),
                    eduOrgUnit.getEducationLevelHighSchool().getFullTitle(),
                    eduOrgUnit.getDevelopForm().getCode(),
                    eduOrgUnit.getDevelopCondition().getCode(),
                    eduOrgUnit.getDevelopTech().getCode(),
                    eduOrgUnit.getDevelopPeriod().getTitle(),
                    student != null ? student.getStudentCategory().getCode() : null,
                    student != null ? student.getCompensationType().getCode() : null,
                    student != null ? student.getCourse().getIntValue() : null,
                    student != null ? student.getStatus().getCode() : null,
                    student != null ? student.getId().toString() : null,
                    student != null ? student.isArchival() ? "true" : "false" : null);
        }
    }
}
