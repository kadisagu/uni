/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.ui.EducationLevelsAutocompleteModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vip_delete
 */
public class DAO extends DefaultCatalogAddEditDAO<EducationLevelsHighSchool, Model>
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.setCatalogCode(EducationLevelsHighSchool.ENTITY_NAME);
        super.prepare(model);

        // фильтруем (реально должны остаться НПО старые, ДПО и ОО)
        DQLSelectBuilder levelBuilder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "s")
                .where(notExists(new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "ss")
                                         .where(eq(property("ss", StructureEducationLevels.parent()), property("s"))).buildQuery()));
        List<StructureEducationLevels> list = getList(levelBuilder);
        CollectionUtils.filter(list, object -> {
            if (object.isGos3()) {
                // 2009 и 2013: все есть в направлениях ОП
                return false;
            }
            if (object.isBasic()) {
                return false;
            }
            if (object.isGos2()) {
                if (object.isHigh()) {
                    // ВПО(ГОС2-ОКСО) - есть в перечнях ОП
                    return false;
                } else if (object.isMiddle()) {
                    // СПО(ГОС2-ОКСО) - есть в перечнях ОП
                    return false;
                }
            }
            return true;
        });

        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(list, StructureEducationLevels.COMPARATOR, false));
        if (model.getCatalogItem().getId() != null)
            model.setLevelType(model.getCatalogItem().getEducationLevel().getLevelType());

        model.setOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING) {
            @Override public ListResult<OrgUnit> findValues(String filter) {
                EducationLevelsHighSchool catalogItem = model.getCatalogItem();
                if (catalogItem.getEducationLevel() == null) return ListResult.getEmpty();
                return super.findValues(filter);
            }

            @Override public Object getValue(Object primaryKey) {
                EducationLevelsHighSchool catalogItem = model.getCatalogItem();
                if (catalogItem.getEducationLevel() == null) return null;
                return super.getValue(primaryKey);
            }
        });
        model.setEducationLevelListModel(new EducationLevelsAutocompleteModel() {
            @SuppressWarnings("unchecked")
            @Override
            protected List<EducationLevels> getFilteredList()
            {
                if (model.getLevelType() == null) return Collections.emptyList();

                Criteria c = getSession().createCriteria(EducationLevels.ENTITY_CLASS);
                c.add(Restrictions.eq(EducationLevels.L_LEVEL_TYPE, model.getLevelType()));
                return c.list();
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.getCatalogItem().getCloseDate() != null && !model.getCatalogItem().getCloseDate().after(model.getCatalogItem().getOpenDate()))
        {
            errors.add("Дата открытия должна быть раньше даты закрытия.", "openDate");
            errors.add("Дата закрытия должна быть позже даты открытия.", "closeDate");
        }
    }

    @Override
    public void update(Model model)
    {
        model.getCatalogItem().setAllowStudents(model.getCatalogItem().getEducationLevel().getLevelType().isAllowStudents());
        super.update(model);
    }
}