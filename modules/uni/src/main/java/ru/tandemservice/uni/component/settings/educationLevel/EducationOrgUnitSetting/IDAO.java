/* $Id$ */
package ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitSetting;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 02.06.2011
 */
public interface IDAO extends IUniDao<Model>
{
}
