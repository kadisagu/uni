/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelMiddleGos2.EducationLevelMiddleGos2Pub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.entity.catalog.EducationLevelMiddle;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

/**
 * @author vip_delete
 * @since 12.11.2009
 */
public class DAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.DAO<EducationLevelMiddle, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        MQBuilder levelBuilder = new MQBuilder(StructureEducationLevels.ENTITY_CLASS, "s")
                .add(MQExpression.eq("s", StructureEducationLevels.P_MIDDLE, true))
                .add(MQExpression.eq("s", StructureEducationLevels.P_GOS2, true));

        MQBuilder qualificationsBuilder = new MQBuilder(Qualifications.ENTITY_CLASS, "q")
                .add(MQExpression.in("q", Qualifications.P_CODE,
                                     QualificationsCodes.BAZOVYY_UROVEN_S_P_O,
                                     QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O));


        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(levelBuilder.<StructureEducationLevels>getResultList(getSession()), true));

        model.setQualificationList(qualificationsBuilder.<Qualifications>getResultList(getSession()));
    }
}
