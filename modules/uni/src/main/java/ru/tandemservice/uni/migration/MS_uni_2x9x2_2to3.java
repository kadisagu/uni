/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;

/**
 * @author Nikolay Fedorovskih
 * @since 17.12.2015
 */
public class MS_uni_2x9x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Блок 9. Для параметров выпуска студентов по направлению подготовки (НПв) не задано направление профессионального образования.
        // Блок 10. Параметры выпуска студентов по направлению подготовки (НПв) не должны быть созданы на базе укрупненной группы направлений.
        final short additionalEduLevelsEntityCode = tool.entityCodes().ensure("educationLevelAdditional");
        final String levWhere =
                " (lev.eduprogramsubject_id is null and lev.discriminator <> " + additionalEduLevelsEntityCode + ") " +
             " or (lt.parent_id is null or lt.code_p in ('2', '12', '16', '19', '21', '31', '35'))";
        final String fromSql = "from educationlevelshighschool_t hs " +
                " inner join educationlevels_t lev on lev.id=hs.educationlevel_id " +
                " left join structureeducationlevels_t lt on lt.id=lev.leveltype_id " +
                " where " + levWhere;

        long count = tool.getNumericResult("select count(*) " + fromSql);
        if (count == 0) {
            return; // everything is ok
        }

        // попытаемся их удалить
        count -= MigrationUtils.deleteNotRelatedRows(tool, "educationLevelsHighSchool", "select hs.id " + fromSql);
        if (count < 0) {
            throw new IllegalStateException("wtf-0");
        }
        if (count == 0) {
            return;  // all deleted
        }

        // Дальше я не гарантирую корректной исправление данных - правьте, пожалуйста, показатель руками
        // А для стендов разработчкиков можно сделать что-то, лишь бы исправить данные.
        if ( ! MS_uni_2x9x2_1to2.canAutoFix()) {
            return;
        }

        final String subjSQL = tool.getDialect().getSQLTranslator().toSql(
                new SQLSelectQuery()
                        .from(SQLFrom.table("educationlevels_t", "lev")
                                      .leftJoin(SQLFrom.table("structureeducationlevels_t", "lt"), "lt.id=lev.leveltype_id"))
                        .top(1).column("lev.id")
                        .where("lev.eduprogramsubject_id=? and not(" + levWhere + ")")
        );

        final String qualSQL = tool.getDialect().getSQLTranslator().toSql(
                new SQLSelectQuery()
                        .from(SQLFrom.table("educationlevels_t", "lev")
                                      .leftJoin(SQLFrom.table("structureeducationlevels_t", "lt"), "lt.id=lev.leveltype_id")
                                      .innerJoin(SQLFrom.table("educationlevelshighschool_t", "hs"), "lev.id=hs.educationlevel_id"))
                        .top(1).column("lev.id")
                        .where("hs.assignedqualification_id=? and not(" + levWhere + ")")
        );

        final String randomSQL = tool.getDialect().getSQLTranslator().toSql(
                new SQLSelectQuery()
                        .from(SQLFrom.table("educationlevels_t", "lev")
                                      .leftJoin(SQLFrom.table("structureeducationlevels_t", "lt"), "lt.id=lev.leveltype_id")
                                      .innerJoin(SQLFrom.table("educationlevelshighschool_t", "hs"), "lev.id=hs.educationlevel_id")
                        )
                        .top(1).column("lev.id")
                        .where("not(" + levWhere + ")")
        );

        // Меняем ссылку на какой-нибудь другой НПм
        final BatchUpdater updater = new BatchUpdater("update educationlevelshighschool_t set educationlevel_id=? where id=?", DBType.LONG, DBType.LONG);
        for (Object[] row : tool.executeQuery(MigrationUtils.processor(3), "select hs.id, lev.eduprogramsubject_id, hs.assignedqualification_id " + fromSql))
        {
            final Long hsId = (Long) row[0];
            final Long eduSubjId = (Long) row[1];
            final Long qId = (Long) row[2];

            long levId = 0;
            if (eduSubjId != null) {
                // Если есть новое направление, ищем какой-нибудь корректный НПм на такое же направление
                levId = tool.getNumericResult(subjSQL, eduSubjId);
            }
            if (levId == 0 && qId != null) {
                // Если есть квалификация, имем какой-нибудь корректный НПм, созданный на НПв с такой же квалификацией
                levId = tool.getNumericResult(qualSQL, qId);
            }
            if (levId == 0) {
                // Иначе выбираем что-то..
                levId = tool.getNumericResult(randomSQL);
            }
            if (levId == 0) {
                throw new IllegalStateException("wtf-1");
            }
            updater.addBatch(levId, hsId);
        }

        if (count != updater.executeUpdate(tool)) {
            throw new IllegalStateException("wtf-2");
        }

        if (tool.hasResultRows("select hs.id " + fromSql)) {
            throw new IllegalStateException("wtf-3");
        }

        // Еще раз фиксим квалификации на всякий случай
        MS_uni_2x9x2_1to2.autoFix(tool);
    }
}