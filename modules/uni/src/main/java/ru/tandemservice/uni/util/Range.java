package ru.tandemservice.uni.util;

/**
 * @author vdanilov
 */
public class Range<T extends Comparable<T>>
{
    private T min;
    public T getMin() { return this.min; }

    private T max;
    public T getMax() { return this.max; }

    public void add(T el) {
        if (null == el) { return; }
        min = (null == min) ? el : (min.compareTo(el) <= 0 ? min : el);
        max = (null == max) ? el : (max.compareTo(el) >= 0 ? max : el);
    }

}
