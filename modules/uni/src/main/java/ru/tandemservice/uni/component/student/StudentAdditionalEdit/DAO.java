/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentAdditionalEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uni.ui.selectModel.PpsEntryModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setContractStudent(UniDefines.COMPENSATION_TYPE_CONTRACT.equals(model.getStudent().getCompensationType().getCode()));
        model.setExternalOrgUnits(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(ExternalOrgUnit.class, "ou")
                    .where(likeUpper(property("ou", ExternalOrgUnit.title()), value(CoreStringUtils.escapeLike(filter))))
                    .order(property("ou", ExternalOrgUnit.title()));

                int count = builder.createCountStatement(new DQLExecutionContext(DAO.this.getSession())).<Number>uniqueResult().intValue();

                builder.top(UniDao.MAX_ROWS);

                return new ListResult<>(DAO.this.getList(builder), count);
            }
        });
        // подготовим список возможных руководителей ВКР
        model.setPpsListModel(new PpsEntryModel() {{
            setColumnTitles(new String[] {"ФИО", "Подразделение", "Данные преподавателя"});
            setLabelProperties(new String[] {PpsEntry.person().fio().s(), PpsEntry.orgUnit().shortTitle().s(), PpsEntry.titlePostOrTimeWorkerData().s()});
        }});
    }

    @Override
    public void validate(Model model, ErrorCollector errorCollector)
    {
        Student student = model.getStudent();

        if (student.getContractNumber() != null)
        {
            Criteria criteria = getSession().createCriteria(Student.class);
            criteria.add(Restrictions.eq(Student.P_CONTRACT_NUMBER, student.getContractNumber()));
            criteria.add(Restrictions.ne(Student.P_ID, student.getId()));
            criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));
            criteria.setProjection(Projections.rowCount());
            if (((Number) criteria.uniqueResult()).intValue() > 0)
            {
                errorCollector.add("Данный номер договора уже закреплен за другим студентом", "contractNumber");
            }
        }
    }
}
