// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsMergeCatalogItems;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;

import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 24.05.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);

        if (null == model.getDataSource())
        {
            DynamicListDataSource<IEntity> dataSource = UniBaseUtils.createDataSource(component, getDao());
            dataSource.addColumn(new SimpleColumn("Старый элемент справочника", "title").setClickable(false).setOrderable(false));
            BlockColumn<IEntity> newItemColumn = new BlockColumn<IEntity>("newItem", "Новый элемент справочника");
            dataSource.addColumn(newItemColumn.setOrderable(false));
            model.setNewItemColumn(newItemColumn);
            model.setDataSource(dataSource);
        }
    }


    public void onClickApply(IBusinessComponent component)
    {
        final Model model = getModel(component);
        String confirmMessage = getDao().getConfirmMessage(model);
        if (null == component.getClientParameter() && null != confirmMessage)
        {
            ConfirmInfo confirm = new ConfirmInfo(confirmMessage, new ClickButtonAction("submit_desktop", "ok", true));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }
        getDao().update(getModel(component));
        deactivate(component);
    }
}