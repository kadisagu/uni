package ru.tandemservice.uni.util.schema;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.tandemframework.core.meta.AbstractMetaObject;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.meta.entity.IRelationSubject;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class YEDSchemaGenerator implements ApplicationContextAware {
    private static final String HOME = YEDSchemaGenerator.class.getPackage().getName().replace(".", "/");
    private final Logger log = Logger.getLogger(this.getClass());

    @Override
    public void setApplicationContext(final ApplicationContext ctx) throws BeansException {

        // вынес генерацию в отдельный поток, чтобы время не отжирала во время поднятия спрингового контекста
        final Thread t = new Thread("yed-schema-generator-thread") {
            @Override public void run() {
                final File file = new File("schema.graphml");
                YEDSchemaGenerator.this.log.info("Entity schema file: " + file.getAbsolutePath());
                try {
                    final Map<String, Map<String, List<IEntityMeta>>> data = SafeMap.get(key -> SafeMap.get(key1 -> new ArrayList<>()));

                    final Collection<IEntityMeta> entities = EntityRuntime.getInstance().getEntities();
                    for (final IEntityMeta meta: entities) {
                        final AbstractMetaObject metaObject = ((AbstractMetaObject)meta);
                        final AbstractMetaObject root = metaObject.getRootWrapper();
                        data.get(StringUtils.trimToEmpty(metaObject.getModuleName())).get(null == root ? "NONE" : root.getName()).add(meta);
                    }

                    file.createNewFile();
                    final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                    final VelocityContext context = new VelocityContext();
                    context.put("data", data);
                    context.put("gen", YEDSchemaGenerator.this);

                    try {
                        final Template TEMPLATE_SCHEMA = Velocity.getTemplate(YEDSchemaGenerator.HOME + "/schema.graphml.vm");
                        TEMPLATE_SCHEMA.merge(context, writer);
                    } catch (final Throwable e) {
                        throw new RuntimeException(e);
                    }

                    writer.flush();
                    writer.close();
                } catch (final Throwable t) {
                    log.error(t.getMessage(), t);
                }
            }
        };

        t.setPriority(Thread.MIN_PRIORITY);
        t.start();
    }

    public int getHeight(IEntityMeta meta) {
        return 20+14*(9+meta.getDeclaredProperties().size());
    }

    public boolean isLinkProperty(IPropertyMeta meta) {
        return (meta instanceof IRelationSubject);
    }

    public boolean isCalculatedProperty(IPropertyMeta meta)
    {
        return meta instanceof IFieldPropertyMeta && ((IFieldPropertyMeta) meta).isCalculatedField();
    }

    public boolean isRequiredProperty(IPropertyMeta meta)
    {
        return meta instanceof IFieldPropertyMeta && ((IFieldPropertyMeta) meta).isRequired();
    }

}
