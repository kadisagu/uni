/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public abstract class AbstractRelationListDAO<Model extends AbstractRelationListModel> extends UniDao<Model> implements IAbstractRelationListDAO<Model>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(getFirstObjectEntityName(), "o");
        new OrderDescriptionRegistry("o").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), (List)builder.getResultList(getSession()));
    }

    protected abstract String getFirstObjectEntityName();
}
