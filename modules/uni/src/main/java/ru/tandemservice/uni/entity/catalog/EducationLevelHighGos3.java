package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.EducationLevelHighGos3Gen;

/**
 * Направление подготовки (специальность) ВПО (ФГОС)
 */
public class EducationLevelHighGos3 extends EducationLevelHighGos3Gen
{
    public EducationLevelHighGos3()
    {
        setCatalogCode("educationLevelHighGos3");
    }
}