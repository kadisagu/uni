package ru.tandemservice.uni.entity.orgstruct.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь между орг. юнитом и его видом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitToKindRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation";
    public static final String ENTITY_NAME = "orgUnitToKindRelation";
    public static final int VERSION_HASH = 1976983948;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_ORG_UNIT_KIND = "orgUnitKind";
    public static final String P_ALLOW_ADD_STUDENT = "allowAddStudent";
    public static final String P_ALLOW_ADD_GROUP = "allowAddGroup";

    private OrgUnit _orgUnit;     // Подразделение
    private OrgUnitKind _orgUnitKind;     // Вид подразделения
    private boolean _allowAddStudent;     // Набор студентов
    private boolean _allowAddGroup;     // Набор групп

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Вид подразделения. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitKind getOrgUnitKind()
    {
        return _orgUnitKind;
    }

    /**
     * @param orgUnitKind Вид подразделения. Свойство не может быть null.
     */
    public void setOrgUnitKind(OrgUnitKind orgUnitKind)
    {
        dirty(_orgUnitKind, orgUnitKind);
        _orgUnitKind = orgUnitKind;
    }

    /**
     * @return Набор студентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowAddStudent()
    {
        return _allowAddStudent;
    }

    /**
     * @param allowAddStudent Набор студентов. Свойство не может быть null.
     */
    public void setAllowAddStudent(boolean allowAddStudent)
    {
        dirty(_allowAddStudent, allowAddStudent);
        _allowAddStudent = allowAddStudent;
    }

    /**
     * @return Набор групп. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowAddGroup()
    {
        return _allowAddGroup;
    }

    /**
     * @param allowAddGroup Набор групп. Свойство не может быть null.
     */
    public void setAllowAddGroup(boolean allowAddGroup)
    {
        dirty(_allowAddGroup, allowAddGroup);
        _allowAddGroup = allowAddGroup;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitToKindRelationGen)
        {
            setOrgUnit(((OrgUnitToKindRelation)another).getOrgUnit());
            setOrgUnitKind(((OrgUnitToKindRelation)another).getOrgUnitKind());
            setAllowAddStudent(((OrgUnitToKindRelation)another).isAllowAddStudent());
            setAllowAddGroup(((OrgUnitToKindRelation)another).isAllowAddGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitToKindRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitToKindRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitToKindRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "orgUnitKind":
                    return obj.getOrgUnitKind();
                case "allowAddStudent":
                    return obj.isAllowAddStudent();
                case "allowAddGroup":
                    return obj.isAllowAddGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "orgUnitKind":
                    obj.setOrgUnitKind((OrgUnitKind) value);
                    return;
                case "allowAddStudent":
                    obj.setAllowAddStudent((Boolean) value);
                    return;
                case "allowAddGroup":
                    obj.setAllowAddGroup((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "orgUnitKind":
                        return true;
                case "allowAddStudent":
                        return true;
                case "allowAddGroup":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "orgUnitKind":
                    return true;
                case "allowAddStudent":
                    return true;
                case "allowAddGroup":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "orgUnitKind":
                    return OrgUnitKind.class;
                case "allowAddStudent":
                    return Boolean.class;
                case "allowAddGroup":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitToKindRelation> _dslPath = new Path<OrgUnitToKindRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitToKindRelation");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Вид подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#getOrgUnitKind()
     */
    public static OrgUnitKind.Path<OrgUnitKind> orgUnitKind()
    {
        return _dslPath.orgUnitKind();
    }

    /**
     * @return Набор студентов. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#isAllowAddStudent()
     */
    public static PropertyPath<Boolean> allowAddStudent()
    {
        return _dslPath.allowAddStudent();
    }

    /**
     * @return Набор групп. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#isAllowAddGroup()
     */
    public static PropertyPath<Boolean> allowAddGroup()
    {
        return _dslPath.allowAddGroup();
    }

    public static class Path<E extends OrgUnitToKindRelation> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private OrgUnitKind.Path<OrgUnitKind> _orgUnitKind;
        private PropertyPath<Boolean> _allowAddStudent;
        private PropertyPath<Boolean> _allowAddGroup;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Вид подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#getOrgUnitKind()
     */
        public OrgUnitKind.Path<OrgUnitKind> orgUnitKind()
        {
            if(_orgUnitKind == null )
                _orgUnitKind = new OrgUnitKind.Path<OrgUnitKind>(L_ORG_UNIT_KIND, this);
            return _orgUnitKind;
        }

    /**
     * @return Набор студентов. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#isAllowAddStudent()
     */
        public PropertyPath<Boolean> allowAddStudent()
        {
            if(_allowAddStudent == null )
                _allowAddStudent = new PropertyPath<Boolean>(OrgUnitToKindRelationGen.P_ALLOW_ADD_STUDENT, this);
            return _allowAddStudent;
        }

    /**
     * @return Набор групп. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation#isAllowAddGroup()
     */
        public PropertyPath<Boolean> allowAddGroup()
        {
            if(_allowAddGroup == null )
                _allowAddGroup = new PropertyPath<Boolean>(OrgUnitToKindRelationGen.P_ALLOW_ADD_GROUP, this);
            return _allowAddGroup;
        }

        public Class getEntityClass()
        {
            return OrgUnitToKindRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitToKindRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
