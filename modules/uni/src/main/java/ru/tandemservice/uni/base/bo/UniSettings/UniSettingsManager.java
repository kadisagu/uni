/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.uni.base.bo.UniSettings.logic.AccreditationSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniSettings.logic.UniAccreditationDao;
import ru.tandemservice.uni.base.bo.UniSettings.logic.IUniAccreditationDao;

/**
 * @author Ekaterina Zvereva
 * @since 29.11.2016
 */
@Configuration
public class UniSettingsManager extends BusinessObjectManager
{
    public static UniSettingsManager instance()
    {
        return instance(UniSettingsManager.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> accreditationSearchListDSHandler() {
        return new AccreditationSearchListDSHandler(getName());
    }

    @Bean
    public IUniAccreditationDao accreditationDao()
    {
        return new UniAccreditationDao();
    }
}