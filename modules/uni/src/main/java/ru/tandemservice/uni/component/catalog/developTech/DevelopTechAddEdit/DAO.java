/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developTech.DevelopTechAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;

/**
 * @author AutoGenerator
 * Created on 16.06.2008
 */
public class DAO extends DefaultCatalogAddEditDAO<DevelopTech, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }

    @Override
    public void update(Model model)
    {
        DevelopTech developTech = model.getCatalogItem();
        EduProgramTrait programTrait = developTech.getProgramTrait();
        if (programTrait != null)
        {
            programTrait.setTitle(developTech.getTitle());
            programTrait.setShortTitle(developTech.getShortTitle());
        }

        super.update(model);
    }
}
