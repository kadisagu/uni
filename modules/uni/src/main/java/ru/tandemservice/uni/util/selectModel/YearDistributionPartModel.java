package ru.tandemservice.uni.util.selectModel;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistribution;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 * @author iolshvang
 * @since 2/24/11
 */

public abstract class YearDistributionPartModel implements IHSelectModel, ISingleSelectModel
{
    public abstract Map<EducationYear, Set<YearDistributionPart>> getValueMap();

    @Override
    public int getLevel(Object value) {
        if (null == value) return 0;
        if (value instanceof YearDistribution) return 0;
        return 1;
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean isSelectable(Object value) {
        return null != value && !(value instanceof YearDistribution);
    }

    @Override
    public Object getRawValue(Object value) {
        return value;
    }

    @Override
    public Object getPackedValue(Object o) {
        return o;
    }

    @Override
    public Object getValue(Object pK) {
        return UniDaoFacade.getCoreDao().get((Long)pK);
    }

    @Override
    public ListResult findValues(String filter)
    {
        ArrayList<IHierarchyItem> itemList = new ArrayList<IHierarchyItem>();
        itemList.addAll(UniDaoFacade.getCoreDao().getCatalogItemList(YearDistribution.class));
        itemList.addAll(UniDaoFacade.getCoreDao().getCatalogItemList(YearDistributionPart.class));
        return new ListResult<IHierarchyItem>(
                new PlaneTree(itemList).getFlatTreeObjectsHierarchicalSorted("code")
        );
    }

    @Override
    public int getColumnCount() {
        return 1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String[] getColumnTitles() {
        return null;
    }

    @Override
    public String getLabelFor(Object value, int columnIndex) {
        if (null == value) return "";
        if (! (value instanceof ICatalogItem)) return "";
        return ((ICatalogItem)value).getTitle();
    }

    @Override
    public String getFullLabel(Object value) {
         return getLabelFor(value, 0);
    }

    @Override
    public ISelectValueStyle getValueStyle(Object value) {
        return null;
    }
}
