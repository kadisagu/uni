package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Разбиение учебного года"
 * Имя сущности : yearDistribution
 * Файл data.xml : catalogs.data.xml
 */
public interface YearDistributionCodes
{
    /** Константа кода (code) элемента : Не разбивается (title) */
    String YEAR_DISTRIBUTION_1 = "1";
    /** Константа кода (code) элемента : На семестры (title) */
    String YEAR_DISTRIBUTION_2 = "2";
    /** Константа кода (code) элемента : На триместры (title) */
    String YEAR_DISTRIBUTION_3 = "3";
    /** Константа кода (code) элемента : На четверти (title) */
    String YEAR_DISTRIBUTION_4 = "4";

    Set<String> CODES = ImmutableSet.of(YEAR_DISTRIBUTION_1, YEAR_DISTRIBUTION_2, YEAR_DISTRIBUTION_3, YEAR_DISTRIBUTION_4);
}
