/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.CustomStateList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.logic.customState.StudentCustomStateSearchListDSHandler;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.ui.ColorRowCustomizer;

/**
 * @author nvankov
 * @since 3/25/13
 */

@Configuration
public class UniStudentCustomStateList extends BusinessComponentManager
{
    public static final String CUSTOM_STATE_LIST_DS = "customStateListDS";


    @Bean
    public ColumnListExtPoint customStateListCL()
    {
        return columnListExtPointBuilder(CUSTOM_STATE_LIST_DS)
                .addColumn(textColumn("title", StudentCustomState.customState().title()).required(true).order())
                .addColumn(dateColumn("beginDate", StudentCustomState.beginDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().width("250px"))
                .addColumn(dateColumn("endDate", StudentCustomState.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().width("250px"))
                .addColumn(textColumn("desc", StudentCustomState.description()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("uniStudentCustomStateListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.customStatusDeleteAlert").permissionKey("uniStudentCustomStateListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CUSTOM_STATE_LIST_DS, customStateListCL(), customStateListDSHandler())
                                       .rowCustomizer(new ColorRowCustomizer<>().coloredProperty(StudentCustomState.customState().s())))
                .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler customStateListDSHandler()
    {
        return new StudentCustomStateSearchListDSHandler(getName());
    }
}
