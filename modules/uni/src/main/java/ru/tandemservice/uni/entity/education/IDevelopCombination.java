package ru.tandemservice.uni.entity.education;

import java.util.Comparator;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;

/**
 * @author vdanilov
 */
public interface IDevelopCombination extends ITitled {

    public static final Comparator<IDevelopCombination> COMPARATOR = new Comparator<IDevelopCombination>() {

        public int compare(ICatalogItem o1, ICatalogItem o2) {
            return o1.getCode().compareTo(o2.getCode());
        }

        // может вызывать мучительную смерть от null-pointer-exception, и это правильно
        @Override public int compare(IDevelopCombination o1, IDevelopCombination o2) {
            int f = compare(o1.getDevelopForm(), o2.getDevelopForm());
            if (0 != f) { return f; }

            int c = compare(o1.getDevelopCondition(), o2.getDevelopCondition());
            if (0 != c) { return c; }

            int t = compare(o1.getDevelopTech(), o2.getDevelopTech());
            if (0 != t) { return t; }

            int g = compare(o1.getDevelopGrid(), o2.getDevelopGrid());
            if (0 != g) { return g; }

            return 0;
        }
    };


    public DevelopForm getDevelopForm();
    public DevelopCondition getDevelopCondition();
    public DevelopTech getDevelopTech();
    public DevelopGrid getDevelopGrid();
}
