/* $Id$ */
package ru.tandemservice.uni.component.documents.d4.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

/**
 * @author Alexey Lopatin
 * @since 22.08.2013
 */
public interface IDAO extends IDocumentAddBaseDAO<Model>
{
}
