// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.GroupStartEduYearEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author oleyba
 * @since 03.12.2010
 */
@Input(keys = "groupId", bindings = "group.id")
public class Model
{
    private Group group = new Group();
    private ISelectModel yearModel;

    public Group getGroup()
    {
        return group;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }

    public ISelectModel getYearModel()
    {
        return yearModel;
    }

    public void setYearModel(ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }
}
