/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.dao;

import org.tandemframework.core.settings.IDataSettingsManager;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uni.dao.group.IGroupDAO;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * Фасад всех дао из модуля UNI
 *
 * @author vip_delete
 */
public class UniDaoFacade
{
    private static SpringBeanCache<IDataSettingsManager> dataSettingsManager = new SpringBeanCache<IDataSettingsManager>("dataSettingsManager");

    /**
     * use {@see org.tandemframework.core.settings.DataSettingsFacade}
     *
     * todo: remove it in 2.2.7
     */
    @Deprecated
    public static IDataSettingsManager getSettingsManager()
    {
        return dataSettingsManager.get();
    }

    public static IUniBaseDao getCoreDao()
    {
        return IUniBaseDao.instance.get();
    }

    public static IOrgstructDAO getOrgstructDao()
    {
        return IOrgstructDAO.instance.get();
    }

    public static IEmployeeDAO getEmployeeDao()
    {
        return IEmployeeDAO.instance.get();
    }

    public static IEducationLevelDAO getEducationLevelDao()
    {
        return IEducationLevelDAO.instance.get();
    }

    public static INumberQueueDAO getNumberDao()
    {
        return INumberQueueDAO.instance.get();
    }

    public static IGroupDAO getGroupDao()
    {
        return IGroupDAO.instance.get();
    }

    public static IUniExportDAO getUniExportDao()
    {
        return IUniExportDAO.instance.get();
    }
}