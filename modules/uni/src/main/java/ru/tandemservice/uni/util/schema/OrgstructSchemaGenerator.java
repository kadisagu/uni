package ru.tandemservice.uni.util.schema;

import java.io.BufferedWriter;
import java.io.CharArrayWriter;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;

public class OrgstructSchemaGenerator extends UniBaseDao implements IOrgstructSchemaGenerator {
    private static final String HOME = OrgstructSchemaGenerator.class.getPackage().getName().replace(".", "/");


    public String getFullTitle(OrgUnit orgUnit) {
        return orgUnit.getTitle() + "\n(" + orgUnit.getOrgUnitType().getTitle() + ")";
    }

    public static int e(int x, int d) {
        return (int)(63.0*Math.exp(-Math.pow(x/5.0-d, 2.0)));
    }

    public String getColor(OrgUnit orgUnit) {
        int p = orgUnit.getOrgUnitType().getPriority();
        long r = (int) (192+e(p, 0)), g = (int) (192+e(p,1)), b = (int) (192+e(p,2));
        return "#"+Long.toHexString(b+(g+r*256)*256);
    }


    @Override
    public String getOrgstructSchema() {

        try {
            final Map<Long, OrgUnit> orgUnitMap = new LinkedHashMap<Long, OrgUnit>();
            for (OrgUnit orgUnit: getList(OrgUnit.class, OrgUnit.P_ARCHIVAL, Boolean.FALSE)) {
                orgUnitMap.put(orgUnit.getId(), orgUnit);
            }

            final Writer charArrayWriter = new CharArrayWriter();
            final Writer bufferedWriter = new BufferedWriter(charArrayWriter);

            final VelocityContext context = new VelocityContext();
            context.put("dao", this);
            context.put("orgUnitMap", orgUnitMap);

            try {
                final Template TEMPLATE_SCHEMA = Velocity.getTemplate(OrgstructSchemaGenerator.HOME + "/orgstruct.graphml.vm");
                TEMPLATE_SCHEMA.merge(context, bufferedWriter);
            } catch (final Throwable e) {
                throw new RuntimeException(e);
            }

            bufferedWriter.flush();
            bufferedWriter.close();
            charArrayWriter.flush();
            charArrayWriter.close();
            return charArrayWriter.toString();

        } catch (final Throwable t) {
            throw new RuntimeException(t);
        }
    }

}
