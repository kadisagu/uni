/* $Id$ */
package ru.tandemservice.uni.catalog.bo.DevelopPeriod.ui.AddEdit;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IdentifiableWrapper;

/**
 * @author Alexey Lopatin
 * @since 10.09.2015
 */
@Configuration
public class DevelopPeriodAddEdit extends BusinessComponentManager
{

    public static final String OPTIONS_DS = "optionsDS";

    public static final IdentifiableWrapper USE_YEAR_AND_MONTH_OPTION = new IdentifiableWrapper(0L, "в годах и месяцах");
    public static final IdentifiableWrapper USE_HOURS_OPTION = new IdentifiableWrapper(1L, "в часах");

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(OPTIONS_DS, optionsDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> optionsDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Lists.newArrayList(USE_YEAR_AND_MONTH_OPTION, USE_HOURS_OPTION));
    }

}