/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.customState;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 3/25/13
 */
public class StudentCustomStateSearchListDSHandler extends DefaultSearchDataSourceHandler
{
    public StudentCustomStateSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long studentCustomStateCI = context.get("studentCustomStateCI");
        Long studentId = context.get("studentId");
        Date activeDateFrom = context.get("activeDateFrom");
        Date activeDateTo = context.get("activeDateTo");

        if (activeDateFrom != null && activeDateTo != null && activeDateFrom.after(activeDateTo))
            return new DSOutput(input);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "st")
                .column("st")
                .fetchPath(DQLJoinType.inner, StudentCustomState.customState().fromAlias("st"), "customState")
                .where(eq(property("st", StudentCustomState.student().id()), value(studentId)));

        if (null != studentCustomStateCI)
            builder.where(eq(property("st", StudentCustomState.customState()), value(studentCustomStateCI)));

        FilterUtils.applyIntersectPeriodFilter(builder, "st", StudentCustomState.P_BEGIN_DATE, StudentCustomState.P_END_DATE, activeDateFrom, activeDateTo);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
