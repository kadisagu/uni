/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.AbstractGroupList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author vip_delete
 */
public abstract class Controller extends AbstractBusinessController<IDAO<Model>, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());

        getDao().prepare(model);
        model.setArchival(isGroupArchivalList());

        if (model.getOrgUnit() != null)
        {
            String permissionPostFix = model.getOrgUnit().getOrgUnitType().getCode() + "_group";
            model.setAddKey("orgUnit_add_" + permissionPostFix);
            model.setDelKey("orgUnit_delete_" + permissionPostFix);
            model.setEditKey("orgUnit_edit_" + permissionPostFix);
            model.setPrintStudentsGroupsListKey("orgUnit_printStudentsGroupsList_" + permissionPostFix);

        } else
        {
            model.setEditKey("editGroupFromList");
            model.setDelKey("deleteGroupFromList");
            model.setPrintStudentsGroupsListKey("printStudentsGroupsList");
        }

        prepareDataSource(component);

        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey()));
    }

    protected abstract String getSettingsKey();

    public void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        model.setDataSource(createNewDataSource(component));
    }

    protected DynamicListDataSource<Group> createNewDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        DynamicListDataSource<Group> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        }, 30);

        dataSource.addColumn(IndicatorColumn.createIconColumn("group", "Группа"));

        dataSource.addColumn(buildGroupTitleClickableColumn());
        dataSource.addColumn(new SimpleColumn("Курс", Group.COURSE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", Group.FORMATIVE_ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", Group.TERRITORIAL_ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", Group.PRODUCTIVE_ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectWithCodeIndexAndGenTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Ориентация ОП", Group.educationOrgUnit().educationLevelHighSchool().programOrientation().shortTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Присваиваемая квалификация", Group.educationOrgUnit().educationLevelHighSchool().assignedQualification().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", Group.educationOrgUnit().developForm().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", Group.educationOrgUnit().developCondition().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Нормативный срок", Group.educationOrgUnit().developPeriod().title().s()).setOrderable(false).setClickable(false));
        if (isShowActiveStudentColumn())
        {
            dataSource.addColumn(new SimpleColumn("Активных студентов", Group.P_ACTIVE_STUDENT_COUNT).setOrderable(false).setClickable(false));
        }

        if (isListAccessable(component))
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getEditKey()));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить группу «{0}»?", Group.P_TITLE).setPermissionKey(model.getDelKey()));
        }
        return dataSource;
    }

    protected FormatterColumn buildGroupTitleClickableColumn() {
        return new SimpleColumn(getMessage("group.title"), Group.P_TITLE, NoWrapFormatter.INSTANCE);
    }

    protected boolean isShowActiveStudentColumn()
    {
        return true;
    }

    protected boolean isListAccessable(IBusinessComponent component)
    {
        OrgUnit orgUnit = getModel(component).getOrgUnit();
        return ((orgUnit != null) && !orgUnit.isArchival());
    }

    protected boolean isGroupArchivalList()
    {
        return false;
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniComponents.GROUP_EDIT, new ParametersMap()
                .add("groupEditId", component.getListenerParameter())
        ));
    }

    public void onClickAdd(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniComponents.GROUP_ADD, new ParametersMap()
                .add("orgUnitId", getModel(component).getOrgUnitId())
        ));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
        getModel(component).getDataSource().refresh();
    }

    public void onClickPrintStudentsGroupsList(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.STUDENTS_GROUPS_LIST_PRINT, new ParametersMap()
        .add("groupIdsList", getDao().getFinalGroupIdsList(getModel(component)))
        ));
    }
}