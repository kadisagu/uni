/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentData;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 03.02.2011
 */
public class StudentDataBlock
{
    // names
//    public static final String STUDENT_DS = "studentDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";
//    public static final String STUDENT_ARCHIVAL_DS = "studentArchivalDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORG_UNIT_DS = "territorialOrgUnitDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String DEVELOP_PERIOD_AUTO_DS = "developPeriodAutoDS";
    public static final String GROUP_DS = "groupDS";
    public static final String ENTRANCE_YEAR_DS = "entranceYearDS";
    public static final String PRODUCING_ORG_UNIT_DS = "producingOrgUnitDS";

    // datasource parameter names
    public static final String PARAM_COURSE_LIST = "courseList";
    public static final String PARAM_QUALIFICATION_LIST = "qualificationList";
    public static final String PARAM_FORMATIVE_ORG_UNIT_LIST = "formativeOrgUnitList";
    public static final String PARAM_TERRITORIAL_ORG_UNIT_LIST = "territorialOrgUnitList";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, StudentDataParam param)
    {
        String name = dataSource.getName();

        if (EDU_LEVEL_DS.equals(name))
        {
            param.getQualification().putParamIfActive(dataSource, PARAM_QUALIFICATION_LIST);

            param.getFormativeOrgUnit().putParamIfActive(dataSource, PARAM_FORMATIVE_ORG_UNIT_LIST);

            param.getTerritorialOrgUnit().putParamIfActive(dataSource, PARAM_TERRITORIAL_ORG_UNIT_LIST);
        } else if (GROUP_DS.endsWith(name))
        {
            param.getCourse().putParamIfActive(dataSource, PARAM_COURSE_LIST);

            param.getFormativeOrgUnit().putParamIfActive(dataSource, PARAM_FORMATIVE_ORG_UNIT_LIST);

            param.getTerritorialOrgUnit().putParamIfActive(dataSource, PARAM_TERRITORIAL_ORG_UNIT_LIST);
        }
    }

    public static IDefaultComboDataSourceHandler createStudentStatusDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, StudentStatus.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(StudentStatus.usedInSystem().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createCompensationTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, CompensationType.class, CompensationType.shortTitle());
    }

    public static IDefaultComboDataSourceHandler createQualificationDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, Qualifications.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Qualifications.id().fromAlias("e")),
                        new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "hs")
                                .column(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().qualification().id().fromAlias("hs")))
                                .buildQuery()
                ));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createEduLevelDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EducationLevelsHighSchool.class, EducationLevelsHighSchool.displayableTitle())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ou").column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("ou")));

                List<Qualifications> qualificationsList = ep.context.get(PARAM_QUALIFICATION_LIST);

                if (qualificationsList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().fromAlias("ou")), qualificationsList));

                List<OrgUnit> formativeOrgUnitList = ep.context.get(PARAM_FORMATIVE_ORG_UNIT_LIST);

                if (formativeOrgUnitList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), formativeOrgUnitList));

                List<OrgUnit> territorialOrgUnitList = ep.context.get(PARAM_TERRITORIAL_ORG_UNIT_LIST);

                if (territorialOrgUnitList != null)
                    builder.where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), territorialOrgUnitList));

                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias("e")), builder.buildQuery()));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createGroupDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, Group.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                List<Course> courseList = ep.context.get(PARAM_COURSE_LIST);

                if (courseList != null)
                    ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Group.course().fromAlias("e")), courseList));

                List<OrgUnit> formativeOrgUnitList = ep.context.get(PARAM_FORMATIVE_ORG_UNIT_LIST);

                if (formativeOrgUnitList != null)
                    ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("e")), formativeOrgUnitList));

                List<OrgUnit> territorialOrgUnitList = ep.context.get(PARAM_TERRITORIAL_ORG_UNIT_LIST);

                if (territorialOrgUnitList != null)
                    ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("e")), territorialOrgUnitList));
            }
        };
    }

    public static IDefaultComboDataSourceHandler createEntranceYearDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> list = new ArrayList<>();

                int currentYear = UniBaseUtils.getCurrentYear();

                for (int i = currentYear - 9; i <= currentYear; i++)
                    list.add(new DataWrapper((long) i, Integer.toString(i)));

                context.put(UIDefines.COMBO_OBJECT_LIST, list);

                return super.execute(input, context);
            }
        }.filtered(true);
    }
}
