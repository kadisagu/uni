package ru.tandemservice.uni.entity.orgstruct.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Старосты групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupCaptainStudentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent";
    public static final String ENTITY_NAME = "groupCaptainStudent";
    public static final int VERSION_HASH = -422150159;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_STUDENT = "student";

    private Group _group;     // Группа
    private Student _student;     // Староста

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Староста. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Староста. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupCaptainStudentGen)
        {
            setGroup(((GroupCaptainStudent)another).getGroup());
            setStudent(((GroupCaptainStudent)another).getStudent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupCaptainStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupCaptainStudent.class;
        }

        public T newInstance()
        {
            return (T) new GroupCaptainStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return Group.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupCaptainStudent> _dslPath = new Path<GroupCaptainStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupCaptainStudent");
    }
            

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Староста. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends GroupCaptainStudent> extends EntityPath<E>
    {
        private Group.Path<Group> _group;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Староста. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return GroupCaptainStudent.class;
        }

        public String getEntityName()
        {
            return "groupCaptainStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
