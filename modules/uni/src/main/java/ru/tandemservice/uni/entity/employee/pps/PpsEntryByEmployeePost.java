package ru.tandemservice.uni.entity.employee.pps;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.pps.gen.PpsEntryByEmployeePostGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/** @see ru.tandemservice.uni.entity.employee.pps.gen.PpsEntryByEmployeePostGen */
public class PpsEntryByEmployeePost extends PpsEntryByEmployeePostGen
{
    public PpsEntryByEmployeePost() {}

    public PpsEntryByEmployeePost(Person person, OrgUnit orgUnit, PostBoundedWithQGandQL post)
    {
        setPerson(person);
        setOrgUnit(orgUnit);
        setPost(post);
    }

    @Override public String getFullTitle() {
        final StringBuilder str = new StringBuilder("ППС (сотрудник): ");
        str.append(getTitle());
        return str.toString();
    }

    @Override
    public String getContextTitle()
    {
        return "Запись ППС на основе сотрудника на должности: " + getTitleInfo();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048617")
    public String getExtendedTitle()
    {
        return getPerson().getFullFio() + " (" + getTitleInfo() + ")";
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048617")
    public String getTitle()
    {
        if (null == getPerson()) {
            return this.getClass().getSimpleName();
        }
        return getPerson().getFio() + " (" + getPost().getTitle() + ", " + getOrgUnit().getShortTitle() + ")";
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048617")
    public String getShortTitle() {
        return getPerson().getFio();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048617")
    public String getTitleInfo()
    {
        // вообще post и orgUnit ненулябельны, но мало ли =)
        StringBuilder sb = new StringBuilder();
        sb.append(" т.н. ").append(getEmployeeCode());
        if (getPost() != null) sb.append(", ").append(getPost().getTitle());
        if (getOrgUnit() != null) sb.append(", ").append(getOrgUnit().getShortTitle());
        return sb.toString();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=33065812")
    public String getTitlePostOrTimeWorkerData() {
        return getPost().getTitle();
    }

    @Override
    public EducationYear getEduYear()
    {
        return null;
    }

    @Override
    public long getTimeAmountAsLong()
    {
        return 0L;
    }
}