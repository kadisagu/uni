package ru.tandemservice.uni.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uni_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность student

		// создано обязательное свойство developPeriodAuto
		{
			// создать колонку
			tool.createColumn("student_t", new DBColumn("developperiodauto_id", DBType.LONG));

			// задать значение по умолчанию
            final List<Object[]> students = tool.executeQuery(MigrationUtils.processor(2), "select s.id, eou.developperiod_id from student_t s inner join educationorgunit_t eou on s.educationorgunit_id = eou.id");
            final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update student_t set developperiodauto_id = ? where id = ?", DBType.LONG, DBType.LONG);

            for (Object[] row : students)
            {
                Long studentId = (Long) row[0];
                Long developPeriodId = (Long) row[1];
                updater.addBatch(developPeriodId, studentId);
            }
            updater.executeUpdate(tool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("student_t", "developperiodauto_id", false);
		}

		// создано свойство developPeriod
		{
			// создать колонку
			tool.createColumn("student_t", new DBColumn("developperiod_id", DBType.LONG));
		}
    }
}