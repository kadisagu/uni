/* $Id$ */
package ru.tandemservice.uni.component.log.EntityLogViewBase;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.event.IEventType;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.util.Catalog;

/**
 * @author oleyba
 * @since 9/19/11
 */
@State({ @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="entityId") })
public class Model extends org.tandemframework.shared.commonbase.logging.bo.EventLog.ui.View.Model
{
    List<IEntityMeta> entityTypeList;


    public List<IEventType> getEventTypeList()
    {
        return Catalog.getInstance().getAllEventTypes();
    }

    public List<IEntityMeta> getEntityTypeList()
    {
        return entityTypeList;
    }

    public void setEntityTypeList(List<IEntityMeta> entityTypeList)
    {
        this.entityTypeList = entityTypeList;
    }
}
