/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

import java.io.File;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.tandemframework.core.view.formatter.DateFormatter;

/**
 * Do not use it
 *
 * @author vip_delete
 * @since 14.12.2009
 */
@Deprecated
public class OrgUnitComponentGenerator
{
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception
    {
        if (args.length != 4)
        {
            System.out.println("Usage: OrgUnitComponentGenerator <entityName> <titleG> <titleA> <titleM>.\n" +
                    "Example: OrgUnitComponentGenerator branch филиала филиал Филиалы.");
            System.exit(1);
        }

        File file = UniSystemUtils.getModulesDir();

        String entityName = args[0];                         // branch
        String prefix = StringUtils.capitalize(entityName);  // Branch
        String titleG = args[1];                             // филиала (род. падеж)
        String titleA = args[2];                             // филиал (вин. падеж)
        String titleM = args[3];                             // Филиалы (мн. число. с большой буквы)

        String destPath = file.getAbsolutePath() + "/uni/src/main/java/ru/tandemservice/uni/component/orgunit";
        String srcPath = file.getAbsolutePath() + "/uni/src/main/java/ru/tandemservice/uni/util/system/templates/orgUnit";

        UniSystemUtils.initVelocity();

        for (String folder : new String[]{"Add", "Edit", "List", "Pub"})
        {
            String destComponentPath = destPath + "/" + entityName + "/" + prefix + folder;
            String srcComponentPath = srcPath + "/" + folder;

            FileUtils.forceMkdir(new File(destComponentPath));


            VelocityContext context = new VelocityContext();
            context.put("entityName", entityName);
            context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
            context.put("simpleName", prefix);
            context.put("title_G", titleG);
            context.put("title_A", titleA);
            context.put("title_M", titleM);

            UniSystemUtils.createFileByTemplate(destComponentPath + "/Model.java", srcComponentPath + "/Model.java.vm", context, false);
            UniSystemUtils.createFileByTemplate(destComponentPath + "/IDAO.java", srcComponentPath + "/IDAO.java.vm", context, false);
            UniSystemUtils.createFileByTemplate(destComponentPath + "/DAO.java", srcComponentPath + "/DAO.java.vm", context, false);
            UniSystemUtils.createFileByTemplate(destComponentPath + "/Controller.java", srcComponentPath + "/Controller.java.vm", context, false);

            for (File f : (Collection<File>) FileUtils.listFiles(new File(srcComponentPath), new String[]{"html.vm"}, false))
            {
                String name = f.getName();
                String fileName = prefix + StringUtils.capitalize(name.substring(0, name.indexOf(".html.vm"))) + ".html";

                UniSystemUtils.createFileByTemplate(destComponentPath + "/" + fileName, srcComponentPath + "/" + name, context, false);
            }
        }
    }
}
