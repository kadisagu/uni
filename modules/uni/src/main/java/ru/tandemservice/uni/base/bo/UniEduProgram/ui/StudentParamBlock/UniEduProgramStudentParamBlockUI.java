/* $Id:$ */
/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.StudentParamBlock;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsAdd.UniEduProgramEduHsAdd;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEdit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsEdit.UniEduProgramEduHsEditUI;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsPub.UniEduProgramEduHsPub;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlPub.UniEduProgramEduLvlPub;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit.UniEduProgramEduOuAddEdit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelMiddle;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/14/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class UniEduProgramStudentParamBlockUI extends UIPresenter
{
    private EntityHolder<EduProgram> holder = new EntityHolder<>();

    private List<EducationOrgUnit> eduOuList = Collections.emptyList();
    private EducationOrgUnit eduOu; // FIXME это видимо не нужно совсем

    private DynamicListDataSource<EducationLevelsHighSchool> eduHsDataSource;


    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setEduOuList(UniEduProgramManager.instance().dao().getEduOuList(getHolder().getValue()));
        createEduHsDataSource();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("eduProgram", getHolder().getValue());
    }

    public void onClickAddEduOu() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class).parameter("programId", getHolder().getId()).activate();
    }

    public void onClickEditEduOu() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteEduOu() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickChangeUsedEduOu() {
        UniEduProgramManager.instance().dao().doChangeUsedEduOu(getListenerParameterAsLong());
    }

    public void onClickAddEduHs() {
        _uiActivation.asRegionDialog(UniEduProgramEduHsAdd.class).parameter("programId", getHolder().getId()).activate();
    }

    public void onClickEditEduHs() {
        _uiActivation.asRegionDialog(UniEduProgramEduHsEdit.class).
            parameter(UniEduProgramEduHsEditUI.PUBLISHER_ID, getListenerParameterAsLong())
            .parameter(UniEduProgramEduHsEditUI.BIND_DISABLED_OU_ORIENTATION, true)
            .activate();
    }

    public void onClickDeleteEduHs() {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public void onClickAllowEduHs()
    {
        _uiSupport.setRefreshScheduled(true);
        UniEduProgramManager.instance().dao().doChangeAllowStudents(getListenerParameterAsLong());
    }

    private void createEduHsDataSource()
    {
        DynamicListDataSource<EducationLevelsHighSchool> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {

            EduProgram p = getHolder().getValue();

            if (p instanceof EduProgramHigherProf)
            {
                final EduProgramHigherProf prof = (EduProgramHigherProf) p;
                final EducationLevels educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4High(prof.getProgramSubject(), prof.getProgramSpecialization());

                List<EducationLevelsHighSchool> eduHsList = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, "e")
                        .where(eq(property("e", EducationLevelsHighSchool.educationLevel()), value(educationLevel)))
                        .where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(p.getOwnerOrgUnit().getOrgUnit())))
                        .where(eq(property("e", EducationLevelsHighSchool.assignedQualification()), value(prof.getProgramQualification())))
                        .where(eq(property("e", EducationLevelsHighSchool.programOrientation()), value(prof.getProgramOrientation())))
                        .order(property("e", EducationLevelsHighSchool.displayableTitle().s()))
                        .createStatement(_uiSupport.getSession()).list();

                UniBaseUtils.createPage(getEduHsDataSource(), eduHsList);
            }
            else if (p instanceof EduProgramSecondaryProf)
            {
                final EduProgramSecondaryProf prof = (EduProgramSecondaryProf) p;
                final EducationLevelMiddle educationLevel = UniEduProgramManager.instance().syncDao().getEduLvl4Middle(prof.getProgramSubject(), prof.isInDepthStudy());

                List<EducationLevelsHighSchool> eduHsList = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, "e")
                        .where(eq(property("e", EducationLevelsHighSchool.educationLevel()), value(educationLevel)))
                        .where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(p.getOwnerOrgUnit().getOrgUnit())))
                        .where(eq(property("e", EducationLevelsHighSchool.assignedQualification()), value(prof.getProgramQualification())))
                        .order(property("e", EducationLevelsHighSchool.displayableTitle().s()))
                        .createStatement(_uiSupport.getSession()).list();
                UniBaseUtils.createPage(getEduHsDataSource(), eduHsList);
            }
            else
            {
                throw new IllegalStateException();
            }
        });

        dataSource.addColumn(new PublisherLinkColumn("Название", EducationLevelsHighSchool.P_FULL_TITLE).setResolver(new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) { return entity.getId(); }
            @Override public String getComponentName(final IEntity entity) { return UniEduProgramEduHsPub.class.getSimpleName(); }
        }));


        dataSource.addColumn(new SimpleColumn("Сокр. название", EducationLevelsHighSchool.P_SHORT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("НПм", EducationLevelsHighSchool.educationLevel().fullTitle().s()).setResolver(new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) { return ((EducationLevelsHighSchool)entity).getEducationLevel().getId(); }
            @Override public String getComponentName(final IEntity entity) { return UniEduProgramEduLvlPub.class.getSimpleName(); }
        }).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Направление ОП", EducationLevelsHighSchool.educationLevel().programSubjectWithCodeIndexAndGenTitle().s())
                                     .setResolver(new SimplePublisherLinkResolver(EducationLevelsHighSchool.educationLevel().eduProgramSubject().id()))
                                     .setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", EducationLevelsHighSchool.educationLevel().programSpecializationTitle().s()).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Квалификация", EducationLevelsHighSchool.assignedQualification().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", EducationLevelsHighSchool.orgUnit().fullTitle().s()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ToggleColumn("Обучение студентов осуществляется", EducationLevelsHighSchool.allowStudents().s()).setListener("onClickAllowEduHs").setDisabledProperty(EducationLevelsHighSchool.P_ALLOW_STUDENTS_DISABLED).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEduHs"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEduHs", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE));

        setEduHsDataSource(dataSource);
    }

    // getters and setters


    public EntityHolder<EduProgram> getHolder()
    {
        return holder;
    }

    public void setHolder(EntityHolder<EduProgram> holder)
    {
        this.holder = holder;
    }

    public Object getEduOu()
    {
        return eduOu;
    }

    public void setEduOu(EducationOrgUnit eduOu)
    {
        this.eduOu = eduOu;
    }

    public List<EducationOrgUnit> getEduOuList()
    {
        return eduOuList;
    }

    public void setEduOuList(List<EducationOrgUnit> eduOuList)
    {
        this.eduOuList = eduOuList;
    }

    public DynamicListDataSource<EducationLevelsHighSchool> getEduHsDataSource()
    {
        return eduHsDataSource;
    }

    public void setEduHsDataSource(DynamicListDataSource<EducationLevelsHighSchool> eduHsDataSource)
    {
        this.eduHsDataSource = eduHsDataSource;
    }
}