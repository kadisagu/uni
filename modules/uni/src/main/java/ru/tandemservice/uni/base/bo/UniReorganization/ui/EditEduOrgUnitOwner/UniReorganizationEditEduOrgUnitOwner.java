/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.ui.EditEduOrgUnitOwner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniReorganization.UniReorganizationManager;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
@Configuration
public class UniReorganizationEditEduOrgUnitOwner extends BusinessComponentManager {

    public static final String OWNER_DS = "ownerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(OWNER_DS, getName(), UniReorganizationManager.formativeOrgUnitReorganizationCustomizedOrgUnitHandler(getName())
                        .customize((alias, dql, context, filter) ->
                                dql.where(eq(property(alias, OrgUnit.archival()), value(Boolean.FALSE)))))
                )
                .create();
    }

}
