/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.customState;

import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.entity.education.StudentCustomState;

import java.util.List;

/**
 * @author nvankov
 * @since 3/25/13
 */
public interface IStudentCustomStateDAO extends ISharedBaseDao
{
    void saveOrUpdateStudentCustomState(StudentCustomState studentCustomState);

    List<StudentCustomState> getActiveStatesList(Long studentId);

    /**
     * Проверка наличия доп. статуса у студента. Ищется такой же доп. статус с пересекающимся периодом или без периода.
     *
     * @param studentCustomState проверяемый доп.статус
     * @return true - если такой доп. статус есть. Иначе - false.
     */
    boolean hasMatchStudentCustomState(StudentCustomState studentCustomState);
}
