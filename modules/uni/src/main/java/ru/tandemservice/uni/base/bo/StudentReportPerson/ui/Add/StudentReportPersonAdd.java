/**
 *$Id$
 */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.StudentReportPerson.StudentReportPersonManager;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentAdditionalData.StudentAdditionalDataBlock;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.block.studentData.StudentDataBlock;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

/**
 * @author Alexander Zhebko
 * @since 26.03.2014
 */
@Configuration
public class StudentReportPersonAdd extends BusinessComponentManager
{
    // sub tab panels
    public static final String STUDENT_TAB_PANEL = "studentTabPanel";

    // second level tabs
    public static final String STUDENT_COMMON_TAB = "studentCommonTab";
    public static final String STUDENT_PRINT_TAB = "studentPrintTab";

    // tab block lists
    public static final String STUDENT_COMMON_BLOCK_LIST = "studentCommonBlockList";

    // print tab block lists
    public static final String STUDENT_SCHEET_BLOCK_LIST = "studentScheetBlockList";

    // block names
    public static final String STUDENT_DATA = "studentData";
    public static final String STUDENT_ADDITIONAL_DATA = "studentAdditionalData";

    // print tab block names
    public static final String STUDENT_SCHEET_COMMON_BLOCK_LIST = "studentScheetCommonBlockList";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(StudentDataBlock.STUDENT_STATUS_DS, studentStatusComboDSHandler()))
                .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
                .addDataSource(selectDS(StudentDataBlock.COMPENSATION_TYPE_DS).handler(compensationTypeComboDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .addDataSource(selectDS(StudentDataBlock.QUALIFICATION_DS, qualificationComboDSHandler()))
                .addDataSource(selectDS(StudentDataBlock.FORMATIVE_ORG_UNIT_DS).handler(UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(StudentDataBlock.TERRITORIAL_ORG_UNIT_DS).handler(UniOrgUnitManager.instance().territorialOrgUnitComboDSHandler()).addColumn(OrgUnit.P_TERRITORIAL_FULL_TITLE))
                .addDataSource(selectDS(StudentDataBlock.EDU_LEVEL_DS).handler(eduLevelComboDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(StudentDataBlock.DEVELOP_PERIOD_AUTO_DS, getName(), DevelopPeriod.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(StudentDataBlock.GROUP_DS, groupComboDSHandler()))
                .addDataSource(selectDS(StudentDataBlock.ENTRANCE_YEAR_DS, entranceYearComboDSHandler()))
                .addDataSource(selectDS(StudentDataBlock.PRODUCING_ORG_UNIT_DS, UniOrgUnitManager.instance().producingOrgUnitComboDSHandler()))
                .addDataSource(selectDS(StudentAdditionalDataBlock.STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
                .addDataSource(selectDS(StudentAdditionalDataBlock.TARGET_ADMISSION_ORG_UNIT_DS, targetAdmissionOrgUnitComboDSHandler()))
                .addDataSource(StudentReportPersonManager.instance().advisorsDS())
                .create();
    }

    @Bean
    public TabPanelExtPoint studentTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(STUDENT_TAB_PANEL)
                .addTab(htmlTab(STUDENT_COMMON_TAB, "StudentCommonTab"))
                .addTab(htmlTab(STUDENT_PRINT_TAB, "StudentPrintTab"))
                .create();
    }

    @Bean
    public BlockListExtPoint studentCommonBlockListExtPoint()
    {
        return blockListExtPointBuilder(STUDENT_COMMON_BLOCK_LIST)
                .addBlock(htmlBlock(STUDENT_DATA, "block/studentData/StudentData"))
                .addBlock(htmlBlock(STUDENT_ADDITIONAL_DATA, "block/studentAdditionalData/StudentAdditionalData"))
                .create();
    }

    @Bean
    public BlockListExtPoint studentScheetBlockListExtPoint()
    {
        return blockListExtPointBuilder(STUDENT_SCHEET_BLOCK_LIST)
                .addBlock(htmlBlock(STUDENT_SCHEET_COMMON_BLOCK_LIST, "print/student/Template"))
                .create();
    }

    // StudentDataBlock
    @Bean
    public IDefaultComboDataSourceHandler studentStatusComboDSHandler()
    {
        return StudentDataBlock.createStudentStatusDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeComboDSHandler()
    {
        return StudentDataBlock.createCompensationTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationComboDSHandler()
    {
        return StudentDataBlock.createQualificationDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLevelComboDSHandler()
    {
        return StudentDataBlock.createEduLevelDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler groupComboDSHandler()
    {
        return StudentDataBlock.createGroupDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler entranceYearComboDSHandler()
    {
        return StudentDataBlock.createEntranceYearDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler targetAdmissionOrgUnitComboDSHandler()
    {
        return StudentAdditionalDataBlock.createTargetAdmissionOrgUnitDS(getName());
    }

    // StudentAdditionalDataBlock

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryDSHandler()
    {
        return StudentAdditionalDataBlock.createStudentCategoryDS(getName());
    }
}