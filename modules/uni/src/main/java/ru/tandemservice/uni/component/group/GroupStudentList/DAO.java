/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.GroupStudentList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author vip_delete
 * @since 12.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Group group = get(Group.class, model.getGroup().getId());
        model.setGroup(group);
    }

    protected Comparator<Student> getStudentComparator() {
        return Comparator.comparingInt((Student s) -> s.getStatus().getPriority())
                .thenComparing(Student.FULL_FIO_AND_ID_COMPARATOR);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Student> getStudentData(Model model)
    {
        Criteria criteria = getCriteria(model);
        List<Student> studentList = criteria.list();
        Collections.sort(studentList, getStudentComparator());
        return studentList;
    }

    protected Criteria getCriteria(Model model)
    {
        Criteria criteria = getSession().createCriteria(Student.class);
        criteria.createAlias(Student.L_STATUS, "status");
        criteria.createAlias(Student.L_PERSON, "person");
        criteria.createAlias("person." + Person.L_IDENTITY_CARD, "identityCard");
        criteria.add(Restrictions.eq(Student.L_GROUP, model.getGroup()));
        criteria.add(Restrictions.eq("status." + StudentStatus.P_ACTIVE, true));
        criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));
        return criteria;
    }

}
