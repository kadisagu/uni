/* $Id$ */
package ru.tandemservice.uni.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 31.10.2013
 */
public class EducationOrgUnitUtil
{
    private static final String EOU_ALIAS = "eou";
    private static final String HS_ALIAS = "hs";

    public static EducationLevels getParentLevel(EducationLevelsHighSchool hs)
    {
        return getParentLevel(hs.getEducationLevel());
    }

    public static EducationLevels getParentLevel(EducationLevels levels)
    {
        StructureEducationLevels levelType = levels.getLevelType();
        if (levelType != null && (levelType.isSpecialization() || levelType.isProfile()) && levels.getParentLevel() != null)
            return levels.getParentLevel();
        return levels;
    }

    private static boolean _isNeedRequest(IEducationLevelModel model, OrgUnit orgUnit, boolean dependFromEduLevel)
    {
        return orgUnit != null ||
                (model.getFormativeOrgUnit() != null &&
                        (!dependFromEduLevel || model.getEducationLevelsHighSchool() != null ||
                                (model instanceof IExtEducationLevelModel && ((IExtEducationLevelModel) model).getParentEduLevel() != null)));
    }

    private static DQLSelectBuilder _createBuilder(IEducationLevelModel model, OrgUnit orgUnit, boolean dependFromEduLevel)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, EOU_ALIAS)
                .predicate(DQLPredicateType.distinct)
                .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias(EOU_ALIAS), HS_ALIAS);

        UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(builder, EOU_ALIAS);

        if (orgUnit != null)
        {
            builder.where(or(
                    eq(property(EOU_ALIAS, EducationOrgUnit.formativeOrgUnit()), value(orgUnit.getId())),
                    eq(property(EOU_ALIAS, EducationOrgUnit.territorialOrgUnit()), value(orgUnit.getId()))
            ));
        }

        if (model.getFormativeOrgUnit() != null)
            builder.where(eq(property(EOU_ALIAS, EducationOrgUnit.formativeOrgUnit()), value(model.getFormativeOrgUnit().getId())));

        if (model.getTerritorialOrgUnit() != null)
            builder.where(eq(property(EOU_ALIAS, EducationOrgUnit.territorialOrgUnit()), value(model.getTerritorialOrgUnit().getId())));

        if (dependFromEduLevel && model.getEducationLevelsHighSchool() != null)
            builder.where(eq(property(HS_ALIAS + ".id"), value(model.getEducationLevelsHighSchool().getId())));

        if (model instanceof IExtEducationLevelModel)
        {
            EducationLevels parentLevel = ((IExtEducationLevelModel) model).getParentEduLevel();
            if (parentLevel != null)
            {
                builder.where(or(
                        eq(property(HS_ALIAS, EducationLevelsHighSchool.educationLevel()), value(parentLevel.getId())),
                        eq(property(HS_ALIAS, EducationLevelsHighSchool.educationLevel().parentLevel()), value(parentLevel.getId()))
                ));
            }
        }

        return builder;
    }

    public static ISelectModel getDevelopFormSelectModel(final IEducationLevelModel model, boolean dependFromEduLevel)
    {
        return getDevelopFormSelectModel(model, null, dependFromEduLevel);
    }

    public static ISelectModel getDevelopFormSelectModel(final IEducationLevelModel model)
    {
        return getDevelopFormSelectModel(model, null, true);
    }

    public static ISelectModel getDevelopFormSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit)
    {
        return getDevelopFormSelectModel(model, orgUnit, true);
    }

    public static ISelectModel getDevelopFormSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit, final boolean dependFromEduLevel)
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (!_isNeedRequest(model, orgUnit, dependFromEduLevel))
                    return new SimpleListResultBuilder<>(Collections.emptyList());

                DQLSelectBuilder builder = _createBuilder(model, orgUnit, dependFromEduLevel)
                        .column(property(EOU_ALIAS, EducationOrgUnit.developForm()))
                        .order(property(EOU_ALIAS, EducationOrgUnit.developForm().code()));

                if (o != null)
                    builder.where(eq(property(EOU_ALIAS, EducationOrgUnit.developForm()), commonValue(o, PropertyType.LONG)));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(EOU_ALIAS, EducationOrgUnit.developForm().title()), value(CoreStringUtils.escapeLike(filter, true))));

                return new DQLListResultBuilder(builder, 50);
            }
        };
    }

    public static ISelectModel getDevelopConditionSelectModel(final IEducationLevelModel model)
    {
        return getDevelopConditionSelectModel(model, null, true);
    }

    public static ISelectModel getDevelopConditionSelectModel(final IEducationLevelModel model, boolean dependFromEduLevel)
    {
        return getDevelopConditionSelectModel(model, null, dependFromEduLevel);
    }

    public static ISelectModel getDevelopConditionSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit)
    {
        return getDevelopConditionSelectModel(model, orgUnit, true);
    }

    public static ISelectModel getDevelopConditionSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit, final boolean dependFromEduLevel)
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (!_isNeedRequest(model, orgUnit, dependFromEduLevel) || model.getDevelopForm() == null)
                    return new SimpleListResultBuilder<>(Collections.emptyList());

                DQLSelectBuilder builder = _createBuilder(model, orgUnit, dependFromEduLevel)
                        .column(property(EOU_ALIAS, EducationOrgUnit.developCondition()))
                        .where(eq(property(EOU_ALIAS, EducationOrgUnit.developForm()), value(model.getDevelopForm().getId())))
                        .order(property(EOU_ALIAS, EducationOrgUnit.developCondition().code()));

                if (o != null)
                    builder.where(eq(property(EOU_ALIAS, EducationOrgUnit.developCondition()), commonValue(o, PropertyType.LONG)));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(EOU_ALIAS, EducationOrgUnit.developCondition().title()), value(CoreStringUtils.escapeLike(filter, true))));

                return new DQLListResultBuilder(builder, 50);
            }
        };
    }

    public static ISelectModel getDevelopTechSelectModel(final IEducationLevelModel model)
    {
        return getDevelopTechSelectModel(model, null, true);
    }

    public static ISelectModel getDevelopTechSelectModel(final IEducationLevelModel model, boolean dependFromEduLevel)
    {
        return getDevelopTechSelectModel(model, null, dependFromEduLevel);
    }

    public static ISelectModel getDevelopTechSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit)
    {
        return getDevelopTechSelectModel(model, orgUnit, true);
    }

    public static ISelectModel getDevelopTechSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit, final boolean dependFromEduLevel)
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (!_isNeedRequest(model, orgUnit, dependFromEduLevel) || model.getDevelopForm() == null || model.getDevelopCondition() == null)
                    return new SimpleListResultBuilder<>(Collections.emptyList());

                DQLSelectBuilder builder = _createBuilder(model, orgUnit, dependFromEduLevel)
                        .column(property(EOU_ALIAS, EducationOrgUnit.developTech()))
                        .where(eq(property(EOU_ALIAS, EducationOrgUnit.developForm()), value(model.getDevelopForm().getId())))
                        .where(eq(property(EOU_ALIAS, EducationOrgUnit.developCondition()), value(model.getDevelopCondition().getId())))
                        .order(property(EOU_ALIAS, EducationOrgUnit.developTech().code()));

                if (o != null)
                    builder.where(eq(property(EOU_ALIAS, EducationOrgUnit.developTech().id()), commonValue(o, PropertyType.LONG)));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(EOU_ALIAS, EducationOrgUnit.developTech().title()), value(CoreStringUtils.escapeLike(filter, true))));

                return new DQLListResultBuilder(builder, 50);
            }
        };
    }

    public static ISelectModel getDevelopPeriodSelectModel(final IEducationLevelModel model)
    {
        return getDevelopPeriodSelectModel(model, null, true);
    }

    public static ISelectModel getDevelopPeriodSelectModel(final IEducationLevelModel model, boolean dependFromEduLevel)
    {
        return getDevelopPeriodSelectModel(model, null, dependFromEduLevel);
    }

    public static ISelectModel getDevelopPeriodSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit)
    {
        return getDevelopPeriodSelectModel(model, orgUnit, true);
    }

    public static ISelectModel getDevelopPeriodSelectModel(final IEducationLevelModel model, final OrgUnit orgUnit, final boolean dependFromEduLevel)
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (!_isNeedRequest(model, orgUnit, dependFromEduLevel) || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null)
                    return new SimpleListResultBuilder<>(Collections.emptyList());

                DQLSelectBuilder builder = _createBuilder(model, orgUnit, dependFromEduLevel)
                        .column(property(EOU_ALIAS, EducationOrgUnit.developPeriod()))
                        .where(eq(property(EOU_ALIAS, EducationOrgUnit.developForm()), value(model.getDevelopForm().getId())))
                        .where(eq(property(EOU_ALIAS, EducationOrgUnit.developCondition()), value(model.getDevelopCondition().getId())))
                        .where(eq(property(EOU_ALIAS, EducationOrgUnit.developTech()), value(model.getDevelopTech().getId())))
                        .order(property(EOU_ALIAS, EducationOrgUnit.developPeriod().code()));

                if (o != null)
                    builder.where(eq(property(EOU_ALIAS, EducationOrgUnit.developPeriod()), commonValue(o, PropertyType.LONG)));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property(EOU_ALIAS, EducationOrgUnit.developPeriod().title()), value(CoreStringUtils.escapeLike(filter, true))));

                return new DQLListResultBuilder(builder, 50);
            }
        };
    }
}