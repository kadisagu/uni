/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui.formatters;

import org.tandemframework.core.view.formatter.IRawFormatter;

/**
 * @author vip_delete
 * @since 02.03.2010
 */
public class CourseRomanFormatter implements IRawFormatter<Integer>
{
    private String space;
    public static final CourseRomanFormatter INSTANCE = new CourseRomanFormatter("&nbsp;") {
        @Override public String format(final Integer source) {
            // todo разобраться, зачем тут сделано NPE для source = null и зачем игнорируется отрицательный курс
            if (source <= 0) { return ""; }
            return super.format(source);
        }
    };
    public static final CourseRomanFormatter INSTANCE_PLAIN = new CourseRomanFormatter("");

    public CourseRomanFormatter(String space) {
        this.space = space;
    }

    @Override
    public String format(Integer source)
    {
        if (source == null) return "";
        switch (source)
        {
            case 1:
                return "I " + space + "курс";
            case 2:
                return "II " + space + "курс";
            case 3:
                return "III " + space + "курс";
            case 4:
                return "IV " + space + "курс";
            case 5:
                return "V " + space + "курс";
            case 6:
                return "VI " + space + "курс";
            case 7:
                return "VII " + space + "курс";
            case 8:
                return "VIII " + space + "курс";
            case 9:
                return "IX " + space + "курс";
            case 10:
                return "X " + space + "курс";
        }
        return source.toString();
    }
}
