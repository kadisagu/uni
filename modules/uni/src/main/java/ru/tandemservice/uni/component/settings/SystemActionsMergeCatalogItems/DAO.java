// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.SystemActionsMergeCatalogItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;

import org.tandemframework.shared.person.catalog.entity.SportRank;
import org.tandemframework.shared.person.catalog.entity.SportType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author oleyba
 * @since 24.05.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        List<SportType> sportTypes = new ArrayList<>();
        for (SportType type : getList(SportType.class, SportType.title().s()))
            if (type.getCode().length() == 3)
                sportTypes.add(type);
        model.setSportTypeModel(new LazySimpleSelectModel<>(sportTypes));
        MQBuilder sportRanks = new MQBuilder(SportRank.ENTITY_CLASS, "s").add(MQExpression.isNotNull("s", SportRank.shortTitle().s()));
        model.setSportRankModel(new LazySimpleSelectModel<>(sportRanks.<SportRank>getResultList(getSession())));
        MQBuilder addressCountries = new MQBuilder(AddressCountry.ENTITY_CLASS, "c").add(MQExpression.isNotNull("c", AddressCountry.digitalCode().s())).add(MQExpression.notEq("c", AddressCountry.code().s(), IKladrDefines.RUSSIA_COUNTRY_CODE));
        model.setAddressCountryModel(new LazySimpleSelectModel<>(addressCountries.<AddressCountry>getResultList(getSession())));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<IEntity> items = new ArrayList<>();
        if (model.getCatalog() != null && model.getCatalog().getId().equals(Model.SPORT_TYPE_CODE))
            for (SportType type : getList(SportType.class, SportType.title().s()))
                if (type.getCode().length() != 3)
                    items.add(type);
        if (model.getCatalog() != null && model.getCatalog().getId().equals(Model.SPORT_RANK_CODE))
        {
            MQBuilder sportRanks = new MQBuilder(SportRank.ENTITY_CLASS, "s").add(MQExpression.isNull("s", SportRank.shortTitle().s()));
            items.addAll(sportRanks.<SportRank>getResultList(getSession()));
        }
        if (model.getCatalog() != null && model.getCatalog().getId().equals(Model.ADDRESS_COUNTRY_CODE))
        {
            MQBuilder addressCountries = new MQBuilder(AddressCountry.ENTITY_CLASS, "c").add(MQExpression.isNull("c", AddressCountry.digitalCode().s()));
            items.addAll(addressCountries.<AddressCountry>getResultList(getSession()));
        }
        DynamicListDataSource<IEntity> dataSource = model.getDataSource();
        dataSource.setTotalSize(items.size());
        dataSource.setCountRow(items.size());
        UniBaseUtils.createPage(dataSource, items);
    }

    @Override
    public String getConfirmMessage(final Model model)
    {
        Map<String, List<String>> replaceMap = new HashMap<>();

        Map<Long, IEntity> valueMap = model.getNewItemColumn().getValueMap();
        for (IEntity item : model.getDataSource().getEntityList())
        {
            ITitled newItem = (ITitled) valueMap.get(item.getId());
            if (newItem != null)
            {
                List<String> oldItems = replaceMap.get(newItem.getTitle());
                if (null == oldItems)
                    replaceMap.put(newItem.getTitle(), oldItems = new ArrayList<>());
                oldItems.add(((ITitled) item).getTitle());
            }
        }
        StringBuilder error = new StringBuilder("Для нескольких старых элементов справочника выбран один и тот же новый элемент. Произвести замену?");
        //StringBuilder error = new StringBuilder("Для нескольких старых элементов справочника выбран один и тот же новый элемент:<br/>");
        boolean merge = false;
        for (Map.Entry<String, List<String>> entry: replaceMap.entrySet())
            if (entry.getValue().size() > 1)
            {
                //error.append("Для ").append(StringUtils.join(entry.getValue(), ", ")).append(" - ").append(entry.getKey()).append("<br/>");
                merge = true;
            }
        //error.append("Произвести замену?");
        if (merge)
            return error.toString();
        return null;
    }

    @Override
    public void update(Model model)
    {
        Class itemClass = null;
        if (model.getCatalog().getId().equals(Model.SPORT_TYPE_CODE))
            itemClass = SportType.class;
        if (model.getCatalog().getId().equals(Model.SPORT_RANK_CODE))
            itemClass = SportRank.class;
        if (model.getCatalog().getId().equals(Model.ADDRESS_COUNTRY_CODE))
            itemClass = AddressCountry.class;
        if (null == itemClass)
            return;
        Map<Long, IEntity> valueMap = model.getNewItemColumn().getValueMap();
        for (IEntity item : model.getDataSource().getEntityList())
            replace(item, valueMap.get(item.getId()), itemClass);
    }

    private void replace(IEntity oldItem, IEntity newItem, Class itemClass)
    {
        if (null == newItem) return;

        if (AddressCountry.class.equals(itemClass))
            for (String table : new String[] {"address_t", "addressItem_t", "addressRegionType_t"} )
            {
                Query query = getSession().createSQLQuery("update " + table + " set country_id = " + newItem.getId() + " where country_id = " + oldItem.getId());
                query.executeUpdate();
            }

        IEntityMeta meta = EntityRuntime.getMeta(itemClass);
        for (IRelationMeta rel : meta.getIncomingRelations())
        {
            String className = rel.getForeignEntity().getClassName();
            String propertyName = rel.getForeignPropertyName();

            MQBuilder builder = new MQBuilder(className, "r");
            builder.add(MQExpression.eq("r", propertyName, oldItem));
            List<IEntity> list = builder.getResultList(getSession());

            for (IEntity entity : list)
            {
                entity.setProperty(propertyName, newItem);
                getSession().update(entity);
            }
        }
        getSession().delete(oldItem);
    }
}
