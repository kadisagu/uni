// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelAdditional.EducationLevelAdditionalPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.EducationLevelAdditional;

/**
 * @author oleyba
 * @since 11.05.2010
 */
public class Controller extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.Controller<EducationLevelAdditional, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EducationLevelAdditional> dataSource = new DynamicListDataSource<EducationLevelAdditional>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelAdditional.P_FULL_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EducationLevelAdditional.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", EducationLevelAdditional.P_QUALIFICATION_TITLE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}
