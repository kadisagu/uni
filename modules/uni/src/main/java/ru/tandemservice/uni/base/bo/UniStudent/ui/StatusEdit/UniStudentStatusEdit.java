/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.StatusEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author Alexey Lopatin
 * @since 15.04.2015
 */
@Configuration
public class UniStudentStatusEdit extends BusinessComponentManager
{
    public static final String STUDENT_STATUS_DS = "studentStatusDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(STUDENT_STATUS_DS, studentStatusDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler studentStatusDS()
    {
        return new EntityComboDataSourceHandler(getName(), StudentStatus.class)
                .where(StudentStatus.usedInSystem(), Boolean.TRUE)
                .filter(StudentStatus.title())
                .order(StudentStatus.priority());
    }
}
