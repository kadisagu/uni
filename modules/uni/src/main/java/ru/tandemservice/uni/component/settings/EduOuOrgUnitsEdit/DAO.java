package ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;

/**
 * @author oleyba
 * @since 03.12.2010
 */
public class DAO  extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model) {
        List<OrgUnit> orgUnits = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou")
        /*.add(MQExpression.notEq("ou", OrgUnit.orgUnitType().code().s(), OrgUnitDefines.ACADEMY))*/ // должна быть академия, иначе это не согласуется с работой демона
        .add(MQExpression.notEq("ou", OrgUnit.archival().s(), Boolean.TRUE))
        .addOrder("ou", OrgUnit.P_TITLE)
        .<OrgUnit>getResultList(getSession());

        model.setOuModel(new LazySimpleSelectModel<OrgUnit>(orgUnits, "fullTitle").setSearchFromStart(false).setSearchProperty("fullTitle"));
        if (model.getIds() != null && model.getIds().size() == 1)
        {
            EducationOrgUnit eduOu = getNotNull(EducationOrgUnit.class, model.getIds().get(0));
            model.setOperationOrgUnit(eduOu.getOperationOrgUnit());
            model.setGroupOrgUnit(eduOu.getGroupOrgUnit());
        }
    }

    @Override
    public void update(Model model)
    {
        switch (model.getDisplayMode()) {
            case OPERATION:
                for (Long id : model.getIds())
                {
                    EducationOrgUnit eduOu = getNotNull(EducationOrgUnit.class, id);
                    eduOu.setOperationOrgUnit(model.getOperationOrgUnit());
                    update(eduOu);
                }
                break;
            case GROUP:
                for (Long id : model.getIds())
                {
                    EducationOrgUnit eduOu = getNotNull(EducationOrgUnit.class, id);
                    eduOu.setGroupOrgUnit(model.getGroupOrgUnit());
                    update(eduOu);
                }
                break;
            case BOTH:
                for (Long id : model.getIds())
                {
                    EducationOrgUnit eduOu = getNotNull(EducationOrgUnit.class, id);
                    eduOu.setOperationOrgUnit(model.getOperationOrgUnit());
                    eduOu.setGroupOrgUnit(model.getGroupOrgUnit());
                    update(eduOu);
                }
                break;
        }

    }
}
