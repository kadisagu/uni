/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author vip_delete
 * @since 16.10.2008
 */
public abstract class AbstractRelationAddDAO<Model extends AbstractRelationAddModel> extends UniDao<Model> implements IAbstractRelationAddDAO<Model>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(Model model)
    {
        model.setFirst(get(model.getFirstId()));

        model.setHierarchical(isHierarchical());
        if (model.isHierarchical())
        {
            List list = getList(getRelationSecondEntityClass(), getRelationSecondEntityClassSortProperty());
            List notLeafList = new ArrayList();
            for (Iterator iter = list.iterator(); iter.hasNext();)
            {
                IHierarchyItem item = (IHierarchyItem) iter.next();
                if (item.getHierarhyParent() != null)
                    notLeafList.add(item.getHierarhyParent());
            }
            Criteria c = getSession().createCriteria(getRelationEntityClass());
            c.add(Restrictions.eq(IEntityRelation.L_FIRST, model.getFirst()));
            c.setProjection(Projections.property(IEntityRelation.L_SECOND));
            List usedList = c.list();
            list.removeAll(notLeafList);
            list.removeAll(usedList);
            model.setSecondHierarchyList(HierarchyUtil.listHierarchyNodesWithParents(list, false));
        } else
        {
            String hql = "from " + getRelationSecondEntityClass().getName() + " b where b not in (select " + IEntityRelation.L_SECOND + " from " + getRelationEntityClass().getName() + " where " + IEntityRelation.L_FIRST + "=:first) order by b.title";
            List list = getSession().createQuery(hql).setParameter("first", model.getFirst()).list();
            model.setSecondList(list);
        }
    }

    protected boolean isHierarchical()
    {
        return IHierarchyItem.class.isAssignableFrom(getRelationSecondEntityClass());
    }

    @SuppressWarnings({"unchecked"})
    protected abstract Class getRelationSecondEntityClass();

    protected abstract String getRelationSecondEntityClassSortProperty();

    @SuppressWarnings({"unchecked"})
    protected abstract Class getRelationEntityClass();

    @Override
    @SuppressWarnings({"unchecked"})
    public void update(Model model)
    {
        Number number = (Number) getSession().createCriteria(getRelationEntityClass())
        .add(Restrictions.eq(IEntityRelation.L_FIRST, model.getFirst()))
        .add(Restrictions.eq(IEntityRelation.L_SECOND, model.getSecond()))
        .setProjection(Projections.rowCount()).uniqueResult();
        int count = number == null ? 0 : number.intValue();
        if (count == 0)
        {
            try
            {
                IEntityRelation rel = (IEntityRelation) getRelationEntityClass().newInstance();
                rel.setFirst(model.getFirst());
                rel.setSecond(model.getSecond());
                save(rel);
            } catch (Exception e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        } else
            throw new ApplicationException("Такая связь уже существует.");
    }
}
