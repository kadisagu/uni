/* $Id$ */
package ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitSetting;

import java.util.Arrays;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.util.EducationOrgUnitSetting;

/**
 * @author Vasily Zhukov
 * @since 02.06.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        DataWrapper yes = TwinComboDataSourceHandler.getYesOption("Да");
        DataWrapper no = TwinComboDataSourceHandler.getNoOption("Нет");
        model.setYesNoList(TwinComboDataSourceHandler.getDefaultOptionList());

        EducationOrgUnitSetting ouSetting = (EducationOrgUnitSetting) getSession().createCriteria(EducationOrgUnitSetting.class).uniqueResult();
        model.setUseShortTitle(ouSetting != null && ouSetting.isUseShortTitle() ? yes : no);
        model.setUseInternalCode(ouSetting != null && ouSetting.isUseInternalCode() ? yes : no);
    }

    @Override
    public void update(Model model)
    {
        EducationOrgUnitSetting ouSetting = (EducationOrgUnitSetting) getSession().createCriteria(EducationOrgUnitSetting.class).uniqueResult();
        if (ouSetting == null)
            ouSetting = new EducationOrgUnitSetting();
        ouSetting.setUseShortTitle(TwinComboDataSourceHandler.getSelectedValueNotNull(model.getUseShortTitle()));
        ouSetting.setUseInternalCode(TwinComboDataSourceHandler.getSelectedValueNotNull(model.getUseInternalCode()));
        getSession().saveOrUpdate(ouSetting);
    }
}
