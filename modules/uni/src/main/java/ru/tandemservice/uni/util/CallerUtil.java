package ru.tandemservice.uni.util;

/**
 * @author vdanilov
 * TODO: description
 */
public class CallerUtil {
    public static interface Action {
        void execute();
    }

    private static class Record {
        public Record(final Record parent, final Object context) {
            this.parent = parent;
            this.context = context;
        }
        final Record parent;
        final Object context;
    }

    private static final ThreadLocal<Record> threadStack = new ThreadLocal<Record>();

    @SuppressWarnings("unchecked")
    public static <T> T context() {
        final Record record = CallerUtil.threadStack.get();
        return (null == record ? null : (T)record.context);
    }

    public static <T> T call(final Action action, final T context) {
        final Record record = new Record(CallerUtil.threadStack.get(), context);
        CallerUtil.threadStack.set(record);
        try {
            action.execute();
        } finally {
            CallerUtil.threadStack.set(record.parent);
        }
        return context;
    }


}
