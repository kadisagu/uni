/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developForm.DevelopFormPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;

/**
 * @author AutoGenerator
 * Created on 16.06.2008
 */
public class Controller extends DefaultCatalogPubController<DevelopForm, Model, IDAO>
{
    @Override
    @SuppressWarnings("unchecked")
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<DevelopForm> dataSource = new DynamicListDataSource<DevelopForm>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", DevelopForm.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокр. название", DevelopForm.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сист. название", DevelopForm.P_DEFAULT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Префикс группы", DevelopForm.P_GROUP).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Используется", DevelopForm.P_ENABLED).setListener("onClickChangeCatalogItemActivity"));
        dataSource.addColumn(new PublisherLinkColumn("Разрешено создание ОП", EduProgramForm.P_TITLE, DevelopForm.L_PROGRAM_FORM)
            .setResolver(new SimplePublisherLinkResolver(IEntity.P_ID).setComponentName(DefaultCatalogItemPubModel.class.getPackage().getName())));

        if (Debug.isDisplay()) {
            dataSource.addColumn(new SimpleColumn("Системный код", "code").setOrderable(false).setWidth(1).setClickable(false));
        }

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));

        return dataSource;
    }

    public void onClickChangeCatalogItemActivity(IBusinessComponent component)
    {
        CatalogManager.instance().modifyDao().doChangeCatalogItemActivity(component.<Long>getListenerParameter());
    }
}
