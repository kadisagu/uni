/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.GroupAdd;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolSelectModel;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 24.12.2007
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setStartEducationYearList(new EducationYearModel());

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, model.getPrincipalContext()));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        model.setEducationLevelsHighSchoolModel(new EducationLevelsHighSchoolSelectModel(model)
        {
            @Override
            protected DQLSelectBuilder createEducationLevelsHighSchoolDQL(String alias)
            {
                return super.createEducationLevelsHighSchoolDQL(alias)
                        .where(eq(property(alias, EducationLevelsHighSchool.educationLevel().levelType().allowGroups()), value(true)));
            }
        });

        model.setDevelopFormModel(EducationOrgUnitUtil.getDevelopFormSelectModel(model));
        model.setDevelopConditionModel(EducationOrgUnitUtil.getDevelopConditionSelectModel(model));
        model.setDevelopTechModel(EducationOrgUnitUtil.getDevelopTechSelectModel(model));
        model.setDevelopPeriodModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model));

        model.setTitleAlgorithmNotExists(get(GroupTitleAlgorithm.class, GroupTitleAlgorithm.P_CURRENT, Boolean.TRUE) == null);
    }

    @Override
    public void update(Model model)
    {
        Group group = model.getGroup();
        group.setCreationDate(new Date());

        EducationOrgUnit educationOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);
        if (educationOrgUnit == null)
            throw new ApplicationException("Подходящее направление подготовки (специальность) подразделения не найдено.");

        group.setEducationOrgUnit(educationOrgUnit);

        group.setParent((OrgUnit) get(model.getParentId()));

        getSession().save(group);
    }
}
