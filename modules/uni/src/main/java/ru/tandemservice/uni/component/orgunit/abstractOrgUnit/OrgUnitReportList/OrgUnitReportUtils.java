package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;

/**
 * @author vdanilov
 */
@Deprecated
public class OrgUnitReportUtils {

    public static final String REPORT_TAB_TITLE = "Вкладка «Отчеты»";
    public static final String REPORTS_TAB_PERMISSION_GROUP = "ReportsTabPermissionGroup";

    /**
     * 
     * @param config
     * @param code
     * @return
     */
    public static PermissionGroupMeta createOrgUnitReportTabPermissionGroup(SecurityConfigMeta config, String code) {
        return PermissionMetaUtil.createPermissionGroup(config, code + REPORTS_TAB_PERMISSION_GROUP, REPORT_TAB_TITLE);
    }

    /**
     * 
     * @param code
     * @return
     */
    public static PermissionGroupMeta getRuntimeOrgUnitReportTabPermissionGroup(String code) {
        return SecurityRuntime.getInstance().getPermissionGroupMap().get(code + OrgUnitReportUtils.REPORTS_TAB_PERMISSION_GROUP);
    }



}
