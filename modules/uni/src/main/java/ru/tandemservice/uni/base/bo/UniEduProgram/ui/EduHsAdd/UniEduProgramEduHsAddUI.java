package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduHsAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduProgramSubjectSelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({
    @Bind(key = "programId", binding = "programHolder.id")
})
public class UniEduProgramEduHsAddUI extends UIPresenter
{
    private final EducationLevelsHighSchool eduHs = new EducationLevelsHighSchool();

    private EntityHolder<EduProgram> programHolder = new EntityHolder<>();

    private EduProgramSubjectIndex subjectIndex;
    private EduProgramSubject subject;
    private IdentifiableWrapper<IEntity> specializationType;
    private EduProgramSpecialization specialization;
    public Qualifications middleQualification;
    private EduProgramQualification assignedQual;
    private EduProgramOrientation orientation;


    @Override
    public void onComponentRefresh()
    {
        getProgramHolder().refresh();

        if (getProgramHolder().getValue() instanceof EduProgramProf) {
            EduProgramProf prof = (EduProgramProf) getProgramHolder().getValue();
            setSubjectIndex(prof.getProgramSubject().getSubjectIndex());
            setSubject(prof.getProgramSubject());
            getEduHs().setOrgUnit(prof.getOwnerOrgUnit().getOrgUnit());
            getEduHs().setTitle(prof.getProgramSubject().getTitle());
            setAssignedQual(prof.getProgramQualification());
        }

        if (getProgramHolder().getValue() instanceof EduProgramHigherProf) {
            EduProgramHigherProf prof = (EduProgramHigherProf) getProgramHolder().getValue();
            setSpecializationType(prof.getProgramSpecialization().isRootSpecialization() ? SPEC_TYPE_ROOT_WRAPPER : SPEC_TYPE_CHILD_WRAPPER);
            setSpecialization(prof.getProgramSpecialization());
            setOrientation(prof.getProgramOrientation());
        }

        if (getProgramHolder().getValue() instanceof EduProgramSecondaryProf) {
            EduProgramSecondaryProf secondaryProf = (EduProgramSecondaryProf) getProgramHolder().getValue();
            setMiddleQualification(DataAccessServices.dao().get(Qualifications.class, Qualifications.code(), secondaryProf.isInDepthStudy() ? QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O : QualificationsCodes.BAZOVYY_UROVEN_S_P_O));
        }

        if (getSpecializationType() == null) {
            setSpecializationType(SPEC_TYPE_ROOT_WRAPPER);
        }

        onChangeSubjectIndex();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EduProgramOrientation.DS_ORIENTATION.equals(dataSource.getName())) {
            dataSource.put(EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX, getSubjectIndex());
        }
    }

    public void onChangeSubjectIndex() {
        initMiddleQualifications();
        initOrgUnitListModel();
        initSpecializationTypeList();
    }

    public void onClickApply()
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();

        EducationLevelsHighSchool eduHs = getEduHs();
        eduHs.setCode("eduHs."+getSubject().getCode()+"."+Long.toString(System.currentTimeMillis(),32));
        eduHs.setAssignedQualification(getAssignedQual());
        eduHs.setProgramOrientation(getOrientation());

        if (isProgramHigher()) {
            eduHs.setEducationLevel(UniEduProgramManager.instance().syncDao().createOrGetEduLvl4High(getSubject(), isShowSpecializationSelect() ? getSpecialization() : null));
        }
        else if (isProgramMiddle()) {
            boolean advanced;
            switch(getMiddleQualification().getCode()) {
                case QualificationsCodes.BAZOVYY_UROVEN_S_P_O: advanced = false; break;
                case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O: advanced = true; break;
                default: throw new IllegalStateException();
            }
            eduHs.setEducationLevel(UniEduProgramManager.instance().syncDao().createOrGetEduLvl4Middle(getSubject(), advanced));
        } else {
            throw new IllegalStateException();
        }
        eduHs.setAllowStudents(eduHs.getEducationLevel().getLevelType().isAllowStudents());

        assert errors != null;
        if (eduHs.getCloseDate() != null && eduHs.getOpenDate() == null) {
            errors.add("Дата открытия должна быть задана, если задана дата закрытия.", "openDate", "closeDate");
        }

        if (eduHs.getCloseDate() != null && eduHs.getOpenDate() != null && !eduHs.getCloseDate().after(eduHs.getOpenDate())) {
            errors.add("Дата открытия должна быть раньше даты закрытия.", "openDate", "closeDate");
        }

        UniEduProgramManager.instance().syncDao().doSaveEducationLevelsHighSchool(eduHs);
        deactivate();
    }

    // utils

    private boolean isProgramHigher() {
        return null != getSubjectIndex() && getSubjectIndex().getProgramKind().isProgramHigherProf();
    }

    // presenter

    public boolean isEduProgramFixed() {
        return getProgramHolder().getId() != null;
    }

    public boolean isProgramMiddle() {
        return null != getSubjectIndex() && getSubjectIndex().getProgramKind().isProgramSecondaryProf();
    }

    public boolean isShowSpecializationBlock() {
        return isProgramHigher();
    }

    public boolean isShowSpecializationSelect() {
        return isProgramHigher() && (null != getSpecializationType() && (SPEC_TYPE_CHILD_WRAPPER.getId()).equals(getSpecializationType().getId()));
    }

    // getters and setters
    public EducationLevelsHighSchool getEduHs() { return eduHs; }

    public EntityHolder<EduProgram> getProgramHolder() { return programHolder; }

    public EduProgramSubjectIndex getSubjectIndex() { return this.subjectIndex; }
    public void setSubjectIndex(EduProgramSubjectIndex subjectIndex) { this.subjectIndex = subjectIndex; }
    public ISelectModel getSubjectIndexModel() { return this.subjectIndexModel; }

    public EduProgramSubject getSubject() { return this.subject; }
    public void setSubject(EduProgramSubject subject) { this.subject = subject; }
    public ISelectModel getSubjectModel() { return this.subjectModel; }

    public IdentifiableWrapper<IEntity> getSpecializationType() { return this.specializationType; }
    public void setSpecializationType(IdentifiableWrapper<IEntity> specializationType) { this.specializationType = specializationType; }

    public ISelectModel getSpecializationModel() { return this.specializationModel; }

    public EduProgramSpecialization getSpecialization() { return this.specialization; }
    public void setSpecialization(EduProgramSpecialization specialization) { this.specialization = specialization; }

    public List<Qualifications> getMiddleQualifications() { return this.middleQualifications; }
    public void setMiddleQualifications(List<Qualifications> middleQualifications) { this.middleQualifications = middleQualifications; }

    public Qualifications getMiddleQualification() { return this.middleQualification; }
    public void setMiddleQualification(Qualifications middleQualification) { this.middleQualification = middleQualification; }

    public ISelectModel getAssignedQualModel() { return this.assignedQualModel; }
    public EduProgramQualification getAssignedQual() { return this.assignedQual; }
    public void setAssignedQual(EduProgramQualification assignedQual) { this.assignedQual = assignedQual; }

    public boolean isShowOrientation() {
        return IUniBaseDao.instance.get().existsEntity(EduProgramOrientation.class, EduProgramOrientation.subjectIndex().s(), getSubjectIndex());
    }
    public EduProgramOrientation getOrientation() { return orientation; }
    public void setOrientation(EduProgramOrientation orientation) { this.orientation = orientation; }

    public ISelectModel getOrgUnitListModel() { return this.orgUnitListModel; }

    // select models

    // перечень направлений
    private final ISelectModel subjectIndexModel = new DQLFullCheckSelectModel(EduProgramSubjectIndex.title()) {
        @Override protected DQLSelectBuilder query(String alias, String filter) {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramSubjectIndex.class, alias);
            if (null != filter) {
                dql.where(or(
                    like(EduProgramSubjectIndex.title().fromAlias(alias), filter),
                    like(EduProgramSubjectIndex.shortTitle().fromAlias(alias), filter)
                ));
            }

            // показываем только ВПО и СПО (НПО и ДПО здесь быть не должно - они создаются старой формой)
            dql.where(in(property(EduProgramSubjectIndex.code().fromAlias(alias)), Arrays.asList(
                EduProgramSubjectIndexCodes.TITLE_2005_50,
                EduProgramSubjectIndexCodes.TITLE_2005_62,
                EduProgramSubjectIndexCodes.TITLE_2005_68,
                EduProgramSubjectIndexCodes.TITLE_2005_65,
                EduProgramSubjectIndexCodes.TITLE_2009_50,
                EduProgramSubjectIndexCodes.TITLE_2009_62,
                EduProgramSubjectIndexCodes.TITLE_2009_68,
                EduProgramSubjectIndexCodes.TITLE_2009_65,
                EduProgramSubjectIndexCodes.TITLE_2009_00,
                EduProgramSubjectIndexCodes.TITLE_2013_01,
                EduProgramSubjectIndexCodes.TITLE_2013_02,
                EduProgramSubjectIndexCodes.TITLE_2013_03,
                EduProgramSubjectIndexCodes.TITLE_2013_04,
                EduProgramSubjectIndexCodes.TITLE_2013_05,
                EduProgramSubjectIndexCodes.TITLE_2013_06,
                EduProgramSubjectIndexCodes.TITLE_2013_07,
                EduProgramSubjectIndexCodes.TITLE_2013_08,
                EduProgramSubjectIndexCodes.TITLE_2013_10
            )));


            dql.order(property(EduProgramSubjectIndex.code().fromAlias(alias)));
            return dql;
        }

        @Override protected void sortOptions(List list) {
            Collections.sort(list, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR);
        }
    };

    // направление
    private final ISelectModel subjectModel = new EduProgramSubjectSelectModel() {
        @Override protected DQLSelectBuilder query(String alias, String filter) {
            if (null == getSubjectIndex()) { return null; }

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramSubject.class, alias);
            dql.where(eq(property(EduProgramSubject.subjectIndex().fromAlias(alias)), value(getSubjectIndex())));
            if (null != filter) {
                dql.where(or(
                    like(EduProgramSubject.subjectCode().fromAlias(alias), filter),
                    like(EduProgramSubject.title().fromAlias(alias), filter)
                ));
            }
            dql.order(property(EduProgramSubject.subjectCode().fromAlias(alias)));
            dql.order(property(EduProgramSubject.title().fromAlias(alias)));

            return dql;
        }
    };

    private final List<IdentifiableWrapper<IEntity>> specializationTypeList = new ArrayList<>();
    public List<IdentifiableWrapper<IEntity>> getSpecializationTypeList() {
        return specializationTypeList;
    }

    private static final IdentifiableWrapper<IEntity> SPEC_TYPE_ROOT_WRAPPER = new IdentifiableWrapper<>(1L, "Общая (соответствует направлению подготовки, специальности)");
    private static final IdentifiableWrapper<IEntity> SPEC_TYPE_CHILD_WRAPPER = new IdentifiableWrapper<>(2L, "В рамках направления подготовки, специальности");

    private boolean isSpecTypeRootAllowed() {
        // если ОП уже на профиль, не создаем из нее НПв общей направленности
        if (getProgramHolder().getValue() instanceof EduProgramHigherProf) {
            EduProgramHigherProf prof = (EduProgramHigherProf) getProgramHolder().getValue();
            if (!prof.getProgramSpecialization().isRootSpecialization()) return false;
        }

        return getSubjectIndex() != null;
    }
    private boolean isSpecTypeChildAllowed() {
        // если ОП уже на общую направленность, не создаем из нее НПв профиля
        if (getProgramHolder().getValue() instanceof EduProgramHigherProf) {
            EduProgramHigherProf prof = (EduProgramHigherProf) getProgramHolder().getValue();
            if (prof.getProgramSpecialization().isRootSpecialization()) return false;
        }

        return getSubjectIndex() != null;
    }
    private void initSpecializationTypeList() {
        specializationTypeList.clear();
        if (isSpecTypeRootAllowed()) specializationTypeList.add(SPEC_TYPE_ROOT_WRAPPER);
        if (isSpecTypeChildAllowed()) specializationTypeList.add(SPEC_TYPE_CHILD_WRAPPER);
    }

    // направленность (для ВПО)
    private final ISelectModel specializationModel = new DQLFullCheckSelectModel(EduProgramSpecialization.title()) {
        @Override protected DQLSelectBuilder query(String alias, String filter) {
            if (null == getSubject()) { return null; }
            if (!isProgramHigher()) { return null; }
            if (!isShowSpecializationSelect()) { return null; }

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramSpecializationChild.class, alias);
            dql.where(eq(property(EduProgramSpecialization.programSubject().fromAlias(alias)), value(getSubject())));
            if (getProgramHolder().getValue() instanceof EduProgramHigherProf) {
                EduProgramHigherProf prof = (EduProgramHigherProf) getProgramHolder().getValue();
                dql.where(eq(property(alias, EduProgramSpecialization.id()), value(prof.getProgramSpecialization().getId())));
            }

            CommonBaseFilterUtil.applySimpleLikeFilter(dql, alias, EduProgramSpecialization.title(), filter);
            dql.order(property(EduProgramSpecialization.title().fromAlias(alias)));
            return dql;
        }
    };

    // присваиваемая квалификация
    private final ISelectModel assignedQualModel = new DQLFullCheckSelectModel(EduProgramQualification.title()) {
        @Override protected DQLSelectBuilder query(String alias, String filter) {
            if (null == getSubject()) { return null; }

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramQualification.class, alias);
            dql.where(exists(new DQLSelectBuilder()
                .fromEntity(EduProgramSubjectQualification.class, "q")
                .where(eq(property("q", EduProgramSubjectQualification.programQualification()), property(alias)))
                .where(eq(property("q", EduProgramSubjectQualification.programSubject()), value(getSubject())))
                .buildQuery()));

            if (null != filter) {
                dql.where(or(
                    like(EduProgramQualification.title().fromAlias(alias), filter)
                ));
            }

            dql.order(property(EduProgramQualification.title().fromAlias(alias)));
            return dql;
        }
    };

    private ISelectModel orgUnitListModel;
    private void initOrgUnitListModel()
    {
        orgUnitListModel = new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING) {
            @Override public ListResult<OrgUnit> findValues(String filter) {
                if (getSubject() == null) { return ListResult.getEmpty(); }
                return super.findValues(filter);
            }
            @Override public Object getValue(Object primaryKey) {
                if (getSubject() == null) { return null; }
                return super.getValue(primaryKey);
            }
        };
    }

    private List<Qualifications> middleQualifications = Collections.emptyList();
    private void initMiddleQualifications()
    {
        if (getProgramHolder().getId() != null) {
            if (getProgramHolder().getValue() instanceof EduProgramHigherProf) {
                setMiddleQualifications(new ArrayList<>());
            } else if (getProgramHolder().getValue() instanceof EduProgramSecondaryProf) {
                EduProgramSecondaryProf prof = (EduProgramSecondaryProf) getProgramHolder().getValue();
                setMiddleQualifications(DataAccessServices.dao().getList(
                    Qualifications.class,
                    Qualifications.code(),
                    prof.isInDepthStudy() ? QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O : QualificationsCodes.BAZOVYY_UROVEN_S_P_O,
                        Qualifications.P_TITLE
                ));
            }
        } else {
            setMiddleQualifications(DataAccessServices.dao().getList(
                Qualifications.class,
                Qualifications.code(),
                Arrays.asList(QualificationsCodes.BAZOVYY_UROVEN_S_P_O, QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O),
                Qualifications.P_TITLE
            ));
        }
    }
}
