package ru.tandemservice.uni.entity.employee.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись в реестре ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PpsEntryGen extends PersonRole
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.employee.pps.PpsEntry";
    public static final String ENTITY_NAME = "ppsEntry";
    public static final int VERSION_HASH = -2041202034;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_EXTENDED_TITLE = "extendedTitle";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE_FIO_INFO_ORG_UNIT = "titleFioInfoOrgUnit";
    public static final String P_TITLE_FIO_INFO_ORG_UNIT_DEGREES_STATUSES = "titleFioInfoOrgUnitDegreesStatuses";
    public static final String P_TITLE_INFO = "titleInfo";
    public static final String P_TITLE_POST_OR_TIME_WORKER_DATA = "titlePostOrTimeWorkerData";

    private OrgUnit _orgUnit;     // Подразделение
    private Date _creationDate;     // Дата создания
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Дата создания.
     */
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PpsEntryGen)
        {
            setOrgUnit(((PpsEntry)another).getOrgUnit());
            setCreationDate(((PpsEntry)another).getCreationDate());
            setRemovalDate(((PpsEntry)another).getRemovalDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PpsEntryGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PpsEntry.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("PpsEntry is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "creationDate":
                    return obj.getCreationDate();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "creationDate":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "creationDate":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "creationDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PpsEntry> _dslPath = new Path<PpsEntry>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PpsEntry");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getExtendedTitle()
     */
    public static SupportedPropertyPath<String> extendedTitle()
    {
        return _dslPath.extendedTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitleFioInfoOrgUnit()
     */
    public static SupportedPropertyPath<String> titleFioInfoOrgUnit()
    {
        return _dslPath.titleFioInfoOrgUnit();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitleFioInfoOrgUnitDegreesStatuses()
     */
    public static SupportedPropertyPath<String> titleFioInfoOrgUnitDegreesStatuses()
    {
        return _dslPath.titleFioInfoOrgUnitDegreesStatuses();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitleInfo()
     */
    public static SupportedPropertyPath<String> titleInfo()
    {
        return _dslPath.titleInfo();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitlePostOrTimeWorkerData()
     */
    public static SupportedPropertyPath<String> titlePostOrTimeWorkerData()
    {
        return _dslPath.titlePostOrTimeWorkerData();
    }

    public static class Path<E extends PpsEntry> extends PersonRole.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Date> _removalDate;
        private SupportedPropertyPath<String> _extendedTitle;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _titleFioInfoOrgUnit;
        private SupportedPropertyPath<String> _titleFioInfoOrgUnitDegreesStatuses;
        private SupportedPropertyPath<String> _titleInfo;
        private SupportedPropertyPath<String> _titlePostOrTimeWorkerData;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(PpsEntryGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(PpsEntryGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getExtendedTitle()
     */
        public SupportedPropertyPath<String> extendedTitle()
        {
            if(_extendedTitle == null )
                _extendedTitle = new SupportedPropertyPath<String>(PpsEntryGen.P_EXTENDED_TITLE, this);
            return _extendedTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(PpsEntryGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitleFioInfoOrgUnit()
     */
        public SupportedPropertyPath<String> titleFioInfoOrgUnit()
        {
            if(_titleFioInfoOrgUnit == null )
                _titleFioInfoOrgUnit = new SupportedPropertyPath<String>(PpsEntryGen.P_TITLE_FIO_INFO_ORG_UNIT, this);
            return _titleFioInfoOrgUnit;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitleFioInfoOrgUnitDegreesStatuses()
     */
        public SupportedPropertyPath<String> titleFioInfoOrgUnitDegreesStatuses()
        {
            if(_titleFioInfoOrgUnitDegreesStatuses == null )
                _titleFioInfoOrgUnitDegreesStatuses = new SupportedPropertyPath<String>(PpsEntryGen.P_TITLE_FIO_INFO_ORG_UNIT_DEGREES_STATUSES, this);
            return _titleFioInfoOrgUnitDegreesStatuses;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitleInfo()
     */
        public SupportedPropertyPath<String> titleInfo()
        {
            if(_titleInfo == null )
                _titleInfo = new SupportedPropertyPath<String>(PpsEntryGen.P_TITLE_INFO, this);
            return _titleInfo;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.employee.pps.PpsEntry#getTitlePostOrTimeWorkerData()
     */
        public SupportedPropertyPath<String> titlePostOrTimeWorkerData()
        {
            if(_titlePostOrTimeWorkerData == null )
                _titlePostOrTimeWorkerData = new SupportedPropertyPath<String>(PpsEntryGen.P_TITLE_POST_OR_TIME_WORKER_DATA, this);
            return _titlePostOrTimeWorkerData;
        }

        public Class getEntityClass()
        {
            return PpsEntry.class;
        }

        public String getEntityName()
        {
            return "ppsEntry";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getExtendedTitle();

    public abstract String getShortTitle();

    public abstract String getTitleFioInfoOrgUnit();

    public abstract String getTitleFioInfoOrgUnitDegreesStatuses();

    public abstract String getTitleInfo();

    public abstract String getTitlePostOrTimeWorkerData();
}
