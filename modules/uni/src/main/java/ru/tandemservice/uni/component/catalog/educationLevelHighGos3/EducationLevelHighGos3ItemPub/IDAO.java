/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3ItemPub;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;

/**
 * @author Vasily Zhukov
 * @since 18.11.2011
 */
public interface IDAO extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsItemPub.IDAO<EducationLevelHighGos3, Model>
{
}
