/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.Pub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uni.entity.report.StudentsAgeSexDistributionReport;

/**
 * @author agolubenko
 * @since 18.05.2009
 */
@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "reportId"))
@Output(@Bind(key = "reportId"))
public class Model
{
    Long _reportId;
    StudentsAgeSexDistributionReport _report;

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public StudentsAgeSexDistributionReport getReport()
    {
        return _report;
    }

    public void setReport(StudentsAgeSexDistributionReport report)
    {
        _report = report;
    }
}
