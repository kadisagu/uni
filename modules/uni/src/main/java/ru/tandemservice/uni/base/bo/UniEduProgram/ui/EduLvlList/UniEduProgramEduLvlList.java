/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.list.column.SimpleColumn;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.UniEduProgramEduLevDSHandler;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

/**
 * @author oleyba
 * @since 8/12/14
 */
@Configuration
public class UniEduProgramEduLvlList extends BusinessComponentManager
{
    public static final String EDU_LEV_DS = "eduLevDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
            .addDataSource(searchListDS(EDU_LEV_DS, eduLevDS(), eduLevDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eduLevDS()
    {
        return columnListExtPointBuilder(EDU_LEV_DS)
            .addColumn(textColumn("code", EducationLevels.inheritedOkso().s()).order().create())
            .addColumn(publisherColumn("title", EducationLevels.title().s()).order()
                .create())
            .addColumn(publisherColumn("eduSubject", EducationLevels.eduProgramSubject().titleWithCode().s())
                .order()
                .primaryKeyPath(EducationLevels.eduProgramSubject().id().s())
                .create())
            .addColumn(textColumn("spec", EducationLevels.eduProgramSpecialization().title().s()).order().create())
            .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduLevDSHandler()
    {
        return new UniEduProgramEduLevDSHandler(getName());
    }
}