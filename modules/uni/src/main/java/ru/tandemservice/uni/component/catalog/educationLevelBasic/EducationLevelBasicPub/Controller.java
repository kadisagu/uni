// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelBasic.EducationLevelBasicPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.EducationLevelBasic;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

/**
 * @author oleyba
 * @since 11.05.2010
 */
public class Controller extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsPub.Controller<EducationLevelBasic, Model, IDAO>
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<EducationLevelBasic> dataSource = new DynamicListDataSource<EducationLevelBasic>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", EducationLevelBasic.P_FULL_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EducationLevelBasic.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подчинено профессии (специальности)", new String[]{EducationLevelBasic.L_PARENT_LEVEL, EducationLevelBasic.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", EducationLevelBasic.P_QUALIFICATION_TITLE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Направление класссификатора", EducationLevels.programSubjectWithCodeIndexAndGenTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", EducationLevels.programSpecializationTitle()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}
