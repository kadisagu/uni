/* $Id$ */
package ru.tandemservice.uni.component.student.StudentPersonCard;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;

/**
 * Фасад для печати rtf-меток с информацией о зачислении студента
 *
 * @author Nikolay Fedorovskih
 * @since 09.09.2014
 */
public interface IPrintStudentEnrData
{
    String UNIEC_SPRING_BEAN_NAME = "uniecPrintStudentEnrData";
    String UNIENR14_SPRING_BEAN_NAME = "unienr14PrintStudentEnrData";

    /**
     * Вставка rtf-меток, связанных с зачислением студента - orderNumber, orderDate, course, ball.
     * Если выписки о зачислении на студента нет, возвращается null.
     *
     * @param studentId id студента
     * @return {@link RtfInjectModifier}, если есть приказ о зачислении студента, null - если нет.
     */
    RtfInjectModifier getEnrPrintDataModifier(Long studentId);
}
