/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSettings.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;
import ru.tandemservice.uniedu.entity.EduAccreditation;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

import java.util.Collection;

/**
 * @author Ekaterina Zvereva
 * @since 23.11.2016
 */
public interface IUniAccreditationDao
{

    void saveOrUpdateAccreditation(DataWrapper row, EduInstitutionOrgUnit orgUnit, EduProgramSubjectIndex subjectIndex);

    void updateProgramSubject(Collection<Long> programSubjectIds, boolean license, EduInstitutionOrgUnit orgUnit);

}
