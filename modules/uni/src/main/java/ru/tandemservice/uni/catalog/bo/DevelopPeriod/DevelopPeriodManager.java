/* $Id$ */
package ru.tandemservice.uni.catalog.bo.DevelopPeriod;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uni.catalog.bo.DevelopPeriod.logic.DevelopPeriodDao;
import ru.tandemservice.uni.catalog.bo.DevelopPeriod.logic.IDevelopPeriodDao;

/**
 * @author Alexey Lopatin
 * @since 10.09.2015
 */
@Configuration
public class DevelopPeriodManager extends BusinessObjectManager
{
    public static DevelopPeriodManager instance()
    {
        return instance(DevelopPeriodManager.class);
    }

    @Bean
    public IDevelopPeriodDao dao()
    {
        return new DevelopPeriodDao();
    }
}