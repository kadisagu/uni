/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.IndividualEduPlan;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
public interface IIndividualEduPlanDao extends INeedPersistenceSupport
{
    /**
     * @return название активного индивидуально учебного плана студента
     */
    String getIndividualEduPlan(Student student);
}
