package ru.tandemservice.uni.component.settings.EduOuOrgUnitsList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 03.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EducationOrgUnit> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn().setSelectCaption("Выбор").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ФП", EducationOrgUnit.formativeOrgUnit().shortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ТП", EducationOrgUnit.territorialOrgUnit().territorialShortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ВП", EducationOrgUnit.educationLevelHighSchool().orgUnit().shortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EducationOrgUnit.educationLevelHighSchool().displayableTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EducationOrgUnit.developForm().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EducationOrgUnit.developCondition().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EducationOrgUnit.developTech().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Нормативный срок", EducationOrgUnit.developPeriod().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Диспетчерская", EducationOrgUnit.operationOrgUnit().fullTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Деканат", EducationOrgUnit.groupOrgUnit().fullTitle()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditOrgUnits"));

        model.setDataSource(dataSource);
    }

    public void onClickSetOperationOrgUnit(IBusinessComponent component)
    {
        List<Long> ids = getSelectedIds(component);
        if (ids.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку.");
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit", new ParametersMap()
                .add("ids", ids)
                .add("displayMode", ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit.Model.DisplayMode.OPERATION)));
        getCheckboxColumn(component).reset();
    }

    public void onClickSetGroupOrgUnit(IBusinessComponent component)
    {
        List<Long> ids = getSelectedIds(component);
        if (ids.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку.");
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit", new ParametersMap()
                .add("ids", ids)
                .add("displayMode", ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit.Model.DisplayMode.GROUP)));
        getCheckboxColumn(component).reset();
    }

    public void onClickSetDefaultOperationOrgUnit(IBusinessComponent component)
    {
        List<Long> ids = getSelectedIds(component);
        if (ids.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку.");
        getDao().setDefaultOperationOrgUnit(ids);
        getCheckboxColumn(component).reset();
    }

    public void onClickSetDefaultGroupOrgUnit(IBusinessComponent component)
    {
        List<Long> ids = getSelectedIds(component);
        if (ids.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку.");
        getDao().setDefaultGroupOrgUnit(ids);
        getCheckboxColumn(component).reset();
    }

    public void onClickEditOrgUnits(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit", new ParametersMap()
                .add("ids", Collections.singletonList((Long) component.getListenerParameter()))
                .add("displayMode", ru.tandemservice.uni.component.settings.EduOuOrgUnitsEdit.Model.DisplayMode.BOTH)));
        getCheckboxColumn(component).reset();
    }

    private List<Long> getSelectedIds(IBusinessComponent component)
    {
        return UniBaseUtils.getIdList(getCheckboxColumn(component).getSelectedObjects());
    }

    private CheckboxColumn getCheckboxColumn(IBusinessComponent component)
    {
        return ((CheckboxColumn) getModel(component).getDataSource().getColumn("checkbox"));
    }

    public void onClickSearch(IBusinessComponent context)
    {
        context.saveSettings();
        getModel(context).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(context);
    }    
}
