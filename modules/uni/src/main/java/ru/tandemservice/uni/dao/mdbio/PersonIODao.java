package ru.tandemservice.uni.dao.mdbio;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.exception.CascadeDeleteViolationException;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.*;

import java.io.*;
import java.util.*;

import static com.google.common.base.Objects.firstNonNull;
import static org.apache.commons.lang.StringUtils.trimToEmpty;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class PersonIODao extends BaseIODao implements IPersonIODao
{
    public static final String PERSON_EDU_DOCUMENT_MDB_TABLE = "person_edudocument_t";

    public void export_personEduDocument(final Database mdb) throws IOException
    {
        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            // Для старых документов об образовании метод вызываться не должен
            throw new IllegalStateException();
        }

        if (null != mdb.getTable(PERSON_EDU_DOCUMENT_MDB_TABLE)) {
            return;
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<EduDocumentKind, String> eduDocumentKindMap = catalogIO.catalogEduDocumentKind().export(mdb);
        final Map<EduLevel, String> eduLevelMap = catalogIO.catalogEduLevel().export(mdb);
        final Map<GraduationHonour, String> graduationHonourMap = catalogIO.catalogGraduationHonour().export(mdb);

        final Session session = this.getSession();
        final List<Object[]> rows = new ArrayList<>(1000);

        for (final List<Long> idsPart : getIdListPartition(PersonEduDocument.class)) {
            for (final PersonEduDocument eduDocument : PersonIODao.this.getList(PersonEduDocument.class, idsPart)) {

                final Object[] row = new Object[18];
                int idx = -1;
                row[++idx] = Long.toHexString(eduDocument.getId());
                row[++idx] = Long.toHexString(eduDocument.getPerson().getId());
                row[++idx] = eduDocumentKindMap.get(eduDocument.getEduDocumentKind());
                row[++idx] = eduDocument.getDocumentKindDetailed();
                row[++idx] = trimToEmpty(eduDocument.getSeria());
                row[++idx] = eduDocument.getNumber();
                row[++idx] = eduDocument.getEduOrganization();
                row[++idx] = AddressIODao.getCountryString(eduDocument.getEduOrganizationAddressItem().getCountry());
                row[++idx] = AddressIODao.getSettlementString(eduDocument.getEduOrganizationAddressItem());
                row[++idx] = eduDocument.getYearEnd();
                row[++idx] = DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.getIssuanceDate());
                row[++idx] = eduLevelMap.get(eduDocument.getEduLevel());
                row[++idx] = trimToEmpty(eduDocument.getDocumentEducationLevel());
                row[++idx] = trimToEmpty(eduDocument.getQualification());
                row[++idx] = trimToEmpty(eduDocument.getEduProgramSubject());
                row[++idx] = graduationHonourMap.get(eduDocument.getGraduationHonour());
                row[++idx] = trimToEmpty(eduDocument.getRegistrationNumber());
                row[++idx] = Math.max(0, firstNonNull(eduDocument.getMark5(), 0)) + "," +
                        Math.max(0, firstNonNull(eduDocument.getMark4(), 0)) + "," +
                        Math.max(0, firstNonNull(eduDocument.getMark3(), 0));

                rows.add(row);
            }
            session.clear();
        }

        new TableBuilder(PERSON_EDU_DOCUMENT_MDB_TABLE)
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("edu_document_kind", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("edu_document_kind_detailed", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("seria", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("number", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("edu_organization", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("edu_organization_country_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("edu_organization_settlement_p", DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("year_end", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("issuance_date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("edu_level", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("document_education_level", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("qualification", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("edu_program_subject", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("graduation_honour", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("registration_number", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("marks", DataType.TEXT).setCompressedUnicode(true).toColumn())

                .toTable(mdb)
                .addRows(rows);
    }

    @SuppressWarnings("deprecation")
    public void export_PersonEduInstitution(final Database mdb) throws IOException
    {
        if (PersonEduDocumentManager.isShowNewEduDocuments()) {
            // Для новых документов об образовании метод вызываться не должен
            throw new IllegalStateException();
        }

        if (null != mdb.getTable("person_eduinstitution_t")) {
            return;
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<EducationalInstitutionTypeKind, String> educationalInstitutionTypeKindMap = catalogIO.catalogEducationalInstitutionTypeKind().export(mdb);
        final Map<EducationDocumentType, String> educationDocumentTypeMap = catalogIO.catalogEducationDocumentType().export(mdb);
        final Map<EducationLevelStage, String> educationLevelStageMap = catalogIO.catalogEducationLevelStage().export(mdb);
        final Map<EmployeeSpeciality, String> employeeSpecialityMap = catalogIO.catalogEmployeeSpeciality().export(mdb);
        final Map<DiplomaQualifications, String> diplomaQualificationsMap = catalogIO.catalogDiplomaQualifications().export(mdb);
        final Map<GraduationHonour, String> graduationHonourMap = catalogIO.catalogGraduationHonour().export(mdb);

        catalogIO.export_catalogEducationalInstitution(mdb);

        final Table person_eduinstitution_t = new TableBuilder("person_eduinstitution_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("edu_institution_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("educational_institution_country_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("educational_institution_settlement_p", DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("educational_institution_type_kind", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("education_document_type", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("education_level_stage", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("employee_speciality", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("diploma_qualification", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("qualification", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("seria", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("number", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("marks", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("region_code", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("graduation_honour", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("year_end", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        final Session session = this.getSession();
        for (final List<Long> idsPart : getIdListPartition(PersonEduInstitution.class)) {
            for (final PersonEduInstitution eduInstitution : PersonIODao.this.getList(PersonEduInstitution.class, idsPart)) {

                person_eduinstitution_t.addRow(
                        Long.toHexString(eduInstitution.getId()),
                        Long.toHexString(eduInstitution.getPerson().getId()),
                        (null == eduInstitution.getEduInstitution() ? null : Long.toHexString(eduInstitution.getEduInstitution().getId())),
                        AddressIODao.getCountryString(eduInstitution.getAddressItem().getCountry()),
                        AddressIODao.getSettlementString(eduInstitution.getAddressItem()),
                        educationalInstitutionTypeKindMap.get(eduInstitution.getEduInstitutionKind()),
                        educationDocumentTypeMap.get(eduInstitution.getDocumentType()),
                        educationLevelStageMap.get(eduInstitution.getEducationLevelStage()),
                        employeeSpecialityMap.get(eduInstitution.getEmployeeSpeciality()),
                        diplomaQualificationsMap.get(eduInstitution.getDiplomaQualification()),
                        eduInstitution.getQualification(),
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(eduInstitution.getIssuanceDate()),
                        trimToEmpty(eduInstitution.getSeria()), trimToEmpty(eduInstitution.getNumber()),
                        (Math.max(0, (null == eduInstitution.getMark5() ? 0 : eduInstitution.getMark5())) + "," + Math.max(0, (null == eduInstitution.getMark4() ? 0 : eduInstitution.getMark4())) + "," + Math.max(0, (null == eduInstitution.getMark3() ? 0 : eduInstitution.getMark3()))),
                        eduInstitution.getRegionCode(),
                        graduationHonourMap.get(eduInstitution.getGraduationHonour()),
                        eduInstitution.getYearEnd()
                );
            }
            session.clear();
        }
    }

    public void export_PersonSportAchievement(final Database mdb) throws IOException
    {
        if (null != mdb.getTable("person_sportachievement_t")) {
            return;
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<SportType, String> sportTypeMap = catalogIO.catalogSportType().export(mdb);
        final Map<SportRank, String> sportRankMap = catalogIO.catalogSportRank().export(mdb);

        final Table person_sportachievement_t = new TableBuilder("person_sportachievement_t")
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("sport_type_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("sport_rank_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("bestachievement_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        final Session session = PersonIODao.this.getSession();

        for (List<Long> idsPart : getIdListPartition(PersonSportAchievement.class)) {
            for (final PersonSportAchievement sportAchievement : PersonIODao.this.getList(PersonSportAchievement.class, "id", idsPart))
            {
                person_sportachievement_t.addRow(
                        Long.toHexString(sportAchievement.getPerson().getId()),
                        sportTypeMap.get(sportAchievement.getSportKind()),
                        sportRankMap.get(sportAchievement.getSportRank()),
                        (sportAchievement.getBestAchievement())
                );
            }
            session.clear();
        }
    }

    public void export_PersonNextOfKin(final Database mdb) throws IOException
    {
        if (null != mdb.getTable("person_nextofkin_t")) {
            return;
        }

        final Table address_t = mdb.getTable("address_t") != null ? mdb.getTable("address_t") : new TableBuilder("address_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("address_p", DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<RelationDegree, String> relationDegreeMap = catalogIO.catalogRelationDegree().export(mdb);

        final Table person_nextofkin_t = new TableBuilder("person_nextofkin_t")
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("relation_degree_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_seria", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_number", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_firstName", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_lastName", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_middleName", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_birthDate", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_date_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_code_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_place_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("employee_place_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("phones_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("address_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        final Session session = this.getSession();
        for (final List<Long> idsPart : getIdListPartition(PersonNextOfKin.class)) {
            for (final PersonNextOfKin nextOfKin : PersonIODao.this.getList(PersonNextOfKin.class, "id", idsPart)) {

                final AddressBase address = nextOfKin.getAddress();
                person_nextofkin_t.addRow(
                        Long.toHexString(nextOfKin.getPerson().getId()),
                        relationDegreeMap.get(nextOfKin.getRelationDegree()),
                        StringUtils.trimToNull(nextOfKin.getPassportSeria()),
                        StringUtils.trimToNull(nextOfKin.getPassportNumber()),
                        StringUtils.trimToNull(nextOfKin.getFirstName()),
                        StringUtils.trimToNull(nextOfKin.getLastName()),
                        StringUtils.trimToNull(nextOfKin.getMiddleName()),
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(nextOfKin.getBirthDate()),
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(nextOfKin.getPassportIssuanceDate()),
                        StringUtils.trimToNull(nextOfKin.getPassportIssuanceCode()),
                        StringUtils.trimToNull(nextOfKin.getPassportIssuancePlace()),
                        StringUtils.trimToNull(nextOfKin.getEmploymentPlace()),
                        StringUtils.trimToNull(nextOfKin.getPhones()),
                        address != null ? Long.toHexString(address.getId()) : null
                );

                if (address != null) {
                    address_t.addRow(
                            Long.toHexString(address.getId()),
                            address.getTitleWithFlat()
                    );
                }
            }
            session.clear();
        }
    }

    public void export_PersonForeignLanguage(final Database mdb) throws IOException
    {
        if (null != mdb.getTable("person_foreignlanguage_t")) {
            return;
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<ForeignLanguage, String> foreignLanguageMap = catalogIO.catalogForeignLanguage().export(mdb);
        final Map<ForeignLanguageSkill, String> foreignLanguageSkillMap = catalogIO.catalogForeignLanguageSkill().export(mdb);

        final Table person_foreignlanguage_t = new TableBuilder("person_foreignlanguage_t")
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("language_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("skill_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        final Session session = this.getSession();
        for (final List<Long> idsPart : getIdListPartition(PersonForeignLanguage.class)) {
            for (final PersonForeignLanguage foreignLanguage : PersonIODao.this.getList(PersonForeignLanguage.class, idsPart)) {
                person_foreignlanguage_t.addRow(
                        Long.toHexString(foreignLanguage.getPerson().getId()),
                        foreignLanguageMap.get(foreignLanguage.getLanguage()),
                        foreignLanguageSkillMap.get(foreignLanguage.getSkill())
                );
            }
            session.clear();
        }
    }

    public void export_PersonBenefit(final Database mdb) throws IOException
    {
        if (null != mdb.getTable("person_benefit_t")) {
            return;
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<Benefit, String> benefitMap = catalogIO.catalogBenefitCategory().export(mdb);

        final Table person_benefit_t = new TableBuilder("person_benefit_t")
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("benefit_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("basic_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("date_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("document_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        final Session session = this.getSession();
        for (final List<Long> idsPart : getIdListPartition(PersonBenefit.class)) {
            for (final PersonBenefit benefit : PersonIODao.this.getList(PersonBenefit.class, idsPart)) {
                person_benefit_t.addRow(
                        Long.toHexString(benefit.getPerson().getId()),
                        benefitMap.get(benefit.getBenefit()),
                        benefit.getBasic(),
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(benefit.getDate()),
                        benefit.getDocument()
                );
            }
            session.clear();
        }
    }

    @Override
    public void export_PersonList(final Database mdb, final boolean full) throws IOException {

        if (null != mdb.getTable("person_t")) {
            return;
        }

        if (full) {
            IAddressIODao.instance.get().export_addressSettlement(mdb);
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<Sex, String> sexMap = catalogIO.catalogSex().export(mdb);
        final Map<IdentityCardType, String> identityCardTypeMap = catalogIO.catalogIdentityCardType().export(mdb);

        final Map<FamilyStatus, String> familyStatusMap = full ? catalogIO.catalogFamilyStatus().export(mdb) : Collections.<FamilyStatus, String>emptyMap();
        final Map<PensionType, String> pensionTypeMap = full ? catalogIO.catalogPensionType().export(mdb) : Collections.<PensionType, String>emptyMap();
        final Map<FlatPresence, String> flatPresenceMap = full ? catalogIO.catalogFlatPresence().export(mdb) : Collections.<FlatPresence, String>emptyMap();

        if (full) {

            if (PersonEduDocumentManager.isShowNewEduDocuments()) {
                this.export_personEduDocument(mdb);
            } else {
                this.export_PersonEduInstitution(mdb);
            }

            this.export_PersonSportAchievement(mdb);
            this.export_PersonNextOfKin(mdb);
            this.export_PersonForeignLanguage(mdb);
            this.export_PersonBenefit(mdb);
        }

        final TableBuilder tableBuilder = new TableBuilder("person_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_type_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_seria", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_number", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_firstName", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_lastName", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_middleName", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_birthDate", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_birthPlace", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_sex_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_date_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_code_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder("identity_place_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("identity_citizenship_p", DataType.MEMO).setCompressedUnicode(true).toColumn());


        if (full) {

            tableBuilder
                    .addColumn(new ColumnBuilder("family_status", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                    .addColumn(new ColumnBuilder("child_count", DataType.INT).toColumn())
                    .addColumn(new ColumnBuilder("pension_type", DataType.MEMO).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("pension_issuance_date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("flat_presence", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())

                    .addColumn(new ColumnBuilder("service_length_years", DataType.INT).toColumn())
                    .addColumn(new ColumnBuilder("service_length_months", DataType.INT).toColumn())
                    .addColumn(new ColumnBuilder("service_length_days", DataType.INT).toColumn())

                    .addColumn(new ColumnBuilder("address_reg_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("address_fact_id", DataType.TEXT).setCompressedUnicode(true).toColumn());

            if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
                tableBuilder.addColumn(new ColumnBuilder("person_edu_institution", DataType.MEMO).setCompressedUnicode(true).toColumn());
            }

            tableBuilder.addColumn(new ColumnBuilder("inn_number", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn());
            tableBuilder.addColumn(new ColumnBuilder("snils_number", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn());
        }

        final Table person_t = tableBuilder.toTable(mdb);

        final Table address_t = mdb.getTable("address_t") != null ? mdb.getTable("address_t") : new TableBuilder("address_t")
                .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("address_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .toTable(mdb);


        final Session session = this.getSession();
        final List<Object[]> rows = new ArrayList<>(1000);
        final List<Object[]> addressRows = new ArrayList<>(1000);

        for (final List<Long> idsPart : getIdListPartition(Person.class)) {

            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Person.class, "p", true)
                    .column(property("p"))
                    .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "idc")
                    .where(in(property("p.id"), idsPart));

            for (final Person person : PersonIODao.this.<Person>getList(dql)) {

                final IdentityCard identityCard = person.getIdentityCard();
                final List<Object> row = new ArrayList<>(person_t.getColumnCount());

                // основные колонки
                row.add(Long.toHexString(person.getId()));
                row.add(identityCardTypeMap.get(identityCard.getCardType()));
                row.add(StringUtils.trimToNull(identityCard.getSeria()));
                row.add(StringUtils.trimToNull(identityCard.getNumber()));
                row.add(StringUtils.trimToNull(identityCard.getFirstName()));
                row.add(StringUtils.trimToNull(identityCard.getLastName()));
                row.add(StringUtils.trimToNull(identityCard.getMiddleName()));
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()));
                row.add(StringUtils.trimToNull(identityCard.getBirthPlace()));
                row.add(sexMap.get(identityCard.getSex()));
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()));
                row.add(identityCard.getIssuanceCode());
                row.add(identityCard.getIssuancePlace());
                row.add(AddressIODao.getCitizenshipString(identityCard.getCitizenship()));

                if (full) {
                    // дополнительные колонки

                    final AddressBase regAddress = identityCard.getAddress();
                    final AddressBase factAddress = person.getAddress();

                    row.add(familyStatusMap.get(person.getFamilyStatus()));
                    row.add(person.getChildCount());
                    row.add(pensionTypeMap.get(person.getPensionType()));
                    row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(person.getPensionIssuanceDate()));
                    row.add(flatPresenceMap.get(person.getFlatPresence()));

                    row.add(Math.max(0, person.getServiceLengthYears()));
                    row.add(Math.max(0, person.getServiceLengthMonths()));
                    row.add(Math.max(0, person.getServiceLengthDays()));
                    row.add(regAddress != null ? Long.toHexString(regAddress.getId()) : null);
                    row.add(factAddress != null ? Long.toHexString(factAddress.getId()) : null);

                    if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
                        row.add(person.getPersonEduInstitution() == null ? null : Long.toHexString(person.getPersonEduInstitution().getId()));
                    }

                    row.add(person.getInnNumber());
                    row.add(person.getSnilsNumber());

                    if (regAddress != null) {
                        addressRows.add(new Object[]{Long.toHexString(regAddress.getId()), regAddress.getTitleWithFlat()});
                    }
                    if (factAddress != null) {
                        addressRows.add(new Object[]{Long.toHexString(factAddress.getId()), factAddress.getTitleWithFlat()});
                    }
                }

                rows.add(row.toArray());
            }
            session.clear();
        }

        address_t.addRows(addressRows);
        person_t.addRows(rows);
    }

    // -----------------------------------------------------------------------------------
    // --------------------------------- IMPORT ------------------------------------------

    @Override
    public Map<String, Long> doImport_PersonList(final Database mdb) throws IOException {

        final Session session = this.getSession();

        final String CACHE_KEY = "PersonIODao.doImport_PersonList";
        {
            final Map<String, Long> personIdMap = DaoCache.get(CACHE_KEY);
            if (null != personIdMap) {
                return personIdMap;
            }
        }

        final Table person_t = mdb.getTable("person_t");
        if (null == person_t) {
            throw new ApplicationException("Таблица «person_t» отсутствует в базе данных.");
        }

        final Table address_t = mdb.getTable("address_t");
        // address_t.id -> address_t.row
        final Map<String, Map<String, Object>> addressMap = new HashMap<>();
        execute(address_t, 128, "Адреса", rows -> {
            for (final Map<String, Object> row : rows) {
                final String id = (String) row.get("id");
                addressMap.put(id, row);
            }
        });

        // справочники
        final IAddressIODao addressIO = IAddressIODao.instance.get();
        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, IdentityCardType> identityCardTypeMap = catalogIO.catalogIdentityCardType().lookup(true);
        final Map<String, Sex> sexMap = catalogIO.catalogSex().lookup(true);
        final Map<String, FamilyStatus> familyStatusMap = catalogIO.catalogFamilyStatus().lookup(false);
        final Map<String, PensionType> pensionTypeMap = catalogIO.catalogPensionType().lookup(false);
        final Map<String, FlatPresence> flatPresenceMap = catalogIO.catalogFlatPresence().lookup(false);

        // список файлов с фотографиями
        final List<String> photoList = new ArrayList<>();
        final File photoDir = getPhotoDir(photoList);

        final IFieldPropertyMeta birthPlaceProperty = getFieldProperty(IdentityCard.class, IdentityCard.P_BIRTH_PLACE);
        final IFieldPropertyMeta issuancePlaceProperty = getFieldProperty(IdentityCard.class, IdentityCard.P_ISSUANCE_PLACE);

        final Map<String, Long> personIdMap = new HashMap<>(person_t.getRowCount());
        execute(person_t, 128, "Персоны (основные данные)", new BatchUtils.Action<Map<String, Object>>()
        {
            @Override
            public void execute(final Collection<Map<String, Object>> rows)
            {
                final Map<String, Object> localPersonMap = Maps.newHashMapWithExpectedSize(rows.size());

                for (final Map<String, Object> row : rows)
                {
                    final String idStr = (String) row.get("id");
                    final Long id = tryParseHexId(idStr);
                    if (id != null) {
                        localPersonMap.put(idStr, id);
                    }
                }

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Person.class, "p").column(property("p"));
                dql.where(in(property("p.id"), localPersonMap.values()));
                for (final Person person : dql.createStatement(session).<Person>list()) {
                    localPersonMap.put(Long.toHexString(person.getId()), person);
                }

                for (final Map<String, Object> row : rows)
                {
                    final String id = (String) row.get("id");
                    final String context = "person_t[" + id + "]";

                    final Person person = createPerson(localPersonMap, id);
                    final IdentityCard identityCard = person.getIdentityCard();

                    try
                    {
                        parseAndFillIDC(row, identityCard, context);
                        parseAndFillPensionInfo(row, context, person, pensionTypeMap);

                        person.setFlatPresence(PersonIODao.this.lookup(flatPresenceMap, row, "flat_presence", context));
                        person.setFamilyStatus(PersonIODao.this.lookup(familyStatusMap, row, "family_status", context));
                        person.setChildCount(PersonIODao.this.integer(row, "child_count", context, false));

                        person.setServiceLengthDays(PersonIODao.this.integer(row, "service_length_days", context, true));
                        person.setServiceLengthMonths(PersonIODao.this.integer(row, "service_length_months", context, true));
                        person.setServiceLengthYears(PersonIODao.this.integer(row, "service_length_years", context, true));

                        person.setInnNumber(StringUtils.trimToNull((String) row.get("inn_number")));
                        person.setSnilsNumber(StringUtils.trimToNull((String) row.get("snils_number")));

                        parseAndFillRegAddress(row, context, identityCard, addressMap);
                        parseAndFillFactAddress(row, context, person, addressMap);
                        savePhoto(id, person);

                        session.saveOrUpdate(identityCard);
                        session.saveOrUpdate(person.getContactData());
                        session.saveOrUpdate(person);
                        identityCard.setPerson(person);
                        session.saveOrUpdate(identityCard);
                        personIdMap.put(id, person.getId());

                    }
                    catch (final Exception t) {
                        logAndThrowPersonParseError(id, context, t);
                    }
                }

                System.err.print(".");
                session.flush();
                session.clear();
            }

            private void savePhoto(String id, Person person)
            {
                if (!photoList.contains(id + ".jpg"))
                    return;

                File file;
                try {
                    file = new File(photoDir, id + ".jpg");
                } catch (Exception e) {
                    throw new IllegalStateException("Error with processing file photo with name: " + id + ".jpg", e);
                }

                if (file.length() > 50 * 1024) {
                    throw new IllegalArgumentException("Photo: " + id + ".jpg size is too large. Size should not exceed 50Kb.");
                }

                try (final InputStream fileInputStream = new FileInputStream(file);
                     final InputStream stream = new BufferedInputStream(fileInputStream))
                {
                    final DatabaseFile photo = new DatabaseFile();
                    photo.setFilename(file.getName());
                    photo.setContentType(DatabaseFile.CONTENT_TYPE_IMAGE_JPG);
                    photo.setContent(CoreUtils.getBytes(stream));
                    person.getIdentityCard().setPhoto(photo);
                    session.save(photo);
                } catch (Exception e) {
                    throw new IllegalStateException("Error with processing file photo with name: " + id + ".jpg", e);
                }
            }

            private void parseAndFillFactAddress(Map<String, Object> row, String context, Person person, Map<String, Map<String, Object>> addressMap)
            {
                final String address_fact_id = (String) row.get("address_fact_id");
                if (StringUtils.isEmpty(address_fact_id)) {
                    return;
                }

                // Если id совпадает с уже существующим адресом, то оставляем существующий адрес
                if (person.getAddress() != null && person.getAddress().getId().equals(tryParseHexId(address_fact_id))) {
                    return;
                }

                final Map<String, Object> addressRow = addressMap.get(address_fact_id);
                if (addressRow == null) {
                    final String person_id = (String) row.get("id");
                    log4j_logger.error(context + ": table address_t does not contain row with id=" + address_fact_id + ", specified in column 'address_fact_id' in table person_t in row with id=" + person_id);
                    return;
                }

                final String addressString = (String) addressRow.get("address_p");
                if (StringUtils.isNotEmpty(addressString)) {
                    final AddressString newAddress = new AddressString();
                    newAddress.setAddress(addressString);
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(person, newAddress, Person.L_ADDRESS);
                }
            }

            private void parseAndFillRegAddress(Map<String, Object> row, String context, IdentityCard identityCard, Map<String, Map<String, Object>> addressMap)
            {
                final String address_reg_id = (String) row.get("address_reg_id");
                if (StringUtils.isEmpty(address_reg_id)) {
                    return;
                }

                // Если id совпадает с уже существующим адресом, то оставляем существующий адрес
                if (identityCard.getAddress() != null && identityCard.getAddress().getId().equals(tryParseHexId(address_reg_id))) {
                    return;
                }

                final Map<String, Object> addressRow = addressMap.get(address_reg_id);
                if (addressRow == null) {
                    final String person_id = (String) row.get("id");
                    log4j_logger.error(context + ": table address_t does not contain row with id=" + address_reg_id + ", specified in column 'address_reg_id' in table person_t in row with id=" + person_id);
                    return;
                }

                final String addressString = (String) addressRow.get("address_p");
                if (StringUtils.isNotEmpty(addressString)) {
                    final AddressString newAddress = new AddressString();
                    newAddress.setAddress(addressString);
                    AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(identityCard, newAddress, IdentityCard.L_ADDRESS);
                }
            }

            private void parseAndFillPensionInfo(Map<String, Object> row, String context, Person person, Map<String, PensionType> pensionTypeMap) {
                person.setPensionType(lookup(pensionTypeMap, row, "pension_type", context));
                person.setPensionIssuanceDate(person.getPensionType() == null ? null : PersonIODao.this.date(row, "pension_issuance_date", context, false));
            }


            private void parseAndFillIDC(Map<String, Object> row, IdentityCard identityCard, String context)
            {
                final String identityType = (String) row.get("identity_type_p");
                final String identitySex = (String) row.get("identity_sex_p");
                identityCard.setCardType(identityCardTypeMap.get(identityType));
                identityCard.setSex(sexMap.get(identitySex));

                final String firstName = StringUtils.trimToNull((String) row.get("identity_firstName"));
                final String lastName = StringUtils.trimToNull((String) row.get("identity_lastName"));
                if (firstName == null || lastName == null) {
                    throw new IllegalArgumentException("Values in fields 'identity_firstName' and 'identity_lastName' must not be null");
                }

                identityCard.setSeria(trimToEmpty((String) row.get("identity_seria")));
                identityCard.setNumber(trimToEmpty((String) row.get("identity_number")));
                identityCard.setFirstName(firstName);
                identityCard.setLastName(lastName);
                identityCard.setMiddleName(trimToEmpty((String) row.get("identity_middleName")));
                identityCard.setBirthPlace(PersonIODao.this.trimmedString(row, "identity_birthPlace", context, birthPlaceProperty));
                identityCard.setIssuanceCode(trimToEmpty((String) row.get("identity_code_p")));
                identityCard.setIssuancePlace(PersonIODao.this.trimmedString(row, "identity_place_p", context, issuancePlaceProperty));
                identityCard.setCitizenship(addressIO.findCitizenship((String) row.get("identity_citizenship_p")));
                identityCard.setBirthDate(PersonIODao.this.date(row, "identity_birthDate", context, false));
                identityCard.setIssuanceDate(PersonIODao.this.date(row, "identity_date_p", context, false));
            }

            private Person createPerson(final Map<String, Object> localPersonMap, final String id)
            {
                Object p = localPersonMap.get(id);
                if (!(p instanceof Person)) {
                    Person person = new Person();
                    person.setIdentityCard(new IdentityCard());
                    person.setContactData(new PersonContactData());
                    localPersonMap.put(id, p = person);
                }
                return (Person) p;
            }
        });

        session.flush();
        session.clear();

        final Map<String, Person> personMap = SafeMap.get((String id) -> {
            final Long personId = personIdMap.get(id);
            if (null == personId) {
                throw new IllegalStateException("Person with id '" + id + "' is not found in the database.");
            }
            return (Person) session.load(Person.class, personId);
        });


        this.doImport_PersonNextOfKinList(mdb, personIdMap, personMap, addressMap);
        this.doImport_PersonForeignLanguage(mdb, personIdMap, personMap);
        this.doImport_PersonSportAchievement(mdb, personIdMap, personMap);
        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            this.doImport_PersonEduInstitution(mdb, personIdMap, personMap);
        } else {
            this.doImport_PersonEduDocument(mdb, personIdMap);
        }
        this.doImport_PersonBenefit(mdb, personIdMap);

        DaoCache.put(CACHE_KEY, personIdMap);
        return personIdMap;
    }

    private File getPhotoDir(List<String> photoList)
    {
        File photoDir = null;
        try {

            photoDir = new File(ApplicationRuntime.getAppInstallPath(), "/data/student_import/photo/");
            if (!photoDir.exists())
                log4j_logger.error("Directory with photo does not exist: " + photoDir.getAbsolutePath() + "");
            else
                photoList.addAll(Arrays.asList(photoDir.list(new RegexFileFilter("\\w+[.]jpg$"))));
        }
        catch (SecurityException e) {
            log4j_logger.error("Denied read access to the directory: " + ApplicationRuntime.getAppInstallPath() + "/data/student_import/photo/", e);
        } catch (Exception t) {
            log4j_logger.error("Error with processing files photos in directory: " + ApplicationRuntime.getAppInstallPath() + "/data/student_import/photo/", t);
        }
        return photoDir;
    }

    protected void doImport_PersonSportAchievement(final Database mdb, final Map<String, Long> personIdMap, Map<String, Person> personMap) throws IOException
    {
        final Table person_sportachievement_t = mdb.getTable("person_sportachievement_t");
        if (null == person_sportachievement_t) {
            throw new ApplicationException("Таблица «person_sportachievement_t» отсутствует в базе данных.");
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, SportType> sportTypeMap = catalogIO.catalogSportType().lookup(true);
        final Map<String, SportRank> sportRankMap = catalogIO.catalogSportRank().lookup(true);

        final Session session = this.getSession();

        final IFieldPropertyMeta bestAchievementProperty = getFieldProperty(PersonSportAchievement.class, PersonSportAchievement.P_BEST_ACHIEVEMENT);

        // удаление старых данных
        execute(person_sportachievement_t, DQL.MAX_VALUES_ROW_NUMBER, "Спортивные достижения персон (1/2)", rows -> {

            final Set<Long> personIds = new HashSet<>(rows.size());
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final Long personId = personIdMap.get(id);
                if (null != personId) {
                    personIds.add(personId);
                }
            }

            new DQLDeleteBuilder(PersonSportAchievement.class)
                    .where(in(property(PersonSportAchievement.person().id()), personIds))
                    .createStatement(session).execute();
        });

        // загрузка новых данных
        execute(person_sportachievement_t, 32, "Спортивные достижения персон (2/2)", rows -> {

            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final String context = "person_sportachievement_t[person_id=" + id + "]";

                try
                {
                    if (StringUtils.isBlank(id)) {
                        throw new IllegalStateException("Field 'person_id' must not be null.");
                    }

                    final Person person = personMap.get(id);
                    final PersonSportAchievement sportAchievement = new PersonSportAchievement(person);
                    sportAchievement.setSportKind(PersonIODao.this.lookup(sportTypeMap, row, "sport_type_p", context));
                    sportAchievement.setSportRank(PersonIODao.this.lookup(sportRankMap, row, "sport_rank_p", context));
                    sportAchievement.setBestAchievement(PersonIODao.this.trimmedString(row, "bestachievement_p", context, bestAchievementProperty));

                    session.save(sportAchievement);

                } catch (final Exception t) {
                    logAndThrowPersonParseError(id, context, t, "Произошла ошибка обработки записи о спортивных достижениях.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });
    }

    protected void doImport_PersonForeignLanguage(final Database mdb, final Map<String, Long> personIdMap, Map<String, Person> personMap) throws IOException
    {
        final Table person_foreignlanguage_t = mdb.getTable("person_foreignlanguage_t");
        if (null == person_foreignlanguage_t) {
            throw new ApplicationException("Таблица «person_foreignlanguage_t» отсутствует в базе данных.");
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, ForeignLanguage> foreignLanguageMap = catalogIO.catalogForeignLanguage().lookup(true);
        final Map<String, ForeignLanguageSkill> foreignLanguageSkillMap = catalogIO.catalogForeignLanguageSkill().lookup(false);

        final Session session = this.getSession();

        // удаление старых
        execute(person_foreignlanguage_t, DQL.MAX_VALUES_ROW_NUMBER, "Иностранные языки персон (1/2)", rows -> {

            final Set<Long> personIds = new HashSet<>(rows.size());
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final Long personId = personIdMap.get(id);
                if (null != personId) {
                    personIds.add(personId);
                }
            }

            new DQLDeleteBuilder(PersonForeignLanguage.class)
                    .where(in(property(PersonForeignLanguage.person().id()), personIds))
                    .createStatement(session).execute();
        });

        // загрузка новых
        execute(person_foreignlanguage_t, 32, "Иностранные языки персон (2/2)", rows -> {

            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final String context = "person_foreignlanguage_t[person_id=" + id + "]";

                try
                {
                    if (StringUtils.isBlank(id)) {
                        throw new IllegalStateException("Field 'person_id' must not be null.");
                    }

                    final Person person = personMap.get(id);
                    final PersonForeignLanguage personForeignLanguage = new PersonForeignLanguage(person);
                    personForeignLanguage.setLanguage(PersonIODao.this.lookup(foreignLanguageMap, row, "language_p", context));
                    personForeignLanguage.setSkill(PersonIODao.this.lookup(foreignLanguageSkillMap, row, "skill_p", context));

                    session.saveOrUpdate(personForeignLanguage);
                }
                catch (final Exception t) {
                    logAndThrowPersonParseError(id, context, t, "Произошла ошибка обработки записи об иностранных языках.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });
    }

    protected void doImport_PersonBenefit(final Database mdb, final Map<String, Long> personIdMap) throws IOException
    {
        final Table person_benefit_t = mdb.getTable("person_benefit_t");
        if (null == person_benefit_t) {
            throw new ApplicationException("Таблица «person_benefit_t» отсутствует в базе данных.");
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, Benefit> catalogBenefitMap = catalogIO.catalogBenefitCategory().lookup(true);

        final Session session = this.getSession();

        final IFieldPropertyMeta basicProperty = getFieldProperty(PersonBenefit.class, PersonBenefit.P_BASIC);
        final IFieldPropertyMeta documentProperty = getFieldProperty(PersonBenefit.class, PersonBenefit.P_DOCUMENT);

        // удаление старых
        execute(person_benefit_t, DQL.MAX_VALUES_ROW_NUMBER, "Льготы персон (1/2)", rows -> {

            final Set<Long> personIds = new HashSet<>(rows.size());
            for (final Map<String, Object> row : rows) {

                final String id = (String) row.get("person_id");
                final Long personId = personIdMap.get(id);
                if (null != personId) {
                    personIds.add(personId);
                }
            }

            new DQLDeleteBuilder(PersonBenefit.class)
                    .where(in(property(PersonBenefit.person().id()), personIds))
                    .createStatement(session).execute();
        });

        // загрузка новых
        execute(person_benefit_t, 32, "Льготы персон (2/2)", new BatchUtils.Action<Map<String, Object>>()
        {
            private final Map<String, Person> personMap = SafeMap.get((String id) -> {
                final Long personId = personIdMap.get(id);
                if (null == personId) {
                    throw new IllegalStateException("Person with id '" + id + "' is not found in the database.");
                }
                return (Person) session.load(Person.class, personId);
            });

            @Override
            public void execute(final Collection<Map<String, Object>> rows)
            {
                for (final Map<String, Object> row : rows)
                {
                    final Object id = row.get("person_id");
                    final String context = "person_benefit_t[person_id=" + id + "]";

                    try
                    {
                        if (StringUtils.isBlank((String) id))
                            throw new IllegalStateException("Field 'person_id' must not be null.");

                        final Person person = personMap.get(id);
                        final PersonBenefit personBenefit = new PersonBenefit(person);
                        final String benefit = (String) row.get("benefit_p");
                        personBenefit.setBenefit(catalogBenefitMap.get(benefit));

                        personBenefit.setBasic(PersonIODao.this.trimmedString(row, "basic_p", context, basicProperty));
                        personBenefit.setDocument(PersonIODao.this.trimmedString(row, "document_p", context, documentProperty));
                        personBenefit.setDate(PersonIODao.this.date(row, "date_p", context, false));

                        session.saveOrUpdate(personBenefit);
                    }
                    catch (final Exception t)
                    {
                        logAndThrowPersonParseError(id, context, t, "Произошла ошибка обработки записи о льготах.");
                    }
                }

                System.err.print(".");
                session.flush();
                session.clear();
            }
        });
    }

    protected void doImport_PersonNextOfKinList(final Database mdb, final Map<String, Long> personIdMap, Map<String, Person> personMap, final Map<String, Map<String, Object>> addressMap) throws IOException
    {
        final Table person_nextofkin_t = mdb.getTable("person_nextofkin_t");
        if (null == person_nextofkin_t) {
            throw new ApplicationException("Таблица «person_nextofkin_t» отсутствует в базе данных.");
        }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, RelationDegree> relationDegreeMap = catalogIO.catalogRelationDegree().lookup(true);

        final Session session = this.getSession();

        // удаление старых
        execute(person_nextofkin_t, DQL.MAX_VALUES_ROW_NUMBER, "Ближайшие родственики персон (1/2)", rows -> {

            final Set<Long> personIds = new HashSet<>(rows.size());
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final Long personId = personIdMap.get(id);
                if (null != personId) {
                    personIds.add(personId);
                }
            }

            new DQLDeleteBuilder(PersonNextOfKin.class)
                    .where(in(property(PersonNextOfKin.person().id()), personIds))
                    .createStatement(session).execute();
        });

        final IFieldPropertyMeta passportIssuancePlaceProperty = getFieldProperty(PersonNextOfKin.class, PersonNextOfKin.P_PASSPORT_ISSUANCE_PLACE);
        final IFieldPropertyMeta employmentPlaceProperty = getFieldProperty(PersonNextOfKin.class, PersonNextOfKin.P_EMPLOYMENT_PLACE);

        // загрузка новых
        execute(person_nextofkin_t, 64, "Ближайшие родственики персон (2/2)", rows -> {

            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final String context = "person_nextofkin_t[person_id=" + id + "]";

                try
                {
                    if (StringUtils.isBlank(id)) {
                        throw new IllegalStateException("Field value for 'person_id' must not be null.");
                    }

                    final Person person = personMap.get(id);
                    final PersonNextOfKin personNexOfKin = new PersonNextOfKin(person);

                    String firstName = StringUtils.trimToNull((String) row.get("identity_firstName"));
                    String lastName = StringUtils.trimToNull((String) row.get("identity_lastName"));
                    if (firstName == null || lastName == null) {
                        throw new IllegalArgumentException("Values in fields 'identity_firstName' and 'identity_lastName' must not be null.");
                    }

                    final String relationDegree = (String) row.get("relation_degree_p");
                    personNexOfKin.setRelationDegree(relationDegreeMap.get(relationDegree));
                    personNexOfKin.setPassportSeria(trimToEmpty((String) row.get("identity_seria")));
                    personNexOfKin.setPassportNumber(trimToEmpty((String) row.get("identity_number")));
                    personNexOfKin.setFirstName(firstName);
                    personNexOfKin.setLastName(lastName);
                    personNexOfKin.setMiddleName(trimToEmpty((String) row.get("identity_middleName")));
                    personNexOfKin.setPassportIssuanceCode(trimToEmpty((String) row.get("identity_code_p")));
                    personNexOfKin.setPassportIssuancePlace(PersonIODao.this.trimmedString(row, "identity_place_p", context, passportIssuancePlaceProperty));
                    personNexOfKin.setBirthDate(PersonIODao.this.date(row, "identity_birthDate", context, false));
                    personNexOfKin.setPassportIssuanceDate(PersonIODao.this.date(row, "identity_date_p", context, false));

                    personNexOfKin.setEmploymentPlace(PersonIODao.this.trimmedString(row, "employee_place_p", context, employmentPlaceProperty));
                    personNexOfKin.setPhones(trimToEmpty((String) row.get("phones_p")));

                    {
                        final String address_id = (String) row.get("address_id");
                        final Long addressId = tryParseHexId(address_id);

                        if (addressId != null)
                        {
                            // Если id совпадает с уже существующим адресом, то оставляем существующий адрес
                            if (personNexOfKin.getAddress() != null && personNexOfKin.getAddress().getId().equals(addressId))
                                return;

                            final Map<String, Object> addressRow =  addressMap.get(address_id);
                            if (addressRow == null)
                            {
                                log4j_logger.error("Table address_t does not contain row with id=" + address_id + ", specified in column 'address_id' in table person_nextofkin_t in row with person_id=" + id);
                                return;
                            }

                            final String addressString = (String) addressRow.get("address_p");
                            if (!StringUtils.isEmpty(addressString))
                            {
                                AddressString newAddress = new AddressString();
                                newAddress.setAddress(addressString);
                                AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(personNexOfKin, newAddress, PersonNextOfKin.L_ADDRESS);
                            }
                        }
                    }

                    session.saveOrUpdate(personNexOfKin);

                } catch (final Exception t) {
                    logAndThrowPersonParseError(id, context, t, "Произошла ошибка обработки записи ближайшего родственника.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });
    }

    @SuppressWarnings("RedundantCast")
    @Override
    public Map<String, Long> doImport_PersonEduDocument(final Database mdb, final Map<String, Long> personIdMap) throws IOException
    {
        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            // сюда не должны попадать
            throw new IllegalStateException();
        }

        final String CACHE_KEY = "PersonIODao.doImport_PersonEduDocument";
        {
            final Map<String, Long> personEduDocumentIdMap = DaoCache.get(CACHE_KEY);
            if (null != personEduDocumentIdMap) {
                return personEduDocumentIdMap;
            }
        }

        final Table person_eduDocument_t = mdb.getTable(PERSON_EDU_DOCUMENT_MDB_TABLE);
        if (null == person_eduDocument_t) {
            throw new ApplicationException("Таблица «" + PERSON_EDU_DOCUMENT_MDB_TABLE + "» отсутствует в базе данных.");
        }

        final IAddressIODao addressIO = IAddressIODao.instance.get();
        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();

        final Map<String, EduDocumentKind> eduDocumentKindMap = catalogIO.catalogEduDocumentKind().lookup(true);
        final Map<String, EduLevel> eduLevelMap = catalogIO.catalogEduLevel().lookup(false);
        final Map<String, GraduationHonour> graduationHonourMap = catalogIO.catalogGraduationHonour().lookup(false);

        final Session session = this.getSession();

        // Мапа с документами. Сначала заполняем её идентификаторами из mdb, потом поднимаем сущности из базы по этим идентификаторам
        // Значения не null будут обновлены, на значения null будут созданы новые сущности
        final Map<Long, PersonEduDocument> documentMap = Maps.newHashMapWithExpectedSize(person_eduDocument_t.getRowCount());

        // Нам нужно смержить существующие документы персон с теми, что етсь в mdb.
        // Идентификаторы персон собираем, чтобы потом удалить те документы этих персон, которые отсутствуют в mdb
        final Set<Long> personIds = Sets.newHashSetWithExpectedSize(person_eduDocument_t.getRowCount());

        // Собираем идентификторы документов и персон
        for (Map<String, Object> row : person_eduDocument_t)
        {
            final String idStr = (String) row.get("id");
            final String personIdStr = (String) row.get("person_id");
            final String context = PERSON_EDU_DOCUMENT_MDB_TABLE + "[id=" + idStr + "]: ";
            final Long id = tryParseHexId(idStr);
            final Long personId = tryParseHexId(personIdStr);

            // Ну и сразу пачка проверок на корректность входящих данных
            final String err;
            if (id == null)
            {
                err = context + "field 'id' in table '" + PERSON_EDU_DOCUMENT_MDB_TABLE + "' mast be valid hex number and not null. Invalid value: " + idStr;
            }
            else if (documentMap.containsKey(id))
            {
                err = context + "not unique id.";
            }
            else if (!personIdMap.containsKey(personIdStr))
            {
                //  Если персона не найдена в базе, то импорт будет остановлен, в лог будет записана ошибка (уровень FATAL).
                err = context + "person with id=" + row.get("person_id") + " not found in mdb table 'person_t'.";
            }
            else if (personId == null)
            {
                err = context + "field 'person_id' must be valid hex number and not null.";
            }
            else
            {
                documentMap.put(id, null); // Собираем идентификаторы документов, чтобы потом их из базы поднять для мержа
                personIds.add(personIdMap.get(personIdStr)); // Собираем идентификаторы персон, чтобы попытаться стереть те их документы, которых нет в mdb
                continue;
            }

            throw new IOFatalException(err);
        }

        // Грузим идентификаторы документов упоминаемых в mdb персон и сразу отбираем те, которые нужно удалить (те, которых нет в mdb)
        final List<Long> docsToRemove = new ArrayList<>(personIds.size());
        BatchUtils.execute(personIds, DQL.MAX_VALUES_ROW_NUMBER, personIdsPart ->
            new DQLSelectBuilder().fromEntity(PersonEduDocument.class, "e")
                    .column(property("e.id"))
                    .where(in(property("e", PersonEduDocument.L_PERSON), personIdsPart))
                    .createStatement(session).<Long>list().stream()
                    .filter(id -> !documentMap.containsKey(id))
                    .forEach(docsToRemove::add)
        );

        final MutableInt count = new MutableInt();
        final MutableInt removedCount = new MutableInt();
        BatchUtils.execute(docsToRemove, DQL.MAX_VALUES_ROW_NUMBER, ids -> {

            count.add(ids.size());
            setProcessStateMessage("Документы об образовании. Удаление старых: " + count + "/" + docsToRemove.size());

            try {
                final int ret = new DQLDeleteBuilder(PersonEduDocument.class)
                        .where(in("id", ids))
                        //.where(new DQLNoReferenceExpressionBuilder(PersonEduDocument.class, "id").getExpression())
                        .createStatement(session).execute();
                removedCount.add(ret);

            } catch (CascadeDeleteViolationException e) {
                throw new IOFatalException("Can't delete persons education documents, which not presents in " + PERSON_EDU_DOCUMENT_MDB_TABLE + " - exists links for they.", e);
            }
        });
        log4j_logger.info("Removed " + removedCount + " old personEduDocument.");

        // Грузим из базы документы по идентификаторам из mdb, чтобы обновить в них данные, если id совпадают
        BatchUtils.execute(documentMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            final List<PersonEduDocument> docs = new DQLSelectBuilder().fromEntity(PersonEduDocument.class, "e", true)
                    .where(in("e.id", ids))
                    .createStatement(session).list();

            for (PersonEduDocument doc : docs) {
                documentMap.put(doc.getId(), doc);
            }
        });

        final IEntityMeta entityMeta = EntityRuntime.getMeta(PersonEduDocument.class);
        final IFieldPropertyMeta eduDocumentKindDetailedMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_DOCUMENT_KIND_DETAILED);
        final IFieldPropertyMeta seriaMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_SERIA);
        final IFieldPropertyMeta numberMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_NUMBER);
        final IFieldPropertyMeta eduOrganizationMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_EDU_ORGANIZATION);
        final IFieldPropertyMeta documentEducationLevelMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_DOCUMENT_EDUCATION_LEVEL);
        final IFieldPropertyMeta qualificationMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_QUALIFICATION);
        final IFieldPropertyMeta eduProgramSubjectMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_EDU_PROGRAM_SUBJECT);
        final IFieldPropertyMeta registrationNumberMeta = (IFieldPropertyMeta) entityMeta.getProperty(PersonEduDocument.P_REGISTRATION_NUMBER);

        final Map<String, Long> personEduDocumentIdMap = Maps.newHashMapWithExpectedSize(person_eduDocument_t.getRowCount());
        execute(person_eduDocument_t, DQL.MAX_VALUES_ROW_NUMBER, "Документы об образовании. Импорт новых, обновление существующих", rows -> {

            for (Map<String, Object> row : rows) {

                final String idStr = (String) row.get("id");
                final Long id = tryParseHexId(idStr);
                final Long personId = personIdMap.get((String) row.get("person_id"));
                final String context = PERSON_EDU_DOCUMENT_MDB_TABLE + "[id=" + idStr + "]: ";

                final AddressCountry country = addressIO.findCountry((String) row.get("edu_organization_country_p"));
                final AddressItem settlement = addressIO.findSettlement(country, (String) row.get("edu_organization_settlement_p"), true);
                if (settlement == null) {
                    log4j_logger.error(context + ":SKIP: edu_organization_settlement_p='" + row.get("edu_organization_settlement_p") + "' is not found.");
                    continue;
                }

                PersonEduDocument doc = documentMap.get(id);
                if (doc == null) {
                    doc = new PersonEduDocument();
                }
                doc.setPerson((Person) session.load(Person.class, personId));
                doc.setEduDocumentKind(PersonIODao.this.lookup(eduDocumentKindMap, row, "edu_document_kind", context));
                doc.setDocumentKindDetailed(PersonIODao.this.trimmedString(row, "edu_document_kind_detailed", context, eduDocumentKindDetailedMeta));
                doc.setSeria(PersonIODao.this.trimmedString(row, "seria", context, seriaMeta));
                doc.setNumber(PersonIODao.this.trimmedString(row, "number", context, numberMeta));
                doc.setEduOrganization(PersonIODao.this.trimmedString(row, "edu_organization", context, eduOrganizationMeta));
                doc.setEduOrganizationAddressItem(settlement);
                doc.setYearEnd(PersonIODao.this.integer(row, "year_end", context, true));
                doc.setIssuanceDate(PersonIODao.this.date(row, "issuance_date", context, false));
                doc.setEduLevel(PersonIODao.this.lookup(eduLevelMap, row, "edu_level", context));
                doc.setDocumentEducationLevel(PersonIODao.this.trimmedString(row, "document_education_level", context, documentEducationLevelMeta));
                doc.setQualification(PersonIODao.this.trimmedString(row, "qualification", context, qualificationMeta));
                doc.setEduProgramSubject(PersonIODao.this.trimmedString(row, "edu_program_subject", context, eduProgramSubjectMeta));
                doc.setGraduationHonour(PersonIODao.this.lookup(graduationHonourMap, row, "graduation_honour", context));
                doc.setRegistrationNumber(PersonIODao.this.trimmedString(row, "registration_number", context, registrationNumberMeta));

                try {
                    final String marks = StringUtils.trimToNull((String) row.get("marks"));
                    if (null != marks) {
                        final String[] m = marks.split(",");
                        doc.setMark5(Math.max(0, Integer.valueOf(m[0])));
                        doc.setMark4(Math.max(0, Integer.valueOf(m[1])));
                        doc.setMark3(Math.max(0, Integer.valueOf(m[2])));
                        doc.countAverageMark();
                    }
                } catch (final Exception t) {
                    log4j_logger.error(context + ": unable to parse value in the field 'marks', It has invalid format: '" + row.get("marks") + "'.");
                }

                session.saveOrUpdate(doc);
                personEduDocumentIdMap.put(idStr, doc.getId());
            }
            System.err.print(".");
            session.flush();
            session.clear();
        });

        DaoCache.put(CACHE_KEY, personEduDocumentIdMap);
        return personEduDocumentIdMap;
    }

    @SuppressWarnings("deprecation")
    protected void doImport_PersonEduInstitution(final Database mdb, final Map<String, Long> personIdMap, Map<String, Person> personMap) throws IOException
    {
        if (PersonEduDocumentManager.isShowNewEduDocuments()) {
            // сюда не должны попадать
            throw new IllegalStateException();
        }

        final Table person_eduinstitution_t = mdb.getTable("person_eduinstitution_t");
        if (null == person_eduinstitution_t) {
            throw new ApplicationException("Таблица «person_eduinstitution_t» отсутствует в базе данных.");
        }

        final IAddressIODao addressIO = IAddressIODao.instance.get();
        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();

        final Map<String, EducationalInstitutionTypeKind> educationalInstitutionTypeKindMap = catalogIO.catalogEducationalInstitutionTypeKind().lookup(true);
        final Map<String, EducationDocumentType> educationDocumentTypeMap = catalogIO.catalogEducationDocumentType().lookup(true);
        final Map<String, EducationLevelStage> educationLevelStageMap = catalogIO.catalogEducationLevelStage().lookup(true);
        final Map<String, EmployeeSpeciality> employeeSpecialityMap = catalogIO.catalogEmployeeSpeciality().lookup(false);
        final Map<String, DiplomaQualifications> diplomaQualificationsMap = catalogIO.catalogDiplomaQualifications().lookup(false);
        final Map<String, GraduationHonour> graduationHonourMap = catalogIO.catalogGraduationHonour().lookup(false);
        final Map<String, EduInstitution> eduInstitutionMap = catalogIO.import_catalogEducationalInstitution(mdb);

        final Session session = this.getSession();

        //для последующего определения Основного законченного учебного заведения персоны из данных таблицы person_t создаем мапу соответствия id персон и id их Основных законченных учебных заведений
        final Table person_t = mdb.getTable("person_t");
        final Map<String, String> mainPersonEduInstitutionIdMap = new HashMap<>();

        // заполнение промежуточных данных
        BatchUtils.execute(person_t, DQL.MAX_VALUES_ROW_NUMBER, rowList -> {
            for (final Map<String, Object> row : rowList)
            {
                final String personId = StringUtils.trimToNull((String) row.get("id"));
                final String eduInstitutionId = StringUtils.trimToNull((String) row.get("person_edu_institution"));
                mainPersonEduInstitutionIdMap.put(personId, eduInstitutionId);
            }
        });

        // удаление старых данных
        execute(person_eduinstitution_t, DQL.MAX_VALUES_ROW_NUMBER, "Образовательные учреждения персон (2/3)", rows -> {

            final Set<Long> personIds = new HashSet<>(rows.size());
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final Long personId = personIdMap.get(id);
                if (null != personId) {
                    personIds.add(personId);
                }
            }

            new DQLUpdateBuilder(Person.class)
                    .set(Person.personEduInstitution().s(), nul(PropertyType.ANY))
                    .where(in(property(Person.id()), personIds))
                    .createStatement(session).execute();

            new DQLDeleteBuilder(PersonEduInstitution.class)
                    .where(in(property(PersonEduInstitution.person().id()), personIds))
                    .createStatement(session).execute();
        });

        // загрузка новых
        execute(person_eduinstitution_t, 64, "Образовательные учреждения персон (3/3)", new BatchUtils.Action<Map<String, Object>>()
        {
            @SuppressWarnings("RedundantCast")
            @Override
            public void execute(final Collection<Map<String, Object>> rows) {

                //сохраняем данные что бы после цикла просетить в персоны их основные уч. заведения
                final Map<Long, PersonEduInstitution> personEduInstitutionMap = new LinkedHashMap<>();

                for (final Map<String, Object> row : rows)
                {
                    final String id = (String) row.get("person_id");
                    final String context = "person_eduinstitution_t[person_id=" + id + "]";

                    try
                    {
                        if (StringUtils.isBlank(id)) {
                            throw new IllegalStateException("Field 'person_id' must not be null.");
                        }

                        final Person person = personMap.get(id);
                        final PersonEduInstitution personEduInstitution = new PersonEduInstitution(person);

                        personEduInstitution.setEduInstitutionKind(educationalInstitutionTypeKindMap.get((String) row.get("educational_institution_type_kind")));
                        personEduInstitution.setDocumentType(educationDocumentTypeMap.get((String) row.get("education_document_type")));

                        personEduInstitution.setEducationLevelStage(PersonIODao.this.lookup(educationLevelStageMap, row, "education_level_stage", context));
                        personEduInstitution.setEmployeeSpeciality(PersonIODao.this.lookup(employeeSpecialityMap, row, "employee_speciality", context));
                        personEduInstitution.setDiplomaQualification(PersonIODao.this.lookup(diplomaQualificationsMap, row, "diploma_qualification", context));

                        personEduInstitution.setQualification(trimToEmpty((String) row.get("qualification")));
                        personEduInstitution.setSeria(trimToEmpty((String) row.get("seria")));
                        personEduInstitution.setNumber(trimToEmpty((String) row.get("number")));
                        personEduInstitution.setRegionCode(trimToEmpty((String) row.get("region_code")));

                        final AddressCountry country = addressIO.findCountry((String) row.get("educational_institution_country_p"));
                        final AddressItem settlement = addressIO.findSettlement(country, (String) row.get("educational_institution_settlement_p"), true);
                        if (null == settlement) {
                            log4j_logger.error(context + ":SKIP: educational_institution_settlement_p='" + row.get("educational_institution_settlement_p") + "' is not found.");
                            continue; // не можем мы такое сохранить
                        }
                        personEduInstitution.setAddressItem(settlement);

                        personEduInstitution.setGraduationHonour(PersonIODao.this.lookup(graduationHonourMap, row, "graduation_honour", context));
                        personEduInstitution.setYearEnd(integer(row, "year_end", context, true));
                        String eduInstitutionId = StringUtils.trimToNull((String) row.get("edu_institution_id"));

                        if (eduInstitutionId != null) {

                            EduInstitution eduInstitution = eduInstitutionMap.get(eduInstitutionId);
                            if (eduInstitution == null) {
                                throw new IOFatalException("No value with id='" + eduInstitutionId + "' found in catalog 'eduinstitution_t'.");
                            }

                            try {
                                parseAndFillEduInstitution(personEduInstitution, eduInstitution);
                            } catch (IllegalStateException e) {
                                log4j_logger.error(context + ": " + e.getMessage());
                                continue;
                            }
                        }

                        personEduInstitution.setIssuanceDate(PersonIODao.this.date(row, "date", context, false));

                        try
                        {
                            final String marks = StringUtils.trimToNull((String) row.get("marks"));
                            if (null != marks) {
                                final String[] m = marks.split(",");
                                personEduInstitution.setMark5(Math.max(0, Integer.valueOf(m[0])));
                                personEduInstitution.setMark4(Math.max(0, Integer.valueOf(m[1])));
                                personEduInstitution.setMark3(Math.max(0, Integer.valueOf(m[2])));
                            }
                        } catch (final Exception t) {
                            log4j_logger.error(context + ": unable to parse value in the field 'marks' is not the valid format: '" + row.get("marks") + "'.");
                        }

                        session.saveOrUpdate(personEduInstitution);

                        {
                            //по id персоны узнаем id ее Основного законченного учебного заведения и если это оно, то запоминаем это
                            String mainEduInstId = mainPersonEduInstitutionIdMap.get(id);
                            String eduInstId = StringUtils.trimToNull((String) row.get("id"));
                            if (eduInstId == null) {
                                throw new IllegalStateException("Field 'id' must not be null.");
                            }

                            if (mainEduInstId != null && mainEduInstId.equals(eduInstId)) {
                                personEduInstitutionMap.put(person.getId(), personEduInstitution);
                            }
                        }
                    } catch (final Exception t) {
                        logAndThrowPersonParseError(id, context, t, "Произошла ошибка обработки записи о полученном образовании");
                    }
                }

                session.flush();
                for (Map.Entry<Long, PersonEduInstitution> entry : personEduInstitutionMap.entrySet()) {
                    new DQLUpdateBuilder(Person.class)
                            .set(Person.L_PERSON_EDU_INSTITUTION, value(entry.getValue()))
                            .where(eq(property(Person.P_ID), value(entry.getKey())))
                            .createStatement(session).execute();
                }

                System.err.print(".");
                session.flush();
                session.clear();
            }

            public void parseAndFillEduInstitution(PersonEduInstitution personEduInstitution, EduInstitution eduInstitution) {
                if (personEduInstitution.getAddressItem().getCountry().getCode() == eduInstitution.getAddress().getSettlement().getCountry().getCode()
                        && personEduInstitution.getAddressItem().getCode().equals(eduInstitution.getAddress().getSettlement().getCode()))
                {
                    personEduInstitution.setEduInstitution(eduInstitution);

                } else {
                    throw new IllegalStateException("SKIP: settlement or country in person_eduinstitution_t is not valid.");
                }
            }
        });
    }

    private void logAndThrowPersonParseError(Object id, String context, Exception t) {
        logAndThrowPersonParseError(id, context, t, "Произошла ошибка при обработке записи.");
    }

    private void logAndThrowPersonParseError(Object id, String context, Exception t, String error) {
        log4j_logger.fatal("Error with processing: " + context);
        log4j_logger.fatal(t.getMessage(), t);
        throw new ApplicationException("Персона «" + id + "»: " + error + ".", t);
    }

}
