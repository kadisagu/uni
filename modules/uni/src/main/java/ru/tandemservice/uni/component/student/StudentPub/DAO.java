/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentPub;

import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Calendar;
/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setOrderData(get(OrderData.class, OrderData.L_STUDENT, model.getStudent()));
        model.setContractStudent(UniDefines.COMPENSATION_TYPE_CONTRACT.equals(model.getStudent().getCompensationType().getCode()));
        model.setStudentDocumentTypeList(getList(StudentDocumentType.class, StudentDocumentType.P_ACTIVE, Boolean.TRUE));

        EducationLevelsHighSchool educationLevelHighSchool = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool();

        model.setSpeciality(educationLevelHighSchool.getEducationLevel().getProgramSubjectWithCodeIndexAndGenTitle());
        model.setSpecialization(educationLevelHighSchool.getEducationLevel().getProgramSpecializationTitle());

        model.setStudentCustomStates(UniStudentManger.instance().studentCustomStateDAO().getActiveStatesList(model.getStudent().getId()));
    }

    @Override
    public void setArchival(Model model, boolean archival)
    {
        Session session = getSession();
        Student student = refresh(model.getStudent());
        if (archival)
        {
            if (student.getStatus().isActive())
            {
                throw new ApplicationException("Студент " + student.getPerson().getFullFio() + " числится в активном состоянии. Списать в архив студента в активном состоянии нельзя.");
            }

            Calendar calendar = Calendar.getInstance();
            student.setArchivingDate(calendar.getTime());
        } else
        {
            if (!ru.tandemservice.uni.component.student.StudentEdit.DAO.checkStudentBookNumber(student, session))
            {
                throw new ApplicationException("Нельзя восстановить студента из архива. Номер зачетной книжки студента совпадает с одним из номеров обучаемых (активных) студентов.");
            }
            student.setPersonalArchivalFileNumber(null);
            student.setDescription(null);
        }
        student.setArchival(archival);
        update(student);
    }

    @Override
    public void updateStudentDocumentDataSource(Model model)
    {
        DynamicListDataSource<StudentDocument> dataSource = model.getDocumentDataSource();
        MQBuilder builder = new MQBuilder(StudentDocument.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", StudentDocument.L_STUDENT, model.getStudent()));
        StudentDocumentType studentDocumentType = model.getSettings().get("studentDocumentType");
        if (studentDocumentType != null)
            builder.add(MQExpression.eq("s", StudentDocument.L_STUDENT_DOCUMENT_TYPE, studentDocumentType));
        builder.addOrder("s", StudentDocument.formingDate().s(), OrderDirection.desc);
        new OrderDescriptionRegistry("s").applyOrder(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}