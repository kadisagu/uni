/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni;

import org.tandemframework.shared.cxfws.ws.PasswordCallback;

/**
 * @author agolubenko
 * @since 18.09.2009
 */
public interface UniProperties
{
    // ws
    String WEB_SERVICE_PASSWORD_PROPERTY = PasswordCallback.WEB_SERVICE_PASSWORD_PROPERTY;

    // smtp
    String SMTP_HOST_PROPERTY = "server.smtp.host";
    String SMTP_FROM_PROPERTY = "server.smtp.from";
    String SMTP_NAME_PROPERTY = "server.smtp.name";
    String SMTP_PASS_PROPERTY = "server.smtp.pass";

    // sakai
    String SAKAI_HOST = "sakai.host";
    String SAKAI_LOGIN = "sakai.login";
    String SAKAI_PASSWORD = "sakai.password";
}
