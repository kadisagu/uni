/**
 * $Id$
 */
package ru.tandemservice.uni.dao.group;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 31.03.2010
 */
public class GroupDAO extends UniBaseDao implements IGroupDAO
{
    @Override
    public boolean isNonArchivalGroupExists(Group group, String groupTitle)
    {
        MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");
        builder.add(MQExpression.eq("g", Group.P_TITLE, groupTitle));
        builder.add(MQExpression.eq("g", Group.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, group.getEducationOrgUnit().getFormativeOrgUnit()));
        builder.add(MQExpression.notEq("g", Group.P_ID, group.getId()));
        return builder.getResultCount(getSession()) > 0;
    }

    private DQLSelectBuilder createGroupCaptainStudentBuilder(Group group, Student student)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(GroupCaptainStudent.class, "s");

        if (null == group)
            return builder.where(nothing());

        if (null != student)
        {
            builder.where(eq(property(GroupCaptainStudent.student().fromAlias("s")), value(student)));
        }

        builder.where(eq(property(GroupCaptainStudent.group().fromAlias("s")), value(group)));

        return builder;
    }

    private DQLSelectBuilder createGroupCaptainStudentBuilder(Group group)
    {
        return createGroupCaptainStudentBuilder(group, null);
    }

    @Override
    public boolean isCaptainStudent(Group group, Student student)
    {
        return existsEntity(createGroupCaptainStudentBuilder(group, student).buildQuery());
    }

    @Override
    public List<Student> getCaptainStudentList(Group group)
    {
        DQLSelectBuilder builder = createGroupCaptainStudentBuilder(group).column(property(GroupCaptainStudent.student().fromAlias("s")));
        return builder.createStatement(getSession()).list();
    }

    @Override
    public void deleteCaptainStudentList(Group group)
    {
        List<GroupCaptainStudent> captainList = createGroupCaptainStudentBuilder(group).createStatement(getSession()).list();
        for (GroupCaptainStudent groupCaptainStudent : captainList)
        {
            delete(groupCaptainStudent);
        }
    }

    @Override
    public void deleteCaptainStudent(Group group, Student student)
    {
        GroupCaptainStudent captain = createGroupCaptainStudentBuilder(group, student).createStatement(getSession()).uniqueResult();
        if (null != captain)
        {
            delete(captain);
        }
    }

    @Override
    public void addCaptainStudent(Group group, Student student)
    {
        if (null != group && null != student)
        {
            GroupCaptainStudent groupCaptainStudent = new GroupCaptainStudent();
            groupCaptainStudent.setGroup(group);
            groupCaptainStudent.setStudent(student);
            saveOrUpdate(groupCaptainStudent);
        }
    }
}