package ru.tandemservice.uni.entity.employee.pps;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author Alexey Lopatin
 * @since 20.06.2016
 */
public interface IPpsEntry
{
    /** @return Учебный год */
    EducationYear getEduYear();

    /** @return суммарное число часов для всех почасовиков данной записи ППС */
    long getTimeAmountAsLong();
}
