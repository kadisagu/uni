/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.documents.DocumentAddBase;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 16.10.2009
 */
public abstract class DocumentAddBaseDAO<Model extends DocumentAddBaseModel> extends UniDao<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        model.setStudentDocumentType(getNotNull(StudentDocumentType.class, model.getStudentDocumentType().getId()));

        Criteria criteria = getCheckCriteria(new Date(), model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
        criteria.setProjection(Projections.max(StudentDocument.P_NUMBER));
        Number number = (Number) criteria.uniqueResult();
        int max = number == null ? 0 : number.intValue();
        model.setNumber(max + 1);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        Session session = getSession();

        // validate
        Criteria criteria = getCheckCriteria(new Date(), model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());
        criteria.add(Restrictions.eq(StudentDocument.P_NUMBER, model.getNumber()));
        List list = criteria.list();

        if (!list.isEmpty())
        {
            IdentityCard card = ((StudentDocument) list.get(0)).getStudent().getPerson().getIdentityCard();
            String sexTitle = SexCodes.MALE.equals(card.getSex().getCode()) ? "студента" : "студентки";
            ContextLocal.getErrorCollector().add("Номер документа должен быть уникальным. Под номером «" + model.getNumber() + "» уже выдан другой документ для " + sexTitle + " «" + card.getFullFio() + "».", "number");
            return;
        }


        final UniScriptItem script =  model.getStudentDocumentType().getScriptItem();


        // create print document
        final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(script, model);
        byte[] document = (byte[])scriptResult.get("document");

        // create database file
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(document);
        session.save(databaseFile);

        // create student document
        StudentDocument studentDocument = new StudentDocument();
        studentDocument.setNumber(model.getNumber());
        studentDocument.setFormingDate(new Date());
        studentDocument.setStudent(model.getStudent());
        studentDocument.setContent(databaseFile);
        studentDocument.setStudentDocumentType(model.getStudentDocumentType());

        getSession().saveOrUpdate(studentDocument);
    }

    private Criteria getCheckCriteria(Date date, OrgUnit formativeOrgUnit)
    {
        int year = CoreDateUtils.getYear(date);
        Criteria c = getSession().createCriteria(StudentDocument.class, "d");
        c.createAlias("d." + StudentDocument.L_STUDENT, "s");
        c.createAlias("s." + Student.L_EDUCATION_ORG_UNIT, "e");
        c.add(Restrictions.ge("d." + StudentDocument.P_FORMING_DATE, CoreDateUtils.getYearFirstTimeMoment(year)));
        c.add(Restrictions.lt("d." + StudentDocument.P_FORMING_DATE, CoreDateUtils.getYearFirstTimeMoment(year + 1)));
        c.add(Restrictions.eq("e." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
        return c;
    }
}
