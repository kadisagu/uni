package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Курс
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CourseGen extends EntityBase
 implements INaturalIdentifiable<CourseGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.Course";
    public static final String ENTITY_NAME = "course";
    public static final int VERSION_HASH = -2099740404;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_INT_VALUE = "intValue";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _intValue;     // Номер
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getIntValue()
    {
        return _intValue;
    }

    /**
     * @param intValue Номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setIntValue(int intValue)
    {
        dirty(_intValue, intValue);
        _intValue = intValue;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CourseGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Course)another).getCode());
            }
            setIntValue(((Course)another).getIntValue());
            setTitle(((Course)another).getTitle());
        }
    }

    public INaturalId<CourseGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<CourseGen>
    {
        private static final String PROXY_NAME = "CourseNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof CourseGen.NaturalId) ) return false;

            CourseGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Course.class;
        }

        public T newInstance()
        {
            return (T) new Course();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "intValue":
                    return obj.getIntValue();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "intValue":
                    obj.setIntValue((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "intValue":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "intValue":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "intValue":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Course> _dslPath = new Path<Course>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Course");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.Course#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.Course#getIntValue()
     */
    public static PropertyPath<Integer> intValue()
    {
        return _dslPath.intValue();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.Course#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Course> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _intValue;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.Course#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(CourseGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.Course#getIntValue()
     */
        public PropertyPath<Integer> intValue()
        {
            if(_intValue == null )
                _intValue = new PropertyPath<Integer>(CourseGen.P_INT_VALUE, this);
            return _intValue;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.Course#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(CourseGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Course.class;
        }

        public String getEntityName()
        {
            return "course";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
