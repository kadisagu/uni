/* $Id:$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author rsizonenko
 * @since 17.12.2014
 */

public class MS_uni_2x7x1_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        {
            tool.executeUpdate("delete from uni_c_script_t where id = (select id from scriptitem_t where code_p = 'studentPersonalCardPrintScript' and catalogcode_p='unienrScriptItem')");
            tool.executeUpdate("delete from scriptitem_t where code_p = 'studentPersonalCardPrintScript' and catalogcode_p='unienrScriptItem'");
        }
    }
}
