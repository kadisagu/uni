package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид подразделения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitKindGen extends EntityBase
 implements INaturalIdentifiable<OrgUnitKindGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.OrgUnitKind";
    public static final String ENTITY_NAME = "orgUnitKind";
    public static final int VERSION_HASH = -1587671803;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ALLOW_STUDENTS = "allowStudents";
    public static final String P_ALLOW_GROUPS = "allowGroups";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _allowStudents;     // Набор студентов
    private boolean _allowGroups;     // Отображать перечень групп
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Набор студентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowStudents()
    {
        return _allowStudents;
    }

    /**
     * @param allowStudents Набор студентов. Свойство не может быть null.
     */
    public void setAllowStudents(boolean allowStudents)
    {
        dirty(_allowStudents, allowStudents);
        _allowStudents = allowStudents;
    }

    /**
     * @return Отображать перечень групп. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowGroups()
    {
        return _allowGroups;
    }

    /**
     * @param allowGroups Отображать перечень групп. Свойство не может быть null.
     */
    public void setAllowGroups(boolean allowGroups)
    {
        dirty(_allowGroups, allowGroups);
        _allowGroups = allowGroups;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitKindGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((OrgUnitKind)another).getCode());
            }
            setAllowStudents(((OrgUnitKind)another).isAllowStudents());
            setAllowGroups(((OrgUnitKind)another).isAllowGroups());
            setTitle(((OrgUnitKind)another).getTitle());
        }
    }

    public INaturalId<OrgUnitKindGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<OrgUnitKindGen>
    {
        private static final String PROXY_NAME = "OrgUnitKindNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OrgUnitKindGen.NaturalId) ) return false;

            OrgUnitKindGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitKind.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "allowStudents":
                    return obj.isAllowStudents();
                case "allowGroups":
                    return obj.isAllowGroups();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "allowStudents":
                    obj.setAllowStudents((Boolean) value);
                    return;
                case "allowGroups":
                    obj.setAllowGroups((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "allowStudents":
                        return true;
                case "allowGroups":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "allowStudents":
                    return true;
                case "allowGroups":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "allowStudents":
                    return Boolean.class;
                case "allowGroups":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitKind> _dslPath = new Path<OrgUnitKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitKind");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Набор студентов. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#isAllowStudents()
     */
    public static PropertyPath<Boolean> allowStudents()
    {
        return _dslPath.allowStudents();
    }

    /**
     * @return Отображать перечень групп. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#isAllowGroups()
     */
    public static PropertyPath<Boolean> allowGroups()
    {
        return _dslPath.allowGroups();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends OrgUnitKind> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _allowStudents;
        private PropertyPath<Boolean> _allowGroups;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(OrgUnitKindGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Набор студентов. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#isAllowStudents()
     */
        public PropertyPath<Boolean> allowStudents()
        {
            if(_allowStudents == null )
                _allowStudents = new PropertyPath<Boolean>(OrgUnitKindGen.P_ALLOW_STUDENTS, this);
            return _allowStudents;
        }

    /**
     * @return Отображать перечень групп. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#isAllowGroups()
     */
        public PropertyPath<Boolean> allowGroups()
        {
            if(_allowGroups == null )
                _allowGroups = new PropertyPath<Boolean>(OrgUnitKindGen.P_ALLOW_GROUPS, this);
            return _allowGroups;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.OrgUnitKind#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(OrgUnitKindGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return OrgUnitKind.class;
        }

        public String getEntityName()
        {
            return "orgUnitKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
