/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.base.bo.UniOrgUnit.ui.AdditionalEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Vasily Zhukov
 * @since 26.03.2012
 */
@Configuration
public class UniOrgUnitAdditionalEdit extends BusinessComponentManager
{
}
