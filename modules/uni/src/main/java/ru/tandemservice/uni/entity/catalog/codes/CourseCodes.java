package ru.tandemservice.uni.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Курс"
 * Имя сущности : course
 * Файл data.xml : catalogs.data.xml
 */
public interface CourseCodes
{
    /** Константа кода (code) элемента : 1 (title) */
    String COURSE_1 = "1";
    /** Константа кода (code) элемента : 2 (title) */
    String COURSE_2 = "2";
    /** Константа кода (code) элемента : 3 (title) */
    String COURSE_3 = "3";
    /** Константа кода (code) элемента : 4 (title) */
    String COURSE_4 = "4";
    /** Константа кода (code) элемента : 5 (title) */
    String COURSE_5 = "5";
    /** Константа кода (code) элемента : 6 (title) */
    String COURSE_6 = "6";
    /** Константа кода (code) элемента : 7 (title) */
    String COURSE_7 = "7";

    Set<String> CODES = ImmutableSet.of(COURSE_1, COURSE_2, COURSE_3, COURSE_4, COURSE_5, COURSE_6, COURSE_7);
}
