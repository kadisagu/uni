// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.EduOuOrgUnitsList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author oleyba
 * @since 03.12.2010
 */
public class Model
{
    DynamicListDataSource<EducationOrgUnit> dataSource;
    private IDataSettings _settings;

    ISelectModel displayOptionList;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _producingOrgUnitListModel;
    private ISelectModel _educationLevelsModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developPeriodListModel;
    private ISelectModel _developConditionListModel;

    public DynamicListDataSource<EducationOrgUnit> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<EducationOrgUnit> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getDisplayOptionList()
    {
        return displayOptionList;
    }

    public void setDisplayOptionList(ISelectModel displayOptionList)
    {
        this.displayOptionList = displayOptionList;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getProducingOrgUnitListModel()
    {
        return _producingOrgUnitListModel;
    }

    public void setProducingOrgUnitListModel(ISelectModel producingOrgUnitListModel)
    {
        _producingOrgUnitListModel = producingOrgUnitListModel;
    }

    public ISelectModel getEducationLevelsModel()
    {
        return _educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel)
    {
        _educationLevelsModel = educationLevelsModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }
}
