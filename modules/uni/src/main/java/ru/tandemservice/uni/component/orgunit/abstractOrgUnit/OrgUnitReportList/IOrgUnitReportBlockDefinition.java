/* $Id$ */
package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author oleyba
 * @since 12/16/11
 * @deprecated
 * @see org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition
 */

@Deprecated
public interface IOrgUnitReportBlockDefinition
{
    /**
     * @return название блока
     */
    String getBlockTitle();

    /**
     * для генерации permissionKey
     * @return префикс
     */
    String getPermissionPrefix();

    /**
     * @param orgUnit текущее подразделении
     * @return true, если на orgUnit требуется отображать этот блок отчетов
     */
    boolean isVisible(OrgUnit orgUnit);
}
