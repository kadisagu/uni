// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui.selectModel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;

import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;

/**
 * @author oleyba
 * @since 2/16/11
 */
public class PpsEntryModel extends DQLFullCheckSelectModel implements ISingleSelectModel, IMultiSelectModel
{
    {
        setColumnTitles(new String[] {"ФИО", "Подразделение", "Данные преподавателя"});
        setLabelProperties(new String[] {PpsEntry.person().fio().s(), PpsEntry.orgUnit().shortTitle().s(), PpsEntry.titleInfo().s()});
    }

    @Override
    protected DQLSelectBuilder query(String alias, String filter)
    {
        if (returnEmpty()) { return null; }

        DQLSelectBuilder ppsDQL = new DQLSelectBuilder().fromEntity(PpsEntry.class, alias);

        ppsDQL.fetchPath(DQLJoinType.left, PpsEntry.orgUnit().fromAlias(alias), "orgUnit");
        ppsDQL.fetchPath(DQLJoinType.inner, PpsEntry.person().fromAlias(alias), "person");
        ppsDQL.fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");

        ppsDQL.where(isNull(property(PpsEntry.removalDate().fromAlias(alias))));

        if (StringUtils.isNotBlank(filter))
        {
            FilterUtils.applyLikeFilter(ppsDQL, filter, IdentityCard.fullFio().fromAlias("idc"), OrgUnit.title().fromAlias("orgUnit"));
        }

        ppsDQL.order(property(IdentityCard.lastName().fromAlias("idc")));
        ppsDQL.order(property(IdentityCard.firstName().fromAlias("idc")));
        ppsDQL.order(property(IdentityCard.middleName().fromAlias("idc")));

        return filter(alias, ppsDQL);
    }

    protected DQLSelectBuilder filter(String alias, DQLSelectBuilder ppsDQL)
    {
        return ppsDQL;
    }

    protected boolean returnEmpty()
    {
        return false;
    }
}
