/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.rtf;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.header.RtfHeadline;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.*;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.*;

/**
 * TODO после часотого использования этого метода перенести его в платформу
 *
 * @author vip_delete
 * @since 18.02.2009
 */
public class UniRtfUtil
{
    public static final List<String> CASE_POSTFIX = Arrays.asList("", "_G", "_D", "_A", "_I", "_P");
    public static final String[] SPECIALITY_CASES = new String[]{"специальность", "специальности", "специальности", "специальность", "специальностью", "специальности"};
    public static final String[] EDU_DIRECTION_CASES = new String[]{"направление подготовки", "направления подготовки", "направлению подготовки", "направление подготовки", "направлением подготовки", "направлении подготовки"};
    public static final String[] EDU_PROFESSION_CASES = new String[]{"профессия", "профессии", "профессии", "профессию", "профессией", "профессии"};

    /**
     * Пока оставить. Может использоваться в пользовательских скриптах.
     *
     * @deprecated use {@link org.tandemframework.rtf.SharedRtfUtil#hasFields(List, Set)}
     */
    public static boolean hasFields(List<IRtfElement> elementList, Set<String> fieldNames)
    {
        return SharedRtfUtil.hasFields(elementList, fieldNames);
    }

    /**
     * Пока оставить. Может использоваться в пользовательских скриптах.
     *
     * @deprecated use {@link org.tandemframework.rtf.SharedRtfUtil#findRtfTableIndex(List, String)}
     */
    public static Integer findRtfTableIndex(List<IRtfElement> elementList, String labelName)
    {
        final Set<String> labelSet = ImmutableSet.of(labelName);
        for (int i = 0; i < elementList.size(); i++)
        {
            IRtfElement element = elementList.get(i);
            if (element instanceof RtfTable)
            {
                final RtfTable table = (RtfTable) element;
                for (RtfRow row : table.getRowList())
                {
                    for (RtfCell cell : row.getCellList())
                    {
                        if (SharedRtfUtil.hasFields(cell.getElementList(), labelSet)) {
                            return i;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Ищет ключевое слово в документе
     *
     * @param document документ
     * @param name     ключевое слово
     * @return результат поиска
     */
    public static RtfSearchResult findRtfMark(RtfDocument document, String name)
    {
        List<IRtfElement> elementList;
        int index;
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfParagraph)
            {
                index = 0;
                elementList = ((RtfParagraph) element).getElementList();
                while (index < elementList.size() && !(elementList.get(index) instanceof RtfField && name.equals(((RtfField) elementList.get(index)).getFieldName())))
                    index++;
                if (index < elementList.size())
                    return new RtfSearchResult(true, elementList, index);
            }
        }
        return new RtfSearchResult(false, null, 0);
    }

    /**
     * Ищет в документе таблицу, с указанной меткой.
     *
     * @param document документ
     * @param name     имя метки
     * @return <code>RtfSearchResult</code>, с содержащим таблицу списком элементов и индексом таблицы в этом списке.
     */
    public static RtfSearchResult findRtfTableMark(final RtfDocument document, String name)
    {
        List<IRtfElement> resultTableElementList = null;
        int resultIndex = 0;
        boolean resultFound = false;
        List<List<IRtfElement>> sourceList = new ArrayList<>();
        List<List<IRtfElement>> newSourceList = new ArrayList<>();
        sourceList.add(document.getElementList());
        while (!sourceList.isEmpty() && !resultFound)
        {
            for (List<IRtfElement> elementList : sourceList)
            {
                for (int i = 0; i < elementList.size(); i++)
                {
                    IRtfElement element = elementList.get(i);
                    if (element instanceof IRtfGroup)
                        newSourceList.add(((IRtfGroup) element).getElementList());
                    if (element instanceof RtfTable)
                    {
                        RtfTable table = (RtfTable) element;
                        for (int currentRowIndex = 0; currentRowIndex < table.getRowList().size(); currentRowIndex++)
                        {
                            RtfRow currentRow = table.getRowList().get(currentRowIndex);
                            String rowName = getRowName(currentRow);
                            if (name.equals(rowName))
                            {
                                resultTableElementList = elementList;
                                resultIndex = i;
                                resultFound = true;
                            }
                        }
                    }
                }
            }
            if (!resultFound)
            {
                sourceList.clear();
                sourceList.addAll(newSourceList);
                newSourceList.clear();
            }
        }

        return new RtfSearchResult(resultFound, resultTableElementList, resultIndex);
    }

    /**
     * Ищет "имя" строки - метку в первой ячейке.
     *
     * @param row строка таблицы
     * @return имя метки
     */
    public static String getRowName(final RtfRow row)
    {
        List<RtfCell> cellList = row.getCellList();

        if (cellList.size() == 0)
            throw new RuntimeException("Invalid document: Row has no cells!");

        for (IRtfElement element : cellList.get(0).getElementList())
            if (element instanceof RtfField)
            {
                RtfField field = (RtfField) element;
                if (field.isMark()) return field.getFieldName();
            }
        return null;
    }

    public static void removeParagraphsByNamesFromAnywhere(RtfDocument document, List<String> fieldNames, boolean delParagraphBefore, boolean delParagraphAfter)
    {
        if (null == fieldNames || fieldNames.isEmpty()) return;
        removeParagraphsByNamesFromRtfElement(document, fieldNames, delParagraphBefore, delParagraphAfter);
    }

    private static void removeParagraphsByNamesFromRtfElement(IRtfElement parentElement, List<String> fieldNames, boolean delParagraphBefore, boolean delParagraphAfter)
    {
        if (null == fieldNames || fieldNames.isEmpty()) return;

        if (parentElement instanceof RtfTable)
        {
            for (RtfRow row : ((RtfTable) parentElement).getRowList())
                for (RtfCell cell : row.getCellList())
                    removeParagraphsFromElementsList(cell.getElementList(), fieldNames, delParagraphBefore, delParagraphAfter);
        }
        else if (parentElement instanceof RtfGroup)
        {
            removeParagraphsFromElementsList(((RtfGroup) parentElement).getElementList(), fieldNames, delParagraphBefore, delParagraphAfter);
            for (IRtfElement element : ((RtfGroup) parentElement).getElementList())
                removeParagraphsByNamesFromRtfElement(element, fieldNames, delParagraphBefore, delParagraphAfter);
        }
    }

    /**
     * @deprecated use
     *             1. RtfTableModifier.put("T", (String[][]) null) to remove one table row
     *             2. RtfTableModifier.remove("T") to remove whole table
     *             3. RtfTableModifier.remove("T", (int) removeBefore, (int) removeAfter) to remove whole table and some paragraphs before and after the table
     */
    @Deprecated
    public static void removeTableByName(RtfDocument document, String tableName, boolean delParagraphBefore, boolean delParagraphAfter)
    {
        List<IRtfElement> elementsToDel = new ArrayList<>();
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfTable)
            {
                for (RtfRow row : ((RtfTable) element).getRowList())
                {
                    for (RtfCell cell : row.getCellList())
                    {
                        for (IRtfElement cellElem : cell.getElementList())
                        {
                            if (cellElem instanceof RtfField)
                            {
                                RtfField field = (RtfField) cellElem;
                                if (field.isMark() && tableName.equals(field.getFieldName()))
                                {
                                    elementsToDel.add(element);
                                    int index = document.getElementList().indexOf(element);
                                    if (delParagraphBefore) elementsToDel.add(document.getElementList().get(index - 1));
                                    if (delParagraphAfter) elementsToDel.add(document.getElementList().get(index + 1));
                                }
                            }
                        }
                    }
                }
            }
        }

        document.getElementList().removeAll(elementsToDel);
    }

    /**
     * Делает строку таблицы жирным шрифтом
     *
     * @param row строка таблицы
     * @deprecated method does not work properly, use IRtfRowIntercepter.beforeInject
     */
    @Deprecated
    public static void setRowBold(RtfRow row)
    {
        IRtfControl boldStart = RtfBean.getElementFactory().createRtfControl(IRtfData.B);
        IRtfControl boldEnd = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
//        boldEnd.setValue(0);

        List<RtfCell> cells = row.getCellList();
        for (RtfCell cell : cells)
            replaceBoldEnd(cell.getElementList());
        cells.get(0).getElementList().add(0, boldStart);
        UniBaseUtils.getLast(cells).getElementList().add(boldEnd);
    }

    /**
     * Делает ячейку таблицы жирным шрифтом
     *
     * @param cell ячейка таблицы
     * @deprecated method does not work properly, use IRtfRowIntercepter.beforeInject
     */
    @Deprecated
    public static void setCellBold(RtfCell cell)
    {
        IRtfControl boldStart = RtfBean.getElementFactory().createRtfControl(IRtfData.B);
        IRtfControl boldEnd = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);

        replaceBoldEnd(cell.getElementList());

        cell.getElementList().add(0, boldStart);
        cell.getElementList().add(boldEnd);
    }

    /**
     * Делает ячейку таблицы курсивом
     *
     * @param cell ячейка таблицы
     * @deprecated method does not work properly, use IRtfRowIntercepter.beforeInject
     */
    @Deprecated
    public static void setCellItalic(RtfCell cell)
    {
        IRtfControl iStart = RtfBean.getElementFactory().createRtfControl(IRtfData.I);
        IRtfControl iEnd = RtfBean.getElementFactory().createRtfControl(IRtfData.I, 0);

        List<IRtfElement> elementList = cell.getElementList();
        List<Integer> italicElementIndexList = new ArrayList<>();
        for (int i = 0; i < elementList.size(); i++)
        {
            IRtfElement element = elementList.get(i);
            if (element instanceof IRtfControl && ((IRtfControl) element).getIndex() == IRtfData.I)
            {
                italicElementIndexList.add(i);
            }
        }
        for (Integer index : italicElementIndexList)
        {
            elementList.set(index, RtfBean.getElementFactory().createRtfControl(IRtfData.I, 1));
        }

        List<IRtfElement> tmp = new ArrayList<>(elementList);
        for (IRtfElement element : tmp)
            if (element instanceof IRtfControl && ((IRtfControl) element).getIndex() == IRtfData.PLAIN)
                elementList.remove(element);

        elementList.add(0, iStart);
        elementList.add(iEnd);
    }

    public static void initEducationType(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool, String postfix)
    {
        final String label = "educationType" + postfix;
        final String[] types = educationLevelsHighSchool.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? SPECIALITY_CASES : EDU_DIRECTION_CASES;
        for (int i = 0; i < CASE_POSTFIX.size(); i++)
        {
            modifier.put(label + CASE_POSTFIX.get(i), types[i]);
        }
    }

    /**
     * Записывает данные в таблицу
     *
     * @param table таблица
     * @param value данные, которые надо записать в таблицу
     */
    public static void modify(RtfTable table, String[][] value)
    {
        modify(table, null, value);
    }

    /**
     * Записывает данные в таблицу (скопировано с небольшими изменениями из {@link org.tandemframework.rtf.modifiers.RtfTableModifier})
     *
     * @param table       таблица
     * @param intercepter перехватчик события до и после записи данных
     * @param value       данные, которые надо записать в таблицу
     */
    public static void modify(RtfTable table, IRtfRowIntercepter intercepter, String[][] value)
    {
        List<RtfRow> rowList = table.getRowList();

        RtfRow lastRow = UniBaseUtils.getLast(rowList);
        rowList.remove(rowList.size() - 1);
        if (intercepter != null)
        {
            intercepter.beforeModify(table, rowList.size());
        }

        int sizeBefore = rowList.size();
        for (String[] dataRow : value)
        {
            RtfRow newRow = lastRow.getClone();

            List<RtfCell> cellList = newRow.getCellList();

            if (dataRow == null)
            {
                for (RtfCell cell : cellList)
                {
                    fillCell(cell, "");
                }
            }
            else
            {
                int length = Math.min(dataRow.length, cellList.size());

                for (int j = 0; j < length; j++)
                {
                    fillCell(cellList.get(j), dataRow[j]);
                }
            }
            rowList.add(newRow);
        }

        if (intercepter != null)
        {
            intercepter.afterModify(table, rowList, sizeBefore);
        }

        table.setRowList(rowList);
    }

    /**
     * Записывает данные в ячейку таблицы (скопировано из {@link org.tandemframework.rtf.modifiers.RtfTableModifier})
     *
     * @param cell  ячейка таблицы
     * @param value данные, которые надо в нее записать
     */
    public static void fillCell(RtfCell cell, String value)
    {
        IRtfGroup groupValue = RtfBean.getElementFactory().createRtfGroup();
        groupValue.getElementList().add(RtfBean.getElementFactory().createRtfText(value));

        List<IRtfElement> elementList = cell.getElementList();
        int i = 0;
        while (i < elementList.size() && !(elementList.get(i) instanceof RtfField && ((RtfField) elementList.get(i)).isMark()))
        {
            i++;
        }

        if (i < elementList.size())
        {
            groupValue.getElementList().addAll(0, ((RtfField) elementList.get(i)).getFormatList());
            elementList.set(i, groupValue);
        }
        else
        {
            elementList.add(groupValue);
        }
    }

    /**
     * Удаляет ячейку из строки с расширением другой ячейки
     * Если какую-то не найдет, ругается
     * Индексы берутся до удаления
     *
     * @param row        строка таблицы, из которой надо удалить ячейку
     * @param cellIndex  индекс ячейки, которую надо удалить из строки
     * @param mergeIndex индекс ячейки, в которую добавить ширину удаленной
     * @throws IndexOutOfBoundsException если указанной ячейки нет в таблице
     */
    public static void deleteCell(RtfRow row, int cellIndex, int mergeIndex)
    {
        RtfCell cell = row.getCellList().get(cellIndex);
        RtfCell destCell = row.getCellList().get(mergeIndex);

        destCell.setWidth(destCell.getWidth() + cell.getWidth());
        row.getCellList().remove(cellIndex);
    }


    // --------------------------


    private static void replaceBoldEnd(List<IRtfElement> elementList)
    {
        List<Integer> boldElementIndexList = new ArrayList<>();
        for (int i = 0; i < elementList.size(); i++)
        {
            IRtfElement element = elementList.get(i);
            if (element instanceof IRtfControl && ((IRtfControl) element).getIndex() == IRtfData.B)
            {
                boldElementIndexList.add(i);
            }
        }
        for (Integer index : boldElementIndexList)
        {
            elementList.set(index, RtfBean.getElementFactory().createRtfControl(IRtfData.B, 1));
        }

        List<IRtfElement> tmp = new ArrayList<>(elementList);
        for (IRtfElement element : tmp)
            if (element instanceof IRtfControl && ((IRtfControl) element).getIndex() == IRtfData.PLAIN)
                elementList.remove(element);
    }

    private static void removeParagraphsFromElementsList(List<IRtfElement> elementsList, List<String> fieldNames, boolean delParagraphBefore, boolean delParagraphAfter)
    {
        List<IRtfElement> tagsToRemove = new ArrayList<>();
        List<Integer> paragraphIndexesToRemove = new ArrayList<>();

        for (IRtfElement element : elementsList)
        {
            if (element instanceof RtfField)
            {
                RtfField field = (RtfField) element;
                if (field.isMark() && fieldNames.contains(field.getFieldName()))
                {
                    tagsToRemove.add(element);
                    int index = elementsList.indexOf(element);
                    if (delParagraphBefore && index - 1 >= 0 && elementsList.get(index - 1) instanceof RtfControl && IRtfData.PAR == ((RtfControl) elementsList.get(index - 1)).getIndex())
                        paragraphIndexesToRemove.add(0, index - 1);
                    if (delParagraphAfter && index + 1 < elementsList.size() && elementsList.get(index + 1) instanceof RtfControl && IRtfData.PAR == ((RtfControl) elementsList.get(index + 1)).getIndex())
                        paragraphIndexesToRemove.add(0, index + 1);
                    fieldNames.remove(field.getFieldName());
                }
            }
        }

        for (Integer parIndex : paragraphIndexesToRemove) elementsList.remove(parIndex.intValue());
        elementsList.removeAll(tagsToRemove);
    }

    public static byte[] deleteRowsWithLabels(byte[] template, List<String> labels)
    {
        RtfDocument document = new RtfReader().read(template);
        deleteRowsWithLabels(document, labels);
        return RtfUtil.toByteArray(document);
    }

    public static void deleteRowsWithLabels(RtfDocument document, List<String> labels)
    {
        deleteRowsWithLabels(document.getElementList(), labels);
        for (RtfHeadline headline : document.getHeader().getHeadlineTable().getHeadlineMap().values())
        {
            deleteRowsWithLabels(headline.getElementList(), labels);
        }
    }

    public static void deleteRowsWithLabels(List<IRtfElement> elementList, List<String> labels)
    {
        if (labels != null && !labels.isEmpty())
            deleteRowsWithLabels(elementList, new HashSet<>(labels));
    }

    public static void deleteRowsWithLabels(List<IRtfElement> elementList, Set<String> labels)
    {
        if (labels == null || labels.isEmpty()) return;
        for (IRtfElement element : elementList)
        {
            if (!(element instanceof RtfTable))
                continue;

            final Set<Integer> deleteRows = new HashSet<>();
            final RtfTable table = (RtfTable) element;

            rowLoop:
            for (int i = 0; i < table.getRowList().size(); i++)
            {
                RtfRow row = table.getRowList().get(i);
                for (RtfCell cell : row.getCellList())
                {
                    if (SharedRtfUtil.hasFields(cell.getElementList(), labels))
                    {
                        deleteRows.add(i);
                        continue rowLoop;
                    }
                }
            }

            if (!deleteRows.isEmpty())
            {
                final List<RtfRow> newRows = Lists.newArrayList();
                for (int i = 0; i < table.getRowList().size(); i++)
                {
                    if (!deleteRows.contains(i)) {
                        newRows.add(table.getRowList().get(i));
                    }
                }
                table.setRowList(newRows);
            }
        }
    }

    /**
     * Применяет для шаблона два модификатора и удаляет строки таблиц с метками и возвращает результат
     *
     * @param template       rtf-шаблон
     * @param injectModifier один модификатор
     * @param tableModifier  второй модификатор
     * @param labels  список меток, строки таблиц с которыми необходимо удалить
     * @return получившийся результат
     */
    public static byte[] toByteArray(byte[] template, RtfInjectModifier injectModifier, RtfTableModifier tableModifier, List<String> labels)
    {
        RtfDocument document = new RtfReader().read(template);
        if (injectModifier != null)
            injectModifier.modify(document);
        if (tableModifier != null)
            tableModifier.modify(document);
        deleteRowsWithLabels(document, labels);
        return RtfUtil.toByteArray(document);
    }


    public static void downloadRtfAsPdf(String fileName, byte[] document)
    {
        if(fileName.endsWith(".rtf"))
            fileName = fileName.split("\\.")[0] + ".pdf";

        BusinessComponentUtils.downloadDocument(new ReportRenderer(fileName, document, true), false);
    }

    public static void downloadRtfScriptResultAsPdf(Map<String, Object> result)
    {
        if (result == null)
            throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов не создал печатную форму.");

        Object document = result.get(IScriptExecutor.DOCUMENT);

        if (!(document instanceof byte[]))
            throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов не создал печатную форму.");

        Object fileNameObject = result.get(IScriptExecutor.FILE_NAME);
        String fileName = fileNameObject == null ? "EnrOrder.pdf" : fileNameObject.toString().split("\\.")[0] + ".pdf";
        if (!fileName.endsWith(".pdf"))
            throw new ApplicationException("Скрипт печати приказа о зачислении абитуриентов создал неверное имя файла. Имя файла должно иметь расширение pdf.");

        downloadRtfAsPdf(fileName, (byte[]) document);
    }

    /**
     * @param elementList список элементов верхнего уровня в документе
     * @param label       метка
     * @return элемент верхнего уровня, содержащий данную метку
     */
    public static IRtfElement findElement(List<? extends IRtfElement> elementList, String label)
    {
        return SharedRtfUtil.findElement(elementList, label);
    }


    public static RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel) {
            for (RtfRow row : table.getRowList()) {
                for (RtfCell cell : row.getCellList()) {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null) {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }

        document.getElementList().remove(table);

        return table;
    }
}
