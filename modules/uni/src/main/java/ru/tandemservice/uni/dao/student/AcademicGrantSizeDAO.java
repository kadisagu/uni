/**
 *$Id$
 */
package ru.tandemservice.uni.dao.student;

import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public class AcademicGrantSizeDAO extends UniBaseDao implements IAcademicGrantSizeDAO
{
    @Override
    public Integer getDefaultGrantSize()
    {
        return null;
    }

    @Override
    public Integer getDefaultGroupManagerBonusSize()
    {
        return null;
    }

    @Override
    public Integer getDefaultSocialGrantSize()
    {
        return null;
    }
}