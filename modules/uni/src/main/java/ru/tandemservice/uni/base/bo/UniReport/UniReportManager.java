package ru.tandemservice.uni.base.bo.UniReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportMeta;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportMeta;

/**
 * @author Vasily Zhukov
 * @since 02.02.2011
 */
@Configuration
public class UniReportManager extends BusinessObjectManager
{
    public static UniReportManager instance()
    {
        return instance(UniReportManager.class);
    }

    @Bean
    public ItemListExtPoint<IReportMeta> uniReportListExtPoint()
    {
        IReportMeta personReport = new ReportMeta("personReportPermissionKey", "Выборка персон", ReportPersonAdd.class, new ParametersMap().add("summaryReport", true));

        return itemList(IReportMeta.class)
                .add(personReport.getKey(), personReport)
                .create();
    }
}
