package ru.tandemservice.uni.tapestry.component.grid;

import org.apache.tapestry.AbstractComponent;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.grid.IGridTerm;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author vdanilov
 */
public abstract class Grid extends AbstractComponent {

    public abstract Object getTerms();

    @SuppressWarnings("unchecked")
    private Map<Integer, IGridTerm> getTermMap() {
        Object terms = getTerms();
        if (terms instanceof Map) {
            return (Map<Integer, IGridTerm>) terms;

        } else if (terms instanceof Collection) {
            HashMap<Integer, IGridTerm> termMap = new HashMap<>();
            for (Object obj : (Collection)terms) {
                IGridTerm term = (IGridTerm)obj;
                termMap.put(term.getTermNumber(), term);
            }
            return termMap;

        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override protected void renderComponent(IMarkupWriter writer, IRequestCycle cycle) {
        List<Entry<Integer, IGridTerm>> sortedTermList = DevelopGridDAO.getSortedIntMapEntryList(getTermMap());

//        writer.begin("table");
//        writer.begin("tr");
//        writer.begin("td");
//        writer.appendAttribute("style", "padding:0px; text-align:center;background-color:black");


        writer.begin("table");
//        writer.appendAttribute("cellpadding", "0");
//        writer.appendAttribute("cellspacing", "1");
        writer.appendAttribute("style", "padding:0px; text-align:center;background-color:black;border-spacing:1px;border-collapse:separate;");

        writer.begin("tr");

        int size = sortedTermList.size();
        int i=0;
        while (i<size) {
            int courseNumber = sortedTermList.get(i).getValue().getCourseNumber();
            int j=i; while (j<size && courseNumber == sortedTermList.get(j).getValue().getCourseNumber()) { j++; }

            writer.begin("td");
            writer.appendAttribute("colspan", (j-i));
            writer.appendAttribute("style", "padding:3px 5px; text-align:center;background-color:white");
            writer.print(courseNumber);
            writer.end();

            i=j;
        }
        writer.end();

        writer.begin("tr");
        for (Entry<Integer, IGridTerm> e: sortedTermList) {
            writer.begin("td");
            writer.appendAttribute("style", "padding:3px 5px; text-align:center;background-color:white");
            writer.print(e.getValue().getTermNumber());
            writer.end();
        }
        writer.end();

        writer.end();

//        writer.end();
//        writer.end();
//        writer.end();
    }


}
