/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduLvlList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author oleyba
 * @since 8/12/14
 */
public class UniEduProgramEduLvlListUI extends UIPresenter
{
    private List<HSelectOption> _levelTypeList;

    @Override
    public void onComponentRefresh()
    {
        setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(DataAccessServices.dao().getList(StructureEducationLevels.class), true));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAsMap("code", "title", "levelType"));
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        onClickSearch();
    }

    // getters and setters

    public List<HSelectOption> getLevelTypeList()
    {
        return _levelTypeList;
    }

    public void setLevelTypeList(List<HSelectOption> levelTypeList)
    {
        _levelTypeList = levelTypeList;
    }
}