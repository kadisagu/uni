/* $Id$ */
package ru.tandemservice.uni.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;

/**
 * @author Alexey Lopatin
 * @since 10.09.2015
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> dynamicCatalogsExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
                .add(StringUtils.uncapitalize(DevelopPeriod.class.getSimpleName()), DevelopPeriod.getUiDesc())
                .create();
    }
}
