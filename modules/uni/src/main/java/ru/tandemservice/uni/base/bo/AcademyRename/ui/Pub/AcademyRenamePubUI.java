/* $Id$ */
package ru.tandemservice.uni.base.bo.AcademyRename.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.base.bo.AcademyRename.ui.AddEdit.AcademyRenameAddEdit;
import ru.tandemservice.uni.base.bo.AcademyRename.ui.AddEdit.AcademyRenameAddEditUI;

/**
 * @author Andrey Avetisov
 * @since 09.07.2014
 */
public class AcademyRenamePubUI extends UIPresenter
{
   public void addAcademyRename()
    {
        _uiActivation.asRegionDialog(AcademyRenameAddEdit.class)
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(AcademyRenameAddEdit.class)
                .parameter(AcademyRenameAddEditUI.ACADEMY_RENAME_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
