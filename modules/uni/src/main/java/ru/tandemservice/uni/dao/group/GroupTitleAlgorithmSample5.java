/* $Id$ */
package ru.tandemservice.uni.dao.group;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author Vasily Zhukov
 * @since 03.11.2011
 */
public class GroupTitleAlgorithmSample5 extends UniBaseDao implements IAbstractGroupTitleDAO
{
    /**
     * Формат - ТХ-(2)(1)(z)(u)(Ал)
     * Пример - ТХ-21zuАл
     * Описание:
     *   ТХ - сокращенное название НПП;
     *   2 - курс группы;
     *   1 - порядковый номер группы в рамках курса + НПП, созданных от одного НПв. Т.е. курса, формирующего подразделения, НПв;
     *   z - название формы освоения для группы;
     *   u - поле "группа" для условия освоения из справочника;
     *   Ал - сокращенное название территориального подразделения (если указано для группы);
     */
    @Override
    public String getTitle(Group group, int number)
    {
        EducationOrgUnit educationOrgUnit = get(group.getEducationOrgUnit());
        Course course = get(group.getCourse());

        StringBuilder buffer = new StringBuilder()
        .append(StringUtils.trimToEmpty(educationOrgUnit.getShortTitle()))
        .append('-')
        .append(course.getCode())
        .append(number % getMaxNumber())
        .append(StringUtils.trimToEmpty(educationOrgUnit.getDevelopForm().getGroup()))
        .append(StringUtils.trimToEmpty(educationOrgUnit.getDevelopCondition().getGroup()));

        if (!(educationOrgUnit.getTerritorialOrgUnit() instanceof TopOrgUnit))
            buffer.append(StringUtils.trimToEmpty(educationOrgUnit.getTerritorialOrgUnit().getTerritorialShortTitle()));

        return buffer.toString();
    }

    @Override
    public int getMaxNumber()
    {
        return 10;
    }

}
