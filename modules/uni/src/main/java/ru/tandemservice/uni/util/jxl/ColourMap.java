package ru.tandemservice.uni.util.jxl;

import jxl.format.Colour;
import jxl.write.WritableWorkbook;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.util.cache.SafeMap;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author vdanilov
 */
public class ColourMap
{
    private static final Map<WritableWorkbook, Map<Long, Colour>> workbookColorMap = new WeakHashMap<>();

    public static Map<Long, Colour> getColourMap(WritableWorkbook book) {
        return SafeMap.safeGet(workbookColorMap, book, book1 -> {
            final MutableInt i = new MutableInt(10);
            return SafeMap.get(new SafeMap.Callback<Long, Colour>() {
                @Override public Colour resolve(final Long key) {
                    try {
                        int r = (int)((key >> 16) & 0xFF);
                        int g = (int)((key >> 8) & 0xFF);
                        int b = (int)((key & 0xFF));
                        book1.setColourRGB(Colour.getAllColours()[i.intValue()], r, g, b);
                        return Colour.getAllColours()[i.intValue()];
                    } finally {
                        i.increment();
                    }
                }
            });
        });
    }


}
