/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

import org.hibernate.Query;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;

import ru.tandemservice.uni.dao.IEntityExportDAO.EntityExporter;

/**
 * @author agolubenko
 * @since Apr 22, 2010
 */
public class UniExportDAO extends UniBaseDao implements IUniExportDAO
{
    private Map<String, EntityExporter<? extends IEntity>> _entityName2Exporter;

    public void setEntityName2Exporter(Map<String, EntityExporter<? extends IEntity>> entityName2Exporter)
    {
        _entityName2Exporter = entityName2Exporter;
    }

    private Set<String> _exportableEntities;

    public void setExportableEntities(Set<String> exportableEntities)
    {
        _exportableEntities = exportableEntities;
    }

    @Override
    public byte[] getEntityData(String entityName, String[] properties, boolean onlyUserRecords) throws Exception
    {
        if (!_exportableEntities.contains(entityName))
        {
            throw new Exception("Данная сущность не является экспортируемой: " + entityName);
        }

        // потоки для вывода
        ByteArrayOutputStream result = new ByteArrayOutputStream(512 * 1024);

        try (DataOutputStream outputStream = new DataOutputStream(new GZIPOutputStream(result, 4096)))
        {
            int i = 0;
            for (Iterator<IEntity> iter = getRowsIterator(entityName, onlyUserRecords); iter.hasNext(); )
            {
                IEntity entity = iter.next();
                for (String property : properties)
                {
                    writeProperty(outputStream, entity.getProperty(property));
                }

                if ((++i) % 10000 == 0)
                {
                    getSession().clear();
                }
            }
        }

        return result.toByteArray();
    }

    private void writeProperty(DataOutputStream outputStream, Object property) throws Exception
    {
        // если property не равно null
        if (property != null)
        {
            // то записываем это
            outputStream.writeBoolean(false);

            // если это ссылка, то нужен идентификатор
            if (property instanceof IEntity)
            {
                property = ((IEntity) property).getId();
            }

            // записываем значение property
            if (property instanceof Integer)
            {
                outputStream.writeInt(((Integer) property).intValue());
            }
            else if (property instanceof Long)
            {
                outputStream.writeLong(((Long) property).longValue());
            }
            else if (property instanceof Boolean)
            {
                outputStream.writeBoolean(((Boolean) property).booleanValue());
            }
            else if (property instanceof String)
            {
                String string = (String) property;
                outputStream.writeInt(string.length());
                outputStream.writeChars(string);
            }
            else
            {
                throw new Exception("Unknown property type: " + property);
            }
        }
        // иначе
        else
        {
            // записываем что значения нет
            outputStream.writeBoolean(true);
        }
    }

    @SuppressWarnings("unchecked")
    private Iterator<IEntity> getRowsIterator(String entityName, boolean onlyUserRecords)
    {
        EntityExporter<? extends IEntity> entityExporter = _entityName2Exporter.get(entityName);
        return (entityExporter != null) ? (Iterator<IEntity>) entityExporter.iterate() : getRows(entityName, onlyUserRecords).iterate();
    }

    /**
     * @param entityName      название entity
     * @param onlyUserRecords true если только пользовательские записи, false в противном случае
     * @return список сущностей
     */
    private Query getRows(String entityName, boolean onlyUserRecords)
    {
        String query = getQueryString(entityName, onlyUserRecords);
        Query result = getSession().createQuery(query);
        if (onlyUserRecords)
        {
            result.setInteger("status", 1);
        }
        return result;
    }

    /**
     * @param entityName      название entity
     * @param onlyUserRecords true если только пользовательские записи, false в противном случае
     * @return строка запроса
     */
    private String getQueryString(String entityName, boolean onlyUserRecords)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entityName);
        if (meta == null)
        {
            throw new RuntimeException("Unknown entityName: '" + entityName + "'");
        }
        String className = meta.getClassName();
        return (!onlyUserRecords) ? "from " + className + " order by id" : "from " + className + " where status = :status order by id";
    }
}
