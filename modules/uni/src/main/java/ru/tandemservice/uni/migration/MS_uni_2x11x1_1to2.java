package ru.tandemservice.uni.migration;

import com.google.common.primitives.Ints;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Здесь была миграция на инвертирование приоритета оценок. Вообще-то ее не должно быть в этом модуле - миграция переехала в модуль Сессия (MS_unisession_2x11x1_1to2).
 * Там осуществляется проверка, что миграция из модуля uni еще не отработала.
 * @author avedernikov
 * @since 27.01.2017
 */
@SuppressWarnings({"unused"})
public class MS_uni_2x11x1_1to2 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("org.tandemframework", "1.6.18"),
						new ScriptDependency("org.tandemframework.shared", "1.11.1")
				};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
		// Не нужно было здесь ничего делать.
	}
}
