// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.OrderDataEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author oleyba
 * @since 03.11.2009
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "student.id")
public class Model
{
    private Student student = new Student();
    private OrderData orderData;

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

    public OrderData getOrderData()
    {
        return orderData;
    }

    public void setOrderData(OrderData orderData)
    {
        this.orderData = orderData;
    }

    public String getStudentSexCase()
    {
        return SexCodes.MALE.equals(getStudent().getPerson().getIdentityCard().getSex().getCode()) ? "студента" : "студентки";
    }
}