package ru.tandemservice.uni.entity.util.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.util.GroupTitleAlgorithm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Алгоритм формирования названия группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupTitleAlgorithmGen extends EntityBase
 implements INaturalIdentifiable<GroupTitleAlgorithmGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.util.GroupTitleAlgorithm";
    public static final String ENTITY_NAME = "groupTitleAlgorithm";
    public static final int VERSION_HASH = -71676852;
    private static IEntityMeta ENTITY_META;

    public static final String P_DAO_NAME = "daoName";
    public static final String P_FORMAT = "format";
    public static final String P_SAMPLE = "sample";
    public static final String P_DESCRIPTION = "description";
    public static final String P_CURRENT = "current";

    private String _daoName;     // Имя
    private String _format;     // Формат
    private String _sample;     // Пример
    private String _description;     // Описание
    private boolean _current;     // Текущий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getDaoName()
    {
        return _daoName;
    }

    /**
     * @param daoName Имя. Свойство не может быть null и должно быть уникальным.
     */
    public void setDaoName(String daoName)
    {
        dirty(_daoName, daoName);
        _daoName = daoName;
    }

    /**
     * @return Формат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFormat()
    {
        return _format;
    }

    /**
     * @param format Формат. Свойство не может быть null.
     */
    public void setFormat(String format)
    {
        dirty(_format, format);
        _format = format;
    }

    /**
     * @return Пример. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSample()
    {
        return _sample;
    }

    /**
     * @param sample Пример. Свойство не может быть null.
     */
    public void setSample(String sample)
    {
        dirty(_sample, sample);
        _sample = sample;
    }

    /**
     * @return Описание. Свойство не может быть null.
     */
    @NotNull
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание. Свойство не может быть null.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Текущий. Свойство не может быть null.
     */
    @NotNull
    public boolean isCurrent()
    {
        return _current;
    }

    /**
     * @param current Текущий. Свойство не может быть null.
     */
    public void setCurrent(boolean current)
    {
        dirty(_current, current);
        _current = current;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupTitleAlgorithmGen)
        {
            if (withNaturalIdProperties)
            {
                setDaoName(((GroupTitleAlgorithm)another).getDaoName());
            }
            setFormat(((GroupTitleAlgorithm)another).getFormat());
            setSample(((GroupTitleAlgorithm)another).getSample());
            setDescription(((GroupTitleAlgorithm)another).getDescription());
            setCurrent(((GroupTitleAlgorithm)another).isCurrent());
        }
    }

    public INaturalId<GroupTitleAlgorithmGen> getNaturalId()
    {
        return new NaturalId(getDaoName());
    }

    public static class NaturalId extends NaturalIdBase<GroupTitleAlgorithmGen>
    {
        private static final String PROXY_NAME = "GroupTitleAlgorithmNaturalProxy";

        private String _daoName;

        public NaturalId()
        {}

        public NaturalId(String daoName)
        {
            _daoName = daoName;
        }

        public String getDaoName()
        {
            return _daoName;
        }

        public void setDaoName(String daoName)
        {
            _daoName = daoName;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof GroupTitleAlgorithmGen.NaturalId) ) return false;

            GroupTitleAlgorithmGen.NaturalId that = (NaturalId) o;

            if( !equals(getDaoName(), that.getDaoName()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDaoName());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDaoName());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupTitleAlgorithmGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupTitleAlgorithm.class;
        }

        public T newInstance()
        {
            return (T) new GroupTitleAlgorithm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "daoName":
                    return obj.getDaoName();
                case "format":
                    return obj.getFormat();
                case "sample":
                    return obj.getSample();
                case "description":
                    return obj.getDescription();
                case "current":
                    return obj.isCurrent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "daoName":
                    obj.setDaoName((String) value);
                    return;
                case "format":
                    obj.setFormat((String) value);
                    return;
                case "sample":
                    obj.setSample((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "current":
                    obj.setCurrent((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "daoName":
                        return true;
                case "format":
                        return true;
                case "sample":
                        return true;
                case "description":
                        return true;
                case "current":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "daoName":
                    return true;
                case "format":
                    return true;
                case "sample":
                    return true;
                case "description":
                    return true;
                case "current":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "daoName":
                    return String.class;
                case "format":
                    return String.class;
                case "sample":
                    return String.class;
                case "description":
                    return String.class;
                case "current":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupTitleAlgorithm> _dslPath = new Path<GroupTitleAlgorithm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupTitleAlgorithm");
    }
            

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getDaoName()
     */
    public static PropertyPath<String> daoName()
    {
        return _dslPath.daoName();
    }

    /**
     * @return Формат. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getFormat()
     */
    public static PropertyPath<String> format()
    {
        return _dslPath.format();
    }

    /**
     * @return Пример. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getSample()
     */
    public static PropertyPath<String> sample()
    {
        return _dslPath.sample();
    }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Текущий. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#isCurrent()
     */
    public static PropertyPath<Boolean> current()
    {
        return _dslPath.current();
    }

    public static class Path<E extends GroupTitleAlgorithm> extends EntityPath<E>
    {
        private PropertyPath<String> _daoName;
        private PropertyPath<String> _format;
        private PropertyPath<String> _sample;
        private PropertyPath<String> _description;
        private PropertyPath<Boolean> _current;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Имя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getDaoName()
     */
        public PropertyPath<String> daoName()
        {
            if(_daoName == null )
                _daoName = new PropertyPath<String>(GroupTitleAlgorithmGen.P_DAO_NAME, this);
            return _daoName;
        }

    /**
     * @return Формат. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getFormat()
     */
        public PropertyPath<String> format()
        {
            if(_format == null )
                _format = new PropertyPath<String>(GroupTitleAlgorithmGen.P_FORMAT, this);
            return _format;
        }

    /**
     * @return Пример. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getSample()
     */
        public PropertyPath<String> sample()
        {
            if(_sample == null )
                _sample = new PropertyPath<String>(GroupTitleAlgorithmGen.P_SAMPLE, this);
            return _sample;
        }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(GroupTitleAlgorithmGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Текущий. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.util.GroupTitleAlgorithm#isCurrent()
     */
        public PropertyPath<Boolean> current()
        {
            if(_current == null )
                _current = new PropertyPath<Boolean>(GroupTitleAlgorithmGen.P_CURRENT, this);
            return _current;
        }

        public Class getEntityClass()
        {
            return GroupTitleAlgorithm.class;
        }

        public String getEntityName()
        {
            return "groupTitleAlgorithm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
