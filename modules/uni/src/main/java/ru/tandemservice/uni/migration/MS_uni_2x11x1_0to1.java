package ru.tandemservice.uni.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uni_2x11x1_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationLevelsHighSchool

        // создано свойство printTitleWithoutOrgUnit
        if (!tool.columnExists("educationlevelshighschool_t", "printtitlewithoutorgunit_p"))
        // создано свойство printTitleWithoutOrgUnit
        {
            // создать колонку
            tool.createColumn("educationlevelshighschool_t", new DBColumn("printtitlewithoutorgunit_p", DBType.createVarchar(255)));

        }
        if (tool.columnExists("educationlevelshighschool_t", "printtitleeight_p"))
        // удалить колонку printtitleeight
        {
            // удалить  колонку
            tool.dropColumn("educationlevelshighschool_t", "printtitleeight_p");

        }

    }
}