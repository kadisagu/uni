// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao.pps;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 22.09.2010
 */
public interface IPpsEntryDao extends IUniBaseDao
{
    SpringBeanCache<IPpsEntryDao> instance = new SpringBeanCache<>(IPpsEntryDao.class.getName());

    PpsEntry getCurrentPpsEntry(IPrincipalContext context);

    List<PpsEntry> getPpsList(IPrincipalContext principalContext);

    /**
     * @param pps запись реестра ППС
     * @return список степеней (отсортированный по уровню степени и далее по сокр.названию)
     */
    List<ScienceDegree> getScienceDegrees(PpsEntry pps);

    /**
     * @param pps запись реестра ППС
     * @return список званий (отсортированный по уровню степени и далее по сокр.названию)
     */
    List<ScienceStatus> getScienceStatuses(PpsEntry pps);

    /**
     * Возвращает суммарное число часов для всех почасовиков данной записи ППС
     *
     * @param ppsEntryIds ids записей реестра ППС
     * @return число часов
     */
    Map<PpsEntry, List<IEntity>> getPpsEntry2TimeWorkerMap(List<Long> ppsEntryIds);

    /**
     * Возвращает билдер записей в реестре ППС (на базе почасовика) - М Почасовики
     *
     * @param alias алиас
     * @param orgUnit подразделение
     * @param eduYear учебный год
     * @return билдер по сущности PpsEntryByTimeworker
     */
    DQLSelectBuilder getPpsEntryByTimeworkerBuilder(String alias, OrgUnit orgUnit, EducationYear eduYear);
}
