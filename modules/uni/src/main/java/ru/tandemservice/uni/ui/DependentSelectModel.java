/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import java.util.*;

/**
 * Список значений списка зависит от многих предыдущих значений
 * _map2value - Коды всех прерыдущих значения зашиты в MultiKey и соотств. им список значений
 * _context - объект из которого будут браться значений
 * _keys - ключи
 *
 * @author vip_delete
 * @since 01.04.2009
 */
@SuppressWarnings({"unchecked"})
public class DependentSelectModel extends BaseSingleSelectModel
{
    private Map<MultiKey, List> _map2value;
    private Object _context;
    private String[] _keys;



    public DependentSelectModel(Map<MultiKey, Set> map2value, Object context, Comparator comparator, String propertyView, String... keys)
    {
        super("id", propertyView);
        _context = context;
        _map2value = new HashMap<>(map2value.size());
        if (comparator == null)
            comparator = new Comparator()
        {
            @Override
            public int compare(Object o1, Object o2)
            {
                return getLabelFor(o1, 0).compareTo(getLabelFor(o2, 0));
            }
        };
        for (Map.Entry<MultiKey, Set> entry : map2value.entrySet())
        {
            List list = new ArrayList(entry.getValue());
            Collections.sort(list, comparator);
            _map2value.put(entry.getKey(), list);
        }
        _keys = keys;
    }

    @Override
    public ListResult findValues(String filter)
    {
        List list = getValues();
        if (list == null)
            return ListResult.getEmpty();

        filter = filter.toLowerCase();
        List values = new ArrayList(list.size());
        for (Object obj : list)
            if (getSearchLabel(obj).startsWith(filter))
                values.add(obj);
        return new ListResult<>(values, values.size());
    }

    protected String getSearchLabel(Object obj)
    {
        return getLabelFor(obj, 0).toLowerCase();
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        List list = getValues();
        if (list == null) return null;
        for (Object obj : list)
            if (getPrimaryKey(obj).equals(primaryKey))
                return obj;
        return null;
    }

    private List getValues()
    {
        List ids = new ArrayList();
        for (String key : _keys)
        {
            Object obj = FastBeanUtils.getValue(_context, key);
            if (obj == null)
                ids.add(-1L); // id null-объекта
            else
                ids.add(getPrimaryKey(obj));
        }
        MultiKey key = new MultiKey(ids.toArray(new Object[ids.size()]));

        return _map2value.get(key);
    }
}
