/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/14
 */
public class UniEduProgramDao extends UniBaseDao implements IUniEduProgramDao
{
    @Override
    public void deleteUnusedEduLevels()
    {
        final DQLDeleteBuilder del = new DQLDeleteBuilder(EducationLevels.class);
        del.where(new DQLCanDeleteExpressionBuilder(EducationLevels.class, "id").getExpression());

        final int count = del.createStatement(getSession()).execute();
        UserContext userContext = UserContext.getInstance();
        if (userContext != null) {
            userContext.getInfoCollector().add("Удалено неиспользуемых элементов: " + count + ".");
        }
    }

    @Override
    public void doChangeAllowStudents(Long eduHsId)
    {
        EducationLevelsHighSchool educationLevelsHighSchool = get(EducationLevelsHighSchool.class, eduHsId);
        educationLevelsHighSchool.setAllowStudents(!educationLevelsHighSchool.isAllowStudents());
        update(educationLevelsHighSchool);
    }

    @Override
    public void doChangeUsedEduOu(Long eduOuId)
    {
        EducationOrgUnit eduOu = get(EducationOrgUnit.class, eduOuId);
        eduOu.setUsed(!eduOu.isUsed());
        update(eduOu);
    }

    @Override
    public void saveOrUpdateEduOu(EducationOrgUnit educationOrgUnit)
    {
        final EducationOrgUnit other = getWithSameKey(educationOrgUnit);
        if ((educationOrgUnit.getId() == null && other != null) || (educationOrgUnit.getId() != null && !educationOrgUnit.equals(other))) {
            throw new ApplicationException("Такой набор параметров для обучения студентов уже существует.");
        }
        saveOrUpdate(educationOrgUnit);
    }
    
    public EducationOrgUnit getWithSameKey(EducationOrgUnit educationOrgUnit)
    {
        Criteria criteria = getSession().createCriteria(EducationOrgUnit.class);
        criteria.add(Restrictions.eq(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, educationOrgUnit.getFormativeOrgUnit()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, educationOrgUnit.getTerritorialOrgUnit()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, educationOrgUnit.getEducationLevelHighSchool()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_FORM, educationOrgUnit.getDevelopForm()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_CONDITION, educationOrgUnit.getDevelopCondition()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_TECH, educationOrgUnit.getDevelopTech()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_PERIOD, educationOrgUnit.getDevelopPeriod()));
        List<EducationOrgUnit> result = criteria.list();
        if (result.size() > 1)
            throw new ApplicationException("Направление подготовки (специальность) подразделения с такими параметрами уже существует.");
        return (result.size() != 0) ? result.get(0) : null;
    }

    @Override
    public List<EducationOrgUnit> getEduOuList(EduProgram p)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EducationOrgUnit.class, "e")
            .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().orgUnit()), value(p.getOwnerOrgUnit().getOrgUnit())))
            .where(eq(property("e", EducationOrgUnit.developForm().programForm()), value(p.getForm())))
            .where(eq(property("e", EducationOrgUnit.developPeriod().eduProgramDuration()), value(p.getDuration())))
            .order(property("e", EducationOrgUnit.educationLevelHighSchool().id().s()))
            ;

        if (p.getEduProgramTrait() == null) {
            dql.where(isNull(property("e", EducationOrgUnit.developTech().programTrait())));
        } else {
            dql.where(eq(property("e", EducationOrgUnit.developTech().programTrait()), value(p.getEduProgramTrait())));
        }

        if (p instanceof EduProgramProf) {
            dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().assignedQualification()), value(((EduProgramProf) p).getProgramQualification())));
            dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()), value(((EduProgramProf) p).getProgramSubject())));

            if (p instanceof EduProgramSecondaryProf) {
                dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().code()), value(((EduProgramSecondaryProf) p).isInDepthStudy() ? QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O : QualificationsCodes.BAZOVYY_UROVEN_S_P_O)));
            } else if (p instanceof EduProgramHigherProf) {
                dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization()), value(((EduProgramHigherProf) p).getProgramSpecialization())));
                dql.where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().programOrientation()), value(((EduProgramHigherProf) p).getProgramOrientation())));
            }
        }
        return dql.createStatement(getSession()).list();
    }
}