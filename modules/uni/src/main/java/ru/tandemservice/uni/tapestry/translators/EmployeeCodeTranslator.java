/**
 * $Id$
 */
package ru.tandemservice.uni.tapestry.translators;

import java.util.Locale;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.translator.AbstractTranslator;
import org.apache.tapestry.valid.ValidationConstraint;
import org.apache.tapestry.valid.ValidatorException;

import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;

/**
 * @author dseleznev
 * Created on: 23.07.2008
 */
public class EmployeeCodeTranslator extends AbstractTranslator
{

    @Override
    protected String formatObject(IFormComponent field, Locale locale, Object object)
    {
        return new EmployeeCodeFormatter().format((String)object);
    }

    @Override
    protected Object parseText(IFormComponent field, ValidationMessages messages, String text) throws ValidatorException
    {
        try
        {
            return Integer.parseInt(text);
        }
        catch (Exception e)
        {
            throw new ValidatorException("Поле «" + field.getDisplayName() + "» должно содержать только цифры.", ValidationConstraint.NUMBER_FORMAT);
        }
    }

}