package ru.tandemservice.uni.util;

import org.apache.log4j.Logger;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.BackgroundProcessThread;
import org.tandemframework.core.process.IBackgroundProcess;
import org.tandemframework.core.process.ProcessDisplayMode;
import org.tandemframework.core.process.ProcessResult;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author vdanilov
 */
public class BackgroundProcessHolder {
    private final Object mutex = new Object();
    private BackgroundProcessThread _thread;

    private void start(final BackgroundProcessThread t) {
        synchronized (this.mutex) {
            if (this.isAlive()) { throw new IllegalStateException(); }
            BusinessComponentUtils.runProcess(this._thread = t);
        }
    }

    private void stop() {
        synchronized (this.mutex) {
        }
    }

    public boolean isAlive() {
        synchronized (this.mutex) {
            return ((null != this._thread) && (this._thread.isStarted()));
        }
    }

    public BackgroundProcessThread getCurrentProcess() {
        synchronized (this.mutex) {
            return this._thread;
        }
    }

    public void start(final String title, final IBackgroundProcess process) {
        this.start(title, process, ProcessDisplayMode.percent);
    }

    public void start(final String title, final IBackgroundProcess process, final ProcessDisplayMode processDisplayMode) {
        synchronized (this.mutex) {
            if (this.isAlive()) { throw new IllegalStateException(); }

            final IBackgroundProcess p = new IBackgroundProcess() {
                final Logger log = Logger.getLogger(process.getClass());

                @Override public void cancel() {
                    try { process.cancel(); }
                    catch (final Throwable t) { this.log.error(t.getMessage(), t); }
                }
                @Override public ProcessResult run(final ProcessState state) {
                    try
                    {
                        try { Thread.sleep(1000); }
                        catch (final Throwable t) { }

                        return process.run(state);
                    }
                    catch (final Throwable t)
                    {
                        this.log.error(t.getMessage(), t);

                        if (t instanceof ApplicationException) {
                            // закрываем диалог (произошла ошибка - ее и показываем)
                            // return new ProcessResult(t);
                            throw (ApplicationException)t;
                        }

                        // закрываем диалог (произошла ошибка, но отображать ее нельзя)
                        // return new ProcessResult(
                        //     new ApplicationException(
                        //         ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY),
                        //         t
                        //     )
                        // );
                        throw new ApplicationException(
                            ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY),
                            t
                        );
                    }
                    finally
                    {
                        BackgroundProcessHolder.this.stop();
                    }
                }
            };
            this.start(new BackgroundProcessThread(title, p, processDisplayMode));
        }
    }

}
