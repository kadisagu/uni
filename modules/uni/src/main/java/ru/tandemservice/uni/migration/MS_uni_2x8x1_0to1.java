package ru.tandemservice.uni.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentDocumentType


        // создано обязательное свойство scriptItem
        {
            // создать колонку
            tool.createColumn("studentdocumenttype_t", new DBColumn("scriptitem_id", DBType.LONG));


            List<Object[]> codes =  tool.executeQuery(MigrationUtils.processor(String.class, String.class), "select td.code_p, td.title_p from studentdocumenttype_t as sdt inner join templatedocument_t as td on sdt.template_id = td.id ");
            Short entityCode = tool.entityCodes().get("uniScriptItem");

            for (Object[] code : codes) {

                Long id = EntityIDGenerator.generateNewId(entityCode);

                String codeP = (String) code[0];
                String title = (String) code[1];

                if (codeP.equals("sd.uni.0") || codeP.equals("sd.uni.1") || codeP.equals("sd.uni.2") || codeP.equals("sd.uni.3"))
                {
                    switch (codeP) {
                        case ("sd.uni.0") : title = "Справка «Действительно является студентом»"; break;
                        case ("sd.uni.1") : title = "Справка «Действительно является студентом» (с визой ректора)"; break;
                        case ("sd.uni.2") : title = "Справка «Действительно является студентом» (в пенсионный фонд)"; break;
                        case ("sd.uni.3") : title = "Справка «В военкомат»"; break;
                    }
                    tool.executeUpdate("insert into scriptitem_t (id, discriminator, code_p, catalogcode_p, usescript_p, title_p) values (?, ?, ?, 'uniScriptItem', ?, ?)", id, entityCode, codeP, Boolean.FALSE, title);
                }
                else {
                    tool.executeUpdate("insert into scriptitem_t (id, discriminator, code_p, catalogcode_p, usescript_p, title_p) values (?, ?, ?, 'uniScriptItem', ?, ?)", id, entityCode, codeP, Boolean.FALSE, title);
                }
                tool.executeUpdate("insert into uni_c_script_t values (?)", id);
            }

            tool.executeUpdate("update studentdocumenttype_t set scriptitem_id=(select si.id from studentdocumenttype_t as sdt inner join templatedocument_t as td on sdt.template_id = td.id inner join scriptitem_t as si on si.code_p = td.code_p where sdt.id = studentdocumenttype_t.id) where scriptitem_id is null");


            // сделать колонку NOT NULL
            tool.setColumnNullable("studentdocumenttype_t", "scriptitem_id", false);

            tool.dropColumn("studentdocumenttype_t", "template_id");


        }


    }
}