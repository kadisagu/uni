// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.sec;

import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.util.IOrgUnitLPGVisibilityResolver;
import org.tandemframework.shared.organization.base.util.IOrgUnitPermissionCustomizer;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 24.11.2010
 */
public class UniOrgUnitPermissionCustomizer extends UniBaseDao implements IOrgUnitPermissionCustomizer, IOrgUnitLPGVisibilityResolver
{
    @Override
    public void customize(OrgUnitType orgUnitType, Map<String, PermissionGroupMeta> permissionGroupMap)
    {
        // additional tabs
        String code = orgUnitType.getCode();

        // если есть хотя бы одно подразделение данного типа, то выводим права для настройки соотв. вкладок
        boolean isAllowGroups = existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.orgUnitKind().allowGroups().s(), Boolean.TRUE,
                OrgUnitToKindRelation.orgUnit().orgUnitType().s(), orgUnitType
        );

        permissionGroupMap.get(code + "GroupPermissionGroup").setHidden(!isAllowGroups);
        permissionGroupMap.get(code + "ArchivalGroupPermissionGroup").setHidden(!isAllowGroups);

        boolean isAllowStudents = existsEntity(
                OrgUnitToKindRelation.class,
                OrgUnitToKindRelation.orgUnitKind().allowStudents().s(), Boolean.TRUE,
                OrgUnitToKindRelation.orgUnit().orgUnitType().s(), orgUnitType
        );

        permissionGroupMap.get(code + "StudentsTabPermissionGroup").setHidden(!isAllowStudents);
        permissionGroupMap.get(code + "ArchivalStudentsTabPermissionGroup").setHidden(!isAllowStudents);
    }

    @Override
    public List<ILPGVisibilityResolver> getResolverList()
    {
        ILPGVisibilityResolver groupResolver = new ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "groupLocalClass";
            }

            @Override
            public boolean isHidden(OrgUnitType orgUnitType)
            {
                Set<OrgUnitType> types = OrgUnitManager.instance().dao().getOrgUnitTypeChildrenFullList(orgUnitType);
                types.add(orgUnitType);

                return !existsEntity(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.orgUnitKind().allowGroups().s(), Boolean.TRUE,
                        OrgUnitToKindRelation.orgUnit().orgUnitType().s(), types
                );
            }
        };

        ILPGVisibilityResolver studentResolver = new ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "studentLocalClass";
            }

            @Override
            public boolean isHidden(OrgUnitType orgUnitType)
            {
                Set<OrgUnitType> types = OrgUnitManager.instance().dao().getOrgUnitTypeChildrenFullList(orgUnitType);
                types.add(orgUnitType);

                return !existsEntity(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.orgUnitKind().allowStudents().s(), Boolean.TRUE,
                        OrgUnitToKindRelation.orgUnit().orgUnitType().s(), types
                );
            }
        };

        ILPGVisibilityResolver studentAndGroupResolver = new ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "studentLocal";
            }

            @Override
            public boolean isHidden(OrgUnitType orgUnitType)
            {
                Set<OrgUnitType> types = OrgUnitManager.instance().dao().getOrgUnitTypeChildrenFullList(orgUnitType);
                types.add(orgUnitType);

                return !existsEntityByCondition(
                        OrgUnitToKindRelation.class, "r",
                        and(
                                or(
                                        eq(property("r", OrgUnitToKindRelation.orgUnitKind().allowStudents()), value(Boolean.TRUE)),
                                        eq(property("r", OrgUnitToKindRelation.orgUnitKind().allowGroups()), value(Boolean.TRUE))
                                ),
                                in(property("r", OrgUnitToKindRelation.orgUnit().orgUnitType()), types)
                        )
                );
            }
        };

        return Arrays.asList(groupResolver, studentResolver, studentAndGroupResolver);
    }
}
