/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitList;

import org.hibernate.Query;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        if (UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING.equals(model.getKindCode()))
            model.setTitle("Выпускающие подразделения");
        else if (UniDefines.CATALOG_ORGUNIT_KIND_FORMING.equals(model.getKindCode()))
            model.setTitle("Формирующие подразделения");
        else if (UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL.equals(model.getKindCode()))
            model.setTitle("Территориальные подразделения");
        else
            throw new ApplicationException("Неизвестный вид подразделения: " + model.getKindCode() + ".");

        final Query q = getSession().createQuery("select r." + OrgUnitTypeToKindRelation.L_ORG_UNIT_TYPE + " from " + OrgUnitTypeToKindRelation.ENTITY_CLASS + " r where r." + OrgUnitTypeToKindRelation.L_ORG_UNIT_KIND + "." + OrgUnitKind.P_CODE + "=:kindCode order by r." + OrgUnitTypeToKindRelation.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_TITLE);
        q.setParameter("kindCode", model.getKindCode());
        model.setOrgUnitTypeList(q.list());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<OrgUnitToKindRelation> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", OrgUnitToKindRelation.L_ORG_UNIT_KIND + "." + ICatalogItem.CATALOG_ITEM_CODE, model.getKindCode()));

        if (model.getOrgUnitType() != null)
            builder.add(MQExpression.eq("e", OrgUnitToKindRelation.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE, model.getOrgUnitType()));
        if (model.getOrgUnitTitle() != null)
            builder.add(MQExpression.like("e", OrgUnitToKindRelation.L_ORG_UNIT + "." + OrgUnit.P_TITLE, CoreStringUtils.escapeLike(model.getOrgUnitTitle())));

        new OrderDescriptionRegistry("e").applyOrder(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    @Override
    public void updateAllowAddStudents(Model model)
    {
        OrgUnitToKindRelation relation = get(OrgUnitToKindRelation.class, model.getOrgUnitToKindRelationId());
        relation.setAllowAddStudent(!relation.isAllowAddStudent());
        update(relation);
    }

    @Override
    public void updateAllowAddGroups(Model model)
    {
        OrgUnitToKindRelation relation = get(OrgUnitToKindRelation.class, model.getOrgUnitToKindRelationId());
        relation.setAllowAddGroup(!relation.isAllowAddGroup());
        update(relation);
    }
}
