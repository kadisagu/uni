/**
 * $Id$
 */
package ru.tandemservice.uni.component.group.StudentPersonCards;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 08.10.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void refreshStudent(Model model);
}