/* $Id$ */
package ru.tandemservice.uni.dao;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.*;
import java.util.function.Function;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil.joinWithSeparator;

/**
 * @author Vasily Zhukov
 * @since 29.06.2011
 */
public class UniSyncDao extends UniBaseDao implements IUniSyncDao
{
    abstract class EduHSTTitleGenerator {

        int detailCount = 0;
        final List<Function<String[], String>> formulaList = new ArrayList<>(3);
        final List<Object[]> rows;

        EduHSTTitleGenerator(int initialCapacity) {
            rows = new ArrayList<>(initialCapacity);
        }

        int addDetail() {
            return detailCount++;
        }

        void addFormula(Function<String[], String> formula) {
            formulaList.add(formula);
        }

        void addEduHS(EducationLevelsHighSchool eduHS, String[] details) {
            rows.add(new Object[]{eduHS, details});
        }

        abstract String getOriginalTitle(EducationLevelsHighSchool eduHS);

        abstract void setNewTitle(EducationLevelsHighSchool eduHS, String title);

        boolean updateTitle(Session session, EducationLevelsHighSchool eduHS, String newTitle) {

            if (!newTitle.trim().equals(getOriginalTitle(eduHS))) {
                setNewTitle(eduHS, newTitle);
                session.update(eduHS);
                return true;
            }
            return false;
        }

        private boolean _regenerateTitles(Session session, int formulaIdx, Collection<Object[]> items) {

            final Function<String[], String> formula = formulaList.get(formulaIdx);
            boolean hasUpdate = false;

            if (formulaIdx >= formulaList.size() - 1) {
                // Алтернативных формул больше нет - просто генерируем для всех названия
                for (Object[] item : items) {
                    hasUpdate |= updateTitle(session, (EducationLevelsHighSchool) item[0], formula.apply((String[]) item[1]));
                }
                return hasUpdate;
            }

            final Multimap<String, Object[]> multimap = ArrayListMultimap.create();
            for (Object[] item : items) {
                multimap.put(formula.apply((String[]) item[1]), item);
            }

            for (Map.Entry<String, Collection<Object[]>> entry : multimap.asMap().entrySet()) {

                Collection<Object[]> set = entry.getValue();
                if (set.size() == 1) {
                    hasUpdate |= updateTitle(session, (EducationLevelsHighSchool) set.iterator().next()[0], entry.getKey());
                } else {
                    hasUpdate |= _regenerateTitles(session, formulaIdx + 1, set);
                }
            }

            return hasUpdate;
        }

        boolean regenerateTitles(Session session) {
            return _regenerateTitles(session, 0, rows);
        }
    }

    class DisplayableTitleGenerator extends EduHSTTitleGenerator {

        final int codePrefixIdx = addDetail();
        final int titleIdx = addDetail();
        final int notationIdx = addDetail();
        final int orgUnitIdx = addDetail();
        final int qualificationIdx = addDetail();
        final int orientationIdx = addDetail();

        @Override
        String getOriginalTitle(EducationLevelsHighSchool eduHS) {
            return eduHS.getDisplayableTitle();
        }

        @Override
        void setNewTitle(EducationLevelsHighSchool eduHS, String newTitle) {
            eduHS.setDisplayableTitle(newTitle);
        }

        DisplayableTitleGenerator(int initialCapacity) {
            super(initialCapacity);
            // Не забываем инкрементировать номер версии при всяком изменении форматов - см. EduLevelHSDisplayableTitleSync.init()
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + bJoin(s[orientationIdx], s[notationIdx]));
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + bJoin(s[orgUnitIdx], s[orientationIdx], s[notationIdx]));
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + bJoin(s[orgUnitIdx], s[orientationIdx], s[qualificationIdx], s[notationIdx]));
        }

        void addEduHS(EducationLevelsHighSchool eduHS, String codePrefix, String title,
                      String eduLevelAndIndexNotation, String orgUnitShortTitle,
                      String qualificationTitle, String orientationAbbreviation)
        {
            final String details[] = new String[detailCount];
            details[codePrefixIdx] = codePrefix;
            details[titleIdx] = title;
            details[notationIdx] = eduLevelAndIndexNotation;
            details[orgUnitIdx] = orgUnitShortTitle;
            details[qualificationIdx] = qualificationTitle;
            details[orientationIdx] = orientationAbbreviation;
            addEduHS(eduHS, details);
        }
    }

    class PrintTitleGenerator extends EduHSTTitleGenerator {

        final int codePrefixIdx = addDetail();
        final int titleIdx = addDetail();
        final int orgUnitIdx = addDetail();

        @Override
        String getOriginalTitle(EducationLevelsHighSchool eduHS) {
            return eduHS.getPrintTitle();
        }

        @Override
        void setNewTitle(EducationLevelsHighSchool eduHS, String newTitle) {
            eduHS.setPrintTitle(newTitle);
        }

        PrintTitleGenerator(int initialCapacity) {
            super(initialCapacity);
            // Не забываем инкрементировать номер версии при всяком изменении форматов - см. EduLevelHSDisplayableTitleSync.init()
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx]);
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + " (" + s[orgUnitIdx] + ")");
        }

        void addEduHS(EducationLevelsHighSchool eduHS, String codePrefix, String title, String orgUnitShortTitle) {
            final String details[] = new String[detailCount];
            details[codePrefixIdx] = codePrefix;
            details[titleIdx] = title;
            details[orgUnitIdx] = orgUnitShortTitle;
            addEduHS(eduHS, details);
        }
    }

    class PrintTitleWithoutOrgUnitGenerator extends EduHSTTitleGenerator {

        final int codePrefixIdx = addDetail();
        final int titleIdx = addDetail();


        @Override
        String getOriginalTitle(EducationLevelsHighSchool eduHS) {
            return eduHS.getPrintTitleWithoutOrgUnit();
        }

        @Override
        void setNewTitle(EducationLevelsHighSchool eduHS, String newTitle) {
            eduHS.setPrintTitleWithoutOrgUnit(newTitle);
        }

        PrintTitleWithoutOrgUnitGenerator(int initialCapacity) {
            super(initialCapacity);
            // Не забываем инкрементировать номер версии при всяком изменении форматов - см. EduLevelHSDisplayableTitleSync.init()
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx]);

        }

        void addEduHS(EducationLevelsHighSchool eduHS, String codePrefix, String title) {
            final String details[] = new String[detailCount];
            details[codePrefixIdx] = codePrefix;
            details[titleIdx] = title;
            addEduHS(eduHS, details);
        }
    }

    class FullTitleGenerator extends EduHSTTitleGenerator {

        final int codePrefixIdx = addDetail();
        final int titleIdx = addDetail();
        final int levelTypeIdx = addDetail();
        final int notationIdx = addDetail();
        final int orgUnitIdx = addDetail();
        final int qualificationIdx = addDetail();
        final int orientationIdx = addDetail();

        @Override
        String getOriginalTitle(EducationLevelsHighSchool eduHS) {
            return eduHS.getFullTitle();
        }

        @Override
        void setNewTitle(EducationLevelsHighSchool eduHS, String newTitle) {
            eduHS.setFullTitle(newTitle);
        }

        FullTitleGenerator(int initialCapacity) {
            super(initialCapacity);
            // Не забываем инкрементировать номер версии при всяком изменении форматов - см. EduLevelHSDisplayableTitleSync.init()
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + bJoin(s[levelTypeIdx], s[orientationIdx], s[notationIdx]));
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + bJoin(s[orgUnitIdx], s[levelTypeIdx], s[orientationIdx], s[notationIdx]));
            addFormula(s -> s[codePrefixIdx] + " " + s[titleIdx] + bJoin(s[orgUnitIdx], s[levelTypeIdx], s[orientationIdx], s[qualificationIdx], s[notationIdx]));
        }

        void addEduHS(EducationLevelsHighSchool eduHS, String codePrefix, String title,
                      String levelTypeTitle, String eduLevelAndIndexNotation, String orgUnitShortTitle,
                      String assignedQualificationTitle, String orientationAbbreviation)
        {
            final String details[] = new String[detailCount];
            details[codePrefixIdx] = codePrefix;
            details[titleIdx] = title;
            details[levelTypeIdx] = levelTypeTitle;
            details[notationIdx] = eduLevelAndIndexNotation;
            details[orgUnitIdx] = orgUnitShortTitle;
            details[qualificationIdx] = assignedQualificationTitle;
            details[orientationIdx] = orientationAbbreviation;
            addEduHS(eduHS, details);
        }
    }

    class FullTitleExtendedGenerator extends EduHSTTitleGenerator {

        final int codePrefixIdx = addDetail();
        final int parentLevelIdx = addDetail();
        final int titleIdx = addDetail();
        final int levelTypeIdx = addDetail();
        final int notationIdx = addDetail();
        final int orgUnitIdx = addDetail();
        final int qualificationIdx = addDetail();
        final int orientationIdx = addDetail();

        @Override
        String getOriginalTitle(EducationLevelsHighSchool eduHS) {
            return eduHS.getFullTitleExtended();
        }

        @Override
        void setNewTitle(EducationLevelsHighSchool eduHS, String newTitle) {
            eduHS.setFullTitleExtended(newTitle);
        }

        FullTitleExtendedGenerator(int initialCapacity) {
            super(initialCapacity);
            // Не забываем инкрементировать номер версии при всяком изменении форматов - см. EduLevelHSDisplayableTitleSync.init()
            addFormula(s -> s[codePrefixIdx] + " " + joinWithSeparator(" | ", s[parentLevelIdx], s[titleIdx]) + bJoin(s[levelTypeIdx], s[orientationIdx], s[notationIdx]));
            addFormula(s -> s[codePrefixIdx] + " " + joinWithSeparator(" | ", s[parentLevelIdx], s[titleIdx]) + bJoin(s[orgUnitIdx], s[levelTypeIdx], s[orientationIdx], s[notationIdx]));
            addFormula(s -> s[codePrefixIdx] + " " + joinWithSeparator(" | ", s[parentLevelIdx], s[titleIdx]) + bJoin(s[orgUnitIdx], s[levelTypeIdx], s[orientationIdx], s[qualificationIdx], s[notationIdx]));
        }

        void addEduHS(EducationLevelsHighSchool eduHS, String codePrefix, String parentLevelTitle, String title,
                      String levelTypeTitle, String eduLevelAndIndexNotation, String orgUnitShortTitle,
                      String assignedQualificationTitle, String orientationAbbreviation)
        {
            final String details[] = new String[detailCount];
            details[codePrefixIdx] = codePrefix;
            details[parentLevelIdx] = parentLevelTitle;
            details[titleIdx] = title;
            details[levelTypeIdx] = levelTypeTitle;
            details[notationIdx] = eduLevelAndIndexNotation;
            details[orgUnitIdx] = orgUnitShortTitle;
            details[qualificationIdx] = assignedQualificationTitle;
            details[orientationIdx] = orientationAbbreviation;
            addEduHS(eduHS, details);
        }
    }

    @Override
    public boolean doUpdateEduHSTitles()
    {
        // Не забываем инкрементировать номер версии при всяком изменении форматов - см. EduLevelHSDisplayableTitleSync.init()

        final Session session = getSession();

        List<EducationLevelsHighSchool> highSchoolList = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "e").column(property("e"))
                .fetchPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("e"), "educationLevel")
                .fetchPath(DQLJoinType.inner, EducationLevelsHighSchool.orgUnit().fromAlias("e"), "orgUnit")
                .fetchPath(DQLJoinType.inner, EducationLevels.levelType().fromAlias("educationLevel"), "levelType")
                .createStatement(getSession()).list();

        final DisplayableTitleGenerator dtGen = new DisplayableTitleGenerator(highSchoolList.size());
        final PrintTitleGenerator ptGen = new PrintTitleGenerator(highSchoolList.size());
        final PrintTitleWithoutOrgUnitGenerator ptWOUGen = new PrintTitleWithoutOrgUnitGenerator(highSchoolList.size());
        final FullTitleGenerator ftGen = new FullTitleGenerator(highSchoolList.size());
        final FullTitleExtendedGenerator ftExtGen = new FullTitleExtendedGenerator(highSchoolList.size());

        for (final EducationLevelsHighSchool highSchool : highSchoolList) {

            String parentLevelTitle = null;
            final String codePrefix = highSchool.getEducationLevel().getTitleCodePrefix();
            final String title = highSchool.getTitle();
            final String eduLevelAndIndexNotation = highSchool.getEducationLevel().getEduProgramSubject() != null ?  highSchool.getEducationLevel().getEduProgramSubject().getSubjectIndex().getEduLevelAndIndexNotation() : null;
            final String levelTypeTitle = highSchool.getEducationLevel().getLevelType().getTitle();
            final String orgUnitShortTitle = highSchool.getOrgUnit().getShortTitle();
            final String assignedQualificationTitle = highSchool.getAssignedQualification() != null ? highSchool.getAssignedQualification().getTitle() : null;
            final String orientationAbbreviation = highSchool.getProgramOrientation() != null ? highSchool.getProgramOrientation().getAbbreviation() : null;

            if (highSchool.getEducationLevel().isProfileOrSpecialization()) {
                parentLevelTitle = highSchool.getEducationLevel().getParentLevel().getTitle();
            }

            dtGen.addEduHS(highSchool, codePrefix, title, eduLevelAndIndexNotation, orgUnitShortTitle, assignedQualificationTitle, orientationAbbreviation);
            ptGen.addEduHS(highSchool, codePrefix, title, orgUnitShortTitle);
            ptWOUGen.addEduHS(highSchool,codePrefix,title);
            ftGen.addEduHS(highSchool, codePrefix, title, levelTypeTitle, eduLevelAndIndexNotation, orgUnitShortTitle, assignedQualificationTitle, orientationAbbreviation);
            ftExtGen.addEduHS(highSchool, codePrefix, parentLevelTitle, title, levelTypeTitle, eduLevelAndIndexNotation, orgUnitShortTitle, assignedQualificationTitle, orientationAbbreviation);
        }

        return ptWOUGen.regenerateTitles(session) |dtGen.regenerateTitles(session) | ptGen.regenerateTitles(session) | ftGen.regenerateTitles(session) | ftExtGen.regenerateTitles(session);
    }

    @Override
    public void doUpdateEduLevelsFieldsFull()
    {
        for (EducationLevels educationLevels : getList(EducationLevels.class)) {
            educationLevels.setInheritedOkso(educationLevels.buildInheritedOksoNonPersistent());
            // educationLevels.setInheritedDiplomaQualification(educationLevels.buildInheritedDiplomaQualificationNonPersistent());
            update(educationLevels);
        }
    }

    @Override
    public boolean doUpdateEduLevelsFields(Collection<Long> ids) {

        final Session session = getSession();

        // построим мн-во ids которые могут быть изменены: ids всместе со всеми (рекурсивно в глубь) чилдами
        final Set<Long> needToUpdateSet = new HashSet<>();

        // поиск в ширину делаем через очередь
        final Queue<Collection<Long>> queueForUpdate = new ArrayDeque<>();
        queueForUpdate.add(ids);

        while (!queueForUpdate.isEmpty()) {

            final Collection<Long> items = queueForUpdate.poll();
            needToUpdateSet.addAll(items);

            for (final List<Long> idsPart : Iterables.partition(items, DQL.MAX_VALUES_ROW_NUMBER)) {

                final List<Long> childIds = new DQLSelectBuilder().fromEntity(EducationLevels.class, "e")
                        .column(property(EducationLevels.id().fromAlias("e")))
                        .where(in(property(EducationLevels.parentLevel().id().fromAlias("e")), idsPart))
                        .createStatement(session).list();

                if (!childIds.isEmpty()) {
                    queueForUpdate.add(childIds);
                }
            }
        }

        boolean hasGlobalUpdate = false;

        for (final EducationLevels educationLevels : UniSyncDao.this.getList(EducationLevels.class, needToUpdateSet)) {

            boolean hasUpdate = false;

            final String inheritedOksoNonPersistent = educationLevels.buildInheritedOksoNonPersistent();
            if (inheritedOksoNonPersistent == null ? educationLevels.getInheritedOkso() != null : !inheritedOksoNonPersistent.equals(educationLevels.getInheritedOkso())) {
                educationLevels.setInheritedOkso(inheritedOksoNonPersistent);
                hasUpdate = true;
            }


            if (hasUpdate) {
                hasGlobalUpdate = true;
                session.update(educationLevels);
            }
        }

        return hasGlobalUpdate;
    }

    // Возвращает " (item, item)", если хотя бы один item задан. Иначе - пустую строку. Лишних запятых нет.
    private static String bJoin(String... items) {
        final String ret = joinWithSeparator(", ", items);
        return StringUtils.isNotEmpty(ret) ? (" (" + ret + ")") : "";
    }
}