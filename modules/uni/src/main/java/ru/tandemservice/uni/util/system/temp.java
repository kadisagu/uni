// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

import java.io.FileInputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;

/**
 * @author oleyba
 * @since 21.05.2010
 */
public class temp
{
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception
    {
        for (String line : (List<String>) IOUtils.readLines(new FileInputStream("C:\\tmp.txt"), "cp1251"))
        {
            String[] r = line.split(";");
            String code = r[2];
            String title = r[1];
            System.out.println("<item id=\""+ code +"\">");
            System.out.println("   <property name=\"title\" value=\"" + title + "\" synchronize=\"system\"/>");
            System.out.println("</item>");
        }
    }
}
