/* $Id$ */
package ru.tandemservice.uni.component.log.EntityLogViewBase;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.DateColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.ui.View.RowItem;

/**
 * @author oleyba
 * @since 9/19/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    protected void prepareDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        final DynamicListDataSource<RowItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Номер транзакции", "transactionNumber").setClickable(false));
        dataSource.addColumn(new DateColumn("Дата", "date", DateFormatter.PATTERN_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип события", "eventType").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сообщение", "message").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пользователь", "principalContextTitle").setClickable(false));
        dataSource.addColumn(new SimpleColumn("IP адрес", "ipAddress").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Объект", "entityTitle").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Свойство", "propertyTitle").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Предыдущее значение", "prevValue").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Новое значение", "newValue").setClickable(false));
        dataSource.setOrder("date", OrderDirection.desc);

        model.setDataSource(dataSource);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);
        model.getSettings().clear();
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();
    }
}
