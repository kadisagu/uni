/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.yearDistributionPart.YearDistributionPartPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEdit;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEditUI;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorLanguage;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;

/**
 * @author agolubenko
 * @since Jul 26, 2010
 */
public class Controller extends DefaultCatalogPubController<YearDistributionPart, Model, IDAO>
{
    @Override
    @SuppressWarnings( { "unchecked" })
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);
        DynamicListDataSource<YearDistributionPart> dataSource = new DynamicListDataSource<YearDistributionPart>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", YearDistributionPart.title().s()));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", "shortTitle").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Разбиение учебного года", YearDistributionPart.yearDistribution().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Редактировать падежные формы", UniDefines.ICO_EDIT_CASE, "onClickEditItemDeclination").setPermissionKey(model.getCatalogItemEdit()));
        return dataSource;
    }

    public void onClickEditItemDeclination(IBusinessComponent context)
    {
        final InflectorLanguage lang = IUniBaseDao.instance.get().get(InflectorLanguage.class, InflectorLanguage.enabled().s(), Boolean.TRUE);
        if (null == lang)
            throw new ApplicationException("Не установлен активный язык склонений.");
        context.createDefaultChildRegion(new ComponentActivator(DeclinableEdit.class.getSimpleName(), new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, context.getListenerParameter())
                .add(DeclinableEditUI.BIND_LANGUAGE_ID, lang.getId())
        ));

    }
}
