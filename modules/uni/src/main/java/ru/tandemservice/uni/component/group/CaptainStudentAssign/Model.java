/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.group.CaptainStudentAssign;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author dseleznev
 *         Created on : 22.01.2008
 */
@Input(keys = "groupId", bindings = "group.id")
public class Model
{
    private Group _group = new Group();
    private boolean _anyGroupStudent;
    private IMultiSelectModel _studentListModel;
    private List<Student> _captainStudentList;
    private List<Student> _captainStudentOldList;

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public boolean isAnyGroupStudent()
    {
        return _anyGroupStudent;
    }

    public void setAnyGroupStudent(boolean anyGroupStudent)
    {
        _anyGroupStudent = anyGroupStudent;
    }

    public IMultiSelectModel getStudentListModel()
    {
        return _studentListModel;
    }

    public void setStudentListModel(IMultiSelectModel studentListModel)
    {
        _studentListModel = studentListModel;
    }

    public List<Student> getCaptainStudentList()
    {
        return _captainStudentList;
    }

    public void setCaptainStudentList(List<Student> captainStudentList)
    {
        _captainStudentList = captainStudentList;
    }

    public List<Student> getCaptainStudentOldList()
    {
        return _captainStudentOldList;
    }

    public void setCaptainStudentOldList(List<Student> captainStudentOldList)
    {
        _captainStudentOldList = captainStudentOldList;
    }
}
