/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EduOuAddEdit.UniEduProgramEduOuAddEdit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author oleyba
 * @since 8/12/14
 */
@State({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="holder.id", required=true)
})
public class UniEduProgramEduOuPubUI extends UIPresenter
{
    private final EntityHolder<EducationOrgUnit> holder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickEdit() {
        _uiActivation.asRegionDialog(UniEduProgramEduOuAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getHolder().getId()).activate();
    }

    public void onClickDelete() {
        DataAccessServices.dao().delete(getHolder().getId());
        deactivate();
    }

    // getters and setters

    public EntityHolder<EducationOrgUnit> getHolder()
    {
        return holder;
    }

    public EducationOrgUnit getEducationOrgUnit() {
        return getHolder().getValue();
    }

    public EducationLevelsHighSchool getEduHs() {
        return getEducationOrgUnit().getEducationLevelHighSchool();
    }

    public EducationLevels getEduLvl() {
        return getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
    }
}