package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип документа, выдаваемого студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentDocumentTypeGen extends EntityBase
 implements INaturalIdentifiable<StudentDocumentTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.StudentDocumentType";
    public static final String ENTITY_NAME = "studentDocumentType";
    public static final int VERSION_HASH = -694760277;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_SCRIPT_ITEM = "scriptItem";
    public static final String P_ACTIVE = "active";
    public static final String P_INDEX = "index";
    public static final String P_BUSINESS_OBJECT_NAME = "businessObjectName";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private UniScriptItem _scriptItem;     // Конфигурация для скриптовой печати модуля «Юни»
    private boolean _active; 
    private int _index; 
    private String _businessObjectName;     // Тип бизнес-объекта
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Конфигурация для скриптовой печати модуля «Юни». Свойство не может быть null.
     */
    @NotNull
    public UniScriptItem getScriptItem()
    {
        return _scriptItem;
    }

    /**
     * @param scriptItem Конфигурация для скриптовой печати модуля «Юни». Свойство не может быть null.
     */
    public void setScriptItem(UniScriptItem scriptItem)
    {
        dirty(_scriptItem, scriptItem);
        _scriptItem = scriptItem;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active  Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index  Свойство не может быть null.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Тип бизнес-объекта.
     */
    @Length(max=255)
    public String getBusinessObjectName()
    {
        return _businessObjectName;
    }

    /**
     * @param businessObjectName Тип бизнес-объекта.
     */
    public void setBusinessObjectName(String businessObjectName)
    {
        dirty(_businessObjectName, businessObjectName);
        _businessObjectName = businessObjectName;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentDocumentTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentDocumentType)another).getCode());
            }
            setScriptItem(((StudentDocumentType)another).getScriptItem());
            setActive(((StudentDocumentType)another).isActive());
            setIndex(((StudentDocumentType)another).getIndex());
            setBusinessObjectName(((StudentDocumentType)another).getBusinessObjectName());
            setTitle(((StudentDocumentType)another).getTitle());
        }
    }

    public INaturalId<StudentDocumentTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentDocumentTypeGen>
    {
        private static final String PROXY_NAME = "StudentDocumentTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentDocumentTypeGen.NaturalId) ) return false;

            StudentDocumentTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentDocumentTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentDocumentType.class;
        }

        public T newInstance()
        {
            return (T) new StudentDocumentType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "scriptItem":
                    return obj.getScriptItem();
                case "active":
                    return obj.isActive();
                case "index":
                    return obj.getIndex();
                case "businessObjectName":
                    return obj.getBusinessObjectName();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "scriptItem":
                    obj.setScriptItem((UniScriptItem) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "businessObjectName":
                    obj.setBusinessObjectName((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "scriptItem":
                        return true;
                case "active":
                        return true;
                case "index":
                        return true;
                case "businessObjectName":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "scriptItem":
                    return true;
                case "active":
                    return true;
                case "index":
                    return true;
                case "businessObjectName":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "scriptItem":
                    return UniScriptItem.class;
                case "active":
                    return Boolean.class;
                case "index":
                    return Integer.class;
                case "businessObjectName":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentDocumentType> _dslPath = new Path<StudentDocumentType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentDocumentType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Конфигурация для скриптовой печати модуля «Юни». Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getScriptItem()
     */
    public static UniScriptItem.Path<UniScriptItem> scriptItem()
    {
        return _dslPath.scriptItem();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Тип бизнес-объекта.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getBusinessObjectName()
     */
    public static PropertyPath<String> businessObjectName()
    {
        return _dslPath.businessObjectName();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StudentDocumentType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private UniScriptItem.Path<UniScriptItem> _scriptItem;
        private PropertyPath<Boolean> _active;
        private PropertyPath<Integer> _index;
        private PropertyPath<String> _businessObjectName;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentDocumentTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Конфигурация для скриптовой печати модуля «Юни». Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getScriptItem()
     */
        public UniScriptItem.Path<UniScriptItem> scriptItem()
        {
            if(_scriptItem == null )
                _scriptItem = new UniScriptItem.Path<UniScriptItem>(L_SCRIPT_ITEM, this);
            return _scriptItem;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(StudentDocumentTypeGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(StudentDocumentTypeGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Тип бизнес-объекта.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getBusinessObjectName()
     */
        public PropertyPath<String> businessObjectName()
        {
            if(_businessObjectName == null )
                _businessObjectName = new PropertyPath<String>(StudentDocumentTypeGen.P_BUSINESS_OBJECT_NAME, this);
            return _businessObjectName;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.StudentDocumentType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentDocumentTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StudentDocumentType.class;
        }

        public String getEntityName()
        {
            return "studentDocumentType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
