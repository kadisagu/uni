/* $Id$ */
package ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.print.student;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.HtmlTagRemoveFormatter;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintColumnIndex;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 10.08.2011
 */
public class StudentPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentPersonalNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentBookNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentState = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentCourse = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentGroup = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentFormativeOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentTerritorialOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentEducationOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentCompensationType = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentDevelopForm = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentDevelopCondition = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentDevelopTech = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentDevelopPeriod = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentDevelopPeriodAuto = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentStudentCategory = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentTargetAdmission = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetAdmissionOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentIndividualEducation = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentStartYear = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentFinalTheme = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentAdvisor = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentPracticePlace = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentEmployement = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentComment = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentEducationDocument = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 22; i++)
            ids.add("chStudent" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        // добавляем колонку удостоверения личности 
        int i = dql.addLastAggregateColumn(Person.identityCard());

        // соединяем студента через left join, потому что мог быть выбран фильтр "не является студентом"
        // если студент был ранее соединент по inner join, то останется inner join
        String studentAlias = dql.leftJoinEntity(Student.class, Student.person());

        // добавляем колонку student, агрегируем в список
        final int studentIndex = dql.addListAggregateColumn(studentAlias);

        // добавляем стандартные колонки персоны
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintColumnIndex());
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("fullFio", i, IdentityCard.fullFio()));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("sex", i, IdentityCard.sex().shortTitle()));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("birthDate", i, IdentityCard.birthDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("cardType", i, IdentityCard.cardType().shortTitle()));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("seria", i, IdentityCard.seria()));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("number", i, IdentityCard.number()));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("issuanceDate", i, IdentityCard.issuanceDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("issuancePlace", i, IdentityCard.issuancePlace()));

        // колонки студента
        if (_studentEducationDocument.isActive()) {
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintColumn("studentEducationDocument")
            {
                @Override
                public Object createColumnValue(Object[] data)
                {
                    if (data[studentIndex] != null && CollectionUtils.isNotEmpty((ArrayList) data[studentIndex])) {
                        Student student = (Student) ((ArrayList) data[studentIndex]).get(0);
                        PersonEduDocument eduDocument = student.getEduDocument();
                        if (eduDocument != null) {
                            return eduDocument.getDocumentKindTitle() + " " + eduDocument.getFullNumber()
                                    + (eduDocument.getRegistrationNumber() == null ? "" : " Рег. номер " + eduDocument.getRegistrationNumber())
                                    + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.getIssuanceDate());
                        }
                    }
                    return "";
                }
            });
        }

        if (_studentPersonalNumber.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentPersonalNumber", studentIndex, Student.personalNumber()));

        if (_studentBookNumber.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentBookNumber", studentIndex, Student.bookNumber()));

        if (_studentState.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentState", studentIndex, Student.status().title()));

        if (_studentCourse.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentCourse", studentIndex, Student.course().title()));

        if (_studentGroup.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentGroup", studentIndex, Student.group().title()));

        if (_studentFormativeOrgUnit.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentFormativeOrgUnit", studentIndex, Student.educationOrgUnit().formativeOrgUnit().fullTitle()));

        if (_studentTerritorialOrgUnit.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentTerritorialOrgUnit", studentIndex, Student.educationOrgUnit().territorialOrgUnit().territorialFullTitle()));

        if (_studentEducationOrgUnit.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentEducationOrgUnit", studentIndex, Student.educationOrgUnit().educationLevelHighSchool().printTitle()));

        if (_studentCompensationType.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentCompensationType", studentIndex, Student.compensationType().shortTitle()));

        if (_studentDevelopForm.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentDevelopForm", studentIndex, Student.educationOrgUnit().developForm().title()));

        if (_studentDevelopCondition.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentDevelopCondition", studentIndex, Student.educationOrgUnit().developCondition().title()));

        if (_studentDevelopTech.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentDevelopTech", studentIndex, Student.educationOrgUnit().developTech().title()));

        if (_studentDevelopPeriod.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentDevelopPeriod", studentIndex, Student.educationOrgUnit().developPeriod().title()));

        if (_studentDevelopPeriodAuto.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentDevelopPeriodAuto", studentIndex, Student.developPeriodAuto().title()));

        if (_studentStudentCategory.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentStudentCategory", studentIndex, Student.studentCategory().title()));

        if (_studentTargetAdmission.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentTargetAdmission", studentIndex, Student.targetAdmission(), source -> source == null ? "" : ((Boolean)source ? "Да" : "Нет")));

        if (_targetAdmissionOrgUnit.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("targetAdmissionOrgUnit", studentIndex, Student.targetAdmissionOrgUnit().title()));

        if (_studentIndividualEducation.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentIndividualEducation", studentIndex, Student.personalEducation(), source -> source == null ? "" : ((Boolean) source ? "Да" : "Нет")));

        if (_studentOrgUnit.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentOrgUnit", studentIndex, Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()));

        if (_studentStartYear.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentStartYear", studentIndex, Student.entranceYear()));

        if (_studentFinalTheme.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentFinalTheme", studentIndex, Student.finalQualifyingWorkTheme()));

        if (_studentAdvisor.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentAdvisor", studentIndex, Student.ppsEntry().titleFioInfoOrgUnitDegreesStatuses()));

        if (_studentPracticePlace.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentPracticePlace", studentIndex, Student.practicePlace()));

        if (_studentEmployement.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentEmployement", studentIndex, Student.employment()));

        if (_studentComment.isActive())
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("studentComment", studentIndex, Student.comment(), HtmlTagRemoveFormatter.INSTANCE));
    }

    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getStudentPersonalNumber()
    {
        return _studentPersonalNumber;
    }

    public IReportPrintCheckbox getStudentBookNumber()
    {
        return _studentBookNumber;
    }

    public IReportPrintCheckbox getStudentState()
    {
        return _studentState;
    }

    public IReportPrintCheckbox getStudentCourse()
    {
        return _studentCourse;
    }

    public IReportPrintCheckbox getStudentGroup()
    {
        return _studentGroup;
    }

    public IReportPrintCheckbox getStudentFormativeOrgUnit()
    {
        return _studentFormativeOrgUnit;
    }

    public IReportPrintCheckbox getStudentTerritorialOrgUnit()
    {
        return _studentTerritorialOrgUnit;
    }

    public IReportPrintCheckbox getStudentEducationOrgUnit()
    {
        return _studentEducationOrgUnit;
    }

    public IReportPrintCheckbox getStudentCompensationType()
    {
        return _studentCompensationType;
    }

    public IReportPrintCheckbox getStudentDevelopForm()
    {
        return _studentDevelopForm;
    }

    public IReportPrintCheckbox getStudentDevelopCondition()
    {
        return _studentDevelopCondition;
    }

    public IReportPrintCheckbox getStudentDevelopTech()
    {
        return _studentDevelopTech;
    }

    public IReportPrintCheckbox getStudentDevelopPeriod()
    {
        return _studentDevelopPeriod;
    }

    public IReportPrintCheckbox getStudentDevelopPeriodAuto()
    {
        return _studentDevelopPeriodAuto;
    }

    public IReportPrintCheckbox getStudentStudentCategory()
    {
        return _studentStudentCategory;
    }

    public IReportPrintCheckbox getStudentTargetAdmission()
    {
        return _studentTargetAdmission;
    }

    public IReportPrintCheckbox getStudentIndividualEducation()
    {
        return _studentIndividualEducation;
    }

    public IReportPrintCheckbox getStudentOrgUnit()
    {
        return _studentOrgUnit;
    }

    public IReportPrintCheckbox getStudentStartYear()
    {
        return _studentStartYear;
    }

    public IReportPrintCheckbox getStudentFinalTheme()
    {
        return _studentFinalTheme;
    }

    public IReportPrintCheckbox getStudentAdvisor()
    {
        return _studentAdvisor;
    }

    public IReportPrintCheckbox getStudentPracticePlace()
    {
        return _studentPracticePlace;
    }

    public IReportPrintCheckbox getStudentEmployement()
    {
        return _studentEmployement;
    }

    public IReportPrintCheckbox getStudentComment()
    {
        return _studentComment;
    }

    public IReportPrintCheckbox getTargetAdmissionOrgUnit()
    {
        return _targetAdmissionOrgUnit;
    }
}
