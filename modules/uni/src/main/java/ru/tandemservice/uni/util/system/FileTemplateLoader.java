/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.log4j.Logger;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;
import org.tandemframework.core.CoreUtils;

/**
 * @author vip_delete
 * @since 21.11.2008
 */
public class FileTemplateLoader extends ResourceLoader
{
    private static final Logger LOGGER = Logger.getLogger(FileTemplateLoader.class);

    @Override
    public void init(ExtendedProperties configuration)
    {

    }

    @Override
    public synchronized InputStream getResourceStream(String name) throws ResourceNotFoundException
    {
        try
        {
            LOGGER.info("Loading tempate with name = '" + name + "'.");
            if (CoreUtils.isNull(name))
                throw new ResourceNotFoundException("No resolver name provided");
            return new FileInputStream(name);
        }
        catch (Exception fnfe)
        {
            throw new ResourceNotFoundException(fnfe.getMessage());
        }
    }

    @Override
    public boolean isSourceModified(Resource resource)
    {
        return false;
    }

    @Override
    public long getLastModified(Resource resource)
    {
        return 0;
    }
}