/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.student.StudentPub;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniStudent.ui.FinishYearEdit.UniStudentFinishYearEdit;
import ru.tandemservice.uni.base.bo.UniStudent.ui.StatusEdit.UniStudentStatusEdit;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;
import ru.tandemservice.uni.entity.employee.StudentDocument;

/**
 * @author vip_delete
 * @since 26.12.2007
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);

        prepareDocumentDataSource(component);
    }

    private void prepareDocumentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDocumentDataSource() != null) return;

        DynamicListDataSource<StudentDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().updateStudentDocumentDataSource(model);
        });


        dataSource.addColumn(new SimpleColumn("Дата формирования", StudentDocument.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип справки", StudentDocument.studentDocumentType().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер", StudentDocument.number().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickDocumentPrint", "Печать").setPermissionKey("printStudentDocument"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDocumentDelete", "Удалить выданный документ {0} от {1}", StudentDocument.studentDocumentType().title().s(), StudentDocument.formingDate().s()).setPermissionKey("deleteStudentDocument"));

        model.setDocumentDataSource(dataSource);
    }

    public void onClickEditStudentAttributes(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.STUDENT_EDIT, "studentTabPanel", (BusinessComponent) component)
                .parameter("studentId", getModel(component).getStudent().getId())
                .top().activate();
    }

    public void onClickEditStudentAdditionalAttributes(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.STUDENT_ADDITIONAL_EDIT, "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickEditStudentArchivalAttributes(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.STUDENT_ARCHIVAL_EDIT, "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickArchive(IBusinessComponent component)
    {
        Model model = getModel(component);
        String selectedTab = model.getSelectedTab();
        String selectedStudentDataTab = model.getSelectedDataTab();

        getDao().setArchival(getModel(component), true);
        onRefreshComponent(component);

        model.setSelectedTab(selectedTab);
        model.setSelectedDataTab(selectedStudentDataTab);
    }

    public void onClickUnarchive(IBusinessComponent component)
    {
        Model model = getModel(component);
        String selectedTab = model.getSelectedTab();
        String selectedStudentDataTab = model.getSelectedDataTab();

        getDao().setArchival(getModel(component), false);
        onRefreshComponent(component);

        model.setSelectedTab(selectedTab);
        model.setSelectedDataTab(selectedStudentDataTab);

        getDao().validate(model, component.getUserContext().getErrorCollector());
    }

    public void onClickChangeStudentGroup(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.STUDENT_GROUP_CHANGE, "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickEditOrderData(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.ORDER_DATA_EDIT, "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickEditComment(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.STUDENT_COMMENT_EDIT, "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickEditEntranceYear(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(IUniComponents.STUDENT_ENTRANCE_YEAR_EDIT, "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickEditFinishYear(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(UniStudentFinishYearEdit.class.getSimpleName(), "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickPrintStudentPersonalCard(IBusinessComponent component)
    {
        if (!IStudentDAO.instance.get().isStudentCardPrintLegacyVersionEnabled()) {
            final UniScriptItem script = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_PRINT_SCRIPT);
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(script, getModel(component).getStudent().getId());
        }
        else {
            activateInRoot(component, new ComponentActivator(IUniComponents.STUDENT_PERSON_CARD, new ParametersMap()
                    .add("studentId", getModel(component).getStudent().getId())
            ));
        }
    }

    public void onClickEditStudentStatus(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegion(UniStudentStatusEdit.class.getSimpleName(), "studentTabPanel", (BusinessComponent) component).top().activate();
    }

    public void onClickDocumentAdd(IBusinessComponent component)
    {
        component.createChildRegion("studentDocuments", new ComponentActivator(IUniComponents.DOCUMENT_ADD));
    }

    public void onClickDocumentPrint(IBusinessComponent component)
    {
        StudentDocument studentDocument = getDao().getNotNull(StudentDocument.class, (Long) component.getListenerParameter());
        if (studentDocument.getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName("StudentDocument.rtf").document(studentDocument.getContent()).rtf(), true);
    }

    public void onClickDocumentDelete(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
        getModel(component).getDocumentDataSource().refresh();
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDocumentDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().remove("studentDocumentType");
        onClickSearch(component);
    }
}
