/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.rtf;

import java.util.List;

import org.tandemframework.rtf.node.IRtfElement;

/**
 * @author vip_delete
 * @since 16.04.2009
 */
public class RtfSearchResult
{
    private boolean _found;
    private List<IRtfElement> _elementList;
    private int _index;

    public RtfSearchResult(boolean found, List<IRtfElement> elementList, int index)
    {
        _found = found;
        _elementList = elementList;
        _index = index;
    }

    public boolean isFound()
    {
        return _found;
    }

    public List<IRtfElement> getElementList()
    {
        return _elementList;
    }

    public int getIndex()
    {
        return _index;
    }
}
