package ru.tandemservice.uni.dao.person;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IPersonReleaseDao
{
    public static final SpringBeanCache<IPersonReleaseDao> instance = new SpringBeanCache<IPersonReleaseDao>(IPersonReleaseDao.class.getName());

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doReleaseUnusedPersons();

}
