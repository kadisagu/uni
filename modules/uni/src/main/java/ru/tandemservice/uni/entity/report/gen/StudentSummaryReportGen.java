package ru.tandemservice.uni.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uni.entity.report.StudentSummaryReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка контингента студентов (по направлениям подготовки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentSummaryReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.report.StudentSummaryReport";
    public static final String ENTITY_NAME = "studentSummaryReport";
    public static final int VERSION_HASH = 709112891;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_SHOW_SPECIALIZATIONS = "showSpecializations";
    public static final String P_STUDENT_STATUS_ALL = "studentStatusAll";
    public static final String P_STUDENT_STATUS_ACADEM = "studentStatusAcadem";
    public static final String P_STUDENT_STATUS_PREG = "studentStatusPreg";
    public static final String P_STUDENT_STATUS_CHILD = "studentStatusChild";

    private OrgUnit _orgUnit;     // Подразделение
    private String _developForm;     // Форма освоения
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _qualification;     // Квалификация
    private String _studentCategory;     // Категория обучаемого
    private String _showSpecializations;     // Выделять специализации
    private String _studentStatusAll;     // В графу «Всего» считать студентов в состояниях
    private String _studentStatusAcadem;     // В графу «Академ. отпуск» считать студентов в состояниях
    private String _studentStatusPreg;     // В графу «Отпуск по берем. и родам» считать студентов в состояниях
    private String _studentStatusChild;     // В графу «Отпуск по уходу за ребенком» считать студентов в состояниях

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Категория обучаемого.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Выделять специализации.
     */
    @Length(max=255)
    public String getShowSpecializations()
    {
        return _showSpecializations;
    }

    /**
     * @param showSpecializations Выделять специализации.
     */
    public void setShowSpecializations(String showSpecializations)
    {
        dirty(_showSpecializations, showSpecializations);
        _showSpecializations = showSpecializations;
    }

    /**
     * @return В графу «Всего» считать студентов в состояниях.
     */
    @Length(max=255)
    public String getStudentStatusAll()
    {
        return _studentStatusAll;
    }

    /**
     * @param studentStatusAll В графу «Всего» считать студентов в состояниях.
     */
    public void setStudentStatusAll(String studentStatusAll)
    {
        dirty(_studentStatusAll, studentStatusAll);
        _studentStatusAll = studentStatusAll;
    }

    /**
     * @return В графу «Академ. отпуск» считать студентов в состояниях.
     */
    @Length(max=255)
    public String getStudentStatusAcadem()
    {
        return _studentStatusAcadem;
    }

    /**
     * @param studentStatusAcadem В графу «Академ. отпуск» считать студентов в состояниях.
     */
    public void setStudentStatusAcadem(String studentStatusAcadem)
    {
        dirty(_studentStatusAcadem, studentStatusAcadem);
        _studentStatusAcadem = studentStatusAcadem;
    }

    /**
     * @return В графу «Отпуск по берем. и родам» считать студентов в состояниях.
     */
    @Length(max=255)
    public String getStudentStatusPreg()
    {
        return _studentStatusPreg;
    }

    /**
     * @param studentStatusPreg В графу «Отпуск по берем. и родам» считать студентов в состояниях.
     */
    public void setStudentStatusPreg(String studentStatusPreg)
    {
        dirty(_studentStatusPreg, studentStatusPreg);
        _studentStatusPreg = studentStatusPreg;
    }

    /**
     * @return В графу «Отпуск по уходу за ребенком» считать студентов в состояниях.
     */
    @Length(max=255)
    public String getStudentStatusChild()
    {
        return _studentStatusChild;
    }

    /**
     * @param studentStatusChild В графу «Отпуск по уходу за ребенком» считать студентов в состояниях.
     */
    public void setStudentStatusChild(String studentStatusChild)
    {
        dirty(_studentStatusChild, studentStatusChild);
        _studentStatusChild = studentStatusChild;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentSummaryReportGen)
        {
            setOrgUnit(((StudentSummaryReport)another).getOrgUnit());
            setDevelopForm(((StudentSummaryReport)another).getDevelopForm());
            setFormativeOrgUnit(((StudentSummaryReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((StudentSummaryReport)another).getTerritorialOrgUnit());
            setDevelopCondition(((StudentSummaryReport)another).getDevelopCondition());
            setDevelopTech(((StudentSummaryReport)another).getDevelopTech());
            setDevelopPeriod(((StudentSummaryReport)another).getDevelopPeriod());
            setQualification(((StudentSummaryReport)another).getQualification());
            setStudentCategory(((StudentSummaryReport)another).getStudentCategory());
            setShowSpecializations(((StudentSummaryReport)another).getShowSpecializations());
            setStudentStatusAll(((StudentSummaryReport)another).getStudentStatusAll());
            setStudentStatusAcadem(((StudentSummaryReport)another).getStudentStatusAcadem());
            setStudentStatusPreg(((StudentSummaryReport)another).getStudentStatusPreg());
            setStudentStatusChild(((StudentSummaryReport)another).getStudentStatusChild());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentSummaryReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentSummaryReport.class;
        }

        public T newInstance()
        {
            return (T) new StudentSummaryReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "developForm":
                    return obj.getDevelopForm();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "qualification":
                    return obj.getQualification();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "showSpecializations":
                    return obj.getShowSpecializations();
                case "studentStatusAll":
                    return obj.getStudentStatusAll();
                case "studentStatusAcadem":
                    return obj.getStudentStatusAcadem();
                case "studentStatusPreg":
                    return obj.getStudentStatusPreg();
                case "studentStatusChild":
                    return obj.getStudentStatusChild();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "showSpecializations":
                    obj.setShowSpecializations((String) value);
                    return;
                case "studentStatusAll":
                    obj.setStudentStatusAll((String) value);
                    return;
                case "studentStatusAcadem":
                    obj.setStudentStatusAcadem((String) value);
                    return;
                case "studentStatusPreg":
                    obj.setStudentStatusPreg((String) value);
                    return;
                case "studentStatusChild":
                    obj.setStudentStatusChild((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "developForm":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "qualification":
                        return true;
                case "studentCategory":
                        return true;
                case "showSpecializations":
                        return true;
                case "studentStatusAll":
                        return true;
                case "studentStatusAcadem":
                        return true;
                case "studentStatusPreg":
                        return true;
                case "studentStatusChild":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "developForm":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "qualification":
                    return true;
                case "studentCategory":
                    return true;
                case "showSpecializations":
                    return true;
                case "studentStatusAll":
                    return true;
                case "studentStatusAcadem":
                    return true;
                case "studentStatusPreg":
                    return true;
                case "studentStatusChild":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "developForm":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "qualification":
                    return String.class;
                case "studentCategory":
                    return String.class;
                case "showSpecializations":
                    return String.class;
                case "studentStatusAll":
                    return String.class;
                case "studentStatusAcadem":
                    return String.class;
                case "studentStatusPreg":
                    return String.class;
                case "studentStatusChild":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentSummaryReport> _dslPath = new Path<StudentSummaryReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentSummaryReport");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Категория обучаемого.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Выделять специализации.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getShowSpecializations()
     */
    public static PropertyPath<String> showSpecializations()
    {
        return _dslPath.showSpecializations();
    }

    /**
     * @return В графу «Всего» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusAll()
     */
    public static PropertyPath<String> studentStatusAll()
    {
        return _dslPath.studentStatusAll();
    }

    /**
     * @return В графу «Академ. отпуск» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusAcadem()
     */
    public static PropertyPath<String> studentStatusAcadem()
    {
        return _dslPath.studentStatusAcadem();
    }

    /**
     * @return В графу «Отпуск по берем. и родам» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusPreg()
     */
    public static PropertyPath<String> studentStatusPreg()
    {
        return _dslPath.studentStatusPreg();
    }

    /**
     * @return В графу «Отпуск по уходу за ребенком» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusChild()
     */
    public static PropertyPath<String> studentStatusChild()
    {
        return _dslPath.studentStatusChild();
    }

    public static class Path<E extends StudentSummaryReport> extends StorableReport.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<String> _showSpecializations;
        private PropertyPath<String> _studentStatusAll;
        private PropertyPath<String> _studentStatusAcadem;
        private PropertyPath<String> _studentStatusPreg;
        private PropertyPath<String> _studentStatusChild;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(StudentSummaryReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(StudentSummaryReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(StudentSummaryReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(StudentSummaryReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(StudentSummaryReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(StudentSummaryReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(StudentSummaryReportGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Категория обучаемого.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(StudentSummaryReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Выделять специализации.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getShowSpecializations()
     */
        public PropertyPath<String> showSpecializations()
        {
            if(_showSpecializations == null )
                _showSpecializations = new PropertyPath<String>(StudentSummaryReportGen.P_SHOW_SPECIALIZATIONS, this);
            return _showSpecializations;
        }

    /**
     * @return В графу «Всего» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusAll()
     */
        public PropertyPath<String> studentStatusAll()
        {
            if(_studentStatusAll == null )
                _studentStatusAll = new PropertyPath<String>(StudentSummaryReportGen.P_STUDENT_STATUS_ALL, this);
            return _studentStatusAll;
        }

    /**
     * @return В графу «Академ. отпуск» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusAcadem()
     */
        public PropertyPath<String> studentStatusAcadem()
        {
            if(_studentStatusAcadem == null )
                _studentStatusAcadem = new PropertyPath<String>(StudentSummaryReportGen.P_STUDENT_STATUS_ACADEM, this);
            return _studentStatusAcadem;
        }

    /**
     * @return В графу «Отпуск по берем. и родам» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusPreg()
     */
        public PropertyPath<String> studentStatusPreg()
        {
            if(_studentStatusPreg == null )
                _studentStatusPreg = new PropertyPath<String>(StudentSummaryReportGen.P_STUDENT_STATUS_PREG, this);
            return _studentStatusPreg;
        }

    /**
     * @return В графу «Отпуск по уходу за ребенком» считать студентов в состояниях.
     * @see ru.tandemservice.uni.entity.report.StudentSummaryReport#getStudentStatusChild()
     */
        public PropertyPath<String> studentStatusChild()
        {
            if(_studentStatusChild == null )
                _studentStatusChild = new PropertyPath<String>(StudentSummaryReportGen.P_STUDENT_STATUS_CHILD, this);
            return _studentStatusChild;
        }

        public Class getEntityClass()
        {
            return StudentSummaryReport.class;
        }

        public String getEntityName()
        {
            return "studentSummaryReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
