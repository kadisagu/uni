/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentPersonCard;

import org.tandemframework.core.component.State;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vip_delete
 * @since 18.09.2008
 */
@State(keys = "studentId", bindings = "student.id")
public class Model
{
    private Student _student = new Student();
    private IStudentPersonCardPrintFactory _printFactory;

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public IStudentPersonCardPrintFactory getPrintFactory()
    {
        return _printFactory;
    }

    public void setPrintFactory(IStudentPersonCardPrintFactory printFactory)
    {
        _printFactory = printFactory;
    }
}
