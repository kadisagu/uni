package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_uni_2x9x3_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception {
		tool.dropColumn("educationlevels_t", "diplomaqualification_id");
		tool.dropColumn("educationlevels_t", "nhrtddplmqlfctn_id");
    }
}