package ru.tandemservice.uni.entity.employee.pps;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.employee.pps.gen.EmployeePost4PpsEntryGen;

/** @see ru.tandemservice.uni.entity.employee.pps.gen.EmployeePost4PpsEntryGen */
public class EmployeePost4PpsEntry extends EmployeePost4PpsEntryGen
{
    public EmployeePost4PpsEntry() {}

    public EmployeePost4PpsEntry(PpsEntryByEmployeePost ppsEntry, EmployeePost employeePost)
    {
        setPpsEntry(ppsEntry);
        setEmployeePost(employeePost);
    }
}