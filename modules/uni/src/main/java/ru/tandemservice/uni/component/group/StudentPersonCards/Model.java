/**
 * $Id$
 */
package ru.tandemservice.uni.component.group.StudentPersonCards;

import java.util.List;

import org.tandemframework.core.component.State;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author dseleznev
 * Created on: 08.10.2008
 */
@State(keys = "groupId", bindings = "group.id")
public class Model
{
    private Group _group = new Group();
    private Student _student;
    private List<Student> _studentsList;
    private IGrouptStudentPersonCardPrintFactory _printFactory;


    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        this._student = student;
    }

    public List<Student> getStudentsList()
    {
        return _studentsList;
    }

    public void setStudentsList(List<Student> studentsList)
    {
        this._studentsList = studentsList;
    }

    public IGrouptStudentPersonCardPrintFactory getPrintFactory()
    {
        return _printFactory;
    }

    public void setPrintFactory(IGrouptStudentPersonCardPrintFactory printFactory)
    {
        _printFactory = printFactory;
    }
}