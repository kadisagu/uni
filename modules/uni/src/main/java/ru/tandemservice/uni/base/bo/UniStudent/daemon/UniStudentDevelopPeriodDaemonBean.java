/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.daemon;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.coalesce;

/**
 * FIXME надо переделать расширение демона из других модулей (подменить дао может только один)
 * FIXME листенер должен работать через контекст и только с теми объектыми, которые изменялись (если я редактирую 1 студента - только его и надо трогать)
 *
 * @author Alexey Lopatin
 * @since 17.02.2016
 */
public class UniStudentDevelopPeriodDaemonBean extends UniBaseDao implements IUniStudentDevelopPeriodDaemonBean
{
    public static final String GLOBAL_DAEMON_LOCK = UniStudentDevelopPeriodDaemonBean.class.getSimpleName() + ".lock";

    public static final SyncDaemon DAEMON = new SyncDaemon(UniStudentDevelopPeriodDaemonBean.class.getName(), 120, GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            Debug.begin("updateDevelopPeriodAuto");
            try
            {
                IUniStudentDevelopPeriodDaemonBean dao = IUniStudentDevelopPeriodDaemonBean.instance.get();
                dao.doUpdateDevelopPeriodAuto();
            }
            finally {
                Debug.end();
            }
        }
    };

    @Override
    protected void initDao()
    {
        // изменение полей «НПП», «Срок освоения» студента
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, Student.class, DAEMON.getAfterCompleteWakeUpListener(),
                ImmutableSet.of(Student.L_EDUCATION_ORG_UNIT,
                                Student.L_DEVELOP_PERIOD)
        );
    }

    @Override
    public void doUpdateDevelopPeriodAuto()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .where(eq(property("s", Student.archival()), value(Boolean.FALSE)));

        addAdditionalExpression(builder);

        builder.column(property("s.id"), "s_id");
        builder.column(coalesce(getDevelopPeriodColumns()), "s_dp");

        new DQLUpdateBuilder(Student.class)
                .fromDataSource(builder.buildQuery(), "x")
                .where(eq(property(Student.id()), property("x.s_id")))
                .where(ne(property(Student.developPeriodAuto()), property("x.s_dp")))
                .set(Student.developPeriodAuto().s(), property("x.s_dp"))
                .createStatement(getSession()).execute();
    }

    protected void addAdditionalExpression(DQLSelectBuilder builder)
    {
        builder.joinEntity("s", DQLJoinType.inner, EducationOrgUnit.class, "eou", eq(property("eou.id"), property("s", Student.educationOrgUnit().id())));
    }

    protected IDQLExpression[] getDevelopPeriodColumns()
    {
        return new IDQLExpression[]{
                property("s", Student.developPeriod()),
                property("eou", EducationOrgUnit.developPeriod())
        };
    }
}
