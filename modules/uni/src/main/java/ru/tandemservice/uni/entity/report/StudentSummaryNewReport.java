package ru.tandemservice.uni.entity.report;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.entity.report.gen.*;

/**
 * Сводка контингента студентов (по направлениям подготовки, форма 2)
 */
public class StudentSummaryNewReport extends StudentSummaryNewReportGen implements IStorableReport
{
    public static final String TEMPLATE_NAME = "studentSummary";
}