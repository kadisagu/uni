/* $Id: controllerAddEdit.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;


/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public class Controller extends DefaultCatalogAddEditController<DevelopGrid, Model, IDAO>
{

    public void onChangeTermOrDevelopPeriod(IBusinessComponent component)
    {

        String title;
        Model model = getModel(component);
        getDao().prepare(model);
        if (model.getDevelopPeriod()!=null&&model.getTerm()!=null)
            {
                Integer sessionNumber = model.getTerm().getIntValue();
                String wordSessionAltG = sessionNumber==1?"сессия":(sessionNumber<5?"сессии":"сессий");
                title = model.getDevelopPeriod().getTitle()+" (" + sessionNumber + " " + wordSessionAltG + ")";
            }
        else title = "";
        model.setTitle(title);
    }

    public void onSaveGrid(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);

        if (model.isEditForm())
        {
            model.getCatalogItem().setTitle(model.getTitle());
            getDao().update(model);
            deactivate(component);
            return;
        }

        for (int i = 0; i < model.getRowTermMap().size(); i++)
        {
            Term term = model.getCurrentTermList().get(i);
            if  (model.getRowTermMap().get(term.getIntValue()).getCurrentCourse()==null)
                errors.add("Поле «Курс» обязательно для заполнения.", "id_course_"+ term.getIntValue());
            if  (model.getRowTermMap().get(term.getIntValue()).getCurrentPart()==null)
                errors.add("Поле «Часть года» обязательно для заполнения.", "id_part_"+ term.getIntValue());
        }
        if (errors.hasErrors()) return;

        model.getCatalogItem().setDevelopPeriod(model.getDevelopPeriod());
        model.getCatalogItem().setTitle(model.getTitle());

        String meta = "{"+model.getCurrentTermList().get(0).getTitle() + ":" + model.getRowTermMap().get(model.getCurrentTermList().get(0).getIntValue()).getCurrentCourse().getTitle();
        for (int i = 1; i < model.getRowTermMap().size(); i++)
        {
            Term term = model.getCurrentTermList().get(i);
            meta += ", "+model.getCurrentTermList().get(i).getTitle() + ":" + model.getRowTermMap().get(term.getIntValue()).getCurrentCourse().getTitle();
        }

        model.getCatalogItem().setMeta(meta + "}");
        getDao().update(model);

        for (int i = 0; i < model.getRowTermMap().size(); i++)
        {
            DevelopGridTerm developGridTerm = new DevelopGridTerm();
            Term term = model.getCurrentTermList().get(i);
            developGridTerm.setTerm(term);
            developGridTerm.setCourse(model.getRowTermMap().get(term.getIntValue()).getCurrentCourse());
            developGridTerm.setPart(model.getRowTermMap().get(term.getIntValue()).getCurrentPart());
            developGridTerm.setDevelopGrid(getModel(component).getCatalogItem());
            getDao().save(developGridTerm);
        }

        deactivate(component);
    }
}
