/* $Id$ */
package ru.tandemservice.uni.base.bo.AcademyRename.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.orgstruct.AcademyRename;

/**
 * @author Andrey Avetisov
 * @since 09.07.2014
 */
@Configuration
public class AcademyRenamePub extends BusinessComponentManager
{
    public static final String ACADEMY_RENAME_DS = "academyRenameDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(this.searchListDS(ACADEMY_RENAME_DS, academyRenameDSColumns(), academyRenameDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint academyRenameDSColumns()
    {
        return columnListExtPointBuilder(ACADEMY_RENAME_DS)
                .addColumn(textColumn("previousFullTitle", AcademyRename.previousFullTitle()))
                .addColumn(textColumn("previousShortTitle", AcademyRename.previousShortTitle()))
                .addColumn(textColumn("date", AcademyRename.date()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("academyRenameDS.delete.alert", AcademyRename.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER)))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> academyRenameDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), AcademyRename.class)
                .order(AcademyRename.date())
                .pageable(true);
    }
}
