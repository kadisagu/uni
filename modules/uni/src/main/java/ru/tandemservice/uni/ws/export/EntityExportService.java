/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ws.export;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;

import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author agolubenko
 * @since Apr 16, 2010
 */
@WebService(serviceName = "EntityExportService")
@BindingType(value = SOAPBinding.SOAP11HTTP_MTOM_BINDING)
public class EntityExportService
{
    @WebMethod
    public byte[] getEntityData(String entityName, String[] properties, boolean onlyUserRecords) throws Exception
    {
        return UniDaoFacade.getUniExportDao().getEntityData(entityName, properties, onlyUserRecords);
    }
}
