/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.group.AbstractGroupList;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private Boolean _archival;
    private DynamicListDataSource<Group> _dataSource;
    private IDataSettings _settings;
    private IPrincipalContext _principalContext;

    //permission keys
    private String _addKey;
    private String _delKey;
    private String _editKey;
    private String _printStudentsGroupsListKey;

    //filter
    private ISelectModel _courseListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _producingOrgUnitListModel;
    private ISelectModel _educationLevelsModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developPeriodListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _endingYearListModel;

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Boolean isArchival()
    {
        return _archival;
    }

    public void setArchival(Boolean archival)
    {
        _archival = archival;
    }

    public DynamicListDataSource<Group> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Group> dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public String getAddKey()
    {
        return _addKey;
    }

    public void setAddKey(String addKey)
    {
        _addKey = addKey;
    }

    public String getDelKey()
    {
        return _delKey;
    }

    public void setDelKey(String delKey)
    {
        _delKey = delKey;
    }

    public String getEditKey()
    {
        return _editKey;
    }

    public void setEditKey(String editKey)
    {
        _editKey = editKey;
    }

    public String getPrintStudentsGroupsListKey()
    {
        return _printStudentsGroupsListKey;
    }

    public void setPrintStudentsGroupsListKey(String printStudentsGroupsListKey)
    {
        _printStudentsGroupsListKey = printStudentsGroupsListKey;
    }

    public ISelectModel getCourseListModel()
    {
        return _courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel)
    {
        _courseListModel = courseListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getProducingOrgUnitListModel()
    {
        return _producingOrgUnitListModel;
    }

    public void setProducingOrgUnitListModel(ISelectModel producingOrgUnitListModel)
    {
        _producingOrgUnitListModel = producingOrgUnitListModel;
    }

    public ISelectModel getEducationLevelsModel()
    {
        return _educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel)
    {
        _educationLevelsModel = educationLevelsModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getDevelopConditionListModel() { return _developConditionListModel; }
    public void setDevelopConditionListModel(ISelectModel developConditionListModel) { _developConditionListModel = developConditionListModel; }

    public ISelectModel getEndingYearListModel()
    {
        return _endingYearListModel;
    }

    public void setEndingYearListModel(ISelectModel endingYearListModel)
    {
        _endingYearListModel = endingYearListModel;
    }
}