/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol;

import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
public class SessionTransferProtocolDao extends UniBaseDao implements ISessionTransferProtocolDao
{
    @Override
    public ISingleSelectModel getSessionTransferProtocolModel(Student student)
    {
        return null;
    }

    @Override
    public PersonEduDocument getEduDocumentFromProtocol(String protocolNumber, Date protocolDate, Student student)
    {
        return null;
    }

    @Override
    public ISessionTransferProtocolProperty getSessionTransferProtocolDocument(String protocolNumber, Date protocolDate, Student student)
    {
        return null;
    }
}