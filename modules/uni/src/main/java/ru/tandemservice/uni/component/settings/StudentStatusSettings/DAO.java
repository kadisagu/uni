/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.settings.StudentStatusSettings;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<StudentStatus> dataSource = model.getDataSource();

        List<StudentStatus> list = getList(StudentStatus.class, StudentStatus.P_PRIORITY);

        dataSource.setCountRow(list.size());
        dataSource.setTotalSize(list.size());
        dataSource.createPage(list);
    }

    @Override
    public void doToggleActive(Long studentStatusId)
    {
        StudentStatus studentStatus = get(StudentStatus.class, studentStatusId);
        studentStatus.setActive(!studentStatus.isActive());
        update(studentStatus);
    }

    @Override
    public void doToggleUsedInSystem(Long studentStatusId)
    {
        StudentStatus studentStatus = get(StudentStatus.class, studentStatusId);
        studentStatus.setUsedInSystem(!studentStatus.isUsedInSystem());
        update(studentStatus);
    }

    @Override
    public void updatePriorityUp(Long studentStatusId)
    {
        changePriority(studentStatusId, true);
    }

    @Override
    public void updatePriorityDown(Long studentStatusId)
    {
        changePriority(studentStatusId, false);
    }

    private void changePriority(Long studentStatusId, boolean up)
    {
        UniBaseUtils.changePriority(StudentStatus.class, studentStatusId, StudentStatus.P_PRIORITY, up, getSession());
    }
}
