package ru.tandemservice.uni.dao.mdbio;

import com.healthmarketscience.jackcess.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uni.entity.catalog.*;

import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class CatalogIODao extends BaseIODao implements ICatalogIODao {



    @Override public ICatalogIO<CompensationType> catalogCompensationType() { return this.catalog(CompensationType.class, "title"); }
    @Override public ICatalogIO<DiplomaQualifications> catalogDiplomaQualifications() { return this.catalog(DiplomaQualifications.class, "title"); }
    @Override public ICatalogIO<GraduationHonour> catalogGraduationHonour() { return this.catalog(GraduationHonour.class, "title"); }
    @Override public ICatalogIO<EduDocumentKind> catalogEduDocumentKind() { return this.catalog(EduDocumentKind.class, EduDocumentKind.P_SHORT_TITLE); }
    @Override public ICatalogIO<EduLevel> catalogEduLevel() { return this.catalog(EduLevel.class, EduLevel.P_SHORT_TITLE); }
    @Override public ICatalogIO<EducationalInstitutionTypeKind> catalogEducationalInstitutionTypeKind() { return this.catalog(EducationalInstitutionTypeKind.class, "shortTitle"); }
    @Override public ICatalogIO<EducationDocumentType> catalogEducationDocumentType() { return this.catalog(EducationDocumentType.class, "title"); }
    @Override public ICatalogIO<EducationLevelStage> catalogEducationLevelStage() { return this.catalog(EducationLevelStage.class, "title"); }
    @Override public ICatalogIO<EmployeeSpeciality> catalogEmployeeSpeciality() { return this.catalog(EmployeeSpeciality.class, "title"); }
    @Override public ICatalogIO<FamilyStatus> catalogFamilyStatus() { return this.catalog(FamilyStatus.class, "title"); }
    @Override public ICatalogIO<FlatPresence> catalogFlatPresence() { return this.catalog(FlatPresence.class, "title"); }
    @Override public ICatalogIO<ForeignLanguage> catalogForeignLanguage() { return this.catalog(ForeignLanguage.class, "title"); }
    @Override public ICatalogIO<ForeignLanguageSkill> catalogForeignLanguageSkill() { return this.catalog(ForeignLanguageSkill.class, "title"); }
    @Override public ICatalogIO<PensionType> catalogPensionType() { return this.catalog(PensionType.class, "title"); }
    @Override public ICatalogIO<RelationDegree> catalogRelationDegree() { return this.catalog(RelationDegree.class, "title"); }
    @Override public ICatalogIO<Sex> catalogSex() { return this.catalog(Sex.class, "title"); }
    @Override public ICatalogIO<SportRank> catalogSportRank() { return this.catalog(SportRank.class, "title"); }
    @Override public ICatalogIO<SportType> catalogSportType() { return this.catalog(SportType.class, "title"); }
    @Override public ICatalogIO<StudentCategory> catalogStudentCategory() { return this.catalog(StudentCategory.class, "title"); }
    @Override public ICatalogIO<StudentStatus> catalogStudentStatusCategory() { return this.catalog(StudentStatus.class, "title"); }
    @Override public ICatalogIO<Benefit> catalogBenefitCategory() { return this.catalog(Benefit.class, "title"); }
    @Override public ICatalogIO<IdentityCardType> catalogIdentityCardType() { return this.catalog(IdentityCardType.class, "title"); }

    @Override public ICatalogIO<DevelopForm> catalogDevelopForm() { return this.catalog(DevelopForm.class, "title"); }
    @Override public ICatalogIO<DevelopCondition> catalogDevelopCondition() { return this.catalog(DevelopCondition.class, "title"); }
    @Override public ICatalogIO<DevelopTech> catalogDevelopTech() { return this.catalog(DevelopTech.class, "title"); }
    @Override public ICatalogIO<DevelopGrid> catalogDevelopGrid() { return this.catalog(DevelopGrid.class, "title"); }


    @Override
    public Map<EduInstitution, String> export_catalogEducationalInstitution(final Database mdb) throws IOException {
        if (null != mdb.getTable("eduinstitution_t")) { return new HashMap<>(); }

        final Map<EducationalInstitutionTypeKind, String> educationalInstitutionTypeKind = this.catalogEducationalInstitutionTypeKind().export(mdb);

        final Table eduinstitution_t = new TableBuilder("eduinstitution_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("title", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("type_kind", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("address_country_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("address_settlement_p", DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
        .toTable(mdb);

        final Session session = this.getSession();
        final Map<EduInstitution, String> eduInstitution2HexMap = new HashMap<>();
        for (final List<Long> idsPart : getIdListPartition(EduInstitution.class)) {
            for (final EduInstitution eduInstitution: CatalogIODao.this.getList(EduInstitution.class, idsPart)) {

                final String hexId = Long.toHexString(eduInstitution.getId());
                eduInstitution2HexMap.put(eduInstitution, hexId);

                eduinstitution_t.addRow(
                        hexId,
                        eduInstitution.getTitle(),
                        educationalInstitutionTypeKind.get(eduInstitution.getEduInstitutionKind()),
                        AddressIODao.getCountryString(eduInstitution.getAddress().getCountry()),
                        AddressIODao.getSettlementString(eduInstitution.getAddress().getSettlement())
                );
            }
            session.clear();
        }

        return eduInstitution2HexMap;
    }

    @Override
    public Map<String, EduInstitution> import_catalogEducationalInstitution(final Database mdb) throws IOException
    {
        final Table eduinstitution_t = mdb.getTable("eduinstitution_t");

        final Session session = this.getSession();
        final IAddressIODao addressIO = IAddressIODao.instance.get();
        final Map<String, EducationalInstitutionTypeKind> eduInstitutionTypeKindMap = this.catalogEducationalInstitutionTypeKind().lookup(true);

        final IFieldPropertyMeta titleProperty = getFieldProperty(EduInstitution.class, EduInstitution.P_TITLE);

        final List<EduInstitution> eduInstList = new DQLSelectBuilder().fromEntity(EduInstitution.class, "x").column(property("x"))
                .createStatement(session).list();
        // подготавливаем мапу существующих ОУ в рамках вида ОУ, страны и населенного пункта ищется ОУ с таким же названием (без учета регистра и без пробелов), чтобы не создать в системе дубли ОУ
        // и список кодов существущих ОУ для генерации новых
        final List<String> eduInstCodeList = new ArrayList<>();
        final Map<MultiKey, EduInstitution> dbEduInstitutionMap = new HashMap<>();
        for (EduInstitution eduInst : eduInstList)
        {
            final MultiKey key = new MultiKey(eduInst.getTitle().trim().replaceAll(" ", "").toUpperCase(), eduInst.getEduInstitutionKind(), eduInst.getAddress().getCountry(), eduInst.getAddress().getSettlement());
            dbEduInstitutionMap.put(key, eduInst);
            eduInstCodeList.add(eduInst.getCode());
        }
        // мапа уже обработаных ОУ из mdb в рамках вида ОУ, страны и населенного пункта ищется ОУ с таким же названием (без учета регистра и без пробелов), чтобы не создать в системе дубли ОУ
        final Map<MultiKey, EduInstitution> processedEduInstitutionMap = new HashMap<>();

        final Map<String, EduInstitution> resultEduInstitutionMap = new HashMap<>();
        execute(eduinstitution_t, 256, "Образовательные учреждения персон (1/3)", new BatchUtils.Action<Map<String, Object>>()
        {
            @Override
            public void execute(final Collection<Map<String, Object>> rows)
            {
                final Map<String, Object> localEduInstMap = new HashMap<>();
                // подготавливаем строки из файла
                for (final Map<String, Object> row : rows)
                {
                    final String hexId = (String) row.get("id");
                    if (StringUtils.isBlank(hexId))
                        throw new IOFatalException("Key for catalog 'eduinstitution_t' must not be null.");

                    final Long id = tryParseHexId(hexId);
                    if (id != null)
                        localEduInstMap.put(hexId, id);
                }

                // простовляем объекты из базы для тех строк для которых они есть
                final List<EduInstitution> list = new DQLSelectBuilder().fromEntity(EduInstitution.class, "x").column(property("x"))
                        .where(in(property("x.id"), localEduInstMap.values()))
                        .createStatement(session).list();
                for (EduInstitution person : list)
                {
                    localEduInstMap.put(Long.toHexString(person.getId()), person);
                }


                // обновляем или создаем Обр. уч.
                for (final Map<String, Object> row : rows)
                {
                    final String hexId = (String) row.get("id");
                    if (StringUtils.isBlank(hexId))
                        throw new IOFatalException("Field value 'id' for catalog 'eduinstitution_t' must not be null.");


                    final String context = "eduinstitution_t[id=" +  hexId + "]";

                    try
                    {
                        EduInstitution eduInstitution = createEduInstitution(localEduInstMap, hexId);

                        if (eduInstitution.getId() != null)
                        {
                            resultEduInstitutionMap.put(hexId, eduInstitution);
                            continue;
                        }

                        final String title = trimmedString(row, "title", context, titleProperty);
                        final EducationalInstitutionTypeKind typeKind = eduInstitutionTypeKindMap.get((String) row.get("type_kind"));
                        final AddressDetailed address = createAddress(eduInstitution, row);

                        final MultiKey key = new MultiKey(title.replaceAll(" ", "").toUpperCase(), typeKind, address.getCountry(), address.getSettlement());

                        if (processedEduInstitutionMap.containsKey(key))
                        {
                            eduInstitution = processedEduInstitutionMap.get(key);
                        }
                        else
                        {
                            if (dbEduInstitutionMap.containsKey(key))
                            {
                                eduInstitution = dbEduInstitutionMap.get(key);
                            }
                            else
                            {
                                eduInstitution.setTitle(title);
                                eduInstitution.setEduInstitutionKind(typeKind);
                                eduInstitution.setAddress(address);

                                session.saveOrUpdate(eduInstitution);
                            }

                            processedEduInstitutionMap.put(key, eduInstitution);
                        }

                        resultEduInstitutionMap.put(hexId, eduInstitution);
                    }
                    catch (Exception e)
                    {
                        throw new IOFatalException("Error with processing: " + context + ": " + e.getMessage(), e);
                    }
                }

                System.err.print(".");
                session.flush();
                session.clear();
            }

            private AddressDetailed createAddress(EduInstitution eduInstitution, Map<String, Object> row)
            {
                AddressDetailed address = eduInstitution.getAddress();

                final AddressCountry country = addressIO.findCountry((String) row.get("address_country_p"));
                final AddressItem settlement = addressIO.findSettlement(country, (String) row.get("address_settlement_p"), true);

                AddressDetailed deleteAddress = null;

                if(address == null)
                {
                    if(IKladrDefines.RUSSIA_COUNTRY_CODE == country.getCode())
                    {
                        address = new AddressInter();
                        address.setCountry(country);
                        address.setSettlement(settlement);
                    }
                    else
                    {
                        address = new AddressInter();
                        address.setCountry(country);
                        address.setSettlement(settlement);
                    }
                }
                else
                {
                    if(IKladrDefines.RUSSIA_COUNTRY_CODE == country.getCode())
                    {
                        if(address instanceof AddressRu)
                        {
                            address.setSettlement(settlement);
                        }
                        else
                        {
                            address = new AddressInter();
                            address.setCountry(country);
                            address.setSettlement(settlement);

                            deleteAddress = eduInstitution.getAddress();
                            eduInstitution.setAddress(null);
                        }
                    }
                    else
                    {
                        if(address instanceof AddressRu)
                        {
                            address = new AddressInter();
                            address.setCountry(country);
                            address.setSettlement(settlement);

                            deleteAddress = eduInstitution.getAddress();
                            eduInstitution.setAddress(null);
                        }
                        else
                        {
                            address.setCountry(country);
                            address.setSettlement(settlement);
                        }
                    }
                }

                if(deleteAddress != null)
                    getSession().delete(deleteAddress);

                if (address.getId() == null)
                    getSession().save(address);

                return address;
            }

            private EduInstitution createEduInstitution(Map<String, Object> localEduInstMap, String hexId)
            {
                Object object = localEduInstMap.get(hexId);
                if (!(object instanceof EduInstitution))
                {
                    localEduInstMap.put(hexId, object = new EduInstitution());
                    ((EduInstitution)object).setCode(newCode(eduInstCodeList));
                }
                return (EduInstitution) object;
            }

            private String newCode(List<String> eduInstCodeList)
            {
                int code = eduInstCodeList.size();

                while (eduInstCodeList.contains(String.valueOf(code)))
                {
                    code++;
                }

                eduInstCodeList.add(String.valueOf(code));

                return String.valueOf(code);
            }
        });

        return resultEduInstitutionMap;
    }
}
