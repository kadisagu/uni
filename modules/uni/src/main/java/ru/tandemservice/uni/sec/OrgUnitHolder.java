package ru.tandemservice.uni.sec;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.hibsupport.entity.EntityHolder;

/**
 * @author vdanilov
 */
public class OrgUnitHolder extends EntityHolder<OrgUnit>
{
    private final CommonPostfixPermissionModelBase secModel = new CommonPostfixPermissionModelBase() {
        @Override public String getPostfix() { return getSecModelPostfix(); }
    };

    public CommonPostfixPermissionModelBase getSecModel() { return this.secModel; }

    protected String getSecModelPostfix()
    {
        final OrgUnit value = getValue();
        if (null == value) { return null; }

        return OrgUnitSecModel.getPostfix(value);
    }
}
