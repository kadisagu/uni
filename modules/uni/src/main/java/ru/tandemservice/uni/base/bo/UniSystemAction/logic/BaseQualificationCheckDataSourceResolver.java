/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSystemAction.logic;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.EntityDebug.CommonEntityDebug;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author Nikolay Fedorovskih
 * @since 07.12.2015
 */
public abstract class BaseQualificationCheckDataSourceResolver
{
    private DynamicListDataSource<IEntity> ds;

    public String getName() {
        return this.getClass().getSimpleName();
    }

    public abstract DQLSelectBuilder getDQL();

    public DynamicListDataSource<IEntity> init(IBusinessComponent component) {
        ds = new DynamicListDataSource<>(component, c -> DataAccessServices.dao().getCalculatedValue(session -> {
            UniBaseUtils.createPage(ds, getDQL(), session);
            return null;
        }));
        ds.addColumn(new PublisherLinkColumn("Объект", "title").setOrderable(false));
        ds.addColumn(new PublisherLinkColumn("Просмотр объекта", "id")
                            .setResolver(new DefaultPublisherLinkResolver(CommonEntityDebug.class.getSimpleName())
                            {
                                @Override
                                public Object getParameters(IEntity entity)
                                {
                                    return ParametersMap.createWith("param", String.valueOf(entity.getId()));
                                }
                            })
                            .setOrderable(false));
        return ds;
    }

    public abstract int getNumber();

    public abstract String getTitle();

    public abstract String getDescription();
}