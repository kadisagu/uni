/**
 *$Id:$
 */
package ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.datasource.column.IIndicatorColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;

import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 01.11.12
 */
public abstract class AbstractUniStudentList extends BusinessComponentManager
{
    // список колонок, которые надо поместить раньше всех остальных
    public static final String PRIORITY_COLUMNS = "AbstractUniStudentList.priorityColumns";

    // studentSearchListColumns
    public static final String STUDENT_COLUMN = "student";
    public static final String FIO_COLUMN = "fio";
    public static final String SEX_COLUMN = "sex";
    public static final String BIRTH_DATE_COLUMN = "birthDate";
    public static final String PASSPORT_COLUMN = "passport";
    public static final String CITIZENSHIP_COLUMN = "citizenship";
    public static final String PERSONAL_NUMBER_COLUMN = "personalNumber";
    public static final String PERSONAL_FILE_NUMBER_COLUMN = "personalFileNumber";
    public static final String BOOK_NUMBER_COLUMN = "bookNumber";
    public static final String ENTRANCE_YEAR_COLUMN = "entranceYear";
    public static final String STATUS_COLUMN = "status";
    public static final String COURSE_COLUMN = "course";
    public static final String COMPENSATION_TYPE_COLUMN = "compensationType";
    public static final String GROUP_COLUMN = "group";
    public static final String STUDENT_CATEGORY_COLUMN = "studentCategory";
    public static final String SPECIAL_PURPOSE_RECRUIT_COLUMN = "specialPurposeRecruit";
    public static final String FORMATIVE_ORG_UNIT_COLUMN = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_COLUMN = "territorialOrgUnit";
    public static final String PRODUCTIVE_ORG_UNIT_COLUMN = "productiveOrgUnit";
    public static final String SPECIALITY_COLUMN = "specialityColumn";
    public static final String SPECIALIZATION_COLUMN = "specializationColumn";
    public static final String QUALIFICATION_COLUMN = "qualification";
    public static final String ORIENTATION_COLUMN = "orientation";
    public static final String DEVELOP_FORM_COLUMN = "developForm";
    public static final String DEVELOP_CONDITION_COLUMN = "developCondition";
    public static final String DEVELOP_PERIOD_COLUMN = "developPeriod";
    public static final String DEVELOP_PERIOD_AUTO_COLUMN = "developPeriodAuto";
    public static final String DEVELOP_COMBINATION_COLUMN = "developCombination";
    public static final String STUDENT_ACTIVE_CUSTOM_STATE_COLUMN = "studentActiveCustomState";

    public static final String DEVELOP_PERIOD_AUTO_DS = "developPeriodAutoDS";

    public static final String STUDENT_SEARCH_LIST_DS = "studentSearchListDS";

    protected IPresenterExtPointBuilder addUniUtilEduOrgUnitAddons(IPresenterExtPointBuilder builder)
    {
        return builder
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class));
    }

    public PresenterExtPoint presenterExtPoint(IPresenterExtPointBuilder builder)
    {
        return addUniUtilEduOrgUnitAddons(builder)
        .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DEVELOP_PERIOD_AUTO_DS, getName(), DevelopPeriod.defaultSelectDSHandler(getName())))
        .addDataSource(UniStudentManger.instance().studentStatusOptionDSConfig())
        .addDataSource(UniStudentManger.instance().studentStatusDSConfig())
        .addDataSource(UniStudentManger.instance().courseDSConfig())
        .addDataSource(UniStudentManger.instance().groupDSConfig())
        .addDataSource(UniStudentManger.instance().compensationTypeDSConfig())
        .addDataSource(UniStudentManger.instance().qualificationDSConfig())
        .addDataSource(UniStudentManger.instance().studentCategoryDSConfig())
        .addDataSource(CommonManager.instance().yesNoDSConfig())
        .addDataSource(UniStudentManger.instance().endingYearDSConfig())
        .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
        .addDataSource(UniStudentManger.instance().countryDSConfig())
        .create();
    }

    /**
     * @return билдер со списком колонок по умолчанию,
     * колонки уже покастомизированны методом customizeStudentSearchListColumns()
     */
    protected final IColumnListExtPointBuilder createStudentSearchListColumnsBuilder()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STUDENT_SEARCH_LIST_DS);

        List<ColumnBase> columnList = prepareColumnList();
        columnList = customizeStudentSearchListColumns(columnList);
        for (ColumnBase item : columnList)
            builder.addColumn(item);

        return builder;
    }

    /**
     * Здесь производим кастомизацию колонок: удаление, добавление.
     * <p><b>Note:</b> Добавить колонки можно и после создания билдера,
     * а вот удалять надо тут.
     */
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList) {
        return columnList;
    }

    /**
     * Добавляет колонки(колонку).
     * @param columns колонки(колонка)
     */
    protected final void insertColumns(List<ColumnBase> columnList, ColumnBase... columns) {
        Collections.addAll(columnList, columns);
    }

    /**
     * Удаляет колонки(колонку).
     * @param names имя колонок(колонки)
     */
    protected final void removeColumns(List<ColumnBase> columnList, String... names) {
        final Set<String> set = new HashSet<>(Arrays.asList(names));
        for (Iterator<ColumnBase> it=columnList.iterator(); it.hasNext(); ) {
            if (set.contains(it.next().getName())) { it.remove(); }
        }
    }

    /**
     * @return Список колонок списка Студентов по умолчанию.
     */
    protected List<ColumnBase> prepareColumnList()
    {
        List<ColumnBase> columnList = new ArrayList<>();
        IMergeRowIdResolver merger = newMergeRowIdResolver();
        columnList.add(newStudentIndicatorColumnBuilder().create());
        columnList.add(newFioColumnBuilder(merger).create());
        columnList.add(newSexColumnBuilder(merger).create());
        columnList.add(newBirthDateColumnBuilder(merger).create());
        columnList.add(newPassportColumnBuilder(merger).create());
        columnList.add(newCitizenshipColumnBuilder(merger).create());
        columnList.add(newPersonalNumberColumnBuilder().create());
        columnList.add(newPersonalFileNumberColumnBuilder().create());
        columnList.add(newBookNumberColumnBuilder().create());
        columnList.add(newEntranceYearColumnBuilder().create());
        columnList.add(newStudentActiveCustomStateColumnBuilder(merger).create());
        columnList.add(newStatusColumnBuilder().create());
        columnList.add(newCourseColumnBuilder().create());
        columnList.add(newCompensationTypeColumnBuilder().create());
        columnList.add(newGroupColumnBuilder().create());
        columnList.add(newCategoryColumnBuilder().create());
        columnList.add(newSpecialPurposeRecruitColumnBuilder().create());
        columnList.add(newFormativeOrgUnitColumnBuilder().create());
        columnList.add(newTerritorialOrgUnitColumnBuilder().create());
        columnList.add(newProductiveOrgUnitColumnBuilder().create());
        columnList.add(newSpecialityColumnBuilder().create());
        columnList.add(newSpecializationColumnBuilder().create());
        columnList.add(newOrientationColumnBuilder().create());
        columnList.add(newQualificationColumnBuilder().create());
        columnList.add(newDevelopFormColumnBuilder().create());
        columnList.add(newDevelopConditionColumnBuilder().create());
        columnList.add(newDevelopPeriodColumnBuilder().create());
        columnList.add(newDevelopPeriodAutoColumnBuilder().create());

        // все колонки, которые перечисленны в проперте - переездают в начало (в указанном порядке), остальные - следуют за ними в порядке объявления
        final String priorityColumnsProperty = StringUtils.trimToNull(ApplicationRuntime.getProperty(PRIORITY_COLUMNS));
        if (null != priorityColumnsProperty) {
            final List<String> list = new ArrayList<>();
            for (String column: StringUtils.split(priorityColumnsProperty, ',')) {
                column = StringUtils.trimToNull(column);
                if (null != column) { list.add(column); }
            }

            Collections.sort(columnList, (o1, o2) -> {
                int i1 = list.indexOf(o1.getName());
                if (i1 < 0) { i1 = Integer.MAX_VALUE; }
                int i2 = list.indexOf(o2.getName());
                if (i2 < 0) { i2 = Integer.MAX_VALUE; }
                return Integer.compare(i1, i2);
            });
        }

        return columnList;
    }

    protected ITextColumnBuilder newDevelopCombinationColumnBuilder() {
        return textColumn(DEVELOP_COMBINATION_COLUMN, Student.educationOrgUnit().developCombinationTitle());
    }

    protected ITextColumnBuilder newDevelopPeriodColumnBuilder() {
        return textColumn(DEVELOP_PERIOD_COLUMN, Student.educationOrgUnit().developPeriod().title());
    }

    protected ITextColumnBuilder newDevelopPeriodAutoColumnBuilder() {
        return textColumn(DEVELOP_PERIOD_AUTO_COLUMN, Student.developPeriodAuto().title());
    }

    protected ITextColumnBuilder newDevelopConditionColumnBuilder() {
        return textColumn(DEVELOP_CONDITION_COLUMN, Student.educationOrgUnit().developCondition().title());
    }

    protected ITextColumnBuilder newDevelopFormColumnBuilder() {
        return textColumn(DEVELOP_FORM_COLUMN, Student.educationOrgUnit().developForm().title());
    }

    protected ITextColumnBuilder newQualificationColumnBuilder() {
        return textColumn(QUALIFICATION_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().assignedQualification().title());
    }

    protected ITextColumnBuilder newOrientationColumnBuilder() {
        return textColumn(ORIENTATION_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().programOrientation().shortTitle());
    }

    protected ITextColumnBuilder newSpecializationColumnBuilder() {
        return textColumn(SPECIALIZATION_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle());
    }

    protected ITextColumnBuilder newSpecialityColumnBuilder() {
        return textColumn(SPECIALITY_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectWithCodeIndexAndGenTitle());
    }

    protected ITextColumnBuilder newProductiveOrgUnitColumnBuilder() {
        return textColumn(PRODUCTIVE_ORG_UNIT_COLUMN, Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fullTitle()).order();
    }

    protected ITextColumnBuilder newTerritorialOrgUnitColumnBuilder() {
        return textColumn(TERRITORIAL_ORG_UNIT_COLUMN, Student.educationOrgUnit().territorialOrgUnit().territorialFullTitle()).order();
    }

    protected ITextColumnBuilder newFormativeOrgUnitColumnBuilder() {
        return textColumn(FORMATIVE_ORG_UNIT_COLUMN, Student.educationOrgUnit().formativeOrgUnit().fullTitle()).order();
    }

    protected ITextColumnBuilder newSpecialPurposeRecruitColumnBuilder() {
        return textColumn(SPECIAL_PURPOSE_RECRUIT_COLUMN, Student.targetAdmission()).formatter(YesNoFormatter.INSTANCE);
    }

    protected ITextColumnBuilder newCategoryColumnBuilder() {
        return textColumn(STUDENT_CATEGORY_COLUMN, Student.studentCategory().title());
    }

    protected ITextColumnBuilder newGroupColumnBuilder() {
        return textColumn(GROUP_COLUMN, Student.group().title()).order();
    }

    protected ITextColumnBuilder newCompensationTypeColumnBuilder() {
        return textColumn(COMPENSATION_TYPE_COLUMN, Student.compensationType().shortTitle()).order();
    }

    protected ITextColumnBuilder newCourseColumnBuilder() {
        return textColumn(COURSE_COLUMN, Student.course().title()).order();
    }

    protected ITextColumnBuilder newStatusColumnBuilder() {
        return textColumn(STATUS_COLUMN, Student.status().title()).order();
    }

    protected ITextColumnBuilder newEntranceYearColumnBuilder() {
        return textColumn(ENTRANCE_YEAR_COLUMN, Student.entranceYear()).order();
    }

    protected ITextColumnBuilder newBookNumberColumnBuilder() {
        return textColumn(BOOK_NUMBER_COLUMN, Student.bookNumber()).order();
    }

    protected ITextColumnBuilder newPersonalFileNumberColumnBuilder() {
        return textColumn(PERSONAL_FILE_NUMBER_COLUMN, Student.personalFileNumber());
    }

    protected ITextColumnBuilder newPersonalNumberColumnBuilder() {
        return textColumn(PERSONAL_NUMBER_COLUMN, Student.personalNumber()).order();
    }

    protected ITextColumnBuilder newCitizenshipColumnBuilder(IMergeRowIdResolver merger) {
        ITextColumnBuilder builder = textColumn(CITIZENSHIP_COLUMN, Student.person().identityCard().citizenship().title()).order();
        if (null == merger) { return builder; }
        return builder.merger(merger);
    }

    protected ITextColumnBuilder newPassportColumnBuilder(IMergeRowIdResolver merger) {
        ITextColumnBuilder builder = textColumn(PASSPORT_COLUMN, Student.person().identityCard().fullNumber()).order();
        if (null == merger) { return builder; }
        return builder.merger(merger);
    }

    protected ITextColumnBuilder newBirthDateColumnBuilder(IMergeRowIdResolver merger) {
        ITextColumnBuilder builder = textColumn(BIRTH_DATE_COLUMN, Student.person().identityCard().birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order();
        if (null == merger) { return builder; }
        return builder.merger(merger);
    }

    protected ITextColumnBuilder newSexColumnBuilder(IMergeRowIdResolver merger) {
        ITextColumnBuilder builder = textColumn(SEX_COLUMN, Student.person().identityCard().sex().shortTitle()).order();
        if (null == merger) { return builder; }
        return builder.merger(merger);
    }

    protected IPublisherColumnBuilder newFioColumnBuilder(IMergeRowIdResolver merger) {
        IPublisherColumnBuilder builder = publisherColumn(FIO_COLUMN, Student.person().fullFio()).publisherLinkResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Student student = ((DataWrapper) entity).getWrapped();
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, student != null ? student.getId() : null).add("selectedStudentTab", "studentTab");
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return null;
            }
        })
        .order()
        .required(true);
        if (null == merger) { return builder; }
        return builder.merger(merger);
    }

    protected IPublisherColumnBuilder newGroupPublisherColumnBuilder()
    {
        IPublisherColumnBuilder builder = publisherColumn(AbstractUniStudentList.GROUP_COLUMN, Student.group().title()).publisherLinkResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Student student = ((DataWrapper) entity).getWrapped();
                Group group = student == null ? null : student.getGroup();
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, group == null ? null : group.getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return null;
            }
        });

        return builder.order();
    }

    protected IIndicatorColumnBuilder newStudentIndicatorColumnBuilder() {
        return indicatorColumn(STUDENT_COLUMN).defaultIndicatorItem(new IndicatorColumn.Item("student"));
    }

    protected ITextColumnBuilder newStudentActiveCustomStateColumnBuilder(IMergeRowIdResolver merger) {
        ITextColumnBuilder builder = textColumn(STUDENT_ACTIVE_CUSTOM_STATE_COLUMN, STUDENT_ACTIVE_CUSTOM_STATE_COLUMN).formatter(new StudentCustomStateCollectionFormatter());
        if (null == merger) { return builder; }
        return builder.merger(merger);
    }

    /**
     * @return Резолвер для мерджа строк в списке Студентов.
     */
    protected IMergeRowIdResolver newMergeRowIdResolver()
    {
        return entity -> entity.getId().toString();
    }
}
