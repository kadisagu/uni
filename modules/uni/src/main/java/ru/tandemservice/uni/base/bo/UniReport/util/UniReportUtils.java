/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReport.util;

import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.exception.GeneralPlatformException;
import org.tandemframework.core.util.SafeSimpleDateFormat;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import org.tandemframework.shared.image.stamp.*;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.io.*;
import java.util.List;

/**
 * @author oleyba
 * @since 10/23/13
 */
public class UniReportUtils
{
    public static final SafeSimpleDateFormat DATE_FORMAT = new SafeSimpleDateFormat("dd_MM_yyyy");
    
    public static String createFilename(String extension, IStorableReport report) {
        return "Report_" + DATE_FORMAT.format(report.getFormingDate()) + "." + extension;
    }

    /**
     * @param reportId id хранимого отчета - IEntity должен удовлетворять интерфейсу {@link ru.tandemservice.uni.IStorableReport}
     */
    public static IDocumentRenderer createRenderer(Long reportId) {
        final IStorableReport report = UniDaoFacade.getCoreDao().getNotNull(reportId);
        if (report.getContent().getContent() == null)
            throw new ApplicationException("Файл отчета пуст.");
        return new CommonBaseRenderer()
            .contentType(CommonBaseUtil.getContentType(report.getContent(), DatabaseFile.CONTENT_TYPE_APPLICATION_RTF))
            .fileName((String) ObjectUtils.defaultIfNull(report.getContent().getFilename(), "report.rtf"))
            .document(report.getContent().getContent());
    }

    // todo план рефакторинга в задаче SH-1062
    /**
     * @deprecated Используйте {@link UniReportUtils#createFilename(String, ru.tandemservice.uni.IStorableReport)} и заполняйте filename при создании DatabaseFile для отчета - а затем используйте {@link UniReportUtils#createRenderer(Long)}
     * @param reportId id хранимого отчета - IEntity должен удовлетворять интерфейсу {@link ru.tandemservice.uni.IStorableReport}
     * @param extension расширение для файла отчета
     */
    public static IDocumentRenderer createRenderer(Long reportId, String extension) {
        final IStorableReport report = UniDaoFacade.getCoreDao().getNotNull(reportId);
        if (report.getContent().getContent() == null)
            throw new ApplicationException("Файл отчета пуст.");
        return new CommonBaseRenderer()
            .contentType(CommonBaseUtil.getContentType(extension, DatabaseFile.CONTENT_TYPE_APPLICATION_RTF))
            .fileName(createFilename(extension, report))
            .document(report.getContent().getContent());
    }

    public static IDocumentRenderer createConvertingToPdfRenderer(String fileName, RtfDocument document)
    {
        RtfUtil.optimizeFontAndColorTable(document);
        return createConvertingToPdfRenderer(fileName, RtfUtil.toByteArray(document));
    }

    public static IDocumentRenderer createConvertingToPdfRenderer(String fileName, RtfDocument document, final String barcode, final boolean addNumOfPages)
    {
        RtfUtil.optimizeFontAndColorTable(document);
        return createConvertingToPdfRenderer(fileName, RtfUtil.toByteArray(document), barcode, addNumOfPages);
    }

    public static IDocumentRenderer createConvertingToPdfRenderer(String fileName, byte[] data)
    {
        return createConvertingToPdfRenderer(fileName, data, null, false);
    }

    public static IDocumentRenderer createConvertingToPdfRenderer(String fileName, byte[] data, final String barcode, final boolean addNumOfPages)
    {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OpenOfficeServiceUtil.convertRtfToPdf(new ByteArrayInputStream(data), baos);
        return new CommonBaseRenderer() {
            @Override
            public void render(OutputStream outputStream)
            {
                writePdfToOutputStream(baos, outputStream, barcode, addNumOfPages);
            }
        }.pdf().fileName(fileName);
    }

    private static void writePdfToOutputStream(ByteArrayOutputStream baos, OutputStream outputStream, String _barcode, boolean _addNumberOfPages)
    {
        if(!StringUtils.isEmpty(_barcode))
        {
            InputStream ins = new ByteArrayInputStream(baos.toByteArray());

            PdfReader prd;
            PdfStamper pst;
            try
            {
                prd = new PdfReader(ins);
                pst = new PdfStamper(prd, baos);
            }
            catch (IOException e) {

                if("PdfReader not opened with owner password".equals(e.getLocalizedMessage()))
                    throw new ApplicationException("Файл защищен от записи паролем. Невозможно поставить штамп.");
                else {
                    throw new GeneralPlatformException(e);
                }
            }
            catch (DocumentException e) {
                throw new GeneralPlatformException(e);
            }

            int numberOfPages = prd.getNumberOfPages();

            Stamp stamp = createStamp(numberOfPages, _barcode, _addNumberOfPages);

            for(int i = 0; i < numberOfPages; i++) {
                stamp.draw(pst, i+1, null, 300, 300);
            }

            try {
                pst.close();
                outputStream.write(baos.toByteArray());
            }
            catch (DocumentException | IOException e) {
                Debug.exception(e);
            }
        }
        else
        {
            try {
                outputStream.write(baos.toByteArray());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static Stamp createStamp(int numberOfPages, String _barcode, boolean _addNumOfPages)
    {
        Stamp stmp = new Stamp();
        stmp.setRightMargin(new SizeHolder(150, SizeUnit.MM));
        stmp.setBottomMargin(new SizeHolder(5, SizeUnit.MM));

        StampVerticalLayout mainLayout = new StampVerticalLayout();
        List<IStampRenderer> mainLayoutRenderers = Lists.newArrayList();
        mainLayout.setRenderers(mainLayoutRenderers);

        mainLayout.setBorder(false);
        mainLayout.setWidth(new SizeHolder(50, SizeUnit.MM));
        mainLayout.setHeight(new SizeHolder(9, SizeUnit.MM));
        mainLayout.setAlign(StampAlign.middle);

        StampHorizontalLayout imageHorizonatlLayout = new StampHorizontalLayout();
        List<IStampRenderer> imageHorizonatlLayoutRenderers = Lists.newArrayList();
        mainLayoutRenderers.add(imageHorizonatlLayout);

        imageHorizonatlLayout.setBorder(false);
//        imageHorizonatlLayout.setVerticalAlign(StampVerticalAlign.top);
        imageHorizonatlLayout.setAlign(StampAlign.left);
        imageHorizonatlLayout.setRenderers(imageHorizonatlLayoutRenderers);

        StampImageCode imageCode = new StampImageCode();
        imageHorizonatlLayoutRenderers.add(imageCode);

        imageCode.setWidth(new SizeHolder(50, SizeUnit.MM));
        imageCode.setHeight(new SizeHolder(6, SizeUnit.MM));

        String errorCorrLvl = ErrorCorrectionLevel.Q.name();
        imageCode.setErrorCorrection(errorCorrLvl);

        String barcode = _barcode + (_addNumOfPages ? String.format("%03d", numberOfPages) : "");

        imageCode.setText(barcode);
        imageCode.setColor("0,0,0");
        imageCode.setFormat(BarcodeFormat.CODE_128.name());

        StampHorizontalLayout valueHorizonatlLayout = new StampHorizontalLayout();

        List<IStampRenderer> valueHorizonatlLayoutRenderers = Lists.newArrayList();
        valueHorizonatlLayout.setBorder(false);
        valueHorizonatlLayout.setAlign(StampAlign.middle);
        valueHorizonatlLayout.setVerticalAlign(StampVerticalAlign.top);
        valueHorizonatlLayout.setRenderers(valueHorizonatlLayoutRenderers);
        mainLayoutRenderers.add(valueHorizonatlLayout);

        StampText valueText = new StampText();

        valueText.setAlign(StampAlign.right);
        valueText.setText(barcode);
        valueText.setFont("Times New Roman,B,7");

        valueHorizonatlLayoutRenderers.add(valueText);

//        textExample.setColor(colorR + "," + colorG + "," + colorB);
//        textExample.setText(i <= strings.size()-1 ? strings.get(i) : "");
//        textExample.setFont(fontFamily + "," + fontBoldItalic + fontSize);

        stmp.setRenderer(mainLayout);
        return stmp;
    }
}
