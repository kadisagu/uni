/**
 * $Id$
 */
package ru.tandemservice.uni.component.reports.studentsGroupsList.Add;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.UniScriptItemCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author dseleznev
 * Created on: 15.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final int FIO_COLUMN_IDX = 0;
    private static final int SEX_COLUMN_IDX = 1;
    private static final int BIRTH_DATE_COLUMN_IDX = 2;
    private static final int COMPENS_TYPE_COLUMN_IDX = 3;
    private static final int THRIFTY_GROUR_COLUMN_IDX = 4;
    private static final int STUD_STATUS_COLUMN_IDX = 5;
    private static final int STUD_NUMBER_COLUMN_IDX = 6;
    private static final int BOOK_NUMBER_COLUMN_IDX = 7;

    private int[] ADDITIONAL_COLUMN_WIDTH_COEFS = new int[] { 386, 76, 138, 71, 48, 201, 111, 121 };
    private String[] ADDITIONAL_COLUMN_TITLES = new String[] { "ФИО студента", "Пол", "Д.Р.", "Б/К", "Ц", "Состояние", "Номер", "Номер З/К" };
    
    @Override
    public void prepare(Model model)
    { 
        initColumns(model);
        model.setStatusesListModel(new LazySimpleSelectModel<>(StudentStatus.class));
        model.setStatusesList(getCatalogItemList(StudentStatus.class));
    }
    
    private void initColumns(Model model)
    {
        // Инициируем добавление всех колонок студента по умолчанию
        model.setSexReq(true);
        model.setBirthDateReq(true);
        model.setCompensationTypeReq(true);
        model.setThriftyGroupReq(true);
        model.setStudentStatusReq(true);
        model.setPersonalNumberReq(true);
        
        // Инициируем добавление части колонок группы по умолчанию
        model.setEduLevelReq(true);
        model.setDevelopFormReq(true);
        model.setDevelopPeriodReq(true);
    }
    
    public void prepareReportData(Model model)
    {
        model.setGroupsList(getGroupsList(model));
        model.setStudentsMap(getStudentsMap(model));
        prepareTableColumns(model);
    }
    
    private void prepareTableColumns(Model model)
    {
        List<Integer> columnSizes = new ArrayList<>();
        List<String> columnHeaders = new ArrayList<>();
        
        columnHeaders.add(ADDITIONAL_COLUMN_TITLES[FIO_COLUMN_IDX]);
        columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[FIO_COLUMN_IDX]);
        
        if(model.isSexReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[SEX_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[SEX_COLUMN_IDX]);
        }
        if(model.isBirthDateReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[BIRTH_DATE_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[BIRTH_DATE_COLUMN_IDX]);
        }
        if(model.isCompensationTypeReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[COMPENS_TYPE_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[COMPENS_TYPE_COLUMN_IDX]);
        }
        if(model.isThriftyGroupReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[THRIFTY_GROUR_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[THRIFTY_GROUR_COLUMN_IDX]);
        }
        if(model.isStudentStatusReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[STUD_STATUS_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[STUD_STATUS_COLUMN_IDX]);
        }
        if(model.isPersonalNumberReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[STUD_NUMBER_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[STUD_NUMBER_COLUMN_IDX]);
        }
        if(model.isBookNumberReq())
        {
            columnHeaders.add(ADDITIONAL_COLUMN_TITLES[BOOK_NUMBER_COLUMN_IDX]);
            columnSizes.add(ADDITIONAL_COLUMN_WIDTH_COEFS[BOOK_NUMBER_COLUMN_IDX]);
        }
        
        int i = 0;
        int completeWidth = 0;
        int finalPerCentWidth = 0;
        int[] colPerCentSizes = new int[columnSizes.size()];
        for(Integer colSize : columnSizes) completeWidth += colSize;
        for(Integer colSize : columnSizes)
        {
            colPerCentSizes[i] = (100 * colSize) / completeWidth;
            finalPerCentWidth += colPerCentSizes[i++];
        }
        
        colPerCentSizes[0] += 100 - finalPerCentWidth;
        model.setColumnHeaders(columnHeaders.toArray(new String[columnHeaders.size()]));
        model.setColumnSizes(colPerCentSizes);
    }
    
    private List<Group> getGroupsList(Model model)
    {
        MQBuilder groupBuilder = new MQBuilder(Group.ENTITY_CLASS, "g");
        groupBuilder.add(MQExpression.in("g", Group.P_ID, model.getGroupIdsList()));
        
        groupBuilder.addOrder("g", Group.L_COURSE + "." + Course.P_TITLE);
        groupBuilder.addOrder("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_TITLE);
        groupBuilder.addOrder("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_TITLE);
        groupBuilder.addOrder("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE);
        groupBuilder.addOrder("g", Group.P_TITLE);
        
        return groupBuilder.getResultList(getSession());
    }
    
    private Map<Group, List<Student>> getStudentsMap(Model model)
    {
        MQBuilder studentBuilder = new MQBuilder(Student.ENTITY_CLASS, "s");
        studentBuilder.add(MQExpression.in("s", Student.L_GROUP + "." + Group.P_ID, model.getGroupIdsList()));
        studentBuilder.add(MQExpression.in("s", Student.L_STATUS, model.getStatusesList()));
        studentBuilder.addOrder("s", Student.L_STATUS + "." + StudentStatus.P_PRIORITY);
        studentBuilder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." +IdentityCard.P_LAST_NAME);
        studentBuilder.addOrder("s", Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
        List<Student> studentsList = studentBuilder.getResultList(getSession());
        
        Map<Group, List<Student>> studentsMap = new TreeMap<>(CommonCollator.comparing(ITitled::getTitle, true, false));
        for(Student student : studentsList)
        {
            Group group = student.getGroup();
            SafeMap.safeGet(studentsMap, group, ArrayList.class).add(student);
            //studentsMap.put(student.getGroup(), list);
        }

        return studentsMap;
    }
    
    @Override
    public Integer preparePrintReport(final Model model)
    {
        prepareReportData(model);

        List<String> checkList = new ArrayList<>();

        if(model.isSexReq()) checkList.add("isSexReq");
        if(model.isBirthDateReq()) checkList.add("isBirthDateReq");
        if(model.isCompensationTypeReq()) checkList.add("isCompensationTypeReq");
        if(model.isThriftyGroupReq()) checkList.add("isThriftyGroupReq");
        if(model.isStudentStatusReq()) checkList.add("isStudentStatusReq");
        if(model.isPersonalNumberReq()) checkList.add("isPersonalNumberReq");
        if(model.isBookNumberReq()) checkList.add("isBookNumberReq");

        if(model.isEduLevelReq()) checkList.add("isEduLevelReq");
        if(model.isDevelopFormReq()) checkList.add("isDevelopFormReq");
        if(model.isDevelopConditionReq()) checkList.add("isDevelopConditionReq");
        if(model.isDevelopTechReq()) checkList.add("isDevelopTechReq");
        if(model.isDevelopPeriodReq()) checkList.add("isDevelopPeriodReq");
        if(model.isFormativeOrgUnitReq()) checkList.add("isFormativeOrgUnitReq");
        if(model.isTerritorialOrgUnitReq()) checkList.add("isTerritorialOrgUnitReq");

        final UniScriptItem scriptItem = getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENTS_GROUPS_LIST);

        final Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                "groupMap", model.getStudentsMap(),
                "checkList", checkList,
                "columnSizes", model.getColumnSizes(),
                "columnHeaders", model.getColumnHeaders()
        );

        return PrintReportTemporaryStorage.registerTemporaryPrintForm((byte[])result.get("document"), (String)result.get("fileName"));
    }
}