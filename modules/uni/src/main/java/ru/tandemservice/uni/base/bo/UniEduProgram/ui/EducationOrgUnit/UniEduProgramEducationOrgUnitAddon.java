/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Утиль для выбора НПП по фильтрам <code>Filters</code>.<p/>
 * Для использования нужно вставить htmlTemplate <code>ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddonTemplate</code>
 * и добавить аддон <code>UniEduProgramEducationOrgUnitAddon</code> по короткому имени класса аддона.
 * В методе <code>onComponentRefresh()</code> презентера можно сконфигурировать утиль соответствующими методами аддона.
 *
 * @author Alexander Shaburov
 * @since 25.10.12
 */
public class UniEduProgramEducationOrgUnitAddon extends UIAddon
{
    public UniEduProgramEducationOrgUnitAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private static final String ALIAS = "alias";

    // Configuration

    private Map<Filters, FilterConfig> _filtersConfigMap = new HashMap<>();
    private boolean _needEnableCheckBox = false;
    private boolean _doubleWidth = false;

    private IDataSettings _settings;

    private String baseUpdateOnChange;
    private String listener = null;

    // SelectModels

    private Map<Filters, CommonFilterSelectModel> _modelsMap = new HashMap<>();

    // Bind values

    private Map<Filters, Object> _valuesMap = new HashMap<>();

    private Filters _currentFilter;
    private Map<MetaDSLPath, Object> _whereAndPropertiesMap = new HashMap<>();
    private Map<MetaDSLPath, Object> _whereOrPropertiesMap = new HashMap<>();


    // Logic

    @Override
    public void onComponentActivate()
    {
        // инициализируем конфигурацию по умолчанию

        _needEnableCheckBox = false;
        _doubleWidth = false;
        for (Filters filter : getPossibleFilters())
        {
            if (Filters.QUALIFICATION.equals(filter))
                _filtersConfigMap.put(filter, new FilterConfig(false, true, false, false, false, false));

            _filtersConfigMap.put(filter, new FilterConfig(true, true, false, false, false, false));
        }
    }

    @Override
    public void onComponentRefresh()
    {
        // инициализируем модели селектов, если селект используется

        for (final Filters filters : getPossibleFilters())
        {
            if (_filtersConfigMap.get(filters).isUsed())
            {
                _modelsMap.put(filters, new CommonFilterSelectModel()
                {
                    @Override
                    protected IListResultBuilder createBuilder(String filter, Object o)
                    {
                        DQLSelectBuilder idBuilder = prepareEducationOrgUnitBuilder(ALIAS, filters);

                        idBuilder.column(property(filters.getPrimaryKeyPath().fromAlias(ALIAS)), "id");

                        if (!StringUtils.isEmpty(filter))
                            idBuilder.where(likeUpper(property(filters.getTitleFilterPath().fromAlias(ALIAS)), value(CoreStringUtils.escapeLike(filter, true))));

                        if (o != null)
                        {
                            if (o instanceof Set)
                                idBuilder.where(in(property(filters.getPrimaryKeyPath().fromAlias(ALIAS)), (Collection) o));
                            else
                                idBuilder.where(eq(property(filters.getPrimaryKeyPath().fromAlias(ALIAS)), commonValue(o, PropertyType.LONG)));
                        }

                        idBuilder.predicate(DQLPredicateType.distinct);

                        DQLSelectBuilder builder = new DQLSelectBuilder()
                                .fromEntity(filters.getClazz(), "entity")
                                .joinDataSource("entity", DQLJoinType.inner, idBuilder.buildQuery(), "id", eq(property("entity.id"), property("id.id")))
                                .column(property("entity"));

                        builder.order(property(filters.getOrderFilterPath().fromAlias("entity")), OrderDirection.asc);

                        return new DQLListResultBuilder(builder, 50);
                    }

                    @Override
                    public String getLabelFor(Object value, int columnIndex)
                    {
                        return (String) FastBeanUtils.getValue(value, filters.getTitlePath());
                    }
                });
            }
        }
    }

    public Object getFilterValue(Filters filter)
    {
        if (_settings != null)
            return _settings.get(filter.getSettingsName());

        return _valuesMap.get(filter);
    }

    /**
     * Фильтруются выборки в фильтрах по фильтрам выбраным выше.
     * Фильтр не применяется если:
     * 1. от него независт текущий фильтр
     * 2. он не используется
     * 3. он не обязательный и не указан
     * 4. используются enableCheckBox и указаный фильтр не включен
     */
    @SuppressWarnings("ConstantConditions")
    protected DQLSelectBuilder applyFilters(DQLSelectBuilder builder, String alias, Filters filter)
    {
        for (Filters filters : getPossibleFilters())
        {
            if (filter != null && !filters.getDependFields().contains(filter.getSettingsName()))
                continue;

            FilterConfig config = _filtersConfigMap.get(filters);

            if (!config.isUsed())
                continue;

            if (_needEnableCheckBox && !config.isCheckEnableCheckBox())
                continue;

            Object value = getFilterValue(filters);

            if (!config.isRequired() && !_needEnableCheckBox)
                if (value == null)
                    continue;
                else if (value instanceof Collection && ((Collection) value).isEmpty())
                    continue;

            filters.applyFilter(builder, value, alias, this);
        }
        return builder;
    }

    /**
     * Получает конечный список НПП фильтрованый по использующимся фильтрам, если они были указаны.
     *
     * @return список НПП
     */
    public List<EducationOrgUnit> getEducationOrgUnitFilteredList()
    {
        DQLSelectBuilder builder = prepareEducationOrgUnitBuilder(ALIAS, null);

        builder.order(property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias(ALIAS)));

        return builder.createStatement(getSession()).list();
    }

    /**
     * Получает билдер для конечного списка НПП, фильтрованый по использующимся фильтрам, если они были указаны.
     * Единственная колонка билдера - НПП.id
     *
     * @return билдер
     */
    public DQLSelectBuilder getEducationOrgUnitIdsFilteredBuilder(String alias)
    {
        DQLSelectBuilder builder = prepareEducationOrgUnitBuilder(alias, null);
        builder.column(alias + ".id");
        return builder;
    }

    public DQLSelectBuilder getEducationOrgUnitIdsFilteredBuilder()
    {
        return getEducationOrgUnitIdsFilteredBuilder(ALIAS);
    }

    protected DQLSelectBuilder prepareEducationOrgUnitBuilder(String alias, Filters filter)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, alias);

        for (Map.Entry<MetaDSLPath, Object> entry : _whereAndPropertiesMap.entrySet())
        {
            Object value = entry.getValue();
            if (null == value)
                builder.where(isNull(property(entry.getKey().fromAlias(alias))));
            else if (value instanceof Collection)
                builder.where(in(property(entry.getKey().fromAlias(alias)), (Collection) value));
            else if (value instanceof IDQLExpression)
                builder.where(in(property(entry.getKey().fromAlias(alias)), (IDQLExpression) value));
            else
                builder.where(eq(property(entry.getKey().fromAlias(alias)), commonValue(value)));
        }

        IDQLExpression expression = null;
        for (Map.Entry<MetaDSLPath, Object> entry : _whereOrPropertiesMap.entrySet())
        {
            Object value = entry.getValue();
            if (null == value)
                expression = or(expression, isNull(property(entry.getKey().fromAlias(alias))));
            else if (value instanceof Collection)
                expression = or(expression, in(property(entry.getKey().fromAlias(alias)), (Collection) value));
            else if (value instanceof IDQLExpression)
                builder.where(in(property(entry.getKey().fromAlias(alias)), (IDQLExpression) value));
            else
                expression = or(expression, eq(property(entry.getKey().fromAlias(alias)), commonValue(value)));
        }

        if (expression != null)
            builder.where(expression);

        applyFilters(builder, alias, filter);
        return builder;
    }

    /**
     * Метод для фильтрации выборки по фильтрам НПП в обход конечному списку НПП, т.е. список НПП не поднимается.
     * Применяет фильтры для билдера. Фильтры будут применятся к полям EducationOrgUnit.
     * Фильтр не применяется если:
     * 1. он не используется
     * 2. он не обязательный и не указан
     * 3. используются enableCheckBox и указаный фильтр не включен
     *
     * @param builder DQLSelectBuilder для которого применяем фильтры
     * @param alias   алиас для EducationOrgUnit
     * @return DQLSelectBuilder
     */
    @SuppressWarnings("ConstantConditions")
    public DQLSelectBuilder applyFilters(DQLSelectBuilder builder, String alias)
    {
        return applyFilters(builder, alias, null);
    }

    /**
     * Обнуляет выбранные значения в фильтрах.
     */
    public void clearFilters()
    {
        for (Filters filter : getPossibleFilters())
        {
            if (_settings != null)
                _settings.clear();
            _valuesMap.put(filter, null);
        }
    }

    public void saveSettings()
    {
        DataSettingsFacade.saveSettings(_settings);
    }


    // Config methods

    /**
     * Устанавливает фильтры, которые необходимо выводить на форме.
     * Фильтры, которые не указаны считаются не используемыми.
     * <p/><b>Note:</b> по умолчанию используются все фильтры из Filters
     *
     * @param useFilters список фильтров на форме
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configUseFilters(@Nullable Filters... useFilters)
    {
        List<Filters> useFilterList = useFilters == null ? Collections.<Filters>emptyList() : Arrays.asList(useFilters);
        for (Filters filter : getPossibleFilters())
        {
            if (useFilterList.contains(filter))
                _filtersConfigMap.get(filter).setUsed(true);
            else
                _filtersConfigMap.get(filter).setUsed(false);
        }

        return this;
    }

    /**
     * Устанавливает обязательные фильтры на форме.
     * Фильтры, которые не указаны считаются не обязательными.
     * <p/><b>Note:</b> по умолчанию все фильтры не обязательные
     *
     * @param requiredFilters список обязательных фильтров
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configRequiredFilters(@Nullable Filters... requiredFilters)
    {
        List<Filters> requiredFieldList = requiredFilters == null ? Collections.<Filters>emptyList() : Arrays.asList(requiredFilters);
        for (Filters filter : getPossibleFilters())
        {
            if (requiredFieldList.contains(filter))
                _filtersConfigMap.get(filter).setRequired(true);
            else
                _filtersConfigMap.get(filter).setRequired(false);
        }

        return this;
    }

    /**
     * Устанавливает задизабленные фильтры на форме.
     * Фильтры, которые не указаны, считаются не задизабленными.
     * Важно: задизаблен может быть только фильтр без чекбокса, при наличии чекбокса эта настройка игнорируется.
     * <p/><b>Note:</b> по умолчанию все фильтры не задизаблены.
     *
     * @param disabledFilters список задизабленных фильтров
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configDisabledFilters(@Nullable Filters... disabledFilters)
    {
        List<Filters> requiredFieldList = disabledFilters == null ? Collections.<Filters>emptyList() : Arrays.asList(disabledFilters);
        for (Filters filter : getPossibleFilters())
        {
            if (requiredFieldList.contains(filter))
                _filtersConfigMap.get(filter).setDisabled(true);
            else
                _filtersConfigMap.get(filter).setDisabled(false);
        }

        return this;
    }

    /**
     * Указывает какие фильтры должны отображаться как мультиселекты.
     * Фильтры, которые не указаны считаются синглселектами.
     * <p/><b>Note:</b> по умолчанию все фильтры мультиселекты
     *
     * @param multiselectFilters список фильтров мультиселектов
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configMultiselectFilters(@Nullable Filters... multiselectFilters)
    {
        List<Filters> multiselectFilterList = multiselectFilters == null ? Collections.<Filters>emptyList() : Arrays.asList(multiselectFilters);
        for (Filters filter : getPossibleFilters())
        {
            if (multiselectFilterList.contains(filter))
                _filtersConfigMap.get(filter).setMultiselect(true);
            else
                _filtersConfigMap.get(filter).setMultiselect(false);
        }

        return this;
    }

    /**
     * Указывает нужны ли enableCheckBox на форме.
     * <p/><b>Note:</b> по умолчанию false
     *
     * @param value нужны ли enableCheckBox
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configNeedEnableCheckBox(boolean value)
    {
        _needEnableCheckBox = value;

        return this;
    }

    /**
     * Указывает у каких фильтров enableCheckBox будет задизаблен.
     * Фильтры у которых enableCheckBox дизаблится проставляются чекнутыми.
     * Фильтры, которые не указаны считаются раздизабленными.
     * <p/><b>Note:</b> по умолчанию все enableCheckBox не задизаблены
     *
     * @param disableEnableCheckBoxFilters фильтры enableCheckBox которых задизаблен
     * @return this
     * @throws IllegalStateException если enableCheckBox не используется
     */
    public UniEduProgramEducationOrgUnitAddon configDisableEnableCheckBoxFilters(@Nullable Filters... disableEnableCheckBoxFilters)
    {
        if (!_needEnableCheckBox)
            throw new IllegalStateException("EnableCheckBox is not used");

        List<Filters> disableEnableCheckBoxFiltersList = disableEnableCheckBoxFilters == null ? Collections.<Filters>emptyList() : Arrays.asList(disableEnableCheckBoxFilters);
        for (Filters filter : getPossibleFilters())
        {
            if (disableEnableCheckBoxFiltersList.contains(filter))
            {
                _filtersConfigMap.get(filter).setDisableEnableCheckBox(true);
                _filtersConfigMap.get(filter).setCheckEnableCheckBox(true);
            }
            else
            {
                _filtersConfigMap.get(filter).setDisableEnableCheckBox(false);
            }
        }

        return this;
    }

    /**
     * Указывает у каких фильтров enableCheckBox чекнуты.
     * Фильтры, которые не указаны считаются нечекнутыми, если фильтр задизаблен, то он останется чекнутым.
     * <p/><b>Note:</b> по умолчанию все enableCheckBox сняты
     *
     * @param checkEnableCheckBoxFilters фильты, у которых enableCheckBox чекнут
     * @return this
     * @throws IllegalStateException если enableCheckBox не используется
     */
    public UniEduProgramEducationOrgUnitAddon configCheckEnableCheckBoxFilters(@Nullable Filters... checkEnableCheckBoxFilters)
    {
        if (!_needEnableCheckBox)
            throw new IllegalStateException("EnableCheckBox is not used");

        List<Filters> checkEnableCheckBoxFiltersList = checkEnableCheckBoxFilters == null ? Collections.<Filters>emptyList() : Arrays.asList(checkEnableCheckBoxFilters);
        for (Filters filter : getPossibleFilters())
        {
            if (checkEnableCheckBoxFiltersList.contains(filter))
            {
                _filtersConfigMap.get(filter).setCheckEnableCheckBox(true);
            }
            else
            {
                if (!_filtersConfigMap.get(filter).isDisableEnableCheckBox())
                    _filtersConfigMap.get(filter).setCheckEnableCheckBox(false);
            }
        }

        return this;
    }

    /**
     * Указывает должны ли фильтры на форме быть двойной ширины.
     *
     * @param value фильтры двойной ширины
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configDoubleWidthFilters(boolean value)
    {
        _doubleWidth = value;

        return this;
    }

    /**
     * Очищает условия
     *
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon clearConfigWhere()
    {
        _whereOrPropertiesMap.clear();
        _whereAndPropertiesMap.clear();
        return this;
    }

    /**
     * Добавляет условие к выборке основного массива НПП. По массиву НПП работают все фильтры.
     * <p/><b>Note:</b> propertyPath должен быть для EducationOrgUnit.
     *
     * @param propertyPath путь до ключа фильтрации
     * @param key          объект по которому фильтруем
     * @param or           условие будет включено в выражение OR
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configWhere(final MetaDSLPath propertyPath, final Object key, boolean or)
    {
        if (or)
            _whereOrPropertiesMap.put(propertyPath, key);
        else
            _whereAndPropertiesMap.put(propertyPath, key);

        return this;
    }

    /**
     * Ключ для хранения настрок. Если ключ указан, то введенные в фильтрах значения будут сохраняться в настройках, иначе будут биндиться в аддон.
     * <p/><b>Note:</b> по умолчанию ключа нет и значения биндятся в аддон
     *
     * @param settingsKey префикс ключей
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configSettings(@NotNull String settingsKey)
    {
        if (settingsKey != null)
            _settings = UniBaseUtils.getDataSettings(getPresenter().getConfig().getBusinessComponent(), settingsKey);

        return this;
    }

    /**
     * Если фильтры утили должны обновлять что-то еще на странице, сюда надо передать строку с перечнем id тех тапестревых компонентов, которые надо обновить
     * <p/><b>Note:</b> по умолчанию все пусто
     *
     * @param baseUpdateOnChange перечень полей, которые надо обновлять
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configBaseUpdateOnChange(@NotNull String baseUpdateOnChange)
    {
        if (baseUpdateOnChange != null)
            this.baseUpdateOnChange = baseUpdateOnChange;

        return this;
    }

    /**
     * Если фильтры утили должны вызывать листенер, то сюда надо передать его название
     * <p/><b>Note:</b> по умолчанию все пусто
     *
     * @param listener листенер
     * @return this
     */
    public UniEduProgramEducationOrgUnitAddon configListener(@NotNull String listener)
    {
        if (listener != null)
            this.listener = listener;

        return this;
    }

    // Calculate getters & setters for render template

    public Iterable getFilters()
    {
        return getPossibleFilters();
    }

    public String getCurrentBlockName()
    {
        return _filtersConfigMap.get(_currentFilter).isUsed() ? (_filtersConfigMap.get(_currentFilter).isMultiselect() ? "multiselect" : "singleselect") : "empty";
    }

    public String getCurrentFieldName()
    {
        return _currentFilter.getFieldName();
    }

    public CommonFilterSelectModel getCurrentModel()
    {
        return _modelsMap.get(_currentFilter);
    }

    public Object getCurrentValues()
    {
        return getFilterValue(_currentFilter);
    }

    public void setCurrentValues(Object value)
    {
        if (value == null)
        {
            if (_settings != null)
                _settings.set(_currentFilter.getSettingsName(), null);
            else
                _valuesMap.put(_currentFilter, null);
        }
        else
        {
            if (_settings != null)
                _settings.set(_currentFilter.getSettingsName(), value);
            else
                _valuesMap.put(_currentFilter, value);
        }
    }

    public String getCurrentUpdateOnChange()
    {
        StringBuilder result = new StringBuilder();

        if (!StringUtils.isEmpty(getBaseUpdateOnChange()))
        {
            result.append(getBaseUpdateOnChange());
        }

        for (String s : _currentFilter.getDependFields())
        {
            if (result.length() != 0)
                result.append(", ");

            result.append("filterId_").append(s).append(".").append(getName());
        }

        return result.toString();
    }

    public Boolean getEnableFilter()
    {
        return _filtersConfigMap.get(_currentFilter).isCheckEnableCheckBox();
    }

    public void setEnableFilter(Boolean value)
    {
        if (value == null)
            _filtersConfigMap.get(_currentFilter).setCheckEnableCheckBox(false);
        else
            _filtersConfigMap.get(_currentFilter).setCheckEnableCheckBox(value);
    }

    public boolean isRequiredFilter()
    {
        return _filtersConfigMap.get(_currentFilter).isRequired() || (_needEnableCheckBox && getEnableFilter());
    }

    public boolean isDisabledFilter()
    {
        return _filtersConfigMap.get(_currentFilter).isDisabled();
    }

    public boolean isDisableEnableCheckBox()
    {
        return _filtersConfigMap.get(_currentFilter).isDisableEnableCheckBox();
    }

    public boolean isDisableField()
    {
        return _needEnableCheckBox && !_filtersConfigMap.get(_currentFilter).isCheckEnableCheckBox();
    }

    public String getStyleClass()
    {
        return _doubleWidth ? (_filtersConfigMap.get(_currentFilter).isMultiselect() ? "double-width" : "double-width") : "";
    }

    public Filters getCurrentFilter()
    {
        return _currentFilter;
    }

    public void setCurrentFilter(Filters filter)
    {
        _currentFilter = filter;
    }

    public boolean isNeedEnableCheckBox()
    {
        return _needEnableCheckBox;
    }

    public String getCurrentEnableCheckBoxUpdateOnChange()
    {
        return getFilterId() + ", " + getCurrentUpdateOnChange();
    }

    public String getFilterId()
    {
        return "filterId_" + _currentFilter.getSettingsName() + "." + getName();
    }

    public String getEnableCheckBoxId()
    {
        return "enableCheckBoxId_" + _currentFilter.getSettingsName() + "." + getName();
    }

    protected Collection<Filters> getPossibleFilters()
    {
        return Filters.BASE_FILTER_SET;
    }

    /**
     * Фильтры, которые могут быть использованы на форме.
     */
    public static class Filters
    {
        public static final Filters QUALIFICATION = new Filters("QUALIFICATION", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), "Уровень образования", Arrays.asList("FORMATIVE_ORG_UNIT", "TERRITORIAL_ORG_UNIT", "PRODUCING_ORG_UNIT", "EDUCATION_LEVEL_HIGH_SCHOOL", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().title(), EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().id(), Qualifications.order(), Qualifications.title(), Qualifications.class);
        public static final Filters FORMATIVE_ORG_UNIT = new Filters("FORMATIVE_ORG_UNIT", EducationOrgUnit.formativeOrgUnit(), "Формирующее подразделение", Arrays.asList("TERRITORIAL_ORG_UNIT", "PRODUCING_ORG_UNIT", "EDUCATION_LEVEL_HIGH_SCHOOL", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.formativeOrgUnit().fullTitle(), EducationOrgUnit.formativeOrgUnit().id(), OrgUnit.title(), OrgUnit.fullTitle(), OrgUnit.class);
        public static final Filters TERRITORIAL_ORG_UNIT = new Filters("TERRITORIAL_ORG_UNIT", EducationOrgUnit.territorialOrgUnit(), "Территориальное подразделение", Arrays.asList("PRODUCING_ORG_UNIT", "EDUCATION_LEVEL_HIGH_SCHOOL", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.territorialOrgUnit().territorialFullTitle(), EducationOrgUnit.territorialOrgUnit().id(), OrgUnit.territorialFullTitle(), OrgUnit.territorialFullTitle(), OrgUnit.class);
        public static final Filters PRODUCING_ORG_UNIT = new Filters("PRODUCING_ORG_UNIT", EducationOrgUnit.educationLevelHighSchool().orgUnit(), "Выпускающее подразделение", Arrays.asList("EDUCATION_LEVEL_HIGH_SCHOOL", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.educationLevelHighSchool().orgUnit().fullTitle(), EducationOrgUnit.educationLevelHighSchool().orgUnit().id(), OrgUnit.title(), OrgUnit.fullTitle(), OrgUnit.class);
        public static final Filters EDUCATION_LEVEL_HIGH_SCHOOL = new Filters("EDUCATION_LEVEL_HIGH_SCHOOL", EducationOrgUnit.educationLevelHighSchool(), "Направление подготовки (специальность)", Arrays.asList("DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.educationLevelHighSchool().fullTitle(), EducationOrgUnit.educationLevelHighSchool().id(), EducationLevelsHighSchool.displayableTitle(), EducationLevelsHighSchool.fullTitle(), EducationLevelsHighSchool.class);
        public static final Filters DEVELOP_FORM = new Filters("DEVELOP_FORM", EducationOrgUnit.developForm(), "Форма освоения", Arrays.asList("DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.developForm().title(), EducationOrgUnit.developForm().id(), DevelopForm.code(), DevelopForm.title(), DevelopForm.class);
        public static final Filters DEVELOP_CONDITION = new Filters("DEVELOP_CONDITION", EducationOrgUnit.developCondition(), "Условие освоения", Arrays.asList("DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.developCondition().title(), EducationOrgUnit.developCondition().id(), DevelopCondition.code(), DevelopCondition.title(), DevelopCondition.class);
        public static final Filters DEVELOP_TECH = new Filters("DEVELOP_TECH", EducationOrgUnit.developTech(), "Технология освоения", Arrays.asList("DEVELOP_PERIOD"), EducationOrgUnit.developTech().title(), EducationOrgUnit.developTech().id(), DevelopTech.code(), DevelopTech.title(), DevelopTech.class);
        public static final Filters DEVELOP_PERIOD = new Filters("DEVELOP_PERIOD", EducationOrgUnit.developPeriod(), "Нормативный срок освоения", Collections.<String>emptyList(), EducationOrgUnit.developPeriod().title(), EducationOrgUnit.developPeriod().id(), DevelopPeriod.priority(), DevelopPeriod.title(), DevelopPeriod.class);

        public static final Collection<Filters> BASE_FILTER_SET = new ArrayList<>(Arrays.asList(
                QUALIFICATION, FORMATIVE_ORG_UNIT, TERRITORIAL_ORG_UNIT, PRODUCING_ORG_UNIT, EDUCATION_LEVEL_HIGH_SCHOOL,
                DEVELOP_FORM, DEVELOP_CONDITION, DEVELOP_TECH, DEVELOP_PERIOD
        ));

        public Filters(String settingsName, MetaDSLPath entityPath, String fieldName, List<String> dependFields, MetaDSLPath titleFilterPath, MetaDSLPath primaryKeyPath, MetaDSLPath orderFilterPath, MetaDSLPath titlePath, Class clazz)
        {
            _settingsName = settingsName;
            _fieldName = fieldName;
            _dependFields = dependFields;
            _entityPath = entityPath;
            _titlePath = titlePath;
            _titleFilterPath = titleFilterPath;
            _primaryKeyPath = primaryKeyPath;
            _orderFilterPath = orderFilterPath;
            _clazz = clazz;
        }

        private Class _clazz;
        private String _settingsName;// название фильтра для идентификаторов
        private String _fieldName;// название фильтра на форме
        private List<String> _dependFields;// фильтры, которые зависят от текущего фильтра
        private MetaDSLPath _entityPath;// путь до проперти сущьности фильтра
        private MetaDSLPath _titlePath;// путь до проперти, которую выводим в качестве названния элементов
        private MetaDSLPath _titleFilterPath;// путь до проперти, по которой фильтруются объекты в фильтре
        private MetaDSLPath _primaryKeyPath;// путь до ключа по которому выбераются чекнутые в фильтрах объекты
        private MetaDSLPath _orderFilterPath;// путь до ключа по которому сортируем фильтр

        // Getters & Setters

        public Class getClazz()
        {
            return _clazz;
        }

        public MetaDSLPath getTitleFilterPath()
        {
            return _titleFilterPath;
        }

        public MetaDSLPath getPrimaryKeyPath()
        {
            return _primaryKeyPath;
        }

        public MetaDSLPath getEntityPath()
        {
            return _entityPath;
        }

        public MetaDSLPath getTitlePath()
        {
            return _titlePath;
        }

        public String getFieldName()
        {
            return _fieldName;
        }

        public List<String> getDependFields()
        {
            return _dependFields;
        }

        public MetaDSLPath getOrderFilterPath()
        {
            return _orderFilterPath;
        }

        public String getSettingsName()
        {
            return _settingsName;
        }

        public void applyFilter(DQLSelectBuilder builder, Object value, String alias, UniEduProgramEducationOrgUnitAddon addon)
        {
            if (value instanceof Collection)
                builder.where(in(property(getEntityPath().fromAlias(alias)), (Collection) value));
            else
                builder.where(eq(property(getEntityPath().fromAlias(alias)), commonValue(value, PropertyType.ANY)));
        }
    }

    public static class FilterConfig
    {
        public FilterConfig(boolean used, boolean multiselect, boolean required, boolean disabled, boolean disableEnableCheckBox, boolean checkEnableCheckBox)
        {
            _used = used;
            _multiselect = multiselect;
            _required = required;
            _disabled = disabled;
            _disableEnableCheckBox = disableEnableCheckBox;
            _checkEnableCheckBox = checkEnableCheckBox;
        }

        private boolean _used;
        private boolean _multiselect;
        private boolean _required;
        private boolean _disabled;
        private boolean _disableEnableCheckBox;
        private boolean _checkEnableCheckBox;

        // Accessors

        public boolean isUsed()
        {
            return _used;
        }

        public void setUsed(boolean used)
        {
            _used = used;
        }

        public boolean isMultiselect()
        {
            return _multiselect;
        }

        public void setMultiselect(boolean multiselect)
        {
            _multiselect = multiselect;
        }

        public boolean isRequired()
        {
            return _required;
        }

        public void setRequired(boolean required)
        {
            _required = required;
        }

        public boolean isDisabled()
        {
            return _disabled;
        }

        public void setDisabled(boolean disabled)
        {
            _disabled = disabled;
        }

        public boolean isDisableEnableCheckBox()
        {
            return _disableEnableCheckBox;
        }

        public void setDisableEnableCheckBox(boolean disableEnableCheckBox)
        {
            _disableEnableCheckBox = disableEnableCheckBox;
        }

        public boolean isCheckEnableCheckBox()
        {
            return _checkEnableCheckBox;
        }

        public void setCheckEnableCheckBox(boolean checkEnableCheckBox)
        {
            _checkEnableCheckBox = checkEnableCheckBox;
        }
    }

    // Getters

    public boolean isEmptyFilters()
    {
        for (Filters filter : getPossibleFilters())
        {
            final FilterConfig config = _filtersConfigMap.get(filter);

            if (!config.isUsed())
                continue;

            if (_needEnableCheckBox && !config.isCheckEnableCheckBox())
                continue;

            final Object value = getFilterValue(filter);

            if (value == null)
                continue;
            if (value instanceof Collection && ((Collection) value).isEmpty())
                continue;

            return false;
        }

        return true;
    }

    public FilterConfig getFilterConfig(Filters filter)
    {
        return _filtersConfigMap.get(filter);
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public Map<Filters, Object> getValuesMap()
    {
        return _valuesMap;
    }

    public Map<Filters, CommonFilterSelectModel> getModelsMap()
    {
        return _modelsMap;
    }

    public String getBaseUpdateOnChange()
    {
        return baseUpdateOnChange;
    }

    public String getListener()
    {
        return listener;
    }
}
