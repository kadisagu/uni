/* $Id$ */
package ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitSetting;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Vasily Zhukov
 * @since 02.06.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
}
