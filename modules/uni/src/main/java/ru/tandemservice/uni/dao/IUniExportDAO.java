/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author agolubenko
 * @since Apr 22, 2010
 */
public interface IUniExportDAO
{
    final String BEAN_NAME = "uniExportDao";
    final SpringBeanCache<IUniExportDAO> instance = new SpringBeanCache<IUniExportDAO>(BEAN_NAME);


    /**
     * @param entityName имя entity
     * @param properties список полей
     * @param onlyUserRecords true если только пользовательские записи, false в противном случае
     * @return сериализованные объекты таблицы
     * @throws Exception ошибка
     */
    byte[] getEntityData(String entityName, String[] properties, boolean onlyUserRecords) throws Exception;
}
