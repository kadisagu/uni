package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки министерского классификатора (НПм)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelsGen extends EntityBase
 implements INaturalIdentifiable<EducationLevelsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.EducationLevels";
    public static final String ENTITY_NAME = "educationLevels";
    public static final int VERSION_HASH = 1248408076;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CATALOG_CODE = "catalogCode";
    public static final String P_TITLE = "title";
    public static final String L_EDU_PROGRAM_SUBJECT = "eduProgramSubject";
    public static final String L_EDU_PROGRAM_SPECIALIZATION = "eduProgramSpecialization";
    public static final String P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE = "eduProgramSpecializationSortingCache";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String L_LEVEL_TYPE = "levelType";
    public static final String L_PARENT_LEVEL = "parentLevel";
    public static final String L_QUALIFICATION = "qualification";
    public static final String P_OKSO = "okso";
    public static final String P_INHERITED_OKSO = "inheritedOkso";
    public static final String P_DISPLAYABLE_SHORT_TITLE = "displayableShortTitle";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_FULL_TITLE_WITH_NEW_DATA = "fullTitleWithNewData";
    public static final String P_FULL_TITLE_WITH_ROOT_LEVEL = "fullTitleWithRootLevel";
    public static final String P_INHERITED_OKSO_PREFIX = "inheritedOksoPrefix";
    public static final String P_PROGRAM_SPECIALIZATION_TITLE = "programSpecializationTitle";
    public static final String P_PROGRAM_SPECIALIZATION_TITLE_IF_CHILD = "programSpecializationTitleIfChild";
    public static final String P_PROGRAM_SUBJECT_TITLE_WITH_CODE = "programSubjectTitleWithCode";
    public static final String P_PROGRAM_SUBJECT_WITH_CODE_INDEX_AND_GEN_TITLE = "programSubjectWithCodeIndexAndGenTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_TITLE_CODE_PREFIX = "titleCodePrefix";

    private String _code;     // Системный код
    private String _catalogCode;     // Код справочника
    private String _title;     // Название
    private EduProgramSubject _eduProgramSubject;     // Направление профессионального образования
    private EduProgramSpecialization _eduProgramSpecialization;     // Направленность ВО (только для ВО)
    private String _eduProgramSpecializationSortingCache;     // Поле для сортировки по направленности
    private String _shortTitle;     // Сокращенное название
    private StructureEducationLevels _levelType;     // Ступень образования
    private EducationLevels _parentLevel;     // Подчинено направлению подготовки (специальности)
    private Qualifications _qualification;     // Квалификация
    private String _okso;     // ОКСО
    private String _inheritedOkso;     // Ближайшее родительское ОКСО. Автообновляемое

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCatalogCode()
    {
        return _catalogCode;
    }

    /**
     * @param catalogCode Код справочника. Свойство не может быть null.
     */
    public void setCatalogCode(String catalogCode)
    {
        dirty(_catalogCode, catalogCode);
        _catalogCode = catalogCode;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * Заполняется только для тех элементов, по которым можно создавать НПв: educationLevelsHighSchool.
     * Если связь заполнена, это означает, что НПм либо сформировано на основе классификатора (либо приравнено к таковой пользователем),
     * в этом случае название, коды (и прочая информация) будут взяты из элемента классификатора.
     *
     * @return Направление профессионального образования.
     */
    public EduProgramSubject getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    /**
     * @param eduProgramSubject Направление профессионального образования.
     */
    public void setEduProgramSubject(EduProgramSubject eduProgramSubject)
    {
        dirty(_eduProgramSubject, eduProgramSubject);
        _eduProgramSubject = eduProgramSubject;
    }

    /**
     * Заполняется только для ВО
     *
     * @return Направленность ВО (только для ВО).
     */
    public EduProgramSpecialization getEduProgramSpecialization()
    {
        return _eduProgramSpecialization;
    }

    /**
     * @param eduProgramSpecialization Направленность ВО (только для ВО).
     */
    public void setEduProgramSpecialization(EduProgramSpecialization eduProgramSpecialization)
    {
        dirty(_eduProgramSpecialization, eduProgramSpecialization);
        _eduProgramSpecialization = eduProgramSpecialization;
    }

    /**
     * При сортировке по этому полю сначала будут идти НПм без направленности, потом с общей, потом по названию направленности. Обновляется регулярным процессом: todo
     *
     * @return Поле для сортировки по направленности.
     */
    @Length(max=255)
    public String getEduProgramSpecializationSortingCache()
    {
        return _eduProgramSpecializationSortingCache;
    }

    /**
     * @param eduProgramSpecializationSortingCache Поле для сортировки по направленности.
     */
    public void setEduProgramSpecializationSortingCache(String eduProgramSpecializationSortingCache)
    {
        dirty(_eduProgramSpecializationSortingCache, eduProgramSpecializationSortingCache);
        _eduProgramSpecializationSortingCache = eduProgramSpecializationSortingCache;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Ступень образования. Свойство не может быть null.
     */
    @NotNull
    public StructureEducationLevels getLevelType()
    {
        return _levelType;
    }

    /**
     * @param levelType Ступень образования. Свойство не может быть null.
     */
    public void setLevelType(StructureEducationLevels levelType)
    {
        dirty(_levelType, levelType);
        _levelType = levelType;
    }

    /**
     * @return Подчинено направлению подготовки (специальности).
     */
    public EducationLevels getParentLevel()
    {
        return _parentLevel;
    }

    /**
     * @param parentLevel Подчинено направлению подготовки (специальности).
     */
    public void setParentLevel(EducationLevels parentLevel)
    {
        dirty(_parentLevel, parentLevel);
        _parentLevel = parentLevel;
    }

    /**
     * @return Квалификация.
     */
    public Qualifications getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(Qualifications qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return ОКСО.
     */
    @Length(max=255)
    public String getOkso()
    {
        return _okso;
    }

    /**
     * @param okso ОКСО.
     */
    public void setOkso(String okso)
    {
        dirty(_okso, okso);
        _okso = okso;
    }

    /**
     * @return Ближайшее родительское ОКСО. Автообновляемое.
     */
    @Length(max=255)
    public String getInheritedOkso()
    {
        return _inheritedOkso;
    }

    /**
     * @param inheritedOkso Ближайшее родительское ОКСО. Автообновляемое.
     */
    public void setInheritedOkso(String inheritedOkso)
    {
        dirty(_inheritedOkso, inheritedOkso);
        _inheritedOkso = inheritedOkso;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationLevelsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EducationLevels)another).getCode());
                setCatalogCode(((EducationLevels)another).getCatalogCode());
            }
            setTitle(((EducationLevels)another).getTitle());
            setEduProgramSubject(((EducationLevels)another).getEduProgramSubject());
            setEduProgramSpecialization(((EducationLevels)another).getEduProgramSpecialization());
            setEduProgramSpecializationSortingCache(((EducationLevels)another).getEduProgramSpecializationSortingCache());
            setShortTitle(((EducationLevels)another).getShortTitle());
            setLevelType(((EducationLevels)another).getLevelType());
            setParentLevel(((EducationLevels)another).getParentLevel());
            setQualification(((EducationLevels)another).getQualification());
            setOkso(((EducationLevels)another).getOkso());
            setInheritedOkso(((EducationLevels)another).getInheritedOkso());
        }
    }

    public INaturalId<EducationLevelsGen> getNaturalId()
    {
        return new NaturalId(getCode(), getCatalogCode());
    }

    public static class NaturalId extends NaturalIdBase<EducationLevelsGen>
    {
        private static final String PROXY_NAME = "EducationLevelsNaturalProxy";

        private String _code;
        private String _catalogCode;

        public NaturalId()
        {}

        public NaturalId(String code, String catalogCode)
        {
            _code = code;
            _catalogCode = catalogCode;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getCatalogCode()
        {
            return _catalogCode;
        }

        public void setCatalogCode(String catalogCode)
        {
            _catalogCode = catalogCode;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EducationLevelsGen.NaturalId) ) return false;

            EducationLevelsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            if( !equals(getCatalogCode(), that.getCatalogCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            result = hashCode(result, getCatalogCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            sb.append("/");
            sb.append(getCatalogCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevels.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EducationLevels is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "catalogCode":
                    return obj.getCatalogCode();
                case "title":
                    return obj.getTitle();
                case "eduProgramSubject":
                    return obj.getEduProgramSubject();
                case "eduProgramSpecialization":
                    return obj.getEduProgramSpecialization();
                case "eduProgramSpecializationSortingCache":
                    return obj.getEduProgramSpecializationSortingCache();
                case "shortTitle":
                    return obj.getShortTitle();
                case "levelType":
                    return obj.getLevelType();
                case "parentLevel":
                    return obj.getParentLevel();
                case "qualification":
                    return obj.getQualification();
                case "okso":
                    return obj.getOkso();
                case "inheritedOkso":
                    return obj.getInheritedOkso();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "catalogCode":
                    obj.setCatalogCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "eduProgramSubject":
                    obj.setEduProgramSubject((EduProgramSubject) value);
                    return;
                case "eduProgramSpecialization":
                    obj.setEduProgramSpecialization((EduProgramSpecialization) value);
                    return;
                case "eduProgramSpecializationSortingCache":
                    obj.setEduProgramSpecializationSortingCache((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "levelType":
                    obj.setLevelType((StructureEducationLevels) value);
                    return;
                case "parentLevel":
                    obj.setParentLevel((EducationLevels) value);
                    return;
                case "qualification":
                    obj.setQualification((Qualifications) value);
                    return;
                case "okso":
                    obj.setOkso((String) value);
                    return;
                case "inheritedOkso":
                    obj.setInheritedOkso((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "catalogCode":
                        return true;
                case "title":
                        return true;
                case "eduProgramSubject":
                        return true;
                case "eduProgramSpecialization":
                        return true;
                case "eduProgramSpecializationSortingCache":
                        return true;
                case "shortTitle":
                        return true;
                case "levelType":
                        return true;
                case "parentLevel":
                        return true;
                case "qualification":
                        return true;
                case "okso":
                        return true;
                case "inheritedOkso":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "catalogCode":
                    return true;
                case "title":
                    return true;
                case "eduProgramSubject":
                    return true;
                case "eduProgramSpecialization":
                    return true;
                case "eduProgramSpecializationSortingCache":
                    return true;
                case "shortTitle":
                    return true;
                case "levelType":
                    return true;
                case "parentLevel":
                    return true;
                case "qualification":
                    return true;
                case "okso":
                    return true;
                case "inheritedOkso":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "catalogCode":
                    return String.class;
                case "title":
                    return String.class;
                case "eduProgramSubject":
                    return EduProgramSubject.class;
                case "eduProgramSpecialization":
                    return EduProgramSpecialization.class;
                case "eduProgramSpecializationSortingCache":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "levelType":
                    return StructureEducationLevels.class;
                case "parentLevel":
                    return EducationLevels.class;
                case "qualification":
                    return Qualifications.class;
                case "okso":
                    return String.class;
                case "inheritedOkso":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationLevels> _dslPath = new Path<EducationLevels>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevels");
    }
            

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getCatalogCode()
     */
    public static PropertyPath<String> catalogCode()
    {
        return _dslPath.catalogCode();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * Заполняется только для тех элементов, по которым можно создавать НПв: educationLevelsHighSchool.
     * Если связь заполнена, это означает, что НПм либо сформировано на основе классификатора (либо приравнено к таковой пользователем),
     * в этом случае название, коды (и прочая информация) будут взяты из элемента классификатора.
     *
     * @return Направление профессионального образования.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getEduProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> eduProgramSubject()
    {
        return _dslPath.eduProgramSubject();
    }

    /**
     * Заполняется только для ВО
     *
     * @return Направленность ВО (только для ВО).
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getEduProgramSpecialization()
     */
    public static EduProgramSpecialization.Path<EduProgramSpecialization> eduProgramSpecialization()
    {
        return _dslPath.eduProgramSpecialization();
    }

    /**
     * При сортировке по этому полю сначала будут идти НПм без направленности, потом с общей, потом по названию направленности. Обновляется регулярным процессом: todo
     *
     * @return Поле для сортировки по направленности.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getEduProgramSpecializationSortingCache()
     */
    public static PropertyPath<String> eduProgramSpecializationSortingCache()
    {
        return _dslPath.eduProgramSpecializationSortingCache();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Ступень образования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getLevelType()
     */
    public static StructureEducationLevels.Path<StructureEducationLevels> levelType()
    {
        return _dslPath.levelType();
    }

    /**
     * @return Подчинено направлению подготовки (специальности).
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getParentLevel()
     */
    public static EducationLevels.Path<EducationLevels> parentLevel()
    {
        return _dslPath.parentLevel();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getQualification()
     */
    public static Qualifications.Path<Qualifications> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return ОКСО.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getOkso()
     */
    public static PropertyPath<String> okso()
    {
        return _dslPath.okso();
    }

    /**
     * @return Ближайшее родительское ОКСО. Автообновляемое.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getInheritedOkso()
     */
    public static PropertyPath<String> inheritedOkso()
    {
        return _dslPath.inheritedOkso();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getDisplayableShortTitle()
     */
    public static SupportedPropertyPath<String> displayableShortTitle()
    {
        return _dslPath.displayableShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getFullTitleWithNewData()
     */
    public static SupportedPropertyPath<String> fullTitleWithNewData()
    {
        return _dslPath.fullTitleWithNewData();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getFullTitleWithRootLevel()
     */
    public static SupportedPropertyPath<String> fullTitleWithRootLevel()
    {
        return _dslPath.fullTitleWithRootLevel();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getInheritedOksoPrefix()
     */
    public static SupportedPropertyPath<String> inheritedOksoPrefix()
    {
        return _dslPath.inheritedOksoPrefix();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSpecializationTitle()
     */
    public static SupportedPropertyPath<String> programSpecializationTitle()
    {
        return _dslPath.programSpecializationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSpecializationTitleIfChild()
     */
    public static SupportedPropertyPath<String> programSpecializationTitleIfChild()
    {
        return _dslPath.programSpecializationTitleIfChild();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSubjectTitleWithCode()
     */
    public static SupportedPropertyPath<String> programSubjectTitleWithCode()
    {
        return _dslPath.programSubjectTitleWithCode();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSubjectWithCodeIndexAndGenTitle()
     */
    public static SupportedPropertyPath<String> programSubjectWithCodeIndexAndGenTitle()
    {
        return _dslPath.programSubjectWithCodeIndexAndGenTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getQualificationTitle()
     */
    public static SupportedPropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getTitleCodePrefix()
     */
    public static SupportedPropertyPath<String> titleCodePrefix()
    {
        return _dslPath.titleCodePrefix();
    }

    public static class Path<E extends EducationLevels> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _catalogCode;
        private PropertyPath<String> _title;
        private EduProgramSubject.Path<EduProgramSubject> _eduProgramSubject;
        private EduProgramSpecialization.Path<EduProgramSpecialization> _eduProgramSpecialization;
        private PropertyPath<String> _eduProgramSpecializationSortingCache;
        private PropertyPath<String> _shortTitle;
        private StructureEducationLevels.Path<StructureEducationLevels> _levelType;
        private EducationLevels.Path<EducationLevels> _parentLevel;
        private Qualifications.Path<Qualifications> _qualification;
        private PropertyPath<String> _okso;
        private PropertyPath<String> _inheritedOkso;
        private SupportedPropertyPath<String> _displayableShortTitle;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<String> _fullTitle;
        private SupportedPropertyPath<String> _fullTitleWithNewData;
        private SupportedPropertyPath<String> _fullTitleWithRootLevel;
        private SupportedPropertyPath<String> _inheritedOksoPrefix;
        private SupportedPropertyPath<String> _programSpecializationTitle;
        private SupportedPropertyPath<String> _programSpecializationTitleIfChild;
        private SupportedPropertyPath<String> _programSubjectTitleWithCode;
        private SupportedPropertyPath<String> _programSubjectWithCodeIndexAndGenTitle;
        private SupportedPropertyPath<String> _qualificationTitle;
        private SupportedPropertyPath<String> _titleCodePrefix;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EducationLevelsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getCatalogCode()
     */
        public PropertyPath<String> catalogCode()
        {
            if(_catalogCode == null )
                _catalogCode = new PropertyPath<String>(EducationLevelsGen.P_CATALOG_CODE, this);
            return _catalogCode;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EducationLevelsGen.P_TITLE, this);
            return _title;
        }

    /**
     * Заполняется только для тех элементов, по которым можно создавать НПв: educationLevelsHighSchool.
     * Если связь заполнена, это означает, что НПм либо сформировано на основе классификатора (либо приравнено к таковой пользователем),
     * в этом случае название, коды (и прочая информация) будут взяты из элемента классификатора.
     *
     * @return Направление профессионального образования.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getEduProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> eduProgramSubject()
        {
            if(_eduProgramSubject == null )
                _eduProgramSubject = new EduProgramSubject.Path<EduProgramSubject>(L_EDU_PROGRAM_SUBJECT, this);
            return _eduProgramSubject;
        }

    /**
     * Заполняется только для ВО
     *
     * @return Направленность ВО (только для ВО).
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getEduProgramSpecialization()
     */
        public EduProgramSpecialization.Path<EduProgramSpecialization> eduProgramSpecialization()
        {
            if(_eduProgramSpecialization == null )
                _eduProgramSpecialization = new EduProgramSpecialization.Path<EduProgramSpecialization>(L_EDU_PROGRAM_SPECIALIZATION, this);
            return _eduProgramSpecialization;
        }

    /**
     * При сортировке по этому полю сначала будут идти НПм без направленности, потом с общей, потом по названию направленности. Обновляется регулярным процессом: todo
     *
     * @return Поле для сортировки по направленности.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getEduProgramSpecializationSortingCache()
     */
        public PropertyPath<String> eduProgramSpecializationSortingCache()
        {
            if(_eduProgramSpecializationSortingCache == null )
                _eduProgramSpecializationSortingCache = new PropertyPath<String>(EducationLevelsGen.P_EDU_PROGRAM_SPECIALIZATION_SORTING_CACHE, this);
            return _eduProgramSpecializationSortingCache;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EducationLevelsGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Ступень образования. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getLevelType()
     */
        public StructureEducationLevels.Path<StructureEducationLevels> levelType()
        {
            if(_levelType == null )
                _levelType = new StructureEducationLevels.Path<StructureEducationLevels>(L_LEVEL_TYPE, this);
            return _levelType;
        }

    /**
     * @return Подчинено направлению подготовки (специальности).
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getParentLevel()
     */
        public EducationLevels.Path<EducationLevels> parentLevel()
        {
            if(_parentLevel == null )
                _parentLevel = new EducationLevels.Path<EducationLevels>(L_PARENT_LEVEL, this);
            return _parentLevel;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getQualification()
     */
        public Qualifications.Path<Qualifications> qualification()
        {
            if(_qualification == null )
                _qualification = new Qualifications.Path<Qualifications>(L_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return ОКСО.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getOkso()
     */
        public PropertyPath<String> okso()
        {
            if(_okso == null )
                _okso = new PropertyPath<String>(EducationLevelsGen.P_OKSO, this);
            return _okso;
        }

    /**
     * @return Ближайшее родительское ОКСО. Автообновляемое.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getInheritedOkso()
     */
        public PropertyPath<String> inheritedOkso()
        {
            if(_inheritedOkso == null )
                _inheritedOkso = new PropertyPath<String>(EducationLevelsGen.P_INHERITED_OKSO, this);
            return _inheritedOkso;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getDisplayableShortTitle()
     */
        public SupportedPropertyPath<String> displayableShortTitle()
        {
            if(_displayableShortTitle == null )
                _displayableShortTitle = new SupportedPropertyPath<String>(EducationLevelsGen.P_DISPLAYABLE_SHORT_TITLE, this);
            return _displayableShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EducationLevelsGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EducationLevelsGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getFullTitleWithNewData()
     */
        public SupportedPropertyPath<String> fullTitleWithNewData()
        {
            if(_fullTitleWithNewData == null )
                _fullTitleWithNewData = new SupportedPropertyPath<String>(EducationLevelsGen.P_FULL_TITLE_WITH_NEW_DATA, this);
            return _fullTitleWithNewData;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getFullTitleWithRootLevel()
     */
        public SupportedPropertyPath<String> fullTitleWithRootLevel()
        {
            if(_fullTitleWithRootLevel == null )
                _fullTitleWithRootLevel = new SupportedPropertyPath<String>(EducationLevelsGen.P_FULL_TITLE_WITH_ROOT_LEVEL, this);
            return _fullTitleWithRootLevel;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getInheritedOksoPrefix()
     */
        public SupportedPropertyPath<String> inheritedOksoPrefix()
        {
            if(_inheritedOksoPrefix == null )
                _inheritedOksoPrefix = new SupportedPropertyPath<String>(EducationLevelsGen.P_INHERITED_OKSO_PREFIX, this);
            return _inheritedOksoPrefix;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSpecializationTitle()
     */
        public SupportedPropertyPath<String> programSpecializationTitle()
        {
            if(_programSpecializationTitle == null )
                _programSpecializationTitle = new SupportedPropertyPath<String>(EducationLevelsGen.P_PROGRAM_SPECIALIZATION_TITLE, this);
            return _programSpecializationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSpecializationTitleIfChild()
     */
        public SupportedPropertyPath<String> programSpecializationTitleIfChild()
        {
            if(_programSpecializationTitleIfChild == null )
                _programSpecializationTitleIfChild = new SupportedPropertyPath<String>(EducationLevelsGen.P_PROGRAM_SPECIALIZATION_TITLE_IF_CHILD, this);
            return _programSpecializationTitleIfChild;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSubjectTitleWithCode()
     */
        public SupportedPropertyPath<String> programSubjectTitleWithCode()
        {
            if(_programSubjectTitleWithCode == null )
                _programSubjectTitleWithCode = new SupportedPropertyPath<String>(EducationLevelsGen.P_PROGRAM_SUBJECT_TITLE_WITH_CODE, this);
            return _programSubjectTitleWithCode;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getProgramSubjectWithCodeIndexAndGenTitle()
     */
        public SupportedPropertyPath<String> programSubjectWithCodeIndexAndGenTitle()
        {
            if(_programSubjectWithCodeIndexAndGenTitle == null )
                _programSubjectWithCodeIndexAndGenTitle = new SupportedPropertyPath<String>(EducationLevelsGen.P_PROGRAM_SUBJECT_WITH_CODE_INDEX_AND_GEN_TITLE, this);
            return _programSubjectWithCodeIndexAndGenTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getQualificationTitle()
     */
        public SupportedPropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new SupportedPropertyPath<String>(EducationLevelsGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.catalog.EducationLevels#getTitleCodePrefix()
     */
        public SupportedPropertyPath<String> titleCodePrefix()
        {
            if(_titleCodePrefix == null )
                _titleCodePrefix = new SupportedPropertyPath<String>(EducationLevelsGen.P_TITLE_CODE_PREFIX, this);
            return _titleCodePrefix;
        }

        public Class getEntityClass()
        {
            return EducationLevels.class;
        }

        public String getEntityName()
        {
            return "educationLevels";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableShortTitle();

    public abstract String getDisplayableTitle();

    public abstract String getFullTitle();

    public abstract String getFullTitleWithNewData();

    public abstract String getFullTitleWithRootLevel();

    public abstract String getInheritedOksoPrefix();

    public abstract String getProgramSpecializationTitle();

    public abstract String getProgramSpecializationTitleIfChild();

    public abstract String getProgramSubjectTitleWithCode();

    public abstract String getProgramSubjectWithCodeIndexAndGenTitle();

    public abstract String getQualificationTitle();

    public abstract String getTitleCodePrefix();
}
