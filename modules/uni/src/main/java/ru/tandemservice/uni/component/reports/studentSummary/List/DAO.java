/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.studentSummary.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.report.StudentSummaryReport;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 23.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentSummaryReport.ENTITY_CLASS, "report");
        builder.add(MQExpression.eq("report", StudentSummaryReport.L_ORG_UNIT, model.getOrgUnit()));

        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
