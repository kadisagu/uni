/* $Id$ */
package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

/**
 * Интерфейс для класса, который умеет задавать контекст списку студентов.
 * Используется для формирования списков студентов и других запросов на их основе.
 *
 * @author Nikolay Fedorovskih
 * @since 24.10.2013
 */
public interface IEducationOrgUnitContextHandler
{
    /**
     * Добавляет условия в выборку студентов для задания контекста.
     * Например, в списке архивных студентов показывать только архивных.
     *
     * @param builder билдер запроса по студентам
     * @param studentAlias алиас для Student
     */
    void setStudentListContext(DQLSelectBuilder builder, String studentAlias, String educationOrgUnitAlias);

}