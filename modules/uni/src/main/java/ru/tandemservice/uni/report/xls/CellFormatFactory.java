/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.report.xls;

import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author agolubenko
 * @since 17.04.2009
 */
public class CellFormatFactory
{
    private WritableCellFormat _defaultCellFormat = new WritableCellFormat();

    private Set<WritableFont> _fontPool = new HashSet<>();
    private Set<WritableCellFormat> _cellFormatPool = new HashSet<>();

    private Map<String, WritableFont> _name2Font = new HashMap<>();
    private Map<String, WritableCellFormat> _name2Format = new HashMap<>();

    public WritableCellFormat getDefaultCellFormat()
    {
        return _defaultCellFormat;
    }

    public void setDefaultCellFormat(WritableCellFormat defaultCellFormat)
    {
        _defaultCellFormat = defaultCellFormat;
    }

    public WritableFont registerFont(String name, WritableFont newFont) throws Exception
    {
        _name2Font.put(name, newFont);
        return registerFont(newFont);
    }

    public WritableCellFormat registerCellFormat(String name, WritableCellFormat newFormat) throws Exception
    {
        _name2Format.put(name, newFormat);
        return registerCellFormat(newFormat);
    }

    public WritableFont registerFont(WritableFont newFont)
    {
        for (WritableFont font : _fontPool)
        {
            if (font.equals(newFont))
            {
                return font;
            }
        }
        _fontPool.add(newFont);
        return newFont;
    }

    public WritableCellFormat registerCellFormat(WritableCellFormat newFormat) throws Exception
    {
        for (WritableCellFormat format : _cellFormatPool)
        {
            if (newFormat.equals(format))
            {
                return format;
            }
        }
        _cellFormatPool.add(newFormat);
        return newFormat;
    }

    public WritableFont getFont(String name)
    {
        return _name2Font.get(name);
    }

    public WritableCellFormat getFormat(String name)
    {
        return _name2Format.get(name);
    }
}