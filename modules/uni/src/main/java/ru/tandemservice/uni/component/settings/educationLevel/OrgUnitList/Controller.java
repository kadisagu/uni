/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.settings.educationLevel.OrgUnitList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

/**
 * @author vip_delete
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        Model model = getModel(context);

        getDao().prepare(model);

        prepareDataSource(context);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<OrgUnitToKindRelation> dataSource = new DynamicListDataSource<>(component, this, 20);

        if (UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL.equals(model.getKindCode()))
        {
            dataSource.addColumn(new SimpleColumn("Название", OrgUnitToKindRelation.orgUnit().territorialTitle().s()).setClickable(false));
        } else
        {
            dataSource.addColumn(new SimpleColumn("Название", OrgUnitToKindRelation.orgUnit().title().s()).setClickable(false));
        }

        dataSource.addColumn(new SimpleColumn("Тип подразделения", OrgUnitToKindRelation.orgUnit().orgUnitType().title().s()).setClickable(false));

        if (UniDefines.CATALOG_ORGUNIT_KIND_FORMING.equals(model.getKindCode()) || UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL.equals(model.getKindCode()))
        {
            dataSource.addColumn(new ToggleColumn("Осуществляет обучение студентов вне групп", OrgUnitToKindRelation.allowAddStudent().s()).setListener("onClickEditAllowAddStudents"));
            dataSource.addColumn(new ToggleColumn("Осуществляет формирование групп", OrgUnitToKindRelation.allowAddGroup().s()).setListener("onClickEditAllowAddGroups"));
        }
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Исключить из списка «{0}»?", new Object[]{new String[]{OrgUnitToKindRelation.L_ORG_UNIT, OrgUnit.P_FULL_TITLE}}));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent context)
    {
        context.createDefaultChildRegion(new ComponentActivator(IUniComponents.ORGUNIT_ADD));
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().deleteRow(context);
    }

    public void onClickEditAllowAddStudents(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.setOrgUnitToKindRelationId((Long) context.getListenerParameter());
        getDao().updateAllowAddStudents(model);
    }

    public void onClickEditAllowAddGroups(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.setOrgUnitToKindRelationId((Long) context.getListenerParameter());
        getDao().updateAllowAddGroups(model);
    }

    public void onClickSearch(IBusinessComponent context)
    {
        getModel(context).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        Model model = getModel(context);
        model.setOrgUnitType(null);
        model.setOrgUnitTitle(null);
        onClickSearch(context);
    }
}
