package ru.tandemservice.uni.entity.education.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Семестр учебной сетки
 *
 * Семестр сетки показывает, что в рамках данной сетки указанный семестр соответствует определенному курсу обучения студента и проходится в определенной части учебного года.
 * Требования: при возрастании семестров курс не может убывать, в рамках курса не может меняться количество частей в году, части года в рамках курса строго возрастают.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DevelopGridTermGen extends EntityBase
 implements INaturalIdentifiable<DevelopGridTermGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.education.DevelopGridTerm";
    public static final String ENTITY_NAME = "developGridTerm";
    public static final int VERSION_HASH = 2098063940;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String L_TERM = "term";
    public static final String L_COURSE = "course";
    public static final String L_PART = "part";
    public static final String P_COURSE_NUMBER = "courseNumber";
    public static final String P_PART_AMOUNT = "partAmount";
    public static final String P_PART_NUMBER = "partNumber";
    public static final String P_TERM_NUMBER = "termNumber";

    private DevelopGrid _developGrid;     // Сетка
    private Term _term;     // Семестр
    private Course _course;     // Курс (для семестра)
    private YearDistributionPart _part;     // Часть учебного года (для семестра)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сетка. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Сетка. Свойство не может быть null.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Курс (для семестра). Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс (для семестра). Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Часть учебного года (для семестра). Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getPart()
    {
        return _part;
    }

    /**
     * @param part Часть учебного года (для семестра). Свойство не может быть null.
     */
    public void setPart(YearDistributionPart part)
    {
        dirty(_part, part);
        _part = part;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DevelopGridTermGen)
        {
            if (withNaturalIdProperties)
            {
                setDevelopGrid(((DevelopGridTerm)another).getDevelopGrid());
                setTerm(((DevelopGridTerm)another).getTerm());
            }
            setCourse(((DevelopGridTerm)another).getCourse());
            setPart(((DevelopGridTerm)another).getPart());
        }
    }

    public INaturalId<DevelopGridTermGen> getNaturalId()
    {
        return new NaturalId(getDevelopGrid(), getTerm());
    }

    public static class NaturalId extends NaturalIdBase<DevelopGridTermGen>
    {
        private static final String PROXY_NAME = "DevelopGridTermNaturalProxy";

        private Long _developGrid;
        private Long _term;

        public NaturalId()
        {}

        public NaturalId(DevelopGrid developGrid, Term term)
        {
            _developGrid = ((IEntity) developGrid).getId();
            _term = ((IEntity) term).getId();
        }

        public Long getDevelopGrid()
        {
            return _developGrid;
        }

        public void setDevelopGrid(Long developGrid)
        {
            _developGrid = developGrid;
        }

        public Long getTerm()
        {
            return _term;
        }

        public void setTerm(Long term)
        {
            _term = term;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DevelopGridTermGen.NaturalId) ) return false;

            DevelopGridTermGen.NaturalId that = (NaturalId) o;

            if( !equals(getDevelopGrid(), that.getDevelopGrid()) ) return false;
            if( !equals(getTerm(), that.getTerm()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDevelopGrid());
            result = hashCode(result, getTerm());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDevelopGrid());
            sb.append("/");
            sb.append(getTerm());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DevelopGridTermGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DevelopGridTerm.class;
        }

        public T newInstance()
        {
            return (T) new DevelopGridTerm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "developGrid":
                    return obj.getDevelopGrid();
                case "term":
                    return obj.getTerm();
                case "course":
                    return obj.getCourse();
                case "part":
                    return obj.getPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "part":
                    obj.setPart((YearDistributionPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "developGrid":
                        return true;
                case "term":
                        return true;
                case "course":
                        return true;
                case "part":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "developGrid":
                    return true;
                case "term":
                    return true;
                case "course":
                    return true;
                case "part":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "developGrid":
                    return DevelopGrid.class;
                case "term":
                    return Term.class;
                case "course":
                    return Course.class;
                case "part":
                    return YearDistributionPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DevelopGridTerm> _dslPath = new Path<DevelopGridTerm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DevelopGridTerm");
    }
            

    /**
     * @return Сетка. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Курс (для семестра). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Часть учебного года (для семестра). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> part()
    {
        return _dslPath.part();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getCourseNumber()
     */
    public static SupportedPropertyPath<Integer> courseNumber()
    {
        return _dslPath.courseNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getPartAmount()
     */
    public static SupportedPropertyPath<Integer> partAmount()
    {
        return _dslPath.partAmount();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getPartNumber()
     */
    public static SupportedPropertyPath<Integer> partNumber()
    {
        return _dslPath.partNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getTermNumber()
     */
    public static SupportedPropertyPath<Integer> termNumber()
    {
        return _dslPath.termNumber();
    }

    public static class Path<E extends DevelopGridTerm> extends EntityPath<E>
    {
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private Term.Path<Term> _term;
        private Course.Path<Course> _course;
        private YearDistributionPart.Path<YearDistributionPart> _part;
        private SupportedPropertyPath<Integer> _courseNumber;
        private SupportedPropertyPath<Integer> _partAmount;
        private SupportedPropertyPath<Integer> _partNumber;
        private SupportedPropertyPath<Integer> _termNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сетка. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Курс (для семестра). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Часть учебного года (для семестра). Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> part()
        {
            if(_part == null )
                _part = new YearDistributionPart.Path<YearDistributionPart>(L_PART, this);
            return _part;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getCourseNumber()
     */
        public SupportedPropertyPath<Integer> courseNumber()
        {
            if(_courseNumber == null )
                _courseNumber = new SupportedPropertyPath<Integer>(DevelopGridTermGen.P_COURSE_NUMBER, this);
            return _courseNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getPartAmount()
     */
        public SupportedPropertyPath<Integer> partAmount()
        {
            if(_partAmount == null )
                _partAmount = new SupportedPropertyPath<Integer>(DevelopGridTermGen.P_PART_AMOUNT, this);
            return _partAmount;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getPartNumber()
     */
        public SupportedPropertyPath<Integer> partNumber()
        {
            if(_partNumber == null )
                _partNumber = new SupportedPropertyPath<Integer>(DevelopGridTermGen.P_PART_NUMBER, this);
            return _partNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uni.entity.education.DevelopGridTerm#getTermNumber()
     */
        public SupportedPropertyPath<Integer> termNumber()
        {
            if(_termNumber == null )
                _termNumber = new SupportedPropertyPath<Integer>(DevelopGridTermGen.P_TERM_NUMBER, this);
            return _termNumber;
        }

        public Class getEntityClass()
        {
            return DevelopGridTerm.class;
        }

        public String getEntityName()
        {
            return "developGridTerm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract int getCourseNumber();

    public abstract int getPartAmount();

    public abstract int getPartNumber();

    public abstract int getTermNumber();
}
