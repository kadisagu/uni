package ru.tandemservice.uni.dao.mdbio;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.healthmarketscience.jackcess.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class StudentIODao extends BaseIODao implements IStudentIODao {

    protected String trim(String string, int len) {
        if (string.length() < len) { return string; }
        return (string.substring(0, len-4)+"...");
    }

    @Override
    public void export_EducationLevelsHighSchoolList(final Database mdb) throws IOException
    {
        if (null != mdb.getTable("edu_lvlhs_t")) { return; }

        IOrgStructIODao.instance.get().export_OrgStructureTable(mdb);

        final Table edu_lvlhs_t = new TableBuilder("edu_lvlhs_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("lvl_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("lvl_type", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("lvl_title", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("orgunit_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("orgunit_title", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("title_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        for (final EducationLevelsHighSchool eduHs: this.getList(
            EducationLevelsHighSchool.class,
            new String[] {
                EducationLevelsHighSchool.educationLevel().levelType().code().s(),
                EducationLevelsHighSchool.educationLevel().code().s(),
                EducationLevelsHighSchool.fullTitle().s()
            }
        )) {
            String path = "";
            StructureEducationLevels levelType = eduHs.getEducationLevel().getLevelType();
            while (null != levelType) {
                String title = StringUtils.trimToNull(levelType.getShortTitle());
                path = (null == title ? levelType.getTitle() : title) + (path.isEmpty() ? "" : " / " + path);
                levelType = levelType.getParent();
            }

            edu_lvlhs_t.addRow(
                Long.toHexString(eduHs.getId()),
                Long.toHexString(eduHs.getEducationLevel().getId()),
                path,
                eduHs.getEducationLevel().getDisplayableTitle(),
                Long.toHexString(eduHs.getOrgUnit().getId()),
                this.trim(eduHs.getOrgUnit().getTitle(), 200) + "("+eduHs.getOrgUnit().getOrgUnitType().getTitle()+")",
                this.trim(eduHs.getDisplayableTitle(), 250)
            );
        }
    }

    @Override
    public void export_EducationOrgUnitList(final Database mdb) throws IOException
    {
        if (null != mdb.getTable("edu_ou_t")) { return; }

        this.export_EducationLevelsHighSchoolList(mdb);

        final Table edu_ou_t = new TableBuilder("edu_ou_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("eduhs_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("formative_ou_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("territorial_ou_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("title_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("options_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("orgunit_p", DataType.MEMO).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        for (final EducationOrgUnit eduOu: this.getList(EducationOrgUnit.class)) {
            edu_ou_t.addRow(
                Long.toHexString(eduOu.getId()),
                Long.toHexString(eduOu.getEducationLevelHighSchool().getId()),
                Long.toHexString(eduOu.getFormativeOrgUnit().getId()),
                Long.toHexString(eduOu.getTerritorialOrgUnit().getId()),
                this.trim(eduOu.getEducationLevelHighSchool().getDisplayableTitle(), 250),
                DevelopUtil.getTitle(eduOu.getDevelopForm(), eduOu.getDevelopCondition(), eduOu.getDevelopTech(), eduOu.getDevelopPeriod()),
                this.trim(eduOu.getFormativeOrgUnit().getTypeTitle(), 150) + " (" + this.trim(eduOu.getTerritorialOrgUnit().getTerritorialShortTitle(), 100)+")"
            );
        }
    }


    @Override
    public void export_GroupList(final Database mdb, boolean includeArchiveStudents) throws IOException
    {
        if (null != mdb.getTable("group_t")) { return; }

        this.export_EducationOrgUnitList(mdb);

        final Table group_t = new TableBuilder("group_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("edu_ou_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("name_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder("year_p", DataType.INT).toColumn())
        .addColumn(new ColumnBuilder("course_p", DataType.INT).toColumn())
        .toTable(mdb);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Group.class, "g")
                .column(property("g"));

        if (includeArchiveStudents)
        {
            dql.where(or(
                    eq(property("g", Group.archival()), value(false)),
                    exists(Student.class,
                           Student.L_GROUP, property("g"))
            ));
        }
        else
        {
            dql.where(or(
                    eq(property("g", Group.archival()), value(false)),
                    exists(Student.class,
                           Student.L_GROUP, property("g"),
                           Student.P_ARCHIVAL, false)
            ));
        }

        for (final Group group: dql.createStatement(getSession()).<Group>list()) {
            group_t.addRow(
                Long.toHexString(group.getId()),
                Long.toHexString(group.getEducationOrgUnit().getId()),
                group.getTitle(),
                group.getStartEducationYear().getIntValue(),
                group.getCourse().getIntValue()
            );
        }
    }

    @Override
    public void export_StudentList(final Database mdb, boolean includeArchiveStudents) throws Exception
    {
        if (null != mdb.getTable("student_t")) { return; }

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<CompensationType, String> compensationTypeMap = catalogIO.catalogCompensationType().export(mdb);
        final Map<StudentCategory, String> studentCategoryMap = catalogIO.catalogStudentCategory().export(mdb);
        final Map<StudentStatus, String> studentStatusMap = catalogIO.catalogStudentStatusCategory().export(mdb);

        this.export_EducationOrgUnitList(mdb);
        this.export_GroupList(mdb, includeArchiveStudents);

        IPersonIODao.instance.get().export_PersonList(mdb, true);

        final TableBuilder tableBuilder = new TableBuilder("student_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("number_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("entrance_p", DataType.INT).toColumn())
        .addColumn(new ColumnBuilder("group_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("edu_ou_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("course_p", DataType.INT).toColumn())
        .addColumn(new ColumnBuilder("compensation_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder("category_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder("bookNumber_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder("status_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder("archival_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("finishYear_p", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("personalFileNumber_p", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
        .addColumn(new ColumnBuilder("specialPurposeRecruit_p", DataType.TEXT).setCompressedUnicode(true).toColumn());
        if (PersonEduDocumentManager.isShowNewEduDocuments()) {
            tableBuilder.addColumn(new ColumnBuilder("edu_document_id", DataType.TEXT).setCompressedUnicode(true).toColumn());
        }
        tableBuilder.addColumn(new ColumnBuilder("edu_document_original", DataType.TEXT).setCompressedUnicode(true).toColumn());

        final Table student_t = tableBuilder.toTable(mdb);

        final Session session = this.getSession();
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "e").column(DQLExpressions.property("e.id"));
        if (!includeArchiveStudents)
            builder.where(DQLExpressions.eq(DQLExpressions.property(StudentGen.archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
        final List<Long> ids = builder.createStatement(session).list();
        final List<Object[]> rows = new ArrayList<>(ids.size());

        BatchUtils.execute(ids, 128, ids1 -> {
            try {
                for (final Student student: StudentIODao.this.getList(Student.class, "id", ids1)) {
                    List<Object> fields = new ArrayList<>(17);

                    fields.add(Long.toHexString(student.getId()));
                    fields.add(Long.toHexString(student.getPerson().getId()));
                    fields.add(student.getPersonalNumber());
                    fields.add(student.getEntranceYear());
                    fields.add(null == student.getGroup() ? "" : Long.toHexString(student.getGroup().getId()));
                    fields.add(Long.toHexString(student.getEducationOrgUnit().getId()));
                    fields.add(student.getCourse().getIntValue());
                    fields.add(compensationTypeMap.get(student.getCompensationType()));
                    fields.add(studentCategoryMap.get(student.getStudentCategory()));
                    fields.add(student.getBookNumber());
                    fields.add(studentStatusMap.get(student.getStatus()));
                    fields.add(student.isArchival() ? "1" : "0");
                    fields.add(student.getFinishYear() == null ? "" : student.getFinishYear());
                    fields.add(student.getPersonalFileNumber() == null ? "" : student.getPersonalFileNumber());
                    fields.add(student.isTargetAdmission() ? "1" : "0");
                    if (PersonEduDocumentManager.isShowNewEduDocuments())
                    {
                        fields.add(student.getEduDocument() != null ? Long.toHexString(student.getEduDocument().getId()) : "");
                    }
                    fields.add(student.isEduDocumentOriginalHandedIn() ? "1" : "0");

                    rows.add(fields.toArray());
                }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();

        });
        student_t.addRows(rows);

        // экспортируем доп. данные - метод определяется в проектном слое
        export_StudentCustomData(mdb);
    }

    @Override
    public void export_StudentListBase(final Database mdb) throws IOException
    {
        final Table student_t = new TableBuilder("student_t")
        .addColumn(new ColumnBuilder("id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("eduhs_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        final Session session = this.getSession();
        final List<Long> ids =  new DQLSelectBuilder().fromEntity(Student.class, "e").column(property("e.id"))
        .where(DQLExpressions.eq(property(StudentGen.archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
        .createStatement(session)
        .list();

        BatchUtils.execute(ids, 128, ids1 -> {
            try {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .column(property(Student.id().fromAlias("s")))
                .column(property(Student.person().id().fromAlias("s")))
                .column(property(Student.educationOrgUnit().educationLevelHighSchool().id().fromAlias("s")))
                .where(in(property(Student.id().fromAlias("s")), ids1))
                ;

                for (final Object[] studentRow : dql.createStatement(session).<Object[]>list()) {
                    student_t.addRow(
                        Long.toHexString((Long) studentRow[0]),
                        Long.toHexString((Long) studentRow[1]),
                        Long.toHexString((Long) studentRow[2])
                    );
                }
                mdb.flush();
            }
            catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }

    @Override
    public Map<String, Long> doImport_GroupList(final Database mdb) throws Exception {

        // импортируем группы из таблицы групп

        final String CACHE_KEY = "StudentIODao.doImport_GroupList";
        {
            final Map<String, Long> groupIdMap = DaoCache.get(CACHE_KEY);
            if (null != groupIdMap) { return groupIdMap; }
        }

        final Table group_t = mdb.getTable("group_t");
        if (null == group_t) { throw new ApplicationException("Таблица «group_t» отсутствует в базе данных."); }

        // подгружаем справочники для импорта

        final Map<Integer, Course> courseMap = DevelopGridDAO.getCourseMap();
        final Map<Integer, EducationYear> yearMap = new HashMap<>();
        for (final EducationYear year : UniDaoFacade.getCoreDao().getList(EducationYear.class)) {
            yearMap.put(year.getIntValue(), year);
        }

        final Set<Long> eduOuIds = new HashSet<>();
        eduOuIds.addAll(new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eduOu").column("eduOu.id").createStatement(getSession()).<Long>list());

        // создаем группы пачками
        final Session session = this.getSession();
        final Map<String, Long> groupIdMap = Maps.newHashMapWithExpectedSize(group_t.getRowCount());
        execute(group_t, 32, "Загрузка групп", new BatchUtils.Action<Map<String, Object>>() {
            @Override public void execute(final Collection<Map<String, Object>> rows) {

                // подгружаем уже существующие группы
                final Map<String, Group> localGroupMap = new HashMap<>();
                {
                    final Set<Long> ids = Sets.newHashSetWithExpectedSize(rows.size());
                    for (final Map<String, Object> row: rows) {
                        final Long id = tryParseHexId((String)row.get("id"));
                        if (id != null) {
                            ids.add(id);
                        }
                    }

                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Group.class, "g").column(DQLExpressions.property("g"));
                    dql.where(DQLExpressions.in(DQLExpressions.property("g.id"), ids));
                    for (final Group group: dql.createStatement(session).<Group>list()) {
                        localGroupMap.put(Long.toHexString(group.getId()), group);
                    }
                }


                for (final Map<String, Object> row: rows) {
                    final String id = (String) row.get("id");
                    try {
                        if (id == null)
                            throw new IllegalStateException("Field value for 'id' must not be null.");

                        final Group group = this.createGroup(localGroupMap, id);

                        group.setTitle(StringUtils.trimToEmpty((String)row.get("name_p")));

                        group.setEducationOrgUnit(parseEduOu(row, eduOuIds));
                        group.setCourse(parseCourse(row, courseMap));

                        parseAndSetEduYear(row, group);

                        session.saveOrUpdate(group);
                        groupIdMap.put(id, group.getId());

                    } catch (final Throwable t) {
                        log4j_logger.fatal("Error while processing: group_t[" + id + "]. See info below.");
                        log4j_logger.fatal(t.getMessage(), t);
                        throw new ApplicationException("Группа «" + id + "»: Произошла ошибка обработки записи.", t);
                    }
                }

                System.err.print(".");
                session.flush();
                session.clear();
            }

            private void parseAndSetEduYear(Map<String, Object> row, Group group)
            {
                Object yearStr = row.get("year_p");
                try
                {
                    final Number number = (Number) yearStr;
                    final EducationYear year = (null == number ? null : yearMap.get(number.intValue()));
                    if (null == year)
                        throw new IllegalArgumentException("Field value for 'year_p' is not valid format: '" + yearStr + "'.");

                    group.setStartEducationYear(year);
                }
                catch (ClassCastException e)
                {
                    throw new IllegalArgumentException("Field value for 'year_p' is not valid format: '" + yearStr + "'.");
                }
            }

            private Group createGroup(final Map<String, Group> localGroupMap, final String id) {
                Group g = localGroupMap.get(id);
                if (null == g) {
                    localGroupMap.put(id, g = new Group());
                    g.setCreationDate(new Date());
                }
                return g;
            }
        });

        session.flush();
        session.clear();

        DaoCache.put(CACHE_KEY, groupIdMap);
        return groupIdMap;
    }

    @Override
    @SuppressWarnings("RedundantCast")
    public Map<String, Long> doImport_StudentList(final Database mdb) throws Exception
    {
        final String CACHE_KEY = "StudentIODao.doImport_StudentList";
        {
            final Map<String, Long> studentIdMap = DaoCache.get(CACHE_KEY);
            if (null != studentIdMap) { return studentIdMap; }
        }

        final Table student_t = mdb.getTable("student_t");
        if (null == student_t) {
            throw new ApplicationException("Таблица «student_t» отсутствует в базе данных.");
        }

        // импортируем персон
        final Map<String, Long> personIdMap = IPersonIODao.instance.get().doImport_PersonList(mdb);

        // Документы об образовании
        final Map<String, Long> eduDocumentMap = PersonEduDocumentManager.isShowNewEduDocuments() ?
                IPersonIODao.instance.get().doImport_PersonEduDocument(mdb, personIdMap) : null;

        // импортируем группы
        final Map<String, Long> groupIdMap = doImport_GroupList(mdb);

        // справочники для импорта
        final Map<Integer, Course> courseMap = DevelopGridDAO.getCourseMap();

        final StudentStatus statusActive = getByCode(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);

        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
        final Map<String, CompensationType> compensationTypeMap = catalogIO.catalogCompensationType().lookup(true);
        final Map<String, StudentCategory> studentCategoryMap = catalogIO.catalogStudentCategory().lookup(true);
        final Map<String, StudentStatus> studentStatusMap = catalogIO.catalogStudentStatusCategory().lookup(true);

        final Set<Long> eduOuIds = new HashSet<>();
        eduOuIds.addAll(new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eduOu").column("eduOu.id").createStatement(getSession()).<Long>list());

        // создаем студентов, пишем в базу пачками по 32 штуки
        final Session session = getSession();
        final Map<String, Long> studentIdMap = Maps.newHashMapWithExpectedSize(student_t.getRowCount());
        final Set<String> personalNumbers = Sets.newHashSetWithExpectedSize(student_t.getRowCount());
        final BatchUtils.Action<Map<String, Object>> action = new BatchUtils.Action<Map<String, Object>>()
        {
            @Override public void execute(final Collection<Map<String, Object>> rows) {

                // подгружаем уже существующих в базе студентов по id
                final Map<String, Student> localStudentMap = Maps.newHashMapWithExpectedSize(rows.size());
                {
                    final Set<Long> ids = Sets.newHashSetWithExpectedSize(rows.size());
                    for (final Map<String, Object> row : rows) {
                        final Long id = tryParseHexId((String) row.get("id"));
                        if (id != null) {
                            ids.add(id);
                        }
                    }

                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, "s").column(DQLExpressions.property("s"));
                    dql.where(DQLExpressions.in(DQLExpressions.property("s.id"), ids));
                    for (final Student student : dql.createStatement(session).<Student>list()) {
                        localStudentMap.put(Long.toHexString(student.getId()), student);
                    }
                }

                for (final Map<String, Object> row : rows) {
                    final String id = (String) row.get("id");
                    final String context = "student_t[" + id + "]";
                    try {
                        if (StringUtils.isBlank(id))
                            throw new IllegalStateException("Field value for 'id' must not be null.");

                        final Student student = this.createStudent(localStudentMap, id);

                        student.setBookNumber(StringUtils.trimToNull((String) row.get("bookNumber_p")));

                        student.setStatus(studentStatusMap.get((String) row.get("status_p")));

                        student.setArchival(getBoolean(row, "archival_p", context, true));

                        parseAndSetStudentPerson(row, student, personIdMap);

                        final EducationOrgUnit educationOrgUnit = parseEduOu(row, eduOuIds);
                        student.setEducationOrgUnit(educationOrgUnit);

                        student.setDevelopPeriodAuto(educationOrgUnit.getDevelopPeriod());

                        parseAndSetStudentGroup(row, student, groupIdMap);

                        student.setEntranceYear(Math.max(1900, StudentIODao.this.integer(row, "entrance_p", context, true)));

                        student.setCourse(parseCourse(row, courseMap));

                        student.setTargetAdmission(getBoolean(row, "specialPurposeRecruit_p", context, true));

                        student.setCompensationType(compensationTypeMap.get((String) row.get("compensation_p")));

                        student.setStudentCategory(studentCategoryMap.get((String) row.get("category_p")));

                        student.setFinishYear(StudentIODao.this.integer(row, "finishYear_p", context, false));

                        student.setPersonalFileNumber(StringUtils.trimToNull((String) row.get("personalFileNumber_p")));

                        parseAndSetEduDocument(row, student);

                        student.setEduDocumentOriginalHandedIn(getBoolean(row, "edu_document_original", context, true));

                        // Номер студенту присваиваем в самом конце - он может генерироваться на основе каких-то других данных студента
                        String personalNumber = StringUtils.trimToNull((String) row.get("number_p"));
                        if (StringUtils.isBlank(personalNumber))
                        {
                            personalNumber = IStudentDAO.instance.get().generateNewPersonalNumber(student);
                            if (personalNumber == null)
                                throw new IllegalArgumentException("Для " + student.getEntranceYear() + " года не осталось свободных персональных номеров.");
                        }
                        else if (!personalNumbers.add(personalNumber))
                        {
                            throw new IOFatalException("Персональный номер студента (number_p) должен быть уникален.");
                        }
                        student.setPersonalNumber(personalNumber);

                        session.saveOrUpdate(student);
                        studentIdMap.put(id, student.getId());
                    }
                    catch (IOFatalException e)
                    {
                        throw new ApplicationException("Студент «" + id + "»: Произошла ошибка обработки записи.", e);
                    }
                    catch (Throwable t)
                    {
                        log4j_logger.fatal("Error while processing: student_t[" + id + "]. See info below.");
                        log4j_logger.fatal(t.getMessage(), t);
                        throw new ApplicationException("Студент «" + id + "»: Произошла ошибка обработки записи.", t);
                    }
                    session.flush();
                }

                System.err.print(".");
                session.clear();
            }

            private Student createStudent(final Map<String, Student> localStudentMap, final String id)
            {
                Student s = localStudentMap.get(id);
                if (null == s) {
                    localStudentMap.put(id, s = new Student());
                    s.setStatus(statusActive);
                }
                return s;
            }

            private void parseAndSetEduDocument(Map<String, Object> row, Student student)
            {
                if (eduDocumentMap != null)
                {
                    final String eduDocumentIdStr = StringUtils.trimToNull((String) row.get("edu_document_id"));
                    if (eduDocumentIdStr != null)
                    {
                        final Long eduDocumentId = eduDocumentMap.get(eduDocumentIdStr);
                        if (eduDocumentId == null)
                            throw new IllegalArgumentException("row with id '" + eduDocumentIdStr + "' (edu_document_id) is not found in table '" + PersonIODao.PERSON_EDU_DOCUMENT_MDB_TABLE + "'.");

                        student.setEduDocument((PersonEduDocument) session.load(PersonEduDocument.class, eduDocumentId));
                    }
                }
            }

            private void parseAndSetStudentGroup(Map<String, Object> row, Student student, Map<String, Long> groupIdMap)
            {
                final String group_id = StringUtils.trimToNull((String) row.get("group_id"));
                Long groupId = null;
                if (group_id != null)
                {
                    groupId = groupIdMap.get(group_id);
                    if (groupId == null)
                        throw new IllegalArgumentException("No entry for group_id=" + group_id);
                }

                final Group group = (null == groupId ? null : (Group) getSession().load(Group.class, groupId));
                student.setGroup(group);
            }

            private void parseAndSetStudentPerson(Map<String, Object> row, Student student, Map<String, Long> personIdMap)
            {
                Object person_id = row.get("person_id");

                if (StringUtils.isBlank((String) person_id))
                    throw new IllegalStateException("Field value for 'person_id' must not be null.");

                final Long personId = personIdMap.get(person_id);
                if (null == personId) {
                    throw new IllegalArgumentException("Person with id = '" + person_id + "' is not found in the database.");
                }
                final Person person = (Person) getSession().load(Person.class, personId);
                student.setPerson(person);
            }
        };

        // Сначала action надо сделать для строк, где персональный номер указан, затем для остальных,
        // чтобы при генерации номера новому студенту не занять номер из mdb
        final List<Map<String, Object>> withNumberRows = new ArrayList<>(student_t.getRowCount());
        final List<Map<String, Object>> withoutNumberRows = new ArrayList<>(student_t.getRowCount());
        for (Map<String, Object> row : student_t)
        {
            if (StringUtils.isBlank((String) row.get("number_p")))
                withoutNumberRows.add(row);
            else
                withNumberRows.add(row);
        }
        final int counter = execute(withNumberRows, 0, student_t.getRowCount(), 32, "Студенты", action);
        execute(withoutNumberRows, counter, student_t.getRowCount(), 32, "Студенты", action);

        DaoCache.put(CACHE_KEY, studentIdMap);

        // импортируем доп. данные - метод определяется в проектном слое
        doImport_StudentCustomData(mdb, studentIdMap, personIdMap, groupIdMap);

        return studentIdMap;
    }

    private Course parseCourse(Map<String, Object> row, Map<Integer, Course> courseMap)
    {
        final Number number = (Number) row.get("course_p");
        final Course course = (null == number ? null : courseMap.get(number.intValue()));
        if (null == course) {
            throw new IllegalArgumentException("Field value for 'course_p' must not be null.");
        }
        return course;
    }

    private EducationOrgUnit parseEduOu(Map<String, Object> row, Set<Long> eduOuIds)
    {
        final String eduOuIdStr = (String) row.get("edu_ou_id");
        if (StringUtils.isBlank(eduOuIdStr)) {
            throw new IllegalStateException("Field value for 'edu_ou_id' must not be null.");
        }

        final Long eduOuId = tryParseHexId(eduOuIdStr);

        if (eduOuId == null)
            throw new IllegalStateException("Field value for 'edu_ou_id' is not valid format (should be long as hex): '" + eduOuIdStr + "'.");

        if (!eduOuIds.contains(eduOuId))
            throw new IllegalStateException("Education orgunit with id = '" + eduOuIdStr + "' is not found in the database.");

        return (EducationOrgUnit) getSession().load(EducationOrgUnit.class, eduOuId);
    }

    /**
     * Обработка дополнительных таблиц из проектных слоев.
     * @param mdb файл сторонней базы
     * @param studentIdMap id студента в mdb -> id студента в сессии
     * @param personIdMap id персоны в mdb -> id персоны в сессии
     * @param groupIdMap id группы в mdb -> id группы в сессии
     */
    @SuppressWarnings("UnusedParameters")
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9864175")
    protected void doImport_StudentCustomData(final Database mdb, final Map<String, Long> studentIdMap, final Map<String, Long> personIdMap, final Map<String, Long> groupIdMap) throws Exception
    {
        // override in child (unifefu)
    }

    /**
     * Экспорт дополнительных таблиц из проектных слоев.
     * @param mdb файл сторонней базы
     */
    @SuppressWarnings("UnusedParameters")
    protected void export_StudentCustomData(final Database mdb) throws Exception
    {
        // override in child (unifefu)
    }
}
