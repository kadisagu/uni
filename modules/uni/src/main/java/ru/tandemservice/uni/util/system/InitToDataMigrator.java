/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.util.system;

/**
 * МЕГА МИГРАТОР МЕТАИНФЫ ПО СПРАВОЧНИКАМ В УНИ
 * <p/>
 * *.init.xml -> *.data.xml
 * *.groups.xml -> *.group.xml
 * <p/>
 * создан только для запуска 1 раз (уже запущен)
 * поэтому можно только смотреть этот мега код
 *
 * @author vip_delete
 * @since 06.10.2009
 */
public class InitToDataMigrator
{
//    public static File getModulesDir()
//    {
//        String modules = System.getProperty("modules");
//
//        if (StringUtils.isEmpty(modules)) throw new RuntimeException("System property 'modules' is not specified");
//
//        File file = new File(modules);
//
//        if (!file.exists()) throw new RuntimeException("Path '" + modules + "' is not exist");
//
//        if (!file.isDirectory()) throw new RuntimeException("Path '" + modules + "' is not a directory");
//
//        return file;
//    }
//
//    public static void main(String[] args) throws Exception
//    {
//        Map<String, String> code2entityName = new HashMap<String, String>();
//        Map<String, String> code2title = new HashMap<String, String>();
//        Set<String> systemSyncCatalogs = new HashSet<String>();
//        Map<String, String> entity2title = new HashMap<String, String>();
//
//        File moduleDir = getModulesDir();
//        List<Object[]> initFileList = new ArrayList<Object[]>();
//
//        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(Thread.currentThread().getContextClassLoader());
//        String pathX = "file:" + moduleDir.getPath() + "/**/src/**/*.init.xml";
//        Resource[] fileList = resolver.getResources(pathX);
//
//        OutputFormat format = new OutputFormat();
//        format.setIndentSize(4);
//        format.setNewlines(true);
//        format.setTrimText(true);
//        format.setPadText(true);
//
//        for (Resource resource : fileList)
//        {
//            File file = resource.getFile();
//            String name = file.getName().substring(0, file.getName().lastIndexOf(".init.xml"));
//            String outputFileName = file.getParent() + "/" + name + ".data.xml";
//
//            // read input *.init.xml
//            Document input = Dom4jUtil.readStream(new FileInputStream(file));
//            Element inputRoot = input.getRootElement();
//
//            initFileList.add(new Object[]{inputRoot, outputFileName});
//
//            for (Iterator iterator = inputRoot.elementIterator(); iterator.hasNext();)
//            {
//                Element catalog = (Element) iterator.next();
//                if (catalog.getName().equals("catalog"))
//                {
//                    String catalogName = catalog.attributeValue("name");
//                    String catalogTitle = catalog.attributeValue("title");
//                    String className = catalog.attributeValue("item-clazz");
//                    String simpleClassName = className.substring(className.lastIndexOf(".") + 1, className.length());
//                    String entityName = StringUtils.uncapitalize(simpleClassName);
//
//                    if (null != code2entityName.put(catalogName, entityName))
//                        throw new RuntimeException("Duplicate catalog definition:" + catalogName);
//
//                    code2title.put(catalogName, catalogTitle);
//                } else if (catalog.getName().equals("catalog-override"))
//                {
//                    String className = catalog.attributeValue("item-clazz");
//                    if (className != null)
//                        throw new RuntimeException("WTF?? item-clazz on catalog-override!?");
//                }
//            }
//        }
//
//        // простейшая загрузка энтити
//
//        String pathY = "file:" + moduleDir.getPath() + "/**/src/**/*.entity.xml";
//        Resource[] fileListY = resolver.getResources(pathY);
//        for (Resource resource : fileListY)
//        {
//            if (resource.getFile().getAbsolutePath().startsWith(moduleDir + "\\kladr"))
//                continue;
//            Element root = Dom4jUtil.readStream(new FileInputStream(resource.getFile())).getRootElement();
//            for (Iterator i = root.elementIterator(); i.hasNext();)
//            {
//                Element entity = (Element) i.next();
//                if (entity.getName().equals("entity"))
//                {
//                    String name = entity.attributeValue("name");
//                    String title = entity.attributeValue("title");
//                    if (title == null)
//                        title = ""; // название не задано для энтити
//                    if (null != entity2title.put(name, title))
//                        throw new RuntimeException("duplicate entity definition: " + name + " in " + resource.getFilename());
//                } else if (entity.getName().equals("interface"))
//                {
//                    //ignore
//                } else
//                    throw new RuntimeException("wtf? unknown tag: " + entity.getName());
//            }
//        }
//
//        for (Object[] obj : initFileList)
//        {
//            Element inputRoot = (Element) obj[0];
//            String outputFileName = (String) obj[1];
//
//            // prepare output *.data.xml
//            Document output = new DOMDocument();
//            output.setXMLEncoding("UTF-8");
//            Namespace namespace = Namespace.get("http://www.tandemframework.org/meta/entity-data");
//
//            Element outputRoot = new DOMElement("data");
//            outputRoot.addAttribute("name", inputRoot.attributeValue("name"));
//            outputRoot.add(namespace);
//            outputRoot.add(Namespace.get("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
//            outputRoot.addAttribute("xsi:schemaLocation", "http://www.tandemframework.org/meta/entity-data http://www.tandemframework.org/schema/meta/entity-data.xsd");
//            output.add(outputRoot);
//
//            // analyze each subelement
//
//            for (Iterator iterator = inputRoot.elementIterator(); iterator.hasNext();)
//            {
//                Element catalog = (Element) iterator.next();
//                String catalogName = catalog.attributeValue("name");
//                String sync = catalog.attributeValue("synchronize");
//                if (sync == null)
//                    sync = "system";
//                String entityName;
//
//                if (catalog.getName().equals("catalog"))
//                {
////                    String catalogTitle = catalog.attributeValue("title");
//                    String className = catalog.attributeValue("item-clazz");
//                    String simpleClassName = className.substring(className.lastIndexOf(".") + 1, className.length());
//                    entityName = StringUtils.uncapitalize(simpleClassName);
//
//                    // check catalog name and simple class name
//                    if (!catalogName.equals(simpleClassName))
//                        System.out.println("catalogName = '" + catalogName + "', simpleClassName = '" + simpleClassName + "'");
//
//                    String entityTitle = entity2title.get(entityName);
//                    if (entityTitle == null)
//                        throw new RuntimeException("entity not found: " + entityName);
//
//                    // check catalog title and entity title
////                    if (!entityTitle.equals(catalogTitle))
////                        System.out.println("catalogTitle = '" + catalogTitle + "', entityTitle = '" + entityTitle + "'");
//
//                } else if (catalog.getName().equals("catalog-override"))
//                {
//                    entityName = code2entityName.get(catalogName);
//                    if (entityName == null)
//                        throw new RuntimeException("cant load entity by catalogName: " + catalogName);
//
//                    String entityTitle = entity2title.get(entityName);
//                    if (entityTitle == null)
//                        throw new RuntimeException("entity not found: " + entityName);
//                } else
//                    throw new RuntimeException("unknown tag: " + catalog.getName());
//
//
//                // create output subelement
//                Element outputCatalog = new DOMElement("entity", namespace);
//                outputCatalog.addAttribute("name", entityName);
//                if (sync.equals("system"))
//                    systemSyncCatalogs.add(catalogName);
//                outputCatalog.addAttribute("synchronize", sync);
//                outputRoot.add(outputCatalog);
//
//                String catalogCode = null;
//
//                // наши мега справочники...
//                if ("uniDSGradeValue".equals(entityName))
//                    catalogCode = "uniDSGradeValue";
//                else if ("uniDSMarkValue".equals(entityName))
//                    catalogCode = "uniDSMarkValue";
//                else if ("attestation".equals(entityName))
//                    catalogCode = "attestation";
//                else if ("practice".equals(entityName))
//                    catalogCode = "practice";
//                else if ("discipline".equals(entityName))
//                    catalogCode = "discipline";
//                else if ("eduLoadType".equals(entityName))
//                    catalogCode = "eduLoadType";
//                else if ("extraEduLoadType".equals(entityName))
//                    catalogCode = "extraEduLoadType";
//                else if ("disciplineIndex".equals(entityName))
//                    catalogCode = "disciplineIndex";
//
//                if (catalogCode != null)
//                {
//                    Element defaultProperty = new DOMElement("default-properties", namespace);
//                    outputCatalog.add(defaultProperty);
//                    Element defaultCatalogCode = new DOMElement("property", namespace);
//                    defaultCatalogCode.addAttribute("name", "catalogCode");
//                    defaultCatalogCode.addAttribute("value", catalogCode);
//                    defaultProperty.add(defaultCatalogCode);
//                }
//
//                // analyze each codes
//
//                for (Iterator codeIterator = catalog.elementIterator(); codeIterator.hasNext();)
//                {
//                    Element item = (Element) codeIterator.next();
//                    String itemName = item.attributeValue("name");
//                    String syncItem = item.attributeValue("synchronize");
//
//                    // create output code
//                    Element outputCode;
//
//                    if (item.getName().equals("item"))
//                    {
//                        outputCode = new DOMElement("item", namespace);
//                        outputCode.addAttribute("id", itemName);
//                        if (syncItem != null)
//                            outputCode.addAttribute("synchronize", syncItem);
//                        outputCatalog.add(outputCode);
//
//                        String itemTitle = item.attributeValue("title");
//
//                        // create output property
//                        Element outputPropertyTitle = new DOMElement("property", namespace);
//                        outputPropertyTitle.addAttribute("name", "title");
//                        outputPropertyTitle.addAttribute("value", itemTitle);
//                        outputPropertyTitle.addAttribute("synchronize", "user"); // название можно менять всегда
//                        outputCode.add(outputPropertyTitle);
//                    } else if (item.getName().equals("item-override"))
//                    {
//                        outputCode = new DOMElement("item-override", namespace);
//                        outputCode.addAttribute("id", itemName);
//                        if (syncItem != null)
//                            outputCode.addAttribute("synchronize", syncItem);
//                        outputCatalog.add(outputCode);
//
//                        String itemTitle = item.attributeValue("title");
//                        if (itemTitle != null)
//                        {
//                            // create output property
//                            Element outputPropertyTitle = new DOMElement("property", namespace);
//                            outputPropertyTitle.addAttribute("name", "title");
//                            outputPropertyTitle.addAttribute("value", itemTitle);
//                            outputPropertyTitle.addAttribute("synchronize", "user"); // название можно менять всегда
//                            outputCode.add(outputPropertyTitle);
//                        }
//                    } else
//                        throw new RuntimeException("unknown tag: " + item.getName());
//
//
//                    // analyze each properties
//
//                    for (Iterator propertyIterator = item.elementIterator(); propertyIterator.hasNext();)
//                    {
//                        Element itemProperty = (Element) propertyIterator.next();
//                        if (itemProperty.getName().equals("property") || itemProperty.getName().equals("property-override"))
//                        {
//                            String propertyName = itemProperty.attributeValue("name");
//                            String propertyValue = itemProperty.attributeValue("value");
//                            String propertySync = itemProperty.attributeValue("synchronize");
//
//                            // create output property
//                            Element outputProperty = new DOMElement("property", namespace);
//                            outputProperty.addAttribute("name", propertyName);
//                            outputProperty.addAttribute("value", propertyValue);
//                            if (propertySync != null)
//                                outputProperty.addAttribute("synchronize", propertySync);
//                            outputCode.add(outputProperty);
//
//                        } else if (itemProperty.getName().equals("itemref"))
//                        {
//                            String propertyName = itemProperty.attributeValue("name");
//                            String propertyRefName = itemProperty.attributeValue("ref-name");
//                            String propertyCatalogName = itemProperty.attributeValue("catalog-name");
//                            String propertyEntityName = code2entityName.get(propertyCatalogName);
//                            String propertySync = itemProperty.attributeValue("synchronize");
//                            if (propertyEntityName == null)
//                                throw new RuntimeException("Cant resolve entity-name by catalog-name: " + propertyCatalogName);
//
//                            // create output property
//                            Element outputProperty = new DOMElement("itemref", namespace);
//                            outputProperty.addAttribute("name", propertyName);
//                            outputProperty.addAttribute("id", propertyRefName);
//                            outputProperty.addAttribute("entity", propertyEntityName);
//                            if (propertySync != null)
//                                outputProperty.addAttribute("synchronize", propertySync);
//                            outputCode.add(outputProperty);
//                        } else
//                            throw new RuntimeException("unknown tag: " + itemProperty.getName());
//                    }
//                }
//            }
//
//            // write output file
//            FileOutputStream out = new FileOutputStream(outputFileName);
//            XMLWriter writer = new XMLWriter(out, format);
//            writer.write(output);
//            out.close();
//            writer.close();
//        }
//
//        // мигрируем *.groups.xml
//        String pathZ = "file:" + moduleDir.getPath() + "/**/src/**/*.groups.xml";
//        Resource[] fileListZ = resolver.getResources(pathZ);
//        for (Resource resource : fileListZ)
//        {
//            Element root = Dom4jUtil.readStream(new FileInputStream(resource.getFile())).getRootElement();
//
//            // prepare output *.data.xml
//            Document output = new DOMDocument();
//            output.setXMLEncoding("UTF-8");
//            Namespace namespace = Namespace.get("http://www.tandemframework.org/meta/catalog-group");
//
//            Element outputRoot = new DOMElement("groups");
//            outputRoot.addAttribute("name", root.attributeValue("name"));
//            outputRoot.add(namespace);
//            outputRoot.add(Namespace.get("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
//            outputRoot.addAttribute("xsi:schemaLocation", "http://www.tandemframework.org/meta/catalog-group http://www.tandemframework.org/schema/meta/meta-catalog-group.xsd");
//            output.add(outputRoot);
//
//            // analyze each subelement
//
//            for (Iterator iterator = root.elementIterator(); iterator.hasNext();)
//            {
//                Element group = (Element) iterator.next();
//                String name = group.attributeValue("name");
//                String title = group.attributeValue("title");
//
//                Element groupNew = new DOMElement("group", namespace);
//                groupNew.addAttribute("name", name);
//                groupNew.addAttribute("title", title);
//                outputRoot.add(groupNew);
//
//                // sub
//                for (Iterator catalogIter = group.elementIterator(); catalogIter.hasNext();)
//                {
//                    Element catalog = (Element) catalogIter.next();
//                    String catalogName = catalog.attributeValue("name");
//                    String hidden = catalog.attributeValue("hidden");
//                    String entityName = code2entityName.get(catalogName);
//                    String catalogTitle = code2title.get(catalogName);
//
//                    if (entityName == null)
//                        throw new RuntimeException("Unknown catalog-name: " + catalogName);
//
//                    if (catalogTitle == null)
//                        throw new RuntimeException("Unknown catalog-name (no title?): " + catalogName);
//
//                    Element catalogNew = new DOMElement("catalog", namespace);
//                    catalogNew.addAttribute("name", entityName);
//                    catalogNew.addAttribute("title", catalogTitle);
//                    if (hidden != null)
//                        catalogNew.addAttribute("hidden", hidden);
//
//                    groupNew.add(catalogNew);
//                }
//            }
//
//            File file = resource.getFile();
//            String name = file.getName().substring(0, file.getName().lastIndexOf(".groups.xml"));
//            String outputFileName = file.getParent() + "/" + name + ".catalog.group.xml";
//
//            // write output file
//            FileOutputStream out = new FileOutputStream(outputFileName);
//            XMLWriter writer = new XMLWriter(out, format);
//            writer.write(output);
//            out.close();
//            writer.close();
//        }
//    }
}
