/* $Id$ */
package ru.tandemservice.uni.ui;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * @since 10.10.2008
 */
public class EducationLevelsHighSchoolSelectModel extends BaseSelectModel implements ISingleSelectModel, IMultiSelectModel
{
    private final IEducationLevelModel _model;

    public EducationLevelsHighSchoolSelectModel(IEducationLevelModel model)
    {
        super(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY);
        _model = model;
    }

    public EducationLevelsHighSchoolSelectModel()
    {
        super(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY);
        _model = null;
    }

    public EducationLevelsHighSchoolSelectModel(String labelProperty)
    {
        super(labelProperty);
        _model = null;
    }

    protected DQLSelectBuilder createEducationLevelsHighSchoolDQL(String alias)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();

        dql.fromEntity(EducationLevelsHighSchool.class, alias);
        dql.column(property(alias));

        return dql;
    }

    protected DQLSelectBuilder createEducationOrgUnitBuilder(String alias)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EducationOrgUnit.class, alias);

        if (getModel() != null)
        {
            if (getModel().getFormativeOrgUnit() != null)
                dql.where(eq(property(alias, EducationOrgUnit.formativeOrgUnit()), value(getModel().getFormativeOrgUnit().getId())));

            if (getModel().getTerritorialOrgUnit() != null)
                dql.where(eq(property(alias, EducationOrgUnit.territorialOrgUnit()), value(getModel().getTerritorialOrgUnit().getId())));
        }

        UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(dql, alias);

        return dql;
    }

    private DQLSelectBuilder internalCreateBuilder(String filter, Object o)
    {
        final String alias = "hs";
        final DQLSelectBuilder dql = createEducationLevelsHighSchoolDQL(alias);

        if (o != null)
        {
            if (o instanceof Long)
                dql.where(eq(property(alias, "id"), value((Long) o)));
            else if (o instanceof Collection)
                dql.where(in(property(alias, "id"), (Collection) o));
        }

        dql.where(exists(
                createEducationOrgUnitBuilder("eou").column(value(1))
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool()), property(alias)))
                        .buildQuery()
        ));

        FilterUtils.applySimpleLikeFilter(dql, alias, getLabelProperties()[0], filter);

        dql.order(property(alias, getLabelProperties()[0]));

        return dql;
    }

    protected boolean isNeedRequest()
    {
        return true;
    }

    @Override
    public ListResult findValues(String filter)
    {
        return isNeedRequest() ?
                new DQLListResultBuilder(internalCreateBuilder(filter, null)).findOptions() :
                new SimpleListResultBuilder<>(Collections.<EducationLevelsHighSchool>emptyList()).findOptions();
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        return isNeedRequest() ? new DQLListResultBuilder(internalCreateBuilder(null, primaryKey)).getSingleValue() : null;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return isNeedRequest() ? new DQLListResultBuilder(internalCreateBuilder(null, primaryKeys)).getValues() : Collections.emptyList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getLabelFor(Object value, int columnIndex)
    {
        return (String) EducationLevelsHighSchool.FAST_BEAN.getPropertyValue(value, getLabelProperties()[columnIndex]);
    }

    @Override
    public Object getPrimaryKey(Object value)
    {
        return ((IEntity) value).getId();
    }

    public IEducationLevelModel getModel()
    {
        return _model;
    }
}