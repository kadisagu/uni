/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.orgUnitList;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitListUI;

/**
 * @author Alexander Shaburov
 * @since 02.11.12
 */
public class OrgUnitStudentSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public OrgUnitStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public IEducationOrgUnitContextHandler getContextHandler(ExecutionContext context)
    {
        return new OrgUnitStudentListContextHandler((Boolean) context.get(UniStudentOrgUnitListUI.PROP_ARCHIVAL),
                                                    (OrgUnit) context.get(UniStudentOrgUnitListUI.PROP_ORG_UNIT));
    }
}
