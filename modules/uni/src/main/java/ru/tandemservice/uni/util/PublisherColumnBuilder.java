/* $*/

package ru.tandemservice.uni.util;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;

/**
 * use org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder
 * @deprecated
 * @author oleyba
 * @since 3/22/11
 */
public class PublisherColumnBuilder
{
    private String caption;
    private Object titleKey;
    private Object entityKey;
    private String selectedTab;
    private String selectedDataTab;
    private String idProperty;

    private String selectedTabBind = "selectedTab";
    private String selectedSubTabBind = "selectedDataTab";

    public PublisherColumnBuilder(String caption, Object titleKey, String selectedTab)
    {
        this.caption = caption;
        this.titleKey = titleKey;
        this.selectedTab = selectedTab;
    }

    public PublisherColumnBuilder entity(Object entityKey)
    {
        this.entityKey = entityKey;
        return this;
    }

    public PublisherColumnBuilder subTab(String selectedDataTab)
    {
        this.selectedDataTab = selectedDataTab;
        return this;
    }

    public PublisherColumnBuilder id(String idProperty)
    {
        this.idProperty = idProperty;
        return this;
    }

    public PublisherColumnBuilder tabBind(String bind)
    {
        this.selectedTabBind = bind;
        return this;
    }

    public PublisherColumnBuilder subTabBind(String bind)
    {
        this.selectedSubTabBind = bind;
        return this;
    }

    public PublisherLinkColumn build()
    {
        PublisherLinkColumn column = entityKey == null ? new PublisherLinkColumn(caption, titleKey) : new PublisherLinkColumn(caption, titleKey, entityKey);
        column.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ParametersMap map = new ParametersMap();
                map.add(PublisherActivator.PUBLISHER_ID_KEY, idProperty == null ? entity.getId() : entity.getProperty(idProperty));
                map.add(selectedTabBind, selectedTab);
                map.add(selectedSubTabBind, selectedDataTab);
                return map;
            }
        });
        return column;
    }

}
