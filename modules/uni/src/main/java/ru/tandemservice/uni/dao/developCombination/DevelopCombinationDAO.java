package ru.tandemservice.uni.dao.developCombination;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.util.DevelopUtil;

/**
 * @author vdanilov
 */
public class DevelopCombinationDAO extends UniBaseDao implements IDevelopCombinationDAO {

    @Override
    public DevelopCombination findSuitableCombination(final IDevelopCombination developCombinationDefinition)
    {
        if (null == developCombinationDefinition) { return null; }
        return this.getByNaturalId(new DevelopCombination.NaturalId(
                developCombinationDefinition.getDevelopForm(),
                developCombinationDefinition.getDevelopCondition(),
                developCombinationDefinition.getDevelopTech(),
                developCombinationDefinition.getDevelopGrid()
        ));
    }

    @Override
    public List<DevelopCombination> findSuitableCombinations(final Collection<IDevelopCombination> combinationList)
    {
        if ((null == combinationList) || combinationList.isEmpty()) { return Collections.emptyList(); }

        final Map<DevelopCombination.NaturalId, DevelopCombination> dbMap = new HashMap<DevelopCombination.NaturalId, DevelopCombination>();
        for (final DevelopCombination c : this.getList(DevelopCombination.class)) {
            dbMap.put((DevelopCombination.NaturalId)c.getNaturalId(), c);
        }

        final List<DevelopCombination> result = new ArrayList<DevelopCombination>(dbMap.size());
        for (final IDevelopCombination c: combinationList) {
            final DevelopCombination value = dbMap.remove(new DevelopCombination.NaturalId(
                    c.getDevelopForm(),
                    c.getDevelopCondition(),
                    c.getDevelopTech(),
                    c.getDevelopGrid()
            ));
            if (null != value) {
                result.add(value);
            }
        }

        return result;
    }

    /**
     * @param developForm
     * @param developCondition
     * @param developTech
     * @param developGrid
     * @return new IDevelopCombination()
     */
    public static IDevelopCombination getDevelopCombinationDefinition(final DevelopForm developForm, final DevelopCondition developCondition, final DevelopTech developTech, final DevelopGrid developGrid) {
        return new IDevelopCombination() {
            @Override public String getTitle() { return DevelopUtil.getTitle(this); }
            @Override public DevelopForm getDevelopForm() { return developForm; }
            @Override public DevelopCondition getDevelopCondition() { return developCondition; }
            @Override public DevelopTech getDevelopTech() { return developTech; }
            @Override public DevelopGrid getDevelopGrid() { return developGrid; }

            @Override public boolean equals(final Object obj) {
                if (obj instanceof IDevelopCombination) {
                    final IDevelopCombination o = (IDevelopCombination)obj;
                    return (
                            this.getDevelopForm().equals(o.getDevelopForm()) &&
                            this.getDevelopCondition().equals(o.getDevelopCondition()) &&
                            this.getDevelopTech().equals(o.getDevelopTech()) &&
                            this.getDevelopGrid().equals(o.getDevelopGrid())
                    );
                }
                return false;
            }

            private int calculateHash() {
                int hash = 0;
                hash = hash*7 + this.getDevelopForm().hashCode();
                hash = hash*7 + this.getDevelopCondition().hashCode();
                hash = hash*7 + this.getDevelopTech().hashCode();
                hash = hash*7 + this.getDevelopGrid().hashCode();
                return hash;
            }

            final int hash = this.calculateHash();
            @Override public int hashCode() { return this.hash; }
        };
    }

}
