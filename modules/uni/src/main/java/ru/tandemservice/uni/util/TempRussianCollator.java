/* $Id$ */
package ru.tandemservice.uni.util;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.Person;

import java.text.Collator;
import java.util.Comparator;

/**
 * Пока не выпиливать. Может где-то в скриптах осталось.
 *
 * @author Ekaterina Zvereva
 * @since 02.09.2014
 * @deprecated use {@link CommonCollator}
 */
@Deprecated
public class TempRussianCollator extends CommonCollator
{
    /**
     * @deprecated use CommonCollator.RUSSIAN_COLLATOR
     */
    @Deprecated
    public static final Collator CORRECT_RUSSIAN_COLLATOR = RUSSIAN_COLLATOR;

    /**
     * @deprecated use Person.FULL_FIO_COMPARATOR
     */
    @Deprecated
    public static final Comparator<Person> FIO_COMPARATOR = Person.FULL_FIO_COMPARATOR;
}