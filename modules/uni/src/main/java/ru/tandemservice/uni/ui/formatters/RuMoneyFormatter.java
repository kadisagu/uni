package ru.tandemservice.uni.ui.formatters;

import org.tandemframework.core.view.formatter.IFormatter;

import java.math.BigDecimal;

/** форматирует денежные данные
 *  @deprecated Не используйте Double для хранения денежных сумм! 999.999 рублей вместо миллиона - это плохо.
 *  Для форматирования чисел в Long как денег используйте {@link MoneyFormatter}. У него есть много полезных методов.
 */
@Deprecated
public class RuMoneyFormatter implements IFormatter<Double>
{
    public static final RuMoneyFormatter INSTANCE = new RuMoneyFormatter();

    @Override
    public String format(Double source)
    {
        if (null == source) {
            return null;
        }

        final BigDecimal d = BigDecimal.valueOf(source);

        final long rub = d.longValue();
        final long cop = d.remainder(BigDecimal.ONE).setScale(2, BigDecimal.ROUND_HALF_UP).movePointRight(2).longValue();
        final boolean negative = rub == 0L && cop < 0L;

        return (negative ? "-" : "") + rub + " руб. " + (cop > -10 && cop < 10 ? "0" : "") + Math.abs(cop) + " коп.";
    }
}
