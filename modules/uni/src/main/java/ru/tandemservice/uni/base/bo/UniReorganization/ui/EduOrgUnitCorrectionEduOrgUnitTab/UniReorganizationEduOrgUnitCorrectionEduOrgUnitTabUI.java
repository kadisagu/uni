/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.ui.EduOrgUnitCorrectionEduOrgUnitTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationCodeEdit.OrgUnitReorganizationCodeEdit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniReorganization.ui.EditEduOrgUnitOwner.UniReorganizationEditEduOrgUnitOwner;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
public class UniReorganizationEduOrgUnitCorrectionEduOrgUnitTabUI extends UIPresenter {

    private UniEduProgramEducationOrgUnitAddon util;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put("util", util);
        dataSource.put("owner", getSettings().get("owner"));
        saveSettings();
    }

    @Override
    public void onComponentRefresh() {
        util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        util
                .configSettings("filterAddon")
                .configWhere(EducationOrgUnit.formativeOrgUnit(), getSettings().get("owner"), false)
                .configNeedEnableCheckBox(false)
                .configUseFilters(
                        UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION,
                        UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                        UniEduProgramEducationOrgUnitAddon.Filters.PRODUCING_ORG_UNIT,
                        UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD);
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(UniReorganizationEduOrgUnitCorrectionEduOrgUnitTab.EDU_ORG_UNIT_DS)).getOptionColumnSelectedObjects("checkboxColumn");
            selected.clear();
        }
    }

    public boolean isOwnerSelected()
    {
        return null != getSettings().get("owner");
    }

    public void onClickChangeEducationOrgUnitOwner()
    {
        final Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(UniReorganizationEduOrgUnitCorrectionEduOrgUnitTab.EDU_ORG_UNIT_DS)).getOptionColumnSelectedObjects("checkboxColumn");
        if (selected.size() == 0) throw new ApplicationException("Не выбран ни один элемент из списка.");
        getActivationBuilder().asRegionDialog(UniReorganizationEditEduOrgUnitOwner.class)
                .parameter("multiValue",
                        selected.stream().collect(Collectors.toList())
                ).activate();
    }

    public void onClickResetFilters()
    {
        if (util != null) util.clearFilters();

        CommonBaseSettingsUtil.clearSettingsExcept(getSettings(), "owner");

        if (util != null && util.getSettings() != null)
            util.saveSettings();

        saveSettings();
    }

    public void onChangeOwner()
    {
        onClickResetFilters();
        util.clearConfigWhere();
        util
                .configWhere(EducationOrgUnit.formativeOrgUnit(), getSettings().get("owner"), false);
    }

    public void applyFilters()
    {
        saveSettings();
        if (util != null && util.getSettings() != null)
            util.saveSettings();
    }

}