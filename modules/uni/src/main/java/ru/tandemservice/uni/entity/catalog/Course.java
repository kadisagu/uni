package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;

import java.util.Comparator;

public class Course extends CourseGen
{
    public static final Comparator<Course> COURSE_COMPARATOR = Comparator.comparingInt(CourseGen::getIntValue);

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new EntityComboDataSourceHandler(ownerId, Course.class)
            .titleProperty(Course.title().s())
            .filter(Course.title())
            .order(Course.intValue());
    }
}