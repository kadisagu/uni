package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;


/**
 * @author vdanilov
 */
public class UniEduProgramRules {

    // не меняйте модификаторы класса - отложенная инициализация (поля заполняются при первом обращении к классу)
    private static class Instantiator {
        private static final UniEduProgramRules INSTANCE = ApplicationRuntime.getBean(UniEduProgramRules.class.getName(), UniEduProgramRules.class);
    }

    public static UniEduProgramRules instance() { return Instantiator.INSTANCE; }

    public String getProgramSubjectTitleCodePrefix(final EducationLevels eduLvl)
    {
        final EduProgramSubject eduProgramSubject = eduLvl.getEduProgramSubject();
        if (eduProgramSubject.is2013()) {
            return eduProgramSubject.getSubjectCode();
        }
        final EduProgramSubjectIndex subjectIndex = eduProgramSubject.getSubjectIndex();
        final String code = subjectIndex.getCode();
        switch (code) {

            // элементы классификаторов 2013 идут без кода уровня (квалификации), и добавлять не надо
            case EduProgramSubjectIndexCodes.TITLE_2013_01:
            case EduProgramSubjectIndexCodes.TITLE_2013_02:
            case EduProgramSubjectIndexCodes.TITLE_2013_03:
            case EduProgramSubjectIndexCodes.TITLE_2013_04:
            case EduProgramSubjectIndexCodes.TITLE_2013_05:
            case EduProgramSubjectIndexCodes.TITLE_2013_06:
            case EduProgramSubjectIndexCodes.TITLE_2013_07:
            case EduProgramSubjectIndexCodes.TITLE_2013_08:
            case EduProgramSubjectIndexCodes.TITLE_2013_10:
            {
                return eduProgramSubject.getSubjectCode();
            }

            // элементы классификаторов 2005 идут уже с кодом уровня(квалификации) => добавлять не надо
            case EduProgramSubjectIndexCodes.TITLE_2005_50:
            case EduProgramSubjectIndexCodes.TITLE_2005_62:
            case EduProgramSubjectIndexCodes.TITLE_2005_65:
            case EduProgramSubjectIndexCodes.TITLE_2005_68:
            {
                return eduProgramSubject.getSubjectCode();
            }

            // для 2009 НПО код квалификации не добавляется (вместо него в классификаторе уже указан код профессии)
            case EduProgramSubjectIndexCodes.TITLE_2009_40:
            {
                return eduProgramSubject.getSubjectCode();
            }

            // для 2009 СПО добавляется код квалификации (51 или 52, если указан)
            case EduProgramSubjectIndexCodes.TITLE_2009_50:
            {
                final Qualifications qualification = eduLvl.getQualification();
                if (null == qualification) { return eduProgramSubject.getSubjectCode(); }
                return eduProgramSubject.getSubjectCode() + "." + qualification.getCode();
            }

            // для элементов 2009 в классификаторах нет кода уровня (квалификации)
            // для элементов 2009 ВПО добавляем код руками (благо, мы знаем какой добавлять)
            case EduProgramSubjectIndexCodes.TITLE_2009_62: return eduProgramSubject.getSubjectCode()+".62";
            case EduProgramSubjectIndexCodes.TITLE_2009_65: return eduProgramSubject.getSubjectCode()+".65";
            case EduProgramSubjectIndexCodes.TITLE_2009_68: return eduProgramSubject.getSubjectCode()+".68";
        }

        // для всего остального (НПО старые, ДПО, прочая муть) - код направления + код квалификации (если указано)
        final Qualifications qualification = eduLvl.getQualification();
        if (null == qualification) { return eduProgramSubject.getSubjectCode(); }
        return eduProgramSubject.getSubjectCode() + "." + qualification.getCode();
    }


}
