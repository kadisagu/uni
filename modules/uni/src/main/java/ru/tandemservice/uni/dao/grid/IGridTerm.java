package ru.tandemservice.uni.dao.grid;


/**
 * @author vdanilov
 */
public interface IGridTerm {
    int getTermNumber();
    int getCourseNumber();
    int getPartNumber();
    int getPartAmount();
}
