/* $Id$ */
package ru.tandemservice.uni.ui.formatters;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IRawFormatter;
import ru.tandemservice.uni.entity.education.StudentCustomState;

import java.util.Arrays;
import java.util.Collection;

/**
 * TODO таблицу заменить на div'ы. Объединить с EnrEntrantCustomStateCollectionFormatter через интерфейс. Сделать инстанс.
 * @author nvankov
 * @since 3/28/13
 */
public class StudentCustomStateCollectionFormatter implements IRawFormatter
{
    @Override
    public String format(Object source)
    {
        if (source == null)
        {
            return "";
        }

        if (source instanceof Object[])
        {
            source = Arrays.asList((Object[]) source);
        }

        if (source instanceof Collection)
        {
            if(!((Collection) source).isEmpty())
            {
                StringBuilder tableBuilder = new StringBuilder("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");

                for (Object o : (Collection) source)
                {
                    StudentCustomState item = (StudentCustomState) o;
                    StringBuilder studentCustomStateStrBuilder = new StringBuilder(item.getCustomState().getShortTitle());
                    if (null != item.getBeginDate() || null != item.getEndDate())
                    {
                        studentCustomStateStrBuilder.append(" (");
                        if (null != item.getBeginDate())
                        {
                            studentCustomStateStrBuilder.append("c ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getBeginDate()));
                            if (null != item.getEndDate())
                                studentCustomStateStrBuilder.append(" ");
                        }
                        if (null != item.getEndDate())
                            studentCustomStateStrBuilder.append("по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getEndDate()));
                        studentCustomStateStrBuilder.append(")");
                    }

                    tableBuilder.append("<tr><td style=\"white-space: nowrap;\"><div style=\"color:").append(item.getCustomState().getHtmlColor()).append("\">").append(studentCustomStateStrBuilder).append("</div></td></tr>");
                }
                tableBuilder.append("</table>");
                return tableBuilder.toString();
            }
        }

        return "";
    }
}

