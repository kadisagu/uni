package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentTab;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDao;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null == model.getOrgUnit())
        {
            model.setOrgUnit((OrgUnit)get(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }

}
