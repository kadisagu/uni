/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.builder.expression.SimpleTextExpression;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;
import org.tandemframework.tapsupport.component.selection.ListResult;

import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
public class OrderExecutorSelectModel extends BaseSingleSelectModel implements IMultiSelectModel
{
    public OrderExecutorSelectModel()
    {
        super();
    }

    @Override
    public ListResult findValues(String filter)
    {
        filter = CoreStringUtils.escapeLike(filter);
        filter = filter.replace(" ", "%");

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "post");
        builder.addJoinFetch("post", EmployeePost.L_ORG_UNIT, "orgUnit");
        builder.addJoinFetch("post", EmployeePost.L_EMPLOYEE, "employee");
        builder.addJoinFetch("employee", Employee.L_PERSON, "person");
        builder.addJoinFetch("person", Person.L_IDENTITY_CARD, "idcard");
        builder.add(MQExpression.eq("employee", Employee.P_ARCHIVAL, Boolean.FALSE));

        builder.add(new SimpleTextExpression("upper(concat(idcard." + IdentityCard.P_LAST_NAME +
            ",concat(idcard." + IdentityCard.P_FIRST_NAME +
            //",concat(idcard." + IdentityCardGen.P_MIDDLE_NAME +
            ",concat(orgUnit." + OrgUnit.P_TITLE +
            ")))) like '" + filter + "'"));
        builder.addOrder("idcard", IdentityCard.P_LAST_NAME);
        builder.addOrder("idcard", IdentityCard.P_FIRST_NAME);
        builder.addOrder("idcard", IdentityCard.P_MIDDLE_NAME);

        return new MQListResultBuilder(builder).findOptions();
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        return DataAccessServices.dao().get(EmployeePost.class, (Long) primaryKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List getValues(Set primaryKeys)
    {
        return DataAccessServices.dao().getList(EmployeePost.class, primaryKeys);
    }

    @Override
    public int getColumnCount()
    {
        return 1;
    }

    @Override
    public String[] getColumnTitles()
    {
        return null;
    }

    @Override
    public String getLabelFor(Object value, int columnIndex)
    {
        return ((EmployeePost) value).getTitleWithOrgUnit();
    }

    @Override
    public ISelectValueStyle getValueStyle(Object value)
    {
        return null;
    }

    public static String getExecutor(EmployeePost employeePost)
    {
        if (employeePost == null) return null;

        IdentityCard identityCard = employeePost.getEmployee().getPerson().getIdentityCard();
        String lastName = identityCard.getLastName();
        String firstName = identityCard.getFirstName();
        String middleName = identityCard.getMiddleName();

        StringBuilder str = new StringBuilder();
        if (StringUtils.isNotEmpty(firstName))
            str.append(firstName.charAt(0)).append(".");
        if (StringUtils.isNotEmpty(middleName))
            str.append(middleName.charAt(0)).append(".");
        str.append(" ").append(lastName);

        return str.toString() + (employeePost.getPhone() == null ? "" : " т. " + employeePost.getPhone());
    }
}
