/* $Id:$ */
package ru.tandemservice.uni.util.groovy;

import com.google.common.base.Preconditions;
import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.Script;
import org.codehaus.groovy.runtime.InvokerHelper;

import java.io.IOException;
import java.util.Map;
import java.util.WeakHashMap;

public class UniScriptLoader
{
    private static final Object MUTEX = new Object();
    private static UniScriptLoader state;

    public static void reset()
    {
        UniScriptLoader state;

        synchronized (MUTEX) {
            state = UniScriptLoader.state;
            UniScriptLoader.state = null;
        }

        if (null != state) {
            state.close();
        }
    }

    public static UniScriptLoader current()
    {
        UniScriptLoader state = UniScriptLoader.state;
        if (null != state) { return UniScriptLoader.state; }

        synchronized (MUTEX) {
            state = UniScriptLoader.state;
            if (null == state) {
                UniScriptLoader.state = state = new UniScriptLoader();
            }
        }

        return state;
    }


    private final GroovyClassLoader loader = new GroovyClassLoader(Thread.currentThread().getContextClassLoader());
    private final Map<Class, Script> scriptInstanceMap = new WeakHashMap<>();
    private UniScriptLoader() {}

    /**
     * @return not-null
     */
    public synchronized IScriptInstance getScriptInstance(GroovyCodeSource source)  {
        final Class klass = loader.parseClass(source); // кэшируется на уровне лоадера

        final Script cachedScript = scriptInstanceMap.get(klass);
        IScriptInstance instance;
        if (null == cachedScript) {
            final Script script = InvokerHelper.createScript(klass, new Binding());
            instance = (IScriptInstance) script.run();

            Preconditions.checkNotNull(instance, "script instance is null, source="+source.getName());

            scriptInstanceMap.put(klass, script);
        }
        else
            instance = (IScriptInstance) cachedScript.run();

        return instance;
    }

    private void close() {
        scriptInstanceMap.clear();

        try { loader.close(); }
        catch (IOException t) {
            //
        }
    }
}