/**
 *$Id:$
 */
package ru.tandemservice.uni.base.bo.UniEduProgram;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IUniEduProgramDao;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IUniEduProgramSyncDao;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.UniEduProgramDao;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.UniEduProgramSyncDao;

/**
 * @author Alexander Shaburov
 * @since 25.10.12
 */
@Configuration
public class UniEduProgramManager extends BusinessObjectManager
{
    public static final String UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME = "UniEduProgramEducationOrgUnitAddon";

    public static UniEduProgramManager instance()
    {
        return instance(UniEduProgramManager.class);
    }

    @Bean
    public IUniEduProgramDao dao() {
        return new UniEduProgramDao();
    }

    @Bean
    public IUniEduProgramSyncDao syncDao() {
        return new UniEduProgramSyncDao();
    }
}

