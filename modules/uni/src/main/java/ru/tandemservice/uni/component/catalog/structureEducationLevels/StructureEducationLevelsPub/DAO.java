/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.structureEducationLevels.StructureEducationLevelsPub;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.ViewWrapper;

import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author vip_delete
 */
public class DAO extends DefaultCatalogPubDAO<StructureEducationLevels, Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepareListItemDataSource(final Model model)
    {
        final List<StructureEducationLevels> list = this.getList(StructureEducationLevels.class);
        Collections.sort(list, StructureEducationLevels.COMPARATOR);
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        for (final ViewWrapper<StructureEducationLevels> wrapper : ViewWrapper.<StructureEducationLevels>getPatchedList(model.getDataSource()))
        {
            wrapper.setViewProperty("upDisabled", null == this.getNear(model, wrapper.getEntity(), -1));
            wrapper.setViewProperty("downDisabled", null == this.getNear(model, wrapper.getEntity(), 1));
        }
    }

    @Override
    public void updatePriorityUp(final Model model, final Long id)
    {
        this.changePriority(model, (StructureEducationLevels)this.get(id), -1);
    }

    @Override
    public void updatePriorityDown(final Model model, final Long id)
    {
        this.changePriority(model, (StructureEducationLevels)this.get(id), 1);
    }

    private void changePriority(final Model model, final StructureEducationLevels element, final int direction)
    {
        model.getCache().clear();

        final int priority = element.getPriority();
        final int newPriority = priority + direction;

        final Session session = this.getSession();
        final StructureEducationLevels near = this.getNear(model, element, direction);

        if (near != null)
        {
            element.setPriority(-1);
            session.update(element);
            session.flush();

            near.setPriority(priority);
            session.update(near);
            session.flush();

            element.setPriority(newPriority);
            session.update(element);
        }

        model.getCache().clear();
    }

    @SuppressWarnings("unchecked")
    private StructureEducationLevels getNear(final Model model, final StructureEducationLevels element, final int direction)
    {
        final StructureEducationLevels parent = element.getParent();

        List<StructureEducationLevels> orderedList = model.getCache().get(parent);

        if (null == orderedList)
        {
            final Criteria criteria = this.getSession().createCriteria(StructureEducationLevels.class);

            if (null == parent) {
                criteria.add(Restrictions.isNull(StructureEducationLevels.parent().s()));
            } else {
                criteria.add(Restrictions.eq(StructureEducationLevels.parent().s(), parent));
            }
            criteria.addOrder(Order.asc(StructureEducationLevels.priority().s()));
            model.getCache().put(parent, orderedList = criteria.list());
        }

        final int pos = orderedList.indexOf(element) + direction;
        if (pos >= 0 && pos < orderedList.size()) {
            return orderedList.get(pos);
        }

        return null;
    }
}
