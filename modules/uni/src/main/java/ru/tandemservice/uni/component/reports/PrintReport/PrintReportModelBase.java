/* $Id: Model.java 13749 2010-07-16 15:00:57Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.reports.PrintReport;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

/**
 * @author vip_delete
 * @since 21.01.2009
 */
@Input( { @Bind(key = "extension", binding = "extension"), @Bind(key = "zip", binding = "zip") })
public class PrintReportModelBase {
    private Boolean _zip;
    private String _extension;

    public Boolean isZip() {
        return _zip;
    }

    public void setZip(Boolean zip) {
        _zip = zip;
    }

    public String getExtension() {
        return _extension;
    }

    public void setExtension(String extension) {
        _extension = extension;
    }
}
