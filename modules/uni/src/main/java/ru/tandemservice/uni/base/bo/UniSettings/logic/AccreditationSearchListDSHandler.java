/* $Id$ */
package ru.tandemservice.uni.base.bo.UniSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniSettings.ui.AccreditationList.UniSettingsAccreditationList;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.uniedu.entity.EduAccreditation;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.LicenseInOrgUnit;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.11.2016
 */
public class AccreditationSearchListDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String TITLE_FIELD = "displayableTitle";
    public static final String LICENSE = "licenseField";
    public static final String ACCREDIT_FIELD = "activeAccreditation";

    public static final String PROPERTY_ACCR_STATE = "accreditation";
    public static final String DISABLED_PROPERTY = "disabled";


    public AccreditationSearchListDSHandler(String ownerId)
    {
        super(ownerId, EduProgramSubject.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EduProgramSubject.class, "e")
                .where(isNull(property("e", EduProgramSubject.outOfSyncDate())))
                .column("e");
        //селекторы
        EduProgramSubjectIndex programSubjectIndex = context.get(UniSettingsAccreditationList.PROGRAM_SUBJECT_INDEX);
        EduInstitutionOrgUnit orgUnit = context.get(UniSettingsAccreditationList.INSTITUTION_ORG_UNIT);

        //Фильтры
        IdentifiableWrapper programType = context.get("typeID");
        IdentifiableWrapper accreditationExists = context.get("accreditation");
        String filteredTitle = context.get("subjectTitle");

        builder.where(eq(property("e", EduProgramSubject.subjectIndex()), value(programSubjectIndex)));

       List<EduProgramSubject> items = builder.createStatement(context.getSession()).list();

        //список аккредитаций
        Map<IPersistentAccreditationOwner, EduAccreditation> accreditationMap = new HashMap<>();
        List<EduAccreditation> accreditationList = new DQLSelectBuilder().fromEntity(EduAccreditation.class, "a")
                .where(eq(property("a", EduAccreditation.institutionOrgUnit()), value(orgUnit)))
                .where(eq(property("a", EduAccreditation.programSubjectIndex()), value(programSubjectIndex)))
                .createStatement(context.getSession())
                .list();
        accreditationList.stream().forEach(e -> {
            if (!accreditationMap.containsKey(e.getOwner()))
                accreditationMap.put(e.getOwner(), e);
        });

        //список лицензий по подразделению
        List<Long> licenseList = new DQLSelectBuilder().fromEntity(LicenseInOrgUnit.class, "lic")
                .column(property("lic", LicenseInOrgUnit.programSubject().id()))
                .where(eq(property("lic", LicenseInOrgUnit.institutionOrgUnit()), value(orgUnit)))
                .where(in(property("lic", LicenseInOrgUnit.programSubject()), items))
                .createStatement(context.getSession()).list();

        List<DataWrapper> resultList = new ArrayList<>();
        Map<IPersistentAccreditationOwner, DataWrapper> parentMap = new HashMap<>();

        for (EduProgramSubject owner : items)
        {
            DataWrapper wrapper = new DataWrapper(owner);
            wrapper.setProperty(TITLE_FIELD, owner.getTitleWithCode());
            wrapper.setProperty(LICENSE, licenseList.contains(owner.getId()));
            wrapper.setProperty(DISABLED_PROPERTY, true);

            IPersistentAccreditationOwner parent = owner.getSubjectGroup();

            if (parentMap.get(parent) == null)
            {
                DataWrapper parentWrapper = new DataWrapper(parent);
                EduAccreditation accreditationParent = accreditationMap.get(parent);
                parentWrapper.setProperty(TITLE_FIELD, parent.getDisplayableTitle().toUpperCase());
                parentWrapper.setProperty(PROPERTY_ACCR_STATE, accreditationParent != null? accreditationParent.getStateAccreditation() : null);
                parentWrapper.setProperty(DISABLED_PROPERTY, false);
                parentWrapper.setProperty(ACCREDIT_FIELD, null);
                parentWrapper.setHierarchyParent(null);
                parentMap.put(parent, parentWrapper);
                if (programType== null)
                    resultList.add(parentWrapper);

            }
            wrapper.setProperty(ACCREDIT_FIELD, licenseList.contains(owner.getId()) &&
                                (StateAccreditationEnum.ACCREDITATION_ACTIVE.equals(parentMap.get(parent).getProperty(PROPERTY_ACCR_STATE)) ||
                                        StateAccreditationEnum.ACCREDITATION_SUSPENDED.equals(parentMap.get(parent).getProperty(PROPERTY_ACCR_STATE))));

            if (programType== null)
                wrapper.setHierarchyParent(parentMap.get(parent));

            resultList.add(wrapper);
        }

        if (programType!= null && programType.getId().equals(UniSettingsAccreditationList.PROGRAM_SUBJECT_GROUPS_ID)) {
            resultList.clear();
            resultList.addAll(parentMap.values());
        }

        if (!StringUtils.isEmpty(filteredTitle))
            resultList = resultList.stream().filter(e->(((String)e.getProperty(TITLE_FIELD)).toLowerCase().contains(filteredTitle.toLowerCase()))).collect(Collectors.toList());

        if (accreditationExists != null)
        {
            boolean value = accreditationExists.getId().equals(UniSettingsAccreditationList.ACCREDITATION_YES_ID);
            resultList = resultList.stream().filter(e->((value? (e.getProperty(PROPERTY_ACCR_STATE) != null && (e.getProperty(PROPERTY_ACCR_STATE).equals(StateAccreditationEnum.ACCREDITATION_ACTIVE) || e.getProperty(PROPERTY_ACCR_STATE).equals(StateAccreditationEnum.ACCREDITATION_SUSPENDED))) :
                    (e.getProperty(PROPERTY_ACCR_STATE) == null || e.getProperty(PROPERTY_ACCR_STATE).equals(StateAccreditationEnum.ACCREDITATION_REVOKED))) ||
                    (e.getProperty(ACCREDIT_FIELD) != null && e.getProperty(ACCREDIT_FIELD).equals(value)))).collect(Collectors.toList());
        }

        return ListOutputBuilder.get(input, resultList).pageable(false).build();

    }

}