/* $Id: Model.java 5483 2008-11-28 12:06:00Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.developGrid.DevelopGridPub;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;

/**
 * @author AutoGenerator
 * Created on 16.06.2008
 */
public class Model extends DefaultCatalogPubModel<DevelopGrid>
{
    private ISelectModel _developPeriodListModel;
    private ISelectModel _termListModel;

    public ISelectModel getDevelopPeriodListModel() {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel) {
        _developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getTermListModel() {
        return _termListModel;
    }

    public void setTermListModel(ISelectModel termListModel) {
        _termListModel = termListModel;
    }
}
