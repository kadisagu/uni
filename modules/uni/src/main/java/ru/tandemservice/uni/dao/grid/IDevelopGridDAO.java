package ru.tandemservice.uni.dao.grid;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vdanilov
 */
public interface IDevelopGridDAO {
    SpringBeanCache<IDevelopGridDAO> instance = new SpringBeanCache<>(IDevelopGridDAO.class.getName());

    /**
     * возвращает для сеток распределение семестров сестки по семестрам.
     * Семестры отсортированы по возрастанию.
     *
     * @param gridIds список идентификаторов сеток (nullable)
     * @return { grid.id -> { term.number -> DevelopGridTerm (term, course, part) } }
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, Map<Integer, DevelopGridTerm>> getDevelopGridDistributionMap(Collection<Long> gridIds);

    /**
     * Для сетки возвращает распределение семестров сетки по семестрам.
     * Семестры отсортированы по возрастанию.
     *
     * @param gridId идентификатор сетки
     * @return { term.number -> DevelopGridTerm (term, course, part) }
     */
    Map<Integer, DevelopGridTerm> getDevelopGridMap(Long gridId);

    /**
     * Список семестров сетки для сетки. Сортировка по номеру семестра.
     * @param grid сетка
     * @return список семестров сетки
     */
    List<DevelopGridTerm> getDevelopGridTermList(DevelopGrid grid);

    /**
     * сохраняет (обновляет) состояние курсов по семестрам (для указанных сеток)
     * @param developGridDistributionMap @see getEducationGridDistributionMap
     */
    @Transactional(propagation=Propagation.REQUIRED)
    void updateDevelopGrids(Map<Long, Map<Integer, DevelopGridTerm>> developGridDistributionMap);


    /**
     * Детализация учебной сетки
     *
     * @param developGrid учебная сетка
     * @return курс -> семестры в этом курсе
     *         ключи-курсы отсортированы по номеру курса
     *         в ключах присутствуют только используемые курсы
     *         все ключи и значения в мапе всегда не null
     *         1 -> [1, 2]       : 1 курс, в нем 2 части, первая часть - это 1-ый семестр, вторая часть - 2-ой семестр
     *         2 -> [3, null, 4] : 2 курс, в нем 3 части, вторая часть не используется
     *         3 -> [null, 5]    : 3 курс, две части, первая часть не используется, вторая соответствует 5-ому семестру
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Course, Integer[]> getDevelopGridDetail(DevelopGrid developGrid);


    /**
     * Список курсов сетки
     * @param developGrid сетка
     * @return набор курсов
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Set<Course> getDevelopGridCourseSet(DevelopGrid developGrid);

    /**
     * создает стандартные сетки (по полю meta, если оно заполнено)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    void doGenerateGridsFromDescription();

    /**
     * @return количество семестров в сетке (номер максимального семестра)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    int getDevelopGridSize(DevelopGrid developGrid);
}
