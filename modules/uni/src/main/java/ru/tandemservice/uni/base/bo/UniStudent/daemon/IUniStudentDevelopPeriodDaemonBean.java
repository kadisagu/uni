package ru.tandemservice.uni.base.bo.UniStudent.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Alexey Lopatin
 * @since 17.02.2016
 */
public interface IUniStudentDevelopPeriodDaemonBean
{
    SpringBeanCache<IUniStudentDevelopPeriodDaemonBean> instance = new SpringBeanCache<>(IUniStudentDevelopPeriodDaemonBean.class.getName());

    /**
     * Обновляет поле «Срок освоения (автообновляемое)» неархивного студента первым из найденных значений
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateDevelopPeriodAuto();
}
