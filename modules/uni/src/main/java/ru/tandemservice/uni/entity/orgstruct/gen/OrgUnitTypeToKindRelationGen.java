package ru.tandemservice.uni.entity.orgstruct.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь между типом и видом орг. юнита
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitTypeToKindRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation";
    public static final String ENTITY_NAME = "orgUnitTypeToKindRelation";
    public static final int VERSION_HASH = -1121786924;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT_TYPE = "orgUnitType";
    public static final String L_ORG_UNIT_KIND = "orgUnitKind";

    private OrgUnitType _orgUnitType;     // Тип орг. юнита
    private OrgUnitKind _orgUnitKind;     // Вид подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип орг. юнита. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    /**
     * @param orgUnitType Тип орг. юнита. Свойство не может быть null.
     */
    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        dirty(_orgUnitType, orgUnitType);
        _orgUnitType = orgUnitType;
    }

    /**
     * @return Вид подразделения. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitKind getOrgUnitKind()
    {
        return _orgUnitKind;
    }

    /**
     * @param orgUnitKind Вид подразделения. Свойство не может быть null.
     */
    public void setOrgUnitKind(OrgUnitKind orgUnitKind)
    {
        dirty(_orgUnitKind, orgUnitKind);
        _orgUnitKind = orgUnitKind;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitTypeToKindRelationGen)
        {
            setOrgUnitType(((OrgUnitTypeToKindRelation)another).getOrgUnitType());
            setOrgUnitKind(((OrgUnitTypeToKindRelation)another).getOrgUnitKind());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitTypeToKindRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitTypeToKindRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitTypeToKindRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnitType":
                    return obj.getOrgUnitType();
                case "orgUnitKind":
                    return obj.getOrgUnitKind();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnitType":
                    obj.setOrgUnitType((OrgUnitType) value);
                    return;
                case "orgUnitKind":
                    obj.setOrgUnitKind((OrgUnitKind) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnitType":
                        return true;
                case "orgUnitKind":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnitType":
                    return true;
                case "orgUnitKind":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnitType":
                    return OrgUnitType.class;
                case "orgUnitKind":
                    return OrgUnitKind.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitTypeToKindRelation> _dslPath = new Path<OrgUnitTypeToKindRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitTypeToKindRelation");
    }
            

    /**
     * @return Тип орг. юнита. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation#getOrgUnitType()
     */
    public static OrgUnitType.Path<OrgUnitType> orgUnitType()
    {
        return _dslPath.orgUnitType();
    }

    /**
     * @return Вид подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation#getOrgUnitKind()
     */
    public static OrgUnitKind.Path<OrgUnitKind> orgUnitKind()
    {
        return _dslPath.orgUnitKind();
    }

    public static class Path<E extends OrgUnitTypeToKindRelation> extends EntityPath<E>
    {
        private OrgUnitType.Path<OrgUnitType> _orgUnitType;
        private OrgUnitKind.Path<OrgUnitKind> _orgUnitKind;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип орг. юнита. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation#getOrgUnitType()
     */
        public OrgUnitType.Path<OrgUnitType> orgUnitType()
        {
            if(_orgUnitType == null )
                _orgUnitType = new OrgUnitType.Path<OrgUnitType>(L_ORG_UNIT_TYPE, this);
            return _orgUnitType;
        }

    /**
     * @return Вид подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.orgstruct.OrgUnitTypeToKindRelation#getOrgUnitKind()
     */
        public OrgUnitKind.Path<OrgUnitKind> orgUnitKind()
        {
            if(_orgUnitKind == null )
                _orgUnitKind = new OrgUnitKind.Path<OrgUnitKind>(L_ORG_UNIT_KIND, this);
            return _orgUnitKind;
        }

        public Class getEntityClass()
        {
            return OrgUnitTypeToKindRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitTypeToKindRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
