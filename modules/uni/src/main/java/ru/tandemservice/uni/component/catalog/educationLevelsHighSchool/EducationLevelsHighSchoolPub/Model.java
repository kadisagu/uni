/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolPub;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author vip_delete
 */
public class Model extends DefaultCatalogPubModel<EducationLevelsHighSchool>
{

    private List<HSelectOption> _levelTypeList;
    private List<StructureEducationLevels> levelTopList;
    private ISelectModel _orgUnitListModel;
    private List<Qualifications> _qualificationList;

    private Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap;

    public List<HSelectOption> getLevelTypeList()
    {
        return _levelTypeList;
    }

    public void setLevelTypeList(List<HSelectOption> levelTypeList)
    {
        _levelTypeList = levelTypeList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<StructureEducationLevels> getLevelTopList()
    {
        return levelTopList;
    }

    public void setLevelTopList(List<StructureEducationLevels> levelTopList)
    {
        this.levelTopList = levelTopList;
    }

    public Map<StructureEducationLevels, Set<StructureEducationLevels>> getStructureMap()
    {
        return structureMap;
    }

    public void setStructureMap(Map<StructureEducationLevels, Set<StructureEducationLevels>> structureMap)
    {
        this.structureMap = structureMap;
    }
}
