package ru.tandemservice.uni.ws.debts;

import java.util.Collection;
import java.util.Map;

import ru.tandemservice.uni.ws.debts.StudentDebtsService.StudentDebtInfo;

/**
 * @author vdanilov
 */
public interface IStudentDebtResolver {

    Map<Long, Collection<StudentDebtInfo>> getDebtMap(Collection<Long> ids);

}
