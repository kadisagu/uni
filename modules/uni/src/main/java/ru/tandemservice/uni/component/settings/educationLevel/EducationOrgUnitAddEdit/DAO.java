/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.component.settings.educationLevel.EducationOrgUnitAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.util.EducationOrgUnitSetting;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.List;

/**
 * @author vip_delete
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (model.getEducationOrgUnitId() != null)
        {
            //редактирование
            model.setEditForm(true);
            model.setEducationOrgUnit(getNotNull(EducationOrgUnit.class, model.getEducationOrgUnitId()));
        } else
        {
            //добавление
            model.setEditForm(false);
            model.setEducationOrgUnit(new EducationOrgUnit());
            model.getEducationOrgUnit().setEducationLevelHighSchool(getNotNull(EducationLevelsHighSchool.class, model.getEducationLevelHighSchoolId()));
            model.getEducationOrgUnit().setTerritorialOrgUnit(TopOrgUnit.getInstance());

            Number number = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                    .column(DQLFunctions.max(DQLExpressions.property(EducationOrgUnit.code().fromAlias("e"))))
                    .createStatement(getSession()).uniqueResult();
            model.getEducationOrgUnit().setCode(number == null ? 1 : number.intValue() + 1);
            model.getEducationOrgUnit().setShortTitle(model.getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());
        }

        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopTechList(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopConditionList(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopPeriodList(EducationCatalogsManager.getDevelopPeriodSelectModel());

        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));

        EducationOrgUnitSetting ouSetting = (EducationOrgUnitSetting) getSession().createCriteria(EducationOrgUnitSetting.class).uniqueResult();
        model.setShortTitleDisabled(ouSetting != null && !ouSetting.isUseShortTitle());
        model.setInternalCodeDisabled(ouSetting != null && !ouSetting.isUseInternalCode());
    }

    @Override
    public void update(Model model)
    {
        if (model.getEducationOrgUnit().getId() == null)
        {
            if (getEducationOrgUnit(model) != null)
                throw new ApplicationException("Направление подготовки (специальность) подразделения с такими параметрами уже существует.");
            save(model.getEducationOrgUnit());
        } else
        {
            EducationOrgUnit educationOrgUnit = getEducationOrgUnit(model);
            if (educationOrgUnit != null && !educationOrgUnit.equals(model.getEducationOrgUnit()))
                throw new ApplicationException("Направление подготовки (специальность) подразделения с такими параметрами уже существует.");
            update(model.getEducationOrgUnit());
        }
    }

    @SuppressWarnings("unchecked")
    private EducationOrgUnit getEducationOrgUnit(Model model)
    {
        Criteria criteria = getSession().createCriteria(EducationOrgUnit.class);
        criteria.add(Restrictions.eq(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getEducationOrgUnit().getFormativeOrgUnit()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getEducationOrgUnit().getTerritorialOrgUnit()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationOrgUnit().getEducationLevelHighSchool()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_FORM, model.getEducationOrgUnit().getDevelopForm()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_CONDITION, model.getEducationOrgUnit().getDevelopCondition()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_TECH, model.getEducationOrgUnit().getDevelopTech()));
        criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_PERIOD, model.getEducationOrgUnit().getDevelopPeriod()));
        List<EducationOrgUnit> result = criteria.list();
        if (result.size() > 1)
            throw new ApplicationException("Направление подготовки (специальность) подразделения с такими параметрами уже существует.");
        return (result.size() != 0) ? result.get(0) : null;
    }
}
