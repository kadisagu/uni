/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;

/**
 * Параметры выпуска студентов по направлению подготовки (НПв)
 *
 * Направление подготовки ОУ, НПв.
 * Показывает, на каких кафедрах происходит выпуск по указанному НПм.
 * Важно, что НПв полностью соответствует НПм: его нельзя использовать для конкретизации.
 * Например, запрещено создавать профили одного и того же НПм с помощью создания нескольких НПв.
 */
public class EducationLevelsHighSchool extends EducationLevelsHighSchoolGen
{

    @EntityDSLSupport()
    @Override
    public boolean isAllowStudentsDisabled()
    {
        return !getEducationLevel().getLevelType().isAllowStudents();
    }

    @EntityDSLSupport()
    @Override
    public String getDisplayableShortTitle()
    {
        StringBuilder result = new StringBuilder(getEducationLevel().getTitleCodePrefix());
        if (getShortTitle() != null) {
            result.append(" ").append(getShortTitle());
        }
        return result.toString();
    }

    @EntityDSLSupport()
    @Override
    public String getTitleWithProfile()
    {
        EducationLevels level = getEducationLevel();
        return getEducationLevel().isProfileOrSpecialization() ? level.getParentLevel().getTitle() + " / " + getTitle() : getTitle();
    }

    @EntityDSLSupport()
    @Override
    public Integer getOrgUnitQuantity()
    {
        return UniDaoFacade.getCoreDao().getCount(EducationOrgUnit.class, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, this);
    }

    public static final String[] ORGUNIT_KEY = {EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_FULL_TITLE};
    public static final String[] EDUCATION_LEVELS_KEY = {EducationLevelsHighSchool.L_EDUCATION_LEVEL, EducationLevels.P_FULL_TITLE};
    public static final String[] QUALIFICATION_TITLE_KEY = {EducationLevelsHighSchool.L_EDUCATION_LEVEL, EducationLevels.P_QUALIFICATION_TITLE};

    /**
     * @return true, если элемент является специальностью или специализацией
     */
    public boolean isSpeciality()
    {
        return getEducationLevel().isSpecialityPrintTitleMode();
    }

    public boolean isSpecialization()
    {
        return getEducationLevel().isSpecialization();
    }

    // TODO: XXX: переделать
    @Override
    @Zlo("если нужно кастомное поле, то надо делать в dao метод, который возвращает его значение")
    @EntityDSLSupport(parts = {P_FULL_TITLE})
    public String getConfigurableTitle()
    {
        return (String) getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY);
    }


    /**
     * @deprecated use {@link EducationLevels#getProgramSubjectTitleWithCode()}
     */
    @Deprecated
    public String getEducationStandartRootTitleWithOkso() {
        return getEducationLevel().isProfileOrSpecialization()
                ? getEducationLevel().getParentLevel().getDisplayableTitle()
                : getEducationLevel().getDisplayableTitle();
    }

    /**
     * Направление/специальность ГОС.
     * Метод оставлен для совместимости с возможными клиентскими скриптами. Удалить после перехода на ОП.
     * @deprecated use {@link EducationLevels#getEduProgramSubject()#getTitle()}
     */
    @Deprecated
    public String getEducationStandartRootTitle() {
        if (getEducationLevel().isProfileOrSpecialization())
            return getEducationLevel().getParentLevel().getTitle();
        return getEducationLevel().getTitle();
    }

    /**
     * Направление/специальность c кодом направления
     * Метод оставлен для совместимости с возможными клиентскими скриптами. Удалить после перехода на ОП.
     * @deprecated use {@link EducationLevels#getProgramSubjectTitleWithCode()}
     */
    @Deprecated
    public String getEducationStandartRootTitleWithCode() {
        return getEducationLevel().isProfileOrSpecialization()
                ? getEducationLevel().getParentLevel().getDisplayableTitle()
                : getEducationLevel().getDisplayableTitle();
    }

    /**
     * Профиль/специализация
     * Метод оставлен для совместимости с возможными клиентскими скриптами. Удалить после перехода на ОП.
     * @deprecated use {@link EducationLevels#getProgramSpecializationTitleIfChild()}
     */
    @Deprecated
    public String getEducationStandartBlockTitle() {
        return getEducationLevel().isProfileOrSpecialization() ? getTitle() : "";
    }

    @EntityDSLSupport
    public String getQualificationTitleNullSafe()
    {
        return null == getAssignedQualification() ? "" : getAssignedQualification().getTitle();
    }

    /**
     * @return Специальное звание. null означает, что не присваивается.
     */
    public String getSpecialDegreeTitle() {
        EduProgramSubject subject = getEducationLevel().getEduProgramSubject();
        EduProgramSubject2009 subj2009 = subject instanceof EduProgramSubject2009 ? (EduProgramSubject2009) subject : null;
        if (subj2009 != null && subj2009.isAssignSpecialDegree())
        {
            return subj2009.getSpecialDegreeTitle();
        }
        return null;
    }
}