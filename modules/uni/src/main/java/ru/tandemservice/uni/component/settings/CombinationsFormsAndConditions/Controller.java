package ru.tandemservice.uni.component.settings.CombinationsFormsAndConditions;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * 
 * @author nkokorina
 * @since 01.03.2010
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<IEntity> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", DevelopCombination.title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", DevelopCombination.developForm().title()));
        dataSource.addColumn(new SimpleColumn("Условие освоения", DevelopCombination.developCondition().title()));
        dataSource.addColumn(new SimpleColumn("Технология освоения", DevelopCombination.developTech().title()));
        dataSource.addColumn(new SimpleColumn("Учебная сетка", DevelopCombination.developGrid().title()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDevelopCombination", "Удалить {0}?", DevelopCombination.P_TITLE));

        model.setDataSource(dataSource);
    }

    public void onClickAddDevelopCombination(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniComponents.DEVELOP_COMBINATION_ADD_EDIT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, null)));
    }

    public void onClickEditDevelopCombination(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniComponents.DEVELOP_COMBINATION_ADD_EDIT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    public void onClickDeleteDevelopCombination(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
