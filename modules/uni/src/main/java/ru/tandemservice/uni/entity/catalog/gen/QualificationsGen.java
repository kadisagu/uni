package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Квалификация
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class QualificationsGen extends EntityBase
 implements INaturalIdentifiable<QualificationsGen>, org.tandemframework.common.catalog.entity.ICatalogItem, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.Qualifications";
    public static final String ENTITY_NAME = "qualifications";
    public static final int VERSION_HASH = 2125464409;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ORDER = "order";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private int _order;     // Порядок вывода
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Порядок вывода. Свойство не может быть null.
     */
    @NotNull
    public int getOrder()
    {
        initLazyForGet("order");
        return _order;
    }

    /**
     * @param order Порядок вывода. Свойство не может быть null.
     */
    public void setOrder(int order)
    {
        initLazyForSet("order");
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof QualificationsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((Qualifications)another).getCode());
            }
            setShortTitle(((Qualifications)another).getShortTitle());
            setOrder(((Qualifications)another).getOrder());
            setTitle(((Qualifications)another).getTitle());
        }
    }

    public INaturalId<QualificationsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<QualificationsGen>
    {
        private static final String PROXY_NAME = "QualificationsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof QualificationsGen.NaturalId) ) return false;

            QualificationsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends QualificationsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Qualifications.class;
        }

        public T newInstance()
        {
            return (T) new Qualifications();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "order":
                    return obj.getOrder();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "order":
                    obj.setOrder((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "order":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "order":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "order":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Qualifications> _dslPath = new Path<Qualifications>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Qualifications");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Порядок вывода. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getOrder()
     */
    public static PropertyPath<Integer> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends Qualifications> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Integer> _order;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(QualificationsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(QualificationsGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Порядок вывода. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getOrder()
     */
        public PropertyPath<Integer> order()
        {
            if(_order == null )
                _order = new PropertyPath<Integer>(QualificationsGen.P_ORDER, this);
            return _order;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.Qualifications#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(QualificationsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return Qualifications.class;
        }

        public String getEntityName()
        {
            return "qualifications";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
