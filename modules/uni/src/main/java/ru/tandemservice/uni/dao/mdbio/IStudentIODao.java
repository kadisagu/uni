package ru.tandemservice.uni.dao.mdbio;

import java.io.IOException;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import com.healthmarketscience.jackcess.Database;

/**
 * @author vdanilov
 */
public interface IStudentIODao {

    SpringBeanCache<IStudentIODao> instance = new SpringBeanCache<>(IStudentIODao.class.getName());

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_EducationLevelsHighSchoolList(Database mdb) throws IOException;

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_EducationOrgUnitList(Database mdb) throws IOException;

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_GroupList(Database mdb, boolean includeArchiveStudents) throws IOException;

    /**
     * Экспорт списка студентов в полном варианте, с экспортом персон, нпп, групп
     * @param mdb файл для экспорта
     * @param includeArchiveStudents включать ли в выборку архивных студентов
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_StudentList(Database mdb, boolean includeArchiveStudents) throws Exception;

    /**
     * Экспорт списка студентов в базовом варианте - только таблица студентов,
     * и в ней только id, персона и нпв
     * @param mdb файл для экспорта
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_StudentListBase(Database mdb) throws IOException;

    /** @return { mdb.group_id -> group.id } */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<String, Long> doImport_GroupList(Database mdb) throws Exception;

    /** @return { mdb.student_id -> student.id } */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<String, Long> doImport_StudentList(Database mdb) throws Exception;


}
