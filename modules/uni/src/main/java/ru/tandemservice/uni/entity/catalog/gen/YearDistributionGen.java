package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.YearDistribution;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Разбиение учебного года
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class YearDistributionGen extends EntityBase
 implements INaturalIdentifiable<YearDistributionGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.YearDistribution";
    public static final String ENTITY_NAME = "yearDistribution";
    public static final int VERSION_HASH = 1386403566;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_AMOUNT = "amount";
    public static final String P_IN_USE = "inUse";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _amount;     // Число частей
    private boolean _inUse;     // Используется
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Число частей. Свойство не может быть null.
     */
    @NotNull
    public int getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Число частей. Свойство не может быть null.
     */
    public void setAmount(int amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isInUse()
    {
        return _inUse;
    }

    /**
     * @param inUse Используется. Свойство не может быть null.
     */
    public void setInUse(boolean inUse)
    {
        dirty(_inUse, inUse);
        _inUse = inUse;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof YearDistributionGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((YearDistribution)another).getCode());
            }
            setAmount(((YearDistribution)another).getAmount());
            setInUse(((YearDistribution)another).isInUse());
            setTitle(((YearDistribution)another).getTitle());
        }
    }

    public INaturalId<YearDistributionGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<YearDistributionGen>
    {
        private static final String PROXY_NAME = "YearDistributionNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof YearDistributionGen.NaturalId) ) return false;

            YearDistributionGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends YearDistributionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) YearDistribution.class;
        }

        public T newInstance()
        {
            return (T) new YearDistribution();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "amount":
                    return obj.getAmount();
                case "inUse":
                    return obj.isInUse();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "amount":
                    obj.setAmount((Integer) value);
                    return;
                case "inUse":
                    obj.setInUse((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "amount":
                        return true;
                case "inUse":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "amount":
                    return true;
                case "inUse":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "amount":
                    return Integer.class;
                case "inUse":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<YearDistribution> _dslPath = new Path<YearDistribution>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "YearDistribution");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Число частей. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#getAmount()
     */
    public static PropertyPath<Integer> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#isInUse()
     */
    public static PropertyPath<Boolean> inUse()
    {
        return _dslPath.inUse();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends YearDistribution> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _amount;
        private PropertyPath<Boolean> _inUse;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(YearDistributionGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Число частей. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#getAmount()
     */
        public PropertyPath<Integer> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Integer>(YearDistributionGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#isInUse()
     */
        public PropertyPath<Boolean> inUse()
        {
            if(_inUse == null )
                _inUse = new PropertyPath<Boolean>(YearDistributionGen.P_IN_USE, this);
            return _inUse;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uni.entity.catalog.YearDistribution#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(YearDistributionGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return YearDistribution.class;
        }

        public String getEntityName()
        {
            return "yearDistribution";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
