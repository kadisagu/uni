/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniReorganization.logic;

import org.tandemframework.shared.commonbase.base.entity.ReorganizationHistoryItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

import java.util.Collection;
import java.util.Date;

/**
 * @author rsizonenko
 * @since 06.04.2016
 */
public class UniReorganizationDao extends UniBaseDao implements IUniReorganizationDao {

    @Override
    public void doChangeEducationOrgUnitOwner(Collection<EducationOrgUnit> input, OrgUnit owner) {

        for (EducationOrgUnit eduOu : input) {
            if (eduOu.getFormativeOrgUnit().getId().equals(owner.getId())) continue;


            ReorganizationHistoryItem historyItem = new ReorganizationHistoryItem();
            historyItem.setObject(eduOu);
            historyItem.setOldElement(eduOu.getFormativeOrgUnit());
            historyItem.setNewElement(owner);
            historyItem.setFieldName("formativeOrgUnit");
            historyItem.setModifyDate(new Date());

            eduOu.setFormativeOrgUnit(owner);


            update(eduOu);
            save(historyItem);

        }
    }

}
