package ru.tandemservice.uni.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelHigh;
import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos2;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки (специальность) ВПО (ГОС2)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelHighGos2Gen extends EducationLevelHigh
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uni.entity.catalog.EducationLevelHighGos2";
    public static final String ENTITY_NAME = "educationLevelHighGos2";
    public static final int VERSION_HASH = -1027366027;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE_LIST = "codeList";
    public static final String P_SPECIALIZATION_NUMBER = "specializationNumber";

    private String _codeList;     // Код перечня
    private String _specializationNumber;     // Номер специализации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код перечня.
     */
    @Length(max=255)
    public String getCodeList()
    {
        return _codeList;
    }

    /**
     * @param codeList Код перечня.
     */
    public void setCodeList(String codeList)
    {
        dirty(_codeList, codeList);
        _codeList = codeList;
    }

    /**
     * @return Номер специализации.
     */
    @Length(max=255)
    public String getSpecializationNumber()
    {
        return _specializationNumber;
    }

    /**
     * @param specializationNumber Номер специализации.
     */
    public void setSpecializationNumber(String specializationNumber)
    {
        dirty(_specializationNumber, specializationNumber);
        _specializationNumber = specializationNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EducationLevelHighGos2Gen)
        {
            setCodeList(((EducationLevelHighGos2)another).getCodeList());
            setSpecializationNumber(((EducationLevelHighGos2)another).getSpecializationNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelHighGos2Gen> extends EducationLevelHigh.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevelHighGos2.class;
        }

        public T newInstance()
        {
            return (T) new EducationLevelHighGos2();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "codeList":
                    return obj.getCodeList();
                case "specializationNumber":
                    return obj.getSpecializationNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "codeList":
                    obj.setCodeList((String) value);
                    return;
                case "specializationNumber":
                    obj.setSpecializationNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "codeList":
                        return true;
                case "specializationNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "codeList":
                    return true;
                case "specializationNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "codeList":
                    return String.class;
                case "specializationNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationLevelHighGos2> _dslPath = new Path<EducationLevelHighGos2>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevelHighGos2");
    }
            

    /**
     * @return Код перечня.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelHighGos2#getCodeList()
     */
    public static PropertyPath<String> codeList()
    {
        return _dslPath.codeList();
    }

    /**
     * @return Номер специализации.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelHighGos2#getSpecializationNumber()
     */
    public static PropertyPath<String> specializationNumber()
    {
        return _dslPath.specializationNumber();
    }

    public static class Path<E extends EducationLevelHighGos2> extends EducationLevelHigh.Path<E>
    {
        private PropertyPath<String> _codeList;
        private PropertyPath<String> _specializationNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код перечня.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelHighGos2#getCodeList()
     */
        public PropertyPath<String> codeList()
        {
            if(_codeList == null )
                _codeList = new PropertyPath<String>(EducationLevelHighGos2Gen.P_CODE_LIST, this);
            return _codeList;
        }

    /**
     * @return Номер специализации.
     * @see ru.tandemservice.uni.entity.catalog.EducationLevelHighGos2#getSpecializationNumber()
     */
        public PropertyPath<String> specializationNumber()
        {
            if(_specializationNumber == null )
                _specializationNumber = new PropertyPath<String>(EducationLevelHighGos2Gen.P_SPECIALIZATION_NUMBER, this);
            return _specializationNumber;
        }

        public Class getEntityClass()
        {
            return EducationLevelHighGos2.class;
        }

        public String getEntityName()
        {
            return "educationLevelHighGos2";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
