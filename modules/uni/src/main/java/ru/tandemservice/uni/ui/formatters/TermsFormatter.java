/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.ui.formatters;

import org.tandemframework.core.view.formatter.IFormatter;

import java.util.List;

/**
 * @deprecated специфичный - нужно откомментировать и перенести в epp после удаления модулей
 */
@Deprecated
public class TermsFormatter implements IFormatter<List<Integer>>
{
    @Override
    public String format(List<Integer> source)
    {
        if(source == null || source.isEmpty()) { return ""; }

        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < source.size())
        {
            int first = source.get(i);
            int last = first;
            i++;
            while (i < source.size() && source.get(i) - last <= 1)
            {
                last = source.get(i);
                i++;
            }

            if (result.length() > 0) { result.append(", "); }
            result.append(first);
            if (first < last) { result.append("-").append(last);}
        }
        return result.toString();
    }
}
