/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.documents.d3.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.OrderData;

import java.util.Arrays;
import java.util.Date;

/**
 * @author vip_delete
 * @since 09.12.2009
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        setEduEnrData(model);

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getCalculatedFIODeclination(model.getStudent().getPerson().getIdentityCard(), InflectorVariantCodes.RU_DATIVE).toUpperCase());
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());
        model.setDepartmentList(Arrays.asList(
                new IdentifiableWrapper(0L, "дневном"),
                new IdentifiableWrapper(1L, "вечернем")
        ));
        model.setDepartment(model.getDepartmentList().get(0));
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");
        model.setDevelopPeriodTitle(model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle());

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        String managerPostTitle;
        if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.DEPARTMENT))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";
        managerPostTitle = managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
        model.setManagerPostTitle(managerPostTitle);

        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

        if (manager != null)
            model.setManagerFio(manager.getPerson().getIdentityCard().getIof());
    }

    private void setEduEnrData(Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null) return;
        if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate())))
        {
            model.setOrderDate(data.getEduEnrollmentOrderDate());
            model.setOrderNumber(data.getEduEnrollmentOrderNumber());
        }
        else
        {
            model.setOrderDate(data.getRestorationOrderDate());
            model.setOrderNumber(data.getRestorationOrderNumber());
        }
    }
}
