/* $Id$ */
package ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 24.10.2013
 */
public class BaseStudentListContextHandler implements IEducationOrgUnitContextHandler
{
    private Boolean _archival;

    public BaseStudentListContextHandler(Boolean archival)
    {
        _archival = archival;
    }

    @Override
    public void setStudentListContext(DQLSelectBuilder builder, String studentAlias, String educationOrgUnitAlias)
    {
        if (_archival != null)
            builder.where(eq(property(studentAlias, Student.P_ARCHIVAL), value(_archival)));
    }
}