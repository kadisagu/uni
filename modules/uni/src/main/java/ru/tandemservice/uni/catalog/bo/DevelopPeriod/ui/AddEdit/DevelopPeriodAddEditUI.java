/* $Id$ */
package ru.tandemservice.uni.catalog.bo.DevelopPeriod.ui.AddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.meta.view.IViewButton;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import ru.tandemservice.uni.catalog.bo.DevelopPeriod.DevelopPeriodManager;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uniedu.catalog.bo.EduProgramDuration.ui.AddEdit.EduProgramDurationAddEdit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 10.09.2015
 */
public class DevelopPeriodAddEditUI extends BaseCatalogItemAddEditUI<DevelopPeriod>
{
    private Integer _numberOfYears;
    private Integer _numberOfMonths;
    private Integer _numberOfHours;
    private boolean _system;
    private IdentifiableWrapper _selectedOption;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        EduProgramDuration eduProgramDuration = getCatalogItem().getEduProgramDuration();
        if (isAddForm() || (!isAddForm() && eduProgramDuration == null)) {
            setNumberOfYears(0);
            setNumberOfMonths(0);
            setNumberOfHours(0);
            getCatalogItem().setLastCourse(1);
            _selectedOption = DevelopPeriodAddEdit.USE_YEAR_AND_MONTH_OPTION;
        }
        else {
            _selectedOption = eduProgramDuration.getNumberOfHours() > 0 ?
                    DevelopPeriodAddEdit.USE_HOURS_OPTION : DevelopPeriodAddEdit.USE_YEAR_AND_MONTH_OPTION;
            setNumberOfHours(eduProgramDuration.getNumberOfHours());
            setNumberOfYears(eduProgramDuration.getNumberOfYears());
            setNumberOfMonths(eduProgramDuration.getNumberOfMonths());
        }

        ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(getCatalogCode()).getItemPolicy(getCatalogItem().getCode());
        setSystem(itemPolicy.getSynchronize() == SynchronizeMeta.system);
    }



    private static IViewButton REFRESH_TITLE_BUTTON = new IViewButton() {
        @Override public String getListener() { return "onChangeDuration"; }
        @Override public String getAlert() { return null; }
        @Override public String getDisableSecondSubmit() { return null; }
        @Override public String getValidate() { return "false"; }
        @Override public String getRenderAsLink() { return null; }
        @Override public String getClick() { return null; }
        @Override public String getLabel() { return "Обновить название"; }
        @Override public String getId() { return null; }
        @Override public String getName() { return null; }
        @Override public String getBefore() { return null; }
        @Override public String getAfter() { return null; }
        @Override public String getPermissionKey() { return null; }
        @Override public String getVisible() { return null; }
        @Override public String getDisabled() { return null; }
        @Override public String getParameters() { return null; }
        @Override public boolean isReplace() { return false; }
    };

    @Override
    public List<IViewButton> getAdditionalButtons()
    {
        return Collections.singletonList(REFRESH_TITLE_BUTTON);
    }

    @Override
    public String getAdditionalPropertiesPageName()
    {
        return this.getClass().getPackage().getName() + ".AdditionalProps";
    }

    @Override
    public void onClickApply()
    {
        EduProgramDuration duration = DevelopPeriodManager.instance().dao().getEduProgramDuration(getNumberOfYears(), getNumberOfMonths(), getNumberOfHours());
        getCatalogItem().setEduProgramDuration(duration);
        super.onClickApply();
    }

    public void onChangeOption()
    {
        if (_selectedOption.equals(EduProgramDurationAddEdit.USE_HOURS_OPTION)) {
            setNumberOfMonths(0);
            setNumberOfYears(0);
        } else {
            setNumberOfHours(0);
        }
        onChangeDuration();
    }

    public void onChangeDuration()
    {
        if (getNumberOfYears() != null && getNumberOfMonths() != null) {
            if (getNumberOfYears() + getNumberOfMonths() == 0)
                getCatalogItem().setLastCourse(1);
            else
                getCatalogItem().setLastCourse(getNumberOfYears() + (getNumberOfMonths() == 0 ? 0 : 1));

            EduProgramDuration foundEduProgramDuration =
                    DevelopPeriodManager.instance().dao().getEduProgramDurationOrNull(_numberOfYears, _numberOfMonths, _numberOfHours);
            if (foundEduProgramDuration != null) {
                getCatalogItem().setTitle(foundEduProgramDuration.getTitle());
            } else {
                getCatalogItem().setTitle(CommonBaseDateUtil.getPeriodWithNames(getNumberOfYears(), getNumberOfMonths(), null, getNumberOfHours(), null));
            }
        }
    }

    public boolean isInYearsAndMonths()
    {
        return _selectedOption.equals(DevelopPeriodAddEdit.USE_YEAR_AND_MONTH_OPTION);
    }

    public Integer getNumberOfYears() { return _numberOfYears; }
    public void setNumberOfYears(Integer numberOfYears) { _numberOfYears = numberOfYears; }

    public Integer getNumberOfMonths() { return _numberOfMonths; }
    public void setNumberOfMonths(Integer numberOfMonths) { _numberOfMonths = numberOfMonths; }

    public Integer getNumberOfHours() { return _numberOfHours; }
    public void setNumberOfHours(Integer numberOfHours) { this._numberOfHours = numberOfHours; }

    public boolean isSystem() { return _system; }
    public void setSystem(boolean system) { _system = system; }

    public IdentifiableWrapper getSelectedOption() {return _selectedOption; }
    public void setSelectedOption(IdentifiableWrapper selectedOption) { this._selectedOption = selectedOption; }

}
