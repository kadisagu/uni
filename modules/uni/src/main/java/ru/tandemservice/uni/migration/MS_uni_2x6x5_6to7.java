/* $Id$ */
package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author azhebko
 * @since 20.08.2014
 */
public class MS_uni_2x6x5_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Исправляем коды справочника "Продолжительность обучения" на новый формат     {Число лет}.{Число месяцев}
        Statement statement = tool.getConnection().createStatement();
        statement.execute("select numberofyears_p, numberofmonths_p from edu_c_rp_duration_t");
        ResultSet resultSet = statement.getResultSet();

        PreparedStatement preparedStatement = tool.getConnection().prepareStatement("update edu_c_rp_duration_t set code_p = ? where numberofyears_p = ? and numberofmonths_p = ?");
        while (resultSet.next())
        {
            preparedStatement.setString(1, String.valueOf(resultSet.getInt(1)) + "." + String.valueOf(resultSet.getInt(2)));
            preparedStatement.setInt(2, resultSet.getInt(1));
            preparedStatement.setInt(3, resultSet.getInt(2));

            preparedStatement.execute();
        }
    }
}