/* $Id:$ */
package ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectFragmentBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.daemon.UniStudentDevelopPeriodDaemonBean;
import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 01.11.12
 */
public abstract class AbstractUniStudentListUI extends UIPresenter
{
    public static final String PROP_STUDENT_STATUS_OPTION = "status";
    public static final String PROP_STUDENT_STATUS_LIST = "studentStatusList";
    public static final String PROP_COURSE_LIST = "courseList";
    public static final String PROP_GROUP_LIST = "groupList";
    public static final String PROP_COMPENSATION_TYPE = "compensationType";
    public static final String PROP_QUALIFICATION_lIST = "qualification";
    public static final String PROP_DEVELOP_PERIOD_AUTO_LIST = "developPeriodAuto";
    public static final String PROP_STUDENT_CATEGORY_LIST = "studentCategory";
    public static final String PROP_SPECIAL_PURPOSE_RECRUIT = "specialPurposeRecruit";
    public static final String PROP_ENDING_YEAR = "endingYear";
    public static final String PROP_LAST_NAME = "personLastName";
    public static final String PROP_FIRST_NAME = "personFirstName";
    public static final String PROP_MIDDLE_NAME = "personMiddleName";
    public static final String PROP_PERSONAL_NUMBER = "personalNumber";
    public static final String PROP_PERSONAL_FILE_NUMBER = "personalFileNumber";
    public static final String PROP_BOOK_NUMBER = "bookNumber";
    public static final String PROP_ENTRANCE_YEAR = "entranceYear";
    public static final String PROP_STUDENT_CUSTOM_STATE_CI = "studentCustomStateCI";
    public static final String PROP_CITIZENSHIP = "citizenship";
    public static final String PROP_OLD_LAST_NAME = "oldLastName";

    private boolean _showDevelopTechFilter;

    public static final String EDU_PROGRAMM_UTIL = "UniEduProgramEducationOrgUnitAddon";

    private String _selectedPage;
    private boolean _hideUnusedEducationOrgUnit;
    private IPrincipalContext _principalContext;
    private AddressCountryVO _addressCountryVO;

    //filter
    private ISelectModel _studentStatusOptionListModel;
    private ISelectModel _studentStatusListModel;
    private ISelectModel _courseListModel;
    private ISelectModel _compensationTypeListModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _studentCategoryListModel;
    private ISelectModel _specialPurposeRecruitListModel;
    private ISelectModel _endingYearListModel;

    public AddressCountryVO getAddressCountryVO()
    {
        return _addressCountryVO;
    }

    public void setAddressCountryVO(AddressCountryVO addressCountryVO)
    {
        _addressCountryVO = addressCountryVO;
    }

    @Override
    public void onComponentRefresh()
    {
        Long addressCountryVOId = _uiSettings.get(PROP_CITIZENSHIP);
        if (null != addressCountryVOId)
        {
            if (addressCountryVOId.equals(0L))
            {
                _addressCountryVO = new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION);
            }
            else
            {
                _addressCountryVO = new AddressCountryVO(DataAccessServices.dao().<AddressCountry>get(addressCountryVOId));
            }
        }

        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
        {
            util.configSettings(getSettingsKey());
            util.configUseFilters(
                    UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.PRODUCING_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD,
                    isShowDevelopTechFilter() ? UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH : null
            );

            if (isHideUnusedEducationOrgUnit())
            {
                final String studentAlias = "_student";
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, studentAlias);
                builder.column(property(studentAlias, Student.educationOrgUnit().id()));

                // Задаем контекст конкретного списка студентов (например, брать только неархивных)
                IEducationOrgUnitContextHandler contextHandler = getContextHandler();
                if (contextHandler != null)
                {
                    builder.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(studentAlias), "_edu");
                    contextHandler.setStudentListContext(builder, studentAlias, "_edu");
                }

                // Добавляем все остальные используемые НПП (те, у которых стоит признак "Используется" и НПВ которых осуществляет обучение студентов)
                DQLSelectFragmentBuilder fragmentBuilder = new DQLSelectFragmentBuilder();
                fragmentBuilder.column(property("_e", EducationOrgUnit.id()));
                fragmentBuilder.fromEntity(EducationOrgUnit.class, "_e");
                UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(fragmentBuilder, "_e");

                builder.union(fragmentBuilder.buildSelectRule(), false);

                // Конфигурируем утиль выбора НПП
                util.configWhere(EducationOrgUnit.id(), builder.buildQuery(), false);
            }
        }
    }

    protected IEducationOrgUnitContextHandler getContextHandler()
    {
        return null;
    }

    /**
     * @return ключ для сеттингов в которые будут биндиться объекты фильтров
     */
    public abstract String getSettingsKey();

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS))
        {
            UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);

            dataSource.put(EDU_PROGRAMM_UTIL, util);

            dataSource.put(PROP_STUDENT_STATUS_OPTION, getSettings().get(PROP_STUDENT_STATUS_OPTION));
            dataSource.put(PROP_LAST_NAME, getSettings().get(PROP_LAST_NAME));
            dataSource.put(PROP_MIDDLE_NAME, getSettings().get(PROP_MIDDLE_NAME));
            dataSource.put(PROP_FIRST_NAME, getSettings().get(PROP_FIRST_NAME));
            dataSource.put(PROP_PERSONAL_NUMBER, getSettings().get(PROP_PERSONAL_NUMBER));
            dataSource.put(PROP_PERSONAL_FILE_NUMBER, getSettings().get(PROP_PERSONAL_FILE_NUMBER));
            dataSource.put(PROP_BOOK_NUMBER, getSettings().get(PROP_BOOK_NUMBER));
            dataSource.put(PROP_ENDING_YEAR, getSettings().get(PROP_ENDING_YEAR));
            dataSource.put(PROP_ENTRANCE_YEAR, getSettings().get(PROP_ENTRANCE_YEAR));
            dataSource.put(PROP_STUDENT_STATUS_LIST, getSettings().get(PROP_STUDENT_STATUS_LIST));
            dataSource.put(PROP_COURSE_LIST, getSettings().get(PROP_COURSE_LIST));
            dataSource.put(PROP_GROUP_LIST, getSettings().get(PROP_GROUP_LIST));
            dataSource.put(PROP_COMPENSATION_TYPE, getSettings().get(PROP_COMPENSATION_TYPE));
            dataSource.put(PROP_QUALIFICATION_lIST, getSettings().get(PROP_QUALIFICATION_lIST));
            dataSource.put(PROP_DEVELOP_PERIOD_AUTO_LIST, getSettings().get(PROP_DEVELOP_PERIOD_AUTO_LIST));
            dataSource.put(PROP_STUDENT_CATEGORY_LIST, getSettings().get(PROP_STUDENT_CATEGORY_LIST));
            dataSource.put(PROP_SPECIAL_PURPOSE_RECRUIT, getSettings().get(PROP_SPECIAL_PURPOSE_RECRUIT));
            dataSource.put(PROP_STUDENT_CUSTOM_STATE_CI, getSettings().get(PROP_STUDENT_CUSTOM_STATE_CI));
            dataSource.put(PROP_CITIZENSHIP, _addressCountryVO);
            dataSource.put(PROP_OLD_LAST_NAME, getSettings().get(PROP_OLD_LAST_NAME));
        }
        if (dataSource.getName().equals(UniStudentManger.GROUP_DS))
        {
            dataSource.put(PROP_COURSE_LIST, getSettings().get(PROP_COURSE_LIST));
        }
    }

    public void onClickSearch()
    {
        _uiSettings.set(PROP_CITIZENSHIP, _addressCountryVO == null ? null : _addressCountryVO.getId());
        getSettings().save();

        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
            util.saveSettings();
    }

    public void onClickClear()
    {
        _addressCountryVO = null;
        getSettings().clear();
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
            util.clearFilters();

        onClickSearch();
    }

    public void onClickDevelopPeriodRefresh()
    {
        UniStudentDevelopPeriodDaemonBean.DAEMON.wakeUpAndWaitDaemon(60 * 2);
    }

    // Getters & Setters

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }

    public boolean isHideUnusedEducationOrgUnit()
    {
        return _hideUnusedEducationOrgUnit;
    }

    public void setHideUnusedEducationOrgUnit(boolean hideUnusedEducationOrgUnit)
    {
        _hideUnusedEducationOrgUnit = hideUnusedEducationOrgUnit;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public ISelectModel getStudentStatusOptionListModel()
    {
        return _studentStatusOptionListModel;
    }

    public void setStudentStatusOptionListModel(ISelectModel studentStatusOptionListModel)
    {
        _studentStatusOptionListModel = studentStatusOptionListModel;
    }

    public ISelectModel getStudentStatusListModel()
    {
        return _studentStatusListModel;
    }

    public void setStudentStatusListModel(ISelectModel studentStatusListModel)
    {
        _studentStatusListModel = studentStatusListModel;
    }

    public ISelectModel getCourseListModel()
    {
        return _courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel)
    {
        _courseListModel = courseListModel;
    }

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public ISelectModel getSpecialPurposeRecruitListModel()
    {
        return _specialPurposeRecruitListModel;
    }

    public void setSpecialPurposeRecruitListModel(ISelectModel specialPurposeRecruitListModel)
    {
        _specialPurposeRecruitListModel = specialPurposeRecruitListModel;
    }

    public ISelectModel getEndingYearListModel()
    {
        return _endingYearListModel;
    }

    public void setEndingYearListModel(ISelectModel endingYearListModel)
    {
        _endingYearListModel = endingYearListModel;
    }

    public boolean isShowDevelopTechFilter()
    {
        return _showDevelopTechFilter;
    }

    public void setShowDevelopTechFilter(boolean value)
    {
        _showDevelopTechFilter = value;
    }

    public String getDaemonDevelopPeriodStatus()
    {
        final Long date = UniStudentDevelopPeriodDaemonBean.DAEMON.getCompleteStatus();
        String status = date == null ? "обновляется" : DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        return "Планируемые сроки обновлены: " + "<b>" + status + "</b>";
    }

    public String getDaemonDevelopPeriodAlert()
    {
        return "Обновить планируемые сроки освоения?";
    }
}