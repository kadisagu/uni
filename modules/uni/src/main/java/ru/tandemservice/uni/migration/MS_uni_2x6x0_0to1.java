package ru.tandemservice.uni.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 23.05.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select p.id, pr.PRINCIPAL_ID, prinrel.PRINCIPAL_ID from PERSONROLE_T pr " +
                "inner join PERSON_T p on pr.PERSON_ID=p.id inner join PERSON2PRINCIPALRELATION_T prinrel on p.ID=prinrel.PERSON_ID " +
                "where prinrel.PRINCIPAL_ID <> pr.PRINCIPAL_ID order by p.id");

        ResultSet rs = stmt.getResultSet();
        PreparedStatement update = tool.prepareStatement("update personrole_t set principal_id=? where person_id=?");

        Map<Long, Long> personIdToPrincipalIdMap = new HashMap<>();
        while (rs.next())
        {
            update.setLong(1, rs.getLong(3));
            update.setLong(2, rs.getLong(1));
            update.executeUpdate();
        }

        update.close();
        stmt.close();
    }
}