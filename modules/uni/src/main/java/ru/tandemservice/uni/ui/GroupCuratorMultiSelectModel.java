package ru.tandemservice.uni.ui;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Stanislav Shibarshin
 * @since 07.11.2016
 */
public class GroupCuratorMultiSelectModel extends BaseMultiSelectModel
{

    private OrgUnit _orgUnit;

    public GroupCuratorMultiSelectModel()
    {
        this(null);
    }

    public GroupCuratorMultiSelectModel(OrgUnit orgUnit)
    {
        super("id", new String[] { Employee.P_FULL_FIO });
        this._orgUnit = orgUnit;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return UniDaoFacade.getCoreDao().getList(createBuilder(null, primaryKeys));
    }

    @Override
    public ListResult findValues(String filter)
    {
        DQLSelectBuilder builder = createBuilder(filter, null);
        return new ListResult<>(UniDaoFacade.getCoreDao().getList(builder, 0, 50), UniDaoFacade.getCoreDao().getCount(builder));
    }

    private DQLSelectBuilder createBuilder(String filter, Set primaryKeys)
    {
        DQLSelectBuilder employeeIdsBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                .where(exists(Group.class, Group.curator().s(), property("ep"), Group.archival().s(), value(false)))
                .column(property("ep", EmployeePost.employee().id()));

        if (_orgUnit != null)
        {
            Collection<String> kindSet = UniDaoFacade.getOrgstructDao().getAllowGroupsOrgUnitKindCodes(_orgUnit);
            IDQLExpression condition = DQLExpressions.exists(Group.class, Group.id().s(), value(0L)); // false

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)) {
                condition = DQLExpressions.or(condition, DQLExpressions.exists(Group.class, Group.curator().s(), property("ep"),
                        Group.educationOrgUnit().educationLevelHighSchool().orgUnit().s(), value(_orgUnit)));
            }
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)) {
                condition = DQLExpressions.or(condition, DQLExpressions.exists(Group.class, Group.curator().s(), property("ep"),
                        Group.educationOrgUnit().formativeOrgUnit().s(), value(_orgUnit)));
            }
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)) {
                condition = DQLExpressions.or(condition, DQLExpressions.exists(Group.class, Group.curator().s(), property("ep"),
                        Group.educationOrgUnit().territorialOrgUnit().s(), value(_orgUnit)));
            }
            employeeIdsBuilder.where(condition);
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Employee.class, "employee")
                .where(in(property("employee", Employee.id()), employeeIdsBuilder.buildQuery()));

        if (CollectionUtils.isNotEmpty(primaryKeys))
            builder.where(in(property("employee", Employee.id()), primaryKeys));
        if (StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property("employee", Employee.person().identityCard().fullFio()), value(CoreStringUtils.escapeLike(filter, true))));
        builder.order(property("employee", Employee.person().identityCard().fullFio()));

        return builder;
    }
}
