/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.entity.education;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.entity.IReorganizationElement;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.entityTitle.EntityTitleUtils;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EducationOrgUnit extends EducationOrgUnitGen implements ITitled
{
    public static final String P_TITLE = EducationOrgUnitGen.P_TITLE;

    // et.educationOrgUnit.educationLevelHighSchool
    public static final String EDU_LVL_HS_COLUMN_TITLE = EntityTitleUtils.et(EducationOrgUnit.class, "educationLevelHighSchool");

    /**
     * Вариант 1
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23101872")
    @EntityDSLSupport(parts={
        EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchool.P_FULL_TITLE
    })
    @Override
    public String getTitle()
    {
        if (getEducationLevelHighSchool() == null) {
            return this.getClass().getSimpleName();
        }
        return getEducationLevelHighSchool().getFullTitle();
    }

    public static final String P_TITLE_WITH_FORM_AND_CONDITION = EducationOrgUnitGen.P_TITLE_WITH_FORM_AND_CONDITION;

    /**
     * Вариант 2
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23101872")
    @EntityDSLSupport(parts={
        EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+"."+EducationLevelsHighSchool.L_EDUCATION_LEVEL+"."+EducationLevels.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_FORM+"."+DevelopForm.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_CONDITION+"."+DevelopCondition.P_CODE
    })
    @Override
    public String getTitleWithFormAndCondition()
    {
        List<String> list = new ArrayList<>(2);
        if (getDevelopForm() != null && StringUtils.isNotEmpty(getDevelopForm().getShortTitle()))
            list.add(getDevelopForm().getShortTitle());
        if (getDevelopCondition() != null && StringUtils.isNotEmpty(getDevelopCondition().getShortTitle()) && !UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(getDevelopCondition().getCode()))
            list.add(getDevelopCondition().getShortTitle());

        if (list.isEmpty())
            return getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + " " + getEducationLevelHighSchool().getTitle();

        return getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + " " + getEducationLevelHighSchool().getTitle() + " (" + StringUtils.join(list.iterator(), ", ") + ")";
    }

    /**
     * Вариант 3
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23101872")
    @EntityDSLSupport(parts={
        EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT+"."+OrgUnit.P_SHORT_TITLE,
        EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT+"."+ OrgUnit.P_TERRITORIAL_SHORT_TITLE
    })
    @Override
    public String getDevelopOrgUnitShortTitle() {
        return getFormativeOrgUnit().getShortTitle() + " (" + getTerritorialOrgUnit().getTerritorialShortTitle()+")";
    }

    @EntityDSLSupport(parts={
        EducationOrgUnitGen.L_DEVELOP_FORM+"."+DevelopForm.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_CONDITION+"."+DevelopCondition.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_TECH+"."+DevelopTech.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_PERIOD+"."+DevelopPeriod.P_CODE
    })
    @Override
    public String getDevelopCombinationTitle() {
        return DevelopUtil.getTitle(getDevelopForm(), getDevelopCondition(), getDevelopTech(), getDevelopPeriod());
    }

    @EntityDSLSupport(parts={
        EducationOrgUnitGen.L_DEVELOP_FORM+"."+DevelopForm.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_CONDITION+"."+DevelopCondition.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_TECH+"."+DevelopTech.P_CODE,
        EducationOrgUnitGen.L_DEVELOP_PERIOD+"."+DevelopPeriod.P_CODE
    })
    @Override
    public String getDevelopCombinationFullTitle()
    {
        StringBuilder combinedTitle = new StringBuilder(getDevelopForm().getTitle());
        if (!UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(getDevelopCondition().getCode()))
            combinedTitle.append(", ").append(getDevelopCondition().getTitle());
        if (!UniDefines.DEVELOP_TECH_SIMPLE.equals(getDevelopTech().getCode()))
            combinedTitle.append(", ").append(getDevelopTech().getTitle());
        combinedTitle.append(", ").append(getDevelopPeriod().getTitle());
        return combinedTitle.toString();
    }

    /**
     * Вариант 4
     * Не использовать в БД-сортировках! (описать это в EntityDSLSupport невозможно)
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23101872")
    @EntityDSLSupport
    @Override
    public String getProgramSubjectShortExtendedTitle()
    {
        final EduProgramSubject progSubj = getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
        if (progSubj != null) {
            final StrBuilder str = new StrBuilder();
            if (progSubj.getSubjectCode() != null)
                str.append(progSubj.getSubjectCode());
            if (progSubj.getShortTitle() != null)
                str.appendSeparator(' ').append(progSubj.getShortTitle());

            EduProgramSpecialization progSpec = getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization();
            if (progSpec != null && progSpec.isRootSpecialization())
                progSpec = null;

            final EduProgramOrientation orientation = getEducationLevelHighSchool().getProgramOrientation();
            if (progSpec != null || orientation != null)
            {
                str.appendSeparator(" (");
                if (progSpec != null)
                    str.append(progSpec.getShortTitle());

                if (orientation != null) {
                    if (progSpec != null)
                        str.append(", ");

                    str.append(orientation.getAbbreviation());
                }
                str.append(')');
            }

            str.appendSeparator(", ");
            str.append(getFormativeOrgUnit().getShortTitle());

            if (!getTerritorialOrgUnit().isTop()) {
                str.appendSeparator(", ");
                str.append(getTerritorialOrgUnit().getShortTitle());
            }

            str.appendSeparator(", ");
            str.append(getDevelopForm().getShortTitle());

            if (!getDevelopCondition().getCode().equals(DevelopConditionCodes.FULL_PERIOD)) {
                str.appendSeparator(", ");
                str.append(getDevelopCondition().getShortTitle());
            }

            return str.toString();
        }
        else {
            return "";
        }
    }

    /**
     * Вариант 5
     * Не использовать в БД-сортировках! (описать это в EntityDSLSupport невозможно)
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23101872")
    @EntityDSLSupport
    @Override
    public String getProgramSubjectShortExtendedTitleWithDevelopPeriod()
    {
        return getProgramSubjectShortExtendedTitle() + ", " + getDevelopPeriod().getTitle();
    }

    @Override
    public IPropertyPath getReorgranizationTitlePath() {
        return titleWithFormAndCondition();
    }

    @Override
    public void changeReorganizationElement(IReorganizationElement element) {
        setFormativeOrgUnit(((OrgUnit) element));
    }

    @Override
    public EventListenerLocker<IDSetEventListener> getLocker() {
        return null;
    }

    @Override
    public List<IEntity> getLinkedEntitiesToUpdate(IReorganizationElement newOwner) {
        return Collections.emptyList();
    }
}