/* $Id$ */
package ru.tandemservice.uni.component.catalog.educationLevelHighGos3.EducationLevelHighGos3AddEdit;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.uni.entity.catalog.EducationLevelHighGos3;

/**
 * @author oleyba
 * @since 5/8/11
 */
public class Controller  extends ru.tandemservice.uni.component.catalog.educationLevels.EducationLevelsAddEdit.Controller<EducationLevelHighGos3, Model, IDAO>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }
}