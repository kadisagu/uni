/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.dao;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;

import java.util.Collection;
import java.util.List;

/**
 * @author lefay
 */
@SuppressWarnings("unchecked")
public interface IOrgstructDAO
{
    String ORG_STRUCT_DAO_BEAN_NAME = "orgstructDAO";
    SpringBeanCache<IOrgstructDAO> instance = new SpringBeanCache<>(ORG_STRUCT_DAO_BEAN_NAME);

    //получает список орг.юнитов по строке-шаблону и виду подразделения
    ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode);

    //получает список орг.юнитов по строке-шаблону и виду подразделения с фильтрацией прав
    ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode, IPrincipalContext context);

    //получает список орг.юнитов по строке-шаблону и виду подразделения с фильтрацией прав с указанием ограничения
    ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode, IPrincipalContext context, int maxResult);

    //получает список орг.юнитов по строке-шаблону и виду подразделения с фильтрацией прав с указанием ограничения
    ListResult<OrgUnit> getOrgUnitList(String filter, String kindCode, IPrincipalContext context, int maxResult, Object primaryKeys);

    //получает список орг.юнитов по виду подразделения
    List<OrgUnit> getOrgUnitList(String kindCode);

    //получает список видов указанного подразделения
    List<OrgUnitKind> getOrgUnitKindList(OrgUnit orgUnit);

    /**
     * Получение кодов видов подразделения, осуществляющих прием студентов, связанных с указанным оргюнитом
     *
     * @param orgUnit оргюнит
     * @return список кодов типа подразделения
     */
    Collection<String> getAllowStudentsOrgUnitKindCodes(OrgUnit orgUnit);

    /**
     * Получение кодов видов подразделения, осуществляющих формирование групп, связанных с указанным оргюнитом
     *
     * @param orgUnit оргюнит
     * @return список кодов типа подразделения
     */
    Collection<String> getAllowGroupsOrgUnitKindCodes(OrgUnit orgUnit);

    //формирующий ли орг.юнит
    boolean isOrgUnitHasKind(OrgUnit orgUnit, String kindCode);

    //может ли орг.юнит содержать студентов
    boolean isAllowStudents(OrgUnit orgUnit);

    //можно ли в орг.юнит добавлять студентов
    boolean isAllowAddStudents(OrgUnit orgUnit);

    //может ли орг.юнит содержать группы
    boolean isAllowGroups(OrgUnit orgUnit);

    //можно ли в орг. юнит добавлять группы
    boolean isAllowAddGroups(OrgUnit orgUnit);

    // является ли формирующим и\или территориальным
    boolean isOrgUnitFormingOrTerritorial(OrgUnit orgUnit);

    boolean hidePermissionsBasedOnFormingAndTerritorialKind(OrgUnitType orgUnitType);
}