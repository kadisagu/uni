/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uni.ui;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 */
public class OrgUnitKindAutocompleteModel extends BaseSingleSelectModel implements IMultiSelectModel
{
    private String _kindCode;
    private IPrincipalContext _principalContext;

    public OrgUnitKindAutocompleteModel(String kindCode, IPrincipalContext principalContext)
    {
        super(kindCode.equals(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL) ? OrgUnit.P_TERRITORIAL_FULL_TITLE : OrgUnit.P_FULL_TITLE);
        _kindCode = kindCode;
        _principalContext = principalContext;
    }

    public OrgUnitKindAutocompleteModel(String kindCode)
    {
        this(kindCode, null);
    }

    private ListResult<OrgUnit> getList(String filter, Object primaryKeys) {
        return UniDaoFacade.getOrgstructDao().getOrgUnitList(filter, _kindCode, _principalContext, 50, primaryKeys);
    }

    @Override
    public ListResult<OrgUnit> findValues(String filter)
    {
        return getList(filter, null);
    }

    @Override
    public List<OrgUnit> getValues(Set primaryKeys)
    {
        return getList(null, primaryKeys).getObjects();
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        if (primaryKey != null) {
            List<OrgUnit> list = getList(null, primaryKey).getObjects();
            if (list.size() > 1) {
                throw new IllegalStateException();
            }
            return list.size() == 1 ? list.get(0) : null;
        }
        return null;
    }

    protected String getKindCode()
    {
        return _kindCode;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }
}
