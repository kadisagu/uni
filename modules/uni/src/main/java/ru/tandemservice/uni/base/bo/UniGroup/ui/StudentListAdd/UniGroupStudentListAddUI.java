/* $Id$ */
package ru.tandemservice.uni.base.bo.UniGroup.ui.StudentListAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Collection;

/**
 * @author Alexey Lopatin
 * @since 15.11.2013
 */
@State({@Bind(key = "groupId", binding = "groupId")})
public class UniGroupStudentListAddUI extends UIPresenter
{
    private Long _groupId;
    private Group _group;

    @Override
    public void onComponentRefresh()
    {
        _group = DataAccessServices.dao().getNotNull(_groupId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UniGroupStudentListAdd.GROUP, getGroup());
    }

    // Getters & Setters

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    // Listeners

    public void onClickApply()
    {
        Collection<IEntity> selected = ((BaseSearchListDataSource) getConfig().getDataSource(UniGroupStudentListAdd.STUDENT_DS)).getOptionColumnSelectedObjects("selected");

        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одного студента.");

        for (IEntity entity : selected)
        {
            Student student = (Student) entity;
            student.setGroup(_group);
            DataAccessServices.dao().update(student);
        }

        deactivate();
    }
}
