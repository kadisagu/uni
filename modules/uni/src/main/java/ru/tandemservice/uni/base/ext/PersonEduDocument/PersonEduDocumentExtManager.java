/* $Id:$ */
package ru.tandemservice.uni.base.ext.PersonEduDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.IPersonEduDocumentDao;
import ru.tandemservice.uni.base.ext.PersonEduDocument.logic.UniPersonEduDocumentDao;

/**
 * @author oleyba
 * @since 5/3/14
 */
@Configuration
public class PersonEduDocumentExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IPersonEduDocumentDao dao()
    {
        return new UniPersonEduDocumentDao();
    }
}
