/* $Id$ */
package ru.tandemservice.uni.component.catalog.structureEducationLevels.StructureEducationLevelsAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author Vasily Zhukov
 * @since 30.06.2011
 */
public class DAO extends DefaultCatalogAddEditDAO<StructureEducationLevels, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setShortTitleRequired(model.getCatalogItem().getParent() == null);
    }
}
