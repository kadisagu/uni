package ru.tandemservice.uni.ui;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;

/**
 * @author vdanilov
 */
public class SimpleRowCustomizer<T extends IEntity> implements IRowCustomizer<T> {

    @Override
    public String getHeadRowStyle() {
        return null;
    }

    @Override
    public String getRowStyle(T entity) {
        return null;
    }

    @Override
    public boolean isClickable(AbstractColumn<?> column, T rowEntity) {
        return false;
    }

    @Override
    public boolean isClickable(IPublisherLinkColumn column, T rowEntity, IEntity itemEntity) {
        return true; // иначе ни одна IPublisherLinkColumn во всем списке кликабельна не будет
    }
}
