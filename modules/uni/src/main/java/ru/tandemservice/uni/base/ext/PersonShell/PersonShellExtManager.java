/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uni.base.ext.PersonShell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.person.base.bo.PersonShell.PersonShellManager;

/**
 * @author Vasily Zhukov
 * @since 15.03.2012
 */
@Configuration
public class PersonShellExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private PersonShellManager _personManager;

    @Bean
    @SuppressWarnings("unchecked")
    public ItemListExtension<String> cssListExtension() {
        return itemListExtension(_personManager.cssListExtPoint())
//                .add("uni-common-table", "css/common-table.css")
                .add("uni-eduplanversion", "css/eduplanversion.css")
                .add("uni-rich-table-list", "css/rich-table-list.css")
                .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ItemListExtension<String> jsListExtension() {
        return itemListExtension(_personManager.jsListExtPoint())
                .add("uni-eduprocshed", "js/eduprocsched.js")
                .add("uni-eduplanversion", "js/eduplanversion.js")
                .add("uni-richtable", "js/richtable.js")
                .add("uni-uni", "js/uni.js")
                .create();
    }


}
