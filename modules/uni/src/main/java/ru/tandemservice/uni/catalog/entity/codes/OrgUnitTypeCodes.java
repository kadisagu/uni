package ru.tandemservice.uni.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип орг. юнита"
 * Имя сущности : orgUnitType
 * Файл data.xml : orgstruct.data.xml
 */
public interface OrgUnitTypeCodes
{
    /** Константа кода (code) элемента : Головное подр. (title) */
    String TOP = "top";
    /** Константа кода (code) элемента : Филиал (title) */
    String BRANCH = "branch";
    /** Константа кода (code) элемента : Представительство (title) */
    String REPRESENTATION = "representation";
    /** Константа кода (code) элемента : Колледж (title) */
    String COLLEGE = "college";
    /** Константа кода (code) элемента : Институт (title) */
    String INSTITUTE = "institute";
    /** Константа кода (code) элемента : Факультет (title) */
    String FACULTY = "faculty";
    /** Константа кода (code) элемента : Кафедра (title) */
    String CATHEDRA = "cathedra";
    /** Константа кода (code) элемента : Лаборатория (title) */
    String LABORATORY = "laboratory";
    /** Константа кода (code) элемента : Отдел (title) */
    String DIVISION = "division";
    /** Константа кода (code) элемента : Центр (title) */
    String DIVISION_CENTER = "divisionCenter";
    /** Константа кода (code) элемента : Управление (title) */
    String DIVISION_MANAGEMENT = "divisionManagement";
    /** Константа кода (code) элемента : Отделение (title) */
    String DIVISION_OFFICE = "divisionOffice";
    /** Константа кода (code) элемента : Сектор (title) */
    String DIVISION_SECTOR = "divisionSector";
    /** Константа кода (code) элемента : Департамент (title) */
    String DEPARTMENT = "department";
    /** Константа кода (code) элемента : Комитет (title) */
    String COMMITTEE = "committee";
    /** Константа кода (code) элемента : Общежитие (title) */
    String HOTEL = "hotel";
    /** Константа кода (code) элемента : Библиотека (title) */
    String LIBRARY = "library";
    /** Константа кода (code) элемента : Музей (title) */
    String MUSEUM = "museum";
    /** Константа кода (code) элемента : Поликлиника (title) */
    String POLYCLINIC = "polyclinic";
    /** Константа кода (code) элемента : Пресс-служба (title) */
    String PRESS_OFFICE = "pressOffice";
    /** Константа кода (code) элемента : Служба (title) */
    String SERVICE = "service";
    /** Константа кода (code) элемента : Техникум (title) */
    String TECHNICAL_SCHOOL = "technicalSchool";
    /** Константа кода (code) элемента : Училище (title) */
    String VOCATIONAL_SCHOOL = "vocationalSchool";
    /** Константа кода (code) элемента : Предметно-цикловая комиссия (title) */
    String SCC = "scc";

    Set<String> CODES = ImmutableSet.of(TOP, BRANCH, REPRESENTATION, COLLEGE, INSTITUTE, FACULTY, CATHEDRA, LABORATORY, DIVISION, DIVISION_CENTER, DIVISION_MANAGEMENT, DIVISION_OFFICE, DIVISION_SECTOR, DEPARTMENT, COMMITTEE, HOTEL, LIBRARY, MUSEUM, POLYCLINIC, PRESS_OFFICE, SERVICE, TECHNICAL_SCHOOL, VOCATIONAL_SCHOOL, SCC);
}
