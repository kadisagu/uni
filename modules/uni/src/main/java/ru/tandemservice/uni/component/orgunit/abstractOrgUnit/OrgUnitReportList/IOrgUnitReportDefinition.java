/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList;

import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author vip_delete
 * @since 24.01.2009
 * @deprecated
 * @see org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition
 */
@Deprecated
public interface IOrgUnitReportDefinition
{
    String LIST_BEAN_NAME = "orgUnitReportDefinitionList";

    /**
     * @return блок, в котором будет отображаться отчет
     */
    IOrgUnitReportBlockDefinition getBlock();

    // название отчета
    String getReportTitle();

    // для генерации permissionKey
    String getPermissionPrefix();

    // список отчетов на подразделении отображается списком блоков
    // этот линк-резолвер запускается при нажатии на отчет.
    // тут либо идет ссылка на список сохраненных отчетов, либо на форму добавления
    // в качестве IEntity всегда приходит null
    IPublisherLinkResolver getLinkResolver();

    /**
     * @param orgUnit текущее подразделении
     * @return true, если на orgUnit требуется отображать этот отчет
     */
    boolean isVisible(OrgUnit orgUnit);
}
