package ru.tandemservice.uni.entity.report;

import ru.tandemservice.uni.entity.report.gen.FiltersPresetGen;

/**
 * Шаблон фильтров
 */
public class FiltersPreset extends FiltersPresetGen
{
    public FiltersPreset()
    {
    }
    
    public FiltersPreset(String settingsKeyPrefix)
    {
        setSettingsKeyPrefix(settingsKeyPrefix);
    }
    
    public FiltersPreset(Long id, String title, String settingsKeyPrefix)
    {
        setId(id);
        setTitle(title);
        setSettingsKeyPrefix(settingsKeyPrefix);
    }
    
    public FiltersPreset(String title, String settingsKeyPrefix, int number)
    {
        setTitle(title);
        setSettingsNumber(number);
        setSettingsKeyPrefix(settingsKeyPrefix);
    }
}