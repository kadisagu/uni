package ru.tandemservice.uni.base.bo.UniEduProgram.logic;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLNoReferenceExpressionBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.IUniSyncDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.EduProgramSubjectManager;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class UniEduProgramSyncDao extends UniBaseDao implements IUniEduProgramSyncDao {

    // { qualification.code, levelType.code, profileLevelType.code }
    private static final String[] CFG_I_2005_51 = new String[] { QualificationsCodes.BAZOVYY_UROVEN_S_P_O, StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_BASIC };
    private static final String[] CFG_I_2005_52 = new String[] { QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O, StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_ADVANCED };
    private static final String[] CFG_I_2005_62 = new String[] { QualificationsCodes.BAKALAVR, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_FIELD, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE };
    private static final String[] CFG_I_2005_65 = new String[] { QualificationsCodes.SPETSIALIST, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION };
    private static final String[] CFG_I_2005_68 = new String[] { QualificationsCodes.MAGISTR, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_FIELD, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE };

    // { qualification.code, levelType.code, profileLevelType.code }
    private static final String[] CFG_I_2009_40 = new String[] { QualificationsCodes.N_P_O_2, StructureEducationLevelsCodes.BASIC_GROUP_PROFESSION };
    private static final String[] CFG_I_2009_51 = new String[] { QualificationsCodes.BAZOVYY_UROVEN_S_P_O, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_BASIC };
    private static final String[] CFG_I_2009_52 = new String[] { QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED };
    private static final String[] CFG_I_2009_62 = new String[] { QualificationsCodes.BAKALAVR, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_FIELD, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE };
    private static final String[] CFG_I_2009_65 = new String[] { QualificationsCodes.SPETSIALIST, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALTY, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALIZATION };
    private static final String[] CFG_I_2009_68 = new String[] { QualificationsCodes.MAGISTR, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_FIELD, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE };
    private static final String[] CFG_I_2009_00 = new String[] { QualificationsCodes.KANDIDAT_NAUK, StructureEducationLevelsCodes.HIGHER_SPECIALITY_POSTGRADUATE, StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_POSTGRADUATE};

    // { qualification.code, levelType.code, profileLevelType.code }
    private static final String[] CFG_I_2013_51 = new String[] { QualificationsCodes.BAZOVYY_UROVEN_S_P_O, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_BASIC };
    private static final String[] CFG_I_2013_52 = new String[] { QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED };
    private static final String[] CFG_I_2013_62 = new String[] { QualificationsCodes.BAKALAVR, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_FIELD, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE };
    private static final String[] CFG_I_2013_65 = new String[] { QualificationsCodes.SPETSIALIST, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALTY, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALIZATION };
    private static final String[] CFG_I_2013_68 = new String[] { QualificationsCodes.MAGISTR, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_FIELD, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE };

    private static final String[] CFG_I_2013_06 = new String[] { QualificationsCodes.ASPIRANTURA, StructureEducationLevelsCodes.HIGHER_SPECIALITY_POSTGRADUATE, StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_POSTGRADUATE };
    private static final String[] CFG_I_2013_07 = new String[] { QualificationsCodes.ADYUNKTURA, StructureEducationLevelsCodes.HIGHER_SPECIALITY_ADJUNCTURE, StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_ADJUNCTURE };
    private static final String[] CFG_I_2013_08 = new String[] { QualificationsCodes.ORDINATURA, StructureEducationLevelsCodes.HIGHER_SPECIALITY_TRAINEESHIP, StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_TRAINEESHIP };
    private static final String[] CFG_I_2013_10 = new String[] { QualificationsCodes.INTERNATURA, StructureEducationLevelsCodes.HIGHER_SPECIALITY_INTERNSHIP, StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_INTERNSHIP };

    protected PairKey<Qualifications, StructureEducationLevels> getLegacyStructureConfig(String eduProgramSubjectIndexCode, boolean specializationOrProfile, boolean advancedMiddleLevel)
    {
        Preconditions.checkArgument(!specializationOrProfile || !advancedMiddleLevel); // В СПО нет специализаций, только специальности

        final String[] cfg;
        switch (eduProgramSubjectIndexCode) {

            // СПО
            case EduProgramSubjectIndexCodes.TITLE_2005_50: cfg = advancedMiddleLevel ? CFG_I_2005_52 : CFG_I_2005_51; break;
            case EduProgramSubjectIndexCodes.TITLE_2009_50: cfg = advancedMiddleLevel ? CFG_I_2009_52 : CFG_I_2009_51; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_01: cfg = advancedMiddleLevel ? CFG_I_2013_52 : CFG_I_2013_51; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_02: cfg = advancedMiddleLevel ? CFG_I_2013_52 : CFG_I_2013_51; break;

            // ВО
            case EduProgramSubjectIndexCodes.TITLE_2005_62: cfg = CFG_I_2005_62; break;
            case EduProgramSubjectIndexCodes.TITLE_2005_65: cfg = CFG_I_2005_65; break;
            case EduProgramSubjectIndexCodes.TITLE_2005_68: cfg = CFG_I_2005_68; break;
            case EduProgramSubjectIndexCodes.TITLE_2009_62: cfg = CFG_I_2009_62; break;
            case EduProgramSubjectIndexCodes.TITLE_2009_65: cfg = CFG_I_2009_65; break;
            case EduProgramSubjectIndexCodes.TITLE_2009_68: cfg = CFG_I_2009_68; break;
            case EduProgramSubjectIndexCodes.TITLE_2009_00: cfg = CFG_I_2009_00; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_03: cfg = CFG_I_2013_62; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_04: cfg = CFG_I_2013_68; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_05: cfg = CFG_I_2013_65; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_06: cfg = CFG_I_2013_06; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_07: cfg = CFG_I_2013_07; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_08: cfg = CFG_I_2013_08; break;
            case EduProgramSubjectIndexCodes.TITLE_2013_10: cfg = CFG_I_2013_10; break;

            default: throw new IllegalStateException();
        }

        // Для специализаций в конфиге должно быть третье значение
        Preconditions.checkArgument((specializationOrProfile && cfg.length == 3) || (!specializationOrProfile && cfg.length >= 2));

        final Qualifications qualification = getCatalogItem(Qualifications.class, cfg[0]);
        final StructureEducationLevels level = getCatalogItem(StructureEducationLevels.class, specializationOrProfile ? cfg[2] : cfg[1]);

        Preconditions.checkNotNull(qualification);
        Preconditions.checkNotNull(level);

        return new PairKey<>(qualification, level);
    }

    @Override
    public Set<String> getSubjectIndexCodeSet(EducationLevels eduLvl)
    {
        final Set<String> codeSet = new HashSet<>(4);

        // 2013
        if (eduLvl instanceof EducationLevelHighGos3s) {
            if (eduLvl.getLevelType().isBachelor()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_03);
            }
            if (eduLvl.getLevelType().isMaster()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_04);
            }
            if (eduLvl.getLevelType().isSpecialty() || eduLvl.getLevelType().isSpecialization()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_05);
            }
        }

        if (eduLvl instanceof EducationLevelMiddleGos3s) {
            codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_01);
            codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_02);
        }

        // 2009
        if (eduLvl instanceof EducationLevelHighGos3) {
            if (eduLvl.getLevelType().isBachelor()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2009_62);
            }
            if (eduLvl.getLevelType().isMaster()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2009_68);
            }
            if (eduLvl.getLevelType().isSpecialty() || eduLvl.getLevelType().isSpecialization()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2009_65);
            }
        }

        if (eduLvl instanceof EducationLevelMiddleGos3) {
            codeSet.add(EduProgramSubjectIndexCodes.TITLE_2009_50);
        }

        // 2005
        if (eduLvl instanceof EducationLevelHighGos2) {
            if (eduLvl.getLevelType().isBachelor()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2005_62);
            }
            if (eduLvl.getLevelType().isMaster()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2005_68);
            }
            if (eduLvl.getLevelType().isSpecialty() || eduLvl.getLevelType().isSpecialization()) {
                codeSet.add(EduProgramSubjectIndexCodes.TITLE_2005_65);
            }
        }

        if (eduLvl instanceof EducationLevelMiddleGos2) {
            codeSet.add(EduProgramSubjectIndexCodes.TITLE_2005_50);
        }

        if (eduLvl instanceof EducationLevelHigher) {
            switch (eduLvl.getLevelType().getCode()) {
                case StructureEducationLevelsCodes.HIGHER_SPECIALITY_POSTGRADUATE:
                case StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_POSTGRADUATE:
                    codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_06);
                case StructureEducationLevelsCodes.HIGHER_SPECIALITY_INTERNSHIP:
                case StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_INTERNSHIP:
                    codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_10);
                case StructureEducationLevelsCodes.HIGHER_SPECIALITY_TRAINEESHIP:
                case StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_TRAINEESHIP:
                    codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_08);
                case StructureEducationLevelsCodes.HIGHER_SPECIALITY_ADJUNCTURE:
                case StructureEducationLevelsCodes.HIGHER_SPECIALIZATION_ADJUNCTURE:
                    codeSet.add(EduProgramSubjectIndexCodes.TITLE_2013_07);
            }
        }

        return codeSet;
    }


    /**
     * @return класс НПв для перечня направлений
     */
    @Override
    public Class<? extends EducationLevels> getEduLvlClass(String subjectIndexCode)
    {
        switch (subjectIndexCode) {
            case EduProgramSubjectIndexCodes.TITLE_2005_50: return EducationLevelMiddleGos2.class;
            case EduProgramSubjectIndexCodes.TITLE_2005_62: return EducationLevelHighGos2.class;
            case EduProgramSubjectIndexCodes.TITLE_2005_65: return EducationLevelHighGos2.class;
            case EduProgramSubjectIndexCodes.TITLE_2005_68: return EducationLevelHighGos2.class;
            case EduProgramSubjectIndexCodes.TITLE_2009_40: return EducationLevelBasic.class;
            case EduProgramSubjectIndexCodes.TITLE_2009_50: return EducationLevelMiddleGos3.class;
            case EduProgramSubjectIndexCodes.TITLE_2009_62: return EducationLevelHighGos3.class;
            case EduProgramSubjectIndexCodes.TITLE_2009_65: return EducationLevelHighGos3.class;
            case EduProgramSubjectIndexCodes.TITLE_2009_68: return EducationLevelHighGos3.class;
            case EduProgramSubjectIndexCodes.TITLE_2009_00: return EducationLevelHigher.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_01: return EducationLevelMiddleGos3s.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_02: return EducationLevelMiddleGos3s.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_03: return EducationLevelHighGos3s.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_04: return EducationLevelHighGos3s.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_05: return EducationLevelHighGos3s.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_06: return EducationLevelHigher.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_07: return EducationLevelHigher.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_08: return EducationLevelHigher.class;
            case EduProgramSubjectIndexCodes.TITLE_2013_10: return EducationLevelHigher.class;
            default: throw new IllegalStateException();
        }
    }


    @Override
    public void doSaveEducationLevelsHighSchool(EducationLevelsHighSchool eduHs)
    {
        final Session session = lock("EducationLevelsHighSchool");

        // session.merge() не помогает, если при редактировании сработал констрейнт уникальности,
        // данные на форме исправили и нажали "Сохранить" еще раз.
        // Надо в платформе разбираться, как так в сессии оказывается другой объект.
        // Пока делаем ручной мерж.
        final EducationLevelsHighSchool inDB_HS = eduHs.getId() != null ? (EducationLevelsHighSchool) session.get(EducationLevelsHighSchool.class, eduHs.getId()) : null;
        if (inDB_HS != null) {
            inDB_HS.update(eduHs, false);
            eduHs = inDB_HS;
        }

        if (eduHs.getEducationLevel() != null && eduHs.getAssignedQualification() != null && eduHs.getEducationLevel().getEduProgramSubject() != null) {
            if (eduHs.getEducationLevel() instanceof EducationLevelMiddle) {
                eduHs.setEducationLevel(createOrGetEduLvl4Middle(
                        eduHs.getEducationLevel().getEduProgramSubject(),
                        QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(eduHs.getEducationLevel().getQualification().getCode())
                ));
            } else if (eduHs.getEducationLevel() instanceof EducationLevelHigh || eduHs.getEducationLevel() instanceof EducationLevelHigher) {
                eduHs.setEducationLevel(createOrGetEduLvl4High(
                        eduHs.getEducationLevel().getEduProgramSubject(),
                        eduHs.getEducationLevel().getEduProgramSpecialization()
                ));
            }
            eduHs.setSubjectQualification(this.getByNaturalId(new EduProgramSubjectQualification.NaturalId(eduHs.getEducationLevel().getEduProgramSubject(), eduHs.getAssignedQualification())));
        }

        if (eduHs.getCloseDate() != null && eduHs.getOpenDate() == null) {
            throw new ApplicationException("Дата открытия должна быть задана, если задана дата закрытия.");
        }

        if (eduHs.getCloseDate() != null && !eduHs.getCloseDate().after(eduHs.getOpenDate())) {
            throw new ApplicationException("Дата открытия должна быть раньше даты закрытия.");
        }

        session.saveOrUpdate(eduHs);
    }

    private Class<? extends EducationLevelMiddle> getMiddleEduLevelClass(EduProgramSubjectIndex subjectIndex) {

        if (!subjectIndex.getProgramKind().isProgramSecondaryProf()) {
            throw new ApplicationException("Направление ОП должно быть уровня СПО.");
        }

        @SuppressWarnings("unchecked")
        final Class<? extends EducationLevelMiddle> eduLvlClass = (Class) getEduLvlClass(subjectIndex.getCode());
        if (!EducationLevelMiddle.class.isAssignableFrom(eduLvlClass)) {
            throw new IllegalStateException();
        }

        return eduLvlClass;
    }

    @Override
    public EducationLevelMiddle getEduLvl4Middle(EduProgramSubject subject, boolean advancedLevel)
    {
        final Class<? extends EducationLevelMiddle> eduLvlClass = getMiddleEduLevelClass(subject.getSubjectIndex());
        final PairKey<Qualifications, StructureEducationLevels> legacyConfig = getLegacyStructureConfig(subject.getSubjectIndex().getCode(), false, advancedLevel);

        return new DQLSelectBuilder()
                .fromEntity(eduLvlClass, "x")
                .column(property("x")).top(1)
                .where(eq(property(EducationLevels.qualification().fromAlias("x")), value(legacyConfig.<Qualifications>getFirst())))
                .where(eq(property(EducationLevels.levelType().fromAlias("x")), value(legacyConfig.<StructureEducationLevels>getSecond())))
                .where(eq(property(EducationLevels.eduProgramSubject().fromAlias("x")), value(subject)))
                .order(property("x.id"))
                .createStatement(getSession())
                .uniqueResult();
    }

    @Override
    public EducationLevelMiddle createOrGetEduLvl4Middle(EduProgramSubject subject, boolean advancedLevel) {

        // поехали
        final Session session = lock("eduLvl.sync."+subject.getId());

        EducationLevelMiddle result = getEduLvl4Middle(subject, advancedLevel);

        if (null == result){

            final Class<? extends EducationLevelMiddle> eduLvlClass = getMiddleEduLevelClass(subject.getSubjectIndex());
            // Не нашли подходящего - создаем
            try {
                result = eduLvlClass.newInstance();
                result.setCode("temp.sync.subj." + subject.getCode() + (advancedLevel ? ".2" : ".1"));
            } catch (InstantiationException | IllegalAccessException e) {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
        }

        // Заполняем новый или исправляем существующий (он легко мог быть с кривым уровнем или квалификацией)
        final PairKey<Qualifications, StructureEducationLevels> legacyConfig = getLegacyStructureConfig(subject.getSubjectIndex().getCode(), false, advancedLevel);

        result.setParentLevel(null);
        result.setLevelType(legacyConfig.getSecond());
        result.setQualification(legacyConfig.getFirst());
        result.setTitle(subject.getTitle());
        result.setShortTitle(subject.getShortTitle());
        result.setEduProgramSubject(subject);
        result.setEduProgramSpecialization(null);
        if (legacyConfig.getSecond().isGos2()) {
            result.setOkso(StringUtils.substringBefore(subject.getSubjectCode(), "."));
            result.setInheritedOkso(result.buildInheritedOksoNonPersistent());
        } else {
            result.setOkso(null);
            result.setInheritedOkso(null);
        }

        session.saveOrUpdate(result);

        return result;
    }

    private Class<? extends EducationLevels> getHighOrHigherEduLevelClass(EduProgramSubject subject) {

        if (!subject.getSubjectIndex().getProgramKind().isProgramHigherProf()) {
            throw new ApplicationException("Направление ОП должно быть с уровнем «Высшее образование».");
        }

        @SuppressWarnings("unchecked")
        final Class<? extends EducationLevels> eduLvlClass = (Class) getEduLvlClass(subject.getSubjectIndex().getCode());
        if (!EducationLevelHigh.class.isAssignableFrom(eduLvlClass) && !EducationLevelHigher.class.isAssignableFrom(eduLvlClass)) {
            throw new IllegalStateException("eduLvlClass is not EducationLevelHigh or EducationLevelHigher");
        }

        return eduLvlClass;
    }

    @Override
    public EducationLevels getEduLvl4High(EduProgramSubject subject, EduProgramSpecialization specialization)
    {
        final Class<? extends EducationLevels> eduLvlClass = getHighOrHigherEduLevelClass(subject);

        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(eduLvlClass, "x")
                .column(property("x")).top(1)
                .where(eq(property(EducationLevels.eduProgramSubject().fromAlias("x")), value(subject)))
                .order(property("x.id"))
                ;

        if (specialization != null) {
            dql.where(eq(property(EducationLevels.eduProgramSpecialization().fromAlias("x")), value(specialization)));
        } else {
            dql.where(eq(property(EducationLevels.eduProgramSpecialization().clazz().fromAlias("x")), value(EntityRuntime.getMeta(EduProgramSpecializationRoot.class).getEntityCode())));
        }

        return dql.createStatement(getSession()).uniqueResult();
    }

    private EducationLevels findEduLlv4High(EduProgramSubject subject, EduProgramSpecialization specialization) {

        final Class<? extends EducationLevels> eduLvlClass = getHighOrHigherEduLevelClass(subject);
        return new DQLSelectBuilder()
                .fromEntity(eduLvlClass, "s")
                .column(property("s"))
                .where(eq(property(EducationLevels.eduProgramSubject().fromAlias("s")), value(subject)))
                .where(eq(property(EducationLevels.eduProgramSpecialization().fromAlias("s")), value(specialization)))
                .createStatement(getSession()).uniqueResult()
                ;
    }

    @Override
    public EducationLevels createOrGetEduLvl4High(EduProgramSubject subject, EduProgramSpecialization specialization) {

        final EduProgramSpecializationRoot rootSpec = EduProgramSubjectManager.instance().dao().createOrGetProgramSpecializationRoot(subject);
        if (specialization == null) {
            specialization = rootSpec;  // Нужна общая направленность, если не указана
        }

        final Class<? extends EducationLevels> eduLvlClass = getHighOrHigherEduLevelClass(subject);

        // поехали
        final Session session = lock("eduLvl.sync."+subject.getId());

        // Ищем НПм на общую направленность
        EducationLevels baseEduLev = findEduLlv4High(subject, rootSpec);
        final PairKey<Qualifications, StructureEducationLevels> baseLegacyConfig = getLegacyStructureConfig(subject.getSubjectIndex().getCode(), false, false);
        if (baseEduLev == null) {
            // не нашли - создаем
            try {
                baseEduLev = eduLvlClass.newInstance();
                baseEduLev.setCode("temp.sync.subj." + subject.getCode());
            } catch (InstantiationException | IllegalAccessException e) {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
        }

        // Заполняем новый или исправляем существующий (он легко мог быть с кривым уровнем или квалификацией)
        baseEduLev.setLevelType(baseLegacyConfig.getSecond());
        baseEduLev.setQualification(baseLegacyConfig.getFirst());
        baseEduLev.setEduProgramSubject(subject);
        baseEduLev.setEduProgramSpecialization(rootSpec);
        baseEduLev.setParentLevel(null);
        baseEduLev.setTitle(subject.getTitle());
        baseEduLev.setShortTitle(subject.getShortTitle());
        if (baseLegacyConfig.getSecond().isGos2()) {
            baseEduLev.setOkso(StringUtils.substringBefore(subject.getSubjectCode(), "."));
            baseEduLev.setInheritedOkso(baseEduLev.buildInheritedOksoNonPersistent());
        } else {
            baseEduLev.setOkso(null);
            baseEduLev.setInheritedOkso(null);
        }

        session.saveOrUpdate(baseEduLev);

        if (specialization.isRootSpecialization()) {
            return baseEduLev; // он нам и нужен
        }


        EducationLevels result = findEduLlv4High(subject, specialization);
        final PairKey<Qualifications, StructureEducationLevels> legacyConfig = getLegacyStructureConfig(subject.getSubjectIndex().getCode(), true, false);

        if (null == result) {
            // не нашли - создаем
            try {
                result = eduLvlClass.newInstance();
                result.setCode("temp.sync.spec." + subject.getCode() + "." + Long.toString(specialization.getId(), 32));
            } catch (InstantiationException | IllegalAccessException e) {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
        }

        // Исправляем существующий (он легко мог быть с кривым уровнем или квалификацией)
        result.setParentLevel(baseEduLev);
        result.setTitle(specialization.getTitle());
        result.setShortTitle(specialization.getShortTitle());
        result.setEduProgramSubject(subject);
        result.setEduProgramSpecialization(specialization);
        result.setLevelType(legacyConfig.getSecond());
        result.setQualification(legacyConfig.getFirst());
        if (legacyConfig.getSecond().isGos2()) {
            result.setInheritedOkso(result.buildInheritedOksoNonPersistent());
        } else {
            result.setOkso(null);
            result.setInheritedOkso(null);
        }

        session.saveOrUpdate(result);

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public void doCleanUpSubjectIndexDataCallback() {

        // убиваем все НПм 2013, на которые нет ссылок, ссылающиеся на неактуальные направления ОП - они создаются автоматом - их нужно убивать

        // СПО 2013
        executeAndClear(
            new DQLDeleteBuilder(EducationLevelMiddleGos3s.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder()
                .fromEntity(EducationLevelMiddleGos3s.class, "s")
                .column(property("s.id"))
                .where(isNotNull(property(EducationLevelMiddleGos3s.eduProgramSubject().outOfSyncDate().fromAlias("s"))))
                .where(new DQLNoReferenceExpressionBuilder(EducationLevelMiddleGos3s.class, "s.id").getExpression())
                .buildQuery()
            ))
        );

        // ВПО 2013
        executeAndClear(
            new DQLDeleteBuilder(EducationLevelHighGos3s.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder()
                .fromEntity(EducationLevelHighGos3s.class, "s")
                .column(property("s.id"))
                .where(isNotNull(property(EducationLevelHighGos3s.eduProgramSubject().outOfSyncDate().fromAlias("s"))))
                .where(new DQLNoReferenceExpressionBuilder(EducationLevelHighGos3s.class, "s.id").getExpression())
                .buildQuery()
            ))
        );

        // обновляем названия в НПм
        Session session = getSession();
        List<EducationLevels> eduLvlList = getList(EducationLevels.class, EducationLevels.shortTitle(), (String)null);
        for (EducationLevels eduLvl: eduLvlList) {
            eduLvl.setShortTitle(CommonBaseStringUtil.getShortTitle(eduLvl.getTitle()));
            session.saveOrUpdate(eduLvl);
        }
        session.flush();

        // Обновляем названия НПв
        IUniSyncDao.instance.get().doUpdateEduHSTitles();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

}