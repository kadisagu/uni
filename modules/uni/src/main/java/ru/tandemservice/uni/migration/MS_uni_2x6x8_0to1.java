package ru.tandemservice.uni.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.dialect.MSSqlDBDialect;
import org.tandemframework.dbsupport.ddl.dialect.PostgresDBDialect;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uni_2x6x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность student

        if (tool.table("student_t").column("personalnumber_p").type() == DBType.INTEGER)
		{
            tool.dropView("session_marks_ext_view");

            tool.renameColumn("student_t", "personalnumber_p", "longpersonalnumber_p");

			// создать колонку
			tool.createColumn("student_t", new DBColumn("personalnumber_p", DBType.createVarchar(255)));

            final int capacity = StudentNumberFormatter.getCapacity();

            if (tool.getDialect() instanceof MSSqlDBDialect)
            {
                tool.executeUpdate("update student_t set personalnumber_p = RIGHT(REPLICATE('0', ?) + CAST((longpersonalnumber_p / 10000 * ? + longpersonalnumber_p % 10000) as varchar(23)), ?)",
                        capacity+2, (int) Math.pow(10, capacity), capacity+2);
            }
            else if (tool.getDialect() instanceof PostgresDBDialect)
            {
                tool.executeUpdate("update student_t set personalnumber_p = lpad(cast((longpersonalnumber_p / 10000 * ? + longpersonalnumber_p % 10000) as varchar(23)), ?, '0')",
                        (int) Math.pow(10, capacity), capacity+2);
            }
            else
            {
                throw new IllegalStateException("Oracle is not supported.");
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("student_t", "personalnumber_p", false);
            // удалить колонку
            tool.dropColumn("student_t", "longpersonalnumber_p");
		}




    }
}