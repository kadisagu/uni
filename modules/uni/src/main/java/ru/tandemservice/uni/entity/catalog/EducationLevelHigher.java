package ru.tandemservice.uni.entity.catalog;

import ru.tandemservice.uni.entity.catalog.gen.*;

/** @see ru.tandemservice.uni.entity.catalog.gen.EducationLevelHigherGen */
public class EducationLevelHigher extends EducationLevelHigherGen
{
    public EducationLevelHigher()
    {
        setCatalogCode("educationLevelHigher");
    }
}