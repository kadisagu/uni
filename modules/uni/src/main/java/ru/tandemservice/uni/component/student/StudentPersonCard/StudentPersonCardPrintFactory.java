/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uni.component.student.StudentPersonCard;

import org.hibernate.Session;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfPictureUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ekachanova
 */
public class StudentPersonCardPrintFactory implements IStudentPersonCardPrintFactory
{
    private Data _data = new Data();

    protected static class RelativeData
    {
        private String fio = "";
        private String birthdate = "";
        private String workplace = "";
        private String address = "";

        public String getFio()
        {
            return fio;
        }

        public void setFio(String fio)
        {
            this.fio = fio;
        }

        public String getBirthdate()
        {
            return birthdate;
        }

        public void setBirthdate(String birthdate)
        {
            this.birthdate = birthdate;
        }

        public String getWorkplace()
        {
            return workplace;
        }

        public void setWorkplace(String workplace)
        {
            this.workplace = workplace;
        }

        public String getAddress()
        {
            return address;
        }

        public void setAddress(String address)
        {
            this.address = address;
        }
    }

    public static class Data implements IData
    {
        private RtfDocument _template;
        private String _fileName;
        private String[][] _personEducationData;
        private String[][] _relativesData;
        private Student _student;
        private OrderData _orderData;

        private Map<RelationDegree, RelativeData> relativesMap = new HashMap<>();
        private String education;

        @Override
        public RtfDocument getTemplate()
        {
            return _template;
        }

        @Override
        public void setTemplate(RtfDocument template)
        {
            _template = template;
        }

        @Override
        public String getFileName()
        {
            return _fileName;
        }

        @Override
        public void setFileName(String fileName)
        {
            _fileName = fileName;
        }

        @Override
        public String[][] getPersonEducationData()
        {
            return _personEducationData;
        }

        public void setPersonEducationData(String[][] personEducationData)
        {
            _personEducationData = personEducationData;
        }

        @Override
        public String[][] getRelativesData()
        {
            return _relativesData;
        }

        public void setRelativesData(String[][] relativesData)
        {
            _relativesData = relativesData;
        }

        @Override
        public Student getStudent()
        {
            return _student;
        }

        public void setStudent(Student student)
        {
            _student = student;
        }

        public OrderData getOrderData()
        {
            return _orderData;
        }

        public void setOrderData(OrderData orderData)
        {
            _orderData = orderData;
        }

        public Map<RelationDegree, RelativeData> getRelativesMap()
        {
            return relativesMap;
        }

        public String getEducation()
        {
            return education;
        }

        public void setEducation(String education)
        {
            this.education = education;
        }
    }

    @Override
    public Data getData()
    {
        return _data;
    }

    public void setData(Data data)
    {
        _data = data;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void initPrintData(Student student, List<PersonEduInstitution> personEduInstitutionList, List<PersonNextOfKin> personNextOfKinList, Session session)
    {
        getData().setStudent(student);
        getData().setOrderData(DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, student));

        if (PersonEduDocumentManager.isShowLegacyEduDocuments()) {
            //[Вид образовательного учреждения] | [Адрес образовательного учреждения], [Тип документа об образовании]: [Серия] №[Номер], дата выдачи: [Дата окончания]
            String[][] personEducationData = new String[personEduInstitutionList.size()][];
            for (int i = 0; i < personEduInstitutionList.size(); i++)
            {
                PersonEduInstitution personEduInstitution = personEduInstitutionList.get(i);

                String educationType = personEduInstitution.getEduInstitutionKind().getTitle();

                Address address = new Address();
                address.setSettlement(personEduInstitution.getAddressItem());
                address.setCountry(address.getSettlement().getCountry());

                StringBuilder buffer = new StringBuilder(address.getTitle() + ", " + personEduInstitution.getDocumentType().getTitle());
                if (personEduInstitution.getSeria() != null)
                    buffer.append(": ").append(personEduInstitution.getSeria());
                if (personEduInstitution.getNumber() != null)
                    buffer.append(" №").append(personEduInstitution.getNumber());
                buffer.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");

                personEducationData[i] = new String[]{educationType, buffer.toString()};
            }
            getData().setPersonEducationData(personEducationData);
            getData().setEducation(personEduInstitutionList.isEmpty() ? "" : formatEducation(personEduInstitutionList.get(0)));
        } else {
            String eduLevel = student.getEduDocument() == null || student.getEduDocument().getEduLevel() == null ? "" : student.getEduDocument().getEduLevel().getTitle();
            String document = student.getEduDocument() == null ? "" : student.getEduDocument().getTitleExtended();
            getData().setPersonEducationData(new String[][]{{eduLevel, document}});
            getData().setEducation(student.getEduDocument() != null ? formatEducation(student.getEduDocument()) : "");
        }

        //[Степень родства] | [ФИО] ([Дата рождения]), место работы: [Место работы] ([Должность]), тел. [Телефон]
        getData().getRelativesMap().clear();
        String[][] relativesData = new String[personNextOfKinList.size()][];
        for (int i = 0; i < personNextOfKinList.size(); i++)
        {
            PersonNextOfKin relative = personNextOfKinList.get(i);

            String relationDegree = relative.getRelationDegree() == null ? null : relative.getRelationDegree().getTitle();

            {
                StringBuilder builder = new StringBuilder();
                builder.append(relative.getFullFio());
                if (relative.getBirthDate() != null)
                    builder.append(" (").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(relative.getBirthDate())).append(")");
                if (relative.getEmploymentPlace() != null)
                    builder.append(", место работы: ").append(relative.getEmploymentPlace());
                if (relative.getPost() != null)
                    builder.append(" (").append(relative.getPost()).append(")");
                if (relative.getPhones() != null)
                    builder.append(", тел. ").append(relative.getPhones());
                relativesData[i] = new String[]{relationDegree == null ? "" : relationDegree, builder.toString()};
            }

            {
                RelativeData relativeData = new RelativeData();
                relativeData.setFio(relative.getFullFio());
                if (relative.getBirthDate() != null)
                    relativeData.setBirthdate(DateFormatter.DEFAULT_DATE_FORMATTER.format(relative.getBirthDate()));
                {
                    StringBuilder workplace = new StringBuilder();
                    if (relative.getEmploymentPlace() != null)
                        workplace.append(relative.getEmploymentPlace());
                    if (relative.getPost() != null)
                        workplace.append(", ").append(relative.getPost());
                    relativeData.setWorkplace(workplace.toString());
                }
                {
                    StringBuilder address = new StringBuilder();
                    if (relative.getAddress() != null)
                        address.append(relative.getAddress().getTitleWithFlat());
                    if (relative.getPhones() != null && relative.getAddress() != null)
                        address.append(", тел. ").append(relative.getPhones());
                    if (relative.getPhones() != null)
                        address.append(relative.getPhones());
                    relativeData.setAddress(address.toString());
                }
                getData().getRelativesMap().put(relative.getRelationDegree(), relativeData);
            }
        }
        getData().setRelativesData(relativesData);
    }

    private String formatEducation(PersonEduDocument personEduInstitution)
    {
        return personEduInstitution.getTitleExtended();
    }

    @SuppressWarnings("deprecation")
    private String formatEducation(PersonEduInstitution personEduInstitution)
    {
        StringBuilder result = new StringBuilder();
        if (personEduInstitution.getEduInstitutionKind() != null)
            result.append(personEduInstitution.getEduInstitutionKind().getShortTitle()).append(" ");
        if (personEduInstitution.getEduInstitution() != null)
            result.append(" ").append(personEduInstitution.getEduInstitution().getTitle());
        result.append(", ");
        if (personEduInstitution.getAddressItem() != null)
            result.append(personEduInstitution.getAddressItem().getTitleWithType()).append(", ");
        result.append(personEduInstitution.getDocumentType().getTitle());
        if (personEduInstitution.getSeria() != null)
            result.append(": ").append(personEduInstitution.getSeria());
        if (personEduInstitution.getNumber() != null)
            result.append(" №").append(personEduInstitution.getNumber());
        result.append(", ").append(personEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }

    @Override
    public RtfDocument createCard()
    {
        RtfDocument document = getData().getTemplate().getClone();
        modifyByStudent(document);
        return document;
    }

    protected void modifyByStudent(RtfDocument document)
    {
        Student student = getData().getStudent();
        DatabaseFile photo = student.getPerson().getIdentityCard().getPhoto();

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        if (null != photo && null != photo.getContent())
            RtfPictureUtil.insertPicture(document, photo.getContent(), "photo", 2000L, 1500L);
        else
            injectModifier.put("photo", "");

        injectModifier
                .put("formativeOrgUnit", getProperty(student, Student.educationOrgUnit().formativeOrgUnit().fullTitle().s()))
                .put("bookNum", getProperty(student, Student.personalFileNumber().s()))
                .put("groupName", getProperty(student, Student.group().title().s()))
                .put("lastName", getProperty(student, Student.person().identityCard().lastName().s()))
                .put("firstName", getProperty(student, Student.person().identityCard().firstName().s()))
                .put("middleName", getProperty(student, Student.person().identityCard().middleName().s()))
                .put("seria", getProperty(student, Student.person().identityCard().seria().s()))
                .put("number", getProperty(student, Student.person().identityCard().number().s()))
                .put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getIssuanceDate()))
                .put("issuance", getProperty(student, Student.person().identityCard().issuancePlace().s()))
                .put("educationOrgUnit", getProperty(student, Student.educationOrgUnit().title().s()))
                .put("sex", getProperty(student, Student.person().identityCard().sex().shortTitle().s()))
                .put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(getData().getStudent().getPerson().getIdentityCard().getBirthDate()))
                .put("birthPlace", getProperty(student, Student.person().identityCard().birthPlace().s()))
                .put("citizenship", getProperty(student, Student.person().identityCard().citizenship().title().s()))
                .put("nationality", getProperty(student, Student.person().identityCard().nationality().title().s()))
                .put("addressRegistration", getProperty(student, Student.person().identityCard().address().titleWithFlat().s()))
                .put("maritalStatus", getProperty(student, Student.person().familyStatus().title().s()))
                .put("addressTitle", getProperty(student, Student.person().address().titleWithFlat().s()))
                .put("homePhone", student.getPerson().getContactData().getPhoneFact())
                .put("mobilePhone", student.getPerson().getContactData().getPhoneMobile())
                .put("fax", "")
                .put("email", student.getPerson().getContactData().getEmail())
                .put("icq", student.getPerson().getContactData().getOther())
                .put("enrollmentOrderNumber", getData().getOrderData() == null ? "" : getData().getOrderData().getEduEnrollmentOrderNumber())
                .put("enrollmentOrderDate", getData().getOrderData() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(getData().getOrderData().getEduEnrollmentOrderDate()))
                .put("education", getData().getEducation())
                .put("workPlace", student.getPerson().getWorkPlace())
                .put("workPlacePosition", student.getPerson().getWorkPlacePosition())
                .put("bookNumber", student.getBookNumber())
                .put("developForm", student.getEducationOrgUnit().getDevelopForm().getTitle())
                .modify(document);

        // Enrollment data
        injectEnrollmentExtractData(student.getId(), document);

        new RtfTableModifier()
                .put("educationType", getData().getPersonEducationData())
                .put("parentType", getData().getRelativesData())
                .modify(document);
    }

    protected void injectEnrollmentExtractData(Long studentId, RtfDocument document)
    {
        RtfInjectModifier modifier = null;

        // Студент может быть зачислен или старым модулем или новым (есть клиенты, у которых оба модуля подключены)

        if (ApplicationRuntime.containsBean(IPrintStudentEnrData.UNIENR14_SPRING_BEAN_NAME))
            modifier = ApplicationRuntime.getBean(IPrintStudentEnrData.UNIENR14_SPRING_BEAN_NAME, IPrintStudentEnrData.class).getEnrPrintDataModifier(studentId);

        if (modifier == null && ApplicationRuntime.containsBean(IPrintStudentEnrData.UNIEC_SPRING_BEAN_NAME))
            modifier = ApplicationRuntime.getBean(IPrintStudentEnrData.UNIEC_SPRING_BEAN_NAME, IPrintStudentEnrData.class).getEnrPrintDataModifier(studentId);

        if (modifier == null)
        {
            // Возможно, лучше будет передавать модифаер в бины и там затирать метки...
            modifier = new RtfInjectModifier()
                    .put("orderNumber", "")
                    .put("orderDate", "")
                    .put("course", "")
                    .put("ball", "");
        }

        modifier.modify(document);
    }

    protected static String getProperty(Student student, String path)
    {
       String value = (String)student.getProperty(path);
       return  value != null ? value : "";
    }
}
