package ru.tandemservice.uniecc.component.entrant.NextOfKinAgreementUsage;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.gen.PersonNextOfKinGen;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniecc.dao.IEccNextOfKinDAO;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import ru.tandemservice.uniecc.entity.relations.gen.NextOfKinStatusRelationGen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO extends UniBaseDao implements IDAO {

	@Override
	public void updateSignInAgreement(final Model model, final Long id) {
		IEccNextOfKinDAO.INSTANCE.get().updateNextOfKinRelationProperty(getPersonId(model), id, NextOfKinStatusRelationGen.P_SIGN_IN_AGREEMENT, true);
	}

	@Override
	public void updateUsedInAgreement(final Model model, final Long id) {
		IEccNextOfKinDAO.INSTANCE.get().updateNextOfKinRelationProperty(getPersonId(model), id, NextOfKinStatusRelationGen.P_USED_IN_AGREEMENT, false);
	}

	@Override
    public void prepareDataSource(final Model model) {
		final Long personId = getPersonId(model);

		final Map<Long, NextOfKinStatusRelation> relations = new HashMap<>();
		/* default */ {
			final MQBuilder builder = new MQBuilder(PersonNextOfKinGen.ENTITY_CLASS, "e");
			builder.add(MQExpression.eq("e", PersonNextOfKinGen.L_PERSON+".id", personId));
			final List<PersonNextOfKin> rows = builder.getResultList(getSession());
			for (final PersonNextOfKin row: rows) {
				final NextOfKinStatusRelation rel = new NextOfKinStatusRelation();
				rel.setId(row.getId());
				rel.setPersonNextOfKin(row);
				relations.put(row.getId(), rel);
			}
		}

		/* real */ {
			final MQBuilder builder = new MQBuilder(NextOfKinStatusRelationGen.ENTITY_CLASS, "e");
			builder.add(MQExpression.eq("e", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN+"."+PersonNextOfKinGen.L_PERSON+".id", personId));
			final List<NextOfKinStatusRelation> rows = builder.getResultList(getSession());
			for (final NextOfKinStatusRelation row: rows) {
				relations.put(row.getPersonNextOfKin().getId(), row);
			}
		}

		final List<NextOfKinStatusRelation> relationsList = new ArrayList<>(relations.values());
		relationsList.sort(CommonCollator.comparing(rel -> rel.getPersonNextOfKin().getFullFio(), true));

		final DynamicListDataSource<NextOfKinStatusRelation> ds = model.getDataSource();
		ds.setTotalSize(relationsList.size());
		ds.setCountRow(relationsList.size());
		ds.createPage(relationsList);

	}

	@SuppressWarnings("unchecked")
	private Long getPersonId(final Model model) {
		Long personId = null;
		final Class entityClass = EntityRuntime.getMeta(model.getId()).getEntityClass();
		if (Person.class.isAssignableFrom(entityClass)) {
			personId = model.getId();
		} else if (PersonRole.class.isAssignableFrom(entityClass)) {
			personId = get(PersonRole.class, model.getId()).getPerson().getId();
		}
		return personId;
	}
}
