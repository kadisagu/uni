/* $Id:$ */
package ru.tandemservice.uniecc.base.ext.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;

import java.util.Arrays;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("uniecc", () -> Arrays.asList(
                "Договоров: " + IUniBaseDao.instance.get().getCount(UniscEduAgreement2Entrant.class, UniscEduAgreement2Entrant.entrant().enrollmentCampaign().educationYear().current().s(), Boolean.TRUE) + " (тек. уч. год), " + IUniBaseDao.instance.get().getCount(UniscEduAgreement2Entrant.class) + " (всего)"
            ))
            .create();
    }
}
