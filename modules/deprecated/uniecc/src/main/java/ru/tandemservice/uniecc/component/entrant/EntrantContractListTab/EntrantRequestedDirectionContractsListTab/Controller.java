/* $Id: Controller.java 8435 2009-06-11 07:08:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantRequestedDirectionContractsListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import ru.tandemservice.uniecc.entity.relations.gen.NextOfKinStatusRelationGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        this.prepareEntrantContractsDataSource(component);
        this.prepareNextOfKinDataSource(component);
    }


    private void prepareNextOfKinDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<NextOfKinStatusRelation> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareNextOfKinDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Родственник", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN + ".title").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN + ".passportTitle").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Телефон", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN + ".phones").setClickable(false).setOrderable(false));


        dataSource.addColumn(new ToggleColumn("Оплачивает", NextOfKinStatusRelationGen.P_USED_IN_AGREEMENT).setListener("onClickUsedInAgreement").setClickable(true).setOrderable(false).setPermissionKey("editEntrantNextOfKinUsed"));
        dataSource.addColumn(new ToggleColumn("Подписывает", NextOfKinStatusRelationGen.P_SIGN_IN_AGREEMENT).setListener("onClickSignInAgreement").setClickable(true).setOrderable(false).setPermissionKey("editEntrantNextOfKinSign"));
        model.setNextOfKinDataSource(dataSource);
    }


    private void prepareEntrantContractsDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<Model.Row> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareEntrantContractsDataSource(model);
        });


        dataSource.addColumn(new SimpleColumn("Направление приема (профиль)", "direction.title").setWidth(40).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", "direction." +  EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT + "." + OrgUnitGen.P_TITLE).setOrderable(false).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", "direction." + EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT + "." + OrgUnitGen.P_TERRITORIAL_TITLE).setOrderable(false).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Характеристики обучения", "direction." +  EducationOrgUnit.P_DEVELOP_COMBINATION_TITLE).setOrderable(false).setClickable(false).setWidth(1));

        dataSource.addColumn(new PublisherLinkColumn("№ дог.", "agreement." + UniscEduAgreementBaseGen.P_NUMBER, "relation").setOrderable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Год", "relation.agreement.config.educationYear.title").setOrderable(false).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Шифр", "relation.agreement.config.cipher").setOrderable(false).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Описание", "relation.agreement.config." + UniscEduOrgUnitGen.P_DESCRIPTION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условия", "relation.agreement.config." + UniscEduOrgUnitGen.P_CONDITIONS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Заказчик", "relation.agreement.customerType.title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Этап", "relation.agreement.firstStageTitle").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действ. c", "relation.agreement." + UniscEduAgreementBaseGen.P_FORMING_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Действ. до", "relation.agreement." + UniscEduAgreementBaseGen.P_DEADLINE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Изм.", "relation.agreement." + UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false).setWidth(1));


        final IEntityHandler addContractDisabler = e -> null != ((Model.Row) e).getRelation();
        dataSource.addColumn(new ActionColumn("Добавить", "add_item", "onClickAddEntrantContract").setPermissionKey("addEntrantContract_directionList").setDisableHandler(addContractDisabler));

        final IEntityHandler editContractDisabler = e -> null == ((Model.Row) e).getRelation();
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintEntrantContract", "Печать").setVerticalHeader(true).setPermissionKey("printEntrantContract_directionList").setDisableHandler(editContractDisabler));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEntrantContract").setPermissionKey("editEntrantContract_directionList").setDisableHandler(editContractDisabler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEntrantContract", "Удалить документ?").setPermissionKey("deleteEntrantContract_directionList").setDisableHandler(editContractDisabler));

        this.getModel(component).setRequestedDirectionsDataSource(dataSource);
    }

    public void onClickUsedInAgreement(final IBusinessComponent component)
    {
        getDao().updateUsedInAgreement(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickSignInAgreement(final IBusinessComponent component)
    {
        getDao().updateSignInAgreement(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickAddEntrantContract(final IBusinessComponent component)
    {
        final UniscEduOrgUnit uniscEduOrgUnit = (getDao().getUniscEduOrgUnit((Long) component.getListenerParameter()));
        final EducationOrgUnit educationOrgUnit = (null != uniscEduOrgUnit ? uniscEduOrgUnit.getEducationOrgUnit() : getDao().getEducationOrgUnit((Long) component.getListenerParameter()));

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
            new ParametersMap()
            .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getEntrant().getId())
            .add("defaultUniscEduOrgUnitId", (null == uniscEduOrgUnit ? null : uniscEduOrgUnit.getId()))
            .add("defaultEducationOrgUnitId", (null == educationOrgUnit ? null : educationOrgUnit.getId()))

        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //        ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
        //        new ParametersMap()
        //                .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getEntrant().getId())
        //                .add("defaultEducationOrgUnit", getDao().getEducationOrgUnit((Long) component.getListenerParameter()))
        //                .add("defaultUniscEduOrgUnit", getDao().getUniscEduOrgUnit((Long) component.getListenerParameter()))
        //));
    }

    public void onClickEditEntrantContract(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //        ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
        //        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        //));
    }

    public void onClickPrintEntrantContract(final IBusinessComponent component)
    {
        this.activateInRoot(component, new ComponentActivator(
            ru.tandemservice.uniecc.component.entrant.EntrantAgreementPrint.Model.class.getPackage().getName(),
            Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }


    public synchronized void onClickDeleteEntrantContract(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        try
        {
            this.getDao().deleteEntrantAgreement(model, (Long) component.getListenerParameter());
        } finally
        {
            this.getDao().prepare(model);
        }
    }


}
