package ru.tandemservice.uniecc.entity.agreements.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор с абитуриентом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreement2EntrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant";
    public static final String ENTITY_NAME = "uniscEduAgreement2Entrant";
    public static final int VERSION_HASH = -84786152;
    private static IEntityMeta ENTITY_META;

    public static final String L_AGREEMENT = "agreement";
    public static final String L_ENTRANT = "entrant";
    public static final String P_PAYMENT_EXISTS = "paymentExists";
    public static final String P_GROUP = "group";

    private UniscEduMainAgreement _agreement;     // Договор
    private Entrant _entrant;     // Абитуриент
    private boolean _paymentExists;     // Проплата по договору
    private String _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniscEduMainAgreement getAgreement()
    {
        return _agreement;
    }

    /**
     * @param agreement Договор. Свойство не может быть null и должно быть уникальным.
     */
    public void setAgreement(UniscEduMainAgreement agreement)
    {
        dirty(_agreement, agreement);
        _agreement = agreement;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Проплата по договору. Свойство не может быть null.
     */
    @NotNull
    public boolean isPaymentExists()
    {
        return _paymentExists;
    }

    /**
     * @param paymentExists Проплата по договору. Свойство не может быть null.
     */
    public void setPaymentExists(boolean paymentExists)
    {
        dirty(_paymentExists, paymentExists);
        _paymentExists = paymentExists;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreement2EntrantGen)
        {
            setAgreement(((UniscEduAgreement2Entrant)another).getAgreement());
            setEntrant(((UniscEduAgreement2Entrant)another).getEntrant());
            setPaymentExists(((UniscEduAgreement2Entrant)another).isPaymentExists());
            setGroup(((UniscEduAgreement2Entrant)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreement2EntrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreement2Entrant.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreement2Entrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "agreement":
                    return obj.getAgreement();
                case "entrant":
                    return obj.getEntrant();
                case "paymentExists":
                    return obj.isPaymentExists();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "agreement":
                    obj.setAgreement((UniscEduMainAgreement) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "paymentExists":
                    obj.setPaymentExists((Boolean) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "agreement":
                        return true;
                case "entrant":
                        return true;
                case "paymentExists":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "agreement":
                    return true;
                case "entrant":
                    return true;
                case "paymentExists":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "agreement":
                    return UniscEduMainAgreement.class;
                case "entrant":
                    return Entrant.class;
                case "paymentExists":
                    return Boolean.class;
                case "group":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreement2Entrant> _dslPath = new Path<UniscEduAgreement2Entrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreement2Entrant");
    }
            

    /**
     * @return Договор. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#getAgreement()
     */
    public static UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
    {
        return _dslPath.agreement();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Проплата по договору. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#isPaymentExists()
     */
    public static PropertyPath<Boolean> paymentExists()
    {
        return _dslPath.paymentExists();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends UniscEduAgreement2Entrant> extends EntityPath<E>
    {
        private UniscEduMainAgreement.Path<UniscEduMainAgreement> _agreement;
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<Boolean> _paymentExists;
        private PropertyPath<String> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#getAgreement()
     */
        public UniscEduMainAgreement.Path<UniscEduMainAgreement> agreement()
        {
            if(_agreement == null )
                _agreement = new UniscEduMainAgreement.Path<UniscEduMainAgreement>(L_AGREEMENT, this);
            return _agreement;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Проплата по договору. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#isPaymentExists()
     */
        public PropertyPath<Boolean> paymentExists()
        {
            if(_paymentExists == null )
                _paymentExists = new PropertyPath<Boolean>(UniscEduAgreement2EntrantGen.P_PAYMENT_EXISTS, this);
            return _paymentExists;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(UniscEduAgreement2EntrantGen.P_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreement2Entrant.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreement2Entrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
