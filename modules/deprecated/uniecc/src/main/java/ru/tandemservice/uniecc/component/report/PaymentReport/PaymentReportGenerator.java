package ru.tandemservice.uniecc.component.report.PaymentReport;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import java.util.*;

/**
 * @author oleyba
 * @since 18.08.2009
 */
public class PaymentReportGenerator extends ru.tandemservice.unisc.component.report.PaymentReport.PaymentReportGenerator
{
    protected Map<Long, UniscEduAgreement2Entrant> entrantMap = new HashMap<>();

    public PaymentReportGenerator(Session session, Long orgUnitId, MQBuilderFactory agreementBuilder, Date dateFrom, Date dateTo)
    {
        super(session, orgUnitId, agreementBuilder, dateFrom, dateTo);
    }

    @Override
    protected void initData()
    {
        fillEntrantMap();
        super.initData();
    }

    @Override
    protected void appendPayment(List<String[]> reportRowList, UniscEduAgreementPayFactRow payment, StringBuilder title)
    {
        Student student = studentMap.get(payment.getAgreement().getId());
        if (student != null)
        {
            super.appendPayment(reportRowList, payment, title);
            return;
        }
        UniscEduAgreement2Entrant rel = entrantMap.get(payment.getAgreement().getId());
        reportRowList.add(new String[]
            {
                    title.toString(),
                    rel.getEntrant().getPerson().getFullFio(),
                    payment.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getShortTitle(),
                    "а",
                    rel.getGroup(),
                    payment.getCostAsString()
            });
    }

    @Override
    protected Comparator<UniscEduAgreementPayFactRow> getPaymentComparator()
    {
        return Comparator.comparing(UniscEduAgreementPayFactRow::getDate)
            .thenComparing(CommonCollator.comparing(r -> {
                final Student s = studentMap.get(r.getAgreement().getId());
                return s == null ? entrantMap.get(r.getAgreement().getId()).getEntrant().getFullFio() : s.getFullFio();
            }, true));
    }

    private void fillEntrantMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "rel");
        builder.addLeftJoinFetch("rel", UniscEduAgreement2Entrant.L_ENTRANT, "entrant_fetch");
        builder.addLeftJoinFetch("entrant_fetch", Student.L_PERSON, "person_fetch");
        builder.addLeftJoinFetch("person_fetch", Person.L_IDENTITY_CARD, "idc_fetch");
        builder.addDomain("pay", UniscEduAgreementPayFactRow.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
        builder.add(MQExpression.in("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));

        for (UniscEduAgreement2Entrant rel : builder.<UniscEduAgreement2Entrant>getResultList(session))
            entrantMap.put(rel.getAgreement().getId(), rel);
    }
}
