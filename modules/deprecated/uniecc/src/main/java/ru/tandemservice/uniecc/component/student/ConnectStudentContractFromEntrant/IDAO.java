package ru.tandemservice.uniecc.component.student.ConnectStudentContractFromEntrant;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model>
{
    void save(Model model);

    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);
}
