/* $Id: IDAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantRequestedDirectionContractsListTab;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;


/**
 * @author vdanilov
 */
public interface IDAO extends IUniDao<Model>
{
	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	void prepareEntrantContractsDataSource(Model model);

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	void prepareNextOfKinDataSource(Model model);

	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	void updateUsedInAgreement(Model model, Long id);

	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	void updateSignInAgreement(Model model, Long id);


	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	void deleteEntrantAgreement(Model model, Long relationId);

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	EducationOrgUnit getEducationOrgUnit(Long id);

	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	UniscEduOrgUnit getUniscEduOrgUnit(Long id);



}
