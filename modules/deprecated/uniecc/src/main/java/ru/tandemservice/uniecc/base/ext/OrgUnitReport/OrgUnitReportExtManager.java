/* $Id: $ */
package ru.tandemservice.uniecc.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;

/**
 * @author Andrey Andreev
 * @since 13.04.2017
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .replace("orgUnitPaymentReport", new OrgUnitReportDefinition("(Старый) Отчет по оплатам", "orgUnitPaymentReport", "uniscOrgUnitReportBlock", "ru.tandemservice.uniecc.component.report.PaymentReport.PaymentReportList", "viewPaymentReportList"))
                .replace("orgUnitAgreementIncomeReport", new OrgUnitReportDefinition("(Старый) Поступление средств по договорам", "orgUnitAgreementIncomeReport", "uniscOrgUnitReportBlock", "ru.tandemservice.uniecc.component.report.AgreementIncomeReport.AgreementIncomeReportList", "viewAgreementIncomeReportList"))
                .create();
    }
}
