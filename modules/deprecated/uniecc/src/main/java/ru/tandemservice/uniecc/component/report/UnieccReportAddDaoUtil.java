package ru.tandemservice.uniecc.component.report;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniecc.component.report.PaymentReport.PaymentReportAdd.Model;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.*;

/**
 * @author oleyba
 * @since 14.08.2009
 */
public class UnieccReportAddDaoUtil
{

    private static abstract class SelectModel extends BaseSingleSelectModel implements IMultiSelectModel {

    }

    public static void initSelectModels(final UnieccReportAddModel model, final IUniBaseDao dao, final Session session)
    {
        model.setFormativeOrgUnitModel(createFormativeOrgUnitAutocompleteModel(model, session));
        model.setTerritorialOrgUnitModel(createTerritorialOrgUnitAutocompleteModel(model, session));
        model.setEducationLevelHighSchoolModel(createEducationLevelsHighSchoolModel(model, session));
        model.setDevelopFormModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setDevelopTechModel(EducationCatalogsManager.getDevelopTechSelectModel());
        model.setDevelopPeriodModel(EducationCatalogsManager.getDevelopPeriodSelectModel());

        fillEntrantGroupMaps(model, session);

        model.setGroupModel(new SelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                if (model.getSelection().getId() == Model.SELECT_ENTRANTS)
                    return getEntrantGroup(primaryKey, model, session);
                if (model.getSelection().getId() == Model.SELECT_STUDENTS)
                    return getStudentGroup(primaryKey, model, session);
                Object studentGroup = getStudentGroup(primaryKey, model, session);
                if (studentGroup != null) return studentGroup;
                return getEntrantGroup(primaryKey, model, session);
            }

            @Override
            public List getValues(Set primaryKeys)
            {
                return new ArrayList();
            }

            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                List<UnieccReportAddModel.GroupWrapper> groupList = new ArrayList<>();
                if (model.getSelection().getId() == Model.SELECT_ENTRANTS || model.getSelection().getId() == Model.SELECT_ALL)
                    groupList.addAll(getEntrantGroups(filter, model, session));
                if (model.getSelection().getId() == Model.SELECT_STUDENTS || model.getSelection().getId() == Model.SELECT_ALL)
                    groupList.addAll(getStudentGroups(filter, model, session));
                Collections.sort(groupList, ITitled.TITLED_COMPARATOR);
                return new ListResult<>(groupList.subList(0, Math.min(groupList.size(), 20)), groupList.size());
            }
        });

        model.setCourseModel(new SelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if (model.getSelection().getId() == Model.SELECT_ENTRANTS)
                    return null;
                return dao.get(Course.class, (Long) primaryKey);
            }

            @Override
            public List getValues(Set primaryKeys)
            {
                if (model.getSelection().getId() == Model.SELECT_ENTRANTS)
                    return new ArrayList();
                MQBuilder builder = new MQBuilder(Course.ENTITY_CLASS, "c");
                builder.add(MQExpression.in("c", "id", primaryKeys));
                return builder.getResultList(DataAccessServices.dao().getComponentSession());
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (model.getSelection().getId() == Model.SELECT_ENTRANTS)
                    return ListResult.getEmpty();
                return new ListResult<>(DevelopGridDAO.getCourseList());
            }
        });
    }


    public static MQBuilderFactory getAgreementBuilder(final UnieccReportAddModel model)
    {
        return new MQBuilderFactory("a")
        {
            @Override
            public MQBuilder createBuilder()
            {
                MQBuilder main = new MQBuilder(UniscEduMainAgreement.ENTITY_CLASS, "a");
                main.addJoin("a", UniscEduMainAgreement.L_CONFIG, "config");
                main.addJoin("config", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "ou");
                appendEduOuConditions(main, model, "ou");
                // договоры со студентами, подходящие по параметрам
                main.addDomain("rel_st", UniscEduAgreement2Student.ENTITY_CLASS);
                if (model.isCourseActive())
                    main.add(MQExpression.eq("rel_st", UniscEduAgreement2Student.L_STUDENT + "." + Student.L_COURSE, model.getCourse()));
                if (model.isGroupActive() && model.getGroup().isStudent())
                    main.add(MQExpression.eq("rel_st", UniscEduAgreement2Student.L_STUDENT + "." + Student.L_GROUP + ".id", model.getGroup().getId()));
                // договоры с абитуриентами, подходящие по параметрам
                main.addDomain("rel_en", UniscEduAgreement2Entrant.ENTITY_CLASS);
                if (model.isGroupActive() && !model.getGroup().isStudent())
                    main.add(MQExpression.eq("rel_en", UniscEduAgreement2Entrant.P_GROUP, model.getGroup().getTitle()));
                // договор может войти в отчет как договор с абитуриентом, только если нет связи со студентом
                MQBuilder sub = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel_st_for_en");
                sub.add(MQExpression.eqProperty("rel_en", UniscEduAgreement2Student.L_AGREEMENT + ".id", "rel_st_for_en", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
                main.add(MQExpression.notExists(sub));
                boolean selectEntrants = (model.getSelection().getId() == Model.SELECT_ALL || model.getSelection().getId() == Model.SELECT_ENTRANTS) && !model.isCourseActive();
                boolean selectStudents = (model.getSelection().getId() == Model.SELECT_ALL || model.getSelection().getId() == Model.SELECT_STUDENTS);
                if (model.isGroupActive())
                {
                    selectEntrants = selectEntrants && !model.getGroup().isStudent();
                    selectStudents = selectStudents && model.getGroup().isStudent();
                }
                if (selectEntrants && selectStudents)
                    main.add(MQExpression.or(MQExpression.eqProperty("a", "id", "rel_st", UniscEduAgreement2Student.L_AGREEMENT + ".id"), MQExpression.eqProperty("a", "id", "rel_en", UniscEduAgreement2Entrant.L_AGREEMENT + ".id")));
                else if (selectStudents)
                    main.add(MQExpression.eqProperty("a", "id", "rel_st", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
                else if (selectEntrants)
                    main.add(MQExpression.eqProperty("a", "id", "rel_en", UniscEduAgreement2Entrant.L_AGREEMENT + ".id"));
                else
                    main.add(MQExpression.eq("a", "id", null));
                return main;
            }
        };
    }

    private static void appendEduOuConditions(MQBuilder builder, UnieccReportAddModel model, String alias)
    {
        if (model.getOrgUnitId() != null)
        {
            builder.addLeftJoin(alias, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr_ou");
            builder.add(MQExpression.or(
                    MQExpression.eq(alias, EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                    MQExpression.eq("terr_ou", "id", model.getOrgUnitId())
            ));
        }
        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelHighSchoolList()));
        if (model.isDevelopFormActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in(alias, EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodList()));
    }

    private static ISelectModel createFormativeOrgUnitAutocompleteModel(final UnieccReportAddModel model, final Session session)
    {
        return new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model);
                return new ListResult<>(getBuilder(OrgUnit.ENTITY_CLASS, ids, filter).<OrgUnit>getResultList(DataAccessServices.dao().getComponentSession()));
            }
        };
    }

    private static ISelectModel createTerritorialOrgUnitAutocompleteModel(final UnieccReportAddModel model, final Session session)
    {
        return new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_FULL_TITLE)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model);
                filter(ids, model.getSelectedFormativeOrgUnitList(), EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
                return new ListResult<>(getBuilder(OrgUnit.ENTITY_CLASS, ids, filter).<OrgUnit>getResultList(DataAccessServices.dao().getComponentSession()));
            }
        };
    }

    private static ISelectModel createEducationLevelsHighSchoolModel(final UnieccReportAddModel model, final Session session)
    {
        return new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            public ListResult<EducationLevelsHighSchool> findValues(String filter)
            {
                MQBuilder ids = getIdsBuilder(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model);
                filter(ids, model.getSelectedFormativeOrgUnitList(), EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
                filter(ids, model.getSelectedTerritorialOrgUnitList(), EducationOrgUnit.L_TERRITORIAL_ORG_UNIT);
                return new ListResult<>(getBuilder(EducationLevelsHighSchool.ENTITY_CLASS, ids, filter).<EducationLevelsHighSchool>getResultList(DataAccessServices.dao().getComponentSession()));
            }
        };
    }

    private static final String EDU_OU_ALIAS = "e";

    private static MQBuilder getIdsBuilder(String selectProperty, UnieccReportAddModel model)
    {
        MQBuilder ids = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, EDU_OU_ALIAS, new String[]{selectProperty + ".id"});
        ids.addDomain("a", UniscEduMainAgreement.ENTITY_CLASS);
        ids.add(MQExpression.eqProperty("a", UniscEduMainAgreement.L_CONFIG + "." + UniscEduOrgUnit.L_EDUCATION_ORG_UNIT + ".id", EDU_OU_ALIAS, "id"));
        if (null == model.getOrgUnitId())
            return ids;
        ids.addLeftJoin(EDU_OU_ALIAS, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "terr_ou");
        ids.add(MQExpression.or(
                MQExpression.eq(EDU_OU_ALIAS, EducationOrgUnit.L_FORMATIVE_ORG_UNIT + ".id", model.getOrgUnitId()),
                MQExpression.eq("terr_ou", "id", model.getOrgUnitId())
        ));
        return ids;
    }

    private static void filter(MQBuilder builder, List selected, String property)
    {
        if (null == selected)
            return;
        if (selected.isEmpty())
            builder.add(MQExpression.isNull(EDU_OU_ALIAS, property));
        else
            builder.add(MQExpression.in(EDU_OU_ALIAS, property, selected));
    }

    private static MQBuilder getBuilder(String entityClass, MQBuilder ids, String filter)
    {
        MQBuilder builder = new MQBuilder(entityClass, "s");
        builder.addOrder("s", "title");
        if (StringUtils.isNotBlank(filter))
            builder.add(MQExpression.like("s", "title", "%" + filter));
        builder.add(MQExpression.in("s", "id", ids));
        return builder;
    }

    // методы для работы с группами студента и абитуриента

    private static Collection<UnieccReportAddModel.GroupWrapper> getEntrantGroups(String filter, UnieccReportAddModel model, Session session)
    {
        ArrayList<UnieccReportAddModel.GroupWrapper> result = new ArrayList<>();
        for (String title : getEntrantGroupTitles(model, filter, session))
            result.add(model.getEntrantGroupMapByTitle().get(title));
        return result;
    }

    private static Object getEntrantGroup(Object primaryKey, UnieccReportAddModel model, Session session)
    {
        for (UnieccReportAddModel.GroupWrapper group : getEntrantGroups(null, model, session))
            if (group.getId().equals(primaryKey))
                return group;
        return null;
    }

    private static void fillEntrantGroupMaps(UnieccReportAddModel model, Session session)
    {
        long idCounter = 0L;
        for (String title : getEntrantGroupTitles(model, null, session))
            model.getEntrantGroupMapByTitle().put(title, new UnieccReportAddModel.GroupWrapper(idCounter++, title, false));
    }

    private static Collection<UnieccReportAddModel.GroupWrapper> getStudentGroups(String filter, UnieccReportAddModel model, Session session)
    {
        MQBuilderFactory factory = getStudentGroupBuilder(model);
        MQBuilder builder = factory.createBuilder();
        if (StringUtils.isNotBlank(filter))
            builder.add(MQExpression.like(factory.getAlias(), factory.getTitleProperty(), "%" + filter));
        Set<UnieccReportAddModel.GroupWrapper> result = new HashSet<>();
        for (Group group : builder.<Group>getResultList(session))
            result.add(new UnieccReportAddModel.GroupWrapper(group.getId(), group.getTitle(), true));
        return result;
    }

    private static UnieccReportAddModel.GroupWrapper getStudentGroup(Object primaryKey, UnieccReportAddModel model, Session session)
    {
        MQBuilderFactory factory = getStudentGroupBuilder(model);
        MQBuilder builder = factory.createBuilder();
        builder.add(MQExpression.eq(factory.getAlias(), factory.getIdProperty(), primaryKey));
        List<Object> list = builder.getResultList(session);
        Group group = list.isEmpty() ? null : (Group) list.get(0);
        return group == null ? null : new UnieccReportAddModel.GroupWrapper(group.getId(), group.getTitle(), true);
    }

    private static MQBuilderFactory getStudentGroupBuilder(UnieccReportAddModel model)
    {
        final MQBuilder groupBuilder = new MQBuilder(Group.ENTITY_CLASS, "g");
        groupBuilder.addOrder("g", Group.P_TITLE);
        groupBuilder.addDomain("r", UniscEduAgreement2Student.ENTITY_CLASS);
        groupBuilder.add(MQExpression.eqProperty("r", UniscEduAgreement2Student.L_STUDENT + "." + Student.L_GROUP + ".id", "g", "id"));
        groupBuilder.addJoin("g", Group.L_EDUCATION_ORG_UNIT, "ou");
        appendEduOuConditions(groupBuilder, model, "ou");
        if (model.isCourseActive())
            groupBuilder.add(MQExpression.eq("g", Group.L_COURSE, model.getCourse()));
        return MQBuilderFactory.create(groupBuilder, "g");
    }

    private static Set<String> getEntrantGroupTitles(UnieccReportAddModel model, String filter, Session session)
    {
        final MQBuilder groupBuilder = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "rel", new String[]{UniscEduAgreement2Entrant.P_GROUP});
        groupBuilder.addJoin("rel", UniscEduAgreement2Entrant.L_AGREEMENT, "a");
        groupBuilder.addJoin("a", UniscEduMainAgreement.L_CONFIG, "config");
        groupBuilder.addJoin("config", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "ou");
        appendEduOuConditions(groupBuilder, model, "ou");
        groupBuilder.add(MQExpression.isNotNull("rel", UniscEduAgreement2Entrant.P_GROUP));
        if (StringUtils.isNotBlank(filter))
            groupBuilder.add(MQExpression.like("rel", UniscEduAgreement2Entrant.P_GROUP, "%" + filter));
        return new HashSet<>(groupBuilder.<String>getResultList(session));
    }
}