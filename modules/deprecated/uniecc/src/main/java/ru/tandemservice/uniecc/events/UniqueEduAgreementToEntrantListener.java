/**
 *$Id:$
 */
package ru.tandemservice.uniecc.events;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

import java.util.*;

/**
 * Листенер для обработки доб.\ред. договора Абитуриента.
 * Проверят, если это нужно, что нельзя создавать два Договора для Абитуриента с одинаковыми НПП.
 *
 * @author Alexander Shaburov
 * @since 01.08.12
 */
public class UniqueEduAgreementToEntrantListener implements IDSetEventListener
{
    public void init()
    {
//        DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, UniscEduAgreement2Entrant.class, this);
//        DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, UniscEduAgreementBase.class, this);
//        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, UniscEduAgreement2Entrant.class, this);
//        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, UniscEduAgreementBase.class, this);
    }

    @Override
    public void onEvent(DSetEvent event)
    {
        if (!isUniqueEduAgreement())
            return;

        Class entityClass = event.getMultitude().getEntityMeta().getEntityClass();

        // нам нужен список Договоров, способ его добычи зависит от того для какого объекта сработал листенер
        List<UniscEduAgreement2Entrant> agreementList = new ArrayList<>();
        if (UniscEduAgreement2Entrant.class.isAssignableFrom(entityClass))
        {
            agreementList = event.getMultitude().isSingular() ?
            Collections.singletonList((UniscEduAgreement2Entrant) event.getMultitude().getSingularEntity()) :
                new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "e").column("e").createStatement(event.getContext()).<UniscEduAgreement2Entrant>list();
        }
        else if (UniscEduAgreementBase.class.isAssignableFrom(entityClass))
        {
            List<UniscEduAgreementBase> entityList = event.getMultitude().isSingular() ?
            Collections.singletonList((UniscEduAgreementBase) event.getMultitude().getSingularEntity()) :
                new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "e").column("e").createStatement(event.getContext()).<UniscEduAgreementBase>list();

            DQLSelectBuilder agreementBuilder = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Entrant.class, "b").column("b")
                    .where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Entrant.agreement().fromAlias("b")), entityList));

            agreementList.addAll(DataAccessServices.dao().<UniscEduAgreement2Entrant>getList(agreementBuilder));
        }

        if (agreementList.isEmpty())
            return;

        // если вдруг действие массовое, то нужно сгруппируем НПП по "их" Абитуриентам
        Map<Entrant, Set<EducationOrgUnit>> entrantEducationOrgUnitSetMap = new HashMap<>();
        for (UniscEduAgreement2Entrant agreement : agreementList)
        {
            Set<EducationOrgUnit> educationOrgUnitSet = entrantEducationOrgUnitSetMap.get(agreement.getEntrant());
            if (educationOrgUnitSet == null)
                entrantEducationOrgUnitSetMap.put(agreement.getEntrant(), educationOrgUnitSet = new HashSet<>());
            educationOrgUnitSet.add(agreement.getAgreement().getConfig().getEducationOrgUnit());
        }

        for (Map.Entry<Entrant, Set<EducationOrgUnit>> entry : entrantEducationOrgUnitSetMap.entrySet())
        {
            // узнаем количество договоров у данного Абитуриента с данными НПП, исключаем Договора, которые попали в обработку листенера
            DQLSelectBuilder resultAgreementBuilder = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Entrant.class, "b").column("b.id")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(UniscEduAgreement2Entrant.entrant().fromAlias("b")), entry.getKey()))
                    .where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Entrant.agreement().config().educationOrgUnit().fromAlias("b")), entry.getValue()))
                    .where(DQLExpressions.notIn(DQLExpressions.property(UniscEduAgreement2Entrant.id().fromAlias("b")), CommonBaseEntityUtil.getIdSet(agreementList)));

            // таких договоров быть не должно, если есть - ошибка
            if (ISharedBaseDao.instance.get().existsEntity(resultAgreementBuilder.buildQuery()))
                throw new ApplicationException("У абитуриента уже есть договор на данное направление подготовки. Нельзя создать более одного договора для абитуриента на одно и тоже направление подготовки.");
        }
    }

    private boolean isUniqueEduAgreement()
    {
        String property = ApplicationRuntime.getProperty("unisc.agreements.uniqueEduAgreementToEntrant");
        return property == null || property.equals("1") || property.equals("true");
    }
}
