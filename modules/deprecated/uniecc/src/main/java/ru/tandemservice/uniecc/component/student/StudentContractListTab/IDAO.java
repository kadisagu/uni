package ru.tandemservice.uniecc.component.student.StudentContractListTab;

import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model>
{
    void doDisconnect(Model model);
}
