/* $Id: $ */
package ru.tandemservice.uniecc.component.entrant.EntrantList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantList.DAO
{
    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantList.Model model)
    {
        super.prepare(model);

        ((Model) model).setPaymentExistsModel(TwinComboDataSourceHandler.getCustomSelectModel("Есть", "Нет"));
    }

    protected MQBuilder getEntrantListBuilder(ru.tandemservice.uniec.component.entrant.EntrantList.Model model)
    {
        MQBuilder builder = super.getEntrantListBuilder(model);

        DataWrapper paymentExistsWrapper = ((Model) model).getPaymentExists();
        if (paymentExistsWrapper != null)
        {
            Boolean paymentExists = TwinComboDataSourceHandler.getSelectedValue(paymentExistsWrapper);

            MQBuilder paymentExistsBuilder = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "ep_ph")
                    .add(MQExpression.eqProperty("ep_ph", UniscEduAgreement2Entrant.entrant(), "entrant", Entrant.id()))
                    .add(MQExpression.eq("ep_ph", UniscEduAgreement2Entrant.paymentExists(), paymentExists));

            builder.add(MQExpression.exists(paymentExistsBuilder));
        }

        return builder;
    }
}
