/* $Id: Controller.java 8435 2009-06-11 07:08:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantAllContractsListTab;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        this.prepareEntrantContractsDataSource(component);
    }

    private void prepareEntrantContractsDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<UniscEduAgreement2Entrant> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshEntrantContractsDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("№ договора", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_NUMBER).setOrderable(false).setClickable(true));
        dataSource.addColumn(new SimpleColumn("Шифр", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.P_CIPHER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + ".title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.P_DESCRIPTION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условия", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.P_CONDITIONS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип заказчика", new String[]{UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CUSTOMER_TYPE, ICatalogItem.CATALOG_ITEM_TITLE}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действует с", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_FORMING_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действует до", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_DEADLINE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Изменен", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));

        dataSource.addColumn(new ToggleColumn("Проверен", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE).setListener("onClickCheckBySignature").setPermissionKey("checkedBySignature_entrant_list"));
        dataSource.addColumn(new ToggleColumn("Оплачено", UniscEduAgreement2EntrantGen.P_PAYMENT_EXISTS).setListener("onClickPaymentExists").setPermissionKey("entrantContractsPaymentExists_list"));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintEntrantContract", "Печать").setPermissionKey("printEntrantContract"));

        final IEntityHandler disabler = entity -> {
            if (model.getEntrant().isArchival())
                return true;

            final UniscEduAgreement2Entrant relation = (UniscEduAgreement2Entrant) entity;
            return relation.getAgreement().isCheckedBySignature() || !IUniscEduAgreementDAO.INSTANCE.get().isMutable(relation.getAgreement());
        };

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEntrantContract").setPermissionKey("editEntrantContract").setDisableHandler(disabler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEntrantContract", "Удалить документ?").setPermissionKey("deleteEntrantContract").setDisableHandler(disabler));
        this.getModel(component).setEntrantContractsDataSource(dataSource);
    }

    public void onClickCheckBySignature(final IBusinessComponent component)
    {
        this.getDao().updateCheckBySignature(this.getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickPaymentExists(final IBusinessComponent component)
    {
        getDao().updatePaymentExists(getModel(component), component.getListenerParameter());
    }

    public void onClickAddEntrantContract(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //        ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
        //        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
        //));
    }

    public void onClickEditEntrantContract(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
        //component.createDefaultChildRegion(new ComponentActivator(
        //        ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
        //        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        //));
    }

    public void onClickPrintEntrantContract(final IBusinessComponent component)
    {
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantAgreementPrint.Model.class.getPackage().getName(),
                Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }

    public synchronized void onClickDeleteEntrantContract(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        try
        {
            this.getDao().deleteEntrantAgreement(model, (Long) component.getListenerParameter());
        } finally
        {
            this.getDao().prepare(model);
        }
    }

}
