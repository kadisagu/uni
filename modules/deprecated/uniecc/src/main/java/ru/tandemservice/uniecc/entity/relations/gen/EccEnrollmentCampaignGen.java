package ru.tandemservice.uniecc.entity.relations.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка зачисления приемной кампании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EccEnrollmentCampaignGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign";
    public static final String ENTITY_NAME = "eccEnrollmentCampaign";
    public static final int VERSION_HASH = -1374811249;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_GENERAL_REQ4_PRE_ENROLL = "generalReq4PreEnroll";
    public static final String P_GENERAL_REQ4_ORDER = "generalReq4Order";
    public static final String P_GENERAL_REQ_PAYMENT = "generalReqPayment";
    public static final String P_TARGET_REQ4_PRE_ENROLL = "targetReq4PreEnroll";
    public static final String P_TARGET_REQ4_ORDER = "targetReq4Order";
    public static final String P_TARGET_REQ_PAYMENT = "targetReqPayment";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private boolean _generalReq4PreEnroll;     // Не ЦП: требуется договор при предзачислении
    private boolean _generalReq4Order;     // Не ЦП: требуется договор при формировании приказа
    private boolean _generalReqPayment;     // Не ЦП: требуется оплата по договору
    private boolean _targetReq4PreEnroll;     // ЦП: требуется договор при предзачислении
    private boolean _targetReq4Order;     // ЦП: требуется договор при формировании приказа
    private boolean _targetReqPayment;     // ЦП: требуется оплата по договору

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Не ЦП: требуется договор при предзачислении. Свойство не может быть null.
     */
    @NotNull
    public boolean isGeneralReq4PreEnroll()
    {
        return _generalReq4PreEnroll;
    }

    /**
     * @param generalReq4PreEnroll Не ЦП: требуется договор при предзачислении. Свойство не может быть null.
     */
    public void setGeneralReq4PreEnroll(boolean generalReq4PreEnroll)
    {
        dirty(_generalReq4PreEnroll, generalReq4PreEnroll);
        _generalReq4PreEnroll = generalReq4PreEnroll;
    }

    /**
     * @return Не ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     */
    @NotNull
    public boolean isGeneralReq4Order()
    {
        return _generalReq4Order;
    }

    /**
     * @param generalReq4Order Не ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     */
    public void setGeneralReq4Order(boolean generalReq4Order)
    {
        dirty(_generalReq4Order, generalReq4Order);
        _generalReq4Order = generalReq4Order;
    }

    /**
     * @return Не ЦП: требуется оплата по договору. Свойство не может быть null.
     */
    @NotNull
    public boolean isGeneralReqPayment()
    {
        return _generalReqPayment;
    }

    /**
     * @param generalReqPayment Не ЦП: требуется оплата по договору. Свойство не может быть null.
     */
    public void setGeneralReqPayment(boolean generalReqPayment)
    {
        dirty(_generalReqPayment, generalReqPayment);
        _generalReqPayment = generalReqPayment;
    }

    /**
     * @return ЦП: требуется договор при предзачислении. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetReq4PreEnroll()
    {
        return _targetReq4PreEnroll;
    }

    /**
     * @param targetReq4PreEnroll ЦП: требуется договор при предзачислении. Свойство не может быть null.
     */
    public void setTargetReq4PreEnroll(boolean targetReq4PreEnroll)
    {
        dirty(_targetReq4PreEnroll, targetReq4PreEnroll);
        _targetReq4PreEnroll = targetReq4PreEnroll;
    }

    /**
     * @return ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetReq4Order()
    {
        return _targetReq4Order;
    }

    /**
     * @param targetReq4Order ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     */
    public void setTargetReq4Order(boolean targetReq4Order)
    {
        dirty(_targetReq4Order, targetReq4Order);
        _targetReq4Order = targetReq4Order;
    }

    /**
     * @return ЦП: требуется оплата по договору. Свойство не может быть null.
     */
    @NotNull
    public boolean isTargetReqPayment()
    {
        return _targetReqPayment;
    }

    /**
     * @param targetReqPayment ЦП: требуется оплата по договору. Свойство не может быть null.
     */
    public void setTargetReqPayment(boolean targetReqPayment)
    {
        dirty(_targetReqPayment, targetReqPayment);
        _targetReqPayment = targetReqPayment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EccEnrollmentCampaignGen)
        {
            setEnrollmentCampaign(((EccEnrollmentCampaign)another).getEnrollmentCampaign());
            setGeneralReq4PreEnroll(((EccEnrollmentCampaign)another).isGeneralReq4PreEnroll());
            setGeneralReq4Order(((EccEnrollmentCampaign)another).isGeneralReq4Order());
            setGeneralReqPayment(((EccEnrollmentCampaign)another).isGeneralReqPayment());
            setTargetReq4PreEnroll(((EccEnrollmentCampaign)another).isTargetReq4PreEnroll());
            setTargetReq4Order(((EccEnrollmentCampaign)another).isTargetReq4Order());
            setTargetReqPayment(((EccEnrollmentCampaign)another).isTargetReqPayment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EccEnrollmentCampaignGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EccEnrollmentCampaign.class;
        }

        public T newInstance()
        {
            return (T) new EccEnrollmentCampaign();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "generalReq4PreEnroll":
                    return obj.isGeneralReq4PreEnroll();
                case "generalReq4Order":
                    return obj.isGeneralReq4Order();
                case "generalReqPayment":
                    return obj.isGeneralReqPayment();
                case "targetReq4PreEnroll":
                    return obj.isTargetReq4PreEnroll();
                case "targetReq4Order":
                    return obj.isTargetReq4Order();
                case "targetReqPayment":
                    return obj.isTargetReqPayment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "generalReq4PreEnroll":
                    obj.setGeneralReq4PreEnroll((Boolean) value);
                    return;
                case "generalReq4Order":
                    obj.setGeneralReq4Order((Boolean) value);
                    return;
                case "generalReqPayment":
                    obj.setGeneralReqPayment((Boolean) value);
                    return;
                case "targetReq4PreEnroll":
                    obj.setTargetReq4PreEnroll((Boolean) value);
                    return;
                case "targetReq4Order":
                    obj.setTargetReq4Order((Boolean) value);
                    return;
                case "targetReqPayment":
                    obj.setTargetReqPayment((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "generalReq4PreEnroll":
                        return true;
                case "generalReq4Order":
                        return true;
                case "generalReqPayment":
                        return true;
                case "targetReq4PreEnroll":
                        return true;
                case "targetReq4Order":
                        return true;
                case "targetReqPayment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "generalReq4PreEnroll":
                    return true;
                case "generalReq4Order":
                    return true;
                case "generalReqPayment":
                    return true;
                case "targetReq4PreEnroll":
                    return true;
                case "targetReq4Order":
                    return true;
                case "targetReqPayment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "generalReq4PreEnroll":
                    return Boolean.class;
                case "generalReq4Order":
                    return Boolean.class;
                case "generalReqPayment":
                    return Boolean.class;
                case "targetReq4PreEnroll":
                    return Boolean.class;
                case "targetReq4Order":
                    return Boolean.class;
                case "targetReqPayment":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EccEnrollmentCampaign> _dslPath = new Path<EccEnrollmentCampaign>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EccEnrollmentCampaign");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Не ЦП: требуется договор при предзачислении. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isGeneralReq4PreEnroll()
     */
    public static PropertyPath<Boolean> generalReq4PreEnroll()
    {
        return _dslPath.generalReq4PreEnroll();
    }

    /**
     * @return Не ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isGeneralReq4Order()
     */
    public static PropertyPath<Boolean> generalReq4Order()
    {
        return _dslPath.generalReq4Order();
    }

    /**
     * @return Не ЦП: требуется оплата по договору. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isGeneralReqPayment()
     */
    public static PropertyPath<Boolean> generalReqPayment()
    {
        return _dslPath.generalReqPayment();
    }

    /**
     * @return ЦП: требуется договор при предзачислении. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isTargetReq4PreEnroll()
     */
    public static PropertyPath<Boolean> targetReq4PreEnroll()
    {
        return _dslPath.targetReq4PreEnroll();
    }

    /**
     * @return ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isTargetReq4Order()
     */
    public static PropertyPath<Boolean> targetReq4Order()
    {
        return _dslPath.targetReq4Order();
    }

    /**
     * @return ЦП: требуется оплата по договору. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isTargetReqPayment()
     */
    public static PropertyPath<Boolean> targetReqPayment()
    {
        return _dslPath.targetReqPayment();
    }

    public static class Path<E extends EccEnrollmentCampaign> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Boolean> _generalReq4PreEnroll;
        private PropertyPath<Boolean> _generalReq4Order;
        private PropertyPath<Boolean> _generalReqPayment;
        private PropertyPath<Boolean> _targetReq4PreEnroll;
        private PropertyPath<Boolean> _targetReq4Order;
        private PropertyPath<Boolean> _targetReqPayment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Не ЦП: требуется договор при предзачислении. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isGeneralReq4PreEnroll()
     */
        public PropertyPath<Boolean> generalReq4PreEnroll()
        {
            if(_generalReq4PreEnroll == null )
                _generalReq4PreEnroll = new PropertyPath<Boolean>(EccEnrollmentCampaignGen.P_GENERAL_REQ4_PRE_ENROLL, this);
            return _generalReq4PreEnroll;
        }

    /**
     * @return Не ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isGeneralReq4Order()
     */
        public PropertyPath<Boolean> generalReq4Order()
        {
            if(_generalReq4Order == null )
                _generalReq4Order = new PropertyPath<Boolean>(EccEnrollmentCampaignGen.P_GENERAL_REQ4_ORDER, this);
            return _generalReq4Order;
        }

    /**
     * @return Не ЦП: требуется оплата по договору. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isGeneralReqPayment()
     */
        public PropertyPath<Boolean> generalReqPayment()
        {
            if(_generalReqPayment == null )
                _generalReqPayment = new PropertyPath<Boolean>(EccEnrollmentCampaignGen.P_GENERAL_REQ_PAYMENT, this);
            return _generalReqPayment;
        }

    /**
     * @return ЦП: требуется договор при предзачислении. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isTargetReq4PreEnroll()
     */
        public PropertyPath<Boolean> targetReq4PreEnroll()
        {
            if(_targetReq4PreEnroll == null )
                _targetReq4PreEnroll = new PropertyPath<Boolean>(EccEnrollmentCampaignGen.P_TARGET_REQ4_PRE_ENROLL, this);
            return _targetReq4PreEnroll;
        }

    /**
     * @return ЦП: требуется договор при формировании приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isTargetReq4Order()
     */
        public PropertyPath<Boolean> targetReq4Order()
        {
            if(_targetReq4Order == null )
                _targetReq4Order = new PropertyPath<Boolean>(EccEnrollmentCampaignGen.P_TARGET_REQ4_ORDER, this);
            return _targetReq4Order;
        }

    /**
     * @return ЦП: требуется оплата по договору. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign#isTargetReqPayment()
     */
        public PropertyPath<Boolean> targetReqPayment()
        {
            if(_targetReqPayment == null )
                _targetReqPayment = new PropertyPath<Boolean>(EccEnrollmentCampaignGen.P_TARGET_REQ_PAYMENT, this);
            return _targetReqPayment;
        }

        public Class getEntityClass()
        {
            return EccEnrollmentCampaign.class;
        }

        public String getEntityName()
        {
            return "eccEnrollmentCampaign";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
