/* $Id: DAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantRequestedDirectionContractsListTab;

import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.gen.PersonNextOfKinGen;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniecc.dao.IEccNextOfKinDAO;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import ru.tandemservice.uniecc.entity.relations.gen.NextOfKinStatusRelationGen;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void updateSignInAgreement(final Model model, final Long id) {
        final Long personId = model.getEntrant().getPerson().getId();
        IEccNextOfKinDAO.INSTANCE.get().updateNextOfKinRelationProperty(personId, id, NextOfKinStatusRelationGen.P_SIGN_IN_AGREEMENT, true);
    }

    @Override
    public void updateUsedInAgreement(final Model model, final Long id) {
        final Long personId = model.getEntrant().getPerson().getId();
        IEccNextOfKinDAO.INSTANCE.get().updateNextOfKinRelationProperty(personId, id, NextOfKinStatusRelationGen.P_USED_IN_AGREEMENT, false);
    }

    @Override
    public void prepare(final Model model) {
        model.setEntrant(get(Entrant.class, model.getEntrant().getId()));
    }

    @Override
    public void prepareNextOfKinDataSource(final Model model) {
        final Long personId = model.getEntrant().getPerson().getId();
        final Map<Long, NextOfKinStatusRelation> relations = new HashMap<>();
        /* default */ {
            final MQBuilder builder = new MQBuilder(PersonNextOfKinGen.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", PersonNextOfKinGen.L_PERSON+".id", personId));
            final List<PersonNextOfKin> rows = builder.getResultList(getSession());
            for (final PersonNextOfKin row: rows) {
                final NextOfKinStatusRelation rel = new NextOfKinStatusRelation();
                rel.setId(row.getId());
                rel.setPersonNextOfKin(row);
                relations.put(row.getId(), rel);
            }
        }

        /* real */ {
            final MQBuilder builder = new MQBuilder(NextOfKinStatusRelationGen.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN+"."+PersonNextOfKinGen.L_PERSON+".id", personId));
            final List<NextOfKinStatusRelation> rows = builder.getResultList(getSession());
            for (final NextOfKinStatusRelation row: rows) {
                relations.put(row.getPersonNextOfKin().getId(), row);
            }
        }

        final List<NextOfKinStatusRelation> relationsList = new ArrayList<>(relations.values());
        relationsList.sort(CommonCollator.comparing(rel -> rel.getPersonNextOfKin().getFullFio(), true));

        final DynamicListDataSource<NextOfKinStatusRelation> ds = model.getNextOfKinDataSource();
        ds.setTotalSize(relationsList.size());
        ds.setCountRow(relationsList.size());
        ds.createPage(relationsList);

    }

    @Override
    public void prepareEntrantContractsDataSource(final Model model) {

        final Entrant entrant = model.getEntrant();
        final MQBuilder directions = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");

        // выбранные направления приема
        final MQBuilder enrollmentDirections = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d", new String[] { RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().id().s() });
        enrollmentDirections.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().s(), entrant));

        // профили (выбранные профили выбранных направлений)
        final MQBuilder profileDirections = new MQBuilder(PriorityProfileEduOu.ENTITY_CLASS, "p", new String[] { PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().id().s() });
        profileDirections.add(MQExpression.eq("p", PriorityProfileEduOu.requestedEnrollmentDirection().entrantRequest().entrant().s(), entrant));

        directions.add(MQExpression.or(
            MQExpression.in("eou", "id", enrollmentDirections),
            MQExpression.in("eou", "id", profileDirections)
        ));

        directions.addOrder("eou", EducationOrgUnit.formativeOrgUnit().title().s());
        directions.addOrder("eou", EducationOrgUnit.territorialOrgUnit().territorialTitle().s());
        directions.addOrder("eou", EducationOrgUnit.educationLevelHighSchool().displayableTitle().s());
        directions.addOrder("eou", EducationOrgUnit.developForm().code().s());
        directions.addOrder("eou", EducationOrgUnit.developCondition().code().s());
        directions.addOrder("eou", EducationOrgUnit.developTech().code().s());
        directions.addOrder("eou", EducationOrgUnit.developPeriod().code().s());

        final List<EducationOrgUnit> educationOrgUnitList = directions.getResultList(getSession());

        // договоры
        final MQBuilder b = new MQBuilder(UniscEduAgreement2EntrantGen.ENTITY_CLASS, "e");
        b.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.P_FORMING_DATE);
        b.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+".id");
        b.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.L_ENTRANT+".id", entrant.getId()));
        final List<UniscEduAgreement2Entrant> relations = b.getResultList(getSession());

        final List<Model.Row> result = new ArrayList<>();
        for (final EducationOrgUnit educationOrgUnit: educationOrgUnitList) {
            boolean exists = false;
            for (final UniscEduAgreement2Entrant rel: relations) {
                if (educationOrgUnit.equals(rel.getAgreement().getConfig().getEducationOrgUnit())) {
                    result.add(new Model.Row(educationOrgUnit, rel));
                    exists = true;
                }
            }

            if (!exists) {
                result.add(new Model.Row(educationOrgUnit, null));
            }
        }

        final DynamicListDataSource<Model.Row> requestedDirectionsDataSource = model.getRequestedDirectionsDataSource();
        requestedDirectionsDataSource.setTotalSize(result.size());
        requestedDirectionsDataSource.setCountRow(result.size());
        requestedDirectionsDataSource.createPage(result);
    }

    @Override
    public void deleteEntrantAgreement(final Model model, final Long relationId) {
        final UniscEduAgreement2Entrant rel = get(UniscEduAgreement2Entrant.class, relationId);
        if (null == rel) { return; }

        final IUniscEduAgreement2PersonRole<PersonRole> relation = IUniscEduAgreementDAO.INSTANCE.get().getRelation(rel.getAgreement());
        if (!rel.getTarget().equals(relation.getTarget())) {
            throw new ApplicationException("Вы не можете удалить договор, поскольку он уже связан со студентом.");
        }

        if (rel.getEntrant().getId().equals(model.getEntrant().getId())) {
            delete(rel);
            delete(rel.getAgreement());
        }
    }

    @Override
    public EducationOrgUnit getEducationOrgUnit(final Long id) {
        final IEntity entity = get(id);
        if (entity instanceof UniscEduAgreement2Entrant) {
            return ((UniscEduAgreement2Entrant)entity).getAgreement().getConfig().getEducationOrgUnit();
        }
        if (entity instanceof RequestedEnrollmentDirection) {
            return ((RequestedEnrollmentDirection)entity).getEnrollmentDirection().getEducationOrgUnit();
        }
        return null;
    }


    @SuppressWarnings("unchecked")
    @Override
    public UniscEduOrgUnit getUniscEduOrgUnit(final Long id) {
        final IEntity entity = get(id);
        if (entity instanceof UniscEduAgreement2Entrant) {
            return ((UniscEduAgreement2Entrant)entity).getAgreement().getConfig();
        }
        if (entity instanceof RequestedEnrollmentDirection) {
            final RequestedEnrollmentDirection requestedEnrollmentDirection = (RequestedEnrollmentDirection)entity;
            final EnrollmentDirection enrollmentDirection = requestedEnrollmentDirection.getEnrollmentDirection();
            final EducationOrgUnit ou = enrollmentDirection.getEducationOrgUnit();
            final List<UniscEduOrgUnit> configList = getSession().createCriteria(UniscEduOrgUnit.class)
            .add(Restrictions.eq(UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, ou))
            .add(Restrictions.eq(UniscEduOrgUnitGen.L_EDUCATION_YEAR, enrollmentDirection.getEnrollmentCampaign().getEducationYear()))
            .list();

            if (configList.size() == 1) { return configList.get(0); }
        }
        return null;
    }

}
