package ru.tandemservice.uniecc.events;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Synchronization;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;

import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniecc.dao.IEccDAO;

/**
 * @author vdanilov
 */
public class PreliminaryEnrollmentStudentCheckListener extends FilteredSingleEntityEventListener<ISingleEntityEvent>
{
    private static final ThreadLocal<PreStudentCheckSyncObject> syncs = new ThreadLocal<PreStudentCheckSyncObject>();

    @Override
    public void onFilteredEvent(final ISingleEntityEvent event)
    {
        PreStudentCheckSyncObject sync = syncs.get();

        if (sync == null)
        {
            final Session session = event.getSession();
            sync = new PreStudentCheckSyncObject(session);
            syncs.set(sync);
            session.getTransaction().registerSynchronization(sync);
        }

        sync.add(event);
    }

    private static final class PreStudentCheckSyncObject implements Synchronization
    {
        private Session _session;
        private List<PreliminaryEnrollmentStudent> _preStudentList = new ArrayList<PreliminaryEnrollmentStudent>();
        private List<AbstractEntrantExtract> _entrantExtractList = new ArrayList<AbstractEntrantExtract>();

        private PreStudentCheckSyncObject(Session session)
        {
            _session = session;
        }

        private void add(ISingleEntityEvent event)
        {
            IEntity entity = event.getEntity();

            if (entity instanceof PreliminaryEnrollmentStudent)
                _preStudentList.add((PreliminaryEnrollmentStudent) entity);
            else if (entity instanceof AbstractEntrantExtract)
                _entrantExtractList.add((AbstractEntrantExtract) entity);
            else
                throw new RuntimeException("You cannot run PreStudentCheck for class '" + entity.getClass().getSimpleName() + "'. Not implemented!");
        }

        @Override
        public void beforeCompletion()
        {
            if (!_preStudentList.isEmpty())
            {
                Set<Long> set = new HashSet<Long>();
                for (PreliminaryEnrollmentStudent preStudent : _preStudentList)
                    set.add(preStudent.getId());
                IEccDAO.INSTANCE.get().preparePreStudentCheck(set);
            }

            if (!_entrantExtractList.isEmpty())
            {
                Set<Long> set = new HashSet<Long>();
                for (AbstractEntrantExtract extract : _entrantExtractList)
                    set.add(extract.getId());
                IEccDAO.INSTANCE.get().updateEntrantExtractCheck(set);
            }

            _session.flush();
        }

        @Override
        public void afterCompletion(int arg)
        {
            syncs.remove();
        }
    }
}
