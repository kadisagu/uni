/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniecc.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vip_delete
 * @since 11.08.2009
 */
public interface IEccDAO
{
    static final SpringBeanCache<IEccDAO> INSTANCE = new SpringBeanCache<IEccDAO>(IEccDAO.class.getName());

    UniscEduMainAgreement getAgreement(Entrant entrant, EducationOrgUnit educationOrgUnit);

    Map<Long, UniscEduMainAgreement> getAgreements(Collection<Long> ecgEntrantRecommendedIds, Map<Long, Long> entrRecomToStudentIdMap);

    /**
     * Массово проверяет студентов предзачисления на соответствие настройке "Ограничения при проведении зачисления"
     *
     * @param preStudentIds студенты предзачисления
     */
    void preparePreStudentCheck(Set<Long> preStudentIds);

    /**
     * Массово проверяет студентов предзачисления из указанных выписок на соответствие настройке "Ограничения при проведении зачисления"
     * и проверяет существование договоров со студентами (при их отсутствии создает их)
     *
     * @param entrantExtractIds абитуриентские выписки
     */
    void updateEntrantExtractCheck(Set<Long> entrantExtractIds);

    /**
     * Получает сводную информацию по договорам абитуриентов
     *
     * @param entrantList список абитуриентов
     * @return ключ (Абитуриент+НПП) -> факт оплаты по договору -> договор
     */
    Map<PairKey<Entrant, EducationOrgUnit>, Map<Boolean, List<UniscEduAgreement2Entrant>>> getAgreementMap(List<Entrant> entrantList);

    /**
     * Проверяет можно ли изменять настройку «Ограничения при проведении зачисления»
     *
     * @param enrollmentCampaign приемная кампания
     * @return true, если изменять можно, false, если нельзя
     */
    boolean isCanChangeEccEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign);

    /**
     * Создает или обновляет существующую настройку для ограничений при зачислении по договорам
     *
     * @param enrollmentCampaign приемная кампания
     * @param configDTO          параметры настройки ограничений
     */
    void saveOrUpdateEccEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign, EccEnrollmentCampaign configDTO);
}
