package ru.tandemservice.uniecc.component.student.StudentContractListTab;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setConnectButtonVisible(false);
        model.setDisconnectButtonVisible(false);

        Student student = get(Student.class, model.getId());
        if (null != student)
        {
            MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "a2s");
            builder.add(MQExpression.eq("a2s", UniscEduAgreement2Student.L_STUDENT, student));
            final List<UniscEduAgreement2Student> agreements = builder.getResultList(getSession());

            if (agreements.isEmpty())
            {
                // # для студента нет зарегистрированных договоров.
                // # существует абитуриент (на основе той же персоны) с договором, не связанным с другим студентом (например с др. категорией)

                MQBuilder b = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "a2e");
                b.add(MQExpression.eq("a2e", UniscEduAgreement2Entrant.entrant().person().toString(), student.getPerson()));
                b.add(MQExpression.notIn("a2e", UniscEduAgreement2Entrant.agreement().id().s(), new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "a2s", new String[]{UniscEduAgreement2Student.agreement().id().s()})));
                if (b.getResultCount(getSession()) > 0)
                {
                    model.setConnectButtonVisible(true);
                }

            } else if (agreements.size() == 1)
            {
                // # для студента зарегистрирован в текущий момент только один договор, который имеет связь с абитуриентом,
                // # к нему не созданы доп. соглашения

                final UniscEduMainAgreement agreement = agreements.iterator().next().getAgreement();
                List<UniscEduAdditAgreement> additionalAgreements = getList(UniscEduAdditAgreement.class, UniscEduAdditAgreement.L_AGREEMENT, agreement);
                if (additionalAgreements.isEmpty())
                {
                    MQBuilder b = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "a2e");
                    b.add(MQExpression.eq("a2e", UniscEduAgreement2Entrant.L_AGREEMENT, agreement));
                    if (b.getResultCount(getSession()) > 0)
                    {
                        model.setDisconnectButtonVisible(true);
                    }
                }
            }
        } else
        {
            logger.error("id is null");
        }
    }

    @Override
    public void doDisconnect(Model model)
    {
        Student student = get(Student.class, model.getId());
        if (null != student)
        {
            synchronized (String.valueOf(student.getId()).intern())
            {
                MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "a2s");
                builder.add(MQExpression.eq("a2s", UniscEduAgreement2Student.L_STUDENT, student));
                builder.add(MQExpression.in("a2s", UniscEduAgreement2Student.agreement().id().s(), new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "a2e", new String[]{UniscEduAgreement2Entrant.agreement().id().s()})));
                final List<UniscEduAgreement2Student> agreements = builder.getResultList(getSession());
                if (1 == agreements.size())
                {
                    // delete relation
                    delete(agreements.iterator().next());
                }
            }
        }
    }
}
