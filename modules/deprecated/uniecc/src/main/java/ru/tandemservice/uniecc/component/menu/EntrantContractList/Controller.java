/* $Id: Controller.java 8435 2009-06-11 07:08:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.menu.EntrantContractList;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantGen;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(component.getSettings());
        this.getDao().prepare(model);
        this.prepareEntrantContractsDataSource(component);
    }

    private void prepareEntrantContractsDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<UniscEduAgreement2Entrant> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshEntrantContractsDataSource(model);
        });

        final IMergeRowIdResolver mergeRowIdResolver = e -> String.valueOf(((UniscEduAgreement2Entrant) ((ViewWrapper) e).getEntity()).getEntrant().getId());
        dataSource.addColumn(IndicatorColumn.createIconColumn("entrant", "Абитуриент").setMergeRowIdResolver(mergeRowIdResolver).setMergeRows(true).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Личный №", UniscEduAgreement2EntrantGen.L_ENTRANT + "." + EntrantGen.P_PERSONAL_NUMBER).setOrderable(false).setClickable(false).setMergeRowIdResolver(mergeRowIdResolver).setMergeRows(true));
        dataSource.addColumn(new PublisherColumnBuilder("Абитуриент", PersonRoleGen.L_PERSON + "." + Person.P_FULLFIO, "contractsTab").subTab("entrantAgreementContractTab").entity(UniscEduAgreement2EntrantGen.L_ENTRANT).build().setOrderable(false).setMergeRowIdResolver(mergeRowIdResolver).setMergeRows(true).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Состояние", UniscEduAgreement2EntrantGen.L_ENTRANT + "." + EntrantGen.L_STATE + ".title").setOrderable(false).setClickable(false).setMergeRowIdResolver(mergeRowIdResolver).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("№ договора", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_NUMBER).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Шифр", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.P_CIPHER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_TERRITORIAL_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.P_TITLE_WITH_FORM_AND_CONDITION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.P_DESCRIPTION).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условия", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + "." + UniscEduOrgUnitGen.P_CONDITIONS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип заказчика", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CUSTOMER_TYPE + "." + ICatalogItem.CATALOG_ITEM_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Способ оплаты", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_PAY_PLAN_FREQ + "." + ICatalogItem.CATALOG_ITEM_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действует с", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_FORMING_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Действует до", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_DEADLINE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Изменен", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Первый платеж", "firstStage").setOrderable(false).setClickable(false));

        dataSource.addColumn(new ToggleColumn("Проверен", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE).setListener("onClickCheckBySignature").setPermissionKey("entrantContractsCheckedBySignature_list"));
        dataSource.addColumn(new ToggleColumn("Оплачено", UniscEduAgreement2EntrantGen.P_PAYMENT_EXISTS).setListener("onClickPaymentExists").setPermissionKey("entrantContractsPaymentExists_list"));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintEntrantContract", "Печать").setPermissionKey("entrantContractsPrint_list").setRequired(true));


        this.getModel(component).setEntrantContractsDataSource(dataSource);
    }

    public void onClickPrintEntrantContract(final IBusinessComponent component)
    {
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantAgreementPrint.Model.class.getPackage().getName(),
                Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())
        ));
    }

    public void onClickCheckBySignature(final IBusinessComponent component)
    {
        this.getDao().updateCheckBySignature(this.getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickPaymentExists(final IBusinessComponent component)
    {
        getDao().updatePaymentExists(getModel(component), (Long) component.getListenerParameter());
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.getEntrantContractsDataSource().refresh();
        component.saveSettings();
    }

    public void onClickClear(final IBusinessComponent component)
    {
        this.getModel(component).getSettings().clear();
        this.onClickSearch(component);
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        final byte[] context = getDao().prepareReport(model, component.getUserContext().getPrincipal().getId());
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(context, "EntrantContracts.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));
    }
}
