/* $Id: Model.java 8355 2009-06-08 08:23:06Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

/**
 * @author vdanilov
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"),
        @Bind(key = "entrantContractListSelectedTab", binding = "entrantContractListSelectedTab")
})
public class Model
{
    private Long id;

    public Long getId() { return id; }
    public void setId(final Long id) { this.id = id; }

    private String entrantContractListSelectedTab;
    public String getEntrantContractListSelectedTab() { return this.entrantContractListSelectedTab; }
    public void setEntrantContractListSelectedTab(final String entrantContractListSelectedTab) { this.entrantContractListSelectedTab = entrantContractListSelectedTab; }

    private boolean _tabVisible;
    public boolean isTabVisible() { return _tabVisible; }
    public void setTabVisible(final boolean tabVisible) { _tabVisible = tabVisible; }
}
