/* $Id: $ */
package ru.tandemservice.uniecc.component.entrant.EntrantList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
public class Model extends ru.tandemservice.uniec.component.entrant.EntrantList.Model
{

    private ISelectModel _paymentExistsModel;

    public ISelectModel getPaymentExistsModel()
    {
        return _paymentExistsModel;
    }

    public void setPaymentExistsModel(ISelectModel paymentExistsModel)
    {
        _paymentExistsModel = paymentExistsModel;
    }

    public DataWrapper getPaymentExists()
    {
        return getSettings().get("payment");
    }

    public void setPaymentExists(DataWrapper paymentExists)
    {
        getSettings().set("payment", paymentExists);
    }
}
