package ru.tandemservice.uniecc.component.entrant.NextOfKinAgreementUsage;

import ru.tandemservice.uni.dao.IUniBaseDao;

public interface IDAO extends IUniBaseDao
{
	void prepareDataSource(Model model);
	void updateUsedInAgreement(Model model, Long id);
	void updateSignInAgreement(Model model, Long id);
}
