/* $Id$ */
package ru.tandemservice.uniecc.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.uniecc.base.ext.GlobalReport.ui.List.UnieccGlobalReportListAddon;

/**
 * @author azhebko
 * @since 31.03.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    public static final String SC_PAYMENT_REPORT = ru.tandemservice.unisc.base.ext.GlobalReport.GlobalReportExtManager.SC_PAYMENT_REPORT;
    public static final String SC_AGREEMENT_INCOME_REPORT = ru.tandemservice.unisc.base.ext.GlobalReport.GlobalReportExtManager.SC_AGREEMENT_INCOME_REPORT;

    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .replace(SC_PAYMENT_REPORT, new GlobalReportDefinition("sc", "scPayment", ru.tandemservice.uniecc.component.report.PaymentReport.PaymentReportList.Model.class.getPackage().getName()))
                .replace(SC_AGREEMENT_INCOME_REPORT, new GlobalReportDefinition("sc", "scAgreementIncome", ru.tandemservice.uniecc.component.report.AgreementIncomeReport.AgreementIncomeReportList.Model.class.getPackage().getName()))
                .create();
    }
}