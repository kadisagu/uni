package ru.tandemservice.uniecc.dao;

import java.util.List;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uni.dao.UniBaseDao;
import org.tandemframework.shared.person.base.entity.gen.PersonNextOfKinGen;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import ru.tandemservice.uniecc.entity.relations.gen.NextOfKinStatusRelationGen;

/**
 * @author root
 */
public class EccNextOfKinDAO extends UniBaseDao implements IEccNextOfKinDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void updateNextOfKinRelationProperty(final Long personId, final Long id, final String name, final boolean unique)
    {
        synchronized (String.valueOf(personId).intern())
        {
            final Class entityClass = EntityRuntime.getMeta(id).getEntityClass();
            NextOfKinStatusRelation rel = null;
            if (NextOfKinStatusRelation.class.isAssignableFrom(entityClass))
            {
                rel = get(NextOfKinStatusRelation.class, id);

            } else if (PersonNextOfKin.class.isAssignableFrom(entityClass))
            {
                final PersonNextOfKin personNextOfKin = get(PersonNextOfKin.class, id);
                rel = get(NextOfKinStatusRelation.class, NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN, personNextOfKin);
                if (null == rel)
                {
                    rel = new NextOfKinStatusRelation();
                    rel.setPersonNextOfKin(personNextOfKin);
                }
            }

            if (null != rel)
            {
                final boolean newFlagValue = Boolean.FALSE.equals(rel.getProperty(name));
                if (newFlagValue && unique)
                {
                    /* если мы выставляем значение в true, то снимаем со всех остальных, если требуется */
                    final MQBuilder builder = new MQBuilder(NextOfKinStatusRelationGen.ENTITY_CLASS, "e");
                    builder.add(MQExpression.eq("e", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN + "." + PersonNextOfKinGen.L_PERSON + ".id", personId));
                    final List<NextOfKinStatusRelation> rows = builder.getResultList(getSession());
                    for (final NextOfKinStatusRelation row : rows)
                    {
                        if (!row.getId().equals(rel.getId()))
                        {
                            row.setProperty(name, Boolean.FALSE);
                            getSession().saveOrUpdate(row);
                        }
                    }
                }
                rel.setProperty(name, newFlagValue);
                getSession().saveOrUpdate(rel);
            }

            getSession().flush();
        }
    }
}
