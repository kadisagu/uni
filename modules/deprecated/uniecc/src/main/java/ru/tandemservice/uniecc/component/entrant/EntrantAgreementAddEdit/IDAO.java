package ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit;

import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author vdanilov
 */
@Deprecated
@SuppressWarnings("deprecation")
public interface IDAO extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.IDAO<Entrant> {

}
