/* $Id: DAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantAllContractsListTab;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EducationYearGen;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getId()));
    }

    @Override
    public void refreshEntrantContractsDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(UniscEduAgreement2EntrantGen.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.L_ENTRANT+".id", model.getId()));
        builder.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.L_EDUCATION_YEAR+"."+EducationYearGen.P_INT_VALUE);
        builder.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".code");
        builder.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+".id");
        builder.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.L_CONFIG+"."+UniscEduOrgUnitGen.P_CIPHER);
        builder.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.P_FORMING_DATE);
        builder.addOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+".id");

        UniBaseUtils.createPage(model.getEntrantContractsDataSource(), builder, getSession());
    }

    @Override
    public void updateCheckBySignature(final Model model, final Long relationId) {
        final UniscEduAgreement2Entrant rel = get(UniscEduAgreement2Entrant.class, relationId);
        if (null == rel) { return; }
        if (rel.getEntrant().getId().equals(model.getId())) {
            rel.getAgreement().setCheckedBySignature(!rel.getAgreement().isCheckedBySignature());
            getSession().saveOrUpdate(rel.getAgreement());
        }
    }

    @Override
    public void updatePaymentExists(Model model, final Long id)
    {
        final UniscEduAgreement2Entrant rel = get(UniscEduAgreement2Entrant.class, id);
        if (null == rel) {
            return;
        }
        rel.setPaymentExists(!rel.isPaymentExists());
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void deleteEntrantAgreement(final Model model, final Long relationId) {
        final UniscEduAgreement2Entrant rel = get(UniscEduAgreement2Entrant.class, relationId);
        if (null == rel) { return; }

        final IUniscEduAgreement2PersonRole<PersonRole> relation = IUniscEduAgreementDAO.INSTANCE.get().getRelation(rel.getAgreement());
        if (!rel.getTarget().equals(relation.getTarget())) {
            throw new ApplicationException("Вы не можете удалить договор, поскольку он уже связан со студентом.");
        }

        if (rel.getEntrant().getId().equals(model.getId())) {
            delete(rel);
            delete(rel.getAgreement());
        }
    }

}
