package ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;

import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author vdanilov
 */
@Output({
    @Bind(key="defaultEducationYearId", binding="relation.entrant.enrollmentCampaign.educationYear.id")
})
@Deprecated
@SuppressWarnings("deprecation")
public class Model extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model<Entrant> {
}
