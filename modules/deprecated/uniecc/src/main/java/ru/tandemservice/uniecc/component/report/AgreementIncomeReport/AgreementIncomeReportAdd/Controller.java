// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniecc.component.report.AgreementIncomeReport.AgreementIncomeReportAdd;

import java.util.Calendar;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author oleyba
 * @since 20.08.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getReport().getDateFrom().after(model.getReport().getDateTo()))
        {
            component.getUserContext().getErrorCollector().add("Дата начала периода построения отчета должна быть не позже даты окончания периода.", "dateFrom", "dateTo");
            return;
        }
        Calendar from = Calendar.getInstance();
        from.setTime(model.getReport().getDateFrom());
        Calendar to = Calendar.getInstance();
        to.setTime(model.getReport().getDateTo());
        if (from.get(Calendar.YEAR) != to.get(Calendar.YEAR))
        {
            component.getUserContext().getErrorCollector().add("Период построения отчета должен быть в рамках одного календарного года.", "dateFrom", "dateTo");
            return;
        }
        
        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }
}