package ru.tandemservice.uniecc.dao;

import java.util.Collection;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreement2PersonRoleResolver;
import ru.tandemservice.unisc.dao.agreement.UniscEduAgreement2StudentResolver;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class UniscEduAgreement2EntrantResolver extends UniBaseDao implements IUniscEduAgreement2PersonRoleResolver
{
    @Override
    public int priority()
    {
        return 1;
    }

    @Override
    public Collection<IUniscEduAgreement2PersonRole<PersonRole>> getRelations4Agreements(Collection<Long> mainAgreementIds)
    {
        return UniscEduAgreement2StudentResolver.resolveRelations(UniscEduAgreement2Entrant.class, UniscEduAgreement2Entrant.agreement().s(), mainAgreementIds, getSession());
    }

    @Override
    public Collection<IUniscEduAgreement2PersonRole<PersonRole>> getRelations4PersonRole(Collection<Long> personRoleIds)
    {
        return UniscEduAgreement2StudentResolver.resolveRelations(UniscEduAgreement2Entrant.class, UniscEduAgreement2Entrant.entrant().s(), personRoleIds, getSession());
    }
}
