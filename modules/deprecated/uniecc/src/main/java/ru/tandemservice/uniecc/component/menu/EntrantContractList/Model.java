/* $Id: Model.java 8355 2009-06-08 08:23:06Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.menu.EntrantContractList;

import java.util.Arrays;
import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * @author vdanilov
 */
public class Model implements IEnrollmentCampaignSelectModel, IEducationLevelModel
{
    public static final long TRUE_OPTION_ID = 0L;
    public static final long FALSE_OPTION_ID = 1L;

    private DynamicListDataSource<UniscEduAgreement2Entrant> _agreementsDataSource;
    public DynamicListDataSource<UniscEduAgreement2Entrant> getEntrantContractsDataSource() { return _agreementsDataSource; }
    public void setEntrantContractsDataSource(final DynamicListDataSource<UniscEduAgreement2Entrant> studentContractsDataSource) { _agreementsDataSource = studentContractsDataSource; }

    private IDataSettings _settings;
    public void setSettings(final IDataSettings settings) { _settings = settings; }

    private List<EnrollmentCampaign> _enrollmentCampaignList;

    @Override public IDataSettings getSettings() { return _settings;  }
    @Override public EnrollmentCampaign getEnrollmentCampaign() { return (EnrollmentCampaign) _settings.get(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME); }
    @Override public void setEnrollmentCampaign(final EnrollmentCampaign enrollmentCampaign) { _settings.set(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign); }
    @Override public List<EnrollmentCampaign> getEnrollmentCampaignList() { return _enrollmentCampaignList; }
    @Override public void setEnrollmentCampaignList(final List<EnrollmentCampaign> enrollmentCampaignList) { _enrollmentCampaignList = enrollmentCampaignList; }

    private ISelectModel cipherModel;
    public ISelectModel getCipherModel() { return this.cipherModel; }
    public void setCipherModel(final ISelectModel cipherModel) { this.cipherModel = cipherModel; }

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _entrantCustomStateCIModel;

    public ISelectModel getFormativeOrgUnitModel() { return this._formativeOrgUnitModel; }
    public void setFormativeOrgUnitModel(final ISelectModel formativeOrgUnitModel) { this._formativeOrgUnitModel = formativeOrgUnitModel; }

    public ISelectModel getTerritorialOrgUnitModel() { return this._territorialOrgUnitModel; }
    public void setTerritorialOrgUnitModel(final ISelectModel territorialOrgUnitModel) { this._territorialOrgUnitModel = territorialOrgUnitModel; }

    public ISelectModel getEducationLevelsHighSchoolModel() { return this._educationLevelsHighSchoolModel; }
    public void setEducationLevelsHighSchoolModel(final ISelectModel educationLevelsHighSchoolModel) { this._educationLevelsHighSchoolModel = educationLevelsHighSchoolModel; }

    public ISelectModel getDevelopFormModel() { return this._developFormModel; }
    public void setDevelopFormModel(final ISelectModel developFormModel) { this._developFormModel = developFormModel; }

    public ISelectModel getDevelopTechModel() { return this._developTechModel; }
    public void setDevelopTechModel(final ISelectModel developTechModel) { this._developTechModel = developTechModel; }

    public ISelectModel getDevelopConditionModel() { return this._developConditionModel; }
    public void setDevelopConditionModel(final ISelectModel developConditionModel) { this._developConditionModel = developConditionModel; }

    public ISelectModel getDevelopPeriodModel() { return this._developPeriodModel; }
    public void setDevelopPeriodModel(final ISelectModel developPeriodModel) { this._developPeriodModel = developPeriodModel; }

    public UniscEduOrgUnit getCipher() { return (UniscEduOrgUnit) this.getSettings().get("cipher"); }
    @Override public OrgUnit getFormativeOrgUnit() { return (OrgUnit) this.getSettings().get("formativeOrgUnit"); }
    @Override public OrgUnit getTerritorialOrgUnit() { return (OrgUnit) this.getSettings().get("territorialOrgUnit"); }
    @Override public EducationLevelsHighSchool getEducationLevelsHighSchool() { return (EducationLevelsHighSchool) this.getSettings().get("eduLevelHighSchool"); }
    @Override public DevelopCondition getDevelopCondition() { return (DevelopCondition) this.getSettings().get("developCondition"); }
    @Override public DevelopForm getDevelopForm() { return (DevelopForm) this.getSettings().get("developForm"); }
    @Override public DevelopPeriod getDevelopPeriod() { return (DevelopPeriod) this.getSettings().get("developPeriod"); }
    @Override public DevelopTech getDevelopTech() { return (DevelopTech) this.getSettings().get("developTech"); }

    private List<IdentifiableWrapper> _archivalList = Arrays.asList(new IdentifiableWrapper(TRUE_OPTION_ID, "Показать архивных"), new IdentifiableWrapper(FALSE_OPTION_ID, "Показать не архивных"));
    public List<IdentifiableWrapper> getArchivalList() { return _archivalList; }
    public Boolean getArchival() {
        final IdentifiableWrapper wrapper = (IdentifiableWrapper) getSettings().get("archival");
        if (wrapper == null) return null;
        return TRUE_OPTION_ID == wrapper.getId(); }

    private List<EntrantState> _entrantStateList;
    public List<EntrantState> getEntrantStateList() { return _entrantStateList; }
    public void setEntrantStateList(List<EntrantState> entrantStateList) { _entrantStateList = entrantStateList; }
    public EntrantState getEntrantState() { return (EntrantState) this.getSettings().get("entrantState"); }

    private List<IdentifiableWrapper> _paymentExistsList = Arrays.asList(new IdentifiableWrapper(TRUE_OPTION_ID, "Есть"), new IdentifiableWrapper(FALSE_OPTION_ID, "Нет"));
    public List<IdentifiableWrapper> getPaymentExistsList() { return _paymentExistsList; }
    public Boolean getPaymentExists() {
        final IdentifiableWrapper wrapper = (IdentifiableWrapper) getSettings().get("paymentExists");
        if (wrapper == null) return null;
        return TRUE_OPTION_ID == wrapper.getId(); }

    private List<UniscCustomerType> _customerTypeList;
    public List<UniscCustomerType> getCustomerTypeList() { return _customerTypeList; }
    public void setCustomerTypeList(List<UniscCustomerType> customerTypeList) {  _customerTypeList = customerTypeList; }
    public UniscCustomerType getCustomerType() { return (UniscCustomerType) this.getSettings().get("customerType"); }

    private List<IdentifiableWrapper> _checkedList = Arrays.asList(new IdentifiableWrapper(TRUE_OPTION_ID, "Да"), new IdentifiableWrapper(FALSE_OPTION_ID, "Нет"));
    public List<IdentifiableWrapper> getCheckedList() { return _checkedList; }
    public Boolean getChecked() {
        final IdentifiableWrapper wrapper = (IdentifiableWrapper) getSettings().get("checked");
        if (wrapper == null) return null;
        return TRUE_OPTION_ID == wrapper.getId(); }

    private List<UniscPayPeriod> _payPeriodList;
    public List<UniscPayPeriod> getPayPeriodList() { return _payPeriodList; }
    public void setPayPeriodList(List<UniscPayPeriod> payPeriodList) { _payPeriodList = payPeriodList; }
    public UniscPayPeriod getPayPeriod() { return (UniscPayPeriod) this.getSettings().get("payPeriod"); }

    public ISelectModel getEntrantCustomStateCIModel()
    {
        return _entrantCustomStateCIModel;
    }

    public void setEntrantCustomStateCIModel(ISelectModel entrantCustomStateCIList)
    {
        _entrantCustomStateCIModel = entrantCustomStateCIList;
    }

    public List<EntrantCustomStateCI> getEntrantCustomStateCIList()
    {
        return _settings.get("entrantCustomStateCIList");
    }

    public void setEntrantCustomStateCIList(List<EntrantCustomStateCI> entrantCustomStateCI)
    {
        _settings.set("entrantCustomStateCIList", entrantCustomStateCI);
    }
}
