/* $Id: Model.java 8355 2009-06-08 08:23:06Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantRequestedDirectionContractsListTab;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.IdentityCard;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;

/**
 * @author vdanilov
 */
@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id"))
public class Model
{
    private Entrant entrant = new Entrant();
    public Entrant getEntrant() { return this.entrant; }
    public void setEntrant(final Entrant entrant) { this.entrant = entrant; }

    public String getAge() {
        final Date birthDate = getEntrant().getPerson().getIdentityCard().getBirthDate();
        if (null != birthDate) {
            final Date now = DateUtils.truncate(new Date(), Calendar.DATE);
            final Date b = DateUtils.truncate(birthDate, Calendar.DATE);
            int y=0;
            while (DateUtils.addYears(b, 1+y).before(now)) { y++; }
            return String.valueOf(y);
        }
        return "";
    }

    public String getPassportTitle() {
        final IdentityCard card = getEntrant().getPerson().getIdentityCard();
        return StringUtils.trimToEmpty(StringUtils.trimToEmpty(card.getSeria()) + ' ' + StringUtils.trimToEmpty(card.getNumber()));
    }

    public String getPassportIssuance() {
        final IdentityCard card = getEntrant().getPerson().getIdentityCard();
        final StringBuilder builder = new StringBuilder();
        if (StringUtils.isNotEmpty(card.getIssuancePlace())) {
            builder.append(card.getIssuancePlace());
        }
        if (StringUtils.isNotEmpty(card.getIssuanceCode())) {
            if (builder.length() > 0) {
                builder.append(" (").append(card.getIssuanceCode()).append(")");
            } else {
                builder.append("Код подразделения: ").append(card.getIssuanceCode());
            }
        }
        return builder.toString();
    }

    public static class Row implements IEntity {
        private final EducationOrgUnit direction;
        public EducationOrgUnit getDirection() { return this.direction; }

        private final UniscEduAgreement2Entrant relation;
        public UniscEduAgreement2Entrant getRelation() { return this.relation; }

        public Row(final EducationOrgUnit direction, final UniscEduAgreement2Entrant relation) {
            this.direction = direction;
            this.relation = relation;
        }

        @Override public Long getId() {
            if (null != relation) { return relation.getId(); }
            if (null != direction) { return direction.getId(); }
            return 0L;
        }

        @Override public void setId(final Long id) {
        }
        @Override public Object getProperty(final Object key) {
            return FastBeanUtils.getValue(this, key);
        }
        @Override public void setProperty(final String key, final Object value) {
            FastBeanUtils.setValue(this, key, value);
        }
    }

    private DynamicListDataSource<NextOfKinStatusRelation> nextOfKinDataSource;
    public DynamicListDataSource<NextOfKinStatusRelation> getNextOfKinDataSource() { return this.nextOfKinDataSource; }
    public void setNextOfKinDataSource(final DynamicListDataSource<NextOfKinStatusRelation> nextOfKinDataSource) { this.nextOfKinDataSource = nextOfKinDataSource; }

    private DynamicListDataSource<Row> requestedDirectionsDataSource;
    public DynamicListDataSource<Row> getRequestedDirectionsDataSource() { return this.requestedDirectionsDataSource; }
    public void setRequestedDirectionsDataSource(final DynamicListDataSource<Row> requestedDirectionsDataSource) { this.requestedDirectionsDataSource = requestedDirectionsDataSource; }

}
