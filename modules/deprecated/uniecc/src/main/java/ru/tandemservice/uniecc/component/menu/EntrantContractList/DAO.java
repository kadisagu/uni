/* $Id: DAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.menu.EntrantContractList;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.settings.IDataSettingsManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen;
import org.tandemframework.shared.person.base.entity.gen.PersonGen;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uni.util.EducationLevelUtil;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantGen;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayPlanRow;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementPayPlanRowGen;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.catalog.gen.UniscCustomerTypeGen;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        final Session session = getSession();
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, session);

        model.setCipherModel(new UniQueryFullCheckSelectModel(UniscEduOrgUnit.P_CIPHER_WITH_TITLE)
        {
            @Override
            protected MQBuilder query(final String alias, final String filter)
            {
                final MQBuilder builder = new MQBuilder(UniscEduOrgUnitGen.ENTITY_CLASS, alias);
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR + ".code");
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".code");
                builder.addOrder(alias, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT + ".id");
                builder.addOrder(alias, UniscEduOrgUnitGen.P_CIPHER);

                final EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
                if (null != enrollmentCampaign) {
                    builder.add(MQExpression.eq(alias, UniscEduOrgUnitGen.L_EDUCATION_YEAR + ".id", enrollmentCampaign.getEducationYear().getId()));
                }
                if (null != filter) {
                    FilterUtils.applySimpleLikeFilter(builder, alias, UniscEduOrgUnit.P_CIPHER, filter);
                }
                return builder;
            }
        });

        model.setFormativeOrgUnitModel(EducationLevelUtil.createFormativeOrgUnitAutocompleteModel());
        model.setTerritorialOrgUnitModel(EducationLevelUtil.createTerritorialOrgUnitAutocompleteModel());
        model.setEducationLevelsHighSchoolModel(EducationLevelUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EducationLevelUtil.createDevelopFormModel(model, true));
        model.setDevelopConditionModel(EducationLevelUtil.createDevelopConditionModel(model, true));
        model.setDevelopTechModel(EducationLevelUtil.createDevelopTechModel(model, true));
        model.setDevelopPeriodModel(EducationLevelUtil.createDevelopPeriodModel(model, true));

        model.setEntrantStateList(getCatalogItemList(EntrantState.class));
        model.setEntrantCustomStateCIModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EntrantCustomStateCI> list = getCatalogItemList(EntrantCustomStateCI.class);
                return new ListResult<>(list, list.size());
            }
        });
        model.setCustomerTypeList(getList(UniscCustomerType.class, UniscCustomerTypeGen.P_HIDDEN, Boolean.FALSE, ICatalogItem.CATALOG_ITEM_CODE));
        model.setPayPeriodList(getCatalogItemListOrderByCode(UniscPayPeriod.class));
    }

    @Override
    public void refreshEntrantContractsDataSource(final Model model)
    {
        final MQBuilder builder = getBuilder(model);
        final DynamicListDataSource<UniscEduAgreement2Entrant> dataSource = model.getEntrantContractsDataSource();
        UniBaseUtils.createPage(dataSource, builder, getSession());
        final Map<Long, String> firstStageCostMap = getFirstStageCostMap(dataSource.getEntityList(), null);
        for (final ViewWrapper<UniscEduAgreement2Entrant> view : ViewWrapper.<UniscEduAgreement2Entrant>getPatchedList(dataSource)) {
            view.setViewProperty("firstStage", firstStageCostMap.containsKey(view.getEntity().getAgreement().getId()) ? firstStageCostMap.get(view.getEntity().getAgreement().getId()) : "");
        }
    }

    @Override
    public void updatePaymentExists(final Model model, final Long id)
    {
        final UniscEduAgreement2Entrant rel = get(UniscEduAgreement2Entrant.class, id);
        if (null == rel) {
            return;
        }
        rel.setPaymentExists(!rel.isPaymentExists());
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void updateCheckBySignature(final Model model, final Long id)
    {
        final UniscEduAgreement2Entrant rel = get(UniscEduAgreement2Entrant.class, id);
        if (null == rel) {
            return;
        }
        rel.getAgreement().setCheckedBySignature(!rel.getAgreement().isCheckedBySignature());
        getSession().saveOrUpdate(rel);
    }

    @Override
    public byte[] prepareReport(final Model model, final Long principalId)
    {
        final ITemplateDocument templateDocument = getCatalogItem(UniscTemplateDocument.class, "entrantContractList");
        final RtfDocument document = new RtfReader().read(templateDocument.getContent());

        // шапка отчета
        final RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("formationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("enrollmentCampaign", model.getEnrollmentCampaign().getTitle());
        injectModifier.modify(document);
        // значения фильтров в шапке
        final List<String[]> filterRows = new ArrayList<>();
        {
            final UniscEduOrgUnit cipher = model.getCipher();
            if (null != cipher) {
                filterRows.add(new String[]{"Шифр:", cipher.getTitle()});
            } else
            {
                if (null != model.getFormativeOrgUnit()) {
                    filterRows.add(new String[]{"Формирующее подр.:", model.getFormativeOrgUnit().getTitle()});
                }
                if (null != model.getTerritorialOrgUnit()) {
                    filterRows.add(new String[]{"Территориальное подр.:", model.getTerritorialOrgUnit().getTerritorialTitle()});
                }
                if (null != model.getEducationLevelsHighSchool()) {
                    filterRows.add(new String[]{"Направление подготовки (специальность):", model.getEducationLevelsHighSchool().getTitle()});
                }
                if (null != model.getDevelopForm()) {
                    filterRows.add(new String[]{"Форма освоения:", model.getDevelopForm().getTitle()});
                }
                if (null != model.getDevelopCondition()) {
                    filterRows.add(new String[]{"Условие освоения:", model.getDevelopCondition().getTitle()});
                }
                if (null != model.getDevelopTech()) {
                    filterRows.add(new String[]{"Технология освоения:", model.getDevelopTech().getTitle()});
                }
                if (null != model.getDevelopPeriod()) {
                    filterRows.add(new String[]{"Срок освоения:", model.getDevelopPeriod().getTitle()});
                }
            }

            final IDataSettings settings = model.getSettings();
            final String lastName = settings.get("lastNameFilter");
            final String firstName = settings.get("firstNameFilter");
            final String middleName = settings.get("middleNameFilter");
            final String personalNumber = settings.get("personalNumberFilter");
            final String agreementNumber = settings.get("agreementNumberFilter");

            if (StringUtils.isNotEmpty(lastName)) {
                filterRows.add(new String[]{"Фамилия:", lastName});
            }
            if (StringUtils.isNotEmpty(firstName)) {
                filterRows.add(new String[]{"Имя:", firstName});
            }
            if (StringUtils.isNotEmpty(middleName)) {
                filterRows.add(new String[]{"Отчество:", middleName});
            }
            if (StringUtils.isNotEmpty(personalNumber)) {
                filterRows.add(new String[]{"Личный номер:", personalNumber});
            }
            if (model.getArchival() != null) {
                filterRows.add(new String[]{"Статус абитуриента:", model.getArchival() ? "В архиве" : "Не в архиве"});
            }
            if (model.getEntrantState() != null) {
                filterRows.add(new String[]{"Состояние абитуриента:", model.getEntrantState().getTitle()});
            }
            if (CollectionUtils.isNotEmpty(model.getEntrantCustomStateCIList())) {
                filterRows.add(new String[]{"Дополнительные статусы:", model.getEntrantCustomStateCIList().stream().map(EntrantCustomStateCI::getTitle).collect(Collectors.joining("; "))});
            }
            if (model.getPaymentExists() != null) {
                filterRows.add(new String[]{"Оплата:", model.getPaymentExists() ? "Есть" : "Нет"});
            }
            if (model.getCustomerType() != null) {
                filterRows.add(new String[]{"Тип заказчика:", model.getCustomerType().getTitle()});
            }
            if (StringUtils.isNotEmpty(agreementNumber)) {
                filterRows.add(new String[]{"Номер договора:", agreementNumber});
            }
            if (model.getChecked() != null) {
                filterRows.add(new String[]{"Проверен:", model.getChecked() ? "Да" : "Нет"});
            }
            if (model.getPayPeriod() != null) {
                filterRows.add(new String[]{"Способ оплаты:", model.getPayPeriod().getTitle()});
            }
        }
        new RtfTableModifier().put("N", filterRows.toArray(new String[filterRows.size()][])).modify(document);

        // колонки таблицы договоров
        final Map<String, Integer> columnsWidth = new HashMap<>();
        columnsWidth.put("n", 8);
        columnsWidth.put("agreement.config.cipher", 2 * 37);
        columnsWidth.put("agreement.config.educationOrgUnit.territorialOrgUnit.title", 7 * 37);
        columnsWidth.put("agreement.config.educationOrgUnit.formativeOrgUnit.title", 5 * 37);
        columnsWidth.put("agreement.number", 172);
        final String fioColumnKey = "entrant.person.fullFio";
        columnsWidth.put(fioColumnKey, 271);
        columnsWidth.put("agreement.config.educationOrgUnit.titleWithFormAndCondition", 117 * 3);
        columnsWidth.put("agreement.config.description", 172);
        columnsWidth.put("agreement.config.conditions", 149);
        columnsWidth.put("agreement.formingDate", 58 * 3);
        columnsWidth.put("agreement.deadlineDate", 58 * 3);
        columnsWidth.put("agreement.payPlanFreq.title", 4 * 37);
        columnsWidth.put("agreement.customerType.title", 61 * 3);
        columnsWidth.put("paymentExists", 43 * 3);

        final List<FormatterColumn> listColumns = getColumns(model, principalId);
        if (listColumns.isEmpty()) {
            throw new IllegalStateException();
        }

        final List<String[]> reportRows = new ArrayList<>();
        int columnCount = 1; // число колонок таблицы договоров
        // заголовок таблицы договоров
        {
            final List<String> headerRow = new ArrayList<>();
            headerRow.add("№");
            for (final FormatterColumn c : listColumns) {
                if (columnsWidth.containsKey(c.getName()))
                {
                    headerRow.add(c.getCaption());
                    columnCount++;
                }
            }
            reportRows.add(headerRow.toArray(new String[headerRow.size()]));
        }

        /* строки отчета */
        {
            int rowNumber = 1;
            final List<UniscEduAgreement2Entrant> agreementList = getBuilder(model).getResultList(getSession());
            for (final UniscEduAgreement2Entrant rel : agreementList)
            {
                final List<String> reportRow = new ArrayList<>();
                reportRow.add(String.valueOf(rowNumber++));
                for (final FormatterColumn listColumn : listColumns)
                {
                    if (!columnsWidth.containsKey(listColumn.getName())) {
                        continue;
                    }
                    if (listColumn instanceof IMultiValuesColumn)
                    {
                        final StringBuilder sb = new StringBuilder();
                        for (final Object entity : ((IMultiValuesColumn) listColumn).getEntityList(rel))
                        {
                            if (sb.length() > 0) {
                                sb.append(", ");
                            }
                            sb.append(listColumn.getContent((IEntity) entity));
                        }
                        reportRow.add(sb.toString());
                    } else {
                        reportRow.add(listColumn.getContent(rel));
                    }
                }
                reportRows.add(reportRow.toArray(new String[reportRow.size()]));
            }
        }

        final int[] parts = new int[columnCount];
        /* ширина колонок */
        {
            final int totalWidth = 2775;
            int i = 0;
            parts[i] = columnsWidth.get("n");
            int used = parts[i++];
            int fioPosition = 0;
            for (final FormatterColumn c : listColumns)
            {
                if (!columnsWidth.containsKey(c.getName())) {
                    continue;
                }
                if (fioColumnKey.equals(c.getName()))
                {
                    fioPosition = i;
                    parts[i++] = 0;
                    continue;
                }
                parts[i] = columnsWidth.get(c.getName());
                used = used + parts[i++];
            }
            parts[fioPosition] = totalWidth - used;
        }

        final RtfTableModifier tableModifier = new RtfTableModifier().put("T", reportRows.toArray(new String[reportRows.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(final RtfTable table, final int currentRowIndex)
            {
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 0, null, parts);
            }
        });
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    @SuppressWarnings("unchecked")
    private List<FormatterColumn> getColumns(final Model model, final Long principalId)
    {
        final List<FormatterColumn> columns = new ArrayList<>();
        final IDataSettingsManager dataSettingsManager = (IDataSettingsManager) ApplicationRuntime.getBean("dataSettingsManager");
        final IDataSettings dataSettings = (null == dataSettingsManager ? null : dataSettingsManager.getSettings(String.valueOf(principalId), "EntrantContractList.contracts..columns"));
        final DynamicListDataSource<UniscEduAgreement2Entrant> ds = model.getEntrantContractsDataSource();
        for (final AbstractColumn c : ds.getColumns())
        {
            if ((null == dataSettings) || c.isVisible(dataSettings))
            {
                if (c instanceof FormatterColumn) {
                    columns.add((FormatterColumn) c);
                } else if ((c instanceof ToggleColumn) || (c instanceof BooleanColumn)) {
                    columns.add(new SimpleColumn(c.getCaption(), c.getKey(), YesNoFormatter.INSTANCE));
                }
            }
        }
        return columns;
    }

    private Map<Long, String> getFirstStageCostMap(final List<UniscEduAgreement2Entrant> relList, final MQBuilder idBuilder)
    {
        final Map<Long, String> firstStageCostMap = new HashMap<Long, String>();

        final MQBuilder builder = new MQBuilder(UniscEduAgreementPayPlanRowGen.ENTITY_CLASS, "r");
        builder.add(MQExpression.isNull("r", UniscEduAgreementPayPlanRowGen.L_REPLACED_BY));
        if (relList != null)
        {
            final List<Long> agreementIds = CommonBaseUtil.getPropertiesList(relList, UniscEduAgreement2Entrant.L_AGREEMENT + ".id");
            builder.add(MQExpression.in("r", UniscEduAgreementPayPlanRowGen.L_OWNER + ".id", agreementIds));
        } else if (idBuilder != null)
        {
            builder.add(MQExpression.in("r", UniscEduAgreementPayPlanRowGen.L_OWNER + ".id", idBuilder));
        }
        builder.addOrder("r", UniscEduAgreementPayPlanRowGen.P_STAGE);
        builder.addOrder("r", UniscEduAgreementPayPlanRowGen.P_DATE);
        for (final UniscEduAgreementPayPlanRow row : new ArrayList<>(builder.<UniscEduAgreementPayPlanRow>getResultList(getSession()))) {
            if (!firstStageCostMap.containsKey(row.getOwner().getId())) {
                firstStageCostMap.put(row.getOwner().getId(), row.getCostAsString());
            }
        }
        return firstStageCostMap;
    }


    private MQBuilder getBuilder(final Model model)
    {
        final MQBuilder builder = new MQBuilder(UniscEduAgreement2EntrantGen.ENTITY_CLASS, "e");

        builder.addJoinFetch("e", UniscEduAgreement2EntrantGen.L_AGREEMENT, "agreement");
        builder.addJoinFetch("agreement", UniscEduAgreementBaseGen.L_CONFIG, "config");
        builder.addJoinFetch("config", UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT, "edu");
        builder.addJoinFetch("e", UniscEduAgreement2EntrantGen.L_ENTRANT, "entrant");
        builder.addJoinFetch("entrant", PersonRoleGen.L_PERSON, "person");
        builder.addJoinFetch("person", PersonGen.L_IDENTITY_CARD, "identityCard");

        final EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (null != enrollmentCampaign) {
            builder.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.L_ENTRANT + "." + EntrantGen.L_ENROLLMENT_CAMPAIGN + ".id", enrollmentCampaign.getId()));
        }

        Date changeDate = model.getSettings().get("changeDate");
        if (null != changeDate)
            builder.add(UniMQExpression.betweenDate("agreement", UniscEduAgreementBaseGen.P_LAST_MODIFICATION_DATE, changeDate, new Date()));

        Date formingDateFrom = model.getSettings().get("formingDateFromFilter");
        Date formingDateTo = model.getSettings().get("formingDateToFilter");
        if (null != formingDateFrom)
            builder.add(UniMQExpression.greatOrEq("agreement", UniscEduAgreementBaseGen.P_FORMING_DATE, formingDateFrom));
        if (null != formingDateTo)
            builder.add(UniMQExpression.lessOrEq("agreement", UniscEduAgreementBaseGen.P_FORMING_DATE, formingDateTo));

        final UniscEduOrgUnit cipher = model.getCipher();
        if (null != cipher)
        {
            builder.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.L_AGREEMENT + "." + UniscEduAgreementBaseGen.L_CONFIG + ".id", cipher.getId()));

        } else
        {
            if (null != model.getFormativeOrgUnit()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT + ".id", model.getFormativeOrgUnit().getId()));
            }
            if (null != model.getTerritorialOrgUnit()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT + ".id", model.getTerritorialOrgUnit().getId()));
            }
            if (null != model.getEducationLevelsHighSchool()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".id", model.getEducationLevelsHighSchool().getId()));
            }
            if (null != model.getDevelopForm()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_FORM + ".id", model.getDevelopForm().getId()));
            }
            if (null != model.getDevelopCondition()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_CONDITION + ".id", model.getDevelopCondition().getId()));
            }
            if (null != model.getDevelopTech()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_TECH + ".id", model.getDevelopTech().getId()));
            }
            if (null != model.getDevelopPeriod()) {
                builder.add(MQExpression.eq("edu", EducationOrgUnitGen.L_DEVELOP_PERIOD + ".id", model.getDevelopPeriod().getId()));
            }
        }

        final IDataSettings settings = model.getSettings();
        final String lastName = settings.get("lastNameFilter");
        final String firstName = settings.get("firstNameFilter");
        final String middleName = settings.get("middleNameFilter");
        final String personalNumber = settings.get("personalNumberFilter");
        final String agreementNumber = settings.get("agreementNumberFilter");

        if (StringUtils.isNotEmpty(lastName)) {
            builder.add(MQExpression.like("identityCard", IdentityCardGen.P_LAST_NAME, CoreStringUtils.escapeLike(lastName)));
        }
        if (StringUtils.isNotEmpty(firstName)) {
            builder.add(MQExpression.like("identityCard", IdentityCardGen.P_FIRST_NAME, CoreStringUtils.escapeLike(firstName)));
        }
        if (StringUtils.isNotEmpty(middleName)) {
            builder.add(MQExpression.like("identityCard", IdentityCardGen.P_MIDDLE_NAME, CoreStringUtils.escapeLike(middleName)));
        }
        if (StringUtils.isNotEmpty(personalNumber)) {
            builder.add(MQExpression.like("entrant", EntrantGen.P_PERSONAL_NUMBER, "%" + personalNumber));
        }
        if (StringUtils.isNotEmpty(agreementNumber)) {
            builder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.P_NUMBER, agreementNumber));
        }

        if (model.getArchival() != null) {
            builder.add(MQExpression.eq("entrant", EntrantGen.P_ARCHIVAL, model.getArchival()));
        }
        if (model.getEntrantState() != null) {
            builder.add(MQExpression.eq("entrant", EntrantGen.L_STATE, model.getEntrantState()));
        }
        if (CollectionUtils.isNotEmpty(model.getEntrantCustomStateCIList())) {
            MQBuilder paymentExistsBuilder = new MQBuilder(EntrantCustomState.ENTITY_CLASS, "ep_ph")
                    .add(MQExpression.eqProperty("ep_ph", EntrantCustomState.entrant(), "entrant", Entrant.id()))
                    .add(MQExpression.in("ep_ph", EntrantCustomState.customState(), model.getEntrantCustomStateCIList()));

            builder.add(MQExpression.exists(paymentExistsBuilder));
        }
        if (model.getPaymentExists() != null) {
            builder.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.P_PAYMENT_EXISTS, model.getPaymentExists()));
        }
        if (model.getCustomerType() != null) {
            builder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.L_CUSTOMER_TYPE, model.getCustomerType()));
        }
        if (model.getChecked() != null) {
            builder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.P_CHECKED_BY_SIGNATURE, model.getChecked()));
        }
        if (model.getPayPeriod() != null) {
            builder.add(MQExpression.eq("agreement", UniscEduAgreementBaseGen.L_PAY_PLAN_FREQ, model.getPayPeriod()));
        }

        /* стандартная сортировка (по умолчанию). Если что-то переписывать, то оставить ее только как сортировку по умолчанию */
        {
            builder.addOrder("identityCard", IdentityCardGen.P_LAST_NAME);
            builder.addOrder("identityCard", IdentityCardGen.P_FIRST_NAME);
            builder.addOrder("identityCard", IdentityCardGen.P_MIDDLE_NAME);
            //builder.addOrder("entrant", EntrantGen.P_PERSONAL_NUMBER);
            builder.addOrder("entrant", "id");
            //builder.addOrder("edu", EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL+".code");
            //builder.addOrder("edu", "id");
            builder.addOrder("agreement", UniscEduAgreementBaseGen.P_FORMING_DATE);
            builder.addOrder("agreement", "id");
        }
        return builder;
    }
}
