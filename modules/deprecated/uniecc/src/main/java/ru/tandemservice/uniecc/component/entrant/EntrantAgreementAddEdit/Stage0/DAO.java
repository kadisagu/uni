package ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Stage0;

import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.EducationLevelModel;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniecc.utils.UnieccPropertyUtils;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0.Model;
import ru.tandemservice.unisc.entity.config.gen.UniscEduOrgUnitGen;


/**
 * @author vdanilov
 */
@Deprecated
@SuppressWarnings("deprecation")
public class DAO extends ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage0.DAO implements IDAO {

    @Override
    protected MQBuilder fillUniScEduOrgUnitBuilder(final Model model, final String domainName) {
        final MQBuilder builder = super.fillUniScEduOrgUnitBuilder(model, domainName);
        if (UnieccPropertyUtils.STRICT_DIRECTION)
        {
            final Entrant entrant = ((Entrant)model.getPersonRole());

            // выбранные направления приема
            final MQBuilder enrollmentDirections = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d", new String[] { RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().id().s() });
            enrollmentDirections.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().s(), entrant));

            // профили (выбранные профили выбранных направлений)
            final MQBuilder profileDirections = new MQBuilder(PriorityProfileEduOu.ENTITY_CLASS, "p", new String[] { PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().id().s() });
            profileDirections.add(MQExpression.eq("p", PriorityProfileEduOu.requestedEnrollmentDirection().entrantRequest().entrant().s(), entrant));

            builder.add(MQExpression.or(
                MQExpression.in(domainName, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+".id", enrollmentDirections),
                MQExpression.in(domainName, UniscEduOrgUnitGen.L_EDUCATION_ORG_UNIT+".id", profileDirections)
            ));
        }
        return builder;
    }


    @Override
    protected void fillEducationOrgUnitModel(final Model model, final EducationLevelModel educationLevelModel) {
        if (UnieccPropertyUtils.STRICT_DIRECTION) {
            educationLevelModel.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
            educationLevelModel.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));

            final Entrant entrant = ((Entrant)model.getPersonRole());

            class SelectModel extends UniQueryFullCheckSelectModel {
                private final Class<? extends ITitled> klass;
                private final String path;
                private final String[] properties;

                public SelectModel(final Class<? extends ITitled> klass, final String path, final String ...properties) {
                    super();
                    this.klass = klass;
                    this.path = path;
                    this.properties = properties;
                }
                @Override protected MQBuilder query(final String alias, final String filter) {
                    final MQBuilder directions = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou", new String[] { path + ".id" });

                    // выбранные направления приема
                    final MQBuilder enrollmentDirections = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d", new String[] { RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().id().s() });
                    enrollmentDirections.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().s(), entrant));

                    // профили (выбранные профили выбранных направлений)
                    final MQBuilder profileDirections = new MQBuilder(PriorityProfileEduOu.ENTITY_CLASS, "p", new String[] { PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().id().s() });
                    profileDirections.add(MQExpression.eq("p", PriorityProfileEduOu.requestedEnrollmentDirection().entrantRequest().entrant().s(), entrant));

                    directions.add(MQExpression.or(
                        MQExpression.in("eou", "id", enrollmentDirections),
                        MQExpression.in("eou", "id", profileDirections)
                    ));

                    if (null != filter) {
                        directions.add(MQExpression.like("eou", path + ".title", "%"+filter+"%"));
                    }

                    directions.add(
                        MQExpression.eq("eou", EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT, educationLevelModel.getFormativeOrgUnit())
                    );
                    directions.add(
                        MQExpression.eq("eou", EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT, educationLevelModel.getTerritorialOrgUnit())
                    );

                    for (final String p: properties) {
                        final Object v = FastBeanUtils.getValue(educationLevelModel,  EducationLevelModel.TRANSLATIONS.get(p));
                        directions.add(MQExpression.eq("eou", p, v));
                    }

                    final MQBuilder result = new MQBuilder(klass.getName(), alias);
                    result.add(MQExpression.in(alias, "id", directions));
                    return result;
                }
            }


            educationLevelModel.setEducationLevelsHighSchoolModel(new SelectModel(
                EducationLevelsHighSchool.class,
                EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL
            ));

            educationLevelModel.setDevelopFormModel(new SelectModel(
                DevelopForm.class,
                EducationOrgUnitGen.L_DEVELOP_FORM,
                EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL
            ));

            educationLevelModel.setDevelopConditionModel(new SelectModel(
                DevelopCondition.class,
                EducationOrgUnitGen.L_DEVELOP_CONDITION,
                EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL,
                EducationOrgUnitGen.L_DEVELOP_FORM
            ));

            educationLevelModel.setDevelopTechModel(new SelectModel(
                DevelopTech.class,
                EducationOrgUnitGen.L_DEVELOP_TECH,
                EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL,
                EducationOrgUnitGen.L_DEVELOP_FORM,
                EducationOrgUnitGen.L_DEVELOP_CONDITION
            ));

            educationLevelModel.setDevelopPeriodModel(new SelectModel(
                DevelopPeriod.class,
                EducationOrgUnitGen.L_DEVELOP_PERIOD,
                EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL,
                EducationOrgUnitGen.L_DEVELOP_FORM,
                EducationOrgUnitGen.L_DEVELOP_CONDITION,
                EducationOrgUnitGen.L_DEVELOP_TECH
            ));
        } else {
            super.fillEducationOrgUnitModel(model, educationLevelModel);
        }
    }
}
