package ru.tandemservice.uniecc.component.entrant.EntrantAgreementPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unisc.UniscComponents;

import java.util.Collections;

@SuppressWarnings("deprecation")
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        component.createChildRegion("agreement", new ComponentActivator(
                UniscComponents.AGREEMENT_PUB,
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getRelation().getAgreement().getId())
        ));
    }

    public void onClickEdit(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getRelation().getId())
        ));
        //this.activate(component, new ComponentActivator(
        //		ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Model.class.getPackage().getName(),
        //		Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getRelation().getId())
        //));
    }
    public void onClickPrint(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantAgreementPrint.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getRelation().getId())
        ));
    }
    public void onClickEditGroup(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniecc.component.entrant.EntrantGroupEdit.Model.class.getPackage().getName(),
                Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getRelation().getId())
        ));
        //this.activate(component, new ComponentActivator(
        //		ru.tandemservice.uniecc.component.entrant.EntrantGroupEdit.Model.class.getPackage().getName(),
        //		Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getRelation().getId())
        //));
    }

}
