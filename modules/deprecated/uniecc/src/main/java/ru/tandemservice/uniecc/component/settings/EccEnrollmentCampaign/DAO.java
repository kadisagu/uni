/* $Id: DAO.java 8433 2009-06-11 06:38:32Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.settings.EccEnrollmentCampaign;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniecc.dao.IEccDAO;
import ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 07.04.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        // если указана приемная кампания, то можно загрузить настройку
        if (model.getEnrollmentCampaign() != null)
        {
            model.setHasPreStudent(!IEccDAO.INSTANCE.get().isCanChangeEccEnrollmentCampaign(model.getEnrollmentCampaign()));

            // инициализируем DTO
            model.setConfigDTO(new EccEnrollmentCampaign());

            // загружаем настройку
            EccEnrollmentCampaign campaign = get(EccEnrollmentCampaign.class, EccEnrollmentCampaign.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());

            // если настройка есть, то сохраняем ее параметры в configDTO
            if (campaign != null)
                model.getConfigDTO().update(campaign);
        }
    }

    @Override
    public void update(Model model)
    {
        IEccDAO.INSTANCE.get().saveOrUpdateEccEnrollmentCampaign(model.getEnrollmentCampaign(), model.getConfigDTO());
    }
}
