/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniecc.dao;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 11.08.2009
 */
public class EccDAO extends UniBaseDao implements IEccDAO
{
    @Override
    public UniscEduMainAgreement getAgreement(Entrant entrant, EducationOrgUnit educationOrgUnit)
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "r", new String[]{UniscEduAgreement2Entrant.L_AGREEMENT});
        builder.add(MQExpression.eq("r", UniscEduAgreement2Entrant.L_ENTRANT, entrant));
        builder.add(MQExpression.eq("r", UniscEduAgreement2Entrant.L_AGREEMENT + "." + UniscEduMainAgreement.L_CONFIG + "." + UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, educationOrgUnit));
        builder.addOrder("r", UniscEduAgreement2Entrant.L_AGREEMENT + "." + UniscEduMainAgreement.P_FORMING_DATE, OrderDirection.desc);
        List<UniscEduMainAgreement> list = builder.getResultList(getSession(), 0, 1);
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public Map<Long, UniscEduMainAgreement> getAgreements(Collection<Long> ecgEntrantRecommendedIds, Map<Long, Long> entrRecomToStudentIdMap)
    {
        List<Long> entrantIds = new ArrayList<>();
        final Map<Long, EcgpEntrantRecommended> recomEntrMap = new HashMap<>();
        final Map<Long, UniscEduMainAgreement> recomEntrContractsMap = new HashMap<>();
        final Map<Long, Student> entrRecomToStudentMap = new HashMap<>();

        final List<EcgpEntrantRecommended> entrantsRecommended = new ArrayList<>();
        BatchUtils.execute(ecgEntrantRecommendedIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> entrantsRecommended.addAll(
                new DQLSelectBuilder()
                .fromEntity(EcgpEntrantRecommended.class, "er").column("er")
                .where(in(property(EcgpEntrantRecommended.id().fromAlias("er")), ids))
                .createStatement(getSession()).<EcgpEntrantRecommended>list()
        ));

        for (EcgpEntrantRecommended entrantRecommended : entrantsRecommended)
        {
            Long entrantId = entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrantId();
            recomEntrMap.put(entrantId, entrantRecommended);
            entrantIds.add(entrantId);
        }

        final List<Student> studentsList = new ArrayList<>();
        BatchUtils.execute(entrRecomToStudentIdMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, ids -> studentsList.addAll(
                new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column("s")
                .where(in(property(Student.id().fromAlias("s")), ids))
                .createStatement(getSession()).<Student>list()
        ));

        for (Student student : studentsList)
        {
            Long entrRecomId = entrRecomToStudentIdMap.get(student.getId());
            entrRecomToStudentMap.put(entrRecomId, student);
        }

        BatchUtils.execute(entrantIds, DQL.MAX_VALUES_ROW_NUMBER, ids -> {

            List<UniscEduAgreement2Entrant> entrantContractsList = new DQLSelectBuilder()
                    .fromEntity(UniscEduAgreement2Entrant.class, "c").column("c")
                    .where(in(property(UniscEduAgreement2Entrant.entrant().id().fromAlias("c")), ids))
                    .order(property(UniscEduAgreement2Entrant.agreement().formingDate().fromAlias("c")), OrderDirection.desc)
                    .createStatement(getSession()).list();

            for (UniscEduAgreement2Entrant contract : entrantContractsList)
            {
                Long entrantId = contract.getEntrant().getId();
                EcgpEntrantRecommended entrRecom = recomEntrMap.get(entrantId);
                Long entrRecomId = entrRecom.getId();
                Student student = entrRecomToStudentMap.get(entrRecomId);
                Long studentId = student.getId();

                if (!recomEntrContractsMap.containsKey(studentId))
                {
                    if (contract.getAgreement().getConfig().getEducationOrgUnit().equals(entrRecom.getProfile().getEducationOrgUnit()))
                    {
                        recomEntrContractsMap.put(studentId, contract.getAgreement());
                    } else if (contract.getAgreement().getConfig().getEducationOrgUnit().equals(student.getEducationOrgUnit()))
                    {
                        recomEntrContractsMap.put(studentId, contract.getAgreement());
                    }
                }
            }
        });
        return recomEntrContractsMap;
    }

    @Override
    public void preparePreStudentCheck(Set<Long> preStudentIds)
    {
        List<PreliminaryEnrollmentStudent> preStudentSet = new ArrayList<>();
        for (Long preStudentId : preStudentIds)
        {
            PreliminaryEnrollmentStudent preStudent = get(preStudentId);
            if (preStudent != null)
                preStudentSet.add(preStudent);
        }

        Map<EnrollmentCampaign, EccEnrollmentCampaign> map = new HashMap<>();
        for (EccEnrollmentCampaign config : getList(EccEnrollmentCampaign.class))
            map.put(config.getEnrollmentCampaign(), config);

        Map<EccEnrollmentCampaign, Set<PreliminaryEnrollmentStudent>> campaign2preStudentMap = new HashMap<>();
        for (PreliminaryEnrollmentStudent preStudent : preStudentSet)
        {
            if (!preStudent.getCompensationType().isBudget())
            {
                EccEnrollmentCampaign campaign = map.get(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getEnrollmentCampaign());
                if (campaign != null)
                {
                    Set<PreliminaryEnrollmentStudent> set = campaign2preStudentMap.get(campaign);
                    if (set == null)
                        campaign2preStudentMap.put(campaign, set = new HashSet<>());
                    set.add(preStudent);
                }
            }
        }

        for (Map.Entry<EccEnrollmentCampaign, Set<PreliminaryEnrollmentStudent>> entry : campaign2preStudentMap.entrySet())
        {
            final EccEnrollmentCampaign campaign = entry.getKey();
            BatchUtils.execute(entry.getValue(), DQL.MAX_VALUES_ROW_NUMBER, elements -> preparePreStudentCheckFast(campaign, false, elements));
        }
    }

    @Override
    public void updateEntrantExtractCheck(Set<Long> entrantExtractIds)
    {
        List<AbstractEntrantExtract> entrantExtractSet = new ArrayList<>();
        for (Long extractId : entrantExtractIds)
        {
            AbstractEntrantExtract extract = get(extractId);
            if (extract != null)
                entrantExtractSet.add(extract);
        }

        BatchUtils.execute(entrantExtractSet, DQL.MAX_VALUES_ROW_NUMBER, this::updateEntrantExtractCheckFast);
    }

    private void preparePreStudentCheckFast(EccEnrollmentCampaign campaign, boolean order, Collection<PreliminaryEnrollmentStudent> preStudentSet)
    {
        Map<EnrollmentDirection, List<EducationOrgUnit>> direction2profile = SafeMap.get(key -> {
            return new DQLSelectBuilder()
                    .fromEntity(ProfileEducationOrgUnit.class, "p")
                    .column(property(ProfileEducationOrgUnit.educationOrgUnit().fromAlias("p")))
                    .where(eq(property(ProfileEducationOrgUnit.enrollmentDirection().fromAlias("p")), value(key)))
                    .createStatement(getSession()).list();
        });

        List<Entrant> entrantList = new ArrayList<>();
        for (PreliminaryEnrollmentStudent preStudent : preStudentSet)
        {
            entrantList.add(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant());
        }
        Map<PairKey<Entrant, EducationOrgUnit>, Map<Boolean, List<UniscEduAgreement2Entrant>>> key2agreementList = getAgreementMap(entrantList);

        boolean targetAdmissionCheck = order ? campaign.isTargetReq4Order() : campaign.isTargetReq4PreEnroll();
        boolean targetAdmissionPaymentCheck = campaign.isTargetReqPayment();

        boolean generalAdmissionCheck = order ? campaign.isGeneralReq4Order() : campaign.isGeneralReq4PreEnroll();
        boolean generalAdmissionPaymentCheck = campaign.isGeneralReqPayment();

        for (PreliminaryEnrollmentStudent preStudent : preStudentSet)
        {
            boolean check;
            boolean payment;

            // в зависимости от ЦП разные условия
            if (preStudent.isTargetAdmission())
            {
                check = targetAdmissionCheck;
                payment = targetAdmissionPaymentCheck;
            } else
            {
                check = generalAdmissionCheck;
                payment = generalAdmissionPaymentCheck;
            }

            if (!check)
            {
                continue; /* не надо ничего проверять */
            }

            boolean ok = false;

            {
                // подготавливаем перечень НПП, по которым будем искать договор (НПП, на которое зачисляют студента, + НПП профилей НПВ с которого зачисляют студента)
                final Set<EducationOrgUnit> educationOrgUnitSet = new HashSet<>(direction2profile.get(preStudent.getRequestedEnrollmentDirection().getEnrollmentDirection()));
                educationOrgUnitSet.add(preStudent.getEducationOrgUnit());

                // ищем договор с оплатой (в направлении приема и профилях выбранных направлений) - если нашли - уходим
                final Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                for (EducationOrgUnit educationOrgUnit : educationOrgUnitSet)
                {
                    final Map<Boolean, List<UniscEduAgreement2Entrant>> map = key2agreementList.get(PairKey.create(entrant, educationOrgUnit));
                    if (payment)
                    {
                        if (null != map && map.containsKey(Boolean.TRUE))
                        {
                            ok = true;
                            break; /* если есть договоры (с оплатой) */
                        }
                    } else
                    {
                        if (null != map)
                        {
                            ok = true;
                            break; /* если есть договоры (любые) */
                        }
                    }
                }
            }

            if (ok)
            {
                continue; /* все хорошо, мы нашли нужный договор */
            }

            // ничего не нашли - ругаемся матом
            final EducationOrgUnit educationOrgUnit = preStudent.getEducationOrgUnit(); // печатаем направление, на которое зачисляют
            if (payment)
            {
                if (order)
                {
                    throw new ApplicationException("Нельзя включить в приказ абитуриента " + preStudent.getTitle() + ", т.к. по направлению (специальности) " + educationOrgUnit.getEducationLevelHighSchool().getDisplayableTitle() + " (и не по одному из его профилей) у него нет оплаты по договору.");
                } else
                {
                    throw new ApplicationException("Нельзя предварительно зачислить абитуриента " + preStudent.getTitle() + ", т.к. по направлению (специальности) " + educationOrgUnit.getEducationLevelHighSchool().getDisplayableTitle() + " (и не по одному из его профилей) у него нет оплаты по договору.");
                }
            } else
            {
                if (order)
                {
                    throw new ApplicationException("Нельзя включить в приказ абитуриента " + preStudent.getTitle() + ", т.к. по направлению (специальности) " + educationOrgUnit.getEducationLevelHighSchool().getDisplayableTitle() + " (и не по одному из его профилей) у него нет договора.");
                } else
                {
                    throw new ApplicationException("Нельзя предварительно зачислить абитуриента " + preStudent.getTitle() + ", т.к. по направлению (специальности) " + educationOrgUnit.getEducationLevelHighSchool().getDisplayableTitle() + " (и не по одному из его профилей) у него нет договора.");
                }
            }

        }
    }

    private void updateEntrantExtractCheckFast(Collection<AbstractEntrantExtract> entrantExtractSet)
    {
        // П Р О В Е Р Я Е М   С Т У Д Е Н Т О В   П Р Е Д З А Ч И С Л Е Н И Я   И З   В С Е Х   В Ы П И С О К

        Map<EnrollmentCampaign, EccEnrollmentCampaign> map = new HashMap<>();
        for (EccEnrollmentCampaign config : getList(EccEnrollmentCampaign.class))
            map.put(config.getEnrollmentCampaign(), config);

        Map<EccEnrollmentCampaign, Set<PreliminaryEnrollmentStudent>> campaign2preStudentMap = new HashMap<>();
        for (AbstractEntrantExtract extract : entrantExtractSet)
        {
            PreliminaryEnrollmentStudent preStudent = extract.getEntity();
            if (!preStudent.getCompensationType().isBudget())
            {
                EccEnrollmentCampaign campaign = map.get(preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getEnrollmentCampaign());
                if (campaign != null)
                {
                    Set<PreliminaryEnrollmentStudent> set = campaign2preStudentMap.get(campaign);
                    if (set == null)
                        campaign2preStudentMap.put(campaign, set = new HashSet<>());
                    set.add(preStudent);
                }
            }
        }

        for (Map.Entry<EccEnrollmentCampaign, Set<PreliminaryEnrollmentStudent>> entry : campaign2preStudentMap.entrySet())
        {
            final EccEnrollmentCampaign campaign = entry.getKey();
            BatchUtils.execute(entry.getValue(), DQL.MAX_VALUES_ROW_NUMBER, elements -> preparePreStudentCheckFast(campaign, true, elements));
        }

        // П Р И Ц Е П Л Я Е М   Д О Г О В О Р Ы   К   С Т У Д Е Н Т А М   И З   В Ы П И С О К

        // загружаем договоры студентов
        List<Student> studentList = new ArrayList<>();
        for (AbstractEntrantExtract extract : entrantExtractSet)
        {
            if (extract instanceof EnrollmentExtract)
            {
                Student student = ((EnrollmentExtract) extract).getStudentNew();
                if (student != null && !student.getCompensationType().isBudget())
                    studentList.add(student);
            }
        }
        List<UniscEduAgreement2Student> list = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Student.class, "e").column("e")
                .where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Student.student().fromAlias("e")), studentList))
                .where(DQLExpressions.eq(DQLExpressions.property(UniscEduAgreement2Student.active().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)))
                .createStatement(new DQLExecutionContext(getSession())).list();
        Map<Student, UniscEduAgreement2Student> student2agreementMap = new HashMap<>();
        for (UniscEduAgreement2Student agreement : list)
            student2agreementMap.put(agreement.getStudent(), agreement);

        // загружаем договоры абитуриентов
        List<Entrant> entrantList = new ArrayList<>();
        for (AbstractEntrantExtract extract : entrantExtractSet)
        {
            entrantList.add(extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant());
        }
        Map<PairKey<Entrant, EducationOrgUnit>, Map<Boolean, List<UniscEduAgreement2Entrant>>> key2agreementList = getAgreementMap(entrantList);

        for (AbstractEntrantExtract extract : entrantExtractSet)
        {
            if (extract instanceof EnrollmentExtract)
            {
                Student student = ((EnrollmentExtract) extract).getStudentNew();
                if (student != null && !student.getCompensationType().isBudget())
                {
                    UniscEduAgreement2Student agreement = student2agreementMap.get(student);

                    // если нет договора со студентом
                    if (agreement == null)
                    {
                        Map<Boolean, List<UniscEduAgreement2Entrant>> agreementMap = key2agreementList.get(PairKey.create(extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant(), student.getEducationOrgUnit()));

                        // если есть абитуриентский договор
                        if (agreementMap != null && !agreementMap.isEmpty())
                        {
                            // договоры с проплатой
                            List<UniscEduAgreement2Entrant> agreementWithPaymentList = agreementMap.get(Boolean.TRUE);

                            // договоры без проплаты
                            List<UniscEduAgreement2Entrant> agreementWithoutPaymentList = agreementMap.get(Boolean.FALSE);

                            // вначале пытаемся взять договор с проплатой, потом без нее
                            UniscEduAgreement2Entrant selected = agreementWithPaymentList != null ? agreementWithPaymentList.get(0) : agreementWithoutPaymentList.get(0);

                            try {
                                UniscEduAgreement2Student relation = new UniscEduAgreement2Student();
                                relation.setActive(true);
                                relation.setStudent(student);
                                relation.setAgreement(selected.getAgreement());
                                getSession().save(relation);

                                IUniscEduAgreementDAO.INSTANCE.get().doConnect(relation);
                                getSession().flush();
                            }
                            catch (Exception e) {
                                String str = "Не удалось привязать договор " + selected.getAgreement().getTitle() + " к студенту " + student.getFullTitle() + ". Возможно, этот договор уже прикреплен к другому студенту. См. ошибку в логе.";
                                Debug.exception(str + e.getMessage(), e);
                                throw new ApplicationException(str, e);
                            }
                        }
                    }
                }
            }
        }
    }

    // ключ -> факт оплаты договоры -> список договоров
    @Override
    public Map<PairKey<Entrant, EducationOrgUnit>, Map<Boolean, List<UniscEduAgreement2Entrant>>> getAgreementMap(List<Entrant> entrantList)
    {
        // загружаем все соглашения
        List<UniscEduAgreement2Entrant> agreementList = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Entrant.class, "e").column("e")
                .where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Entrant.entrant().fromAlias("e")), entrantList))
                .createStatement(getSession()).list();

        // ключ -> факт оплаты договоры -> список договоров
        Map<PairKey<Entrant, EducationOrgUnit>, Map<Boolean, List<UniscEduAgreement2Entrant>>> key2agreementList = new HashMap<>();

        // пробегаем все соглашения
        for (UniscEduAgreement2Entrant agreement : agreementList)
        {
            PairKey<Entrant, EducationOrgUnit> key = PairKey.create(agreement.getEntrant(), agreement.getAgreement().getConfig().getEducationOrgUnit());

            Map<Boolean, List<UniscEduAgreement2Entrant>> map = key2agreementList.get(key);
            if (map == null)
                key2agreementList.put(key, map = new HashMap<>());

            List<UniscEduAgreement2Entrant> list = map.get(agreement.isPaymentExists());
            if (list == null)
                map.put(agreement.isPaymentExists(), list = new ArrayList<>());
            list.add(agreement);
        }

        return key2agreementList;
    }

    @Override
    public boolean isCanChangeEccEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        return true;
    }

    @Override
    public void saveOrUpdateEccEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign, EccEnrollmentCampaign configDTO)
    {
        if (!isCanChangeEccEnrollmentCampaign(enrollmentCampaign))
            throw new ApplicationException("Нельзя изменять настройку «Ограничения при проведении зачисления», так как уже начался процесс предзачисления абитуриентов.");

        // если договор вообще не нужен, то и оплата по нему тоже не нужна
        if (!configDTO.isGeneralReq4Order() && !configDTO.isGeneralReq4PreEnroll())
            configDTO.setGeneralReqPayment(false);

        // если договор вообще не нужен, то и оплата по нему тоже не нужна
        if (!configDTO.isTargetReq4Order() && !configDTO.isTargetReq4PreEnroll())
            configDTO.setTargetReqPayment(false);

        boolean allFieldAreFalse = !configDTO.isGeneralReq4Order() && !configDTO.isGeneralReq4PreEnroll() && !configDTO.isTargetReq4Order() && !configDTO.isTargetReq4PreEnroll();

        // берем существующую настройку
        EccEnrollmentCampaign config = get(EccEnrollmentCampaign.class, EccEnrollmentCampaign.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign);

        if (config == null)
        {
            if (!allFieldAreFalse)
            {
                // сохраняем настройку, только если есть что сохранять
                config = new EccEnrollmentCampaign();
                config.update(configDTO);
                config.setEnrollmentCampaign(enrollmentCampaign);
                save(config);
            }
        } else
        {
            config.update(configDTO);

            if (allFieldAreFalse)
            {
                // все поля false. удаляем всю настройку, чтобы в базе не мешалась
                // заодно ускоряется PreliminaryEnrollmentStudentCheckListener
                delete(config);
            } else
            {
                // если есть что сохранять - сохраняем
                update(config);
            }
        }
    }
}
