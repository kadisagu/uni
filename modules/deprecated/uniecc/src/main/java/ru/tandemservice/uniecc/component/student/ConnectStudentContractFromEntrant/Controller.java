package ru.tandemservice.uniecc.component.student.ConnectStudentContractFromEntrant;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.setDataSource(new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(model);
        }));

        final DynamicListDataSource<UniscEduAgreement2Entrant> dataSource = model.getDataSource();
        dataSource.addColumn(new RadioButtonColumn("select"));
        dataSource.addColumn(new SimpleColumn("Номер", UniscEduAgreement2EntrantGen.agreement().number()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Шифр", UniscEduAgreement2EntrantGen.agreement().config().cipher()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки", UniscEduAgreement2EntrantGen.agreement().config().educationOrgUnit().titleWithFormAndCondition().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Описание", UniscEduAgreement2EntrantGen.agreement().config().description()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условия", UniscEduAgreement2EntrantGen.agreement().config().conditions()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип заказчика", UniscEduAgreement2EntrantGen.agreement().customerType().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Способ оплаты", UniscEduAgreement2EntrantGen.agreement().payPlanFreq().title()).setClickable(false).setOrderable(false));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final IEntity e = ((RadioButtonColumn) model.getDataSource().getColumn("select")).getSelectedEntity();
        if (null == e)
        {
            throw new ApplicationException("Не выбран договор.");
        }

        this.getDao().save(model);
        if (!UserContext.getInstance().getErrorCollector().hasErrors())
        {
            this.deactivate(component);
        }
    }
}
