package ru.tandemservice.uniecc.component.student.ConnectStudentContractFromEntrant;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;

@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long id;

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    private DynamicListDataSource<UniscEduAgreement2Entrant> dataSource;

    public DynamicListDataSource<UniscEduAgreement2Entrant> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<UniscEduAgreement2Entrant> dataSource)
    {
        this.dataSource = dataSource;
    }
}
