/* $Id$ */
package ru.tandemservice.uniecc.services;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.services.ContractAttributes;
import ru.tandemservice.uni.services.IEntrantEduContractDataService;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniecc.dao.IEccDAO;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class EntrantEduContractService implements IEntrantEduContractDataService
{
    @Override
    public ContractAttributes getEntrantContractAttributes(Long ecgpEntrantRecommendedId, Long studentId)
    {
        EcgpEntrantRecommended entrantRecommended = DataAccessServices.dao().getNotNull(ecgpEntrantRecommendedId);
        UniscEduMainAgreement contract = IEccDAO.INSTANCE.get().getAgreement(entrantRecommended.getPreStudent().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant(), entrantRecommended.getProfile().getEducationOrgUnit());
        if (null == contract) contract = IUniscEduAgreementDAO.INSTANCE.get().getStudentContract(studentId);
        if (null != contract) return new ContractAttributes(contract.getNumber(), contract.getFormingDate());
        return null;
    }

    @Override
    public Map<Long, ContractAttributes> getEntrantContractAttributes(List<CoreCollectionUtils.Pair<Long, Long>> studentIds)
    {
        List<Long> ecgpEntrantRecommendedIdList = new ArrayList<Long>();
        Map<Long, Long> studentToEntrantRecommendedMap = new HashMap<Long, Long>();
        for(CoreCollectionUtils.Pair<Long, Long> pair : studentIds)
        {
            ecgpEntrantRecommendedIdList.add(pair.getX());
            studentToEntrantRecommendedMap.put(pair.getY(), pair.getX());
        }

        Map<Long, ContractAttributes> result = new HashMap<Long, ContractAttributes>();
        for (Map.Entry<Long, UniscEduMainAgreement> entry : IEccDAO.INSTANCE.get().getAgreements(ecgpEntrantRecommendedIdList, studentToEntrantRecommendedMap).entrySet())
        {
            result.put(entry.getKey(), new ContractAttributes(entry.getValue().getNumber(), entry.getValue().getFormingDate()));
        }
        return result;
    }
}