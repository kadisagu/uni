package ru.tandemservice.uniecc.component.entrant.NextOfKinAgreementUsage;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.base.entity.gen.PersonNextOfKinGen;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import ru.tandemservice.uniecc.entity.relations.gen.NextOfKinStatusRelationGen;

public class Controller extends AbstractBusinessController<IDAO, Model> {

	@Override public void onRefreshComponent(final IBusinessComponent component) {
        Long entrantId = ((ru.tandemservice.uniec.component.entrant.EntrantPub.Model)component.getModel(component.getName())).getEntrant().getId();
		final Model model = this.getModel(component);
        model.setId(entrantId);
		if (null == model.getDataSource()) {
			final String prefix = (this.getClass().getName().equals(component.getName()) ? "" : (getItemName() + ":"));
			final DynamicListDataSource<NextOfKinStatusRelation> dataSource = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().prepareDataSource(Controller.this.getModel(component1));
            });
			dataSource.addColumn(new SimpleColumn("ФИО", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN+"."+ PersonNextOfKin.P_FULLFIO).setClickable(false).setOrderable(false));
			dataSource.addColumn(new SimpleColumn("Степень родства", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN+"."+PersonNextOfKinGen.L_RELATION_DEGREE+".title").setClickable(false).setOrderable(false));
			dataSource.addColumn(new ToggleColumn("Оплачивает", NextOfKinStatusRelationGen.P_USED_IN_AGREEMENT).setListener(prefix+"onClickUsedInAgreement").setClickable(true).setOrderable(false).setPermissionKey("editEntrantNextOfKinStatus"));
			dataSource.addColumn(new ToggleColumn("Подписывает", NextOfKinStatusRelationGen.P_SIGN_IN_AGREEMENT).setListener(prefix+"onClickSignInAgreement").setClickable(true).setOrderable(false).setPermissionKey("editEntrantNextOfKinStatus"));
			model.setDataSource(dataSource);
		}
	}

	public void onClickUsedInAgreement(final IBusinessComponent component) {
		getDao().updateUsedInAgreement(getModel(component), (Long)component.getListenerParameter());
	}
	public void onClickSignInAgreement(final IBusinessComponent component) {
		getDao().updateSignInAgreement(getModel(component), (Long)component.getListenerParameter());
	}

}
