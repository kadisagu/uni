package ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;

import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author vdanilov
 */
@Deprecated
@SuppressWarnings("deprecation")
public class Controller extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Controller<Entrant> {
    @Override
    protected void createStageScope(final IBusinessComponent component, final ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model<Entrant> model, String nextComponentName) {
        if (nextComponentName.endsWith("Stage0")) {
            nextComponentName = ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit.Stage0.Model.class.getPackage().getName();
        }
        super.createStageScope(component, model, nextComponentName);
    }

    @Override
    public void onRenderComponent(IBusinessComponent component)
    {
        ContextLocal.beginPageTitlePart(getModel(component).isCreationFormFlag() ? "Добавление договора" : "Редактирование договора");
    }
}
