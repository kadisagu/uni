package ru.tandemservice.uniecc.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author root
 */
public interface IEccNextOfKinDAO
{
    public static final SpringBeanCache<IEccNextOfKinDAO> INSTANCE = new SpringBeanCache<IEccNextOfKinDAO>(IEccNextOfKinDAO.class.getName());

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    void updateNextOfKinRelationProperty(final Long personId, final Long id, final String name, final boolean unique);
}
