// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniecc.component.report.AgreementIncomeReport.AgreementIncomeReportList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisc.entity.report.UniscAgreementIncomeReport;

/**
 * @author oleyba
 * @since 20.08.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (OrgUnitManager.instance().dao().getTopOrgUnitId().equals(model.getOrgUnitId()))
            model.setOrgUnitId(null);
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit) getNotNull(model.getOrgUnitId()));
            model.setSecModel(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(UniscAgreementIncomeReport.ENTITY_CLASS, "report");
        if (model.getOrgUnitId() != null)
            builder.add(MQExpression.eq("report", UniscAgreementIncomeReport.L_ORG_UNIT + ".id", model.getOrgUnitId()));
        else
            builder.add(MQExpression.isNull("report", UniscAgreementIncomeReport.L_ORG_UNIT));
        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}

