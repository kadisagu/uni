package ru.tandemservice.uniecc.entity.relations.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка ближайших родственников
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NextOfKinStatusRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation";
    public static final String ENTITY_NAME = "nextOfKinStatusRelation";
    public static final int VERSION_HASH = 1952297270;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON_NEXT_OF_KIN = "personNextOfKin";
    public static final String P_USED_IN_AGREEMENT = "usedInAgreement";
    public static final String P_SIGN_IN_AGREEMENT = "signInAgreement";

    private PersonNextOfKin _personNextOfKin;     // направление подготовки
    private boolean _usedInAgreement;     // Фигурирует в договоре
    private boolean _signInAgreement;     // Подписывает договор

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return направление подготовки. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonNextOfKin getPersonNextOfKin()
    {
        return _personNextOfKin;
    }

    /**
     * @param personNextOfKin направление подготовки. Свойство не может быть null и должно быть уникальным.
     */
    public void setPersonNextOfKin(PersonNextOfKin personNextOfKin)
    {
        dirty(_personNextOfKin, personNextOfKin);
        _personNextOfKin = personNextOfKin;
    }

    /**
     * @return Фигурирует в договоре. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsedInAgreement()
    {
        return _usedInAgreement;
    }

    /**
     * @param usedInAgreement Фигурирует в договоре. Свойство не может быть null.
     */
    public void setUsedInAgreement(boolean usedInAgreement)
    {
        dirty(_usedInAgreement, usedInAgreement);
        _usedInAgreement = usedInAgreement;
    }

    /**
     * @return Подписывает договор. Свойство не может быть null.
     */
    @NotNull
    public boolean isSignInAgreement()
    {
        return _signInAgreement;
    }

    /**
     * @param signInAgreement Подписывает договор. Свойство не может быть null.
     */
    public void setSignInAgreement(boolean signInAgreement)
    {
        dirty(_signInAgreement, signInAgreement);
        _signInAgreement = signInAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NextOfKinStatusRelationGen)
        {
            setPersonNextOfKin(((NextOfKinStatusRelation)another).getPersonNextOfKin());
            setUsedInAgreement(((NextOfKinStatusRelation)another).isUsedInAgreement());
            setSignInAgreement(((NextOfKinStatusRelation)another).isSignInAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NextOfKinStatusRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NextOfKinStatusRelation.class;
        }

        public T newInstance()
        {
            return (T) new NextOfKinStatusRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "personNextOfKin":
                    return obj.getPersonNextOfKin();
                case "usedInAgreement":
                    return obj.isUsedInAgreement();
                case "signInAgreement":
                    return obj.isSignInAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "personNextOfKin":
                    obj.setPersonNextOfKin((PersonNextOfKin) value);
                    return;
                case "usedInAgreement":
                    obj.setUsedInAgreement((Boolean) value);
                    return;
                case "signInAgreement":
                    obj.setSignInAgreement((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "personNextOfKin":
                        return true;
                case "usedInAgreement":
                        return true;
                case "signInAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "personNextOfKin":
                    return true;
                case "usedInAgreement":
                    return true;
                case "signInAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "personNextOfKin":
                    return PersonNextOfKin.class;
                case "usedInAgreement":
                    return Boolean.class;
                case "signInAgreement":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NextOfKinStatusRelation> _dslPath = new Path<NextOfKinStatusRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NextOfKinStatusRelation");
    }
            

    /**
     * @return направление подготовки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation#getPersonNextOfKin()
     */
    public static PersonNextOfKin.Path<PersonNextOfKin> personNextOfKin()
    {
        return _dslPath.personNextOfKin();
    }

    /**
     * @return Фигурирует в договоре. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation#isUsedInAgreement()
     */
    public static PropertyPath<Boolean> usedInAgreement()
    {
        return _dslPath.usedInAgreement();
    }

    /**
     * @return Подписывает договор. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation#isSignInAgreement()
     */
    public static PropertyPath<Boolean> signInAgreement()
    {
        return _dslPath.signInAgreement();
    }

    public static class Path<E extends NextOfKinStatusRelation> extends EntityPath<E>
    {
        private PersonNextOfKin.Path<PersonNextOfKin> _personNextOfKin;
        private PropertyPath<Boolean> _usedInAgreement;
        private PropertyPath<Boolean> _signInAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return направление подготовки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation#getPersonNextOfKin()
     */
        public PersonNextOfKin.Path<PersonNextOfKin> personNextOfKin()
        {
            if(_personNextOfKin == null )
                _personNextOfKin = new PersonNextOfKin.Path<PersonNextOfKin>(L_PERSON_NEXT_OF_KIN, this);
            return _personNextOfKin;
        }

    /**
     * @return Фигурирует в договоре. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation#isUsedInAgreement()
     */
        public PropertyPath<Boolean> usedInAgreement()
        {
            if(_usedInAgreement == null )
                _usedInAgreement = new PropertyPath<Boolean>(NextOfKinStatusRelationGen.P_USED_IN_AGREEMENT, this);
            return _usedInAgreement;
        }

    /**
     * @return Подписывает договор. Свойство не может быть null.
     * @see ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation#isSignInAgreement()
     */
        public PropertyPath<Boolean> signInAgreement()
        {
            if(_signInAgreement == null )
                _signInAgreement = new PropertyPath<Boolean>(NextOfKinStatusRelationGen.P_SIGN_IN_AGREEMENT, this);
            return _signInAgreement;
        }

        public Class getEntityClass()
        {
            return NextOfKinStatusRelation.class;
        }

        public String getEntityName()
        {
            return "nextOfKinStatusRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
