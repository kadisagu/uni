package ru.tandemservice.uniecc.component.report.AgreementIncomeReport;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.UniscDefines;
import ru.tandemservice.unisc.component.report.AgreementIncomeReport.AgreementIncomeReportRow;
import ru.tandemservice.unisc.component.report.PaymentReport.MQBuilderFactory;
import ru.tandemservice.unisc.component.report.UniscRtfUtils;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementPayFactRow;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

import java.util.*;

/**
 * @author oleyba
 * @since 25.08.2009
 */
public class AgreementIncomeReportByGroupGenerator
{
    protected static final String NO_GROUP_ENTRANTS = "вне групп (а)";
    protected static final String NO_GROUP = "вне групп";

    protected Session session;
    protected Long orgUnitId;
    protected MQBuilderFactory agreementBuilder;
    protected Date dateFrom;
    protected Date dateTo;

    protected Map<Long, Student> studentByAgreementId = new HashMap<>();
    protected Map<Long, UniscEduAgreement2Entrant> entrantByAgreementId = new HashMap<>();
    protected Map<Long, ReportRow> reportRowByGroupId = new HashMap<>();
    protected Map<String, ReportRow> reportRowByGroupTitle = new HashMap<>();
    protected Map<Long, ReportRow> reportRowByAgreementId = new HashMap<Long, ReportRow>()
    {
        private static final long serialVersionUID = 0L;

        @Override
        public ReportRow get(Object key)
        {
            ReportRow row;
            Student student = studentByAgreementId.get(key);
            if (student != null)
            {
                Long groupId = student.getGroup() == null ? null : student.getGroup().getId();
                String groupTitle = student.getGroup() == null ? NO_GROUP : student.getGroup().getTitle();
                row = reportRowByGroupId.get(groupId);
                if (row == null) reportRowByGroupId.put(groupId, row = new ReportRow(groupTitle));
            }
            else
            {
                final UniscEduAgreement2Entrant entrant = entrantByAgreementId.get(key);
                if (entrant == null) return null;
                String groupId = entrant.getGroup() == null ? NO_GROUP_ENTRANTS : entrant.getGroup();
                row = reportRowByGroupTitle.get(groupId);
                if (row == null) reportRowByGroupTitle.put(groupId, row = new ReportRow(groupId));
            }
            return row;
        }
    };
    protected Map<MultiKey, AgreementIncomeReportRow.RowGroup> groupMap = new HashMap<>();
    protected List<Integer> months = new ArrayList<>();


    public AgreementIncomeReportByGroupGenerator(Session session, Long orgUnitId, MQBuilderFactory agreementBuilder, Date dateFrom, Date dateTo)
    {
        this.session = session;
        this.orgUnitId = orgUnitId;
        this.agreementBuilder = agreementBuilder;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        for (int month = CommonBaseDateUtil.getMonthStartingWithOne(dateFrom); month <= CommonBaseDateUtil.getMonthStartingWithOne(dateTo); month++)
            months.add(month);
    }

    public DatabaseFile generateReportContent()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniscTemplateDocument.class, UniscDefines.TEMPLATE_AGREEMENT_INCOME_REPORT_BY_GROUP);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        fillStudentMap();
        fillEntrantMap();
        fillPaymentMap();

        final List<String[]> reportRowList = new ArrayList<>();
        List<AgreementIncomeReportRow.RowGroup> groups = new ArrayList<>(groupMap.values());
        Collections.sort(groups, AgreementIncomeReportRow.RowGroup.comparator);
        long total = 0L;
        for (AgreementIncomeReportRow.RowGroup group : groups)
            total = total + group.appendRows(reportRowList, months, ReportRow.getComparator(), 1);

        modify(document, reportRowList, total);

        byte[] data = RtfUtil.toByteArray(document);
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected void modify(RtfDocument document, final List<String[]> reportRowList, long total)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", reportRowList.toArray(new String[reportRowList.size()][]));
        final int[] parts = new int[months.size()];
        Arrays.fill(parts, 1);

        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 1, (newCell, index) -> {
                    String title = StringUtils.capitalize(RussianDateFormatUtils.getMonthName(months.get(index), true));
                    newCell.setElementList(Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(title)));
                }, parts);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 1, null, parts);
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                UniscRtfUtils.afterModify(newRowList, startIndex, reportRowList);
            }
        });
        tableModifier.modify(document);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("periodReport", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo));
        OrgUnit orgUnit = orgUnitId == null ? null : UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId);
        injectModifier.put("scope", orgUnit == null ? "ИТОГО по всему ОУ" : "ИТОГО по поздразделению (" + (orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle()) + ")");
        injectModifier.put("total", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(0.001d * total));
        injectModifier.modify(document);

    }

    protected void fillPaymentMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreementPayFactRow.ENTITY_CLASS, "p");
        builder.add(MQExpression.in("p", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("p", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));
        builder.addOrder("p", UniscEduAgreementPayFactRow.P_DATE);
        builder.addLeftJoinFetch("p", UniscEduAgreementPayFactRow.L_TYPE, "type_fetch");
        builder.addLeftJoinFetch("p", UniscEduAgreementPayFactRow.L_AGREEMENT, "agr_fetch");
        builder.addLeftJoinFetch("agr_fetch", UniscEduMainAgreement.L_CONFIG, "conf_fetch");
        builder.addLeftJoinFetch("conf_fetch", UniscEduOrgUnit.L_EDUCATION_ORG_UNIT, "agr_edu_ou_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, "agr_form_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "agr_terr_fetch");
        builder.addLeftJoinFetch("agr_edu_ou_fetch", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "agr_hs_fetch");
        builder.addLeftJoinFetch("agr_hs_fetch", EducationLevelsHighSchool.L_EDUCATION_LEVEL, "agr_lev_fetch");
        MQBuilder groupArchive = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel_g");
        groupArchive.add(MQExpression.eqProperty("rel_g", UniscEduAgreement2Student.L_AGREEMENT, "p", UniscEduAgreementPayFactRow.L_AGREEMENT));
        groupArchive.add(MQExpression.eq("rel_g", UniscEduAgreement2Student.L_STUDENT + "." + Group.P_ARCHIVAL, Boolean.TRUE));
        builder.add(MQExpression.notExists(groupArchive));

        for (UniscEduAgreementPayFactRow payment : builder.<UniscEduAgreementPayFactRow>getResultList(session))
        {
            ReportRow reportRow = reportRowByAgreementId.get(payment.getAgreement().getId());
            if (reportRow == null) continue;
            reportRow.addPayment(payment);
            getRowGroup(payment).addRow(reportRow);
        }
    }

    private AgreementIncomeReportRow.RowGroup getRowGroup(UniscEduAgreementPayFactRow payment)
    {
        EducationOrgUnit eduOu = payment.getAgreement().getConfig().getEducationOrgUnit();
        MultiKey key = new MultiKey(eduOu.getFormativeOrgUnit().getId(), eduOu.getTerritorialOrgUnit().getId());
        AgreementIncomeReportRow.RowGroup rowGroup = groupMap.get(key);
        if (rowGroup == null)
            groupMap.put(key, rowGroup = new AgreementIncomeReportRow.RowGroup(eduOu));
        return rowGroup;
    }

    protected void fillStudentMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Student.ENTITY_CLASS, "rel");
        builder.addLeftJoinFetch("rel", UniscEduAgreement2Student.L_STUDENT, "student_fetch");
        builder.addLeftJoinFetch("student_fetch", Student.L_GROUP, "group_fetch");
        builder.add(MQExpression.eq("group_fetch", Group.P_ARCHIVAL, Boolean.FALSE));
        builder.addDomain("pay", UniscEduAgreementPayFactRow.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
        builder.add(MQExpression.in("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));

        for (UniscEduAgreement2Student rel : builder.<UniscEduAgreement2Student>getResultList(session))
            studentByAgreementId.put(rel.getAgreement().getId(), rel.getStudent());
    }

    protected void fillEntrantMap()
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreement2Entrant.ENTITY_CLASS, "rel");
        builder.addDomain("pay", UniscEduAgreementPayFactRow.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", "rel", UniscEduAgreement2Student.L_AGREEMENT + ".id"));
        builder.add(MQExpression.in("pay", UniscEduAgreementPayFactRow.L_AGREEMENT + ".id", agreementBuilder.createIdBuilder()));
        builder.add(UniMQExpression.betweenDate("pay", UniscEduAgreementPayFactRow.P_DATE, dateFrom, dateTo));

        for (UniscEduAgreement2Entrant rel : builder.<UniscEduAgreement2Entrant>getResultList(session))
            entrantByAgreementId.put(rel.getAgreement().getId(), rel);
    }

    protected static class ReportRow extends AgreementIncomeReportRow
    {
        private String title;


        public ReportRow(String group)
        {
            this.title = group;
        }

        public static Comparator<AgreementIncomeReportRow> getComparator()
        {
            return (o1, o2) -> {
                if (((ReportRow)o1).title.equals(NO_GROUP_ENTRANTS)) return -1;
                if (((ReportRow)o2).title.equals(NO_GROUP_ENTRANTS)) return 1;
                if (((ReportRow)o1).title.equals(NO_GROUP)) return -1;
                if (((ReportRow)o2).title.equals(NO_GROUP)) return 1;
                return ((ReportRow)o1).title.compareTo(((ReportRow)o2).title);
            };
        }

        @Override
        public List<String> getTitles()
        {
            return Arrays.asList(title);
        }

        public long append(List<String[]> reportRowList, Set<Integer> months)
        {
            List<String> result = new ArrayList<>();
            result.addAll(getTitles());
            for (int month = 1; month <= 12; month++)
                if (months.contains(month))
                    result.add(getSum(month));
            result.add(getSum(null));
            reportRowList.add(result.toArray(new String[result.size()]));
            return getTotal();
        }
    }
}