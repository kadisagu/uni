// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniecc.component.entrant.EntrantGroupEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;

/**
 * @author oleyba
 * @since 25.08.2009
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "relation.id")
public class Model
{
    private UniscEduAgreement2Entrant relation = new UniscEduAgreement2Entrant();

    public UniscEduAgreement2Entrant getRelation()
    {
        return relation;
    }

    public void setRelation(UniscEduAgreement2Entrant relation)
    {
        this.relation = relation;
    }
}
