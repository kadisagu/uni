/* $Id: Model.java 8355 2009-06-08 08:23:06Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.settings.EccEnrollmentCampaign;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniecc.entity.relations.EccEnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 07.04.2011
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private boolean _hasPreStudent;
    private EccEnrollmentCampaign _configDTO;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _settings.get(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(final EnrollmentCampaign enrollmentCampaign)
    {
        _settings.set(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public boolean isHasPreStudent()
    {
        return _hasPreStudent;
    }

    public void setHasPreStudent(boolean hasPreStudent)
    {
        _hasPreStudent = hasPreStudent;
    }

    public EccEnrollmentCampaign getConfigDTO()
    {
        return _configDTO;
    }

    public void setConfigDTO(EccEnrollmentCampaign configDTO)
    {
        _configDTO = configDTO;
    }
}
