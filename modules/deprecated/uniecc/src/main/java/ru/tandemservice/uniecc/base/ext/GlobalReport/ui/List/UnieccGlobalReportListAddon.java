/* $Id$ */
package ru.tandemservice.uniecc.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author azhebko
 * @since 31.03.2014
 */
public class UnieccGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "unieccGlobalReportListAddon";

    public UnieccGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}