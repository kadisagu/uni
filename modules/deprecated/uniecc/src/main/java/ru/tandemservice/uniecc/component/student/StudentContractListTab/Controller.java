package ru.tandemservice.uniecc.component.student.StudentContractListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickConnectEntrantAgreement(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniecc.component.student.ConnectStudentContractFromEntrant"));
    }

    public void onClickDisconnectEntrantAgreement(IBusinessComponent component)
    {
        getDao().doDisconnect(getModel(component));
        onRefreshComponent(component);
    }
}
