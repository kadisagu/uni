package ru.tandemservice.uniecc.component.entrant.EntrantAgreementPub;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;

public class DAO extends UniDao<Model> implements IDAO {
	@Override
	public void prepare(Model model) {
		if (null != model.getRelation().getId()) {
			model.setRelation(get(UniscEduAgreement2Entrant.class, model.getRelation().getId()));
            final IUniscEduAgreement2PersonRole<PersonRole> relation = IUniscEduAgreementDAO.INSTANCE.get().getRelation(model.getRelation().getAgreement());
            model.setRelatedWithStudent(!model.getRelation().getTarget().equals(relation.getTarget()));
		}
	}
}
