package ru.tandemservice.uniecc.component.report;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisc.component.report.IUniscReport;

import java.util.*;

/**
 * @author oleyba
 * @since 20.08.2009
 */
public abstract class UnieccReportAddModel
{
    public static final long SELECT_ALL = 0L;
    public static final long SELECT_STUDENTS = 1L;
    public static final long SELECT_ENTRANTS = 2L;

    private Long orgUnitId;

    private Map<String, GroupWrapper> entrantGroupMapByTitle = new HashMap<String, GroupWrapper>();

    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _educationLevelHighSchoolActive;
    private boolean _developFormActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;
    private boolean _courseActive;
    private boolean _groupActive;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _courseModel;
    private ISelectModel _groupModel;
    private List<IdentifiableWrapper> selectionList = Arrays.asList(
            new IdentifiableWrapper(SELECT_ALL, "по студентам и абитуриентам"),
            new IdentifiableWrapper(SELECT_STUDENTS, "только по студентам"),
            new IdentifiableWrapper(SELECT_ENTRANTS, "только по абитуриентам"));

    private List<OrgUnit> _formativeOrgUnitList = new ArrayList<OrgUnit>();
    private List<OrgUnit> _territorialOrgUnitList = new ArrayList<OrgUnit>();
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList = new ArrayList<EducationLevelsHighSchool>();
    private List<DevelopForm> _developFormList = new ArrayList<DevelopForm>();
    private List<DevelopCondition> _developConditionList = new ArrayList<DevelopCondition>();
    private List<DevelopTech> _developTechList = new ArrayList<DevelopTech>();
    private List<DevelopPeriod> _developPeriodList = new ArrayList<DevelopPeriod>();
    private Course course;
    private GroupWrapper group;
    private IdentifiableWrapper selection = new IdentifiableWrapper(SELECT_ALL, "по студентам и абитуриентам");

    public static class GroupWrapper extends IdentifiableWrapper<Group>
    {
        private static final long serialVersionUID = 5614540633857469540L;
        private boolean student;

        public GroupWrapper(Long id, String title, boolean student)
        {
            super(id, title);
            this.student = student;
        }

        public boolean isStudent()
        {
            return student;
        }
    }

    public void fillReportParams(IUniscReport report)
    {
        report.setFormingDate(new Date());

        if (isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE), "; "));
        if (isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE), "; "));
        if (isEducationLevelHighSchoolActive())
            report.setEduLevelTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getEducationLevelHighSchoolList(), "fullTitle"), "; "));
        if (isDevelopFormActive())
            report.setDevelopFormTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getDevelopFormList(), "title"), "; "));
        if (isDevelopConditionActive())
            report.setDevelopConditionTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getDevelopConditionList(), "title"), "; "));
        if (isDevelopTechActive())
            report.setDevelopTechTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getDevelopTechList(), "title"), "; "));
        if (isDevelopPeriodActive())
            report.setDevelopPeriodTitle(StringUtils.join(CommonBaseUtil.getPropertiesList(getDevelopPeriodList(), "title"), "; "));
        if (isCourseActive())
            report.setCourse(getCourse().getTitle());
        if (getOrgUnitId() !=null)
            report.setOrgUnit(UniDaoFacade.getCoreDao().get(OrgUnit.class, getOrgUnitId()));
        if (isGroupActive())
            report.setGroup(getGroup().getTitle());
        report.setSelection(getSelection().getTitle());
    }

    public boolean isCourseDisabled()
    {
        return SELECT_ENTRANTS == getSelection().getId();
    }

    public boolean isGroupDisabled()
    {
        return false;
    }

    // UnieccReportAddDaoUtil

    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    // accessors

    public List<IdentifiableWrapper> getSelectionList()
    {
        return selectionList;
    }

    public void setSelectionList(List<IdentifiableWrapper> selectionList)
    {
        this.selectionList = selectionList;
    }

    public IdentifiableWrapper getSelection()
    {
        return selection;
    }

    public void setSelection(IdentifiableWrapper selection)
    {
        this.selection = selection;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelHighSchoolModel()
    {
        return _educationLevelHighSchoolModel;
    }

    public void setEducationLevelHighSchoolModel(ISelectModel educationLevelHighSchoolModel)
    {
        _educationLevelHighSchoolModel = educationLevelHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel() { return _developFormModel; }
    public void setDevelopFormModel(ISelectModel developFormModel) { _developFormModel = developFormModel; }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopTechModel() { return _developTechModel; }
    public void setDevelopTechModel(ISelectModel developTechModel) { _developTechModel = developTechModel; }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public ISelectModel getCourseModel()
    {
        return _courseModel;
    }

    public void setCourseModel(ISelectModel courseModel)
    {
        _courseModel = courseModel;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public Course getCourse()
    {
        return course;
    }

    public void setCourse(Course course)
    {
        this.course = course;
    }

    public boolean isCourseActive()
    {
        return _courseActive && (SELECT_ENTRANTS != getSelection().getId());
    }

    public void setCourseActive(boolean courseActive)
    {
        _courseActive = courseActive;
    }

    public boolean isGroupActive()
    {
        return _groupActive;
    }

    public void setGroupActive(boolean groupActive)
    {
        _groupActive = groupActive;
    }

    public GroupWrapper getGroup()
    {
        return group;
    }

    public void setGroup(GroupWrapper group)
    {
        this.group = group;
    }

    public Map<String, GroupWrapper> getEntrantGroupMapByTitle()
    {
        return entrantGroupMapByTitle;
    }

    public void setEntrantGroupMapByTitle(Map<String, GroupWrapper> entrantGroupMapByTitle)
    {
        this.entrantGroupMapByTitle = entrantGroupMapByTitle;
    }
}