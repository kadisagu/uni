package ru.tandemservice.uniecc.entity.agreements.entrant;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;

/**
 * Договор с абитуриентом
 */
public class UniscEduAgreement2Entrant extends UniscEduAgreement2EntrantGen implements IUniscEduAgreement2PersonRole<Entrant>
{
    @Override
    public String getPermissionContext()
    {
        return "Entrant";
    }

    @Override
    public Entrant getTarget()
    {
        return getEntrant();
    }

    @Override
    public String getTitle()
    {
        return "Договор с абитуриентом №" + getAgreement().getNumber() + " " + getEntrant().getPerson().getFullFio() + " " + getEntrant().getEnrollmentCampaign().getTitle();
    }
}
