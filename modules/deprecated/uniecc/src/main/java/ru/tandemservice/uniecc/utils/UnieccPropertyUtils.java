package ru.tandemservice.uniecc.utils;

import ru.tandemservice.unisc.utils.UniscPropertyUtils;

/**
 * @author vdanilov
 */
public class UnieccPropertyUtils extends UniscPropertyUtils
{
    /**
     * выбирать только те направления, которые являются направлениями приема в рамках указанной приемной кампании
     */
    public static final boolean STRICT_DIRECTION = Boolean.valueOf(UniscPropertyUtils.get("uniecc.constraint.strict-direction", Boolean.TRUE.toString()).toLowerCase()).booleanValue();
}
