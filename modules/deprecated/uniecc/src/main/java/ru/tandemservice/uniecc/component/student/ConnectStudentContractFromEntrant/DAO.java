package ru.tandemservice.uniecc.component.student.ConnectStudentContractFromEntrant;

import java.util.List;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;
import ru.tandemservice.unisc.entity.agreements.student.gen.UniscEduAgreement2StudentGen;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {

    }

    @Override
    public void refreshDataSource(final Model model)
    {
        final Student student = this.get(Student.class, model.getId());
        final MQBuilder builder = new MQBuilder(UniscEduAgreement2EntrantGen.ENTITY_CLASS, "a2e");
        builder.add(MQExpression.eq("a2e", UniscEduAgreement2EntrantGen.entrant().person().toString(), student.getPerson()));
        builder.add(MQExpression.notIn("a2e", UniscEduAgreement2EntrantGen.agreement().id().s(), new MQBuilder(UniscEduAgreement2StudentGen.ENTITY_CLASS, "a2s", new String[]{UniscEduAgreement2StudentGen.agreement().id().s()})));

        final List<UniscEduAgreement2Entrant> resultList = builder.getResultList(this.getSession());
        final DynamicListDataSource<UniscEduAgreement2Entrant> dataSource = model.getDataSource();
        dataSource.setTotalSize(resultList.size());
        dataSource.setCountRow(resultList.size());
        dataSource.createPage(resultList);
    }

    @Override
    public void save(final Model model)
    {
        final Student student = this.get(Student.class, model.getId());
        final UniscEduAgreement2Entrant e = (UniscEduAgreement2Entrant) ((RadioButtonColumn) model.getDataSource().getColumn("select")).getSelectedEntity();
        if (null != e)
        {
            synchronized (String.valueOf(student.getId()).intern())
            {
                if (!getList(UniscEduAgreement2Student.class, UniscEduAgreement2Student.L_STUDENT, student).isEmpty())
                {
                    throw new ApplicationException("Для данного студента уже существует договор.");
                }

                final UniscEduMainAgreement agreement = e.getAgreement();
                final UniscEduAgreement2Student a2s = new UniscEduAgreement2Student();
                a2s.setActive(true);
                a2s.setAgreement(agreement);
                a2s.setStudent(student);
                this.save(a2s);
                getSession().flush();
            }
        }
    }
}
