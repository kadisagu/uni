package ru.tandemservice.uniecc.component.student.StudentContractListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long id;

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    private boolean connectButtonVisible;

    public boolean isConnectButtonVisible()
    {
        return this.connectButtonVisible;
    }

    public void setConnectButtonVisible(boolean visible)
    {
        this.connectButtonVisible = visible;
    }

    private boolean disconnectButtonVisible;

    public boolean isDisconnectButtonVisible()
    {
        return this.disconnectButtonVisible;
    }

    public void setDisconnectButtonVisible(boolean visible)
    {
        this.disconnectButtonVisible = visible;
    }
}
