package ru.tandemservice.uniecc.component.entrant.EntrantAgreementAddEdit;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.gen.PersonNextOfKinGen;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.gen.UniscEduAgreement2EntrantGen;
import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;
import ru.tandemservice.uniecc.entity.relations.gen.NextOfKinStatusRelationGen;
import ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.Model;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.IUniscEduAgreement2PersonRole;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.gen.UniscEduAgreementBaseGen;

/**
 * @author vdanilov
 */
@Deprecated
@SuppressWarnings("deprecation")
public class DAO extends ru.tandemservice.unisc.component.agreement.AgreementRelationAddEdit.DAO<Entrant> {

    @Override
    public void prepare(final Model<Entrant> model) {
        super.prepare(model);

        if (null != model.getRelation().getId()) {
            final IUniscEduAgreement2PersonRole<PersonRole> relation = IUniscEduAgreementDAO.INSTANCE.get().getRelation(model.getAgreement());
            if (!model.getRelation().getTarget().equals(relation.getTarget())) {
                throw new ApplicationException("Вы не можете изменять договор, поскольку он уже связан со студентом.");
            }
            if (model.getRelation().getTarget().isArchival())
                throw new ApplicationException("Вы не можете изменять договор архивного абитуриента.");
            if (model.getRelation().getAgreement().isCheckedBySignature())
                throw new ApplicationException("Вы не можете изменять договор, поскольку он уже проверен.");
        }
    }

    @Override
    protected IUniscEduAgreement2PersonRole<Entrant> createRelation(final Entrant personRole) {
        final UniscEduAgreement2Entrant relation = new UniscEduAgreement2Entrant();
        relation.setAgreement(new UniscEduMainAgreement());
        relation.setEntrant(personRole);
        return relation;
    }

    @Override
    protected void refreshAgreementAfterStageExecution(final Model<Entrant> model) {
        if ((null != model.getAgreement().getConfig()) && (null == model.getAgreement().getId())) {
            final MQBuilder builder = new MQBuilder(UniscEduAgreement2EntrantGen.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.L_ENTRANT+".id", model.getRelation().getTarget().getId()));
            builder.add(MQExpression.eq("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.L_CONFIG+".id", model.getAgreement().getConfig().getId()));
            builder.addDescOrder("e", UniscEduAgreement2EntrantGen.L_AGREEMENT+"."+UniscEduAgreementBaseGen.P_FORMING_DATE);
            final List<UniscEduAgreement2EntrantGen> result = builder.getResultList(this.getSession());
            if (!result.isEmpty()) {
                model.setId(result.get(0).getId());
                this.prepare(model);
                return;
            }
        }
        super.refreshAgreementAfterStageExecution(model);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UniscEduAgreementNaturalPerson> getDefaultNaturalPersonList(final Model model) {
        return _getDefaultNaturalPersonList(model);
    }

    @SuppressWarnings("unchecked")
    protected List<UniscEduAgreementNaturalPerson> _getDefaultNaturalPersonList(final Model model) {
        final MQBuilder builder = new MQBuilder(NextOfKinStatusRelationGen.ENTITY_CLASS, "e");
        builder.add(MQExpression.or(
            MQExpression.eq("e", NextOfKinStatusRelationGen.P_USED_IN_AGREEMENT, Boolean.TRUE),
            MQExpression.eq("e", NextOfKinStatusRelationGen.P_SIGN_IN_AGREEMENT, Boolean.TRUE)
        ));
        builder.addJoin("e", NextOfKinStatusRelationGen.L_PERSON_NEXT_OF_KIN, "k");
        builder.add(MQExpression.eq("k", PersonNextOfKinGen.L_PERSON+".id", model.getPersonRole().getPerson().getId()));
        builder.addOrder("k", PersonNextOfKinGen.L_RELATION_DEGREE+".code");
        builder.addOrder("k", PersonNextOfKinGen.P_LAST_NAME);
        builder.addOrder("k", PersonNextOfKinGen.P_FIRST_NAME);
        builder.addOrder("k", PersonNextOfKinGen.P_MIDDLE_NAME);
        return new ArrayList<>(CollectionUtils.collect(builder.getResultList(DAO.this.getSession()), new Transformer()
        {
            private boolean isSignedByAnybody = false;

            @Override
            public Object transform(final Object input)
            {
                final NextOfKinStatusRelation relation = (NextOfKinStatusRelation) input;
                final UniscEduAgreementNaturalPerson np = new UniscEduAgreementNaturalPerson().from(relation.getPersonNextOfKin());
                if (!isSignedByAnybody)
                {
                    np.setSignInAgreement(relation.isSignInAgreement());
                    isSignedByAnybody |= np.isSignInAgreement();
                }
                np.setUsedInAgreement(relation.isUsedInAgreement());
                return np;
            }
        }));
    }

}
