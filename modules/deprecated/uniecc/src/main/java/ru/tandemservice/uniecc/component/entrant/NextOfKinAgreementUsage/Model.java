package ru.tandemservice.uniecc.component.entrant.NextOfKinAgreementUsage;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniecc.entity.relations.NextOfKinStatusRelation;

public class Model {
	private Long id;
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	
	private DynamicListDataSource<NextOfKinStatusRelation> dataSource;
	public DynamicListDataSource<NextOfKinStatusRelation> getDataSource() { return dataSource; }
	public void setDataSource(DynamicListDataSource<NextOfKinStatusRelation> dataSource) { this.dataSource = dataSource; }
}
