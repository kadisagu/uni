/* $Id: Model.java 8355 2009-06-08 08:23:06Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniecc.component.entrant.EntrantContractListTab.EntrantAllContractsListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;

/**
 * @author vdanilov
 */
@State(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
	private Long id;
	public Long getId() { return id; }
	public void setId(final Long id) { this.id = id; }

	private DynamicListDataSource<UniscEduAgreement2Entrant> _agreementsDataSource;
	public DynamicListDataSource<UniscEduAgreement2Entrant> getEntrantContractsDataSource() { return _agreementsDataSource; }
	public void setEntrantContractsDataSource(final DynamicListDataSource<UniscEduAgreement2Entrant> studentContractsDataSource) { _agreementsDataSource = studentContractsDataSource; }

    private Entrant entrant;
    public Entrant getEntrant() { return entrant; }
    public void setEntrant(Entrant entrant) { this.entrant = entrant; }

    public boolean isReadOnly() { return entrant.isArchival(); }
}
