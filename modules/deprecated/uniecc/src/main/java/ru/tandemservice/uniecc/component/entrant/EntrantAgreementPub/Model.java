package ru.tandemservice.uniecc.component.entrant.EntrantAgreementPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;

@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="relation.id"),
    @Bind(key="selectedTab", binding="selectedTab")
})
public class Model  {

    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(final String selectedTab) { this.selectedTab = selectedTab; }

    private UniscEduAgreement2Entrant relation = new UniscEduAgreement2Entrant();
    public UniscEduAgreement2Entrant getRelation() { return relation; }
    public void setRelation(final UniscEduAgreement2Entrant relation) { this.relation = relation; }

    private boolean relatedWithStudent;

    public boolean isRelatedWithStudent() { return relatedWithStudent; }
    public void setRelatedWithStudent(boolean relatedWithStudent) { this.relatedWithStudent = relatedWithStudent; }

    public boolean isReadOnly() {
        if (getRelation().getEntrant().isArchival())
            return true;
        if (getRelation().getAgreement().isCheckedBySignature())
            return true;
        if (isRelatedWithStudent())
            return true;
        return (!IUniscEduAgreementDAO.INSTANCE.get().isMutable(getRelation().getAgreement()));
    }

    public Entrant getEntrant() { return getRelation().getEntrant(); }

}
