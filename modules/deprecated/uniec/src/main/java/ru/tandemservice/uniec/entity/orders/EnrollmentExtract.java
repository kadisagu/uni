package ru.tandemservice.uniec.entity.orders;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.movestudent.utils.ICommentableExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.dao.IGeneratedCommentForEntrantOrders;
import ru.tandemservice.uniec.entity.orders.gen.EnrollmentExtractGen;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;

/**
 * Выписка приказа о зачислении абитуриентов
 */
public abstract class EnrollmentExtract extends EnrollmentExtractGen implements ICommentableExtract, IEnrollmentExtract
{
    @Override
    public void doExtractPrint(boolean printPdf)
    {
        if (!canBePrinted())
            throw new ApplicationException("Выписка не может быть распечатана.");
        EcOrderManager.instance().dao().getDownloadExtractPrintForm(getId(), printPdf);
    }

    @Override
    public void doOrderPrint(boolean printPdf)
    {
        if (!canBePrinted())
            throw new ApplicationException("Приказ не может быть распечатан.");
        EcOrderManager.instance().dao().getDownloadPrintForm(getOrder().getId(), printPdf);
    }

    @Override
    public String getPrintScriptCatalogCode()
    {
        return UniecDefines.SCRIPT_ENROLLMENT_EXTRACT;
    }

    // Генерируемое примечание приказа
    public String getGeneratedComment(boolean isPrintOrderData)
    {
        final String methodName = "getComment";
        try
        {
            Method method = IGeneratedCommentForEntrantOrders.instance.get().getClass().getMethod(methodName, new Class[]{this.getClass(), boolean.class});
            IGeneratedCommentForEntrantOrders obj = IGeneratedCommentForEntrantOrders.instance.get();
            return (String) method.invoke(obj, this, isPrintOrderData);
        }
        catch (NoSuchMethodException e1)
        {
            String orderStr = getType().getTitle();

            if (isPrintOrderData && null != this.getParagraph() && null != this.getParagraph().getOrder())
            {
                AbstractEntrantOrder order = this.getParagraph().getOrder();
                if (null != order.getNumber())
                    orderStr += ", приказ № " + order.getNumber() + (order.getCommitDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) + " года." : "");
                else if (order.getCommitDate() != null)
                    orderStr += ", приказ от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate());
            }
            return orderStr;
        }
        catch (InvocationTargetException | IllegalAccessException e2)
        {
            throw new RuntimeException(e2);
        }
    }

    @Override
    public Date getEntranceDate()
    {
        if (getOrder().getEnrollmentDate() != null)
            return getOrder().getEnrollmentDate();

        // Если дата зачиелния не задана - зачисление с 1 сентября
        Calendar calendar = Calendar.getInstance();
        calendar.set(getOrder().getEnrollmentCampaign().getEducationYear().getIntValue(), Calendar.SEPTEMBER, 1);
        return calendar.getTime();
    }

    @Override
    public Group getGroupNew()
    {
        return null;
    }
}