/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryInternalStateExam.EntrantRegistryInternalStateExamAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SafeMap.Callback;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.bo.util.AddressInterTitleBuilder;
import org.tandemframework.shared.fias.base.bo.util.AddressRuTitleBuilder;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil.DetailLevel;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author agolubenko
 * @since Jul 1, 2010
 */
public class EntrantRegistryInternalStateExamReportBuilder
{
    private Model _model;
    private Session _session;

    public EntrantRegistryInternalStateExamReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        DatabaseFile content = new DatabaseFile();
        content.setContent(generateContent());
        return content;
    }

    private byte[] generateContent()
    {
        // получаем шаблон
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REGISTRY_INTERNAL_STATE_EXAM);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate()).getClone();

        String academyShortTitle = TopOrgUnit.getInstance().getShortTitle();

        // формируем список строк
        int number = 1;
        List<String[]> rows = new ArrayList<>();
        
        // для каждого абитуриента
        for (Entry<Entrant, Set<StateExamSubject>> entry : getEntrant2StateExamSubjects(getEntrantsIdsBuilder()).entrySet())
        {
            Entrant entrant = entry.getKey();
            Person person = entrant.getPerson();
            IdentityCard identityCard = person.getIdentityCard();
            AddressBase address = identityCard.getAddress();

            // добавляем строку
            List<String> row = new ArrayList<>();
            row.add(Integer.toString(number++));
            row.add(null);
            row.add(person.getFullFio().replace(" ", "\n"));
            String addressString = "";
            if(address != null)
            {
                if(address instanceof AddressRu)
                {
                    addressString = new AddressRuTitleBuilder((AddressRu) address).country().settlementWithParents().street().house().houseUnit().flat().toString();
                }
                else if(address instanceof AddressInter)
                {
                    addressString = new AddressInterTitleBuilder((AddressInter) address).country().region().settlement().addressLocation().toString();
                }
                else if (address instanceof AddressString)
                    addressString = ((AddressString) address).getAddress();
            }
            row.add(!StringUtils.isEmpty(addressString) ? addressString : null);


            row.add(identityCard.getFullNumber());
            row.add(UniStringUtils.join(identityCard.getIssuancePlace(), identityCard.getIssuanceCode()));
            row.add(null);
            row.add(UniStringUtils.join(entry.getValue(), StateExamSubject.title().s(), "\n"));
            row.add(null);
            row.add(academyShortTitle);
            rows.add(row.toArray(new String[row.size()]));
        }

        // инжектим полученную таблицу
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", rows.toArray(new String[rows.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // для колонок с ФИО и перечнем предметов сделать переносы строк 
                if (value != null && (colIndex == 2 || colIndex == 7))
                {
                    IRtfText result = RtfBean.getElementFactory().createRtfText(value.replace("\n", "\\par"));
                    result.setRaw(true);
                    return Collections.<IRtfElement> singletonList(result);
                }
                return null;
            }
        });
        tableModifier.modify(document);

        // возвращаем файл
        return RtfUtil.toByteArray(document);
    }

    /**
     * @return билдер для поиска идентификаторов абитуриентов, удовлетворяющих параметрам отчета
     */
    private MQBuilder getEntrantsIdsBuilder()
    {
        List<EnrollmentDirection> enrollmentDirections = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);

        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.entrantRequest().regDate().s(), _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.addSelect("red", new Object[] { RequestedEnrollmentDirection.entrantRequest().entrant().id().s() });

        if (_model.isQualificationActive())
        {
            CollectionUtils.filter(enrollmentDirections, object -> _model.getQualificationList().contains(((EnrollmentDirection) object).getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()));
        }
        if (_model.isStudentCategoryActive())
        {
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategoryList()));
        }
        if (_model.isCompensationTypeActive())
        {
            builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), _model.getCompensationType()));
        }

        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival().s(), Boolean.FALSE));
        builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.in("red", RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirections));
        builder.setNeedDistinct(true);
        return builder;
    }

    /**
     * @param entrantsIdsBuilder билдер поиска идентификаторов абитуриентов
     * @return абитуриент - предметы ЕГЭ
     */
    private Map<Entrant, Set<StateExamSubject>> getEntrant2StateExamSubjects(MQBuilder entrantsIdsBuilder)
    {
        // подготавливаем данные
        EnrollmentCampaign enrollmentCampaign = _model.getReport().getEnrollmentCampaign();
        Map<Discipline2RealizationWayRelation, StateExamSubject> discipline2Subject = getDiscipline2Subject(enrollmentCampaign);
        EntrantDataUtil entrantDataUtil = new EntrantDataUtil(_session, enrollmentCampaign, getRequestedEnrollmentDirectionsBuilder(entrantsIdsBuilder), DetailLevel.EXAM_PASS);

        Map<Long, Set<StateExamSubject>> entrantId2StateExamSubjects = new HashMap<>();
        Callback<Long, Set<StateExamSubject>> callback = key -> new TreeSet<>(ITitled.TITLED_COMPARATOR);

        // для каждого выбранного направления приема
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : entrantDataUtil.getDirectionSet())
        {
            Long entrantId = requestedEnrollmentDirection.getEntrantRequest().getEntrant().getId();

            // для каждого выбранного вступительного испытания
            for (ChosenEntranceDiscipline chosenEntranceDiscipline : entrantDataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection))
            {
                // для каждой дисциплины для сдачи
                for (ExamPassDiscipline examPassDiscipline : entrantDataUtil.getExamPassDisciplineSet(chosenEntranceDiscipline))
                {
                    // если она по ЕГЭ (вуз), то добавляем соответствующий предмет в набор
                    if (examPassDiscipline.getSubjectPassForm().getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                    {
                        StateExamSubject stateExamSubject = discipline2Subject.get(examPassDiscipline.getEnrollmentCampaignDiscipline());
                        SafeMap.safeGet(entrantId2StateExamSubjects, entrantId, callback).add(stateExamSubject);
                    }
                }
            }
        }

        // рассортировываем непосредственно по абитуриентам
        Map<Entrant, Set<StateExamSubject>> result = new LinkedHashMap<>();
        for (Entrant entrant : getEntrants(entrantId2StateExamSubjects.keySet()))
        {
            result.put(entrant, entrantId2StateExamSubjects.get(entrant.getId()));
        }
        return result;
    }

    /**
     * @param entrantIds идентификаторы абитуриентов
     * @return список абитуриентов
     */
    private List<Entrant> getEntrants(Collection<Long> entrantIds)
    {
        MQBuilder builder = new MQBuilder(Entrant.ENTITY_CLASS, "entrant");
        builder.addJoinFetch("entrant", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "identityCard");
        builder.addLeftJoinFetch("identityCard", IdentityCard.address().s(), "address");
        builder.add(MQExpression.in("entrant", Entrant.id().s(), entrantIds));
        builder.addOrder("identityCard", IdentityCard.lastName().s());
        builder.addOrder("identityCard", IdentityCard.firstName().s());
        builder.addOrder("identityCard", IdentityCard.middleName().s());
        return builder.getResultList(_session);
    }

    /**
     * @param entrantsIdsBuilder билдер поиска идентификаторов абитуриентов
     * @return билдер поиска выбранных направлений приема данных абитуриентов
     */
    private MQBuilder getRequestedEnrollmentDirectionsBuilder(MQBuilder entrantsIdsBuilder)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");
        builder.addJoinFetch("requestedEnrollmentDirection", RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.entrantRequest().entrant().id().s(), entrantsIdsBuilder));
        return builder;
    }

    /**
     * @param enrollmentCampaign приемная кампания
     * @return дисциплина приемной кампании - предмет ЕГЭ
     */
    private Map<Discipline2RealizationWayRelation, StateExamSubject> getDiscipline2Subject(EnrollmentCampaign enrollmentCampaign)
    {
        MQBuilder builder = new MQBuilder(ConversionScale.ENTITY_CLASS, "conversionScale");
        builder.add(MQExpression.eq("conversionScale", ConversionScale.discipline().enrollmentCampaign().s(), enrollmentCampaign));

        Map<Discipline2RealizationWayRelation, StateExamSubject> result = new HashMap<>();
        for (ConversionScale conversionScale : builder.<ConversionScale> getResultList(_session))
        {
            result.put(conversionScale.getDiscipline(), conversionScale.getSubject());
        }
        return result;
    }
}
