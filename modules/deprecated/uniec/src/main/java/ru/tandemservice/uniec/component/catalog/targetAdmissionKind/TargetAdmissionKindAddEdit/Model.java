/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindAddEdit;

import java.util.List;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author agolubenko
 * @since 01.06.2008
 */
public class Model extends DefaultCatalogAddEditModel<TargetAdmissionKind>
{
    private List<TargetAdmissionKind> _parentList;

    public List<TargetAdmissionKind> getParentList()
    {
        return _parentList;
    }

    public void setParentList(List<TargetAdmissionKind> parentList)
    {
        _parentList = parentList;
    }
}
