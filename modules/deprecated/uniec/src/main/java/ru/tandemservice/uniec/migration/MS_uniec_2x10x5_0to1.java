package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x5_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrolledDealListReport

		// создана новая сущность
		if (!tool.tableExists("fefuenrolleddeallistreport_t"))// таблица в ДВФУ, если есть то удалится в MS_unifefu_2x10x5_0to1
		{
			// создать таблицу
			DBTable dbt = new DBTable("enrolleddeallistreport_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrolleddeallistreport"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("content_id", DBType.LONG).setNullable(false), 
				new DBColumn("formingdate_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("entrantenrollmentordertype_p", DBType.createVarchar(255)), 
				new DBColumn("enrollmentorder_p", DBType.createVarchar(255)), 
				new DBColumn("compensationtype_id", DBType.LONG), 
				new DBColumn("studentcategorytitle_p", DBType.createVarchar(255)), 
				new DBColumn("qualificationtitle_p", DBType.createVarchar(255)), 
				new DBColumn("formativeorgunit_p", DBType.TEXT), 
				new DBColumn("territorialorgunit_p", DBType.TEXT), 
				new DBColumn("developform_p", DBType.createVarchar(255)), 
				new DBColumn("developcondition_p", DBType.createVarchar(255)), 
				new DBColumn("developtech_p", DBType.createVarchar(255)), 
				new DBColumn("developperiod_p", DBType.createVarchar(255)), 
				new DBColumn("excludeparallel_p", DBType.BOOLEAN), 
				new DBColumn("includeforeignperson_p", DBType.BOOLEAN)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrolledDealListReport");

		}


    }
}