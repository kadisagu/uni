package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Зачетный балл по предмету ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamSubjectPassScoreGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore";
    public static final String ENTITY_NAME = "stateExamSubjectPassScore";
    public static final int VERSION_HASH = 371461792;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_STATE_EXAM_SUBJECT = "stateExamSubject";
    public static final String P_MARK = "mark";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private StateExamSubject _stateExamSubject;     // Предмет ЕГЭ
    private int _mark;     // Зачетный балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public StateExamSubject getStateExamSubject()
    {
        return _stateExamSubject;
    }

    /**
     * @param stateExamSubject Предмет ЕГЭ. Свойство не может быть null.
     */
    public void setStateExamSubject(StateExamSubject stateExamSubject)
    {
        dirty(_stateExamSubject, stateExamSubject);
        _stateExamSubject = stateExamSubject;
    }

    /**
     * @return Зачетный балл. Свойство не может быть null.
     */
    @NotNull
    public int getMark()
    {
        return _mark;
    }

    /**
     * @param mark Зачетный балл. Свойство не может быть null.
     */
    public void setMark(int mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamSubjectPassScoreGen)
        {
            setEnrollmentCampaign(((StateExamSubjectPassScore)another).getEnrollmentCampaign());
            setStateExamSubject(((StateExamSubjectPassScore)another).getStateExamSubject());
            setMark(((StateExamSubjectPassScore)another).getMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamSubjectPassScoreGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamSubjectPassScore.class;
        }

        public T newInstance()
        {
            return (T) new StateExamSubjectPassScore();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "stateExamSubject":
                    return obj.getStateExamSubject();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "stateExamSubject":
                    obj.setStateExamSubject((StateExamSubject) value);
                    return;
                case "mark":
                    obj.setMark((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "stateExamSubject":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "stateExamSubject":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "stateExamSubject":
                    return StateExamSubject.class;
                case "mark":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamSubjectPassScore> _dslPath = new Path<StateExamSubjectPassScore>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamSubjectPassScore");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore#getStateExamSubject()
     */
    public static StateExamSubject.Path<StateExamSubject> stateExamSubject()
    {
        return _dslPath.stateExamSubject();
    }

    /**
     * @return Зачетный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore#getMark()
     */
    public static PropertyPath<Integer> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends StateExamSubjectPassScore> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private StateExamSubject.Path<StateExamSubject> _stateExamSubject;
        private PropertyPath<Integer> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore#getStateExamSubject()
     */
        public StateExamSubject.Path<StateExamSubject> stateExamSubject()
        {
            if(_stateExamSubject == null )
                _stateExamSubject = new StateExamSubject.Path<StateExamSubject>(L_STATE_EXAM_SUBJECT, this);
            return _stateExamSubject;
        }

    /**
     * @return Зачетный балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.StateExamSubjectPassScore#getMark()
     */
        public PropertyPath<Integer> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Integer>(StateExamSubjectPassScoreGen.P_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return StateExamSubjectPassScore.class;
        }

        public String getEntityName()
        {
            return "stateExamSubjectPassScore";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
