/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionPub;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

/**
 * @author Боба
 * @since 30.07.2008
 */
@State(keys = {PublisherActivator.PUBLISHER_ID_KEY}, bindings = {"enrollmentDirection.id"})
public class Model
{
    private EnrollmentDirection _enrollmentDirection = new EnrollmentDirection();
    private List<CompensationType> _compensationTypes;
    private String _targetAdmissionPlanBudget;
    private String _targetAdmissionPlanContract;
    private DynamicListDataSource<EntranceDiscipline> _dataSource;
    private DynamicListDataSource<ProfileEducationOrgUnit> _profileDataSource;
    private Map<StudentCategory, DynamicListDataSource<EntranceDiscipline>> _dataSourceMap;
    private List<StudentCategory> _studentCategoryList;
    private StudentCategory _studentCategory;
    private Long _educationDisciplineRowId;
    
    private String _budgetPlanStyle;
    private String _contractPlanStyle;

    // Getters & Setters

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public List<CompensationType> getCompensationTypes()
    {
        return _compensationTypes;
    }

    public void setCompensationTypes(List<CompensationType> compensationTypes)
    {
        _compensationTypes = compensationTypes;
    }

    public String getTargetAdmissionPlanBudget()
    {
        return _targetAdmissionPlanBudget;
    }

    public void setTargetAdmissionPlanBudget(String targetAdmissionPlanBudget)
    {
        _targetAdmissionPlanBudget = targetAdmissionPlanBudget;
    }

    public String getTargetAdmissionPlanContract()
    {
        return _targetAdmissionPlanContract;
    }

    public void setTargetAdmissionPlanContract(String targetAdmissionPlanContract)
    {
        _targetAdmissionPlanContract = targetAdmissionPlanContract;
    }

    public DynamicListDataSource<EntranceDiscipline> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntranceDiscipline> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<StudentCategory, DynamicListDataSource<EntranceDiscipline>> getDataSourceMap()
    {
        return _dataSourceMap;
    }

    public void setDataSourceMap(Map<StudentCategory, DynamicListDataSource<EntranceDiscipline>> dataSourceMap)
    {
        _dataSourceMap = dataSourceMap;
    }

    public DynamicListDataSource<ProfileEducationOrgUnit> getProfileDataSource()
    {
        return _profileDataSource;
    }

    public void setProfileDataSource(DynamicListDataSource<ProfileEducationOrgUnit> profileDataSource)
    {
        _profileDataSource = profileDataSource;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public void setStudentCategory(StudentCategory studentCategory)
    {
        _studentCategory = studentCategory;
    }

    public Long getEducationDisciplineRowId()
    {
        return _educationDisciplineRowId;
    }

    public void setEducationDisciplineRowId(Long educationDisciplineRowId)
    {
        _educationDisciplineRowId = educationDisciplineRowId;
    }

    public String getBudgetPlanStyle()
    {
        return _budgetPlanStyle;
    }

    public void setBudgetPlanStyle(String budgetPlanStyle)
    {
        _budgetPlanStyle = budgetPlanStyle;
    }

    public String getContractPlanStyle()
    {
        return _contractPlanStyle;
    }

    public void setContractPlanStyle(String contractPlanStyle)
    {
        _contractPlanStyle = contractPlanStyle;
    }
}
