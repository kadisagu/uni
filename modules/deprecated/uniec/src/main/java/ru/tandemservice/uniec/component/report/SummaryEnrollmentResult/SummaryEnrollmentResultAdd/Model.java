/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryEnrollmentResultsReport;

/**
 * @author Vasily Zhukov
 * @since 29.07.2010
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    final static int ENROLLMENT_CAMP_STAGE_DOCUMENTS = 0;
    final static int ENROLLMENT_CAMP_STAGE_EXAMS = 1;
    final static int ENROLLMENT_CAMP_STAGE_ENROLLMENT = 2;

    private String _selectedTabId;

    private SummaryEnrollmentResultsReport _report = new SummaryEnrollmentResultsReport();
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<IdentifiableWrapper> _enrollmentCampaignStageList;
    private IdentifiableWrapper _enrollmentCampaignStage;
    private ISelectModel _developFormList;
    private List<CompensationType> _compensationTypeList;

    private boolean _developConditionActive;
    private ISelectModel _developConditionListModel;
    private List<DevelopCondition> _developConditionList;

    private boolean _qualificationActive;
    private ISelectModel _qualificationListModel;
    private List<Qualifications> _qualificationList;

    private boolean _studentCategoryActive;
    private ISelectModel _studentCategoryListModel;
    private List<StudentCategory> _studentCategoryList;

    private boolean _parallelActive;
    private List<CommonYesNoUIObject> _parallelList;
    private CommonYesNoUIObject _parallel;

    private String _column;
    private List<String> _columnList;
    private boolean _checkAllColumns;
    private Set<String> _hideColumns = new HashSet<String>();

    // Print settings

    public boolean isColumnVisible()
    {
        return !_hideColumns.contains(_column);
    }

    public void setColumnVisible(boolean columnVisible)
    {
        if (columnVisible) _hideColumns.remove(_column);
        else _hideColumns.add(_column);
    }

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // Getters & Setters

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        _selectedTabId = selectedTabId;
    }

    public SummaryEnrollmentResultsReport getReport()
    {
        return _report;
    }

    public void setReport(SummaryEnrollmentResultsReport report)
    {
        _report = report;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<IdentifiableWrapper> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<IdentifiableWrapper> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public IdentifiableWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(IdentifiableWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<CommonYesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<CommonYesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public CommonYesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(CommonYesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public String getColumn()
    {
        return _column;
    }

    public void setColumn(String column)
    {
        _column = column;
    }

    public List<String> getColumnList()
    {
        return _columnList;
    }

    public void setColumnList(List<String> columnList)
    {
        _columnList = columnList;
    }

    public boolean isCheckAllColumns()
    {
        return _checkAllColumns;
    }

    public void setCheckAllColumns(boolean checkAllColumns)
    {
        _checkAllColumns = checkAllColumns;
    }

    public Set<String> getHideColumns()
    {
        return _hideColumns;
    }

    public void setHideColumns(Set<String> hideColumns)
    {
        _hideColumns = hideColumns;
    }
}
