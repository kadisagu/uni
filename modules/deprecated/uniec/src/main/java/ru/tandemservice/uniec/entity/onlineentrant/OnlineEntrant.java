package ru.tandemservice.uniec.entity.onlineentrant;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniec.entity.onlineentrant.gen.OnlineEntrantGen;

/**
 * Онлайн-абитуриент
 */
public class OnlineEntrant extends OnlineEntrantGen
{
    public static final String P_FULL_FIO = "fullFio";

    public String getFullFio()
    {
        String lastName = getLastName();
        String firstName = getFirstName();
        String middleName = getMiddleName();

        StringBuilder str = new StringBuilder().append(lastName);
        if (StringUtils.isNotEmpty(firstName))
        {
            str.append(" ").append(firstName);
        }
        if (StringUtils.isNotEmpty(middleName))
        {
            str.append(" ").append(middleName);
        }
        return str.toString();
    }

    public static final String P_FULL_PASSPORT_NUMBER = "fullPassportNumber";

    public String getFullPassportNumber()
    {
        String passportSeria = getPassportSeria();
        String passportNumber = getPassportNumber();

        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(passportSeria))
        {
            sb.append(passportSeria).append(" ");
        }
        if (StringUtils.isNotEmpty(passportNumber))
        {
            sb.append(passportNumber);
        }
        return sb.toString();
    }

    public static final String P_FULL_TITLE = "fullTitle";

    public String getFullTitle()
    {
        StringBuilder result = new StringBuilder();
        result.append(getPersonalNumber()).append(" ");
        result.append(getFullFio());

        String fullPassportNumber = getFullPassportNumber();
        if (StringUtils.isNotEmpty(fullPassportNumber))
        {
            result.append(" (").append(fullPassportNumber).append(")");
        }
        return result.toString();
    }

    /**
     * @return Персональный код регистрации вида год-номер "\\d{4}-\\d+"
     */
    public String getPersonalCode()
    {
        return String.valueOf(getRegistrationYear()) + "-" + String.valueOf(getPersonalNumber());
    }
}