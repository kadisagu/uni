/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e1;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class SplitBudgetMasterEntrantsStuListExtractDao extends UniBaseDao implements IExtractComponentDao<SplitEntrantsStuListExtract>
{
    @Override
    public void doCommit(SplitEntrantsStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());
        update(extract.getEntity());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            // DEV-4960
            extract.setPrevOrderDate(orderData.getSplitToSpecializationOrderDate());
            extract.setPrevOrderNumber(orderData.getSplitToSpecializationOrderNumber());
        }
        // DEV-4960
        orderData.setSplitToSpecializationOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(SplitEntrantsStuListExtract extract, Map parameters)
    {
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        // DEV-4960
        orderData.setSplitToSpecializationOrderDate(extract.getPrevOrderDate());
        orderData.setSplitToSpecializationOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}