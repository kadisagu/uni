/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 03.08.2010
 */
interface IStateExamEnrollmentExcelBuilder
{
    /**
     * создает Excel-файл отчета
     *
     * @param highSchoolTitle     название ОУ
     * @param enrollmentCampStage стадия приемной кампании
     * @param periodTitle         название периода отчета
     * @param model               модель данных
     * @param reportRowList       список строк отчета
     * @return бинарное представление excel-файла отчета
     * @throws Exception ошибка
     */
    byte[] buildExcelContent(String highSchoolTitle,
                             String enrollmentCampStage,
                             String periodTitle,
                             Model model,
                             List<ReportRow> reportRowList) throws Exception;
}
