package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма сдачи дисциплины"
 * Имя сущности : subjectPassForm
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface SubjectPassFormCodes
{
    /** Константа кода (code) элемента : Единый Государственный Экзамен (title) */
    String STATE_EXAM = "1";
    /** Константа кода (code) элемента : Тестирование (title) */
    String TEST = "2";
    /** Константа кода (code) элемента : Экзамен (title) */
    String EXAM = "3";
    /** Константа кода (code) элемента : Собеседование (title) */
    String INTERVIEW = "4";
    /** Константа кода (code) элемента : Единый Государственный Экзамен (вуз) (title) */
    String STATE_EXAM_INTERNAL = "5";

    Set<String> CODES = ImmutableSet.of(STATE_EXAM, TEST, EXAM, INTERVIEW, STATE_EXAM_INTERNAL);
}
