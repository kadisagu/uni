/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.entity.catalog.CompensationType;

/**
 * @author Vasily Zhukov
 * @since 26.04.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String VIEW_PROPERTY_PROFILE_PRIORITY_DISABLED = "changeProfilePriorityDisabled";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        if (model.getSettings().get("compensationType") == null)
        {
            model.getSettings().set("compensationType", model.getCompensationTypeList().get(0));
            onChangeCompensationType(component);
        }
    }

    public void onChangeCompensationType(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        onRefreshComponent(component);
    }

    public void onClickEditPriorities(IBusinessComponent component)
    {
        Model model = getModel(component);
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityEdit.Controller.class.getPackage().getName(), new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, model.getEntrant().getId())
                .add("compensationType", null == compensationType ? null : compensationType.getId())
        ));
    }

    public void onClickEditPriorityProfileEduOu(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit.Controller.class.getPackage().getName(), new ParametersMap()
                .add("requestedEnrollmentDirectionId", component.<Long>getListenerParameter())
        ));
    }

}
