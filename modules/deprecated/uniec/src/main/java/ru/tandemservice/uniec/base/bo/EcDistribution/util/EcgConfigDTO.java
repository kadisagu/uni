/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.util.Assert;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;

/**
 * Неперсистентная конфигурация распределения (аналог EcgDistributionConfig)
 *
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
public class EcgConfigDTO
{
    private IPersistentEcgItem _ecgItem;
    private CompensationType _compensationType;
    private boolean _secondHighAdmission;
    private MultiKey _multiKey;

    public EcgConfigDTO(EcgDistributionConfig config)
    {
        this(config.getEcgItem(), config.getCompensationType(), config.isSecondHighAdmission());
    }

    public EcgConfigDTO(IPersistentEcgItem ecgItem, CompensationType compensationType, boolean secondHighAdmission)
    {
        Assert.notNull(ecgItem);
        Assert.notNull(compensationType);
        _ecgItem = ecgItem;
        _compensationType = compensationType;
        _secondHighAdmission = secondHighAdmission;
    }

    @Override
    public int hashCode()
    {
        if (_multiKey == null)
            _multiKey = new MultiKey(_ecgItem.getId(), _compensationType.getId(), _secondHighAdmission);
        return _multiKey.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!(obj instanceof EcgConfigDTO)) return false;
        EcgConfigDTO other = (EcgConfigDTO) obj;
        return _ecgItem.equals(other._ecgItem) && _compensationType.equals(other._compensationType) && _secondHighAdmission == other._secondHighAdmission;
    }

    @Override
    public String toString()
    {
        return "EcgDistributionConfigKey-" + _ecgItem.getId() + "-" + _compensationType.getId() + "-" + _secondHighAdmission;
    }

    // Getters

    public IPersistentEcgItem getEcgItem()
    {
        return _ecgItem;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public boolean isSecondHighAdmission()
    {
        return _secondHighAdmission;
    }
}
