/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.form3NKDecription2008.Form3NKDecription2008Add;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author vip_delete
 * @since 15.05.2009
 */
class Form3NKDecription2008ReportBuilder
{
    private Model _model;
    private Session _session;

    Form3NKDecription2008ReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        // направление подготовки ОУ -> количество выбранных направления приема
        final Map<EducationLevelsHighSchool, Set<RequestedEnrollmentDirection>> direction2requestedSet = new HashMap<>();
        {
            MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");

            // apply filters
            applyFilters(builder);

            for (RequestedEnrollmentDirection requested : builder.<RequestedEnrollmentDirection>getResultList(_session))
            {
                EducationLevelsHighSchool key = requested.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();
                Set<RequestedEnrollmentDirection> set = direction2requestedSet.get(key);
                if (set == null)
                    direction2requestedSet.put(key, set = new HashSet<>());
                set.add(requested);
            }
        }

        // направление подготовки -> направление приема
        final Map<EducationOrgUnit, EnrollmentDirection> educationOrgUnit2direction = new HashMap<>();
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
            for (EnrollmentDirection enrollmentDirection : builder.<EnrollmentDirection>getResultList(_session))
                educationOrgUnit2direction.put(enrollmentDirection.getEducationOrgUnit(), enrollmentDirection);
        }

        // ур. обр ОУ -> план приема на бюджет
        final Map<EducationLevelsHighSchool, Integer> direction2statePlan = new HashMap<>();
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
            builder.addJoin("d", EnrollmentDirection.educationOrgUnit().s(), "eduOu");
            applyEduOuFilters(builder, "eduOu");
            for (EnrollmentDirection direction : builder.<EnrollmentDirection>getResultList(_session))
            {
                EducationLevelsHighSchool key = direction.getEducationOrgUnit().getEducationLevelHighSchool();
                Integer statePlan = direction2statePlan.get(key);
                if (statePlan == null) statePlan = 0;

                Integer mp = direction.getMinisterialPlan();
                //Integer tp = direction.getTargetAdmissionPlan();

                int mplan = mp == null ? 0 : mp;
                //int tplan = tp == null ? 0 : tp.intValue();

                direction2statePlan.put(key, statePlan += mplan);
            }
        }

        // направление подготовки ОУ -> количество поступивших
        final Map<EducationLevelsHighSchool, Set<PreliminaryEnrollmentStudent>> direction2preStudentSet = new HashMap<>();
        {
            MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
            builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");

            // apply filters
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
            builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+ EntrantRequest.L_ENTRANT+"."+ Entrant.P_ARCHIVAL, false));
            builder.addJoin("p", PreliminaryEnrollmentStudent.educationOrgUnit().s(), "eduOu");
            applyEduOuFilters(builder, "eduOu");
            if (_model.isCompensTypeActive())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.compensationType().s(), _model.getCompensationType()));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.studentCategory().s(), _model.getStudentCategoryList()));
            if (_model.isParallelActive() && _model.getParallel().isTrue())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));

            for (PreliminaryEnrollmentStudent preStudent : builder.<PreliminaryEnrollmentStudent>getResultList(_session))
            {
                EnrollmentDirection enrollmentDirection = educationOrgUnit2direction.get(preStudent.getEducationOrgUnit());
                if (enrollmentDirection != null)
                {
                    EducationLevelsHighSchool key = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool();
                    Set<PreliminaryEnrollmentStudent> set = direction2preStudentSet.get(key);
                    if (set == null)
                        direction2preStudentSet.put(key, set = new HashSet<>());
                    set.add(preStudent);
                }
            }
        }

        AddressDetailed address = TopOrgUnit.getInstance().getAddress();
        AddressItem city = address == null ? null : address.getSettlement();
        String highSchoolCityTitle = city == null ? "город ОУ" : city.getTitleWithType();

        // список направлений приема удовлетв. фильтрам. ( по ним будут строится строки отчета)
        Set<EnrollmentDirection> studentEnrollmentDirectionSet = getEnrollmentDirectionSet(false);
        Set<EnrollmentDirection> masterEnrollmentDirectionSet = getEnrollmentDirectionSet(true);

        // id большой строки -> мн-во ур. обр ОУ в нее входящих
        Map<OrgUnit, Set<EducationLevelsHighSchool>> studentRow2highSchool = getRowId2HighSchoolMap(studentEnrollmentDirectionSet);
        Map<OrgUnit, Set<EducationLevelsHighSchool>> masterRow2highSchool = getRowId2HighSchoolMap(masterEnrollmentDirectionSet);

        // строки таблицы для студентов
        List<Form3NK2008Row> studentRowList = getForm3NKRowList(highSchoolCityTitle, direction2statePlan, studentRow2highSchool, direction2requestedSet, direction2preStudentSet);

        // строки таблицы для магистрантов
        List<Form3NK2008Row> masterRowList = getForm3NKRowList(highSchoolCityTitle, direction2statePlan, masterRow2highSchool, direction2requestedSet, direction2preStudentSet);

        // создаем таблицу для студентов
        final List<String[]> studentTable = new ArrayList<>();
        for (Form3NK2008Row row : studentRowList)
        {
            studentTable.add(new String[]{row.getTitle()});
            appendToTable(studentTable, row.getSubRowList());
        }

        // создаем таблицу для магистрантов
        final List<String[]> masterTable = new ArrayList<>();
        for (Form3NK2008Row row : masterRowList)
        {
            masterTable.add(new String[]{row.getTitle()});
            appendToTable(masterTable, row.getSubRowList());
        }

        // СОЗДАЕМ ПЕЧАТНУЮ ФОРМУ

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_FORM_3NK_DECRIPTION_2008);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("campaignTitle", _model.getReport().getEnrollmentCampaign().getTitle());
        String developFormCode = _model.getDevelopForm().getCode();

        if (DevelopFormCodes.FULL_TIME_FORM.equals(developFormCode))
            injectModifier.put("developFormTitle", "Очное отделение");
        else if (DevelopFormCodes.CORESP_FORM.equals(developFormCode))
            injectModifier.put("developFormTitle", "Заочное отделение");
        else if (DevelopFormCodes.PART_TIME_FORM.equals(developFormCode))
            injectModifier.put("developFormTitle", "Очно-заочное отделение");
        else if (DevelopFormCodes.EXTERNAL_FORM.equals(developFormCode))
            injectModifier.put("developFormTitle", "Экстернат");
        else if (DevelopFormCodes.APPLICANT_FORM.equals(developFormCode))
            injectModifier.put("developFormTitle", "Самостоятельное обучение и итоговая аттестация");


        injectModifier.modify(document);

        final IRtfControl boldStart = RtfBean.getElementFactory().createRtfControl(IRtfData.B);
        final IRtfControl boldEnd = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
//        boldEnd.setValue(0);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", studentTable.toArray(new String[studentTable.size()][]));
        tableModifier.put("M", masterTable.toArray(new String[masterTable.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = 0; i < studentTable.size(); i++)
                {
                    if (studentTable.get(i).length == 1)
                    {
                        List<IRtfElement> list = newRowList.get(startIndex + i).getCellList().get(0).getElementList();
                        list.add(0, boldStart);
                        list.add(boldEnd);
                    }
                }
            }
        });
        tableModifier.put("M", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = 0; i < masterTable.size(); i++)
                {
                    if (masterTable.get(i).length == 1)
                    {
                        List<IRtfElement> list = newRowList.get(startIndex + i).getCellList().get(0).getElementList();
                        list.add(0, boldStart);
                        list.add(boldEnd);
                    }
                }
            }
        });
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private void applyFilters(MQBuilder builder)
    {
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().s(), _model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategoryList()));
        if (_model.isCompensTypeActive())
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.compensationType().s(), _model.getCompensationType()));
        builder.addJoin("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().s(), "ou");
        applyEduOuFilters(builder, "ou");
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+ EntrantRequest.L_ENTRANT+"."+ Entrant.P_ARCHIVAL, false));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
    }

    private void applyEduOuFilters(MQBuilder builder, String eduOu)
    {
        if (_model.isQualificationActive())
            builder.add(MQExpression.in(eduOu, EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in(eduOu, EducationOrgUnit.formativeOrgUnit().s(), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in(eduOu, EducationOrgUnit.territorialOrgUnit().s(), _model.getTerritorialOrgUnitList()));
        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in(eduOu, EducationOrgUnit.educationLevelHighSchool().s(), _model.getEducationLevelHighSchoolList()));
        builder.add(MQExpression.eq(eduOu, EducationOrgUnit.developForm().s(), _model.getDevelopForm()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in(eduOu, EducationOrgUnit.developCondition().s(), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in(eduOu, EducationOrgUnit.developTech().s(), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
        builder.add(MQExpression.in(eduOu, EducationOrgUnit.developPeriod().s(), _model.getDevelopPeriodList()));
    }


    // список направлений приема удовлетв. фильтрам. ( по ним будут строится строки отчета)
    private Set<EnrollmentDirection> getEnrollmentDirectionSet(boolean onlyMasters)
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
        builder.addJoin("d", EnrollmentDirection.educationOrgUnit().s(), "ou");
        applyEduOuFilters(builder, "ou");

        if (onlyMasters)
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE + "." + StructureEducationLevels.P_MASTER, Boolean.TRUE));
        else
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE + "." + StructureEducationLevels.P_MASTER, Boolean.FALSE));
        return new HashSet<>(builder.<EnrollmentDirection>getResultList(_session));
    }

    // id большой строки -> мн-во ур. обр ОУ в нее входящих
    private static Map<OrgUnit, Set<EducationLevelsHighSchool>> getRowId2HighSchoolMap(Set<EnrollmentDirection> enrollmentDirectionSet)
    {
        Map<OrgUnit, Set<EducationLevelsHighSchool>> result = new HashMap<>();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirectionSet)
        {
            OrgUnit key = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit();
            if (!key.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) && !key.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.REPRESENTATION))
                key = null;

            Set<EducationLevelsHighSchool> set = result.get(key);
            if (set == null)
                result.put(key, set = new HashSet<>());
            set.add(enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool());
        }
        return result;
    }

    private static List<Form3NK2008Row> getForm3NKRowList(String highSchoolCityTitle, Map<EducationLevelsHighSchool, Integer> direction2statePlan, Map<OrgUnit, Set<EducationLevelsHighSchool>> row2highSchool, Map<EducationLevelsHighSchool, Set<RequestedEnrollmentDirection>> direction2requestedSet, Map<EducationLevelsHighSchool, Set<PreliminaryEnrollmentStudent>> direction2preStudentSet)
    {
        Form3NK2008Row firstRow = null;
        List<Form3NK2008Row> rowList = new ArrayList<>();

        Set<EducationLevelsHighSchool> set = row2highSchool.remove(null);
        if (set != null)
            firstRow = new Form3NK2008Row(highSchoolCityTitle, getForm3NKSubRowList(set, direction2statePlan, direction2requestedSet, direction2preStudentSet));

        for (Map.Entry<OrgUnit, Set<EducationLevelsHighSchool>> entry : row2highSchool.entrySet())
            rowList.add(new Form3NK2008Row(entry.getKey().getShortTitle(), getForm3NKSubRowList(entry.getValue(), direction2statePlan, direction2requestedSet, direction2preStudentSet)));

        Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
        if (firstRow != null)
            rowList.add(0, firstRow);
        return rowList;
    }

    private static List<Form3NK2008SubRow> getForm3NKSubRowList(Set<EducationLevelsHighSchool> set, Map<EducationLevelsHighSchool, Integer> direction2statePlan, Map<EducationLevelsHighSchool, Set<RequestedEnrollmentDirection>> direction2requestedSet, Map<EducationLevelsHighSchool, Set<PreliminaryEnrollmentStudent>> direction2preStudentSet)
    {
        List<Form3NK2008SubRow> reportRowList = new ArrayList<>();

        for (EducationLevelsHighSchool highSchool : set)
        {
            String title = highSchool.getPrintTitle();
            Form3NK2008SubRow row = new Form3NK2008SubRow(title,
                    title + " (" + highSchool.getOrgUnit().getShortTitle() + ")",
                    direction2statePlan.get(highSchool),
                    direction2requestedSet.get(highSchool),
                    direction2preStudentSet.get(highSchool)
            );
            row.buildRow();
            reportRowList.add(row);
        }

        Collections.sort(reportRowList, ITitled.TITLED_COMPARATOR);

        Form3NK2008SubRow preRow = null;
        for (Form3NK2008SubRow reportRow : reportRowList)
        {
            if (preRow != null && preRow.getTitle().equals(reportRow.getTitle()))
            {
                preRow.useFullTitle();
                reportRow.useFullTitle();
            }
            preRow = reportRow;
        }

        Collections.sort(reportRowList, new Comparator<Form3NK2008SubRow>()
        {
            @Override
            public int compare(Form3NK2008SubRow o1, Form3NK2008SubRow o2)
            {
                String title1 = o1.isUseFullTitle() ? o1.getFullTitle() : o1.getTitle();
                String title2 = o2.isUseFullTitle() ? o2.getFullTitle() : o2.getTitle();
                return title1.compareTo(title2);
            }
        });

        return reportRowList;
    }

    private static void appendToTable(List<String[]> table, List<Form3NK2008SubRow> rowList)
    {
        for (Form3NK2008SubRow row : rowList)
        {
            table.add(new String[]{
                    row.isUseFullTitle() ? row.getFullTitle() : row.getTitle(),
                    row.getStatePlan() == 0 ? "" : Integer.toString(row.getStatePlan()),
                    row.getRequestCount() == 0 ? "" : Integer.toString(row.getRequestCount()),
                    row.getEnrolledCount() == 0 ? "" : Integer.toString(row.getEnrolledCount()),
                    row.getContractCount() == 0 ? "" : Integer.toString(row.getContractCount()),
                    row.getSecondEduCount() == 0 ? "" : Integer.toString(row.getSecondEduCount())
            });
        }
    }
}
