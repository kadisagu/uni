package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uniec.entity.entrant.gen.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.not;

/** @see ru.tandemservice.uniec.entity.entrant.gen.IndProg2Request4ExcludeGen */
public class IndProg2Request4Exclude extends IndProg2Request4ExcludeGen
{
    public IndProg2Request4Exclude()
    {
    }

    public IndProg2Request4Exclude(EntrantIndividualProgress individualProgress, EntrantRequest request)
    {
        setIndividualProgress(individualProgress);
        setRequest(request);
    }

    /**
     * @param requestId Выражение для id Заявления абитуриента или сам id (IDQLExpression, Long, List of Long)
     * @param progress Выражение для Индивидуального достижения абитуриента
     * @return Выражение для фильтрациии Индивидиуальных достижения абитуриента не учитываемых в заявлении.
     */
    public static IDQLExpression getExpression4ExcludeEntrantIndividualProgressById(Object requestId, IDQLExpression progress)
    {
        return not(exists(IndProg2Request4Exclude.class,
                          IndProg2Request4Exclude.request().id().s(), requestId,
                          IndProg2Request4Exclude.individualProgress().s(), progress));
    }

    /**
     * @param request Выражение для Заявления абитуриента или само заявление (IDQLExpression, EntrantRequest, List of EntrantRequest)
     * @param progress Выражение для Индивидуального достижения абитуриента
     * @return Выражение для фильтрациии Индивидиуальных достижения абитуриента не учитываемых в заявлении.
     */
    public static IDQLExpression getExpression4ExcludeEntrantIndividualProgress(Object request, IDQLExpression progress)
    {
        return not(exists(IndProg2Request4Exclude.class,
                          IndProg2Request4Exclude.request().s(), request,
                          IndProg2Request4Exclude.individualProgress().s(), progress));
    }
}