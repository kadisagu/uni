/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e1.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 13.07.2012
 */
public class Model extends AbstractListExtractPubModel<SplitBudgetBachelorEntrantsStuListExtract>
{
}