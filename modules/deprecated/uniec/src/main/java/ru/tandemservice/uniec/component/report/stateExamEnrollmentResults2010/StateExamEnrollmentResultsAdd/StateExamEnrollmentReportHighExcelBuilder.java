/* $Id: StateExamEnrollmentReportExcelBuilder.java 10599 2009-11-17 08:33:06Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * логика создания excel файла для ВПО
 *
 * @author vip_delete
 * @since 24.04.2009
 */
class StateExamEnrollmentReportHighExcelBuilder implements IStateExamEnrollmentExcelBuilder
{
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] buildExcelContent(String highSchoolTitle,
                                    String enrollmentCampStage,
                                    String periodTitle,
                                    Model model,
                                    List<ReportRow> reportRowList) throws Exception
    {
        String paramsTitle = getParamsTitle(model);
        Long reportAlgorithmId = model.getReportAlgorithm().getId();
        boolean showEduGroups = model.isShowEduGroups();

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // create fonts
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial9 = new WritableFont(WritableFont.ARIAL, 9);
        WritableFont arial9bold = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
        WritableFont arial9boldGray = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
        arial9boldGray.setColour(Colour.GRAY_50);
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat rowTitleFormat = new WritableCellFormat(arial9bold);
        rowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        rowTitleFormat.setWrap(true);

        WritableCellFormat subRowTitleFormat = new WritableCellFormat(arial9);
        subRowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        subRowTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        subRowTitleFormat.setWrap(true);

        WritableCellFormat subRowTitleFormatGray = new WritableCellFormat(arial9boldGray);
        subRowTitleFormatGray.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        subRowTitleFormatGray.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat subSubRowTitleFormat = new WritableCellFormat(arial8);
        subSubRowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        subSubRowTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        // create list
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.setColumnView(0, 70);// ~ 490 пикселей

        // row 1
        sheet.addCell(new Label(1, 0, highSchoolTitle, new WritableCellFormat(arial10bold)));

        // row 2
        sheet.addCell(new Label(1, 1, "Сведения по результатам " + (Model.REPORT_ALGORITHM_EXAM == reportAlgorithmId ? "экзаменов" : "ЕГЭ") + " " + enrollmentCampStage + " (для программ ВПО) (по заявлениям " + periodTitle + ")", new WritableCellFormat(arial10bold)));

        // row 4
        sheet.addCell(new Label(0, 3, paramsTitle, new WritableCellFormat(arial8)));

        // write table by columns at this coordinates
        final int TABLE_START_ROW_INDEX = 4;
        final int TABLE_START_COLUMN_INDEX = 0;

        // usefull indexes
        int row;
        int column;

        // write table header
        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX;

            sheet.addCell(new Label(column, row, "Направление подготовки (специальность) / Предмет", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);

            sheet.addCell(new Label(column + 1, row, "Всего   чел.", headerFormat));
            sheet.mergeCells(column + 1, row, column + 1, row + 1);

            sheet.addCell(new Label(column + 2, row, "Средний балл", headerFormat));
            sheet.mergeCells(column + 2, row, column + 2, row + 1);

            sheet.addCell(new Label(column + 3, row, "Количество абитуриентов с результатами " + (reportAlgorithmId == Model.REPORT_ALGORITHM_EXAM ? "экзаменов" : "единого государственного экзамена") + ", распределенное по интервалам тестовых баллов (чел)", headerFormat));
            sheet.mergeCells(column + 3, row, column + 22, row);

            for (int i = 0; i < 20; i++)
                sheet.addCell(new Label(column + 3 + i, row + 1, i == 0 ? "0-5" : (i * 5 + 1) + "-" + ((i + 1) * 5), headerFormat));
        }

        // write table data
        {
            row = TABLE_START_ROW_INDEX + 2;
            column = TABLE_START_COLUMN_INDEX;

            for (ReportRow reportRow : reportRowList)
            {
                if (model.isGroupByBranchAndRepresentation())
                {
                    // рисуем row
                    sheet.addCell(new Label(column, row, reportRow.getTitle(), rowTitleFormat));
                    for (int i = 0; i < 22; i++)
                        sheet.addCell(new Label(column + i + 1, row, "", rowTitleFormat));
                    row++;
                }

                for (Map.Entry<EducationLevels, Set<ReportSubRow>> entry : reportRow.getSubRowMap().entrySet())
                {
                    if (showEduGroups)
                    {
                        // рисуем укрупненную группу
                        sheet.addCell(new Label(column, row, entry.getKey().getDisplayableTitle(), subRowTitleFormatGray));

                        // fill cells
                        int eduGroupRowIndex = row;
                        for (int i = 0; i < 22; i++)
                            sheet.addCell(new Label(column + i + 1, row, "", rowTitleFormat));
                        row++;

                        // определяем предметы
                        Map<Long, ReportSubSubRow> eduGroupMap = new HashMap<>();
                        for (ReportSubRow subRow : entry.getValue())
                        {
                            for (ReportSubSubRow key : subRow.getSubSubRowList())
                            {
                                ReportSubSubRow eduGroupRow = eduGroupMap.get(key.getId());
                                if (eduGroupRow == null)
                                    eduGroupMap.put(key.getId(), eduGroupRow = new ReportSubSubRow(key.getId(), key.getTitle(), new HashMap<Entrant, Integer>()));
                                if (key.getMarks() != null)
                                    eduGroupRow.getMarks().putAll(key.getMarks());
                            }
                        }
                        List<ReportSubSubRow> eduGroupRows = new ArrayList<>();
                        eduGroupRows.addAll(eduGroupMap.values());
                        Collections.sort(eduGroupRows, ITitled.TITLED_COMPARATOR);

                        // рисуем предметы укрупненной группы
                        Set<Entrant> eduGroupEntrantSet = new HashSet<>();
                        for (ReportSubSubRow eduGroupSubject : eduGroupRows)
                        {
                            sheet.addCell(new Label(column, row, "   " + eduGroupSubject.getTitle(), subRowTitleFormatGray));

                            int[] data = new int[20]; //0-5, 6-10, и тд
                            int sum = 0;
                            int ave = 0;
                            for (Map.Entry<Entrant, Integer> mark : eduGroupSubject.getMarks().entrySet())
                            {
                                eduGroupEntrantSet.add(mark.getKey());
                                int m = mark.getValue();
                                sum++;
                                ave += m;
                                int index = m == 0 ? 0 : (m - 1) / 5;
                                if (index < data.length)
                                    data[index]++;
                            }
                            if (sum != 0)
                                ave /= sum;
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, row, sum, subRowTitleFormatGray));
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + 2, row, ave, subRowTitleFormatGray));
                            for (int i = 0; i < 20; i++)
                                sheet.addCell(JExcelUtil.getNumberNullableCell(column + i + 3, row, data[i], subRowTitleFormatGray));
                            row++;
                        }
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, eduGroupRowIndex, eduGroupEntrantSet.size(), subRowTitleFormatGray));
                    }

                    // рисуем все subRow
                    for (ReportSubRow subRow : entry.getValue())
                    {
                        sheet.addCell(new Label(column, row, subRow.getTitle(), subRowTitleFormat));
                        // fill cells
                        int subRowIndex = row;
                        for (int i = 0; i < 22; i++)
                            sheet.addCell(new Label(column + i + 1, row, "", subRowTitleFormat));
                        row++;

                        // рисуем все предметы из набора экзаменов
                        Set<Entrant> entrantSet = new HashSet<>();
                        for (ReportSubSubRow subSubRow : subRow.getSubSubRowList())
                        {
                            sheet.addCell(new Label(column, row, "   " + subSubRow.getTitle(), subSubRowTitleFormat));
                            int[] data = new int[20]; //0-5, 6-10, и тд
                            int sum = 0;
                            int ave = 0;
                            if (subSubRow.getMarks() != null)
                                for (Map.Entry<Entrant, Integer> mark : subSubRow.getMarks().entrySet())
                                {
                                    entrantSet.add(mark.getKey());
                                    int m = mark.getValue();
                                    sum++;
                                    ave += m;
                                    int index = m == 0 ? 0 : (m - 1) / 5;
                                    if (index < data.length)
                                        data[index]++;
                                }
                            if (sum != 0)
                                ave /= sum;
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, row, sum, subSubRowTitleFormat));
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + 2, row, ave, subSubRowTitleFormat));
                            for (int i = 0; i < 20; i++)
                                sheet.addCell(JExcelUtil.getNumberNullableCell(column + i + 3, row, data[i], subSubRowTitleFormat));
                            row++;
                        }
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, subRowIndex, entrantSet.size(), subSubRowTitleFormat));
                    }
                }
            }
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    private String getParamsTitle(Model model)
    {
        // получаем строку параметров отчета
        List<String> paramsTitleList = new ArrayList<>();
        paramsTitleList.add("Форма обучения: " + model.getReport().getDevelopForm().getTitle());
        if (model.isDevelopConditionActive())
            paramsTitleList.add("Условие обучения: " + UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));
        paramsTitleList.add("Финансирование: " + model.getReport().getCompensationType().getShortTitle());
        if (model.isQualificationActive())
            paramsTitleList.add("Квалификация: " + UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, ", "));
        if (model.isStudentCategoryActive())
            paramsTitleList.add("Категория поступающего: " + UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, ", "));
        if (model.isFormativeOrgUnitActive())
            paramsTitleList.add("Формирующее подр.: " + UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_TYPE_TITLE, ", "));
        if (model.isTerritorialOrgUnitActive())
            paramsTitleList.add("Территориальное подр.: " + UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TYPE_TITLE, ", "));
        return StringUtils.join(paramsTitleList, "  ");
    }
}
