package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность base4ExamByDifferentSources

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("base4exambydifferentsources_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_base4exambydifferentsources"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("base4ExamByDifferentSources");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность entrant

		// создано обязательное свойство specialCondition4Exam
		{
			// создать колонку
			tool.createColumn("entrant_t", new DBColumn("specialcondition4exam_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update entrant_t set specialcondition4exam_p=? where specialcondition4exam_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("entrant_t", "specialcondition4exam_p", false);

		}

		// создано свойство base4ExamByDifferentSources
		{
			// создать колонку
			tool.createColumn("entrant_t", new DBColumn("base4exambydifferentsources_id", DBType.LONG));

		}
    }
}