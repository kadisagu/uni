/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.passDisciplineData;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 02.07.2013
 */
public class PassDisciplineDataBlock
{
    public static final String EDUCATION_SUBJECT_DS = "educationSubjectDS";
    public static final String SUBJECT_PASS_FORM_DS = "subjectPassFormDS";

    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, IReportParam<EnrollmentCampaign> enrollmentCampaignParam)
    {
        String name = dataSource.getName();

        if (EDUCATION_SUBJECT_DS.equals(name) || SUBJECT_PASS_FORM_DS.equals(name))
        {
            enrollmentCampaignParam.putParamIfActive(dataSource, ENROLLMENT_CAMPAIGN_PARAM);
        }
    }

    public static IDefaultComboDataSourceHandler createEducationSubjectDS(String name)
    {
        DefaultComboDataSourceHandler handler = new DefaultComboDataSourceHandler(name, Discipline2RealizationWayRelation.class, Discipline2RealizationWayRelation.educationSubject().title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                EnrollmentCampaign enrollmentCampaign = ep.context.get(ENROLLMENT_CAMPAIGN_PARAM);
                ep.dqlBuilder.where(
                        eq(property(Discipline2RealizationWayRelation.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign))
                );
            }

            @Override
            protected DSOutput prepareOutput(ExecutionParameters<DSInput, DSOutput> ep, boolean needEntitySelection)
            {
                DSOutput preOutput = super.prepareOutput(ep, needEntitySelection);
                List<DataWrapper> resultList = new ArrayList<>();
                for (Discipline2RealizationWayRelation item : preOutput.<Discipline2RealizationWayRelation>getRecordList())
                {
                    DataWrapper dataWrapper = new DataWrapper(item.getId(), item.getFullTitle(), item);
                    resultList.add(dataWrapper);
                }
                return ListOutputBuilder.get(ep.input, resultList).pageable(false).build();
            }
        };
        handler.setPopupListSize(500);
        return handler;
    }

    public static IDefaultComboDataSourceHandler createSubjectPassFormDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, SubjectPassForm.class, SubjectPassForm.title())
        {
            @Override
            protected ExecutionParameters<DSInput, DSOutput> prepareExecutionParameters(DSInput input, ExecutionContext context)
            {
                EnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN_PARAM);
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Discipline2RealizationFormRelation.class, "d")
                        .predicate(DQLPredicateType.distinct)
                        .column(property("e"))
                        .joinEntity("d", DQLJoinType.left, SubjectPassForm.class, "e",
                                    eq(
                                            property(Discipline2RealizationFormRelation.subjectPassForm().id().fromAlias("d")),
                                            property(SubjectPassForm.id().fromAlias("e"))
                                    ))
                        .where(and(
                                eq(
                                        property(Discipline2RealizationFormRelation.discipline().enrollmentCampaign().fromAlias("d")),
                                        value(enrollmentCampaign)
                                ),
                                eq(
                                        property(Discipline2RealizationFormRelation.type().code().fromAlias("d")),
                                        value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)
                                )
                        ));

                return new ExecutionParameters<>(input, new DSOutput(input), context, builder);
            }
        };
    }
}