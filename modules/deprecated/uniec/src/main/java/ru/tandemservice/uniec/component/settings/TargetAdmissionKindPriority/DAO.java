/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.TargetAdmissionKindPriority;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author agolubenko
 * @since 20.06.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getDataSource(), getCatalogItemList(TargetAdmissionKind.class));
    }

    @Override
    public void updatePriorityDown(Long targetAdmissionKindId)
    {
        changePriorityForTargetAdmissionKind(targetAdmissionKindId, 1);
    }

    @Override
    public void updatePriorityUp(Long targetAdmissionKindId)
    {
        changePriorityForTargetAdmissionKind(targetAdmissionKindId, -1);
    }

    private void changePriorityForTargetAdmissionKind(Long targetAdmissionKindId, int direction)
    {
        TargetAdmissionKind targetAdmissionKind = get(TargetAdmissionKind.class, targetAdmissionKindId);
        int priority = targetAdmissionKind.getPriority();
        int newPriority = priority + direction;

        TargetAdmissionKind near = getNear((TargetAdmissionKind) targetAdmissionKind.getParent(), newPriority);

        if (near != null)
        {
            Session session = getSession();

            near.setPriority(priority);
            session.update(near);

            targetAdmissionKind.setPriority(newPriority);
            session.update(targetAdmissionKind);
        }
    }

    private TargetAdmissionKind getNear(TargetAdmissionKind parent, int priority)
    {
        Criteria criteria = getSession().createCriteria(TargetAdmissionKind.class);
        if (parent == null)
        {
            criteria.add(Restrictions.isNull(TargetAdmissionKind.L_PARENT));
        }
        else
        {
            criteria.add(Restrictions.eq(TargetAdmissionKind.L_PARENT, parent));
        }
        criteria.add(Restrictions.eq(TargetAdmissionKind.P_PRIORITY, priority));
        return (TargetAdmissionKind) criteria.uniqueResult();
    }
}
