package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневный рейтинг абитуриентов по направлению подготовки (специальности) (форма 2)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantDailyRatingByED2ReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report";
    public static final String ENTITY_NAME = "entrantDailyRatingByED2Report";
    public static final int VERSION_HASH = -570105854;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_HEADER_WITHOUT_PARAM = "headerWithoutParam";
    public static final String P_ONLY_WITH_ORIGINALS = "onlyWithOriginals";
    public static final String P_WITHOUT_DOCUMENT_INFO = "withoutDocumentInfo";
    public static final String P_WITHOUT_DETAIL_SUM_MARK = "withoutDetailSumMark";
    public static final String P_SHOW_DISCIPLINE_TITLES = "showDisciplineTitles";
    public static final String P_SHOW_REQUESTED_DIRECTION_PRIORITY = "showRequestedDirectionPriority";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String P_NOT_PRINT_SPES_WITHOUT_REQUEST = "notPrintSpesWithoutRequest";
    public static final String P_NOT_PRINT_NUM_INFO = "notPrintNumInfo";
    public static final String P_WITHOUT_AGREE4_ENROLLMENT = "withoutAgree4Enrollment";
    public static final String L_COMPETITION_KIND = "competitionKind";
    public static final String P_WITHOUT_TARGET_ADMISSION = "withoutTargetAdmission";
    public static final String P_SORT_BY_ORIGINAL_DOC_HANDED_IN = "sortByOriginalDocHandedIn";
    public static final String P_WITHOUT_ENROLLED = "withoutEnrolled";
    public static final String P_ENTRANT_CUSTOM_STATE_TITLE = "entrantCustomStateTitle";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategoryTitle;     // Категория поступающего
    private boolean _headerWithoutParam = true;     // Печтать заголовок без параметров
    private boolean _onlyWithOriginals;     // Не включать абитуриентов без оригиналов документов
    private boolean _withoutDocumentInfo;     // Без информации о документах об образовании
    private boolean _withoutDetailSumMark;     // Без расшифровки суммы баллов
    private boolean _showDisciplineTitles;     // Выводить названия дисциплин
    private boolean _showRequestedDirectionPriority;     // Выводить приоритеты
    private String _qualificationTitle;     // Квалификация
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private boolean _notPrintSpesWithoutRequest;     // Не печатать направления/специальности, по которым нет заявлений
    private boolean _notPrintNumInfo;     // Не печатать информацию о цифрах приема
    private boolean _withoutAgree4Enrollment = true;     // Без информации о согласии на зачисление
    private CompetitionKind _competitionKind;     // Вид конкурса
    private boolean _withoutTargetAdmission = false;     // Без сведений о целевом приеме
    private boolean _sortByOriginalDocHandedIn = false;     // Сортировать с выделением оригиналов документов
    private boolean _withoutEnrolled = false;     // Не выводить зачисленных абитуриентов
    private String _entrantCustomStateTitle;     // Дополнительные статусы абитуриентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Печтать заголовок без параметров. Свойство не может быть null.
     */
    @NotNull
    public boolean isHeaderWithoutParam()
    {
        return _headerWithoutParam;
    }

    /**
     * @param headerWithoutParam Печтать заголовок без параметров. Свойство не может быть null.
     */
    public void setHeaderWithoutParam(boolean headerWithoutParam)
    {
        dirty(_headerWithoutParam, headerWithoutParam);
        _headerWithoutParam = headerWithoutParam;
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOnlyWithOriginals()
    {
        return _onlyWithOriginals;
    }

    /**
     * @param onlyWithOriginals Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    public void setOnlyWithOriginals(boolean onlyWithOriginals)
    {
        dirty(_onlyWithOriginals, onlyWithOriginals);
        _onlyWithOriginals = onlyWithOriginals;
    }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutDocumentInfo()
    {
        return _withoutDocumentInfo;
    }

    /**
     * @param withoutDocumentInfo Без информации о документах об образовании. Свойство не может быть null.
     */
    public void setWithoutDocumentInfo(boolean withoutDocumentInfo)
    {
        dirty(_withoutDocumentInfo, withoutDocumentInfo);
        _withoutDocumentInfo = withoutDocumentInfo;
    }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutDetailSumMark()
    {
        return _withoutDetailSumMark;
    }

    /**
     * @param withoutDetailSumMark Без расшифровки суммы баллов. Свойство не может быть null.
     */
    public void setWithoutDetailSumMark(boolean withoutDetailSumMark)
    {
        dirty(_withoutDetailSumMark, withoutDetailSumMark);
        _withoutDetailSumMark = withoutDetailSumMark;
    }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowDisciplineTitles()
    {
        return _showDisciplineTitles;
    }

    /**
     * @param showDisciplineTitles Выводить названия дисциплин. Свойство не может быть null.
     */
    public void setShowDisciplineTitles(boolean showDisciplineTitles)
    {
        dirty(_showDisciplineTitles, showDisciplineTitles);
        _showDisciplineTitles = showDisciplineTitles;
    }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowRequestedDirectionPriority()
    {
        return _showRequestedDirectionPriority;
    }

    /**
     * @param showRequestedDirectionPriority Выводить приоритеты. Свойство не может быть null.
     */
    public void setShowRequestedDirectionPriority(boolean showRequestedDirectionPriority)
    {
        dirty(_showRequestedDirectionPriority, showRequestedDirectionPriority);
        _showRequestedDirectionPriority = showRequestedDirectionPriority;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintSpesWithoutRequest()
    {
        return _notPrintSpesWithoutRequest;
    }

    /**
     * @param notPrintSpesWithoutRequest Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    public void setNotPrintSpesWithoutRequest(boolean notPrintSpesWithoutRequest)
    {
        dirty(_notPrintSpesWithoutRequest, notPrintSpesWithoutRequest);
        _notPrintSpesWithoutRequest = notPrintSpesWithoutRequest;
    }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintNumInfo()
    {
        return _notPrintNumInfo;
    }

    /**
     * @param notPrintNumInfo Не печатать информацию о цифрах приема. Свойство не может быть null.
     */
    public void setNotPrintNumInfo(boolean notPrintNumInfo)
    {
        dirty(_notPrintNumInfo, notPrintNumInfo);
        _notPrintNumInfo = notPrintNumInfo;
    }

    /**
     * @return Без информации о согласии на зачисление. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutAgree4Enrollment()
    {
        return _withoutAgree4Enrollment;
    }

    /**
     * @param withoutAgree4Enrollment Без информации о согласии на зачисление. Свойство не может быть null.
     */
    public void setWithoutAgree4Enrollment(boolean withoutAgree4Enrollment)
    {
        dirty(_withoutAgree4Enrollment, withoutAgree4Enrollment);
        _withoutAgree4Enrollment = withoutAgree4Enrollment;
    }

    /**
     * @return Вид конкурса.
     */
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    /**
     * @param competitionKind Вид конкурса.
     */
    public void setCompetitionKind(CompetitionKind competitionKind)
    {
        dirty(_competitionKind, competitionKind);
        _competitionKind = competitionKind;
    }

    /**
     * @return Без сведений о целевом приеме. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutTargetAdmission()
    {
        return _withoutTargetAdmission;
    }

    /**
     * @param withoutTargetAdmission Без сведений о целевом приеме. Свойство не может быть null.
     */
    public void setWithoutTargetAdmission(boolean withoutTargetAdmission)
    {
        dirty(_withoutTargetAdmission, withoutTargetAdmission);
        _withoutTargetAdmission = withoutTargetAdmission;
    }

    /**
     * @return Сортировать с выделением оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isSortByOriginalDocHandedIn()
    {
        return _sortByOriginalDocHandedIn;
    }

    /**
     * @param sortByOriginalDocHandedIn Сортировать с выделением оригиналов документов. Свойство не может быть null.
     */
    public void setSortByOriginalDocHandedIn(boolean sortByOriginalDocHandedIn)
    {
        dirty(_sortByOriginalDocHandedIn, sortByOriginalDocHandedIn);
        _sortByOriginalDocHandedIn = sortByOriginalDocHandedIn;
    }

    /**
     * @return Не выводить зачисленных абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutEnrolled()
    {
        return _withoutEnrolled;
    }

    /**
     * @param withoutEnrolled Не выводить зачисленных абитуриентов. Свойство не может быть null.
     */
    public void setWithoutEnrolled(boolean withoutEnrolled)
    {
        dirty(_withoutEnrolled, withoutEnrolled);
        _withoutEnrolled = withoutEnrolled;
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     */
    @Length(max=255)
    public String getEntrantCustomStateTitle()
    {
        return _entrantCustomStateTitle;
    }

    /**
     * @param entrantCustomStateTitle Дополнительные статусы абитуриентов.
     */
    public void setEntrantCustomStateTitle(String entrantCustomStateTitle)
    {
        dirty(_entrantCustomStateTitle, entrantCustomStateTitle);
        _entrantCustomStateTitle = entrantCustomStateTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntrantDailyRatingByED2ReportGen)
        {
            setEnrollmentCampaign(((EntrantDailyRatingByED2Report)another).getEnrollmentCampaign());
            setDateFrom(((EntrantDailyRatingByED2Report)another).getDateFrom());
            setDateTo(((EntrantDailyRatingByED2Report)another).getDateTo());
            setCompensationType(((EntrantDailyRatingByED2Report)another).getCompensationType());
            setStudentCategoryTitle(((EntrantDailyRatingByED2Report)another).getStudentCategoryTitle());
            setHeaderWithoutParam(((EntrantDailyRatingByED2Report)another).isHeaderWithoutParam());
            setOnlyWithOriginals(((EntrantDailyRatingByED2Report)another).isOnlyWithOriginals());
            setWithoutDocumentInfo(((EntrantDailyRatingByED2Report)another).isWithoutDocumentInfo());
            setWithoutDetailSumMark(((EntrantDailyRatingByED2Report)another).isWithoutDetailSumMark());
            setShowDisciplineTitles(((EntrantDailyRatingByED2Report)another).isShowDisciplineTitles());
            setShowRequestedDirectionPriority(((EntrantDailyRatingByED2Report)another).isShowRequestedDirectionPriority());
            setQualificationTitle(((EntrantDailyRatingByED2Report)another).getQualificationTitle());
            setDevelopFormTitle(((EntrantDailyRatingByED2Report)another).getDevelopFormTitle());
            setDevelopConditionTitle(((EntrantDailyRatingByED2Report)another).getDevelopConditionTitle());
            setEnrollmentDirection(((EntrantDailyRatingByED2Report)another).getEnrollmentDirection());
            setNotPrintSpesWithoutRequest(((EntrantDailyRatingByED2Report)another).isNotPrintSpesWithoutRequest());
            setNotPrintNumInfo(((EntrantDailyRatingByED2Report)another).isNotPrintNumInfo());
            setWithoutAgree4Enrollment(((EntrantDailyRatingByED2Report)another).isWithoutAgree4Enrollment());
            setCompetitionKind(((EntrantDailyRatingByED2Report)another).getCompetitionKind());
            setWithoutTargetAdmission(((EntrantDailyRatingByED2Report)another).isWithoutTargetAdmission());
            setSortByOriginalDocHandedIn(((EntrantDailyRatingByED2Report)another).isSortByOriginalDocHandedIn());
            setWithoutEnrolled(((EntrantDailyRatingByED2Report)another).isWithoutEnrolled());
            setEntrantCustomStateTitle(((EntrantDailyRatingByED2Report)another).getEntrantCustomStateTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantDailyRatingByED2ReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantDailyRatingByED2Report.class;
        }

        public T newInstance()
        {
            return (T) new EntrantDailyRatingByED2Report();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "headerWithoutParam":
                    return obj.isHeaderWithoutParam();
                case "onlyWithOriginals":
                    return obj.isOnlyWithOriginals();
                case "withoutDocumentInfo":
                    return obj.isWithoutDocumentInfo();
                case "withoutDetailSumMark":
                    return obj.isWithoutDetailSumMark();
                case "showDisciplineTitles":
                    return obj.isShowDisciplineTitles();
                case "showRequestedDirectionPriority":
                    return obj.isShowRequestedDirectionPriority();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "notPrintSpesWithoutRequest":
                    return obj.isNotPrintSpesWithoutRequest();
                case "notPrintNumInfo":
                    return obj.isNotPrintNumInfo();
                case "withoutAgree4Enrollment":
                    return obj.isWithoutAgree4Enrollment();
                case "competitionKind":
                    return obj.getCompetitionKind();
                case "withoutTargetAdmission":
                    return obj.isWithoutTargetAdmission();
                case "sortByOriginalDocHandedIn":
                    return obj.isSortByOriginalDocHandedIn();
                case "withoutEnrolled":
                    return obj.isWithoutEnrolled();
                case "entrantCustomStateTitle":
                    return obj.getEntrantCustomStateTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "headerWithoutParam":
                    obj.setHeaderWithoutParam((Boolean) value);
                    return;
                case "onlyWithOriginals":
                    obj.setOnlyWithOriginals((Boolean) value);
                    return;
                case "withoutDocumentInfo":
                    obj.setWithoutDocumentInfo((Boolean) value);
                    return;
                case "withoutDetailSumMark":
                    obj.setWithoutDetailSumMark((Boolean) value);
                    return;
                case "showDisciplineTitles":
                    obj.setShowDisciplineTitles((Boolean) value);
                    return;
                case "showRequestedDirectionPriority":
                    obj.setShowRequestedDirectionPriority((Boolean) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "notPrintSpesWithoutRequest":
                    obj.setNotPrintSpesWithoutRequest((Boolean) value);
                    return;
                case "notPrintNumInfo":
                    obj.setNotPrintNumInfo((Boolean) value);
                    return;
                case "withoutAgree4Enrollment":
                    obj.setWithoutAgree4Enrollment((Boolean) value);
                    return;
                case "competitionKind":
                    obj.setCompetitionKind((CompetitionKind) value);
                    return;
                case "withoutTargetAdmission":
                    obj.setWithoutTargetAdmission((Boolean) value);
                    return;
                case "sortByOriginalDocHandedIn":
                    obj.setSortByOriginalDocHandedIn((Boolean) value);
                    return;
                case "withoutEnrolled":
                    obj.setWithoutEnrolled((Boolean) value);
                    return;
                case "entrantCustomStateTitle":
                    obj.setEntrantCustomStateTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "headerWithoutParam":
                        return true;
                case "onlyWithOriginals":
                        return true;
                case "withoutDocumentInfo":
                        return true;
                case "withoutDetailSumMark":
                        return true;
                case "showDisciplineTitles":
                        return true;
                case "showRequestedDirectionPriority":
                        return true;
                case "qualificationTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "notPrintSpesWithoutRequest":
                        return true;
                case "notPrintNumInfo":
                        return true;
                case "withoutAgree4Enrollment":
                        return true;
                case "competitionKind":
                        return true;
                case "withoutTargetAdmission":
                        return true;
                case "sortByOriginalDocHandedIn":
                        return true;
                case "withoutEnrolled":
                        return true;
                case "entrantCustomStateTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "headerWithoutParam":
                    return true;
                case "onlyWithOriginals":
                    return true;
                case "withoutDocumentInfo":
                    return true;
                case "withoutDetailSumMark":
                    return true;
                case "showDisciplineTitles":
                    return true;
                case "showRequestedDirectionPriority":
                    return true;
                case "qualificationTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "notPrintSpesWithoutRequest":
                    return true;
                case "notPrintNumInfo":
                    return true;
                case "withoutAgree4Enrollment":
                    return true;
                case "competitionKind":
                    return true;
                case "withoutTargetAdmission":
                    return true;
                case "sortByOriginalDocHandedIn":
                    return true;
                case "withoutEnrolled":
                    return true;
                case "entrantCustomStateTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategoryTitle":
                    return String.class;
                case "headerWithoutParam":
                    return Boolean.class;
                case "onlyWithOriginals":
                    return Boolean.class;
                case "withoutDocumentInfo":
                    return Boolean.class;
                case "withoutDetailSumMark":
                    return Boolean.class;
                case "showDisciplineTitles":
                    return Boolean.class;
                case "showRequestedDirectionPriority":
                    return Boolean.class;
                case "qualificationTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "notPrintSpesWithoutRequest":
                    return Boolean.class;
                case "notPrintNumInfo":
                    return Boolean.class;
                case "withoutAgree4Enrollment":
                    return Boolean.class;
                case "competitionKind":
                    return CompetitionKind.class;
                case "withoutTargetAdmission":
                    return Boolean.class;
                case "sortByOriginalDocHandedIn":
                    return Boolean.class;
                case "withoutEnrolled":
                    return Boolean.class;
                case "entrantCustomStateTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantDailyRatingByED2Report> _dslPath = new Path<EntrantDailyRatingByED2Report>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantDailyRatingByED2Report");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Печтать заголовок без параметров. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isHeaderWithoutParam()
     */
    public static PropertyPath<Boolean> headerWithoutParam()
    {
        return _dslPath.headerWithoutParam();
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isOnlyWithOriginals()
     */
    public static PropertyPath<Boolean> onlyWithOriginals()
    {
        return _dslPath.onlyWithOriginals();
    }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutDocumentInfo()
     */
    public static PropertyPath<Boolean> withoutDocumentInfo()
    {
        return _dslPath.withoutDocumentInfo();
    }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutDetailSumMark()
     */
    public static PropertyPath<Boolean> withoutDetailSumMark()
    {
        return _dslPath.withoutDetailSumMark();
    }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isShowDisciplineTitles()
     */
    public static PropertyPath<Boolean> showDisciplineTitles()
    {
        return _dslPath.showDisciplineTitles();
    }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isShowRequestedDirectionPriority()
     */
    public static PropertyPath<Boolean> showRequestedDirectionPriority()
    {
        return _dslPath.showRequestedDirectionPriority();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isNotPrintSpesWithoutRequest()
     */
    public static PropertyPath<Boolean> notPrintSpesWithoutRequest()
    {
        return _dslPath.notPrintSpesWithoutRequest();
    }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isNotPrintNumInfo()
     */
    public static PropertyPath<Boolean> notPrintNumInfo()
    {
        return _dslPath.notPrintNumInfo();
    }

    /**
     * @return Без информации о согласии на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutAgree4Enrollment()
     */
    public static PropertyPath<Boolean> withoutAgree4Enrollment()
    {
        return _dslPath.withoutAgree4Enrollment();
    }

    /**
     * @return Вид конкурса.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getCompetitionKind()
     */
    public static CompetitionKind.Path<CompetitionKind> competitionKind()
    {
        return _dslPath.competitionKind();
    }

    /**
     * @return Без сведений о целевом приеме. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutTargetAdmission()
     */
    public static PropertyPath<Boolean> withoutTargetAdmission()
    {
        return _dslPath.withoutTargetAdmission();
    }

    /**
     * @return Сортировать с выделением оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isSortByOriginalDocHandedIn()
     */
    public static PropertyPath<Boolean> sortByOriginalDocHandedIn()
    {
        return _dslPath.sortByOriginalDocHandedIn();
    }

    /**
     * @return Не выводить зачисленных абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutEnrolled()
     */
    public static PropertyPath<Boolean> withoutEnrolled()
    {
        return _dslPath.withoutEnrolled();
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getEntrantCustomStateTitle()
     */
    public static PropertyPath<String> entrantCustomStateTitle()
    {
        return _dslPath.entrantCustomStateTitle();
    }

    public static class Path<E extends EntrantDailyRatingByED2Report> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<Boolean> _headerWithoutParam;
        private PropertyPath<Boolean> _onlyWithOriginals;
        private PropertyPath<Boolean> _withoutDocumentInfo;
        private PropertyPath<Boolean> _withoutDetailSumMark;
        private PropertyPath<Boolean> _showDisciplineTitles;
        private PropertyPath<Boolean> _showRequestedDirectionPriority;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private PropertyPath<Boolean> _notPrintSpesWithoutRequest;
        private PropertyPath<Boolean> _notPrintNumInfo;
        private PropertyPath<Boolean> _withoutAgree4Enrollment;
        private CompetitionKind.Path<CompetitionKind> _competitionKind;
        private PropertyPath<Boolean> _withoutTargetAdmission;
        private PropertyPath<Boolean> _sortByOriginalDocHandedIn;
        private PropertyPath<Boolean> _withoutEnrolled;
        private PropertyPath<String> _entrantCustomStateTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantDailyRatingByED2ReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantDailyRatingByED2ReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(EntrantDailyRatingByED2ReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Печтать заголовок без параметров. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isHeaderWithoutParam()
     */
        public PropertyPath<Boolean> headerWithoutParam()
        {
            if(_headerWithoutParam == null )
                _headerWithoutParam = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_HEADER_WITHOUT_PARAM, this);
            return _headerWithoutParam;
        }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isOnlyWithOriginals()
     */
        public PropertyPath<Boolean> onlyWithOriginals()
        {
            if(_onlyWithOriginals == null )
                _onlyWithOriginals = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_ONLY_WITH_ORIGINALS, this);
            return _onlyWithOriginals;
        }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutDocumentInfo()
     */
        public PropertyPath<Boolean> withoutDocumentInfo()
        {
            if(_withoutDocumentInfo == null )
                _withoutDocumentInfo = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_WITHOUT_DOCUMENT_INFO, this);
            return _withoutDocumentInfo;
        }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutDetailSumMark()
     */
        public PropertyPath<Boolean> withoutDetailSumMark()
        {
            if(_withoutDetailSumMark == null )
                _withoutDetailSumMark = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_WITHOUT_DETAIL_SUM_MARK, this);
            return _withoutDetailSumMark;
        }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isShowDisciplineTitles()
     */
        public PropertyPath<Boolean> showDisciplineTitles()
        {
            if(_showDisciplineTitles == null )
                _showDisciplineTitles = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_SHOW_DISCIPLINE_TITLES, this);
            return _showDisciplineTitles;
        }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isShowRequestedDirectionPriority()
     */
        public PropertyPath<Boolean> showRequestedDirectionPriority()
        {
            if(_showRequestedDirectionPriority == null )
                _showRequestedDirectionPriority = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_SHOW_REQUESTED_DIRECTION_PRIORITY, this);
            return _showRequestedDirectionPriority;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EntrantDailyRatingByED2ReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(EntrantDailyRatingByED2ReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntrantDailyRatingByED2ReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isNotPrintSpesWithoutRequest()
     */
        public PropertyPath<Boolean> notPrintSpesWithoutRequest()
        {
            if(_notPrintSpesWithoutRequest == null )
                _notPrintSpesWithoutRequest = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_NOT_PRINT_SPES_WITHOUT_REQUEST, this);
            return _notPrintSpesWithoutRequest;
        }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isNotPrintNumInfo()
     */
        public PropertyPath<Boolean> notPrintNumInfo()
        {
            if(_notPrintNumInfo == null )
                _notPrintNumInfo = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_NOT_PRINT_NUM_INFO, this);
            return _notPrintNumInfo;
        }

    /**
     * @return Без информации о согласии на зачисление. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutAgree4Enrollment()
     */
        public PropertyPath<Boolean> withoutAgree4Enrollment()
        {
            if(_withoutAgree4Enrollment == null )
                _withoutAgree4Enrollment = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_WITHOUT_AGREE4_ENROLLMENT, this);
            return _withoutAgree4Enrollment;
        }

    /**
     * @return Вид конкурса.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getCompetitionKind()
     */
        public CompetitionKind.Path<CompetitionKind> competitionKind()
        {
            if(_competitionKind == null )
                _competitionKind = new CompetitionKind.Path<CompetitionKind>(L_COMPETITION_KIND, this);
            return _competitionKind;
        }

    /**
     * @return Без сведений о целевом приеме. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutTargetAdmission()
     */
        public PropertyPath<Boolean> withoutTargetAdmission()
        {
            if(_withoutTargetAdmission == null )
                _withoutTargetAdmission = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_WITHOUT_TARGET_ADMISSION, this);
            return _withoutTargetAdmission;
        }

    /**
     * @return Сортировать с выделением оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isSortByOriginalDocHandedIn()
     */
        public PropertyPath<Boolean> sortByOriginalDocHandedIn()
        {
            if(_sortByOriginalDocHandedIn == null )
                _sortByOriginalDocHandedIn = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_SORT_BY_ORIGINAL_DOC_HANDED_IN, this);
            return _sortByOriginalDocHandedIn;
        }

    /**
     * @return Не выводить зачисленных абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#isWithoutEnrolled()
     */
        public PropertyPath<Boolean> withoutEnrolled()
        {
            if(_withoutEnrolled == null )
                _withoutEnrolled = new PropertyPath<Boolean>(EntrantDailyRatingByED2ReportGen.P_WITHOUT_ENROLLED, this);
            return _withoutEnrolled;
        }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.uniec.entity.report.EntrantDailyRatingByED2Report#getEntrantCustomStateTitle()
     */
        public PropertyPath<String> entrantCustomStateTitle()
        {
            if(_entrantCustomStateTitle == null )
                _entrantCustomStateTitle = new PropertyPath<String>(EntrantDailyRatingByED2ReportGen.P_ENTRANT_CUSTOM_STATE_TITLE, this);
            return _entrantCustomStateTitle;
        }

        public Class getEntityClass()
        {
            return EntrantDailyRatingByED2Report.class;
        }

        public String getEntityName()
        {
            return "entrantDailyRatingByED2Report";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
