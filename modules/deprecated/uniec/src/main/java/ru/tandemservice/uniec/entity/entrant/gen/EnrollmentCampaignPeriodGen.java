package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Период приемной кампании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCampaignPeriodGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod";
    public static final String ENTITY_NAME = "enrollmentCampaignPeriod";
    public static final int VERSION_HASH = -1220056849;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private Date _dateFrom;     // Дата начала периода
    private Date _dateTo;     // Дата окончания периода
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала периода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Дата начала периода. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Дата окончания периода. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Дата окончания периода. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCampaignPeriodGen)
        {
            setDateFrom(((EnrollmentCampaignPeriod)another).getDateFrom());
            setDateTo(((EnrollmentCampaignPeriod)another).getDateTo());
            setEnrollmentCampaign(((EnrollmentCampaignPeriod)another).getEnrollmentCampaign());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCampaignPeriodGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCampaignPeriod.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCampaignPeriod();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "enrollmentCampaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "enrollmentCampaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCampaignPeriod> _dslPath = new Path<EnrollmentCampaignPeriod>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCampaignPeriod");
    }
            

    /**
     * @return Дата начала периода. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Дата окончания периода. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends EnrollmentCampaignPeriod> extends EntityPath<E>
    {
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала периода. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrollmentCampaignPeriodGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Дата окончания периода. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrollmentCampaignPeriodGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return EnrollmentCampaignPeriod.class;
        }

        public String getEntityName()
        {
            return "enrollmentCampaignPeriod";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
