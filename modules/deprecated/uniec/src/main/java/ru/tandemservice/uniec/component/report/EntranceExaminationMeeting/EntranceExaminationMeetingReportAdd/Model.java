/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters.Filter;

/**
 * @author agolubenko
 * @since 10.10.2008
 */
@Input(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    public static String ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS = "по ходу приема документов";
    public static String ENROLLMENT_CAMPAIGN_STAGE_EXAMS = "по результатам сдачи вступительных испытаний";
    public static String ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT = "по результатам зачисления";

    private static final Parameters FILTER_PARAMETERS = new Parameters();
    static
    {
        FILTER_PARAMETERS.setRequired(true, Filter.ENROLLMENT_CAMPAIGN, Filter.FORMATIVE_ORG_UNIT, Filter.DEVELOP_FORM);
    }

    private EntranceExaminationMeetingReport _report = new EntranceExaminationMeetingReport();

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _studentCategoryModel;
    private ISelectModel _qualificationListModel;

    private List<Qualifications> _qualificationList;
    private List<StudentCategory> _studentCategoryList;
    private List<CompensationType> _compensationTypeList;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<String> _enrollmentCampaignStageList = Arrays.asList(ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS, ENROLLMENT_CAMPAIGN_STAGE_EXAMS, ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT);

    private boolean _territorialOrgUnitActive;
    private boolean _developConditionActive;
    private boolean _developTechActive;
    private boolean _developPeriodActive;
    private boolean _studentCategoryActive;
    private boolean _qualificationActive;

    private boolean _byAllEducationLevels;
    private boolean _pickOutUnlicensedEducationLevels;
    
    private List<OrgUnit> _territorialOrgUnitList = new ArrayList<OrgUnit>();
    private List<DevelopTech> _developTechList = new ArrayList<DevelopTech>();
    private List<DevelopCondition> _developConditionList = new ArrayList<DevelopCondition>();
    private List<DevelopPeriod> _developPeriodList = new ArrayList<DevelopPeriod>();

    private IPrincipalContext _principalContext;

    private Long _orgUnitId;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getReport().getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getReport().setEnrollmentCampaign(enrollmentCampaign);
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getFormativeOrgUnit());
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return (isTerritorialOrgUnitActive()) ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return (!isByAllEducationLevels()) ? MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEducationLevelsHighSchool()) : null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getDevelopForm());
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return (isDevelopConditionActive()) ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return (isDevelopTechActive()) ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return (isDevelopPeriodActive()) ? getDevelopPeriodList() : null;
    }

    @Override
    public Parameters getParameters()
    {
        return FILTER_PARAMETERS;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    public EntranceExaminationMeetingReport getReport()
    {
        return _report;
    }

    public void setReport(EntranceExaminationMeetingReport report)
    {
        _report = report;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public List<String> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<String> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public boolean isByAllEducationLevels()
    {
        return _byAllEducationLevels;
    }

    public void setByAllEducationLevels(boolean byAllEducationLevels)
    {
        _byAllEducationLevels = byAllEducationLevels;
    }

    public boolean isPickOutUnlicensedEducationLevels()
    {
        return _pickOutUnlicensedEducationLevels;
    }

    public void setPickOutUnlicensedEducationLevels(boolean pickOutUnlicensedEducationLevels)
    {
        _pickOutUnlicensedEducationLevels = pickOutUnlicensedEducationLevels;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public boolean isFormativeDisabled()
    {
        return getOrgUnitId() != null;
    }
}