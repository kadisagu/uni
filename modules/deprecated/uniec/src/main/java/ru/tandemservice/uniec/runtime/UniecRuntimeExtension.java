/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;

/**
 * @author agolubenko
 * @since 02.06.2009
 */
public class UniecRuntimeExtension implements IRuntimeExtension
{
    private IUniecRuntimeExtensionDAO _uniecRuntimeExtensionDAO;

    public IUniecRuntimeExtensionDAO getUniecRuntimeExtensionDAO()
    {
        return _uniecRuntimeExtensionDAO;
    }

    public void setUniecRuntimeExtensionDAO(IUniecRuntimeExtensionDAO uniecRuntimeExtensionDAO)
    {
        _uniecRuntimeExtensionDAO = uniecRuntimeExtensionDAO;
    }

    @Override
    public void init(Object object)
    {
        //_uniecRuntimeExtensionDAO.doInitNotUsedRelationDegrees();
    }

    @Override
    public void destroy()
    {

    }
}
