// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author oleyba
 * @since 04.05.2010
 */
public class UniecOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniec");
        config.setName("uniec-sec-config");
        config.setTitle("");

        ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();
            ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            PermissionGroupMeta pgEntrantTab = PermissionMetaUtil.createPermissionGroup(config, code + "EntrantsTabPermissionGroup", "Вкладка «(Старый) Абитуриенты»");
            PermissionMetaUtil.createGroupRelation(config, pgEntrantTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgEntrantTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgEntrantTab, "orgUnit_viewEntrantTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEntrantTab, "orgUnit_printEntantList_" + code, "Печать списка абитуриентов");

            PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "UniecReportPermissionGroup", "Отчеты модуля «(Старый) Абитуриенты»");
            PermissionMetaUtil.createPermission(permissionGroupReports, "viewEntranceExaminationMeetingReportList_" + code, "Протокол заседания приемной комиссии (с выделением групп «Общий прием» и «Целевой прием»)");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}