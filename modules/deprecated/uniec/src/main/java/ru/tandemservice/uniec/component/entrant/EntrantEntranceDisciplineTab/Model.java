/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author vip_delete
 * @since 16.03.2009
 */
@State({@Bind(key = "entrantId", binding = "entrant.id")})
public class Model
{
    public static final String COLUMN_ENROLLMENT_DIRECTION_TITLE = "enrollmentDirectionTitle";
    public static final String COLUMN_ENROLLMENT_DISCIPLINES_TITLE = "enrollmentDisciplinesTitle";

    private Entrant _entrant = new Entrant();
    private boolean _formingForEntrant;
    private List<EntrantRequest> _entrantRequestList;
    private EntrantRequest _entrantRequest;
    private Set<RequestedEnrollmentDirection> _nonActualDisciplinesDirectionSet = new HashSet<>();
    private Set<ExamPassDiscipline> _nonActualExamPassSet = new HashSet<>();

    // ientrantId -> entrantExamList
    private Map<Long, EntrantExamListWrapper> _entrantExamListMap = new HashMap<>();

    // examList -> set of examPassDiscipline
    private Map<EntrantExamList, Set<ExamPassDiscipline>> _examPassMap = new HashMap<>();

    // entrantRequest -> dataSource
    private Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> _entranceDisciplineDataSourceMap = new HashMap<>();

    // direction -> list of chosenDiscipline
    private Map<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> _chosenEntranceDisciplineMap = new HashMap<>();

    public EntrantExamListWrapper getCurrentExamListWrapper()
    {
        return _entrantExamListMap.get(_formingForEntrant ? _entrant.getId() : _entrantRequest.getId());
    }
    
    public boolean isAccessible()
    {
        return !_entrant.isArchival();
    }

    // Getters & Setters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public boolean isFormingForEntrant()
    {
        return _formingForEntrant;
    }

    public void setFormingForEntrant(boolean formingForEntrant)
    {
        _formingForEntrant = formingForEntrant;
    }

    public List<EntrantRequest> getEntrantRequestList()
    {
        return _entrantRequestList;
    }

    public void setEntrantRequestList(List<EntrantRequest> entrantRequestList)
    {
        _entrantRequestList = entrantRequestList;
    }

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public Set<ExamPassDiscipline> getNonActualExamPassSet()
    {
        return _nonActualExamPassSet;
    }

    public void setNonActualExamPassSet(Set<ExamPassDiscipline> nonActualExamPassSet)
    {
        _nonActualExamPassSet = nonActualExamPassSet;
    }

    public Map<Long, EntrantExamListWrapper> getEntrantExamListMap()
    {
        return _entrantExamListMap;
    }

    public void setEntrantExamListMap(Map<Long, EntrantExamListWrapper> entrantExamListMap)
    {
        _entrantExamListMap = entrantExamListMap;
    }

    public Map<EntrantExamList, Set<ExamPassDiscipline>> getExamPassMap()
    {
        return _examPassMap;
    }

    public void setExamPassMap(Map<EntrantExamList, Set<ExamPassDiscipline>> examPassMap)
    {
        _examPassMap = examPassMap;
    }

    public Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> getEntranceDisciplineDataSourceMap()
    {
        return _entranceDisciplineDataSourceMap;
    }

    public void setEntranceDisciplineDataSourceMap(Map<EntrantRequest, DynamicListDataSource<RequestedEnrollmentDirection>> entranceDisciplineDataSourceMap)
    {
        _entranceDisciplineDataSourceMap = entranceDisciplineDataSourceMap;
    }

    public Map<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> getChosenEntranceDisciplineMap()
    {
        return _chosenEntranceDisciplineMap;
    }

    public void setChosenEntranceDisciplineMap(Map<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> chosenEntranceDisciplineMap)
    {
        _chosenEntranceDisciplineMap = chosenEntranceDisciplineMap;
    }

    public Set<RequestedEnrollmentDirection> getNonActualDisciplinesDirectionSet()
    {
        return _nonActualDisciplinesDirectionSet;
    }

    public void setNonActualDisciplinesDirectionSet(Set<RequestedEnrollmentDirection> nonActualDisciplinesDirectionSet)
    {
        _nonActualDisciplinesDirectionSet = nonActualDisciplinesDirectionSet;
    }

    public boolean isNotAllDisciplinesHasPassDate()
    {
        EntrantExamListWrapper currentExamListWrapper = getCurrentExamListWrapper();
        if(currentExamListWrapper == null) return false;

        DynamicListDataSource<ExamPassDiscipline> dataSource = currentExamListWrapper.getExamPassDisciplineDataSource();
        if (dataSource == null) return false;

        List<ExamPassDiscipline> passDisciplines = dataSource.getEntityList();

        return !CollectionUtils.isEmpty(passDisciplines) && passDisciplines.stream().anyMatch(dis -> dis.getPassDate() == null);
    }
}
