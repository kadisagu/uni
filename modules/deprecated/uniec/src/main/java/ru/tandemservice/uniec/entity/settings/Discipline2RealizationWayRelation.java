package ru.tandemservice.uniec.entity.settings;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.settings.gen.Discipline2RealizationWayRelationGen;

import java.util.Arrays;
import java.util.List;

public class Discipline2RealizationWayRelation extends Discipline2RealizationWayRelationGen
{
    @Override
    public String getTitle()
    {
        String wayShortTitle = StringUtils.trimToNull(getSubjectPassWay().getShortTitle());
        if (null == wayShortTitle || getSubjectPassWay().getCode().equals(UniecDefines.SUBJECT_PASS_WAY_COMMON)) { return getEducationSubject().getTitle(); }
        return (getEducationSubject().getTitle() + " (" + wayShortTitle + ")");
    }

    @Override
    public String getFullTitle() {
        return getTitle();
    }

    @Override
    public String getShortTitle()
    {
        String educationSubjectShortTitle = StringUtils.trimToEmpty(getEducationSubject().getShortTitle());
        String wayShortTitle = StringUtils.trimToNull(getSubjectPassWay().getShortTitle());
        if (null == wayShortTitle || getSubjectPassWay().getCode().equals(UniecDefines.SUBJECT_PASS_WAY_COMMON)) { return getEducationSubject().getShortTitle(); }
        return (educationSubjectShortTitle + " (" + wayShortTitle + ")");
    }

    @Override
    public List<Discipline2RealizationWayRelation> getDisciplines()
    {
        return Arrays.asList(this);
    }
}