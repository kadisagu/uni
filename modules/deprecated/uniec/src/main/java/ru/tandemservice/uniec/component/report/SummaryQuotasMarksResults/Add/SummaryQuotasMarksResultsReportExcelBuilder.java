/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import java.io.ByteArrayOutputStream;
import java.util.List;

import jxl.SheetSettings;
import jxl.Workbook;
import jxl.biff.DisplayFormat;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.Orientation;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;
import jxl.format.ScriptStyle;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
class SummaryQuotasMarksResultsReportExcelBuilder
{
    public static byte[] buildExcelContent(String paramsTitle,
                                           List<StateExamSubject> stateExamSubjectList,
                                           List<Discipline2RealizationWayRelation> disciplineList,
                                           List<String> enrollmentColumnTitleList,
                                           List<ISumLev> list) throws Exception
    {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // create fonts
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10Gray = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GRAY_25, ScriptStyle.NORMAL_SCRIPT);
        WritableFont arial10boldGray = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GRAY_25, ScriptStyle.NORMAL_SCRIPT);
        DisplayFormat doubleOneDidestDisplayFormat = new NumberFormat("0.0");

        // create cell formats
        WritableCellFormat paramFormat = new WritableCellFormat(arial10);

        WritableCellFormat headerFormat = new WritableCellFormat(arial10);
        headerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        WritableCellFormat verticalHeaderFormat = new WritableCellFormat(arial10);
        verticalHeaderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        verticalHeaderFormat.setAlignment(Alignment.CENTRE);
        verticalHeaderFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        verticalHeaderFormat.setWrap(true);
        verticalHeaderFormat.setOrientation(Orientation.PLUS_90);

        WritableCellFormat cellFormat = new WritableCellFormat(arial10, NumberFormats.INTEGER);
        cellFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        cellFormat.setAlignment(Alignment.CENTRE);
        cellFormat.setVerticalAlignment(VerticalAlignment.TOP);
        cellFormat.setWrap(true);

        WritableCellFormat doubleCellFormat = new WritableCellFormat(arial10, doubleOneDidestDisplayFormat);
        doubleCellFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        doubleCellFormat.setAlignment(Alignment.CENTRE);
        doubleCellFormat.setVerticalAlignment(VerticalAlignment.TOP);
        doubleCellFormat.setWrap(true);

        WritableCellFormat leftCellFormat = new WritableCellFormat(arial10);
        leftCellFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        leftCellFormat.setVerticalAlignment(VerticalAlignment.TOP);
        leftCellFormat.setWrap(true);

        WritableCellFormat cellGrayFormat = new WritableCellFormat(arial10Gray, NumberFormats.INTEGER);
        cellGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        cellGrayFormat.setAlignment(Alignment.CENTRE);
        cellGrayFormat.setVerticalAlignment(VerticalAlignment.TOP);
        cellGrayFormat.setWrap(true);

        WritableCellFormat doubleCellGrayFormat = new WritableCellFormat(arial10Gray, doubleOneDidestDisplayFormat);
        doubleCellGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        doubleCellGrayFormat.setAlignment(Alignment.CENTRE);
        doubleCellGrayFormat.setVerticalAlignment(VerticalAlignment.TOP);
        doubleCellGrayFormat.setWrap(true);

        WritableCellFormat leftCellGrayFormat = new WritableCellFormat(arial10Gray);
        leftCellGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        leftCellGrayFormat.setVerticalAlignment(VerticalAlignment.TOP);
        leftCellGrayFormat.setWrap(true);

        WritableCellFormat cellBoldGrayFormat = new WritableCellFormat(arial10boldGray, NumberFormats.INTEGER);
        cellBoldGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        cellBoldGrayFormat.setAlignment(Alignment.CENTRE);
        cellBoldGrayFormat.setVerticalAlignment(VerticalAlignment.TOP);
        cellBoldGrayFormat.setWrap(true);

        WritableCellFormat doubleCellBoldGrayFormat = new WritableCellFormat(arial10boldGray, doubleOneDidestDisplayFormat);
        doubleCellBoldGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        doubleCellBoldGrayFormat.setAlignment(Alignment.CENTRE);
        doubleCellBoldGrayFormat.setVerticalAlignment(VerticalAlignment.TOP);
        doubleCellBoldGrayFormat.setWrap(true);

        WritableCellFormat leftCellBoldGrayFormat = new WritableCellFormat(arial10boldGray);
        leftCellBoldGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        leftCellBoldGrayFormat.setVerticalAlignment(VerticalAlignment.TOP);
        leftCellBoldGrayFormat.setWrap(true);

        WritableCellFormat[] formatData = new WritableCellFormat[3];
        formatData[0] = cellBoldGrayFormat;
        formatData[1] = cellGrayFormat;
        formatData[2] = cellFormat;

        WritableCellFormat[] leftFormatData = new WritableCellFormat[3];
        leftFormatData[0] = leftCellBoldGrayFormat;
        leftFormatData[1] = leftCellGrayFormat;
        leftFormatData[2] = leftCellFormat;

        WritableCellFormat[] doubleFormatData = new WritableCellFormat[3];
        doubleFormatData[0] = doubleCellBoldGrayFormat;
        doubleFormatData[1] = doubleCellGrayFormat;
        doubleFormatData[2] = doubleCellFormat;

        // create list
        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        SheetSettings settings = sheet.getSettings();
        settings.setCopies(1);
        settings.setOrientation(PageOrientation.LANDSCAPE);
        settings.setScaleFactor(85);
        settings.setPaperSize(PaperSize.A3);
        settings.setDefaultColumnWidth(6);

//        settings.setTopMargin(0.39);
//        settings.setLeftMargin(0.39);
//        settings.setRightMargin(0.39);
//        settings.setBottomMargin(0.2);

        // рисуем параметры
        sheet.addCell(new Label(1, 0, paramsTitle, paramFormat));

        // рисуем таблицу со третьей строки
        final int startRow = 2;
        int r = startRow;

        // рисуем таблицу с первой колонки
        final int startCol = 0;
        int c = startCol;

        // выставляем высоты ячеек таблицы
        sheet.setRowView(r, 1500);
        sheet.setRowView(r + 1, 2200);

        // рисуем шапку таблицы
        sheet.addCell(new Label(c, r, "№ п/п", headerFormat));
        sheet.mergeCells(c, r, c, r + 1);
        c++;

        sheet.addCell(new Label(c, r, "Форма обучения", verticalHeaderFormat));
        sheet.mergeCells(c, r, c, r + 1);
        c++;

        sheet.addCell(new Label(c, r, "Направления подготовки по УГС", headerFormat));
        sheet.mergeCells(c, r, c, r + 1);
        c++;

        sheet.addCell(new Label(c, r, "Направление подготовки (специальность)", headerFormat));
        sheet.mergeCells(c, r, c, r + 1);
        c++;

        sheet.addCell(new Label(c, r, "Бюджетные места", headerFormat));
        sheet.addCell(new Label(c, r + 1, "Количество мест", verticalHeaderFormat));
        sheet.addCell(new Label(c + 1, r + 1, "Количество заявлений", verticalHeaderFormat));
        sheet.mergeCells(c, r, c + 1, r);
        c += 2;

        sheet.addCell(new Label(c, r, "Целевой прием", headerFormat));
        sheet.addCell(new Label(c, r + 1, "Количество мест", verticalHeaderFormat));
        sheet.addCell(new Label(c + 1, r + 1, "Количество заявлений", verticalHeaderFormat));
        sheet.mergeCells(c, r, c + 1, r);
        c += 2;

        sheet.addCell(new Label(c, r, "Договора с физ. и юр. лицами с полным возмещением затрат", headerFormat));
        sheet.addCell(new Label(c, r + 1, "Количество мест", verticalHeaderFormat));
        sheet.addCell(new Label(c + 1, r + 1, "Количество заявлений", verticalHeaderFormat));
        sheet.mergeCells(c, r, c + 1, r);
        c += 2;

        sheet.addCell(new Label(c, r, "Средние баллы по ЕГЭ\nпо направлениям подготовки\n(по специальностям)", headerFormat));
        if (stateExamSubjectList.isEmpty())
        {
            sheet.addCell(new Blank(c, r + 1, verticalHeaderFormat));
            c++;
        } else
        {
            sheet.mergeCells(c, r, c + stateExamSubjectList.size() - 1, r);
            for (int i = 0; i < stateExamSubjectList.size(); i++)
                sheet.addCell(new Label(c + i, r + 1, stateExamSubjectList.get(i).getTitle(), verticalHeaderFormat));
            c += stateExamSubjectList.size();
        }

        sheet.addCell(new Label(c, r, "Средние баллы по дополнительным испытаниям по направлениям подготовки (по специальностям)", headerFormat));
        for (int i = 0; i < 4; i++)
            sheet.addCell(new Blank(c + i, r + 1, verticalHeaderFormat));
        sheet.mergeCells(c, r, c + 3, r);
        c += 4;

        sheet.addCell(new Label(c, r, "Средние баллы по вступительным испытаниям, форму и перечень которых вузы определяют самостоятельно", headerFormat));
        if (disciplineList.isEmpty())
        {
            sheet.addCell(new Blank(c, r + 1, verticalHeaderFormat));
            c++;
        } else
        {
            sheet.mergeCells(c, r, c + disciplineList.size() - 1, r);
            for (int i = 0; i < disciplineList.size(); i++)
                sheet.addCell(new Label(c + i, r + 1, disciplineList.get(i).getTitle(), verticalHeaderFormat));
            c += disciplineList.size();
        }

        sheet.addCell(new Label(c, r, "Количество зачисленных абитуриентов", headerFormat));
        if (enrollmentColumnTitleList.isEmpty())
        {
            sheet.addCell(new Blank(c, r + 1, verticalHeaderFormat));
            c++;
        } else
        {
            sheet.mergeCells(c, r, c + enrollmentColumnTitleList.size() - 1, r);
            for (int i = 0; i < enrollmentColumnTitleList.size(); i++)
                sheet.addCell(new Label(c + i, r + 1, enrollmentColumnTitleList.get(i), verticalHeaderFormat));
            c += enrollmentColumnTitleList.size();
        }

        sheet.addCell(new Label(c, r, "Количество мест по допол-му приему", verticalHeaderFormat));
        sheet.mergeCells(c, r, c, r + 1);
        c++;

        sheet.addCell(new Label(c, r, "Конкурс (количество поданных заявлений на одно место)", headerFormat));
        sheet.addCell(new Label(c, r + 1, "бюджет", verticalHeaderFormat));
        sheet.addCell(new Label(c + 1, r + 1, "целевой прием", verticalHeaderFormat));
        sheet.addCell(new Label(c + 2, r + 1, "внебюджет", verticalHeaderFormat));
        sheet.mergeCells(c, r, c + 2, r);
        c += 3;

        // рисуем номера колонок таблицы

        sheet.addCell(new Blank(startCol, startRow + 2, headerFormat));
        for (int i = 1; i < c; i++)
            sheet.addCell(JExcelUtil.getNumberNullableCell(startCol + i, startRow + 2, i, cellFormat));
        for (int i = 0; i < c; i++)
            sheet.setColumnView(i, 6);
        sheet.setColumnView(2, 30);
        sheet.setColumnView(3, 30);

        int index = 1;
        r = startRow + 3;

        // рисуем строки
        for (ISumLev sumLev : list)
        {
            c = startCol;

            int level = sumLev.getLevel();
            WritableCellFormat format = formatData[level];
            WritableCellFormat leftFormat = leftFormatData[level];
            WritableCellFormat doubleFormat = doubleFormatData[level];

            // № п/п
            sheet.addCell(level == 2 ? new Label(c, r, Integer.toString(index++), cellFormat) : new Blank(c, r, cellFormat));
            c++;

            // Форма обучения
            sheet.addCell(new Blank(c++, r, format));

            // Направления подготовки по УГС
            sheet.addCell(new Label(c++, r, sumLev.getTitle(), leftFormat));

            // Направление подготовки (специальность)
            sheet.addCell(new Label(c++, r, sumLev.getSubTitle(), leftFormat));

            // Бюджетные места (Количество  мест)
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getBudgetPlan(), format));

            // Бюджетные места (Количество заявлений)
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getBudgetCount(), format));

            // Целевой прием (Количество  мест)
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getTargetPlan(), format));

            // Целевой прием (Количество заявлений)
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getTargetCount(), format));

            // Договора с физ. и юр. лицами с полным возмещением затрат (Количество  мест)
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getContractPlan(), format));

            // Договора с физ. и юр. лицами с полным возмещением затрат (Количество заявлений)
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getContractCount(), format));

            // средние оценки по предметам ЕГЭ
            if (stateExamSubjectList.isEmpty())
                sheet.addCell(new Blank(c++, r, doubleFormat));
            else
                for (StateExamSubject stateExamSubject : stateExamSubjectList)
                {
                    double avgMark = sumLev.getAvgStateExamMark(stateExamSubject);

                    sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, avgMark, doubleFormat));
                }

            // средние баллы по доп. испытаниям
            sheet.addCell(new Blank(c++, r, format));
            sheet.addCell(new Blank(c++, r, format));
            sheet.addCell(new Blank(c++, r, format));
            sheet.addCell(new Blank(c++, r, format));

            // средние баллы по дисциплинам
            if (disciplineList.isEmpty())
                sheet.addCell(new Blank(c++, r, doubleFormat));
            else
                for (Discipline2RealizationWayRelation discipline : disciplineList)
                {
                    double avgMark = sumLev.getAvgDisciplineMark(discipline);

                    sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, avgMark, doubleFormat));
                }

            // количество зачисленных
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getEnrollCount(0), format));
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getEnrollCount(1), format));
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getEnrollCount(2), format));

            // доп. прием
            sheet.addCell(new Blank(c++, r, doubleFormat));

            // конкурс
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getBudgetPlan() == 0 ? 0 : (double) sumLev.getBudgetCount() / (double) sumLev.getBudgetPlan(), doubleFormat));
            sheet.addCell(JExcelUtil.getNumberNullableCell(c++, r, sumLev.getTargetPlan() == 0 ? 0 : (double) sumLev.getTargetCount() / (double) sumLev.getTargetPlan(), doubleFormat));
            sheet.addCell(JExcelUtil.getNumberNullableCell(c, r, sumLev.getContractPlan() == 0 ? 0 : (double) sumLev.getContractCount() / (double) sumLev.getContractPlan(), doubleFormat));

            r++;
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }
}
