package ru.tandemservice.uniec.component.menu.StateExamImportList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Julia Oschepkova
 */
public interface IDAO extends IUniDao<Model>
{
    public byte[] getPrintImportedFileRequest(long id);

    public byte[] getPrintResultFileRequest(long id);
}
