/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
class EnrollmentResultByEGEReportBuilder
{
    private Session _session;
    private Model _model;

    EnrollmentResultByEGEReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private static final int LEVEL_TOP = 0;
    private static final int LEVEL_GROUP = 1;
    private static final int LEVEL_DIRECTION = 2;

    private byte[] buildReport()
    {
        MQBuilder builder = getPreStudentBuilder();

        List<PreliminaryEnrollmentStudent> preStudentSet = builder.getResultList(_session);

        builder.getSelectAliasList().clear();
        builder.addSelect("p", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});

        EntrantDataUtil dataUtil = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS);

        Set<EnrollmentDirection> directionSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
            directionSet.add(direction.getEnrollmentDirection());

        final List<ISumLev> list = getDataRowList(dataUtil, preStudentSet, directionSet);
        String[][] data = new String[list.size()][9];
        DoubleFormatter formatter = new DoubleFormatter(1, true);
        for (int i = 0; i < list.size(); i++)
        {
            ISumLev sumLev = list.get(i);

            double commonAvg = sumLev.getCommonAvg();
            double benefitAvg = sumLev.getBenefitAvg();
            double olympiadAvg = sumLev.getOlympiadAvg();
            double targetAvg = sumLev.getTargetAvg();
            int count = sumLev.getCount();
            int benefitCount = sumLev.getBenefitCount();
            int olympiadCount = sumLev.getOlympiadCount();
            int targetCount = sumLev.getTargetCount();
            
            String[] row = data[i];
            row[0] = sumLev.getTitle();
            if (commonAvg > 0.0)
                row[1] = formatter.format(commonAvg);
            if (benefitAvg > 0.0)
                row[2] = formatter.format(benefitAvg);
            if (olympiadAvg > 0.0)
                row[3] = formatter.format(olympiadAvg);
            if (targetAvg > 0.0)
                row[4] = formatter.format(targetAvg);
            if (count > 0)
                row[5] = Integer.toString(count);
            if (benefitCount > 0)
                row[6] = Integer.toString(benefitCount);
            if (olympiadCount > 0)
                row[7] = Integer.toString(olympiadCount);
            if (targetCount > 0)
                row[8] = Integer.toString(targetCount);
        }

        // создаем rtf-файл

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_BY_EGE);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        final int grayColor = document.getHeader().getColorTable().addColor(128, 128, 128);

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("year", Integer.toString(_model.getEnrollmentCampaign().getEducationYear().getIntValue()));
        im.put("params", getParamsTitle());
        im.modify(document);

        RtfTableModifier tm = new RtfTableModifier();
        tm.put("T", data);
        tm.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                ISumLev sumLev = list.get(rowIndex);

                switch (sumLev.getLevel())
                {
                    case LEVEL_TOP:
                        return new RtfString().color(grayColor).boldBegin().append(value).boldEnd().toList();
                    case LEVEL_GROUP:
                        return new RtfString().color(grayColor).append(value).toList();
                    case LEVEL_DIRECTION:
                        return null; // просто подставить
                    default:
                        throw new RuntimeException("Unknown level: " + sumLev.getLevel());
                }
            }
        });
        tm.modify(document);

        return RtfUtil.toByteArray(document);
    }

    @SuppressWarnings("unchecked")
    private String getParamsTitle()
    {
        List<String> developFormTitleList = new ArrayList<>();
        for (DevelopForm item : _model.isDevelopFormActive() ? _model.getDevelopFormList() : (List<DevelopForm>) _model.getDevelopFormListModel().findValues("").getObjects())
            developFormTitleList.add(item.getTitle().toLowerCase());

        List<String> compensationTypeTitleList = new ArrayList<>();
        for (CompensationType item : _model.isCompensationTypeActive() ? Collections.singletonList(_model.getCompensationType()) : (List<CompensationType>) _model.getCompensationTypeListModel().findValues("").getObjects())
            compensationTypeTitleList.add(item.getShortTitle().toLowerCase());

        return "Форма обучения: " + StringUtils.join(developFormTitleList, ", ") + "; Вид затрат: " + StringUtils.join(compensationTypeTitleList, ", ");
    }

    private MQBuilder getPreStudentBuilder()
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.addJoin("r", RequestedEnrollmentDirection.entrantRequest().s(), "request");
        builder.addJoin("request", EntrantRequest.entrant().s(), "entrant");
        builder.addJoin("entrant", Entrant.person().s(), "person");
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if (_model.isCompensationTypeActive())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getCompensationType()));
        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.parallel().s(), Boolean.FALSE));

        builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));
        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));
        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        return builder;
    }

    private List<ISumLev> getDataRowList(EntrantDataUtil dataUtil, List<PreliminaryEnrollmentStudent> preStudentSet, Set<EnrollmentDirection> directionSet)
    {
        // направление приема -> [укрупненная группа, группировочная группа]
        Map<EnrollmentDirection, EnrollmentDirectionDescription> directionDataMap = new HashMap<>();

        Map<EducationLevels, SumLev> mapLev1 = new HashMap<>();
        Map<EducationLevels, SumLev> mapLev2 = new HashMap<>();
        Map<EnrollmentDirection, SumLev> mapLev3 = new HashMap<>();

        // подготавливаем данные
        for (EnrollmentDirection enrollmentDirection : directionSet)
        {
            // направление для группировки
            EducationLevels educationLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            while (!educationLevel.getLevelType().isGroupingLevel())
                educationLevel = educationLevel.getParentLevel();

            // укрупненная группа
            EducationLevels educationLevelGroup = educationLevel;
            while (educationLevelGroup.getParentLevel() != null)
                educationLevelGroup = educationLevelGroup.getParentLevel();

            EnrollmentDirectionDescription data = new EnrollmentDirectionDescription();
            data.educationLevel = educationLevel;
            data.educationLevelGroup = educationLevelGroup;
            directionDataMap.put(enrollmentDirection, data);

            if (!mapLev1.containsKey(educationLevelGroup))
                mapLev1.put(educationLevelGroup, new SumLev(dataUtil, educationLevelGroup.getDisplayableTitle(), educationLevelGroup.getId(), LEVEL_TOP));

            if (!mapLev2.containsKey(educationLevel))
                mapLev2.put(educationLevel, new SumLev(dataUtil, educationLevel.getDisplayableTitle(), educationLevel.getId(), LEVEL_GROUP));

            if (!mapLev3.containsKey(enrollmentDirection))
                mapLev3.put(enrollmentDirection, new SumLev(dataUtil, enrollmentDirection.getPrintTitle(), enrollmentDirection.getId(), LEVEL_DIRECTION));
        }

        // наполняем данные
        for (PreliminaryEnrollmentStudent preStudent : preStudentSet)
            mapLev3.get(preStudent.getRequestedEnrollmentDirection().getEnrollmentDirection()).add(preStudent);

        // группируем данные
        Map<SumLev, Map<SumLev, Set<SumLev>>> resultMap = new TreeMap<>();

        for (Map.Entry<EnrollmentDirection, SumLev> entry : mapLev3.entrySet())
        {
            // направление приема
            EnrollmentDirection enrollmentDirection = entry.getKey();
            EnrollmentDirectionDescription data = directionDataMap.get(enrollmentDirection);

            SumLev sumLev1 = mapLev1.get(data.educationLevelGroup);
            SumLev sumLev2 = mapLev2.get(data.educationLevel);
            SumLev sumLev3 = entry.getValue();

            Map<SumLev, Set<SumLev>> map = resultMap.get(sumLev1);
            if (map == null)
                if (null != resultMap.put(sumLev1, map = new TreeMap<>()))
                    throw new RuntimeException("Fatal override");

            Set<SumLev> list = map.get(sumLev2);
            if (list == null)
                if (null != map.put(sumLev2, list = new TreeSet<>()))
                    throw new RuntimeException("Fatal override");

            if (!list.add(sumLev3))
                throw new RuntimeException("Fatal override");
        }

        // создаем список строк
        List<ISumLev> list = new ArrayList<>();

        for (Map.Entry<SumLev, Map<SumLev, Set<SumLev>>> entry : resultMap.entrySet())
        {
            SumLev sumLev1 = entry.getKey();
            list.add(sumLev1);

            for (Map.Entry<SumLev, Set<SumLev>> subEntry : entry.getValue().entrySet())
            {
                SumLev sumLev2 = subEntry.getKey();
                list.add(sumLev2);

                for (SumLev sumLev3 : subEntry.getValue())
                {
                    list.add(sumLev3);

                    sumLev2.append(sumLev3);
                }

                sumLev1.append(sumLev2);
            }
        }

        return list;
    }

    private static class EnrollmentDirectionDescription
    {
        private EducationLevels educationLevel;
        private EducationLevels educationLevelGroup;
    }
}
