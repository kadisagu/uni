/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.CompetitionGroupEntrantsByEDReport.CompetitionGroupEntrantsByEDReportAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.CompetitionGroupEntrantsByEDReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author agolubenko
 * @since Jun 29, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setCompetitionGroupList(getCompetitionGroups(model.getEnrollmentCampaign()));
        model.getReport().setDateFrom((model.getEnrollmentCampaign() != null) ? model.getEnrollmentCampaign().getStartDate() : CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())));
        model.getReport().setDateTo(new Date());
    }

    @Override
    public List<CompetitionGroup> getCompetitionGroups(EnrollmentCampaign enrollmentCampaign)
    {
        List<CompetitionGroup> competitionGroupList = getList(CompetitionGroup.class, CompetitionGroup.enrollmentCampaign().s(), enrollmentCampaign);
        Collections.sort(competitionGroupList, CompetitionGroup.TITLED_COMPARATOR);
        return competitionGroupList;
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        CompetitionGroupEntrantsByEDReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setCompetitionGroupTitle(model.getCompetitionGroup().getTitle());

        DatabaseFile databaseFile = getReportContent(model, session);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    public DatabaseFile getReportContent(Model model, Session session) {
        return new CompetitionGroupEntrantsByEDReportBuilder(model, session).getContent();
    }
}
