/* $Id: StateExamEnrollmentReportBuilder.java 12652 2010-05-05 12:00:19Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.util.*;

/**
 * логика построения отчета
 *
 * @author vip_delete
 * @since 23.04.2009
 */
class StateExamEnrollmentReportBuilder
{
    private Model _model;
    private Session _session;
    private ReportRowKeyFactory _keyFactory;

    StateExamEnrollmentReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
        _keyFactory = createKeyFactory();
    }

    DatabaseFile getContent(IStateExamEnrollmentExcelBuilder excelBuilder)
    {
        byte[] data = buildReport(excelBuilder);
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport(IStateExamEnrollmentExcelBuilder excelBuilder)
    {
        final MQBuilder ouBuilder = getEducationOrgUnitBuilder();

        // статистика оценок
        final Map<MultiKey, Map<Entrant, Integer>> multiKey2markSet = getSubItemMarkSet(ouBuilder);

        // список направлений приема удовлетв. фильтрам. (по ним будут строится строки отчета)
        final Set<EducationOrgUnit> ouSet = new HashSet<>(ouBuilder.<EducationOrgUnit>getResultList(_session));

        // ур. обр. ОУ -> набор под строк
        final Map<EducationLevelsHighSchool, List<IdentifiableWrapper>> highSchool2subjectList = getSubItemHighSchoolMap(ouSet);

        // получаем список строк отчета с подстроками (в которых тоже есть подстроки). Первую строку выделяем отдельно
        List<ReportRow> reportRowList = getReportRowList(ouSet, highSchool2subjectList, multiKey2markSet);

        // текстовые параметры отчета
        String highSchoolTitle = TopOrgUnit.getInstance().getTitle();
        String enrollmentcampStage = _model.getEnrollmentCampaignStage().getTitle();
        String periodTitle = "c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getDateTo());

        // получаем бинарное представление excel-файла
        try
        {
            return excelBuilder.buildExcelContent(
                    highSchoolTitle,
                    enrollmentcampStage,
                    periodTitle,
                    _model,
                    reportRowList);
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private List<ReportRow> getReportRowList(Set<EducationOrgUnit> ouSet, Map<EducationLevelsHighSchool, List<IdentifiableWrapper>> highSchool2discipline, Map<MultiKey, Map<Entrant, Integer>> multiKey2markSet)
    {
        Comparator<EducationLevels> educationLevelsComparator = new Comparator<EducationLevels>()
        {
            @Override
            public int compare(EducationLevels o1, EducationLevels o2)
            {
                return o1.getDisplayableTitle().compareTo(o2.getDisplayableTitle());
            }
        };

        Map<OrgUnit, Map<EducationLevels, Set<ReportSubRow>>> reportRowMap = new HashMap<>();

        for (Map.Entry<OrgUnit, Set<EducationLevelsHighSchool>> entry : getRowId2HighSchoolMap(ouSet).entrySet())
        {
            // создаем подстроки
            Map<EducationLevels, Set<ReportSubRow>> subRowMap = new TreeMap<>(educationLevelsComparator);

            for (EducationLevelsHighSchool highSchool : entry.getValue())
            {
                List<IdentifiableWrapper> subItemList = highSchool2discipline.get(highSchool);
                if (subItemList != null)
                {
                    EducationLevels parentLevel = highSchool.getEducationLevel();
                    while (parentLevel.getParentLevel() != null) parentLevel = parentLevel.getParentLevel();

                    Set<ReportSubRow> subRowSet = subRowMap.get(parentLevel);
                    if (subRowSet == null)
                        subRowMap.put(parentLevel, subRowSet = new TreeSet<>(ITitled.TITLED_COMPARATOR));

                    // создаем под-под-строки
                    List<ReportSubSubRow> subSubRowList = new ArrayList<>();
                    for (IdentifiableWrapper subItem : subItemList)
                        subSubRowList.add(new ReportSubSubRow(subItem.getId(), subItem.getTitle(), multiKey2markSet.get(new MultiKey(entry.getKey(), highSchool, subItem.getId()))));
                    subRowSet.add(new ReportSubRow(highSchool.getPrintTitle(), highSchool.getEducationLevel().getTitleCodePrefix(), subSubRowList));
                }
            }

            if (!subRowMap.isEmpty())
                reportRowMap.put(entry.getKey(), subRowMap);
        }

        List<ReportRow> reportRowList = new ArrayList<>();

        if (_model.isGroupByBranchAndRepresentation())
        {
            AddressDetailed address = TopOrgUnit.getInstance().getAddress();
            AddressItem city = address == null ? null : address.getSettlement();
            String highSchoolCityTitle = city == null ? "город ОУ" : city.getTitleWithType();

            ReportRow firstRow = null;

            for (Map.Entry<OrgUnit, Map<EducationLevels, Set<ReportSubRow>>> entry : reportRowMap.entrySet())
            {
                OrgUnit orgUnit = entry.getKey();
                Map<EducationLevels, Set<ReportSubRow>> subRowMap = entry.getValue();

                if (orgUnit == null)
                {
                    firstRow = new ReportRow(highSchoolCityTitle, subRowMap);
                } else
                {
                    String title = orgUnit.getNominativeCaseTitle();
                    if (title == null)
                        title = orgUnit.getTitle();
                    reportRowList.add(new ReportRow(title, subRowMap));
                }
            }

            Collections.sort(reportRowList, ITitled.TITLED_COMPARATOR);

            if (firstRow != null)
                reportRowList.add(0, firstRow);
        } else
        {
            reportRowList.add(new ReportRow(null, reportRowMap.get(null)));
        }

        return reportRowList;
    }

    private Map<MultiKey, Map<Entrant, Integer>> getSubItemMarkSet(MQBuilder ouBuilder)
    {
        MQBuilder requestedBuilder = getRequestedEnrollmentDirectionBuilder(ouBuilder);
        EntrantDataUtil dataUtil = new EntrantDataUtil(_session, _model.getReport().getEnrollmentCampaign(), requestedBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        Map<Integer, SubjectPassForm> markSource2passForm = getMarkSource2passForm();
        Map<Discipline2RealizationWayRelation, StateExamSubject> discipline2stateExamSubject = getDiscipline2stateExamSubject();

        if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT == _model.getEnrollmentCampaignStage().getId())
            requestedBuilder.addSelect("p", new String[]{PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT});
        else
            requestedBuilder.addSelect("ed", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT});

        final boolean algorithmExam = _model.getReportAlgorithm().getId() == Model.REPORT_ALGORITHM_EXAM;
        final boolean algorithmStateExam = _model.getReportAlgorithm().getId() == Model.REPORT_ALGORITHM_STATE_EXAM;
        final boolean allowWave1 = !_model.isStateExamPeriodActive() || _model.getStateExamPeriod().getId() == Model.STATE_EXAM_PERIOD_WAVE_1;
        final boolean allowWave2 = !_model.isStateExamPeriodActive() || _model.getStateExamPeriod().getId() == Model.STATE_EXAM_PERIOD_WAVE_2;

        Map<MultiKey, Map<Entrant, Integer>> result = new HashMap<>();
        requestedBuilder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        requestedBuilder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        for (Object[] dataRow : requestedBuilder.<Object[]>getResultList(_session))
        {
            RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) dataRow[0];
            EducationOrgUnit orgUnit = (EducationOrgUnit) dataRow[1];
            Entrant entrant = direction.getEntrantRequest().getEntrant();

            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
            {
                Double finalMark = chosen.getFinalMark();

                // нет финальной оценки - не учитываем в статистике
                if (finalMark == null) continue;

                Integer markSource = chosen.getFinalMarkSource();

                // нет источника балла - не учитываем в статистике
                if (markSource == null) continue;

                SubjectPassForm subjectPassForm = markSource2passForm.get(markSource);

                // по источнику балла не найдена форма сдачи - не учитываем в статистике
                if (subjectPassForm == null) continue;

                String code = subjectPassForm.getCode();

                Discipline2RealizationWayRelation discipline = chosen.getEnrollmentCampaignDiscipline();

                MarkStatistic statistic = dataUtil.getMarkStatistic(chosen);

                if (algorithmExam)
                {
                    // для стадии по результатам экзаменов источником финальной оценки могут быть: экзамен, тестирование, интервью
                    if (UniecDefines.SUBJECT_PASS_FORM_EXAM.equals(code) || UniecDefines.SUBJECT_PASS_FORM_TEST.equals(code) || UniecDefines.SUBJECT_PASS_FORM_INTERVIEW.equals(code))
                    {
                        // берем оценку по форме сдачи
                        PairKey<ExamPassMark, ExamPassMarkAppeal> pair = statistic.getInternalMarkMap().get(subjectPassForm);

                        // нет оценки по форме сдачи, по которой стоит источник балла (такого не может быть, однако игнорируем такие оценки)
                        if (pair == null) continue;

                        // берем либо апелляцию, если есть, либо просто оценку, если нет апелляции
                        double doubleMark = pair.getSecond() != null ? pair.getSecond().getMark() : pair.getFirst().getMark();

                        // по идее, все оценки должны быть целыми
                        int mark = (int) Math.round(doubleMark);

                        // добавляем оценку в статистику
                        addMark(result, orgUnit, entrant, discipline.getId(), mark);
                    }
                } else if (algorithmStateExam)
                {
                    if (code.equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM))
                    {
                        // берем оценку по егэ
                        StateExamSubjectMark mark = statistic.getStateExamMark();

                        // нет оценки по егэ, игнорируем такое выбранное вступительное испытание
                        if (mark == null) continue;

                        String certificatTypeCode = mark.getCertificate().getStateExamType().getCode();

                        // сертификат первой волны, но первая волна не разрешена параметрами отчета
                        if (UniecDefines.STATE_EXAM_TYPE_FIRST.equals(certificatTypeCode) && !allowWave1) continue;

                        // сертификат второй волны, но вторая волна не разрешена параметрами отчета
                        if (UniecDefines.STATE_EXAM_TYPE_SECOND.equals(certificatTypeCode) && !allowWave2) continue;

                        // добавляем оценку в статистику
                        addMark(result, orgUnit, entrant, mark.getSubject().getId(), mark.getMark());
                    } else if (code.equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                    {
                        // вторая волна не разрешена параметрами отчета, а форму сдачи ЕГЭ (вуз) относим ко второй волне
                        if (!allowWave2) continue;

                        // определяем предмет ЕГЭ, за который зачитывает эта дисциплина
                        StateExamSubject stateExamSubject = discipline2stateExamSubject.get(discipline);

                        // если нет такого предмета ЕГЭ, то не учитываем это выбранное вступительное испытание
                        if (stateExamSubject == null) continue;

                        // берем оценку по форме сдачи
                        PairKey<ExamPassMark, ExamPassMarkAppeal> pair = statistic.getInternalMarkMap().get(subjectPassForm);

                        // нет оценки по форме сдачи, по которой стоит источник балла (это бага и, наверно, надо писть ее в консоль)
                        if (pair == null) continue;

                        // берем либо апелляцию, если есть, либо просто оценку, если нет апелляции
                        double doubleMark = pair.getSecond() != null ? pair.getSecond().getMark() : pair.getFirst().getMark();

                        // по идее, все оценки должны быть целыми
                        int mark = (int) Math.round(doubleMark);

                        // добавляем оценку в статистику
                        addMark(result, orgUnit, entrant, stateExamSubject.getId(), mark);
                    }
                } else
                    throw new RuntimeException("Unknown report algorithm!");
            }
        }
        return result;
    }

    //MultiKey : orgUnitId + highSchoolId + id_под_строки

    private void addMark(Map<MultiKey, Map<Entrant, Integer>> result, EducationOrgUnit eduOrgUnit, Entrant entrant, Long id, int markValue)
    {
        MultiKey key = new MultiKey(_keyFactory.createRowId(eduOrgUnit.getFormativeOrgUnit()), eduOrgUnit.getEducationLevelHighSchool(), id);

        Map<Entrant, Integer> set = result.get(key);
        if (set == null)
            result.put(key, set = new HashMap<>());

        Integer existingMarkValue = set.get(entrant);
        if (existingMarkValue == null || markValue > existingMarkValue)
            set.put(entrant, markValue);
    }

    // ур. обр. ОУ -> набор подстрок

    private Map<EducationLevelsHighSchool, List<IdentifiableWrapper>> getSubItemHighSchoolMap(Set<EducationOrgUnit> outSet)
    {
        final Map<PairKey<Long, String>, Set<IdentifiableWrapper>> discipline2SubItems = getDisciplineSubItemMap();

        EnrollmentCampaign enrollmentCampaign = _model.getReport().getEnrollmentCampaign();
        boolean examSetDiff = enrollmentCampaign.isExamSetDiff();
        String compensationTypeCode = enrollmentCampaign.isEnrollmentPerCompTypeDiff() ?
                _model.getReport().getCompensationType().getCode() :
                    UniDefines.COMPENSATION_TYPE_BUDGET;

                Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(_session, _model.getReport().getEnrollmentCampaign());

                Map<EducationLevelsHighSchool, Set<IdentifiableWrapper>> highSchool2scaleSet = new HashMap<>();

                Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();
                for (ExamSet examSet : examSetList)
                {
                    // если есть разница в наборах и на форме выбраны категории и категория текущиго набора не та, то игнорируем такой набор
                    if (examSetDiff && _model.isStudentCategoryActive() && !_model.getStudentCategoryList().contains(examSet.getStudentCategory()))
                        continue;

                    Set<IdentifiableWrapper> itemSet = new HashSet<>();
                    for (ExamSetItem examSetItem : examSet.getSetItemList())
                    {
                        // если выбрали бюджет на форме, а вступительное испытание не поддерживаем бюджет, то игнорируем его
                        if (_model.getReport().getCompensationType().isBudget() && !examSetItem.isBudget())
                            continue;

                        // если выбрали контракт на форме, а вступительное испытание не поддерживаем контракт, то игнорируем его
                        if (!_model.getReport().getCompensationType().isBudget() && !examSetItem.isContract())
                            continue;

                        for (Discipline2RealizationWayRelation discipline : ExamSetUtil.getDisciplines(examSetItem, group2directions))
                        {
                            PairKey<Long, String> pairKey = PairKey.create(discipline.getId(), compensationTypeCode);
                            Set<IdentifiableWrapper> set = discipline2SubItems.get(pairKey);
                            if (set != null)
                                itemSet.addAll(set);
                        }
                    }

                    for (EnrollmentDirection direction : examSet.getList())
                    {
                        if (outSet.contains(direction.getEducationOrgUnit()))
                        {
                            EducationLevelsHighSchool key = direction.getEducationOrgUnit().getEducationLevelHighSchool();
                            Set<IdentifiableWrapper> set = highSchool2scaleSet.get(key);
                            if (set == null)
                                highSchool2scaleSet.put(key, set = new HashSet<>());
                            set.addAll(itemSet);
                        }
                    }
                }

                Map<EducationLevelsHighSchool, List<IdentifiableWrapper>> result = new HashMap<>();
                for (Map.Entry<EducationLevelsHighSchool, Set<IdentifiableWrapper>> entry : highSchool2scaleSet.entrySet())
                {
                    List<IdentifiableWrapper> itemList = new ArrayList<>(entry.getValue());
                    if (!itemList.isEmpty())
                    {
                        Collections.sort(itemList, ITitled.TITLED_COMPARATOR);
                        result.put(entry.getKey(), itemList);
                    }
                }

                return result;
    }

    // id большой строки -> мн-во ур. обр ОУ в нее входящих

    private Map<OrgUnit, Set<EducationLevelsHighSchool>> getRowId2HighSchoolMap(Set<EducationOrgUnit> ouSet)
    {
        Map<OrgUnit, Set<EducationLevelsHighSchool>> result = new HashMap<>();
        for (EducationOrgUnit educationOrgUnit : ouSet)
        {
            OrgUnit key = _keyFactory.createRowId(educationOrgUnit.getFormativeOrgUnit());
            Set<EducationLevelsHighSchool> set = result.get(key);
            if (set == null)
                result.put(key, set = new HashSet<>());
            set.add(educationOrgUnit.getEducationLevelHighSchool());
        }
        return result;
    }

    // id-дисциплины приемной кампании+код.вида затрат -> мн-во возможных строк соотв. этой дисциплине по виду затрат

    private Map<PairKey<Long, String>, Set<IdentifiableWrapper>> getDisciplineSubItemMap()
    {
        Map<PairKey<Long, String>, Set<IdentifiableWrapper>> result = new HashMap<>();

        // анализируем формы сдачи дисциплин выбранной приемной кампании
        // если есть разница в приема на бюджет/контракт, то берем только формы сдачи с указанным на форме видом затрат
        // иначе берем формы сдачи созданные только на бюджет
        List<Discipline2RealizationFormRelation> relationList = new MQBuilder(Discipline2RealizationFormRelation.ENTITY_CLASS, "r")
        .add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()))
        .add(_model.getReport().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff() ?
                MQExpression.eq("r", Discipline2RealizationFormRelation.L_TYPE, _model.getReport().getCompensationType()) :
                    MQExpression.eq("r", Discipline2RealizationFormRelation.L_TYPE + "." + CompensationType.P_CODE, UniDefines.COMPENSATION_TYPE_BUDGET)
        ).getResultList(_session);

        Set<String> examFormSet = new HashSet<>(Arrays.asList(
                UniecDefines.SUBJECT_PASS_FORM_EXAM,
                UniecDefines.SUBJECT_PASS_FORM_TEST,
                UniecDefines.SUBJECT_PASS_FORM_INTERVIEW
        ));

        Set<String> stateExamFormSet = new HashSet<>(Arrays.asList(
                UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM,
                UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL
        ));

        // дисциплина приемной кампании -> мн-во шкал перевода к предметам егэ
        Map<Discipline2RealizationWayRelation, Set<StateExamSubject>> discipline2scaleSet = new HashMap<>();
        MQBuilder builder = new MQBuilder(ConversionScale.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", ConversionScale.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.isNotNull("s", ConversionScale.L_SUBJECT));
        for (ConversionScale conversionScale : builder.<ConversionScale>getResultList(_session))
        {
            Set<StateExamSubject> set = discipline2scaleSet.get(conversionScale.getDiscipline());
            if (set == null)
                discipline2scaleSet.put(conversionScale.getDiscipline(), set = new HashSet<>());
            set.add(conversionScale.getSubject());
        }

        for (Discipline2RealizationFormRelation formRelation : relationList)
        {
            String formCode = formRelation.getSubjectPassForm().getCode();
            Discipline2RealizationWayRelation discipline = formRelation.getDiscipline();
            CompensationType compensationType = formRelation.getType();
            PairKey<Long, String> pairKey = PairKey.create(discipline.getId(), compensationType.getCode());

            if (_model.getReportAlgorithm().getId() == Model.REPORT_ALGORITHM_EXAM)
            {
                if (examFormSet.contains(formCode))
                {
                    // для этого алгоритма работы отчета в строках - дисциплины приемной кампании, поэтому
                    // тут максимум одна строка бывает
                    result.put(pairKey, Collections.<IdentifiableWrapper>singleton(new IdentifiableWrapper<>(discipline)));
                }
            } else if (_model.getReportAlgorithm().getId() == Model.REPORT_ALGORITHM_STATE_EXAM)
            {
                if (stateExamFormSet.contains(formCode))
                {
                    Set<StateExamSubject> subjectSet = discipline2scaleSet.get(discipline);
                    if (subjectSet != null)
                    {
                        Set<IdentifiableWrapper> set = result.get(pairKey);
                        if (set == null)
                            result.put(pairKey, set = new HashSet<>());

                        for (StateExamSubject subject : subjectSet)
                            set.add(new IdentifiableWrapper<>(subject));
                    }
                }
            } else
                throw new RuntimeException("Unknown report alogorithm: " + _model.getReportAlgorithm().getId());
        }

        return result;
    }

    private MQBuilder getEducationOrgUnitBuilder()
    {
        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getReport().getDevelopForm()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                        MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList())
            );
        return builder;
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder(MQBuilder ouBuilder)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.addJoin("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        if (Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS == _model.getEnrollmentCampaignStage().getId())
        {
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            builder.add(MQExpression.in("ed", EnrollmentDirection.L_EDUCATION_ORG_UNIT, ouBuilder));
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        } else if (Model.ENROLLMENT_CAMP_STAGE_EXAMS == _model.getEnrollmentCampaignStage().getId())
        {
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            builder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE,
                    UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                    UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                    UniecDefines.ENTRANT_STATE_ACTIVE_CODE
            ));
            builder.add(MQExpression.in("ed", EnrollmentDirection.L_EDUCATION_ORG_UNIT, ouBuilder));
        } else if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT == _model.getEnrollmentCampaignStage().getId())
        {
            builder.addDomain("p", PreliminaryEnrollmentStudent.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("r", RequestedEnrollmentDirection.P_ID, "p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            if (_model.isParallelActive() && _model.getParallel().isTrue())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, ouBuilder));
        } else
            throw new RuntimeException("Unknown report stage: " + _model.getEnrollmentCampaignStage().getId());
        return builder;
    }

    @SuppressWarnings("unchecked")
    private Map<Integer, SubjectPassForm> getMarkSource2passForm()
    {
        List<SubjectPassForm> list = _session.createCriteria(SubjectPassForm.class).list();
        Map<String, SubjectPassForm> code2passForm = new HashMap<>();
        for (SubjectPassForm passForm : list)
            code2passForm.put(passForm.getCode(), passForm);
        Map<Integer, SubjectPassForm> map = new HashMap<>();
        map.put(UniecDefines.FINAL_MARK_DISCIPLINE_EXAM, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_EXAM));
        map.put(UniecDefines.FINAL_MARK_APPEAL_EXAM, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_EXAM));
        map.put(UniecDefines.FINAL_MARK_DISCIPLINE_TEST, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_TEST));
        map.put(UniecDefines.FINAL_MARK_APPEAL_TEST, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_TEST));
        map.put(UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_INTERVIEW));
        map.put(UniecDefines.FINAL_MARK_APPEAL_INTERVIEW, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_INTERVIEW));
        map.put(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL));
        map.put(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL));
        map.put(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM));
        map.put(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2, code2passForm.get(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM));
        return map;
    }

    @SuppressWarnings("unchecked")
    private Map<Discipline2RealizationWayRelation, StateExamSubject> getDiscipline2stateExamSubject()
    {
        List<ConversionScale> list = _session.createCriteria(ConversionScale.class).list();
        Map<Discipline2RealizationWayRelation, StateExamSubject> map = new HashMap<>();
        for (ConversionScale scale : list)
            map.put(scale.getDiscipline(), scale.getSubject());
        return map;
    }

    private ReportRowKeyFactory createKeyFactory()
    {
        if (_model.isGroupByBranchAndRepresentation()) //группировать результаты по территориальному признаку
            return new ReportRowKeyFactory()
        {
            @Override
            public OrgUnit createRowId(OrgUnit orgUnit)
            {
                String code = orgUnit.getOrgUnitType().getCode();
                if (!OrgUnitTypeCodes.BRANCH.equals(code) && !OrgUnitTypeCodes.REPRESENTATION.equals(code))
                    orgUnit = null;
                return orgUnit;
            }
        };
        else // без группировки
            return new ReportRowKeyFactory()
        {
            @Override
            public OrgUnit createRowId(OrgUnit orgUnit)
            {
                return null;
            }
        };
    }

    private static interface ReportRowKeyFactory
    {
        OrgUnit createRowId(OrgUnit orgUnit);
    }
}
