/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Pub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByEGEReport;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(EnrollmentResultByEGEReport.class, model.getReport().getId()));
    }
}
