/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

/**
 * @author agolubenko
 * @since 06.03.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final Long STATUS_ACCEPTED_ID = 0L;
    public static final Long STATUS_NOT_ACCEPTED_ID = 1L;

    public static final Long STATUS_SENT_ID = 0L;
    public static final Long STATUS_NOT_SENT_ID = 1L;

    private IDataSettings _settings;
    private DynamicListDataSource<EntrantStateExamCertificate> _dataSource;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<IdentifiableWrapper<?>> _acceptedList = new ArrayList<IdentifiableWrapper<?>>();
    {
        _acceptedList.add(new IdentifiableWrapper<IEntity>(STATUS_ACCEPTED_ID, "Да"));
        _acceptedList.add(new IdentifiableWrapper<IEntity>(STATUS_NOT_ACCEPTED_ID, "Нет"));
    }
    private List<IdentifiableWrapper<?>> _sentList = new ArrayList<IdentifiableWrapper<?>>();
    {
        _sentList.add(new IdentifiableWrapper<IEntity>(STATUS_SENT_ID, "Да"));
        _sentList.add(new IdentifiableWrapper<IEntity>(STATUS_NOT_SENT_ID, "Нет"));
    }

    private Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> _certificate2SubjectsMarks;
    private StateExamSubjectMark _currentStateExamSubjectMark;
    
    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<EntrantStateExamCertificate> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntrantStateExamCertificate> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<IdentifiableWrapper<?>> getAcceptedList()
    {
        return _acceptedList;
    }

    public void setAcceptedList(List<IdentifiableWrapper<?>> acceptedList)
    {
        _acceptedList = acceptedList;
    }

    public List<IdentifiableWrapper<?>> getSentList()
    {
        return _sentList;
    }

    public void setSentList(List<IdentifiableWrapper<?>> sentList)
    {
        _sentList = sentList;
    }

    public Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> getCertificate2SubjectsMarks()
    {
        return _certificate2SubjectsMarks;
    }

    public void setCertificate2SubjectsMarks(Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> certificate2SubjectsMarks)
    {
        _certificate2SubjectsMarks = certificate2SubjectsMarks;
    }

    public StateExamSubjectMark getCurrentStateExamSubjectMark()
    {
        return _currentStateExamSubjectMark;
    }

    public void setCurrentStateExamSubjectMark(StateExamSubjectMark currentStateExamSubjectMark)
    {
        _currentStateExamSubjectMark = currentStateExamSubjectMark;
    }
}