/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummarySheetExamReport;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.report.SummarySheetExamReport;

/**
 * @author Alexander Shaburov
 * @since 18.07.12
 */
public interface ISummarySheetExamReportDAO extends INeedPersistenceSupport
{
    /**
     * Подготавливает модель формы добавления отчета.<p/>
     * Заполняет модели селектов и значения по умолчанию.
     * @param model модель
     * @return подготовленную модель
     */
    <M extends SummarySheetExamReportModel> M prepareModel(M model);

    /**
     * Листенер на изменеие поля Приемная компания.
     * Сетит даты заявления.
     * @param model модель
     * @return модель
     */
    <M extends SummarySheetExamReportModel> M onChangeEnrollmentCampaign(M model);

    /**
     * Создает отчет и заполняет его поля.
     *
     * @param model      модель
     * @param reportFile файл отчета
     * @return Отчет "Сводная экзаменационная ведомость (ручной механизм ЭГ)"
     */
    <M extends SummarySheetExamReportModel> SummarySheetExamReport createReport(M model, DatabaseFile reportFile);

    /**
     * Создает печатную форму отчета "Сводная экзаменационная ведомость (ручной механизм ЭГ)".
     *
     * @param model модель
     * @return Файл отчета хранимый в базе данных
     */
    <M extends SummarySheetExamReportModel> DatabaseFile createPrintReportFile(M model);

    /**
     * Производит проверки перед печатью.
     * @param model модель
     * @return Коллектор пользовательских ошибок
     */
    <M extends SummarySheetExamReportModel> ErrorCollector validate(M model);
}
