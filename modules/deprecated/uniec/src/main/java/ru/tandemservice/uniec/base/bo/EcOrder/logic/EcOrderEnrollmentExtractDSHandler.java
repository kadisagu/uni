/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderEnrollmentExtractDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ENROLLMENT_PARAGRAPH = "enrollmentParagraphView";
    public static final String ENROLLMENT_ORDER_PARAM = "enrollmentOrderView";
    public static final String EDU_INSTITUTION = "eduInstitutionView";
    public static final String NO_EXCLUDE = "noExcludeView";

    public EcOrderEnrollmentExtractDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractEntrantExtract.class, "e");

        EnrollmentParagraph paragraph = context.get(ENROLLMENT_PARAGRAPH);
        EnrollmentOrder order = context.get(ENROLLMENT_ORDER_PARAM);
        if (paragraph != null)
        {
            builder.where(eq(property(AbstractEntrantExtract.paragraph().fromAlias("e")), value(paragraph)));
            order = (EnrollmentOrder) paragraph.getOrder();
        }
        else
        {
            builder.where(eq(property(AbstractEntrantExtract.paragraph().order().fromAlias("e")), value(order)));
        }
        builder.order(property(AbstractEntrantExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio().fromAlias("e")));

        List<AbstractEntrantExtract> extractList = builder.createStatement(context.getSession()).list();

        if (extractList.isEmpty())
            return new DSOutput(input);

        boolean noExclude = order.isReadonly() || extractList.size() == 1;
        List<DataWrapper> recordList = new ArrayList<>(extractList.size());
        for (AbstractEntrantExtract extract : extractList)
        {
            DataWrapper record = new DataWrapper(extract.getId(), extract.getTitle(), extract);

            String eduInstitutionTitle = "";
            PersonEduInstitution personEduInstitution = extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution();
            if (personEduInstitution != null)
            {
                EducationLevelStage educationLevelStage = personEduInstitution.getEducationLevel();
                if (educationLevelStage != null)
                    eduInstitutionTitle = educationLevelStage.getShortTitle();
            }

            record.setProperty(EDU_INSTITUTION, eduInstitutionTitle);
            record.setProperty(NO_EXCLUDE, noExclude);

            recordList.add(record);
        }

        return ListOutputBuilder.get(input, recordList).build();
    }
}
