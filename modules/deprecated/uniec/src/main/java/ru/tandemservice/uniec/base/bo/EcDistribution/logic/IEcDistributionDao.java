/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.logic;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgConfigDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgPriorityInfo;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRateRowDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRecommended;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgHasBringOriginal;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaFreeDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaUsedDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgRecommendedDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgRecommendedPreStudentDTO;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionTAQuota;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
public interface IEcDistributionDao extends INeedPersistenceSupport
{
    /**
     * Номер волны первого распределения
     */
    public static final int DISTRIBUTION_FIRST_WAVE = 1;

    /**
     * Получает список отсортированных конкурсных групп из указанной приемной кампании
     *
     * @param enrollmentCampaign приемная кампания
     * @return список конкурсных групп
     */
    List<CompetitionGroup> getCompetitionGroupList(EnrollmentCampaign enrollmentCampaign, List<Qualifications> qualificationList);

    /**
     * Получает максимальный номер волны среди всех распределений
     *
     * @param enrollmentCampaign  приемная кампания
     * @param compensationType    вид возмещения затрат
     * @param secondHighAdmission true, если распределения на второе высшее
     * @return максимальный номер волны среди всех распределений
     */
    Integer getDistributionMaxWave(EnrollmentCampaign enrollmentCampaign, CompensationType compensationType, boolean secondHighAdmission);

    /**
     * Получает название файла для скачивания
     * В имени файла зашиваются ключевые параметры распределения
     *
     * @param distribution распределение
     * @return имя файла без расширения
     */
    String getDistributionFileName(IEcgDistribution distribution);

    /**
     * Получает название файла для скачивания
     * В имени файла зашиваются ключевые параметры распределения
     *
     * @param distributionDTO распределение
     * @return имя файла без расширения
     */
    String getDistributionFileName(EcgDistributionDTO distributionDTO);

    /**
     * Получает отсортированный список направлений приема
     * <p/>
     * Если есть распределение первой волны, то все данные берутся из него, иначе берутся данные на текущий момент
     *
     * @param configDTO конфигурация распределения
     * @return список направлений приема
     */
    List<EnrollmentDirection> getDistributionDirectionList(EcgConfigDTO configDTO);

    /**
     * Получает список отсортированных используемых видов целевого приема
     *
     * @param configDTO конфигурация распределения
     * @return используемые виды целевого приема
     */
    List<TargetAdmissionKind> getDistributionTaKindList(EcgConfigDTO configDTO);

    /**
     * Получает отсортированный список направлений приема в указанном распределении
     *
     * @param distribution основное распределение
     * @return список направлений приема
     */
    List<EnrollmentDirection> getDistributionDirectionList(EcgDistribution distribution);

    /**
     * Получает список отсортированных используемых видов целевого приема в указанном распределении
     *
     * @param distribution основное распределение
     * @return используемые виды целевого приема
     */
    List<TargetAdmissionKind> getDistributionTaKindList(EcgDistribution distribution);

    /**
     * Получает список планов приема по видам цп по указанному распределению
     * Список всегда не пуст, так как всегда есть план приема по цп
     *
     * @param distributionId идентификатор основного распределения
     * @return список планов приема по видам цп
     */
    List<EcgDistributionTAQuota> getDistributionTAQuotaList(Long distributionId);

    /**
     * Получает список строк на странице распределений
     *
     * @param enrollmentCampaign           приемная кампания
     * @param compensationType             вид затрат
     * @param secondHighAdmission          true, если распределения на второе высшее
     * @param wave                         номер волны
     * @param state                        состояние распределения
     * @param competitionGroup             конкурсная группа (фильтр)
     * @param formativeOrgUnitList         формирующие подр. (фильтр)
     * @param territorialOrgUnitList       территориальные подр. (фильтр)
     * @param educationLevelHighSchoolList направления подготовки (специальности) вуза (фильтр)
     * @param developFormList              формы освоения (фильтр)
     * @param developConditionList         условия освоения (фильтр)
     * @param developTechList              технологии освоения (фильтр)
     * @param developPeriodList            сроки освоения (фильтр)
     * @return DTO распределений
     */
    List<EcgDistributionDTO> getDistributionDTOList(EnrollmentCampaign enrollmentCampaign,
        CompensationType compensationType,
        boolean secondHighAdmission,
        Integer wave,
        EcgDistributionState state,
        CompetitionGroup competitionGroup,
        List<Qualifications> qualificationList,
        List<OrgUnit> formativeOrgUnitList,
        List<OrgUnit> territorialOrgUnitList,
        List<EducationLevelsHighSchool> educationLevelHighSchoolList,
        List<DevelopForm> developFormList,
        List<DevelopCondition> developConditionList,
        List<DevelopTech> developTechList,
        List<DevelopPeriod> developPeriodList);

    /**
     * Сохраняет распределение абитуриентов
     * <p/>
     * Если распределение создается на конкурсную группу, то после создания распределения изменения в конкурсной группе
     * (добавление в нее новых направлений или удаление старых) никак не влияет на список направлений в распределении.
     * Фактически запрещено изменять конкурсную группу после того как на нее создано распределение.
     * <p/>
     * Для каждого направления из распределения (по конкурсным группам или по направлениям) сохраняются планы приема
     * по видам целевого приема. Берутся только используемые листовые виды целевого приема на момент создания распределения.
     * Изменение списка используемых видов целевого приема в настройке никак не влияет на планы по видам целевого приема
     * в распределении. Фактически запрещено изменять список используемых видов целевого приема после создания первого
     * же распределения.
     * <p/>
     * Если используемых видов целевого приема нет, то будет использоваться вид целевого приема по умолчанию (системный
     * элемент - он всегда существует), поэтому можно считать, что всегда есть как минимум один вид целевого приема и
     * план по нему.
     *
     * @param configDTO  конфигурация распределения
     * @param wave       номер волны, начиная с 1
     * @param quotaMap   планы приема по направлениям (направление->план)
     * @param taQuotaMap планы приема по направлениям и видам целевого приема (направление->вид цп->план)
     * @return сохраненное распределение
     */
    EcgDistribution saveDistribution(EcgConfigDTO configDTO, int wave, Map<Long, Integer> quotaMap, Map<Long, Map<Long, Integer>> taQuotaMap);

    /**
     * Сохраняет уточняющее распределение
     *
     * @param distributionId идентификатор распределения на которое следует создать уточняющее
     * @return сохраненное распределение
     */
    EcgDistributionDetail saveDistributionDetail(Long distributionId);

    /**
     * Обновляет планы приема распределения
     *
     * @param distributionId идентификатор распределения
     * @param quotaMap       планы приема по направлениям (направление->план)
     * @param taQuotaMap     планы приема по направлениям и видам целевого приема (направление->вид цп->план)
     */
    void updateDistribution(Long distributionId, Map<Long, Integer> quotaMap, Map<Long, Map<Long, Integer>> taQuotaMap);

    /**
     * Обновляет дату согласования для распределения
     *
     * @param distributionId идентификатор распределения
     * @param approvalDate   дата утверждения
     */
    void updateApprovalDate(Long distributionId, Date approvalDate);

    /**
     * Удаляет основное распределение
     *
     * @param distributionId идентификатор основного распределения
     */
    void deleteDistribution(Long distributionId);

    /**
     * Удаляет уточняющее распределение
     *
     * @param distributionDetailId идентификатор уточняющего распределения
     */
    void deleteDistributionDetail(Long distributionDetailId);

    /**
     * Получает распределение с максимальной волной для указанной конфигурации распределения
     *
     * @param configDTO конфигурация распределения
     * @return распределение с максимальной волной для указанной конфигурации распределения
     */
    EcgDistribution getDistributionWithMaxWave(EcgConfigDTO configDTO);

    /**
     * Получает распределение с максимальной волной для указанной конфигурации распределения
     *
     * @param config конфигурация распределения
     * @return распределение с максимальной волной для указанной конфигурации распределения
     */
    EcgDistribution getDistributionWithMaxWave(EcgDistributionConfig config);

    /**
     * Получает планы приема при добавлении нового распределения на указанную конфигурацию
     *
     * @param configDTO конфигурация распределения
     * @return планы приема
     */
    IEcgQuotaDTO getNextQuotaDTO(EcgConfigDTO configDTO);

    /**
     * Получает планы приема для указанного распределения
     *
     * @param distribution распределение
     * @return планы приема
     */
    IEcgQuotaDTO getCurrentQuotaDTO(EcgDistribution distribution);

    /**
     * Получает кол-во занятых мест и по направлениям и видам цп для указанного распределения
     *
     * @param distribution распределение
     * @return кол-во занятых мест
     */
    IEcgQuotaUsedDTO getUsedQuotaDTO(IEcgDistribution distribution);

    /**
     * Получает кол-во свободных мест и по направлениям и видам цп для указанного распределения
     *
     * @param distribution распределение
     * @return кол-во свободных мест
     */
    IEcgQuotaFreeDTO getFreeQuotaDTO(IEcgDistribution distribution);

    /**
     * Получает список отсортированных строк-рейтингов абитуриентов
     *
     * @param distribution   распределение
     * @param usedTaKindList используемые в распределении виды цп
     * @param rateRule       правило выборки строк для рейтинга
     * @return рейтинг абитуриентов в распределении
     */
    List<? extends IEcgEntrantRateRowDTO> getEntrantRateRowList(IEcgDistribution distribution, List<TargetAdmissionKind> usedTaKindList, IEcgEntrantRateRowDTO.Rule rateRule);

    /**
     * Добавляет рекомендованных абитуриентов в распределение
     *
     * @param distribution       распределение
     * @param targetAdmission    true, если следует рекомендовать по целевому приему
     * @param chosenDirectionIds выбранные направления приема, по которым нужно рекомендовать абитуриентов к зачислению
     */
    void saveEntrantRecommendedList(IEcgDistribution distribution, boolean targetAdmission, Collection<Long> chosenDirectionIds);

    /**
     * Получает список отсортированных рекомендованных к зачислению абитуриентов в распределении
     *
     * @param distribution распределение
     * @return рекомендованные к зачислению абитуриенты из распределения
     */
    List<IEcgRecommendedDTO> getEntrantRecommendedRowList(IEcgDistribution distribution);

    /**
     * Получает список отсортированных рекомендованных к зачислению абитуриентов в распределении с сопоставленным предзачислением
     *
     * @param distribution утвержденное распределение на направление, либо утвержденное уточняющее распределение на конкурсную группу
     * @return рекомендованные к зачислению абитуриенты из распределения с сопоставленным предзачислением
     */
    List<IEcgRecommendedPreStudentDTO> getEntrantRecommendedPreStudentRowList(IEcgDistribution distribution);

    /**
     * Аннулирует рекомендацию к зачислению
     *
     * @param entrantRecommended рекомендованный к зачислению
     */
    void deleteEntrantRecommended(IEcgEntrantRecommended entrantRecommended);

    /**
     * Заполняет основное распределение в автоматическом режиме
     *
     * @param distribution основное распределение, которое требуется заполнить
     */
    void doDistributionFill(EcgDistribution distribution);

    /**
     * Заполняет основное распределение в автоматическом режиме
     * поступающими на общих основаниях с оригиналами документов
     *
     * @param distribution основное распределение, которое требуется заполнить
     */
    void doDistributionFillWithOriginal(EcgDistribution distribution);

    /**
     * Фиксирует основное распределение (список резерва)
     *
     * @param distribution основное распределение, которое требуется зафиксировать
     */
    void doDistributionLock(EcgDistribution distribution);

    /**
     * Утверждает распределение (фиксируются статусы сдачи оригиналов документов)
     *
     * @param distribution распределение, которое требуется утвердить
     */
    void doDistributionApprove(IEcgDistribution distribution);

    /**
     * Переутверждает распределение (заново фиксируются статусы сдачи оригиналов документов)
     *
     * @param distribution распределение, которое требуется переутвердить
     */
    void doDistributionReApprove(IEcgDistribution distribution);

    /**
     * Массово создает основные распределения следующей волны по указанным конфигурациям
     * Для всех конфигураций следующая волна должна быть одинаковой
     * Планы приема заполняются автоматически
     * Для тех распределений где план нулевой, распределение не создается
     * <p/>
     * Может бросаться ApplicationException после изменения данных, т.е. нужно его ловить и чистить сессию
     *
     * @param configSet конфигурации
     */
    void doBulkMakeDistribution(Set<EcgConfigDTO> configSet);

    /**
     * Массово заполняет основные распределения последней волны по указанным конфигурациям
     * Для всех конфигураций последняя волна должна быть одинаковой
     * Если какое-то распределение нельзя заполнить (например, оно зафиксировано), то будет сообщение об этом
     * <p/>
     * Может бросаться ApplicationException после изменения данных, т.е. нужно его ловить и чистить сессию
     *
     * @param configSet конфигурации
     */
    void doBulkFillDistribution(Set<EcgConfigDTO> configSet);

    /**
     * Массово заполняет основные распределения последней волны по указанным конфигурациям
     * абитуриентами поступающими на общих основаниях с оригиналами документов
     * Для всех конфигураций последняя волна должна быть одинаковой
     * Если какое-то распределение нельзя заполнить (например, оно зафиксировано), то будет сообщение об этом
     * <p/>
     * Может бросаться ApplicationException после изменения данных, т.е. нужно его ловить и чистить сессию
     *
     * @param configSet конфигурации
     */
    void doBulkFillWithOriginalDistribution(Set<EcgConfigDTO> configSet);

    /**
     * Массово фиксирует основные распределения последней волны по указанным конфигурациям
     * Для всех конфигураций последняя волна должна быть одинаковой
     * Если какое-то распределение нельзя зафиксировать (например, нет рекомендованных), то будет сообщение об этом
     * <p/>
     * Может бросаться ApplicationException после изменения данных, т.е. нужно его ловить и чистить сессию
     *
     * @param configSet конфигурации
     */
    void doBulkLockDistribution(Set<EcgConfigDTO> configSet);

    /**
     * Массово утверждает основные распределения последней волны по указанным конфигурациям
     * Для всех конфигураций последняя волна должна быть одинаковой
     * Если какое-то распределение нельзя утвердить (например, оно не зафиксировано), то будет сообщение об этом
     * <p/>
     * Может бросаться ApplicationException после изменения данных, т.е. нужно его ловить и чистить сессию
     *
     * @param configSet конфигурации
     */
    void doBulkApproveDistribution(Set<EcgConfigDTO> configSet);

    /**
     * Массово удаляет основные распределения последней волны по указанным конфигурациям (и их уточняющие распределения если они есть)
     * Для всех конфигураций последняя волна должна быть одинаковой
     *
     * @param configSet конфигурации
     */
    void doBulkDeleteDistribution(Set<EcgConfigDTO> configSet);

    /**
     * Массово предзачисляет рекомендованных абитуриентов, у которых еще нет предзачисления
     * Те рекомендованные, у которых уже есть предзачисление пропускаются
     * <p/>
     * Может бросаться ApplicationException после изменения данных, т.е. нужно его ловить и чистить сессию
     *
     * @param recommendedIds идентификаторы рекомендованных
     * @param orderType      тип приказа
     */
    void doBulkPreEnroll(Set<Long> recommendedIds, EntrantEnrollmentOrderType orderType);

    /**
     * Получает rtf-файл описания распределения
     *
     * @param distributionId распределение
     * @return rtf-файл
     */
    byte[] getRtfPrint(Long distributionId);

    /**
     * Получает xml-файл описания для одного распределения
     *
     * @param distributionId распределение
     * @return xml-файл
     */
    byte[] getXmlPrint(Long distributionId);

    /**
     * Получает zip-файл из rtf-файлов с описанием указанных распределений
     *
     * @param distributionIds распределения
     * @return zip-файл
     */
    byte[] getBulkRtfPrint(Set<Long> distributionIds);

    /**
     * Получает xml-файл описания для указанных распределений
     * Все распределения должны быть в одной приемной кампании
     *
     * @param distributionIds распределения
     * @return zip-файл
     */
    byte[] getBulkXmlPrint(Set<Long> distributionIds);

    /**
     * Получает описание свободных мест / планов в html формате
     *
     * @param distribution распределение
     * @param quotaMap     планы по направлениям
     * @param taQuotaMap   планы по направлениям по видам цп
     */
    void getQuotaHTMLDescription(IEcgDistribution distribution, Map<Long, String> quotaMap, Map<Long, String> taQuotaMap);

    /**
     * Проверяет, разрешено ли выбранному направлению приема находиться в распределении с указанной конфигурацией
     * Должны совпадать виды затрат и категории поступающих
     * Проверка на то, что направление приема должно быть в распределении тут специально опускается
     *
     * @param config    конфигурация распределения
     * @param direction выбранное направление приема
     * @return true, если выбранное направление приема может находиться в этом распределении
     */
    boolean isConfigAllowDirection(EcgDistributionConfig config, RequestedEnrollmentDirection direction);

    /**
     * Проверяет, позволяет ли распределение внутри себя предзачислять
     *
     * @param distribution распределение
     * @return true, если разрешено предзачислять абитуриентов в распределении
     */
    boolean isDistributionAllowEnrollment(IEcgDistribution distribution);

    /**
     * Проверяет, является ли распределение последним и утвержденным
     * В этом случае можно его переутверждать и предзачислять в нем
     *
     * @param distribution распределение
     * @return true, если разрешено переутверждать распределение
     */
    boolean isDistributionLastApproved(IEcgDistribution distribution);

    /**
     * Получает список рекомендованных из распределения текущей волны и распределений предыдущих волн
     * и соответствующие на текущий момент (если распределение не утверждено) или на момент утверждения
     * (если распределение утверждено) факт существования предзачисления и оригиналы документов
     *
     * @param distribution основное распределение
     * @return рекомендованные
     */
    Map<EcgEntrantRecommended, IEcgHasBringOriginal> getEntrantRecommendedWithPreStudentList(EcgDistribution distribution);

    /**
     * Получает список абитуриентов, у которых есть предзачисление по распределению в текущий момент
     *
     * @param distribution распределение
     * @return абитуриенты с предзачислением
     */
    Map<Long, PreliminaryEnrollmentStudent> getEntrantPreStudentMap(IEcgDistribution distribution);

    /**
     * Получает информацию о приоритетах НП по абитуриентам
     *
     * @param distribution распределение
     * @param entrantIds   id абитуриентов
     * @return id абитуриента -> информация по приоритетам НП абитуриента в этом распределении
     */
    Map<Long, EcgPriorityInfo> getEntrantPriorityMap(IEcgDistribution distribution, Collection<Long> entrantIds);

    /**
     * Получает рекомендуемые планы приема для распределения на указанную конфигурацию
     *
     * @param configDTO конфигурация распределения
     * @param isFirstWave
     * @return планы приема
     */
    IEcgQuotaDTO getRecomendedQuotaDTO(EcgConfigDTO configDTO, boolean isFirstWave);
}
