/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.Qualification2CompetitionKind;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author agolubenko
 * @since 17.06.2008
 */
public class DAO extends ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent.DAO<Model, CompetitionKind, Qualifications, Boolean, Qualification2CompetitionKindRelation> implements IDAO
{
    @Override
    public void prepareData(Model model)
    {
        model.setStudentCategoryList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(StudentCategory.class));
        model.setMatrixMapData(new HashMap<StudentCategory, Map<CellCoordinates, Boolean>>());
    }

    @Override
    public void update(Model model)
    {
        Map<CellCoordinates, Qualification2CompetitionKindRelation> ids = fillMatrixData(model);

        for (CellCoordinates cellCoordinates : model.getMatrixData().keySet())
        {
            if (model.getMatrixData().get(cellCoordinates))
            {
                if (!ids.containsKey(cellCoordinates))
                {
                    Qualification2CompetitionKindRelation relation = new Qualification2CompetitionKindRelation();
                    relation.setCompetitionKind(getNotNull(CompetitionKind.class, cellCoordinates.getRowId()));
                    relation.setQualification(getNotNull(Qualifications.class, cellCoordinates.getColumnId()));
                    relation.setEnrollmentCampaign(model.getEnrollmentCampaign());
                    relation.setStudentCategory(model.getStudentCategory());
                    getSession().save(relation);
                }
            }
            else
            {
                if (ids.containsKey(cellCoordinates))
                {
                    getSession().delete(ids.get(cellCoordinates));
                }
            }
        }
    }

    @Override
    protected List<Qualifications> getHorizontalObjects(Model model)
    {
        return CatalogManager.instance().dao().getCatalogItemListOrderByCode(Qualifications.class);
    }

    @Override
    protected Long getLeftId(Qualification2CompetitionKindRelation object)
    {
        return object.getCompetitionKind().getId();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<Qualification2CompetitionKindRelation> getObjects(Model model)
    {
        Criteria criteria = getSession().createCriteria(Qualification2CompetitionKindRelation.class);
        criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_STUDENT_CATEGORY, model.getStudentCategory()));
        return criteria.list();
    }

    @Override
    protected Long getRightId(Qualification2CompetitionKindRelation object)
    {
        return object.getQualification().getId();
    }

    @Override
    protected Boolean getValue(Qualification2CompetitionKindRelation object)
    {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<CompetitionKind> getVerticalObjects(Model model)
    {
        Criteria criteria = getSession().createCriteria(EnrollmentCompetitionKind.class);
        criteria.add(Restrictions.eq(EnrollmentCompetitionKind.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        criteria.add(Restrictions.eq(EnrollmentCompetitionKind.P_USED, Boolean.TRUE));
        criteria.add(Restrictions.isNotNull(EnrollmentCompetitionKind.L_COMPETITION_KIND));
        criteria.setProjection(Projections.property(EnrollmentCompetitionKind.L_COMPETITION_KIND));
        List<CompetitionKind> result = criteria.list();
        Collections.sort(result, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        return result;
    }
}
