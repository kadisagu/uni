/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupSetList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSetOpened;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.List;

/**
 * @author vip_delete
 * @since 10.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithExamGroupAutoFormingAndAlgorithm(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(ExamGroupSet.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ExamGroupSet.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        builder.addOrder("e", ExamGroupSet.P_BEGIN_DATE);
        List<ExamGroupSet> list = builder.getResultList(getSession());

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        if (!list.isEmpty())
        {
            ExamGroupSetOpened opened = get(ExamGroupSetOpened.class, ExamGroupSetOpened.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());
            for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
                wrapper.setViewProperty(Model.P_OPEN, opened != null && wrapper.getEntity().equals(opened.getExamGroupSet()));
        }
    }

    @Override
    public void updateExamGroupSetStatus(Long examGroupSetId, boolean open)
    {
        ExamGroupSet examGroupSet = getNotNull(ExamGroupSet.class, examGroupSetId);
        if (open)
            UniecDAOFacade.getExamGroupSetDao().openExamGroupSet(examGroupSet);
        else
            UniecDAOFacade.getExamGroupSetDao().closeExamGroupSet(examGroupSet);
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        Long examGroupSetId = (Long) component.getListenerParameter();
        ExamGroupSet examGroupSet = getNotNull(ExamGroupSet.class, examGroupSetId);
        if (existsEntity(ExamGroup.class, ExamGroup.L_EXAM_GROUP_SET, examGroupSet))
            throw new ApplicationException("Удалить можно только пустой набор.");
        delete(examGroupSet);
    }
}
