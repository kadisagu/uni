/* $Id: Model.java 10428 2009-10-29 14:08:07Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKPub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private EntranceExamMeetingByCKReport2010 _report = new EntranceExamMeetingByCKReport2010();

    public EntranceExamMeetingByCKReport2010 getReport()
    {
        return _report;
    }

    public void setReport(EntranceExamMeetingByCKReport2010 report)
    {
        _report = report;
    }
}
