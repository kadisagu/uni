package ru.tandemservice.uniec.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * План приема в распределении по конкурсной группе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistribQuotaGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.ecg.EcgDistribQuota";
    public static final String ENTITY_NAME = "ecgDistribQuota";
    public static final int VERSION_HASH = 1216819800;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_DIRECTION = "direction";
    public static final String P_QUOTA = "quota";

    private EcgDistribObject _distribution;     // распределение
    private EnrollmentDirection _direction;     // направление приема
    private int _quota;     // Количество мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return распределение. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribObject getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution распределение. Свойство не может быть null.
     */
    public void setDistribution(EcgDistribObject distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return направление приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getDirection()
    {
        return _direction;
    }

    /**
     * @param direction направление приема. Свойство не может быть null.
     */
    public void setDirection(EnrollmentDirection direction)
    {
        dirty(_direction, direction);
        _direction = direction;
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getQuota()
    {
        return _quota;
    }

    /**
     * @param quota Количество мест. Свойство не может быть null.
     */
    public void setQuota(int quota)
    {
        dirty(_quota, quota);
        _quota = quota;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistribQuotaGen)
        {
            setDistribution(((EcgDistribQuota)another).getDistribution());
            setDirection(((EcgDistribQuota)another).getDirection());
            setQuota(((EcgDistribQuota)another).getQuota());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistribQuotaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistribQuota.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistribQuota();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "direction":
                    return obj.getDirection();
                case "quota":
                    return obj.getQuota();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribObject) value);
                    return;
                case "direction":
                    obj.setDirection((EnrollmentDirection) value);
                    return;
                case "quota":
                    obj.setQuota((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "direction":
                        return true;
                case "quota":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "direction":
                    return true;
                case "quota":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribObject.class;
                case "direction":
                    return EnrollmentDirection.class;
                case "quota":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistribQuota> _dslPath = new Path<EcgDistribQuota>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistribQuota");
    }
            

    /**
     * @return распределение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribQuota#getDistribution()
     */
    public static EcgDistribObject.Path<EcgDistribObject> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribQuota#getDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> direction()
    {
        return _dslPath.direction();
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribQuota#getQuota()
     */
    public static PropertyPath<Integer> quota()
    {
        return _dslPath.quota();
    }

    public static class Path<E extends EcgDistribQuota> extends EntityPath<E>
    {
        private EcgDistribObject.Path<EcgDistribObject> _distribution;
        private EnrollmentDirection.Path<EnrollmentDirection> _direction;
        private PropertyPath<Integer> _quota;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return распределение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribQuota#getDistribution()
     */
        public EcgDistribObject.Path<EcgDistribObject> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribObject.Path<EcgDistribObject>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribQuota#getDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> direction()
        {
            if(_direction == null )
                _direction = new EnrollmentDirection.Path<EnrollmentDirection>(L_DIRECTION, this);
            return _direction;
        }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.ecg.EcgDistribQuota#getQuota()
     */
        public PropertyPath<Integer> quota()
        {
            if(_quota == null )
                _quota = new PropertyPath<Integer>(EcgDistribQuotaGen.P_QUOTA, this);
            return _quota;
        }

        public Class getEntityClass()
        {
            return EcgDistribQuota.class;
        }

        public String getEntityName()
        {
            return "ecgDistribQuota";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
