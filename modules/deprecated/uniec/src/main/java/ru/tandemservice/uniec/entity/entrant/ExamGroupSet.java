package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.entity.entrant.gen.ExamGroupSetGen;

/**
 * Набор экзаменационной группы
 */
public class ExamGroupSet extends ExamGroupSetGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate());
    }
}