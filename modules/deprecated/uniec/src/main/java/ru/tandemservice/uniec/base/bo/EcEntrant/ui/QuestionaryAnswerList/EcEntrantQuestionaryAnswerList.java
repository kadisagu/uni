/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.QuestionaryAnswerList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.survey.base.entity.CompletedAnswer;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.survey.QuestionaryEntrant;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 23.03.2017
 */
@Configuration
public class EcEntrantQuestionaryAnswerList extends BusinessComponentManager
{
    public static final String ENTRANT_ID_PARAM = "entrantId";
    public static final String TEMPLATE_ID_PARAM = "templateId";
    public static final String FOR_CAMPAIGN_PARAM = "forCampaign";

    public static final String COMPLETED_ANSWER_DS = "completedAnswerDS";


    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(COMPLETED_ANSWER_DS, completedAnswerDSColumn(), completedAnswerDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint completedAnswerDSColumn()
    {
        return columnListExtPointBuilder(COMPLETED_ANSWER_DS)
                .addColumn(textColumn("question", CompletedAnswer.question().question().title()))
                .addColumn(textColumn("answer", "").formatter(source -> ((CompletedAnswer) source).getPrintValue()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> completedAnswerDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long entrantId = context.get(ENTRANT_ID_PARAM);
                Long templateId = context.get(TEMPLATE_ID_PARAM);
                boolean forCampaign = context.get(FOR_CAMPAIGN_PARAM);

                String alias = "ca";
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(CompletedAnswer.class, alias)
                        .where(eq(property(alias, CompletedAnswer.checked()), value(Boolean.TRUE)))

                        .order(property(alias, CompletedAnswer.question().questionGroup().priority()))
                        .order(property(alias, CompletedAnswer.question().priority()));

                if (forCampaign)
                {
                    Entrant entrant = DataAccessServices.dao().get(entrantId);
                    SurveyTemplate2EnrCampaignRel rel = DataAccessServices.dao()
                            .getUnique(SurveyTemplate2EnrCampaignRel.class, SurveyTemplate2EnrCampaignRel.campaign().s(), entrant.getEnrollmentCampaign());
                    if (rel == null) dql.where(nothing());
                    else if (templateId != null && !templateId.equals(rel.getTemplate().getId())) dql.where(nothing());
                }

                IDQLExpression[] expressions;
                if (templateId != null)
                {
                    expressions = new IDQLExpression[3];
                    expressions[2] = eq(property("que", QuestionaryEntrant.template().id()), value(templateId));
                }
                else expressions = new IDQLExpression[2];

                expressions[0] = eq(property("que"), property(alias, CompletedAnswer.questionary()));
                expressions[1] = eq(property("que", QuestionaryEntrant.entrant()), value(entrantId));

                dql.where(existsByExpr(QuestionaryEntrant.class, "que", and(expressions)));


                Number count = dql.createCountStatement(new DQLExecutionContext(context.getSession())).uniqueResult();

                DSOutput output = DQLSelectOutputBuilder.get(input, dql, getSession()).build();
                output.setCountRecord(count.intValue());

                return output;
            }
        };
    }
}
