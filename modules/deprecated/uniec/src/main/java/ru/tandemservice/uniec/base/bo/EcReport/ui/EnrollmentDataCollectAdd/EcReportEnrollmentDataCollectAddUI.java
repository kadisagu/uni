/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectAdd;

import jxl.write.WriteException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect.EnrollmentDataCollectReportModel;
import ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectPub.EcReportEnrollmentDataCollectPub;
import ru.tandemservice.uniec.entity.report.EnrollmentDataCollectReport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
public class EcReportEnrollmentDataCollectAddUI extends UIPresenter
{
    private EnrollmentDataCollectReportModel _model;

    @Override
    public void onComponentRefresh()
    {
        _model = EcReportManager.instance().enrollmentDataCollectDao().createModel();

        final UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        util
                .configNeedEnableCheckBox(true)
                .configUseFilters(
                        UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION,
                        UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
                        UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                        UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH,
                        UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD);
    }

    public void onClickApply()
    {
        final UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        _model.setEduFiltersUtil(util);

        DatabaseFile reportFile;
        try
        {
            reportFile = EcReportManager.instance().enrollmentDataCollectDao().createPrintReportFile(_model);
        }
        catch (IOException | WriteException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        final EnrollmentDataCollectReport report = EcReportManager.instance().enrollmentDataCollectDao().createReport(_model, reportFile);

        final DatabaseFile finalReportFile = reportFile;
        DataAccessServices.dao().doInTransaction(session -> {
            session.save(finalReportFile);
            session.save(report);

            return null;
        });

        deactivate();

        getActivationBuilder().asDesktopRoot(EcReportEnrollmentDataCollectPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }


    // Accessors

    public EnrollmentDataCollectReportModel getModel()
    {
        return _model;
    }

    public void setModel(EnrollmentDataCollectReportModel model)
    {
        _model = model;
    }
}
