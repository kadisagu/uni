/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.util;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderEnrollmentExtractDSHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.BlockParPub.EcOrderBlockParPub;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "paragraphId")
})
public abstract class BaseEcOrderParPubUI extends UIPresenter
{
    public static final String EC_ORDER_BLOCK_PAR_PUB_REGION = "ecOrderBlockParPubRegion";

    private Long _paragraphId;
    private String _formTitle;
    private EnrollmentParagraph _paragraph;
    private EnrollmentOrderType _type;
    private EnrollmentExtract _extract;
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        _paragraph = DataAccessServices.dao().getNotNull(_paragraphId);
        _extract = _paragraph.getFirstExtract();
        _secModel = new CommonPostfixPermissionModel("enrollmentOrder");
        EnrollmentOrder order = (EnrollmentOrder) _paragraph.getOrder();
        _type = EcOrderManager.instance().dao().getEnrollmentOrderType(order.getEnrollmentCampaign(), order.getType());
        _formTitle = "Параграф приказа «" + order.getType().getTitle() + "»";

        // отображает общую часть карточки параграфа о зачислении
        UIPresenter presenter = _uiSupport.getChildUI(EC_ORDER_BLOCK_PAR_PUB_REGION);
        if (presenter == null)
            _uiActivation.asRegion(EcOrderBlockParPub.class, EC_ORDER_BLOCK_PAR_PUB_REGION).parameter(UIPresenter.PUBLISHER_ID, _paragraphId).activate();
        else
            presenter.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcOrderManager.ENROLLMENT_EXTRACT_DS.equals(dataSource.getName()))
            dataSource.put(EcOrderEnrollmentExtractDSHandler.ENROLLMENT_PARAGRAPH, _paragraph);
    }

    public boolean isCanAnnul()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(_paragraph.getOrder().getState().getCode());
    }

    // Getters & Setters

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public String getFormTitle()
    {
        return _formTitle;
    }

    public void setFormTitle(String formTitle)
    {
        _formTitle = formTitle;
    }

    public EnrollmentParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrollmentParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public EnrollmentOrderType getType()
    {
        return _type;
    }

    public void setType(EnrollmentOrderType type)
    {
        _type = type;
    }

    public EnrollmentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(EnrollmentExtract extract)
    {
        _extract = extract;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    // Listeners

    public void onClickEditParagraph()
    {
        EnrollmentOrder order = (EnrollmentOrder) _paragraph.getOrder();

        // validate
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) return;

        _uiActivation.asRegion(EcOrderUtil.getParagraphAddEditComponent(order.getType()))
                .parameter(BaseEcOrderParAddEditUI.PARAMETER_PARAGRAPH_ID, _paragraphId)
                .activate();
    }

    public void onClickDeleteParagraph()
    {
        EnrollmentOrder order = (EnrollmentOrder) _paragraph.getOrder();

        // validate
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode())) return;

        EcOrderManager.instance().dao().deleteParagraph(_paragraph);

        deactivate();
    }

    public void onClickPrintExtract()
    {
        EcOrderManager.instance().dao().getDownloadExtractPrintForm(getListenerParameterAsLong(), false);
    }

    public void onClickDeleteExtract()
    {
        EnrollmentExtract extract = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());

        EcOrderManager.instance().dao().deleteExtract(extract);
    }

    public void onClickAnnulExtract()
    {
        EnrollmentExtract extract = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());

        EcOrderManager.instance().dao().executeAnnulExtract(extract);

        if (extract.getParagraph().getExtractCount() == 0)
            deactivate();
    }
}
