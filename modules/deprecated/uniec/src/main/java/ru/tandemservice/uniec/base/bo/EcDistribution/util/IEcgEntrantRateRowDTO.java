/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.List;

import org.tandemframework.core.entity.IEntity;

/**
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public interface IEcgEntrantRateRowDTO extends IEntity, IEcgCortegeDTO
{
    /**
     * правило выборки строк для рейтинга
     */
    public static enum Rule
    {
        RULE_TARGET_ADMISSION,          // добавляются по целевому приему
        RULE_NON_COMPETITIVE,           // добавляются не по общим основаниям (не по конкурсному приему)
        RULE_COMPETITIVE,               // добавляются по общим основаниям (по конкурсному приему)
        RULE_COMPETITIVE_WITH_ORIGINAL, // добавляются по общим основаниям (по конкурсному приему) с оригиналами
    }

    final String COMPETITION_KIND_TITLE = "competitionKindTitle";
    final String TARGET_ADMISSION_KIND_TITLE = "targetAdmissionKindTitle";
    final String FINAL_MARK = "finalMark";
    final String PROFILE_MARK = "profileMark";
    final String CERTIFICATE_AVERAGE_MARK = "certificateAverageMark";
    final String GRADUATED_PROFILE_EDU_INSTITUTION_TITLE = "graduatedProfileEduInstitutionTitle";
    final String FIO = "fio";
    final String PRIORITIES = "priorities";
    final String ORIGINAL_DOCUMENT_HANDED_IN_TITLE = "originalDocumentHandedInTitle";

    /**
     * @return название вид конкурса
     */
    String getCompetitionKindTitle();

    /**
     * @return название вида цп
     */
    String getTargetAdmissionKindTitle();

    /**
     * @return "Да", если закончил профильное учебное учреждение по первому направлению из directionList
     *         "", в другом случае
     */
    String getGraduatedProfileEduInstitutionTitle();

    /**
     * @return сокр. названия всех НПВ из directionList
     */
    String getPriorities();

    /**
     * @return "оригиналы", если сданы оригиналы документов по первому направлению из directionList
     *         "копии", в другом случае
     */
    String getOriginalDocumentHandedInTitle();

    /**
     * @return список направлений по приоритетам
     *         все направления всегда по одному виду цп и одному виду конкурса
     *         всегда не null и не пусто
     */
    List<IEcgEntrantRateDirectionDTO> getDirectionList();
}
