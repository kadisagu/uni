/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertParAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 05.08.2013
 */
@Input({
               @Bind(key = EcOrderRevertParAddEditUI.PARAMETER_ORDER_ID, binding = EcOrderRevertParAddEditUI.PARAMETER_ORDER_ID),
               @Bind(key = EcOrderRevertParAddEditUI.PARAMETER_PARAGRAPH_ID, binding = EcOrderRevertParAddEditUI.PARAMETER_PARAGRAPH_ID)
       })
public class EcOrderRevertParAddEditUI extends UIPresenter
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_PARAGRAPH_ID = "paragraphId";

    private Long orderId;
    private Long paragraphId;
    private EnrollmentRevertParagraph paragraph;
    private EntrantEnrollmentOrderType canceledOrderType;
    private Set<EnrollmentExtract> selectedExtracts = new LinkedHashSet<>();
    private boolean isAddForm;

    @Override
    public void onComponentRefresh()
    {
        isAddForm = orderId != null;

        if (isAddForm)
        {
            paragraph = new EnrollmentRevertParagraph();
            paragraph.setOrder(DataAccessServices.dao().getNotNull(EnrollmentOrder.class, orderId));
        }
        else
        {
            paragraph = DataAccessServices.dao().getNotNull(EnrollmentRevertParagraph.class, paragraphId);
            canceledOrderType = paragraph.getCanceledOrder().getType();
            for (IAbstractExtract extract : paragraph.getExtractList())
            {
                selectedExtracts.add(((EnrollmentRevertExtract) extract).getCanceledExtract());
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EcOrderRevertParAddEdit.ENROLLMENT_CAMPAIGN_PARAM, ((EnrollmentOrder) paragraph.getOrder()).getEnrollmentCampaign());
        switch (dataSource.getName())
        {
            case EcOrderRevertParAddEdit.ORDER_DS:
                dataSource.put(EcOrderRevertParAddEdit.ORDER_TYPE_PARAM, canceledOrderType);
                break;
            case EcOrderRevertParAddEdit.EXTRACT_DS:
                dataSource.put(EcOrderRevertParAddEdit.ORDER_PARAM, paragraph.getCanceledOrder());
                break;
        }
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcOrderRevertParAddEdit.EXTRACT_DS.equals(dataSource.getName()))
        {
            AbstractListDataSource<IEntity> ds = ((BaseSearchListDataSource) dataSource).getLegacyDataSource();
            CheckboxColumn column = (CheckboxColumn) ds.getColumn(EcOrderRevertParAddEdit.CHECKBOX_COLUMN);
            column.reset();
            for (IAbstractExtract extract : selectedExtracts)
            {
                column.selectEntity(extract);
            }
        }
    }

    // Listeners

    public void onClickApply()
    {
        DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource(EcOrderRevertParAddEdit.EXTRACT_DS)).getLegacyDataSource();
        CheckboxColumn column = ((CheckboxColumn) ds.getColumn(EcOrderRevertParAddEdit.CHECKBOX_COLUMN));
        selectedExtracts.clear();
        for (IEntity entity : column.getSelectedObjects())
        {
            selectedExtracts.add((EnrollmentExtract) ((DataWrapper) entity).getWrapped());
        }

        EcOrderManager.instance().dao().saveOrUpdateEnrollmentRevertParagraph(paragraph, selectedExtracts);

        deactivate();
    }

    // Getters & Setters
    public String getPermissionKey()
    {
        return isAddForm ? "addParagraph_enrollmentOrder" : "editParagraph_enrollmentOrder";
    }

    public String getFormTitle()
    {
        EnrollmentOrder order = (EnrollmentOrder) paragraph.getOrder();
        return (isAddForm ? "Добавление" : "Редактирование") +
                " параграфа приказа" +
                (order.getNumber() != null ? " №" + order.getNumber() : "") +
                (order.getCommitDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) : "") +
                " «" + order.getType().getTitle() + "»";
    }

    public boolean isEditForm()
    {
        return !isAddForm;
    }

    public EntrantEnrollmentOrderType getCanceledOrderType()
    {
        return canceledOrderType;
    }

    public void setCanceledOrderType(EntrantEnrollmentOrderType canceledOrderType)
    {
        this.canceledOrderType = canceledOrderType;
    }

    public Long getOrderId()
    {
        return orderId;
    }

    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public Long getParagraphId()
    {
        return paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        this.paragraphId = paragraphId;
    }

    public EnrollmentRevertParagraph getParagraph()
    {
        return paragraph;
    }
}