/* $Id: DAO.java 13269 2010-06-17 15:59:00Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.report.EntranceExamMeetingByCKReport2010;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;
import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setPrintFormList(Arrays.asList(new IdentifiableWrapper(Model.PRINT_FORM_1, "Форма 1"), new IdentifiableWrapper(Model.PRINT_FORM_2, "Форма 2"), new IdentifiableWrapper(Model.PRINT_FORM_3, "Форма 3")));
        model.setPrintForm(model.getPrintFormList().get(0));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setFormativeOrgUnitModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelsHighSchoolModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setCustomStateRulesModel(TwinComboDataSourceHandler.getCustomSelectModel("Содержит", "Не содержит"));
        model.setEntrantCustomStateListModel(new LazySimpleSelectModel<>(EntrantCustomStateCI.class));
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EntranceExamMeetingByCKReport2010 report = model.getReport();
        report.setPrintForm(model.getPrintForm().getTitle());
        report.setFormingDate(new Date());
        report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        report.setQualificationTitle(StringUtils.lowerCase(CommonBaseStringUtil.join(model.getQualificationList(), "title", "; ")));
        report.setNotIncludeEnrollment(model.isNotIncludeEnrollment());

        if (!model.isByAllEnrollmentDirections())
        {
            report.setFormativeOrgUnitTitle(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
            if (model.isTerritorialOrgUnitActive())
                report.setTerritorialOrgUnitTitle(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
            if (model.isEducationLevelsHighSchoolActive())
                report.setEducationLevelHighSchoolTitle(UniStringUtils.join(model.getEducationLevelsHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "; "));
            if (model.isDevelopFormActive())
                report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.P_TITLE, "; "));
            if (model.isDevelopConditionActive())
                report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
            if (model.isDevelopTechActive())
                report.setDevelopTechTitle(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.P_TITLE, "; "));
            if (model.isDevelopPeriodActive())
                report.setDevelopPeriodTitle(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, "; "));
        }

        if (CollectionUtils.isNotEmpty(model.getEntrantCustomStateList()))
            report.setEntrantCustomStateTitle(model.getCustomStateRule().getTitle() + ": "
                                                      + UniStringUtils.join(model.getEntrantCustomStateList(), EntrantCustomStateCI.P_TITLE, ", "));

        EntranceExamMeetingRtfBuilder rtfBuilder;
        switch (model.getPrintForm().getId().intValue())
        {
            case (int) Model.PRINT_FORM_1:
                rtfBuilder = new EntranceExamMeetingForm1RtfBuilder();
                break;
            case (int) Model.PRINT_FORM_2:
                rtfBuilder = new EntranceExamMeetingForm2RtfBuilder();
                break;
            case (int) Model.PRINT_FORM_3:
                rtfBuilder = new EntranceExamMeetingForm3RtfBuilder();
                break;
            default:
                throw new RuntimeException("No RtfBuilder for '" + model.getPrintForm().getTitle() + "'");
        }

        DatabaseFile databaseFile = new EntranceExamMeetingByCKReportBuilder(model, session).getContent(rtfBuilder);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
