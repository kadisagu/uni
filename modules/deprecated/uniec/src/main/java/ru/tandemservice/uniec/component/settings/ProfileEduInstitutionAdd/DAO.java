/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProfileEduInstitutionAdd;

import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.fias.base.bo.util.SettlementAutocompleteModel;
import org.tandemframework.shared.fias.base.entity.AddressCountry;

import org.tandemframework.shared.person.base.bo.PersonSetting.util.EduInstitutionKindAutocompleteModel;
import org.tandemframework.shared.person.base.bo.PersonSetting.util.EduInstitutionTypeAutocompleteModel;
import ru.tandemservice.uni.dao.UniDao;
import org.tandemframework.shared.person.base.bo.Person.util.EduInstitutionAutocompleteModel;

/**
 * @author vip_delete
 * @since 04.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setCountries(new CountryAutocompleteModel());
        model.setSettlements(new SettlementAutocompleteModel(model));
        model.setPersonEduInstitutionTypeList(new EduInstitutionTypeAutocompleteModel(""));
        model.setPersonEduInstitutionKindList(new EduInstitutionKindAutocompleteModel(model, ""));
        model.setPersonEduInstitutionList(new EduInstitutionAutocompleteModel<Model>(model));

        model.setAddressCountry(get(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE));
    }

    @Override
    public void update(Model model)
    {
        getSession().refresh(model.getPersonEduInstitution());
        model.getPersonEduInstitution().setProfile(true);
        update(model.getPersonEduInstitution());
    }
}
