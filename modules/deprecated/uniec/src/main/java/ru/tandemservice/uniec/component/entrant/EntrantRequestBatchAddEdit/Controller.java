/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantRequestBatchAddEdit;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;

import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 * @since 07.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        preparePossibleDirectionDataSource(component);

        prepareSelectedRequestedEnrollmentDirectionDataSource(component);

        getDao().prepare(model);

        model.setRegDateDisabled(!CoreServices.securityService().check(model.getEntrantRequest().getEntrant(), component.getUserContext().getPrincipalContext(), "editRequestRegistrationDate"));

        // показывать номер заявления на форме редактирования или при условии его ручного ввода
        model.setRegNumberVisible(model.isEditForm() || model.getEnrollmentCampaign().isNumberOnRequestManual());
    }

    public void onClickNext(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);

        dao.update(model);

        if (model.isAddForm())
        {
            component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_REQUEST_DOCUMENT_ADD_EDIT, new ParametersMap()
                    .add("entrantRequestId", model.getEntrantRequest().getId())
                    .add("addForm", Boolean.TRUE)
            ));
        } else
        {
            deactivate(component);
            ((BusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh();
        }
    }

    @SuppressWarnings("unchecked")
    private void preparePossibleDirectionDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        //if (model.getPossibleDirectionDataSource() != null) return;

        DynamicListDataSource<EnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new BlockColumn("checkbox", ""));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EnrollmentDirection.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EnrollmentDirection.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EnrollmentDirection.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EnrollmentDirection.educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));

        model.setPossibleDirectionDataSource(dataSource);
    }

    private void prepareSelectedRequestedEnrollmentDirectionDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        //if (model.getSelectedRequestedEnrollmentDirectionDataSource() != null) return;

        model.setSelectedRequestedEnrollmentDirectionDataSource(EntrantRequestAddEditUtil.getSelectedRequestedEnrollmentDirectionDataSource(component, component1 -> {
            model.getSelectedRequestedEnrollmentDirectionDataSource().setCountRow(model.getSelectedRequestedEnrollmentDirectionList().size());
            UniBaseUtils.createPage(model.getSelectedRequestedEnrollmentDirectionDataSource(), model.getSelectedRequestedEnrollmentDirectionList());
        }));
    }

    // Event Listeners

    public void onClickSelect(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().prepareSelectedEnrollmentDirectionList(getModel(component), errors);
        if (!errors.hasErrors()) onClickReset(component);
    }

    public void onClickReset(IBusinessComponent component)
    {
        getDao().reset(getModel(component));
    }

    public void onClickUp(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = (Long) component.getListenerParameter();
        EntrantRequestAddEditUtil.processClickUp(model.getSelectedRequestedEnrollmentDirectionList(), id);
    }

    public void onClickDown(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = (Long) component.getListenerParameter();
        EntrantRequestAddEditUtil.processClickDown(model.getSelectedRequestedEnrollmentDirectionList(), id);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long id = (Long) component.getListenerParameter();
        Set<Long> existedIds = model.getExistedRequestedEnrollmentDirectionIds();
        List<RequestedEnrollmentDirection> forDeleteList = model.getForDelete();
        EntrantRequestAddEditUtil.processClickDelete(model.getSelectedRequestedEnrollmentDirectionList(), id, existedIds, forDeleteList);
    }

    public void resetCompetitionKind(IBusinessComponent component)
    {
        getModel(component).setCompetitionKind(null);
    }
}
