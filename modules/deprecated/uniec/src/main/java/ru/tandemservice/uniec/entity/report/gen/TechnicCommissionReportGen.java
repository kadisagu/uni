package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.TechnicCommissionReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка для технических комиссий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TechnicCommissionReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.TechnicCommissionReport";
    public static final String ENTITY_NAME = "technicCommissionReport";
    public static final int VERSION_HASH = 2038782095;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_ENROLLMENT_CAMPAIGN_STAGE = "enrollmentCampaignStage";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_COMPETITION_GROUP_TITLE = "competitionGroupTitle";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_TECHNIC_COMMISSION = "technicCommission";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private String _enrollmentCampaignStage;     // Стадия приемной кампании
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _competitionGroupTitle;     // Конкурсная группа
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _studentCategory;     // Категория поступающего
    private String _technicCommission; 
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    /**
     * @param enrollmentCampaignStage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentCampaignStage(String enrollmentCampaignStage)
    {
        dirty(_enrollmentCampaignStage, enrollmentCampaignStage);
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Конкурсная группа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompetitionGroupTitle()
    {
        return _competitionGroupTitle;
    }

    /**
     * @param competitionGroupTitle Конкурсная группа. Свойство не может быть null.
     */
    public void setCompetitionGroupTitle(String competitionGroupTitle)
    {
        dirty(_competitionGroupTitle, competitionGroupTitle);
        _competitionGroupTitle = competitionGroupTitle;
    }

    /**
     * @return Заявления с.
     */
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по.
     */
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория поступающего.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getTechnicCommission()
    {
        return _technicCommission;
    }

    /**
     * @param technicCommission 
     */
    public void setTechnicCommission(String technicCommission)
    {
        dirty(_technicCommission, technicCommission);
        _technicCommission = technicCommission;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TechnicCommissionReportGen)
        {
            setEnrollmentCampaign(((TechnicCommissionReport)another).getEnrollmentCampaign());
            setEnrollmentCampaignStage(((TechnicCommissionReport)another).getEnrollmentCampaignStage());
            setCompensationType(((TechnicCommissionReport)another).getCompensationType());
            setCompetitionGroupTitle(((TechnicCommissionReport)another).getCompetitionGroupTitle());
            setDateFrom(((TechnicCommissionReport)another).getDateFrom());
            setDateTo(((TechnicCommissionReport)another).getDateTo());
            setStudentCategory(((TechnicCommissionReport)another).getStudentCategory());
            setTechnicCommission(((TechnicCommissionReport)another).getTechnicCommission());
            setExcludeParallel(((TechnicCommissionReport)another).getExcludeParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TechnicCommissionReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TechnicCommissionReport.class;
        }

        public T newInstance()
        {
            return (T) new TechnicCommissionReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "enrollmentCampaignStage":
                    return obj.getEnrollmentCampaignStage();
                case "compensationType":
                    return obj.getCompensationType();
                case "competitionGroupTitle":
                    return obj.getCompetitionGroupTitle();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "technicCommission":
                    return obj.getTechnicCommission();
                case "excludeParallel":
                    return obj.getExcludeParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "enrollmentCampaignStage":
                    obj.setEnrollmentCampaignStage((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "competitionGroupTitle":
                    obj.setCompetitionGroupTitle((String) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "technicCommission":
                    obj.setTechnicCommission((String) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "enrollmentCampaignStage":
                        return true;
                case "compensationType":
                        return true;
                case "competitionGroupTitle":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "studentCategory":
                        return true;
                case "technicCommission":
                        return true;
                case "excludeParallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "enrollmentCampaignStage":
                    return true;
                case "compensationType":
                    return true;
                case "competitionGroupTitle":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "studentCategory":
                    return true;
                case "technicCommission":
                    return true;
                case "excludeParallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "enrollmentCampaignStage":
                    return String.class;
                case "compensationType":
                    return CompensationType.class;
                case "competitionGroupTitle":
                    return String.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "studentCategory":
                    return String.class;
                case "technicCommission":
                    return String.class;
                case "excludeParallel":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TechnicCommissionReport> _dslPath = new Path<TechnicCommissionReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TechnicCommissionReport");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getEnrollmentCampaignStage()
     */
    public static PropertyPath<String> enrollmentCampaignStage()
    {
        return _dslPath.enrollmentCampaignStage();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Конкурсная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getCompetitionGroupTitle()
     */
    public static PropertyPath<String> competitionGroupTitle()
    {
        return _dslPath.competitionGroupTitle();
    }

    /**
     * @return Заявления с.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getTechnicCommission()
     */
    public static PropertyPath<String> technicCommission()
    {
        return _dslPath.technicCommission();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    public static class Path<E extends TechnicCommissionReport> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _enrollmentCampaignStage;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _competitionGroupTitle;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<String> _technicCommission;
        private PropertyPath<Boolean> _excludeParallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getEnrollmentCampaignStage()
     */
        public PropertyPath<String> enrollmentCampaignStage()
        {
            if(_enrollmentCampaignStage == null )
                _enrollmentCampaignStage = new PropertyPath<String>(TechnicCommissionReportGen.P_ENROLLMENT_CAMPAIGN_STAGE, this);
            return _enrollmentCampaignStage;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Конкурсная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getCompetitionGroupTitle()
     */
        public PropertyPath<String> competitionGroupTitle()
        {
            if(_competitionGroupTitle == null )
                _competitionGroupTitle = new PropertyPath<String>(TechnicCommissionReportGen.P_COMPETITION_GROUP_TITLE, this);
            return _competitionGroupTitle;
        }

    /**
     * @return Заявления с.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(TechnicCommissionReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(TechnicCommissionReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(TechnicCommissionReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return 
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getTechnicCommission()
     */
        public PropertyPath<String> technicCommission()
        {
            if(_technicCommission == null )
                _technicCommission = new PropertyPath<String>(TechnicCommissionReportGen.P_TECHNIC_COMMISSION, this);
            return _technicCommission;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.TechnicCommissionReport#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(TechnicCommissionReportGen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

        public Class getEntityClass()
        {
            return TechnicCommissionReport.class;
        }

        public String getEntityName()
        {
            return "technicCommissionReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
