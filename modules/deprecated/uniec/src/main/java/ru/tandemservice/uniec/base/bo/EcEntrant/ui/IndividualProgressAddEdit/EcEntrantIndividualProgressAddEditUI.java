/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.IndividualProgressAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.IndProg2Request4Exclude;
import ru.tandemservice.uniec.entity.entrant.gen.IndProg2Request4ExcludeGen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
@Input({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantId"),
                @Bind(key="individualProgress", binding = "progressId")
       })
public class EcEntrantIndividualProgressAddEditUI extends UIPresenter
{
    private Long entrantId;
    private Long progressId;
    private Entrant _entrant;
    private EntrantIndividualProgress _individualProgress;
    private List<EntrantRequest> _entrantRequests;
    private List<IndProg2Request4Exclude> _request4ExcludeList = new ArrayList<>();

    @Override
    public void onComponentRefresh()
    {
        if (isAddForm())
        {
            if (entrantId == null)
                throw new ApplicationException("Невозможно создать достижение.");

            _entrant = DataAccessServices.dao().get(entrantId);
            _individualProgress = new EntrantIndividualProgress();
            _individualProgress.setEntrant(_entrant);
        }
        else
        {
            if (progressId == null)
                throw new ApplicationException("Невозможно создать достижение.");

            _individualProgress = DataAccessServices.dao().get(progressId);
            _entrant = _individualProgress.getEntrant();

            _request4ExcludeList = DataAccessServices.dao()
                    .getList(IndProg2Request4Exclude.class, IndProg2Request4Exclude.individualProgress().s(), _individualProgress);

            _entrantRequests = _request4ExcludeList.stream().map(IndProg2Request4ExcludeGen::getRequest).collect(Collectors.toList());
        }
    }

    public void onClickApply()
    {
        ICommonDAO dao = DataAccessServices.dao();

        //проверяем, что выставленный балл не превышает максимально допустимый, указанный в справочнике
        if (_individualProgress.getIndividualProgressType().getMaxMark() < _individualProgress.getMark())
            throw new ApplicationException("Балл за данное индивидуальное достижение не может превышать " + _individualProgress.getIndividualProgressType().getMaxMark());

        dao.saveOrUpdate(_individualProgress);

        List<EntrantRequest> exists = new ArrayList<>();
        _request4ExcludeList.forEach(i2r -> {
            if(_entrantRequests.contains(i2r.getRequest()))
                exists.add(i2r.getRequest());
            else
                dao.delete(i2r);
        });

        _entrantRequests.stream()
                .filter(r -> !exists.contains(r))
                .map(r -> new IndProg2Request4Exclude(_individualProgress, r))
                .forEach(dao::save);

        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EcEntrantIndividualProgressAddEdit.INDIVIDUAL_PROGRESS_DS))
        {
            dataSource.put(EcEntrantIndividualProgressAddEdit.ENTRANT, _entrant);
            Long progressId = _individualProgress.getIndividualProgressType() != null ? _individualProgress.getIndividualProgressType().getId() : null;
            dataSource.put(EcEntrantIndividualProgressAddEdit.PROGRESS_ID, progressId);
        }

        if (dataSource.getName().equals(EcEntrantIndividualProgressAddEdit.ENTRANT_REQUEST_DS))
            dataSource.put(EcEntrantIndividualProgressAddEdit.ENTRANT, _entrant);
    }

    public boolean isEditForm()
    {
        return !isAddForm();
    }

    public boolean isAddForm()
    {
        return  progressId == null;
    }

    public Long getProgressId()
    {
        return progressId;
    }

    public void setProgressId(Long progressId)
    {
        this.progressId = progressId;
    }

    public Long getEntrantId()
    {
        return entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        this.entrantId = entrantId;
    }

    public EntrantIndividualProgress getIndividualProgress()
    {
        return _individualProgress;
    }

    public void setIndividualProgress(EntrantIndividualProgress individualProgress)
    {
        _individualProgress = individualProgress;
    }

    public void onChangeProgressType()
    {
        if (_individualProgress.getIndividualProgressType() != null)
            _individualProgress.setMark(_individualProgress.getIndividualProgressType().getMaxMark());
    }

    public List<EntrantRequest> getEntrantRequests()
    {
        return _entrantRequests;
    }

    public void setEntrantRequests(List<EntrantRequest> entrantRequests)
    {
        _entrantRequests = entrantRequests;
    }
}