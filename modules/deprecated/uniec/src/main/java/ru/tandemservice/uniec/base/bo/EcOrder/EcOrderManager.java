/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ActionDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.BooleanDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.CheckboxDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.RadioDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.NoWrapFormatter;

import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderDao;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderEnrollmentExtractDSHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderPreStudentDSHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEcOrderDao;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Configuration
public class EcOrderManager extends BusinessObjectManager
{
    public static final String PRE_STUDENT_DS = "preStudentDS";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String RADIOBOX_COLUMN = "radiobox";

    public static final String ENROLLMENT_EXTRACT_DS = "enrollmentExtractDS";

    public static EcOrderManager instance()
    {
        return instance(EcOrderManager.class);
    }

    @Bean
    public IEcOrderDao dao()
    {
        return new EcOrderDao();
    }

    /**
     * Создается точка расширения "Список студентов предзачисления на форме добавления/редактирования параграфа", единая
     * для всех форм добавления/редактирования параграфов о зачислении
     *
     * @return точка расширения "Список студентов предзачисления на форме добавления/редактирования параграфа"
     */
    @Bean
    public ColumnListExtPoint preStudentDS()
    {
        return ColumnListExtPoint.with(getName(), PRE_STUDENT_DS)
                .addColumn(CheckboxDSColumn.with().name(CHECKBOX_COLUMN).create())
                .addColumn(RadioDSColumn.with().name(RADIOBOX_COLUMN).visible("ui:type.selectHeadman").create())
                .addColumn(TextDSColumn.with().name("fio").path(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("sex").path(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().sex().shortTitle()).create())
                .addColumn(TextDSColumn.with().name("passport").path(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("conditions").path(PreliminaryEnrollmentStudent.enrollmentConditions()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(BooleanDSColumn.with().name("originals").path(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().originalDocumentHandedIn()).create())
                .addColumn(TextDSColumn.with().name("eduInstitution").path(EcOrderPreStudentDSHandler.EDU_INSTITUTION).create())
                .addColumn(TextDSColumn.with().name("territorialOrgUnit").path(PreliminaryEnrollmentStudent.educationOrgUnit().territorialOrgUnit().territorialShortTitle()).visible("ui:enrollParagraphPerEduOrgUnit").create())
                .addColumn(TextDSColumn.with().name("developCondition").path(PreliminaryEnrollmentStudent.educationOrgUnit().developCondition().title()).visible("ui:enrollParagraphPerEduOrgUnit").create())
                .addColumn(TextDSColumn.with().name("developTech").path(PreliminaryEnrollmentStudent.educationOrgUnit().developTech().title()).visible("ui:enrollParagraphPerEduOrgUnit").create())
                .addColumn(TextDSColumn.with().name("developPeriod").path(PreliminaryEnrollmentStudent.educationOrgUnit().developPeriod().title()).visible("ui:enrollParagraphPerEduOrgUnit").create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> preStudentDSHandler()
    {
        return new EcOrderPreStudentDSHandler(getName());
    }

    /**
     * Создается точка расширения "Список выписок о зачислении в карточке параграфа", единая
     * для всех публикаторов параграфов о зачислении
     *
     * @return точка расширения "Список выписок о зачислении в карточке параграфа"
     */
    @Bean
    public ColumnListExtPoint enrollmentExtractDS()
    {
        return ColumnListExtPoint.with(getName(), ENROLLMENT_EXTRACT_DS)
                .addColumn(TextDSColumn.with().name("fio").path(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("sex").path(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().sex().shortTitle()).create())
                .addColumn(TextDSColumn.with().name("passport").path(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullNumber()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("conditions").path(EnrollmentExtract.entity().enrollmentConditions()).formatter(NoWrapFormatter.INSTANCE).create())
                .addColumn(TextDSColumn.with().name("eduInstitution").path(EcOrderEnrollmentExtractDSHandler.EDU_INSTITUTION).create())
                .addColumn(TextDSColumn.with().name("territorialOrgUnit").path(EnrollmentExtract.entity().educationOrgUnit().territorialOrgUnit().territorialShortTitle()).visible("ui:paragraph.enrollParagraphPerEduOrgUnit").create())
                .addColumn(TextDSColumn.with().name("developCondition").path(EnrollmentExtract.entity().educationOrgUnit().developCondition().title()).visible("ui:paragraph.enrollParagraphPerEduOrgUnit").create())
                .addColumn(TextDSColumn.with().name("developTech").path(EnrollmentExtract.entity().educationOrgUnit().developTech().title()).visible("ui:paragraph.enrollParagraphPerEduOrgUnit").create())
                .addColumn(TextDSColumn.with().name("developPeriod").path(EnrollmentExtract.entity().educationOrgUnit().developPeriod().title()).visible("ui:paragraph.enrollParagraphPerEduOrgUnit").create())
                .addColumn(ActionDSColumn.with().name("print").icon(CommonDefines.ICON_PRINT).listener("onClickPrintExtract").permissionKey("ui:secModel.printExtract").create())
                .addColumn(ActionDSColumn.with().name("exclude").icon(CommonDefines.ICON_DELETE).listener("onClickDeleteExtract").alert(FormattedMessage.with().template("enrollmentExtractDS.exclude.alert").parameter(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio().s()).create()).permissionKey("ui:secModel.deleteExtract").disabled(EcOrderEnrollmentExtractDSHandler.NO_EXCLUDE).create())
                .addColumn(ActionDSColumn.with().name("annul").icon(CommonDefines.ICON_DELETE).listener("onClickAnnulExtract").alert(FormattedMessage.with().template("enrollmentExtractDS.annul.alert").parameter(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio().s()).create()).permissionKey("ui:secModel.annulExtract").visible("ui:canAnnul").create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentExtractDSHandler()
    {
        return new EcOrderEnrollmentExtractDSHandler(getName());
    }
}
