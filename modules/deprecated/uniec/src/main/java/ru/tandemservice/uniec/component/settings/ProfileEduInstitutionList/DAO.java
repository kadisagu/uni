/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProfileEduInstitutionList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.fias.base.bo.util.SettlementAutocompleteModel;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressItem;

import org.tandemframework.shared.person.base.bo.PersonSetting.util.EduInstitutionKindAutocompleteModel;
import org.tandemframework.shared.person.base.bo.PersonSetting.util.EduInstitutionTypeAutocompleteModel;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author vip_delete
 * @since 04.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderRegistry = new OrderDescriptionRegistry("ci");

    static
    {
        _orderRegistry.setOrders(EduInstitution.SETTLEMENT_TITLE_WITH_TYPE, new OrderDescription("ci", new String[]{EduInstitution.L_ADDRESS, Address.L_SETTLEMENT, AddressItem.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        model.setCountriesList(new CountryAutocompleteModel());
        model.setSettlementsList(new SettlementAutocompleteModel(model));
        model.setEduInstitutionTypesList(new EduInstitutionTypeAutocompleteModel(null));
        model.setEduInstitutionKindsList(new EduInstitutionKindAutocompleteModel(model, null));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EduInstitution.ENTITY_CLASS, "ci");
        builder.add(MQExpression.eq("ci", EduInstitution.P_PROFILE, Boolean.TRUE));

        if (null != model.getEduInstitutionType() && null == model.getEduInstitutionKind())
        {
            AbstractExpression expr1 = MQExpression.isNotNull("ci", EduInstitution.L_EDU_INSTITUTION_KIND + "." + EducationalInstitutionTypeKind.L_PARENT);
            AbstractExpression expr2 = MQExpression.eq("ci", EduInstitution.L_EDU_INSTITUTION_KIND + "." + EducationalInstitutionTypeKind.L_PARENT, model.getEduInstitutionType());
            AbstractExpression expr3 = MQExpression.eq("ci", EduInstitution.L_EDU_INSTITUTION_KIND, model.getEduInstitutionType());
            builder.add(MQExpression.or(MQExpression.and(expr1, expr2), expr3));
        }

        if (null != model.getEduInstitutionKind())
            builder.add(MQExpression.eq("ci", EduInstitution.L_EDU_INSTITUTION_KIND, model.getEduInstitutionKind()));

        if (null != model.getAddressCountry())
            builder.add(MQExpression.eq("ci", EduInstitution.L_ADDRESS + "." + Address.L_COUNTRY, model.getAddressCountry()));

        if (null != model.getAddressItem())
            builder.add(MQExpression.eq("ci", EduInstitution.L_ADDRESS + "." + Address.L_SETTLEMENT, model.getAddressItem()));

        _orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        EduInstitution eduInstitution = getNotNull(EduInstitution.class, (Long) component.getListenerParameter());
        getSession().refresh(eduInstitution);
        eduInstitution.setProfile(false);
        update(eduInstitution);
    }
}
