/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantAbsenceNote;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantMarkUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        final Session session = getSession();
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setFormingForEntrant(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT.equals(model.getEntrant().getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode()));
        model.setEntrantRequestList(getList(EntrantRequest.class, EntrantRequest.L_ENTRANT, model.getEntrant(), EntrantRequest.P_REG_DATE));
        model.setHasUnusedOlympiadDiploma(false);

        List<OlympiadDiploma> diplomaList = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "e")
        .column(property(Discipline2OlympiadDiplomaRelation.diploma().fromAlias("e")))
        .predicate(DQLPredicateType.distinct)
        .where(eq(property(Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("e")), DQLExpressions.value(model.getEntrant())))
        .createStatement(new DQLExecutionContext(session)).list();

        if (!diplomaList.isEmpty())
        {
            Number chosenNumber = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "e")
            .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().entrant().fromAlias("e")), DQLExpressions.value(model.getEntrant())))
            .createCountStatement(new DQLExecutionContext(session)).uniqueResult();

            if (chosenNumber != null && chosenNumber.intValue() > 0)
            {
                MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RequestedEnrollmentDirection.entrantRequest().entrant(), model.getEntrant()));

                EntrantDataUtil dataUtil = new EntrantDataUtil(session, model.getEntrant().getEnrollmentCampaign(), directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

                Set<OlympiadDiploma> usedDiplomaSet = new HashSet<>();
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                {
                    for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                    {
                        Set<OlympiadDiploma> set = dataUtil.getMarkStatistic(chosen).getDiplomaSet();
                        if (set != null)
                            usedDiplomaSet.addAll(set);
                    }
                }

                // 1. факт: usedDiplomaSet является подмножеством diplomaList
                // 2. факт: в обоих множествах все элементы различны
                // 3. следовательно: множества равны <=> их размеры совпадают
                // 4. следовательно: если размеры разные, то существуют не используемые дипломы
                model.setHasUnusedOlympiadDiploma(diplomaList.size() != usedDiplomaSet.size());
            }
        }

        // заполняем селект для блока с направлениями приема по собеседованию
        List<IIdentifiableWrapper> passOrNotList = new ArrayList<>();
        IdentifiableWrapper passInterview = new IdentifiableWrapper(Model.PASS_INTERVIEW_CODE, "Пройдено");
        IdentifiableWrapper failInterview = new IdentifiableWrapper(Model.FAIL_INTERVIEW_CODE, "Не пройдено");
        passOrNotList.add(passInterview);
        passOrNotList.add(failInterview);
        model.setPassOrNotList(passOrNotList);

        // загружаем все выбранные вступительные испытания в нужном порядке
        // сортировка по дате добавления заявления, потом по приоритету выбранного направления приема, потом по названию
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        builder.addOrder("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE);
        builder.addOrder("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.P_PRIORITY);
        List<ChosenEntranceDiscipline> list = builder.getResultList(session);
        Collections.sort(list, new EntityComparator<>(new EntityOrder(ChosenEntranceDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE + "." + Discipline2RealizationWayRelation.P_TITLE)));

        // заполняем map результатов собеседований по всем выбранным направлениям приема
        MQBuilder interviewBuilder = new MQBuilder(InterviewResult.ENTITY_CLASS, "i");
        interviewBuilder.add(MQExpression.eq("i", InterviewResult.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        List<InterviewResult> interviewResultList = interviewBuilder.getResultList(session);
        for (InterviewResult interviewResult : interviewResultList)
            model.getInterviewBlockColumnMap().put(interviewResult.getRequestedEnrollmentDirection().getId(), interviewResult.isPassed() ? passInterview : failInterview);

        // определяем будет ли виден блок "Сумма баллов по выбранным направлениям подготовки (специальностям)"
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_INTERVIEW));
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        List<RequestedEnrollmentDirection> directionList = directionBuilder.getResultList(session);
        model.setHasDirectionList(directionList.size() > 0);
        for (RequestedEnrollmentDirection direction : directionList)
            model.getHasDirectionSet().add(direction.getEntrantRequest());

        // определяем будет ли виден блок "Результаты собеседования"
        MQBuilder directionInterviewBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionInterviewBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_INTERVIEW));
        directionInterviewBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        List<RequestedEnrollmentDirection> directionInterviewList = directionInterviewBuilder.getResultList(session);
        model.setHasDirectionInterviewList(directionInterviewList.size() > 0);
        for (RequestedEnrollmentDirection direction : directionInterviewList)
            model.getHasDirectionInterviewSet().add(direction.getEntrantRequest());

        // определяем будет ли виден блок "Оценки по дисциплинам"
        MQBuilder disciplineBuilder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "c");
        disciplineBuilder.add(MQExpression.eq("c", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        disciplineBuilder.setNeedDistinct(true);
        List<ChosenEntranceDiscipline> disciplineList = disciplineBuilder.getResultList(session);
        model.setHasMarkList(disciplineList.size() > 0);
        for (ChosenEntranceDiscipline discipline : disciplineList)
            model.getHasMarkSet().add(discipline.getChosenEnrollmentDirection().getEntrantRequest());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareRequestedEnrollmentDirectionDataSource(Model model)
    {
        final Session session = getSession();
        final EnrollmentCampaign enrollmentCampaign = model.getEntrant().getEnrollmentCampaign();

        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_INTERVIEW));
        builder.add(model.getEntrantRequest() != null ?
            MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest()) :
                MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant())
        );

        EntrantDataUtil dataUtil = new EntrantDataUtil(session, enrollmentCampaign, builder, EntrantDataUtil.DetailLevel.EXAM_PASS);

        List<RequestedEnrollmentDirection> directionList = new ArrayList<>(dataUtil.getDirectionSet());
        Collections.sort(directionList, new Comparator<RequestedEnrollmentDirection>()
            {
            @Override
            public int compare(RequestedEnrollmentDirection o1, RequestedEnrollmentDirection o2)
            {
                int result = o1.getEntrantRequest().getRegDate().compareTo(o2.getEntrantRequest().getRegDate());
                if (result == 0)
                    result = o1.getPriority() - o2.getPriority();
                return result;
            }
            });

        DynamicListDataSource dataSource = model.getDirectionDataSource();
        dataSource.setCountRow(directionList.size());
        UniBaseUtils.createPage(dataSource, directionList);

        // считаем значения в доп. колонках

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) wrapper.getEntity();
            if (direction.isWithoutEntranceDisciplines())
            {
                wrapper.setViewProperty(Model.P_MARK_SUM, "без испытаний");
                wrapper.setViewProperty(Model.P_EXAM_COUNT, "");
                wrapper.setViewProperty(Model.P_PASS_COUNT, "");
            }
            else
            {
                Set<ChosenEntranceDiscipline> chosenList = dataUtil.getChosenEntranceDisciplineSet(direction);

                /* TEST: StringBuilder sb = new StringBuilder(); */

                double markSum = 0.0;
                int passCount = 0;
                for (ChosenEntranceDiscipline chosenEntranceDiscipline : chosenList)
                    if (chosenEntranceDiscipline.getFinalMark() != null)
                    {
                        passCount++;
                        markSum += chosenEntranceDiscipline.getFinalMark();
                        /* TEST: sb.append(", ").append(chosenEntranceDiscipline.getTitle()).append(": ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(chosenEntranceDiscipline.getFinalMark())); */
                    }
                markSum += dataUtil.getIndividualProgressMarkByRequest(direction.getEntrantRequest().getId());

                wrapper.setViewProperty(Model.P_MARK_SUM, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(markSum) /* TEST: + sb.toString() */);
                wrapper.setViewProperty(Model.P_EXAM_COUNT, chosenList.size());
                wrapper.setViewProperty(Model.P_PASS_COUNT, passCount);
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareRequestedEnrollmentDirectionInterviewDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_INTERVIEW));
        if (model.getEntrantRequest() != null)
            builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest()));
        builder.addOrder("e", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE);
        builder.addOrder("e", RequestedEnrollmentDirection.P_PRIORITY);
        List<RequestedEnrollmentDirection> list = builder.getResultList(getSession());

        DynamicListDataSource dataSource = model.getDirectionInterviewDataSource();
        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);
    }

    private static final int BUDGET_INDEX = 0;
    private static final int CONTRACT_INDEX = 1;

    @Override
    public void prepareMarkResultDataSource(Model model)
    {
        final Session session = getSession();
        final EnrollmentCampaign enrollmentCampaign = model.getEntrant().getEnrollmentCampaign();
        final boolean compensationTypeDiff = enrollmentCampaign.isEnrollmentPerCompTypeDiff();
        final Set<MultiKey> realizationFormSet = UniecDAOFacade.getEntrantDAO().getRealizationFormSet(enrollmentCampaign);
        final CompensationType compensationTypeBudget = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        final CompensationType compensationTypeContract = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.add(model.getEntrantRequest() != null ?
            MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest()) :
                MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant())
        );

        EntrantDataUtil dataUtil = new EntrantDataUtil(session, enrollmentCampaign, directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        // собираем статистику по всем дисциплинам приемной кампании
        MarkStatisticCollector.CollectScope collectScope = model.isFormingForEntrant() ? MarkStatisticCollector.CollectScope.ENTRANT : MarkStatisticCollector.CollectScope.ENTRANT_REQUEST;
        MarkStatisticCollector collector = new MarkStatisticCollector(collectScope, compensationTypeDiff, dataUtil);

        // создаем список
        List<DisciplineMarkWrapper> result = new ArrayList<>();
        for (Discipline2RealizationWayRelation discipline : collector.getDisciplineSet())
        {
            // значения
            String stateExamMark = null;
            String stateExamScaledMark = null;
            String olympiadMark = null;

            String stateExamInternalMark = null;
            String stateExamInternalMarkAppeal = null;
            String examMark = null;
            String examMarkAppeal = null;
            String testMark = null;
            String testMarkAppeal = null;
            String interviewMark = null;
            String interviewMarkAppeal = null;

            // первое число - итоговый баг по бюджету, второе - по контракту
            Double[] finalMark = new Double[]{0.0, 0.0};

            // определяем оценку по ЕГЭ
            if (collector.getStateExamMark(discipline) != null)
            {
                stateExamMark = Integer.toString(collector.getStateExamMark(discipline).getMark());
                if (compensationTypeDiff)
                {
                    String budgetMark = "-";
                    String contractMark = "-";
                    Double budget = collector.getScaledStateExamMarkBudget(discipline);
                    Double contract = collector.getScaledStateExamMarkContract(discipline);
                    if (budget != null)
                    {
                        finalMark[BUDGET_INDEX] = budget;
                        budgetMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[BUDGET_INDEX]);
                    }
                    if (contract != null)
                    {
                        finalMark[CONTRACT_INDEX] = contract;
                        contractMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[CONTRACT_INDEX]);
                    }
                    stateExamScaledMark = budgetMark + "/" + contractMark;
                } else
                {
                    finalMark[BUDGET_INDEX] = collector.getScaledStateExamMark(discipline);
                    stateExamScaledMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[BUDGET_INDEX]);
                }
            }

            // вычисляем зачтение по олимпиаде
            if (collector.getOlympiad(discipline) != null)
            {
                olympiadMark = Integer.toString(collector.getOlympiad(discipline));
                finalMark[BUDGET_INDEX] = (double) discipline.getMaxMark();
                finalMark[CONTRACT_INDEX] = (double) discipline.getMaxMark();
            }

            // внутренние формы сдачи
            for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> entryKey : collector.getInternalMarkMap(discipline).entrySet())
            {
                SubjectPassForm passForm = entryKey.getKey();
                String[] marks = getMarkForForm(finalMark, entryKey.getValue(), passForm, realizationFormSet,
                    compensationTypeDiff, discipline, compensationTypeBudget, compensationTypeContract, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS);
                if (UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL.equals(passForm.getCode()))
                {
                    stateExamInternalMark = marks[DISCIPLINE_INDEX];
                    stateExamInternalMarkAppeal = marks[DISCIPLINE_APPEAL_INDEX];
                } else if (UniecDefines.SUBJECT_PASS_FORM_EXAM.equals(passForm.getCode()))
                {
                    examMark = marks[DISCIPLINE_INDEX];
                    examMarkAppeal = marks[DISCIPLINE_APPEAL_INDEX];
                } else if (UniecDefines.SUBJECT_PASS_FORM_TEST.equals(passForm.getCode()))
                {
                    testMark = marks[DISCIPLINE_INDEX];
                    testMarkAppeal = marks[DISCIPLINE_APPEAL_INDEX];
                } else if (UniecDefines.SUBJECT_PASS_FORM_INTERVIEW.equals(passForm.getCode()))
                {
                    interviewMark = marks[DISCIPLINE_INDEX];
                    interviewMarkAppeal = marks[DISCIPLINE_APPEAL_INDEX];
                }
            }

            // создаем
            String finalMarkValue;
            if (compensationTypeDiff)
                finalMarkValue = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[BUDGET_INDEX]) + "/" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[CONTRACT_INDEX]);
            else
                finalMarkValue = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(finalMark[BUDGET_INDEX]);

            // создаем строку
            DisciplineMarkWrapper wrapper = new DisciplineMarkWrapper(discipline, stateExamMark, stateExamScaledMark, olympiadMark, finalMarkValue);

            wrapper.setStateExamInternalMark(stateExamInternalMark);
            wrapper.setStateExamInternalMarkAppeal(stateExamInternalMarkAppeal);
            wrapper.setExamMark(examMark);
            wrapper.setExamMarkAppeal(examMarkAppeal);
            wrapper.setTestMark(testMark);
            wrapper.setTestMarkAppeal(testMarkAppeal);
            wrapper.setInterviewMark(interviewMark);
            wrapper.setInterviewMarkAppeal(interviewMarkAppeal);
            result.add(wrapper);
        }

        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        DynamicListDataSource<DisciplineMarkWrapper> dataSource = model.getMarkDataSource();
        dataSource.setCountRow(result.size());
        UniBaseUtils.createPage(dataSource, result);
    }

    private static final int DISCIPLINE_INDEX = 0;
    private static final int DISCIPLINE_APPEAL_INDEX = 1;

    private static String[] getMarkForForm(Double[] finalMark,
                                           PairKey<ExamPassMark, ExamPassMarkAppeal> common,
                                           SubjectPassForm passForm,
                                           Set<MultiKey> realizationFormSet,
                                           boolean compensationTypeDiff,
                                           Discipline2RealizationWayRelation discipline,
                                           CompensationType budget,
                                           CompensationType contract,
                                           DoubleFormatter markFormatter)
    {
        String mark = "";
        String markAppeal = null;

        if (common != null)
        {
            ExamPassMark passMark = common.getFirst();

            if (passMark.getEntrantAbsenceNote() != null)
            {
                mark = passMark.getEntrantAbsenceNote().getShortTitle();
            } else
            {
                markAppeal = "";
                double finalMarkValue = passMark.getMark();

                mark = markFormatter.format(finalMarkValue);
                ExamPassMarkAppeal appealMark = common.getSecond();
                if (appealMark != null)
                {
                    finalMarkValue = appealMark.getMark();
                    markAppeal = markFormatter.format(finalMarkValue);
                }
                boolean forBudget = !compensationTypeDiff || realizationFormSet.contains(new MultiKey(discipline, passForm, budget));
                boolean forContract = !compensationTypeDiff || realizationFormSet.contains(new MultiKey(discipline, passForm, contract));
                if (forBudget)
                    finalMark[BUDGET_INDEX] = Math.max(finalMark[BUDGET_INDEX], finalMarkValue);
                if (forContract)
                    finalMark[CONTRACT_INDEX] = Math.max(finalMark[CONTRACT_INDEX], finalMarkValue);
            }
        }
        return new String[]{mark, markAppeal};
    }

    @Override
    public void updateEntrantRequestData(Model model, Long entrantRequestId)
    {
        // В этом методе надо всегда брать новое состояние из базы данных (никаких кешей в модели не должно быть)

        EntrantRequest entrantRequest = null;
        if (entrantRequestId != null)
            entrantRequest = getNotNull(EntrantRequest.class, entrantRequestId);

        // сохраняем результаты собеседования в выбранной ведомости (entrantRequest = null, если экзам листы форм. на абитуриента)
        syncInterviewResults(model, entrantRequest);

        // сохраняем оценки в выбранной ведомости (entrantRequest = null, если экзам листы форм. на абитуриента)
        syncExamPassMarks(model, entrantRequest);
    }

    private void syncInterviewResults(Model model, EntrantRequest entrantRequest)
    {
        final Session session = getSession();

        // берем текущее состояние базы
        Map<RequestedEnrollmentDirection, InterviewResult> interviewResultMap = new HashMap<>();
        MQBuilder interviewBuilder = new MQBuilder(InterviewResult.ENTITY_CLASS, "i");
        interviewBuilder.add(MQExpression.eq("i", InterviewResult.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        List<InterviewResult> interviewResultList = interviewBuilder.getResultList(session);

        // сохраняем текущее состояние базы в удобную структуру по выбранным направлениям приема
        for (InterviewResult interviewResult : interviewResultList)
            interviewResultMap.put(interviewResult.getRequestedEnrollmentDirection(), interviewResult);

        // в model.getInterviewBlockColumnMap находятся все выбранные направления приема со страницы оценок по виду конкурса собеседование
        for (Map.Entry<Long, IIdentifiableWrapper> entry : model.getInterviewBlockColumnMap().entrySet())
        {
            RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, entry.getKey());
            // анализируем только выбранные направления приема указанного заявления (или все, если заявление только одно)
            if (entrantRequest == null || direction.getEntrantRequest().equals(entrantRequest))
            {
                IIdentifiableWrapper value = entry.getValue();
                InterviewResult interviewResult = interviewResultMap.get(direction);

                // код стандартной синхронизации:
                // если выбрано пусто - удаляем InterviewResult
                // если выбрано что-то, то либо обновляем существующий объект, либо создаем новый
                if (value == null)
                {
                    if (interviewResult != null)
                        session.delete(interviewResult);
                } else
                {
                    if (interviewResult != null)
                    {
                        interviewResult.setPassed(Model.PASS_INTERVIEW_CODE == value.getId());
                        session.update(interviewResult);
                    } else
                    {
                        interviewResult = new InterviewResult();
                        interviewResult.setRequestedEnrollmentDirection(direction);
                        interviewResult.setPassed(Model.PASS_INTERVIEW_CODE == value.getId());
                        session.save(interviewResult);
                    }
                }
            }
        }
    }

    private void syncExamPassMarks(Model model, EntrantRequest entrantRequest)
    {
        List<DisciplineMarkWrapper> list;
        if (entrantRequest == null)
            list = model.getMarkResultDataSource().getEntityList();
        else
            list = model.getMarkResultDataSourceMap().get(entrantRequest).getEntityList();

        List<EntrantAbsenceNote> entrantAbsenceNoteList = getCatalogItemList(EntrantAbsenceNote.class);
        for (DisciplineMarkWrapper wrapper : list)
        {
            Discipline2RealizationWayRelation discipline = getNotNull(Discipline2RealizationWayRelation.class, wrapper.getId());
            String stateExamInternalMark = wrapper.getStateExamInternalMark();
            String stateExamInternalMarkAppeal = wrapper.getStateExamInternalMarkAppeal();
            String examMark = wrapper.getExamMark();
            String examMarkAppeal = wrapper.getExamMarkAppeal();
            String testMark = wrapper.getTestMark();
            String testMarkAppeal = wrapper.getTestMarkAppeal();
            String interviewMark = wrapper.getInterviewMark();
            String interviewMarkAppeal = wrapper.getInterviewMarkAppeal();

            // небольшие тримы строк для простоты анализа
            if (stateExamInternalMark != null) stateExamInternalMark = stateExamInternalMark.trim();
            if (stateExamInternalMarkAppeal != null) stateExamInternalMarkAppeal = stateExamInternalMarkAppeal.trim();
            if (examMark != null) examMark = examMark.trim();
            if (examMarkAppeal != null) examMarkAppeal = examMarkAppeal.trim();
            if (testMark != null) testMark = testMark.trim();
            if (testMarkAppeal != null) testMarkAppeal = testMarkAppeal.trim();
            if (interviewMark != null) interviewMark = interviewMark.trim();
            if (interviewMarkAppeal != null) interviewMarkAppeal = interviewMarkAppeal.trim();

            // на что создан экзам. лист
            Long ientrantId = entrantRequest != null ? entrantRequest.getId() : model.getEntrant().getId();

            // запускаем синхронизацию оценок для дисциплины по форме сдачи ЕГЭ (вуз)
            syncMark(stateExamInternalMark, stateExamInternalMarkAppeal, discipline, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL, entrantAbsenceNoteList, ientrantId);

            // запускаем синхронизацию оценок для дисциплины по форме сдачи экзамена
            syncMark(examMark, examMarkAppeal, discipline, UniecDefines.SUBJECT_PASS_FORM_EXAM, entrantAbsenceNoteList, ientrantId);

            // запускаем синхронизацию оценок для дисциплины по форме сдачи тестирование
            syncMark(testMark, testMarkAppeal, discipline, UniecDefines.SUBJECT_PASS_FORM_TEST, entrantAbsenceNoteList, ientrantId);

            // запускаем синхронизацию оценок для дисциплины по форме сдачи собеседование
            syncMark(interviewMark, interviewMarkAppeal, discipline, UniecDefines.SUBJECT_PASS_FORM_INTERVIEW, entrantAbsenceNoteList, ientrantId);
        }
    }

    private void syncMark(String mark, String markAppeal, Discipline2RealizationWayRelation discipline, String passFormCode, List<EntrantAbsenceNote> entrantAbsenceNoteList, Long ientrantId)
    {
        final Session session = getSession();

        MQBuilder examBuilder = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d");
        examBuilder.add(MQExpression.eq("d", ExamPassDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE, discipline));
        examBuilder.add(MQExpression.eq("d", ExamPassDiscipline.L_SUBJECT_PASS_FORM + "." + SubjectPassForm.P_CODE, passFormCode));
        examBuilder.add(MQExpression.eq("d", ExamPassDiscipline.L_ENTRANT_EXAM_LIST + "." + EntrantExamList.L_ENTRANT + ".id", ientrantId));
        ExamPassDiscipline examPassDiscipline = (ExamPassDiscipline) examBuilder.uniqueResult(getSession());

        // нет дисциплины для сдачи - ничего не делаем
        if (examPassDiscipline == null) return;

        // получаем оценку по дисциплине для сдачи + апелляцию, если она есть
        ExamPassMark examPassMark = get(ExamPassMark.class, ExamPassMark.L_EXAM_PASS_DISCIPLINE, examPassDiscipline);
        ExamPassMarkAppeal examPassMarkAppeal = null;
        if (examPassMark != null)
            examPassMarkAppeal = get(ExamPassMarkAppeal.class, ExamPassMarkAppeal.L_EXAM_PASS_MARK, examPassMark);

        // если на форме поставили пусто, то удаляем оценку для сдачи (если была апелляция по ней, то она тоже удалится)
        if (StringUtils.isEmpty(mark))
        {
            if (examPassMarkAppeal != null)
                session.delete(examPassMarkAppeal);
            if (examPassMark != null)
                session.delete(examPassMark);
        } else
        {
            if (examPassMark == null)
            {
                examPassMark = new ExamPassMark();
                examPassMark.setExamPassDiscipline(examPassDiscipline);
            }
            EntrantMarkUtil.parseAndSetMark(examPassMark, entrantAbsenceNoteList, mark);
            session.saveOrUpdate(examPassMark);

            // проставление апелляции
            if (StringUtils.isEmpty(markAppeal))
            {
                if (examPassMarkAppeal != null)
                    session.delete(examPassMarkAppeal);
            } else
            {
                if (examPassMarkAppeal == null)
                {
                    examPassMarkAppeal = new ExamPassMarkAppeal();
                    examPassMarkAppeal.setExamPassMark(examPassMark);
                }
                try
                {
                    examPassMarkAppeal.setMark(Double.parseDouble(markAppeal));
                } catch (NumberFormatException e)
                {
                    throw new ApplicationException("Неправильный формат одной из оценок по апелляции: " + markAppeal+".");
                }
                session.saveOrUpdate(examPassMarkAppeal);
            }
        }
    }
}
