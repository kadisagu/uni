/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcExamGroup.ui.SpecificationTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcExamGroup.EcExamGroupManager;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification;

/**
 * @author Alexander Shaburov
 * @since 09.07.12
 */
@Configuration
public class EcExamGroupSpecificationTab extends BusinessComponentManager
{
    //dataSource
    final public static String EXAM_GROUP_SPEC_DS = "examGroupSpecDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EXAM_GROUP_SPEC_DS, columnListExtPoint(), EcExamGroupManager.instance().examGroupSpecDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListExtPoint()
    {
        return columnListExtPointBuilder(EXAM_GROUP_SPEC_DS)
                .addColumn(textColumn("discipline", ExamGroupSpecification.discipline().educationSubject().title()))
                .addColumn(blockColumn("startDisciplineDate"))
                .addColumn(blockColumn("finishDisciplineDate"))
                .addColumn(blockColumn("address"))
                .addColumn(blockColumn("classroom"))
                .addColumn(blockColumn("examCommissionMembership"))
                .create();
    }
}
