/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;

/**
 * @author oleyba
 * @since 21.07.2009
 */
public class EntrantRatingSummaryReportBuilder
{
    private Model _model;
    private Session _session;
    private boolean useIndProgress;

    public EntrantRatingSummaryReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
        useIndProgress = model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark();
    }

    public DatabaseFile getContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_RATING_SUMMARY);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());

        if (_model.getReport().isByExamSet())
            createReportByExamSet(template, result);
        else
            createReportByExamCount(template, result);

        byte[] data = RtfUtil.toByteArray(result);
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("deprecation")
    private void createReportByExamCount(RtfDocument template, RtfDocument result)
    {
        EntrantDataUtil entrantDataUtil = new EntrantDataUtil(_session, _model.getReport().getEnrollmentCampaign(), getBuilder());
        Map<Integer, List<EntrantWrapper>> tableMap = getTableMapByExamCount(entrantDataUtil);

        List<Integer> examCountList = new ArrayList<>(tableMap.keySet());
        Collections.sort(examCountList, Collections.reverseOrder());
        for (final Integer examCount : examCountList)
        {
            RtfDocument document = template.getClone();

            String headerTitle = "Рейтинг абитуриентов на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()) + "\\par "
                    + "Количество вступительных испытаний: " + String.valueOf(examCount) + "\\par "
                    + (_model.getReport().getQualificationTitle() == null ? "" : _model.getReport().getQualificationTitle() + "\\par ")
                    + _model.getReport().getDevelopForm().getTitle() + " форма обучения" + (_model.getReport().getDevelopConditionTitle() == null ? "" : " (" + _model.getReport().getDevelopConditionTitle() + ")") + "                   " + (_model.getReport().getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения");

            IRtfText rtfText = RtfBean.getElementFactory().createRtfText(headerTitle);
            rtfText.setRaw(true);
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("PARAM", Arrays.asList((IRtfElement) rtfText));
            injectModifier.modify(document);

            RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
            RtfRow header = table.getRowList().get(0);
            if  (!useIndProgress)
                UniRtfUtil.deleteCell(header, 6, 5);

            List<String[]> tableData = getTableContent(entrantDataUtil, tableMap.get(examCount), examCount);

            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("T", tableData.toArray(new String[tableData.size()][]));
            modifier.put("T", new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    if (!useIndProgress)
                        UniRtfUtil.deleteCell(table.getRowList().get(currentRowIndex), 6, 5);

                    int[] parts = new int[examCount];
                    Arrays.fill(parts, 1);

                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 5, (newCell, index) -> {
                        String caption = String.valueOf(index + 1) + "-е исп.";
                        newCell.setElementList(Arrays.asList((IRtfElement) RtfBean.getElementFactory().createRtfText(caption)));
                    }, parts);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 5, null, parts);
                }
            });
            modifier.modify(document);

            result.getElementList().addAll(document.getElementList());
        }
    }

    @SuppressWarnings("deprecation")
    private void createReportByExamSet(RtfDocument template, RtfDocument result)
    {
        EntrantDataUtil entrantDataUtil = new EntrantDataUtil(_session, _model.getReport().getEnrollmentCampaign(), getBuilder());
        Map<ExamSet, List<EntrantWrapper>> tableMap = getTableMapByExamSet(entrantDataUtil);

        //List<ExamSet> examCountList = new ArrayList<ExamSet>(tableMap.keySet());
        //Collections.sort(examCountList, Collections.reverseOrder());
        // todo сортировка таблиц
        for (final ExamSet examSet : tableMap.keySet())
        {
            RtfDocument document = template.getClone();

            String headerTitle = "Рейтинг абитуриентов на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()) + "\\par "
                    + "Набор вступительных испытаний: " + getCaption(examSet) + "\\par "
                    + (_model.getReport().getQualificationTitle() == null ? "" : _model.getReport().getQualificationTitle() + "\\par ")
                    + _model.getReport().getDevelopForm().getTitle() + " форма обучения" + (_model.getReport().getDevelopConditionTitle() == null ? "" : " (" + _model.getReport().getDevelopConditionTitle() + ")") + "                   " + (_model.getReport().getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения");

            IRtfText rtfText = RtfBean.getElementFactory().createRtfText(headerTitle);
            rtfText.setRaw(true);
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("PARAM", Arrays.asList((IRtfElement) rtfText));
            injectModifier.modify(document);

            RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
            RtfRow header = table.getRowList().get(0);
            if  (!useIndProgress)
                UniRtfUtil.deleteCell(header, 6, 5);


            final int examCount = examSet.getSetItemList().size();
            List<String[]> tableData = getTableContent(entrantDataUtil, tableMap.get(examSet), examCount);

            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("T", tableData.toArray(new String[tableData.size()][]));
            modifier.put("T", new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    if (!useIndProgress)
                        UniRtfUtil.deleteCell(table.getRowList().get(currentRowIndex), 6, 5);

                    int[] parts = new int[examCount];
                    Arrays.fill(parts, 1);

                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 5, (newCell, index) -> {
                        String caption = String.valueOf(index + 1) + "-е исп.";
                        newCell.setElementList(Arrays.asList((IRtfElement) RtfBean.getElementFactory().createRtfText(caption)));
                    }, parts);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 5, null, parts);
                }
            });
            modifier.modify(document);

            result.getElementList().addAll(document.getElementList());
        }
    }

    private String getCaption(ExamSet examSet)
    {
        List<ExamSetItem> setItemList = examSet.getSetItemList();

        StringBuilder result = new StringBuilder();
        int counter = 1;
        for (ExamSetItem item : setItemList)
        {
            result.append(counter++).append(". ");
            result.append(StringUtils.capitalize(item.getSubject().getTitle()));
            if (!item.getChoice().isEmpty())
            {
                List<String> titleList = new ArrayList<>();
                for (SetDiscipline discipline : item.getChoice())
                    titleList.add(StringUtils.capitalize(discipline.getTitle()));
                result.append(", ").append(StringUtils.join(titleList.iterator(), ", "));
            }
            result.append(" ");
        }
        return result.toString();
    }

    private List<String[]> getTableContent(EntrantDataUtil entrantDataUtil, List<EntrantWrapper> entrantWrapperList, Integer examCount)
    {
        Collections.sort(entrantWrapperList, new RequestedEnrollmentDirectionComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getReport().getEnrollmentCampaign())));
        List<String[]> tableData = new ArrayList<>();
        int counter = 1;
        Set<Long> usedEntrantIds = new HashSet<>();
        Map<Person, Set<PersonBenefit>> personBenefits = EntrantDataUtil.getPersonBenefitMap(_session, getBuilder());
        for (EntrantWrapper wrapper : entrantWrapperList)
        {
            Entrant entrant = wrapper.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
            if (usedEntrantIds.contains(entrant.getId()))
                continue;
            usedEntrantIds.add(entrant.getId());
            tableData.add(print(wrapper, entrantDataUtil, personBenefits.get(entrant.getPerson()), counter++, examCount));
        }
        return tableData;
    }

    private Map<Integer, List<EntrantWrapper>> getTableMapByExamCount(EntrantDataUtil entrantDataUtil)
    {
        EnrollmentCampaign enrollmentCampaign = _model.getReport().getEnrollmentCampaign();
        Map<MultiKey, Integer> examCountForCategoryAndDirection = new HashMap<>();
        Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();
        for (ExamSet examSet : examSetList)
        {
            for (EnrollmentDirection direction : examSet.getList())
            {
                MultiKey key = new MultiKey(examSet.getStudentCategory().getCode(), direction);
                examCountForCategoryAndDirection.put(key, examSet.getSetItemList().size());
            }
        }
        Map<Integer, List<EntrantWrapper>> tableMap = new HashMap<>();
        for (RequestedEnrollmentDirection direction : getBuilder().<RequestedEnrollmentDirection>getResultList(_session))
        {
            Integer examCount = examCountForCategoryAndDirection.get(new MultiKey(enrollmentCampaign.isExamSetDiff() ? direction.getStudentCategory().getCode() : UniDefines.STUDENT_CATEGORY_STUDENT, direction.getEnrollmentDirection()));
            if (examCount != null && examCount > 0)
            {
                List<EntrantWrapper> directionList = tableMap.get(examCount);
                if (directionList == null)
                    tableMap.put(examCount, directionList = new ArrayList<>());
                directionList.add(new EntrantWrapper(direction, entrantDataUtil));
            }
        }
        return tableMap;
    }

    private Map<ExamSet, List<EntrantWrapper>> getTableMapByExamSet(EntrantDataUtil entrantDataUtil)
    {
        EnrollmentCampaign enrollmentCampaign = _model.getReport().getEnrollmentCampaign();
        Map<MultiKey, ExamSet> examCountForCategoryAndDirection = new HashMap<>();
        Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();
        for (ExamSet examSet : examSetList)
        {
            for (EnrollmentDirection direction : examSet.getList())
            {
                MultiKey key = new MultiKey(examSet.getStudentCategory().getCode(), direction);
                examCountForCategoryAndDirection.put(key, examSet);
            }
        }
        Map<ExamSet, List<EntrantWrapper>> tableMap = new HashMap<>();
        for (RequestedEnrollmentDirection direction : getBuilder().<RequestedEnrollmentDirection>getResultList(_session))
        {
            ExamSet examSet = examCountForCategoryAndDirection.get(new MultiKey(enrollmentCampaign.isExamSetDiff() ? direction.getStudentCategory().getCode() : UniDefines.STUDENT_CATEGORY_STUDENT, direction.getEnrollmentDirection()));
            if (examSet != null)
            {
                List<EntrantWrapper> directionList = tableMap.get(examSet);
                if (directionList == null)
                    tableMap.put(examSet, directionList = new ArrayList<>());
                directionList.add(new EntrantWrapper(direction, entrantDataUtil));
            }
        }
        return tableMap;
    }

    private MQBuilder getBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.addJoin("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "enrollmentDirection");
        builder.addJoin("enrollmentDirection", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        builder.addLeftJoinFetch("red", RequestedEnrollmentDirection.L_PROFILE_CHOSEN_ENTRANCE_DISCIPLINE, "profileDiscipline");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
        builder.addJoinFetch("entrantRequest", EntrantRequest.L_ENTRANT, "entrant");
        builder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        builder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");

        // параметры отчета
        builder.add(MQExpression.eq("enrollmentDirection", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if (_model.isStudentCategoryActive())
        {
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        }
        if (_model.isQualificationActive())
        {
            builder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        }
        builder.add(MQExpression.eq("educationOrgUnit", EducationOrgUnit.L_DEVELOP_FORM, _model.getReport().getDevelopForm()));
        if (_model.isDevelopConditionActive())
        {
            builder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        }
        if (_model.isFormativeOrgUnitActive())
        {
            builder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        }
        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("educationOrgUnit", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                    MQExpression.in("educationOrgUnit", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList())
            );
        if (_model.getReport().isOnlyWithOriginals())
            builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
        // не включать активных, выбывших из конкурса, забравших документы
        builder.add(MQExpression.notIn("red", RequestedEnrollmentDirection.L_STATE + ".code", UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        // не архивные
        builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, Boolean.FALSE));

        if (_model.getCustomStateRuleValue() != null)
        {
            AbstractExpression expression4EntrantCustomState = EntrantCustomState.getExistsExpression4EntrantCustomState(
                    "red", RequestedEnrollmentDirection.entrantRequest().entrant().s(),
                    _model.getCustomStateRuleValue(), _model.getEntrantCustomStateList(), new Date());
            if (expression4EntrantCustomState != null)
                builder.add(expression4EntrantCustomState);
        }

        return builder;
    }

    public static class EntrantWrapper implements IReportRow
    {
        private RequestedEnrollmentDirection requestedEnrollmentDirection;
        private Double finalMark;
        private Double profileMark;
        private int indProgressMark;
        private Double averageEduInstMark;

        public EntrantWrapper(RequestedEnrollmentDirection requestedEnrollmentDirection, EntrantDataUtil entrantDataUtil)
        {
            this.requestedEnrollmentDirection = requestedEnrollmentDirection;
            this.indProgressMark = entrantDataUtil.getIndividualProgressMarkByRequest(requestedEnrollmentDirection.getEntrantRequest().getId());
            this.finalMark = entrantDataUtil.getFinalMark(requestedEnrollmentDirection) + this.indProgressMark;
            this.profileMark = (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
            averageEduInstMark = entrantDataUtil.getAverageEduInstMark(requestedEnrollmentDirection.getEntrantRequest().getEntrant().getId());
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirection;
        }

        @Override
        public Double getFinalMark()
        {
            return finalMark;
        }
        
        @Override
        public Double getProfileMark()
        {
            return profileMark;
        }

        @Override
        public String getFio()
        {
            return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE; // мы не грузим эти данные, так что по ним не сравниваем
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return averageEduInstMark;
        }

        public int getIndProgressMark()
        {
            return indProgressMark;
        }
    }

    protected String[] print(EntrantWrapper wrapper, EntrantDataUtil entrantDataUtil, Set<PersonBenefit> personBenefits, int number, int examsCount)
    {
        RequestedEnrollmentDirection requestedEnrollmentDirection = wrapper.getRequestedEnrollmentDirection();
        List<String> row = new ArrayList<>();
        row.add(String.valueOf(number));
        row.add(getRegNumber(requestedEnrollmentDirection));
        row.add(requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPerson().getFullFio());
        row.add(requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "Оригинал" : "Копия");
        row.add(getCategory(requestedEnrollmentDirection, personBenefits));
        List<Double> marks = new ArrayList<>(entrantDataUtil.getMarkMap(requestedEnrollmentDirection).values());
        Collections.sort(marks, Collections.reverseOrder());
        for (Double mark : marks)
            row.add(mark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(mark));
        for (int i = marks.size(); i < examsCount; i++)
            row.add("");

        if (useIndProgress)
            row.add(String.valueOf(wrapper.getIndProgressMark()));
        row.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(wrapper.getFinalMark()));
        return row.toArray(new String[row.size()]);
    }

    protected String getCategory(RequestedEnrollmentDirection requestedEnrollmentDirection, Set<PersonBenefit> personBenefits)
    {
        String result;
        CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
        if (!requestedEnrollmentDirection.isTargetAdmission())
        {
            result = competitionKind.getTitle();
        } else
        {
            result = "Целевой прием";

            if (!competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
            {
                result += ", " + competitionKind.getShortTitle();
            }
        }
        return result;
    }

    protected String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getEntrantRequest().getStringNumber();
    }
}
