/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public class EcgpDistributionDTO extends IdentifiableWrapper
{
    private static final long serialVersionUID = 1L;

    public static final String ECG_ITEM = "ecgItem";
    public static final String COMPENSATION_TYPE = "compensationType";
    public static final String TITLE = "title";
    public static final String FORMING_DATE = "formingDate";
    public static final String ADD_DISABLED = "addDisabled";
    public static final String EDIT_DISABLED = "editDisabled";
    public static final String DELETE_DISABLED = "deleteDisabled";

    private Long _persistentId;
    private EnrollmentDirection _ecgItem;
    private CompensationType _compensationType;
    private boolean _secondHighAdmission;
    private Date _formingDate;

    /**
     * Создает DTO на основное распределение
     *
     * @param id           идентификатор
     * @param distribution основное распределение
     */
    public EcgpDistributionDTO(long id, EcgpDistribution distribution)
    {
        super(id, distribution.getTitle());
        _persistentId = distribution.getId();
        _ecgItem = distribution.getConfig().getEcgItem();
        _compensationType = distribution.getConfig().getCompensationType();
        _secondHighAdmission = distribution.getConfig().isSecondHighAdmission();
        _formingDate = distribution.getFormingDate();
    }

    /**
     * Создает заглушку под новое основное распределение
     *
     * @param id                  идентификатор
     * @param ecgItem             объект распределения
     * @param compensationType    вид возмещения затрат
     * @param secondHighAdmission true, если распределение на второе высшее
     */
    public EcgpDistributionDTO(long id, EnrollmentDirection ecgItem, CompensationType compensationType, boolean secondHighAdmission)
    {
        super(id, "");
        _persistentId = null;
        _ecgItem = ecgItem;
        _compensationType = compensationType;
        _secondHighAdmission = secondHighAdmission;
    }

    // ConfigKey

    public EcgpConfigDTO getConfigDTO()
    {
        return new EcgpConfigDTO(_ecgItem, _compensationType, _secondHighAdmission);
    }

    //

    public boolean isAddDisabled()
    {
        return _persistentId != null;
    }

    public boolean isEditDisabled()
    {
        return _persistentId == null;
    }

    public boolean isDeleteDisabled()
    {
        return _persistentId == null;
    }

    // Getters

    public Long getPersistentId()
    {
        return _persistentId;
    }

    public EnrollmentDirection getEcgItem()
    {
        return _ecgItem;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public boolean isSecondHighAdmission()
    {
        return _secondHighAdmission;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }
}
