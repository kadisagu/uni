/* $Id$ */
package ru.tandemservice.uniec.base.entity.ecg;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
public interface IPersistentEcgItem extends IEntity, ITitled
{
    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    EnrollmentCampaign getEnrollmentCampaign();

    @Override
    /**
     * @return Название. Свойство не может быть null.
     */
    String getTitle();
}
