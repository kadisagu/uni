/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.CompetitionGroupAddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;

import java.util.*;

/**
 * @author vip_delete
 * @since 27.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentCampaign.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EnrollmentCampaign.P_USE_COMPETITION_GROUP, Boolean.TRUE));
        builder.addOrder("e", EnrollmentCampaign.P_ID, OrderDirection.desc);

        List<EnrollmentCampaign> list = builder.getResultList(getSession());
        if (model.getEnrollmentCampaign() == null && !list.isEmpty())
        {
            model.setEnrollmentCampaign(list.get(0));
        }
        model.setEnrollmentCampaignList(list);

        if (model.getCompetitionGroup().getId() != null)
        {
            // форма редактирования
            model.setCompetitionGroup(getNotNull(CompetitionGroup.class, model.getCompetitionGroup().getId()));
            model.getSelectedEnrollmentDirectionSet().addAll(ids(getList(EnrollmentDirection.class, EnrollmentDirection.L_COMPETITION_GROUP, model.getCompetitionGroup())));
            model.setDistributed(existsEntity(EcgDistribObject.class, EcgDistribObject.L_COMPETITION_GROUP, model.getCompetitionGroup()));
        }
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareEnrollmentDirectionDataSource(Model model)
    {
        List<EnrollmentDirection> list;
        if (model.getFormativeOrgUnit() == null)
        {
            list = Collections.emptyList();
        } else
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getCompetitionGroup().getEnrollmentCampaign()));
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
            builder.add(MQExpression.isNull("d", EnrollmentDirection.L_COMPETITION_GROUP));
            list = builder.getResultList(getSession());
        }

        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        Map<Long, Boolean> valueMap = new HashMap<Long, Boolean>();
        for (EnrollmentDirection item : list)
        {
            valueMap.put(item.getId(), model.getSelectedEnrollmentDirectionSet().contains(item) || model.getCompetitionGroup().equals(item.getCompetitionGroup()));
        }
        ((BlockColumn<Boolean>) model.getDataSource().getColumn(Model.CHECKBOX_COLUMN)).setValueMap(valueMap);
    }

    @Override
    public void prepareChoosenEnrollmentDirectionDataSource(Model model)
    {
        List<EnrollmentDirection> list = new ArrayList<EnrollmentDirection>();
        for (Long id : model.getSelectedEnrollmentDirectionSet())
            list.add(get(EnrollmentDirection.class, id));
        Collections.sort(list, ITitled.TITLED_COMPARATOR);
        model.getChoosenDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getChoosenDataSource(), list);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void selectEnrollmentDirection(Model model)
    {
        Map<Long, Boolean> valueMap = ((BlockColumn<Boolean>) model.getDataSource().getColumn(Model.CHECKBOX_COLUMN)).getValueMap();
        for (Map.Entry<Long, Boolean> entity : valueMap.entrySet())
        {
            if (entity.getValue())
            {
                model.getSelectedEnrollmentDirectionSet().add(entity.getKey());
            }
        }
        model.setFormativeOrgUnit(null);
        model.setTerritorialOrgUnit(null);
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();
        session.refresh(model.getEnrollmentCampaign());

        if (model.getCompetitionGroup().getId() == null)
        {
            // форма добавления
            validateCompetitionGroup(model);
            session.save(model.getCompetitionGroup());
            for (Long id : model.getSelectedEnrollmentDirectionSet())
            {
                final EnrollmentDirection direction = get(EnrollmentDirection.class, id);
                direction.setCompetitionGroup(model.getCompetitionGroup());
                session.update(direction);
            }
        } else
        {
            // форма редактирования
            validateCompetitionGroup(model);

            List<EnrollmentDirection> used = getList(EnrollmentDirection.class, EnrollmentDirection.L_COMPETITION_GROUP, model.getCompetitionGroup());

            if (existsEntity(EcgDistribObject.class, EcgDistribObject.L_COMPETITION_GROUP, model.getCompetitionGroup())
                    && !CollectionUtils.disjunction(used, model.getSelectedEnrollmentDirectionSet()).isEmpty())
            {
                // есть уже распределение
                throw new ApplicationException(model.getMessageSource().getMessage("competition.group.distributed.exception", null, null));
            }

            // исключаем из конкурсной группы невыбранные
            for (EnrollmentDirection direction : used)
            {
                if (!model.getSelectedEnrollmentDirectionSet().remove(direction.getId()))
                {
                    direction.setCompetitionGroup(null);
                    session.update(direction);
                }
            }

            // добавляем в конкурсную группу выбранные
            for (Long id : model.getSelectedEnrollmentDirectionSet())
            {
                final EnrollmentDirection direction = get(EnrollmentDirection.class, id);
                direction.setCompetitionGroup(model.getCompetitionGroup());
                session.update(direction);
            }
        }
    }

    private void validateCompetitionGroup(Model model)
    {
        if (!model.getEnrollmentCampaign().isUseCompetitionGroup())
        {
            throw new ApplicationException(model.getMessageSource().getMessage("competition.groups.used.exception", null, null));
        }

        if (model.getSelectedEnrollmentDirectionSet().isEmpty())
        {
            throw new ApplicationException(model.getMessageSource().getMessage("competition.group.empty.exception", null, null));
        }

        {
            FlushMode flushMode = getSession().getFlushMode();
            try
            {
                getSession().setFlushMode(FlushMode.MANUAL);

                // проверяем уникальность названия
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompetitionGroup.class, "e")
                        .where(DQLExpressions.eq(DQLExpressions.property(CompetitionGroup.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(model.getEnrollmentCampaign())))
                        .where(DQLExpressions.eq(DQLExpressions.property(CompetitionGroup.title().fromAlias("e")), DQLExpressions.value(model.getCompetitionGroup().getTitle())));
                if (model.getCompetitionGroup().getId() != null)
                    builder.where(DQLExpressions.ne(DQLExpressions.property(CompetitionGroup.id().fromAlias("e")), DQLExpressions.value(model.getCompetitionGroup().getId())));
                Number number = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                if (number != null && number.intValue() > 0)
                {
                    ContextLocal.getErrorCollector().add("Название конкурсной группы должно быть уникальным в рамках приемной кампании", "title");
                    throw new ApplicationException();
                }
            } finally
            {
                getSession().setFlushMode(flushMode);
            }
        }

        final Long dirId = model.getSelectedEnrollmentDirectionSet().iterator().next();
        DevelopForm developForm = get(EnrollmentDirection.class, dirId).getEducationOrgUnit().getDevelopForm();
        for (Long id : model.getSelectedEnrollmentDirectionSet())
        {
            if (!developForm.equals(get(EnrollmentDirection.class, id).getEducationOrgUnit().getDevelopForm()))
                throw new ApplicationException("В одну группу могут входить направления подготовки (специальности) только с одной формой освоения.");
        }

        if (model.getCompetitionGroup().getEnrollmentCampaign().isCompetitionGroupExamSetEqual())
        {
            // запускаем проверку равенстра наборов экзаменов для выбранных направлений приема
            for (ExamSet examSet : UniecDAOFacade.getExamSetDAO().getExamSetMap(model.getEnrollmentCampaign()).values())
            {
                // набор должен либо содержать все выбранные направления приема, либо не содержать ни одного
                int count = 0;
                for (Long id : model.getSelectedEnrollmentDirectionSet())
                {
                    if (examSet.getList().contains(get(EnrollmentDirection.class, id)))
                    {
                        count++;
                    }
                }
                if (count > 0 && count < model.getSelectedEnrollmentDirectionSet().size())
                {
                    throw new ApplicationException("Выбранные направления приема должны содержать одинаковый набор вступительных испытаний.");
                }
            }
        }
    }
}
