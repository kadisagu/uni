/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;

import java.util.List;

/**
 * @author vip_delete
 * @since 11.02.2009
 */
@Input({@Bind(key = "enrollmentCampaignId", binding = "enrollmentCampaignId")})
public class Model
{
    private Long _enrollmentCampaignId;
    private EnrollmentCampaign _enrollmentCampaign;
    private EnrollmentCampaignPeriod _period = new EnrollmentCampaignPeriod();
    private ISelectModel _educationYearList;
    private ISelectModel _developFormList;
    private List<CompensationType> _compensationTypeList;
    private List<EnrollmentCampaignType> _enrollmentCampaignTypes;
    private List<Qualifications> _qualificationList;

    private boolean _maxEnrollmentDirectionDisabled;
    private boolean _maxMinisterialDirectionDisabled;

    // Прием на бюджетные места и по договору
    public static final long ENROLLMENT_PER_COMPENSATION_TYPE_DIFFERENCE = 0;    // Отличается
    public static final long ENROLLMENT_PER_COMPENSATION_TYPE_NO_DIFFERENCE = 1; // Не отличается
    private List<IdentifiableWrapper> _enrollmentPerCompensationTypeList;
    private IdentifiableWrapper _enrollmentPerCompensationType;
    private boolean _enrollmentPerCompensationTypeDisabled;

    // Формирование экзаменационных листов
    public static final long ENTRANT_EXAM_LIST_FORMING_FOR_ENTRANT = 0; // Для абитуриента в целом
    public static final long ENTRANT_EXAM_LIST_FORMING_FOR_REQUEST = 1; // Для каждого заявления абитуриента
    private List<IdentifiableWrapper> _entrantExamListFormingList;
    private IdentifiableWrapper _entrantExamListForming;
    private boolean _entrantExamListFormingDisabled;

    // Конкурсные группы
    public static final long COMPETITION_GROUP_NO_USE = 0;              // Не использовать
    public static final long COMPETITION_GROUP_USE_EXAM_SET_EQUALS = 1; // Использовать, в группу входят только направления с одинаковым набором вступительных испытаний
    //public static final long COMPETITION_GROUP_USE_EXAM_SET_NOT_EQUALS = 2; // Использовать, в группу могут входить направления с разными наборами вступительных испытаний
    private List<IdentifiableWrapper> _competitionGroupList;
    private IdentifiableWrapper _competitionGroup;
    private boolean _competitionGroupDisabled;

    private int _currentDirectionRestrictionIndex;
    private EnrollmentDirectionRestriction _currentDirectionRestriction;
    private List<EnrollmentDirectionRestriction> _directionRestrictionList;

    private int _currentCompetitionGroupRestrictionIndex;
    private CompetitionGroupRestriction _currentCompetitionGroupRestriction;
    private List<CompetitionGroupRestriction> _competitionGroupRestrictionList;

    // Число выбираемых направлений приема по внеконкурсному приему
    public static final long DIRECTION_FOR_OUT_OF_COMPETITION_ONE = 0;     // Одно
    public static final long DIRECTION_FOR_OUT_OF_COMPETITION_MANY = 1;    // Без ограничений
    private List<IdentifiableWrapper> _directionForOutOfCompetitionList;
    private IdentifiableWrapper _directionForOutOfCompetition;

    // Число выбираемых направлений подготовки (специальностей) по целевому приему
    public static final long DIRECTION_FOR_TARGET_ADMISSION_ONE = 0;     // Одно
    public static final long DIRECTION_FOR_TARGET_ADMISSION_MANY = 1;    // Без ограничений
    private List<IdentifiableWrapper> _directionForTargetAdmissionList;
    private IdentifiableWrapper _directionForTargetAdmission;

    // Присваивать регистрационный номер заявления
    public static final long NUMBER_ON_REQUEST_AUTO = 0;   // Автоматически
    public static final long NUMBER_ON_REQUEST_MANUAL = 1; // Вручную
    private List<IdentifiableWrapper> _numberOnRequestList;
    private IdentifiableWrapper _numberOnRequest;
    private boolean _numberOnRequestDisabled;

    // Присваивать регистрационный номер на направлении подготовки (специальности)
    public static final long NUMBER_ON_DIRECTION_AUTO = 0;   // Автоматически
    public static final long NUMBER_ON_DIRECTION_MANUAL = 1; // Вручную
    private List<IdentifiableWrapper> _numberOnDirectionList;
    private IdentifiableWrapper _numberOnDirection;
    private boolean _numberOnDirectionDisabled;

    // Выбор направлений абитуриентом
    public static final long DIRECTION_SELECTION_ONE_COMPENSATION_TYPE = 0;             // Только с одним видом возмещения затрат
    public static final long DIRECTION_SELECTION_MANY_COMPENSATION_TYPE_NO_CONTEST = 1; // С разными видами возмещения затрат, абитуриент не участвует в конкурсе на контракт
    public static final long DIRECTION_SELECTION_MANY_COMPENSATION_TYPE_CONTEST = 2;    // С разными видами возмещения затрат, абитуриент может участвовать в конкурсе на контракт
    private List<IdentifiableWrapper> _directionSelectionList;
    private IdentifiableWrapper _directionSelection;
    private boolean _directionSelectionDisabled;

    // Мастер добавления абитуриента
    public static final long ENTRANT_REGISTRATION_WIZARD_REGULAR = 0; // Обычный мастер добавления
    public static final long ENTRANT_REGISTRATION_WIZARD_SEARCH = 1; // Мастер добавления с предварительным поиском
    public static final long ENTRANT_REGISTRATION_WIZARD_ONLINE = 2; // Мастер добавления с предварительным поиском, в том числе по онлайн-абитуриентам
    private List<IdentifiableWrapper> _entrantRegistrationWizardList;
    private IdentifiableWrapper _entrantRegistrationWizard;
    private boolean _entrantRegistrationWizardDisabled;

    // Наборы вступительных испытаний для категорий поступающих
    public static final long COMPETITION_GROUP_EXAM_SET_EQUALS_FOR_ALL_CATEGORY = 0;    // Одинаковые для всех категорий
    public static final long COMPETITION_GROUP_EXAM_SET_NO_EQUALS_FOR_ALL_CATEGORY = 1; // Разные для разных категорий
    private List<IdentifiableWrapper> _competitionGroupExamSetList;
    private IdentifiableWrapper _competitionGroupExamSet;
    private boolean _competitionGroupExamSetDisabled;

    // Формирование дисциплин для сдачи при наличии свидетельства ЕГЭ
    public static final long PASS_DISCIPLINE_FORMING_NO_FORMING = 0;  // Не формировать дисциплину для сдачи при наличии свидетельства ЕГЭ
    public static final long PASS_DISCIPLINE_FORMING_YES_FORMING = 1; // Всегда формировать дисциплину для сдачи
    private List<IdentifiableWrapper> _passDisciplineFormingList;
    private IdentifiableWrapper _passDisciplineForming;
    private boolean _passDisciplineFormingDisabled;

    // Выбор направлений абитуриентом (по свидетельству ЕГЭ)
    public static final long DIRECTION_SELECTION_BY_STATE_EXAM_NEED_COVERING = 0;    // Направление приема должно покрываться свидетельством ЕГЭ
    public static final long DIRECTION_SELECTION_BY_STATE_EXAM_NO_NEED_COVERING = 1; // Направление приема может не покрываться свидетельством ЕГЭ
    private List<IdentifiableWrapper> _directionSelectionByStateExamList;
    private IdentifiableWrapper _directionSelectionByStateExam;
    private boolean _directionSelectionByStateExamDisabled;

    // Регистрация свидетельств ЕГЭ по умолчанию
    public static final long STATE_EXAM_REGISTRATION_CHECKED = 0;     // Зачтенными
    public static final long STATE_EXAM_REGISTRATION_NO_CHECKED = 1;  // Не зачтенными
    private List<IdentifiableWrapper> _stateExamRegistrationList;
    private IdentifiableWrapper _stateExamRegistration;
    private boolean _stateExamRegistrationDisabled;

    // Номер для свидетельств ЕГЭ 1-ой волны
    public static final long STATE_EXAM_WAVE1_NUMBER_REQUIRED = 0;    // Обязателен
    public static final long STATE_EXAM_WAVE1_NUMBER_NO_REQUIRED = 1; // Необязателен
    private List<IdentifiableWrapper> _stateExamWave1NumberList;
    private IdentifiableWrapper _stateExamWave1Number;
    private boolean _stateExamWave1NumberDisabled;

    // Зачтение диплома олимпиады
    public static final long OLYMPIAD_DIPLOMA_FOR_DIRECTION = 0;     // За дисциплины по одному выбранному направлению приема
    public static final long OLYMPIAD_DIPLOMA_FOR_ALL_DIRECTIONS = 1;// За дисциплины по всем выбранным направлениям приема
    private List<IdentifiableWrapper> _olympiadDiplomaRuleList;
    private IdentifiableWrapper _olympiadDiplomaRule;
    private boolean _olympiadDiplomaRuleDisabled;

    // Учитывать приоритеты вступительных испытаний
    public static final long PRIORITIES_ADMISSION_TESTS_CONSIDER = 0;     // За дисциплины по одному выбранному направлению приема
    public static final long PRIORITIES_ADMISSION_TESTS_IGNORE = 1;// За дисциплины по всем выбранным направлениям приема
    private List<IdentifiableWrapper> _prioritiesAdmissionTestRuleList;
    private IdentifiableWrapper _prioritiesAdmissionTestRule;


    //Учитывать индивидуальные достижения в сумме конкурсных баллов
    public static final long INDIVIDUAL_PROGRESS_CONSIDER_YES = 0; // да
    public static final long INDIVIDUAL_PROGRESS_CONSIDER_NO = 1; // нет
    private List<IdentifiableWrapper> _individualProgressList;
    private IdentifiableWrapper _individualProgressConsider;

    //Используемые кнопки для заявления в карточке абитуриента
    private boolean _usePrintRequestButton;
    private boolean _usePrintExamSheetButton;
    private boolean _usePrintDocumentsInventoryAndReceiptButton;
    private boolean _usePrintAuthCardButton;
    private boolean _usePrintLetterboxButton;
    private boolean _useChangePriorityButton;

    // Формирование экзаменационных групп
    public static final long FORMING_EXAM_GROUP_AUTO = 0;   // Автоматически
    public static final long FORMING_EXAM_GROUP_MANUAL = 1; // Вручную
    private List<IdentifiableWrapper> _formingExamGroupList;
    private IdentifiableWrapper _formingExamGroup;
    private boolean _formingExamGroupDisabled;

    // Прием оригиналов документов
    public static final long ACCEPT_ORIGINAL_DOCUMENT_FOR_DIRECTION = 0; // На направление подготовки (специальность)
    public static final long ACCEPT_ORIGINAL_DOCUMENT_FOR_REQUEST = 1;   // В рамках заявления
    private List<IdentifiableWrapper> _acceptOriginalDocumentList;
    private IdentifiableWrapper _acceptOriginalDocument;
    private boolean _acceptOriginalDocumentDisabled;

    // Оригиналы документов при зачислении
    public static final long ORIGINAL_DOCUMENTS_NEED = 0;     // Обязательны
    public static final long ORIGINAL_DOCUMENTS_NO_NEED = 1;  // Необязательны
    private List<IdentifiableWrapper> _originalDocumentList;
    private IdentifiableWrapper _needOriginalDocForPreliminary;
    private IdentifiableWrapper _needOriginalDocForOrder;
    private boolean _originalDocumentDisabled;

    public ISelectModel _requiredAgreementModel;
    private DataWrapper _requiredAgreement4PreEnrollment;
    private DataWrapper _requiredAgreement4Order;

    // Формирование параграфа приказа о зачислении (по умолчанию)
    public static final long ENROLL_PARAGRAPH_FORMING_RULE_1 = 0;     // На направление подготовки (специальность)
    public static final long ENROLL_PARAGRAPH_FORMING_RULE_2 = 1;     // На направление подготовки ОУ, формирующее подр. и форму освоения
    private List<IdentifiableWrapper> _enrollParagraphRuleList;
    private IdentifiableWrapper _enrollParagraphRule;
    private boolean _enrollParagraphRuleDisabled;

    // Getters & Setters

    public Long getEnrollmentCampaignId()
    {
        return _enrollmentCampaignId;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        _enrollmentCampaignId = enrollmentCampaignId;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public EnrollmentCampaignPeriod getPeriod()
    {
        return _period;
    }

    public void setPeriod(EnrollmentCampaignPeriod period)
    {
        _period = period;
    }

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<IdentifiableWrapper> getEnrollmentPerCompensationTypeList()
    {
        return _enrollmentPerCompensationTypeList;
    }

    public void setEnrollmentPerCompensationTypeList(List<IdentifiableWrapper> enrollmentPerCompensationTypeList)
    {
        _enrollmentPerCompensationTypeList = enrollmentPerCompensationTypeList;
    }

    public IdentifiableWrapper getEnrollmentPerCompensationType()
    {
        return _enrollmentPerCompensationType;
    }

    public void setEnrollmentPerCompensationType(IdentifiableWrapper enrollmentPerCompensationType)
    {
        _enrollmentPerCompensationType = enrollmentPerCompensationType;
    }

    public boolean isEnrollmentPerCompensationTypeDisabled()
    {
        return _enrollmentPerCompensationTypeDisabled;
    }

    public void setEnrollmentPerCompensationTypeDisabled(boolean enrollmentPerCompensationTypeDisabled)
    {
        _enrollmentPerCompensationTypeDisabled = enrollmentPerCompensationTypeDisabled;
    }

    public List<IdentifiableWrapper> getEntrantExamListFormingList()
    {
        return _entrantExamListFormingList;
    }

    public void setEntrantExamListFormingList(List<IdentifiableWrapper> entrantExamListFormingList)
    {
        _entrantExamListFormingList = entrantExamListFormingList;
    }

    public IdentifiableWrapper getEntrantExamListForming()
    {
        return _entrantExamListForming;
    }

    public void setEntrantExamListForming(IdentifiableWrapper entrantExamListForming)
    {
        _entrantExamListForming = entrantExamListForming;
    }

    public boolean isEntrantExamListFormingDisabled()
    {
        return _entrantExamListFormingDisabled;
    }

    public void setEntrantExamListFormingDisabled(boolean entrantExamListFormingDisabled)
    {
        _entrantExamListFormingDisabled = entrantExamListFormingDisabled;
    }

    public List<IdentifiableWrapper> getCompetitionGroupList()
    {
        return _competitionGroupList;
    }

    public void setCompetitionGroupList(List<IdentifiableWrapper> competitionGroupList)
    {
        _competitionGroupList = competitionGroupList;
    }

    public IdentifiableWrapper getCompetitionGroup()
    {
        return _competitionGroup;
    }

    public void setCompetitionGroup(IdentifiableWrapper competitionGroup)
    {
        _competitionGroup = competitionGroup;
    }

    public boolean isCompetitionGroupDisabled()
    {
        return _competitionGroupDisabled;
    }

    public void setCompetitionGroupDisabled(boolean competitionGroupDisabled)
    {
        _competitionGroupDisabled = competitionGroupDisabled;
    }

    public int getCurrentDirectionRestrictionIndex()
    {
        return _currentDirectionRestrictionIndex;
    }

    public void setCurrentDirectionRestrictionIndex(int currentDirectionRestrictionIndex)
    {
        _currentDirectionRestrictionIndex = currentDirectionRestrictionIndex;
    }

    public EnrollmentDirectionRestriction getCurrentDirectionRestriction()
    {
        return _currentDirectionRestriction;
    }

    public void setCurrentDirectionRestriction(EnrollmentDirectionRestriction currentDirectionRestriction)
    {
        _currentDirectionRestriction = currentDirectionRestriction;
    }

    public List<EnrollmentDirectionRestriction> getDirectionRestrictionList()
    {
        return _directionRestrictionList;
    }

    public void setDirectionRestrictionList(List<EnrollmentDirectionRestriction> directionRestrictionList)
    {
        _directionRestrictionList = directionRestrictionList;
    }

    public int getCurrentCompetitionGroupRestrictionIndex()
    {
        return _currentCompetitionGroupRestrictionIndex;
    }

    public void setCurrentCompetitionGroupRestrictionIndex(int currentCompetitionGroupRestrictionIndex)
    {
        _currentCompetitionGroupRestrictionIndex = currentCompetitionGroupRestrictionIndex;
    }

    public CompetitionGroupRestriction getCurrentCompetitionGroupRestriction()
    {
        return _currentCompetitionGroupRestriction;
    }

    public void setCurrentCompetitionGroupRestriction(CompetitionGroupRestriction currentCompetitionGroupRestriction)
    {
        _currentCompetitionGroupRestriction = currentCompetitionGroupRestriction;
    }

    public List<CompetitionGroupRestriction> getCompetitionGroupRestrictionList()
    {
        return _competitionGroupRestrictionList;
    }

    public void setCompetitionGroupRestrictionList(List<CompetitionGroupRestriction> competitionGroupRestrictionList)
    {
        _competitionGroupRestrictionList = competitionGroupRestrictionList;
    }

    public List<IdentifiableWrapper> getDirectionForOutOfCompetitionList()
    {
        return _directionForOutOfCompetitionList;
    }

    public void setDirectionForOutOfCompetitionList(List<IdentifiableWrapper> directionForOutOfCompetitionList)
    {
        _directionForOutOfCompetitionList = directionForOutOfCompetitionList;
    }

    public IdentifiableWrapper getDirectionForOutOfCompetition()
    {
        return _directionForOutOfCompetition;
    }

    public void setDirectionForOutOfCompetition(IdentifiableWrapper directionForOutOfCompetition)
    {
        _directionForOutOfCompetition = directionForOutOfCompetition;
    }

    public List<IdentifiableWrapper> getDirectionForTargetAdmissionList()
    {
        return _directionForTargetAdmissionList;
    }

    public void setDirectionForTargetAdmissionList(List<IdentifiableWrapper> directionForTargetAdmissionList)
    {
        _directionForTargetAdmissionList = directionForTargetAdmissionList;
    }

    public IdentifiableWrapper getDirectionForTargetAdmission()
    {
        return _directionForTargetAdmission;
    }

    public void setDirectionForTargetAdmission(IdentifiableWrapper directionForTargetAdmission)
    {
        _directionForTargetAdmission = directionForTargetAdmission;
    }

    public List<IdentifiableWrapper> getNumberOnRequestList()
    {
        return _numberOnRequestList;
    }

    public void setNumberOnRequestList(List<IdentifiableWrapper> numberOnRequestList)
    {
        _numberOnRequestList = numberOnRequestList;
    }

    public IdentifiableWrapper getNumberOnRequest()
    {
        return _numberOnRequest;
    }

    public void setNumberOnRequest(IdentifiableWrapper numberOnRequest)
    {
        _numberOnRequest = numberOnRequest;
    }

    public boolean isNumberOnRequestDisabled()
    {
        return _numberOnRequestDisabled;
    }

    public void setNumberOnRequestDisabled(boolean numberOnRequestDisabled)
    {
        _numberOnRequestDisabled = numberOnRequestDisabled;
    }

    public List<IdentifiableWrapper> getNumberOnDirectionList()
    {
        return _numberOnDirectionList;
    }

    public void setNumberOnDirectionList(List<IdentifiableWrapper> numberOnDirectionList)
    {
        _numberOnDirectionList = numberOnDirectionList;
    }

    public IdentifiableWrapper getNumberOnDirection()
    {
        return _numberOnDirection;
    }

    public void setNumberOnDirection(IdentifiableWrapper numberOnDirection)
    {
        _numberOnDirection = numberOnDirection;
    }

    public boolean isNumberOnDirectionDisabled()
    {
        return _numberOnDirectionDisabled;
    }

    public void setNumberOnDirectionDisabled(boolean numberOnDirectionDisabled)
    {
        _numberOnDirectionDisabled = numberOnDirectionDisabled;
    }

    public List<IdentifiableWrapper> getDirectionSelectionList()
    {
        return _directionSelectionList;
    }

    public void setDirectionSelectionList(List<IdentifiableWrapper> directionSelectionList)
    {
        _directionSelectionList = directionSelectionList;
    }

    public IdentifiableWrapper getDirectionSelection()
    {
        return _directionSelection;
    }

    public void setDirectionSelection(IdentifiableWrapper directionSelection)
    {
        _directionSelection = directionSelection;
    }

    public boolean isDirectionSelectionDisabled()
    {
        return _directionSelectionDisabled;
    }

    public void setDirectionSelectionDisabled(boolean directionSelectionDisabled)
    {
        _directionSelectionDisabled = directionSelectionDisabled;
    }

    public List<IdentifiableWrapper> getEntrantRegistrationWizardList()
    {
        return _entrantRegistrationWizardList;
    }

    public void setEntrantRegistrationWizardList(List<IdentifiableWrapper> entrantRegistrationWizardList)
    {
        _entrantRegistrationWizardList = entrantRegistrationWizardList;
    }

    public IdentifiableWrapper getEntrantRegistrationWizard()
    {
        return _entrantRegistrationWizard;
    }

    public void setEntrantRegistrationWizard(IdentifiableWrapper entrantRegistrationWizard)
    {
        _entrantRegistrationWizard = entrantRegistrationWizard;
    }

    public boolean isEntrantRegistrationWizardDisabled()
    {
        return _entrantRegistrationWizardDisabled;
    }

    public void setEntrantRegistrationWizardDisabled(boolean entrantRegistrationWizardDisabled)
    {
        _entrantRegistrationWizardDisabled = entrantRegistrationWizardDisabled;
    }

    public List<IdentifiableWrapper> getCompetitionGroupExamSetList()
    {
        return _competitionGroupExamSetList;
    }

    public void setCompetitionGroupExamSetList(List<IdentifiableWrapper> competitionGroupExamSetList)
    {
        _competitionGroupExamSetList = competitionGroupExamSetList;
    }

    public IdentifiableWrapper getCompetitionGroupExamSet()
    {
        return _competitionGroupExamSet;
    }

    public void setCompetitionGroupExamSet(IdentifiableWrapper competitionGroupExamSet)
    {
        _competitionGroupExamSet = competitionGroupExamSet;
    }

    public boolean isCompetitionGroupExamSetDisabled()
    {
        return _competitionGroupExamSetDisabled;
    }

    public void setCompetitionGroupExamSetDisabled(boolean competitionGroupExamSetDisabled)
    {
        _competitionGroupExamSetDisabled = competitionGroupExamSetDisabled;
    }

    public List<IdentifiableWrapper> getPassDisciplineFormingList()
    {
        return _passDisciplineFormingList;
    }

    public void setPassDisciplineFormingList(List<IdentifiableWrapper> passDisciplineFormingList)
    {
        _passDisciplineFormingList = passDisciplineFormingList;
    }

    public IdentifiableWrapper getPassDisciplineForming()
    {
        return _passDisciplineForming;
    }

    public void setPassDisciplineForming(IdentifiableWrapper passDisciplineForming)
    {
        _passDisciplineForming = passDisciplineForming;
    }

    public boolean isPassDisciplineFormingDisabled()
    {
        return _passDisciplineFormingDisabled;
    }

    public void setPassDisciplineFormingDisabled(boolean passDisciplineFormingDisabled)
    {
        _passDisciplineFormingDisabled = passDisciplineFormingDisabled;
    }

    public List<IdentifiableWrapper> getDirectionSelectionByStateExamList()
    {
        return _directionSelectionByStateExamList;
    }

    public void setDirectionSelectionByStateExamList(List<IdentifiableWrapper> directionSelectionByStateExamList)
    {
        _directionSelectionByStateExamList = directionSelectionByStateExamList;
    }

    public IdentifiableWrapper getDirectionSelectionByStateExam()
    {
        return _directionSelectionByStateExam;
    }

    public void setDirectionSelectionByStateExam(IdentifiableWrapper directionSelectionByStateExam)
    {
        _directionSelectionByStateExam = directionSelectionByStateExam;
    }

    public boolean isDirectionSelectionByStateExamDisabled()
    {
        return _directionSelectionByStateExamDisabled;
    }

    public void setDirectionSelectionByStateExamDisabled(boolean directionSelectionByStateExamDisabled)
    {
        _directionSelectionByStateExamDisabled = directionSelectionByStateExamDisabled;
    }

    public List<IdentifiableWrapper> getStateExamRegistrationList()
    {
        return _stateExamRegistrationList;
    }

    public void setStateExamRegistrationList(List<IdentifiableWrapper> stateExamRegistrationList)
    {
        _stateExamRegistrationList = stateExamRegistrationList;
    }

    public IdentifiableWrapper getStateExamRegistration()
    {
        return _stateExamRegistration;
    }

    public void setStateExamRegistration(IdentifiableWrapper stateExamRegistration)
    {
        _stateExamRegistration = stateExamRegistration;
    }

    public boolean isStateExamRegistrationDisabled()
    {
        return _stateExamRegistrationDisabled;
    }

    public void setStateExamRegistrationDisabled(boolean stateExamRegistrationDisabled)
    {
        _stateExamRegistrationDisabled = stateExamRegistrationDisabled;
    }

    public List<IdentifiableWrapper> getStateExamWave1NumberList()
    {
        return _stateExamWave1NumberList;
    }

    public void setStateExamWave1NumberList(List<IdentifiableWrapper> stateExamWave1NumberList)
    {
        _stateExamWave1NumberList = stateExamWave1NumberList;
    }

    public IdentifiableWrapper getStateExamWave1Number()
    {
        return _stateExamWave1Number;
    }

    public void setStateExamWave1Number(IdentifiableWrapper stateExamWave1Number)
    {
        _stateExamWave1Number = stateExamWave1Number;
    }

    public boolean isStateExamWave1NumberDisabled()
    {
        return _stateExamWave1NumberDisabled;
    }

    public void setStateExamWave1NumberDisabled(boolean stateExamWave1NumberDisabled)
    {
        _stateExamWave1NumberDisabled = stateExamWave1NumberDisabled;
    }

    public List<IdentifiableWrapper> getOlympiadDiplomaRuleList()
    {
        return _olympiadDiplomaRuleList;
    }

    public void setOlympiadDiplomaRuleList(List<IdentifiableWrapper> olympiadDiplomaRuleList)
    {
        _olympiadDiplomaRuleList = olympiadDiplomaRuleList;
    }

    public IdentifiableWrapper getOlympiadDiplomaRule()
    {
        return _olympiadDiplomaRule;
    }

    public void setOlympiadDiplomaRule(IdentifiableWrapper olympiadDiplomaRule)
    {
        _olympiadDiplomaRule = olympiadDiplomaRule;
    }

    public boolean isOlympiadDiplomaRuleDisabled()
    {
        return _olympiadDiplomaRuleDisabled;
    }

    public void setOlympiadDiplomaRuleDisabled(boolean olympiadDiplomaRuleDisabled)
    {
        _olympiadDiplomaRuleDisabled = olympiadDiplomaRuleDisabled;
    }

    public List<IdentifiableWrapper> getPrioritiesAdmissionTestRuleList()
    {
        return _prioritiesAdmissionTestRuleList;
    }

    public void setPrioritiesAdmissionTestRuleList(List<IdentifiableWrapper> prioritiesAdmissionTestRuleList)
    {
        _prioritiesAdmissionTestRuleList = prioritiesAdmissionTestRuleList;
    }

    public IdentifiableWrapper getPrioritiesAdmissionTestRule()
    {
        return _prioritiesAdmissionTestRule;
    }

    public void setPrioritiesAdmissionTestRule(IdentifiableWrapper prioritiesAdmissionTestRule)
    {
        _prioritiesAdmissionTestRule = prioritiesAdmissionTestRule;
    }


    public boolean isUsePrintRequestButton()
    {
        return _usePrintRequestButton;
    }

    public void setUsePrintRequestButton(boolean usePrintRequestButton)
    {
        _usePrintRequestButton = usePrintRequestButton;
    }

    public boolean isUsePrintExamSheetButton()
    {
        return _usePrintExamSheetButton;
    }

    public void setUsePrintExamSheetButton(boolean usePrintExamSheetButton)
    {
        _usePrintExamSheetButton = usePrintExamSheetButton;
    }

    public boolean isUsePrintDocumentsInventoryAndReceiptButton()
    {
        return _usePrintDocumentsInventoryAndReceiptButton;
    }

    public void setUsePrintDocumentsInventoryAndReceiptButton(boolean usePrintDocumentsInventoryAndReceiptButton)
    {
        _usePrintDocumentsInventoryAndReceiptButton = usePrintDocumentsInventoryAndReceiptButton;
    }

    public boolean isUsePrintAuthCardButton()
    {
        return _usePrintAuthCardButton;
    }

    public void setUsePrintAuthCardButton(boolean usePrintAuthCardButton)
    {
        _usePrintAuthCardButton = usePrintAuthCardButton;
    }

    public boolean isUsePrintLetterboxButton()
    {
        return _usePrintLetterboxButton;
    }

    public void setUsePrintLetterboxButton(boolean usePrintLetterboxButton)
    {
        _usePrintLetterboxButton = usePrintLetterboxButton;
    }

    public boolean isUseChangePriorityButton()
    {
        return _useChangePriorityButton;
    }

    public void setUseChangePriorityButton(boolean useChangePriorityButton)
    {
        _useChangePriorityButton = useChangePriorityButton;
    }

    public List<IdentifiableWrapper> getFormingExamGroupList()
    {
        return _formingExamGroupList;
    }

    public void setFormingExamGroupList(List<IdentifiableWrapper> formingExamGroupList)
    {
        _formingExamGroupList = formingExamGroupList;
    }

    public IdentifiableWrapper getFormingExamGroup()
    {
        return _formingExamGroup;
    }

    public void setFormingExamGroup(IdentifiableWrapper formingExamGroup)
    {
        _formingExamGroup = formingExamGroup;
    }

    public boolean isFormingExamGroupDisabled()
    {
        return _formingExamGroupDisabled;
    }

    public void setFormingExamGroupDisabled(boolean formingExamGroupDisabled)
    {
        _formingExamGroupDisabled = formingExamGroupDisabled;
    }

    public List<IdentifiableWrapper> getAcceptOriginalDocumentList()
    {
        return _acceptOriginalDocumentList;
    }

    public void setAcceptOriginalDocumentList(List<IdentifiableWrapper> acceptOriginalDocumentList)
    {
        _acceptOriginalDocumentList = acceptOriginalDocumentList;
    }

    public IdentifiableWrapper getAcceptOriginalDocument()
    {
        return _acceptOriginalDocument;
    }

    public void setAcceptOriginalDocument(IdentifiableWrapper acceptOriginalDocument)
    {
        _acceptOriginalDocument = acceptOriginalDocument;
    }

    public boolean isAcceptOriginalDocumentDisabled()
    {
        return _acceptOriginalDocumentDisabled;
    }

    public void setAcceptOriginalDocumentDisabled(boolean acceptOriginalDocumentDisabled)
    {
        _acceptOriginalDocumentDisabled = acceptOriginalDocumentDisabled;
    }

    public List<IdentifiableWrapper> getOriginalDocumentList()
    {
        return _originalDocumentList;
    }

    public void setOriginalDocumentList(List<IdentifiableWrapper> originalDocumentList)
    {
        _originalDocumentList = originalDocumentList;
    }

    public IdentifiableWrapper getNeedOriginalDocForPreliminary()
    {
        return _needOriginalDocForPreliminary;
    }

    public void setNeedOriginalDocForPreliminary(IdentifiableWrapper needOriginalDocForPreliminary)
    {
        _needOriginalDocForPreliminary = needOriginalDocForPreliminary;
    }

    public IdentifiableWrapper getNeedOriginalDocForOrder()
    {
        return _needOriginalDocForOrder;
    }

    public void setNeedOriginalDocForOrder(IdentifiableWrapper needOriginalDocForOrder)
    {
        _needOriginalDocForOrder = needOriginalDocForOrder;
    }

    public boolean isOriginalDocumentDisabled()
    {
        return _originalDocumentDisabled;
    }

    public void setOriginalDocumentDisabled(boolean originalDocumentDisabled)
    {
        _originalDocumentDisabled = originalDocumentDisabled;
    }

    public List<IdentifiableWrapper> getEnrollParagraphRuleList()
    {
        return _enrollParagraphRuleList;
    }

    public void setEnrollParagraphRuleList(List<IdentifiableWrapper> enrollParagraphRuleList)
    {
        _enrollParagraphRuleList = enrollParagraphRuleList;
    }

    public IdentifiableWrapper getEnrollParagraphRule()
    {
        return _enrollParagraphRule;
    }

    public void setEnrollParagraphRule(IdentifiableWrapper enrollParagraphRule)
    {
        _enrollParagraphRule = enrollParagraphRule;
    }

    public boolean isEnrollParagraphRuleDisabled()
    {
        return _enrollParagraphRuleDisabled;
    }

    public void setEnrollParagraphRuleDisabled(boolean enrollParagraphRuleDisabled)
    {
        _enrollParagraphRuleDisabled = enrollParagraphRuleDisabled;
    }

    public boolean isMaxEnrollmentDirectionDisabled()
    {
        return _maxEnrollmentDirectionDisabled;
    }

    public void setMaxEnrollmentDirectionDisabled(boolean maxEnrollmentDirectionDisabled)
    {
        _maxEnrollmentDirectionDisabled = maxEnrollmentDirectionDisabled;
    }

    public boolean isMaxMinisterialDirectionDisabled()
    {
        return _maxMinisterialDirectionDisabled;
    }

    public void setMaxMinisterialDirectionDisabled(boolean maxMinisterialDirectionDisabled)
    {
        _maxMinisterialDirectionDisabled = maxMinisterialDirectionDisabled;
    }

    public List<IdentifiableWrapper> getIndividualProgressList()
    {
        return _individualProgressList;
    }

    public void setIndividualProgressList(List<IdentifiableWrapper> individualProgressList)
    {
        _individualProgressList = individualProgressList;
    }

    public IdentifiableWrapper getIndividualProgressConsider()
    {
        return _individualProgressConsider;
    }

    public void setIndividualProgressConsider(IdentifiableWrapper individualProgressConsider)
    {
        _individualProgressConsider = individualProgressConsider;
    }

    public List<EnrollmentCampaignType> getEnrollmentCampaignTypes()
    {
        return _enrollmentCampaignTypes;
    }

    public void setEnrollmentCampaignTypes(List<EnrollmentCampaignType> enrollmentCampaignTypes)
    {
        _enrollmentCampaignTypes = enrollmentCampaignTypes;
    }

    public DataWrapper getRequiredAgreement4Order()
    {
        return _requiredAgreement4Order;
    }

    public void setRequiredAgreement4Order(DataWrapper requiredAgreement4Order)
    {
        _requiredAgreement4Order = requiredAgreement4Order;
    }

    public DataWrapper getRequiredAgreement4PreEnrollment()
    {
        return _requiredAgreement4PreEnrollment;
    }

    public void setRequiredAgreement4PreEnrollment(DataWrapper requiredAgreement4PreEnrollment)
    {
        _requiredAgreement4PreEnrollment = requiredAgreement4PreEnrollment;
    }

    public ISelectModel getRequiredAgreementModel()
    {
        return _requiredAgreementModel;
    }

    public void setRequiredAgreementModel(ISelectModel requiredAgreementModel)
    {
        this._requiredAgreementModel = requiredAgreementModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }
}
