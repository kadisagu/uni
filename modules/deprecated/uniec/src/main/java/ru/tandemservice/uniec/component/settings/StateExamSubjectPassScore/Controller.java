/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

/**
 * @author oleyba
 * @since 07.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        prepareDataSource(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        int columnWidth = 10;
        DynamicListDataSource<Model.SubjectWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Предмет", Model.SubjectWrapper.VIEW_P_SUBJECT + "." + StateExamSubject.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn<Integer>("mark", "Зачетный балл").setWidth(columnWidth));
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);
        dao.update(model);
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }
}
