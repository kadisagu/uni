package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сертификат ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantStateExamCertificateGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate";
    public static final String ENTITY_NAME = "entrantStateExamCertificate";
    public static final int VERSION_HASH = -37859210;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENTRANT = "entrant";
    public static final String P_NUMBER = "number";
    public static final String P_ISSUANCE_DATE = "issuanceDate";
    public static final String P_ORIGINAL = "original";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_SENT = "sent";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String L_STATE_EXAM_TYPE = "stateExamType";

    private int _version; 
    private Entrant _entrant;     // (Старый) Абитуриент
    private String _number;     // Номер
    private Date _issuanceDate;     // Дата выдачи
    private boolean _original;     // Оригинал
    private boolean _accepted;     // Зачтено
    private boolean _sent;     // Отправлено
    private Date _registrationDate;     // Дата добавления
    private StateExamType _stateExamType;     // Типы экзаменов ЕГЭ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=13)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getIssuanceDate()
    {
        return _issuanceDate;
    }

    /**
     * @param issuanceDate Дата выдачи.
     */
    public void setIssuanceDate(Date issuanceDate)
    {
        dirty(_issuanceDate, issuanceDate);
        _issuanceDate = issuanceDate;
    }

    /**
     * @return Оригинал. Свойство не может быть null.
     */
    @NotNull
    public boolean isOriginal()
    {
        return _original;
    }

    /**
     * @param original Оригинал. Свойство не может быть null.
     */
    public void setOriginal(boolean original)
    {
        dirty(_original, original);
        _original = original;
    }

    /**
     * @return Зачтено. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Зачтено. Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * @return Отправлено. Свойство не может быть null.
     */
    @NotNull
    public boolean isSent()
    {
        return _sent;
    }

    /**
     * @param sent Отправлено. Свойство не может быть null.
     */
    public void setSent(boolean sent)
    {
        dirty(_sent, sent);
        _sent = sent;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Типы экзаменов ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public StateExamType getStateExamType()
    {
        return _stateExamType;
    }

    /**
     * @param stateExamType Типы экзаменов ЕГЭ. Свойство не может быть null.
     */
    public void setStateExamType(StateExamType stateExamType)
    {
        dirty(_stateExamType, stateExamType);
        _stateExamType = stateExamType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantStateExamCertificateGen)
        {
            setVersion(((EntrantStateExamCertificate)another).getVersion());
            setEntrant(((EntrantStateExamCertificate)another).getEntrant());
            setNumber(((EntrantStateExamCertificate)another).getNumber());
            setIssuanceDate(((EntrantStateExamCertificate)another).getIssuanceDate());
            setOriginal(((EntrantStateExamCertificate)another).isOriginal());
            setAccepted(((EntrantStateExamCertificate)another).isAccepted());
            setSent(((EntrantStateExamCertificate)another).isSent());
            setRegistrationDate(((EntrantStateExamCertificate)another).getRegistrationDate());
            setStateExamType(((EntrantStateExamCertificate)another).getStateExamType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantStateExamCertificateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantStateExamCertificate.class;
        }

        public T newInstance()
        {
            return (T) new EntrantStateExamCertificate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "entrant":
                    return obj.getEntrant();
                case "number":
                    return obj.getNumber();
                case "issuanceDate":
                    return obj.getIssuanceDate();
                case "original":
                    return obj.isOriginal();
                case "accepted":
                    return obj.isAccepted();
                case "sent":
                    return obj.isSent();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "stateExamType":
                    return obj.getStateExamType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "issuanceDate":
                    obj.setIssuanceDate((Date) value);
                    return;
                case "original":
                    obj.setOriginal((Boolean) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "sent":
                    obj.setSent((Boolean) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "stateExamType":
                    obj.setStateExamType((StateExamType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "entrant":
                        return true;
                case "number":
                        return true;
                case "issuanceDate":
                        return true;
                case "original":
                        return true;
                case "accepted":
                        return true;
                case "sent":
                        return true;
                case "registrationDate":
                        return true;
                case "stateExamType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "entrant":
                    return true;
                case "number":
                    return true;
                case "issuanceDate":
                    return true;
                case "original":
                    return true;
                case "accepted":
                    return true;
                case "sent":
                    return true;
                case "registrationDate":
                    return true;
                case "stateExamType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "entrant":
                    return Entrant.class;
                case "number":
                    return String.class;
                case "issuanceDate":
                    return Date.class;
                case "original":
                    return Boolean.class;
                case "accepted":
                    return Boolean.class;
                case "sent":
                    return Boolean.class;
                case "registrationDate":
                    return Date.class;
                case "stateExamType":
                    return StateExamType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantStateExamCertificate> _dslPath = new Path<EntrantStateExamCertificate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantStateExamCertificate");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getIssuanceDate()
     */
    public static PropertyPath<Date> issuanceDate()
    {
        return _dslPath.issuanceDate();
    }

    /**
     * @return Оригинал. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#isOriginal()
     */
    public static PropertyPath<Boolean> original()
    {
        return _dslPath.original();
    }

    /**
     * @return Зачтено. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * @return Отправлено. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#isSent()
     */
    public static PropertyPath<Boolean> sent()
    {
        return _dslPath.sent();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Типы экзаменов ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getStateExamType()
     */
    public static StateExamType.Path<StateExamType> stateExamType()
    {
        return _dslPath.stateExamType();
    }

    public static class Path<E extends EntrantStateExamCertificate> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _issuanceDate;
        private PropertyPath<Boolean> _original;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Boolean> _sent;
        private PropertyPath<Date> _registrationDate;
        private StateExamType.Path<StateExamType> _stateExamType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EntrantStateExamCertificateGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EntrantStateExamCertificateGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getIssuanceDate()
     */
        public PropertyPath<Date> issuanceDate()
        {
            if(_issuanceDate == null )
                _issuanceDate = new PropertyPath<Date>(EntrantStateExamCertificateGen.P_ISSUANCE_DATE, this);
            return _issuanceDate;
        }

    /**
     * @return Оригинал. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#isOriginal()
     */
        public PropertyPath<Boolean> original()
        {
            if(_original == null )
                _original = new PropertyPath<Boolean>(EntrantStateExamCertificateGen.P_ORIGINAL, this);
            return _original;
        }

    /**
     * @return Зачтено. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(EntrantStateExamCertificateGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * @return Отправлено. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#isSent()
     */
        public PropertyPath<Boolean> sent()
        {
            if(_sent == null )
                _sent = new PropertyPath<Boolean>(EntrantStateExamCertificateGen.P_SENT, this);
            return _sent;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(EntrantStateExamCertificateGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Типы экзаменов ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate#getStateExamType()
     */
        public StateExamType.Path<StateExamType> stateExamType()
        {
            if(_stateExamType == null )
                _stateExamType = new StateExamType.Path<StateExamType>(L_STATE_EXAM_TYPE, this);
            return _stateExamType;
        }

        public Class getEntityClass()
        {
            return EntrantStateExamCertificate.class;
        }

        public String getEntityName()
        {
            return "entrantStateExamCertificate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
