/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.enrollmentextract;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 09.04.2009
 */
public class CommonEnrollmentExtractDao extends UniBaseDao implements ICommonEnrollmentExtractDao
{
    @Override
    public OrderData doCommit(EnrollmentExtract extract)
    {
        // создаем студента и заполняем его
        Student student = new Student();
        student.setCompensationType(extract.getEntity().getCompensationType());
        student.setCourse(extract.getCourse());
        student.setEducationOrgUnit(extract.getEntity().getEducationOrgUnit());
        student.setDevelopPeriodAuto(extract.getEntity().getEducationOrgUnit().getDevelopPeriod());
        student.setEntranceYear(UniBaseUtils.getCurrentYear());
        student.setStudentCategory(extract.getEntity().getStudentCategory());
        student.setPerson(extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson());
        student.setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
        student.setTargetAdmission(extract.getEntity().isTargetAdmission());

        // сохраняем
        save(student);

        // заполняем дату и номер приказа
        OrderData orderData = new OrderData();
        orderData.setStudent(student);

        if (extract.getOrder().getEnrollmentDate() != null)
            orderData.setEduEnrollmentOrderEnrDate(extract.getOrder().getEnrollmentDate());
        else
            orderData.setEduEnrollmentOrderEnrDate(extract.getOrder().getCommitDate());

        orderData.setEduEnrollmentOrderDate(extract.getOrder().getCommitDate());
        orderData.setEduEnrollmentOrderNumber(extract.getOrder().getNumber());

        save(orderData);

        // записываем его в выписку
        extract.setStudentNew(student);

        // устанавливается состояние «Зачислен» для выбранного направления приема
        extract.getEntity().getRequestedEnrollmentDirection().setState(getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_ENROLED_CODE));
        update(extract.getEntity().getRequestedEnrollmentDirection());

        return orderData;
    }

    @Override
    public void doRollback(EnrollmentExtract extract)
    {
        // удаляем студента
        PersonManager.instance().dao().deletePersonRole(extract.getStudentNew().getId());
        extract.setStudentNew(null);

        // устанавливается состояние «В приказе» для выбранного направления приема
        extract.getEntity().getRequestedEnrollmentDirection().setState(getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_IN_ORDER));
        update(extract.getEntity().getRequestedEnrollmentDirection());
    }

    @Override
    public void doCommitRevertExtract(EnrollmentRevertExtract revertExtract)
    {
        final Session session = getSession();

        session.refresh(revertExtract);

        EnrollmentExtract canceledExtract = revertExtract.getCanceledExtract();
        session.refresh(canceledExtract);
        session.refresh(canceledExtract.getOrder());

        if (!canceledExtract.getOrder().getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED) || !canceledExtract.isCommitted())
            throw new ApplicationException("Отменять можно выписки только проведенных приказов.");

        // Удаляем ссылку на отменяемую выписку
        revertExtract.setCanceledExtract(null);

        // Откатываем отменяемую выписку
        doRollback(canceledExtract);

        // Удаляем выписку и студента предзачисления
        EcOrderManager.instance().dao().executeAnnulExtract(canceledExtract);

        // Устанавливаем состояние «К зачислению» для ВНП
        revertExtract.getEntity().setState(getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE));
        session.update(revertExtract.getEntity());
    }
}
