/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e1.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public interface IDAO extends IAbstractListParagraphPubDAO<SplitBudgetMasterEntrantsStuListExtract, Model>
{
}