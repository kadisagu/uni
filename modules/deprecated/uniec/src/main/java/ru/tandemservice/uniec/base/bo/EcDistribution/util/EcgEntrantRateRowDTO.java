/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;

/**
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public class EcgEntrantRateRowDTO extends IdentifiableWrapper<Entrant> implements IEcgEntrantRateRowDTO
{
    private static final long serialVersionUID = 1L;
    private String _fio;
    private double _finalMark;
    private Double _profileMark;
    private Double _averageMark;
    private boolean _originalDocumentHandedIn;
    private boolean _graduatedProfileEduInstitution;
    private boolean _targetAdmission;
    private TargetAdmissionKind _targetAdmissionKind;
    private CompetitionKind _competitionKind;
    private List<IEcgEntrantRateDirectionDTO> _directionList;

    public EcgEntrantRateRowDTO(Long entrantId, String fio, double finalMark, Double profileMark, Double averageMark, boolean originalDocumentHandedIn, boolean graduatedProfileEduInstitution, boolean targetAdmission, TargetAdmissionKind targetAdmissionKind, CompetitionKind competitionKind, List<IEcgEntrantRateDirectionDTO> directionList) throws ClassCastException
    {
        super(entrantId, null);
        _fio = fio;
        _finalMark = finalMark;
        _profileMark = profileMark;
        _averageMark = averageMark;
        _originalDocumentHandedIn = originalDocumentHandedIn;
        _graduatedProfileEduInstitution = graduatedProfileEduInstitution;
        _targetAdmission = targetAdmission;
        _targetAdmissionKind = targetAdmissionKind;
        _competitionKind = competitionKind;
        _directionList = directionList;
    }

    // Getters

    @Override
    public String getCompetitionKindTitle()
    {
        return _targetAdmission ? (EnrollmentCompetitionKind.TARGET_ADMISSION + (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(_competitionKind.getCode()) ? "" : ", " + _competitionKind.getTitle())) : _competitionKind.getTitle();
    }

    @Override
    public String getTargetAdmissionKindTitle()
    {
        return _targetAdmissionKind == null ? null : _targetAdmissionKind.getFullHierarhyTitle();
    }

    @Override
    public String getGraduatedProfileEduInstitutionTitle()
    {
        return _graduatedProfileEduInstitution ? "Да" : "";
    }

    @Override
    public String getPriorities()
    {
        List<String> titleList = new ArrayList<String>();
        for (IEcgEntrantRateDirectionDTO item : _directionList)
            titleList.add(item.getTitle());
        return StringUtils.join(titleList, ", ");
    }

    @Override
    public String getOriginalDocumentHandedInTitle()
    {
        return _originalDocumentHandedIn ? "оригиналы" : "копии";
    }

    @Override
    public List<IEcgEntrantRateDirectionDTO> getDirectionList()
    {
        return _directionList;
    }

    @Override
    public IEntity getEntity()
    {
        return null;
    }

    @Override
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    @Override
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    @Override
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    @Override
    public Double getFinalMark()
    {
        return _finalMark;
    }

    @Override
    public Double getProfileMark()
    {
        return _profileMark;
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution()
    {
        return _graduatedProfileEduInstitution;
    }

    @Override
    public Double getCertificateAverageMark()
    {
        return _averageMark;
    }

    @Override
    public String getFio()
    {
        return _fio;
    }
}
