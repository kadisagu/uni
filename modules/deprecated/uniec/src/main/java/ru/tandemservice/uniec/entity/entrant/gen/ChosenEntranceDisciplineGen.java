package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранное вступительное испытание
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChosenEntranceDisciplineGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline";
    public static final String ENTITY_NAME = "chosenEntranceDiscipline";
    public static final int VERSION_HASH = 1293932382;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_CHOSEN_ENROLLMENT_DIRECTION = "chosenEnrollmentDirection";
    public static final String L_ENROLLMENT_CAMPAIGN_DISCIPLINE = "enrollmentCampaignDiscipline";
    public static final String P_FINAL_MARK = "finalMark";
    public static final String P_FINAL_MARK_SOURCE = "finalMarkSource";

    private int _version; 
    private RequestedEnrollmentDirection _chosenEnrollmentDirection;     // Выбранное направление приема
    private Discipline2RealizationWayRelation _enrollmentCampaignDiscipline;     // Дисциплина набора вступительных испытаний
    private Double _finalMark;     // Итоговый балл
    private Integer _finalMarkSource;     // Источник итога

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getChosenEnrollmentDirection()
    {
        return _chosenEnrollmentDirection;
    }

    /**
     * @param chosenEnrollmentDirection Выбранное направление приема. Свойство не может быть null.
     */
    public void setChosenEnrollmentDirection(RequestedEnrollmentDirection chosenEnrollmentDirection)
    {
        dirty(_chosenEnrollmentDirection, chosenEnrollmentDirection);
        _chosenEnrollmentDirection = chosenEnrollmentDirection;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getEnrollmentCampaignDiscipline()
    {
        return _enrollmentCampaignDiscipline;
    }

    /**
     * @param enrollmentCampaignDiscipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setEnrollmentCampaignDiscipline(Discipline2RealizationWayRelation enrollmentCampaignDiscipline)
    {
        dirty(_enrollmentCampaignDiscipline, enrollmentCampaignDiscipline);
        _enrollmentCampaignDiscipline = enrollmentCampaignDiscipline;
    }

    /**
     * @return Итоговый балл.
     */
    public Double getFinalMark()
    {
        return _finalMark;
    }

    /**
     * @param finalMark Итоговый балл.
     */
    public void setFinalMark(Double finalMark)
    {
        dirty(_finalMark, finalMark);
        _finalMark = finalMark;
    }

    /**
     * @return Источник итога.
     */
    public Integer getFinalMarkSource()
    {
        return _finalMarkSource;
    }

    /**
     * @param finalMarkSource Источник итога.
     */
    public void setFinalMarkSource(Integer finalMarkSource)
    {
        dirty(_finalMarkSource, finalMarkSource);
        _finalMarkSource = finalMarkSource;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChosenEntranceDisciplineGen)
        {
            setVersion(((ChosenEntranceDiscipline)another).getVersion());
            setChosenEnrollmentDirection(((ChosenEntranceDiscipline)another).getChosenEnrollmentDirection());
            setEnrollmentCampaignDiscipline(((ChosenEntranceDiscipline)another).getEnrollmentCampaignDiscipline());
            setFinalMark(((ChosenEntranceDiscipline)another).getFinalMark());
            setFinalMarkSource(((ChosenEntranceDiscipline)another).getFinalMarkSource());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChosenEntranceDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChosenEntranceDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new ChosenEntranceDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "chosenEnrollmentDirection":
                    return obj.getChosenEnrollmentDirection();
                case "enrollmentCampaignDiscipline":
                    return obj.getEnrollmentCampaignDiscipline();
                case "finalMark":
                    return obj.getFinalMark();
                case "finalMarkSource":
                    return obj.getFinalMarkSource();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "chosenEnrollmentDirection":
                    obj.setChosenEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "enrollmentCampaignDiscipline":
                    obj.setEnrollmentCampaignDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "finalMark":
                    obj.setFinalMark((Double) value);
                    return;
                case "finalMarkSource":
                    obj.setFinalMarkSource((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "chosenEnrollmentDirection":
                        return true;
                case "enrollmentCampaignDiscipline":
                        return true;
                case "finalMark":
                        return true;
                case "finalMarkSource":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "chosenEnrollmentDirection":
                    return true;
                case "enrollmentCampaignDiscipline":
                    return true;
                case "finalMark":
                    return true;
                case "finalMarkSource":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "chosenEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "enrollmentCampaignDiscipline":
                    return Discipline2RealizationWayRelation.class;
                case "finalMark":
                    return Double.class;
                case "finalMarkSource":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChosenEntranceDiscipline> _dslPath = new Path<ChosenEntranceDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChosenEntranceDiscipline");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getChosenEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> chosenEnrollmentDirection()
    {
        return _dslPath.chosenEnrollmentDirection();
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getEnrollmentCampaignDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> enrollmentCampaignDiscipline()
    {
        return _dslPath.enrollmentCampaignDiscipline();
    }

    /**
     * @return Итоговый балл.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getFinalMark()
     */
    public static PropertyPath<Double> finalMark()
    {
        return _dslPath.finalMark();
    }

    /**
     * @return Источник итога.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getFinalMarkSource()
     */
    public static PropertyPath<Integer> finalMarkSource()
    {
        return _dslPath.finalMarkSource();
    }

    public static class Path<E extends ChosenEntranceDiscipline> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _chosenEnrollmentDirection;
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _enrollmentCampaignDiscipline;
        private PropertyPath<Double> _finalMark;
        private PropertyPath<Integer> _finalMarkSource;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(ChosenEntranceDisciplineGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getChosenEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> chosenEnrollmentDirection()
        {
            if(_chosenEnrollmentDirection == null )
                _chosenEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_CHOSEN_ENROLLMENT_DIRECTION, this);
            return _chosenEnrollmentDirection;
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getEnrollmentCampaignDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> enrollmentCampaignDiscipline()
        {
            if(_enrollmentCampaignDiscipline == null )
                _enrollmentCampaignDiscipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_ENROLLMENT_CAMPAIGN_DISCIPLINE, this);
            return _enrollmentCampaignDiscipline;
        }

    /**
     * @return Итоговый балл.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getFinalMark()
     */
        public PropertyPath<Double> finalMark()
        {
            if(_finalMark == null )
                _finalMark = new PropertyPath<Double>(ChosenEntranceDisciplineGen.P_FINAL_MARK, this);
            return _finalMark;
        }

    /**
     * @return Источник итога.
     * @see ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline#getFinalMarkSource()
     */
        public PropertyPath<Integer> finalMarkSource()
        {
            if(_finalMarkSource == null )
                _finalMarkSource = new PropertyPath<Integer>(ChosenEntranceDisciplineGen.P_FINAL_MARK_SOURCE, this);
            return _finalMarkSource;
        }

        public Class getEntityClass()
        {
            return ChosenEntranceDiscipline.class;
        }

        public String getEntityName()
        {
            return "chosenEntranceDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
