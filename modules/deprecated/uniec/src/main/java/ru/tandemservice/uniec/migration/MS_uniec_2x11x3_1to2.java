package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x11x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность surveyTemplate2EnrCampaignRel

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("survey_templ_campaign_rel_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_62c9111e"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("template_id", DBType.LONG).setNullable(false), 
				new DBColumn("campaign_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("surveyTemplate2EnrCampaignRel");
		}


		////////////////////////////////////////////////////////////////////////////////
		// сущность questionaryEntrant

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("survey_usueqestionaryentrant_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_questionaryentrant"),
				new DBColumn("entrant_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("questionaryEntrant");

			// удалить constraint в parent-таблице
			tool.dropConstraint("survey_base_questionary_t", "chk_class_basequestionary");

		}
    }
}