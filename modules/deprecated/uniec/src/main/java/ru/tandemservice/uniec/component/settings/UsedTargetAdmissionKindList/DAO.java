// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.UsedTargetAdmissionKindList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.UseExternalOrgUnitForTA;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 07.07.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setExternalOrgUnitChooseList(Arrays.asList(new IdentifiableWrapper(Model.USE_EXTERNAL_ORG_UNIT_YES, "Использовать"), new IdentifiableWrapper(Model.USE_EXTERNAL_ORG_UNIT_NO, "Не использовать")));
        prepareExternalOrgUnitOptionGroup(model);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        if (model.getEnrollmentCampaign() == null)
        {
            UniBaseUtils.createPage(dataSource, new ArrayList());
            ViewWrapper.getPatchedList(dataSource);
        } else
        {
            List<TargetAdmissionKind> list = getList(TargetAdmissionKind.class, TargetAdmissionKind.priority().s());
            dataSource.setCountRow(list.size());
            UniBaseUtils.createPage(dataSource, list);

            Set<Long> notUsedIds = new HashSet<>();
            for (NotUsedTargetAdmissionKind item : getList(NotUsedTargetAdmissionKind.class, NotUsedTargetAdmissionKind.enrollmentCampaign().s(), model.getEnrollmentCampaign()))
                notUsedIds.add(item.getTargetAdmissionKind().getId());

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(dataSource))
                viewWrapper.setViewProperty(Model.P_IN_USE, !notUsedIds.contains(viewWrapper.getEntity().getId()));
        }
    }
    
    @Override
    public void prepareExternalOrgUnitOptionGroup(Model model)
    {
        if (model.getEnrollmentCampaign() != null)
        {
            UseExternalOrgUnitForTA item = get(UseExternalOrgUnitForTA.class, UseExternalOrgUnitForTA.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());
            model.setExternalOrgUnitChoose(model.getExternalOrgUnitChooseList().get(item == null ? 1 : 0));
        }
    }

    @Override
    public void update(Model model, Long id)
    {
        TargetAdmissionKind targetAdmissionKind = getNotNull(TargetAdmissionKind.class, id);

        // получаем выключенные типы/виды
        Map<Long, NotUsedTargetAdmissionKind> notUsedMap = new HashMap<>();
        for (NotUsedTargetAdmissionKind item : getList(NotUsedTargetAdmissionKind.class, NotUsedTargetAdmissionKind.enrollmentCampaign().s(), model.getEnrollmentCampaign()))
            notUsedMap.put(item.getTargetAdmissionKind().getId(), item);

        NotUsedTargetAdmissionKind notUsedTargetAdmissionKind = notUsedMap.get(id);
        if (notUsedTargetAdmissionKind != null)
        {
            // надо включить levelStage для кампании
            delete(notUsedTargetAdmissionKind);

            //Если включается элемент второго уровня, то включается и элемент первого уровня.
            if (targetAdmissionKind.getParent() != null)
            {
                NotUsedTargetAdmissionKind notUsedItem = notUsedMap.get(targetAdmissionKind.getParent().getId());
                if (notUsedItem != null)
                    delete(notUsedItem);
            }
            //Если включается элемент первого уровня, то не следует включать элементы второго уровня.
        } else
        {
            // надо выключить levelStage для кампании

            NotUsedTargetAdmissionKind notUsedItem = new NotUsedTargetAdmissionKind();
            notUsedItem.setTargetAdmissionKind(targetAdmissionKind);
            notUsedItem.setEnrollmentCampaign(model.getEnrollmentCampaign());
            save(notUsedItem);

            //Eсли выключается элемент первого уровня, то выключаются при этом и все подчиненные
            for (TargetAdmissionKind childItem : getList(TargetAdmissionKind.class, TargetAdmissionKind.L_PARENT, targetAdmissionKind))
            {
                if (!notUsedMap.containsKey(childItem.getId()))
                {
                    notUsedItem = new NotUsedTargetAdmissionKind();
                    notUsedItem.setTargetAdmissionKind(childItem);
                    notUsedItem.setEnrollmentCampaign(model.getEnrollmentCampaign());
                    save(notUsedItem);
                }
            }
        }
    }

    @Override
    public void update(Model model)
    {
        if (model.getEnrollmentCampaign() == null) return;

        UseExternalOrgUnitForTA item = get(UseExternalOrgUnitForTA.class, UseExternalOrgUnitForTA.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());

        if (model.getExternalOrgUnitChoose().getId().equals(Model.USE_EXTERNAL_ORG_UNIT_YES))
        {
            if (item == null)
            {
                item = new UseExternalOrgUnitForTA();
                item.setEnrollmentCampaign(model.getEnrollmentCampaign());
                save(item);
            }
        } else
        {
            if (item != null)
            {
                delete(item);
            }
        }
    }
}
