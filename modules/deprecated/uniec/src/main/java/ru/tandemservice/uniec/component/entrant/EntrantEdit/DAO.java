/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantEdit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse;
import ru.tandemservice.uniec.entity.entrant.EntrantAccessDepartment;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity;

/**
 * @author vip_delete
 * @since 05.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setRecomendationListModel(new LazySimpleSelectModel<EnrollmentRecommendation>(EnrollmentRecommendation.class));
        model.setAccessCourseListModel(new LazySimpleSelectModel<AccessCourse>(AccessCourse.class));
        model.setAccessDepartmentListModel(new LazySimpleSelectModel<AccessDepartment>(AccessDepartment.class));
        model.setSourceInfoAboutUniversityListModel(new LazySimpleSelectModel<SourceInfoAboutUniversity>(SourceInfoAboutUniversity.class));

        model.setRecomendationList(new ArrayList<EnrollmentRecommendation>());
        for (EntrantEnrolmentRecommendation item : getList(EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.L_ENTRANT, model.getEntrant()))
            model.getRecomendationList().add(item.getRecommendation());

        model.setAccessCourseList(new ArrayList<AccessCourse>());
        for (EntrantAccessCourse item : getList(EntrantAccessCourse.class, EntrantAccessCourse.L_ENTRANT, model.getEntrant()))
            model.getAccessCourseList().add(item.getCourse());
        Collections.sort(model.getAccessCourseList(), ITitled.TITLED_COMPARATOR);

        model.setAccessDepartmentList(new ArrayList<AccessDepartment>());
        for (EntrantAccessDepartment item : getList(EntrantAccessDepartment.class, EntrantAccessDepartment.L_ENTRANT, model.getEntrant()))
            model.getAccessDepartmentList().add(item.getAccessDepartment());
        Collections.sort(model.getAccessDepartmentList(), ITitled.TITLED_COMPARATOR);

        model.setSourceInfoAboutUniversityList(new ArrayList<SourceInfoAboutUniversity>());
        for (EntrantInfoAboutUniversity item : getList(EntrantInfoAboutUniversity.class, EntrantInfoAboutUniversity.L_ENTRANT, model.getEntrant()))
            model.getSourceInfoAboutUniversityList().add(item.getSourceInfo());

        model.setBase4ExamByDifferentSourcesModel(new StaticSelectModel("id", "title", getCatalogItemList(Base4ExamByDifferentSources.class)));
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();

        // VALIDATE
        if (model.getEntrant().isPassArmy() && model.getEntrant().getBeginArmy() != null && model.getEntrant().getEndArmy() != null && !model.getEntrant().getBeginArmy().before(model.getEntrant().getEndArmy()))
        {
            errors.add("Дата начала службы должна быть раньше даты ее окончания", "beginArmy");
            return;
        }

        final Session session = getSession();

        // EnrollmentRecommendation
        List<EnrollmentRecommendation> enrollmentRecommendationForSave = new ArrayList<EnrollmentRecommendation>(model.getRecomendationList());
        for (EntrantEnrolmentRecommendation item : getList(EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.L_ENTRANT, model.getEntrant()))
        {
            if (!model.getRecomendationList().contains(item.getRecommendation()))
                session.delete(item);
            else
                enrollmentRecommendationForSave.remove(item.getRecommendation());
        }
        for (EnrollmentRecommendation enrollmentRecommendation : enrollmentRecommendationForSave)
        {
            EntrantEnrolmentRecommendation item = new EntrantEnrolmentRecommendation();
            item.setEntrant(model.getEntrant());
            item.setRecommendation(enrollmentRecommendation);
            session.save(item);
        }

        // AccessCourse
        List<AccessCourse> accessCourseForSave = new ArrayList<AccessCourse>(model.getAccessCourseList());
        for (EntrantAccessCourse item : getList(EntrantAccessCourse.class, EntrantAccessCourse.L_ENTRANT, model.getEntrant()))
        {
            if (!model.getAccessCourseList().contains(item.getCourse()))
                session.delete(item);
            else
                accessCourseForSave.remove(item.getCourse());
        }
        for (AccessCourse accessCourse : accessCourseForSave)
        {
            EntrantAccessCourse item = new EntrantAccessCourse();
            item.setEntrant(model.getEntrant());
            item.setCourse(accessCourse);
            session.save(item);
        }

        // AccessDepartment
        List<AccessDepartment> accessDepartmentForSave = new ArrayList<AccessDepartment>(model.getAccessDepartmentList());
        for (EntrantAccessDepartment item : getList(EntrantAccessDepartment.class, EntrantAccessDepartment.L_ENTRANT, model.getEntrant()))
        {
            if (!model.getAccessDepartmentList().contains(item.getAccessDepartment()))
                session.delete(item);
            else
                accessDepartmentForSave.remove(item.getAccessDepartment());
        }
        for (AccessDepartment accessDepartment : accessDepartmentForSave)
        {
            EntrantAccessDepartment item = new EntrantAccessDepartment();
            item.setEntrant(model.getEntrant());
            item.setAccessDepartment(accessDepartment);
            session.save(item);
        }

        // SourceInfoAboutUniversity
        List<SourceInfoAboutUniversity> sourceInfoAboutUniversityForSave = new ArrayList<SourceInfoAboutUniversity>(model.getSourceInfoAboutUniversityList());
        for (EntrantInfoAboutUniversity item : getList(EntrantInfoAboutUniversity.class, EntrantInfoAboutUniversity.L_ENTRANT, model.getEntrant()))
        {
            if (!model.getSourceInfoAboutUniversityList().contains(item.getSourceInfo()))
                session.delete(item);
            else
                sourceInfoAboutUniversityForSave.remove(item.getSourceInfo());
        }
        for (SourceInfoAboutUniversity sourceInfoAboutUniversity : sourceInfoAboutUniversityForSave)
        {
            EntrantInfoAboutUniversity item = new EntrantInfoAboutUniversity();
            item.setEntrant(model.getEntrant());
            item.setSourceInfo(sourceInfoAboutUniversity);
            session.save(item);
        }

        if (!model.getEntrant().isPassArmy())
        {
            model.getEntrant().setBeginArmy(null);
            model.getEntrant().setEndArmy(null);
            model.getEntrant().setEndArmyYear(null);
        }

        session.update(model.getEntrant());
    }
}
