/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentCampaignList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.HashMap;
import java.util.Map;

/**
 * @author vip_delete
 * @since 11.02.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentCampaign> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", EnrollmentCampaign.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Учебный год", EnrollmentCampaign.educationYear().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Период приема", EnrollmentCampaign.P_PERIOD_LIST_TITLE).setClickable(false).setOrderable(false));

        ToggleColumn openedColumn = new ToggleColumn("Открыта", EnrollmentCampaign.P_CLOSED, "Закрыта", "Открыта");
        IndicatorColumn.Item onItem = new IndicatorColumn.Item(ToggleColumn.TOGGLED_ON, "Открыта");
        onItem.setPrintLabel(ToggleColumn.DEFAULT_TRUE_LABEL);
        openedColumn.indicator(Boolean.FALSE, onItem);
        IndicatorColumn.Item offItem = new IndicatorColumn.Item(ToggleColumn.TOGGLED_OFF, "Закрыта");
        offItem.setPrintLabel(ToggleColumn.DEFAULT_FALSE_LABEL);
        openedColumn.indicator(Boolean.TRUE, offItem);
        openedColumn.toggleOnListener("onTurnClose").toggleOffListener("onTurnOpen");
        dataSource.addColumn(openedColumn);

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEnrollmentCampaign"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEnrollmentCampaign", "Удалить приемную кампанию {0}?", EnrollmentCampaign.P_TITLE));

        model.setDataSource(dataSource);
    }

    public void onClickAddEnrollmentCampaign(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_CAMPAIGN_ADD_EDIT, new ParametersMap()
                .add("enrollmentCampaignId", null)
        ));
    }

    public void onClickCopyEnrollmentCampaign(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_CAMPAIGN_COPY, new ParametersMap()));
    }

    public void onClickEditEnrollmentCampaign(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENROLLMENT_CAMPAIGN_ADD_EDIT, new ParametersMap()
                .add("enrollmentCampaignId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteEnrollmentCampaign(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }

    public void onTurnClose(IBusinessComponent component)
    {
        getDao().setEnrollmentCampaignClose(component.getListenerParameter(), true);
    }

    public void onTurnOpen(IBusinessComponent component)
    {
        getDao().setEnrollmentCampaignClose(component.getListenerParameter(), false);
    }
}
