/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.BlockParPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "paragraphId")
})
public class EcOrderBlockParPubUI extends UIPresenter
{
    private Long _paragraphId;
    private EnrollmentParagraph _paragraph;
    private EnrollmentExtract _extract;
    private EnrollmentOrderType _type;

    @Override
    public void onComponentRefresh()
    {
        _paragraph = DataAccessServices.dao().getNotNull(_paragraphId);
        _extract = _paragraph.getFirstExtract();
        _type = EcOrderManager.instance().dao().getEnrollmentOrderType(((EnrollmentOrder) _paragraph.getOrder()).getEnrollmentCampaign(), ((EnrollmentOrder) _paragraph.getOrder()).getType());
    }

    // Getters & Setters

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public EnrollmentParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EnrollmentParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public EnrollmentExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(EnrollmentExtract extract)
    {
        _extract = extract;
    }

    public EnrollmentOrderType getType()
    {
        return _type;
    }

    public void setType(EnrollmentOrderType type)
    {
        _type = type;
    }
}
