/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProtocolVisaList;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author vip_delete
 * @since 21.07.2009
 */
public interface IProtocolEntity
{
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_COMPENSATION_TYPE = "compensationType";


    String getFio();

    void setFio(String fio);

    EnrollmentCampaign getEnrollmentCampaign();

    void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign);

    OrgUnit getOrgUnit();

    void setOrgUnit(OrgUnit orgUnit);

    CompensationType getCompensationType();

    void setCompensationType(CompensationType compensationType);
}
