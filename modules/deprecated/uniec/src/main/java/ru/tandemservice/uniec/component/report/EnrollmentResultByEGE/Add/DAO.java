/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByEGEReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(CompensationType.class), CompensationType.P_SHORT_TITLE));
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EnrollmentResultByEGEReport report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(CommonBaseStringUtil.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));

        if (model.isQualificationActive())
            report.setQualificationTitle(CommonBaseStringUtil.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));

        if (model.isCompensationTypeActive())
            report.setCompensationTypeTitle(model.getCompensationType().getShortTitle());

        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnitTitle(CommonBaseStringUtil.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));

        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnitTitle(CommonBaseStringUtil.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));

        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchoolTitle(CommonBaseStringUtil.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "; "));

        if (model.isDevelopFormActive())
            report.setDevelopFormTitle(CommonBaseStringUtil.join(model.getDevelopFormList(), DevelopForm.P_TITLE, "; "));

        if (model.isDevelopConditionActive())
            report.setDevelopConditionTitle(CommonBaseStringUtil.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));

        if (model.isDevelopTechActive())
            report.setDevelopTechTitle(CommonBaseStringUtil.join(model.getDevelopTechList(), DevelopTech.P_TITLE, "; "));

        if (model.isDevelopPeriodActive())
            report.setDevelopPeriodTitle(CommonBaseStringUtil.join(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, "; "));

        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        DatabaseFile databaseFile = new EnrollmentResultByEGEReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
