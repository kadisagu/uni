/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EnrollmentDirectionsAdmissionPlan;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

/**
 * @author agolubenko
 * @since 21.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        Session session = getSession();
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, session);
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(true, MultiEnrollmentDirectionUtil.Parameters.Filter.DEVELOP_FORM);
        reloadModelEntities(model);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void createReport(Model model)
    {
        IScriptItem templateDocument = getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_DIRECTION_ADMISSION_PLAN);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        model.setContentType("text/rtf");
        model.setFileName(templateDocument.getTemplateFileName());

        RtfDocument document = template.getClone();

        // заполнить год и форму обучения
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("year", Integer.toString(model.getEnrollmentCampaign().getEducationYear().getIntValue()));
        modifier.put("educationForm", model.getDevelopForm().getTitle());
        modifier.put("vuz", TopOrgUnit.getInstance().getTitle());
        modifier.modify(document);

        // заполнить параметры отчета
        List<String[]> parameters = new ArrayList<>();
        if (model.isQualificationActive())
            addParameterRow(parameters, "Квалификация:", UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, ", "));
        if (model.isFormativeOrgUnitActive())
            addParameterRow(parameters, "Формирующее подр.:", UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_TITLE, ", "));
        if (model.isTerritorialOrgUnitActive())
            addParameterRow(parameters, "Территориальное подр.:", UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_TITLE, ", "));
        if (model.isEducationLevelHighSchoolActive())
            addParameterRow(parameters, "Направление подготовки (специальность):", UniStringUtils.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, ", "));
        addParameterRow(parameters, "Форма освоения:", model.getDevelopForm().getTitle());
        if (model.isDevelopConditionActive())
            addParameterRow(parameters, "Условие освоения:", UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));
        if (model.isDevelopTechActive())
            addParameterRow(parameters, "Технология освоения:", UniStringUtils.join(model.getDevelopTechList(), DevelopTech.P_TITLE, ", "));
        if (model.isDevelopPeriodActive())
            addParameterRow(parameters, "Срок освоения:", UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, ", "));
        new RtfTableModifier().put("P", parameters.toArray(new String[0][])).modify(document);

        // найти направления приема удовлетворяющие параметрам отчета
        List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections(model);

        // рассортировать направления приема, сформировав неупорядоченную структуру отчета
        Map<OrgUnit, Map<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>>> data = prepareData(enrollmentDirections);

        // получить детализацию плана целевого приема по всем направлениям
        Map<EnrollmentDirection, Map<TargetAdmissionKind, Integer>> direction2TargetAdmissionKind2Plan = SafeMap.get(key -> new HashMap<>());
        final List<TargetAdmissionKind> targetAdmissionKinds = getTargetAdmissionKinds(enrollmentDirections, direction2TargetAdmissionKind2Plan);

        // сформировать упорядоченный список строк
        final List<Row> rows = getRows(data, direction2TargetAdmissionKind2Plan, targetAdmissionKinds);

        // напечатать их по шаблону
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", toArray(rows));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            // перед этим
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // если целевой прием детализирован по двум и более видам
                if (targetAdmissionKinds.size() > 1)
                {
                    // разбить колонку целевого приема
                    int[] parts = new int[targetAdmissionKinds.size() + 1];
                    Arrays.fill(parts, 1);

                    int targetTotalPlanColumn = 3;
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), targetTotalPlanColumn, (newCell, index) -> {
                        if (index > 0) // в первой подколонке "Всего"
                        {
                            // в остальных напечатать сокращенные названия видов целевого приема
                            newCell.getElementList().set(0, RtfBean.getElementFactory().createRtfText(targetAdmissionKinds.get(index - 1).getShortTitle()));
                        }
                    }, parts);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), targetTotalPlanColumn, null, parts);
                }
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                Row dataRow = rows.get(rowIndex);
                if (dataRow instanceof Level2Row)
                {
                    if (colIndex == 0)
                    {
                        cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QC));
                        return new RtfString().fontSize(24).append(value).toList();
                    }
                } else if (dataRow instanceof Level1Row || dataRow instanceof Level3Row)
                {
                    // set bold
                    RtfString rtfString = new RtfString().boldBegin().append(value).boldEnd();

                    if (dataRow instanceof Level3Row)
                        cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QC));

                    return rtfString.toList();
                }

                return null;
            }

            @Override
            // после этого
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // пройтись по всем строкам
                for (int i = 0; i < rows.size(); i++)
                {
                    Row row = rows.get(i);

                    if (row instanceof Level2Row)
                    {
                        RtfRow rtfRow = newRowList.get(startIndex + i);
                        RtfUtil.unitAllCell(rtfRow, 0);
                    }
                }
            }
        });
        tableModifier.modify(document);

        model.setContent(RtfUtil.toByteArray(document));
    }

    /**
     * Добавляет параметр для отображения в шапке отчета, если он был задан
     *
     * @param parameters список строк для параметров отчета
     * @param title      название параметра
     * @param value      значение параметра
     */
    private void addParameterRow(List<String[]> parameters, String title, String value)
    {
        if (!StringUtils.isEmpty(value))
        {
            parameters.add(new String[]{title, value});
        }
    }

    /**
     * @param enrollmentDirections список направлений приема
     * @return неупорядоченная структура отчета (рассортированные направления приема)
     */
    private Map<OrgUnit, Map<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>>> prepareData(List<EnrollmentDirection> enrollmentDirections)
    {
        Map<OrgUnit, Map<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>>> result = SafeMap.get(
                key -> SafeMap.get(key1 -> SafeMap.get(key2 -> SafeMap.get(key3 -> SafeMap.get(key4 -> new ArrayList<>()))))
        );

        // сгруппировать направления приема
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            OrgUnit formativeOrgUnit = getFormativeOrgUnit(enrollmentDirection);
            EducationLevels educationLevel = getEducationLevel(enrollmentDirection);
            StructureEducationLevels structureLevel = educationLevel.getLevelType();
            while (structureLevel.getParent() != null) structureLevel = structureLevel.getParent();
            EducationLevelsHighSchool educationLevelHighSchool = getEducationLevelHighSchool(enrollmentDirection);
            OrgUnit territorialOrgUnit = getTerritorialOrgUnit(enrollmentDirection);

            // сначала по формирующему орг. юниту, затем по направлению подготовки (специальности) ВПО, затем по направлению подготовки (специальности) ОУ, затем по территориальному орг. юниту
            result.get(formativeOrgUnit).get(structureLevel).get(educationLevel).get(educationLevelHighSchool).get(territorialOrgUnit).add(enrollmentDirection);
        }

        return result;
    }

    /**
     * @param enrollmentDirections список направлений приема
     * @param direction2TargetAdmissionKind2Plan
     *                             направление приема -> вид целевого приема -> план по этому виду
     * @return список видов целевого приема, которые заданы хотя бы для одного направления приема
     */
    @SuppressWarnings("unchecked")
    private List<TargetAdmissionKind> getTargetAdmissionKinds(Collection<EnrollmentDirection> enrollmentDirections, Map<EnrollmentDirection, Map<TargetAdmissionKind, Integer>> direction2TargetAdmissionKind2Plan)
    {
        if (enrollmentDirections.isEmpty())
        {
            return Collections.emptyList();
        }

        Criteria criteria = getSession().createCriteria(TargetAdmissionPlanRelation.class);
        criteria.add(Restrictions.in(TargetAdmissionPlanRelation.L_ENTRANCE_EDUCATION_ORG_UNIT, enrollmentDirections));
        List<TargetAdmissionPlanRelation> relations = criteria.list();

        Set<TargetAdmissionKind> targetAdmissionKinds = new HashSet<>();
        for (TargetAdmissionPlanRelation relation : relations)
        {
            EnrollmentDirection enrollmentDirection = relation.getEntranceEducationOrgUnit();
            TargetAdmissionKind targetAdmissionKind = relation.getTargetAdmissionKind();
            int planValue = relation.getPlanValue();

            direction2TargetAdmissionKind2Plan.get(enrollmentDirection).put(targetAdmissionKind, planValue);
            targetAdmissionKinds.add(targetAdmissionKind);
        }

        List<TargetAdmissionKind> result = new ArrayList<>(targetAdmissionKinds);
        Collections.sort(result, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);
        return result;
    }

    /**
     * @param rows список строк
     * @return таблица ячеек
     */
    private String[][] toArray(List<Row> rows)
    {
        String[][] result = new String[rows.size()][];
        for (int i = 0; i < rows.size(); i++)
        {
            result[i] = rows.get(i).toStringArray();
        }
        return result;
    }

    /**
     * @param data                 Формирующий орг. юнит -> Направление подготовки (специальности) ВПО -> Направление подготовки (специальности) ВПО -> Территориальный орг. юнит -> Список направлений приема
     * @param direction2TargetAdmissionKind2Plan
     *                             Направление приема -> Вид целевого приема -> План
     * @param targetAdmissionKinds Список направлений целевого приема
     * @return упорядоченный список строк отчета, с подсчитанными значениями плана
     */
    private List<Row> getRows(Map<OrgUnit, Map<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>>> data, Map<EnrollmentDirection, Map<TargetAdmissionKind, Integer>> direction2TargetAdmissionKind2Plan, List<TargetAdmissionKind> targetAdmissionKinds)
    {
        final List<Row> result = new ArrayList<>();
        Row totalRow = new Level1Row(false, "ИТОГО ПО ОУ", targetAdmissionKinds);

        for (Map.Entry<Level1Row, Map<Level2Row, Map<Level3Row, List<Level4Row>>>> level1Entry : getRowsMap(data, direction2TargetAdmissionKind2Plan, targetAdmissionKinds).entrySet())
        {
            Level1Row level1Row = level1Entry.getKey();
            result.add(level1Row);

            for (Map.Entry<Level2Row, Map<Level3Row, List<Level4Row>>> level2Entry : level1Entry.getValue().entrySet())
            {
                Level2Row level2Row = level2Entry.getKey();
                result.add(level2Row);

                for (Map.Entry<Level3Row, List<Level4Row>> level3Entry : level2Entry.getValue().entrySet())
                {
                    Level3Row level3Row = level3Entry.getKey();
                    result.add(level3Row);

                    for (Level4Row row : level3Entry.getValue())
                    {
                        result.add(row);

                        level3Row.add(row);
                    }
                    level2Row.add(level3Row);
                }
                level1Row.add(level2Row);
            }
            totalRow.add(level1Row);
        }
        result.add(totalRow);

        return result;
    }

    /**
     * Создает структуру строк отчета
     *
     * @param data                 Формирующий орг. юнит -> Направление подготовки (специальности) ВПО -> Направление подготовки (специальности) ВПО -> Территориальный орг. юнит -> Список направлений приема
     * @param direction2TargetAdmissionKind2Plan
     *                             Направление приема -> Вид целевого приема -> План
     * @param targetAdmissionKinds Список направлений целевого приема
     * @return Строка первого уровня -> Строка второго уровня -> Список строк третьего уровня
     */
    private Map<Level1Row, Map<Level2Row, Map<Level3Row, List<Level4Row>>>> getRowsMap(Map<OrgUnit, Map<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>>> data, Map<EnrollmentDirection, Map<TargetAdmissionKind, Integer>> direction2TargetAdmissionKind2Plan, List<TargetAdmissionKind> targetAdmissionKinds)
    {
        Map<Level1Row, Map<Level2Row, Map<Level3Row, List<Level4Row>>>> result = new TreeMap<>();
        for (Map.Entry<OrgUnit, Map<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>>> level1Entry : data.entrySet())
        {
            Map<Level2Row, Map<Level3Row, List<Level4Row>>> level2Rows = new TreeMap<>();
            result.put(createLevel1Row(level1Entry.getKey(), targetAdmissionKinds), level2Rows);

            for (Map.Entry<StructureEducationLevels, Map<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>>> level2Entry : level1Entry.getValue().entrySet())
            {
                Map<Level3Row, List<Level4Row>> level3Rows = new TreeMap<>();
                level2Rows.put(createLevel2Row(level2Entry.getKey(), targetAdmissionKinds), level3Rows);

                for (Map.Entry<EducationLevels, Map<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>>> level3Entry : level2Entry.getValue().entrySet())
                {
                    Map<MultiKey, List<Level4Row>> oksoQualificationAndEducationLevelHighSchool2Rows = SafeMap.get(key -> new ArrayList<>());
                    List<Level4Row> level4Rows = new ArrayList<>();
                    level3Rows.put(createLevel3Row(level3Entry.getKey(), targetAdmissionKinds), level4Rows);

                    for (Map.Entry<EducationLevelsHighSchool, Map<OrgUnit, List<EnrollmentDirection>>> leve4Entry : level3Entry.getValue().entrySet())
                    {
                        EducationLevelsHighSchool educationLevelHighSchool = leve4Entry.getKey();
                        for (Map.Entry<OrgUnit, List<EnrollmentDirection>> fourthLevelEntry : leve4Entry.getValue().entrySet())
                        {
                            Level4Row level4Row = createLevel4Row(fourthLevelEntry.getKey(), educationLevelHighSchool, targetAdmissionKinds, fourthLevelEntry.getValue(), direction2TargetAdmissionKind2Plan);
                            oksoQualificationAndEducationLevelHighSchool2Rows.get(new MultiKey(level4Row.okso, level4Row.educationLevelHighSchoolTitle)).add(level4Row);
                            level4Rows.add(level4Row);
                        }
                    }
                    // для каждой получившейся группы строк
                    for (List<Level4Row> rows : oksoQualificationAndEducationLevelHighSchool2Rows.values())
                    {
                        // для каждой строки в группе
                        for (Level4Row row : rows)
                        {
                            // сформировать название с учетом количества строк в группе
                            row.fillTitle(rows.size() != 1);
                        }
                    }
                    // отсортировать строки
                    Collections.sort(level4Rows);
                }
            }
        }
        return result;
    }

    /**
     * @param formativeOrgUnit     формирующий орг. юнит
     * @param targetAdmissionKinds виды целевого приема
     * @return строка первого уровня
     */
    private Level1Row createLevel1Row(OrgUnit formativeOrgUnit, List<TargetAdmissionKind> targetAdmissionKinds)
    {
        String title = (formativeOrgUnit instanceof TopOrgUnit) ? (String) formativeOrgUnit.getProperty(new String[]{OrgUnit.L_ADDRESS, Address.L_SETTLEMENT, AddressItem.P_TITLE_WITH_TYPE}) : (StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getFullTitle() : formativeOrgUnit.getNominativeCaseTitle());
        return new Level1Row(formativeOrgUnit instanceof TopOrgUnit, title, targetAdmissionKinds);
    }

    private Level2Row createLevel2Row(StructureEducationLevels structureLevel, List<TargetAdmissionKind> targetAdmissionKinds)
    {
        return new Level2Row(targetAdmissionKinds, structureLevel);
    }

    /**
     * @param educationLevel       направление подготовки (специальности) ВПО
     * @param targetAdmissionKinds виды целевого приема
     * @return строка второго уровня
     */
    private Level3Row createLevel3Row(EducationLevels educationLevel, List<TargetAdmissionKind> targetAdmissionKinds)
    {
        return new Level3Row(educationLevel.getTitle(), educationLevel.getTitleCodePrefix(), targetAdmissionKinds);
    }

    /**
     * @param territorialOrgUnit       территориальный орг. юнит
     * @param educationLevelHighSchool направление подготовки (специальности) ОУ
     * @param targetAdmissionKinds     виды целевого приема
     * @param enrollmentDirections     направления приема
     * @param direction2TargetAdmissionKind2Plan
     *                                 детализация плана по видам целового приема
     * @return строка третьего уровня, объединяющая в себе несколько направлений приема
     */
    private Level4Row createLevel4Row(OrgUnit territorialOrgUnit, EducationLevelsHighSchool educationLevelHighSchool, List<TargetAdmissionKind> targetAdmissionKinds, List<EnrollmentDirection> enrollmentDirections, Map<EnrollmentDirection, Map<TargetAdmissionKind, Integer>> direction2TargetAdmissionKind2Plan)
    {
        EducationLevels educationLevel = educationLevelHighSchool.getEducationLevel();

        Level4Row result = new Level4Row(null, educationLevel.getTitleCodePrefix(), targetAdmissionKinds);
        result.educationLevelHighSchoolTitle = educationLevelHighSchool.getTitle();
        result.graduatingOrgUnitTitle = educationLevelHighSchool.getOrgUnit().getShortTitle();
        result.territorialOrgUnitTitle = ((territorialOrgUnit != null && territorialOrgUnit.getAddress() != null) ? territorialOrgUnit.getAddress().getSettlement().getTitle() : "");

        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            if (enrollmentDirection.isBudget()) {
                result.addBudgetTotalPlan(enrollmentDirection.getMinisterialPlan());
                result.addTargetTotalPlan(enrollmentDirection.getTargetAdmissionPlanBudget());
            }
            if (enrollmentDirection.isContract()) {
                result.addContractTotalPlan(enrollmentDirection.getContractPlan());
            }

            result.addAll(direction2TargetAdmissionKind2Plan.get(enrollmentDirection));
        }
        return result;
    }

    /**
     * @param model модель
     * @return список направлений приема, удовлетворяющих параметрам выборки
     */
    @SuppressWarnings("unchecked")
    private List<EnrollmentDirection> getEnrollmentDirections(Model model)
    {
        Criteria criteria = getSession().createCriteria(EnrollmentDirection.class);
        criteria.createAlias(EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.add(Restrictions.eq(EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        if (model.isQualificationActive())
        {
            criteria.createAlias("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "educationLevelHighSchool");
            criteria.createAlias("educationLevelHighSchool." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "educationLevel");
            criteria.add(Restrictions.in("educationLevel." + EducationLevels.L_QUALIFICATION, model.getQualificationList()));
        }
        if (model.isFormativeOrgUnitActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnitList()));
        }
        if (model.isTerritorialOrgUnitActive())
        {
            if (!model.getTerritorialOrgUnitList().isEmpty())
            {
                criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnitList()));
            } else
            {
                criteria.add(Restrictions.isNull("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT));
            }
        }
        if (model.isEducationLevelHighSchoolActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelHighSchoolList()));
        }
        criteria.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
        if (model.isDevelopConditionActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionList()));
        }
        if (model.isDevelopTechActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTechList()));
        }
        if (model.isDevelopPeriodActive())
        {
            criteria.add(Restrictions.in("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodList()));
        }
        return criteria.list();
    }

    /**
     * @param enrollmentDirection направление приема
     * @return формирующий орг. юнит
     */
    private OrgUnit getFormativeOrgUnit(EnrollmentDirection enrollmentDirection)
    {
        OrgUnit result = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit();

        String orgUnitTypeCode = result.getOrgUnitType().getCode();
        if (!orgUnitTypeCode.equals("branch") && !orgUnitTypeCode.equals("representation"))
            result = TopOrgUnit.getInstance();

        return result;
    }

    /**
     * @param enrollmentDirection направление приема
     * @return территориальный орг. юнит
     */
    private OrgUnit getTerritorialOrgUnit(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getTerritorialOrgUnit();
    }

    /**
     * @param enrollmentDirection направление приема
     * @return направление подготовки (специальности) ВПО
     */
    private EducationLevels getEducationLevel(EnrollmentDirection enrollmentDirection)
    {
        EducationLevels result = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        while (result.getParentLevel() != null)
        {
            result = result.getParentLevel();
        }
        return result;
    }

    /**
     * @param enrollmentDirection направление приема
     * @return направление подготовки (специальности) ОУ
     */
    private EducationLevelsHighSchool getEducationLevelHighSchool(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool();
    }

    /**
     * Строка отчета
     *
     * @author agolubenko
     * @since 24.04.2009
     */
    static abstract class Row implements Comparable<Row>
    {
        String title;
        String okso;
        Integer budgetTotalPlan;
        Integer contractTotalPlan;
        Integer targetTotalPlan;
        Map<TargetAdmissionKind, Integer> targetAdmissionKind2Plan = new LinkedHashMap<>();

        public Row(String title, String okso, List<TargetAdmissionKind> targetAdmissionKinds)
        {
            this.title = title;
            this.okso = okso;

            // сразу создать задать порядок видов целевого приема (LinkedHashMap)
            for (TargetAdmissionKind targetAdmissionKind : targetAdmissionKinds)
            {
                targetAdmissionKind2Plan.put(targetAdmissionKind, null);
            }
        }

        /**
         * Добавляет значения планов по видам целевого приема
         *
         * @param targetAdmissionKind2Plan Вид целевого приема -> План
         */
        public void addAll(Map<TargetAdmissionKind, Integer> targetAdmissionKind2Plan)
        {
            for (Map.Entry<TargetAdmissionKind, Integer> entry : targetAdmissionKind2Plan.entrySet())
            {
                TargetAdmissionKind targetAdmissionKind = entry.getKey();
                Integer value = entry.getValue();

                if (value != null)
                {
                    this.targetAdmissionKind2Plan.put(targetAdmissionKind, UniBaseUtils.safeSum(this.targetAdmissionKind2Plan.get(targetAdmissionKind), value));
                }
            }
        }

        /**
         * Добавляет значение плана по итоговому плану бюджета
         *
         * @param value значение плана
         */
        public void addBudgetTotalPlan(Integer value)
        {
            budgetTotalPlan = UniBaseUtils.safeSum(budgetTotalPlan, value);
        }

        /**
         * Добавляет значение плана по итоговому плану целевого приема
         *
         * @param value значение плана
         */
        public void addTargetTotalPlan(Integer value)
        {
            targetTotalPlan = UniBaseUtils.safeSum(targetTotalPlan, value);
        }

        /**
         * Добавляет значение плана по итоговому плану контракта
         *
         * @param value значения плана
         */
        public void addContractTotalPlan(Integer value)
        {
            contractTotalPlan = UniBaseUtils.safeSum(contractTotalPlan, value);
        }

        /**
         * Добавляет значения планов другой строки к этой
         *
         * @param row строка
         */
        public void add(Row row)
        {
            addBudgetTotalPlan(row.budgetTotalPlan);
            addTargetTotalPlan(row.targetTotalPlan);
            addContractTotalPlan(row.contractTotalPlan);

            addAll(row.targetAdmissionKind2Plan);
        }

        public String[] toStringArray()
        {
            String[] result = getCells();

            // в пустых ячейках надо показывать '-'
            for (int i = 2; i < result.length; i++)
            {
                if (result[i] == null)
                {
                    result[i] = "-";
                }
            }

            return result;
        }

        public abstract String[] getCells();
    }

    static class Level1Row extends Row
    {
        private boolean _academy;

        public Level1Row(boolean academy, String title, List<TargetAdmissionKind> targetAdmissionKinds)
        {
            super(title, null, targetAdmissionKinds);
            _academy = academy;
        }

        @Override
        public String[] getCells()
        {
            int taCount = targetAdmissionKind2Plan.size();

            String[] result = new String[5 + (taCount > 1 ? taCount : 0)];
            result[0] = title;
            result[1] = okso;
            result[2] = UniBaseUtils.toString(budgetTotalPlan);
            result[3] = UniBaseUtils.toString(targetTotalPlan);
            result[result.length - 1] = UniBaseUtils.toString(contractTotalPlan);

            int index = 4;
            if (taCount > 1) {
                for (Integer plan : targetAdmissionKind2Plan.values()) {
                    result[index++] = UniBaseUtils.toString(plan);
                }
            }

            return result;
        }

        @Override
        public int compareTo(Row o)
        {
            return _academy ? -1 : title.compareTo(o.title);
        }
    }

    static class Level2Row extends Row
    {
        private StructureEducationLevels _stuctureLevel;

        Level2Row(List<TargetAdmissionKind> targetAdmissionKinds, StructureEducationLevels stuctureLevel)
        {
            super(stuctureLevel.getTitle(), null, targetAdmissionKinds);
            _stuctureLevel = stuctureLevel;
        }

        @Override
        public String[] getCells()
        {
            String[] result = new String[5 + targetAdmissionKind2Plan.size()];
            result[0] = _stuctureLevel.getTitle();
            return result;
        }

        @Override
        public int compareTo(Row o)
        {
            return Integer.compare(_stuctureLevel.getPriority(), ((Level2Row) o)._stuctureLevel.getPriority());
        }
    }

    static class Level3Row extends Row
    {
        public Level3Row(String title, String okso, List<TargetAdmissionKind> targetAdmissionKinds)
        {
            super(title, okso, targetAdmissionKinds);
        }

        @Override
        public String[] getCells()
        {
            String[] result = new String[5 + targetAdmissionKind2Plan.size()];
            result[0] = title;
            result[1] = okso;
            return result;
        }

        @Override
        public int compareTo(Row o)
        {
            return okso == null ? (o.okso == null ? 0 : -1) : (o.okso == null ? 1 : okso.compareTo(o.okso));
        }
    }

    static class Level4Row extends Row
    {
        String educationLevelHighSchoolTitle;
        String graduatingOrgUnitTitle;
        String territorialOrgUnitTitle;

        public Level4Row(String title, String okso, List<TargetAdmissionKind> targetAdmissionKinds)
        {
            super(title, okso, targetAdmissionKinds);
        }

        public void fillTitle(boolean includeGraduatingOrgUnit)
        {
            title = okso + " " + educationLevelHighSchoolTitle;
            if (includeGraduatingOrgUnit)
            {
                title += " (" + graduatingOrgUnitTitle + ")";
            }
            if (!StringUtils.isEmpty(territorialOrgUnitTitle))
            {
                title += " (" + territorialOrgUnitTitle + ")";
            }
        }

        @Override
        public String[] getCells()
        {
            int taCount = targetAdmissionKind2Plan.size();

            String[] result = new String[5 + (taCount > 1 ? taCount : 0)];
            result[0] = title;
            result[1] = okso;
            result[2] = UniBaseUtils.toString(budgetTotalPlan);
            result[3] = UniBaseUtils.toString(targetTotalPlan);
            result[result.length - 1] = UniBaseUtils.toString(contractTotalPlan);

            int index = 4;
            if (taCount > 1) {
                for (Integer plan : targetAdmissionKind2Plan.values()) {
                    result[index++] = UniBaseUtils.toString(plan);
                }
            }

            return result;
        }

        @Override
        public int compareTo(Row o)
        {
            int result = okso.compareTo(o.okso);
            if (result != 0)
            {
                return result;
            }
            return territorialOrgUnitTitle.compareTo(((Level4Row) o).territorialOrgUnitTitle);
        }
    }
}