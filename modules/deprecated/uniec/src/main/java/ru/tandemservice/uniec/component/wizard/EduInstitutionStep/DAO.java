/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EduInstitutionStep;

import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author vip_delete
 * @since 14.06.2009
 */
public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionAddEdit.DAO implements IDAO
{
    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionAddEdit.Model m)
    {
        Model model = (Model) m;
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        m.setPersonId(model.getEntrant().getPerson().getId());
        m.setPersonRoleName("Entrant");

        // если есть основной документ об образовании, то показываем его
        if (model.getEntrant().getPerson().getPersonEduInstitution() != null)
            m.setPersonEduInstitution(model.getEntrant().getPerson().getPersonEduInstitution());

        super.prepare(m);

        Long onlineEntrantId = model.getOnlineEntrant().getId();
        if (onlineEntrantId != null)
        {
            model.setOnlineEntrant(get(OnlineEntrant.class, onlineEntrantId));
            prepareOnlineEntrantEduInstitution(model);
        }
    }

    protected void prepareOnlineEntrantEduInstitution(Model model)
    {
        OnlineEntrant onlineEntrant = model.getOnlineEntrant();

        EduInstitution eduInstitution = onlineEntrant.getEduInstitution();
        PersonEduInstitution personEduInstitution = model.getPersonEduInstitution();
        AddressItem settlement = onlineEntrant.getEduInstitutionSettlement();

        // законченное образовательное учреждение
        model.setAddressCountry(onlineEntrant.getEduInstitutionCountry());
        model.setAddressItem(settlement);
        if (eduInstitution != null)
        {
            EducationalInstitutionTypeKind eduInstitutionKind = eduInstitution.getEduInstitutionKind();
            if (eduInstitutionKind != null)
            {
                model.setEduInstitutionType(eduInstitutionKind.getParent());
                model.setEduInstitutionKind(eduInstitutionKind);
            }
        }
        personEduInstitution.setEduInstitution(eduInstitution);

        // документ о полученном образовании
        model.setEducationLevelStage(onlineEntrant.getEduDocumentLevel());
        personEduInstitution.setDocumentType(onlineEntrant.getEduDocumentType());
        personEduInstitution.setRegionCode((settlement != null) ? settlement.getInheritedRegionCode() : null);
        personEduInstitution.setSeria(onlineEntrant.getEduDocumentSeria());
        personEduInstitution.setNumber(onlineEntrant.getEduDocumentNumber());
        model.setYearEnd(onlineEntrant.getEduDocumentYearEnd());
        model.setIssuanceDate(onlineEntrant.getEduDocumentDate());
        model.setGraduationHonour(onlineEntrant.getEduDocumentGraduationHonour());

        // статистика оценок
        personEduInstitution.setMark5(onlineEntrant.getMark5());
        personEduInstitution.setMark4(onlineEntrant.getMark4());
        personEduInstitution.setMark3(onlineEntrant.getMark3());
    }
}
