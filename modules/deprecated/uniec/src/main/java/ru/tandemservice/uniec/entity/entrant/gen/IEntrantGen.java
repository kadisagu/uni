package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.IEntrant;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniec.entity.entrant.IEntrant;

/**
 * Абстрактный абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEntrantGen extends InterfaceStubBase
 implements IEntrant{
    public static final int VERSION_HASH = 1082358311;

    public static final String P_ENTRANT_ID = "entrantId";

    private long _entrantId;

    @NotNull

    public long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(long entrantId)
    {
        _entrantId = entrantId;
    }

    private static final Path<IEntrant> _dslPath = new Path<IEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniec.entity.entrant.IEntrant");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.IEntrant#getEntrantId()
     */
    public static PropertyPath<Long> entrantId()
    {
        return _dslPath.entrantId();
    }

    public static class Path<E extends IEntrant> extends EntityPath<E>
    {
        private PropertyPath<Long> _entrantId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.IEntrant#getEntrantId()
     */
        public PropertyPath<Long> entrantId()
        {
            if(_entrantId == null )
                _entrantId = new PropertyPath<Long>(IEntrantGen.P_ENTRANT_ID, this);
            return _entrantId;
        }

        public Class getEntityClass()
        {
            return IEntrant.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniec.entity.entrant.IEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
