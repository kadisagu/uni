package ru.tandemservice.uniec.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип вступительного испытания"
 * Имя сущности : entranceDisciplineType
 * Файл data.xml : uniec.catalogs.data.xml
 */
public interface EntranceDisciplineTypeCodes
{
    /** Константа кода (code) элемента : Обычное (title) */
    String USUAL = "1";
    /** Константа кода (code) элемента : Профильной направленности (title) */
    String PROFILE = "2";
    /** Константа кода (code) элемента : Творческой и (или) профессиональной направленности (title) */
    String CREATIVE_PROFESSIONAL = "3";

    Set<String> CODES = ImmutableSet.of(USUAL, PROFILE, CREATIVE_PROFESSIONAL);
}
