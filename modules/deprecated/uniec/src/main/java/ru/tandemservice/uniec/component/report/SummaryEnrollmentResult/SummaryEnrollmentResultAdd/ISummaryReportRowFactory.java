/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import java.util.List;

import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * Фабрика создания списка наименьших элементов строки по направлению приема
 * В одной строке отчета может быть несколько направлений приема, в этом случае список ISummaryRowItem
 * объединяется в строке отчета: SummaryReportRow.appendRowItems
 * 
 * После того как строка полностью наполнена, идет подсчет статистики: SummaryReportRow.buildRow
 *
 * @author vip_delete
 * @since 23.04.2009
 */
interface ISummaryReportRowFactory
{
    List<ISummaryRowItem> createSummaryReportItem(EnrollmentDirection item);
}
