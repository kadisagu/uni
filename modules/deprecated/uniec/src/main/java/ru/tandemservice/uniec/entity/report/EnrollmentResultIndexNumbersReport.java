package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.EnrollmentResultIndexNumbersReportGen;

/**
 * Результаты приема (план, конкурс, проходной балл)
 */
public class EnrollmentResultIndexNumbersReport extends EnrollmentResultIndexNumbersReportGen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}