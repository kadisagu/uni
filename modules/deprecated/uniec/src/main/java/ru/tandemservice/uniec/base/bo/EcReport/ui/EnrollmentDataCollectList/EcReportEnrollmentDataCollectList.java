/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDataCollectList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect.EnrollmentDataCollectReportSearchDSHandler;
import ru.tandemservice.uniec.entity.report.EnrollmentDataCollectReport;

/**
 * @author Alexander Shaburov
 * @since 14.08.13
 */
@Configuration
public class EcReportEnrollmentDataCollectList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_SELECT_DS = "enrollmentCampaignSelectDS";
    public static final String REPORT_LIST_SEARCH_DS = "reportListSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_SELECT_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(REPORT_LIST_SEARCH_DS, reportListSearchDSColumns(), reportListSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportListSearchDSColumns()
    {
        return columnListExtPointBuilder(REPORT_LIST_SEARCH_DS)
                .addColumn(indicatorColumn("ico").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("formingDate", EnrollmentDataCollectReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("fromTo", EnrollmentDataCollectReportSearchDSHandler.V_PROP_FROM_TO))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintReport").permissionKey("printUniecStorableReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete", alert("reportListSearchDSHandler.delete.alert", EnrollmentDataCollectReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME)).permissionKey("deleteUniecStorableReport"))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> reportListSearchDSHandler()
    {
        return new EnrollmentDataCollectReportSearchDSHandler(getName());
    }
}
