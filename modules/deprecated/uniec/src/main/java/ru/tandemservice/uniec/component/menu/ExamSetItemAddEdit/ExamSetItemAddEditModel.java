/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.ExamSetItemAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;

import ru.tandemservice.uniec.component.entrant.EntranceDisciplineAddEdit.EntranceDisciplineAddEditModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.examset.ExamSet;

/**
 * @author vip_delete
 * @since 06.02.2009
 */
@Input({
        @Bind(key = ExamSetItemAddEditModel.EXAM_SET_ID, binding = "examSetId"),
        @Bind(key = ExamSetItemAddEditModel.EXAM_SET_ITEM_ID, binding = "examSetItemId")
})
@Return({
        @Bind(key = ExamSetItemAddEditModel.EXAM_SET_ID, binding = "examSetId")
})
public class ExamSetItemAddEditModel extends EntranceDisciplineAddEditModel
{
    public static final String EXAM_SET_ID = "examSetId";
    public static final String EXAM_SET_ITEM_ID = "examSetItemId";

    private String _examSetId;
    private String _examSetItemId;
    private ExamSet _examSet;
    private List<EntranceDiscipline> _entranceDisciplineList;
    private List<EnrollmentDirection> _entranceEducationOrgUnitList;

    // Getters & Setters

    public String getExamSetId()
    {
        return _examSetId;
    }

    public void setExamSetId(String examSetId)
    {
        _examSetId = examSetId;
    }

    public String getExamSetItemId()
    {
        return _examSetItemId;
    }

    public void setExamSetItemId(String examSetItemId)
    {
        _examSetItemId = examSetItemId;
    }

    public ExamSet getExamSet()
    {
        return _examSet;
    }

    public void setExamSet(ExamSet examSet)
    {
        _examSet = examSet;
    }

    public List<EntranceDiscipline> getEntranceDisciplineList()
    {
        return _entranceDisciplineList;
    }

    public void setEntranceDisciplineList(List<EntranceDiscipline> entranceDisciplineList)
    {
        _entranceDisciplineList = entranceDisciplineList;
    }

    public List<EnrollmentDirection> getEntranceEducationOrgUnitList()
    {
        return _entranceEducationOrgUnitList;
    }

    public void setEntranceEducationOrgUnitList(List<EnrollmentDirection> entranceEducationOrgUnitList)
    {
        _entranceEducationOrgUnitList = entranceEducationOrgUnitList;
    }
}
