package ru.tandemservice.uniec.entity.entrant;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.gen.*;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.notExistsByExpr;

/**
 * Дополнительный статус абитуриента
 */
public class EntrantCustomState extends EntrantCustomStateGen
{

    /**
     * @param entrant         Выражение Абитуриента
     * @param exists          Содержит/Не содержит
     * @param customStateList Список дополнительных статусов абитуриента
     * @param date            Дата на которую проверятся действие дополнительных статусов
     * @return Выражение существования/не существования дополнительных статусов у абитуриентов или null если список пустой
     */
    public static IDQLExpression getExistsExpression4EntrantCustomState(IDQLExpression entrant, boolean exists, List<EntrantCustomStateCI> customStateList, Date date)
    {
        if (CollectionUtils.isNotEmpty(customStateList))
        {
            String alias = "ecs";
            IDQLExpression customStateExp = getExpression4EntrantCustomState(entrant, alias, customStateList, date);
            if (exists) return existsByExpr(EntrantCustomState.class, alias, customStateExp);
            else return notExistsByExpr(EntrantCustomState.class, alias, customStateExp);
        }

        return null;
    }

    /**
     * @param entrant         Выражение Абитуриента
     * @param alias           Алиас дополнительного статуса абитуриента
     * @param customStateList Список дополнительных статусов абитуриента
     * @param date            Дата на которую проверятся действие дополнительных статусов
     * @return Выражение условий дополнительного статуса абитуриента
     */
    public static IDQLExpression getExpression4EntrantCustomState(IDQLExpression entrant, String alias, List<EntrantCustomStateCI> customStateList, Date date)
    {
        IDQLExpression expression = and(
                eq(property(alias, EntrantCustomState.entrant()), entrant),
                and(
                        or(isNull(property(alias, EntrantCustomState.beginDate())),
                           ge(valueDate(date), property(alias, EntrantCustomState.beginDate()))),
                        or(isNull(property(alias, EntrantCustomState.endDate())),
                           le(valueDate(date), property(alias, EntrantCustomState.endDate())))
                )
        );

        if (CollectionUtils.isNotEmpty(customStateList))
            expression = and(expression, in(property(alias, EntrantCustomState.customState()), customStateList));

        return expression;
    }


    /**
     * @param entrantAlias    Алиас для entrantPath
     * @param entrantPath     Путь до абитуриента
     * @param exists          Содержит/Не содержит
     * @param customStateList Список дополнительных статусов абитуриента
     * @param date            Дата на которую проверятся действие дополнительных статусов
     * @return Выражение существования/не существования дополнительных статусов у абитуриентов или null если список пустой
     */
    public static AbstractExpression getExistsExpression4EntrantCustomState(String entrantAlias, String entrantPath,
                                                                            boolean exists, List<EntrantCustomStateCI> customStateList, Date date)
    {
        if (CollectionUtils.isNotEmpty(customStateList))
        {
            String alias = "ecs";
            MQBuilder customStateBuilder = new MQBuilder(EntrantCustomState.ENTITY_CLASS, alias);
            customStateBuilder.add(MQExpression.in(alias, EntrantCustomState.customState().s(), customStateList));
            customStateBuilder.add(MQExpression.eqProperty(alias, EntrantCustomState.entrant().s(), entrantAlias, entrantPath));
            customStateBuilder.add(MQExpression.and(
                    MQExpression.or(MQExpression.isNull(alias, EntrantCustomState.beginDate().s()),
                                    MQExpression.lessOrEq(alias, EntrantCustomState.beginDate().s(), date)),
                    MQExpression.or(MQExpression.isNull(alias, EntrantCustomState.endDate().s()),
                                    MQExpression.greatOrEq(alias, EntrantCustomState.endDate().s(), date))
            ));

            if (exists)
                return MQExpression.exists(customStateBuilder);
            else return MQExpression.notExists(customStateBuilder);
        }

        return null;
    }
}