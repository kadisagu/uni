/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ReasonToBasicsEnrollmentSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alikhanov
 * @since 05.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEnrollmentOrderReason(get(EnrollmentOrderReason.class, model.getEnrollmentOrderReasonId()));
        model.setSelectedBasicsList(model.getEnrollmentOrderReason().getBasics());
        model.setEnrollmentOrderBasicsSelectModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EnrollmentOrderBasic.ENTITY_CLASS, "basic");
                builder.add(MQExpression.like("basic",EnrollmentOrderBasic.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("basic", EnrollmentOrderBasic.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<Long> reasonsToSay = new ArrayList<>();
        MQBuilder builder = new MQBuilder(EnrollmentOrderReasonToBasicsRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EnrollmentOrderReasonToBasicsRel.L_REASON, model.getEnrollmentOrderReason()));
        List<EnrollmentOrderReasonToBasicsRel> relList = builder.getResultList(getSession());

        for(EnrollmentOrderReasonToBasicsRel rel : relList)
            if(!model.getSelectedBasicsList().contains(rel.getBasic()))
                getSession().delete(rel);
            else
                reasonsToSay.add(rel.getBasic().getId());

        for(EnrollmentOrderBasic basic : model.getSelectedBasicsList())
            if(!reasonsToSay.contains(basic.getId()))
            {
                EnrollmentOrderReasonToBasicsRel rel = new EnrollmentOrderReasonToBasicsRel();
                rel.setReason(model.getEnrollmentOrderReason());
                rel.setBasic(basic);
                getSession().save(rel);
            }
    }
}
