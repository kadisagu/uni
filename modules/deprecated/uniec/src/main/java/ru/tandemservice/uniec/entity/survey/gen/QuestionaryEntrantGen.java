package ru.tandemservice.uniec.entity.survey.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.survey.base.entity.BaseQuestionary;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.survey.QuestionaryEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Анкета абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class QuestionaryEntrantGen extends BaseQuestionary
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.survey.QuestionaryEntrant";
    public static final String ENTITY_NAME = "questionaryEntrant";
    public static final int VERSION_HASH = -1100907954;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";

    private Entrant _entrant;     // Абитуриент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент.
     */
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof QuestionaryEntrantGen)
        {
            setEntrant(((QuestionaryEntrant)another).getEntrant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends QuestionaryEntrantGen> extends BaseQuestionary.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) QuestionaryEntrant.class;
        }

        public T newInstance()
        {
            return (T) new QuestionaryEntrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                    return obj.getEntrant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entrant":
                    return Entrant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<QuestionaryEntrant> _dslPath = new Path<QuestionaryEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "QuestionaryEntrant");
    }
            

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.uniec.entity.survey.QuestionaryEntrant#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    public static class Path<E extends QuestionaryEntrant> extends BaseQuestionary.Path<E>
    {
        private Entrant.Path<Entrant> _entrant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.uniec.entity.survey.QuestionaryEntrant#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

        public Class getEntityClass()
        {
            return QuestionaryEntrant.class;
        }

        public String getEntityName()
        {
            return "questionaryEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
