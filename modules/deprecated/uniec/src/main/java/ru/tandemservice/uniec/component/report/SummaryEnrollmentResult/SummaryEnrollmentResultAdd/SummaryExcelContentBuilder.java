/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jxl.SheetSettings;
import jxl.Workbook;
import jxl.biff.CellReferenceHelper;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.PaperSize;
import jxl.format.ScriptStyle;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import org.tandemframework.shared.fias.base.entity.AddressItem;

import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * Печатает excel-формы всех трех сводок: прием документов, ход сдачи экзаменов, результаты зачисления
 *
 * @author vip_delete
 * @since 21.04.2009
 */
class SummaryExcelContentBuilder
{
    /**
     * создает Excel-файл отчета
     *
     * @param reportTitle             название отчета
     * @param highSchoolTitle         название ОУ
     * @param highSchoolArea          регион ОУ
     * @param highSchoolCity          город ОУ
     * @param periodTitle             название периода отчета
     * @param paramsTitle             строка параметров
     * @param benefitList             льготы (справочник)
     * @param targetAdmissionKindList виды целевого приема (справочник)
     * @param profileKnowledgeList    профильные знания (справочник)
     * @param reportRowList           список строк отчета
     * @param showEduGroup            показывать укрупненные группы
     * @param hideColumns             список колонок для скрытия
     * @return бинарное представление excel-файла отчета
     * @throws Exception ошибка
     */
    public static byte[] buildExcelContent(String reportTitle,
                                           String highSchoolTitle,
                                           AddressItem highSchoolArea,
                                           AddressItem highSchoolCity,
                                           String periodTitle,
                                           String paramsTitle,
                                           List<Benefit> benefitList,
                                           List<TargetAdmissionKind> targetAdmissionKindList,
                                           List<ProfileKnowledge> profileKnowledgeList,
                                           Map<EducationLevels, Set<SummaryReportRow>> reportRowList,
                                           boolean showEduGroup,
                                           Collection<String> hideColumns) throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // create fonts
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial10boldGray = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GRAY_50, ScriptStyle.NORMAL_SCRIPT);
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat boldFormat = new WritableCellFormat(arial10bold);

        WritableCellFormat paramFormat = new WritableCellFormat(arial8);

        WritableCellFormat blackBorderFormat = new WritableCellFormat(arial10);
        blackBorderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        blackBorderFormat.setWrap(true);
        blackBorderFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat boldBlackBorderFormat = new WritableCellFormat(arial10bold);
        boldBlackBorderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        boldBlackBorderFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat boldBlackBorderFormatGray = new WritableCellFormat(arial10boldGray);
        boldBlackBorderFormatGray.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        boldBlackBorderFormatGray.setAlignment(Alignment.CENTRE);
        boldBlackBorderFormatGray.setVerticalAlignment(VerticalAlignment.TOP);
        boldBlackBorderFormatGray.setWrap(true);

        WritableCellFormat integerBlackBorderFormat = new WritableCellFormat(arial10, NumberFormats.INTEGER);
        integerBlackBorderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        integerBlackBorderFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat boldIntegerBlackBorderFormatGray = new WritableCellFormat(arial10boldGray, NumberFormats.INTEGER);
        boldIntegerBlackBorderFormatGray.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        boldIntegerBlackBorderFormatGray.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat integerBoldBlackBorderFormat = new WritableCellFormat(arial10bold, NumberFormats.INTEGER);
        integerBoldBlackBorderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        integerBoldBlackBorderFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat floatBlackBorderFormat = new WritableCellFormat(arial10, NumberFormats.FLOAT);
        floatBlackBorderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        floatBlackBorderFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat boldFloatBlackBorderFormatGray = new WritableCellFormat(arial10boldGray, NumberFormats.FLOAT);
        boldFloatBlackBorderFormatGray.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        boldFloatBlackBorderFormatGray.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat floatBoldBlackBorderFormat = new WritableCellFormat(arial10bold, NumberFormats.FLOAT);
        floatBoldBlackBorderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        floatBoldBlackBorderFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
        headerFormat.setWrap(true);

        // create list
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.setColumnView(0, 50); // ~350 пикселей

        SheetSettings settings = sheet.getSettings();
        settings.setCopies(1);
        settings.setScaleFactor(85);
        settings.setPaperSize(PaperSize.A3);
        settings.setDefaultColumnWidth(5); // ~40 пикселей

        settings.setTopMargin(0.39);
        settings.setLeftMargin(0.39);
        settings.setRightMargin(0.39);
        settings.setBottomMargin(0.2);

        // row 1
        sheet.addCell(new Label(0, 0, highSchoolTitle, boldFormat));

        // row 2
        sheet.addCell(new Label(0, 1, "Информация приемной комиссии " + periodTitle, boldFormat));

        // row 3
        sheet.addCell(new Label(0, 2, reportTitle, boldFormat));

        // row 5
        sheet.addCell(new Label(0, 4, paramsTitle, paramFormat));

        // write table by columns at this coordinates
        final int TABLE_START_ROW_INDEX = 6;
        final int TABLE_START_COLUMN_INDEX = 0;
        int height = 0;
        for (Set<SummaryReportRow> set : reportRowList.values())
            height += set.size();
        final int HEIGHT = height;

        // usefull indexes
        int row;
        int column;
        int columnDelta = 0;

        for (Set<SummaryReportRow> set : reportRowList.values())
        {
            SummaryReportRow preRow = null;
            for (SummaryReportRow reportRow : set)
            {
                if (preRow != null && preRow.getTitle().equals(reportRow.getTitle()))
                {
                    preRow.useFullTitle();
                    reportRow.useFullTitle();
                }
                preRow = reportRow;
            }
        }

        // Amazing index-logic

        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX;
            sheet.addCell(new Label(column, row, "Направление подготовки (специальность)", headerFormat));
            sheet.mergeCells(column, row, column, row + 2);
            column++;

            sheet.addCell(new Label(column, row, "План", headerFormat));
            sheet.mergeCells(column, row, column, row + 2);
            column++;

            sheet.addCell(new Label(column, row, "Всего", headerFormat));
            sheet.mergeCells(column, row, column, row + 2);
            column++;

            sheet.addCell(new Label(column, row, "Конкурс", headerFormat));
            sheet.mergeCells(column, row, column, row + 2);

            row = TABLE_START_ROW_INDEX + 3;
            column = TABLE_START_COLUMN_INDEX;

            int sumPlan = 0;
            int sumCount = 0;
            List<Integer> rows = new ArrayList<Integer>();
            for (Map.Entry<EducationLevels, Set<SummaryReportRow>> eduGroup : reportRowList.entrySet())
            {
                int size = eduGroup.getValue().size();
                if (size > 0)
                {
                    if (showEduGroup)
                    {
                        rows.add(row);
                        int plan = 0;
                        int count = 0;
                        for (SummaryReportRow reportRow : eduGroup.getValue())
                        {
                            plan += reportRow.getPlan();
                            count += reportRow.getCount();
                        }
                        sheet.addCell(new Label(column, row, eduGroup.getKey().getDisplayableTitle(), boldBlackBorderFormatGray));
                        sheet.addCell(new Formula(column + 1, row, "SUM(" + JExcelUtil.getRange(column + 1, row + 1, column + 1, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        sheet.addCell(new Formula(column + 2, row, "SUM(" + JExcelUtil.getRange(column + 2, row + 1, column + 2, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 3, row, plan == 0 ? 0 : (double) count / (double) plan, boldFloatBlackBorderFormatGray));
                        row++;
                    }
                    for (SummaryReportRow reportRow : eduGroup.getValue())
                    {
                        sheet.addCell(new Label(column, row, reportRow.isUseFullTitle() ? reportRow.getFullTitle() : reportRow.getTitle(), blackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, row, reportRow.getPlan(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 2, row, reportRow.getCount(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 3, row, reportRow.getPlan() == 0 ? 0 : (double) reportRow.getCount() / (double) reportRow.getPlan(), floatBlackBorderFormat));
                        row++;

                        sumPlan += reportRow.getPlan();
                        sumCount += reportRow.getCount();
                    }
                }
            }
            if (HEIGHT > 0)
            {
                sheet.addCell(new Label(column, row, "  Итого по ОУ", boldBlackBorderFormat));
                if (showEduGroup)
                {
                    sheet.addCell(new Formula(column + 1, row, getSumFormula(rows, column + 1), integerBoldBlackBorderFormat));
                    sheet.addCell(new Formula(column + 2, row, getSumFormula(rows, column + 2), integerBoldBlackBorderFormat));
                    sheet.addCell(JExcelUtil.getNumberNullableCell(column + 3, row, sumPlan == 0 ? 0 : (double) sumCount / (double) sumPlan, floatBoldBlackBorderFormat));
                } else
                {
                    sheet.addCell(JExcelUtil.getColumnSumFormula(column + 1, row, HEIGHT, integerBoldBlackBorderFormat));
                    sheet.addCell(JExcelUtil.getColumnSumFormula(column + 2, row, HEIGHT, integerBoldBlackBorderFormat));
                    sheet.addCell(JExcelUtil.getNumberNullableCell(column + 3, row, sumPlan == 0 ? 0 : (double) sumCount / (double) sumPlan, floatBoldBlackBorderFormat));
                }
            }
            columnDelta += 4;
        }

        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX + columnDelta;
            sheet.addCell(new Label(column, row, "Внеконкурсный прием", headerFormat));
            sheet.addCell(new Label(column, row + 1, "всего", headerFormat));
            sheet.mergeCells(column, row + 1, column, row + 2);
            for (int i = 0; i < benefitList.size(); i++)
            {
                sheet.addCell(new Label(column + i + 1, row + 1, benefitList.get(i).getShortTitle(), headerFormat));
                sheet.mergeCells(column + i + 1, row + 1, column + i + 1, row + 2);
            }
            sheet.mergeCells(column, row, column + benefitList.size(), row);

            row = TABLE_START_ROW_INDEX + 3;
            column = TABLE_START_COLUMN_INDEX + columnDelta;
            List<Integer> rows = new ArrayList<Integer>();
            for (Map.Entry<EducationLevels, Set<SummaryReportRow>> eduGroup : reportRowList.entrySet())
            {
                int size = eduGroup.getValue().size();
                if (size > 0)
                {
                    if (showEduGroup)
                    {
                        rows.add(row);
                        for (int i = 0; i < benefitList.size() + 1; i++)
                            sheet.addCell(new Formula(column + i, row, "SUM(" + JExcelUtil.getRange(column + i, row + 1, column + i, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        row++;
                    }
                    for (SummaryReportRow reportRow : eduGroup.getValue())
                    {
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column, row, reportRow.getCountNoContest(), integerBlackBorderFormat));
                        for (int i = 0; i < benefitList.size(); i++)
                        {
                            Benefit item = benefitList.get(i);
                            Integer value = reportRow.getNoContestStats().get(item.getCode());
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + i + 1, row, value, integerBlackBorderFormat));
                        }
                        row++;
                    }
                }
            }
            if (HEIGHT > 0)
            {
                if (showEduGroup)
                {
                    sheet.addCell(new Formula(column, row, getSumFormula(rows, column), integerBoldBlackBorderFormat));
                    for (int i = 0; i < benefitList.size(); i++)
                        sheet.addCell(new Formula(column + i + 1, row, getSumFormula(rows, column + i + 1), integerBoldBlackBorderFormat));
                } else
                {
                    sheet.addCell(JExcelUtil.getColumnSumFormula(column, row, HEIGHT, integerBlackBorderFormat));
                    for (int i = 0; i < benefitList.size(); i++)
                        sheet.addCell(JExcelUtil.getColumnSumFormula(column + i + 1, row, HEIGHT, integerBoldBlackBorderFormat));
                }
            }
            columnDelta += benefitList.size() + 1;
        }

        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX + columnDelta;
            sheet.addCell(new Label(column, row, "Целевой прием", headerFormat));
            sheet.addCell(new Label(column, row + 1, "всего", headerFormat));
            sheet.mergeCells(column, row + 1, column, row + 2);
            if (targetAdmissionKindList.size() > 1)
            {
                for (int i = 0; i < targetAdmissionKindList.size(); i++)
                {
                    sheet.addCell(new Label(column + i + 1, row + 1, targetAdmissionKindList.get(i).getShortTitle(), headerFormat));
                    sheet.mergeCells(column + i + 1, row + 1, column + i + 1, row + 2);
                }
                sheet.mergeCells(column, row, column + targetAdmissionKindList.size(), row);
            }
            row += 3;
            List<Integer> rows = new ArrayList<Integer>();
            for (Map.Entry<EducationLevels, Set<SummaryReportRow>> eduGroup : reportRowList.entrySet())
            {
                int size = eduGroup.getValue().size();
                if (size > 0)
                {
                    if (showEduGroup)
                    {
                        rows.add(row);
                        sheet.addCell(new Formula(column, row, "SUM(" + JExcelUtil.getRange(column, row + 1, column, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        if (targetAdmissionKindList.size() > 1)
                            for (int i = 0; i < targetAdmissionKindList.size(); i++)
                                sheet.addCell(new Formula(column + i + 1, row, "SUM(" + JExcelUtil.getRange(column + i + 1, row + 1, column + i + 1, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        row++;
                    }
                    for (SummaryReportRow reportRow : eduGroup.getValue())
                    {
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column, row, reportRow.getCountTargetAdmission(), integerBlackBorderFormat));
                        if (targetAdmissionKindList.size() > 1)
                            for (int i = 0; i < targetAdmissionKindList.size(); i++)
                            {
                                TargetAdmissionKind item = targetAdmissionKindList.get(i);
                                Integer value = reportRow.getTargetAdmissionStats().get(item.getCode());
                                sheet.addCell(JExcelUtil.getNumberNullableCell(column + i + 1, row, value, integerBlackBorderFormat));
                            }
                        row++;
                    }
                }
            }
            if (HEIGHT > 0)
            {
                if (showEduGroup)
                {
                    sheet.addCell(new Formula(column, row, getSumFormula(rows, column), integerBoldBlackBorderFormat));
                    if (targetAdmissionKindList.size() > 1)
                        for (int i = 0; i < targetAdmissionKindList.size(); i++)
                            sheet.addCell(new Formula(column + i + 1, row, getSumFormula(rows, column + i + 1), integerBoldBlackBorderFormat));
                } else
                {
                    sheet.addCell(JExcelUtil.getColumnSumFormula(column, row, HEIGHT, integerBlackBorderFormat));
                    if (targetAdmissionKindList.size() > 1)
                        for (int i = 0; i < targetAdmissionKindList.size(); i++)
                            sheet.addCell(JExcelUtil.getColumnSumFormula(column + i + 1, row, HEIGHT, integerBoldBlackBorderFormat));
                }
            }
            columnDelta += 1 + (targetAdmissionKindList.size() > 1 ? targetAdmissionKindList.size() : 0);
        }

        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX + columnDelta;

            sheet.addCell(new Label(column, row, "Уч. Олимп", headerFormat));
            sheet.mergeCells(column, row, column + 2, row);

            sheet.addCell(new Label(column, row + 1, "всего", headerFormat));
            sheet.mergeCells(column, row + 1, column, row + 2);

            sheet.addCell(new Label(column + 1, row + 1, "без экз.", headerFormat));
            sheet.mergeCells(column + 1, row + 1, column + 1, row + 2);

            sheet.addCell(new Label(column + 2, row + 1, "100 балл", headerFormat));
            sheet.mergeCells(column + 2, row + 1, column + 2, row + 2);

            row += 3;
            List<Integer> rows = new ArrayList<Integer>();
            for (Map.Entry<EducationLevels, Set<SummaryReportRow>> eduGroup : reportRowList.entrySet())
            {
                int size = eduGroup.getValue().size();
                if (size > 0)
                {
                    if (showEduGroup)
                    {
                        rows.add(row);
                        for (int i = 0; i < 3; i++)
                            sheet.addCell(new Formula(column + i, row, "SUM(" + JExcelUtil.getRange(column + i, row + 1, column + i, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        row++;
                    }
                    for (SummaryReportRow reportRow : eduGroup.getValue())
                    {
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column, row, reportRow.getWithoutExam() + reportRow.getWithDiplom(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, row, reportRow.getWithoutExam(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 2, row, reportRow.getWithDiplom(), integerBlackBorderFormat));
                        row++;
                    }
                }
            }
            if (HEIGHT > 0)
            {
                if (showEduGroup)
                {
                    for (int i = 0; i < 3; i++)
                        sheet.addCell(new Formula(column + i, row, getSumFormula(rows, column + i), integerBoldBlackBorderFormat));
                } else
                {
                    for (int i = 0; i < 3; i++)
                        sheet.addCell(JExcelUtil.getColumnSumFormula(column + i, row, HEIGHT, integerBoldBlackBorderFormat));
                }
            }
            columnDelta += 3;
        }

        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX + columnDelta;

            int c = column;

            sheet.addCell(new Label(c, row, "Среднее полное образ-е", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "в тек.", headerFormat));
            sheet.addCell(new Label(c + 1, row + 2, "всего", headerFormat));
            sheet.addCell(new Label(c + 2, row + 2, "днев", headerFormat));
            sheet.addCell(new Label(c + 3, row + 2, "веч", headerFormat));
            sheet.addCell(new Label(c + 4, row + 2, "кор", headerFormat));
            sheet.mergeCells(column, row, column + 4, row);
            sheet.mergeCells(column + 1, row + 1, column + 4, row + 1);
            sheet.mergeCells(column, row + 1, column, row + 2);
            c += 5;

            sheet.addCell(new Label(c, row, "НПО", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "в тек.", headerFormat));
            sheet.mergeCells(c, row, c + 1, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 1, row + 2);
            c += 2;

            sheet.addCell(new Label(c, row, "СПО", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "в т.ч. СППО", headerFormat));
            sheet.addCell(new Label(c + 2, row + 1, "в тек.", headerFormat));
            sheet.addCell(new Label(c + 2, row + 2, "всего", headerFormat));
            sheet.addCell(new Label(c + 3, row + 2, "в т.ч. СППО", headerFormat));
            sheet.mergeCells(c, row, c + 3, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 1, row + 2);
            sheet.mergeCells(c + 2, row + 1, c + 3, row + 1);
            c += 4;

            sheet.addCell(new Label(c, row, "ВО", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "бак", headerFormat));
            sheet.addCell(new Label(c + 1, row + 2, "всего", headerFormat));
            sheet.addCell(new Label(c + 2, row + 2, "в тек.", headerFormat));
            sheet.addCell(new Label(c + 3, row + 1, "спец", headerFormat));
            sheet.addCell(new Label(c + 3, row + 2, "всего", headerFormat));
            sheet.addCell(new Label(c + 4, row + 2, "в тек.", headerFormat));
            sheet.addCell(new Label(c + 5, row + 1, "магистр", headerFormat));
            sheet.addCell(new Label(c + 5, row + 2, "всего", headerFormat));
            sheet.addCell(new Label(c + 6, row + 2, "в тек.", headerFormat));
            sheet.mergeCells(c, row, c + 6, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 2, row + 1);
            sheet.mergeCells(c + 3, row + 1, c + 4, row + 1);
            sheet.mergeCells(c + 5, row + 1, c + 6, row + 1);
            c += 7;

            sheet.addCell(new Label(c, row, "непВО", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Слушатели", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "тек. студ.", headerFormat));
            sheet.mergeCells(c, row, c + 1, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 1, row + 2);
            c += 2;

            sheet.addCell(new Label(c, row, "Мужчин", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "в/сл", headerFormat));
            sheet.mergeCells(c, row, c + 1, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 1, row + 2);
            c += 2;

            sheet.addCell(new Label(c, row, highSchoolArea == null ? "регион ОУ" : highSchoolArea.getTitleWithType(), headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, highSchoolCity == null ? "город ОУ" : highSchoolCity.getTitleWithType(), headerFormat));
            sheet.addCell(new Label(c + 2, row + 1, "село", headerFormat));
            sheet.mergeCells(c, row, c + 2, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 1, row + 2);
            sheet.mergeCells(c + 2, row + 1, c + 2, row + 2);
            c += 3;

            sheet.addCell(new Label(c, row, "Др. р-ны России", headerFormat));
            sheet.addCell(new Label(c, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(c + 1, row + 1, "село", headerFormat));
            sheet.mergeCells(c, row, c + 1, row);
            sheet.mergeCells(c, row + 1, c, row + 2);
            sheet.mergeCells(c + 1, row + 1, c + 1, row + 2);
            c += 2;

            sheet.addCell(new Label(c, row, "Др. гос-ва", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Иностр. граж-не", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Медаль", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Диплом с отл", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Стаж по профилю", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Проф класс", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);
            c++;

            sheet.addCell(new Label(c, row, "Пр. право", headerFormat));
            sheet.mergeCells(c, row, c, row + 2);

            row += 3;
            List<Integer> rows = new ArrayList<Integer>();
            for (Map.Entry<EducationLevels, Set<SummaryReportRow>> eduGroup : reportRowList.entrySet())
            {
                int size = eduGroup.getValue().size();
                if (size > 0)
                {
                    if (showEduGroup)
                    {
                        rows.add(row);
                        for (int i = 0; i < 35; i++)
                            sheet.addCell(new Formula(column + i, row, "SUM(" + JExcelUtil.getRange(column + i, row + 1, column + i, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        row++;
                    }
                    for (SummaryReportRow reportRow : eduGroup.getValue())
                    {
                        int i = column;
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountTotalEducation(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentTotalEducation(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentTotalEducation() - reportRow.getEveningSchool() - reportRow.getStupidSchool(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getEveningSchool(), integerBlackBorderFormat));              // веч.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getStupidSchool(), integerBlackBorderFormat));               // кор.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountBeginEducation(), integerBlackBorderFormat));        // НПО всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentBeginEducation(), integerBlackBorderFormat));      // НПО в тек.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountProfileEducation(), integerBlackBorderFormat));      // СПО всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountSPPOProfileEducation(), integerBlackBorderFormat));  // СПО в т.ч. СППО
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentProfileEducation(), integerBlackBorderFormat));    // СПО в тек. всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentSPPOProfileEducation(), integerBlackBorderFormat));// СПО в тек. т.ч. СППО
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountHighProfileEducation(), integerBlackBorderFormat));  // ВО всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountHighProfileEducationBachelors(), integerBlackBorderFormat));    // ВО бак всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentHighProfileEducationBachelors(), integerBlackBorderFormat));  // ВО бак в тек.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountHighProfileEducationSpec(), integerBlackBorderFormat));         // ВО спец всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentHighProfileEducationSpec(), integerBlackBorderFormat));       // ВО спец в тек.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountHighProfileEducationMaster(), integerBlackBorderFormat));       // ВО магистр всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCurrentHighProfileEducationMaster(), integerBlackBorderFormat));     // ВО магистр в тек.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountHalfHighEducation(), integerBlackBorderFormat));     // непВО
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountListeners(), integerBlackBorderFormat));             // Слушатели всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountStudListeners(), integerBlackBorderFormat));         // Слушатели тек. студ.
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountMans(), integerBlackBorderFormat));                  // Мужчин
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountSoldiers(), integerBlackBorderFormat));              // Мужчин в/сл
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountInHighSchoolArea(), integerBlackBorderFormat));      // Регион ОУ всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountInHighSchoolCity(), integerBlackBorderFormat));      // Регион ОУ город ОУ
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountInHighSchoolVillage(), integerBlackBorderFormat));   // Регион ОУ село
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountInOtherArea(), integerBlackBorderFormat));           // Др. р-ны России всего
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountInOtherVillage(), integerBlackBorderFormat));        // Др. р-ны России село
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountForeignArea(), integerBlackBorderFormat));           // Др. гос-ва
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountForeignCitizenship(), integerBlackBorderFormat));    // Иностр. граж-не
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getMedal(), integerBlackBorderFormat));                      // Медаль
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getDiploma(), integerBlackBorderFormat));                    // Диплом с отл
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getCountServiceLength(), integerBlackBorderFormat));         // Стаж по профилю
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i++, row, reportRow.getWithProfileClass(), integerBlackBorderFormat));           // Проф класс
                        sheet.addCell(JExcelUtil.getNumberNullableCell(i, row, reportRow.getWithRecomendations(), integerBlackBorderFormat));           // Пр. право
                        row++;
                    }
                }
            }
            if (HEIGHT > 0)
            {
                if (showEduGroup)
                {
                    for (int i = 0; i < 35; i++)
                        sheet.addCell(new Formula(column + i, row, getSumFormula(rows, column + i), integerBoldBlackBorderFormat));
                } else
                {
                    for (int i = 0; i < 35; i++)
                        sheet.addCell(JExcelUtil.getColumnSumFormula(column + i, row, HEIGHT, integerBoldBlackBorderFormat));
                }
            }
            columnDelta += 35;
        }

        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX + columnDelta;

            sheet.addCell(new Label(column, row, "ЕГЭ -I (МОУ)", headerFormat));
            sheet.addCell(new Label(column, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(column + 1, row + 1, "1 экз.", headerFormat));
            sheet.addCell(new Label(column + 2, row + 1, "2 экз.", headerFormat));
            sheet.addCell(new Label(column + 3, row + 1, "3 экз.", headerFormat));
            sheet.addCell(new Label(column + 4, row + 1, "4 экз.", headerFormat));
            sheet.mergeCells(column, row, column + 4, row);
            sheet.mergeCells(column, row + 1, column, row + 2);
            sheet.mergeCells(column + 1, row + 1, column + 1, row + 2);
            sheet.mergeCells(column + 2, row + 1, column + 2, row + 2);
            sheet.mergeCells(column + 3, row + 1, column + 3, row + 2);
            sheet.mergeCells(column + 4, row + 1, column + 4, row + 2);

            sheet.addCell(new Label(column + 5, row, "ЕГЭ -II (вуз)", headerFormat));
            sheet.addCell(new Label(column + 5, row + 1, "всего", headerFormat));
            sheet.addCell(new Label(column + 6, row + 1, "1 экз.", headerFormat));
            sheet.addCell(new Label(column + 7, row + 1, "2 экз.", headerFormat));
            sheet.addCell(new Label(column + 8, row + 1, "3 экз.", headerFormat));
            sheet.addCell(new Label(column + 9, row + 1, "4 экз.", headerFormat));
            sheet.mergeCells(column + 5, row, column + 9, row);
            sheet.mergeCells(column + 5, row + 1, column + 5, row + 2);
            sheet.mergeCells(column + 6, row + 1, column + 6, row + 2);
            sheet.mergeCells(column + 7, row + 1, column + 7, row + 2);
            sheet.mergeCells(column + 8, row + 1, column + 8, row + 2);
            sheet.mergeCells(column + 9, row + 1, column + 9, row + 2);

            row += 3;
            List<Integer> rows = new ArrayList<Integer>();
            for (Map.Entry<EducationLevels, Set<SummaryReportRow>> eduGroup : reportRowList.entrySet())
            {
                int size = eduGroup.getValue().size();
                if (size > 0)
                {
                    if (showEduGroup)
                    {
                        rows.add(row);
                        for (int i = 0; i < 10; i++)
                            sheet.addCell(new Formula(column + i, row, "SUM(" + JExcelUtil.getRange(column + i, row + 1, column + i, row + size) + ")", boldIntegerBlackBorderFormatGray));
                        row++;
                    }
                    for (SummaryReportRow reportRow : eduGroup.getValue())
                    {
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column, row, reportRow.getWave1countStateExam(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 1, row, reportRow.getWave1stateExam()[0], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 2, row, reportRow.getWave1stateExam()[1], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 3, row, reportRow.getWave1stateExam()[2], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 4, row, reportRow.getWave1stateExam()[3], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 5, row, reportRow.getWave2countStateExam(), integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 6, row, reportRow.getWave2stateExam()[0], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 7, row, reportRow.getWave2stateExam()[1], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 8, row, reportRow.getWave2stateExam()[2], integerBlackBorderFormat));
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 9, row, reportRow.getWave2stateExam()[3], integerBlackBorderFormat));
                        row++;
                    }
                }
            }
            if (HEIGHT > 0)
            {
                if (showEduGroup)
                {
                    for (int i = 0; i < 10; i++)
                        sheet.addCell(new Formula(column + i, row, getSumFormula(rows, column + i), integerBoldBlackBorderFormat));
                } else
                {
                    for (int i = 0; i < 10; i++)
                        sheet.addCell(JExcelUtil.getColumnSumFormula(column + i, row, HEIGHT, integerBoldBlackBorderFormat));
                }
            }
        }

        JExcelUtil.hideColumns(sheet, 3, 6, 100, 6, hideColumns);

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    private static String getSumFormula(List<Integer> rows, int column)
    {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < rows.size() - 1; i++)
        {
            CellReferenceHelper.getCellReference(column, rows.get(i), buffer);
            buffer.append("+");
        }
        CellReferenceHelper.getCellReference(column, rows.get(rows.size() - 1), buffer);
        return buffer.toString();
    }
}
