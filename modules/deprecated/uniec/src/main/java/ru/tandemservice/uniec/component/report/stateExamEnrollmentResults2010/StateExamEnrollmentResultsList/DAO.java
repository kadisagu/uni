/* $Id: DAO.java 10153 2009-09-30 13:55:18Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = (EnrollmentCampaign) model.getSettings().get("enrollmentCampaign");
        DevelopForm developForm = (DevelopForm) model.getSettings().get("developForm");
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");

        MQBuilder builder = new MQBuilder(StateExamEnrollmentReport2010.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", StateExamEnrollmentReport2010.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (developForm != null)
            builder.add(MQExpression.eq("r", StateExamEnrollmentReport2010.L_DEVELOP_FORM, developForm));
        if (compensationType != null)
            builder.add(MQExpression.eq("r", StateExamEnrollmentReport2010.L_COMPENSATION_TYPE, compensationType));
        new OrderDescriptionRegistry("r").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();
        StateExamEnrollmentReport2010 report = getNotNull(StateExamEnrollmentReport2010.class, id);
        delete(report);
        delete(report.getContent());
    }
}
