/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.ContactDataStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String REGION_ADDRESS = "addressRegion";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setInline(true);
        addressConfig.setAreaVisible(true);
        addressConfig.setFieldSetTitle("Фактический адрес");
        addressConfig.setAddressBase(model.getAddress());

        component.createChildRegion(REGION_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));
    }

    // Wizard Actions

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);

        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();

        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        AddressBase address = addressBaseEditInlineUI.getResult();

        model.setAddress(address);

        getDao().update(model);

        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.ENTRANT_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
        ));
    }

    public void onClickSkip(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().doSkip(model);
        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.ENTRANT_DATA_STEP, new ParametersMap()
                .add("entrantId", model.getEntrant().getId())
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
                .add("onlineEntrantId", model.getOnlineEntrant().getId())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        Model model = getModel(component);
        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();

        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        AddressBase address = addressBaseEditInlineUI.getResult();

        model.setAddress(address);

        getDao().update(model);

        deactivate(component);
    }
}
