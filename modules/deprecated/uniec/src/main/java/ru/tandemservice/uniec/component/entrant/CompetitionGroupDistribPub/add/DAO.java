package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add;

import org.apache.commons.collections.CollectionUtils;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase.Row;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO.IEntrantRateRow;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public abstract class DAO extends UniBaseDao implements IDAO {

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model) {
        model.setDistrib(this.get(EcgDistribObject.class, model.getDistrib().getId()));
        IEnrollmentCompetitionGroupDAO.INSTANCE.get().checkEditable(model.getDistrib());


        if (!model.isFiltered()) {
            model.setDir2quotaMap(IEnrollmentCompetitionGroupDAO.INSTANCE.get().getDir2QuotaMap(model.getDistrib()));

            final List<IEnrollmentCompetitionGroupDAO.IEntrantRateRow> entrantRateRows4Add = this.getEntrants4Add(model);
            if (null != model.getDistrib().getParent()) {
                // для уточняющих распределений - только с оригиналами
                CollectionUtils.filter(entrantRateRows4Add, object -> {
                    /* только тех, кто пришес оригиналы документов */
                    final IEntrantRateRow entrantRateRow = (IEntrantRateRow)object;
                    return entrantRateRow.getOriginalDocumentHandedInPriority() != Integer.MAX_VALUE;
                });
            }

            model.setEntrantRateRows(new ArrayList<>(CollectionUtils.collect(entrantRateRows4Add, input -> new Row((IEntrantRateRow) input))));

            postProcessRows(model.getDistrib(), model.getEntrantRateRows(), model.getDir2quotaMap());
        }
    }

    protected void postProcessRows(final EcgDistribObject distrib, final List<ModelBase.Row> entrantRateRows, final Map<Long, Integer> dir2quotaMap) {
        if (null != distrib.getParent()) {

            final Map<Long, ModelBase.Row> entrant2row = new HashMap<>();
            for (final ModelBase.Row row: entrantRateRows) {
                if (null != entrant2row.put(row.getRow().getEntrant().getId(), row)) {
                    throw new IllegalArgumentException("У абитуриента «"+row.getRow().getEntrant().getPerson().getFullFio()+"» указано несколько направлений приема");
                }
            }

            // выделяем всех тех, кто есть в родительском распределении
            final List<IEntrantRateRow> parentRows = IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows(distrib.getParent());
            for (IEntrantRateRow parentRow: parentRows) {
                Row row = entrant2row.get(parentRow.getEntrant().getId());
                if (null != row) { row.setChecked(true); }
            }
        }

        /* для основного распределения хочется, чтобы выделялись автоматом только те, кто влезает по квоте */ {
            int totalQuota = 0;
            for (final Integer i: dir2quotaMap.values()) {
                totalQuota += i;
            }
            Double totalMark = Double.MAX_VALUE;
            Double profileMark = Double.MAX_VALUE;
            for (final ModelBase.Row row: entrantRateRows) {
                totalQuota --;

                if (totalQuota == 0) {
                    totalMark = row.getRow().getTotalMark();
                    profileMark = row.getRow().getProfileMark();
                }
                if (totalQuota < 0) {
                    if (null != totalMark) {
                        if (totalMark.equals(row.getRow().getTotalMark()) && UniBaseUtils.compare(profileMark, row.getRow().getProfileMark(), true) == 0) {
                            /* если баллы совпадают, то их тоже включаем в распределение, потому пускай руками выбирают */
                            row.setChecked(true);
                        } else {
                            row.setChecked(false);
                            totalMark = null;
                        }
                    } else {
                        row.setChecked(false);
                    }
                }
            }
        }
    }


    protected abstract List<IEnrollmentCompetitionGroupDAO.IEntrantRateRow> getEntrants4Add(final Model model);

    @Override
    public void save(final Model model) {
        final Map<Entrant, RequestedEnrollmentDirection> directions = new HashMap<>();
        for (final ModelBase.Row row: model.getEntrantRateRows()) {
            final Entrant entrant = row.getRow().getEntrant();
            if ((null != row.getDirection()) && (null == directions.get(entrant))) {
                directions.put(entrant, row.getDirection());
            }
        }
        save(model, directions);
    }

    protected void save(final Model model, final Map<Entrant, RequestedEnrollmentDirection> directions) {
        this.getSession();

        //        if (null != model.getDistrib().getParent()) {
        //            // для уточняющего распределения пересчитываем план приема по факту
        //            if (false) {
        //                // 07.06.10 Толик сказал, что это не нужно (# 4820)
        //
        //                final Map<EnrollmentDirection, MutableInt> countMap = SafeMap.get(MutableInt.class);
        //                final MQBuilder builder = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "rel", new String[] { EcgDistribRelationGen.L_ENTRANT_DIRECTION });
        //                builder.add(MQExpression.eq("rel", EcgDistribRelationGen.L_QUOTA+"."+EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
        //
        //                for (final RequestedEnrollmentDirection dir: builder.<RequestedEnrollmentDirection>getResultList(session)) {
        //                    countMap.get(dir.getEnrollmentDirection()).increment();
        //
        //                }
        //                for (final RequestedEnrollmentDirection dir: directions.values()) {
        //                    countMap.get(dir.getEnrollmentDirection()).increment();
        //                }
        //
        //                final Map<EnrollmentDirection, EcgDistribQuota> quotaMap = SafeMap.get(new SafeMap.Callback<EnrollmentDirection, EcgDistribQuota>() {
        //                    @Override public EcgDistribQuota resolve(final EnrollmentDirection key) {
        //                        final EcgDistribQuota q = new EcgDistribQuota();
        //                        q.setDirection(key);
        //                        q.setDistribution(model.getDistrib());
        //                        return q;
        //                    }
        //                });
        //
        //
        //                final MQBuilder qb = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q");
        //                qb.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
        //                final List<EcgDistribQuota> quotas = qb.getResultList(session);
        //                for (final EcgDistribQuota quota: quotas) {
        //                    quotaMap.put(quota.getDirection(), quota);
        //                }
        //
        //                for (final Map.Entry<EnrollmentDirection, EcgDistribQuota> e: quotaMap.entrySet()) {
        //                    final EcgDistribQuota quota = e.getValue();
        //                    final MutableInt removed = countMap.remove(e.getKey());
        //                    quota.setQuota(null == removed ? 0 : removed.intValue());
        //                    getSession().saveOrUpdate(quota);
        //                }
        //            }
        //
        //        }

        IEnrollmentCompetitionGroupDAO.INSTANCE.get().doAppendEntrantDirections(model.getDistrib(), directions.values());
    }


}
