/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantEnrollmentDocumentTab;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author oleyba
 * @since 16.03.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")
})
public class Model
{
    private Entrant _entrant = new Entrant();

    private List<EntrantRequest> _entrantRequestList;
    private EntrantRequest _entrantRequest;
    private Map<EntrantRequest, DynamicListDataSource<EntrantEnrollmentDocument>> _dataSourceMap;

    public String getRequestRegDate()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(_entrantRequest.getRegDate());
    }
    
    public boolean isAccessible()
    {
        return !getEntrant().isArchival();
    }

    // Getters & Setters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public List<EntrantRequest> getEntrantRequestList()
    {
        return _entrantRequestList;
    }

    public void setEntrantRequestList(List<EntrantRequest> entrantRequestList)
    {
        _entrantRequestList = entrantRequestList;
    }

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public Map<EntrantRequest, DynamicListDataSource<EntrantEnrollmentDocument>> getDataSourceMap()
    {
        return _dataSourceMap;
    }

    public void setDataSourceMap(Map<EntrantRequest, DynamicListDataSource<EntrantEnrollmentDocument>> dataSourceMap)
    {
        _dataSourceMap = dataSourceMap;
    }
}