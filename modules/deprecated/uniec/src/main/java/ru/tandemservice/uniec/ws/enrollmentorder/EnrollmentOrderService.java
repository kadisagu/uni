// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.uniec.ws.enrollmentorder;

import java.util.Date;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Vasily Zhukov
 * @since 25.02.2011
 */
@WebService
public class EnrollmentOrderService
{
    /**
     * Получение информации о приказах о зачислении
     *
     * @param enrollmentCampaignTitle название приемной кампании
     * @param dateFrom                Дата приказа с
     * @param dateTo                  Дата приказа по
     * @param compensationTypeId      Код вида возмещения затрат
     * @param orderTypeIds            Коды типов приказов о зачислении
     * @param formativeOrgUnitIds     Айди формирующих подразделений
     * @param developFormIds          Коды форм освоения
     * @param withPrintData           true, если нужно прицеплять печатные формы, false в противном случае
     * @return Инфомрация о приказах о зачислении
     */
    public EnrollmentOrderEnvironmentNode getEnrollmentOrderEnvironment(
            @WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle,
            @WebParam(name = "dateFrom") Date dateFrom,
            @WebParam(name = "dateTo") Date dateTo,
            @WebParam(name = "compensationTypeId") String compensationTypeId,
            @WebParam(name = "orderTypeIds") List<String> orderTypeIds,
            @WebParam(name = "formativeOrgUnitIds") List<String> formativeOrgUnitIds,
            @WebParam(name = "developFormIds") List<String> developFormIds,
            @WebParam(name = "withPrintData") Boolean withPrintData)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        return IEnrollmentOrderServiceDao.INSTANCE.get().getEnrollmentOrderEnvironmentData(enrollmentCampaignTitle, dateFrom, dateTo, compensationTypeId, orderTypeIds, formativeOrgUnitIds, developFormIds, withPrintData);
    }
}
