/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public interface IMarkDistributionDAO<Model extends MarkDistributionModel> extends IUniDao<Model>
{
}
