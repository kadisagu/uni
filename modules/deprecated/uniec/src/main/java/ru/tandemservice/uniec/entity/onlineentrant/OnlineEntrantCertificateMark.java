package ru.tandemservice.uniec.entity.onlineentrant;

import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.onlineentrant.gen.OnlineEntrantCertificateMarkGen;

/**
 * Балл сертификата ЕГЭ онлайн-абитуриента
 */
public class OnlineEntrantCertificateMark extends OnlineEntrantCertificateMarkGen
{

    public OnlineEntrantCertificateMark()
    {
    }

    public OnlineEntrantCertificateMark(OnlineEntrant entrant, StateExamSubject subject)
    {
        setEntrant(entrant);
        setSubject(subject);
    }
}