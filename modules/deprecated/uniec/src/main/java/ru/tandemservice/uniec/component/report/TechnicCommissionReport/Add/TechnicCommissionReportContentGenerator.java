/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.TechnicCommissionReport.Add;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.TechnicCommissionReport;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author agolubenko
 * @since 13.07.2009
 */
class TechnicCommissionReportContentGenerator
{
    // источник итогового балла -> форма сдачи
    private static Map<Integer, String> _markSource2SubjectPassForm = new HashMap<>();

    static
    {
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_DISCIPLINE_EXAM, UniecDefines.SUBJECT_PASS_FORM_EXAM);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_APPEAL_EXAM, UniecDefines.SUBJECT_PASS_FORM_EXAM);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_DISCIPLINE_TEST, UniecDefines.SUBJECT_PASS_FORM_TEST);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_APPEAL_TEST, UniecDefines.SUBJECT_PASS_FORM_TEST);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW, UniecDefines.SUBJECT_PASS_FORM_INTERVIEW);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_APPEAL_INTERVIEW, UniecDefines.SUBJECT_PASS_FORM_INTERVIEW);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM);
        _markSource2SubjectPassForm.put(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM);
    }

    private Session _session;
    private Model _model;

    // remove this vars. take it from _model
    private TechnicCommissionReport _report;
    private List<StudentCategory> _studentCategories;
    private List<String> _technicCommissions;

    private Map<String, WritableCellFormat> _formatMap = new HashMap<>();
    private WritableSheet _sheet;

    public TechnicCommissionReportContentGenerator(Model model, TechnicCommissionReport report, List<StudentCategory> studentCategories, List<String> technicCommissions, Session session)
    {
        _session = session;
        _model = model;

        _report = report;
        _studentCategories = studentCategories;
        _technicCommissions = technicCommissions;
    }

    public byte[] generateReportContent() throws Exception
    {
        if (_report.getEnrollmentCampaign().isExamSetDiff() && _studentCategories.size() != 1)
            throw new RuntimeException("Если есть различия в наборах, то отчет должен строиться по одной категории поступающих");

        // получить выбранные направления приема, удовлетворяющие параметрам выборки отчета
        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = getRequestedEnrollmentDirections();

        // получить дисциплины приемной кампании
        List<Discipline2RealizationWayRelation> examDisciplines = getExamDisciplines();

        // сформировать список строк
        List<Row> rows = getTableRows(requestedEnrollmentDirections);

        // заполнить итоговые баллы и их источники
        fillRowsMarks(rows, examDisciplines);

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создать лист
        _sheet = workbook.createSheet("Отчет", 0);
        _sheet.getSettings().setCopies(1);

        // подготовить форматы ячеек
        prepareFormats();

        // заполнить заголовк таблицы с 3-ей строки. высота заголовка 2 строки
        writeTableHeader(0, 3, examDisciplines, requestedEnrollmentDirections);

        // заполнить таблицу с 5-ой строки
        fillTableBody(0, 5, rows, getMarkSource2SubjectPassForm());

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    /**
     * Подготавливает форматы ячеек
     */
    private void prepareFormats() throws Exception
    {
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

        WritableCellFormat titleFormat = new WritableCellFormat(arial10bold);
        _formatMap.put("title", titleFormat);

        WritableCellFormat paramFormat = new WritableCellFormat(arial10);
        _formatMap.put("paramFormat", paramFormat);

        WritableCellFormat rowFormat = new WritableCellFormat(arial10);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setWrap(true);
        rowFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _formatMap.put("rowFormat", rowFormat);

        WritableCellFormat headerFormat = new WritableCellFormat(arial10bold);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setWrap(true);
        _formatMap.put("header", headerFormat);

        WritableCellFormat markFormat = new WritableCellFormat(arial10);
        markFormat.setAlignment(Alignment.CENTRE);
        markFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _formatMap.put("mark", markFormat);

        WritableCellFormat totalMarkFormat = new WritableCellFormat(arial10bold);
        totalMarkFormat.setAlignment(Alignment.CENTRE);
        totalMarkFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _formatMap.put("totalMark", totalMarkFormat);
    }

    /**
     * @return строка детализации отчета
     */
    private String getDetails()
    {
        StringBuilder result = new StringBuilder();
        result.append("Приемная кампания: ").append(_report.getEnrollmentCampaign().getTitle()).append(" ");
        result.append("Стадия: ").append(_report.getEnrollmentCampaignStage()).append(" ");
        result.append("Родственная группа: ").append(_model.getCompetitionGroup().getTitle()).append(" ");
        if (_report.getDateFrom() != null)
        {
            result.append("Заявления с: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getDateFrom())).append(" ");
        }
        if (_report.getDateTo() != null)
        {
            result.append("Заявления по: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getDateTo())).append(" ");
        }
        result.append("Вид возмещения затрат: ").append(_report.getCompensationType().getShortTitle()).append(" ");
        result.append("Категория поступающего: ").append(_report.getStudentCategory().toLowerCase()).append(" ");
        if (!StringUtils.isEmpty(_report.getTechnicCommission()))
        {
            result.append("Техническая комиссия: ").append(_report.getTechnicCommission());
        }
        return result.toString();
    }

    /**
     * @param rows            строки таблицы
     * @param examDisciplines дисциплины приемной кампании
     */
    private void fillRowsMarks(List<Row> rows, List<Discipline2RealizationWayRelation> examDisciplines)
    {
        MQBuilder builder = getDirectionsBuilder();
        EntrantDataUtil entrantDataUtil = new EntrantDataUtil(_session, _report.getEnrollmentCampaign(), builder);

        // для каждой строки
        for (Row row : rows)
        {
            // подготовить данные
            Map<Discipline2RealizationWayRelation, ChosenEntranceDiscipline> disciplines = new HashMap<>();
            for (ChosenEntranceDiscipline chosenDiscipline : entrantDataUtil.getChosenEntranceDisciplineSet(row.direction))
            {
                disciplines.put(chosenDiscipline.getEnrollmentCampaignDiscipline(), chosenDiscipline);
            }

            // для каждой дисциплины приемной кампании
            for (Discipline2RealizationWayRelation discipline : examDisciplines)
            {
                // добавить данные по итоговым баллам
                ChosenEntranceDiscipline chosenDiscipline = disciplines.get(discipline);
                row.marks.add(((chosenDiscipline != null) ? new Pair<>(chosenDiscipline.getFinalMarkSource(), chosenDiscipline.getFinalMark()) : null));
            }
        }
    }

    /**
     * Заполняет шапку таблицы
     */
    private void writeTableHeader(int column, int row, List<Discipline2RealizationWayRelation> examDisciplines, List<RequestedEnrollmentDirection> requestedEnrollmentDirections) throws Exception
    {
        WritableCellFormat titleFormat = _formatMap.get("title");
        WritableCellFormat paramFormat = _formatMap.get("paramFormat");
        WritableCellFormat headerFormat = _formatMap.get("header");

        _sheet.addCell(new Label(0, 0, "Сводка по заявлениям абитуриентов на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getFormingDate()), titleFormat));
        _sheet.addCell(new Label(0, 1, getDetails(), paramFormat));
        if (!requestedEnrollmentDirections.isEmpty())
        {
            DevelopForm developForm = requestedEnrollmentDirections.get(0).getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();
            _sheet.addCell(new Label(0, 2, "Форма освоения: " + developForm.getTitle(), paramFormat));
        }

        _sheet.addCell(new Label(column, row, "№ п/п", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 5);
        column++;

        _sheet.addCell(new Label(column, row, "Фамилия", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 15);
        column++;

        _sheet.addCell(new Label(column, row, "Имя", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 16);
        column++;

        _sheet.addCell(new Label(column, row, "Отчество", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 18);
        column++;

        _sheet.addCell(new Label(column, row, "№ паспорта", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 13);
        column++;

        _sheet.addCell(new Label(column, row, "Приоритеты", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 30);
        column++;

        _sheet.addCell(new Label(column, row, "Вид обучения", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 11);
        column++;

        _sheet.addCell(new Label(column, row, "Форма обучения", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 11);
        column++;

        _sheet.addCell(new Label(column, row, "Категория поступающего", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 11);
        column++;

        _sheet.addCell(new Label(column, row, "Документ об образовании", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 14);
        column++;

        _sheet.addCell(new Label(column, row, "Свидетельство ЕГЭ", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 11);
        column++;

        _sheet.addCell(new Label(column, row, "Льгота", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 10);
        column++;

        for (Discipline2RealizationWayRelation discipline : examDisciplines)
        {
            _sheet.addCell(new Label(column, row, discipline.getTitle(), headerFormat));
            _sheet.mergeCells(column, row, column + 1, row);
            _sheet.addCell(new Label(column, row + 1, "Тип экзамена", headerFormat));
            _sheet.setColumnView(column, 10);
            _sheet.addCell(new Label(column + 1, row + 1, "Балл", headerFormat));
            _sheet.setColumnView(column + 1, 10);
            column += 2;
        }

        _sheet.addCell(new Label(column, row, "Рейтинг", headerFormat));
        _sheet.mergeCells(column, row, column, row + 1);
        _sheet.setColumnView(column, 10);

        _sheet.setRowView(row + 1, 700);
    }

    /**
     * Заполняет таблицу
     */
    private void fillTableBody(int column, int row, List<Row> rows, Map<Integer, SubjectPassForm> markSource2SubjectPassForm) throws Exception
    {
        int counter = 1;
        for (Row item : rows)
            item.writeRow(_sheet, column, row++, counter++, markSource2SubjectPassForm, _formatMap);
    }

    /**
     * @param requestedEnrollmentDirections выбранные направления приема
     * @return строки таблицы
     */
    private List<Row> getTableRows(Collection<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        // вспомогательные структуры
        Map<Entrant, Row> entrant2Row = new HashMap<>();
        Map<Entrant, List<RequestedEnrollmentDirection>> entrant2Directions = SafeMap.get(key -> new ArrayList<>());

        // для каждого направления приема
        for (RequestedEnrollmentDirection direction : requestedEnrollmentDirections)
        {
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            entrant2Directions.get(entrant).add(direction);

            // если еще нет строки, то сформировать
            Row row = entrant2Row.get(entrant);
            if (row == null)
            {
                entrant2Row.put(entrant, row = new Row(direction));
            }
            // иначе добавить разделитель в приоритетах
            else
            {
                row.directionPriorities += ", ";
            }

            // если хотя бы по одному направлению сданы оригиналы, то отметить это
            if (direction.isOriginalDocumentHandedIn())
            {
                row.originalDocument = true;
            }

            // добавить направление в приоритеты
            row.directionPriorities += direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();
        }

        // проставить льготы
        Map<Person, String> benefits = getBenefits(entrant2Row.keySet());
        for (Entry<Entrant, Row> entry : entrant2Row.entrySet())
        {
            entry.getValue().benefits = benefits.get(entry.getKey().getPerson());
        }

        // проставить признак оригинальности сертификатов ЕГЭ
        for (Entrant entrant : getOriginalCertificateEntrants(entrant2Row.keySet()))
        {
            entrant2Row.get(entrant).originalCertificate = true;
        }

        List<Row> result = new ArrayList<>(entrant2Row.values());
        Collections.sort(result);
        return result;
    }

    /**
     * @return дисциплины приемной кампании
     */
    @SuppressWarnings("unchecked")
    private List<Discipline2RealizationWayRelation> getExamDisciplines()
    {
        Set<Discipline2RealizationWayRelation> result = new HashSet<>();

        // получить дисциплины по всем направлениям
        Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> direction2Disciplines = ExamSetUtil.getDirection2DisciplineMap(_session, _report.getEnrollmentCampaign(), _studentCategories, _report.getCompensationType());

        // для направлений отчета найти уникальный набор дисциплин
        Criteria criteria = _session.createCriteria(EnrollmentDirection.class);
        criteria.add(Restrictions.eq(EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
        for (EnrollmentDirection enrollmentDirection : (List<EnrollmentDirection>) criteria.list())
        {
            result.addAll(direction2Disciplines.get(enrollmentDirection));
        }
        return new ArrayList<>(result);
    }

    /**
     * @return билдер, по которому можно получить выбранные направления приема отчета
     */
    private MQBuilder getDirectionsBuilder()
    {
        MQBuilder builder;

        String enrollmentCampaignStage = _report.getEnrollmentCampaignStage();
        if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS) || enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_EXAMS))
        {
            builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");
            builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
            builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "enrollmentDirection");
            builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_STATE, "state");
            builder.addJoin("entrantRequest", EntrantRequest.L_ENTRANT, "entrant");
            builder.add(MQExpression.in("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _studentCategories));
            builder.add(MQExpression.eq("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _report.getCompensationType()));
            builder.add(MQExpression.eq("enrollmentDirection", EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
            builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, false));
            builder.add(MQExpression.notEq("state", EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_EXAMS))
            {
                builder.add(MQExpression.notIn("state", EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
            }
        } else if (enrollmentCampaignStage.equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT))
        {
            builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "preliminaryEnrollmentStudent");
            builder.addDomain("enrollmentDirection", EnrollmentDirection.ENTITY_CLASS);
            builder.addJoin("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "requestedEnrollmentDirection");
            builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
            builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_STATE, "state");
            builder.addJoin("entrantRequest", EntrantRequest.L_ENTRANT, "entrant");
            builder.add(MQExpression.in("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _studentCategories));
            builder.add(MQExpression.eq("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _report.getCompensationType()));
            builder.add(MQExpression.eq("enrollmentDirection", EnrollmentDirection.L_COMPETITION_GROUP, _model.getCompetitionGroup()));
            builder.add(MQExpression.eqProperty("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "enrollmentDirection", EnrollmentDirection.L_EDUCATION_ORG_UNIT));

            if (_model.isParallelActive() && _model.getParallel().isTrue())
                builder.add(MQExpression.eq("preliminaryEnrollmentStudent", PreliminaryEnrollmentStudent.P_PARALLEL, java.lang.Boolean.FALSE));
            builder.getSelectAliasList().clear();
            builder.addSelect("requestedEnrollmentDirection");
        } else
        {
            throw new RuntimeException("Unknown enrollment campaign stage");
        }

        if (_report.getDateFrom() != null)
        {
            builder.add(MQExpression.greatOrEq("entrantRequest", EntrantRequest.P_REG_DATE, _report.getDateFrom()));
        }
        if (_report.getDateTo() != null)
        {
            builder.add(MQExpression.less("entrantRequest", EntrantRequest.P_REG_DATE, CoreDateUtils.add(_report.getDateTo(), Calendar.DATE, 1)));
        }
        if (!_technicCommissions.isEmpty())
        {
            builder.add(MQExpression.in("entrantRequest", EntrantRequest.P_TECHNIC_COMMISSION, _technicCommissions));
        }
        return builder;
    }

    /**
     * @return выбранные направления приема отчета
     */
    private List<RequestedEnrollmentDirection> getRequestedEnrollmentDirections()
    {
        MQBuilder builder = getDirectionsBuilder();
        builder.addJoinFetch("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        builder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        builder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");
        return builder.addOrder("requestedEnrollmentDirection", RequestedEnrollmentDirection.P_PRIORITY).getResultList(_session);
    }

    /**
     * @param entrants абитуриенты
     * @return льготы абитуриентов
     */
    @SuppressWarnings("unchecked")
    private Map<Person, String> getBenefits(Collection<Entrant> entrants)
    {
        if (entrants.isEmpty())
        {
            return Collections.emptyMap();
        }

        // получить персон по абитуриентам
        Collection<Person> persons = new ArrayList<>();
        for (Entrant entrant : entrants)
        {
            persons.add(entrant.getPerson());
        }

        Map<Person, List<String>> person2Benefits = SafeMap.get(key -> new ArrayList<>());

        // получить льготы по каждой персоне
        Criteria criteria = _session.createCriteria(PersonBenefit.class);
        criteria.createAlias(PersonBenefit.L_BENEFIT, "benefit");
        criteria.add(Restrictions.in(PersonBenefit.L_PERSON, persons));
        criteria.addOrder(Order.asc("benefit." + Benefit.P_TITLE));
        for (PersonBenefit benefit : (List<PersonBenefit>) criteria.list())
        {
            person2Benefits.get(benefit.getPerson()).add(benefit.getBenefit().getTitle());
        }

        // сформировать строки льгот
        Map<Person, String> result = new HashMap<>();
        for (Entry<Person, List<String>> entry : person2Benefits.entrySet())
        {
            result.put(entry.getKey(), StringUtils.join(entry.getValue(), ", "));
        }
        return result;
    }

    /**
     * @param entrants абитуриенты
     * @return абитуриенты, которые сдали оригиналы сертификатов
     */
    @SuppressWarnings("unchecked")
    private Set<Entrant> getOriginalCertificateEntrants(Collection<Entrant> entrants)
    {
        if (entrants.isEmpty())
        {
            return Collections.emptySet();
        }

        Criteria criteria = _session.createCriteria(EntrantStateExamCertificate.class);
        criteria.add(Restrictions.in(EntrantStateExamCertificate.L_ENTRANT, entrants));
        criteria.add(Restrictions.eq(EntrantStateExamCertificate.P_ORIGINAL, true));
        criteria.setProjection(Projections.distinct(Projections.property(EntrantStateExamCertificate.L_ENTRANT)));
        return new HashSet<>(criteria.list());
    }

    /**
     * @return источник балла -> форма сдачи
     */
    @SuppressWarnings("unchecked")
    private Map<Integer, SubjectPassForm> getMarkSource2SubjectPassForm()
    {
        Map<Integer, SubjectPassForm> result = new HashMap<>();

        Map<String, SubjectPassForm> subjectPassFormByCode = new HashMap<>();
        for (SubjectPassForm subjectPassForm : (List<SubjectPassForm>) _session.createCriteria(SubjectPassForm.class).list())
        {
            subjectPassFormByCode.put(subjectPassForm.getCode(), subjectPassForm);
        }
        for (Entry<Integer, String> entry : _markSource2SubjectPassForm.entrySet())
        {
            Integer markSource = entry.getKey();
            String code = entry.getValue();
            result.put(markSource, subjectPassFormByCode.get(code));
        }
        return result;
    }

    /**
     * Строка таблицы отчета
     *
     * @author agolubenko
     * @since 15.07.2009
     */
    private static class Row implements Comparable<Row>
    {
        RequestedEnrollmentDirection direction;
        String directionPriorities = "";
        boolean originalDocument;
        boolean originalCertificate;
        String benefits;

        List<Pair<Integer, Double>> marks = new ArrayList<>();

        public Row(RequestedEnrollmentDirection direction)
        {
            this.direction = direction;
        }

        void writeRow(WritableSheet sheet, int column, int row, int number, Map<Integer, SubjectPassForm> markSource2SubjectPassForm, Map<String, WritableCellFormat> formatMap) throws Exception
        {
            WritableCellFormat rowFormat = formatMap.get("rowFormat");
            WritableCellFormat markFormat = formatMap.get("mark");
            WritableCellFormat totalMarkFormat = formatMap.get("totalMark");

            IdentityCard identityCard = direction.getEntrantRequest().getEntrant().getPerson().getIdentityCard();
            double totalMark = 0.0;

            // номер
            sheet.addCell(new Number(column++, row, (double) number, rowFormat));

            // данные удостоверения личности
            sheet.addCell(new Label(column++, row, identityCard.getLastName(), rowFormat));
            sheet.addCell(new Label(column++, row, identityCard.getFirstName(), rowFormat));
            sheet.addCell(new Label(column++, row, identityCard.getMiddleName(), rowFormat));
            sheet.addCell(new Label(column++, row, identityCard.getFullNumber(), rowFormat));

            // данные по направлениям
            sheet.addCell(new Label(column++, row, directionPriorities, rowFormat));
            sheet.addCell(new Label(column++, row, direction.getCompensationType().getShortTitle().toLowerCase(), rowFormat));
            sheet.addCell(new Label(column++, row, direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase(), rowFormat));
            sheet.addCell(new Label(column++, row, direction.getStudentCategory().getTitle().toLowerCase(), rowFormat));

            // данные по документам и льготам
            sheet.addCell(new Label(column++, row, originalDocument ? "оригинал" : "копия", rowFormat));
            sheet.addCell(new Label(column++, row, originalCertificate ? "оригинал" : "копия", rowFormat));
            sheet.addCell(new Label(column++, row, benefits, rowFormat));

            // данные по оценкам
            for (Pair<Integer, Double> pair : marks)
            {
                // если надо сдавать дисциплину
                if (pair != null)
                {
                    Integer source = pair.getX();
                    Double mark = pair.getY();

                    // в источнике сокращенное название формы сдачи
                    sheet.addCell(new Label(column++, row, (source != null) ? (!source.equals(UniecDefines.FINAL_MARK_OLYMPIAD)) ? markSource2SubjectPassForm.get(source).getShortTitle() : "олимпиада" : null, markFormat));

                    // в балле итоговый балл

                    if (mark != null)
                    {
                        sheet.addCell(new Number(column++, row, mark, markFormat));
                        totalMark += mark; // считаем рейтинг
                    } else
                    {
                        sheet.addCell(new Blank(column++, row, markFormat));
                    }
                } else // иначе
                {
                    sheet.addCell(new Label(column++, row, "x", markFormat));
                    sheet.addCell(new Label(column++, row, "x", markFormat));
                }
            }

            sheet.addCell(new Number(column, row, totalMark, totalMarkFormat));
        }

        /**
         * @return длина строки без учета динамических колонок
         */
        static int getRowLength()
        {
            return 13;
        }

        @Override
        public int compareTo(Row o)
        {
            EntrantRequest r1 = this.direction.getEntrantRequest();
            EntrantRequest r2 = o.direction.getEntrantRequest();

            // сравнить по дате регистрации
            int result = r1.getRegDate().compareTo(r2.getRegDate());

            // если в одно время, то по фамилии
            if (result == 0) {
                result = Person.FULL_FIO_AND_ID_COMPARATOR.compare(r1.getEntrant().getPerson(), r2.getEntrant().getPerson());
            }
            return result;
        }
    }
}
