/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantRequestStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

import java.util.List;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
@Input({
        @Bind(key = "onlineEntrantId", binding = "onlineEntrant.id"),
        @Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey")
})
public class Model extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model
{
    private OnlineEntrant _onlineEntrant = new OnlineEntrant();
    private String _entrantMasterPermKey;

    private IMultiSelectModel _customStateModel;
    private List<EntrantCustomStateCI> _customStateList;

    private IMultiSelectModel _individualProgressModel;
    private List<IndividualProgress> _individualProgressList;

    public String getEntrantMasterPermKey()
    {
        if(StringUtils.isEmpty(_entrantMasterPermKey)) return "addEntrantMaster";
        return _entrantMasterPermKey;
    }

    public void setEntrantMasterPermKey(String entrantMasterPermKey)
    {
        _entrantMasterPermKey = entrantMasterPermKey;
    }

    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(OnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public List<IndividualProgress> getIndividualProgressList()
    {
        return _individualProgressList;
    }

    public void setIndividualProgressList(List<IndividualProgress> individualProgressList)
    {
        _individualProgressList = individualProgressList;
    }

    public IMultiSelectModel getIndividualProgressModel()
    {
        return _individualProgressModel;
    }

    public void setIndividualProgressModel(IMultiSelectModel individualProgressModel)
    {
        _individualProgressModel = individualProgressModel;
    }

    public List<EntrantCustomStateCI> getCustomStateList()
    {
        return _customStateList;
    }

    public void setCustomStateList(List<EntrantCustomStateCI> customStateList)
    {
        _customStateList = customStateList;
    }

    public IMultiSelectModel getCustomStateModel()
    {
        return _customStateModel;
    }

    public void setCustomStateModel(IMultiSelectModel customStateModel)
    {
        _customStateModel = customStateModel;
    }
}
