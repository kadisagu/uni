package ru.tandemservice.uniec.component.report.EnrolledDealList.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniec.entity.report.EnrolledDealListReport;

@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private EnrolledDealListReport _report = new EnrolledDealListReport();

    public EnrolledDealListReport getReport()
    {
        return _report;
    }

    public void setReport(EnrolledDealListReport report)
    {
        _report = report;
    }
}
