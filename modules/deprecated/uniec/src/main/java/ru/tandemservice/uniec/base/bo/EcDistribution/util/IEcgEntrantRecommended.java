/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 21.07.2011
 */
public interface IEcgEntrantRecommended extends IEntity
{
    IEcgDistribution getDistribution();
    
    RequestedEnrollmentDirection getDirection();
    
    TargetAdmissionKind getTargetAdmissionKind();
}
