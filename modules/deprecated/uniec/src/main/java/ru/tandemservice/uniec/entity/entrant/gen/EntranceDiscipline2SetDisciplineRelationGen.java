package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вступительное испытание по выбору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceDiscipline2SetDisciplineRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation";
    public static final String ENTITY_NAME = "entranceDiscipline2SetDisciplineRelation";
    public static final int VERSION_HASH = 1807447251;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANCE_DISCIPLINE = "entranceDiscipline";
    public static final String L_SET_DISCIPLINE = "setDiscipline";

    private EntranceDiscipline _entranceDiscipline;     // Вступительное испытание для направления для приема
    private SetDiscipline _setDiscipline;     // Элемент набора вступительных испытаний

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null.
     */
    @NotNull
    public EntranceDiscipline getEntranceDiscipline()
    {
        return _entranceDiscipline;
    }

    /**
     * @param entranceDiscipline Вступительное испытание для направления для приема. Свойство не может быть null.
     */
    public void setEntranceDiscipline(EntranceDiscipline entranceDiscipline)
    {
        dirty(_entranceDiscipline, entranceDiscipline);
        _entranceDiscipline = entranceDiscipline;
    }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public SetDiscipline getSetDiscipline()
    {
        return _setDiscipline;
    }

    /**
     * @param setDiscipline Элемент набора вступительных испытаний. Свойство не может быть null.
     */
    public void setSetDiscipline(SetDiscipline setDiscipline)
    {
        dirty(_setDiscipline, setDiscipline);
        _setDiscipline = setDiscipline;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntranceDiscipline2SetDisciplineRelationGen)
        {
            setEntranceDiscipline(((EntranceDiscipline2SetDisciplineRelation)another).getEntranceDiscipline());
            setSetDiscipline(((EntranceDiscipline2SetDisciplineRelation)another).getSetDiscipline());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceDiscipline2SetDisciplineRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceDiscipline2SetDisciplineRelation.class;
        }

        public T newInstance()
        {
            return (T) new EntranceDiscipline2SetDisciplineRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entranceDiscipline":
                    return obj.getEntranceDiscipline();
                case "setDiscipline":
                    return obj.getSetDiscipline();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entranceDiscipline":
                    obj.setEntranceDiscipline((EntranceDiscipline) value);
                    return;
                case "setDiscipline":
                    obj.setSetDiscipline((SetDiscipline) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entranceDiscipline":
                        return true;
                case "setDiscipline":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entranceDiscipline":
                    return true;
                case "setDiscipline":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entranceDiscipline":
                    return EntranceDiscipline.class;
                case "setDiscipline":
                    return SetDiscipline.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceDiscipline2SetDisciplineRelation> _dslPath = new Path<EntranceDiscipline2SetDisciplineRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceDiscipline2SetDisciplineRelation");
    }
            

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation#getEntranceDiscipline()
     */
    public static EntranceDiscipline.Path<EntranceDiscipline> entranceDiscipline()
    {
        return _dslPath.entranceDiscipline();
    }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation#getSetDiscipline()
     */
    public static SetDiscipline.Path<SetDiscipline> setDiscipline()
    {
        return _dslPath.setDiscipline();
    }

    public static class Path<E extends EntranceDiscipline2SetDisciplineRelation> extends EntityPath<E>
    {
        private EntranceDiscipline.Path<EntranceDiscipline> _entranceDiscipline;
        private SetDiscipline.Path<SetDiscipline> _setDiscipline;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вступительное испытание для направления для приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation#getEntranceDiscipline()
     */
        public EntranceDiscipline.Path<EntranceDiscipline> entranceDiscipline()
        {
            if(_entranceDiscipline == null )
                _entranceDiscipline = new EntranceDiscipline.Path<EntranceDiscipline>(L_ENTRANCE_DISCIPLINE, this);
            return _entranceDiscipline;
        }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline2SetDisciplineRelation#getSetDiscipline()
     */
        public SetDiscipline.Path<SetDiscipline> setDiscipline()
        {
            if(_setDiscipline == null )
                _setDiscipline = new SetDiscipline.Path<SetDiscipline>(L_SET_DISCIPLINE, this);
            return _setDiscipline;
        }

        public Class getEntityClass()
        {
            return EntranceDiscipline2SetDisciplineRelation.class;
        }

        public String getEntityName()
        {
            return "entranceDiscipline2SetDisciplineRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
