/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcSettings.ui.IndividualProgressSetting;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcSettings.EcSettingsManager;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
public class EcSettingsIndividualProgressSettingUI extends UIPresenter
{
    public static final String PARAM_ENR_CAMPAIGN_DS = "enrollmentCampaign";

    @Override
    public void onComponentRefresh()
    {
        if (getSettings().get(PARAM_ENR_CAMPAIGN_DS) == null)
        {
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            getSettings().set(PARAM_ENR_CAMPAIGN_DS, enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
        }
    }

    public void onRefresh()
    {
        saveSettings();
    }

    public boolean isCurrentDocumentTypeUse() {
        return getConfig().getDataSource(EcSettingsIndividualProgressSetting.ENTRANT_INDIVIDUAL_PROGRESS_DS).<DataWrapper>getCurrent().get(EnrCampaignEntrantIndividualProgress.INDIVID_ARCHIEVEMENT_USED);
    }

    public void onToggleEntrantIndividualProgressUse() {
        EcSettingsManager.instance().individualProgressDAO().toggleIndividualProgressUse(getListenerParameterAsLong(), getEnrollmentCampaign());
        getSupport().setRefreshScheduled(true);
    }

    public boolean isNothingSelected()
    {
        return getEnrollmentCampaign() == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_ENR_CAMPAIGN_DS, getEnrollmentCampaign());
    }

    // Getters & Setters

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(PARAM_ENR_CAMPAIGN_DS);
    }
}