/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EnrollmentDocumentPriorityList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 06.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<EnrollmentDocument> dataSource = model.getDataSource();

        List<EnrollmentDocument> list = getList(EnrollmentDocument.class, EnrollmentDocument.P_PRIORITY);
        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);

        Map<EnrollmentDocument, UsedEnrollmentDocument> map = new HashMap<EnrollmentDocument, UsedEnrollmentDocument>();
        for (UsedEnrollmentDocument usedEnrollmentDocument : getList(UsedEnrollmentDocument.class, UsedEnrollmentDocument.enrollmentCampaign().s(), model.getEnrollmentCampaign()))
            map.put(usedEnrollmentDocument.getEnrollmentDocument(), usedEnrollmentDocument);

        for (ViewWrapper<EnrollmentDocument> wrapper : ViewWrapper.<EnrollmentDocument> getPatchedList(dataSource))
        {
            UsedEnrollmentDocument settings = map.get(wrapper.getEntity());
            wrapper.setViewProperty("used", null != settings && settings.isUsed());
            wrapper.setViewProperty("notUsed", null == settings || !settings.isUsed());
            wrapper.setViewProperty("printEducationDocumentInfo", null == settings ? Boolean.FALSE : settings.isPrintEducationDocumentInfo());
            wrapper.setViewProperty(UsedEnrollmentDocument.P_PRINT_EDU_DOCUMENT_ATTACHMENT_INFO, null == settings ? Boolean.FALSE : settings.isPrintEduDocumentAttachmentInfo());
            wrapper.setViewProperty("printStateExamCertificateNumber", null == settings ? Boolean.FALSE : settings.isPrintStateExamCertificateNumber());
            wrapper.setViewProperty("printOriginalityInfo", null == settings ? Boolean.FALSE : settings.isPrintOriginalityInfo());
        }
    }

    @Override
    public void updatePriority(Long documentId, int direction)
    {
        final Session session = getSession();

        EnrollmentDocument document = getNotNull(EnrollmentDocument.class, documentId);
        List<EnrollmentDocument> list = getList(EnrollmentDocument.class, EnrollmentDocument.P_PRIORITY);
        int i = 0;
        while (i < list.size() && !list.get(i).equals(document))
        {
            i++;
        }
        if (direction == -1 && i == 0)
        {
            return;
        }
        if (direction == 1 && i == list.size() - 1)
        {
            return;
        }

        EnrollmentDocument prev = list.get(i + direction);

        int currPriority = document.getPriority();
        int prevPriority = prev.getPriority();

        prev.setPriority(Integer.MIN_VALUE);
        session.update(prev);
        session.flush();

        document.setPriority(prevPriority);
        session.update(document);
        session.flush();

        prev.setPriority(currPriority);
        session.update(document);
        session.flush();
    }

    @Override
    public void changeEducationDocumentInfo(Model model, Long documentId)
    {
        UsedEnrollmentDocument settings = getSettings(model.getEnrollmentCampaign(), documentId);
        settings.setPrintEducationDocumentInfo(!settings.isPrintEducationDocumentInfo());
        getSession().saveOrUpdate(settings);
    }

    @Override
    public void changeAttachmentInfo(Model model, Long documentId)
    {
        UsedEnrollmentDocument settings = getSettings(model.getEnrollmentCampaign(), documentId);
        settings.setPrintEduDocumentAttachmentInfo(!settings.isPrintEduDocumentAttachmentInfo());
        getSession().saveOrUpdate(settings);
    }

    @Override
    public void changePrintStateExamCertificateNumber (Model model, Long documentId)
    {
        UsedEnrollmentDocument settings = getSettings(model.getEnrollmentCampaign(), documentId);
        settings.setPrintStateExamCertificateNumber(!settings.isPrintStateExamCertificateNumber());
        getSession().saveOrUpdate(settings);
    }

    @Override
    public void changePrintOriginalityInfo(Model model, Long documentId)
    {
        UsedEnrollmentDocument settings = getSettings(model.getEnrollmentCampaign(), documentId);
        settings.setPrintOriginalityInfo(!settings.isPrintOriginalityInfo());
        getSession().saveOrUpdate(settings);
    }

    @Override
    public void changeUsed(Model model, Long documentId)
    {
        UsedEnrollmentDocument settings = getSettings(model.getEnrollmentCampaign(), documentId);
        settings.setUsed(!settings.isUsed());
        getSession().saveOrUpdate(settings);
    }

    private UsedEnrollmentDocument getSettings(EnrollmentCampaign enrollmentCampaign, Long documentId)
    {
        EnrollmentDocument enrollmentDocument = get(EnrollmentDocument.class, documentId);

        Criteria criteria = getSession().createCriteria(UsedEnrollmentDocument.class);
        criteria.add(Restrictions.eq(UsedEnrollmentDocument.enrollmentCampaign().s(), enrollmentCampaign));
        criteria.add(Restrictions.eq(UsedEnrollmentDocument.enrollmentDocument().s(), enrollmentDocument));
        UsedEnrollmentDocument settings = (UsedEnrollmentDocument) criteria.uniqueResult();

        if (settings == null)
        {
            settings = new UsedEnrollmentDocument();
            settings.setEnrollmentCampaign(enrollmentCampaign);
            settings.setEnrollmentDocument(enrollmentDocument);
        }

        return settings;
    }
}
