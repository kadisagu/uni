/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.base.bo.EcEntrant.ui.IndividualProgressAddEdit.EcEntrantIndividualProgressAddEdit;
import ru.tandemservice.uniec.dao.IStateExamCertificateDAO;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.unimove.IAbstractOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public final class Controller extends AbstractBusinessController<IDAO, Model>
{
    private IStateExamCertificateDAO _certificateDAO;

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareOlympiadDiplomaDataSource(component);
        prepareEntrantStateExamCertificates(component);
        preparePreliminaryEnrollmentDataSource(component);
        prepareIndividualProgressDataSource(component);
    }


    //Таблица индивидуальных достижений
    private void prepareIndividualProgressDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getIndividualProgressDataSource() != null) return;

        DynamicListDataSource<EntrantIndividualProgress> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareIndividualProgressDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Вид индивидуального достижения", EntrantIndividualProgress.individualProgressType().title()).setClickable(false));
        SimpleColumn requests = new SimpleColumn("Заявления", "requests")
        {
            @Override
            public String getContent(final IEntity entity)
            {
                return getDao().getRequests4IndividualProgress((EntrantIndividualProgress) entity);
            }
        };
        requests.setClickable(false);
        dataSource.addColumn(requests);
        dataSource.addColumn(new SimpleColumn("Балл", EntrantIndividualProgress.mark()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditIndividualProgress").setPermissionKey("editEntrantIndividualProgress").setDisableHandler(model.getDefaultEntityHandler()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить индивидуальное достижение «{0}»?", EntrantIndividualProgress.individualProgressType().title()).setPermissionKey("deleteEntrantIndividualProgress").setDisableHandler(model.getDefaultEntityHandler()));
        model.setIndividualProgressDataSource(dataSource);
    }

    private void preparePreliminaryEnrollmentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getPreliminaryEnrollmentDataSource() != null) return;

        DynamicListDataSource<PreliminaryEnrollmentStudent> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().preparePreliminaryEnrollmentDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", PreliminaryEnrollmentStudent.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", PreliminaryEnrollmentStudent.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", PreliminaryEnrollmentStudent.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Переведен с", new String[]{PreliminaryEnrollmentStudent.P_TRANSFER_SOURCE, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип приказа", PreliminaryEnrollmentStudent.entrantEnrollmentOrderType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условия зачисления", PreliminaryEnrollmentStudent.P_ENROLLMENT_CONDITIONS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", new String[]{PreliminaryEnrollmentStudent.VIEW_P_L_ENROLLMENT_ORDER + "." + IAbstractOrder.P_NUMBER}).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintStudentCard", "Печать ЛК").setPermissionKey("printEntrantStudentCard"));
        model.setPreliminaryEnrollmentDataSource(dataSource);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareOlympiadDiplomaDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getOlympiadDiplomaDataSource() != null) return;

        DynamicListDataSource<Model.OlympiadDiplomaWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareOlympiadDiplomaDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Тип олимпиады", "diploma." + OlympiadDiploma.L_DIPLOMA_TYPE + ".title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Степень отличия", "diploma." + OlympiadDiploma.L_DEGREE + ".title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Название олимпиады", "diploma." + OlympiadDiploma.P_OLYMPIAD).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Предмет", "diploma." + OlympiadDiploma.P_SUBJECT).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Серия и номер", "diploma." + OlympiadDiploma.DIPLOM_KEY).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", "diploma." + OlympiadDiploma.P_ISSUANCE_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Населенный пункт", "diploma." + OlympiadDiploma.L_SETTLEMENT + "." + AddressItem.P_TITLE).setOrderable(false).setClickable(false));
        if (model.getEntrant().getEnrollmentCampaign().isOlympiadDiplomaForDirection())
            dataSource.addColumn(new MultiValuesColumn("Зачитывать по направлению", Model.OlympiadDiplomaWrapper.ENROLLMENT_DIRECTION_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new MultiValuesColumn("Засчитывать по дисциплине", Model.OlympiadDiplomaWrapper.DISCIPLINE_LIST_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditOlympiadDiploma").setPermissionKey("editEntrantOlympiadDiploma").setDisableHandler(model.getDefaultEntityHandler()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить диплом участника олимпиады «{0}»?", Model.OlympiadDiplomaWrapper.P_DIPLOMA + "." + OlympiadDiploma.P_OLYMPIAD).setPermissionKey("deleteEntrantOlympiadDiploma").setDisableHandler(model.getDefaultEntityHandler()));

        model.setOlympiadDiplomaDataSource(dataSource);
    }

    private void prepareEntrantStateExamCertificates(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getEntrantStateExamCertificatesDataSource() != null) return;

        DynamicListDataSource<EntrantStateExamCertificate> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEntrantStateExamCertificatesDataSource(model);
        });

        dataSource.addColumn(new ToggleColumn("Оригинал", EntrantStateExamCertificate.P_ORIGINAL).setListener("onChangeCertificateOriginal").setPermissionKey("editEntrantStateExamCertificate_entrant").setDisableHandler(model.getDefaultEntityHandler()));
        dataSource.addColumn(new BlockColumn<>("subjectsMarks", "Баллы по предметам"));
        dataSource.addColumn(new ToggleColumn("Зачтено", EntrantStateExamCertificate.P_ACCEPTED).setListener("onChangeCertificateChecked").setPermissionKey("editCheckedEntrantStateExamCertificate_entrant").setDisableHandler(model.getDefaultEntityHandler()));
        dataSource.addColumn(new ToggleColumn("Проверено", EntrantStateExamCertificate.P_SENT).setListener("onChangeCertificateSent").setPermissionKey("editSentEntrantStateExamCertificate_entrant").setDisableHandler(model.getDefaultEntityHandler()));

        IEntityHandler entityHandler = entity -> (!model.isAccessible() || ((EntrantStateExamCertificate) entity).isSent());
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStateExamCertificate", null).setPermissionKey("editEntrantStateExamCertificate_entrant").setDisableHandler(entityHandler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить свидетельство о результатах ЕГЭ № {0}?", EntrantStateExamCertificate.P_TITLE).setPermissionKey("deleteEntrantStateExamCertificate_entrant").setDisableHandler(entityHandler));

        model.setEntrantStateExamCertificatesDataSource(dataSource);
    }

    public void onClickPrintStudentCard(IBusinessComponent component)
    {
        byte[] content = getDao().printStudentCard((Long) component.getListenerParameter());
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Личная карточка.rtf");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));
    }

    public void onClickAddOlympiadDiploma(IBusinessComponent component)
    {
        Map<String, Object> params = new HashMap<>();
        params.put(ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model.DIPLOMA_ID_BINDING_KEY, null);
        params.put(ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model.ENTRANT_ID_BINDING_KEY, getModel(component).getEntrant().getId());
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.OLYMPIAD_DIPLOMA_ADD_EDIT, params));
    }

    public void onClickEditOlympiadDiploma(IBusinessComponent component)
    {
        Map<String, Object> params = new HashMap<>();
        params.put(ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model.DIPLOMA_ID_BINDING_KEY, component.getListenerParameter());
        params.put(ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model.ENTRANT_ID_BINDING_KEY, null);
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.OLYMPIAD_DIPLOMA_ADD_EDIT, params));
    }

    public void onClickAddEntrantEnrolmentRecommendation(IBusinessComponent component)
    {
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.ENTRANT_ENROLMENT_RECOMMENTDATION_ADD_EDIT));
    }

    public void onClickAddStateExamCertificate(IBusinessComponent component)
    {
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.ENTRANT_STATE_EXAM_CERTIFICATE_ADD_EDIT));
    }

    public void onClickEditEntrantRegistrationDate(IBusinessComponent component)
    {
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.ENTRANT_REGISTRATION_DATE_EDIT));
    }

    public void onClickEditStateExamCertificate(IBusinessComponent component)
    {
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.ENTRANT_STATE_EXAM_CERTIFICATE_ADD_EDIT, new ParametersMap().add("certificateId", component.getListenerParameter())));
    }

    public void onChangeCertificateOriginal(IBusinessComponent component)
    {
        _certificateDAO.changeCertificateOriginal((Long) component.getListenerParameter());
    }

    public void onChangeCertificateChecked(IBusinessComponent component)
    {
        try
        {
            _certificateDAO.changeCertificateChecked((Long) component.getListenerParameter());
        } finally
        {
            // при следующем рендере почистить сессию
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onChangeCertificateSent(IBusinessComponent component)
    {
        try
        {
            _certificateDAO.changeCertificateSent((Long) component.getListenerParameter());
        } finally
        {
            // при следующем рендере почистить сессию
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onClickDelete(IBusinessComponent context)
    {
        getDao().delete((Long) context.getListenerParameter());
    }

    public void onClickEditEntrantData(IBusinessComponent component)
    {
        component.createChildRegion(Model.SCOPE_NAME, new ComponentActivator(IUniecComponents.ENTRANT_EDIT, new ParametersMap().add("entrantId", getModel(component).getEntrant().getId())));
    }

    public void setCertificateDAO(IStateExamCertificateDAO certificateDAO)
    {
        _certificateDAO = certificateDAO;
    }

    public void onClickAddIndividualProgress(IBusinessComponent component)
    {
        Model model = getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
            new ComponentActivator(EcEntrantIndividualProgressAddEdit.class.getSimpleName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getEntrant().getId())));
    }

    public void onClickEditIndividualProgress(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(EcEntrantIndividualProgressAddEdit.class.getSimpleName(),
                                                          new ParametersMap().add("individualProgress", component.getListenerParameter())));
    }

}
