/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderPreStudentDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String EC_ORDER_PRE_STUDENT_DS_PARAM = "ecOrderPreStudentDSParam";
    public static final String EDU_INSTITUTION = "eduInstitutionView";

    public EcOrderPreStudentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Param param = context.get(EC_ORDER_PRE_STUDENT_DS_PARAM);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.column("p");
        builder.joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias("p"), "ou");
        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("p")), DQLExpressions.value(param.getOrder().getEnrollmentCampaign())));
        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.entrantEnrollmentOrderType().fromAlias("p")), DQLExpressions.value(param.getOrder().getType())));
        builder.where(DQLExpressions.eq(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().archival().fromAlias("p")), DQLExpressions.value(Boolean.FALSE)));

        if (param.isEnrollParagraphPerEduOrgUnit())
        {
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), DQLExpressions.value(param.getFormativeOrgUnit())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.territorialOrgUnit().fromAlias("ou")), DQLExpressions.value(param.getTerritorialOrgUnit())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("ou")), DQLExpressions.value(param.getEducationLevelsHighSchool())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("ou")), DQLExpressions.value(param.getDevelopForm())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developCondition().fromAlias("ou")), DQLExpressions.value(param.getDevelopCondition())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("ou")), DQLExpressions.value(param.getDevelopTech())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developPeriod().fromAlias("ou")), DQLExpressions.value(param.getDevelopPeriod())));
        } else
        {
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("ou")), DQLExpressions.value(param.getFormativeOrgUnit())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("ou")), DQLExpressions.value(param.getEducationLevelsHighSchool())));
            builder.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("ou")), DQLExpressions.value(param.getDevelopForm())));
        }

        // не отображаем абитуриентов, которые уже в других приказах
        if (param.isEditForm())
        {
            builder.where(DQLExpressions.notIn(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("p")),
                    new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e")
                            .column(DQLExpressions.property(EnrollmentExtract.entity().id().fromAlias("e")))
                            .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(param.getOrder().getEnrollmentCampaign())))
                            .where(DQLExpressions.ne(DQLExpressions.property(EnrollmentExtract.paragraph().fromAlias("e")), DQLExpressions.value(param.getParagraph())))
                            .buildQuery()
            ));
        } else
        {
            builder.where(DQLExpressions.notIn(DQLExpressions.property(PreliminaryEnrollmentStudent.id().fromAlias("p")),
                    new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e")
                            .column(DQLExpressions.property(EnrollmentExtract.entity().id().fromAlias("e")))
                            .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(param.getOrder().getEnrollmentCampaign())))
                            .buildQuery()
            ));
        }

        builder.order(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio().fromAlias("p")));

        List<PreliminaryEnrollmentStudent> list = builder.createStatement(context.getSession()).list();

        List<DataWrapper> recordList = new ArrayList<DataWrapper>();

        for (PreliminaryEnrollmentStudent preStudent : list)
        {
            DataWrapper record = new DataWrapper(preStudent.getId(), preStudent.getTitle(), preStudent);

            PersonEduInstitution personEduInstitution = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution();
            String eduInsitutionTitle = "";
            if (personEduInstitution != null)
            {
                EducationLevelStage educationLevelStage = personEduInstitution.getEducationLevel();
                if (educationLevelStage != null)
                    eduInsitutionTitle = educationLevelStage.getShortTitle();
            }

            record.setProperty(EDU_INSTITUTION, eduInsitutionTitle);

            recordList.add(record);
        }

        return ListOutputBuilder.get(input, recordList).build();
    }

    public static final class Param
    {
        private EnrollmentOrder _order;
        private EnrollmentParagraph _paragraph;
        private OrgUnit _formativeOrgUnit;
        private OrgUnit _territorialOrgUnit;
        private EducationLevelsHighSchool _educationLevelsHighSchool;
        private DevelopForm _developForm;
        private DevelopCondition _developCondition;
        private DevelopTech _developTech;
        private DevelopPeriod _developPeriod;
        private boolean _enrollParagraphPerEduOrgUnit;
        private boolean _editForm;

        private Param()
        {
        }

        public static Param createAddFormParam(EnrollmentOrder order)
        {
            Param param = new Param();
            param._editForm = false;
            param._order = order;
            return param;
        }

        public static Param createEditFormParam(EnrollmentParagraph paragraph)
        {
            Param param = new Param();
            param._editForm = true;
            param._order = (EnrollmentOrder) paragraph.getOrder();
            param._paragraph = paragraph;
            return param;
        }

        public Param addParams(OrgUnit formativeOrgUnit, OrgUnit terrritorialOrgUnit, EducationLevelsHighSchool educationLevelsHighSchool, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod)
        {
            _enrollParagraphPerEduOrgUnit = true;
            _formativeOrgUnit = formativeOrgUnit;
            _territorialOrgUnit = terrritorialOrgUnit;
            _educationLevelsHighSchool = educationLevelsHighSchool;
            _developForm = developForm;
            _developCondition = developCondition;
            _developTech = developTech;
            _developPeriod = developPeriod;
            return this;
        }

        public Param addParams(OrgUnit formativeOrgUnit, EducationLevelsHighSchool educationLevelsHighSchool, DevelopForm developForm)
        {
            _formativeOrgUnit = formativeOrgUnit;
            _educationLevelsHighSchool = educationLevelsHighSchool;
            _developForm = developForm;
            return this;
        }

        // Getters

        public EnrollmentOrder getOrder()
        {
            return _order;
        }

        public EnrollmentParagraph getParagraph()
        {
            return _paragraph;
        }

        public OrgUnit getFormativeOrgUnit()
        {
            return _formativeOrgUnit;
        }

        public OrgUnit getTerritorialOrgUnit()
        {
            return _territorialOrgUnit;
        }

        public EducationLevelsHighSchool getEducationLevelsHighSchool()
        {
            return _educationLevelsHighSchool;
        }

        public DevelopForm getDevelopForm()
        {
            return _developForm;
        }

        public DevelopCondition getDevelopCondition()
        {
            return _developCondition;
        }

        public DevelopTech getDevelopTech()
        {
            return _developTech;
        }

        public DevelopPeriod getDevelopPeriod()
        {
            return _developPeriod;
        }

        public boolean isEnrollParagraphPerEduOrgUnit()
        {
            return _enrollParagraphPerEduOrgUnit;
        }

        public boolean isEditForm()
        {
            return _editForm;
        }
    }
}
