/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.valid.ValidatorException;

/**
 * @author vip_delete
 * @since 30.03.2009
 */
public class EntrantMarkValidator extends BaseValidator
{
    private boolean _forAppeal;
    private int _minMark;
    private int _maxMark;

    public EntrantMarkValidator(boolean forAppeal, int minMark, int maxMark)
    {
        _forAppeal = forAppeal;
        _minMark = minMark;
        _maxMark = maxMark;
    }

    @Override
    public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
    {
        String input = (String) object;

        if (input == null) return;

        input = input.trim();

        if (!_forAppeal)
            if ("н".equals(input)) return;

        try
        {
            input = input.replace(',', '.');
            double mark = Double.parseDouble(input);

            if (mark < _minMark)
                throw new ValidatorException("Оценка должна быть не менее " + _minMark);
            if (mark > _maxMark)
                throw new ValidatorException("Оценка должна быть не более " + _maxMark);

            double frac = mark - (int) mark;
            if (frac != 0.0 && frac != 0.5)
                throw new ValidatorException("Оценка должна быть кратна 0.5");
        } catch (NumberFormatException e)
        {
            if (_forAppeal)
                throw new ValidatorException("В поле оценки по апелляции можно вводить только числа");
            else
                throw new ValidatorException("В поле оценки можно вводить только числа и букву 'н', если абитуриент не явился");
        }
    }
}
