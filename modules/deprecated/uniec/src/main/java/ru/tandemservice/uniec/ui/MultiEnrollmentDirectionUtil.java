/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.ui;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil.Parameters.Filter;

import java.util.*;

/**
 * @author agolubenko
 * @since 01.07.2009
 */
public class MultiEnrollmentDirectionUtil
{
    /**
     * @param model модель
     * @return список направлений приема, удовлетворяющих всем фильтрам на форме
     */
    @SuppressWarnings("unchecked")
    public static List<EnrollmentDirection> getEnrollmentDirections(Model model, Session session)
    {
        Criteria criteria = session.createCriteria(EnrollmentDirection.class, "enrollmentDirection");
        criteria.createAlias("enrollmentDirection." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        if (applyFilters(model, criteria, Arrays.asList(Filter.values())))
            return Collections.emptyList();
        return criteria.list();
    }

    public static ISelectModel createFormativeOrgUnitAutocompleteModel(final IPrincipalContext context, final Model model)
    {
        return new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                ListResult<OrgUnit> result = MultiEnrollmentDirectionUtil.findValues(model, Filter.FORMATIVE_ORG_UNIT, filter, OrgUnit.P_TITLE);
                if (context != null)
                {
                    CollectionUtils.filter(result.getObjects(), object -> {
                        OrgUnit orgUnit = (OrgUnit) object;
                        return CoreServices.securityService().check(orgUnit, context, "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode());
                    });
                }
                return result;
            }
        };
    }

    public static ISelectModel createTerritorialOrgUnitAutocompleteModel(final IPrincipalContext context, final Model model)
    {
        return new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_FULL_TITLE)
        {
            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                ListResult<OrgUnit> result = MultiEnrollmentDirectionUtil.findValues(model, Filter.TERRITORIAL_ORG_UNIT, filter, OrgUnit.P_TERRITORIAL_FULL_TITLE);
                if (context != null)
                {
                    CollectionUtils.filter(result.getObjects(), object -> {
                        OrgUnit orgUnit = (OrgUnit) object;
                        return CoreServices.securityService().check(orgUnit, context, "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode());
                    });
                }
                return result;
            }
        };
    }

    public static ISelectModel createEducationLevelsHighSchoolModel(final Model model)
    {
        return new FullCheckSelectModel(EducationLevelsHighSchool.P_FULL_TITLE)
        {
            @Override
            public ListResult<EducationLevelsHighSchool> findValues(String filter)
            {
                return MultiEnrollmentDirectionUtil.findValues(model, Filter.EDUCATION_LEVEL_HIGH_SCHOOL, filter, EducationLevelsHighSchool.P_DISPLAYABLE_TITLE);
            }
        };
    }

    public static ISelectModel createDevelopFormModel(final Model model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            public ListResult<DevelopForm> findValues(String filter)
            {
                return MultiEnrollmentDirectionUtil.findValues(model, Filter.DEVELOP_FORM, filter, DevelopForm.P_CODE);
            }
        };
    }

    public static ISelectModel createDevelopConditionModel(final Model model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            public ListResult<DevelopCondition> findValues(String filter)
            {
                return MultiEnrollmentDirectionUtil.findValues(model, Filter.DEVELOP_CONDITION, filter, DevelopCondition.P_CODE);
            }
        };
    }

    public static ISelectModel createDevelopTechModel(final Model model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            public ListResult<DevelopTech> findValues(String filter)
            {
                return MultiEnrollmentDirectionUtil.findValues(model, Filter.DEVELOP_TECH, filter, DevelopTech.P_CODE);
            }
        };
    }

    public static ISelectModel createDevelopPeriodModel(final Model model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            public ListResult<DevelopPeriod> findValues(String filter)
            {
                return MultiEnrollmentDirectionUtil.findValues(model, Filter.DEVELOP_PERIOD, filter, DevelopPeriod.P_PRIORITY);
            }
        };
    }

    /**
     * @param model модель
     * @param filter фильтр
     * @param filterString строка фильтрации
     * @param orderProperty поле для сортировки
     * @return список объектов доступных для выбора в данном фильтре, 
     * то есть удовлетворяющих условиям выборки всех фильтров, от которых он зависит
     */
    @SuppressWarnings("unchecked")
    private static <T extends IEntity> ListResult<T> findValues(Model model, Filter filter, String filterString, String orderProperty)
    {
        Session session = DataAccessServices.dao().getComponentSession();
        // получаем сначала идентификаторы этих объектов
        List<Long> ids = findValueIds(session, model, filter);
        if (ids.isEmpty())
        {
            return ListResult.getEmpty();
        }

//        Number count;
//        {
//            Criteria criteria = session.createCriteria(filter.getClazz());
//            criteria.add(Restrictions.in(IEntity.P_ID, ids));
//
//            // добавляем фильтрацию по названию, если необходимо
//            if (StringUtils.isNotEmpty(filterString)) {
//                criteria.add(Restrictions.ilike("title", "%" + filterString + "%"));
//            }
//            criteria.setProjection(Projections.count("id"));
//            count = (Number) criteria.uniqueResult();
//        }
        
        // строим условия выборки самих объектов
        Criteria criteria = session.createCriteria(filter.getClazz());
        criteria.add(Restrictions.in(IEntity.P_ID, ids));
        
        // добавляем фильтрацию по названию, если необходимо
        if (StringUtils.isNotEmpty(filterString)) {
            criteria.add(Restrictions.ilike("title", "%" + filterString + "%"));
        }
        
        // сортируем
        criteria.addOrder(Order.asc(orderProperty));
        //criteria.setMaxResults(50);
        
        // возвращаем результат
        //return new ListResult<T>(criteria.list(), count == null ? 0 : count.longValue());
        return new ListResult<>(criteria.list());
    }

    /**
     * @param session сессия
     * @param model модель
     * @param filter фильтр
     * @return список идентификаторов объектов доступных для выбора в данном фильтре, 
     * то есть удовлетворяющих условиям выборки всех фильтров, от которых он зависит
     */
    @SuppressWarnings("unchecked")
    private static List<Long> findValueIds(Session session, Model model, Filter filter)
    {
        // получаем список фильтров (родительских), от которых зависит данный
        List<Filter> parentFilters = new ArrayList<>();
        for (int i = 0; i < filter.ordinal(); i++)
        {
            parentFilters.add(Filter.values()[i]);
        }

        // строим условия выборки
        Criteria criteria = session.createCriteria(EnrollmentDirection.class, "enrollmentDirection");
        criteria.createAlias("enrollmentDirection." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.createAlias(filter.getPropertyName(), "target");

        // применяем фильтры
        if (applyFilters(model, criteria, parentFilters))
        {
            return Collections.emptyList();
        }

        // выбираем только идентификаторы
        criteria.setProjection(Projections.distinct(Projections.property("target." + IEntity.P_ID)));

        // возвращаем результат
        return criteria.list();
    }

    /**
     * Применяет фильтры к критериям выборки
     * 
     * @param model модель
     * @param criteria критерии
     * @param filters фильтры
     * @return true если нужно возвратить пустой список, false в противном случае
     */
    private static boolean applyFilters(Model model, Criteria criteria, List<Filter> filters)
    {
        for (Filter filter : filters)
        {
            if (applyFilter(model, criteria, filter))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Применяет фильтр к критериям выборки
     * 
     * @param model модель
     * @param criteria критерии
     * @param filter фильтр
     * @return true если нужно возвратить пустой список, false в противном случае
     */
    private static boolean applyFilter(Model model, Criteria criteria, Filter filter)
    {
        List<?> values = getList(model, filter);
        Parameters parameters = model.getParameters();

        //Если фильтр является обязательным, то надо возвратить пустой список
        if (values == null)
        {
            return parameters.isRequired(filter);
        }
        else
        {
            criteria.add((!values.isEmpty()) ? Restrictions.in(filter.getPropertyName(), values) : Restrictions.isNull(filter.getPropertyName()));
        }
        return false;
    }

    /**
     * @param model модель
     * @param filter фильтр
     * @return список выбранных на форме значений для этого фильтра
     */
    public static List<?> getList(Model model, Filter filter)
    {
        switch (filter)
        {
            case ENROLLMENT_CAMPAIGN:
                return model.getSelectedEnrollmentCampaignList();
            case FORMATIVE_ORG_UNIT:
                return model.getSelectedFormativeOrgUnitList();
            case TERRITORIAL_ORG_UNIT:
                return model.getSelectedTerritorialOrgUnitList();
            case EDUCATION_LEVEL_HIGH_SCHOOL:
                return model.getSelectedEducationLevelHighSchoolList();
            case DEVELOP_FORM:
                return model.getSelectedDevelopFormList();
            case DEVELOP_CONDITION:
                return model.getSelectedDevelopConditionList();
            case DEVELOP_TECH:
                return model.getSelectedDevelopTechList();
            case DEVELOP_PERIOD:
                return model.getSelectedDevelopPeriodList();
            default:
                throw new RuntimeException();
        }
    }
    
    /**
     * Метод служит для использования в селектах без множественного выбора в переопределенных методах модели
     * 
     * @param object объект
     * @return список выбранных опций (либо пустой, если ничего не выбрано, либо с единственной выбранной)
     */
    public static <T> List<T> getSingletonList(T object)
    {
        return (object != null) ? Collections.singletonList(object) : Collections.<T> emptyList();
    }

    /**
     * Параметры фильтров
     * 
     * По умолчанию все фильтры являются необязательными (
     * 
     * @author agolubenko
     * @since 06.07.2009
     */
    public static class Parameters
    {
        // всегда ли фильтр включен
        private Map<Filter, Boolean> necessity = new HashMap<>();
        {
            setRequired(false, Filter.values());
        }

        /**
         * @param filter фильтр
         * @return true если фильтр обязателен, false в противном случае
         */
        public boolean isRequired(Filter filter)
        {
            return necessity.get(filter);
        }

        /**
         * Устанавливает обязательность для фильтров
         * 
         * @param required true если фильтры обязательны, false в противном случае
         * @param filters фильтры
         */
        public void setRequired(boolean required, Filter... filters)
        {
            for (Filter filter : filters)
            {
                this.necessity.put(filter, required);
            }
        }

        /**
         * Фильтр 
         * @author agolubenko
         * @since 06.07.2009
         */
        public enum Filter
        {
            ENROLLMENT_CAMPAIGN(EnrollmentCampaign.class, "enrollmentDirection", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN), 
            FORMATIVE_ORG_UNIT(OrgUnit.class, "educationOrgUnit", EducationOrgUnit.L_FORMATIVE_ORG_UNIT), 
            TERRITORIAL_ORG_UNIT(OrgUnit.class, "educationOrgUnit", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), 
            EDUCATION_LEVEL_HIGH_SCHOOL(EducationLevelsHighSchool.class, "educationOrgUnit", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL), 
            DEVELOP_FORM(DevelopForm.class, "educationOrgUnit", EducationOrgUnit.L_DEVELOP_FORM), 
            DEVELOP_CONDITION(DevelopCondition.class, "educationOrgUnit", EducationOrgUnit.L_DEVELOP_CONDITION), 
            DEVELOP_TECH(DevelopTech.class, "educationOrgUnit", EducationOrgUnit.L_DEVELOP_TECH), 
            DEVELOP_PERIOD(DevelopPeriod.class, "educationOrgUnit", EducationOrgUnit.L_DEVELOP_PERIOD);

            private Class<? extends IEntity> clazz;
            private String alias;
            private String property;

            Filter(Class<? extends IEntity> clazz, String alias, String property)
            {
                this.clazz = clazz;
                this.alias = alias;
                this.property = property;
            }

            public String getPropertyName()
            {
                return alias + "." + property;
            }

            public Class<? extends IEntity> getClazz()
            {
                return clazz;
            }
        }
    }

    /**
     * Модель с зависимыми селектами, для которой применяется логика данной утилиты.
     * Правила имплементации методов:
     * Если фильтр неактивен, то возвращает null
     * Если фильтр активен, но ничего не выбрано, то возвращает пустой список
     * Если фильтр активен, и что-то выбрано, то возвращает список выбранных опций
     * 
     * @author agolubenko
     * @since 06.07.2009
     */
    public interface Model
    {
        Parameters getParameters();

        List<EnrollmentCampaign> getSelectedEnrollmentCampaignList();

        List<OrgUnit> getSelectedFormativeOrgUnitList();

        List<OrgUnit> getSelectedTerritorialOrgUnitList();

        List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList();

        List<DevelopForm> getSelectedDevelopFormList();

        List<DevelopCondition> getSelectedDevelopConditionList();

        List<DevelopTech> getSelectedDevelopTechList();

        List<DevelopPeriod> getSelectedDevelopPeriodList();
    }
}
