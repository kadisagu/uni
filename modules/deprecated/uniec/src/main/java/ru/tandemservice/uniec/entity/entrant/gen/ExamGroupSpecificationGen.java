package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Характеристика экзам. группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamGroupSpecificationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification";
    public static final String ENTITY_NAME = "examGroupSpecification";
    public static final int VERSION_HASH = 1943580765;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_GROUP = "examGroup";
    public static final String L_DISCIPLINE = "discipline";
    public static final String P_START_DISCIPLINE_DATE = "startDisciplineDate";
    public static final String P_FINISH_DISCIPLINE_DATE = "finishDisciplineDate";
    public static final String P_ADDRESS = "address";
    public static final String P_CLASSROOM = "classroom";
    public static final String P_EXAM_COMMISSION_MEMBERSHIP = "examCommissionMembership";

    private ExamGroup _examGroup;     // Экзаменационная группа
    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private Date _startDisciplineDate;     // Дата и время начала ВИ
    private Date _finishDisciplineDate;     // Дата и время окончания ВИ
    private String _address;     // Адрес
    private String _classroom;     // Аудитория
    private String _examCommissionMembership;     // Состав экзаменационной комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Экзаменационная группа. Свойство не может быть null.
     */
    @NotNull
    public ExamGroup getExamGroup()
    {
        return _examGroup;
    }

    /**
     * @param examGroup Экзаменационная группа. Свойство не может быть null.
     */
    public void setExamGroup(ExamGroup examGroup)
    {
        dirty(_examGroup, examGroup);
        _examGroup = examGroup;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Дата и время начала ВИ.
     */
    public Date getStartDisciplineDate()
    {
        return _startDisciplineDate;
    }

    /**
     * @param startDisciplineDate Дата и время начала ВИ.
     */
    public void setStartDisciplineDate(Date startDisciplineDate)
    {
        dirty(_startDisciplineDate, startDisciplineDate);
        _startDisciplineDate = startDisciplineDate;
    }

    /**
     * @return Дата и время окончания ВИ.
     */
    public Date getFinishDisciplineDate()
    {
        return _finishDisciplineDate;
    }

    /**
     * @param finishDisciplineDate Дата и время окончания ВИ.
     */
    public void setFinishDisciplineDate(Date finishDisciplineDate)
    {
        dirty(_finishDisciplineDate, finishDisciplineDate);
        _finishDisciplineDate = finishDisciplineDate;
    }

    /**
     * @return Адрес.
     */
    @Length(max=255)
    public String getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес.
     */
    public void setAddress(String address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Аудитория.
     */
    @Length(max=255)
    public String getClassroom()
    {
        return _classroom;
    }

    /**
     * @param classroom Аудитория.
     */
    public void setClassroom(String classroom)
    {
        dirty(_classroom, classroom);
        _classroom = classroom;
    }

    /**
     * @return Состав экзаменационной комиссии.
     */
    @Length(max=255)
    public String getExamCommissionMembership()
    {
        return _examCommissionMembership;
    }

    /**
     * @param examCommissionMembership Состав экзаменационной комиссии.
     */
    public void setExamCommissionMembership(String examCommissionMembership)
    {
        dirty(_examCommissionMembership, examCommissionMembership);
        _examCommissionMembership = examCommissionMembership;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamGroupSpecificationGen)
        {
            setExamGroup(((ExamGroupSpecification)another).getExamGroup());
            setDiscipline(((ExamGroupSpecification)another).getDiscipline());
            setStartDisciplineDate(((ExamGroupSpecification)another).getStartDisciplineDate());
            setFinishDisciplineDate(((ExamGroupSpecification)another).getFinishDisciplineDate());
            setAddress(((ExamGroupSpecification)another).getAddress());
            setClassroom(((ExamGroupSpecification)another).getClassroom());
            setExamCommissionMembership(((ExamGroupSpecification)another).getExamCommissionMembership());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamGroupSpecificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamGroupSpecification.class;
        }

        public T newInstance()
        {
            return (T) new ExamGroupSpecification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examGroup":
                    return obj.getExamGroup();
                case "discipline":
                    return obj.getDiscipline();
                case "startDisciplineDate":
                    return obj.getStartDisciplineDate();
                case "finishDisciplineDate":
                    return obj.getFinishDisciplineDate();
                case "address":
                    return obj.getAddress();
                case "classroom":
                    return obj.getClassroom();
                case "examCommissionMembership":
                    return obj.getExamCommissionMembership();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examGroup":
                    obj.setExamGroup((ExamGroup) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "startDisciplineDate":
                    obj.setStartDisciplineDate((Date) value);
                    return;
                case "finishDisciplineDate":
                    obj.setFinishDisciplineDate((Date) value);
                    return;
                case "address":
                    obj.setAddress((String) value);
                    return;
                case "classroom":
                    obj.setClassroom((String) value);
                    return;
                case "examCommissionMembership":
                    obj.setExamCommissionMembership((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examGroup":
                        return true;
                case "discipline":
                        return true;
                case "startDisciplineDate":
                        return true;
                case "finishDisciplineDate":
                        return true;
                case "address":
                        return true;
                case "classroom":
                        return true;
                case "examCommissionMembership":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examGroup":
                    return true;
                case "discipline":
                    return true;
                case "startDisciplineDate":
                    return true;
                case "finishDisciplineDate":
                    return true;
                case "address":
                    return true;
                case "classroom":
                    return true;
                case "examCommissionMembership":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examGroup":
                    return ExamGroup.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "startDisciplineDate":
                    return Date.class;
                case "finishDisciplineDate":
                    return Date.class;
                case "address":
                    return String.class;
                case "classroom":
                    return String.class;
                case "examCommissionMembership":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamGroupSpecification> _dslPath = new Path<ExamGroupSpecification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamGroupSpecification");
    }
            

    /**
     * @return Экзаменационная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getExamGroup()
     */
    public static ExamGroup.Path<ExamGroup> examGroup()
    {
        return _dslPath.examGroup();
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Дата и время начала ВИ.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getStartDisciplineDate()
     */
    public static PropertyPath<Date> startDisciplineDate()
    {
        return _dslPath.startDisciplineDate();
    }

    /**
     * @return Дата и время окончания ВИ.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getFinishDisciplineDate()
     */
    public static PropertyPath<Date> finishDisciplineDate()
    {
        return _dslPath.finishDisciplineDate();
    }

    /**
     * @return Адрес.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getAddress()
     */
    public static PropertyPath<String> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getClassroom()
     */
    public static PropertyPath<String> classroom()
    {
        return _dslPath.classroom();
    }

    /**
     * @return Состав экзаменационной комиссии.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getExamCommissionMembership()
     */
    public static PropertyPath<String> examCommissionMembership()
    {
        return _dslPath.examCommissionMembership();
    }

    public static class Path<E extends ExamGroupSpecification> extends EntityPath<E>
    {
        private ExamGroup.Path<ExamGroup> _examGroup;
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private PropertyPath<Date> _startDisciplineDate;
        private PropertyPath<Date> _finishDisciplineDate;
        private PropertyPath<String> _address;
        private PropertyPath<String> _classroom;
        private PropertyPath<String> _examCommissionMembership;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Экзаменационная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getExamGroup()
     */
        public ExamGroup.Path<ExamGroup> examGroup()
        {
            if(_examGroup == null )
                _examGroup = new ExamGroup.Path<ExamGroup>(L_EXAM_GROUP, this);
            return _examGroup;
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Дата и время начала ВИ.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getStartDisciplineDate()
     */
        public PropertyPath<Date> startDisciplineDate()
        {
            if(_startDisciplineDate == null )
                _startDisciplineDate = new PropertyPath<Date>(ExamGroupSpecificationGen.P_START_DISCIPLINE_DATE, this);
            return _startDisciplineDate;
        }

    /**
     * @return Дата и время окончания ВИ.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getFinishDisciplineDate()
     */
        public PropertyPath<Date> finishDisciplineDate()
        {
            if(_finishDisciplineDate == null )
                _finishDisciplineDate = new PropertyPath<Date>(ExamGroupSpecificationGen.P_FINISH_DISCIPLINE_DATE, this);
            return _finishDisciplineDate;
        }

    /**
     * @return Адрес.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getAddress()
     */
        public PropertyPath<String> address()
        {
            if(_address == null )
                _address = new PropertyPath<String>(ExamGroupSpecificationGen.P_ADDRESS, this);
            return _address;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getClassroom()
     */
        public PropertyPath<String> classroom()
        {
            if(_classroom == null )
                _classroom = new PropertyPath<String>(ExamGroupSpecificationGen.P_CLASSROOM, this);
            return _classroom;
        }

    /**
     * @return Состав экзаменационной комиссии.
     * @see ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification#getExamCommissionMembership()
     */
        public PropertyPath<String> examCommissionMembership()
        {
            if(_examCommissionMembership == null )
                _examCommissionMembership = new PropertyPath<String>(ExamGroupSpecificationGen.P_EXAM_COMMISSION_MEMBERSHIP, this);
            return _examCommissionMembership;
        }

        public Class getEntityClass()
        {
            return ExamGroupSpecification.class;
        }

        public String getEntityName()
        {
            return "examGroupSpecification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
