/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 29.07.2010
 */
public interface IDAO extends IUniDao<Model>
{
}
