package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.CompetitiveAdmission;

import java.util.List;
import java.util.Map;

import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase.Row;
import ru.tandemservice.uniec.dao.ecg.EnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO.IEntrantRateRow;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.DAO implements IDAO {
    @Override
    protected List<IEntrantRateRow> getEntrants4Add(final ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.Model model) {
        return IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows4Add(model.getDistrib(), EnrollmentCompetitionGroupDAO.COMPETITIVE_ADMISSION_SELECTOR);
    }

    @Override
    protected void postProcessRows(EcgDistribObject distrib, List<Row> entrantRateRows, Map<Long, Integer> dir2quotaMap) {
        if (null != distrib.getParent()) {
            for (final ModelBase.Row row: entrantRateRows) {
                // выделяем всех (потом по рейтингу отсеятся)
                row.setChecked(true);
            }
        }
        super.postProcessRows(distrib, entrantRateRows, dir2quotaMap);
    }
}
