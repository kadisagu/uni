/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.runtime;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.person.base.entity.NotUsedRelationDegree;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;

/**
 * @author agolubenko
 * @since 02.06.2009
 */
public class UniecRuntimeExtensionDAO extends UniBaseDao implements IUniecRuntimeExtensionDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void doInitNotUsedRelationDegrees()
    {
        IDataSettings settings = DataSettingsFacade.getSettings("");
        if (settings.get("NotUsedRelationDegreesInitialized") == null)
        {
            Session session = getSession();

            Criteria criteria = session.createCriteria(RelationDegree.class);
            criteria.add(Restrictions.not(Restrictions.in(RelationDegree.P_CODE, new String[]{RelationDegreeCodes.FATHER, RelationDegreeCodes.MOTHER, RelationDegreeCodes.TUTOR})));
            for (RelationDegree relationDegree : (List<RelationDegree>) criteria.list())
            {
                NotUsedRelationDegree notUsedRelationDegree = new NotUsedRelationDegree();
                notUsedRelationDegree.setRelationDegree(relationDegree);
                notUsedRelationDegree.setPersonRoleName(Entrant.class.getSimpleName());
                session.save(notUsedRelationDegree);
            }

            settings.set("NotUsedRelationDegreesInitialized", true);
            DataSettingsFacade.saveSettings(settings);
        }
    }
}
