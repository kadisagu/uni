package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительный статус абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantCustomStateGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantCustomState";
    public static final String ENTITY_NAME = "entrantCustomState";
    public static final int VERSION_HASH = 1931161468;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_DESCRIPTION = "description";
    public static final String L_ENTRANT = "entrant";
    public static final String L_CUSTOM_STATE = "customState";

    private Date _beginDate;     // Дата начала действия статуса
    private Date _endDate;     // Дата окончания действия статуса
    private String _description;     // Примечание
    private Entrant _entrant;     // Абитуриент
    private EntrantCustomStateCI _customState;     // Дополнительный статус абитуриента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала действия статуса.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала действия статуса.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания действия статуса.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания действия статуса.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Примечание.
     */
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Примечание.
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Дополнительный статус абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EntrantCustomStateCI getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус абитуриента. Свойство не может быть null.
     */
    public void setCustomState(EntrantCustomStateCI customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantCustomStateGen)
        {
            setBeginDate(((EntrantCustomState)another).getBeginDate());
            setEndDate(((EntrantCustomState)another).getEndDate());
            setDescription(((EntrantCustomState)another).getDescription());
            setEntrant(((EntrantCustomState)another).getEntrant());
            setCustomState(((EntrantCustomState)another).getCustomState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantCustomStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantCustomState.class;
        }

        public T newInstance()
        {
            return (T) new EntrantCustomState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "description":
                    return obj.getDescription();
                case "entrant":
                    return obj.getEntrant();
                case "customState":
                    return obj.getCustomState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "customState":
                    obj.setCustomState((EntrantCustomStateCI) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "description":
                        return true;
                case "entrant":
                        return true;
                case "customState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "description":
                    return true;
                case "entrant":
                    return true;
                case "customState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "description":
                    return String.class;
                case "entrant":
                    return Entrant.class;
                case "customState":
                    return EntrantCustomStateCI.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantCustomState> _dslPath = new Path<EntrantCustomState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantCustomState");
    }
            

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Дополнительный статус абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getCustomState()
     */
    public static EntrantCustomStateCI.Path<EntrantCustomStateCI> customState()
    {
        return _dslPath.customState();
    }

    public static class Path<E extends EntrantCustomState> extends EntityPath<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _description;
        private Entrant.Path<Entrant> _entrant;
        private EntrantCustomStateCI.Path<EntrantCustomStateCI> _customState;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EntrantCustomStateGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EntrantCustomStateGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EntrantCustomStateGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Дополнительный статус абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantCustomState#getCustomState()
     */
        public EntrantCustomStateCI.Path<EntrantCustomStateCI> customState()
        {
            if(_customState == null )
                _customState = new EntrantCustomStateCI.Path<EntrantCustomStateCI>(L_CUSTOM_STATE, this);
            return _customState;
        }

        public Class getEntityClass()
        {
            return EntrantCustomState.class;
        }

        public String getEntityName()
        {
            return "entrantCustomState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
