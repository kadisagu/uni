/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.SummarySheetExamAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexander Shaburov
 * @since 18.07.12
 */
@Configuration
public class EcReportSummarySheetExamAdd extends BusinessComponentManager
{
    public static String MODEL = "model";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
