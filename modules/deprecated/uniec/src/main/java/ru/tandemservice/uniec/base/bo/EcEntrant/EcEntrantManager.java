/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.EcEntrantDao;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.EntrantCustomStateDAO;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.IEcEntrantDao;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.IEntrantCustomStateDAO;

/**
 * @author Vasily Zhukov
 * @since 26.04.2012
 */
@Configuration
public class EcEntrantManager extends BusinessObjectManager
{
    public static EcEntrantManager instance()
    {
        return instance(EcEntrantManager.class);
    }

    @Bean
    public IEcEntrantDao dao()
    {
        return new EcEntrantDao();
    }

    @Bean
    public IEntrantCustomStateDAO entrantCustomStateDAO()
    {
        return new EntrantCustomStateDAO();
    }
}
