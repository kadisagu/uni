/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.Discipline2RealizationForm;

import java.util.HashMap;
import java.util.Map;

import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author agolubenko
 * @since 09.06.2008
 */
public class Model extends ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent.Model<Discipline2RealizationWayRelation, SubjectPassFormWrapper, Boolean>
{
    private Map<CompensationType, Map<CellCoordinates, Boolean>> _compensationType2MatrixData = new HashMap<CompensationType, Map<CellCoordinates, Boolean>>();
    private CompensationType _compensationType;
    
    private CompensationType _budget;
    private CompensationType _contract;

    private String _selectedPage;

    public Map<CompensationType, Map<CellCoordinates, Boolean>> getCompensationType2MatrixData()
    {
        return _compensationType2MatrixData;
    }

    public void setCompensationType2MatrixData(Map<CompensationType, Map<CellCoordinates, Boolean>> compensationType2MatrixData)
    {
        _compensationType2MatrixData = compensationType2MatrixData;
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public CompensationType getBudget()
    {
        return _budget;
    }

    public void setBudget(CompensationType budget)
    {
        _budget = budget;
    }

    public CompensationType getContract()
    {
        return _contract;
    }

    public void setContract(CompensationType contract)
    {
        _contract = contract;
    }
}
