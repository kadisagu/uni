/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.dao.examgroup.IAbstractExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public abstract class MarkDistributionController<IDAO extends IMarkDistributionDAO<Model>, Model extends MarkDistributionModel> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        // создаем общие сеттинги для всех наследуемых бизнес компонентов компонентов
        model.setCommon(DataSettingsFacade.getSettings("abstractMarkDistribution"));

        // создаем сеттинги только для текущего бизнес компонента
        model.setSettings(component.getSettings());

        // подготавливаем данные в модели
        getDao().prepare(model);

        if (model.getRedirectComponentName() != null)
        {
            deactivate(component);

            activateInRoot(component, new ComponentActivator(model.getRedirectComponentName()));
        } else
        {
            // создаем датасурс
            prepareListDataSource(component);
        }
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("ФИО абитуриента", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT_REQUEST, EntrantRequest.L_ENTRANT, Entrant.L_PERSON, Person.P_FULLFIO}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пол", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT_REQUEST, EntrantRequest.L_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.L_SEX, Sex.P_SHORT_TITLE}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT_REQUEST, EntrantRequest.L_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.P_BIRTH_DATE}, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT_REQUEST, EntrantRequest.L_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.P_FULL_NUMBER}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ экзам. листа", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT_REQUEST, EntrantRequest.P_STRING_NUMBER}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.COLUMN_EXAM_PASS_DISCIPLINE_PAPER_CODE, "Шифр работы").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.COLUMN_EXAM_PASS_DISCIPLINE_MARK, "Оценка").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.COLUMN_EXAM_PASS_DISCIPLINE_EXAMINERS, "Экзаменаторы").setOrderable(false).setClickable(false));
        model.setDataSource(dataSource);
    }

    // поиск фильтров

    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        Model model = getModel(component);

        // вначале определяем, какой бизнес компонент требуется для ввода оценок по экзаменационным группам
        IAbstractExamGroupLogic logic = UniecDAOFacade.getExamGroupSetDao().getCurrentExamGroupLogic(model.getEnrollmentCampaign());
        String redirectComponentName = logic.getMarkDistributionBusinessComponent();
        String currentComponentName = getClass().getPackage().getName();

        if (!redirectComponentName.equals(currentComponentName))
        {
            // сохраняем сеттинги
            DataSettingsFacade.saveSettings(model.getCommon());

            // деактивируем текущий компонент
            deactivate(component);

            // загружаем новый
            activateInRoot(component, new ComponentActivator(logic.getMarkDistributionBusinessComponent()));
        }
    }

    public void onClickShow(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
}
