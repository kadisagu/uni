/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author vip_delete
 * @since 25.03.2009
 */
class DisciplineMarkWrapper extends IdentifiableWrapper<Discipline2RealizationWayRelation>
{
    private static final long serialVersionUID = 9023173945066633141L;
    public static final String COLUMN_STATE_EXAM_INTERNAL = "columnStateExamInternal";
    public static final String COLUMN_STATE_EXAM_INTERNAL_APPEAL = "columnStateExamInternalAppeal";
    public static final String COLUMN_EXAM = "columnExam";
    public static final String COLUMN_EXAM_APPEAL = "columnExamAppeal";
    public static final String COLUMN_TEST = "columnTest";
    public static final String COLUMN_TEST_APPEAL = "columnTestAppeal";
    public static final String COLUMN_INTERVIEW = "columnInterview";
    public static final String COLUMN_INTERVIEW_APPEAL = "columnInterviewAppeal";

    public static final String P_TITLE = "title";
    public static final String P_STATE_EXAM = "stateExamMark";
    public static final String P_STATE_EXAM_VALUE = "stateExamScaledMark";
    public static final String P_OLYMPIAD = "olympiadMark";
    public static final String P_FINAL_MARK = "finalMark";

    private String _stateExamMark;
    private String _stateExamScaledMark;
    private String _olympiadMark;
    private String _stateExamInternalMark;
    private String _stateExamInternalMarkAppeal;
    private String _examMark;
    private String _examMarkAppeal;
    private String _testMark;
    private String _testMarkAppeal;
    private String _interviewMark;
    private String _interviewMarkAppeal;
    private String _finalMark;
    private int _minMark;
    private int _maxMark;

    public DisciplineMarkWrapper(Discipline2RealizationWayRelation discipline, String stateExamMark, String stateExamScaledMark, String olympiadMark, String finalMark)
    {
        super(discipline);
        _stateExamMark = stateExamMark;
        _stateExamScaledMark = stateExamScaledMark;
        _olympiadMark = olympiadMark;
        _finalMark = finalMark;
        _minMark = discipline.getMinMark();
        _maxMark = discipline.getMaxMark();
    }

    // Getters & Setters

    public String getStateExamMark()
    {
        return _stateExamMark;
    }

    public void setStateExamMark(String stateExamMark)
    {
        _stateExamMark = stateExamMark;
    }

    public String getStateExamScaledMark()
    {
        return _stateExamScaledMark;
    }

    public void setStateExamScaledMark(String stateExamScaledMark)
    {
        _stateExamScaledMark = stateExamScaledMark;
    }

    public String getOlympiadMark()
    {
        return _olympiadMark;
    }

    public void setOlympiadMark(String olympiadMark)
    {
        _olympiadMark = olympiadMark;
    }

    public String getStateExamInternalMark()
    {
        return _stateExamInternalMark;
    }

    public void setStateExamInternalMark(String stateExamInternalMark)
    {
        _stateExamInternalMark = stateExamInternalMark;
    }

    public String getStateExamInternalMarkAppeal()
    {
        return _stateExamInternalMarkAppeal;
    }

    public void setStateExamInternalMarkAppeal(String stateExamInternalMarkAppeal)
    {
        _stateExamInternalMarkAppeal = stateExamInternalMarkAppeal;
    }

    public String getExamMark()
    {
        return _examMark;
    }

    public void setExamMark(String examMark)
    {
        _examMark = examMark;
    }

    public String getExamMarkAppeal()
    {
        return _examMarkAppeal;
    }

    public void setExamMarkAppeal(String examMarkAppeal)
    {
        _examMarkAppeal = examMarkAppeal;
    }

    public String getTestMark()
    {
        return _testMark;
    }

    public void setTestMark(String testMark)
    {
        _testMark = testMark;
    }

    public String getTestMarkAppeal()
    {
        return _testMarkAppeal;
    }

    public void setTestMarkAppeal(String testMarkAppeal)
    {
        _testMarkAppeal = testMarkAppeal;
    }

    public String getInterviewMark()
    {
        return _interviewMark;
    }

    public void setInterviewMark(String interviewMark)
    {
        _interviewMark = interviewMark;
    }

    public String getInterviewMarkAppeal()
    {
        return _interviewMarkAppeal;
    }

    public void setInterviewMarkAppeal(String interviewMarkAppeal)
    {
        _interviewMarkAppeal = interviewMarkAppeal;
    }

    public String getFinalMark()
    {
        return _finalMark;
    }

    public void setFinalMark(String finalMark)
    {
        _finalMark = finalMark;
    }

    public int getMinMark()
    {
        return _minMark;
    }

    public void setMinMark(int minMark)
    {
        _minMark = minMark;
    }

    public int getMaxMark()
    {
        return _maxMark;
    }

    public void setMaxMark(int maxMark)
    {
        _maxMark = maxMark;
    }
}
