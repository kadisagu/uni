/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e0.ListOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubController;
import ru.tandemservice.movestudent.entity.StudentListOrder;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Controller extends AbstractListOrderPubController<StudentListOrder, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new MultiValuesColumn("Направление подготовки", Model.EDU_ORGUNIT_EXTENDED, null, NewLineFormatter.NOBR_IN_LINES).setClickable(false).setOrderable(false));
    }
}