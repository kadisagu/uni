/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcEntrant.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 26.04.2012
 */
public interface IEcEntrantDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет (или обновляет) заявление абитуриенту с новым списком выбранных направлений подготовки (специальностей)
     *
     * @param selectedList           список выбранных направлений приема для сохранения
     * @param forDeleteList          список выбранных направлений приема, которые надо удалить из заявления
     * @param addForm                true, если это форма добавления
     * @param entrantRequest         заявление абитуриента
     * @param entrantRequestNumber   номер заявления
     * @param entrantDirectionNumber номер по направлению (специальности)
     * @param profileKnowledgeMap    данные выбранных профильных знаний по направлению приема (либо null, если таких данных нет)
     * @param profileMarkMap         данные оценок по профильным предметам (либо null, если таких данных нет)
     */
    void saveOrUpdateEntrantRequest(
            List<RequestedEnrollmentDirection> selectedList,
            List<RequestedEnrollmentDirection> forDeleteList,
            boolean addForm,
            EntrantRequest entrantRequest,
            Integer entrantRequestNumber,
            Integer entrantDirectionNumber,
            Map<Long, List<ProfileKnowledge>> profileKnowledgeMap,
            Map<Long, List<ProfileExaminationMark>> profileMarkMap);

    /**
     * Сохраняет оригиналы документы по направлению приему
     *
     * @param direction выбранное направление приема
     * @param save      true, если добавляем оригиналы
     */
    void updateOriginalDocuments(RequestedEnrollmentDirection direction, boolean save);

    /**
     * Удаляет ВНП
     *
     * @param directionId идентификатор ВНП
     */
    void deleteRequestedEnrollmentDirection(Long directionId);

    /**
     * Удаляет заявление абитуриента
     *
     * @param entrantRequestId идентификатор заявления абитуриента
     */
    void deleteEntrantRequest(Long entrantRequestId);

    /**
     * Нормализует приоритеты ВНП среди всех заявлений абитуриента в разрезе вида возмещения затрат
     *
     * @param entrant абитуриент
     */
    void doNormalizePriorityPerEntrant(Entrant entrant);
}
