package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantExamListGen;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.Collections;
import java.util.List;

/**
 * Экзаменационный лист
 */
public class EntrantExamList extends EntrantExamListGen
{
    public List<ExamPassDiscipline> getExamPassDisciplineList()
    {
        List<ExamPassDiscipline> list = UniDaoFacade.getCoreDao().getList(ExamPassDiscipline.class, ExamPassDiscipline.L_ENTRANT_EXAM_LIST, this);
        Collections.sort(list, new EntityComparator<>(new EntityOrder(new String[]{ExamPassDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE, Discipline2RealizationWayRelation.P_TITLE})));
        return list;
    }

    public static final String P_NUMBER = "number";

    public String getNumber()
    {
        return UniecDAOFacade.getEntrantExamListNumberGenerator().getExamListNumber(getEntrant());
    }
}