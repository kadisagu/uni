// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.uniec.ws.entrantrating;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Ежедневный рейтинг абитуриентов (с выделением групп «Общий прием» и «Целевой прием»)
 *
 * @author Vasily Zhukov
 * @since 24.03.2011
 */
@XmlRootElement
public class EntrantRatingTAEnvironmentNode
{
    /**
     * Название приемной кампании
     */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /**
     * Название года обучения приемной кампании
     */
    @XmlAttribute(required = true)
    public String educationYearTitle;

    /**
     * Дата приказа с
     */
    @XmlAttribute(required = true)
    public Date dateFrom;

    /**
     * Дата приказа по
     */
    @XmlAttribute(required = true)
    public Date dateTo;

    /**
     * Код вида возмещения затрат
     */
    @XmlAttribute(required = true)
    public String compensationTypeId;

    /**
     * Коды категорий обучаемых
     */
    @XmlAttribute
    @XmlList
    public List<String> studentCategoryIds;

    /**
     * Скрывать направления приема без заявлений. Только при построении отчета по нескольким направлениям приема.
     */
    @XmlAttribute
    public Boolean hideEmptyDirections;

    /**
     * Коды квалификаций. Только при построении отчета по нескольким направлениям приема.
     */
    @XmlAttribute
    @XmlList
    public List<String> qualificationIds;

    /**
     * Коды форм освоения. Только при построении отчета по нескольким направлениям приема.
     */
    @XmlAttribute
    @XmlList
    public List<String> developFormIds;

    /**
     * Коды условий освоения. Только при построении отчета по нескольким направлениям приема.
     */
    @XmlAttribute
    @XmlList
    public List<String> developConditionIds;

    /**
     * Идентификатор направления приема. Только при построении отчета по одному направлению приема.
     */
    @XmlAttribute
    public String enrollmentDirectionId;

    /**
     * Все виды целевого приема в указанной приемной кампании
     */
    public TargetAdmissionKindNode targetAdmissionKind = new TargetAdmissionKindNode();

    /**
     * Все виды конкурса
     */
    public CompetitionKindNode competitionKind = new CompetitionKindNode();

    /**
     * Все льготы
     */
    public BenefitNode benefit = new BenefitNode();

    /**
     * Все типы приказов о зачислении
     */
    public EnrollmentOrderTypeNode orderType = new EnrollmentOrderTypeNode();

    /**
     * Все состояния документов у абитуриентов из распределения
     */
    public DocumentStateNode documentState = new DocumentStateNode();

    /**
     * Все состояния зачисления у абитуриентов из распределения
     */
    public EnrollmentStateNode enrollmentState = new EnrollmentStateNode();

    /**
     * Все направления приема в указанной приемной кампании
     */
    public EnrollmentDirectionNode enrollmentDirection = new EnrollmentDirectionNode();

    /**
     * Простая строка таблицы
     */
    public static class Row implements Comparable<Row>
    {
        public Row()
        {
        }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /**
     * Все виды целевого приема в указанной приемной кампании
     */
    public static class TargetAdmissionKindNode
    {
        public static class TargetAdmissionKindRow extends Row
        {
            public TargetAdmissionKindRow()
            {
            }

            public TargetAdmissionKindRow(String shortTitle, String title, String id, String parent, int priority, boolean used)
            {
                super(shortTitle, title, id);
                this.parent = parent;
                this.priority = priority;
                this.used = used;
            }

            /**
             * Родительский вид целевого приема
             *
             * @see TargetAdmissionKindNode
             */
            @XmlAttribute
            public String parent;

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<TargetAdmissionKindRow> row = new ArrayList<TargetAdmissionKindRow>();
    }

    /**
     * Все виды конкурса
     */
    public static class CompetitionKindNode
    {
        public static class CompetitionKindRow extends Row
        {
            public CompetitionKindRow()
            {
            }

            public CompetitionKindRow(String shortTitle, String title, String id, boolean used)
            {
                super(shortTitle, title, id);
                this.used = used;
            }

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<CompetitionKindRow> row = new ArrayList<CompetitionKindRow>();
    }

    /**
     * Все льготы
     */
    public static class BenefitNode
    {
        public static class BenefitRow extends Row
        {
            public BenefitRow()
            {
            }

            public BenefitRow(String shortTitle, String title, String id, boolean used)
            {
                super(shortTitle, title, id);
                this.used = used;
            }

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<BenefitRow> row = new ArrayList<BenefitRow>();
    }

    public static class EnrollmentOrderTypeNode
    {
        public static class EnrollmentOrderTypeRow extends Row
        {
            public EnrollmentOrderTypeRow()
            {
            }

            public EnrollmentOrderTypeRow(String shortTitle, String title, String id, boolean used)
            {
                super(shortTitle, title, id);
                this.used = used;
            }

            /**
             * Используется ли в текущей приемной кампании
             * true - используется; false - не используется
             */
            @XmlAttribute(required = true)
            public boolean used;
        }

        public List<EnrollmentOrderTypeRow> row = new ArrayList<EnrollmentOrderTypeRow>();
    }

    public static class DistributionStateNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    public static class DocumentStateNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    public static class EnrollmentStateNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Все направления приема в указанной приемной кампании
     */
    public static class EnrollmentDirectionNode
    {
        /**
         * Направление приема в указанной приемной кампании
         */
        public static class DirectionRow
        {
            /**
             * Все дисциплины набора вступительных испытаний
             */
            public static class DisciplineNode
            {
                /**
                 * Дисциплина набора вступительного испытания
                 */
                public static class DisciplineRow
                {
                    public DisciplineRow()
                    {
                    }

                    public DisciplineRow(String title, String shortTitle, String id)
                    {
                        this.title = title;
                        this.shortTitle = shortTitle;
                        this.id = id;
                    }

                    /**
                     * Название дисциплины набора вступительного испытания
                     *
                     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
                     */
                    @XmlAttribute
                    public String title;

                    /**
                     * Сокращенное название дисциплины набора вступительного испытания
                     *
                     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
                     */
                    @XmlAttribute
                    public String shortTitle;

                    /**
                     * Идентификатор дисциплины набора вступительного испытания
                     *
                     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
                     */
                    @XmlAttribute(required = true)
                    public String id;
                }

                public List<DisciplineRow> row = new ArrayList<DisciplineRow>();
            }

            /**
             * Все строки отчета (выбранные направления приема) по общему приему
             */
            public static class GeneralAdmissionNode
            {
                public List<RequestedEnrollmentDirectionRow> row = new ArrayList<RequestedEnrollmentDirectionRow>();
            }

            /**
             * Все строки отчета (выбранные направления приема) по целевому приему
             */
            public static class TargetAdmissionNode
            {
                public List<RequestedEnrollmentDirectionRow> row = new ArrayList<RequestedEnrollmentDirectionRow>();
            }

            /**
             * Строка отчета (выбранное направление приема)
             */
            public static class RequestedEnrollmentDirectionRow
            {
                /**
                 * Регистрационный номер выбранного направления приема (специальности)
                 */
                @XmlAttribute(required = true)
                public Integer regNumber;

                /**
                 * ФИО абитуриента
                 */
                @XmlAttribute(required = true)
                public String fio;

                /**
                 * Вид целевого приема
                 *
                 * @see TargetAdmissionKindNode
                 */
                @XmlAttribute(required = true)
                public String targetAdmissionKind;

                /**
                 * Вид конкурса
                 *
                 * @see CompetitionKindNode
                 */
                @XmlAttribute(required = true)
                public String competitionKind;

                /**
                 * Льготы персоны
                 *
                 * @see BenefitNode
                 */
                @XmlAttribute
                @XmlList
                public List<String> benefitList;

                /**
                 * Состояние документов
                 *
                 * @see DocumentStateNode
                 */
                @XmlAttribute(required = true)
                public String documentState;

                /**
                 * Состояние зачисления
                 *
                 * @see EnrollmentStateNode
                 */
                @XmlAttribute
                public String enrollmentState;

                /**
                 * Идентификатор приказа о зачислении
                 */
                @XmlAttribute
                public String orderId;

                /**
                 * Номер приказа о зачислении
                 */
                @XmlAttribute
                public String orderNumber;

                /**
                 * Тип приказа о зачислении
                 *
                 * @see EnrollmentOrderTypeNode
                 */
                @XmlAttribute
                public String orderType;

                /**
                 * Вид затрат из условий зачисления, на который зачислен абитуриент по текущему выбранному направлению приема
                 */
                @XmlAttribute
                public String enrollmentCompensationType;

                /**
                 * Средний бал из основного законченного образовательного учреждения
                 */
                @XmlAttribute
                public String averageEduInstitutionMark;

                /**
                 * Общий стаж работы по профилю в месяцах по выбранному направлению приема
                 */
                @XmlAttribute
                public String profileWorkExperienceMonths;

                /**
                 * Сумма баллов (финальная оценка по выбранному направлению приема)
                 */
                @XmlAttribute(required = true)
                public String finalMark;

                /**
                 * Оценоки по дисциплинам вступительных испытаний
                 * Каждая оценка может быть:
                 * 1. x, если не должен сдавать
                 * 2. -, если должен сдавать, но еще не сдавал
                 * 3. число, если есть финальная оценка (если шкала перевода содержит шаг 0.5, то финальная оценка может быть дробной с двумя знаками после запятой)
                 *
                 * @see DisciplineNode
                 */
                @XmlAttribute
                @XmlList
                public List<String> marks = new ArrayList<String>();

                /**
                 * Идентификатор абитуриента
                 */
                @XmlAttribute(required = true)
                public String entrantId;
            }

            /**
             * Идентификатор этого направления приема
             */
            @XmlAttribute(required = true)
            public String id;

            /**
             * Все дисциплины набора вступительных испытаний
             */
            public DisciplineNode discipline = new DisciplineNode();

            /**
             * Все строки отчета (абитуриенты) по общему приему
             */
            public GeneralAdmissionNode generalAdmission = new GeneralAdmissionNode();

            /**
             * Все строки отчета (абитуриенты) по целевому приему
             */
            public TargetAdmissionNode targetAdmission = new TargetAdmissionNode();
        }

        public List<DirectionRow> row = new ArrayList<DirectionRow>();
    }
}
