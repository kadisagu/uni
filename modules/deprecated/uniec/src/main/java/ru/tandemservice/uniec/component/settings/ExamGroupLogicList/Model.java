/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.settings.ExamGroupLogicList;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.ExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final String EXAM_GROUP_LOGIC_CURRENT_PROPERTY = "current";
    public static final String EXAM_GROUP_LOGIC_DISABLED_PROPERTY = "disabled";

    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private DynamicListDataSource<ExamGroupLogic> _dataSource;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public DynamicListDataSource<ExamGroupLogic> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExamGroupLogic> dataSource)
    {
        _dataSource = dataSource;
    }
}
