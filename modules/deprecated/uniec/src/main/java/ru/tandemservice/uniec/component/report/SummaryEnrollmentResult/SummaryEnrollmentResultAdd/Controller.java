/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Vasily Zhukov
 * @since 29.07.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setCheckAllColumns(true);

        onCheckAllColumns(component);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
        activateInRoot(component, new PublisherActivator(getModel(component).getReport()));
    }

    public void onCheckAllColumns(IBusinessComponent component)
    {
        Model model = getModel(component);
        for (String column : model.getColumnList())
        {
            model.setColumn(column);
            model.setColumnVisible(model.isCheckAllColumns());
        }
    }
}