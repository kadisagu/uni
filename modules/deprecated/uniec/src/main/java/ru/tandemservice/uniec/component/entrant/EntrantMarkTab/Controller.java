/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.component.impl.ComponentRegion;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        model.clear();

        getDao().prepare(model);

        // создаем DataSource для выбранных направлений приема в зависимости от правил формирования ведомостей
        if (model.isFormingForEntrant())
        {
            prepareRequestedEnrollmentDirectionDataSource(component);
        } else
        {
            for (EntrantRequest entrantRequest : model.getEntrantRequestList())
            {
                model.setEntrantRequest(entrantRequest);
                prepareRequestedEnrollmentDirectionDataSource(component);
            }
        }

        // создаем DataSource для выбранных направлей приема с видом конкурса "собеседование" в зависимости от правил формирования ведомостей
        if (model.isFormingForEntrant())
        {
            prepareRequestedEnrollmentDirectionInterviewDataSource(component);
        } else
        {
            for (EntrantRequest entrantRequest : model.getEntrantRequestList())
            {
                model.setEntrantRequest(entrantRequest);
                prepareRequestedEnrollmentDirectionInterviewDataSource(component);
            }
        }

        // создаем DataSource для вступительных испытаний в зависимости от правил формирования ведомостей
        if (model.isFormingForEntrant())
        {
            prepareMarkResultDataSource(component);
        } else
        {
            for (EntrantRequest entrantRequest : model.getEntrantRequestList())
            {
                model.setEntrantRequest(entrantRequest);
                prepareMarkResultDataSource(component);
            }
        }
    }

    private void prepareRequestedEnrollmentDirectionDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDirectionDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareRequestedEnrollmentDirectionDataSource(model);
        });
        dataSource.addColumn(new PublisherLinkColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", RequestedEnrollmentDirection.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", RequestedEnrollmentDirection.state().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Сумма баллов", Model.P_MARK_SUM).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во испытаний", Model.P_EXAM_COUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Сдано", Model.P_PASS_COUNT).setClickable(false).setOrderable(false));

        model.setDirectionDataSource(dataSource);
    }

    @SuppressWarnings("unchecked")
    private void prepareRequestedEnrollmentDirectionInterviewDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDirectionInterviewDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareRequestedEnrollmentDirectionInterviewDataSource(model);
        });
        dataSource.addColumn(new PublisherLinkColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.P_SHORT_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        BlockColumn blockColumn = new BlockColumn(Model.COLUMN_PASS_OR_NOT, "Пройдено/не пройдено");
        blockColumn.setValueMap(model.getInterviewBlockColumnMap());
        dataSource.addColumn(blockColumn.setClickable(false).setOrderable(false));

        model.setDirectionInterviewDataSource(dataSource);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void prepareMarkResultDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getMarkDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<DisciplineMarkWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareMarkResultDataSource(model);
        });
        AbstractColumn column = new SimpleColumn("", new String[]{DisciplineMarkWrapper.P_TITLE}).setClickable(false).setOrderable(false);
        column.setStyle("width:30%");
        dataSource.addColumn(column);
        model.setMarkDataSource(dataSource);

        dataSource.addColumn(new SimpleColumn("ЕГЭ", DisciplineMarkWrapper.P_STATE_EXAM).setClickable(false).setOrderable(false));
        AbstractColumn simpleColumn = new SimpleColumn("ЕГЭ по\nшкале вуза", DisciplineMarkWrapper.P_STATE_EXAM_VALUE).setClickable(false).setOrderable(false);
        simpleColumn.setHeaderStyle("white-space:nowrap;");
        dataSource.addColumn(simpleColumn);
        dataSource.addColumn(new SimpleColumn("Олимпиада", DisciplineMarkWrapper.P_OLYMPIAD).setClickable(false).setOrderable(false));

        // ЕГЭ (вуз)
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_STATE_EXAM_INTERNAL, "ЕГЭ (вуз)").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_STATE_EXAM_INTERNAL_APPEAL, "Апелляция").setClickable(false).setOrderable(false));

        // Экзамен
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_EXAM, "Экзамен").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_EXAM_APPEAL, "Апелляция").setClickable(false).setOrderable(false));

        // Тестирование
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_TEST, "Тестирование").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_TEST_APPEAL, "Апелляция").setClickable(false).setOrderable(false));

        // Собеседование
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_INTERVIEW, "Собеседование").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(DisciplineMarkWrapper.COLUMN_INTERVIEW_APPEAL, "Апелляция").setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Итог", DisciplineMarkWrapper.P_FINAL_MARK).setClickable(false).setOrderable(false));
    }

    public void onClickApply(IBusinessComponent component)
    {
        final Model model = getModel(component);

        getDao().updateEntrantRequestData(model, (Long) component.getListenerParameter());

        //TODO: hack #54866
        ((BusinessComponent) ((ComponentRegion) component.getParentRegion()).getOwner()).refresh();

        // все прошло отлично, надо загрузить в модель новое состояние базы данных
        onRefreshComponent(component);
    }
}
