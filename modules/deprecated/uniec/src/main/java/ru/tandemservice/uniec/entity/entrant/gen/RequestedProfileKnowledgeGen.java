package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Профильное знание выбранного направления приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RequestedProfileKnowledgeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge";
    public static final String ENTITY_NAME = "requestedProfileKnowledge";
    public static final int VERSION_HASH = -552605221;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";
    public static final String L_PROFILE_KNOWLEDGE = "profileKnowledge";

    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление приема
    private ProfileKnowledge _profileKnowledge;     // Профильное знание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление приема. Свойство не может быть null.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    /**
     * @return Профильное знание. Свойство не может быть null.
     */
    @NotNull
    public ProfileKnowledge getProfileKnowledge()
    {
        return _profileKnowledge;
    }

    /**
     * @param profileKnowledge Профильное знание. Свойство не может быть null.
     */
    public void setProfileKnowledge(ProfileKnowledge profileKnowledge)
    {
        dirty(_profileKnowledge, profileKnowledge);
        _profileKnowledge = profileKnowledge;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RequestedProfileKnowledgeGen)
        {
            setRequestedEnrollmentDirection(((RequestedProfileKnowledge)another).getRequestedEnrollmentDirection());
            setProfileKnowledge(((RequestedProfileKnowledge)another).getProfileKnowledge());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RequestedProfileKnowledgeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RequestedProfileKnowledge.class;
        }

        public T newInstance()
        {
            return (T) new RequestedProfileKnowledge();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
                case "profileKnowledge":
                    return obj.getProfileKnowledge();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "profileKnowledge":
                    obj.setProfileKnowledge((ProfileKnowledge) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
                case "profileKnowledge":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
                case "profileKnowledge":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "profileKnowledge":
                    return ProfileKnowledge.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RequestedProfileKnowledge> _dslPath = new Path<RequestedProfileKnowledge>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RequestedProfileKnowledge");
    }
            

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    /**
     * @return Профильное знание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge#getProfileKnowledge()
     */
    public static ProfileKnowledge.Path<ProfileKnowledge> profileKnowledge()
    {
        return _dslPath.profileKnowledge();
    }

    public static class Path<E extends RequestedProfileKnowledge> extends EntityPath<E>
    {
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;
        private ProfileKnowledge.Path<ProfileKnowledge> _profileKnowledge;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

    /**
     * @return Профильное знание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge#getProfileKnowledge()
     */
        public ProfileKnowledge.Path<ProfileKnowledge> profileKnowledge()
        {
            if(_profileKnowledge == null )
                _profileKnowledge = new ProfileKnowledge.Path<ProfileKnowledge>(L_PROFILE_KNOWLEDGE, this);
            return _profileKnowledge;
        }

        public Class getEntityClass()
        {
            return RequestedProfileKnowledge.class;
        }

        public String getEntityName()
        {
            return "requestedProfileKnowledge";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
