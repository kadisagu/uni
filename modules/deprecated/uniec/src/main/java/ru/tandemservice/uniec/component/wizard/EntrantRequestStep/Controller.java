/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.EntrantRequestStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author vip_delete
 * @since 16.06.2009
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Controller
{
    // Wizard Actions

    @Override
    protected void activateNextRegion(IBusinessComponent component)
    {
        deactivate(component);

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.ENTRANT_DOCUMENT_STEP, new ParametersMap()
                .add("entrantRequestId", getModel(component).getEntrantRequest().getId())
                .add("entrantMasterPermKey", ((Model) getModel(component)).getEntrantMasterPermKey())
        ));
    }

    public void onClickStop(IBusinessComponent component)
    {
        IDAO dao = (IDAO) getDao();
        Model model = (Model) getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        validateFields(component);

        // если можно добавлять только одно выбранное направление подготовки для заявления и форма добавления
        // то должны произойти те же действия, что и по кнопке "Выбрать" в случае, когда можно добавлять несколько
        if (model.getEntrantRequest().getId() == null && model.isOneDirectionPerRequest())
            dao.prepareSelectedEnrollmentDirectionList(model, errors);

        if (errors.hasErrors()) return;

        dao.update(model);

        deactivate(component);

        activateInRoot(component, new PublisherActivator(model.getEntrantRequest().getEntrant(), new ParametersMap()
                .add("selectedTab", "entrantTab")
                .add("entrantMasterPermKey", model.getEntrantMasterPermKey())
                .add("selectedDataTab", "entranceDisciplineTab")
        ));
    }
}
