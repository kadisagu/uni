/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.AccessCourses.AccessCoursesAdd;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;

/**
 * @author ekachanova
 */
class AccessCoursesReportBuilder
{
    private Model _model;
    private Session _session;

    AccessCoursesReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings({"unchecked"})
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ACCESS_COURSES);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getTitle());
        injectModifier.modify(document);

        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);
        if (_model.isQualificationActive())
            enrollmentDirectionList = filterByQualification(enrollmentDirectionList);

        Set<Long>[] sets = new Set[12];
        for (int i = 0; i < 12; i++)
            sets[i] = new HashSet<>();

        {   //по ходу приема документов
            MQBuilder builder = getBaseDirectionBuilder(enrollmentDirectionList);
            builder.addSelect("r", new String[]{
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                    RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE});

            process(builder, sets, 0, 1);
            builder.addDomain("accessCourse", EntrantAccessCourse.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("accessCourse", EntrantAccessCourse.L_ENTRANT, "r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
            builder.setNeedDistinct(true);
            process(builder, sets, 2, 3);
        }
        {   //по результатам сдачи вступительных испытаний
            MQBuilder builder = getBaseDirectionBuilder(enrollmentDirectionList);
            builder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
            builder.addSelect("r", new String[]{
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id",
                    RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE});

            process(builder, sets, 4, 5);
            builder.addDomain("accessCourse", EntrantAccessCourse.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("accessCourse", EntrantAccessCourse.L_ENTRANT, "r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
            builder.setNeedDistinct(true);
            process(builder, sets, 6, 7);
        }
        {   //по результатам зачисления
            MQBuilder builder = getBasePreliminaryBuilder(enrollmentDirectionList);
            builder.addSelect("p", new String[]{"id", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE});

            process(builder, sets, 8, 9);
            builder.addDomain("accessCourse", EntrantAccessCourse.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("accessCourse", EntrantAccessCourse.L_ENTRANT, "r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
            builder.setNeedDistinct(true);
            process(builder, sets, 10, 11);
        }

        String[][] data = new String[2][13];
        data[0] = new String[13];
        data[1] = new String[13];
        data[0][0] = _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0];
        List<Integer> textCellIndexes = Arrays.asList(0, 1, 4, 5, 8, 9);
        for (int i = 0; i < 12; i++)
        {
            data[0][i + 1] = String.valueOf(sets[i].size());
            if (textCellIndexes.contains(i))
                data[1][i + 1] = "% от всего приема";
            else
                data[1][i + 1] = sets[i - 2].size() > 0 ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(((double) sets[i].size() * 100) / sets[i - 2].size()) : "";
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", data);
        tableModifier.modify(document);

        RtfTable table = (RtfTable) document.getElementList().get(4);
        table.getRowList().get(3).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
        table.getRowList().get(4).getCellList().get(0).setMergeType(MergeType.VERTICAL_MERGED_NEXT);

        for (int j = 1; j < table.getRowList().get(3).getCellList().size(); j++)
        {
            RtfBorder oldBottom = table.getRowList().get(3).getCellList().get(j).getCellBorder().getBottom();
            RtfBorder newBottom = RtfBorder.getInstance(10, oldBottom.getColorIndex(), oldBottom.getStyle());
            table.getRowList().get(3).getCellList().get(j).getCellBorder().setBottom(newBottom);

            RtfBorder oldTop = table.getRowList().get(4).getCellList().get(j).getCellBorder().getTop();
            RtfBorder newTop = RtfBorder.getInstance(10, oldTop.getColorIndex(), oldTop.getStyle());
            table.getRowList().get(4).getCellList().get(j).getCellBorder().setTop(newTop);
        }
        for (int j = 1; j < table.getRowList().get(4).getCellList().size(); j += 2)
        {
            table.getRowList().get(4).getCellList().get(j++).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
            table.getRowList().get(4).getCellList().get(j++).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
        }

        return RtfUtil.toByteArray(document);
    }

    private void process(MQBuilder builder, Set<Long>[] sets, int budgetIndex, int contractIndex)
    {
        for (Object[] obj : builder.<Object[]>getResultList(_session))
        {
            Long id = (Long) obj[0];
            String compenstypeCode = (String) obj[1];
            if (compenstypeCode.equals(UniDefines.COMPENSATION_TYPE_BUDGET))
                sets[budgetIndex].add(id);
            else
                sets[contractIndex].add(id);
        }
    }

    private List<EnrollmentDirection> filterByQualification(List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EnrollmentDirection> result = new ArrayList<>();
        for (EnrollmentDirection direction : enrollmentDirectionList)
            if (_model.getQualificationList().contains(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
                result.add(direction);
        return result;
    }

    private MQBuilder getBaseDirectionBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        return builder;
    }

    private MQBuilder getBasePreliminaryBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));
        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        return builder;
    }
}
