/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantRequestBatchAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;
import ru.tandemservice.uniec.entity.settings.UseExternalOrgUnitForTA;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;

import java.util.*;

/**
 * @author vip_delete
 * @since 07.06.2009
 */
@SuppressWarnings({"unchecked"})
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setAddForm(model.getEntrantRequest().getId() == null);
        model.setEditForm(!model.isAddForm());

        if (model.isEditForm())
        {
            model.setEntrantRequest(getNotNull(EntrantRequest.class, model.getEntrantRequest().getId()));
            List<RequestedEnrollmentDirection> list = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest(), RequestedEnrollmentDirection.P_PRIORITY);
            model.setSelectedRequestedEnrollmentDirectionList(list);
            model.setForDelete(new ArrayList<RequestedEnrollmentDirection>());
            model.setExistedRequestedEnrollmentDirectionIds(new HashSet<Long>());
            for (RequestedEnrollmentDirection item : list)
                model.getExistedRequestedEnrollmentDirectionIds().add(item.getId());
            model.setRegNumber(model.getEntrantRequest().getRegNumber());
        } else
        {
            model.getEntrantRequest().setRegDate(new Date());
            model.getEntrantRequest().setEntrant(getNotNull(Entrant.class, model.getEntrantRequest().getEntrant().getId()));
            model.setSelectedRequestedEnrollmentDirectionList(new ArrayList<RequestedEnrollmentDirection>());
            model.setForDelete(new ArrayList<RequestedEnrollmentDirection>());
            model.setExistedRequestedEnrollmentDirectionIds(new HashSet<Long>());
        }

        model.setStateExamRestriction(model.getEnrollmentCampaign().isStateExamRestriction());

        //final Session session = getSession();
        model.setFormativeOrgUnitModel(new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, "f");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.setProjection(Projections.distinct(Projections.property("f." + OrgUnit.P_ID)));

                List<Long> ids = c.list();

                if (ids.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(OrgUnit.class);
                listCriteria.add(Restrictions.in(OrgUnit.P_ID, ids));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(OrgUnit.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(OrgUnit.P_TITLE));

                return new ListResult<>(listCriteria.list(), ids.size());
            }
        });
        model.setTerritorialOrgUnitModel(new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "t");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (model.getFormativeOrgUnit() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.setProjection(Projections.distinct(Projections.property("t." + OrgUnit.P_ID)));

                List<Long> ids = c.list();

                if (ids.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(OrgUnit.class);
                listCriteria.add(Restrictions.in(OrgUnit.P_ID, ids));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(OrgUnit.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(OrgUnit.P_TERRITORIAL_FULL_TITLE));

                return new ListResult<>(listCriteria.list(), ids.size());
            }
        });
        model.setEducationLevelsHighSchoolModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "h");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (model.getFormativeOrgUnit() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                if (model.getTerritorialOrgUnit() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                c.setProjection(Projections.distinct(Projections.property("h." + EducationLevelsHighSchool.P_ID)));

                List<Long> ids = c.list();

                if (ids.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(EducationLevelsHighSchool.class);
                listCriteria.add(Restrictions.in(EducationLevelsHighSchool.P_ID, ids));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(EducationLevelsHighSchool.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(EducationLevelsHighSchool.P_TITLE));

                return new ListResult<>(listCriteria.list(), ids.size());
            }
        });
        model.setDevelopFormModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_FORM, "f");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (model.getFormativeOrgUnit() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                if (model.getTerritorialOrgUnit() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                if (model.getEducationLevelsHighSchool() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));

                c.setProjection(Projections.distinct(Projections.property("f." + DevelopForm.P_ID)));

                List<Long> developFormIds = c.list();

                if (developFormIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopForm.class);
                listCriteria.add(Restrictions.in(DevelopForm.P_ID, developFormIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopForm.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopForm.P_CODE));

                return new ListResult<>(listCriteria.list(), developFormIds.size());
            }
        });

        model.setUseExternalOrgUnitForTA(get(UseExternalOrgUnitForTA.class, UseExternalOrgUnitForTA.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        model.setExternalOrgUnitModel(new LazySimpleSelectModel<>(ExternalOrgUnit.class, ExternalOrgUnit.P_TITLE_WITH_LEGAL_FORM));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));
        model.setCompetitionKindModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EnrollmentDirection> checkedEnrollmentDirections = getCheckedEnrollmentDirections(model);
                if (checkedEnrollmentDirections.isEmpty())
                {
                    return ListResult.getEmpty();
                }
                Criteria criteria = getSession().createCriteria(Qualification2CompetitionKindRelation.class);
                criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_ENROLLMENT_CAMPAIGN, model.getEntrantRequest().getEntrant().getEnrollmentCampaign()));
                criteria.add(Restrictions.in(Qualification2CompetitionKindRelation.L_QUALIFICATION, CommonBaseEntityUtil.getPropertiesSet(checkedEnrollmentDirections, EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification())));
                criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_STUDENT_CATEGORY, model.getStudentCategory()));
                criteria.setProjection(Projections.distinct(Projections.property(Qualification2CompetitionKindRelation.L_COMPETITION_KIND)));
                List<CompetitionKind> list = criteria.list();

                return new ListResult<>(list, list.size());
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<EnrollmentDirection> list = new ArrayList<>();
        if (model.getFormativeOrgUnit() != null || model.getTerritorialOrgUnit() != null || model.getEducationLevelsHighSchool() != null)
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
            if (model.getFormativeOrgUnit() != null)
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
            if (model.getTerritorialOrgUnit() != null)
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
            if (model.getEducationLevelsHighSchool() != null)
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
            if (model.getDevelopForm() != null)
                builder.add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
            list = builder.getResultList(getSession());

            // определяем какие чекбоксы надо скрыть, а какие оставить видимыми
            List<MultiKey> canBeSelected = EntrantRequestAddEditUtil.getAddingEnrollmentDirections(getSession(), list, null, model.getSelectedRequestedEnrollmentDirectionList(), model.getEnrollmentCampaign().isDirectionPerCompTypeDiff(), model.isStateExamRestriction(), model.getEntrantRequest().getEntrant(), model.getForDelete());
            model.getCanBeSelectedDirectionIds().clear();
            for (MultiKey key : canBeSelected)
                model.getCanBeSelectedDirectionIds().add(((EnrollmentDirection) key.getKey(0)).getId());

            BlockColumn<Boolean> column = (BlockColumn) model.getPossibleDirectionDataSource().getColumn("checkbox");
            Map<Long, Boolean> valueMap = new HashMap<>();
            for (EnrollmentDirection direction : list)
                valueMap.put(direction.getId(), false);
            column.setValueMap(valueMap);
        }

        model.getPossibleDirectionDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getPossibleDirectionDataSource(), list);
    }

    @Override
    public void prepareSelectedEnrollmentDirectionList(Model model, ErrorCollector errorCollector)
    {
        EntrantRequest entrantRequest = model.getEntrantRequest();

        // выходим, если есть ошибка
        List<MultiKey> enrollmentDirectionList = getAddingEnrollmentDirection(model, errorCollector);
        if (errorCollector.hasErrors()) return;

        // вычисляем минимальный id (у вновь добавленных будут отрицательные id)
        long id = 0L;
        List<RequestedEnrollmentDirection> alreadySelectedDirections = model.getSelectedRequestedEnrollmentDirectionList();
        for (RequestedEnrollmentDirection item : alreadySelectedDirections)
        {
            id = Math.min(id, item.getId());
        }
        id--;

        List<RequestedEnrollmentDirection> newDirections = new ArrayList<>();
        for (MultiKey key : enrollmentDirectionList)
        {
            // заполняем объект значениями по умолчанию
            final RequestedEnrollmentDirection newDirection = new RequestedEnrollmentDirection();
            newDirection.setEnrollmentDirection((EnrollmentDirection) key.getKey(0));
            newDirection.setCompensationType(model.getCompensationType());
            newDirection.setStudentCategory(model.getStudentCategory());
            newDirection.setCompetitionKind(model.getCompetitionKind());
            newDirection.setTargetAdmission(model.isTargetAdmission());
            newDirection.setExternalOrgUnit(model.getExternalOrgUnit());

            newDirection.setEnrollmentDirection((EnrollmentDirection) key.getKey(0));
            newDirection.setCompensationType((CompensationType) key.getKey(1));
            newDirection.setEntrantRequest(entrantRequest);
            newDirection.setRegDate(new Date());

            // этот id нужен, чтобы уметь отображать объект в SearchList
            newDirection.setId(id--);

            newDirections.add(newDirection);
        }

        // добавляем в уже выбранные
        List<RequestedEnrollmentDirection> mergedDirections = new ArrayList<>(alreadySelectedDirections);
        EntrantRequestAddEditUtil.mergeSelectedDirections(mergedDirections, newDirections);

        // проверяем, можно ли добавлять такие направления
        if (!EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), mergedDirections, model.getForDelete(), model.getEntrantRequest()))
            return;

        // если проверка успешная, то обновляем список выбранных
        model.setSelectedRequestedEnrollmentDirectionList(mergedDirections);

        // сбрасываем выбранные на форме параметры
        reset(model);
    }

    @Override
    public void update(Model model)
    {
        EcEntrantManager.instance().dao().saveOrUpdateEntrantRequest(
                model.getSelectedRequestedEnrollmentDirectionList(),
                model.getForDelete(),
                model.isAddForm(),
                model.getEntrantRequest(),
                model.getRegNumber(),
                null,
                null,
                null
        );
    }

    @Override
    public void reset(Model model)
    {
        model.setFormativeOrgUnit(null);
        model.setTerritorialOrgUnit(null);
        model.setEducationLevelsHighSchool(null);
        model.setDevelopForm(null);
        model.setCompensationType(null);
        model.setCompetitionKind(null);
        model.setStudentCategory(null);
        model.setTargetAdmission(false);

    }

    private List<MultiKey> getAddingEnrollmentDirection(Model model, ErrorCollector errorCollector)
    {
        Criteria criteria = getSession().createCriteria(Qualification2CompetitionKindRelation.class);
        criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_ENROLLMENT_CAMPAIGN, model.getEntrantRequest().getEntrant().getEnrollmentCampaign()));
        criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_STUDENT_CATEGORY, model.getStudentCategory()));
        criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_COMPETITION_KIND, model.getCompetitionKind()));
        criteria.setProjection(Projections.distinct(Projections.property(Qualification2CompetitionKindRelation.L_QUALIFICATION)));
        List<Qualifications> possibleQualifications = criteria.list();

        // получаем список направлений приема, которые надо добавить в заявление.
        List<EnrollmentDirection> directionForAddingList = new ArrayList<>();
        for (Object object : ((BlockColumn) model.getPossibleDirectionDataSource().getColumn("checkbox")).getValueMap().entrySet())
        {
            Long id = (Long) ((Map.Entry) object).getKey();
            Boolean checked = (Boolean) ((Map.Entry) object).getValue();
            if (checked)
            {
                EnrollmentDirection enrollmentDirection = getNotNull(EnrollmentDirection.class, id);
                if (!possibleQualifications.contains(enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
                {   //todo
                    errorCollector.add("Нельзя выбрать вид конкурса \"" + model.getCompetitionKind().getTitle() + "\" для категории поступающего \""
                            + model.getStudentCategory().getTitle() + "\" и направления приема с квалификацией \"" + enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getTitle() + "\".");
                    return null;
                }
                directionForAddingList.add(enrollmentDirection);
            }
        }

        if (directionForAddingList.isEmpty())
        {
            errorCollector.add("Ничего не выбрано среди возможных направлений приема.");
            return null;
        }

        // на основе выбранных направлений приема определяем какие из них и по каким видам затрат можно реально добавить в заявление
        List<MultiKey> selectableDirections = EntrantRequestAddEditUtil.getAddingEnrollmentDirections(getSession(), directionForAddingList, model.getCompensationType(), model.getSelectedRequestedEnrollmentDirectionList(), model.getEnrollmentCampaign().isDirectionPerCompTypeDiff(), model.isStateExamRestriction(), model.getEntrantRequest().getEntrant(), model.getForDelete());
        if (selectableDirections.isEmpty())
        {
            if (model.isStateExamRestriction())
                errorCollector.add("Нельзя добавить направление подготовки (специальность) абитуриенту, возможно оно уже выбрано или не покрывается свидетельством ЕГЭ.");
            else
                errorCollector.add("Нельзя добавить направление подготовки (специальность) абитуриенту, возможно оно уже выбрано.");
        }
        return selectableDirections;
    }

    private List<EnrollmentDirection> getCheckedEnrollmentDirections(Model model)
    {
        List<EnrollmentDirection> checkedDirectionList = new ArrayList<>();
        for (Object object : ((BlockColumn) model.getPossibleDirectionDataSource().getColumn("checkbox")).getValueMap().entrySet())
        {
            Long id = (Long) ((Map.Entry) object).getKey();
            Boolean checked = (Boolean) ((Map.Entry) object).getValue();
            if (checked)
            {
                checkedDirectionList.add(getNotNull(EnrollmentDirection.class, id));
            }
        }
        return checkedDirectionList;
    }
}
