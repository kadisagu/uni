package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.egeData.EgeDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.egeData.EgeDataParam;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantData.EntrantDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantData.EntrantDataParam;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataParam;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.passDisciplineData.PassDisciplineDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.passDisciplineData.PassDisciplineDataParam;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.preliminaryData.PreliminaryDataBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.preliminaryData.PreliminaryDataParam;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.print.entrant.EntrantPrintBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.print.preenroll.PreEnrollPrintBlock;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.print.request.EntrantRequestPrintBlock;
import ru.tandemservice.uniec.dao.UniecDAOFacade;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EcReportPersonAddUI extends UIPresenter implements IReportDQLModifierOwner
{
    public static final String ENTRANT_SCHEET = "entrantScheet";

    // tab flag
    private boolean _entrantScheet;

    private String _selectedTab;
    private EntrantDataParam _entrantData = new EntrantDataParam();
    private EgeDataParam _egeData = new EgeDataParam();
    private PassDisciplineDataParam _passDisciplineData = new PassDisciplineDataParam();
    private EntrantRequestDataParam _entrantRequestData = new EntrantRequestDataParam();
    private PreliminaryDataParam _preliminaryData = new PreliminaryDataParam();

    // double row data
    private boolean _doubleEntrantRowData;

    // print blocks
    private IReportPrintBlock _entrantPrintBlock = new EntrantPrintBlock();
    private IReportPrintBlock _entrantRequestPrintBlock = new EntrantRequestPrintBlock();
    private IReportPrintBlock _preEnrollPrintBlock = new PreEnrollPrintBlock();

    @Override
    public void onComponentRefresh()
    {
        _entrantScheet = ((ReportPersonAddUI) _uiSupport.getParentUI()).isScheetVisible(ENTRANT_SCHEET);

        _doubleEntrantRowData = true;

        // выбираем по умолчанию абитуриентов из последней приемной кампании
        _entrantData.getEnrollmentCampaign().setActive(true);

        if(_entrantData.getEnrollmentCampaign().isActive() && _entrantData.getEnrollmentCampaign().getData() == null)
        {
            _entrantData.getEnrollmentCampaign().setData(UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign());
        }

        // является абитуриентом
        _entrantData.getEntrant().setActive(true);

        if(_entrantData.getEntrant().isActive() && _entrantData.getEntrant().getData() == null)
        {
            _entrantData.getEntrant().setData(new IdentifiableWrapper(EntrantDataBlock.ENTRANT_YES, "да"));
        }

        // выбираем по умолчанию неархивных абитуриентов
        _entrantData.getEntrantArchival().setActive(true);

        if(_entrantData.getEntrantArchival().isActive() && _entrantData.getEntrantArchival().getData() == null)
        {
            _entrantData.getEntrantArchival().setData(new IdentifiableWrapper(EntrantDataBlock.ENTRANT_ARCHIVAL_NOT, "нет"));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (_entrantScheet)
        {
            EntrantDataBlock.onBeforeDataSourceFetch(dataSource, _entrantData);
            EgeDataBlock.onBeforeDataSourceFetch(dataSource, _egeData);
            PassDisciplineDataBlock.onBeforeDataSourceFetch(dataSource, _entrantData.getEnrollmentCampaign());
            EntrantRequestDataBlock.onBeforeDataSourceFetch(dataSource, _entrantRequestData);
            PreliminaryDataBlock.onBeforeDataSourceFetch(dataSource, _preliminaryData);
        }
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_entrantScheet)
        {
            printInfo.addSheet(ENTRANT_SCHEET);

            printInfo.setDoubleRowData(ENTRANT_SCHEET, _doubleEntrantRowData);

            // фильтры модифицируют запросы
            _entrantData.modify(dql, printInfo);
            _egeData.modify(dql, printInfo);
            _passDisciplineData.modify(dql, printInfo);
            _entrantRequestData.modify(dql, printInfo);
            _preliminaryData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
            _entrantPrintBlock.modify(dql, printInfo);
            _entrantRequestPrintBlock.modify(dql, printInfo);
            _preEnrollPrintBlock.modify(dql, printInfo);
        }
    }

    // Getters

    public EntrantDataParam getEntrantData()
    {
        return _entrantData;
    }

    public EgeDataParam getEgeData()
    {
        return _egeData;
    }

    public PassDisciplineDataParam getPassDisciplineData()
    {
        return _passDisciplineData;
    }

    public EntrantRequestDataParam getEntrantRequestData()
    {
        return _entrantRequestData;
    }

    public PreliminaryDataParam getPreliminaryData()
    {
        return _preliminaryData;
    }

    public IReportPrintBlock getEntrantPrintBlock()
    {
        return _entrantPrintBlock;
    }

    public IReportPrintBlock getEntrantRequestPrintBlock()
    {
        return _entrantRequestPrintBlock;
    }

    public IReportPrintBlock getPreEnrollPrintBlock()
    {
        return _preEnrollPrintBlock;
    }

    // Getters & Setters

    public boolean isEntrantScheet()
    {
        return _entrantScheet;
    }

    public void setEntrantScheet(boolean entrantScheet)
    {
        _entrantScheet = entrantScheet;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public boolean isDoubleEntrantRowData()
    {
        return _doubleEntrantRowData;
    }

    public void setDoubleEntrantRowData(boolean doubleEntrantRowData)
    {
        _doubleEntrantRowData = doubleEntrantRowData;
    }

    public void onChangeHasIndividualProgressActive(){
        if(_entrantData.getEntrantHasIndividualProgress().isActive() && _entrantData.getEntrantIndividualProgress().isActive())
            _entrantData.getEntrantIndividualProgress().setActive(false);
    }

    public void onChangeIndividualProgressActive(){
        if(_entrantData.getEntrantIndividualProgress().isActive() && _entrantData.getEntrantHasIndividualProgress().isActive())
            _entrantData.getEntrantHasIndividualProgress().setActive(false);
    }

    public void onChangeEntrantCustomStateExist()
    {
        Boolean exist = TwinComboDataSourceHandler.getSelectedValue(_entrantData.getEntrantCustomStateExist().getDataIfActive());
        _entrantData.getEntrantCustomStateList().setActive(_entrantData.getEntrantCustomStateExist().isActive() && exist != null);
    }
}
