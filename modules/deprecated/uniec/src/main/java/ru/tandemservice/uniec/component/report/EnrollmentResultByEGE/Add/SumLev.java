/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Add;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableInt;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.util.EntrantDataUtil;

/**
 * @author Vasily Zhukov
 * @since 19.08.2011
 */
class SumLev implements ISumLev
{
    private EntrantDataUtil _dataUtil;
    private String _title;
    private Long _id;
    private int _level;

    // данные для колонок "средние баллы по егэ"
    private MutableDouble _commonSum = new MutableDouble();
    private MutableInt _commonSumCount = new MutableInt();
    private MutableDouble _benefitSum = new MutableDouble();
    private MutableInt _benefitSumCount = new MutableInt();
    private MutableDouble _olympiadSum = new MutableDouble();
    private MutableInt _olympiadSumCount = new MutableInt();
    private MutableDouble _targetSum = new MutableDouble();
    private MutableInt _targetSumCount = new MutableInt();

    // данные для колонок "зачислено"
    private int _count;
    private int _benefitCount;
    private int _olympiadCount;
    private int _targetCount;

    private Set<Long> usedSumIds = new HashSet<Long>();

    SumLev(EntrantDataUtil dataUtil, String title, Long id, int level)
    {
        _dataUtil = dataUtil;
        _title = title;
        _id = id;
        _level = level;
    }

    void add(PreliminaryEnrollmentStudent preStudent)
    {
        Long key = preStudent.getId();

        if (usedSumIds.add(key))
        {
            _count++;

            MutableDouble sum = null;
            MutableInt count = null;

            if (preStudent.isTargetAdmission())
            {
                _targetCount++;
                sum = _targetSum;
                count = _targetSumCount;
            } else if (preStudent.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
            {
                _benefitCount++;
                sum = _benefitSum;
                count = _benefitSumCount;
            } else if (preStudent.getRequestedEnrollmentDirection().getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
            {
                boolean hasOlympiadMark = false;
                for (ChosenEntranceDiscipline chosen : _dataUtil.getChosenEntranceDisciplineSet(preStudent.getRequestedEnrollmentDirection()))
                    hasOlympiadMark = hasOlympiadMark || (chosen.getFinalMarkSource() != null && chosen.getFinalMarkSource().equals(UniecDefines.FINAL_MARK_OLYMPIAD));

                if (hasOlympiadMark)
                {
                    _olympiadCount++;
                    sum = _olympiadSum;
                    count = _olympiadSumCount;
                } else
                {
                    //_commonCount++;
                    sum = _commonSum;
                    count = _commonSumCount;
                }
            }

            if (sum != null && count != null)
                for (ChosenEntranceDiscipline chosen : _dataUtil.getChosenEntranceDisciplineSet(preStudent.getRequestedEnrollmentDirection()))
                {
                    if (chosen.getFinalMark() != null && chosen.getFinalMarkSource() != null)
                    {
                        int source = chosen.getFinalMarkSource();

                        if (source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 ||
                                source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2 ||
                                source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL ||
                                source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL)
                        {
                            sum.add(chosen.getFinalMark());
                            count.increment();
                        }
                    }
                }
        }
    }

    @Override
    public int hashCode()
    {
        return _id.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        return _id.equals(((SumLev) obj)._id);
    }

    @Override
    public int compareTo(ISumLev o)
    {
        if (_level != ((SumLev) o)._level)
            throw new RuntimeException("invalid compare");

        if (equals(o))
            return 0;

        int r = getTitle().compareTo(o.getTitle());
        if (r != 0) return r;

        return _id.compareTo(((SumLev) o)._id);
    }

    public void append(SumLev sumLev)
    {
        _commonSum.add(sumLev._commonSum);
        _commonSumCount.add(sumLev._commonSumCount);
        _benefitSum.add(sumLev._benefitSum);
        _benefitSumCount.add(sumLev._benefitSumCount);
        _olympiadSum.add(sumLev._olympiadSum);
        _olympiadSumCount.add(sumLev._olympiadSumCount);
        _targetSum.add(sumLev._targetSum);
        _targetSumCount.add(sumLev._targetSumCount);

        _count += sumLev._count;
        _benefitCount += sumLev._benefitCount;
        _olympiadCount += sumLev._olympiadCount;
        _targetCount += sumLev._targetCount;
    }

    //

    @Override
    public int getLevel()
    {
        return _level;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    @Override
    public double getCommonAvg()
    {
        return _commonSumCount.intValue() == 0 ? 0.0 : _commonSum.doubleValue() / _commonSumCount.doubleValue();
    }

    @Override
    public double getBenefitAvg()
    {
        return _benefitSumCount.intValue() == 0 ? 0.0 : _benefitSum.doubleValue() / _benefitSumCount.doubleValue();
    }

    @Override
    public double getOlympiadAvg()
    {
        return _olympiadSumCount.intValue() == 0 ? 0.0 : _olympiadSum.doubleValue() / _olympiadSumCount.doubleValue();
    }

    @Override
    public double getTargetAvg()
    {
        return _targetSumCount.intValue() == 0 ? 0.0 : _targetSum.doubleValue() / _targetSumCount.doubleValue();
    }

    @Override
    public int getCount()
    {
        return _count;
    }

    @Override
    public int getBenefitCount()
    {
        return _benefitCount;
    }

    @Override
    public int getOlympiadCount()
    {
        return _olympiadCount;
    }

    @Override
    public int getTargetCount()
    {
        return _targetCount;
    }
}
