/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcSettings.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
public class EcSettingsIndividualProgressDAO extends UniBaseDao implements IEcSettingsIndividualProgressDAO
{

    @Override
    public void toggleIndividualProgressUse(Long individualProgressId, EnrollmentCampaign enrollmentCampaign)
    {
        lock("enrCampaignEntrantIndividualProgress." + enrollmentCampaign.getId() + "." + individualProgressId);

        final IndividualProgress individualProgress = getNotNull(individualProgressId);
        EnrCampaignEntrantIndividualProgress campaignIndividualProgress = getByNaturalId(new EnrCampaignEntrantIndividualProgress.NaturalId(individualProgress, enrollmentCampaign));

        if (campaignIndividualProgress != null)
        {
            delete(campaignIndividualProgress);
            return;
        }
        else if (campaignIndividualProgress == null)
        {
            campaignIndividualProgress = new EnrCampaignEntrantIndividualProgress(enrollmentCampaign, individualProgress);
            saveOrUpdate(campaignIndividualProgress);
        }
    }
}