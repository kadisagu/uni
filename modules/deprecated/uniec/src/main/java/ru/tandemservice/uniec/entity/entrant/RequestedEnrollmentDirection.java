package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

/**
 * Выбранное направление приема
 */
public class RequestedEnrollmentDirection extends RequestedEnrollmentDirectionGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return getEnrollmentDirection().getTitle();
    }

    public static final String P_STRING_NUMBER = "stringNumber";

    public String getStringNumber()
    {
        return UniecDAOFacade.getRequestedEnrollmentDirectionNumberFormatter().format(this);
    }

    public static final String P_SHORT_TITLE = "shortTitle";

    public String getShortTitle()
    {
        final EducationLevelsHighSchool levelsHighSchool = getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();
        EducationLevels educationLevel = levelsHighSchool.getEducationLevel();
        return educationLevel.getTitleCodePrefix() + " " + levelsHighSchool.getShortTitle() + " (" + educationLevel.getLevelType().getTitle() + ")";
    }

    public static final String P_WITHOUT_ENTRANCE_DISCIPLINES = "withoutEntranceDisciplines";

    public boolean isWithoutEntranceDisciplines()
    {
        String code = getCompetitionKind().getCode();
        return UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(code) || UniecDefines.COMPETITION_KIND_INTERVIEW.equals(code);
    }

    public static final String P_TOTAL_PROFILE_WORK_EXPERIENCE_MONTHS = "totalProfileWorkExperienceMonths";

    public int getTotalProfileWorkExperienceMonths()
    {
        Integer years = getProfileWorkExperienceYears();
        Integer months = getProfileWorkExperienceMonths();

        return (years == null ? 0 : years) * 12 + (months == null ? 0 : months);
    }
}