/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRequestByCGDailyReport.EntrantRequestByCGDailyReportAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author agolubenko
 * @since Jun 10, 2010
 */
@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompetitionGroupListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<CompetitionGroup> findValues(String filter)
            {
                Session session = getSession();

                MQBuilder builder = getCompetitionGroupsBuilder(model, filter);

                List<CompetitionGroup> result = builder.getResultList(session);
                Collections.sort(result, CompetitionGroup.TITLED_COMPARATOR);

                return new ListResult<CompetitionGroup>(result, result.size());
            }
        });

        model.getReport().setDateFrom((model.getEnrollmentCampaign() != null) ? model.getEnrollmentCampaign().getStartDate() : CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())));
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        EntrantRequestByCGDailyReport report = model.getReport();
        report.setFormingDate(new Date());

        List<CompetitionGroup> competitionGroupList = model.getCompetitionGroupList();
        if (competitionGroupList.isEmpty())
        {
            competitionGroupList = getCompetitionGroupsBuilder(model, null).getResultList(session);
            Collections.sort(competitionGroupList, CompetitionGroup.TITLED_COMPARATOR);
        }
        report.setCompetitionGroupTitle(UniStringUtils.join(competitionGroupList, CompetitionGroup.title().s(), "; "));

        DatabaseFile content = new DatabaseFile();
        content.setContent(new EntrantRequestByCGDailyReportContentGenerator(session, report, competitionGroupList).generateReportContent());
        session.save(content);

        report.setContent(content);
        session.save(report);
    }

    private MQBuilder getCompetitionGroupsBuilder(Model model, String filter)
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "enrollmentDirection");
        builder.addJoin("enrollmentDirection", EnrollmentDirection.competitionGroup().s(), "competitionGroup");
        builder.add(MQExpression.eq("enrollmentDirection", EnrollmentDirection.enrollmentCampaign().s(), model.getEnrollmentCampaign()));
        if (model.getReport().getDevelopForm() != null)
        {
            builder.add(MQExpression.eq("enrollmentDirection", EnrollmentDirection.educationOrgUnit().developForm().s(), model.getReport().getDevelopForm()));
        }
        if (StringUtils.isNotEmpty(filter))
        {
            builder.add(MQExpression.like("enrollmentDirection", EnrollmentDirection.competitionGroup().title().s(), CoreStringUtils.escapeLike(filter)));
        }
        builder.getSelectAliasList().clear();
        builder.addSelect("competitionGroup");
        builder.setNeedDistinct(true);
        return builder;
    }
}
