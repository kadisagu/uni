/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;

/**
 * @author Vasily Zhukov
 * @since 27.04.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        
        prepareListDataSource(component);
    }
    
    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model =getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<PriorityProfileEduOuDTO> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().educationLevelHighSchool().fullTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().formativeOrgUnit().shortTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developForm().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developCondition().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developTech().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developPeriod().title().s()).setOrderable(false));
        dataSource.addColumn(new BlockColumn("priority", "Приоритет").setOrderable(false));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        
        deactivate(component);
    }
}
