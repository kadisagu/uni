/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantLetterDistribution.EntrantLetterDistributionAdd;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 06.07.2009
 */
class ReportRowGroup extends ReportRow
{
    private List<ReportRow> _rowList = new ArrayList<ReportRow>();

    public ReportRowGroup(String title)
    {
        super(null, title, null);
    }

    public List<ReportRow> getRowList()
    {
        return _rowList;
    }

    @Override
    public boolean isUseFullTitle()
    {
        return false;
    }
}
