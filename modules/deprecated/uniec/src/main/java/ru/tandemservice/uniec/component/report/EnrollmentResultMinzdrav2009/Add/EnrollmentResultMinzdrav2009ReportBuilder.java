/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultMinzdrav2009.Add;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.base.entity.codes.AddressCountryTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 21.08.2009
 */
class EnrollmentResultMinzdrav2009ReportBuilder
{
    private Model _model;
    private Session _session;

    EnrollmentResultMinzdrav2009ReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        Map<String, String> code2Title_A = new HashMap<>();
        code2Title_A.put("1", "очную");
        code2Title_A.put("2", "заочную");
        code2Title_A.put("3", "очно-заочную");
        code2Title_A.put("4", "экстернат");
        code2Title_A.put("5", "самостоятельную");

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("developForm_A", code2Title_A.get(_model.getDevelopForm().getCode()));
        injectModifier.put("vuz", TopOrgUnit.getInstance().getTitle());
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle());
        injectModifier.put("formingDate", new SimpleDateFormat("d MMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(_model.getReport().getFormingDate()));

//        List<EnrollmentDirection> directionList = new ArrayList<EnrollmentDirection>();
//        if (!_model.isQualificationActive())
//            directionList.addAll(MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session));
//        else
//            for (EnrollmentDirection direction : MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session))
//                if (_model.getQualificationList().contains(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
//                    directionList.add(direction);


        _session.createCriteria(EducationLevelsHighSchool.class);
        _session.createCriteria(EducationLevels.class);
        _session.createCriteria(Qualifications.class);

        final List<BlockItem> dataList = new ArrayList<>();
        int[] total = new int[14];
        final Map<String, Integer> title2count = new HashMap<>();
        for (Map.Entry<Long, int[]> entry : getData().entrySet())
        {
            EducationLevelsHighSchool item = (EducationLevelsHighSchool) _session.get(EducationLevelsHighSchool.class, entry.getKey());
            int[] data = entry.getValue();

            for (int i = 0; i < total.length; i++) total[i] += data[i];

            String title = item.getPrintTitle();
            String fullTitle = title + " (" + item.getOrgUnit().getShortTitle() + ")";
            dataList.add(new BlockItem(item.getPrintTitle(), fullTitle, data));

            Integer count = title2count.get(title);
            if (count == null) count = 0;
            title2count.put(title, count + 1);
        }
        Collections.sort(dataList, ITitled.TITLED_COMPARATOR);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", new String[0][]);
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfRow templateRow = table.getRowList().get(currentRowIndex);
                for (BlockItem item : dataList)
                {
                    int[] data = item.getData();
                    String title = item.getTitle();
                    String fullTitle = item.getFullTitle();
                    int count = title2count.get(title);

                    // titleRow
                    RtfRow titleRow = templateRow.getClone();
                    insertDataInRow(titleRow, "\\b " + (count > 1 ? fullTitle : title) + "\\b0 ", null, null, null, null, null);

                    // row1
                    RtfRow row1 = templateRow.getClone();
                    insertDataInRow(row1, "Подано заявлений", "01", data[0], data[1], data[2], data[3]);

                    // row2
                    RtfRow row2 = templateRow.getClone();
                    insertDataInRow(row2, "    В т.ч. целевой прием", null, data[4], data[4], data[5], data[6]);

                    // row3
                    RtfRow row3 = templateRow.getClone();
                    insertDataInRow(row3, "Зачислено", "02", data[7], data[8], data[9], data[10]);

                    // row4
                    RtfRow row4 = templateRow.getClone();
                    insertDataInRow(row4, "    В т.ч. целевой прием", null, data[11], data[11], data[12], data[13]);

                    table.getRowList().add(titleRow);
                    table.getRowList().add(row1);
                    table.getRowList().add(row2);
                    table.getRowList().add(row3);
                    table.getRowList().add(row4);
                }
            }
        });

        injectModifier.put("S00", Integer.toString(total[0]));
        injectModifier.put("S01", Integer.toString(total[1]));
        injectModifier.put("S02", Integer.toString(total[2]));
        injectModifier.put("S03", Integer.toString(total[3]));
        injectModifier.put("S10", Integer.toString(total[4]));
        injectModifier.put("S11", Integer.toString(total[4]));
        injectModifier.put("S12", Integer.toString(total[5]));
        injectModifier.put("S13", Integer.toString(total[6]));
        injectModifier.put("S20", Integer.toString(total[7]));
        injectModifier.put("S21", Integer.toString(total[8]));
        injectModifier.put("S22", Integer.toString(total[9]));
        injectModifier.put("S23", Integer.toString(total[10]));
        injectModifier.put("S30", Integer.toString(total[11]));
        injectModifier.put("S31", Integer.toString(total[11]));
        injectModifier.put("S32", Integer.toString(total[12]));
        injectModifier.put("S33", Integer.toString(total[13]));

        // c1
        Number c1 = new DQLSelectBuilder().fromDataSource(getRequestedBuilder()
                .column(value("0"), "x")
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
                .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )))
                .group(property("request.id"))
                .group(property("ct.id"))
                .buildQuery(), "t")
                .createCountStatement(new DQLExecutionContext(_session)).uniqueResult();
        injectModifier.put("C1", c1 == null ? "0" : Integer.toString(c1.intValue()));

        // c2
        DQLSelectBuilder c2Builder = new DQLSelectBuilder()
                .column(DQLFunctions.sum(property("ed." + EnrollmentDirection.P_MINISTERIAL_PLAN)))
                .fromEntity(EnrollmentDirection.class, "ed")
                .joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou")
                .where(eq(property("ed." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        patchEduOrgUnit(c2Builder);
        Number c2 = c2Builder.createStatement(_session).uniqueResult();
        injectModifier.put("C2", c2 == null ? "0" : Integer.toString(c2.intValue()));

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_MINZDRAV_2009);
        return RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), injectModifier, tableModifier);
    }

    private static void insertDataInRow(RtfRow row, String title, String code, Integer n1, Integer n2, Integer n3, Integer n4)
    {
        List<RtfCell> list = row.getCellList();
        IRtfText titleText = RtfBean.getElementFactory().createRtfText("\\ql " + title);
        titleText.setRaw(true);
        IRtfText codeText = RtfBean.getElementFactory().createRtfText((code == null ? "" : code) + "\\qc");
        codeText.setRaw(true);

        list.get(0).setElementList(Collections.singletonList((IRtfElement) titleText));
        list.get(1).setElementList(Collections.singletonList((IRtfElement) codeText));

        list.get(2).setElementList(n1 == null ? Collections.<IRtfElement>emptyList() : Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(Integer.toString(n1))));
        list.get(3).setElementList(n2 == null ? Collections.<IRtfElement>emptyList() : Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(Integer.toString(n2))));
        list.get(4).setElementList(n3 == null ? Collections.<IRtfElement>emptyList() : Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(Integer.toString(n3))));
        list.get(5).setElementList(n4 == null ? Collections.<IRtfElement>emptyList() : Collections.singletonList((IRtfElement) RtfBean.getElementFactory().createRtfText(Integer.toString(n4))));
    }

    private Map<Long, int[]> getData()
    {
        // ROW 1

        DQLSelectBuilder r01 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .group(property("request.id"))
                .group(property("ct.id"))
                .group(property("hs.id"));

        DQLSelectBuilder r02 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .where(or(
                        eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)),
                        eq(property("d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE))
                ))
                .group(property("request.id"))
                .group(property("hs.id"));

        DQLSelectBuilder r03 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
                .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                        eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
                .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)))
                .group(property("request.id"))
                .group(property("ct.id"))
                .group(property("hs.id"));

        DQLSelectBuilder r04 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
                .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)))
                .group(property("request.id"))
                .group(property("ct.id"))
                .group(property("hs.id"));

        // ROW 2

        DQLSelectBuilder r11 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .where(eq(property("d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE)))
                .group(property("request.id"))
                .group(property("hs.id"));

        DQLSelectBuilder r13 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
                .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                        eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
                .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)))
                .where(eq(property("d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE)))
                .group(property("request.id"))
                .group(property("hs.id"));

        DQLSelectBuilder r14 = getRequestedBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
                .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)))
                .where(eq(property("d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE)))
                .group(property("request.id"))
                .group(property("hs.id"));

        // ROW 3

        DQLSelectBuilder r21 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid");

        DQLSelectBuilder r22 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid")
                .where(or(
                        eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)),
                        eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE))
                ));

        DQLSelectBuilder r23 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
                .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                        eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
                .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)));

        DQLSelectBuilder r24 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
                .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)));

        // ROW 4

        DQLSelectBuilder r31 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid")
                .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE)));

        DQLSelectBuilder r33 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
                .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                        eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
                .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)))
                .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE)));

        DQLSelectBuilder r34 = getPreliminaryBuilder()
                .column(property("hs.id"), "hsid")
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
                .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)))
                .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE)));

        // DATA

        Map<Long, int[]> data = new HashMap<>();
        DQLSelectBuilder[] builder = new DQLSelectBuilder[]{
                r01, r02, r03, r04,
                r11, r13, r14,
                r21, r22, r23, r24,
                r31, r33, r34
        };

        for (int i = 0; i < builder.length; i++)
        {
            for (Object[] row : getGroupedByHS(builder[i]).createStatement(new DQLExecutionContext(_session)).<Object[]>list())
            {
                Long id = ((Number) row[0]).longValue();
                Number count = (Number) row[1];

                int[] arr = data.get(id);
                if (arr == null)
                    data.put(id, arr = new int[builder.length]);
                arr[i] = count == null ? 0 : count.intValue();
            }
        }

        return data;
    }

    private static DQLSelectBuilder getGroupedByHS(DQLSelectBuilder query)
    {
        return new DQLSelectBuilder()
                .column(property("t.hsid"), "id")
                .column(DQLFunctions.countStar(), "count")
                .fromDataSource(query.buildQuery(), "t")
                .group(property("t.hsid"));
    }

    @SuppressWarnings("deprecation")
    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");

        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        patchEduOrgUnit(builder);

        if (_model.isStudentCategoryActive())
            builder.where(in(property("d." + RequestedEnrollmentDirection.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));

        return builder;
    }

    @SuppressWarnings("deprecation")
    private DQLSelectBuilder getPreliminaryBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");

        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        patchEduOrgUnit(builder);

        if (_model.isStudentCategoryActive())
            builder.where(in(property("p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));
        if (_model.isParallelActive() && _model.getReport().getExcludeParallel())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder)
    {
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        if (_model.isQualificationActive())
            builder.where(in(property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            builder.where(_model.getTerritorialOrgUnitList().isEmpty() ?
                    isNull(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)) :
                    in(property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList())
            );
        }
        builder.where(eq(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), value(_model.getDevelopForm())));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_TECH), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), _model.getDevelopPeriodList()));
    }

    private static class BlockItem implements ITitled
    {
        private String _title;
        private String _fullTitle;
        private int[] _data;

        private BlockItem(String title, String fullTitle, int[] data)
        {
            _title = title;
            _fullTitle = fullTitle;
            _data = data;
        }

        @Override
        public String getTitle()
        {
            return _title;
        }

        public String getFullTitle()
        {
            return _fullTitle;
        }

        public int[] getData()
        {
            return _data;
        }
    }
}
