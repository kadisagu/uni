/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.PassDisciplineDates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.DisciplineDateSetting;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EntranceExamPhase;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author vip_delete
 * @since 08.06.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final String SUBJECT_PASS_FORM_FILTER_NAME = "subjectPassForm";
    public static final String COMPENSATION_TYPE_FILTER_NAME = "compensationType";
    public static final String DEVELOP_FORM_FILTER_NAME = "developForm";


    private List<SubjectPassForm> _subjectPassFormList;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompensationType> _compensationTypeList;
    private List<DevelopForm> _developFormList;
    private IDataSettings _settings;
    private DynamicListDataSource _dataSource;
    private Map<Long, DisciplineDateSetting> _dateSettingByDiscipline;
    private Map<Long, List<EntranceExamPhase>> _phaseByDiscipline;
    private EntranceExamPhase _currentPhase;
    private List<EntranceExamPhase> _phases4Delete = new ArrayList<>();
    private Long _inEditMode;


    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }


    // Data Sources
    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public List<SubjectPassForm> getSubjectPassFormList()
    {
        return _subjectPassFormList;
    }

    public void setSubjectPassFormList(List<SubjectPassForm> subjectPassFormList)
    {
        _subjectPassFormList = subjectPassFormList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public Map<Long, DisciplineDateSetting> getDateSettingByDiscipline()
    {
        return _dateSettingByDiscipline;
    }

    public void setDateSettingByDiscipline(Map<Long, DisciplineDateSetting> dateSettingByDiscipline)
    {
        _dateSettingByDiscipline = dateSettingByDiscipline;
    }

    public Map<Long, List<EntranceExamPhase>> getPhaseByDiscipline()
    {
        return _phaseByDiscipline;
    }

    public void setPhaseByDiscipline(Map<Long, List<EntranceExamPhase>> phaseByDiscipline)
    {
        _phaseByDiscipline = phaseByDiscipline;
    }


    // Current values
    public Discipline2RealizationWayRelation getCurrent()
    {
        return (Discipline2RealizationWayRelation) _dataSource.getCurrentEntity();
    }

    public DisciplineDateSetting getCurrentDateSetting()
    {
        return _dateSettingByDiscipline.get(getCurrent().getId());
    }

    public List<EntranceExamPhase> getCurrentPassDates()
    {
        List<EntranceExamPhase> entranceExamPhases = _phaseByDiscipline.get(getCurrent().getId());
        return entranceExamPhases == null ? Collections.emptyList() : entranceExamPhases;
    }

    public EntranceExamPhase getCurrentPhase()
    {
        return _currentPhase;
    }

    public void setCurrentPhase(EntranceExamPhase currentPhase)
    {
        _currentPhase = currentPhase;
    }

    public CoreCollectionUtils.Pair<Long, EntranceExamPhase> getPhaseParam()
    {
        return new CoreCollectionUtils.Pair<>(getCurrent().getId(), getCurrentPhase());
    }

    public Long getInEditMode()
    {
        return _inEditMode;
    }

    public void setInEditMode(Long inEditMode)
    {
        _inEditMode = inEditMode;
    }

    public boolean isCurrentInEditMode()
    {
        Discipline2RealizationWayRelation current = getCurrent();

        return current != null && _inEditMode != null && _inEditMode.equals(current.getId());
    }

    public boolean isEditMode()
    {
        return _inEditMode != null;
    }


    // Getters|Setters
    public SubjectPassForm getSubjectPassForm()
    {
        return (SubjectPassForm) getSettings().get(SUBJECT_PASS_FORM_FILTER_NAME);
    }

    public void setSubjectPassForm(SubjectPassForm enrollmentCampaign)
    {
        getSettings().set(SUBJECT_PASS_FORM_FILTER_NAME, enrollmentCampaign);
    }

    public CompensationType getCompensationType()
    {
        return (CompensationType) getSettings().get(COMPENSATION_TYPE_FILTER_NAME);
    }

    public void setCompensationType(CompensationType enrollmentCampaign)
    {
        getSettings().set(COMPENSATION_TYPE_FILTER_NAME, enrollmentCampaign);
    }

    public DevelopForm getDevelopForm()
    {
        return (DevelopForm) getSettings().get(DEVELOP_FORM_FILTER_NAME);
    }

    public void setDevelopForm(DevelopForm enrollmentCampaign)
    {
        getSettings().set(DEVELOP_FORM_FILTER_NAME, enrollmentCampaign);
    }

    public void addPhase4Delete(EntranceExamPhase phase)
    {
        _phases4Delete.add(phase);
    }

    public List<EntranceExamPhase> getPhases4Delete()
    {
        return _phases4Delete;
    }

    public boolean isShowSearchList()
    {
        return (getSubjectPassForm() != null) && (getEnrollmentCampaign() != null) && (getCompensationType() != null) && (getDevelopForm() != null);
    }

    public String getCurrentDateString()
    {
        if(_currentPhase == null) return "";

        int index = getCurrentPassDates().indexOf(_currentPhase);

        return "Этап " + String.valueOf(index+1) + ": " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_currentPhase.getPassDate());
    }
}
