/* $Id$ */
package ru.tandemservice.uniec.component.settings.EnrollmentCampaignModuleForms;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;


/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 12.07.12
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickSave(IBusinessComponent component)
    {
        getDao().update(getModel(component));

    }
}