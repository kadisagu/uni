/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic1.MarkDistribution;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.MarkDistributionModel;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public class Model extends MarkDistributionModel
{
    private ISelectModel _examGroupTypeListModel;
    private ISelectModel _compensationTypeListModel;
    private ISelectModel _developFormListModel;

    // Getters & Setters

    public ISelectModel getExamGroupTypeListModel()
    {
        return _examGroupTypeListModel;
    }

    public void setExamGroupTypeListModel(ISelectModel examGroupTypeListModel)
    {
        _examGroupTypeListModel = examGroupTypeListModel;
    }

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }
}
