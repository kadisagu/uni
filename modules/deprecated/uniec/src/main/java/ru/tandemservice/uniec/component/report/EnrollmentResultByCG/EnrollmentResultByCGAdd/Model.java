/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByCG.EnrollmentResultByCGAdd;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EnrollmentResultByCGReport;

/**
 * @author ekachanova
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private EnrollmentResultByCGReport _report = new EnrollmentResultByCGReport();
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private String _competitionGroupName;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // Getters & Setters

    public EnrollmentResultByCGReport getReport()
    {
        return _report;
    }

    public void setReport(EnrollmentResultByCGReport report)
    {
        _report = report;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public String getCompetitionGroupName()
    {
        return _competitionGroupName;
    }

    public void setCompetitionGroupName(String competitionGroupName)
    {
        _competitionGroupName = competitionGroupName;
    }
}
