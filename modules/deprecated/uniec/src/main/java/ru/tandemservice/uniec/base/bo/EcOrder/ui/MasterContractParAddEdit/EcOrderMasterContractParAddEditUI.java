/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.MasterContractParAddEdit;

import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EntrMasterContractExtract;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderMasterContractParAddEditUI extends BaseEcOrderParAddEditUI
{
    @Override
    public EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent)
    {
        return new EntrMasterContractExtract();
    }
}
