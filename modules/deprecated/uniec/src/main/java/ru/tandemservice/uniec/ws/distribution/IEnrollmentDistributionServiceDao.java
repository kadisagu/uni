/* $Id$ */
package ru.tandemservice.uniec.ws.distribution;

import java.util.Set;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;

/**
 * @author Vasily Zhukov
 * @since 24.07.2011
 */
public interface IEnrollmentDistributionServiceDao
{
    public static final SpringBeanCache<IEnrollmentDistributionServiceDao> INSTANCE = new SpringBeanCache<IEnrollmentDistributionServiceDao>(IEnrollmentDistributionServiceDao.class.getName());

    EnrollmentDistributionEnvironmentNode getEnrollmentDistributionEnvironmentNode(Set<IEcgDistribution> distributionSet);
}
