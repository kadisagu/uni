/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEnrolmentRecommentdationAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation;

/**
 * @author Боба
 * @since 08.08.2008
 */
@Input(keys = { "entrantEnrolmentRecommendationId", "entrantId" }, bindings = { "entrantEnrolmentRecommendation.id", "entrant.id" })
public class Model
{
    private EntrantEnrolmentRecommendation _entrantEnrolmentRecommendation = new EntrantEnrolmentRecommendation();
    private List<EnrollmentRecommendation> _enrolmentRecommendationsList;
    private Entrant _entrant = new Entrant();

    public List<EnrollmentRecommendation> getEnrolmentRecommendationsList()
    {
        return _enrolmentRecommendationsList;
    }

    public void setEnrolmentRecommendationsList(List<EnrollmentRecommendation> enrolmentRecommendationsList)
    {
        _enrolmentRecommendationsList = enrolmentRecommendationsList;
    }

    public EntrantEnrolmentRecommendation getEntrantEnrolmentRecommendation()
    {
        return _entrantEnrolmentRecommendation;
    }

    public void setEntrantEnrolmentRecommendation(EntrantEnrolmentRecommendation entrantEnrolmentRecommendation)
    {
        _entrantEnrolmentRecommendation = entrantEnrolmentRecommendation;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }
}
