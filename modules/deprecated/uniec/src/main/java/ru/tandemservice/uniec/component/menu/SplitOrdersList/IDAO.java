/* $Id$ */
package ru.tandemservice.uniec.component.menu.SplitOrdersList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 20.08.2012
 */
public interface IDAO extends IUniDao<Model>
{
}