// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.uniec.ws.entrantrating;

import java.util.Date;
import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Vasily Zhukov
 * @since 24.03.2011
 */
@WebService
public class EntrantRatingTAService
{
    /**
     * Получение рейтинга абитуриентов по всем направлениями подготовки (специальностям)
     *
     * @param enrollmentCampaignTitle Название приемной кампании
     * @param dateFrom                Дата приказа с
     * @param dateTo                  Дата приказа по
     * @param compensationTypeId      Код вида возмещения затрат
     * @param studentCategoryIds      Коды категорий обучаемых
     * @param hideEmptyDirections     Скрывать направления приема без абитуриентов
     * @param qualificationIds        Коды квалификаций
     * @param developFormIds          Коды форм освоения
     * @param developConditionIds     Коды условий освоения
     * @return Рейтинг абитуриентов
     */
    public EntrantRatingTAEnvironmentNode getEntrantRatingEnvironment(
            @WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle,
            @WebParam(name = "dateFrom") Date dateFrom,
            @WebParam(name = "dateTo") Date dateTo,
            @WebParam(name = "compensationTypeId") String compensationTypeId,
            @WebParam(name = "studentCategoryIds") List<String> studentCategoryIds,
            @WebParam(name = "hideEmptyDirections") Boolean hideEmptyDirections,
            @WebParam(name = "qualificationIds") List<String> qualificationIds,
            @WebParam(name = "developFormIds") List<String> developFormIds,
            @WebParam(name = "developConditionIds") List<String> developConditionIds)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        if (dateFrom == null)
            throw new RuntimeException("WebParam 'dateFrom' is not specified!");

        if (dateTo == null)
            throw new RuntimeException("WebParam 'dateTo' is not specified!");

        if (compensationTypeId == null)
            throw new RuntimeException("WebParam 'compensationTypeId' is not specified!");

        return IEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(enrollmentCampaignTitle, dateFrom, dateTo, compensationTypeId, studentCategoryIds, Boolean.TRUE.equals(hideEmptyDirections), qualificationIds, developFormIds, developConditionIds);
    }

    /**
     * Получение рейтинга абитуриентов по выбранному направлению подготовки (специальности)
     *
     * @param enrollmentCampaignTitle название приемной кампании
     * @param dateFrom                Дата приказа с
     * @param dateTo                  Дата приказа по
     * @param compensationTypeId      Код вида возмещения затрат
     * @param studentCategoryIds      Коды категорий обучаемых
     * @param enrollmentDirectionId   Идентификатор направления приема
     * @return Рейтинг абитуриентов
     */
    public EntrantRatingTAEnvironmentNode getEntrantRatingEnvironmentForEnrollmentDirection(
            @WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle,
            @WebParam(name = "dateFrom") Date dateFrom,
            @WebParam(name = "dateTo") Date dateTo,
            @WebParam(name = "compensationTypeId") String compensationTypeId,
            @WebParam(name = "studentCategoryIds") List<String> studentCategoryIds,
            @WebParam(name = "enrollmentDirectionId") String enrollmentDirectionId)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        if (dateFrom == null)
            throw new RuntimeException("WebParam 'dateFrom' is not specified!");

        if (dateTo == null)
            throw new RuntimeException("WebParam 'dateTo' is not specified!");

        if (compensationTypeId == null)
            throw new RuntimeException("WebParam 'compensationTypeId' is not specified!");

        if (enrollmentDirectionId == null)
            throw new RuntimeException("WebParam 'enrollmentDirectionId' is not specified!");

        return IEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(enrollmentCampaignTitle, dateFrom, dateTo, compensationTypeId, studentCategoryIds, enrollmentDirectionId);
    }
}
