/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.component.report.EntrantStudentCard.EntrantStudentCardPrintFactory;
import ru.tandemservice.uniec.dao.IStateExamCertificateDAO;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.AccessCourse;
import ru.tandemservice.uniec.entity.catalog.AccessDepartment;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private final static OrderDescriptionRegistry _entrantStateExamCertificatesOrderSettings = new OrderDescriptionRegistry("certificate");
    private IStateExamCertificateDAO _certificateDAO;

    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));

        List<String> sourceInfoList = new ArrayList<>();
        for (EntrantInfoAboutUniversity item : getList(EntrantInfoAboutUniversity.class, EntrantInfoAboutUniversity.L_ENTRANT, model.getEntrant()))
            sourceInfoList.add(item.getSourceInfo().getTitle());
        model.setSourceInfoTitle(StringUtils.join(sourceInfoList.iterator(), ", "));

        MQBuilder builder = new MQBuilder(EntrantEnrolmentRecommendation.ENTITY_CLASS, "e", new String[]{EntrantEnrolmentRecommendation.L_RECOMMENDATION});
        builder.add(MQExpression.eq("e", EntrantEnrolmentRecommendation.L_ENTRANT, model.getEntrant()));
        builder.addOrder("e", EntrantEnrolmentRecommendation.recommendation().title().s());
        model.setEnrollmentRecomendationsTitle(UniStringUtils.join(builder.<EnrollmentRecommendation>getResultList(getSession()), EnrollmentRecommendation.P_TITLE, "\n"));

        builder = new MQBuilder(EntrantAccessCourse.ENTITY_CLASS, "e", new String[]{EntrantAccessCourse.L_COURSE});
        builder.add(MQExpression.eq("e", EntrantAccessCourse.L_ENTRANT, model.getEntrant()));
        builder.addOrder("e", EntrantAccessCourse.course().title().s());
        model.setAccessCoursesTitle(UniStringUtils.join(builder.<AccessCourse>getResultList(getSession()), AccessCourse.P_TITLE, "\n"));

        builder = new MQBuilder(EntrantAccessDepartment.ENTITY_CLASS, "e", new String[]{EntrantAccessDepartment.L_ACCESS_DEPARTMENT});
        builder.add(MQExpression.eq("e", EntrantAccessDepartment.L_ENTRANT, model.getEntrant()));
        builder.addOrder("e", EntrantAccessDepartment.accessDepartment().title().s());
        model.setAccessDepartmentsTitle(UniStringUtils.join(builder.<AccessDepartment>getResultList(getSession()), AccessDepartment.P_TITLE, "\n"));

        model.setEntrantCustomStates(EcEntrantManager.instance().entrantCustomStateDAO().getActiveStatesList(model.getEntrant().getId(), new Date()));
    }

    /**
     * Метод вынесен для возможности кастомизации в проектах
     * @param entrant абитуриент
     * @return список дипломов участника олимпиады
     */
    protected List<OlympiadDiploma> getDiplomaList(Entrant entrant)
    {
        return getList(OlympiadDiploma.class, OlympiadDiploma.L_ENTRANT, entrant);
    }

    @Override
    public void prepareOlympiadDiplomaDataSource(Model model)
    {
        List<OlympiadDiploma> diplomaList = getDiplomaList(model.getEntrant());

        List<Discipline2OlympiadDiplomaRelation> relationList = new DQLSelectBuilder().fromEntity(Discipline2OlympiadDiplomaRelation.class, "e").column("e")
                .where(eq(property(Discipline2OlympiadDiplomaRelation.diploma().entrant().fromAlias("e")), value(model.getEntrant())))
                .createStatement(new DQLExecutionContext(getSession()))
                .list();

        Map<OlympiadDiploma, List<Discipline2OlympiadDiplomaRelation>> map = new HashMap<>();
        for (Discipline2OlympiadDiplomaRelation relation : relationList)
        {
            List<Discipline2OlympiadDiplomaRelation> list = map.get(relation.getDiploma());
            if (list == null)
                map.put(relation.getDiploma(), list = new ArrayList<>());
            list.add(relation);
        }

        List<Model.OlympiadDiplomaWrapper> resultList = new ArrayList<>();

        for (OlympiadDiploma diploma : diplomaList)
        {
            List<Discipline2OlympiadDiplomaRelation> list = map.get(diploma);

            if (list == null || list.isEmpty())
            {
                // нет зачтений
                resultList.add(new Model.OlympiadDiplomaWrapper(diploma, null, null));
            } else
            {
                // есть зачтения
                List<EnrollmentDirection> enrollmentDirectionList = new ArrayList<>();
                List<CompensationType> compensationTypeList = new ArrayList<>();
                List<String> titleList = new ArrayList<>();

                for (Discipline2OlympiadDiplomaRelation relation : list)
                {
                    enrollmentDirectionList.add(relation.getEnrollmentDirection());
                    compensationTypeList.add(relation.getCompensationType());
                    titleList.add(relation.getDiscipline().getTitle());
                }

                Collections.sort(titleList);

                EnrollmentDirection enrollmentDirection = enrollmentDirectionList.iterator().next();
                CompensationType compensationType = compensationTypeList.iterator().next();

                String enrollmentDirectionTitle = compensationType == null ?
                        (enrollmentDirection == null ? null : enrollmentDirection.getTitle()) :
                        (enrollmentDirection == null ? null : enrollmentDirection.getTitle() + " (" + compensationType.getShortTitle() + ")");

                String disciplineListTitle = StringUtils.join(titleList, ", ");

                resultList.add(new Model.OlympiadDiplomaWrapper(diploma, enrollmentDirectionTitle, disciplineListTitle));
            }
        }

        if (resultList.isEmpty())
            model.getOlympiadDiplomaDataSource().setCountRow(2);
        else
            model.getOlympiadDiplomaDataSource().setCountRow(resultList.size());

        UniBaseUtils.createPage(model.getOlympiadDiplomaDataSource(), resultList);
    }

    @Override
    public void prepareEntrantStateExamCertificatesDataSource(Model model)
    {
        DynamicListDataSource<EntrantStateExamCertificate> dataSource = model.getEntrantStateExamCertificatesDataSource();
        dataSource.setCountRow(3);

        MQBuilder builder = new MQBuilder(EntrantStateExamCertificate.ENTITY_CLASS, "certificate");
        builder.add(MQExpression.eq("certificate", EntrantStateExamCertificate.L_ENTRANT, model.getEntrant()));
        _entrantStateExamCertificatesOrderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());

        model.setCertificate2SubjectsMarks(_certificateDAO.prepareCertificatesSubjectsMarks(model.getEntrantStateExamCertificatesDataSource().getEntityList()));
    }

    public void setCertificateDAO(IStateExamCertificateDAO certificateDAO)
    {
        _certificateDAO = certificateDAO;
    }

    @Override
    public void preparePreliminaryEnrollmentDataSource(Model model)
    {
        DynamicListDataSource<PreliminaryEnrollmentStudent> dataSource = model.getPreliminaryEnrollmentDataSource();
        List<PreliminaryEnrollmentStudent> preliminaryEnrollmentStudentList = UniecDAOFacade.getEntrantDAO().getPreliminaryEnrollmentStudentList(model.getEntrant());
        dataSource.setCountRow(preliminaryEnrollmentStudentList.size());
        UniBaseUtils.createPage(dataSource, preliminaryEnrollmentStudentList);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
            wrapper.setViewProperty("parallelLockedDisabled", !UniDefines.STUDENT_CATEGORY_LISTENER.equals(((PreliminaryEnrollmentStudent) wrapper.getEntity()).getStudentCategory().getCode()));
    }

    @Override
    public byte[] printStudentCard(Long studentId)
    {
        PreliminaryEnrollmentStudent student = get(PreliminaryEnrollmentStudent.class, studentId);
        EnrollmentCampaign campaign = student.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getEnrollmentCampaign();
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "s");
        builder.addJoinFetch("s", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.addJoinFetch("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON, "p");
        builder.add(MQExpression.eq("s", "id", studentId));
        return new EntrantStudentCardPrintFactory(getSession(), campaign, builder, "s", "p", "d").getReportContent();
    }

    @Override
    public void prepareIndividualProgressDataSource(Model model)
    {
        DynamicListDataSource<EntrantIndividualProgress> dataSource = model.getIndividualProgressDataSource();
        dataSource.setCountRow(3);
        List<EntrantIndividualProgress> individualProgressList = getList(EntrantIndividualProgress.class, EntrantIndividualProgress.entrant(), model.getEntrant());
        UniBaseUtils.createPage(dataSource, individualProgressList);
    }

    @Override
    public String getRequests4IndividualProgress(EntrantIndividualProgress achievement){
        return new DQLSelectBuilder()
                .fromEntity(EntrantRequest.class, "req")
                .column(property("req", EntrantRequest.regNumber()))
                .where(notExistsByExpr(IndProg2Request4Exclude.class, "ip2req",
                                 and(eq(property("ip2req", IndProg2Request4Exclude.individualProgress()), value(achievement)),
                                     eq(property("ip2req", IndProg2Request4Exclude.request()), property("req")))
                ))
                .where(eq(property("req", EntrantRequest.entrant()), value(achievement.getEntrant())))
                .order(property("req", EntrantRequest.regNumber()))
                .createStatement(getSession()).<Integer>list()
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
    }
}