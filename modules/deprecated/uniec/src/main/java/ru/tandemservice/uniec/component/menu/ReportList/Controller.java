/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ReportList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author ekachanova
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<DAO.ReportDefinitionWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model, component1.getUserContext().getPrincipalContext());
        });

        PublisherLinkColumn plc = new PublisherLinkColumn("Название отчета", "reportDefinition.title");
        plc.setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return ((DAO.ReportDefinitionWrapper) entity).getReportDefinition().getParameters();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return ((DAO.ReportDefinitionWrapper) entity).getReportDefinition().getComponentName();
            }
        });
        plc.setStyle("color:#002f86;");
        dataSource.addColumn(plc.setOrderable(false));

        model.setDataSource(dataSource);
    }
}
