/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantBenefitDistrib.EntrantBenefitDistribAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.NotUsedBenefit;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author ekachanova
 */
class EntrantBenefitDistribReportBuilder
{
    private Model _model;
    private Session _session;

    EntrantBenefitDistribReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        final Map<OrgUnit, Map<MultiKey, ReportRow>> formOrgUnit2ReportRows = new HashMap<>();
        final Map<OrgUnit, IReportRow> formOrgUnit2TotalReportRow = new HashMap<>();
        final Map<OrgUnit, IReportRow> formOrgUnit2TotalHumanReportRow = new HashMap<>();
        final Map<EnrollmentDirection, MultiKey> direction2key = new HashMap<>();

        // prepare tables structure
        for (EnrollmentDirection direction : getEnrollmentDirectionBuilder().<EnrollmentDirection>getResultList(_session))
        {
            OrgUnit formativeOrgUnit = direction.getEducationOrgUnit().getFormativeOrgUnit();
            Map<MultiKey, ReportRow> key2row = formOrgUnit2ReportRows.get(formativeOrgUnit);
            if (key2row == null)
                formOrgUnit2ReportRows.put(formativeOrgUnit, key2row = new HashMap<>());
            EducationLevelsHighSchool highSchool = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            boolean branchOrRepresentation;
            String orgUnitType = formativeOrgUnit.getOrgUnitType().getCode();

            MultiKey key;
            if (branchOrRepresentation = (OrgUnitTypeCodes.BRANCH.equals(orgUnitType) || OrgUnitTypeCodes.REPRESENTATION.equals(orgUnitType)))
                key = new MultiKey(highSchool.getId(), formativeOrgUnit.getId());
            else
                key = new MultiKey(new Object[]{highSchool.getId()});

            direction2key.put(direction, key);

            if (!formOrgUnit2TotalReportRow.containsKey(formativeOrgUnit))
                formOrgUnit2TotalReportRow.put(formativeOrgUnit, new ReportRow("Итого", ""));

            if (!formOrgUnit2TotalHumanReportRow.containsKey(formativeOrgUnit))
                formOrgUnit2TotalHumanReportRow.put(formativeOrgUnit, new HumanReportRow("Итого (человек)"));

            ReportRow row = key2row.get(key);
            if (row == null)
            {
                String title = branchOrRepresentation ? highSchool.getPrintTitle() + " (" + formativeOrgUnit.getShortTitle() + ")" : highSchool.getDisplayableTitle();
                key2row.put(key, new ReportRow(title, title + " (" + highSchool.getOrgUnit().getShortTitle() + ")"));
            }
        }
        ReportRow summaryRow = new ReportRow("Итого", "");
        HumanReportRow summaryHumanRow = new HumanReportRow("Итого (человек)");

        // calc tables digits
        boolean enroll = _model.getReport().getEnrollmentCampaignStage().equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT);
        final Map<Long, Set<String>> personId2BenefitTitles = getPersonId2BenefitTitles(enroll ? getPreliminaryEnrollmentStudentBuilder() : getRequestedEnrollmentDirectionBuilder());
        for (Object obj : (enroll ? getPreliminaryEnrollmentStudentBuilder() : getRequestedEnrollmentDirectionBuilder()).getResultList(_session))
        {
            RequestedEnrollmentDirection requested = enroll ? ((PreliminaryEnrollmentStudent) obj).getRequestedEnrollmentDirection() : (RequestedEnrollmentDirection) obj;

            OrgUnit formOrgUnit = requested.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit();
            ReportRow row = formOrgUnit2ReportRows.get(formOrgUnit).get(direction2key.get(requested.getEnrollmentDirection()));
            ReportRow totalReportRow = (ReportRow) formOrgUnit2TotalReportRow.get(formOrgUnit);
            HumanReportRow totalHumanReportRow = (HumanReportRow) formOrgUnit2TotalHumanReportRow.get(formOrgUnit);

            Long personId = requested.getEntrantRequest().getEntrant().getPerson().getId();
            Long humanId = enroll ? ((PreliminaryEnrollmentStudent) obj).getId() : personId;

            Set<String> benefitTitles = personId2BenefitTitles.get(personId);
            if (CollectionUtils.isNotEmpty(benefitTitles))
            {
                for (String benefitTitle : benefitTitles)
                {
                    row.getBenefitTitle2count().put(benefitTitle, row.getCount(benefitTitle) + 1);
                    totalReportRow.getBenefitTitle2count().put(benefitTitle, totalReportRow.getCount(benefitTitle) + 1);
                    summaryRow.getBenefitTitle2count().put(benefitTitle, summaryRow.getCount(benefitTitle) + 1);

                    Set<Long> humanIds = totalHumanReportRow.getBenefitTitle2humanIds().get(benefitTitle);
                    if (humanIds == null)
                        totalHumanReportRow.getBenefitTitle2humanIds().put(benefitTitle, humanIds = new HashSet<>());
                    humanIds.add(humanId);

                    Set<Long> summaryHumanIds = summaryHumanRow.getBenefitTitle2humanIds().get(benefitTitle);
                    if (summaryHumanIds == null)
                        summaryHumanRow.getBenefitTitle2humanIds().put(benefitTitle, summaryHumanIds = new HashSet<>());
                    summaryHumanIds.add(humanId);
                }
            }
            row.setTotal(row.getTotal() + 1);
            totalReportRow.setTotal(totalReportRow.getTotal() + 1);
            totalHumanReportRow.getTotal().add(humanId);

            summaryRow.setTotal(summaryRow.getTotal() + 1);
            summaryHumanRow.getTotal().add(humanId);
        }

        final List<String> benefitTitleList = getBenefitTitleList();

        // create rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_BENEFIT_DISTRIB);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        final IRtfControl b = RtfBean.getElementFactory().createRtfControl(IRtfData.B);
        final IRtfControl b0 = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
        //        b0.setValue(0);
        IRtfControl page = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        IRtfElement par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
        IRtfElement pard = RtfBean.getElementFactory().createRtfControl(IRtfData.PARD);
        IRtfControl qc = RtfBean.getElementFactory().createRtfControl(IRtfData.QC);

        IRtfText h1 = RtfBean.getElementFactory().createRtfText(TopOrgUnit.getInstance().getTitle());
        IRtfText h2 = RtfBean.getElementFactory().createRtfText("Распределение по видам льгот абитуриентов, поступающих вне конкурса");
        IRtfText h3 = RtfBean.getElementFactory().createRtfText(StringUtils.capitalize(_model.getReport().getEnrollmentCampaignStage()) + " на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()));

        document.getElementList().add(b);
        document.getElementList().add(qc);
        document.getElementList().add(h1);
        document.getElementList().add(par);
        document.getElementList().add(qc);
        document.getElementList().add(h2);
        document.getElementList().add(b0);
        document.getElementList().add(par);
        document.getElementList().add(qc);
        document.getElementList().add(h3);
        document.getElementList().add(par);
        document.getElementList().add(pard);

        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        RtfRow header = table.getRowList().get(1);
        if (benefitTitleList.isEmpty())
        {
            UniRtfUtil.fillCell(header.getCellList().get(2), "");
        } else
        {
            int[] parts = new int[benefitTitleList.size()];
            Arrays.fill(parts, 1);
            RtfUtil.splitRow(header, 2, (newCell, index) -> newCell.setElementList(Arrays.asList((IRtfElement) RtfBean.getElementFactory().createRtfText(benefitTitleList.get(index)))), parts);
        }

        RtfDocument table_doc = RtfBean.getElementFactory().createRtfDocument();
        table_doc.getElementList().add(table);
        document.getElementList().remove(table);

        List<OrgUnit> formOrgUnitList = new ArrayList<>(formOrgUnit2ReportRows.keySet());
        Collections.sort(formOrgUnitList, (o1, o2) -> {
            int result = Integer.compare(o1.getOrgUnitType().getPriority(), o2.getOrgUnitType().getPriority());
            if (result == 0)
                result = o1.getTitle().compareTo(o2.getTitle());
            return result;
        });

        RtfRowIntercepterBase splitIntercepter = new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                int[] parts = new int[benefitTitleList.size()];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 2, null, parts);
            }
        };

        for (OrgUnit formOrgUnit : formOrgUnitList)
        {
            IRtfText formOrgUnitTitle = RtfBean.getElementFactory().createRtfText(StringUtils.isNotEmpty(formOrgUnit.getNominativeCaseTitle())
                    ? formOrgUnit.getNominativeCaseTitle() : formOrgUnit.getTitle());
            document.getElementList().add(b);
            document.getElementList().add(formOrgUnitTitle);
            document.getElementList().add(b0);
            document.getElementList().add(par);

            List<ReportRow> reportRowList = new ArrayList<>(formOrgUnit2ReportRows.get(formOrgUnit).values());
            Collections.sort(reportRowList);

            String[][] data = new String[reportRowList.size()][benefitTitleList.size() + 2];
            Set<String> existsTitles = new HashSet<>();
            for (int i = 0; i < reportRowList.size(); i++)
            {
                ReportRow reportRow = reportRowList.get(i);
                String title = reportRow.getTitle();
                if (existsTitles.contains(title))
                    title = reportRow.getFullTitle();
                else
                    existsTitles.add(title);
                data[i][0] = title;
                data[i][1] = String.valueOf(reportRow.getTotal());
                fillDataByBenefits(benefitTitleList, reportRow, data, i);
            }

            RtfDocument part = table_doc.getClone();
            RtfTableModifier modifier = new RtfTableModifier();
            modifier.put("T", data);
            modifier.put("T", splitIntercepter);

            String[][] totalData = new String[1][benefitTitleList.size() + 2];
            fillDataRow(formOrgUnit2TotalReportRow.get(formOrgUnit), benefitTitleList, totalData, 0);
            modifier.put("TOTAL", totalData);
            modifier.put("TOTAL", splitIntercepter);

            String[][] totalHumanData = new String[1][benefitTitleList.size() + 2];
            fillDataRow(formOrgUnit2TotalHumanReportRow.get(formOrgUnit), benefitTitleList, totalHumanData, 0);
            modifier.put("TOTAL_HUMAN", totalHumanData);
            modifier.put("TOTAL_HUMAN", splitIntercepter);

            modifier.modify(part);

            document.getElementList().add(part);
            document.getElementList().add(page);
        }

        IRtfText summaryTitle = RtfBean.getElementFactory().createRtfText("Итого по " + TopOrgUnit.getInstance().getShortTitle());
        document.getElementList().add(b);
        document.getElementList().add(summaryTitle);
        document.getElementList().add(b0);
        document.getElementList().add(par);

        String[][] summaryData = new String[2][benefitTitleList.size() + 2];
        fillDataRow(summaryRow, benefitTitleList, summaryData, 0);
        fillDataRow(summaryHumanRow, benefitTitleList, summaryData, 1);

        RtfDocument part = table_doc.getClone();
        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("T", summaryData);
        modifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                int[] parts = new int[benefitTitleList.size()];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 2, null, parts);
                table.getRowList().remove(table.getRowList().size() - 2);
                table.getRowList().remove(table.getRowList().size() - 1);
            }
        });
        modifier.modify(part);
        document.getElementList().add(part);

        return RtfUtil.toByteArray(document);
    }

    private static void fillDataRow(IReportRow reportRow, List<String> benefitTitleList, String[][] data, int index)
    {
        data[index][0] = reportRow.getTitle();
        data[index][1] = String.valueOf(reportRow.getTotalCount());
        fillDataByBenefits(benefitTitleList, reportRow, data, index);
    }

    private static void fillDataByBenefits(List<String> benefitTitleList, IReportRow reportRow, String[][] data, int index)
    {
        for (int k = 0; k < benefitTitleList.size(); k++)
        {
            data[index][k + 2] = String.valueOf(reportRow.getCount(benefitTitleList.get(k)));
        }
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        builder.addJoin("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));

        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));

        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                        MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));

        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));

        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));

        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));

        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));

        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION));

        if (_model.getReport().getCompensationType() != null)
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if (!_model.getStudentCategoryList().isEmpty())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));

        if (_model.getReport().getEnrollmentCampaignStage().equals(Model.getENROLLMENT_CAMPAIGN_STAGE_DOCUMENTS()))
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

        if (_model.getReport().getEnrollmentCampaignStage().equals(Model.ENROLLMENT_CAMPAIGN_STAGE_EXAMS))
        {
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
        }
        return builder;
    }

    private MQBuilder getPreliminaryEnrollmentStudentBuilder()
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        if (_model.getReport().getCompensationType() != null)
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if (!_model.getStudentCategoryList().isEmpty())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));

        builder.add(UniMQExpression.betweenDate("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_CODE, UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION));

        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));

        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));

        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                        MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));

        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));

        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));

        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));

        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));

        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));

        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, false));
        return builder;
    }

    private MQBuilder getEnrollmentDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));

        builder.addJoin("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));

        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));

        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                        MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));

        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));

        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));

        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));

        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));

        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        return builder;
    }

    private Map<Long, Set<String>> getPersonId2BenefitTitles(MQBuilder subBuilder)
    {
        MQBuilder builder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "pb");
        if (_model.getReport().getEnrollmentCampaignStage().equals(Model.ENROLLMENT_CAMPAIGN_STAGE_ENROLLMENT))
            subBuilder.addSelect("p", new String[]{PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON});
        else
            subBuilder.addSelect("r", new String[]{RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON});
        builder.add(MQExpression.in("pb", PersonBenefit.L_PERSON, subBuilder));
        Map<Long, Set<String>> personId2BenefitTitles = new HashMap<>();
        for (PersonBenefit pb : builder.<PersonBenefit>getResultList(_session))
        {
            Set<String> benefitTitles = personId2BenefitTitles.get(pb.getPerson().getId());
            if (benefitTitles == null)
                personId2BenefitTitles.put(pb.getPerson().getId(), benefitTitles = new HashSet<>());
            benefitTitles.add(pb.getBenefit().getTitle());
        }
        return personId2BenefitTitles;
    }

    private List<String> getBenefitTitleList()
    {
        MQBuilder builder = new MQBuilder(Benefit.ENTITY_CLASS, "b", new String[]{Benefit.P_TITLE});
        MQBuilder sub = new MQBuilder(NotUsedBenefit.ENTITY_CLASS, "n", new String[]{NotUsedBenefit.L_BENEFIT + ".id"});
        sub.add(MQExpression.eq("n", NotUsedBenefit.P_PERSON_ROLE_NAME, Entrant.class.getSimpleName()));
        builder.add(MQExpression.notIn("b", "id", sub));
        builder.addOrder("b", Benefit.P_TITLE);
        return builder.getResultList(_session);
    }

    private interface IReportRow
    {
        int getCount(String benefitTitle);

        int getTotalCount();

        String getTitle();
    }

    @SuppressWarnings("unchecked")
    private static class ReportRow implements Comparable, IReportRow
    {
        private String _title;
        private String _fullTitle;
        private int _total;
        private Map<String, Integer> _benefitTitle2count = new HashMap<>();

        private ReportRow(String title, String fullTitle)
        {
            _title = title;
            _fullTitle = fullTitle;
        }

        public Map<String, Integer> getBenefitTitle2count()
        {
            return _benefitTitle2count;
        }

        @Override
        public String getTitle()
        {
            return _title;
        }

        public String getFullTitle()
        {
            return _fullTitle;
        }

        public void setTotal(int total)
        {
            _total = total;
        }

        @Override
        public int getCount(String benefitTitle)
        {
            Integer count = getBenefitTitle2count().get(benefitTitle);
            return count == null ? 0 : count;
        }

        @Override
        public int getTotalCount()
        {
            return getTotal();
        }

        public int getTotal()
        {
            return _total;
        }

        @Override
        public int compareTo(Object o)
        {
            return getFullTitle().compareTo(((ReportRow) o).getFullTitle());
        }
    }

    private static class HumanReportRow implements IReportRow
    {
        private String _title;
        private Set<Long> _total = new HashSet<>();
        private Map<String, Set<Long>> _benefitTitle2humanIds = new HashMap<>();

        private HumanReportRow(String title)
        {
            _title = title;
        }

        public Map<String, Set<Long>> getBenefitTitle2humanIds()
        {
            return _benefitTitle2humanIds;
        }

        @Override
        public String getTitle()
        {
            return _title;
        }

        @Override
        public int getCount(String benefitTitle)
        {
            Set<Long> humanIds = getBenefitTitle2humanIds().get(benefitTitle);
            return humanIds == null ? 0 : humanIds.size();
        }

        @Override
        public int getTotalCount()
        {
            return getTotal().size();
        }

        public Set<Long> getTotal()
        {
            return _total;
        }
    }
}
