package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantData;

import java.util.Arrays;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;

import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantDataBlock
{
    // names
    public static final String ENTRANT_DS = "entrantDS";
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String HAS_OLYMPIAD_DIPLOMA_DS = "hasOlympiadDiplomaDS";
    public static final String RECOMENDATIONS_DS = "recomendationsDS";
    public static final String ACCESS_COURSES_DS = "accessCoursesDS";
    public static final String PASS_PROFILE_EDUCATION_DS = "passProfileEducationDS";
    public static final String HAS_EXAM_RESULT_DS = "hasExamResultDS";
    public static final String INFO_SOURCE_DS = "infoSourceDS";
    public static final String ENTRANT_ARCHIVAL_DS = "entrantArchivalDS";
    public static final String ENTRANT_CUSTOM_STATE_EXIST_DS = "entrantCustomStateExistDS";
    public static final String ENTRANT_CUSTOM_STATE_LIST_DS = "entrantCustomStateListDS";
    public static final String ENTRANT_HAS_INDIVIDUAL_PROGRESS_DS = "entrantHasIndividualProgressDS";
    public static final String ENTRANT_INDIVIDUAL_PROGRESS_DS = "entrantIndividualProgressDS";
    public static final String BASE_4_EXAM_BY_DIFFERENT_SOURCES_DS = "base4ExamByDifferentSourcesDS";

    // ids
    public static final long ENTRANT_YES = 0L;
    public static final long ENTRANT_NOT = 1L;
    public static final long HAS_OLYMPIAD_DIPLOMA_YES = 0L;
    public static final long HAS_OLYMPIAD_DIPLOMA_NOT = 1L;
    public static final long PASS_PROFILE_EDUCATION_YES = 0L;
    public static final long PASS_PROFILE_EDUCATION_NOT = 1L;
    public static final long HAS_EXAM_RESULT_YES = 0L;
    public static final long HAS_EXAM_RESULT_NOT = 1L;
    public static final long ENTRANT_ARCHIVAL_YES = 0L;
    public static final long ENTRANT_ARCHIVAL_NOT = 1L;
    public static final long ENTRANT_HAS_INDIVIDUAL_PROGRESS_YES = 0L;
    public static final long ENTRANT_HAS_INDIVIDUAL_PROGRESS_NOT = 1L;

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EntrantDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createEntrantDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(ENTRANT_YES, "да"), new IdentifiableWrapper(ENTRANT_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createEnrollmentCampaignDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrollmentCampaign.class, EnrollmentCampaign.id());
    }

    public static IDefaultComboDataSourceHandler createHasOlympiadDiplomaDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(HAS_OLYMPIAD_DIPLOMA_YES, "да"), new IdentifiableWrapper(HAS_OLYMPIAD_DIPLOMA_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createRecomendationsDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EnrollmentRecommendation.class);
    }

    public static IDefaultComboDataSourceHandler createAccessCoursesDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, AccessCourse.class);
    }

    public static IDefaultComboDataSourceHandler createPassProfileEducationDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(PASS_PROFILE_EDUCATION_YES, "да"), new IdentifiableWrapper(PASS_PROFILE_EDUCATION_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createHasExamResultDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(HAS_EXAM_RESULT_YES, "да"), new IdentifiableWrapper(HAS_EXAM_RESULT_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createInfoSourceDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, SourceInfoAboutUniversity.class);
    }

    public static IDefaultComboDataSourceHandler createEntrantArchivalDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(ENTRANT_ARCHIVAL_YES, "да"), new IdentifiableWrapper(ENTRANT_ARCHIVAL_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createEntrantCustomStateExistDS(String name)
    {
        TwinComboDataSourceHandler handler = new TwinComboDataSourceHandler(name);
        handler.yesTitle("Содержит");
        handler.noTitle("Не содержит");
        return handler;
    }

    public static IDefaultComboDataSourceHandler createEntrantCustomStateListDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, EntrantCustomStateCI.class);
    }

    public static IDefaultComboDataSourceHandler createEntrantHasIndividualProgressDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(ENTRANT_HAS_INDIVIDUAL_PROGRESS_YES, "да"),
                                      new IdentifiableWrapper(ENTRANT_HAS_INDIVIDUAL_PROGRESS_NOT, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createEntrantIndividualProgressDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, IndividualProgress.class);
    }
}
