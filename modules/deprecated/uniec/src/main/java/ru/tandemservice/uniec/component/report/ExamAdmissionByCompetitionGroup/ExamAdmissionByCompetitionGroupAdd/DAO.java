/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.ExamAdmissionByCompetitionGroup.ExamAdmissionByCompetitionGroupAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.report.ExamAdmissionByCGReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setCompetitionGroupList(getCompetitionGroupList(model));
        model.setCompensTypeList(getList(CompensationType.class, CompensationType.P_SHORT_TITLE));
    }

    @Override
    public List<CompetitionGroup> getCompetitionGroupList(Model model)
    {
        List<CompetitionGroup> competitionGroupList = getList(CompetitionGroup.class, CompetitionGroup.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());
        Collections.sort(competitionGroupList, CompetitionGroup.TITLED_COMPARATOR);
        return competitionGroupList;
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        ExamAdmissionByCGReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setCompetitionGroupTitle(model.getCompetitionGroup().getTitle());

        DatabaseFile databaseFile = new ExamAdmissionByCGReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
