/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.EcProfileDistributionManager;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.Pub.EcProfileDistributionPub;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.EcgpDistributionDTO;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Arrays;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Configuration
public class EcProfileDistributionList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String DISTRIBUTION_CATEGORY_DS = "distributionCategoryDS";
    public static final String DISTRIBUTION_DS = "distributionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeComboDSHandler()).addColumn(CompensationType.P_SHORT_TITLE))
                .addDataSource(selectDS(DISTRIBUTION_CATEGORY_DS, distributionCategoryComboDSHandler()))
                .addDataSource(searchListDS(DISTRIBUTION_DS, distributionDS(), distributionDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), CompensationType.class, CompensationType.P_SHORT_TITLE);
    }

    @Bean
    public IDefaultComboDataSourceHandler distributionCategoryComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        EcProfileDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS,
                        EcProfileDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION
                ));
    }

    @Bean
    public ColumnListExtPoint distributionDS()
    {
        return columnListExtPointBuilder(DISTRIBUTION_DS)
                .addColumn(checkboxColumn("checkbox").create())
                .addColumn(textColumn("ecgItem", EcgpDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.P_TITLE).width("20").required(true).create())
                .addColumn(textColumn("formativeOrgUnit", EcgpDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.educationOrgUnit().formativeOrgUnit().shortTitle().s()).width("10").create())
                .addColumn(textColumn("territorialOrgUnit", EcgpDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).width("10").create())
                .addColumn(textColumn("fctp", EcgpDistributionDTO.ECG_ITEM + "." + EnrollmentDirection.educationOrgUnit().developCombinationTitle().s()).width("10").create())
                .addColumn(textColumn("compensationType", EcgpDistributionDTO.COMPENSATION_TYPE + "." + CompensationType.shortTitle().s()).width("10").create())
                .addColumn(publisherColumn("title", EcgpDistributionDTO.TITLE).publisherLinkResolver(getPublisherLinkResolver()).required(true).create())
                .addColumn(blockColumn("quota", "quotaBlockColumn").width("10").create())
                //.addColumn(dateColumn("formingDate", EcgpDistributionDTO.FORMING_DATE).width("5").create())
                .addColumn(indicatorColumn("add").path(EcgpDistributionDTO.ADD_DISABLED)
                        .addIndicator(Boolean.FALSE, new IndicatorColumn.Item(CommonDefines.ICON_ADD.getName(), CommonDefines.ICON_ADD.getLabel(), "onClickAdd")).clickable(true)
                        .required(true).permissionKey("profileDistributionSectEdit").create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("distributionDS.delete.alert", EcgpDistributionDTO.TITLE)).disabled(EcgpDistributionDTO.DELETE_DISABLED).permissionKey("profileDistributionSectEdit").create())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> distributionDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }

    private static IPublisherLinkResolver getPublisherLinkResolver()
    {
        return new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                return EcProfileDistributionPub.class.getSimpleName();
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                return ((EcgpDistributionDTO) entity).getPersistentId();
            }
        };
    }
}
