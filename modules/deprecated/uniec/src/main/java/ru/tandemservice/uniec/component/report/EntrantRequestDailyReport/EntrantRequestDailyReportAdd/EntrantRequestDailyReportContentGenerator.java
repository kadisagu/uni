/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRequestDailyReport.EntrantRequestDailyReportAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntrantRequestDailyReport;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;

/**
 * @author oleyba
 * @since 13.06.2009
 */
class EntrantRequestDailyReportContentGenerator
{
    private Model _model;
    private EntrantRequestDailyReport report;
    private Session session;

    public EntrantRequestDailyReportContentGenerator(Model model, EntrantRequestDailyReport report, Session session)
    {
        _model = model;
        this.report = report;
        this.session = session;
    }

    public byte[] generateReportContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REQUEST_DAILY_REPORT);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();

        initReportData();

        if (report.getEnrollmentDirection() != null)
        {
            final EducationOrgUnit eduOrgUnit = report.getEnrollmentDirection().getEducationOrgUnit();
            String enrollmentDirectionTitle = eduOrgUnit.getEducationLevelHighSchool().getPrintTitle()
                    + " (" + StringUtils.lowerCase(eduOrgUnit.getDevelopForm().getTitle()) + ", "
                    + StringUtils.lowerCase(eduOrgUnit.getDevelopCondition().getTitle()) + ", "
                    + eduOrgUnit.getDevelopPeriod().getTitle() + ")";
            final String formativeOrgUnit = eduOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle();
            final String territorialOrgUnit = eduOrgUnit.getTerritorialOrgUnit() == null ? "" : eduOrgUnit.getTerritorialOrgUnit().getNominativeCaseTitle();
            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put("headerTableB", new String[][]{{enrollmentDirectionTitle, formativeOrgUnit, territorialOrgUnit}, {"Проходной балл на "+DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())+ budget_mark}});
            tableModifier.put("headerTableC", new String[][]{{enrollmentDirectionTitle, formativeOrgUnit, territorialOrgUnit}, {"Проходной балл на "+DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())+ contract_mark}});
            tableModifier.modify(document);
        } else
        {
            //по всему ОУ (форма обучения: очная, заочная; квалификация: специалист; категория поступающего: студент)
            String header = "по всему образовательному учреждению";
            if (!_model.getStudentCategoryList().isEmpty() || !_model.getQualificationList().isEmpty() || !_model.getDevelopFormList().isEmpty() || !_model.getDevelopConditionList().isEmpty())
            {
                header = header + " (";
                if (!_model.getDevelopFormList().isEmpty())
                    header = header + "форма обучения: " + UniStringUtils.join(_model.getDevelopFormList(), "title", ", ") + "; ";
                if (!_model.getDevelopConditionList().isEmpty())
                    header = header + "условие обучения: " + UniStringUtils.join(_model.getDevelopConditionList(), "title", ", ") + "; ";
                if (!_model.getQualificationList().isEmpty())
                    header = header + "квалификация: " + UniStringUtils.join(_model.getQualificationList(), "title", ", ") + "; ";
                if (!_model.getStudentCategoryList().isEmpty())
                    header = header + "категория поступающего: " + UniStringUtils.join(_model.getStudentCategoryList(), "title", ", ") + "; ";
                header = header.substring(0, header.length() - 2) + ")";
            }
            header = StringUtils.lowerCase(header);
            RtfTableModifier tableModifier = new RtfTableModifier();
            tableModifier.put("headerTableB", new String[][]{{header}});
            tableModifier.put("headerTableC", new String[][]{{header}});
            RtfRowIntercepterBase intercepter = new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    RtfUtil.unitAllCell(table.getRowList().get(0), 0);
                }
            };
            tableModifier.put("headerTableB", intercepter);
            tableModifier.put("headerTableC", intercepter);
            tableModifier.modify(document);
        }

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("cityTitle", cityTitle);
        modifier.put("areaTitle", areaTitle);
        modifier.modify(document);

        fillReportTable(document, budgetMap, "T");
        fillReportTable(document, contractMap, "S");

        return RtfUtil.toByteArray(document);
    }

    // ключ - дата, направление приема
    private Map<Date, ReportRow> budgetMap = new HashMap<>();
    private Map<Date, ReportRow> contractMap = new HashMap<>();
    String cityTitle = "город ОУ";
    String areaTitle = "область ОУ";
    String budget_mark;
    String contract_mark;

    private void initReportData()
    {
        // загружаем местоположение ОУ
        AddressDetailed address = TopOrgUnit.getInstance().getAddress();
        AddressItem city = address == null ? null : address.getSettlement();
        if (city != null) cityTitle = city.getTitleWithType();
        AddressItem area = city;
        if (area != null)
            while (area.getParent() != null) area = area.getParent();
        if (area != null) areaTitle = area.getTitleWithType();

        // выбранные направления приема заявлений, которые подходят под параметры отчета
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        filterByReportParams(builder, "red");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.compensationType().s(), "ct");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.competitionKind().s(), "ck");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.entrantRequest().s(), "eq");
        builder.addJoinFetch("eq", EntrantRequest.entrant().s(), "e");
        builder.addJoinFetch("e", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "idc");
        builder.addLeftJoinFetch("idc", IdentityCard.address().s(), "address");
        builder.addOrder("red", RequestedEnrollmentDirection.P_PRIORITY);
        List<RequestedEnrollmentDirection> directions = builder.<RequestedEnrollmentDirection>getResultList(session);

        // считаем заявления
        for (RequestedEnrollmentDirection red : directions)
        {
            Long requestId = red.getEntrantRequest().getId();

            Calendar regDateC = Calendar.getInstance();
            regDateC.setTime(red.getEntrantRequest().getRegDate());
            regDateC.set(Calendar.HOUR_OF_DAY, 0);
            regDateC.set(Calendar.MINUTE, 0);
            regDateC.set(Calendar.SECOND, 0);
            regDateC.set(Calendar.MILLISECOND, 0);
            Date regDate = regDateC.getTime();

            // считаем или по бюджету, или по контракту
            Map<Date, ReportRow> rowMap = UniDefines.COMPENSATION_TYPE_BUDGET.equals(red.getCompensationType().getCode()) ? budgetMap : contractMap;
            ReportRow row = rowMap.get(regDate);
            if (null == row)
                    rowMap.put(regDate, row = new ReportRow());

            // считаем заявление только один раз, по первому по приоритету направлению
            if (row.requestIdSet.contains(requestId)) continue;
            row.requestIdSet.add(requestId);

            String competitionKindCode = red.getCompetitionKind().getCode();
            if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionKindCode))
                row.competitive++;
            // целевой прием
            if (red.isTargetAdmission())
                row.targetAdmission++;
            else
            {
                // по видам конкурса, если не целевой прием
                if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(competitionKindCode))
                    row.beyondCompetition++;
                if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKindCode))
                    row.withoutEntranceDisciplines++;
            }

            final Entrant entrant = red.getEntrantRequest().getEntrant();
            if (SexCodes.FEMALE.equals(entrant.getPerson().getIdentityCard().getSex().getCode()))
                row.female++;

            AddressBase entrantAddress = entrant.getPerson().getIdentityCard().getAddress();
            AddressItem entrantCity = (entrantAddress != null && entrantAddress instanceof AddressDetailed )? ((AddressDetailed)entrantAddress).getSettlement() : null;
            AddressItem entrantArea = entrantCity;
            if (entrantArea != null)
                while (entrantArea.getParent() != null) entrantArea = entrantArea.getParent();
            // город ОУ
            if (city != null && city.equals(entrantCity))
                row.city++;
                // область ОУ
            else if (area != null && area.equals(entrantArea))
                row.area++;

            GraduationHonour graduationHonour = entrant.getPerson().getPersonEduInstitution() == null ? null : entrant.getPerson().getPersonEduInstitution().getGraduationHonour();
            if (graduationHonour == null) continue;
            String graduationHonourCode = graduationHonour.getCode();
            // золотая медаль
            if (GraduationHonourCodes.GOLD_MEDAL.equals(graduationHonourCode))
                row.goldMedal++;
            // серебряная медаль
            if (GraduationHonourCodes.SILVER_MEDAL.equals(graduationHonourCode))
                row.silverMedal++;
            // красный диплом
            if (GraduationHonourCodes.RED_DIPLOMA.equals(graduationHonourCode))
                row.redDiplom++;
        }

        // считаем проходной балл
        EnrollmentDirection enrollmentDirection = report.getEnrollmentDirection();
        if (enrollmentDirection != null)
        {
            List<RequestedDirectionWrapper> budget = new ArrayList<>();
            List<RequestedDirectionWrapper> budget_ta = new ArrayList<>();
            List<RequestedDirectionWrapper> contract = new ArrayList<>();
            List<RequestedDirectionWrapper> contract_ta = new ArrayList<>();

            MQBuilder directionsBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
            filterByReportParams(directionsBuilder, "red");
            EntrantDataUtil dataUtil = new EntrantDataUtil(session, report.getEnrollmentCampaign(), directionsBuilder);

            for (RequestedEnrollmentDirection direction : directions)
            {
                List<RequestedDirectionWrapper> list = direction.getCompensationType().isBudget() ? (direction.isTargetAdmission() ? budget_ta : budget) : (direction.isTargetAdmission() ? contract_ta : contract);
                list.add(new RequestedDirectionWrapper(direction, dataUtil));
            }

            Map<CompetitionKind, Integer> priorities = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(report.getEnrollmentCampaign());
            for (List<RequestedDirectionWrapper> list : Arrays.asList(budget, budget_ta, contract, contract_ta))
                Collections.sort(list, new RequestedEnrollmentDirectionComparator(priorities));

            budget_mark = printEntranceMark(" на бюджет: ", budget, budget_ta, enrollmentDirection.getMinisterialPlan(), enrollmentDirection.getTargetAdmissionPlanBudget()).toString();
            contract_mark = printEntranceMark(" по договору: ", contract, contract_ta, enrollmentDirection.getContractPlan(), enrollmentDirection.getTargetAdmissionPlanContract()).toString();
        }
    }

    private StringBuilder printEntranceMark(String start, List<RequestedDirectionWrapper> list, List<RequestedDirectionWrapper> list_ta, Integer plan, Integer taPlan)
    {
        StringBuilder budgetB = new StringBuilder(start);
        Integer budgetPlan = wrapToZero(plan) - wrapToZero(taPlan);
        if (budgetPlan <= 0)
           budgetB.append("-");
        else
           budgetB.append(countMatk(list, budgetPlan));
        if (wrapToZero(taPlan) > 0)
        {
            budgetB.append(", по целевому приему: ");
            budgetB.append(countMatk(list_ta, taPlan));
        }
        return budgetB;
    }

    private Integer wrapToZero(Integer plan)
    {
        return null == plan ? 0 : plan;
    }

    private String countMatk(List<RequestedDirectionWrapper> list, Integer plan)
    {
        return list.isEmpty() ? " " : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(list.get(Math.min(list.size() - 1, plan)).getFinalMark());
    }

    /**
     * Накладывает фильтр по параметрам отчета - условия накладываются от альяса выбранного напр. приема
     *
     * @param builder - билдер
     * @param alias   - альяс выбранного напр. приема
     */
    private void filterByReportParams(MQBuilder builder, String alias)
    {
        builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, report.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate(alias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, report.getDateFrom(), report.getDateTo()));

        if (_model.getStudentCategoryList() != null && !_model.getStudentCategoryList().isEmpty())
            builder.add(MQExpression.in(alias, RequestedEnrollmentDirection.L_STUDENT_CATEGORY + ".id", CommonBaseUtil.getPropertiesList(_model.getStudentCategoryList(), "id")));
        if (_model.getQualificationList() != null && !_model.getQualificationList().isEmpty())
            builder.add(MQExpression.in(alias, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION + ".id", CommonBaseUtil.getPropertiesList(_model.getQualificationList(), "id")));
        if (_model.getDevelopConditionList() != null && !_model.getDevelopConditionList().isEmpty())
            builder.add(MQExpression.in(alias, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().s(), _model.getDevelopConditionList()));
        if (_model.getDevelopFormList() != null && !_model.getDevelopFormList().isEmpty())
            builder.add(MQExpression.in(alias, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().s(), _model.getDevelopFormList()));
        if (report.getEnrollmentDirection() != null)
            builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, report.getEnrollmentDirection()));
        builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq(alias, RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
    }

    /**
     * Заполняет основную таблицу
     *
     * @param page ртф
     */
    private void fillReportTable(RtfDocument page, Map<Date, ReportRow> rowMap, String key)
    {
        final List<Date> dateList = new ArrayList<>();
        dateList.addAll(rowMap.keySet());
        Collections.sort(dateList);
        DateFormatter formatter = new DateFormatter("dd.MM");

        List<String[]> tableRows = new ArrayList<>();

        ReportRow current = new ReportRow();
        for (Date date : dateList)
        {
            // накапливаем результат за предыдущие даты
            current.add(rowMap.get(date));
            List<String> row = new ArrayList<>();
            row.add(formatter.format(date));
            row.addAll(current.print());
            tableRows.add(row.toArray(new String[row.size()]));
        }

        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put(key, tableRows.toArray(new String[tableRows.size()][]));
        modifier.modify(page);
    }

    private static class ReportRow
    {
        Set<Long> requestIdSet = new HashSet<>();
        int targetAdmission = 0; // целевой прием
        int goldMedal = 0; // золотая медаль
        int silverMedal = 0; // серебряная медаль
        int redDiplom = 0; // красный диплом
        int withoutEntranceDisciplines = 0; // без вступительных испытаний
        int beyondCompetition = 0; // вне конкурса
        int competitive = 0; // на общих основаниях
        int female = 0; // женщины
        int city = 0; // город ОУ
        int area = 0; // область ОУ

        void add(ReportRow block)
        {
            requestIdSet.addAll(block.requestIdSet);
            targetAdmission = targetAdmission + block.targetAdmission;
            goldMedal = goldMedal + block.goldMedal;
            silverMedal = silverMedal + block.silverMedal;
            redDiplom = redDiplom + block.redDiplom;
            withoutEntranceDisciplines = withoutEntranceDisciplines + block.withoutEntranceDisciplines;
            beyondCompetition = beyondCompetition + block.beyondCompetition;
            competitive = competitive + block.competitive;
            female = female + block.female;
            city = city + block.city;
            area = area + block.area;
        }

        Integer total()
        {
            return requestIdSet.size();
        }

        Integer male()
        {
            return total() - female;
        }

        Integer medal()
        {
            return goldMedal + silverMedal;
        }

        Integer otherRegions()
        {
            return total() - city - area;
        }

        List<String> print()
        {
            return Arrays.asList(
                String.valueOf(total()),
                String.valueOf(targetAdmission),
                String.valueOf(medal()),
                String.valueOf(goldMedal),
                String.valueOf(silverMedal),
                String.valueOf(redDiplom),
                String.valueOf(withoutEntranceDisciplines),
                String.valueOf(beyondCompetition),
                String.valueOf(competitive),
                String.valueOf(male()),
                String.valueOf(female),
                String.valueOf(city),
                String.valueOf(area),
                String.valueOf(otherRegions()));
        }
    }

    private class RequestedDirectionWrapper implements IReportRow
    {
        RequestedEnrollmentDirection requestedEnrollmentDirection;
        Double finalMark;
        private Double averageEduInstMark;

        private RequestedDirectionWrapper(RequestedEnrollmentDirection direction, EntrantDataUtil dataUtil)
        {
            this.requestedEnrollmentDirection = direction;
            this.finalMark = dataUtil.getFinalMark(direction);
            averageEduInstMark = dataUtil.getAverageEduInstMark(direction.getEntrantRequest().getEntrant().getId());
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirection;
        }

        @Override
        public Double getFinalMark()
        {
            return finalMark;
        }

        // все, что участвует в сравнении после суммарного балла, нас не интересует для определения проходного балла
        @Override public Double getProfileMark(){return null;}
        @Override public String getFio(){return "";}
        @Override public Boolean isGraduatedProfileEduInstitution(){return Boolean.FALSE;}
        @Override public Double getCertificateAverageMark(){return averageEduInstMark;}
    }
}
