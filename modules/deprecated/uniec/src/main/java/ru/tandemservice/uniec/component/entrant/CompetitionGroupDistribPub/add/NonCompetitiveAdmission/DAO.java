package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.NonCompetitiveAdmission;

import java.util.List;

import ru.tandemservice.uniec.dao.ecg.EnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO.IEntrantRateRow;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.DAO implements IDAO {
    @Override
    protected List<IEntrantRateRow> getEntrants4Add(final ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.Model model) {
        return IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows4Add(model.getDistrib(), EnrollmentCompetitionGroupDAO.NONCOMPETITIVE_ADMISSION_SELECTOR);
    }
}
