/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author agolubenko
 * @since 10.10.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        Session session = getSession();

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, session);
        EntranceExaminationMeetingReport report = model.getReport();
        report.setRequestsFromDate(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        report.setRequestsToDate(CoreDateUtils.getDayFirstTimeMoment(new Date()));

        model.setFormativeOrgUnitModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelsHighSchoolModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopTechModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setDevelopConditionModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        if (null != model.getOrgUnitId())
        {
            model.getReport().setOrgUnit(get(model.getOrgUnitId()));
            model.getReport().setFormativeOrgUnit(model.getReport().getOrgUnit());
        }
    }

    protected EntranceExaminationMeetingReportContentGenerator createReportGenerator(EntranceExaminationMeetingReport report, List<EnrollmentDirection> enrollmentDirections, List<StudentCategory> studentCategories)
    {
        return new EntranceExaminationMeetingReportContentGenerator(report, enrollmentDirections, studentCategories, getSession());
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        List<EnrollmentDirection> enrollmentDirections = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, session);
        if (enrollmentDirections.isEmpty())
        {
            throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");
        }

        EntranceExaminationMeetingReport report = model.getReport();
        report.setFormingDate(new Date());
        if (model.isTerritorialOrgUnitActive())
        {
            report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_TITLE, ", "));
        }
        if (model.isDevelopConditionActive())
        {
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));
        }
        if (model.isDevelopTechActive())
        {
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.P_TITLE, ", "));
        }
        if (model.isDevelopPeriodActive())
        {
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, ", "));
        }
        if (model.isStudentCategoryActive())
        {
            report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, ", "));
        }
        if (model.isQualificationActive())
        {
            enrollmentDirections = filterDirectionsByQualifications(enrollmentDirections, model.getQualificationList());
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(generateContent(model, enrollmentDirections));
        session.save(content);

        report.setContent(content);
        session.save(report);
    }

    /**
     * @param enrollmentDirections направления приема
     * @return отфильтрованные направления по квалификации
     */
    private List<EnrollmentDirection> filterDirectionsByQualifications(List<EnrollmentDirection> enrollmentDirections, List<Qualifications> qualifications)
    {
        List<EnrollmentDirection> result = new ArrayList<>();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            Qualifications qualification = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
            if (qualifications.contains(qualification))
            {
                result.add(enrollmentDirection);
            }
        }
        return result;
    }

    /**
     * @param model модель
     * @param enrollmentDirections направления приема
     * @return содержимое отчета
     */
    private byte[] generateContent(Model model, List<EnrollmentDirection> enrollmentDirections)
    {
        EntranceExaminationMeetingReport report = model.getReport();
        List<StudentCategory> studentCategories = model.isStudentCategoryActive() ? model.getStudentCategoryList() : Collections.<StudentCategory> emptyList();

        try
        {
            byte[] result;
            // если не по всем направлениям, то просто отчет
            if (!model.isByAllEducationLevels())
            {
                result = createReportGenerator(report, enrollmentDirections, studentCategories).generateReportContent();
            }
            // иначе архив с отчетами
            else
            {
                result = createZipFile(report, studentCategories, getGroupedEnrollmentDirections(enrollmentDirections, model.isPickOutUnlicensedEducationLevels()));
            }
            return result;
        }
        catch (ApplicationException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private List<List<EnrollmentDirection>> getGroupedEnrollmentDirections(List<EnrollmentDirection> enrollmentDirections, boolean pickOutUnlicensedEducationLevels)
    {
        List<List<EnrollmentDirection>> result = new ArrayList<>();

        // если не выделять нелицензированные направления/специальности
        if (!pickOutUnlicensedEducationLevels)
        {
            // то для каждого направления свой отчет
            for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
            {
                result.add(Collections.singletonList(enrollmentDirection));
            }
        }
        // иначе
        else
        {
            // группируем направления по всем параметрам, кроме территориального подразделения
            Map<MultiKey, List<EnrollmentDirection>> enrollmentDirectionGroups = (Map) SafeMap.get(ArrayList.class);
            for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
            {
                EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();

                OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
                EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();
                DevelopForm developForm = educationOrgUnit.getDevelopForm();
                DevelopCondition developCondition = educationOrgUnit.getDevelopCondition();
                DevelopTech developTech = educationOrgUnit.getDevelopTech();
                DevelopPeriod developPeriod = educationOrgUnit.getDevelopPeriod();

                MultiKey key = new MultiKey(new Object[] { formativeOrgUnit, educationLevelHighSchool, developForm, developCondition, developTech, developPeriod });
                enrollmentDirectionGroups.get(key).add(enrollmentDirection);
            }
            result = new ArrayList<>(enrollmentDirectionGroups.values());
        }
        return result;
    }

    /**
     * @param report отчет
     * @param studentCategories категории поступающих
     * @param enrollmentDirections сгруппированные направления приема
     * @return содержимое zip-архива с отчетами
     */
    private byte[] createZipFile(EntranceExaminationMeetingReport report, List<StudentCategory> studentCategories, List<List<EnrollmentDirection>> enrollmentDirections) throws Exception
    {
        ByteArrayOutputStream out = null;
        try
        {
            out = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(out);
            for (int i = 0; i < enrollmentDirections.size(); i++)
            {
                zipOut.putNextEntry(new ZipEntry("Otchet " + (i + 1) + ".xls"));
                zipOut.write(createReportGenerator(report, enrollmentDirections.get(i), studentCategories).generateReportContent());
                zipOut.closeEntry();
            }
            zipOut.close();
            return out.toByteArray();
        }
        finally
        {
            if (out != null)
            {
                out.close();
            }
        }
    }
}
