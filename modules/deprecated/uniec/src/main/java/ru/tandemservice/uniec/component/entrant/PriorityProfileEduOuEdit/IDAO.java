/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 27.04.2012
 */
public interface IDAO extends IUniDao<Model>
{
}
