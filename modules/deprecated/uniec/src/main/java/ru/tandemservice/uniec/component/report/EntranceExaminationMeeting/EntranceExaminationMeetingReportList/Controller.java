/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.report.EntranceExaminationMeetingReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author agolubenko
 * @since 10.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EntranceExaminationMeetingReport> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Протокол")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", EntranceExaminationMeetingReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[] { EntranceExaminationMeetingReport.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE }).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", EntranceExaminationMeetingReport.P_TERRITORIAL_ORG_UNIT).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EntranceExaminationMeetingReport.educationLevelsHighSchool().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EntranceExaminationMeetingReport.developForm().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", EntranceExaminationMeetingReport.compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить отчет от «{0}»?", EntranceExaminationMeetingReport.P_FORMING_DATE).setPermissionKey(model.getDeleteKey()));
        dataSource.setOrder(EntranceExaminationMeetingReport.P_FORMING_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd", new ParametersMap().add("orgUnitId", getModel(component).getOrgUnitId())));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        EntrantFilterUtil.resetEnrollmentCampaignFilter(getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        Long reportId = (Long) component.getListenerParameter();
        boolean doZip = getDao().getNotNull(EntranceExaminationMeetingReport.class, reportId).getEducationLevelsHighSchool() != null;
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("reportId", reportId);
        parameters.put("extension", doZip ? "xls" : "zip");
        parameters.put("zip", doZip);
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, parameters));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
