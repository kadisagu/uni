/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 27.04.2012
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setRequestedEnrollmentDirection(getNotNull(RequestedEnrollmentDirection.class, model.getRequestedEnrollmentDirection().getId()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        List<PriorityProfileEduOu> list = getList(PriorityProfileEduOu.class, PriorityProfileEduOu.requestedEnrollmentDirection(), model.getRequestedEnrollmentDirection(), PriorityProfileEduOu.P_PRIORITY);
        List<ProfileEducationOrgUnit> additionalList = getList(ProfileEducationOrgUnit.class, ProfileEducationOrgUnit.enrollmentDirection(), model.getRequestedEnrollmentDirection().getEnrollmentDirection(), ProfileEducationOrgUnit.P_ID);

        Set<ProfileEducationOrgUnit> used = new HashSet<ProfileEducationOrgUnit>();
        List<PriorityProfileEduOuDTO> dtoList = new ArrayList<PriorityProfileEduOuDTO>();
        long id = -1;
        int priority = 1;
        for (PriorityProfileEduOu item : list)
        {
            PriorityProfileEduOuDTO dto = new PriorityProfileEduOuDTO();
            dto.setProfileEducationOrgUnit(item.getProfileEducationOrgUnit());
            dto.setPriority(priority++);
            dto.setId(id--); // id тут совсем не важен
            dtoList.add(dto);
            used.add(item.getProfileEducationOrgUnit());
        }

        for (ProfileEducationOrgUnit item : additionalList)
        {
            if (used.add(item))
            {
                PriorityProfileEduOuDTO dto = new PriorityProfileEduOuDTO();
                dto.setProfileEducationOrgUnit(item);
                dto.setId(id--);
                dtoList.add(dto);
            }
        }

        model.getDataSource().setCountRow(dtoList.size());
        UniBaseUtils.createPage(model.getDataSource(), dtoList);
    }

    @Override
    public void update(Model model)
    {
        EntrantUtil.lockEntrant(getSession(), model.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant());

        List<PriorityProfileEduOuDTO> withPriorityList = new ArrayList<PriorityProfileEduOuDTO>();
        for (PriorityProfileEduOuDTO item : model.getDataSource().getEntityList())
            if (item.getPriority() != null)
                withPriorityList.add(item);
        Collections.sort(withPriorityList, new Comparator<PriorityProfileEduOuDTO>()
        {
            @Override
            public int compare(PriorityProfileEduOuDTO o1, PriorityProfileEduOuDTO o2)
            {
                return o1.getPriority() - o2.getPriority();
            }
        });

        // удаляем все существующие приоритеты
        for (PriorityProfileEduOu item : getList(PriorityProfileEduOu.class, PriorityProfileEduOu.requestedEnrollmentDirection(), model.getRequestedEnrollmentDirection()))
            getSession().delete(item);
        getSession().flush();

        // сохраняем новые приоритеты
        int priority = 1;
        for (PriorityProfileEduOuDTO item : withPriorityList)
        {
            PriorityProfileEduOu priorityProfileEduOu = new PriorityProfileEduOu();
            priorityProfileEduOu.setRequestedEnrollmentDirection(model.getRequestedEnrollmentDirection());
            priorityProfileEduOu.setProfileEducationOrgUnit(item.getProfileEducationOrgUnit());
            priorityProfileEduOu.setPriority(priority++);
            save(priorityProfileEduOu);
        }
    }
}
