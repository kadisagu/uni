/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.entity.entrant.ExamGroupRow;

import java.util.Set;

/**
 * @author vip_delete
 * @since 12.06.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "examGroup.id")
})
public class Model
{
    private ExamGroup _examGroup = new ExamGroup();
    private String _statusTitle;
    private Set<ExamGroupRow> _invalidRowSet;
    private DynamicListDataSource<ExamGroupRow> _dataSource;

    private String _selectedTab;

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public ExamGroup getExamGroup()
    {
        return _examGroup;
    }

    public void setExamGroup(ExamGroup examGroup)
    {
        _examGroup = examGroup;
    }

    public String getStatusTitle()
    {
        return _statusTitle;
    }

    public void setStatusTitle(String statusTitle)
    {
        _statusTitle = statusTitle;
    }

    public Set<ExamGroupRow> getInvalidRowSet()
    {
        return _invalidRowSet;
    }

    public void setInvalidRowSet(Set<ExamGroupRow> invalidRowSet)
    {
        _invalidRowSet = invalidRowSet;
    }

    public DynamicListDataSource<ExamGroupRow> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExamGroupRow> dataSource)
    {
        _dataSource = dataSource;
    }
}
