package ru.tandemservice.uniec.ws.enrollment;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
public interface IEnrollmentServiceDao
{
    public static final SpringBeanCache<IEnrollmentServiceDao> INSTANCE = new SpringBeanCache<IEnrollmentServiceDao>(IEnrollmentServiceDao.class.getName());

    public EnrollmentEnvironmentNode getEnrollmentEnvironmentData(String enrollmentCampaignTitle);
}
