package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма сдачи дисциплины набора вступительных испытаний
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Discipline2RealizationFormRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation";
    public static final String ENTITY_NAME = "discipline2RealizationFormRelation";
    public static final int VERSION_HASH = 1579949699;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_SUBJECT_PASS_FORM = "subjectPassForm";
    public static final String L_TYPE = "type";

    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private SubjectPassForm _subjectPassForm;     // Форма сдачи дисциплины
    private CompensationType _type;     // Вид возмещения затрат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     */
    @NotNull
    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    /**
     * @param subjectPassForm Форма сдачи дисциплины. Свойство не может быть null.
     */
    public void setSubjectPassForm(SubjectPassForm subjectPassForm)
    {
        dirty(_subjectPassForm, subjectPassForm);
        _subjectPassForm = subjectPassForm;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getType()
    {
        return _type;
    }

    /**
     * @param type Вид возмещения затрат. Свойство не может быть null.
     */
    public void setType(CompensationType type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Discipline2RealizationFormRelationGen)
        {
            setDiscipline(((Discipline2RealizationFormRelation)another).getDiscipline());
            setSubjectPassForm(((Discipline2RealizationFormRelation)another).getSubjectPassForm());
            setType(((Discipline2RealizationFormRelation)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Discipline2RealizationFormRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Discipline2RealizationFormRelation.class;
        }

        public T newInstance()
        {
            return (T) new Discipline2RealizationFormRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "subjectPassForm":
                    return obj.getSubjectPassForm();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "subjectPassForm":
                    obj.setSubjectPassForm((SubjectPassForm) value);
                    return;
                case "type":
                    obj.setType((CompensationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "subjectPassForm":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "subjectPassForm":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "subjectPassForm":
                    return SubjectPassForm.class;
                case "type":
                    return CompensationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Discipline2RealizationFormRelation> _dslPath = new Path<Discipline2RealizationFormRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Discipline2RealizationFormRelation");
    }
            

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation#getSubjectPassForm()
     */
    public static SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
    {
        return _dslPath.subjectPassForm();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation#getType()
     */
    public static CompensationType.Path<CompensationType> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends Discipline2RealizationFormRelation> extends EntityPath<E>
    {
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private SubjectPassForm.Path<SubjectPassForm> _subjectPassForm;
        private CompensationType.Path<CompensationType> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation#getSubjectPassForm()
     */
        public SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
        {
            if(_subjectPassForm == null )
                _subjectPassForm = new SubjectPassForm.Path<SubjectPassForm>(L_SUBJECT_PASS_FORM, this);
            return _subjectPassForm;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation#getType()
     */
        public CompensationType.Path<CompensationType> type()
        {
            if(_type == null )
                _type = new CompensationType.Path<CompensationType>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return Discipline2RealizationFormRelation.class;
        }

        public String getEntityName()
        {
            return "discipline2RealizationFormRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
