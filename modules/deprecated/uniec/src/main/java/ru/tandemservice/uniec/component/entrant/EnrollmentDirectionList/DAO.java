/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author agolubenko
 * @since 03.07.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("ee");

    static
    {
        //_orderSettings.setOrders(new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_DEVELOP_PERIOD, DevelopPeriod.P_LAST_COURSE}, new OrderDescription("develPeriod", DevelopPeriod.P_LAST_COURSE));
        //_orderSettings.setOrders(EnrollmentDirection.FORMATIVE_ORG_UNIT_SHORT_TITLE_KEY, new OrderDescription("eduLevel", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_SHORT_TITLE}));
        //_orderSettings.setOrders(EnrollmentDirection.TERRITORIAL_ORG_UNIT_SHORT_TITLE_KEY, new OrderDescription("eduLevel", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_SHORT_TITLE}));
        _orderSettings.setOrders(EnrollmentDirection.P_INTERVIEW, new OrderDescription(EnrollmentDirection.P_INTERVIEW));
        _orderSettings.setOrders(EnrollmentDirection.P_MINISTERIAL_PLAN, new OrderDescription(EnrollmentDirection.P_MINISTERIAL_PLAN));
        _orderSettings.setOrders(EnrollmentDirection.P_CONTRACT_PLAN, new OrderDescription(EnrollmentDirection.P_CONTRACT_PLAN));
        _orderSettings.setOrders(EnrollmentDirection.P_TARGET_ADMISSION_PLAN_BUDGET, new OrderDescription(EnrollmentDirection.P_TARGET_ADMISSION_PLAN_BUDGET));
        _orderSettings.setOrders(EnrollmentDirection.educationOrgUnit().developPeriod().title().s(), new OrderDescription(EnrollmentDirection.educationOrgUnit().developPeriod().priority().s()));
    }

    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setLevelTypeList(HierarchyUtil.listHierarchyNodesWithParents(getList(StructureEducationLevels.class), true));
        model.setFormativeOrgUnitsModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitsModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "ee");
        builder.addJoin("ee", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "e");
        builder.addJoin("e", EducationOrgUnit.L_DEVELOP_PERIOD, "develPeriod");
        builder.addJoin("e", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "eduLevelHighSchool");
        builder.addJoin("eduLevelHighSchool", EducationLevelsHighSchool.L_EDUCATION_LEVEL, "eduLevel");

        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        StructureEducationLevels levelType = (StructureEducationLevels) model.getSettings().get("levelType");
        String code = (String) model.getSettings().get("okso");
        String title = CoreStringUtils.escapeLike((String) model.getSettings().get("title"));
        OrgUnit formativeOrgUnit = (OrgUnit) model.getSettings().get("formativeOrgUnit");
        OrgUnit territorialOrgUnit = (OrgUnit) model.getSettings().get("territorialOrgUnit");
        DevelopForm developForm = (DevelopForm) model.getSettings().get("developForm");

        builder.add(MQExpression.eq("ee", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));

        if (levelType != null)
        {
            builder.add(MQExpression.eq("eduLevelHighSchool", EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_LEVEL_TYPE, levelType));
        }

        if (code != null)
        {
            //определяем те элементы которые должны попасть в результат запроса
            Set<Long> ids = new HashSet<Long>();
            for (EducationLevelsHighSchool educationLevelsHighSchool : getList(EducationLevelsHighSchool.class))
            {
                if (code.equals(educationLevelsHighSchool.getEducationLevel().getTitleCodePrefix()))
                {
                    ids.add(educationLevelsHighSchool.getId());
                }
            }

            builder.add(MQExpression.in("eduLevelHighSchool", EducationLevelsHighSchool.P_ID, ids));
        }
        if (title != null)
        {
            builder.add(MQExpression.like("eduLevelHighSchool", EducationLevelsHighSchool.P_TITLE, title));
        }
        if (formativeOrgUnit != null)
        {
            builder.add(MQExpression.eq("e", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, formativeOrgUnit));
        }
        if (territorialOrgUnit != null)
        {
            builder.add(MQExpression.eq("e", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, territorialOrgUnit));
        }
        if (developForm != null)
        {
            builder.add(MQExpression.eq("e", EducationOrgUnit.L_DEVELOP_FORM, developForm));
        }

        DynamicListDataSource<EnrollmentDirection> dataSource = model.getDataSource();
        EntityOrder entityOrder = dataSource.getEntityOrder();

        _orderSettings.applyOrder(builder, entityOrder);
        dataSource.setTotalSize(builder.getResultCount(getSession()));
        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();
        List list = builder.getResultList(getSession(), (int) startRow, (int) countRow);

        List<Object[]> dataList = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e")
        .column(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")))
        .column(DQLFunctions.sum(DQLExpressions.property(ProfileEducationOrgUnit.budgetPlan().fromAlias("p"))))
        .column(DQLFunctions.sum(DQLExpressions.property(ProfileEducationOrgUnit.contractPlan().fromAlias("p"))))
        .joinEntity("e", DQLJoinType.inner, ProfileEducationOrgUnit.class, "p", "p." + ProfileEducationOrgUnit.L_ENROLLMENT_DIRECTION + "=e.id")
        .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), UniBaseUtils.getIdList(list)))
        .group(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")))
        .createStatement(getSession()).list();
        Map<Long, int[]> directionId2profileSum = new HashMap<Long, int[]>();
        for (Object[] row : dataList)
        {
            Long id = (Long) row[0];
            int budgetPlan = row[1] == null ? 0 : ((Number) row[1]).intValue();
            int contractPlan = row[2] == null ? 0 : ((Number) row[2]).intValue();
            directionId2profileSum.put(id, new int[]{budgetPlan, contractPlan});
        }
        model.setDirectionId2profileSum(directionId2profileSum);
        if (model.isShowPossibleProfileColumn())
            model.setIdsWithPossibleProfileSet(getIdsWithPossibleProfileSet(list));

        dataSource.createPage(list);
    }

    @Override
    public void doChangeBudget(Long enrollmentDirectionId, boolean budget)
    {
        EnrollmentDirection direction = getNotNull(EnrollmentDirection.class, enrollmentDirectionId);
        direction.setBudget(budget);
        getSession().update(direction);
    }

    @Override
    public void doChangeContract(Long enrollmentDirectionId, boolean contract)
    {
        EnrollmentDirection direction = getNotNull(EnrollmentDirection.class, enrollmentDirectionId);
        direction.setContract(contract);
        getSession().update(direction);
    }

    @Override
    public void doChangeCrimea(Long enrollmentDirectionId, boolean crimea)
    {
        EnrollmentDirection direction = getNotNull(EnrollmentDirection.class, enrollmentDirectionId);
        direction.setCrimea(crimea);
        getSession().update(direction);
    }

    private Set<Long> getIdsWithPossibleProfileSet(List<EnrollmentDirection> enrollmentDirectionList)
    {
        // быстро загружаем весь граф НПМин
        List<Object[]> list = new DQLSelectBuilder().fromEntity(EducationLevels.class, "e")
        .column(DQLExpressions.property(EducationLevels.id().fromAlias("e")))
        .column(DQLExpressions.property(EducationLevels.parentLevel().id().fromAlias("e")))
        .where(DQLExpressions.isNotNull(DQLExpressions.property(EducationLevels.parentLevel().fromAlias("e"))))
        .createStatement(getSession()).list();
        Map<Long, List<Long>> id2childIds = new HashMap<Long, List<Long>>(list.size() * 2);
        for (Object[] eduLevel : list)
        {
            Long id = (Long) eduLevel[0];
            Long parentId = (Long) eduLevel[1];
            List<Long> childIds = id2childIds.get(parentId);
            if (childIds == null)
                id2childIds.put(parentId, childIds = new ArrayList<Long>());
            childIds.add(id);
        }

        Set<Long> ids = new HashSet<Long>();

        for (EnrollmentDirection enrollmentDirection : enrollmentDirectionList)
        {
            // берем текущее НПМин находим для него все НПМин, которые могут быть профилями к нему (поиск в ширину)
            EducationLevels eduLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

            // НПМин, которые могут быть профилями
            List<Long> possibleEduLevelList = new ArrayList<Long>();
            possibleEduLevelList.add(eduLevel.getId());

            // запускаем поиск в ширину на графе
            Queue<Long> queue = new ArrayDeque<Long>();
            queue.add(eduLevel.getId());
            while (!queue.isEmpty())
            {
                List<Long> childs = id2childIds.get(queue.poll());
                if (childs != null)
                {
                    possibleEduLevelList.addAll(childs);
                    queue.addAll(childs);
                }
            }

            // получаем возможные НПП
            Number count = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
            .where(DQLExpressions.in(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().id().fromAlias("e")), possibleEduLevelList))
            .where(DQLExpressions.notIn(DQLExpressions.property(EducationOrgUnit.id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d")
                .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().id().fromAlias("d")))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), DQLExpressions.value(enrollmentDirection.getEnrollmentCampaign())))
                .buildQuery()))
                .createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

            if (count != null && count.intValue() > 0)
                ids.add(enrollmentDirection.getId());
        }
        return ids;
    }
}
