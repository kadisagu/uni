/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgConfigDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistributionConfig;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 15.05.2012
 */
public class EcgpConfigDTO extends EcgConfigDTO
{
    public EcgpConfigDTO(EcgpDistributionConfig config)
    {
        this(config.getEcgItem(), config.getCompensationType(), config.isSecondHighAdmission());
    }

    public EcgpConfigDTO(EnrollmentDirection ecgItem, CompensationType compensationType, boolean secondHighAdmission)
    {
        super(ecgItem, compensationType, secondHighAdmission);
    }

    @Override
    public EnrollmentDirection getEcgItem()
    {
        return (EnrollmentDirection) super.getEcgItem();
    }
}
