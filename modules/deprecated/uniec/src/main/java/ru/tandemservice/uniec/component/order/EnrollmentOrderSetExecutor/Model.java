/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentOrderSetExecutor;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

/**
 * @author vip_delete
 * @since 08.04.2009
 */
@Input({
        @Bind(key = "orderId", binding = "order.id")
})
public class Model
{
    private EnrollmentOrder _order = new EnrollmentOrder();
    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;

    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrollmentOrder order)
    {
        _order = order;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }
}
