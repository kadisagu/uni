package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневная сводка по заявлениям с указанием конкурса
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DailySummaryWithCMReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport";
    public static final String ENTITY_NAME = "dailySummaryWithCMReport";
    public static final int VERSION_HASH = -1547251194;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";

    private DatabaseFile _content;     // Печатная форма
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _formingDate;     // Дата формирования
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _compensationType;     // Вид возмещения затрат
    private String _studentCategory;     // Категория поступающего
    private String _qualification;     // Квалификация
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _educationLevelHighSchool;     // Направления подготовки (специальности)
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория поступающего.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Направления подготовки (специальности).
     */
    public String getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направления подготовки (специальности).
     */
    public void setEducationLevelHighSchool(String educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DailySummaryWithCMReportGen)
        {
            setContent(((DailySummaryWithCMReport)another).getContent());
            setEnrollmentCampaign(((DailySummaryWithCMReport)another).getEnrollmentCampaign());
            setFormingDate(((DailySummaryWithCMReport)another).getFormingDate());
            setDateFrom(((DailySummaryWithCMReport)another).getDateFrom());
            setDateTo(((DailySummaryWithCMReport)another).getDateTo());
            setCompensationType(((DailySummaryWithCMReport)another).getCompensationType());
            setStudentCategory(((DailySummaryWithCMReport)another).getStudentCategory());
            setQualification(((DailySummaryWithCMReport)another).getQualification());
            setFormativeOrgUnit(((DailySummaryWithCMReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((DailySummaryWithCMReport)another).getTerritorialOrgUnit());
            setEducationLevelHighSchool(((DailySummaryWithCMReport)another).getEducationLevelHighSchool());
            setDevelopForm(((DailySummaryWithCMReport)another).getDevelopForm());
            setDevelopCondition(((DailySummaryWithCMReport)another).getDevelopCondition());
            setDevelopTech(((DailySummaryWithCMReport)another).getDevelopTech());
            setDevelopPeriod(((DailySummaryWithCMReport)another).getDevelopPeriod());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DailySummaryWithCMReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DailySummaryWithCMReport.class;
        }

        public T newInstance()
        {
            return (T) new DailySummaryWithCMReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "formingDate":
                    return obj.getFormingDate();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "qualification":
                    return obj.getQualification();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "formingDate":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategory":
                        return true;
                case "qualification":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "formingDate":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategory":
                    return true;
                case "qualification":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "formingDate":
                    return Date.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return String.class;
                case "studentCategory":
                    return String.class;
                case "qualification":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "educationLevelHighSchool":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DailySummaryWithCMReport> _dslPath = new Path<DailySummaryWithCMReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DailySummaryWithCMReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getEducationLevelHighSchool()
     */
    public static PropertyPath<String> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    public static class Path<E extends DailySummaryWithCMReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _educationLevelHighSchool;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(DailySummaryWithCMReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(DailySummaryWithCMReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(DailySummaryWithCMReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(DailySummaryWithCMReportGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(DailySummaryWithCMReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(DailySummaryWithCMReportGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(DailySummaryWithCMReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(DailySummaryWithCMReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getEducationLevelHighSchool()
     */
        public PropertyPath<String> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new PropertyPath<String>(DailySummaryWithCMReportGen.P_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(DailySummaryWithCMReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(DailySummaryWithCMReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(DailySummaryWithCMReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.DailySummaryWithCMReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(DailySummaryWithCMReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

        public Class getEntityClass()
        {
            return DailySummaryWithCMReport.class;
        }

        public String getEntityName()
        {
            return "dailySummaryWithCMReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
