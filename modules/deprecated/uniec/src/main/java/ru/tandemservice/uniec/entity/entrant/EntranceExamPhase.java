package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uniec.entity.entrant.gen.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @see ru.tandemservice.uniec.entity.entrant.gen.EntranceExamPhaseGen
 */
public class EntranceExamPhase extends EntranceExamPhaseGen
{

    /**
     *
     * @return [Дата сдачи], [Форма сдачи], [Форма освоения], [Вид возмещения затрат]
     */
    public String getFullDescription()
    {
        String date = DateFormatter.DEFAULT_DATE_FORMATTER.format(getPassDate());
        String passForm = getSetting().getSubjectPassForm().getShortTitle();
        String compensationType = getSetting().getCompensationType().getShortTitle();
        String developForm = getSetting().getDevelopForm().getShortTitle();

        return date + ", " + passForm + ", " + developForm + ", " + compensationType;
    }


    /**
     * Список возможных этапов вступительных испытаний для дисциплины для сдачи, отсортированный по дате этапа.
     */
    public static DQLSelectBuilder getSelectBuilder4ExamPassDiscipline(ExamPassDiscipline passDiscipline)
    {
        IEntrant iEntrant = passDiscipline.getEntrantExamList().getEntrant();

        String alias = "phase";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EntranceExamPhase.class, alias)
                .column(property(alias))
                .where(eq(property(alias, EntranceExamPhase.setting().discipline()), value(passDiscipline.getEnrollmentCampaignDiscipline())))
                .where(eq(property(alias, EntranceExamPhase.setting().subjectPassForm()), value(passDiscipline.getSubjectPassForm())))
                .order(property(alias, EntranceExamPhase.passDate()));

        IDQLExpression entrantExpression = null;
        if (iEntrant instanceof Entrant)
            entrantExpression = eq(property("dir", RequestedEnrollmentDirection.entrantRequest().entrant()), value((Entrant) iEntrant));
        if (iEntrant instanceof EntrantRequest)
            entrantExpression = eq(property("dir", RequestedEnrollmentDirection.entrantRequest()), value((EntrantRequest) iEntrant));

        builder.where(existsByExpr(RequestedEnrollmentDirection.class, "dir", and(
                entrantExpression,
                eq(property("dir", RequestedEnrollmentDirection.compensationType()), property(alias, EntranceExamPhase.setting().compensationType())),
                eq(property("dir", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm()),
                   property(alias, EntranceExamPhase.setting().developForm()))
        )));

        return builder;
    }
}