package ru.tandemservice.uniec.ws.enrollmentorder;

import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 25.02.2011
 */
public class EnrollmentOrderServiceDao extends UniBaseDao implements IEnrollmentOrderServiceDao
{
    @Override
    public EnrollmentOrderEnvironmentNode getEnrollmentOrderEnvironmentData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeCode, List<String> orderTypeCodes, List<String> formativeOrgUnitIds, List<String> developFormCodes, Boolean withPrintData)
    {
        EnrollmentCampaign enrollmentCampaign = get(EnrollmentCampaign.class, EnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);

        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        // выбираем приказы, пустых приказов быть не может, так как требуются только приказы в состоянии "согласовано" и "проведено",
        // а в таких состояниях всегда есть параграфы и выписки -> можно делать селект сразу из выписок и inner join к параграфам и приказам,
        // чтобы можно было накладывать фильтры по формирующим подразделениям и формам освоения по полям выписок и приказов

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e").column("e")
        .joinEntity("e", DQLJoinType.inner, EnrollmentParagraph.class, "p", DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.paragraph().id().fromAlias("e")), DQLExpressions.property(EnrollmentParagraph.id().fromAlias("p"))))
        .joinEntity("p", DQLJoinType.inner, EnrollmentOrder.class, "o", DQLExpressions.eq(DQLExpressions.property(EnrollmentParagraph.order().id().fromAlias("p")), DQLExpressions.property(EnrollmentOrder.id().fromAlias("o"))))
        .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrder.enrollmentCampaign().fromAlias("o")), DQLExpressions.value(enrollmentCampaign)))
        .where(DQLExpressions.in(DQLExpressions.property(EnrollmentOrder.state().code().fromAlias("o")), Arrays.asList(UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
        .order(DQLExpressions.property(EnrollmentOrder.commitDate().fromAlias("o")))
        .order(DQLExpressions.property(EnrollmentOrder.id().fromAlias("o")))
        .order(DQLExpressions.property(EnrollmentParagraph.number().fromAlias("p")))
        .order(DQLExpressions.property(EnrollmentExtract.number().fromAlias("e")));

        if (dateFrom != null)
            dql.where(DQLExpressions.ge(DQLExpressions.property(EnrollmentOrder.commitDate().fromAlias("o")), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(dateFrom))));

        if (dateTo != null)
        {
            dateTo = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(dateTo, Calendar.DAY_OF_YEAR, 1));
            dql.where(DQLExpressions.lt(DQLExpressions.property(EnrollmentOrder.commitDate().fromAlias("o")), DQLExpressions.valueDate(dateTo)));
        }

        if (compensationTypeCode != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrder.type().compensationType().code().fromAlias("o")), DQLExpressions.value(compensationTypeCode)));

        if (orderTypeCodes != null && !orderTypeCodes.isEmpty())
            dql.where(DQLExpressions.in(DQLExpressions.property(EnrollmentOrder.type().code().fromAlias("o")), orderTypeCodes));

        if (formativeOrgUnitIds != null && !formativeOrgUnitIds.isEmpty())
        {
            List<Long> ids = new ArrayList<>();
            for (String stringId : formativeOrgUnitIds) ids.add(Long.parseLong(stringId));
            dql.where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().educationOrgUnit().formativeOrgUnit().id().fromAlias("e")), ids));
        }

        if (developFormCodes != null && !developFormCodes.isEmpty())
            dql.where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().educationOrgUnit().developForm().code().fromAlias("e")), developFormCodes));

        List<EnrollmentExtract> enrollmentExtractList = dql.createStatement(getSession()).list();

        return getEnrollmentOrderEnvironmentData(enrollmentCampaign, enrollmentExtractList, withPrintData);
    }

    @Override
    public EnrollmentOrderEnvironmentNode getEnrollmentOrderEnvironmentData(EnrollmentCampaign enrollmentCampaign, List<EnrollmentOrder> enrollmentOrderList)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e").column("e")
        .joinEntity("e", DQLJoinType.inner, EnrollmentParagraph.class, "p", DQLExpressions.eq(DQLExpressions.property(EnrollmentExtract.paragraph().id().fromAlias("e")), DQLExpressions.property(EnrollmentParagraph.id().fromAlias("p"))))
        .joinEntity("p", DQLJoinType.inner, EnrollmentOrder.class, "o", DQLExpressions.eq(DQLExpressions.property(EnrollmentParagraph.order().id().fromAlias("p")), DQLExpressions.property(EnrollmentOrder.id().fromAlias("o"))))
        .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrder.enrollmentCampaign().fromAlias("o")), DQLExpressions.value(enrollmentCampaign)))
        .where(DQLExpressions.in(DQLExpressions.property(EnrollmentOrder.state().code().fromAlias("o")), Arrays.asList(UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
        .order(DQLExpressions.property(EnrollmentOrder.commitDate().fromAlias("o")))
        .order(DQLExpressions.property(EnrollmentOrder.id().fromAlias("o")))
        .order(DQLExpressions.property(EnrollmentParagraph.number().fromAlias("p")))
        .order(DQLExpressions.property(EnrollmentExtract.number().fromAlias("e")));

        dql.where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.paragraph().order().fromAlias("e")), enrollmentOrderList));

        List<EnrollmentExtract> enrollmentExtractList = dql.createStatement(getSession()).list();

        return getEnrollmentOrderEnvironmentData(enrollmentCampaign, enrollmentExtractList, true);
    }

    private EnrollmentOrderEnvironmentNode getEnrollmentOrderEnvironmentData(EnrollmentCampaign enrollmentCampaign, List<EnrollmentExtract> enrollmentExtractList, boolean withPrintData)
    {
        EnrollmentOrderEnvironmentNode envNode = new EnrollmentOrderEnvironmentNode();

        envNode.enrollmentCampaignTitle = enrollmentCampaign.getTitle();
        envNode.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        // сохраняем нужные справочники

        for (CompensationType row : getList(CompensationType.class, CompensationType.P_CODE))
            envNode.compensationType.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (EntrantEnrollmentOrderType row : getList(EntrantEnrollmentOrderType.class, EntrantEnrollmentOrderType.P_CODE))
            envNode.enrollmentOrderType.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopForm row : getList(DevelopForm.class, DevelopForm.P_CODE))
            envNode.developForm.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopCondition row : getList(DevelopCondition.class, DevelopCondition.P_CODE))
            envNode.developCondition.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopTech row : getList(DevelopTech.class, DevelopTech.P_CODE))
            envNode.developTech.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (DevelopPeriod row : getList(DevelopPeriod.class, DevelopPeriod.P_CODE))
            envNode.developPeriod.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getTitle(), row.getCode()));

        for (Qualifications row : getList(Qualifications.class, Qualifications.P_CODE))
            envNode.qualification.row.add(new EnrollmentOrderEnvironmentNode.Row(row.getShortTitle(), row.getTitle(), row.getCode()));

        for (StructureEducationLevels row : getList(StructureEducationLevels.class, StructureEducationLevels.P_CODE))
            envNode.structureEducationLevel.row.add(new EnrollmentOrderEnvironmentNode.StructureEducationLevelNode.StructureEducationLevelRow(row.getTitle(), row.getCode(), row.getParent() == null ? null : row.getParent().getCode()));

        // создаем структуру данных: приказы в порядке даты приказа -> параграфы в порядке номеров -> выписки в порядке номеров
        final Map<EnrollmentOrder, Map<EnrollmentParagraph, List<EnrollmentExtract>>> map = new LinkedHashMap<>();

        for (EnrollmentExtract extract : enrollmentExtractList)
        {
            EnrollmentParagraph paragraph = (EnrollmentParagraph) extract.getParagraph();
            EnrollmentOrder order = (EnrollmentOrder) paragraph.getOrder();

            Map<EnrollmentParagraph, List<EnrollmentExtract>> subMap = map.get(order);
            if (subMap == null)
                map.put(order, subMap = new LinkedHashMap<>());
            List<EnrollmentExtract> list = subMap.get(paragraph);
            if (list == null)
                subMap.put(paragraph, list = new ArrayList<>());
            list.add(extract);
        }

        final Map<EnrollmentOrder, byte[]> orderTextMap = new HashMap<>();

        if (withPrintData)
        {
            BatchUtils.execute(map.keySet(), 1000, new BatchUtils.Action<EnrollmentOrder>()
                {
                @Override
                public void execute(Collection<EnrollmentOrder> elements)
                {
                    ScrollableResults results = new DQLSelectBuilder().fromEntity(EnrollmentOrderTextRelation.class, "r")
                    .column("r")
                    .where(DQLExpressions.in(DQLExpressions.property(EnrollmentOrderTextRelation.order().fromAlias("r")), map.keySet()))
                    .createStatement(new DQLExecutionContext(getSession())).scroll(ScrollMode.FORWARD_ONLY);

                    while (results.next())
                    {
                        EnrollmentOrderTextRelation relation = (EnrollmentOrderTextRelation) results.get()[0];
                        orderTextMap.put(relation.getOrder(), relation.getText());
                    }
                }
                });
        }

        Set<Long> usedOrgUnitIds = new HashSet<>();
        Set<Long> usedEducationLevelHighSchoolIds = new HashSet<>();
        Set<OrgUnitType> usedOrgUnitTypes = new HashSet<>();

        for (Map.Entry<EnrollmentOrder, Map<EnrollmentParagraph, List<EnrollmentExtract>>> orderMapEntry : map.entrySet())
        {
            EnrollmentOrder order = orderMapEntry.getKey();
            EnrollmentOrderEnvironmentNode.EnrollmentOrderNode.EnrollmentOrderRow orderRow = new EnrollmentOrderEnvironmentNode.EnrollmentOrderNode.EnrollmentOrderRow();

            orderRow.id = order.getId();
            orderRow.text = orderTextMap.get(order);
            orderRow.enrollmentOrderType = order.getType().getCode();
            orderRow.date = order.getCommitDate();
            orderRow.number = order.getNumber();

            for (Map.Entry<EnrollmentParagraph, List<EnrollmentExtract>> paragraphListEntry : orderMapEntry.getValue().entrySet())
            {
                EnrollmentParagraph paragraph = paragraphListEntry.getKey();
                EnrollmentOrderEnvironmentNode.EnrollmentParagraphNode.EnrollmentParagraphRow paragraphRow = new EnrollmentOrderEnvironmentNode.EnrollmentParagraphNode.EnrollmentParagraphRow();

                EducationOrgUnit ou = paragraphListEntry.getValue().get(0).getEntity().getEducationOrgUnit();

                if (usedOrgUnitIds.add(ou.getFormativeOrgUnit().getId()))
                {
                    OrgUnit orgUnit = ou.getFormativeOrgUnit();
                    usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                    String settlemenetTitle = null;
                    if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
                        settlemenetTitle = orgUnit.getAddress().getSettlement().getTitle();
                    envNode.orgUnit.row.add(new EnrollmentOrderEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlemenetTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle()));
                }

                if (usedOrgUnitIds.add(ou.getTerritorialOrgUnit().getId()))
                {
                    OrgUnit orgUnit = ou.getTerritorialOrgUnit();
                    usedOrgUnitTypes.add(orgUnit.getOrgUnitType());
                    String settlemenetTitle = null;
                    if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
                        settlemenetTitle = orgUnit.getAddress().getSettlement().getTitle();
                    envNode.orgUnit.row.add(new EnrollmentOrderEnvironmentNode.OrgUnitNode.OrgUnitRow(orgUnit.getShortTitle(), orgUnit.getTitle(), orgUnit.getId().toString(), orgUnit.getOrgUnitType().getCode(), orgUnit.getNominativeCaseTitle(), settlemenetTitle, orgUnit.getTerritorialTitle(), orgUnit.getTerritorialShortTitle()));
                }

                if (usedEducationLevelHighSchoolIds.add(ou.getEducationLevelHighSchool().getId()))
                {
                    EducationLevelsHighSchool hs = ou.getEducationLevelHighSchool();
                    envNode.educationLevelHighSchool.row.add(new EnrollmentOrderEnvironmentNode.EducationLevelHighSchoolNode.EducationLevelHighSchoolRow(hs.getShortTitle(), hs.getTitle(), hs.getId().toString(), hs.getEducationLevel().getTitleCodePrefix(), hs.getEducationLevel().getQualification().getCode()));
                }

                if (paragraph.getGroupManager() != null)
                    paragraphRow.groupManager = paragraph.getGroupManager().getEntity().getId().toString();
                paragraphRow.developPeriod = ou.getDevelopPeriod().getCode();
                paragraphRow.developTech = ou.getDevelopTech().getCode();
                paragraphRow.developCondition = ou.getDevelopCondition().getCode();
                paragraphRow.developForm = ou.getDevelopForm().getCode();
                paragraphRow.structureEducationLevel = ou.getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode();
                paragraphRow.educationLevelHighSchool = ou.getEducationLevelHighSchool().getId().toString();
                if (ou.getTerritorialOrgUnit() != null)
                    paragraphRow.territorialOrgUnit = ou.getTerritorialOrgUnit().getId().toString();
                paragraphRow.formativeOrgUnit = ou.getFormativeOrgUnit().getId().toString();
                paragraphRow.educationOrgUnit = ou.getId().toString();

                for (EnrollmentExtract extract : paragraphListEntry.getValue())
                {
                    Entrant entrant = extract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    IdentityCard card = entrant.getPerson().getIdentityCard();
                    paragraphRow.extract.row.add(new EnrollmentOrderEnvironmentNode.EnrollmentExtractNode.EnrollmentExtractRow(card.getMiddleName(), card.getFirstName(), card.getLastName(), entrant.getId().toString()));
                }

                orderRow.paragraph.row.add(paragraphRow);
            }

            envNode.enrollmentOrder.row.add(orderRow);
        }

        List<OrgUnitType> orgUnitTypeList = new ArrayList<>(usedOrgUnitTypes);
        Collections.sort(orgUnitTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        for (OrgUnitType type : orgUnitTypeList)
            envNode.orgUnitType.row.add(new EnrollmentOrderEnvironmentNode.Row(type.getTitle(), type.getCode()));

        Collections.sort(envNode.orgUnit.row);
        Collections.sort(envNode.educationLevelHighSchool.row);

        return envNode;
    }
}
