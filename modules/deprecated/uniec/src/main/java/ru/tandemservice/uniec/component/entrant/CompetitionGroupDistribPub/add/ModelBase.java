package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class ModelBase {

    public static class BooleanFormatter implements IFormatter<Boolean> {
        public static final BooleanFormatter INSTANCE = new BooleanFormatter();

        @Override public String format(final Boolean source) {
            return Boolean.TRUE.equals(source) ? "Да" : "";
        }
    }

    public static class CopiesFormatter implements IFormatter<Boolean> {
        public static final CopiesFormatter INSTANCE = new CopiesFormatter();

        @Override public String format(final Boolean source) {
            return Boolean.TRUE.equals(source) ? "оригиналы" : "копии";
        }
    }

    public static class Row extends IdentifiableWrapper<Entrant> {
        private static final long serialVersionUID = 1L;

        private final IEnrollmentCompetitionGroupDAO.IEntrantRateRow row;
        public IEnrollmentCompetitionGroupDAO.IEntrantRateRow getRow() { return this.row; }

        public Row(final IEnrollmentCompetitionGroupDAO.IEntrantRateRow r) {
            super(r.getEntrant().getId(), r.getEntrant().getPerson().getFullFio());
            this.row = r;
        }

        private boolean preEnrolled = false;
        public boolean isPreEnrolled() { return this.preEnrolled; }
        public void setPreEnrolled(final boolean preEnrolled) { this.preEnrolled = preEnrolled; }

        private boolean checked = true;
        public boolean isChecked() { return (this.checked || this.isPreEnrolled()); }
        public void setChecked(final boolean checked) { this.checked = checked; }

        private RequestedEnrollmentDirection direction;
        public RequestedEnrollmentDirection getDirection() { return this.direction; }
        public void setDirection(final RequestedEnrollmentDirection direction) { this.direction = direction; }

        @SuppressWarnings("unchecked")
        public void filter(final Map<Long, MutableInt> dir2Quota) {
            this.directions = new ArrayList<>(CollectionUtils.select(this.row.getDirections(), object -> {
                final MutableInt i = dir2Quota.get(((RequestedEnrollmentDirection) object).getEnrollmentDirection().getId());
                return ((null != i) && (i.intValue() > 0));
            }));
        }

        List<RequestedEnrollmentDirection> directions = Collections.emptyList();
        public List<RequestedEnrollmentDirection> getDirections() {
            if (this.isPreEnrolled()) {
                return Collections.singletonList(getDirection());
            }
            return this.directions;
        }

        public String getTotalMark() {
            return (null == this.row.getTotalMark() ? "" : String.format("%.0f", this.row.getTotalMark()));
        }

        public String getProfileMark() {
            return (null == this.row.getProfileMark() ? "" : String.format("%.0f", this.row.getProfileMark()));
        }

        public String getCertificateAverageMark() {
            return (null == this.row.getCertificateAverageMark() ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(this.row.getCertificateAverageMark()));
        }

        public boolean isGraduatedProfileEduInstitution() {
            return (this.row.getGraduatedProfileEduInstitutionPriority() < Integer.MAX_VALUE);
        }

        public boolean isOriginalDocumentHandedIn() {
            return (this.row.getOriginalDocumentHandedInPriority() < Integer.MAX_VALUE);
        }

        public String getDirectionsPriorities() {
            return UniStringUtils.join(
                    this.row.getDirections(),
                    RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnitGen.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchoolGen.P_SHORT_TITLE,
                    ", "
            );
        }

        public boolean isTargetAdmission() {
            return (null != this.row.getTargetAdmissionKind());
        }

        public String getCompetitionKind() {
            if (isTargetAdmission()) {
                return EnrollmentCompetitionKind.TARGET_ADMISSION;
            }

            return this.row.getCompetitionKind().getTitle();
        }

        public String getBenefitsExistence() {
            if (isTargetAdmission()) {
                return EnrollmentCompetitionKind.TARGET_ADMISSION;
            }

            final CompetitionKind competitionKind = this.row.getCompetitionKind();
            if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionKind.getCode())) {
                return "без вступительных испытаний";
            } else if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(competitionKind.getCode())) {
                return "льготы";
            }
            return ""; // this.row.getCompetitionKind().getTitle();
        }
    }

    private List<Row> entrantRateRows = Collections.emptyList();
    public List<Row> getEntrantRateRows() { return this.entrantRateRows; }
    public void setEntrantRateRows(final List<Row> entrantRateRows) { this.entrantRateRows = entrantRateRows; }

    private final AbstractListDataSource<Row> entrantRateRowsDataSource = new AbstractListDataSource<Row>() {
        @Override public List<Row> getEntityList() { return ModelBase.this.entrantRateRows; }
        @Override public long getTotalSize() { return this.getEntityList().size(); }
        @Override public long getCountRow() { return this.getTotalSize(); }
        @Override public long getStartRow() { return 0; }
        @Override public void onRefresh() {}
        @Override public void onChangeOrder() {}
        @Override public AbstractListDataSource<Row> getCopy() { return this; }
    };
    public AbstractListDataSource<Row> getEntrantRateRowsDataSource() { return this.entrantRateRowsDataSource; }
}
