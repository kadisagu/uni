/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.EnrollmentCampaignAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EnrollmentCampaignType;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.EntrantExamListsFormingFeature;
import ru.tandemservice.uniec.entity.catalog.codes.EnrollmentCampaignTypeCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.CompetitionGroupRestriction;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.EnrollmentDirectionRestriction;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

/**
 * @author vip_delete
 * @since 11.02.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(new EducationYearModel());

        // Прием на бюджетные места и по договору (настройку изменять нельзя)
        model.setEnrollmentPerCompensationTypeDisabled(false);
        model.setEnrollmentPerCompensationTypeList(Arrays.asList(
                new IdentifiableWrapper(Model.ENROLLMENT_PER_COMPENSATION_TYPE_DIFFERENCE, "Отличается"),
                new IdentifiableWrapper(Model.ENROLLMENT_PER_COMPENSATION_TYPE_NO_DIFFERENCE, "Не отличается")
        ));

        // Формирование экзаменационных листов
        model.setEntrantExamListFormingList(Arrays.asList(
                new IdentifiableWrapper(Model.ENTRANT_EXAM_LIST_FORMING_FOR_ENTRANT, "Для абитуриента в целом"),
                new IdentifiableWrapper(Model.ENTRANT_EXAM_LIST_FORMING_FOR_REQUEST, "Для каждого заявления абитуриента")
        ));

        // Конкурсные группы
        model.setCompetitionGroupList(Arrays.asList(
                new IdentifiableWrapper(Model.COMPETITION_GROUP_NO_USE, "Не использовать"),
                new IdentifiableWrapper(Model.COMPETITION_GROUP_USE_EXAM_SET_EQUALS, "Использовать, в группу входят только направления с одинаковым набором вступительных испытаний")
                //new IdentifiableWrapper(Model.COMPETITION_GROUP_USE_EXAM_SET_NOT_EQUALS, "Использовать, в группу могут входить направления с разными наборами вступительных испытаний")
        ));

        // Число выбираемых направлений приема по внеконкурсному приему
        model.setDirectionForOutOfCompetitionList(Arrays.asList(
                new IdentifiableWrapper(Model.DIRECTION_FOR_OUT_OF_COMPETITION_ONE, "Одно"),
                new IdentifiableWrapper(Model.DIRECTION_FOR_OUT_OF_COMPETITION_MANY, "Без ограничений")
        ));

        // Число выбираемых направлений подготовки (специальностей) по целевому приему
        model.setDirectionForTargetAdmissionList(Arrays.asList(
                new IdentifiableWrapper(Model.DIRECTION_FOR_TARGET_ADMISSION_ONE, "Одно"),
                new IdentifiableWrapper(Model.DIRECTION_FOR_TARGET_ADMISSION_MANY, "Без ограничений")
        ));

        // Присваивать регистрационный номер заявления
        model.setNumberOnRequestList(Arrays.asList(
                new IdentifiableWrapper(Model.NUMBER_ON_REQUEST_AUTO, "Автоматически"),
                new IdentifiableWrapper(Model.NUMBER_ON_REQUEST_MANUAL, "Вручную")
        ));

        // Присваивать регистрационный номер на направлении подготовки (специальности)
        model.setNumberOnDirectionList(Arrays.asList(
                new IdentifiableWrapper(Model.NUMBER_ON_DIRECTION_AUTO, "Автоматически"),
                new IdentifiableWrapper(Model.NUMBER_ON_DIRECTION_MANUAL, "Вручную")
        ));

        // Выбор направлений абитуриентом
        model.setDirectionSelectionList(Arrays.asList(
                new IdentifiableWrapper(Model.DIRECTION_SELECTION_ONE_COMPENSATION_TYPE, "Только с одним видом возмещения затрат"),
                new IdentifiableWrapper(Model.DIRECTION_SELECTION_MANY_COMPENSATION_TYPE_NO_CONTEST, "С разными видами возмещения затрат, абитуриент не участвует в конкурсе на контракт"),
                new IdentifiableWrapper(Model.DIRECTION_SELECTION_MANY_COMPENSATION_TYPE_CONTEST, "С разными видами возмещения затрат, абитуриент может участвовать в конкурсе на контракт")
        ));

        // Мастер регистрации абитуриента
        model.setEntrantRegistrationWizardList(Arrays.asList(
                new IdentifiableWrapper(Model.ENTRANT_REGISTRATION_WIZARD_REGULAR, "Обычный"),
                new IdentifiableWrapper(Model.ENTRANT_REGISTRATION_WIZARD_SEARCH, "С поиском"),
                new IdentifiableWrapper(Model.ENTRANT_REGISTRATION_WIZARD_ONLINE, "С поиском, в том числе по онлайн-абитуриентам")
        ));

        // Наборы вступительных испытаний для категорий поступающих
        model.setCompetitionGroupExamSetList(Arrays.asList(
                new IdentifiableWrapper(Model.COMPETITION_GROUP_EXAM_SET_EQUALS_FOR_ALL_CATEGORY, "Одинаковые для всех категорий"),
                new IdentifiableWrapper(Model.COMPETITION_GROUP_EXAM_SET_NO_EQUALS_FOR_ALL_CATEGORY, "Разные для разных категорий")
        ));

        // Формирование дисциплин для сдачи при наличии свидетельства ЕГЭ
        model.setPassDisciplineFormingList(Arrays.asList(
                new IdentifiableWrapper(Model.PASS_DISCIPLINE_FORMING_NO_FORMING, "Не формировать дисциплину для сдачи при наличии свидетельства ЕГЭ"),
                new IdentifiableWrapper(Model.PASS_DISCIPLINE_FORMING_YES_FORMING, "Всегда формировать дисциплину для сдачи")
        ));

        // Выбор направлений абитуриентом (по свидетельству ЕГЭ)
        model.setDirectionSelectionByStateExamList(Arrays.asList(
                new IdentifiableWrapper(Model.DIRECTION_SELECTION_BY_STATE_EXAM_NEED_COVERING, "Направление приема должно покрываться свидетельством ЕГЭ"),
                new IdentifiableWrapper(Model.DIRECTION_SELECTION_BY_STATE_EXAM_NO_NEED_COVERING, "Направление приема может не покрываться свидетельством ЕГЭ")
        ));

        // Регистрация свидетельств ЕГЭ по умолчанию
        model.setStateExamRegistrationList(Arrays.asList(
                new IdentifiableWrapper(Model.STATE_EXAM_REGISTRATION_CHECKED, "Зачтенными"),
                new IdentifiableWrapper(Model.STATE_EXAM_REGISTRATION_NO_CHECKED, "Не зачтенными")
        ));

        // Номер для свидетельств ЕГЭ 1-ой волны
        model.setStateExamWave1NumberList(Arrays.asList(
                new IdentifiableWrapper(Model.STATE_EXAM_WAVE1_NUMBER_REQUIRED, "Обязателен"),
                new IdentifiableWrapper(Model.STATE_EXAM_WAVE1_NUMBER_NO_REQUIRED, "Необязателен")
        ));

        // Зачтение диплома олимпиады (настройку изменять нельзя)
        model.setOlympiadDiplomaRuleDisabled(false);
        model.setOlympiadDiplomaRuleList(Arrays.asList(
                new IdentifiableWrapper(Model.OLYMPIAD_DIPLOMA_FOR_DIRECTION, "За дисциплины по одному выбранному направлению приема"),
                new IdentifiableWrapper(Model.OLYMPIAD_DIPLOMA_FOR_ALL_DIRECTIONS, "За дисциплины по всем выбранным направлениям приема")
        ));

        // Учитывать приоритеты вступительных испытаний
        model.setPrioritiesAdmissionTestRuleList(Arrays.asList(
                new IdentifiableWrapper(Model.PRIORITIES_ADMISSION_TESTS_CONSIDER, "Да"),
                new IdentifiableWrapper(Model.PRIORITIES_ADMISSION_TESTS_IGNORE, "Нет")
        ));

        // Формирование экзаменационных групп
        model.setFormingExamGroupList(Arrays.asList(
                new IdentifiableWrapper(Model.FORMING_EXAM_GROUP_AUTO, "Автоматически"),
                new IdentifiableWrapper(Model.FORMING_EXAM_GROUP_MANUAL, "Вручную")
        ));

        // Прием оригиналов документов
        model.setAcceptOriginalDocumentList(Arrays.asList(
                new IdentifiableWrapper(Model.ACCEPT_ORIGINAL_DOCUMENT_FOR_DIRECTION, "На выбранное направление приема"),
                new IdentifiableWrapper(Model.ACCEPT_ORIGINAL_DOCUMENT_FOR_REQUEST, "В рамках заявления")
        ));

        // Оригиналы документов при зачислении
        model.setOriginalDocumentList(Arrays.asList(
                new IdentifiableWrapper(Model.ORIGINAL_DOCUMENTS_NEED, "Обязательны"),
                new IdentifiableWrapper(Model.ORIGINAL_DOCUMENTS_NO_NEED, "Необязательны")
        ));

        // Формирование параграфа приказа о зачислении (по умолчанию)
        model.setEnrollParagraphRuleList(Arrays.asList(
                new IdentifiableWrapper(Model.ENROLL_PARAGRAPH_FORMING_RULE_1, "На направление приема"),
                new IdentifiableWrapper(Model.ENROLL_PARAGRAPH_FORMING_RULE_2, "На направление подготовки ОУ, формирующее подр. и форму освоения")
        ));

        model.setIndividualProgressList(Arrays.asList(
                new IdentifiableWrapper(Model.INDIVIDUAL_PROGRESS_CONSIDER_YES, "Да"),
                new IdentifiableWrapper(Model.INDIVIDUAL_PROGRESS_CONSIDER_NO, "Нет")
        ));

        model.setEnrollmentCampaignTypes(getList(EnrollmentCampaignType.class));
        model.setQualificationList(getList(Qualifications.class));
        model.setRequiredAgreementModel(TwinComboDataSourceHandler.getCustomSelectModel("Обязательно", "Необязательно"));

        if (model.getEnrollmentCampaignId() != null)
        {
            // форма редактирования
            EnrollmentCampaign enrollmentCampaign = getNotNull(EnrollmentCampaign.class, model.getEnrollmentCampaignId());
            model.setEnrollmentCampaign(enrollmentCampaign);

            // О периодах - сейчас ограничимся одним, потом доработаем, чтобы можно было несколько периодов задавать для приемной кампании.
            List<EnrollmentCampaignPeriod> periodList = getList(EnrollmentCampaignPeriod.class, EnrollmentCampaignPeriod.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign);
            if (periodList.size() != 1)
                throw new ApplicationException("Пока возможен только один период приемной кампании.");
            model.setPeriod(periodList.get(0));

            // prepare
            final boolean hasExamPassDisciplines = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d")
                    .add(MQExpression.eq("d", ExamPassDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                    .getResultCount(getSession()) > 0;
            final boolean hasEntrantRequests = new MQBuilder(EntrantRequest.ENTITY_CLASS, "r")
                    .add(MQExpression.eq("r", EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                    .getResultCount(getSession()) > 0;
            final boolean hasCompetitionGroups = existsEntity(CompetitionGroup.class, CompetitionGroup.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign);
            final boolean hasEntranceDisciplines = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "d")
                    .add(MQExpression.eq("d", EntranceDiscipline.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                    .getResultCount(getSession()) > 0;
            List<EnrollmentCampaign> list = getList(EnrollmentCampaign.class, EnrollmentCampaign.P_ID);
            final boolean isLastEnrollmentCampaign = list.get(list.size() - 1).equals(enrollmentCampaign);

            model.setMaxEnrollmentDirectionDisabled(hasEntrantRequests);
            model.setMaxMinisterialDirectionDisabled(hasEntrantRequests);
            model.setOlympiadDiplomaRuleDisabled(hasEntrantRequests);

            // Прием на бюджетные места и по договору
            model.setEnrollmentPerCompensationType(model.getEnrollmentPerCompensationTypeList().get(enrollmentCampaign.isEnrollmentPerCompTypeDiff() ? 0 : 1));

            // Формирование экзаменационных листов
            model.setEntrantExamListForming(model.getEntrantExamListFormingList().get(enrollmentCampaign.getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT) ? 0 : 1));
            model.setEntrantExamListFormingDisabled(hasExamPassDisciplines);

            // Конкурсные группы
            int index = (!enrollmentCampaign.isUseCompetitionGroup()) ? 0 : (enrollmentCampaign.isCompetitionGroupExamSetEqual() ? 1 : 2);
            if (index == 2)
                model.setCompetitionGroup(new IdentifiableWrapper(2L, ""));
                //model.setCompetitionGroup(null);
            else
                model.setCompetitionGroup(model.getCompetitionGroupList().get(index));
            model.setCompetitionGroupDisabled(hasEntrantRequests || hasCompetitionGroups);

            // Ограничения
            {
                List<EnrollmentDirectionRestriction> itemList = getList(EnrollmentDirectionRestriction.class, EnrollmentDirectionRestriction.enrollmentCampaign(), enrollmentCampaign, EnrollmentDirectionRestriction.P_ID);
                List<EnrollmentDirectionRestriction> dtoList = new ArrayList<>();
                long i = -1;
                for (EnrollmentDirectionRestriction item : itemList)
                {
                    EnrollmentDirectionRestriction dto = new EnrollmentDirectionRestriction();
                    dto.setId(i--);
                    dto.update(item);
                    dtoList.add(dto);
                }
                model.setDirectionRestrictionList(dtoList);
            }
            {
                List<CompetitionGroupRestriction> itemList = getList(CompetitionGroupRestriction.class, CompetitionGroupRestriction.enrollmentCampaign(), enrollmentCampaign, CompetitionGroupRestriction.P_ID);
                List<CompetitionGroupRestriction> dtoList = new ArrayList<>();
                long i = -1;
                for (CompetitionGroupRestriction item : itemList)
                {
                    CompetitionGroupRestriction dto = new CompetitionGroupRestriction();
                    dto.setId(i--);
                    dto.update(item);
                    dtoList.add(dto);
                }
                model.setCompetitionGroupRestrictionList(dtoList);
            }

            // Мастер регистрации абитуриента
            model.setEntrantRegistrationWizard(model.getEntrantRegistrationWizardList().get(enrollmentCampaign.isSearchBeforeEntrantRegistration() ? enrollmentCampaign.isUseOnlineEntrantRegistration() ? 2 : 1 : 0));
            model.setEntrantRegistrationWizardDisabled(!isLastEnrollmentCampaign);

            // Число выбираемых направлений приема по внеконкурсному приему
            model.setDirectionForOutOfCompetition(model.getDirectionForOutOfCompetitionList().get(enrollmentCampaign.isOneDirectionForOutOfCompetition() ? 0 : 1));

            // Число выбираемых направлений подготовки (специальностей) по целевому приему
            model.setDirectionForTargetAdmission(model.getDirectionForTargetAdmissionList().get(enrollmentCampaign.isOneDirectionForTargetAdmission() ? 0 : 1));

            // Присваивать регистрационный номер заявления
            model.setNumberOnRequest(model.getNumberOnRequestList().get(enrollmentCampaign.isNumberOnRequestManual() ? 1 : 0));
            model.setNumberOnRequestDisabled(false);

            // Присваивать регистрационный номер на направлении подготовки (специальности)
            model.setNumberOnDirection(model.getNumberOnDirectionList().get(enrollmentCampaign.isNumberOnDirectionManual() ? 1 : 0));
            model.setNumberOnDirectionDisabled(false);

            // Выбор направлений абитуриентом
            model.setDirectionSelection(model.getDirectionSelectionList().get(!enrollmentCampaign.isDirectionPerCompTypeDiff() ? 0 : (!enrollmentCampaign.isContractAutoCompetition() ? 1 : 2)));
            model.setDirectionSelectionDisabled(false);

            // Наборы вступительных испытаний для категорий поступающих
            model.setCompetitionGroupExamSet(model.getCompetitionGroupExamSetList().get(enrollmentCampaign.isExamSetDiff() ? 1 : 0));
            model.setCompetitionGroupExamSetDisabled(hasEntranceDisciplines);

            // Формирование дисциплин для сдачи при наличии свидетельства ЕГЭ
            model.setPassDisciplineForming(model.getPassDisciplineFormingList().get(enrollmentCampaign.isDisciplineFormRestriction() ? 0 : 1));
            model.setPassDisciplineFormingDisabled(false);

            // Выбор направлений абитуриентом (по свидетельству ЕГЭ)
            model.setDirectionSelectionByStateExam(model.getDirectionSelectionByStateExamList().get(enrollmentCampaign.isStateExamRestriction() ? 0 : 1));
            model.setDirectionSelectionByStateExamDisabled(false);

            // Регистрация свидетельств ЕГЭ по умолчанию
            model.setStateExamRegistration(model.getStateExamRegistrationList().get(enrollmentCampaign.isStateExamAutoCheck() ? 0 : 1));
            model.setStateExamRegistrationDisabled(false);

            //Номер для свидетельств ЕГЭ 1-ой волны
            model.setStateExamWave1Number(model.getStateExamWave1NumberList().get(enrollmentCampaign.isStateExamWave1NumberRequired() ? 0 : 1));
            model.setStateExamWave1NumberDisabled(false);

            //Используемые кнопки для заявления в карточке абитуриента
            model.setUsePrintRequestButton(model.getEnrollmentCampaign().isUsePrintRequestButton());
            model.setUsePrintExamSheetButton(model.getEnrollmentCampaign().isUsePrintExamSheetButton());
            model.setUsePrintDocumentsInventoryAndReceiptButton(model.getEnrollmentCampaign().isUsePrintDocumentsInventoryAndReceiptButton());
            model.setUsePrintAuthCardButton(model.getEnrollmentCampaign().isUsePrintAuthCardButton());
            model.setUsePrintLetterboxButton(model.getEnrollmentCampaign().isUsePrintLetterboxButton());
            model.setUseChangePriorityButton(model.getEnrollmentCampaign().isUseChangePriorityButton());

            // Формирование экзаменационных групп
            model.setFormingExamGroup(model.getFormingExamGroupList().get(enrollmentCampaign.isFormingExamGroupAuto() ? 0 : 1));
            model.setFormingExamGroupDisabled(hasEntrantRequests);

            // Прием оригиналов документов
            model.setAcceptOriginalDocument(model.getAcceptOriginalDocumentList().get(enrollmentCampaign.isOriginalDocumentPerDirection() ? 0 : 1));
            model.setAcceptOriginalDocumentDisabled(hasEntrantRequests);

            // Оригиналы документов при зачислении
            model.setNeedOriginalDocForOrder(model.getOriginalDocumentList().get(enrollmentCampaign.isNeedOriginDocForOrder() ? 0 : 1));
            model.setNeedOriginalDocForPreliminary(model.getOriginalDocumentList().get(enrollmentCampaign.isNeedOriginDocForPreliminary() ? 0 : 1));
            model.setOriginalDocumentDisabled(false);

            // Формирование параграфа приказа о зачислении (по умолчанию)
            model.setEnrollParagraphRule(model.getEnrollParagraphRuleList().get(enrollmentCampaign.isEnrollParagraphPerEduOrgUnit() ? 0 : 1));
            model.setEnrollParagraphRuleDisabled(false);

            // Зачтение диплома олимпиады
            model.setOlympiadDiplomaRule(model.getOlympiadDiplomaRuleList().get(enrollmentCampaign.isOlympiadDiplomaForDirection() ? 0 : 1));

            //учитывать приоритеты вступительных испытаний
            model.setPrioritiesAdmissionTestRule(model.getPrioritiesAdmissionTestRuleList().get(enrollmentCampaign.isPrioritiesAdmissionTest() ? 0:1));

            //Учитывать индивидуальные достижения в сумме конкурсных баллов
            model.setIndividualProgressConsider(model.getIndividualProgressList().get(enrollmentCampaign.isUseIndividualProgressInAmountMark() ? 0:1));

            model.setRequiredAgreement4PreEnrollment(enrollmentCampaign.isRequiredAgreement4PreEnrollment() ?
                                                             TwinComboDataSourceHandler.getYesOption("Обязательно") :
                                                             TwinComboDataSourceHandler.getNoOption("Необязательно"));
            model.setRequiredAgreement4Order(enrollmentCampaign.isRequiredAgreement4Order() ?
                                                     TwinComboDataSourceHandler.getYesOption("Обязательно") :
                                                     TwinComboDataSourceHandler.getNoOption("Необязательно"));
        }
        else
        {
            // форма добавления
            EnrollmentCampaign enrollmentCampaign = new EnrollmentCampaign();
            model.setEnrollmentCampaign(enrollmentCampaign);
            enrollmentCampaign.setEnrollmentCampaignType(getByCode(EnrollmentCampaignType.class, EnrollmentCampaignTypeCodes.INDEFINED));
            enrollmentCampaign.setClosed(false);
            EducationYear educationYear = get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE);
            EducationYear nextYear = get(EducationYear.class, EducationYear.P_INT_VALUE, educationYear.getIntValue() + 1);
            enrollmentCampaign.setEducationYear(nextYear);
            if (nextYear != null)
                enrollmentCampaign.setTitle(nextYear.getTitle());
            model.getPeriod().setEnrollmentCampaign(enrollmentCampaign);

            model.setDirectionForOutOfCompetition(model.getDirectionForOutOfCompetitionList().get(0));
            model.setDirectionForTargetAdmission(model.getDirectionForTargetAdmissionList().get(0));

            model.setUsePrintRequestButton(true);
            model.setUsePrintExamSheetButton(true);
            model.setUsePrintDocumentsInventoryAndReceiptButton(true);
            model.setUsePrintAuthCardButton(true);
            model.setUsePrintLetterboxButton(true);
            model.setUseChangePriorityButton(true);
            model.setIndividualProgressConsider(model.getIndividualProgressList().get(1));

            model.setRequiredAgreement4PreEnrollment(TwinComboDataSourceHandler.getNoOption("Необязательно"));
            model.setRequiredAgreement4Order(TwinComboDataSourceHandler.getNoOption("Необязательно"));
        }

        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
    }

    @Override
    public void update(Model model)
    {
        EnrollmentCampaign ec = model.getEnrollmentCampaign();

        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (!model.getPeriod().getDateFrom().before(model.getPeriod().getDateTo()))
            errors.add("Дата начала должна быть раньше даты окончания.", "dateFrom", "dateTo");

        // проверяем ограничения
        boolean hasDefaultRestriction = false;
        Set<MultiKey> used = new HashSet<>();
        if (ec.isUseCompetitionGroup())
        {
            for (CompetitionGroupRestriction dto : model.getCompetitionGroupRestrictionList())
            {
                if (!used.add(new MultiKey(dto.getDevelopForm(), dto.getCompensationType())))
                    errors.add("Нельзя сохранить настройку приемной кампании, т.к. не должно быть одинаковых ключей (форма освоения и вид затрат) ограничений числа выбираемых конкурсных групп.");

                boolean defaultRestriction = dto.getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM) && dto.getCompensationType().isBudget();
                hasDefaultRestriction = hasDefaultRestriction || defaultRestriction;

                if (!defaultRestriction && dto.getMaxCompetitionGroup() == null)
                    errors.add("Нельзя сохранить настройку приемной кампании, т.к. не должно быть дополнительных ключей (форма освоения и вид затрат) ограничений числа выбираемых конкурсных групп без заданных опций.");

                CompetitionGroupRestriction obj = new CompetitionGroupRestriction();
                obj.update(dto);
                obj.setEnrollmentCampaign(ec);
            }
        } else
        {
            for (EnrollmentDirectionRestriction dto : model.getDirectionRestrictionList())
            {
                if (!used.add(new MultiKey(dto.getDevelopForm(), dto.getCompensationType())))
                    errors.add("Нельзя сохранить настройку приемной кампании, т.к. не должно быть одинаковых ключей (форма освоения и вид затрат) ограничений числа выбираемых направлений подготовки.");

                boolean defaultRestriction = dto.getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM) && dto.getCompensationType().isBudget();
                hasDefaultRestriction = hasDefaultRestriction || defaultRestriction;

                if (!defaultRestriction && dto.getMaxEnrollmentDirection() == null && dto.getMaxMinisterialDirection() == null && dto.getMaxFormativeOrgUnit() == null)
                    errors.add("Нельзя сохранить настройку приемной кампании, т.к. не должно быть дополнительных ключей (форма освоения и вид затрат) ограничений числа выбираемых направлений подготовки без заданных опций.");

                if (dto.getMaxEnrollmentDirection() != null && dto.getMaxMinisterialDirection() != null && dto.getMaxEnrollmentDirection() < dto.getMaxMinisterialDirection())
                    errors.add("Нельзя сохранить настройку приемной кампании, т.к. для ключа (форма освоения и вид затрат) ограничений число выбираемых направлений подготовки для приема должно быть больше либо равно числу выбираемых направлений подготовки по классификатору.");

                EnrollmentDirectionRestriction obj = new EnrollmentDirectionRestriction();
                obj.update(dto);
                obj.setEnrollmentCampaign(ec);
            }
        }

        if (!hasDefaultRestriction)
            errors.add("Отсутствует ограничение по умолчанию: очная + бюджет");

        if (errors.hasErrors())
            return;

        // Заполняем все флаги приемной кампании
        ec.setEnrollmentPerCompTypeDiff(Model.ENROLLMENT_PER_COMPENSATION_TYPE_DIFFERENCE == model.getEnrollmentPerCompensationType().getId());
        ec.setExamSetDiff(Model.COMPETITION_GROUP_EXAM_SET_NO_EQUALS_FOR_ALL_CATEGORY == model.getCompetitionGroupExamSet().getId());
        ec.setOneDirectionForOutOfCompetition(Model.DIRECTION_FOR_OUT_OF_COMPETITION_ONE == model.getDirectionForOutOfCompetition().getId());
        ec.setOneDirectionForTargetAdmission(Model.DIRECTION_FOR_TARGET_ADMISSION_ONE == model.getDirectionForTargetAdmission().getId());
        ec.setNumberOnRequestManual(Model.NUMBER_ON_REQUEST_MANUAL == model.getNumberOnRequest().getId());
        ec.setNumberOnDirectionManual(Model.NUMBER_ON_DIRECTION_MANUAL == model.getNumberOnDirection().getId());
        ec.setDirectionPerCompTypeDiff(Model.DIRECTION_SELECTION_ONE_COMPENSATION_TYPE != model.getDirectionSelection().getId());
        ec.setSearchBeforeEntrantRegistration(Model.ENTRANT_REGISTRATION_WIZARD_REGULAR != model.getEntrantRegistrationWizard().getId());
        ec.setUseOnlineEntrantRegistration(Model.ENTRANT_REGISTRATION_WIZARD_ONLINE == model.getEntrantRegistrationWizard().getId());
        ec.setDisciplineFormRestriction(Model.PASS_DISCIPLINE_FORMING_NO_FORMING == model.getPassDisciplineForming().getId());
        ec.setUseCompetitionGroup(Model.COMPETITION_GROUP_NO_USE != model.getCompetitionGroup().getId());
        ec.setCompetitionGroupExamSetEqual(Model.COMPETITION_GROUP_USE_EXAM_SET_EQUALS == model.getCompetitionGroup().getId());
        ec.setContractAutoCompetition(Model.DIRECTION_SELECTION_MANY_COMPENSATION_TYPE_CONTEST == model.getDirectionSelection().getId());
        ec.setStateExamRestriction(Model.DIRECTION_SELECTION_BY_STATE_EXAM_NEED_COVERING == model.getDirectionSelectionByStateExam().getId());
        ec.setStateExamAutoCheck(Model.STATE_EXAM_REGISTRATION_CHECKED == model.getStateExamRegistration().getId());
        ec.setStateExamWave1NumberRequired(Model.STATE_EXAM_WAVE1_NUMBER_REQUIRED == model.getStateExamWave1Number().getId());
        ec.setFormingExamGroupAuto(Model.FORMING_EXAM_GROUP_AUTO == model.getFormingExamGroup().getId());
        ec.setOriginalDocumentPerDirection(Model.ACCEPT_ORIGINAL_DOCUMENT_FOR_DIRECTION == model.getAcceptOriginalDocument().getId());
        ec.setEntrantExamListsFormingFeature(getCatalogItem(EntrantExamListsFormingFeature.class, Model.ENTRANT_EXAM_LIST_FORMING_FOR_ENTRANT == model.getEntrantExamListForming().getId() ? UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT : UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_REQUEST));
        ec.setNeedOriginDocForOrder(Model.ORIGINAL_DOCUMENTS_NEED == model.getNeedOriginalDocForOrder().getId());
        ec.setNeedOriginDocForPreliminary(Model.ORIGINAL_DOCUMENTS_NEED == model.getNeedOriginalDocForPreliminary().getId());
        ec.setEnrollParagraphPerEduOrgUnit(Model.ENROLL_PARAGRAPH_FORMING_RULE_1 == model.getEnrollParagraphRule().getId());
        ec.setOlympiadDiplomaForDirection(Model.OLYMPIAD_DIPLOMA_FOR_DIRECTION == model.getOlympiadDiplomaRule().getId());
        ec.setPrioritiesAdmissionTest(Model.PRIORITIES_ADMISSION_TESTS_CONSIDER == model.getPrioritiesAdmissionTestRule().getId());
        ec.setUsePrintRequestButton(model.isUsePrintRequestButton());
        ec.setUsePrintExamSheetButton(model.isUsePrintExamSheetButton());
        ec.setUsePrintDocumentsInventoryAndReceiptButton(model.isUsePrintDocumentsInventoryAndReceiptButton());
        ec.setUsePrintAuthCardButton(model.isUsePrintAuthCardButton());
        ec.setUsePrintLetterboxButton(model.isUsePrintLetterboxButton());
        ec.setUseChangePriorityButton(model.isUseChangePriorityButton());
        ec.setUseIndividualProgressInAmountMark(Model.INDIVIDUAL_PROGRESS_CONSIDER_YES == model.getIndividualProgressConsider().getId());
        ec.setRequiredAgreement4PreEnrollment(TwinComboDataSourceHandler.getSelectedValue(model.getRequiredAgreement4PreEnrollment()));
        ec.setRequiredAgreement4Order(TwinComboDataSourceHandler.getSelectedValue(model.getRequiredAgreement4Order()));

        Session session = getSession();

        // если используются конкурсные группы, то нельзя внучную указывать номер на направлении приема
        if (ec.isUseCompetitionGroup())
            ec.setNumberOnDirectionManual(false);

        // если нельзя создавать направления приема с разными виды возмещения затрат, то абитуриент не может участвовать в конкурсе на контракт
        if (!ec.isDirectionPerCompTypeDiff())
            ec.setContractAutoCompetition(false);

        // если экзаменационные группы формируются вручную, тогда удаляем алгоритм формирования экзаменационных групп
        if (!ec.isFormingExamGroupAuto() && ec.getId() != null)
        {
            CurrentExamGroupLogic logic = get(CurrentExamGroupLogic.class, CurrentExamGroupLogic.L_ENROLLMENT_CAMPAIGN, ec);
            if (logic != null)
                delete(logic);
        }

        // сохраняем/обновляем приемную компания
        session.saveOrUpdate(ec);
        session.saveOrUpdate(model.getPeriod());

        // если добавляем, то создать настройки приоритетов вида конкурса
        if (model.getEnrollmentCampaignId() == null)
        {
            // создаем настройки для используемых видов конкурса
            for (CompetitionKind competitionKind : getList(CompetitionKind.class))
            {
                EnrollmentCompetitionKind priority = new EnrollmentCompetitionKind();
                priority.setCompetitionKind(competitionKind);
                priority.setEnrollmentCampaign(ec);
                priority.setPriority(competitionKind.getPriority());
                session.save(priority);
            }

            // создаем настройку "целевой прием", которая не ссылается на вид конкурса
            EnrollmentCompetitionKind targetAdmissionPriority = new EnrollmentCompetitionKind();
            targetAdmissionPriority.setEnrollmentCampaign(ec);
            targetAdmissionPriority.setPriority(3);
            targetAdmissionPriority.setUsed(true);
            session.save(targetAdmissionPriority);

            // создаем настройки для используемых типов приказов
            for (EntrantEnrollmentOrderType orderType : getList(EntrantEnrollmentOrderType.class))
            {
                if (!orderType.getCode().equals(EntrantEnrollmentOrderType.REVERT_ORDER_CODE))
                {
                    EnrollmentOrderType type = new EnrollmentOrderType();
                    type.setEntrantEnrollmentOrderType(orderType);
                    type.setEnrollmentCampaign(ec);
                    type.setPriority(orderType.getDefaultPriority());
                    session.save(type);
                }
            }
        }

        // удаляем все ограничения
        for (EnrollmentDirectionRestriction item : getList(EnrollmentDirectionRestriction.class, EnrollmentDirectionRestriction.enrollmentCampaign(), ec))
            delete(item);
        for (CompetitionGroupRestriction item : getList(CompetitionGroupRestriction.class, CompetitionGroupRestriction.enrollmentCampaign(), ec))
            delete(item);
        session.flush();

        // сохраняем новые ограничения
        if (ec.isUseCompetitionGroup())
        {
            for (CompetitionGroupRestriction dto : model.getCompetitionGroupRestrictionList())
            {
                CompetitionGroupRestriction obj = new CompetitionGroupRestriction();
                obj.update(dto);
                obj.setEnrollmentCampaign(ec);
                session.save(obj);
            }
        } else
        {
            for (EnrollmentDirectionRestriction dto : model.getDirectionRestrictionList())
            {
                EnrollmentDirectionRestriction obj = new EnrollmentDirectionRestriction();
                obj.update(dto);
                obj.setEnrollmentCampaign(ec);
                session.save(obj);
            }
        }
    }
}
