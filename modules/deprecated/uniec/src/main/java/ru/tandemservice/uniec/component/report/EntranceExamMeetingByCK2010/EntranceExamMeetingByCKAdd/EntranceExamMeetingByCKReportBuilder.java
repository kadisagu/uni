/* $Id: EntranceExamMeetingByCKReportBuilder.java 13764 2010-07-16 18:29:10Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import java.util.*;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
class EntranceExamMeetingByCKReportBuilder
{
    private Model _model;
    private Session _session;

    EntranceExamMeetingByCKReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent(EntranceExamMeetingRtfBuilder rtfBuilder)
    {
        byte[] data = buildReport(rtfBuilder);
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport(EntranceExamMeetingRtfBuilder builder)
    {
        // получаем направления приема
        List<EnrollmentDirection> enrollmentDirectionList = getEnrollmentDirectionList();

        // создаем билдер
        MQBuilder directionBuilder = getMQBuilder(enrollmentDirectionList);

        // валидируем
        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examSetDataMap = ExamSetUtil.getDirectionExamSetDataMap(_session, _model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());
        for (EnrollmentDirectionExamSetData examSetData : examSetDataMap.values())
            if (!examSetData.isStructuresEqual())
                throw new ApplicationException("Для направления приема '" + examSetData.getEnrollmentDirection().getTitle() + "' по категориям поступающих и виду затрат наборы экзаменов различные.");

        // получаем группировку
        List<EnrollmentDirectionGroup> enrollmentDirectionGroupList = getEnrollmentDirectionGroupList(enrollmentDirectionList);

        // вид конкурса -> приоритет
        Map<CompetitionKind, Integer> competitionKindPriorities = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign());

        // приоритет -> вид конкурса
        Map<Integer, CompetitionKind> priority2CompetitionKind = new TreeMap<Integer, CompetitionKind>();
        for (Map.Entry<CompetitionKind, Integer> entry : competitionKindPriorities.entrySet())
            if (!_model.getReport().isOnlyCompetitiveAdmission() || (entry.getKey() != null && entry.getKey().getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION)))
                priority2CompetitionKind.put(entry.getValue(), entry.getKey());

        // создаем утиль
        EntrantDataUtil dataUtil = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), directionBuilder);

        // инициализируем rtf builder
        builder.init(_session, _model, examSetDataMap, dataUtil, directionBuilder, priority2CompetitionKind);

        // создаем результирующий файл
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(builder.getTemplate().getHeader());
        result.setSettings(builder.getTemplate().getSettings());

        // по всем группам направлений приема
        for (EnrollmentDirectionGroup group : enrollmentDirectionGroupList)
        {
            List<EnrollmentDirection> list = group.getEnrollmentDirectionList();

            // true, если в группе все структуры наборов одинаковые
            boolean structuresEqualInWholeGroup;

            if (list.size() == 1)
            {
                structuresEqualInWholeGroup = true;
            } else
            {
                ExamSetStructure structure = examSetDataMap.get(list.get(0)).getExamSetStructure();
                int len = list.size();
                int i;

                // проверяем равенство структур наборов экзаменов
                i = 1;
                while (i < len && structure.equals(examSetDataMap.get(list.get(i)).getExamSetStructure())) i++;
                structuresEqualInWholeGroup = i == len;

                if (!structuresEqualInWholeGroup)
                {
                    // если структуры экзаменов не равны, то надо проверить равенство структур с точностью до типа
                    i = 1;
                    while (i < len && structure.kindEquals(examSetDataMap.get(list.get(i)).getExamSetStructure())) i++;
                    if (i < len)
                        throw new ApplicationException("Для направлений приема '" + examSetDataMap.get(list.get(0)).getEnrollmentDirection().getTitle() + "' и '" + examSetDataMap.get(list.get(i)).getEnrollmentDirection().getTitle() + "', попавших в одну группу, различные типы вступительных испытаний.");
                }
            }

            // названия типов вступительных испытаний
            // это корректно, так как выше стоит проверка на равенство структур с точностью до типа
            List<String> typeTitles = examSetDataMap.get(list.get(0)).getExamSetItemKindTitleList();

            // названия вступительных испытаний
            List<String[]> titles = null;
            if (structuresEqualInWholeGroup)
            {
                // это корректно только если структуры полностью равны
                titles = examSetDataMap.get(list.get(0)).getExamSetItemTitleList();
            }

            Map<CompetitionKind, List<ReportRow>> competitionKind2RowList = SafeMap.get(ArrayList.class);

            for (EnrollmentDirection direction : group.getEnrollmentDirectionList())
            {
                for (RequestedEnrollmentDirection item : dataUtil.getDirectionSet(direction))
                {
                    ReportRow row = new ReportRow(item, dataUtil);

                    competitionKind2RowList.get(row.getCompetitionKind()).add(row);
                }
            }

            for (List<ReportRow> rowList : competitionKind2RowList.values())
                Collections.sort(rowList, new RequestedEnrollmentDirectionComparator(competitionKindPriorities));

            // названия из типов вступительных испытаний

            // получам список элементов для текущей группы
            List<IRtfElement> res = builder.buildReportPart(group, competitionKind2RowList, structuresEqualInWholeGroup, typeTitles, titles, _model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark());

            // добавляем их в результирующий файл
            result.getElementList().addAll(res);
        }

        return RtfUtil.toByteArray(result);
    }

    private List<EnrollmentDirection> getEnrollmentDirectionList()
    {
        // получаем список направлений приема
        List<EnrollmentDirection> enrollmentDirectionList;
        if (_model.isByAllEnrollmentDirections())
        {
            MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
            if (!_model.getQualificationList().isEmpty())
                builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
            if (!_model.getDevelopFormList().isEmpty())
                builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
            if (!_model.getDevelopConditionList().isEmpty())
                builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
            enrollmentDirectionList = builder.getResultList(_session);
        } else
        {
            enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);
        }
        Collections.sort(enrollmentDirectionList, ITitled.TITLED_COMPARATOR);
        return enrollmentDirectionList;
    }

    private MQBuilder getMQBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        // создаем запрос по выбранным направления приема
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        directionBuilder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
        if (_model.getReport().isOnlyWithOriginals())
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
        if (_model.getReport().isOnlyCompetitiveAdmission())
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.competitionKind().code(), UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION));
        if (!_model.getStudentCategoryList().isEmpty())
            directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        if (_model.isNotIncludeEnrollment())
        {
            directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_IN_ORDER));
            directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ENROLED_CODE));
        }

        if (_model.getCustomStateRuleValue() != null)
        {
            AbstractExpression expression4EntrantCustomState = EntrantCustomState.getExistsExpression4EntrantCustomState(
                    "r", RequestedEnrollmentDirection.entrantRequest().entrant().s(),
                    _model.getCustomStateRuleValue(), _model.getEntrantCustomStateList(), new Date());
            if (expression4EntrantCustomState != null)
                directionBuilder.add(expression4EntrantCustomState);
        }

        return directionBuilder;
    }

    @SuppressWarnings("unchecked")
    private List<EnrollmentDirectionGroup> getEnrollmentDirectionGroupList(List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EnrollmentDirectionGroup> groupList = new ArrayList<EnrollmentDirectionGroup>();
        final int DIRECTION_LIST = 0;
        final int GROUP_TITLE = 1;
        if (_model.isGroupByEducationLevel())
        {
            Map<MultiKey, Object[]> key2group = new HashMap<MultiKey, Object[]>();

            for (EnrollmentDirection direction : enrollmentDirectionList)
            {
                EducationLevels educationLevels = direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

                // ищем
                while (educationLevels != null && !educationLevels.getLevelType().isGroupingLevel())
                    educationLevels = educationLevels.getParentLevel();
                if (educationLevels == null)
                    throw new ApplicationException("Нельзя сформировать отчет, т.к. для направления «" + direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle() + "» в справочнике не задано направление, которому оно подчинено, необходимое для группировки данных.");

                EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
                MultiKey key = new MultiKey(new Object[]{
                    educationLevels,
                    educationOrgUnit.getFormativeOrgUnit().getId(),
                    educationOrgUnit.getDevelopForm().getId(),
                    educationOrgUnit.getDevelopCondition().getId(),
                    educationOrgUnit.getDevelopTech().getId(),
                    educationOrgUnit.getDevelopPeriod().getId()
                });

                Object[] data = key2group.get(key);
                List<EnrollmentDirection> directionList;
                if (data == null)
                    key2group.put(key, new Object[]{
                        directionList = new ArrayList<EnrollmentDirection>(),                  // DIRECTION_LIST
                        educationLevels.getTitleCodePrefix() + " " + educationLevels.getTitle() // GROUP_TITLE
                    });
                else
                    directionList = (List<EnrollmentDirection>) data[DIRECTION_LIST];
                directionList.add(direction);
            }

            for (Map.Entry<MultiKey, Object[]> entry : key2group.entrySet())
            {
                List<EnrollmentDirection> directionList = (List<EnrollmentDirection>) entry.getValue()[DIRECTION_LIST];
                String groupTitle = (String) entry.getValue()[GROUP_TITLE];

                EducationLevels educationLevels = (EducationLevels) entry.getKey().getKey(0);
                EducationOrgUnit educationOrgUnit = directionList.get(0).getEducationOrgUnit();
                groupList.add(new EnrollmentDirectionGroup(groupTitle, educationLevels, directionList,
                    educationOrgUnit.getFormativeOrgUnit(),
                    educationOrgUnit.getDevelopForm(),
                    educationOrgUnit.getDevelopCondition(),
                    educationOrgUnit.getDevelopTech(),
                    educationOrgUnit.getDevelopPeriod()
                ));
            }
        } else
        {
            for (EnrollmentDirection direction : enrollmentDirectionList)
            {
                EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
                EducationLevelsHighSchool highSchool = educationOrgUnit.getEducationLevelHighSchool();
                groupList.add(new EnrollmentDirectionGroup(highSchool.getEducationLevel().getTitleCodePrefix() + " " + highSchool.getTitleWithProfile(),
                    direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(),
                    Arrays.asList(direction),
                    educationOrgUnit.getFormativeOrgUnit(),
                    educationOrgUnit.getDevelopForm(),
                    educationOrgUnit.getDevelopCondition(),
                    educationOrgUnit.getDevelopTech(),
                    educationOrgUnit.getDevelopPeriod()
                ));
            }
        }

        Collections.sort(groupList, new Comparator<EnrollmentDirectionGroup>()
            {
            @Override
            public int compare(EnrollmentDirectionGroup o1, EnrollmentDirectionGroup o2)
            {
                OrgUnit f1 = o1.getEnrollmentDirectionList().get(0).getEducationOrgUnit().getFormativeOrgUnit();
                OrgUnit f2 = o2.getEnrollmentDirectionList().get(0).getEducationOrgUnit().getFormativeOrgUnit();
                int result = f1.getTitle().compareTo(f2.getTitle());
                if (result != 0) return result;

                return o1.getTitle().compareTo(o2.getTitle());
            }
            });

        return groupList;
    }
}
