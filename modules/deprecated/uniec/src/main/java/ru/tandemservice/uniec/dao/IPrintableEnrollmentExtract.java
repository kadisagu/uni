/* $Id$ */
package ru.tandemservice.uniec.dao;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.IPrintableExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 08.08.2013
 */
public interface IPrintableEnrollmentExtract<E, P extends IAbstractParagraph> extends IAbstractExtract<E, P>, IPrintableExtract
{
    DatabaseFile getContent();

    void setContent(DatabaseFile content);

    String getPrintScriptCatalogCode();

    EnrollmentOrder getOrder();
}