/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.MarkDistributionModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public class Model extends MarkDistributionModel implements IEnrollmentCampaignModel
{
    private ISelectModel _compensationTypeListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _educationLevelsHighSchoolListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developPeriodListModel;
    private IPrincipalContext _principalContext;

    // IEnrollmentCampaignModel

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return (OrgUnit) getSettings().get("formativeOrgUnit");
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return (OrgUnit) getSettings().get("territorialOrgUnit");
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return (EducationLevelsHighSchool) getSettings().get("educationLevelsHighSchool");
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return (DevelopForm) getSettings().get("developForm");
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return (DevelopCondition) getSettings().get("developCondition");
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return (DevelopTech) getSettings().get("developTech");
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return (DevelopPeriod) getSettings().get("developPeriod");
    }

    // Getters & Setters

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getEducationLevelsHighSchoolListModel()
    {
        return _educationLevelsHighSchoolListModel;
    }

    public void setEducationLevelsHighSchoolListModel(ISelectModel educationLevelsHighSchoolListModel)
    {
        _educationLevelsHighSchoolListModel = educationLevelsHighSchoolListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }
}
