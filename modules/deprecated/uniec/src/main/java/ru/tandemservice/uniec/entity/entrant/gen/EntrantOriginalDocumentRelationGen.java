package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление приема по которому сданы оригиналы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantOriginalDocumentRelationGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation";
    public static final String ENTITY_NAME = "entrantOriginalDocumentRelation";
    public static final int VERSION_HASH = -1904938134;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENTRANT = "entrant";
    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";

    private int _version; 
    private Entrant _entrant;     // (Старый) Абитуриент
    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление приема

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantOriginalDocumentRelationGen)
        {
            setVersion(((EntrantOriginalDocumentRelation)another).getVersion());
            setEntrant(((EntrantOriginalDocumentRelation)another).getEntrant());
            setRequestedEnrollmentDirection(((EntrantOriginalDocumentRelation)another).getRequestedEnrollmentDirection());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantOriginalDocumentRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantOriginalDocumentRelation.class;
        }

        public T newInstance()
        {
            return (T) new EntrantOriginalDocumentRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "entrant":
                    return obj.getEntrant();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "entrant":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "entrant":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "entrant":
                    return Entrant.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantOriginalDocumentRelation> _dslPath = new Path<EntrantOriginalDocumentRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantOriginalDocumentRelation");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    public static class Path<E extends EntrantOriginalDocumentRelation> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private Entrant.Path<Entrant> _entrant;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EntrantOriginalDocumentRelationGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

        public Class getEntityClass()
        {
            return EntrantOriginalDocumentRelation.class;
        }

        public String getEntityName()
        {
            return "entrantOriginalDocumentRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
