/* $Id$ */
package ru.tandemservice.uniec.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudent.entity.StudentListOrder;

/**
 * @author Dmitry Seleznev
 * @since 16.08.2012
 */
public interface IUniecOrderDAO
{
    String BEAN_NAME = "uniecOrderDao";

    final SpringBeanCache<IUniecOrderDAO> instance = new SpringBeanCache<IUniecOrderDAO>(BEAN_NAME);

    /**
     * Возвращает печатную форму приказа по студентам
     * Запускается из контроллера печати приказа
     *
     * @param template   - шаблон приказа
     * @param order      - приказ по студентам
     * @param scriptCode - код элемента справочника "Скрипты" модуля "Абитуриент"
     * @return - документ в формате RTF, готовый к выгрузке в базу, или пользователю
     */
    RtfDocument getOrderPrintForm(byte[] template, StudentListOrder order, String scriptCode);
}