/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Боба
 * @since 12.08.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", RequestedEnrollmentDirection.competitionKind().title().s()).setClickable(false).setOrderable(false));

        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_REQUEST_ADD_EDIT, new ParametersMap()
                .add("entrantRequestId", getModel(component).getEntrantRequest().getId())
        ));
    }
}
