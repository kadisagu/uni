/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect.EnrollmentDataCollectDao;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect.IEnrollmentDataCollectDao;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport.EnrollmentDirProfReportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport.EnrollmentDirProfReportListDSHandler;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport.IEnrollmentDirProfReportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EntrantDataExport.EntrantDataExportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EntrantDataExport.IEntrantDataExportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummarySheetExamReport.ISummarySheetExamReportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummarySheetExamReport.SummarySheetExamReportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummarySheetExamReport.SummarySheetExamReportListDSHandler;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport.ISummaryStateExamMarkReportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport.SummaryStateExamMarkReportDAO;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport.SummaryStateExamMarkReportListDSHandler;

/**
 * @author Alexander Shaburov
 * @since 02.07.12
 */
@Configuration
public class EcReportManager extends BusinessObjectManager
{
    public static EcReportManager instance()
    {
        return instance(EcReportManager.class);
    }

    @Bean
    public IEnrollmentDirProfReportDAO enrollmentDirProfReportDAO()
    {
        return new EnrollmentDirProfReportDAO();
    }

    @Bean
    public ISummarySheetExamReportDAO summarySheetExamReportDAO()
    {
        return new SummarySheetExamReportDAO();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentDirProfReportListDSHandler()
    {
        return new EnrollmentDirProfReportListDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> summarySheetExamReportListDSHandler()
    {
        return new SummarySheetExamReportListDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> summaryStateExamMarkReportListDSHandler()
    {
        return new SummaryStateExamMarkReportListDSHandler(getName());
    }

    @Bean
    public ISummaryStateExamMarkReportDAO summaryStateExamMarkReportDAO()
    {
        return new SummaryStateExamMarkReportDAO();
    }

    @Bean
    public IEntrantDataExportDAO entrantDataExportDAO()
    {
        return new EntrantDataExportDAO();
    }

    @Bean
    public IEnrollmentDataCollectDao enrollmentDataCollectDao()
    {
        return new EnrollmentDataCollectDao();
    }
}
