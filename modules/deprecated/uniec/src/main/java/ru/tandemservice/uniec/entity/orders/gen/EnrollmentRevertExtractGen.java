package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из приказа об изменении приказа о зачислении
 *
 * После проведения отменяемая выписка удаляется. Если удаляемая выписка была единственной в параграфе, то параграф тоже удаляется (без выписок параграф не живет).
 * Вместе с удалением выписки удаляется и соответствующий студент предзачисления, чтобы не поплыли отчеты, которые считают по предзачисленным студентам и чтобы
 * можно было студента зачислить другим приказом.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentRevertExtractGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract";
    public static final String ENTITY_NAME = "enrollmentRevertExtract";
    public static final int VERSION_HASH = -1434367264;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_NUMBER = "number";
    public static final String L_PARAGRAPH = "paragraph";
    public static final String L_STATE = "state";
    public static final String L_CONTENT = "content";
    public static final String P_COMMITTED = "committed";
    public static final String L_CANCELED_EXTRACT = "canceledExtract";
    public static final String L_ENTITY = "entity";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String L_COURSE = "course";

    private Date _createDate;     // Дата формирования
    private Integer _number;     // Номер выписки в параграфе
    private EnrollmentRevertParagraph _paragraph;     // Параграф приказа об изменении приказа о зачислении
    private ExtractStates _state;     // Состояние выписки
    private DatabaseFile _content;     // Сохраненная печатная форма
    private boolean _committed;     // Проведена
    private EnrollmentExtract _canceledExtract;     // Выписка для отмены
    private RequestedEnrollmentDirection _entity;     // Выбранное направление приема
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)
    private CompensationType _compensationType;     // Вид возмещения затрат
    private StudentCategory _studentCategory;     // Категория обучаемого
    private Course _course;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Параграф приказа об изменении приказа о зачислении. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentRevertParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф приказа об изменении приказа о зачислении. Свойство не может быть null.
     */
    public void setParagraph(EnrollmentRevertParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Сохраненная печатная форма.
     */
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Сохраненная печатная форма.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Проведена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведена. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return Выписка для отмены.
     */
    public EnrollmentExtract getCanceledExtract()
    {
        return _canceledExtract;
    }

    /**
     * @param canceledExtract Выписка для отмены.
     */
    public void setCanceledExtract(EnrollmentExtract canceledExtract)
    {
        dirty(_canceledExtract, canceledExtract);
        _canceledExtract = canceledExtract;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Выбранное направление приема. Свойство не может быть null.
     */
    public void setEntity(RequestedEnrollmentDirection entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentRevertExtractGen)
        {
            setCreateDate(((EnrollmentRevertExtract)another).getCreateDate());
            setNumber(((EnrollmentRevertExtract)another).getNumber());
            setParagraph(((EnrollmentRevertExtract)another).getParagraph());
            setState(((EnrollmentRevertExtract)another).getState());
            setContent(((EnrollmentRevertExtract)another).getContent());
            setCommitted(((EnrollmentRevertExtract)another).isCommitted());
            setCanceledExtract(((EnrollmentRevertExtract)another).getCanceledExtract());
            setEntity(((EnrollmentRevertExtract)another).getEntity());
            setEducationOrgUnit(((EnrollmentRevertExtract)another).getEducationOrgUnit());
            setCompensationType(((EnrollmentRevertExtract)another).getCompensationType());
            setStudentCategory(((EnrollmentRevertExtract)another).getStudentCategory());
            setCourse(((EnrollmentRevertExtract)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentRevertExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentRevertExtract.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentRevertExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "number":
                    return obj.getNumber();
                case "paragraph":
                    return obj.getParagraph();
                case "state":
                    return obj.getState();
                case "content":
                    return obj.getContent();
                case "committed":
                    return obj.isCommitted();
                case "canceledExtract":
                    return obj.getCanceledExtract();
                case "entity":
                    return obj.getEntity();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "paragraph":
                    obj.setParagraph((EnrollmentRevertParagraph) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "canceledExtract":
                    obj.setCanceledExtract((EnrollmentExtract) value);
                    return;
                case "entity":
                    obj.setEntity((RequestedEnrollmentDirection) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "number":
                        return true;
                case "paragraph":
                        return true;
                case "state":
                        return true;
                case "content":
                        return true;
                case "committed":
                        return true;
                case "canceledExtract":
                        return true;
                case "entity":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategory":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "number":
                    return true;
                case "paragraph":
                    return true;
                case "state":
                    return true;
                case "content":
                    return true;
                case "committed":
                    return true;
                case "canceledExtract":
                    return true;
                case "entity":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategory":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "paragraph":
                    return EnrollmentRevertParagraph.class;
                case "state":
                    return ExtractStates.class;
                case "content":
                    return DatabaseFile.class;
                case "committed":
                    return Boolean.class;
                case "canceledExtract":
                    return EnrollmentExtract.class;
                case "entity":
                    return RequestedEnrollmentDirection.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentRevertExtract> _dslPath = new Path<EnrollmentRevertExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentRevertExtract");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Параграф приказа об изменении приказа о зачислении. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getParagraph()
     */
    public static EnrollmentRevertParagraph.Path<EnrollmentRevertParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return Выписка для отмены.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCanceledExtract()
     */
    public static EnrollmentExtract.Path<EnrollmentExtract> canceledExtract()
    {
        return _dslPath.canceledExtract();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getEntity()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends EnrollmentRevertExtract> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Integer> _number;
        private EnrollmentRevertParagraph.Path<EnrollmentRevertParagraph> _paragraph;
        private ExtractStates.Path<ExtractStates> _state;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Boolean> _committed;
        private EnrollmentExtract.Path<EnrollmentExtract> _canceledExtract;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _entity;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private CompensationType.Path<CompensationType> _compensationType;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(EnrollmentRevertExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EnrollmentRevertExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Параграф приказа об изменении приказа о зачислении. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getParagraph()
     */
        public EnrollmentRevertParagraph.Path<EnrollmentRevertParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new EnrollmentRevertParagraph.Path<EnrollmentRevertParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(EnrollmentRevertExtractGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return Выписка для отмены.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCanceledExtract()
     */
        public EnrollmentExtract.Path<EnrollmentExtract> canceledExtract()
        {
            if(_canceledExtract == null )
                _canceledExtract = new EnrollmentExtract.Path<EnrollmentExtract>(L_CANCELED_EXTRACT, this);
            return _canceledExtract;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getEntity()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> entity()
        {
            if(_entity == null )
                _entity = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return EnrollmentRevertExtract.class;
        }

        public String getEntityName()
        {
            return "enrollmentRevertExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
