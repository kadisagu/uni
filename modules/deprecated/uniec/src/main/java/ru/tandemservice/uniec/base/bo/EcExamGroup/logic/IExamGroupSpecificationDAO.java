/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcExamGroup.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSpecification;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 09.07.12
 */
public interface IExamGroupSpecificationDAO extends INeedPersistenceSupport
{
    /**
     * Производит проверки при сохранении изменений на странице.
     * @param entityList список Характеристик экзам. группы
     * @return <code>ErrorCollector</code>
     */
    ErrorCollector validate(List<ExamGroupSpecification> entityList);

    /**
     * Производит сохранение или обновление элементов списка.
     * @param entityList список Характеристик экзам. группы
     */
    List<ExamGroupSpecification> saveOrUpdateEntity(List<ExamGroupSpecification> entityList);
}
