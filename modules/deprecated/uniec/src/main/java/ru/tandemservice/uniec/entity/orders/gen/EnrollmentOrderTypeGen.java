package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип приказа в приемной кампании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentOrderType";
    public static final String ENTITY_NAME = "enrollmentOrderType";
    public static final int VERSION_HASH = -215103102;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ENTRANT_ENROLLMENT_ORDER_TYPE = "entrantEnrollmentOrderType";
    public static final String P_PRIORITY = "priority";
    public static final String P_USED = "used";
    public static final String P_BASIC = "basic";
    public static final String P_COMMAND = "command";
    public static final String P_GROUP = "group";
    public static final String P_SELECT_HEADMAN = "selectHeadman";
    public static final String P_REASON_AND_BASIC = "reasonAndBasic";
    public static final String P_USE_ENROLLMENT_DATE = "useEnrollmentDate";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private EntrantEnrollmentOrderType _entrantEnrollmentOrderType;     // Тип приказа на зачисление абитуриента в личный состав студентов
    private int _priority;     // Приоритет
    private boolean _used;     // Используется
    private boolean _basic;     // Отображать основание
    private boolean _command;     // Отображать приказываю
    private boolean _group;     // Отображать группу
    private boolean _selectHeadman;     // Выбор старосты
    private boolean _reasonAndBasic;     // Причина и основание
    private boolean _useEnrollmentDate;     // Используется дата зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов.
     */
    public EntrantEnrollmentOrderType getEntrantEnrollmentOrderType()
    {
        return _entrantEnrollmentOrderType;
    }

    /**
     * @param entrantEnrollmentOrderType Тип приказа на зачисление абитуриента в личный состав студентов.
     */
    public void setEntrantEnrollmentOrderType(EntrantEnrollmentOrderType entrantEnrollmentOrderType)
    {
        dirty(_entrantEnrollmentOrderType, entrantEnrollmentOrderType);
        _entrantEnrollmentOrderType = entrantEnrollmentOrderType;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        return _used;
    }

    /**
     * @param used Используется. Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        dirty(_used, used);
        _used = used;
    }

    /**
     * @return Отображать основание. Свойство не может быть null.
     */
    @NotNull
    public boolean isBasic()
    {
        return _basic;
    }

    /**
     * @param basic Отображать основание. Свойство не может быть null.
     */
    public void setBasic(boolean basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Отображать приказываю. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommand()
    {
        return _command;
    }

    /**
     * @param command Отображать приказываю. Свойство не может быть null.
     */
    public void setCommand(boolean command)
    {
        dirty(_command, command);
        _command = command;
    }

    /**
     * @return Отображать группу. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroup()
    {
        return _group;
    }

    /**
     * @param group Отображать группу. Свойство не может быть null.
     */
    public void setGroup(boolean group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Выбор старосты. Свойство не может быть null.
     */
    @NotNull
    public boolean isSelectHeadman()
    {
        return _selectHeadman;
    }

    /**
     * @param selectHeadman Выбор старосты. Свойство не может быть null.
     */
    public void setSelectHeadman(boolean selectHeadman)
    {
        dirty(_selectHeadman, selectHeadman);
        _selectHeadman = selectHeadman;
    }

    /**
     * @return Причина и основание. Свойство не может быть null.
     */
    @NotNull
    public boolean isReasonAndBasic()
    {
        return _reasonAndBasic;
    }

    /**
     * @param reasonAndBasic Причина и основание. Свойство не может быть null.
     */
    public void setReasonAndBasic(boolean reasonAndBasic)
    {
        dirty(_reasonAndBasic, reasonAndBasic);
        _reasonAndBasic = reasonAndBasic;
    }

    /**
     * @return Используется дата зачисления. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseEnrollmentDate()
    {
        return _useEnrollmentDate;
    }

    /**
     * @param useEnrollmentDate Используется дата зачисления. Свойство не может быть null.
     */
    public void setUseEnrollmentDate(boolean useEnrollmentDate)
    {
        dirty(_useEnrollmentDate, useEnrollmentDate);
        _useEnrollmentDate = useEnrollmentDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderTypeGen)
        {
            setEnrollmentCampaign(((EnrollmentOrderType)another).getEnrollmentCampaign());
            setEntrantEnrollmentOrderType(((EnrollmentOrderType)another).getEntrantEnrollmentOrderType());
            setPriority(((EnrollmentOrderType)another).getPriority());
            setUsed(((EnrollmentOrderType)another).isUsed());
            setBasic(((EnrollmentOrderType)another).isBasic());
            setCommand(((EnrollmentOrderType)another).isCommand());
            setGroup(((EnrollmentOrderType)another).isGroup());
            setSelectHeadman(((EnrollmentOrderType)another).isSelectHeadman());
            setReasonAndBasic(((EnrollmentOrderType)another).isReasonAndBasic());
            setUseEnrollmentDate(((EnrollmentOrderType)another).isUseEnrollmentDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderType.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "entrantEnrollmentOrderType":
                    return obj.getEntrantEnrollmentOrderType();
                case "priority":
                    return obj.getPriority();
                case "used":
                    return obj.isUsed();
                case "basic":
                    return obj.isBasic();
                case "command":
                    return obj.isCommand();
                case "group":
                    return obj.isGroup();
                case "selectHeadman":
                    return obj.isSelectHeadman();
                case "reasonAndBasic":
                    return obj.isReasonAndBasic();
                case "useEnrollmentDate":
                    return obj.isUseEnrollmentDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "entrantEnrollmentOrderType":
                    obj.setEntrantEnrollmentOrderType((EntrantEnrollmentOrderType) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
                case "basic":
                    obj.setBasic((Boolean) value);
                    return;
                case "command":
                    obj.setCommand((Boolean) value);
                    return;
                case "group":
                    obj.setGroup((Boolean) value);
                    return;
                case "selectHeadman":
                    obj.setSelectHeadman((Boolean) value);
                    return;
                case "reasonAndBasic":
                    obj.setReasonAndBasic((Boolean) value);
                    return;
                case "useEnrollmentDate":
                    obj.setUseEnrollmentDate((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "entrantEnrollmentOrderType":
                        return true;
                case "priority":
                        return true;
                case "used":
                        return true;
                case "basic":
                        return true;
                case "command":
                        return true;
                case "group":
                        return true;
                case "selectHeadman":
                        return true;
                case "reasonAndBasic":
                        return true;
                case "useEnrollmentDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "entrantEnrollmentOrderType":
                    return true;
                case "priority":
                    return true;
                case "used":
                    return true;
                case "basic":
                    return true;
                case "command":
                    return true;
                case "group":
                    return true;
                case "selectHeadman":
                    return true;
                case "reasonAndBasic":
                    return true;
                case "useEnrollmentDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "entrantEnrollmentOrderType":
                    return EntrantEnrollmentOrderType.class;
                case "priority":
                    return Integer.class;
                case "used":
                    return Boolean.class;
                case "basic":
                    return Boolean.class;
                case "command":
                    return Boolean.class;
                case "group":
                    return Boolean.class;
                case "selectHeadman":
                    return Boolean.class;
                case "reasonAndBasic":
                    return Boolean.class;
                case "useEnrollmentDate":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderType> _dslPath = new Path<EnrollmentOrderType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderType");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#getEntrantEnrollmentOrderType()
     */
    public static EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> entrantEnrollmentOrderType()
    {
        return _dslPath.entrantEnrollmentOrderType();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    /**
     * @return Отображать основание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isBasic()
     */
    public static PropertyPath<Boolean> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Отображать приказываю. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isCommand()
     */
    public static PropertyPath<Boolean> command()
    {
        return _dslPath.command();
    }

    /**
     * @return Отображать группу. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isGroup()
     */
    public static PropertyPath<Boolean> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Выбор старосты. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isSelectHeadman()
     */
    public static PropertyPath<Boolean> selectHeadman()
    {
        return _dslPath.selectHeadman();
    }

    /**
     * @return Причина и основание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isReasonAndBasic()
     */
    public static PropertyPath<Boolean> reasonAndBasic()
    {
        return _dslPath.reasonAndBasic();
    }

    /**
     * @return Используется дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isUseEnrollmentDate()
     */
    public static PropertyPath<Boolean> useEnrollmentDate()
    {
        return _dslPath.useEnrollmentDate();
    }

    public static class Path<E extends EnrollmentOrderType> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> _entrantEnrollmentOrderType;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _used;
        private PropertyPath<Boolean> _basic;
        private PropertyPath<Boolean> _command;
        private PropertyPath<Boolean> _group;
        private PropertyPath<Boolean> _selectHeadman;
        private PropertyPath<Boolean> _reasonAndBasic;
        private PropertyPath<Boolean> _useEnrollmentDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#getEntrantEnrollmentOrderType()
     */
        public EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> entrantEnrollmentOrderType()
        {
            if(_entrantEnrollmentOrderType == null )
                _entrantEnrollmentOrderType = new EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType>(L_ENTRANT_ENROLLMENT_ORDER_TYPE, this);
            return _entrantEnrollmentOrderType;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrollmentOrderTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_USED, this);
            return _used;
        }

    /**
     * @return Отображать основание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isBasic()
     */
        public PropertyPath<Boolean> basic()
        {
            if(_basic == null )
                _basic = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_BASIC, this);
            return _basic;
        }

    /**
     * @return Отображать приказываю. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isCommand()
     */
        public PropertyPath<Boolean> command()
        {
            if(_command == null )
                _command = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_COMMAND, this);
            return _command;
        }

    /**
     * @return Отображать группу. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isGroup()
     */
        public PropertyPath<Boolean> group()
        {
            if(_group == null )
                _group = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Выбор старосты. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isSelectHeadman()
     */
        public PropertyPath<Boolean> selectHeadman()
        {
            if(_selectHeadman == null )
                _selectHeadman = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_SELECT_HEADMAN, this);
            return _selectHeadman;
        }

    /**
     * @return Причина и основание. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isReasonAndBasic()
     */
        public PropertyPath<Boolean> reasonAndBasic()
        {
            if(_reasonAndBasic == null )
                _reasonAndBasic = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_REASON_AND_BASIC, this);
            return _reasonAndBasic;
        }

    /**
     * @return Используется дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderType#isUseEnrollmentDate()
     */
        public PropertyPath<Boolean> useEnrollmentDate()
        {
            if(_useEnrollmentDate == null )
                _useEnrollmentDate = new PropertyPath<Boolean>(EnrollmentOrderTypeGen.P_USE_ENROLLMENT_DATE, this);
            return _useEnrollmentDate;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderType.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
