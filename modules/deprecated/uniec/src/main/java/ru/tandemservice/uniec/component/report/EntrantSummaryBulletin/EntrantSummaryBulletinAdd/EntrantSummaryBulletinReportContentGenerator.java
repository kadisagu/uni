/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntrantSummaryBulletin.EntrantSummaryBulletinAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.AccessCourse;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.EntrantSummaryBulletinReport;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author oleyba
 * @since 20.06.2009
 */
class EntrantSummaryBulletinReportContentGenerator
{
    private EntrantSummaryBulletinReport report;
    private Model model;
    private Session session;

    public EntrantSummaryBulletinReportContentGenerator(EntrantSummaryBulletinReport report, Model model, Session session)
    {
        this.report = report;
        this.model = model;
        this.session = session;
    }

    public byte[] generateReportContent(ErrorCollector errors)
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_SUMMARY_BULLETIN);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();
        List<IRtfElement> elementList = document.getElementList();

        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl par = elementFactory.createRtfControl(IRtfData.PAR);
        IRtfControl pard = elementFactory.createRtfControl(IRtfData.PARD);

        // получить шаблон таблицы
        RtfTable emptyTable = (RtfTable) UniRtfUtil.findElement(elementList, "T");

        // и удалить его из документа
        int index = elementList.indexOf(emptyTable);
        elementList.remove(index);

        // подготовить данные
        Map<Person, String> benefitMap = getBenefitMap();
        Map<Entrant, String> accessCourseMap = getAccessCourseMap();
        Map<Person, String> motherWorkplaceMap = getNextOfKinWorkplaceMap(RelationDegreeCodes.MOTHER);
        Map<Person, String> fatherWorkplaceMap = getNextOfKinWorkplaceMap(RelationDegreeCodes.FATHER);

        Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directions = getRequestedEnrollmentDirections();
        if (directions.isEmpty())
        {
            errors.add("При выбранных параметрах отчета абитуриенты не найдены.");
            return null;
        }

        // для каждой таблицы данных

        for (Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> entry : directions.entrySet())
        {
            List<String[]> rows = new ArrayList<>();
            // для каждого выбранного направления приема сформировать строку
            for (RequestedEnrollmentDirection requestedEnrollmentDirection : entry.getValue())
            {
                EntrantRequest entrantRequest = requestedEnrollmentDirection.getEntrantRequest();
                Entrant entrant = entrantRequest.getEntrant();
                Person person = entrant.getPerson();
                IdentityCard identityCard = person.getIdentityCard();

                //номер
                String numberStr = entrantRequest.getStringNumber();

                // ФИО абитуриента
                String fio = person.getFullFio();

                // Пол
                String sex = identityCard.getSex().getShortTitle();

                // Адрес, до населенного пункта, например, обл Свердловская, г Троицк
                String address = null;
                if (identityCard.getAddress() != null && identityCard.getAddress() instanceof AddressDetailed && ((AddressDetailed)identityCard.getAddress()).getSettlement() != null)
                    address = ((AddressDetailed)identityCard.getAddress()).getSettlement().getDirectTitle();
                PersonEduInstitution eduInstitution = person.getPersonEduInstitution();

                // сокращенное название уровня образования, если ступень - то уровня, к которому она относится
                String eduStage = null;

                // год окончания, например, «2009»;
                String graduationYear = null;

                // средний балл аттестата, округленный до десятых, например, «4,0»;
                String averageMark = null;

                // медаль, выводится «да» если есть;
                String medal = null;

                // выводится «да», если красный диплом;
                String diplom = null;

                if (eduInstitution != null)
                {
                    final EducationLevelStage stage = eduInstitution.getEducationLevelStage();
                    eduStage = (stage.getParent() != null) ? stage.getParent().getShortTitle() : stage.getShortTitle();
                    graduationYear = Integer.toString(eduInstitution.getYearEnd());
                    averageMark = getAverageMark(eduInstitution);

                    if (eduInstitution.getGraduationHonour() != null)
                    {
                        String code = eduInstitution.getGraduationHonour().getCode();
                        if (GraduationHonourCodes.GOLD_MEDAL.equals(code))
                        {
                            medal = "да";
                        } else if (GraduationHonourCodes.SILVER_MEDAL.equals(code))
                        {
                            medal = "да";
                        } else if (GraduationHonourCodes.RED_DIPLOMA.equals(code))
                        {
                            diplom = "да";
                        }
                    }
                }
                // название вида конкурса, если целевик, то пишется «целевой»;
                String competitionKind = (requestedEnrollmentDirection.isTargetAdmission()) ? "целевой" : requestedEnrollmentDirection.getCompetitionKind().getTitle();

                // если вид конкурса «вне конкурса», то в этой колонке выводятся льготы;
                String benefit = UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(requestedEnrollmentDirection.getCompetitionKind().getCode()) ? benefitMap.get(person) : null;

                // если по договору, то выводить «да»;
                String contract = (UniDefines.COMPENSATION_TYPE_CONTRACT.equals(requestedEnrollmentDirection.getCompensationType().getCode())) ? "да" : null;

                // Довуз. – подготовительные курсы
                String accessCourse = accessCourseMap.get(entrant);

                // Место работы отца
                String fatherWorkplace = fatherWorkplaceMap.get(person);

                // Место работы матери
                String motherWorkplace = motherWorkplaceMap.get(person);

                // текущее состояние по направлению
                String state = requestedEnrollmentDirection.getState().getTitle();

                rows.add(new String[]{numberStr, fio, sex, address, eduStage, graduationYear, averageMark, medal, diplom, competitionKind, benefit, contract, accessCourse, fatherWorkplace, motherWorkplace, state});
            }

            // сформировать таблицу документа
            RtfTable table = emptyTable.getClone();
            UniRtfUtil.modify(table, rows.toArray(new String[][]{}));

            // заполнить ее заголовок
            RtfInjectModifier injectModifier = new RtfInjectModifier();
            injectModifier.put("header", getHeader(entry.getKey()));
            injectModifier.modify(Collections.<IRtfElement>singletonList(table));

            // и вставить в документ
            elementList.add(index++, table);
            elementList.add(index++, pard);
            elementList.add(index++, par);
        }
        return RtfUtil.toByteArray(document);
    }

    /**
     * Место работы родственника с соотв. степенью родства.
     * Название организации и через запятую должность - например, ООО «Левис», механик.
     *
     * @param relationDegreeCode - код степени родства
     * @return карта по персоне
     */
    private Map<Person, String> getNextOfKinWorkplaceMap(String relationDegreeCode)
    {
        MQBuilder builder = new MQBuilder(PersonNextOfKin.ENTITY_CLASS, "pnk");
        builder.add(MQExpression.eq("pnk", PersonNextOfKin.L_RELATION_DEGREE + ".code", relationDegreeCode));
        builder.addDomain("red", RequestedEnrollmentDirection.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + ".id", "pnk", PersonNextOfKin.L_PERSON + ".id"));
        filterByReportParams(builder, "red");

        Map<Person, String> result = new HashMap<>();
        for (PersonNextOfKin nextOfKin : builder.<PersonNextOfKin>getResultList(session))
        {
            result.put(nextOfKin.getPerson(), UniStringUtils.joinWithSeparator(", ", nextOfKin.getEmploymentPlace(), nextOfKin.getPost()));
        }
        return result;
    }

    /**
     * Подготовительные курсы - сокращенные названия через запятую
     *
     * @return карта по абитуриенту
     */
    private Map<Entrant, String> getAccessCourseMap()
    {
        UniDaoFacade.getCoreDao().getCatalogItemList(AccessCourse.class);
        MQBuilder builder = new MQBuilder(EntrantAccessCourse.ENTITY_CLASS, "ac");
        builder.addJoinFetch("ac", EntrantAccessCourse.entrant().s(), "e");
        builder.addDomain("red", RequestedEnrollmentDirection.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + ".id", "ac", EntrantAccessCourse.L_ENTRANT + ".id"));
        filterByReportParams(builder, "red");

        Map<Entrant, String> result = new HashMap<>();
        for (EntrantAccessCourse entrantAccessCourse : builder.<EntrantAccessCourse>getResultList(session))
        {
            Entrant entrant = entrantAccessCourse.getEntrant();
            result.put(entrant, UniStringUtils.joinWithSeparator(", ", result.get(entrant), entrantAccessCourse.getCourse().getShortTitle()));
        }
        return result;
    }

    /**
     * @return сокращенные названия льгот через запятую
     */
    private Map<Person, String> getBenefitMap()
    {
        UniDaoFacade.getCoreDao().getCatalogItemList(Benefit.class);
        MQBuilder builder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "pb");
        builder.addDomain("red", RequestedEnrollmentDirection.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + ".id", "pb", PersonBenefit.L_PERSON + ".id"));
        filterByReportParams(builder, "red");

        Map<Person, String> result = new HashMap<>();
        for (PersonBenefit personBenefit : builder.<PersonBenefit>getResultList(session))
        {
            Person person = personBenefit.getPerson();
            result.put(person, UniStringUtils.joinWithSeparator(", ", result.get(person), personBenefit.getBenefit().getShortTitle()));
        }
        return result;
    }

    /**
     * @return выбранные направления приема, сгруппированные по направлениям приема
     */
    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> getRequestedEnrollmentDirections()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        filterByReportParams(builder, "red");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.enrollmentDirection().s(), "ed");
        builder.addJoinFetch("ed", EnrollmentDirection.educationOrgUnit().s(), "ou");
        builder.addJoinFetch("ou", EducationOrgUnit.educationLevelHighSchool().s(), "hs");
        builder.addJoinFetch("hs", EducationLevelsHighSchool.educationLevel().s(), "el");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.compensationType().s(), "ct");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.competitionKind().s(), "ck");
        builder.addJoinFetch("red", RequestedEnrollmentDirection.entrantRequest().s(), "eq");
        builder.addJoinFetch("eq", EntrantRequest.entrant().s(), "e");
        builder.addJoinFetch("e", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "idc");
        builder.addLeftJoinFetch("idc", IdentityCard.address().s(), "address");
        builder.addOrder("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_NUMBER);

        Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> result = SafeMap.get(ArrayList.class);
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : builder.<RequestedEnrollmentDirection>getResultList(session)) {
            result.get(requestedEnrollmentDirection.getEnrollmentDirection()).add(requestedEnrollmentDirection);
        }
        return result;
    }

    /**
     * Накладывает фильтр по параметрам отчета - условия накладываются от альяса выбранного напр. приема
     *
     * @param builder        - билдер
     * @param reqEnrDirAlias - альяс выбранного напр. приема
     */
    private void filterByReportParams(MQBuilder builder, String reqEnrDirAlias)
    {
        builder.add(UniMQExpression.betweenDate(reqEnrDirAlias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, report.getDateFrom(), report.getDateTo()));
        builder.add(MQExpression.eq(reqEnrDirAlias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, report.getEnrollmentCampaign()));

        if (report.getCompensationType() != null)
        {
            builder.add(MQExpression.eq(reqEnrDirAlias, RequestedEnrollmentDirection.L_COMPENSATION_TYPE, report.getCompensationType()));
        }
        if (model.getStudentCategoryList() != null && !model.getStudentCategoryList().isEmpty())
        {
            builder.add(MQExpression.in(reqEnrDirAlias, RequestedEnrollmentDirection.L_STUDENT_CATEGORY + ".id", CommonBaseUtil.getPropertiesList(model.getStudentCategoryList(), "id")));
        }
        if (report.getEnrollmentDirection() != null)
        {
            builder.add(MQExpression.eq(reqEnrDirAlias, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, report.getEnrollmentDirection()));
        }
        else
        {
            if (!model.getQualificationList().isEmpty())
                builder.add(MQExpression.in(reqEnrDirAlias, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));
            if (!model.getDevelopFormList().isEmpty())
                builder.add(MQExpression.in(reqEnrDirAlias, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().s(), model.getDevelopFormList()));
            if (!model.getDevelopConditionList().isEmpty())
                builder.add(MQExpression.in(reqEnrDirAlias, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().s(), model.getDevelopConditionList()));
        }
        builder.add(MQExpression.eq(reqEnrDirAlias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
    }

    /**
     * @param enrollmentDirection направление приема
     * @return заголовок отчета
     *         [по специальности/по направлению] [оксо. название направления подготовки] ([форма обучения], [возмещение затрат]) [формирующее подр.] ([территориальное подр.])
     */
    private String getHeader(EnrollmentDirection enrollmentDirection)
    {
        EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();
        EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();
        StructureEducationLevels levelType = educationLevelHighSchool.getEducationLevel().getLevelType();

        DevelopForm developForm = educationOrgUnit.getDevelopForm();
        CompensationType compensationType = report.getCompensationType();
        String formativeOrgUnitTitle = educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() == null ? educationOrgUnit.getFormativeOrgUnit().getTitle() : educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle();
        String territorialOrgUnitTitle = (educationOrgUnit.getTerritorialOrgUnit() != null) ? educationOrgUnit.getTerritorialOrgUnit().getNominativeCaseTitle() : null;

        StringBuilder result = new StringBuilder();
        result.append("по ").append(levelType.getDativeCaseSimpleShortTitle()).append(" ");
        result.append(educationLevelHighSchool.getPrintTitle()).append(" ");
        result.append("(форма ").append(StringUtils.lowerCase(developForm.getTitle()));
        if (compensationType != null)
        {
            result.append(", ").append(compensationType.getShortTitle());
        }
        result.append(")");
        if (formativeOrgUnitTitle != null)
        {
            result.append(" ").append(formativeOrgUnitTitle);
        }
        if (territorialOrgUnitTitle != null)
        {
            result.append(" (").append(territorialOrgUnitTitle).append(")");
        }
        return result.toString();
    }

    /**
     * @param lastEduInstitution учебное заведение
     * @return средний балл по аттестату
     */
    private String getAverageMark(PersonEduInstitution lastEduInstitution)
    {
        Double averageMark = lastEduInstitution.getAverageMark();
        return (averageMark != null) ? String.format("%.1f", averageMark) : null;
    }

}
