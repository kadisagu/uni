/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionAddEdit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;
import ru.tandemservice.uniec.entity.settings.UseExternalOrgUnitForTA;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;
import ru.tandemservice.uniec.util.EntrantUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 13.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.setAddForm(model.getRequestedEnrollmentDirection().getId() == null);
        model.setEditForm(!model.isAddForm());

        if (model.isAddForm())
        {
            // форма добавления
            model.setEntrantRequest(getNotNull(EntrantRequest.class, model.getEntrantRequest().getId()));
            model.getEntrantRequest().getEntrant().getEnrollmentCampaign();
            model.setStateExamRestriction(model.getEnrollmentCampaign().isStateExamRestriction());
            model.setTerritorialOrgUnit(TopOrgUnit.getInstance());
        }
        else
        {
            // форма редактирования
            model.setRequestedEnrollmentDirection(getNotNull(RequestedEnrollmentDirection.class, model.getRequestedEnrollmentDirection().getId()));
            model.setEntrantRequest(model.getRequestedEnrollmentDirection().getEntrantRequest());
            model.setRegNumber(model.getRequestedEnrollmentDirection().getNumber());
            model.setOldCompensationType(model.getRequestedEnrollmentDirection().getCompensationType());
            model.setOldCompetitionKind(model.getRequestedEnrollmentDirection().getCompetitionKind());

            // загружаем профильные знания
            List<RequestedProfileKnowledge> requestedProfileKnowledgeList = getList(RequestedProfileKnowledge.class, RequestedProfileKnowledge.L_REQUESTED_ENROLLMENT_DIRECTION, model.getRequestedEnrollmentDirection());
            List<ProfileKnowledge> profileKnowledgeList = new ArrayList<>();
            for (RequestedProfileKnowledge requestedProfileKnowledge : requestedProfileKnowledgeList)
                profileKnowledgeList.add(requestedProfileKnowledge.getProfileKnowledge());
            model.setProfileKnowledgeList(profileKnowledgeList);

            // загружаем значения по умолчанию для селектов
            final EducationOrgUnit educationOrgUnit = model.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit();
            model.setFormativeOrgUnit(educationOrgUnit.getFormativeOrgUnit());
            model.setTerritorialOrgUnit(educationOrgUnit.getTerritorialOrgUnit());
            model.setDevelopForm(educationOrgUnit.getDevelopForm());
            model.setDevelopTech(educationOrgUnit.getDevelopTech());
            model.setDevelopCondition(educationOrgUnit.getDevelopCondition());
            model.setDevelopPeriod(educationOrgUnit.getDevelopPeriod());
            model.setEducationLevelsHighSchool(educationOrgUnit.getEducationLevelHighSchool());
        }

        // модели селектов
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setCompensationTypeModel(EnrollmentDirectionUtil.createCompensationTypeModel(model));
        model.setUseExternalOrgUnitForTA(get(UseExternalOrgUnitForTA.class, UseExternalOrgUnitForTA.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        model.setExternalOrgUnitModel(new LazySimpleSelectModel<>(ExternalOrgUnit.class, ExternalOrgUnit.P_TITLE_WITH_LEGAL_FORM));
        model.setTargetAdmissionKindList(UniecDAOFacade.getTargetAdmissionDao().getTargetAdmissionKindOptionList(model.getEnrollmentCampaign()));
        model.setProfileKnowledgeListModel(new LazySimpleSelectModel<>(ProfileKnowledge.class));
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));
        model.setCompetitionKindModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getEducationLevelsHighSchool() == null) return ListResult.getEmpty();

                Criteria criteria = getSession().createCriteria(Qualification2CompetitionKindRelation.class);
                criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_ENROLLMENT_CAMPAIGN, model.getEntrantRequest().getEntrant().getEnrollmentCampaign()));
                criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_QUALIFICATION, model.getEducationLevelsHighSchool().getEducationLevel().getQualification()));
                criteria.add(Restrictions.eq(Qualification2CompetitionKindRelation.L_STUDENT_CATEGORY, model.getRequestedEnrollmentDirection().getStudentCategory()));
                criteria.setProjection(Projections.distinct(Projections.property(Qualification2CompetitionKindRelation.L_COMPETITION_KIND)));
                List<CompetitionKind> list = criteria.list();

                EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());
                if (enrollmentDirection != null && !enrollmentDirection.isInterview())
                    list.remove(getCatalogItem(CompetitionKind.class, UniecDefines.COMPETITION_KIND_INTERVIEW));

                return new ListResult<>(list, list.size());
            }
        });

        // надпись на форме про сданные оригиналы документов
        {
            Criteria c = getSession().createCriteria(RequestedEnrollmentDirection.class, "r");
            c.createAlias("r." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "req");
            c.add(Restrictions.eq("req." + EntrantRequest.L_ENTRANT, model.getEntrantRequest().getEntrant()));
            c.add(Restrictions.eq("r." + RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
            if (model.isEditForm())
                c.add(Restrictions.ne("r." + RequestedEnrollmentDirection.P_ID, model.getRequestedEnrollmentDirection().getId()));
            List<RequestedEnrollmentDirection> list = c.list();
            for (RequestedEnrollmentDirection oldDirection : list)
            {
                model.setOriginalHandedIn(true);
                StructureEducationLevels levelType = oldDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
                model.setOriginalPlace(levelType.getDativeCaseShortTitle() + " " + oldDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle() + " (заявление №" + oldDirection.getEntrantRequest().getStringNumber() + ")");
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean prepareProfileDisciplineList(Model model)
    {
        EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());
        if (enrollmentDirection == null) return false;
        StudentCategory studentCategory = model.getRequestedEnrollmentDirection().getStudentCategory();
        if (studentCategory == null) return false;
        ExamSet examSet = ExamSetUtil.getExamSet(enrollmentDirection, studentCategory);
        if (examSet == null) return false;

        List<Discipline2RealizationWayRelation> profileDisciplineList = examSet.getProfileDisciplineList();
        model.setProfileDisciplineList(profileDisciplineList);

        // заполняем в матрице оценки по профильным предметам, если они уже есть
        Criteria c = getSession().createCriteria(ProfileExaminationMark.class);
        c.add(Restrictions.eq(ProfileExaminationMark.L_ENTRANT, model.getEntrantRequest().getEntrant()));

        Map<CellCoordinates, Integer> matrixData = model.getMatrixData();
        for (ProfileExaminationMark mark : (List<ProfileExaminationMark>) c.list())
            if (profileDisciplineList.contains(mark.getDiscipline()))
                matrixData.put(new CellCoordinates(mark.getDiscipline().getId(), Model.MARK_COLUMN_ID), mark.getMark());

        return profileDisciplineList.size() > 0;
    }

    @Override
    public void update(Model model, ErrorCollector errorCollector)
    {
        final Session session = getSession();

        EntrantUtil.lockEnrollmentCampaign(session, model.getEntrantRequest().getEntrant());

        // обновляем настройки приемной кампании
        session.refresh(model.getEnrollmentCampaign());
        FlushMode defaultFlushMode = session.getFlushMode();
        RequestedEnrollmentDirection requestedEnrollmentDirection = model.getRequestedEnrollmentDirection();

        // чтобы выбранное направление приема, которое изменилось на форме, не ушло в базу при флаше - сработает constraint
        session.setFlushMode(FlushMode.MANUAL);

        try
        {
            // в силу кода выбора параметров на форме с помощью EnrollmentDirectionUtil (см метод prepare), такой ошибки быть не должно
            // однако надо проверить, чтобы не было NPE
            EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, session);
            if (enrollmentDirection == null)
                throw new ApplicationException("Направление приема с указанными параметрами не найдено.");
            session.refresh(enrollmentDirection);

            if (!enrollmentDirection.isContract() && requestedEnrollmentDirection.getCompensationType() != null && !requestedEnrollmentDirection.getCompensationType().isBudget())
                throw new ApplicationException("Нет приема на контракт по выбранному направлению приема.");

            if (!enrollmentDirection.isBudget() && requestedEnrollmentDirection.getCompensationType() != null && requestedEnrollmentDirection.getCompensationType().isBudget())
                throw new ApplicationException("Нет приема на бюджет по выбранному направлению приема.");

            // если целевой прием не указан, то и не должно быть организации и типа целевого приема
            if (!requestedEnrollmentDirection.isTargetAdmission())
            {
                requestedEnrollmentDirection.setExternalOrgUnit(null);
                requestedEnrollmentDirection.setTargetAdmissionKind(null);
            }

            // находим максимальный приоритет среди всех существующих в базе ВНП среди всех заявлений этого абитуриента в рамках вида возмещения затрат
            // все новым ВНП будут добавляться в конец списка приоритетов ВНП среди всех заявлений
            int budgetMaxPriority = Integer.MIN_VALUE;
            int contractMaxPriority = Integer.MIN_VALUE;
            for (RequestedEnrollmentDirection direction : getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().entrant(), model.getEntrantRequest().getEntrant()))
            {
                int priority = direction.getPriorityPerEntrant();
                if (direction.getCompensationType().isBudget())
                {
                    if (priority > budgetMaxPriority)
                        budgetMaxPriority = priority;
                } else
                {
                    if (priority > contractMaxPriority)
                        contractMaxPriority = priority;
                }
            }
            budgetMaxPriority++;
            contractMaxPriority++;
            boolean needPriorityPerEntrantNormalization = false;

            if (model.isEditForm())
            {
                // форма редактирования

                // проверяем, не выбрано ли такое уже (направление приема + вид затрат)
                MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
                if (model.getEnrollmentCampaign().isDirectionPerCompTypeDiff())
                    builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, requestedEnrollmentDirection.getCompensationType()));
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrantRequest().getEntrant()));
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirection));
                builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.P_ID, requestedEnrollmentDirection.getId()));
                if (builder.getResultCount(session) > 0)
                {
                    errorCollector.add("Направление приема с указанными параметрами уже выбрано.");
                    return;
                }

                // VALIDATE

                if (model.getRegNumber() == null)
                {
                    // определение максимального рег. номера
                    int maxReg = getMaxRegNumber(enrollmentDirection, session);
                    requestedEnrollmentDirection.setNumber(maxReg + 1);
                } else if (!model.getRegNumber().equals(requestedEnrollmentDirection.getNumber()) || !enrollmentDirection.equals(requestedEnrollmentDirection.getEnrollmentDirection()))
                {
                    // либо явное указание рег. номера на форме при создании, либо его редактирование
                    Criteria c = session.createCriteria(RequestedEnrollmentDirection.class);
                    c.add(Restrictions.eq(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirection));
                    c.add(Restrictions.eq(RequestedEnrollmentDirection.P_NUMBER, model.getRegNumber()));
                    RequestedEnrollmentDirection checkDirection = (RequestedEnrollmentDirection) c.uniqueResult();
                    if (checkDirection == null)
                        requestedEnrollmentDirection.setNumber(model.getRegNumber());
                    else
                        errorCollector.add("№ на направлении/специальности «" + model.getRegNumber() + "» уже занят абитуриентом «" + checkDirection.getEntrantRequest().getEntrant().getPerson().getFullFio() + "».", "regNumber");
                }

                // можно ли добавить такие направления
                EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), Collections.singletonList(requestedEnrollmentDirection), model.getEntrantRequest());

                // если валидация не прошла - уходим
                if (errorCollector.hasErrors())
                    return;

                // UPDATE
                // если сменился вид затрат, то должен смениться приоритет ВНП среди всех заявлений в разрезе вида затрат
                if (!model.getOldCompensationType().equals(requestedEnrollmentDirection.getCompensationType()))
                {
                    requestedEnrollmentDirection.setPriorityPerEntrant(requestedEnrollmentDirection.getCompensationType().isBudget() ? budgetMaxPriority++ : contractMaxPriority++);
                    needPriorityPerEntrantNormalization = true;
                }

                if (!model.getOldCompetitionKind().equals(requestedEnrollmentDirection.getCompetitionKind()))
                {
                    // при смене вида конкурса, надо удалить все выбранные вступительные испытания
                    requestedEnrollmentDirection.setProfileChosenEntranceDiscipline(null);
                    session.update(requestedEnrollmentDirection);
                    session.flush();
                    for (ChosenEntranceDiscipline discipline : getList(ChosenEntranceDiscipline.class, ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, requestedEnrollmentDirection))
                        session.delete(discipline);
                }

                // сдаем оригиналы по направлению приема
                EcEntrantManager.instance().dao().updateOriginalDocuments(requestedEnrollmentDirection, requestedEnrollmentDirection.isOriginalDocumentHandedIn());

                // актуализируем оценки по профильным предметам
                updateProfileExaminationMark(model);

                // актуализируем профильные знания
                updateProfileKnowledge(requestedEnrollmentDirection, model.getProfileKnowledgeList());

                requestedEnrollmentDirection.setEnrollmentDirection(enrollmentDirection);

                session.update(requestedEnrollmentDirection);

                if (needPriorityPerEntrantNormalization)
                    EcEntrantManager.instance().dao().doNormalizePriorityPerEntrant(model.getEntrantRequest().getEntrant());
            } else
            {
                // форма добавления
                List<RequestedEnrollmentDirection> newDirections = new ArrayList<>();

                // VALIDATE

                // получаем список направлений приема, которые надо добавить
                // исключаются направления, которые нельзя добавлять
                // учитываются конкурсные группы и возможность участия в конкурсе на контракт
                List<MultiKey> list = getAddingEnrollmentDirection(model, enrollmentDirection, errorCollector);
                if (errorCollector.hasErrors()) return;

                // определяем максимальный приоритет направления приема
                int maxPriority = getMaxPriority(model.getEntrantRequest());

                // определяем максимальные номера для направлений
                Map<EnrollmentDirection, MutableInt> maxRegNumberMap = SafeMap.get(key -> new MutableInt(getMaxRegNumber(key, session)));


                // определение максимального рег. номера
                // по всем направлениям приема и видам возмещения затрат
                EntrantState activeState = getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_ACTIVE_CODE);
                for (MultiKey item : list)
                {
                    // создаем новое выбранное направление приема и заполняем значениями по умолчанию
                    RequestedEnrollmentDirection direction = new RequestedEnrollmentDirection();
                    direction.update(requestedEnrollmentDirection);

                    direction.setEnrollmentDirection((EnrollmentDirection) item.getKey(0));
                    direction.setCompensationType((CompensationType) item.getKey(1));
                    direction.setEntrantRequest(model.getEntrantRequest());
                    direction.setRegDate(new Date());
                    direction.setPriority(++maxPriority);
                    direction.setPriorityPerEntrant(direction.getCompensationType().isBudget() ? budgetMaxPriority++ : contractMaxPriority++);
                    direction.setState(activeState);

                    // берем следующий регистрационный номер
                    MutableInt maxRegNumber = maxRegNumberMap.get(direction.getEnrollmentDirection());
                    maxRegNumber.increment();
                    direction.setNumber(maxRegNumber.intValue());

                    newDirections.add(direction);
                }

                // валидируем выбранные направления
                EntrantRequestAddEditUtil.validateRequestedDirections(getSession(), newDirections, model.getEntrantRequest());
                // если валидация не прошла - уходим
                if (errorCollector.hasErrors())
                    return;

                // UPDATE

                for (RequestedEnrollmentDirection direction : newDirections)
                {
                    // то сохраняем новые направления
                    session.save(direction);

                    if (requestedEnrollmentDirection.isOriginalDocumentHandedIn())
                        EcEntrantManager.instance().dao().updateOriginalDocuments(direction, true);

                    // добавляем все профильные знания
                    updateProfileKnowledge(direction, model.getProfileKnowledgeList());
                }

                // актуализируем оценки по профильным предметам
                updateProfileExaminationMark(model);
            }
        } finally
        {
            session.setFlushMode(defaultFlushMode);
        }
    }

    private List<MultiKey> getAddingEnrollmentDirection(Model model, EnrollmentDirection direction, ErrorCollector errorCollector)
    {
        List<EnrollmentDirection> result;

        // если
        // 1. используются конкурсные группы
        // 2. выбранное направление приема находится в одной из них
        // 3. выбрана категория обучающегося "Студент" или "Слушатель"
        // 4. выбрано условие освоения "Полный срок",
        //    то добавляем все направления приема с полным сроком из этой конкурсной группы
        String studentCategoryCode = model.getRequestedEnrollmentDirection().getStudentCategory().getCode();
        boolean studentOrListener = UniDefines.STUDENT_CATEGORY_STUDENT.equals(studentCategoryCode) || UniDefines.STUDENT_CATEGORY_LISTENER.equals(studentCategoryCode);
        boolean fullTime = UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(direction.getEducationOrgUnit().getDevelopCondition().getCode());

        if (model.getEnrollmentCampaign().isUseCompetitionGroup() && direction.getCompetitionGroup() != null && studentOrListener && fullTime)
        {
            result = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d")
            .add(MQExpression.eq("d", EnrollmentDirection.L_COMPETITION_GROUP, direction.getCompetitionGroup()))
            .add(MQExpression.eq("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION + "." + DevelopCondition.P_CODE, UniDefines.DEVELOP_CONDITION_FULL_TIME))
            .getResultList(getSession());
            Collections.sort(result, ITitled.TITLED_COMPARATOR);
        } else
        {
            result = Arrays.asList(direction);
        }

        List<RequestedEnrollmentDirection> selectedList = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest());
        List<MultiKey> selectableDirections = EntrantRequestAddEditUtil.getAddingEnrollmentDirections(getSession(),
            result,
            model.isContractAutoCompetition() ? null : model.getRequestedEnrollmentDirection().getCompensationType(),
                selectedList,
                model.getEnrollmentCampaign().isDirectionPerCompTypeDiff(),
                model.isStateExamRestriction(),
                model.getEntrantRequest().getEntrant(),
                new ArrayList<>()
        );

        if (selectableDirections.isEmpty())
        {
            if (model.isStateExamRestriction())
                errorCollector.add("Нельзя добавить направление подготовки (специальность) абитуриенту, возможно оно уже выбрано или не покрывается свидетельством ЕГЭ.");
            else
                errorCollector.add("Нельзя добавить направление подготовки (специальность) абитуриенту, возможно оно уже выбрано.");
            return null;
        }

        return selectableDirections;
    }

    private int getMaxPriority(EntrantRequest entrantRequest)
    {
        Number priorityNumber = (Number) getSession().createCriteria(RequestedEnrollmentDirection.class)
        .add(Restrictions.eq(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest))
        .setProjection(Projections.max(RequestedEnrollmentDirection.P_PRIORITY))
        .uniqueResult();
        return priorityNumber == null ? 0 : priorityNumber.intValue();
    }

    public static int getMaxRegNumber(EnrollmentDirection enrollmentDirection, Session session)
    {
        Number regNumber = (Number) session.createCriteria(RequestedEnrollmentDirection.class)
        .add(Restrictions.eq(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirection))
        .setProjection(Projections.max(RequestedEnrollmentDirection.P_NUMBER))
        .uniqueResult();
        return regNumber == null ? 0 : regNumber.intValue();
    }

    private void updateProfileExaminationMark(Model model)
    {
        Session session = getSession();
        // актуализируем оценки по профильным предметам
        for (Map.Entry<CellCoordinates, Integer> entry : model.getMatrixData().entrySet())
        {
            final Long disciplineId = entry.getKey().getRowId();
            Discipline2RealizationWayRelation discipline = (Discipline2RealizationWayRelation) session.get(Discipline2RealizationWayRelation.class, disciplineId);
            if (discipline == null)
                throw new ObjectNotFoundException(disciplineId, Discipline2RealizationWayRelation.class.getName());

            Criteria c = session.createCriteria(ProfileExaminationMark.class);
            c.add(Restrictions.eq(ProfileExaminationMark.L_ENTRANT, model.getEntrantRequest().getEntrant()));
            c.add(Restrictions.eq(ProfileExaminationMark.L_DISCIPLINE, discipline));
            ProfileExaminationMark savedProfileMark = (ProfileExaminationMark) c.uniqueResult();

            if (entry.getValue() != null)
            {
                // если такой оценки нет, то сохраняем ее, иначе обновляем значение оценки
                if (savedProfileMark == null)
                {
                    ProfileExaminationMark profileMark = new ProfileExaminationMark();
                    profileMark.setMark(entry.getValue());
                    profileMark.setDiscipline(discipline);
                    profileMark.setEntrant(model.getEntrantRequest().getEntrant());
                    session.save(profileMark);
                } else
                {
                    savedProfileMark.setMark(entry.getValue());
                    session.update(savedProfileMark);
                }
            }
            else
            {
                if (savedProfileMark != null)
                    session.delete(savedProfileMark);
            }
        }
    }

    private void updateProfileKnowledge(RequestedEnrollmentDirection direction, List<ProfileKnowledge> profileKnowledgeList)
    {
        Session session = getSession();
        Map<ProfileKnowledge, RequestedProfileKnowledge> requestedProfileKnowledgMap = new HashMap<>();

        // загружаем профильные знания, которые есть в указанном выбранном направлении приема
        if (direction.getId() != null)
            for (RequestedProfileKnowledge requestedProfileKnowledge : getList(RequestedProfileKnowledge.class, RequestedProfileKnowledge.requestedEnrollmentDirection().s(), direction))
                requestedProfileKnowledgMap.put(requestedProfileKnowledge.getProfileKnowledge(), requestedProfileKnowledge);

        if (profileKnowledgeList != null)
            for (ProfileKnowledge profileKnowledge : profileKnowledgeList)
            {
                // сейчас надо сохранить это профильное знание, если оно еще не сохранено
                if (requestedProfileKnowledgMap.remove(profileKnowledge) == null)
                {
                    // сохраняем
                    RequestedProfileKnowledge requestedProfileKnowledge = new RequestedProfileKnowledge();
                    requestedProfileKnowledge.setProfileKnowledge(profileKnowledge);
                    requestedProfileKnowledge.setRequestedEnrollmentDirection(direction);
                    session.save(requestedProfileKnowledge);
                }
            }

        // удаляем не выбранные в списке, но существующие в базе профильные знания (в requestedProfileKnowledgMap остались только такие)
        for (RequestedProfileKnowledge requestedProfileKnowledge : requestedProfileKnowledgMap.values())
            session.delete(requestedProfileKnowledge);
    }
}
