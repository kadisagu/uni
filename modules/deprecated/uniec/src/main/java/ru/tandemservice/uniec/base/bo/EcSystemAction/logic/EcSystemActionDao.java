/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcSystemAction.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.ExamGroupLogic;
import ru.tandemservice.uniec.entity.catalog.ExamGroupType;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;

import java.sql.PreparedStatement;
import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class EcSystemActionDao extends UniBaseDao implements IEcSystemActionDao
{
    @Override
    public void updateEntrantData()
    {
        Session session = getSession();
        for (Entrant entrant : getList(Entrant.class))
            session.update(entrant);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    @Override
    public void setRequestedEnrollmentDirectionProfileDisciplines()
    {
        Session session = getSession();
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        // System.out.println("Start updating exam sets and requested enrollment directions");

        try
        {
            PreparedStatement statement = session.connection().prepareStatement("update requestedenrollmentdirection_t set prflchsnentrncdscpln_id = ? where id = ?");
            EntranceDisciplineKind highSchoolKind = (EntranceDisciplineKind) session.createCriteria(EntranceDisciplineKind.class).add(Restrictions.eq(EntranceDisciplineKind.code().s(), UniecDefines.ENTRANCE_DISCIPLINE_KIND_HIGH_SCHOOL)).uniqueResult();
            List<StudentCategory> studentCategories = session.createCriteria(StudentCategory.class).list();

            // для каждой приемной кампании
            for (EnrollmentCampaign enrollmentCampaign : (List<EnrollmentCampaign>) session.createCriteria(EnrollmentCampaign.class).list())
            {
                // для каждого набора вступительных испытаний
                for (ExamSet examSet : UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values())
                {
                    // ищем профильные испытания
                    ExamSetItem firstProfileExamSetItem = null;
                    for (ExamSetItem examSetItem : examSet.getSetItemList())
                    {
                        if (examSetItem.isProfile())
                        {
                            // всем кроме первого попавшегося ставим тип «Назначено вузом»,
                            if (firstProfileExamSetItem != null)
                            {
                                for (EntranceDiscipline entranceDiscipline : examSetItem.getList())
                                {
                                    entranceDiscipline.setKind(highSchoolKind);
                                    session.update(entranceDiscipline);
                                }
                            } else
                            {
                                firstProfileExamSetItem = examSetItem;
                            }

                            // удаляем испытания по выбору
                            for (EntranceDiscipline entranceDiscipline : examSetItem.getList())
                            {
                                for (EntranceDiscipline2SetDisciplineRelation relation : (List<EntranceDiscipline2SetDisciplineRelation>) session.createCriteria(EntranceDiscipline2SetDisciplineRelation.class).add(Restrictions.eq(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().s(), entranceDiscipline)).list())
                                {
                                    session.delete(relation);
                                }
                            }
                        }
                    }
                }

                // получаем структуру наборов, выбранных вступительных направлений и испытаний
                MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "direction");
                builder.add(MQExpression.eq("direction", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().s(), enrollmentCampaign));
                EntrantDataUtil entrantDataUtil = new EntrantDataUtil(session, enrollmentCampaign, builder);
                Map<StudentCategory, Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>>> direction2DisciplinesMap = getDirection2DisciplinesMap(session, enrollmentCampaign, studentCategories);

                // для каждого выбранного направления
                builder.addJoinFetch("direction", RequestedEnrollmentDirection.enrollmentDirection().s(), "enrollmentDirection");
                for (RequestedEnrollmentDirection requestedEnrollmentDirection : builder.<RequestedEnrollmentDirection>getResultList(session))
                {
                    // получаем выбранные вступительные испытания
                    Set<ChosenEntranceDiscipline> chosenEntranceDisciplines = entrantDataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection);
                    List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> entranceDisciplineRow = direction2DisciplinesMap.get(requestedEnrollmentDirection.getStudentCategory()).get(requestedEnrollmentDirection.getEnrollmentDirection());

                    if (chosenEntranceDisciplines.isEmpty())
                    {
                        continue;
                    }

                    // ищем среди них профильное
                    int profileIndex = getProfileIndex(entranceDisciplineRow);
                    if (profileIndex != -1)
                    {
                        // записываем в выбранное направление
                        ChosenEntranceDiscipline profileChosenEntranceDiscipline = MarkDistributionUtil.getNearestDistribution(chosenEntranceDisciplines, entranceDisciplineRow)[profileIndex];
                        if (profileChosenEntranceDiscipline != null)
                        {
                            statement.setLong(1, profileChosenEntranceDiscipline.getId());
                            statement.setLong(2, requestedEnrollmentDirection.getId());
                            statement.execute();
                        } else
                        {
                            // System.out.print(requestedEnrollmentDirection.getId() + ", ");
                        }
                    }
                }
            }

            // System.out.println("Finish updating exam sets and requested enrollment directions");
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        } finally
        {
            eventLock.release();
        }
    }

    @SuppressWarnings("deprecation")
    private Map<StudentCategory, Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>>> getDirection2DisciplinesMap(Session session, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategories)
    {
        Map<StudentCategory, Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>>> result = new HashMap<StudentCategory, Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>>>();
        for (StudentCategory studentCategory : studentCategories)
        {
            result.put(studentCategory, ExamSetUtil.getDirection2disciplinesMap(session, enrollmentCampaign, Collections.singletonList(studentCategory), null));
        }
        return result;
    }

    private int getProfileIndex(List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> entranceDisciplineRow)
    {
        for (int i = 0; i < entranceDisciplineRow.size(); i++)
        {
            if (entranceDisciplineRow.get(i).getFirst().getCode().equals(UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE))
            {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void doCorrectExamGroupNamesMechanism1()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            Session session = getSession();
            MQBuilder currentLogicUsingBuilder = new MQBuilder(CurrentExamGroupLogic.ENTITY_CLASS, "c");
            currentLogicUsingBuilder.add(MQExpression.eq("c", CurrentExamGroupLogic.L_EXAM_GROUP_LOGIC + "." + ExamGroupLogic.P_CODE, UniecDefines.EXAM_GROUP_NAME_MECH1));
            List<CurrentExamGroupLogic> currentExamGroupLogicList = currentLogicUsingBuilder.getResultList(getSession());
            for (CurrentExamGroupLogic currentItem : currentExamGroupLogicList)
            {
                EnrollmentCampaign enrollmentCampaign = currentItem.getEnrollmentCampaign();
                MQBuilder groupBuilder = new MQBuilder(ExamGroup.ENTITY_CLASS, "e");
                groupBuilder.add(MQExpression.eq("e", ExamGroup.L_EXAM_GROUP_SET + "." + ExamGroupSet.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
                List<ExamGroup> examGroupList = groupBuilder.getResultList(getSession());
                for (ExamGroup examGroupItem : examGroupList)
                {
                    String key = examGroupItem.getKey();
                    String[] keys = key.split(";");
                    ExamGroupType examGroupType = get(ExamGroupType.class, Long.parseLong(keys[0]));
                    CompensationType compensationType = get(CompensationType.class, Long.parseLong(keys[1]));
                    DevelopForm developForm = get(DevelopForm.class, Long.parseLong(keys[2]));
                    String oldShortTitle = examGroupItem.getShortTitle();
                    if (oldShortTitle.split("-").length > 1)//если уже новый тип формирования имени
                        continue;
                    String number;
                    if (oldShortTitle.endsWith("д"))
                        number = oldShortTitle.split("д")[0];
                    else if (oldShortTitle.endsWith("Д"))
                        number = oldShortTitle.split("Д")[0];
                    else number = oldShortTitle;

                    StringBuilder buf = new StringBuilder();
                    buf.append(number);
                    if (examGroupType.getCode().compareTo(UniecDefines.CATALOG_EXAM_GROUP_TYPE_SPEC) == 0)
                        buf.append("-Б/C");
                    else if (examGroupType.getCode().compareTo(UniecDefines.CATALOG_EXAM_GROUP_TYPE_MASTER) == 0)
                        buf.append("-М");
                    else if (examGroupType.getCode().compareTo(UniecDefines.CATALOG_EXAM_GROUP_TYPE_SHORT) == 0)
                        buf.append("-С");

                    if (StringUtils.isNotEmpty(developForm.getGroup()))
                        buf.append("-").append(developForm.getGroup());
                    if (!compensationType.isBudget())
                        buf.append("-Д");

                    String shortTitle = buf.toString();
                    String title = "Группа №" + shortTitle;
                    examGroupItem.setTitle(title);
                    examGroupItem.setShortTitle(shortTitle);
                    session.update(examGroupItem);
                }
            }
        } finally
        {
            eventLock.release();
        }
    }
}
