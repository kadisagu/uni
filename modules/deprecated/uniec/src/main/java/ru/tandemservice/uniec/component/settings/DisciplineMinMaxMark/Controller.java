/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author agolubenko
 * @since 13.02.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        prepareDataSource(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);
    }

    protected void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        int columnWidth = 10;
        DynamicListDataSource<Discipline2RealizationWayRelation> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дисциплина", Discipline2RealizationWayRelation.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn<Integer>("minMark", "Минимальный балл").setWidth(columnWidth));
        dataSource.addColumn(new BlockColumn<Integer>("maxMark", "Максимальный балл").setWidth(columnWidth));
        dataSource.addColumn(new BlockColumn<Double>("step", "Шаг").setWidth(columnWidth));
        dataSource.addColumn(new BlockColumn<Double>("passMark", "Зачетный балл").setWidth(columnWidth));
        dataSource.addColumn(new BlockColumn<StateExamSubject>("stateExamSubject", "Предмет ЕГЭ"));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = getDao();
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        dao.validate(model, errors);
        if (!errors.hasErrors())
        {
            dao.update(model);
        }
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }

    public void onClickUp(IBusinessComponent component)
    {
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(component.<Long>getListenerParameter(), Discipline2RealizationWayRelation.enrollmentCampaign(), getModel(component).getEnrollmentCampaign());
    }

    public void onClickDown(IBusinessComponent component)
    {
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(component.<Long>getListenerParameter(), Discipline2RealizationWayRelation.enrollmentCampaign(), getModel(component).getEnrollmentCampaign());
    }
}
