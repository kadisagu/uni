package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.egeData;

import java.util.Arrays;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EgeDataBlock
{
    // names
    public static final String STATE_EXAM_INTERNAL_DS = "stateExamInternalDS";
    public static final String HAS_STATE_EXAM_CERTIFICATE_DS = "hasStateExamCertificateDS";
    public static final String STATE_EXAM_TYPE_DS = "stateExamTypeDS";
    public static final String CERTIFICATE_ISSUANCE_YEAR_DS = "certificateIssuanceYearDS";
    public static final String STATE_EXAM_SUBJECT_DS = "stateExamSubjectDS";

    // ids
    public static final Long STATE_EXAM_INTERNAL_YES = 0L;
    public static final Long STATE_EXAM_INTERNAL_NO = 1L;
    public static final Long HAS_STATE_EXAM_CERTIFICATE_YES = 0L;
    public static final Long HAS_STATE_EXAM_CERTIFICATE_NO = 1L;

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, EgeDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createStateExamInternalDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(STATE_EXAM_INTERNAL_YES, "да"), new IdentifiableWrapper(STATE_EXAM_INTERNAL_NO, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createHasStateExamCertificateDS(String name)
    {
        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(HAS_STATE_EXAM_CERTIFICATE_YES, "да"), new IdentifiableWrapper(HAS_STATE_EXAM_CERTIFICATE_NO, "нет")))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createStateExamTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, StateExamType.class);
    }

    public static IDefaultComboDataSourceHandler createCertificateIssuanceYearDS(String name)
    {
        long currentYear = UniBaseUtils.getCurrentYear();

        return new SimpleTitledComboDataSourceHandler(name)
                .addAll(Arrays.asList(new IdentifiableWrapper(currentYear - 2000, Long.toString(currentYear)), new IdentifiableWrapper(currentYear - 2001, Long.toString(currentYear - 1)), new IdentifiableWrapper(currentYear - 2002, Long.toString(currentYear - 2))))
                .filtered(true);
    }

    public static IDefaultComboDataSourceHandler createStateExamSubjectDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, StateExamSubject.class);
    }
}
