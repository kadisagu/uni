/* $Id$ */
package ru.tandemservice.uniec.component.catalog.entrantCustomStateCI.EntrantCustomStateCIAddEdit;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
public class DAO extends DefaultCatalogAddEditDAO<EntrantCustomStateCI, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setHtmlColor(model.getCatalogItem().getHtmlColor());
    }

    @Override
    public void update(Model model)
    {
        model.getCatalogItem().setHtmlColor(model.getHtmlColor());
        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if(!StringUtils.isEmpty(model.getHtmlColor()))
        {
            if (!model.getHtmlColor().matches("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"))
                errors.add("Цвет должен быть задан в формате #AABBCC", "htmlColor");
        }
        super.validate(model, errors);
    }
}
