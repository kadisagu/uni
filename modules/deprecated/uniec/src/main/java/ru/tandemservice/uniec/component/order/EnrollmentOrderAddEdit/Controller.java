/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertAddEdit.EcOrderRevertAddEdit;

/**
 * @author vip_delete
 * @since 07.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        if (!model.isAddForm() && model.getOrder().isRevert())
        {
            deactivate(component);
            activateInRoot(component, new ComponentActivator(EcOrderRevertAddEdit.class.getSimpleName(), ParametersMap.createWith(UIPresenter.PUBLISHER_ID, model.getOrder().getId())));
        }
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model, component.getUserContext().getErrorCollector());

        if (component.getUserContext().getErrorCollector().hasErrors()) return;

        deactivate(component);

        if (model.isAddForm())
            activateInRoot(component, new PublisherActivator(model.getOrder()));
    }
}
