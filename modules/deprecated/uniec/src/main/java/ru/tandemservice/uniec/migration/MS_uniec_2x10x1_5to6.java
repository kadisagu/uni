package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x10x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrollmentCampaign

		// создано обязательное свойство requiredAgreement4PreEnrollment
		{
			// создать колонку
			tool.createColumn("enrollmentcampaign_t", new DBColumn("rqrdagrmnt4prenrllmnt_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enrollmentcampaign_t set rqrdagrmnt4prenrllmnt_p=? where rqrdagrmnt4prenrllmnt_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enrollmentcampaign_t", "rqrdagrmnt4prenrllmnt_p", false);

		}

		// создано обязательное свойство requiredAgreement4Order
		{
			// создать колонку
			tool.createColumn("enrollmentcampaign_t", new DBColumn("requiredagreement4order_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update enrollmentcampaign_t set requiredagreement4order_p=? where requiredagreement4order_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enrollmentcampaign_t", "requiredagreement4order_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность requestedEnrollmentDirection

		// создано обязательное свойство agree4Enrollment
		{
			// создать колонку
			tool.createColumn("requestedenrollmentdirection_t", new DBColumn("agree4enrollment_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update requestedenrollmentdirection_t set agree4enrollment_p=? where agree4enrollment_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("requestedenrollmentdirection_t", "agree4enrollment_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность entrantDailyRatingByEDReport

		// создано обязательное свойство withoutAgree4Enrollment
		{
			// создать колонку
			tool.createColumn("entrantdailyratingbyedreport_t", new DBColumn("withoutagree4enrollment_p", DBType.BOOLEAN));

			tool.executeUpdate("update entrantdailyratingbyedreport_t set withoutagree4enrollment_p=? where withoutagree4enrollment_p is null", true);

			// сделать колонку NOT NULL
			tool.setColumnNullable("entrantdailyratingbyedreport_t", "withoutagree4enrollment_p", false);

		}
    }
}