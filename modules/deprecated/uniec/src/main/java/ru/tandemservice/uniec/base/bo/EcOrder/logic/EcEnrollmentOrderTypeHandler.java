/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 25.07.2013
 */
public class EcEnrollmentOrderTypeHandler extends EntityComboDataSourceHandler
{
    private static final String ALIAS_2 = "eot";
    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";

    public EcEnrollmentOrderTypeHandler(String ownerId)
    {
        super(ownerId, EntrantEnrollmentOrderType.class);
    }

    @Override
    protected DQLSelectBuilder query(final String alias, final String filter)
    {
        DQLSelectBuilder query = super.query(alias, filter);
        query.joinEntity(alias, DQLJoinType.inner, EnrollmentOrderType.class, ALIAS_2,
                         and(
                                 eq(
                                         property(EnrollmentOrderType.entrantEnrollmentOrderType().id().fromAlias(ALIAS_2)),
                                         property(EntrantEnrollmentOrderType.id().fromAlias(alias))
                                 ),
                                 eq(
                                         property(EnrollmentOrderType.used().fromAlias(ALIAS_2)),
                                         value(Boolean.TRUE)
                                 )
                         ));
        query.order(property(EnrollmentOrderType.priority().fromAlias(ALIAS_2)));
        return query;
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);
        dql.where(
                eq(
                        property(EnrollmentOrderType.enrollmentCampaign().fromAlias(ALIAS_2)),
                        commonValue(context.get(ENROLLMENT_CAMPAIGN_PARAM))
                )
        );
    }
}