/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.ui;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

/**
 * Используется на формах в которых необходимо выбрать направление приема по
 * форм. подр, территориальное подр., направлению подготовки ОУ, форме, условию, технологии и сроку освоения.
 * причем выбрать можно было бы всегда.
 *
 * @author vip_delete
 * @since 13.03.2009
 */
public final class EnrollmentDirectionUtil
{
    public static EnrollmentDirection getEnrollmentDirection(final IEnrollmentCampaignModel model, final Session session)
    {
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign == null) return null;

        OrgUnit formativeOrgUnit = model.getFormativeOrgUnit();
        if (formativeOrgUnit == null) return null;

        OrgUnit territorialOrgUnit = model.getTerritorialOrgUnit();
        if (territorialOrgUnit == null) return null;

        EducationLevelsHighSchool educationLevelsHighSchool = model.getEducationLevelsHighSchool();
        if (educationLevelsHighSchool == null) return null;

        DevelopForm developForm = model.getDevelopForm();
        if (developForm == null) return null;

        DevelopCondition developCondition = model.getDevelopCondition();
        if (developCondition == null) return null;

        DevelopTech developTech = model.getDevelopTech();
        if (developTech == null) return null;

        DevelopPeriod developPeriod = model.getDevelopPeriod();
        if (developPeriod == null) return null;

        return new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("e")), DQLExpressions.value(formativeOrgUnit)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("e")), DQLExpressions.value(territorialOrgUnit)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().fromAlias("e")), DQLExpressions.value(educationLevelsHighSchool)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("e")), DQLExpressions.value(developForm)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developCondition().fromAlias("e")), DQLExpressions.value(developCondition)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developTech().fromAlias("e")), DQLExpressions.value(developTech)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developPeriod().fromAlias("e")), DQLExpressions.value(developPeriod)))
                .createStatement(session).uniqueResult();
    }

    public static ISelectModel createFormativeOrgUnitAutocompleteModel(final IEnrollmentCampaignModel model)
    {
        return createFormativeOrgUnitAutocompleteModel(null, model);
    }

    public static ISelectModel createFormativeOrgUnitAutocompleteModel(final IPrincipalContext context, final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel(OrgUnit.P_FULL_TITLE)
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, "f");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.setProjection(Projections.distinct(Projections.property("f." + OrgUnit.P_ID)));

                List<Long> ids = c.list();

                if (ids.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(OrgUnit.class);
                listCriteria.add(Restrictions.in(OrgUnit.P_ID, ids));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(OrgUnit.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(OrgUnit.P_TITLE));

                if (context == null)
                    return new ListResult<OrgUnit>(listCriteria.list());

                List<OrgUnit> list = listCriteria.list();
                List<OrgUnit> result = new ArrayList<OrgUnit>();
                for (OrgUnit orgUnit : list)
                    if (CoreServices.securityService().check(orgUnit, context, "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode()))
                        result.add(orgUnit);

                return new ListResult<OrgUnit>(result);
            }
        };
    }

    public static ISelectModel createTerritorialOrgUnitAutocompleteModel(final IEnrollmentCampaignModel model)
    {
        return createTerritorialOrgUnitAutocompleteModel(null, model);
    }

    public static ISelectModel createTerritorialOrgUnitAutocompleteModel(final IPrincipalContext context, final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel(OrgUnit.P_TERRITORIAL_FULL_TITLE)
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, "t");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (model.getFormativeOrgUnit() != null)
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.setProjection(Projections.distinct(Projections.property("t." + OrgUnit.P_ID)));

                List<Long> ids = c.list();

                if (ids.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(OrgUnit.class);
                listCriteria.add(Restrictions.in(OrgUnit.P_ID, ids));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(OrgUnit.P_TERRITORIAL_FULL_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(OrgUnit.P_TERRITORIAL_FULL_TITLE));

                if (context == null)
                    return new ListResult<OrgUnit>(listCriteria.list());

                List<OrgUnit> list = listCriteria.list();
                List<OrgUnit> result = new ArrayList<OrgUnit>();
                for (OrgUnit orgUnit : list)
                    if (CoreServices.securityService().check(orgUnit, context, "orgUnit_viewPub_" + orgUnit.getOrgUnitType().getCode()))
                        result.add(orgUnit);

                return new ListResult<OrgUnit>(result);
            }
        };
    }

    public static ISelectModel createEducationLevelsHighSchoolModel(final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null) return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "h");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                c.setProjection(Projections.distinct(Projections.property("h." + EducationLevelsHighSchool.P_ID)));

                List<Long> ids = c.list();

                if (ids.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(EducationLevelsHighSchool.class);
                listCriteria.add(Restrictions.in(EducationLevelsHighSchool.P_ID, ids));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(EducationLevelsHighSchool.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE));

                return new ListResult<EducationLevelsHighSchool>(listCriteria.list());
            }
        };
    }

    public static ISelectModel createDevelopFormModel(final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_FORM, "f");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));

                c.setProjection(Projections.distinct(Projections.property("f." + DevelopForm.P_ID)));

                List<Long> developFormIds = c.list();

                if (developFormIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopForm.class);
                listCriteria.add(Restrictions.in(DevelopForm.P_ID, developFormIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopForm.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopForm.P_CODE));

                return new ListResult<DevelopForm>(listCriteria.list());
            }
        };
    }

    public static ISelectModel createDevelopConditionModel(final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_CONDITION, "c");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));

                c.setProjection(Projections.distinct(Projections.property("c." + DevelopCondition.P_ID)));

                List<Long> developConditionIds = c.list();

                if (developConditionIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopCondition.class);
                listCriteria.add(Restrictions.in(DevelopCondition.P_ID, developConditionIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopCondition.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopCondition.P_CODE));

                return new ListResult<DevelopCondition>(listCriteria.list());
            }
        };
    }

    public static ISelectModel createDevelopTechModel(final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_TECH, "t");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
                c.setProjection(Projections.distinct(Projections.property("t." + DevelopTech.P_ID)));

                List<Long> developTechIds = c.list();

                if (developTechIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopTech.class);
                listCriteria.add(Restrictions.in(DevelopTech.P_ID, developTechIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopTech.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopTech.P_CODE));

                return new ListResult<DevelopTech>(listCriteria.list());
            }
        };
    }

    public static ISelectModel createDevelopPeriodModel(final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getEducationLevelsHighSchool() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_PERIOD, "p");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));

                c.setProjection(Projections.distinct(Projections.property("p." + DevelopPeriod.P_ID)));

                List<Long> developPeriodIds = c.list();

                if (developPeriodIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopPeriod.class);
                listCriteria.add(Restrictions.in(DevelopPeriod.P_ID, developPeriodIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopPeriod.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopPeriod.P_PRIORITY));

                return new ListResult<DevelopPeriod>(listCriteria.list());
            }
        };
    }

    public static ISelectModel createCompensationTypeModel(final IEnrollmentCampaignModel model)
    {
        return new FullCheckSelectModel(CompensationType.P_SHORT_TITLE)
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                EnrollmentDirection direction = EnrollmentDirectionUtil.getEnrollmentDirection(model, session);

                if (direction == null)
                    return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompensationType.class, "ct");

                if (!direction.isBudget())
                    builder.where(DQLExpressions.ne(DQLExpressions.property(CompensationType.code().fromAlias("ct")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));

                if (!direction.isContract())
                    builder.where(DQLExpressions.ne(DQLExpressions.property(CompensationType.code().fromAlias("ct")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)));

                if (filter != null)
                    builder.where(DQLExpressions.like(DQLExpressions.property(CompensationType.title().fromAlias("ct")), DQLExpressions.value("%" + filter + "%")));

                builder.order(DQLExpressions.property(CompensationType.code().fromAlias("ct")));

                return new ListResult<CompensationType>(builder.createStatement(session).<CompensationType>list());
            }
        };
    }
}
