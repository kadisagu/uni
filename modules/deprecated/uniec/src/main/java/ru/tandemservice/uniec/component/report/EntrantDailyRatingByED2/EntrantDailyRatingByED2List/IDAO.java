/* $Id$ */
package ru.tandemservice.uniec.component.report.EntrantDailyRatingByED2.EntrantDailyRatingByED2List;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Andrey Andreev
 * @since 16.06.2016
 */
public interface IDAO extends IUniDao<Model>
{
}
