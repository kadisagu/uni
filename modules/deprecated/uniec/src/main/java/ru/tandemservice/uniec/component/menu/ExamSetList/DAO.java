/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.ExamSetList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.entity.EntityComparator;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Боба
 * @since 19.08.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setStudentCategoryList(getCatalogItemListOrderByCode(StudentCategory.class));
        StudentCategory studentCategory = (StudentCategory) model.getSettings().get("studentCategory");
        if (studentCategory == null)
            model.getSettings().set("studentCategory", getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        if (model.getEnrollmentCampaign() == null)
        {
            UniBaseUtils.createPage(model.getDataSource(), Collections.<ExamSet>emptyList());
        } else
        {
            StudentCategory studentCategory = model.getEnrollmentCampaign().isExamSetDiff() ? (StudentCategory) model.getSettings().get("studentCategory") : getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT);
            if (studentCategory == null)
            {
                UniBaseUtils.createPage(model.getDataSource(), Collections.<ExamSet>emptyList());
            } else
            {
                List<ExamSet> result = new ArrayList<ExamSet>();
                Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(model.getEnrollmentCampaign()).values();
                for (ExamSet examSet : examSetList)
                    if (studentCategory.equals(examSet.getStudentCategory()))
                        result.add(examSet);

                Collections.sort(result, new EntityComparator<ExamSet>(model.getDataSource().getEntityOrder()));

                model.getId2examSetId().clear();
                for (ExamSet examSet : result)
                    model.getId2examSetId().put(examSet.getId(), examSet.getExamSetId());

                UniBaseUtils.createPage(model.getDataSource(), result);
            }
        }
    }
}
