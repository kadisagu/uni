package ru.tandemservice.uniec.ws.enrollment;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
@WebService
public class EnrollmentService
{
    public EnrollmentEnvironmentNode getEnrollmentEnvironment(@WebParam(name = "enrollmentCampaignTitle") String enrollmentCampaignTitle)
    {
        if (enrollmentCampaignTitle == null)
            throw new RuntimeException("WebParam 'enrollmentCampaignTitle' is not specified!");

        return IEnrollmentServiceDao.INSTANCE.get().getEnrollmentEnvironmentData(enrollmentCampaignTitle);
    }
}
