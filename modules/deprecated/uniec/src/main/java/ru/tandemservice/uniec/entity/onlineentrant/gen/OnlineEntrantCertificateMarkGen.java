package ru.tandemservice.uniec.entity.onlineentrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Балл сертификата ЕГЭ онлайн-абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEntrantCertificateMarkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark";
    public static final String ENTITY_NAME = "onlineEntrantCertificateMark";
    public static final int VERSION_HASH = -1862972951;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_SUBJECT = "subject";
    public static final String P_MARK = "mark";
    public static final String P_YEAR = "year";

    private OnlineEntrant _entrant;     // Онлайн-абитуриент
    private StateExamSubject _subject;     // Предмет ЕГЭ
    private int _mark;     // Балл
    private Integer _year;     // Год сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     */
    @NotNull
    public OnlineEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Онлайн-абитуриент. Свойство не может быть null.
     */
    public void setEntrant(OnlineEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public StateExamSubject getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет ЕГЭ. Свойство не может быть null.
     */
    public void setSubject(StateExamSubject subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public int getMark()
    {
        return _mark;
    }

    /**
     * @param mark Балл. Свойство не может быть null.
     */
    public void setMark(int mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Год сдачи.
     */
    public Integer getYear()
    {
        return _year;
    }

    /**
     * @param year Год сдачи.
     */
    public void setYear(Integer year)
    {
        dirty(_year, year);
        _year = year;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEntrantCertificateMarkGen)
        {
            setEntrant(((OnlineEntrantCertificateMark)another).getEntrant());
            setSubject(((OnlineEntrantCertificateMark)another).getSubject());
            setMark(((OnlineEntrantCertificateMark)another).getMark());
            setYear(((OnlineEntrantCertificateMark)another).getYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEntrantCertificateMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEntrantCertificateMark.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEntrantCertificateMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "subject":
                    return obj.getSubject();
                case "mark":
                    return obj.getMark();
                case "year":
                    return obj.getYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((OnlineEntrant) value);
                    return;
                case "subject":
                    obj.setSubject((StateExamSubject) value);
                    return;
                case "mark":
                    obj.setMark((Integer) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "subject":
                        return true;
                case "mark":
                        return true;
                case "year":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "subject":
                    return true;
                case "mark":
                    return true;
                case "year":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return OnlineEntrant.class;
                case "subject":
                    return StateExamSubject.class;
                case "mark":
                    return Integer.class;
                case "year":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEntrantCertificateMark> _dslPath = new Path<OnlineEntrantCertificateMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEntrantCertificateMark");
    }
            

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getEntrant()
     */
    public static OnlineEntrant.Path<OnlineEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getSubject()
     */
    public static StateExamSubject.Path<StateExamSubject> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getMark()
     */
    public static PropertyPath<Integer> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Год сдачи.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    public static class Path<E extends OnlineEntrantCertificateMark> extends EntityPath<E>
    {
        private OnlineEntrant.Path<OnlineEntrant> _entrant;
        private StateExamSubject.Path<StateExamSubject> _subject;
        private PropertyPath<Integer> _mark;
        private PropertyPath<Integer> _year;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getEntrant()
     */
        public OnlineEntrant.Path<OnlineEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new OnlineEntrant.Path<OnlineEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Предмет ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getSubject()
     */
        public StateExamSubject.Path<StateExamSubject> subject()
        {
            if(_subject == null )
                _subject = new StateExamSubject.Path<StateExamSubject>(L_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getMark()
     */
        public PropertyPath<Integer> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Integer>(OnlineEntrantCertificateMarkGen.P_MARK, this);
            return _mark;
        }

    /**
     * @return Год сдачи.
     * @see ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(OnlineEntrantCertificateMarkGen.P_YEAR, this);
            return _year;
        }

        public Class getEntityClass()
        {
            return OnlineEntrantCertificateMark.class;
        }

        public String getEntityName()
        {
            return "onlineEntrantCertificateMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
