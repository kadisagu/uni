package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка приказа о зачислении абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentExtractGen extends AbstractEntrantExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentExtract";
    public static final String ENTITY_NAME = "enrollmentExtract";
    public static final int VERSION_HASH = 1397191632;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_STUDENT_NEW = "studentNew";
    public static final String P_GROUP_TITLE = "groupTitle";

    private Course _course;     // Курс
    private Student _studentNew;     // Студент
    private String _groupTitle;     // Зачислить в группу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Студент.
     */
    public Student getStudentNew()
    {
        return _studentNew;
    }

    /**
     * @param studentNew Студент.
     */
    public void setStudentNew(Student studentNew)
    {
        dirty(_studentNew, studentNew);
        _studentNew = studentNew;
    }

    /**
     * @return Зачислить в группу.
     */
    @Length(max=255)
    public String getGroupTitle()
    {
        return _groupTitle;
    }

    /**
     * @param groupTitle Зачислить в группу.
     */
    public void setGroupTitle(String groupTitle)
    {
        dirty(_groupTitle, groupTitle);
        _groupTitle = groupTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrollmentExtractGen)
        {
            setCourse(((EnrollmentExtract)another).getCourse());
            setStudentNew(((EnrollmentExtract)another).getStudentNew());
            setGroupTitle(((EnrollmentExtract)another).getGroupTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentExtractGen> extends AbstractEntrantExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrollmentExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "studentNew":
                    return obj.getStudentNew();
                case "groupTitle":
                    return obj.getGroupTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "studentNew":
                    obj.setStudentNew((Student) value);
                    return;
                case "groupTitle":
                    obj.setGroupTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "studentNew":
                        return true;
                case "groupTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "studentNew":
                    return true;
                case "groupTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "studentNew":
                    return Student.class;
                case "groupTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentExtract> _dslPath = new Path<EnrollmentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Студент.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentExtract#getStudentNew()
     */
    public static Student.Path<Student> studentNew()
    {
        return _dslPath.studentNew();
    }

    /**
     * @return Зачислить в группу.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentExtract#getGroupTitle()
     */
    public static PropertyPath<String> groupTitle()
    {
        return _dslPath.groupTitle();
    }

    public static class Path<E extends EnrollmentExtract> extends AbstractEntrantExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Student.Path<Student> _studentNew;
        private PropertyPath<String> _groupTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Студент.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentExtract#getStudentNew()
     */
        public Student.Path<Student> studentNew()
        {
            if(_studentNew == null )
                _studentNew = new Student.Path<Student>(L_STUDENT_NEW, this);
            return _studentNew;
        }

    /**
     * @return Зачислить в группу.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentExtract#getGroupTitle()
     */
        public PropertyPath<String> groupTitle()
        {
            if(_groupTitle == null )
                _groupTitle = new PropertyPath<String>(EnrollmentExtractGen.P_GROUP_TITLE, this);
            return _groupTitle;
        }

        public Class getEntityClass()
        {
            return EnrollmentExtract.class;
        }

        public String getEntityName()
        {
            return "enrollmentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
