/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionAdd;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.EducationLevelUtil;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

/**
 * @author vip_delete
 * @since 16.02.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsHighSchoolModel(EducationLevelUtil.createEducationLevelsHighSchoolModel(model));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareEducationOrgUnitDataSource(Model model)
    {
        Criteria criteria = getSession().createCriteria(EducationOrgUnit.class);
        criteria.add(Restrictions.eq(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
        if (model.getTerritorialOrgUnit() != null)
        {
            criteria.add(Restrictions.eq(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
        }
        if (model.getEducationLevelsHighSchool() != null)
        {
            criteria.add(Restrictions.eq(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
        }

        List<EducationOrgUnit> list = criteria.list();
        Collections.sort(list, new EntityComparator<EducationOrgUnit>(new EntityOrder("id", OrderDirection.asc)));
        model.getEducationOrgUnitDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getEducationOrgUnitDataSource(), list);

        Set<Long> usedDirectionIds = getUsedDirectionIds(model);
        Map<Long, Boolean> valueMap = new HashMap<>();
        for (EducationOrgUnit item : list)
        {
            valueMap.put(item.getId(), model.getSelectedEducationOrgUnitSet().contains(item) || usedDirectionIds.contains(item.getId()));
        }
        ((BlockColumn<Boolean>) model.getEducationOrgUnitDataSource().getColumn(Model.CHECKBOX_COLUMN)).setValueMap(valueMap);
    }

    @Override
    public void prepareEducationOrgUnitSelectedDataSource(Model model)
    {
        List<EducationOrgUnit> list = new ArrayList<>(model.getSelectedEducationOrgUnitSet());
        Collections.sort(list, new EntityComparator<EducationOrgUnit>(new EntityOrder("id", OrderDirection.asc)));
        model.getEducationOrgUnitSelectedDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getEducationOrgUnitSelectedDataSource(), list);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void selectEducationOrgUnit(Model model)
    {
        Set<Long> usedDirectionIds = getUsedDirectionIds(model);
        for (Map.Entry<Long, Boolean> entity : ((BlockColumn<Boolean>) model.getEducationOrgUnitDataSource().getColumn(Model.CHECKBOX_COLUMN)).getValueMap().entrySet())
        {
            if (entity.getValue() && !usedDirectionIds.contains(entity.getKey()))
            {
                model.getSelectedEducationOrgUnitSet().add(get(EducationOrgUnit.class, entity.getKey()));
            }
        }

        model.setFormativeOrgUnit(null);
        model.setTerritorialOrgUnit(null);
        model.setEducationLevelsHighSchool(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        Criteria c = getSession().createCriteria(EnrollmentDirection.class);
        c.add(Restrictions.eq(EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        c.setProjection(Projections.property(EnrollmentDirection.L_EDUCATION_ORG_UNIT + ".id"));
        Set<Long> usedIds = new HashSet<Long>(c.list());

        for (EducationOrgUnit entity : model.getSelectedEducationOrgUnitSet())
        {
            if (!usedIds.contains(entity.getId()))
            {
                EnrollmentDirection enrollmentDirection = new EnrollmentDirection();
                enrollmentDirection.setEducationOrgUnit(entity);
                enrollmentDirection.setEnrollmentCampaign(model.getEnrollmentCampaign());
                enrollmentDirection.setBudget(true);
                enrollmentDirection.setContract(true);
                save(enrollmentDirection);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private Set<Long> getUsedDirectionIds(Model model)
    {
        // нельзя создавать НП на НПП, на которые уже созданы НП и на те НПП, которые являются профилями к некоторым НП. Все в рамках текущей примной кампании.
        List<Long> ids = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e")
                .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(model.getEnrollmentCampaign())))
                .union(new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "p")
                        .column(DQLExpressions.property(ProfileEducationOrgUnit.educationOrgUnit().id().fromAlias("p")))
                        .where(DQLExpressions.eq(DQLExpressions.property(ProfileEducationOrgUnit.enrollmentDirection().enrollmentCampaign().fromAlias("p")), DQLExpressions.value(model.getEnrollmentCampaign())))
                        .buildSelectRule()
                ).createStatement(getSession()).list();
        
        return new HashSet<>(ids);
    }
}
