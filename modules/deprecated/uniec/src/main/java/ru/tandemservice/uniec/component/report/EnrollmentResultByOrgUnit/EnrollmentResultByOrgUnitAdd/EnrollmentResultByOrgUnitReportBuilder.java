/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByOrgUnit.EnrollmentResultByOrgUnitAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.GraduationHonour;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.GraduationHonourCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;

/**
 * @author ekachanova
 */
public class EnrollmentResultByOrgUnitReportBuilder
{
    private Model _model;
    private Session _session;

    EnrollmentResultByOrgUnitReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_BY_ORGUNIT);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getTitle());
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);
        injectModifier.put("formativeOrgUnit", StringUtils.isNotEmpty(_model.getReport().getFormativeOrgUnit().getNominativeCaseTitle())
                ? _model.getReport().getFormativeOrgUnit().getNominativeCaseTitle() : _model.getReport().getFormativeOrgUnit().getTitle());
        if(StringUtils.isNotEmpty(_model.getReport().getDevelopForm()))
            injectModifier.put("developForm", _model.getReport().getDevelopForm().replace(";", ",").toLowerCase() + " форма обучения");
        else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("developForm"), false, false);

        if(_model.getReport().getCompensationType() != null)
            injectModifier.put("compensType", _model.getReport().getCompensationType().getCode().equalsIgnoreCase(UniDefines.COMPENSATION_TYPE_BUDGET) ? "На места, финансируемые из средств федерального бюджета" : "На места с оплатой стоимости обучения");
        else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("compensType"), false, false);

        injectModifier.modify(document);

        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);
        if(_model.isQualificationActive())
            enrollmentDirectionList = filterByQualification(enrollmentDirectionList);

        Map<Long, ReportRow> eduLevelsHighSchoolId2ReportRows = new HashMap<>();
        Map<Long, Integer> eduLevelsHighSchoolId2BudgetPlan = new HashMap<>();
        Integer totalBudgetPlan = 0;
        for(EnrollmentDirection direction : enrollmentDirectionList)
        {
            if(!eduLevelsHighSchoolId2ReportRows.containsKey(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId()))
            {
                eduLevelsHighSchoolId2ReportRows.put(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId(), new ReportRow(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId(), direction.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle(), direction.getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit().getShortTitle()));
            }
            if(direction.getMinisterialPlan() != null)
            {
                Integer budgetPlan = eduLevelsHighSchoolId2BudgetPlan.get(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId());
                eduLevelsHighSchoolId2BudgetPlan.put(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId(), (budgetPlan == null ? 0 : budgetPlan) + direction.getMinisterialPlan());
                totalBudgetPlan += direction.getMinisterialPlan();
            }
        }
        ReportRow totalRow = new ReportRow(null, "ИТОГО (по заявлениям):", "");
        ReportRow totalHumanRow = new ReportRow(null, "ИТОГО (человек):", "");

        {   //Подано заявлений
            final MQBuilder directionBuilder = getBaseDirectionBuilder(enrollmentDirectionList);
            List<Long> medalPersonIds = getMedalPersonIds(directionBuilder);

            directionBuilder.addSelect("r", new String[] {
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+".id",
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+"."+Entrant.L_PERSON+".id",
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+"."+Entrant.L_PERSON+"."+ Person.L_IDENTITY_CARD+"."+ IdentityCard.L_SEX+"."+ Sex.P_CODE,
                    RequestedEnrollmentDirection.L_COMPETITION_KIND+"."+ CompetitionKind.P_CODE,
                    RequestedEnrollmentDirection.P_TARGET_ADMISSION,
                    RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirection.L_EDUCATION_ORG_UNIT+"."+ EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL+".id"
            });

            for(Object[] obj : directionBuilder.<Object[]>getResultList(_session))
            {
                Long requestId = (Long)obj[0];
                Long personId = (Long)obj[1];
                String sexCode = (String)obj[2];
                String competitionKindCode = (String)obj[3];
                boolean targetAdmittion = (Boolean)obj[4];
                Long eduLevelsHighSchoolId = (Long)obj[5];
                ReportRow row = eduLevelsHighSchoolId2ReportRows.get(eduLevelsHighSchoolId);

                row.entered.add(requestId);
                totalHumanRow.entered.add(personId);
                if(sexCode.equals(SexCodes.MALE))
                {
                    row.enteredMale.add(requestId);
                    totalHumanRow.enteredMale.add(personId);
                }
                if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
                {
                    row.enteredBeyondCompetition.add(requestId);
                    totalHumanRow.enteredBeyondCompetition.add(personId);
                }
                if(targetAdmittion)
                {
                    row.enteredTarget.add(requestId);
                    totalHumanRow.enteredTarget.add(personId);
                }
                if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_INTERVIEW))
                {
                    row.enteredInterview.add(requestId);
                    totalHumanRow.enteredInterview.add(personId);
                }
                if(medalPersonIds.contains(personId))
                {
                    row.enteredMedal.add(requestId);
                    totalHumanRow.enteredMedal.add(personId);
                }
            }
        }

        {   //Допущено к участию в конкурсе
            final MQBuilder directionBuilder = getBaseDirectionBuilder(enrollmentDirectionList);
            directionBuilder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.L_STATE+"."+ EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
            List<Long> medalPersonIds = getMedalPersonIds(directionBuilder);

            directionBuilder.addSelect("r", new String[] {
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+".id",
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+"."+Entrant.L_PERSON+".id",
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+"."+Entrant.L_PERSON+"."+ Person.L_IDENTITY_CARD+"."+ IdentityCard.L_SEX+"."+ Sex.P_CODE,
                    RequestedEnrollmentDirection.L_COMPETITION_KIND+"."+ CompetitionKind.P_CODE,
                    RequestedEnrollmentDirection.P_TARGET_ADMISSION,
                    RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirection.L_EDUCATION_ORG_UNIT+"."+ EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL+".id"
            });

            for(Object[] obj : directionBuilder.<Object[]>getResultList(_session))
            {
                Long requestId = (Long)obj[0];
                Long personId = (Long)obj[1];
                String sexCode = (String)obj[2];
                String competitionKindCode = (String)obj[3];
                boolean targetAdmission = (Boolean)obj[4];
                Long eduLevelsHighSchoolId = (Long)obj[5];
                ReportRow row = eduLevelsHighSchoolId2ReportRows.get(eduLevelsHighSchoolId);

                row.permitted.add(requestId);
                totalHumanRow.permitted.add(personId);
                if(sexCode.equals(SexCodes.MALE))
                {
                    row.permittedMale.add(requestId);
                    totalHumanRow.permittedMale.add(personId);
                }
                if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
                {
                    row.permittedBeyondCompetition.add(requestId);
                    totalHumanRow.permittedBeyondCompetition.add(personId);
                }
                if(targetAdmission)
                {
                    row.permittedTarget.add(requestId);
                    totalHumanRow.permittedTarget.add(personId);
                }
                if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_INTERVIEW))
                {
                    row.permittedInterview.add(requestId);
                    totalHumanRow.permittedInterview.add(personId);
                }
                if(medalPersonIds.contains(personId))
                {
                    row.permittedMedal.add(requestId);
                    totalHumanRow.permittedMedal.add(personId);
                }
            }
        }

        {   //Зачислено
            final MQBuilder directionBuilder = getBasePreliminaryBuilder(enrollmentDirectionList);
            directionBuilder.addSelect("p", new String[]{PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION});
            List<Long> medalPersonIds = getMedalPersonIds(directionBuilder);
            List<Long> notEmptyStateExamDirectionIds = getNotEmptyStateExamDirectionIds(directionBuilder);
            List<Long> notEmptyInterviewDirectionIds = getNotEmptyInterviewDirectionIds(directionBuilder);
            List<Long> notEmptyExamDirectionIds = getNotEmptyExamDirectionIds(directionBuilder);
            List<Long> notEmptyOneDiscDirectionIds = getNotEmptyOneDiscDirectionIds(directionBuilder);

            final MQBuilder preliminaryBuilder = getBasePreliminaryBuilder(enrollmentDirectionList);
            preliminaryBuilder.addSelect("p", new String[] {
                    "id",
                    PreliminaryEnrollmentStudent.P_TARGET_ADMISSION,
                    PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT+"."+EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL+".id"
            });
            preliminaryBuilder.addSelect("r", new String[] {
                    "id",
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+"."+Entrant.L_PERSON+".id",
                    RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+"."+Entrant.L_PERSON+"."+ Person.L_IDENTITY_CARD+"."+ IdentityCard.L_SEX+"."+ Sex.P_CODE,
                    RequestedEnrollmentDirection.L_COMPETITION_KIND+"."+ CompetitionKind.P_CODE});

            for(Object[] obj : preliminaryBuilder.<Object[]>getResultList(_session))
            {
                Long preliminaryId = (Long)obj[0];
                boolean targetAdmittion = (Boolean)obj[1];
                Long eduLevelsHighSchoolId = (Long)obj[2];
                Long directionId = (Long)obj[3];
                Long personId = (Long)obj[4];
                String sexCode = (String)obj[5];
                String competitionKindCode = (String)obj[6];
                ReportRow row = eduLevelsHighSchoolId2ReportRows.get(eduLevelsHighSchoolId);

                row.enrolled.add(preliminaryId);
                if(sexCode.equals(SexCodes.MALE))
                    row.enrolledMale.add(preliminaryId);
                if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
                    row.enrolledBeyondCompetiton.add(preliminaryId);
                if(targetAdmittion)
                    row.enrolledTarget.add(preliminaryId);
                if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_INTERVIEW))
                    row.enrolledInterview.add(preliminaryId);
                if(notEmptyStateExamDirectionIds.contains(directionId))
                    row.enrolledEGE.add(preliminaryId);

                if(medalPersonIds.contains(personId))
                {
                    if(competitionKindCode.equals(UniecDefines.COMPETITION_KIND_INTERVIEW) || notEmptyInterviewDirectionIds.contains(directionId))
                        row.enrolledMedalInterview.add(preliminaryId);

                    if(notEmptyOneDiscDirectionIds.contains(directionId))
                        row.enrolledMedalOneCompetition.add(preliminaryId);

                    if(notEmptyExamDirectionIds.contains(directionId))
                        row.enrolledMedalExam.add(preliminaryId);

                    if(notEmptyStateExamDirectionIds.contains(directionId))
                        row.enrolledMedalEGE.add(preliminaryId);
                }
            }
        }

        String[][] data = new String[eduLevelsHighSchoolId2ReportRows.size()][26];
        List<ReportRow> rows = new ArrayList<>(eduLevelsHighSchoolId2ReportRows.values());
        Collections.sort(rows);

        Set<ReportRow> equalsTitlesRows = new HashSet<>();  //для таких строк выводить еще и выпускающее подр.
        ReportRow prevRow = null;
        for(int i = 0; i < rows.size(); i++)   //отсортированы по title
        {
            ReportRow row = rows.get(i);
            if(prevRow != null && prevRow.title.equals(row.title))
            {
                equalsTitlesRows.add(prevRow);
                equalsTitlesRows.add(row);
            }
            prevRow = row;
        }

        int[] totalValues = new int[22];
        for(ReportRow row : rows)
        {
            totalValues[0] += row.entered.size();
            totalValues[1] += row.enteredMale.size();
            totalValues[2] += row.enteredBeyondCompetition.size();
            totalValues[3] += row.enteredTarget.size();
            totalValues[4] += row.enteredInterview.size();
            totalValues[5] += row.enteredMedal.size();

            totalValues[6] += row.permitted.size();
            totalValues[7] += row.permittedMale.size();
            totalValues[8] += row.permittedBeyondCompetition.size();
            totalValues[9] += row.permittedTarget.size();
            totalValues[10] += row.permittedInterview.size();
            totalValues[11] += row.permittedMedal.size();

            totalValues[12] += row.enrolled.size();
            totalValues[13] += row.enrolledMale.size();
            totalValues[14] += row.enrolledBeyondCompetiton.size();
            totalValues[15] += row.enrolledTarget.size();
            totalValues[16] += row.enrolledInterview.size();
            totalValues[17] += row.enrolledEGE.size();
            totalValues[18] += row.enrolledMedalInterview.size();
            totalValues[19] += row.enrolledMedalOneCompetition.size();
            totalValues[20] += row.enrolledMedalExam.size();
            totalValues[21] += row.enrolledMedalEGE.size();
        }

        DoubleFormatter doubleFormatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;
        for(int i = 0; i < rows.size(); i++)
        {
            ReportRow row = rows.get(i);
            String rowTitle = row.title;
            if(equalsTitlesRows.contains(row))
                rowTitle += " (" + row.graduateOrgUnitTitle + ")";
            Double enteredCompetition = null;
            Double permittedCompetition = null;
            Integer budgetPlan = eduLevelsHighSchoolId2BudgetPlan.get(row.eduLevelsHighSchoolId);
            if(budgetPlan != null && budgetPlan > 0)
            {
                enteredCompetition = ((double)row.entered.size())/budgetPlan;
                permittedCompetition = ((double)row.permitted.size())/budgetPlan;
            }

            data[i] = new String[]{String.valueOf(i + 1), rowTitle, convert(row.entered), convert(row.enteredMale),
                    convert(row.enteredBeyondCompetition), convert(row.enteredTarget), convert(row.enteredInterview),
                    convert(row.enteredMedal), doubleFormatter.format(enteredCompetition),
                    convert(row.permitted), convert(row.permittedMale), convert(row.permittedBeyondCompetition),
                    convert(row.permittedTarget), convert(row.permittedInterview), convert(row.permittedMedal),
                    doubleFormatter.format(permittedCompetition),
                    convert(row.enrolled), convert(row.enrolledMale), convert(row.enrolledBeyondCompetiton),
                    convert(row.enrolledTarget), convert(row.enrolledInterview), convert(row.enrolledEGE),
                    convert(row.enrolledMedalInterview), convert(row.enrolledMedalOneCompetition), convert(row.enrolledMedalExam),
                    convert(row.enrolledMedalEGE)};
        }

        Double totalEnteredCompetition = null;
        Double totalPermittedCompetition = null;
        Double totalHumanEnteredCompetition = null;
        Double totalHumanPermittedCompetition = null;
        if(totalBudgetPlan > 0)
        {
            totalEnteredCompetition = ((double)totalValues[0])/totalBudgetPlan;
            totalPermittedCompetition = ((double)totalValues[6])/totalBudgetPlan;
            totalHumanEnteredCompetition = ((double)totalHumanRow.entered.size())/totalBudgetPlan;
            totalHumanPermittedCompetition = ((double)totalHumanRow.permitted.size())/totalBudgetPlan;
        }
        String[][] totalData = new String[2][25];

        totalData[0] = new String[]{totalRow.title, String.valueOf(totalValues[0]), String.valueOf(totalValues[1]),
                String.valueOf(totalValues[2]), String.valueOf(totalValues[3]), String.valueOf(totalValues[4]),
                String.valueOf(totalValues[5]), doubleFormatter.format(totalEnteredCompetition),
                String.valueOf(totalValues[6]), String.valueOf(totalValues[7]), String.valueOf(totalValues[8]),
                String.valueOf(totalValues[9]), String.valueOf(totalValues[10]), String.valueOf(totalValues[11]),
                doubleFormatter.format(totalPermittedCompetition),
                String.valueOf(totalValues[12]), String.valueOf(totalValues[13]), String.valueOf(totalValues[14]),
                String.valueOf(totalValues[15]), String.valueOf(totalValues[16]), String.valueOf(totalValues[17]),
                String.valueOf(totalValues[18]), String.valueOf(totalValues[19]), String.valueOf(totalValues[20]),
                String.valueOf(totalValues[21])};

        totalData[1] = new String[]{totalHumanRow.title, convert(totalHumanRow.entered), convert(totalHumanRow.enteredMale),
                convert(totalHumanRow.enteredBeyondCompetition), convert(totalHumanRow.enteredTarget), convert(totalHumanRow.enteredInterview),
                convert(totalHumanRow.enteredMedal), doubleFormatter.format(totalHumanEnteredCompetition),
                convert(totalHumanRow.permitted), convert(totalHumanRow.permittedMale), convert(totalHumanRow.permittedBeyondCompetition),
                convert(totalHumanRow.permittedTarget), convert(totalHumanRow.permittedInterview), convert(totalHumanRow.permittedMedal),
                doubleFormatter.format(totalHumanPermittedCompetition),
                String.valueOf(totalValues[12]), String.valueOf(totalValues[13]), String.valueOf(totalValues[14]),
                String.valueOf(totalValues[15]), String.valueOf(totalValues[16]), String.valueOf(totalValues[17]),
                String.valueOf(totalValues[18]), String.valueOf(totalValues[19]), String.valueOf(totalValues[20]),
                String.valueOf(totalValues[21])};

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", data);
        tableModifier.put("TOTAL", totalData);
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private static String convert(Set set)
    {
        return String.valueOf(set.size());
    }

    private List<EnrollmentDirection> filterByQualification(List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EnrollmentDirection> result = new ArrayList<>();
        for(EnrollmentDirection direction : enrollmentDirectionList)
            if(_model.getQualificationList().contains(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
                result.add(direction);
        return result;
    }

    private MQBuilder getBaseDirectionBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if(_model.isCompensTypeActive())
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if(_model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        return builder;
    }

    private MQBuilder getBasePreliminaryBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if(_model.isCompensTypeActive())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if(_model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if(_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        return builder;
    }

    @SuppressWarnings("deprecation")
    private List<Long> getMedalPersonIds(MQBuilder builder)
    {
        MQBuilder eduBuilder = new MQBuilder(PersonEduInstitution.ENTITY_CLASS, "eduInstitution", new String[]{PersonEduInstitution.L_PERSON+".id"});
        eduBuilder.addDomain("requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        eduBuilder.add(MQExpression.eqProperty("eduInstitution", PersonEduInstitution.L_PERSON, "requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON));
        eduBuilder.add(MQExpression.in("requestedDirection", "id", builder));
        eduBuilder.add(MQExpression.in("eduInstitution", PersonEduInstitution.L_GRADUATION_HONOUR+"."+ GraduationHonour.P_CODE, GraduationHonourCodes.GOLD_MEDAL, GraduationHonourCodes.SILVER_MEDAL));
        eduBuilder.setNeedDistinct(true);
        return eduBuilder.getResultList(_session);
    }

    private List<Long> getNotEmptyStateExamDirectionIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[] {
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION+".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE,
                UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1,
                UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2,
                UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL,
                UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL));
        builder.setNeedDistinct(true);
        return builder.getResultList(_session);
    }

    private List<Long> getNotEmptyInterviewDirectionIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[] {
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION+".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE,
                UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW,
                UniecDefines.FINAL_MARK_APPEAL_INTERVIEW));
        builder.setNeedDistinct(true);
        return builder.getResultList(_session);
    }

    private List<Long> getNotEmptyOneDiscDirectionIds(MQBuilder directionBuilder)
    {
        Map<Long, Set<Long>> directionId2ChosenIds = new HashMap<>();
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[] {
                "id", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION+".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.notIn("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE,
                UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW,
                UniecDefines.FINAL_MARK_APPEAL_INTERVIEW));
        builder.setNeedDistinct(true);
        for(Object[] obj : builder.<Object[]>getResultList(_session))
        {
            Long chosenId = (Long)obj[0];
            Long directionId = (Long)obj[1];
            Set<Long> chosenIds = directionId2ChosenIds.get(directionId);
            if(chosenIds == null)
                directionId2ChosenIds.put(directionId, chosenIds = new HashSet<>());
            chosenIds.add(chosenId);
        }
        List<Long> notEmptyOneDiscDirectionIds = new ArrayList<>();
        for(Map.Entry<Long, Set<Long>> entry : directionId2ChosenIds.entrySet())
        {
            if(entry.getValue().size() == 1)
                notEmptyOneDiscDirectionIds.add(entry.getKey());
        }
        return notEmptyOneDiscDirectionIds;
    }

    private List<Long> getNotEmptyExamDirectionIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[] {
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION+".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.notIn("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE,
                UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1,
                UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2,
                UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL,
                UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL,
                UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW,
                UniecDefines.FINAL_MARK_APPEAL_INTERVIEW));
        builder.setNeedDistinct(true);
        return builder.getResultList(_session);
    }

    @SuppressWarnings("unchecked")
    private class ReportRow implements Comparable
    {
        private Long eduLevelsHighSchoolId;
        private String title;
        private String graduateOrgUnitTitle;

        private Set<Long> entered = new HashSet<>();
        private Set<Long> enteredMale = new HashSet<>();
        private Set<Long> enteredBeyondCompetition = new HashSet<>();
        private Set<Long> enteredTarget = new HashSet<>();
        private Set<Long> enteredInterview = new HashSet<>();
        private Set<Long> enteredMedal = new HashSet<>();

        private Set<Long> permitted = new HashSet<>();
        private Set<Long> permittedMale = new HashSet<>();
        private Set<Long> permittedBeyondCompetition = new HashSet<>();
        private Set<Long> permittedTarget = new HashSet<>();
        private Set<Long> permittedInterview = new HashSet<>();
        private Set<Long> permittedMedal = new HashSet<>();

        private Set<Long> enrolled = new HashSet<>();
        private Set<Long> enrolledMale = new HashSet<>();
        private Set<Long> enrolledBeyondCompetiton = new HashSet<>();
        private Set<Long> enrolledTarget = new HashSet<>();
        private Set<Long> enrolledInterview = new HashSet<>();
        private Set<Long> enrolledEGE = new HashSet<>();
        private Set<Long> enrolledMedalInterview = new HashSet<>();
        private Set<Long> enrolledMedalOneCompetition = new HashSet<>();
        private Set<Long> enrolledMedalExam = new HashSet<>();
        private Set<Long> enrolledMedalEGE = new HashSet<>();

        private ReportRow(Long eduLevelsHighSchoolId, String title, String graduateOrgUnitTitle)
        {
            this.eduLevelsHighSchoolId = eduLevelsHighSchoolId;
            this.title = title;
            this.graduateOrgUnitTitle = graduateOrgUnitTitle;
        }

        @Override
        public int compareTo(Object o)
        {
            int result = title.compareTo(((ReportRow)o).title);
            return result == 0 ? graduateOrgUnitTitle.compareTo(((ReportRow)o).graduateOrgUnitTitle) : result;
        }
    }
}
