// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryForm2.EntrantRegistryForm2Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author oleyba
 * @since 09.06.2010
 */
public class EntrantRegistryForm2ReportBuilder
{
    private Model model;
    private Session session;

    private IUniBaseDao dao = UniDaoFacade.getCoreDao();
    private RtfDocument template;
    private EntrantDataUtil util;

    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directionMap = new HashMap<>();
    private Map<Person, Set<PersonBenefit>> benefitMap;
    private Map<Entrant, Set<EntrantEnrolmentRecommendation>> recommendationMap;
    private Map<Long, List<String>> orderNumberMap;

    private int taKindsCount;

    public EntrantRegistryForm2ReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        init();

        DatabaseFile content = new DatabaseFile();
        content.setContent(generateContent());
        return content;
    }

    private void init()
    {
        IScriptItem templateDocument = dao.getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REGISTRY_FORM_2);
        template = new RtfReader().read(templateDocument.getCurrentTemplate());

        taKindsCount = dao.getCount(TargetAdmissionKind.class);

        MQBuilder builder = getRequestedEnrollmentDirectionBuilder();

        util = new EntrantDataUtil(session, model.getReport().getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
        {
            List<RequestedEnrollmentDirection> directions = directionMap.get(direction.getEnrollmentDirection());
            if (null == directions)
                directionMap.put(direction.getEnrollmentDirection(), directions = new ArrayList<>());
            directions.add(direction);
        }

        for (Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> direction : directionMap.entrySet())
        {
            Collections.sort(direction.getValue(), (o1, o2) -> Integer.compare(o1.getNumber(), o2.getNumber()));
        }

        benefitMap = EntrantDataUtil.getPersonBenefitMap(session, builder);
        recommendationMap = EntrantDataUtil.getEnrollmentRecomendationMap(session, builder);

        MQBuilder orderNumbers = new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{EnrollmentExtract.entity().requestedEnrollmentDirection().id().s()});
        orderNumbers.addJoin("e", EnrollmentExtract.paragraph().s(), "p");
        orderNumbers.addSelect("p", new Object[]{EnrollmentParagraph.order().number().s()});
        orderNumbers.add((MQExpression.in("e", EnrollmentExtract.entity().requestedEnrollmentDirection().s(), builder)));

        orderNumberMap = new HashMap<>();

        for (Object[] row : orderNumbers.<Object[]>getResultList(session))
        {
            Long directionId = (Long) row[0];
            List<String> numbers = orderNumberMap.get(directionId);
            if (null == numbers)
                orderNumberMap.put(directionId, numbers = new ArrayList<>());
            numbers.add((String) row[1]);
        }

        // грузим апельсины бочками
        dao.getList(DevelopForm.class);
        dao.getList(CompensationType.class);
        dao.getList(CompetitionKind.class);
        dao.getList(OrgUnit.class, OrgUnit.orgUnitType().code(), OrgUnitTypeCodes.FACULTY);
        dao.getList(OrgUnit.class, OrgUnit.orgUnitType().code(), OrgUnitTypeCodes.BRANCH);
        dao.getList(OrgUnit.class, OrgUnit.orgUnitType().code(), OrgUnitTypeCodes.REPRESENTATION);
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList()));
        if (model.isCompensTypeActive())
            builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.compensationType().s(), model.getCompensationType()));
        builder.addJoin("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().s(), "edu");
        if (model.isQualificationActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList()));
        if (model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.formativeOrgUnit().s(), model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.territorialOrgUnit().s(), model.getTerritorialOrgUnitList()));
        if (model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.educationLevelHighSchool().s(), model.getEducationLevelHighSchoolList()));
        if (model.isDevelopFormActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developForm().s(), model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developCondition().s(), model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developTech().s(), model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            builder.add(MQExpression.in("edu", EducationOrgUnit.developPeriod().s(), model.getDevelopPeriodList()));

        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        return builder;
    }

    private static final String dateTime = DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date());

    private byte[] print(EnrollmentDirection enrollmentDirection)
    {
        RtfDocument document = template.getClone();

        EducationOrgUnit eduOu = enrollmentDirection.getEducationOrgUnit();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("dateTime", dateTime);
        injectModifier.put("ouFormTitle", eduOu.getFormativeOrgUnit().getNominativeCaseTitle() == null ? eduOu.getFormativeOrgUnit().getTitle() : eduOu.getFormativeOrgUnit().getNominativeCaseTitle());
        injectModifier.put("ouTerrTitle", eduOu.getTerritorialOrgUnit() == null ? "" : (eduOu.getTerritorialOrgUnit().getNominativeCaseTitle() == null ? eduOu.getTerritorialOrgUnit().getTerritorialTitle() : eduOu.getTerritorialOrgUnit().getNominativeCaseTitle()));
        injectModifier.put("eduOrgUnitTitle", eduOu.getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());
        injectModifier.put("developForm", eduOu.getDevelopForm().getTitle());
        injectModifier.modify(document);

        List<String[]> tableData = new ArrayList<>();
        final List<String[]> marks = new ArrayList<>();

        for (RequestedEnrollmentDirection direction : directionMap.get(enrollmentDirection))
        {
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            Person person = entrant.getPerson();
            IdentityCard card = person.getIdentityCard();

            List<String> row = new ArrayList<>();
            row.add(String.valueOf(direction.getNumber()));
            row.add(person.getFullFio());
            row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()));
            PersonEduInstitution eduInstitution = person.getPersonEduInstitution();
            List<String> eduInstTitle = new ArrayList<>();
            if (eduInstitution != null)
            {
                //[Тип документа], [серия] [номер], [год окончания] г., [Сокращ. наз. уровня. образ.], [Название ОУ], ([Код субъекта РФ]) [Населенный пункт], [Квалификация по диплому], [Специальность по диплому]
                eduInstTitle.add(eduInstitution.getDocumentType().getTitle());
                eduInstTitle.add(UniStringUtils.join(eduInstitution.getSeria(), eduInstitution.getNumber()));
                eduInstTitle.add(Integer.toString(eduInstitution.getYearEnd()) + " г.");
                if (eduInstitution.getEducationStage() != null)
                    eduInstTitle.add(eduInstitution.getEducationStage().getShortTitle());
                if (eduInstitution.getEduInstitution() != null)
                {
                    eduInstTitle.add(eduInstitution.getEduInstitution().getTitle());
                    if (eduInstitution.getEduInstitution().getAddress() != null && eduInstitution.getEduInstitution().getAddress().getSettlement() != null)
                    {
                        AddressItem settlement = eduInstitution.getEduInstitution().getAddress().getSettlement();
                        eduInstTitle.add("(" + settlement.getInheritedRegionCode() + ") " + settlement.getTitleWithType());
                    }
                }
                if (eduInstitution.getDiplomaQualification() != null)
                    eduInstTitle.add(eduInstitution.getDiplomaQualification().getTitle());
                if (eduInstitution.getEmployeeSpeciality() != null)
                    eduInstTitle.add(eduInstitution.getEmployeeSpeciality().getTitle());
            }
            row.add(UniStringUtils.joinNotEmpty(eduInstTitle, ", "));
            AddressBase address = card.getAddress();
            StringBuilder addressTitle = new StringBuilder();
            if (address != null)
            {
                addressTitle.append(address.getTitleWithFlat());
                boolean hasMobilePhone = StringUtils.isNotEmpty(person.getContactData().getPhoneMobile());
                boolean hasHomePhone = StringUtils.isNotEmpty(person.getContactData().getPhoneFact());
                if (hasMobilePhone || hasHomePhone)
                {
                    if (hasMobilePhone)
                        addressTitle.append(", т.").append(person.getContactData().getPhoneMobile());
                    if (hasHomePhone)
                        addressTitle.append(", т.").append(person.getContactData().getPhoneFact());
                }
            }
            row.add(addressTitle.toString());
            row.add(card.getCitizenship().getShortTitle());
            row.add("");
            row.add("");
            row.add("");
            row.add(direction.getCompensationType().getShortTitle());
            Set<PersonBenefit> benefits = benefitMap.get(person);
            row.add(direction.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION) ? (benefits == null || benefits.isEmpty()) ? "?" : UniStringUtils.join(benefits, "title", ", ") : "");
            if (direction.isTargetAdmission())
            {
                if (taKindsCount <= 1 || direction.getTargetAdmissionKind() == null)
                    row.add("да");
                else
                    row.add(direction.getTargetAdmissionKind().getShortTitle());
            } else
                row.add("");
            Set<EntrantEnrolmentRecommendation> recommendations = recommendationMap.get(entrant);
            row.add(recommendations == null ? null : UniStringUtils.join(recommendations, EntrantEnrolmentRecommendation.recommendation().title().s(), ", "));
            List<String> orderNumbers = orderNumberMap.get(direction.getId());
            row.add(orderNumbers == null ? "" : StringUtils.join(orderNumbers, ", "));

            tableData.add(getRow(row, direction));

            // оценки - отдельно из-за переводов строки

            List<String> state = new ArrayList<>();
            List<String> stateInternal = new ArrayList<>();
            List<String> internal = new ArrayList<>();
            Set<ChosenEntranceDiscipline> entranceDisciplines = util.getChosenEntranceDisciplineSet(direction);
            for (ChosenEntranceDiscipline discipline : entranceDisciplines)
            {
                String discTitle = discipline.getEnrollmentCampaignDiscipline().getShortTitle();
                MarkStatistic statistic = util.getMarkStatistic(discipline);
                // если есть оценка по ЕГЭ - добавляем в список для колонки ЕГЭ I
                if (statistic.getScaledStateExamMark() != null)
                    state.add(discTitle + " - " + DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(statistic.getScaledStateExamMark()));
                // если есть оценка или дисц. для сдачи по ЕГЭ (вуз) - добавляем в список для колонки ЕГЭ II
                Map<String, PairKey<ExamPassMark, ExamPassMarkAppeal>> internalMarks = new HashMap<>();
                for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> entry : statistic.getInternalMarkMap().entrySet())
                    internalMarks.put(entry.getKey().getCode(), entry.getValue());
                if (internalMarks.containsKey(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                {
                    PairKey<ExamPassMark, ExamPassMarkAppeal> stateInternalMark = internalMarks.get(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL);
                    stateInternal.add(discTitle + " - " + (stateInternalMark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(stateInternalMark.getSecond() != null ? stateInternalMark.getSecond().getMark() : stateInternalMark.getFirst().getMark())));
                }
                // если есть оценка или дисц. для сдачи по внутр. формам сдачи, или диплом олимпиады - добавляем в список для колонки Экз. вуза
                Double maxInternalMark = null;
                boolean foundInternalMark = false;
                for (String key : new String[]{UniecDefines.SUBJECT_PASS_FORM_EXAM, UniecDefines.SUBJECT_PASS_FORM_INTERVIEW, UniecDefines.SUBJECT_PASS_FORM_TEST})
                {
                    if (internalMarks.containsKey(key))
                    {
                        foundInternalMark = true;
                        PairKey<ExamPassMark, ExamPassMarkAppeal> internalMark = internalMarks.get(key);
                        if (internalMark != null)
                            maxInternalMark = Math.max(maxInternalMark == null ? .0 : maxInternalMark, internalMark.getSecond() != null ? internalMark.getSecond().getMark() : internalMark.getFirst().getMark());
                    }
                }
                if (statistic.getOlympiad() != null)
                {
                    foundInternalMark = true;
                    maxInternalMark = (double) statistic.getOlympiad();
                }
                if (foundInternalMark)
                    internal.add(discTitle + " - " + (maxInternalMark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(maxInternalMark)));
            }
            marks.add(new String[]{StringUtils.join(state, "\\par "), StringUtils.join(stateInternal, "\\par "), StringUtils.join(internal, "\\par ")});
        }

        final int rowCount = tableData.size();

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.toArray(new String[rowCount][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex >= 6 && colIndex <= 8)
                {
                    String content = marks.get(rowIndex)[colIndex - 6];
                    IRtfText text = RtfBean.getElementFactory().createRtfText(content);
                    text.setRaw(true);
                    return Collections.singletonList((IRtfElement) text);
                }
                return null;
            }
        });
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private byte[] generateContent()
    {
        try
        {
            ByteArrayOutputStream out = null;
            try
            {
                out = new ByteArrayOutputStream();
                ZipOutputStream zipOut = new ZipOutputStream(out);
                List<EnrollmentDirection> enrollmentDirections = new ArrayList<>(directionMap.keySet());
                if (enrollmentDirections.isEmpty())
                    throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");
                Map<String, Integer> numbers = new HashMap<>();
                for (EnrollmentDirection direction : enrollmentDirections)
                {
                    EducationOrgUnit orgUnit = direction.getEducationOrgUnit();
                    StringBuilder filename = new StringBuilder();
                    filename.append(orgUnit.getFormativeOrgUnit().getShortTitle()).append(" ");
                    if (orgUnit.getTerritorialOrgUnit() != null)
                        filename.append("(").append(orgUnit.getTerritorialOrgUnit().getTerritorialTitle()).append(") ");
                    filename.append(orgUnit.getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle()).append(" ");
                    filename.append("(").append(orgUnit.getDevelopForm().getTitle()).append(", ")
                            .append(StringUtils.trimToNull(orgUnit.getDevelopCondition().getShortTitle()) == null ? orgUnit.getDevelopCondition().getTitle() : orgUnit.getDevelopCondition().getShortTitle()).append(", ")
                            .append(StringUtils.trimToNull(orgUnit.getDevelopTech().getShortTitle()) == null ? orgUnit.getDevelopTech().getTitle() : orgUnit.getDevelopTech().getShortTitle()).append(", ")
                            .append(orgUnit.getDevelopPeriod().getTitle()).append(")");
                    String filenameStr = CoreStringUtils.transliterate(filename.toString());
                    if (numbers.containsKey(filenameStr))
                    {
                        int number = numbers.get(filenameStr) + 1;
                        numbers.put(filenameStr, number);
                        filenameStr = filenameStr + " - " + String.valueOf(number);
                    } else
                        numbers.put(filenameStr, 0);
                    zipOut.putNextEntry(new ZipEntry(filenameStr + ".rtf"));
                    zipOut.write(print(direction));
                    zipOut.closeEntry();
                }
                zipOut.close();
                return out.toByteArray();
            } finally
            {
                if (out != null)
                    out.close();
            }
        } catch (ApplicationException e)
        {
            throw e;
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Для кастомизации в проектах
     * @return
     */
    public String[] getRow(List<String> row, RequestedEnrollmentDirection direction) {
        return row.toArray(new String[row.size()]);
    }
}
