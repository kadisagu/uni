// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.DailySummaryWithCM.Add;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 09.06.2010
 */
public class DailySummaryWithCMReportBuilder
{
    private Model model;
    private Session session;

    public DailySummaryWithCMReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        DatabaseFile content = new DatabaseFile();
        content.setContent(print());
        return content;
    }

    private byte[] print()
    {
        IUniBaseDao coreDao = UniDaoFacade.getCoreDao();

        IScriptItem templateDocument = coreDao.getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_DAILY_SUMMARY_WITH_CM);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfDocument document = template.getClone();

        CompensationType budget = coreDao.getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = coreDao.getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);
        List<DevelopForm> developForms = coreDao.getCatalogItemList(DevelopForm.class);
        Calendar lastDate = Calendar.getInstance(); lastDate.setTime(model.getReport().getDateTo());

        Map<OrgUnit, Map<EducationLevelsHighSchool, Set<CoreCollectionUtils.Pair<EnrollmentDirection, CompensationType>>>> ouMap = new HashMap<>();
        for (EnrollmentDirection direction : getEnrollmentDirectionBuilder().createStatement(session).<EnrollmentDirection>list()) {
            if (direction.isBudget())
                SafeMap.safeGet(SafeMap.safeGet(ouMap, direction.getEducationOrgUnit().getFormativeOrgUnit(), HashMap.class), direction.getEducationOrgUnit().getEducationLevelHighSchool(), HashSet.class).add(new CoreCollectionUtils.Pair<>(direction, budget));
            if (direction.isContract())
                SafeMap.safeGet(SafeMap.safeGet(ouMap, direction.getEducationOrgUnit().getFormativeOrgUnit(), HashMap.class), direction.getEducationOrgUnit().getEducationLevelHighSchool(), HashSet.class).add(new CoreCollectionUtils.Pair<>(direction, contract));
        }

        Map<CoreCollectionUtils.Pair<EnrollmentDirection, CompensationType>, List<RequestedEnrollmentDirection>> directionMap = new HashMap<>();
        for (RequestedEnrollmentDirection reqDirection : getRequestedBuilder().column("d").createStatement(session).<RequestedEnrollmentDirection>list()) {
            EnrollmentDirection direction = reqDirection.getEnrollmentDirection();
            SafeMap.safeGet(directionMap, new CoreCollectionUtils.Pair<>(direction, reqDirection.getCompensationType()), ArrayList.class ).add(reqDirection);
            if (reqDirection.getCompensationType().isBudget())
                SafeMap.safeGet(SafeMap.safeGet(ouMap, direction.getEducationOrgUnit().getFormativeOrgUnit(), HashMap.class), direction.getEducationOrgUnit().getEducationLevelHighSchool(), HashSet.class).add(new CoreCollectionUtils.Pair<>(direction, budget));
            else
                SafeMap.safeGet(SafeMap.safeGet(ouMap, direction.getEducationOrgUnit().getFormativeOrgUnit(), HashMap.class), direction.getEducationOrgUnit().getEducationLevelHighSchool(), HashSet.class).add(new CoreCollectionUtils.Pair<>(direction, contract));
        }

        List<OrgUnit> formativeOuList = new ArrayList<>(ouMap.keySet());
        Collections.sort(formativeOuList, ITitled.TITLED_COMPARATOR);

        List<String[]> tableData = new ArrayList<>();
        final Set<Integer> grayRowIndexes = new HashSet<>();
        final Set<Integer> boldHeaderRowIndexes = new HashSet<>();
        final Set<Integer> totalsRowIndexes = new HashSet<>();
        final int grayColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);

        Map<DevelopForm, RowData> ouTotals = new HashMap<>();
        ouTotals.put(null, new RowData());
        for (DevelopForm developForm : developForms) {
            ouTotals.put(developForm, new RowData("Итого по " + developForm.getGenCaseTitle() + ":"));
        }

        Map<Object, RowData> totals = new HashMap<>();
        int rowIndex = 0;
        for (OrgUnit ou : formativeOuList) {
            tableData.add(new String[] {ou.getPrintTitle()});
            grayRowIndexes.add(rowIndex++);
            totals.put(ou, new RowData("", "", "", "Итого по факультету:"));
            for (DevelopForm developForm : developForms) {
                totals.put(developForm, new RowData("", "", "", "Итого по " + developForm.getGenCaseTitle() + ":"));
            }

            Map<EducationLevelsHighSchool, Set<CoreCollectionUtils.Pair<EnrollmentDirection, CompensationType>>> hsMap = ouMap.get(ou);
            List<EducationLevelsHighSchool> hsList = new ArrayList<>(hsMap.keySet());
            Collections.sort(hsList, ITitled.TITLED_COMPARATOR);
            for (EducationLevelsHighSchool hs : hsList) {
                tableData.add(new String[] {(hs.isSpeciality() ? "Специальность " : "Направление ") + hs.getEducationLevel().getTitleCodePrefix() + " " + hs.getTitle()});
                boldHeaderRowIndexes.add(rowIndex++);

                List<CoreCollectionUtils.Pair<EnrollmentDirection, CompensationType>> pairs = new ArrayList<>(hsMap.get(hs));
                Collections.sort(pairs, (o1, o2) -> {
                    int result = o1.getX().getTitle().compareTo(o1.getX().getTitle());
                    if (result == 0)
                        result = o1.getX().getEducationOrgUnit().getDevelopCondition().getCode().compareTo(o2.getX().getEducationOrgUnit().getDevelopCondition().getCode());
                    if (result == 0)
                        result = o1.getX().getEducationOrgUnit().getDevelopForm().getCode().compareTo(o2.getX().getEducationOrgUnit().getDevelopForm().getCode());
                    if (result == 0)
                        result = o1.getX().getEducationOrgUnit().getDevelopPeriod().getPriority() - o2.getX().getEducationOrgUnit().getDevelopPeriod().getPriority();
                    if (result == 0)
                        result = o1.getX().getId().compareTo(o2.getX().getId());
                    if (result == 0)
                        result = o1.getY().getCode().compareTo(o2.getY().getCode());
                    return result;
                });
                for (CoreCollectionUtils.Pair<EnrollmentDirection, CompensationType> pair : pairs) {
                    List<RequestedEnrollmentDirection> directions = directionMap.get(pair);

                    RowData row = new RowData(
                        pair.getX().getTitle(),
                        pair.getX().getEducationOrgUnit().getDevelopCondition().getTitle(),
                        pair.getX().getEducationOrgUnit().getDevelopForm().getTitle() + ", " + pair.getY().getTitle(),
                        pair.getX().getEducationOrgUnit().getDevelopPeriod().getTitle()
                    );


                    Integer rowPlan = pair.getY().isBudget() ? pair.getX().getMinisterialPlan() : pair.getX().getContractPlan();
                    row.plan = rowPlan == null ? 0 : rowPlan;
                    row.reqCount = directions == null ? 0 : directions.size();
                    row.origCount = directions == null ? 0 : CollectionUtils.countMatches(directions, RequestedEnrollmentDirectionGen::isOriginalDocumentHandedIn);
                    row.atLastDay = directions == null ? 0 : CollectionUtils.countMatches(
                            directions,
                            requestedEnrollmentDirection -> DateUtils.isSameDay(model.getReport().getDateTo(), requestedEnrollmentDirection.getEntrantRequest().getRegDate())
                    );

                    tableData.add(row.printRow()); rowIndex++;
                    ouTotals.get(null).addRow(row);
                    ouTotals.get(pair.getX().getEducationOrgUnit().getDevelopForm()).addRow(row);
                    totals.get(ou).addRow(row);
                    totals.get(pair.getX().getEducationOrgUnit().getDevelopForm()).addRow(row);
                }
            }
            for (DevelopForm developForm : developForms) {
                tableData.add(totals.get(developForm).printRow());
                totalsRowIndexes.add(rowIndex++);
            }
            tableData.add(totals.get(ou).printRow());
            totalsRowIndexes.add(rowIndex++);
        }

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.toArray(new String[tableData.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (grayRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                else if (boldHeaderRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                else if (totalsRowIndexes.contains(rowIndex) && colIndex == 3)
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                else if (totalsRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList) {
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QL);
                    for (int i = 1; i < row.getCellList().size(); i++)
                        SharedRtfUtil.setCellAlignment(row.getCellList().get(i), IRtfData.QC);
                }

                for (Integer rowIndex : grayRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                    RtfCell cell = row.getCellList().get(0);
                    cell.setBackgroundColorIndex(grayColorIndex);
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                }

                for (Integer rowIndex : boldHeaderRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 0);
                }

                for (Integer rowIndex : totalsRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.deleteCellNextIncrease(row, 0);
                    RtfUtil.deleteCellNextIncrease(row, 0);
                    RtfUtil.deleteCellNextIncrease(row, 0);

                    RtfCell cell = row.getCellList().get(0);
                    SharedRtfUtil.setCellAlignment(cell, IRtfData.QR);
                }
            }
        });
        tableModifier.modify(document);

        tableData = new ArrayList<>();
        for (DevelopForm developForm : developForms) {
            tableData.add(ouTotals.get(developForm).printRow());
        }

        tableModifier = new RtfTableModifier().put("T1", tableData.toArray(new String[tableData.size()][]));
        tableModifier.modify(document);

        RowData totalData = ouTotals.get(null);
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReport().getDateTo()));
        injectModifier.put("T2", String.valueOf(totalData.plan));
        injectModifier.put("T3", String.valueOf(totalData.reqCount));
        injectModifier.put("T4", String.valueOf(totalData.atLastDay));
        injectModifier.put("T5", String.valueOf(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalData.plan == 0 ? .0 : (double) totalData.reqCount / totalData.plan)));
        injectModifier.put("T6", String.valueOf(totalData.origCount));
        injectModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private static class RowData
    {
        List<String> result = new ArrayList<>();
        int reqCount;
        int origCount;
        int plan;
        int atLastDay;

        private RowData(String... data)
        {
            Collections.addAll(result, data);
        }

        private String[] printRow()
        {
            result.add(String.valueOf(plan));
            result.add(String.valueOf(reqCount));
            result.add(String.valueOf(atLastDay));
            result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(plan == 0 ? .0 : (double) reqCount / plan));
            result.add(String.valueOf(origCount));
            return result.toArray(new String[result.size()]);
        }

        private void addRow(RowData row)
        {
            reqCount += row.reqCount;
            origCount += row.origCount;
            plan += row.plan;
            atLastDay += row.atLastDay;
        }
    }

    private DQLSelectBuilder getEnrollmentDirectionBuilder()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EnrollmentDirection.class, "d")
        .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), value(model.getEnrollmentCampaign())))
        .where(or(
            eq(property(EnrollmentDirection.budget().fromAlias("d")), value(Boolean.TRUE)),
            eq(property(EnrollmentDirection.contract().fromAlias("d")), value(Boolean.TRUE))))
            .column("d");

        dql.joinPath(DQLJoinType.inner, "d." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        dql.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        dql.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");

        patchOU(dql);

        return dql;
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirection.class, "d");
        dql.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        dql.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        dql.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");
        dql.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        dql.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        dql.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        dql.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");

        dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.P_ARCHIVAL), DQLExpressions.value(Boolean.FALSE)));
        dql.where(DQLExpressions.ne(DQLExpressions.property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), DQLExpressions.value(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY)));
        dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), DQLExpressions.value(model.getReport().getEnrollmentCampaign())));
        dql.where(DQLExpressions.betweenDays("request." + EntrantRequest.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));

        if (model.getCompensationType() != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property("ct." + CompensationType.P_ID), DQLExpressions.value(model.getCompensationType().getId())));
        if (model.isStudentCategoryActive())
            dql.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.studentCategory().fromAlias("d")), model.getStudentCategoryList()));
        patchOU(dql);

        return dql;
    }

    private void patchOU(DQLSelectBuilder dql)
    {
        if (model.isQualificationActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("el." + EducationLevels.L_QUALIFICATION), model.getQualificationList()));
        if (model.isFormativeOrgUnitActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
        {
            if (model.getTerritorialOrgUnitList().isEmpty())
                dql.where(DQLExpressions.isNull(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), model.getTerritorialOrgUnitList()));
        }
        if (model.isDevelopFormActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_FORM), model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_TECH), model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), model.getDevelopPeriodList()));
    }
}
