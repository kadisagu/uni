package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribAddEdit;

import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
	void save(Model model);
	void validate(EcgDistribQuota q, int quota) throws ApplicationException;
}
