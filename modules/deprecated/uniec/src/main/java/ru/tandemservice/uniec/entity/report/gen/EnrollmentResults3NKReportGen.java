package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма 3НК (результаты приема, по форме 2008 года)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentResults3NKReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport";
    public static final String ENTITY_NAME = "enrollmentResults3NKReport";
    public static final int VERSION_HASH = 202823135;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_FORMING_ORG_UNIT_TITLE = "formingOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";

    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private CompensationType _compensationType;     // Вид возмещения затрат
    private DevelopForm _developForm;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _qualificationTitle;     // Квалификация
    private String _formingOrgUnitTitle;     // Формирующее подр.
    private String _territorialOrgUnitTitle;     // Территориальное подр.
    private String _studentCategory;     // Категория поступающего
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormingOrgUnitTitle()
    {
        return _formingOrgUnitTitle;
    }

    /**
     * @param formingOrgUnitTitle Формирующее подр..
     */
    public void setFormingOrgUnitTitle(String formingOrgUnitTitle)
    {
        dirty(_formingOrgUnitTitle, formingOrgUnitTitle);
        _formingOrgUnitTitle = formingOrgUnitTitle;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подр..
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория поступающего.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrollmentResults3NKReportGen)
        {
            setDateFrom(((EnrollmentResults3NKReport)another).getDateFrom());
            setDateTo(((EnrollmentResults3NKReport)another).getDateTo());
            setEnrollmentCampaign(((EnrollmentResults3NKReport)another).getEnrollmentCampaign());
            setCompensationType(((EnrollmentResults3NKReport)another).getCompensationType());
            setDevelopForm(((EnrollmentResults3NKReport)another).getDevelopForm());
            setDevelopConditionTitle(((EnrollmentResults3NKReport)another).getDevelopConditionTitle());
            setQualificationTitle(((EnrollmentResults3NKReport)another).getQualificationTitle());
            setFormingOrgUnitTitle(((EnrollmentResults3NKReport)another).getFormingOrgUnitTitle());
            setTerritorialOrgUnitTitle(((EnrollmentResults3NKReport)another).getTerritorialOrgUnitTitle());
            setStudentCategory(((EnrollmentResults3NKReport)another).getStudentCategory());
            setExcludeParallel(((EnrollmentResults3NKReport)another).getExcludeParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentResults3NKReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentResults3NKReport.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentResults3NKReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "compensationType":
                    return obj.getCompensationType();
                case "developForm":
                    return obj.getDevelopForm();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "formingOrgUnitTitle":
                    return obj.getFormingOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "excludeParallel":
                    return obj.getExcludeParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "formingOrgUnitTitle":
                    obj.setFormingOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "compensationType":
                        return true;
                case "developForm":
                        return true;
                case "developConditionTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "formingOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "studentCategory":
                        return true;
                case "excludeParallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "compensationType":
                    return true;
                case "developForm":
                    return true;
                case "developConditionTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "formingOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "studentCategory":
                    return true;
                case "excludeParallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "compensationType":
                    return CompensationType.class;
                case "developForm":
                    return DevelopForm.class;
                case "developConditionTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "formingOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "studentCategory":
                    return String.class;
                case "excludeParallel":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentResults3NKReport> _dslPath = new Path<EnrollmentResults3NKReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentResults3NKReport");
    }
            

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getFormingOrgUnitTitle()
     */
    public static PropertyPath<String> formingOrgUnitTitle()
    {
        return _dslPath.formingOrgUnitTitle();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    public static class Path<E extends EnrollmentResults3NKReport> extends StorableReport.Path<E>
    {
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private CompensationType.Path<CompensationType> _compensationType;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _formingOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<Boolean> _excludeParallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EnrollmentResults3NKReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EnrollmentResults3NKReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EnrollmentResults3NKReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EnrollmentResults3NKReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getFormingOrgUnitTitle()
     */
        public PropertyPath<String> formingOrgUnitTitle()
        {
            if(_formingOrgUnitTitle == null )
                _formingOrgUnitTitle = new PropertyPath<String>(EnrollmentResults3NKReportGen.P_FORMING_ORG_UNIT_TITLE, this);
            return _formingOrgUnitTitle;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(EnrollmentResults3NKReportGen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(EnrollmentResults3NKReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.EnrollmentResults3NKReport#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(EnrollmentResults3NKReportGen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

        public Class getEntityClass()
        {
            return EnrollmentResults3NKReport.class;
        }

        public String getEntityName()
        {
            return "enrollmentResults3NKReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
