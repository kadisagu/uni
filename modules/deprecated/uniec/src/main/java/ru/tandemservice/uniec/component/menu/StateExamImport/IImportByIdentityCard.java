package ru.tandemservice.uniec.component.menu.StateExamImport;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author vdanilov
 */
public interface IImportByIdentityCard extends ITitled {

    /**
     * @param enrollmentCampaign
     * @param deleteNotFound
     * @param importedContent
     * @return
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    byte[] doImport(EnrollmentCampaign enrollmentCampaign, boolean deleteNotFound, byte[] importedContent);


}
