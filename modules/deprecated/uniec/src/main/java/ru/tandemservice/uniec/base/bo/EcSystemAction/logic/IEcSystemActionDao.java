/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public interface IEcSystemActionDao extends INeedPersistenceSupport
{
    void updateEntrantData();

    /**
     * Ограничивает количество профильных испытаний в наборе одним
     * Сбрасывает испытания по выбору для профильных испытания
     * Вычисляет профильное испытание для выбранного направления приема
     */
    void setRequestedEnrollmentDirectionProfileDisciplines();

    /**
     * правит старые названия экзаменационных групп
     * которые используют механизм 1
     */
    void doCorrectExamGroupNamesMechanism1();
}
