package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.gen.RequestedProfileKnowledgeGen;

/**
 * requestedProfileKnowledge
 */
public class RequestedProfileKnowledge extends RequestedProfileKnowledgeGen implements ITitled
{
    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        return getProfileKnowledge().getTitle();
    }
}