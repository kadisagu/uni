/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.List;
import java.util.Map;

import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * Планы приема по направлениям и по видам целевого приема
 *
 * @author Vasily Zhukov
 * @since 11.07.2011
 */
public class EcgQuotaDTO implements IEcgQuotaDTO
{
    /**
     * используемые виды цп
     * всегда не пусто
     */
    private List<TargetAdmissionKind> _taKindList;

    /**
     * направление приема -> план
     * план всегда не null
     */
    private Map<Long, Integer> _quotaMap;

    /**
     * направление приема -> вид целевого приема -> план
     * план всегда не null
     */
    private Map<Long, Map<Long, Integer>> _taQuotaMap;

    /**
     * количество свободных мест
     */
    private Integer _totalQuota;

    public EcgQuotaDTO(List<TargetAdmissionKind> taKindList, Map<Long, Integer> quotaMap, Map<Long, Map<Long, Integer>> taQuotaMap)
    {
        _taKindList = taKindList;
        _quotaMap = quotaMap;
        _taQuotaMap = taQuotaMap;
    }

    // Getters

    @Override
    public List<TargetAdmissionKind> getTaKindList()
    {
        return _taKindList;
    }

    @Override
    public Map<Long, Integer> getQuotaMap()
    {
        return _quotaMap;
    }

    @Override
    public Map<Long, Map<Long, Integer>> getTaQuotaMap()
    {
        return _taQuotaMap;
    }

    @Override
    public int getTotalQuota()
    {
        if (_totalQuota == null)
        {
            int count = 0;

            for (Integer quota : _quotaMap.values())
                count += quota;

            for (Map<Long, Integer> map : _taQuotaMap.values())
                for (Integer taQuota : map.values())
                    count += taQuota;

            _totalQuota = count;
        }

        return _totalQuota;
    }

    @Override
    public boolean isEmpty()
    {
        return getTotalQuota() == 0;
    }
}
