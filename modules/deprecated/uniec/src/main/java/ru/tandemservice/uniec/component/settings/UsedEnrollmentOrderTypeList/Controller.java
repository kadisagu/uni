/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.UsedEnrollmentOrderTypeList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;

/**
 * @author vip_delete
 * @since 22.07.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());
        
        getDao().prepare(model);

        //model.setMessageSource(getMessageSource());

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentOrderType> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", EnrollmentOrderType.entrantEnrollmentOrderType().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));

        dataSource.addColumn(new ToggleColumn("Использовать", EnrollmentOrderType.P_USED).setListener("onClickUsed"));

        dataSource.addColumn(new ToggleColumn("Основание", EnrollmentOrderType.P_BASIC, "Указывается в приказе", "Не указывается в приказе").setListener("onClickBasic"));
        dataSource.addColumn(new ToggleColumn("Текст в блоке «Приказываю»", EnrollmentOrderType.P_COMMAND, "Указывается в приказе", "Не указывается в приказе").setListener("onClickCommand"));
        dataSource.addColumn(new ToggleColumn("Группа", EnrollmentOrderType.P_GROUP, "Указывается в приказе", "Не указывается в приказе").setListener("onClickGroup"));
        dataSource.addColumn(new ToggleColumn("Староста", EnrollmentOrderType.P_SELECT_HEADMAN, "Указывается в приказе", "Не указывается в приказе").setListener("onClickSelectHeadman"));
        dataSource.addColumn(new ToggleColumn(getMessage("reasonAndBasic"), EnrollmentOrderType.P_REASON_AND_BASIC, getMessage("reasonAndBasicAdd"), getMessage("reasonAndBasicDelete")).setListener("onClickReasonAndBasic"));
        dataSource.addColumn(new ToggleColumn("Дата зачисления", EnrollmentOrderType.P_USE_ENROLLMENT_DATE, "Указывается в приказе", "Не указывается в приказе").setListener("onClickUseEnrollmentDate"));
        model.setDataSource(dataSource);
    }

    public void onClickUp(IBusinessComponent component)
    {
        CommonManager.instance().commonPriorityDao().doChangePriorityUp((Long)component.getListenerParameter(), EnrollmentOrderType.P_PRIORITY, EnrollmentOrderType.enrollmentCampaign(), getModel(component).getEnrollmentCampaign());
    }

    public void onClickDown(IBusinessComponent component)
    {
        CommonManager.instance().commonPriorityDao().doChangePriorityDown((Long)component.getListenerParameter(), EnrollmentOrderType.P_PRIORITY, EnrollmentOrderType.enrollmentCampaign(), getModel(component).getEnrollmentCampaign());
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }

    public void onClickUsed(IBusinessComponent component)
    {
        getDao().updateToggleUsed((Long) component.getListenerParameter());
    }

    public void onClickBasic(IBusinessComponent component)
    {
        getDao().updateToggleBasic((Long) component.getListenerParameter());
    }

    public void onClickCommand(IBusinessComponent component)
    {
        getDao().updateToggleCommand((Long) component.getListenerParameter());
    }

    public void onClickGroup(IBusinessComponent component)
    {
        getDao().updateToggleGroup((Long) component.getListenerParameter());
    }

    public void onClickReasonAndBasic(IBusinessComponent component)
    {
        getDao().updateToggleReasonAndBasic((Long) component.getListenerParameter());
    }

    public void onClickSelectHeadman(IBusinessComponent component)
    {
        getDao().updateToggleSelectHeadman((Long) component.getListenerParameter());
    }

    public void onClickUseEnrollmentDate(IBusinessComponent component)
    {
        getDao().updateToggleUseEnrollmentDate((Long) component.getListenerParameter());
    }
}
