package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.entity.report.gen.EntrantRequestByCGDailyReportGen;

/**
 * Ежедневная сводка по заявлениям (по конкурсным группам) с нарастающим итогом
 */
public class EntrantRequestByCGDailyReport extends EntrantRequestByCGDailyReportGen
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}