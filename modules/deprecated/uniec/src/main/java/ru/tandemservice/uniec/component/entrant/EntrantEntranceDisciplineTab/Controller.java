/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.component.entrant.ChooseEntranceDiscipline.ChooseEntranceDisciplineModel;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author vip_delete
 * @since 16.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        for (EntrantRequest entrantRequest : model.getEntrantRequestList())
            prepareEntranceDisciplineDataSource(component, entrantRequest);

        if (model.isFormingForEntrant())
            prepareExamPassDisciplineDataSource(component, null);
        else
            for (EntrantRequest entrantRequest : model.getEntrantRequestList())
                prepareExamPassDisciplineDataSource(component, entrantRequest);
    }

    private void prepareEntranceDisciplineDataSource(IBusinessComponent component, EntrantRequest entrantRequest)
    {
        final Model model = getModel(component);

        if (model.getEntranceDisciplineDataSourceMap().containsKey(entrantRequest)) return;

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEntranceDisciplineDataSource(model);
        });

        AbstractColumn<?> enrollmentDirectionTitleColumn = new SimpleColumn(Model.COLUMN_ENROLLMENT_DIRECTION_TITLE, "Направление подготовки (специальность)", "", new BaseRawFormatter<RequestedEnrollmentDirection>()
        {
            @Override
            public String format(RequestedEnrollmentDirection source)
            {
                EducationOrgUnit educationOrgUnit = source.getEnrollmentDirection().getEducationOrgUnit();
                List<String> lines = new ArrayList<>();

                lines.add(educationOrgUnit.getTitleWithFormAndCondition());
                lines.add("<b>" + educationOrgUnit.getFormativeOrgUnit().getOrgUnitType().getTitle() + ": </b>" + educationOrgUnit.getFormativeOrgUnit().getTitle());
                if (educationOrgUnit.getTerritorialOrgUnit() != null)
                {
                    lines.add("<b>Территориальное подр.: </b>" + educationOrgUnit.getTerritorialOrgUnit().getTerritorialTitle());
                }
                lines.add("<b>Вид затрат: </b>" + source.getCompensationType().getShortTitle() + "&nbsp;&nbsp;&nbsp;<b>Вид конкурса: </b>" + source.getCompetitionKind().getTitle());
                lines.add("<b>Состояние абитуриента: </b>" + source.getState().getTitle());
                return StringUtils.join(lines.iterator(), "<br/>");
            }
        }).setClickable(false).setOrderable(false);
        enrollmentDirectionTitleColumn.setStyle("width:50%;vertical-align:top;");
        dataSource.addColumn(enrollmentDirectionTitleColumn);

        AbstractColumn<?> enrollmentDisciplinesTitleColumn = new SimpleColumn(Model.COLUMN_ENROLLMENT_DISCIPLINES_TITLE, "Выбранные вступительные испытания", "", new BaseRawFormatter<RequestedEnrollmentDirection>()
        {
            @Override
            public String format(RequestedEnrollmentDirection source)
            {
                Set<ChosenEntranceDiscipline> chosenDisciplinesSet = model.getChosenEntranceDisciplineMap().get(source);
                if (chosenDisciplinesSet == null)
                {
                    return source.getCompetitionKind().getTitle();
                }

                List<ChosenEntranceDiscipline> chosenEntranceDisciplines = new ArrayList<>(chosenDisciplinesSet);
                Collections.sort(chosenEntranceDisciplines, ITitled.TITLED_COMPARATOR);

                List<String> lines = new ArrayList<>();
                for (ChosenEntranceDiscipline item : chosenEntranceDisciplines)
                {
                    String line = (lines.size() + 1) + ". " + item.getTitle();
                    lines.add((model.getNonActualDisciplinesDirectionSet().contains(source)) ? "<span style='color:red'>" + line + "</span>" : line);
                }
                return StringUtils.join(lines.iterator(), "<br/>");
            }
        }).setClickable(false).setOrderable(false);
        enrollmentDisciplinesTitleColumn.setStyle("width:50%;vertical-align:top;");
        dataSource.addColumn(enrollmentDisciplinesTitleColumn);
        dataSource.addColumn(new ActionColumn("Выбрать вступительные испытания", ActionColumn.EDIT, "onClickAddEntranceDisciplines")
                                     .setPermissionKey("chooseEntranceDiscipline")
                                     .setDisableHandler(entity -> !model.isAccessible() || ((RequestedEnrollmentDirection) entity).isWithoutEntranceDisciplines()));

        model.getEntranceDisciplineDataSourceMap().put(entrantRequest, dataSource);
    }

    private void prepareExamPassDisciplineDataSource(IBusinessComponent component, EntrantRequest entrantRequest)
    {
        final Model model = getModel(component);
        EntrantExamListWrapper wrapper = model.getEntrantExamListMap().get(entrantRequest == null ? model.getEntrant().getId() : entrantRequest.getId());

        if (wrapper == null || wrapper.getExamPassDisciplineDataSource() != null) return;

        final EntrantExamList examList = wrapper.getEntrantExamList();

        DynamicListDataSource<ExamPassDiscipline> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEntrantExamPassDisciplineDataSource(examList, model);
        });

        AbstractColumn<?> examPassDisciplineTitleColumn = new SimpleColumn("Название предмета", "", new BaseRawFormatter<ExamPassDiscipline>()
        {
            @Override
            public String format(ExamPassDiscipline source)
            {
                String line = source.getEnrollmentCampaignDiscipline().getTitle();

                if (model.getNonActualExamPassSet().contains(source))
                    return "<div style='color:red'>" + line + "</div>";
                else
                    return line;
            }
        }).setClickable(false).setOrderable(false);

        dataSource.addColumn(examPassDisciplineTitleColumn);
        dataSource.addColumn(new SimpleColumn("Форма сдачи", ExamPassDiscipline.subjectPassForm().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Аудитория", ExamPassDiscipline.auditory().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата сдачи", "")
                                     .setFormatter(new BaseRawFormatter<ExamPassDiscipline>()
                                                   {
                                                       @Override
                                                       public String format(ExamPassDiscipline source)
                                                       {
                                                           Date passDate = source.getPassDate();
                                                           if (passDate == null) return  "<div style='color:red'>Дата не задана</div>";
                                                           else
                                                               return DateFormatter.DEFAULT_DATE_FORMATTER.format(passDate);
                                                       }
                                                   })
                                     .setOrderable(false).setClickable(false));
        IEntityHandler entityHandler = entity -> !model.isAccessible();
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("editExamPassDiscipline").setDisableHandler(entityHandler));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить дисциплину для сдачи?").setPermissionKey("deleteExamPassDiscipline").setDisableHandler(entityHandler));
        wrapper.setExamPassDisciplineDataSource(dataSource);
    }

    public void onClickAddEntranceDisciplines(IBusinessComponent component)
    {
        Map<String, Object> params = new HashMap<>();
        params.put(ChooseEntranceDisciplineModel.BINDING_DIRECTION_ID, component.getListenerParameter());
        params.put(ChooseEntranceDisciplineModel.BINDING_ENTRANT_REQUEST_ID, null);
        params.put(ChooseEntranceDisciplineModel.BINDING_ENTRANT_ID, null);
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.CHOOSE_ENTRANCE_DISCIPLINE, params));
    }

    public void onClickAddEntranceDisciplinesForRequest(IBusinessComponent component)
    {
        Map<String, Object> params = new HashMap<>();
        params.put(ChooseEntranceDisciplineModel.BINDING_DIRECTION_ID, null);
        params.put(ChooseEntranceDisciplineModel.BINDING_ENTRANT_REQUEST_ID, component.getListenerParameter());
        params.put(ChooseEntranceDisciplineModel.BINDING_ENTRANT_ID, null);
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.CHOOSE_ENTRANCE_DISCIPLINE, params));
    }

    public void onClickAddEntranceDisciplinesForEntrant(IBusinessComponent component)
    {
        Map<String, Object> params = new HashMap<>();
        params.put(ChooseEntranceDisciplineModel.BINDING_DIRECTION_ID, null);
        params.put(ChooseEntranceDisciplineModel.BINDING_ENTRANT_REQUEST_ID, null);
        params.put(ChooseEntranceDisciplineModel.BINDING_ENTRANT_ID, component.getListenerParameter());
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.CHOOSE_ENTRANCE_DISCIPLINE, params));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.EXAM_PASS_DISCIPLINE_EDIT, new ParametersMap().add("examPassDisciplineId", component.getListenerParameter())));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
        onRefreshComponent(component);
    }
}
