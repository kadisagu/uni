package ru.tandemservice.uniec.base.entity.ecgp.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpDistributionConfig;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение абитуриентов по профилям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgpDistributionGen extends EntityBase
 implements INaturalIdentifiable<EcgpDistributionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution";
    public static final String ENTITY_NAME = "ecgpDistribution";
    public static final int VERSION_HASH = -1209793381;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONFIG = "config";
    public static final String P_FORMING_DATE = "formingDate";

    private EcgpDistributionConfig _config;     // Конфигурация распределения абитуриентов по профилям
    private Date _formingDate;     // Дата формирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Конфигурация распределения абитуриентов по профилям. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EcgpDistributionConfig getConfig()
    {
        return _config;
    }

    /**
     * @param config Конфигурация распределения абитуриентов по профилям. Свойство не может быть null и должно быть уникальным.
     */
    public void setConfig(EcgpDistributionConfig config)
    {
        dirty(_config, config);
        _config = config;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgpDistributionGen)
        {
            if (withNaturalIdProperties)
            {
                setConfig(((EcgpDistribution)another).getConfig());
            }
            setFormingDate(((EcgpDistribution)another).getFormingDate());
        }
    }

    public INaturalId<EcgpDistributionGen> getNaturalId()
    {
        return new NaturalId(getConfig());
    }

    public static class NaturalId extends NaturalIdBase<EcgpDistributionGen>
    {
        private static final String PROXY_NAME = "EcgpDistributionNaturalProxy";

        private Long _config;

        public NaturalId()
        {}

        public NaturalId(EcgpDistributionConfig config)
        {
            _config = ((IEntity) config).getId();
        }

        public Long getConfig()
        {
            return _config;
        }

        public void setConfig(Long config)
        {
            _config = config;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgpDistributionGen.NaturalId) ) return false;

            EcgpDistributionGen.NaturalId that = (NaturalId) o;

            if( !equals(getConfig(), that.getConfig()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getConfig());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getConfig());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgpDistributionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgpDistribution.class;
        }

        public T newInstance()
        {
            return (T) new EcgpDistribution();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "config":
                    return obj.getConfig();
                case "formingDate":
                    return obj.getFormingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "config":
                    obj.setConfig((EcgpDistributionConfig) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "config":
                        return true;
                case "formingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "config":
                    return true;
                case "formingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "config":
                    return EcgpDistributionConfig.class;
                case "formingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgpDistribution> _dslPath = new Path<EcgpDistribution>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgpDistribution");
    }
            

    /**
     * @return Конфигурация распределения абитуриентов по профилям. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution#getConfig()
     */
    public static EcgpDistributionConfig.Path<EcgpDistributionConfig> config()
    {
        return _dslPath.config();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    public static class Path<E extends EcgpDistribution> extends EntityPath<E>
    {
        private EcgpDistributionConfig.Path<EcgpDistributionConfig> _config;
        private PropertyPath<Date> _formingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Конфигурация распределения абитуриентов по профилям. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution#getConfig()
     */
        public EcgpDistributionConfig.Path<EcgpDistributionConfig> config()
        {
            if(_config == null )
                _config = new EcgpDistributionConfig.Path<EcgpDistributionConfig>(L_CONFIG, this);
            return _config;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecgp.EcgpDistribution#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EcgpDistributionGen.P_FORMING_DATE, this);
            return _formingDate;
        }

        public Class getEntityClass()
        {
            return EcgpDistribution.class;
        }

        public String getEntityName()
        {
            return "ecgpDistribution";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
