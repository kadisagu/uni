/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamExport;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Arrays;

/**
 * @author vip_delete
 * @since 06.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        model.setExportTypeList(Arrays.asList(
                new IdentifiableWrapper((long) UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITHOUT_FIO, "По номерам свидетельств ЕГЭ (без ФИО) (ФБС)"),
                new IdentifiableWrapper((long) UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD_WITHOUT_FIO, "По данным удостоверения личности (без ФИО) (ФБС)"),
                new IdentifiableWrapper((long) UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER, "По номерам свидетельств ЕГЭ (ФБС)"),
                new IdentifiableWrapper((long) UniecDefines.EXPORT_TYPE_BY_IDENTITY_CARD, "По данным удостоверения личности (ФБС)"),
                new IdentifiableWrapper((long) UniecDefines.EXPORT_TYPE_BY_CERTIFICATE_NUMBER_WITH_FIO, "По регистрационному номеру и ФИО (нов.) (ФБС)"),
                new IdentifiableWrapper((long) UniecDefines.EXPORT_TYPE_BY_FIO_WITH_DOC, "По ФИО, номеру и серии документа (нов.) (ФБС)")
        ));

        model.setAcceptedList(Arrays.asList(
                new IdentifiableWrapper(Model.CERTIFICATE_ACCEPTED_ID, "Да"),
                new IdentifiableWrapper(Model.CERTIFICATE_NOT_ACCEPTED_ID, "Нет")
        ));

        model.setSentList(Arrays.asList(
                new IdentifiableWrapper(Model.CERTIFICATE_SENT_ID, "Да"),
                new IdentifiableWrapper(Model.CERTIFICATE_NOT_SENT_ID, "Нет")
        ));

        model.setSent(new IdentifiableWrapper(Model.CERTIFICATE_NOT_SENT_ID, "Нет"));
        
        model.setCertificateHoldingList(Arrays.asList(
                new IdentifiableWrapper(Model.CERTIFICATE_HOLDING_ID, "имеющих свидетельства ЕГЭ"),
                new IdentifiableWrapper(Model.CERTIFICATE_NOT_HOLDING_ID, "не имеющих свидетельства ЕГЭ")
        ));
    }
}
