/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import java.util.Map;

/**
 * Занятые места по профилям
 * 
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
public interface IEcgpQuotaUsedDTO
{
    /**
     * @return суммарное количество использованных мест
     */
    int getTotalUsed();

    /**
     * @return профиль -> занято мест (всегда не null)
     */
    Map<Long, Integer> getUsedMap();
}
