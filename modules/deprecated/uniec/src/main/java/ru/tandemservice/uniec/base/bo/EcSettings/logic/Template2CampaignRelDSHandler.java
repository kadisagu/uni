/* $Id: $ */
package ru.tandemservice.uniec.base.bo.EcSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcSettings.EcSettingsManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
public class Template2CampaignRelDSHandler extends DefaultComboDataSourceHandler
{
    public final static String REL = "rel";

    public Template2CampaignRelDSHandler(String ownerId)
    {
        super(ownerId, SurveyTemplate2EnrCampaignRel.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EducationYear educationYear = context.get(EcSettingsManager.EDUCATION_YEAR_PARAM);
        if (educationYear == null)
            return ListOutputBuilder.get(input, Collections.emptyList()).build();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentCampaign.class, "cmp")
                .column(property("cmp"))
                .where(eq(property("cmp", EnrollmentCampaign.educationYear()), value(educationYear)))
                .order(property("cmp", EnrollmentCampaign.title()));

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();

        List<Long> campaignIds = output.getRecordIds();
        Map<Long, SurveyTemplate2EnrCampaignRel> relByCampaingMap = new DQLSelectBuilder()
                .fromEntity(SurveyTemplate2EnrCampaignRel.class, "rel")
                .column(property("rel", SurveyTemplate2EnrCampaignRel.campaign().id()))
                .column(property("rel"))
                .where(in(property("rel", SurveyTemplate2EnrCampaignRel.campaign().id()), campaignIds))
                .createStatement(context.getSession()).<Object[]>list()
                .stream()
                .collect(Collectors.toMap(row -> (Long) row[0], row -> (SurveyTemplate2EnrCampaignRel) row[1]));

        for (DataWrapper wrapper : DataWrapper.wrap(output))
            wrapper.setProperty(REL, relByCampaingMap.get(wrapper.<EnrollmentCampaign>getWrapped().getId()));

        return output;
    }
}
