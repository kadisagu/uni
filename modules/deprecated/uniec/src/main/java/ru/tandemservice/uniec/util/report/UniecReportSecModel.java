// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util.report;

import org.tandemframework.sec.runtime.SecurityRuntime;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author oleyba
 * @since 04.05.2010
 */
public abstract class UniecReportSecModel
{
    private CommonPostfixPermissionModel secModel;

    public abstract OrgUnit getOrgUnit();
    public abstract String getViewKey();

    public Object getSecuredObject()
    {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getAddKey()
    {
        return null == getOrgUnit() ? "addUniecStorableReport" : getSecModel().getPermission("addStorableReport");
    }

    public String getDeleteKey()
    {
        return null == getOrgUnit() ? "deleteUniecStorableReport" : getSecModel().getPermission("deleteStorableReport");
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this.secModel = secModel;
    }
}

