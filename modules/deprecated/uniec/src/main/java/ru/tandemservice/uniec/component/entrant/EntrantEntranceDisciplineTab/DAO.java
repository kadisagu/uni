/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;

import java.util.*;

/**
 * @author vip_delete
 * @since 16.03.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings({"unchecked", "deprecation"})
    @Override
    public void prepare(Model model)
    {
        final Session session = getSession();

        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setFormingForEntrant(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT.equals(model.getEntrant().getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode()));
        model.setEntrantRequestList(getList(EntrantRequest.class, EntrantRequest.L_ENTRANT, model.getEntrant(), EntrantRequest.P_REG_DATE));

        // загружаем все выбранные вступительные испытания в нужном порядке
        MQBuilder chosenBuilder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "d");
        chosenBuilder.add(MQExpression.eq("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        List<ChosenEntranceDiscipline> chosenList = chosenBuilder.getResultList(session);

        // сохраняем их в удобной для показа структуре
        model.getChosenEntranceDisciplineMap().clear();
        for (ChosenEntranceDiscipline item : chosenList)
        {
            Set<ChosenEntranceDiscipline> chosenListItem = model.getChosenEntranceDisciplineMap().get(item.getChosenEnrollmentDirection());
            if (chosenListItem == null)
                model.getChosenEntranceDisciplineMap().put(item.getChosenEnrollmentDirection(), chosenListItem = new HashSet<>());
            chosenListItem.add(item);
        }

        // загружаем все экзаменационные листы
        MQBuilder examListBuilder = new MQBuilder(EntrantExamList.ENTITY_CLASS, "e");
        if (model.isFormingForEntrant())
        {
            examListBuilder.add(MQExpression.eq("e", EntrantExamList.L_ENTRANT + ".id", model.getEntrant().getId()));
        } else
        {
            examListBuilder.addDomain("r", EntrantRequest.ENTITY_CLASS);
            examListBuilder.add(MQExpression.eq("r", EntrantRequest.L_ENTRANT, model.getEntrant()));
            examListBuilder.add(MQExpression.eqProperty("e", EntrantExamList.L_ENTRANT + ".id", "r", EntrantRequest.P_ID));
        }

        // сохраняем их в удобной для показа структуре
        model.getEntrantExamListMap().clear();
        for (EntrantExamList examList : examListBuilder.<EntrantExamList>getResultList(session))
        {
            model.getEntrantExamListMap().put(((IEntity) examList.getEntrant()).getId(), new EntrantExamListWrapper(examList));
        }

        // загружаем все дисциплины для сдачи
        MQBuilder examPassBuilder = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d");
        examPassBuilder.add(MQExpression.in("d", ExamPassDiscipline.L_ENTRANT_EXAM_LIST, examListBuilder));

        // сохраняем их в удобной для показа структуре
        model.getExamPassMap().clear();
        Set<ExamPassDiscipline> examPassSet = new HashSet<>();
        for (ExamPassDiscipline examPass : examPassBuilder.<ExamPassDiscipline>getResultList(session))
        {
            examPassSet.add(examPass);
            Set<ExamPassDiscipline> set = model.getExamPassMap().get(examPass.getEntrantExamList());
            if (set == null)
                model.getExamPassMap().put(examPass.getEntrantExamList(), set = new HashSet<>());
            set.add(examPass);
        }

        // вычисляем неактуальные выбранные вступительные испытания и дисциплины для сдачи
        model.getNonActualDisciplinesDirectionSet().clear();
        model.getNonActualExamPassSet().clear();

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));

        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), model.getEntrant().getEnrollmentCampaign(), directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS);

        Set<ExamPassDiscipline> actualExamPassSet = new HashSet<ExamPassDiscipline>();
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                actualExamPassSet.addAll(dataUtil.getExamPassDisciplineSet(chosen));

        model.getNonActualExamPassSet().addAll(CollectionUtils.subtract(examPassSet, actualExamPassSet));

        //Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> direction2DisciplinesMap = ExamSetUtil.getDirection2disciplinesMap(getSession(), model.getEntrant().getEnrollmentCampaign(), null, null);
        for (Map.Entry<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> entry : model.getChosenEntranceDisciplineMap().entrySet())
        {
            RequestedEnrollmentDirection requestedEnrollmentDirection = entry.getKey();
            List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure = getExamSetStructure(requestedEnrollmentDirection);
            //direction2DisciplinesMap.get(requestedEnrollmentDirection.getEnrollmentDirection());

            List<ChosenEntranceDiscipline> distribution = Arrays.asList(MarkDistributionUtil.getNearestDistribution(entry.getValue(), structure));
            if (distribution.contains(null) || !distribution.containsAll(entry.getValue()))
            {
                model.getNonActualDisciplinesDirectionSet().add(requestedEnrollmentDirection);
            }
        }
    }

    private List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> getExamSetStructure(RequestedEnrollmentDirection direction)
    {
        StudentCategory studentCategory = direction.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff() ? direction.getStudentCategory() : UniDaoFacade.getCoreDao().getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT);
        ExamSet examSet = ExamSetUtil.getExamSet(direction.getEnrollmentDirection(), studentCategory);
        Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(getSession(), direction.getEnrollmentDirection().getEnrollmentCampaign());
        List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structureList = new ArrayList<>();
        for (ExamSetItem item : examSet.getSetItemList())
        {
            // если выбрали бюджет на форме, а вступительное испытание не поддерживаем бюджет, то игнорируем его
            CompensationType compensationType = direction.getCompensationType();
            if (compensationType != null && compensationType.isBudget() && !item.isBudget())
                continue;
            // если выбрали контракт на форме, а вступительное испытание не поддерживаем контракт, то игнорируем его
            if (compensationType != null && !compensationType.isBudget() && !item.isContract())
                continue;
            structureList.add(PairKey.create(item.getKind(), ExamSetUtil.getDisciplines(item, group2directions)));
        }
        return structureList;
    }

    @Override
    public void prepareEntranceDisciplineDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest()));
        builder.addOrder("e", RequestedEnrollmentDirection.P_PRIORITY);

        List<RequestedEnrollmentDirection> list = builder.getResultList(getSession());

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getEntranceDisciplineDataSourceMap().get(model.getEntrantRequest());
        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);
    }

    @Override
    public void prepareEntrantExamPassDisciplineDataSource(EntrantExamList entrantExamList, Model model)
    {
        DynamicListDataSource<ExamPassDiscipline> dataSource = model.getCurrentExamListWrapper().getExamPassDisciplineDataSource();
        Set<ExamPassDiscipline> examPassSet = model.getExamPassMap().get(entrantExamList);

        List<ExamPassDiscipline> list = new ArrayList<>(examPassSet == null ? new HashSet<ExamPassDiscipline>() : examPassSet);
        Collections.sort(list, new Comparator<ExamPassDiscipline>()
                {
            @Override
            public int compare(ExamPassDiscipline o1, ExamPassDiscipline o2)
            {
                int result = o1.getEnrollmentCampaignDiscipline().getTitle().compareTo(o2.getEnrollmentCampaignDiscipline().getTitle());
                if (result == 0)
                    result = o1.getSubjectPassForm().getCode().compareTo(o2.getSubjectPassForm().getCode());
                return result;
            }
                });
        dataSource.setCountRow(list.size());
        UniBaseUtils.createPage(dataSource, list);
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        ExamPassDiscipline examPassDiscipline = getNotNull(ExamPassDiscipline.class, (Long) component.getListenerParameter());
        UniecDAOFacade.getEntrantDAO().deleteExamPassDiscipline(examPassDiscipline);
    }
}
