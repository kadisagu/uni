/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util;

import com.google.common.collect.Iterables;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.EntranceDisciplineGen;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 10.07.2009
 */
public class EntrantDataUtil
{
    public enum DetailLevel
    {
        EXAM_PASS,                      // дополнительно считаются дисциплины для сдачи
        EXAM_PASS_WITH_MARK,            // дополнительно считаются оценки по этим дисциплинам для сдачи
        EXAM_PASS_WITH_MARK_STATISTICS  // дополнительно считается статистика по формам сдачи для этих оценок
    }

    private final Set<RequestedEnrollmentDirection> _directionSet = new HashSet<>();
    private final Map<EnrollmentDirection, Set<RequestedEnrollmentDirection>> _directionMap = new HashMap<>();
    private final Map<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> _direction2chosen = new HashMap<>();
    private final Map<ChosenEntranceDiscipline, Set<ExamPassDiscipline>> _chosen2exampass = new HashMap<>();
    private final Map<ChosenEntranceDiscipline, MarkStatistic> _chosen2markstats = new HashMap<>();
    private final Map<ExamPassDiscipline, PairKey<ExamPassMark, ExamPassMarkAppeal>> _examPass2mark = new HashMap<>();
    private Map<Long, Double> averageEduInstMarkMap = new HashMap<>();
    private Map<Long, Integer> _individualProgressMarkByRequest;//сумма баллов за индивидуальные достижения по заявлениям

    private DetailLevel _detailLevel;

    public EntrantDataUtil(Session session, EnrollmentCampaign enrollmentCampaign, MQBuilder builder)
    {
        init(session, enrollmentCampaign, builder, DetailLevel.EXAM_PASS_WITH_MARK);
    }

    public EntrantDataUtil(Session session, EnrollmentCampaign enrollmentCampaign, MQBuilder builder, DetailLevel detailLevel)
    {
        init(session, enrollmentCampaign, builder, detailLevel);
    }

    // MAIN DATA

    public Set<RequestedEnrollmentDirection> getDirectionSet()
    {
        return _directionSet;
    }

    public Set<RequestedEnrollmentDirection> getDirectionSet(EnrollmentDirection direction)
    {
        Set<RequestedEnrollmentDirection> set = _directionMap.get(direction);
        return set == null ? Collections.<RequestedEnrollmentDirection>emptySet() : set;
    }

    public Set<ChosenEntranceDiscipline> getChosenEntranceDisciplineSet(RequestedEnrollmentDirection direction)
    {
        Set<ChosenEntranceDiscipline> set = _direction2chosen.get(direction);
        return set == null ? Collections.<ChosenEntranceDiscipline>emptySet() : set;
    }

    public Set<ExamPassDiscipline> getExamPassDisciplineSet(ChosenEntranceDiscipline chosen)
    {
        if (_detailLevel.ordinal() < DetailLevel.EXAM_PASS.ordinal())
            throw new RuntimeException("Method getExamPassDisciplineSet is not accessed with detailLevel = " + _detailLevel.name());
        Set<ExamPassDiscipline> set = _chosen2exampass.get(chosen);
        return set == null ? Collections.<ExamPassDiscipline>emptySet() : set;
    }

    public PairKey<ExamPassMark, ExamPassMarkAppeal> getExamPassMark(ExamPassDiscipline examPass)
    {
        if (_detailLevel.ordinal() < DetailLevel.EXAM_PASS_WITH_MARK.ordinal())
            throw new RuntimeException("Method getExamPassMark is not accessed with detailLevel = " + _detailLevel.name());
        return _examPass2mark.get(examPass);
    }

    public MarkStatistic getMarkStatistic(ChosenEntranceDiscipline chosen)
    {
        if (_detailLevel.ordinal() < DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS.ordinal())
            throw new RuntimeException("Method getMarkStatistic is not accessed with detailLevel = " + _detailLevel.name());
        return _chosen2markstats.get(chosen);
    }

    // UTIL METHODS

    /**
     * Создает мап оценок по выбранным вступ. исп.
     *
     * @param direction выбранное направление приема
     * @return мап оценок discipline -> mark
     * mark = оценка, если финальная оценка по выбранному вступ. исп. не null
     * mark = -1, если финальная оценка по выбранному вступ. исп. равна null
     */
    public Map<Discipline2RealizationWayRelation, Double> getMarkMap(RequestedEnrollmentDirection direction)
    {
        Map<Discipline2RealizationWayRelation, Double> markMap = new HashMap<>();
        for (ChosenEntranceDiscipline chosen : getChosenEntranceDisciplineSet(direction))
            markMap.put(chosen.getEnrollmentCampaignDiscipline(), chosen.getFinalMark() == null ? -1.0 : chosen.getFinalMark());
        return markMap;
    }

    /**
     * Вычисляет финальную оценку по выбранному направлению приема
     * Если мы не строили статистику по direction, то и нет смысла считать финальную оценку
     *
     * @param direction выбранное направление приема, которое есть среди _directionSet
     * @return финальная оценка по направлению приема = сумме баллов финальных оценок актуальных выбранных вступ. исп.
     */
    public double getFinalMark(RequestedEnrollmentDirection direction)
    {
        double finalMark = 0.0;
        for (ChosenEntranceDiscipline chosen : getChosenEntranceDisciplineSet(direction))
            if (chosen.getFinalMark() != null)
                finalMark += chosen.getFinalMark();
        return finalMark;
    }

    /**
     * Вычисляет финальную оценку по дисциплине для сдачи
     * Если мы не строили статистику по examPass, то и нет смысла считать финальную оценку
     *
     * @param examPass дисциплина для сдачи, которая есть в выбранных направлениях среди _directionSet
     * @return финальная оценка по дисциплине для сдачи = апелляция если есть, сама оценка если есть, или -1 если оценки еще нет
     */
    public double getFinalMark(ExamPassDiscipline examPass)
    {
        PairKey<ExamPassMark, ExamPassMarkAppeal> pair = getExamPassMark(examPass);
        if (pair == null) return -1.0; // no mark yet
        return pair.getSecond() != null ? pair.getSecond().getMark() : pair.getFirst().getMark();
    }

    // PRIVATE DATA

    private void init(Session session, EnrollmentCampaign enrollmentCampaign, MQBuilder builder, DetailLevel detailLevel)
    {
        if (detailLevel == null)
            throw new RuntimeException("DetailLevel cannot be null!");

        // загрузим средние баллы аттестата тут

        averageEduInstMarkMap = getAverageEduInstMark(enrollmentCampaign, session);

        //получаем сумму баллов за индивидуальные достижения
        _individualProgressMarkByRequest = getIndividualProgressMarkByRequestMap(enrollmentCampaign, session);

        // дальше пошел Васин код, его не трогаем

        _detailLevel = detailLevel;

        // LOAD PARAMETERS
        boolean formingForEntrant = UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT.equals(enrollmentCampaign.getEntrantExamListsFormingFeature().getCode());
        boolean compensationTypeDiff = enrollmentCampaign.isEnrollmentPerCompTypeDiff();
        boolean examSetDiff = enrollmentCampaign.isExamSetDiff();

        // FETCH DATA
        _directionSet.addAll(builder.<RequestedEnrollmentDirection>getResultList(session));

        for (RequestedEnrollmentDirection direction : _directionSet)
        {
            Set<RequestedEnrollmentDirection> set = _directionMap.get(direction.getEnrollmentDirection());
            if (set == null)
                _directionMap.put(direction.getEnrollmentDirection(), set = new HashSet<>());
            set.add(direction);
        }

        // FILL TEMP MAPS
        MQBuilder chosenBuilder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "__chosen");
        chosenBuilder.addJoinFetch("__chosen", ChosenEntranceDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE, "__chosenDiscipline");
        if (detailLevel == DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS)
            chosenBuilder.addJoinFetch("__chosen", ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().s(), "__entrantRequest");
        chosenBuilder.add(MQExpression.in("__chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, builder));

        for (ChosenEntranceDiscipline chosenEntranceDiscipline : chosenBuilder.<ChosenEntranceDiscipline>getResultList(session))
        {
            Set<ChosenEntranceDiscipline> set = _direction2chosen.get(chosenEntranceDiscipline.getChosenEnrollmentDirection());
            if (set == null)
                _direction2chosen.put(chosenEntranceDiscipline.getChosenEnrollmentDirection(), set = new HashSet<>());
            set.add(chosenEntranceDiscipline);
        }

        MQBuilder examPassBuilder = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "__examPass");
        examPassBuilder.addDomain("__chosen", ChosenEntranceDiscipline.ENTITY_CLASS);
        examPassBuilder.add(MQExpression.eqProperty("__examPass", ExamPassDiscipline.L_ENTRANT_EXAM_LIST + "." + EntrantExamList.L_ENTRANT + ".id", "__chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + (formingForEntrant ? "." + EntrantRequest.L_ENTRANT : "") + ".id"));
        examPassBuilder.add(MQExpression.eqProperty("__examPass", ExamPassDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE, "__chosen", ChosenEntranceDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE));
        examPassBuilder.add(MQExpression.in("__chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, builder));
        examPassBuilder.addSelect("__chosen");

        for (Object[] row : examPassBuilder.<Object[]>getResultList(session))
        {
            ExamPassDiscipline examPassDiscipline = (ExamPassDiscipline) row[0];
            ChosenEntranceDiscipline chosenEntranceDiscipline = (ChosenEntranceDiscipline) row[1];

            Set<ExamPassDiscipline> set = _chosen2exampass.get(chosenEntranceDiscipline);
            if (set == null)
                _chosen2exampass.put(chosenEntranceDiscipline, set = new HashSet<>());
            set.add(examPassDiscipline);
        }

        examPassBuilder.getSelectAliasList().clear();
        examPassBuilder.addSelect("__examPass");

        MQBuilder excludePassFormBuilder = new MQBuilder(ExcludeSubjectPassForm.ENTITY_CLASS, "__exclude");
        excludePassFormBuilder.add(MQExpression.in("__exclude", ExcludeSubjectPassForm.chosenEntranceDiscipline().chosenEnrollmentDirection().s(), builder));
        Set<MultiKey> excludePassFormSet = new HashSet<>();
        for (ExcludeSubjectPassForm item : excludePassFormBuilder.<ExcludeSubjectPassForm>getResultList(session))
            excludePassFormSet.add(new MultiKey(item.getChosenEntranceDiscipline(), item.getSubjectPassForm()));

        // REMOVE NON ACTUAL DATA

        Map<Long, Set<Discipline2RealizationWayRelation>> group2discipline = EntrantUtil.getDisciplinesGroupMap(session, enrollmentCampaign);

        Set<MultiKey> realizationFormSet = UniecDAOFacade.getEntrantDAO().getRealizationFormSet(enrollmentCampaign);

        Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();
        for (RequestedEnrollmentDirection direction : _directionSet)
        {
            for (ExamSet examSet : examSetList)
            {
                // если есть отличия в наборах по категориям и категория текущего набора не равна категории
                // выбранного направления, то пропускаем такой набор
                if (examSetDiff && !examSet.getStudentCategory().equals(direction.getStudentCategory()))
                    continue;

                // если текущий набор не содержит выбранное направление приема, то пропускаем такой набор
                if (!examSet.getList().contains(direction.getEnrollmentDirection()))
                    continue;

                // подходящий набор найден (по определению он только один)

                // из набора получаем все SetDiscipline с раскрытием дисциплин по выбору
                Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
                for (ExamSetItem item : examSet.getSetItemList())
                {
                    // вступительное испытание не подджерживает вид затрат выбранного направления приема
                    if (direction.getCompensationType().isBudget() && !item.isBudget())
                        continue;

                    // вступительное испытание не подджерживает вид затрат выбранного направления приема
                    if (!direction.getCompensationType().isBudget() && !item.isContract())
                        continue;

                    // hint: если нет отличий в приеме на бюджет и по договору, то item.budget == item.contract == true

                    disciplineSet.addAll(ExamSetUtil.getDisciplines(item, group2discipline));
                }

                // исключим из direction2chosen и chosen2exampass все неактуальные дисциплины
                Set<ChosenEntranceDiscipline> chosenSet = _direction2chosen.get(direction);
                if (chosenSet != null)
                {
                    for (Iterator<ChosenEntranceDiscipline> iter = chosenSet.iterator(); iter.hasNext(); )
                    {
                        ChosenEntranceDiscipline chosen = iter.next();
                        if (!disciplineSet.contains(chosen.getEnrollmentCampaignDiscipline()))
                        {
                            iter.remove();
                            _chosen2exampass.remove(chosen);
                        }
                    }

                    // исключим из chosen2exampass и examPass2mark все неактуальные дисципилын для сдачи
                    for (ChosenEntranceDiscipline chosen : chosenSet)
                    {
                        Set<ExamPassDiscipline> examPassSet = _chosen2exampass.get(chosen);
                        if (examPassSet != null)
                        {
                            for (Iterator<ExamPassDiscipline> iter = examPassSet.iterator(); iter.hasNext(); )
                            {
                                ExamPassDiscipline examPass = iter.next();
                                Discipline2RealizationWayRelation discipline = examPass.getEnrollmentCampaignDiscipline();
                                SubjectPassForm subjectPassForm = examPass.getSubjectPassForm();

                                MultiKey realizationFormSetKey = compensationTypeDiff ? new MultiKey(discipline, subjectPassForm, direction.getCompensationType()) : new MultiKey(discipline, subjectPassForm);

                                if (!realizationFormSet.contains(realizationFormSetKey))
                                {
                                    iter.remove();
                                }
                                else
                                {
                                    MultiKey excludeMultiKey = new MultiKey(chosen, subjectPassForm);
                                    if (excludePassFormSet.contains(excludeMultiKey))
                                    {
                                        iter.remove();
                                    }
                                }
                            }
                        }
                    }
                }

                break;
            }
        }

        // если уровень детализации "дисциплины для сдачи", то заканчиваем расчеты
        if (detailLevel == DetailLevel.EXAM_PASS) return;

        MQBuilder passMarkBuilder = new MQBuilder(ExamPassMark.ENTITY_CLASS, "__passMark");
        passMarkBuilder.add(MQExpression.in("__passMark", ExamPassMark.L_EXAM_PASS_DISCIPLINE, examPassBuilder));

        MQBuilder markAppealBuilder = new MQBuilder(ExamPassMarkAppeal.ENTITY_CLASS, "__markAppeal");
        markAppealBuilder.add(MQExpression.in("__markAppeal", ExamPassMarkAppeal.L_EXAM_PASS_MARK, passMarkBuilder));
        Map<ExamPassMark, ExamPassMarkAppeal> passMark2markAppeal = new HashMap<>();
        for (ExamPassMarkAppeal markAppeal : markAppealBuilder.<ExamPassMarkAppeal>getResultList(session))
            passMark2markAppeal.put(markAppeal.getExamPassMark(), markAppeal);

        for (ExamPassMark passMark : passMarkBuilder.<ExamPassMark>getResultList(session))
            _examPass2mark.put(passMark.getExamPassDiscipline(), PairKey.create(passMark, passMark2markAppeal.get(passMark)));

        // если уровень детализации "оценки по дисциплинам для сдачи", то заканчиваем расчеты
        if (detailLevel == DetailLevel.EXAM_PASS_WITH_MARK) return;

        // загружаем оценки по всем сертификатам ЕГЭ
        Map<Entrant, Map<StateExamSubject, StateExamSubjectMark[]>> stateExamSubjectMarkMap = getStateExamSubjectMarkMap(session, builder);

        // загружаем дипломы олимпиад
        Map<Entrant, OlympiadStatistic> olympiadStatisticMap = getOlympiadStatisticMap(session, builder);

        // загружаем шкалы перевода
        Map<Discipline2RealizationWayRelation, ConversionScale> conversionScaleMap = UniecDAOFacade.getEntrantDAO().getConversionScales(enrollmentCampaign);

        // загружаем форму сдачи для ЕГЭ
        SubjectPassForm stateExamPassForm = UniDaoFacade.getCoreDao().getByNaturalId(new SubjectPassForm.NaturalId(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM));

        // загружаем внутренние формы сдачи
        List<SubjectPassForm> internalPassFormList = UniDaoFacade.getCoreDao().getList(SubjectPassForm.class, SubjectPassForm.P_INTERNAL, Boolean.TRUE, SubjectPassForm.P_CODE);

        for (Set<ChosenEntranceDiscipline> chosenSet : _direction2chosen.values())
            for (ChosenEntranceDiscipline chosen : chosenSet)
            {
                final RequestedEnrollmentDirection direction = chosen.getChosenEnrollmentDirection();
                final CompensationType compensationType = direction.getCompensationType();
                final Entrant entrant = direction.getEntrantRequest().getEntrant();
                final Discipline2RealizationWayRelation discipline = chosen.getEnrollmentCampaignDiscipline();

                Map<StateExamSubject, StateExamSubjectMark[]> markMap = stateExamSubjectMarkMap.get(chosen.getChosenEnrollmentDirection().getEntrantRequest().getEntrant());

                StateExamSubjectMark[] stateExamMarkArray;
                StateExamSubjectMark stateExamMark = null;         // оценка из зачтенного сертификата ЕГЭ
                Double scaledStateExamMark = null;                 // оценка из зачтенного сертификата ЕГЭ приведенная к шкале вуза
                StateExamSubjectMark stateExamMarkPossible = null; // оценка из всех сертификатов ЕГЭ
                Double scaledStateExamMarkPossible = null;         // оценка из всех сертификатов ЕГЭ приведенная к шкале вуза
                Set<OlympiadDiploma> diplomaSet = null;
                LinkedHashMap<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> internalMarkMap = new LinkedHashMap<>();

                // вычисляем возможный балл по ЕГЭ
                MultiKey realizationFormSetKey = compensationTypeDiff ? new MultiKey(discipline, stateExamPassForm, compensationType) : new MultiKey(discipline, stateExamPassForm);

                // если дисциплину можно сдавать по форме сдачи ЕГЭ и эта форма сдачи не исключена для этой выбранной дисциплины для сдачи, то можно посчитать бал по ЕГЭ
                if (realizationFormSet.contains(realizationFormSetKey) && !excludePassFormSet.contains(new MultiKey(chosen, stateExamPassForm)))
                {
                    // берется шкала перевода
                    ConversionScale conversionScale = conversionScaleMap.get(discipline);
                    // если существует шкала перевода
                    // если существуют оценки по сертификатам ЕГЭ
                    // если дисциплине соответствует предмет ЕГЭ
                    // если существует оценка по этому предмету ЕГЭ
                    if (conversionScale != null && markMap != null && (stateExamMarkArray = markMap.get(conversionScale.getSubject())) != null)
                    {
                        // тогда можно посчитать ЕГЭ по шкале вуза

                        // если нет разницы в приеме на бюджет/контракт, тогда надо передать true, иначе надо передать true при бюджете и false для контракта
                        boolean budget = !compensationTypeDiff || compensationType.isBudget();

                        stateExamMarkPossible = stateExamMarkArray[EGE_MARK_FROM_ALL_CERTIFICATES];
                        scaledStateExamMarkPossible = conversionScale.getInternalMark(stateExamMarkPossible.getMark(), budget);

                        stateExamMark = stateExamMarkArray[EGE_MARK_FROM_ACCEPTED_CERTIFICATES];
                        if (stateExamMark != null)
                            scaledStateExamMark = conversionScale.getInternalMark(stateExamMark.getMark(), budget);
                    }
                }

                // вычисляем зачтение по олимпиаде
                OlympiadStatistic olympiadStatistic = olympiadStatisticMap.get(entrant);
                if (olympiadStatistic != null)
                    diplomaSet = olympiadStatistic.getDiplomaSet(chosen);

                // все остальное
                Map<PairKey<Discipline2RealizationWayRelation, String>, ExamPassDiscipline> key2exampass = new HashMap<>();
                for (ExamPassDiscipline examPass : getExamPassDisciplineSet(chosen))
                    key2exampass.put(PairKey.create(examPass.getEnrollmentCampaignDiscipline(), examPass.getSubjectPassForm().getCode()), examPass);

                for (SubjectPassForm passForm : internalPassFormList)
                {
                    // берем дисциплину для сдачи
                    ExamPassDiscipline examPass = key2exampass.get(PairKey.create(discipline, passForm.getCode()));

                    // если есть дисциплина для сдачи,
                    if (examPass != null)
                        internalMarkMap.put(passForm, _examPass2mark.get(examPass));
                }

                _chosen2markstats.put(chosen, new MarkStatistic(chosen, stateExamMark, scaledStateExamMark, stateExamMarkPossible, scaledStateExamMarkPossible, diplomaSet, internalMarkMap));
            }
    }

    // ADDITIONAL UTIL METHODS

    /**
     * Вычисляет мн-во льгот для каждой персоны абитуриента, у которых есть выбранные направления приема
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return person -> мн-во льгот
     */
    public static Map<Person, Set<PersonBenefit>> getPersonBenefitMap(Session session, MQBuilder builder)
    {
        Map<Person, Set<PersonBenefit>> person2benefitsMap = new HashMap<>();

        MQBuilder eduBuilder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "__benefit");
        eduBuilder.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        eduBuilder.add(MQExpression.eqProperty("__benefit", PersonBenefit.L_PERSON, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON));
        eduBuilder.add(MQExpression.in("__requestedDirection", "id", builder));
        for (PersonBenefit personBenefit : eduBuilder.<PersonBenefit>getResultList(session))
        {
            Set<PersonBenefit> set = person2benefitsMap.get(personBenefit.getPerson());
            if (set == null)
                person2benefitsMap.put(personBenefit.getPerson(), set = new HashSet<>());
            set.add(personBenefit);
        }

        return person2benefitsMap;
    }

    /**
     * Вычисляет мн-во льгот для каждой персоны абитуриента, у которых есть выбранные направления приема
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return person -> мн-во льгот
     */
    public static Map<Person, Set<Benefit>> getBenefitMap(Session session, MQBuilder builder)
    {
        Map<Person, Set<Benefit>> person2benefitsMap = new HashMap<>();

        MQBuilder b = new MQBuilder(PersonBenefit.ENTITY_CLASS, "__benefit");
        b.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        b.add(MQExpression.eqProperty("__benefit", PersonBenefit.L_PERSON, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON));
        b.add(MQExpression.in("__requestedDirection", "id", builder));
        for (PersonBenefit personBenefit : b.<PersonBenefit>getResultList(session))
        {
            Set<Benefit> set = person2benefitsMap.get(personBenefit.getPerson());
            if (set == null)
                person2benefitsMap.put(personBenefit.getPerson(), set = new HashSet<>());
            set.add(personBenefit.getBenefit());
        }

        return person2benefitsMap;
    }

    /**
     * Вычисляет мн-во подготовительных курсов для каждого абитуриента, у которого есть выбранные направления приема
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return абитуриент -> мн-во подготовительных курсов
     */
    public static Map<Entrant, Set<EntrantAccessCourse>> getAccessCourseMap(Session session, MQBuilder builder)
    {
        Map<Entrant, Set<EntrantAccessCourse>> entrant2accessCourseMap = new HashMap<>();

        MQBuilder eduBuilder = new MQBuilder(EntrantAccessCourse.ENTITY_CLASS, "__accessCourse");
        eduBuilder.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        eduBuilder.add(MQExpression.eqProperty("__accessCourse", EntrantAccessCourse.L_ENTRANT, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
        eduBuilder.add(MQExpression.in("__requestedDirection", "id", builder));
        for (EntrantAccessCourse entrantAccessCourse : eduBuilder.<EntrantAccessCourse>getResultList(session))
        {
            Set<EntrantAccessCourse> set = entrant2accessCourseMap.get(entrantAccessCourse.getEntrant());
            if (set == null)
                entrant2accessCourseMap.put(entrantAccessCourse.getEntrant(), set = new HashSet<>());
            set.add(entrantAccessCourse);
        }

        return entrant2accessCourseMap;
    }

    /**
     * Вычисляет мн-во рекомендаций к зачислению для каждого абитуриента, у которого есть выбранные направления приема
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return абитуриент -> мн-во рекомендаций к зачислению
     */
    public static Map<Entrant, Set<EntrantEnrolmentRecommendation>> getEnrollmentRecomendationMap(Session session, MQBuilder builder)
    {
        Map<Entrant, Set<EntrantEnrolmentRecommendation>> entrant2enrollmentRecommendationMap = new HashMap<>();

        MQBuilder eduBuilder = new MQBuilder(EntrantEnrolmentRecommendation.ENTITY_CLASS, "__enrollmentRecommendation");
        eduBuilder.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        eduBuilder.add(MQExpression.eqProperty("__enrollmentRecommendation", EntrantEnrolmentRecommendation.L_ENTRANT, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
        eduBuilder.add(MQExpression.in("__requestedDirection", "id", builder));
        for (EntrantEnrolmentRecommendation entrantEnrolmentRecommendation : eduBuilder.<EntrantEnrolmentRecommendation>getResultList(session))
        {
            Set<EntrantEnrolmentRecommendation> set = entrant2enrollmentRecommendationMap.get(entrantEnrolmentRecommendation.getEntrant());
            if (set == null)
            {
                entrant2enrollmentRecommendationMap.put(entrantEnrolmentRecommendation.getEntrant(), set = new HashSet<>());
            }
            set.add(entrantEnrolmentRecommendation);
        }

        return entrant2enrollmentRecommendationMap;
    }

    /**
     * Вычисляет мн-во дипломов олимпиад для каждого абитуриента, к которого есть выбранные направления приема
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return абитуриент -> мн-во димпломов олимпиад
     */
    public static Map<Entrant, Set<OlympiadDiploma>> getOlympiadDiplomaMap(Session session, MQBuilder builder)
    {
        Map<Entrant, Set<OlympiadDiploma>> entrant2olympiadDiplomaMap = new HashMap<>();

        MQBuilder dipBuilder = new MQBuilder(OlympiadDiploma.ENTITY_CLASS, "__olympiadDiploma");
        dipBuilder.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        dipBuilder.add(MQExpression.eqProperty("__olympiadDiploma", OlympiadDiploma.L_ENTRANT, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
        dipBuilder.add(MQExpression.in("__requestedDirection", "id", builder));
        for (OlympiadDiploma olympiadDiploma : dipBuilder.<OlympiadDiploma>getResultList(session))
        {
            Set<OlympiadDiploma> set = entrant2olympiadDiplomaMap.get(olympiadDiploma.getEntrant());
            if (set == null)
            {
                entrant2olympiadDiplomaMap.put(olympiadDiploma.getEntrant(), set = new HashSet<>());
            }
            set.add(olympiadDiploma);
        }

        return entrant2olympiadDiplomaMap;
    }

    /**
     * Вычисляет мн-во профильных знаний для каждого выбранного направления приема
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return выбранное направление приема -> мн-во профильных знаний
     */
    public static Map<RequestedEnrollmentDirection, Set<RequestedProfileKnowledge>> getProfileKnowledgeMap(Session session, MQBuilder builder)
    {
        Map<RequestedEnrollmentDirection, Set<RequestedProfileKnowledge>> entrant2profileKnowledgeMap = new HashMap<>();

        MQBuilder eduBuilder = new MQBuilder(RequestedProfileKnowledge.ENTITY_CLASS, "__knowledge");
        eduBuilder.add(MQExpression.in("__knowledge", RequestedProfileKnowledge.L_REQUESTED_ENROLLMENT_DIRECTION, builder));
        for (RequestedProfileKnowledge profileKnowledge : eduBuilder.<RequestedProfileKnowledge>getResultList(session))
        {
            Set<RequestedProfileKnowledge> set = entrant2profileKnowledgeMap.get(profileKnowledge.getRequestedEnrollmentDirection());
            if (set == null)
                entrant2profileKnowledgeMap.put(profileKnowledge.getRequestedEnrollmentDirection(), set = new HashSet<>());
            set.add(profileKnowledge);
        }

        return entrant2profileKnowledgeMap;
    }

    /**
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return выбранное направление приема -> результат собеседования
     */
    private static Map<RequestedEnrollmentDirection, InterviewResult> getInterviewResultMap(Session session, MQBuilder builder)
    {
        Map<RequestedEnrollmentDirection, InterviewResult> result = new HashMap<>();

        MQBuilder eduBuilder = new MQBuilder(InterviewResult.ENTITY_CLASS, "__interviewResult");
        eduBuilder.add(MQExpression.in("__interviewResult", InterviewResult.requestedEnrollmentDirection().s(), builder));
        for (InterviewResult interviewResult : eduBuilder.<InterviewResult>getResultList(session))
        {
            result.put(interviewResult.getRequestedEnrollmentDirection(), interviewResult);
        }

        return result;
    }

    private static final int EGE_MARK_FROM_ALL_CERTIFICATES = 0;
    private static final int EGE_MARK_FROM_ACCEPTED_CERTIFICATES = 1;

    /**
     * Вычисляет мап оценок по всем сертификатам егэ
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return абитуриент -> предмет егэ -> [максимальная оценка среди всех сертификатов, максимальная оценка среди только зачтенных сертификатов]
     */
    private static Map<Entrant, Map<StateExamSubject, StateExamSubjectMark[]>> getStateExamSubjectMarkMap(Session session, MQBuilder builder)
    {
        Map<Entrant, Map<StateExamSubject, StateExamSubjectMark[]>> entrant2subjectMark = new HashMap<>();

        MQBuilder markBuilder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "__stateMark");
        markBuilder.addJoinFetch("__stateMark", StateExamSubjectMark.L_CERTIFICATE, "__certificate");
        markBuilder.addJoinFetch("__certificate", EntrantStateExamCertificate.L_ENTRANT, "__entrant");
        markBuilder.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        markBuilder.add(MQExpression.eqProperty("__stateMark", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
        markBuilder.add(MQExpression.in("__requestedDirection", "id", builder));
        for (StateExamSubjectMark mark : markBuilder.<StateExamSubjectMark>getResultList(session))
        {
            EntrantStateExamCertificate certificate = mark.getCertificate();

            Map<StateExamSubject, StateExamSubjectMark[]> map = entrant2subjectMark.get(certificate.getEntrant());
            if (map == null)
                entrant2subjectMark.put(mark.getCertificate().getEntrant(), map = new HashMap<>());

            StateExamSubject key = mark.getSubject();
            StateExamSubjectMark[] maxMark = map.get(key);

            if (maxMark == null)
            {
                maxMark = certificate.isAccepted() ? new StateExamSubjectMark[]{mark, mark} : new StateExamSubjectMark[]{mark, null};
                map.put(key, maxMark);
            }
            else
            {
                // оценка по ЕГЭ
                int value = mark.getMark();

                // сравниваем с оценкой из всех сертификатов
                if (value > maxMark[EGE_MARK_FROM_ALL_CERTIFICATES].getMark())
                    maxMark[EGE_MARK_FROM_ALL_CERTIFICATES] = mark;

                // сравниваем с оценкой из зачтенных сертификатов
                if (certificate.isAccepted() && (maxMark[EGE_MARK_FROM_ACCEPTED_CERTIFICATES] == null || value > maxMark[EGE_MARK_FROM_ACCEPTED_CERTIFICATES].getMark()))
                    maxMark[EGE_MARK_FROM_ACCEPTED_CERTIFICATES] = mark;
            }
        }
        return entrant2subjectMark;
    }

    /**
     * Вычисляет мап дисциплин покрывающихся дипломами олимпиад
     *
     * @param session сессия
     * @param builder билдер, возвращающий выбранные направления приема
     * @return абитуриент -> дисциплина -> мн-во дипломов, по которым зачитывается эти дисциплина
     */
    private static Map<Entrant, OlympiadStatistic> getOlympiadStatisticMap(Session session, MQBuilder builder)
    {
        Map<Entrant, Map<OlympiadStatistic.Key, Set<OlympiadDiploma>>> map = new HashMap<>();

        MQBuilder diplomaBuilder = new MQBuilder(Discipline2OlympiadDiplomaRelation.ENTITY_CLASS, "__diplomaRelation");
        diplomaBuilder.addJoinFetch("__diplomaRelation", Discipline2OlympiadDiplomaRelation.L_DIPLOMA, "__diploma");
        diplomaBuilder.addJoinFetch("__diploma", OlympiadDiploma.L_ENTRANT, "__entrant");
        diplomaBuilder.addDomain("__requestedDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        diplomaBuilder.add(MQExpression.eqProperty("__diplomaRelation", Discipline2OlympiadDiplomaRelation.L_DIPLOMA + "." + OlympiadDiploma.L_ENTRANT, "__requestedDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT));
        diplomaBuilder.add(MQExpression.in("__requestedDirection", "id", builder));

        for (Discipline2OlympiadDiplomaRelation relation : diplomaBuilder.<Discipline2OlympiadDiplomaRelation>getResultList(session))
        {
            Entrant entrant = relation.getDiploma().getEntrant();
            Map<OlympiadStatistic.Key, Set<OlympiadDiploma>> diplomaMap = map.get(entrant);
            if (diplomaMap == null)
                map.put(entrant, diplomaMap = new HashMap<>());

            OlympiadStatistic.Key key = new OlympiadStatistic.Key(relation.getEnrollmentDirection(), relation.getCompensationType(), relation.getDiscipline());
            Set<OlympiadDiploma> list = diplomaMap.get(key);
            if (list == null)
                diplomaMap.put(key, list = new HashSet<>());
            list.add(relation.getDiploma());
        }

        Map<Entrant, OlympiadStatistic> olympiadStatisticMap = new HashMap<>();
        for (Map.Entry<Entrant, Map<OlympiadStatistic.Key, Set<OlympiadDiploma>>> entry : map.entrySet())
            olympiadStatisticMap.put(entry.getKey(), new OlympiadStatistic(entry.getValue()));
        return olympiadStatisticMap;
    }

    /**
     * Метод для шустрой загрузки средних баллов аттестата всех абитуриентов
     *
     * @param campaign ПК
     * @param session  сессия
     * @return id абитуриента - средний балл аттестата по основному док. о полученном образовании персоны
     */
    public static Map<Long, Double> getAverageEduInstMark(EnrollmentCampaign campaign, Session session)
    {
        Map<Long, Double> map = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "e")
                .where(eq(property(Entrant.enrollmentCampaign().fromAlias("e")), value(campaign)))
                .column(property(Entrant.id().fromAlias("e")))
                .column(property(Entrant.person().personEduInstitution().mark3().fromAlias("e")))
                .column(property(Entrant.person().personEduInstitution().mark4().fromAlias("e")))
                .column(property(Entrant.person().personEduInstitution().mark5().fromAlias("e")));

        for (Object[] row : dql.createStatement(session).<Object[]>list())
        {
            Long entrantId = (Long) row[0];
            Integer mark3 = (Integer) row[1];
            if (null == mark3) mark3 = 0;
            Integer mark4 = (Integer) row[2];
            if (null == mark4) mark4 = 0;
            Integer mark5 = (Integer) row[3];
            if (null == mark5) mark5 = 0;
            int count = mark3 + mark4 + mark5;
            Double average = count == 0 ? null : (3 * mark3 + 4 * mark4 + 5 * mark5) / (double) count;
            map.put(entrantId, average);
        }

        return map;
    }

    /**
     * Средний балл аттестата по основному
     *
     * @param entrantId id абитуриента
     * @return средний балл аттестата по основному док. о полученном образовании персоны
     */
    public Double getAverageEduInstMark(Long entrantId)
    {
        return averageEduInstMarkMap.get(entrantId);
    }

    /**
     * Сумма баллов за индивидуальные достижения абитуриента по заявлениям
     *
     * @param campaign - приемная кампания
     * @param session  сессия
     * @return id заявления - сумма баллов индивидуальных достижений
     */
    public static Map<Long, Integer> getIndividualProgressMarkByRequestMap(EnrollmentCampaign campaign, Session session)
    {
        if (!campaign.isUseIndividualProgressInAmountMark())
            return new HashMap<>();

        String eipAlias = "eip";
        String reqAlias = "req";
        List<Object[]> raws = new DQLSelectBuilder()
                .fromEntity(EntrantIndividualProgress.class, eipAlias)
                .column(DQLFunctions.sum(property(eipAlias, EntrantIndividualProgress.mark())))
                .where(eq(property(reqAlias, EntrantIndividualProgress.entrant().enrollmentCampaign()), value(campaign)))

                .joinEntity(eipAlias, DQLJoinType.inner, EntrantRequest.class, reqAlias,
                            and(eq(property(eipAlias, EntrantIndividualProgress.entrant()),
                                   property(reqAlias, EntrantRequest.entrant())),
                                IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(property(reqAlias), property(eipAlias))
                            ))
                .column(property(reqAlias, EntrantRequest.id()))
                .group(property(reqAlias, EntrantRequest.id()))
                .createStatement(session).list();

        return raws.stream().collect(Collectors.toMap(raw -> (Long) raw[1], raw -> ((Number) raw[0]).intValue()));
    }

    /**
     * Сумма баллов за индивидуальные достижения абитуриента по заявлениям
     *
     * @param requestIds список заявлений
     * @param session    сессия
     * @return id заявления - сумма баллов индивидуальных достижений
     */
    public static Map<Long, Integer> getIndividualProgressMarkByRequestMap(List<Long> requestIds, Session session)
    {
        if (requestIds.isEmpty())
            return new HashMap<>();

        String eipAlias = "eip";
        String reqAlias = "req";
        List<Object[]> raws = new DQLSelectBuilder()
                .fromEntity(EntrantIndividualProgress.class, eipAlias)
                .column(DQLFunctions.sum(property(eipAlias, EntrantIndividualProgress.mark())))

                .joinEntity(eipAlias, DQLJoinType.inner, EntrantRequest.class, reqAlias,
                            and(eq(property(eipAlias, EntrantIndividualProgress.entrant()),
                                   property(reqAlias, EntrantRequest.entrant())),
                                IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(requestIds, property(eipAlias))
                            ))
                .column(property(reqAlias, EntrantRequest.id()))
                .group(property(reqAlias, EntrantRequest.id()))
                .createStatement(session).list();

        return raws.stream().collect(Collectors.toMap(raw -> (Long) raw[1], raw -> ((Number) raw[0]).intValue()));
    }

    /**
     * @param requestId id Заявления абитуриента
     * @return сумма баллов за индивидуальные достижения учитывающиеся в этом заявлении
     */
    public int getIndividualProgressMarkByRequest(Long requestId)
    {
        return _individualProgressMarkByRequest.get(requestId) == null ? 0 : _individualProgressMarkByRequest.get(requestId);
    }

    public static Map<EnrollmentDirection, Map<StudentCategory, List<EntranceDiscipline>>> getDisciplinesByStudentCategoryByDirection(
            Session session, EnrollmentCampaign enrollmentCampaign)
    {
        return new DQLSelectBuilder()
                .fromEntity(EntranceDiscipline.class, "ed")
                .column(property("ed"))
                .where(eq(property("ed", EntranceDiscipline.enrollmentDirection().enrollmentCampaign()), value(enrollmentCampaign)))

                .createStatement(session).<EntranceDiscipline>list().stream()
                .collect(Collectors.groupingBy(
                        EntranceDisciplineGen::getEnrollmentDirection,
                        Collectors.mapping(
                                dis -> dis,
                                Collectors.groupingBy(EntranceDisciplineGen::getStudentCategory,
                                                      Collectors.mapping(dis -> dis, Collectors.toList())))));
    }

    public static Map<EnrollmentDirection, Map<StudentCategory, List<EntranceDiscipline>>> getDisciplinesByStudentCategoryByDirection(
            Session session, List<RequestedEnrollmentDirection> requestedDirectionList)
    {
        List<EntranceDiscipline> disciplines = new ArrayList<>();
        for (final List partOfRed : Iterables.partition(requestedDirectionList, DQL.MAX_VALUES_ROW_NUMBER))
        {
            disciplines.addAll(
                    new DQLSelectBuilder()
                            .fromEntity(EntranceDiscipline.class, "ed")
                            .column(property("ed"))
                            .where(existsByExpr(RequestedEnrollmentDirection.class, "red",
                                                and(in(property("red"), partOfRed),
                                                    eq(property("ed", EntranceDiscipline.enrollmentDirection()),
                                                       property("red", RequestedEnrollmentDirection.enrollmentDirection())))))

                            .createStatement(session).<EntranceDiscipline>list()
            );
        }

        return disciplines.stream()
                .collect(Collectors.groupingBy(
                        EntranceDisciplineGen::getEnrollmentDirection,
                        Collectors.mapping(
                                dis -> dis,
                                Collectors.groupingBy(EntranceDisciplineGen::getStudentCategory,
                                                      Collectors.mapping(dis -> dis, Collectors.toList())))));
    }

    /**
     * Абитуриенты с преймущественными правами из списка выбранных направлений
     *
     * @return map(Entrant Id, list[EntrantEnrolmentRecommendation) )
     */
    public static Map<Long, List<EntrantEnrolmentRecommendation>> getEntrantEnrolmentRecommendations(Session session, DQLSelectBuilder requestedDirectionsBuilder)
    {
        return new DQLSelectBuilder()
                .fromEntity(EntrantEnrolmentRecommendation.class, "e")
                .column(property("e"))
                .where(existsByExpr(RequestedEnrollmentDirection.class, "req",
                                    and(eq(property("req", RequestedEnrollmentDirection.entrantRequest().entrant()),
                                           property("e", EntrantEnrolmentRecommendation.entrant())),
                                        in(property("req"), requestedDirectionsBuilder.buildQuery()))
                ))
                .createStatement(session).<EntrantEnrolmentRecommendation>list().stream()

                .collect(Collectors.groupingBy(rec -> rec.getEntrant().getId(), Collectors.mapping(rec -> rec, Collectors.toList())));
    }
}
