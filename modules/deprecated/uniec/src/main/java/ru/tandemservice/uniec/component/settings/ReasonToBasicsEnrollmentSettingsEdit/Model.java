/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ReasonToBasicsEnrollmentSettingsEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;

/**
 * @author alikhanov
 * @since 05.08.2010
 */
@Input({@Bind (key = "enrollmentOrderReasonId")})
public class Model
{
    private Long _enrollmentOrderReasonId;
    private EnrollmentOrderReason _enrollmentOrderReason;

    private IMultiSelectModel _enrollmentOrderBasicsSelectModel;
    private List<EnrollmentOrderBasic> _selectedBasicsList;

    public List<EnrollmentOrderBasic> getSelectedBasicsList()
    {
        return _selectedBasicsList;
    }

    public void setSelectedBasicsList(List<EnrollmentOrderBasic> selectedBasicsList)
    {
        _selectedBasicsList = selectedBasicsList;
    }

    public Long getEnrollmentOrderReasonId()
    {
        return _enrollmentOrderReasonId;
    }

    public void setEnrollmentOrderReasonId(Long enrollmentOrderReasonId)
    {
        _enrollmentOrderReasonId = enrollmentOrderReasonId;
    }

    public EnrollmentOrderReason getEnrollmentOrderReason()
    {
        return _enrollmentOrderReason;
    }

    public void setEnrollmentOrderReason(EnrollmentOrderReason enrollmentOrderReason)
    {
        _enrollmentOrderReason = enrollmentOrderReason;
    }

    public IMultiSelectModel getEnrollmentOrderBasicsSelectModel()
    {
        return _enrollmentOrderBasicsSelectModel;
    }

    public void setEnrollmentOrderBasicsSelectModel(IMultiSelectModel enrollmentOrderBasicsSelectModel)
    {
        _enrollmentOrderBasicsSelectModel = enrollmentOrderBasicsSelectModel;
    }
}
