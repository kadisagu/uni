/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.CompetitionGroupList;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author vip_delete
 * @since 27.05.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private DynamicListDataSource<EnrollmentDirection> _dataSource;
    private List<Long> _distributedIds = Collections.emptyList();

    // IEnrollmentCampaignSelectModel
    
    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public DynamicListDataSource<EnrollmentDirection> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentDirection> dataSource)
    {
        _dataSource = dataSource;
    }

    public List<Long> getDistributedIds()
    {
        return _distributedIds;
    }

    public void setDistributedIds(List<Long> distributedIds)
    {
        _distributedIds = distributedIds;
    }
}
