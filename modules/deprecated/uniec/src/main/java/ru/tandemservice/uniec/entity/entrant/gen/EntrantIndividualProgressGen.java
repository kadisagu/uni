package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальное достижение абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantIndividualProgressGen extends EntityBase
 implements INaturalIdentifiable<EntrantIndividualProgressGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress";
    public static final String ENTITY_NAME = "entrantIndividualProgress";
    public static final int VERSION_HASH = 286554894;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_INDIVIDUAL_PROGRESS_TYPE = "individualProgressType";
    public static final String P_MARK = "mark";

    private Entrant _entrant;     // Абитуриент
    private IndividualProgress _individualProgressType;     // Достижение
    private int _mark;     // Балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Достижение. Свойство не может быть null.
     */
    @NotNull
    public IndividualProgress getIndividualProgressType()
    {
        return _individualProgressType;
    }

    /**
     * @param individualProgressType Достижение. Свойство не может быть null.
     */
    public void setIndividualProgressType(IndividualProgress individualProgressType)
    {
        dirty(_individualProgressType, individualProgressType);
        _individualProgressType = individualProgressType;
    }

    /**
     * @return Балл. Свойство не может быть null.
     */
    @NotNull
    public int getMark()
    {
        return _mark;
    }

    /**
     * @param mark Балл. Свойство не может быть null.
     */
    public void setMark(int mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantIndividualProgressGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EntrantIndividualProgress)another).getEntrant());
                setIndividualProgressType(((EntrantIndividualProgress)another).getIndividualProgressType());
            }
            setMark(((EntrantIndividualProgress)another).getMark());
        }
    }

    public INaturalId<EntrantIndividualProgressGen> getNaturalId()
    {
        return new NaturalId(getEntrant(), getIndividualProgressType());
    }

    public static class NaturalId extends NaturalIdBase<EntrantIndividualProgressGen>
    {
        private static final String PROXY_NAME = "EntrantIndividualProgressNaturalProxy";

        private Long _entrant;
        private Long _individualProgressType;

        public NaturalId()
        {}

        public NaturalId(Entrant entrant, IndividualProgress individualProgressType)
        {
            _entrant = ((IEntity) entrant).getId();
            _individualProgressType = ((IEntity) individualProgressType).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public Long getIndividualProgressType()
        {
            return _individualProgressType;
        }

        public void setIndividualProgressType(Long individualProgressType)
        {
            _individualProgressType = individualProgressType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EntrantIndividualProgressGen.NaturalId) ) return false;

            EntrantIndividualProgressGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            if( !equals(getIndividualProgressType(), that.getIndividualProgressType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            result = hashCode(result, getIndividualProgressType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            sb.append("/");
            sb.append(getIndividualProgressType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantIndividualProgressGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantIndividualProgress.class;
        }

        public T newInstance()
        {
            return (T) new EntrantIndividualProgress();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "individualProgressType":
                    return obj.getIndividualProgressType();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "individualProgressType":
                    obj.setIndividualProgressType((IndividualProgress) value);
                    return;
                case "mark":
                    obj.setMark((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "individualProgressType":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "individualProgressType":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "individualProgressType":
                    return IndividualProgress.class;
                case "mark":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantIndividualProgress> _dslPath = new Path<EntrantIndividualProgress>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantIndividualProgress");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Достижение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress#getIndividualProgressType()
     */
    public static IndividualProgress.Path<IndividualProgress> individualProgressType()
    {
        return _dslPath.individualProgressType();
    }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress#getMark()
     */
    public static PropertyPath<Integer> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends EntrantIndividualProgress> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private IndividualProgress.Path<IndividualProgress> _individualProgressType;
        private PropertyPath<Integer> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Достижение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress#getIndividualProgressType()
     */
        public IndividualProgress.Path<IndividualProgress> individualProgressType()
        {
            if(_individualProgressType == null )
                _individualProgressType = new IndividualProgress.Path<IndividualProgress>(L_INDIVIDUAL_PROGRESS_TYPE, this);
            return _individualProgressType;
        }

    /**
     * @return Балл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress#getMark()
     */
        public PropertyPath<Integer> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<Integer>(EntrantIndividualProgressGen.P_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return EntrantIndividualProgress.class;
        }

        public String getEntityName()
        {
            return "entrantIndividualProgress";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
