/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDirProfList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport.EnrollmentDirProfReportListDSHandler;
import ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDirProfPub.EcReportEnrollmentDirProfPub;
import ru.tandemservice.uniec.entity.report.EnrollmentDirProfReport;

/**
 * @author Alexander Shaburov
 * @since 02.07.12
 */
@Configuration
public class EcReportEnrollmentDirProfList extends BusinessComponentManager
{
    // dataSource
    public static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(REPORT_LIST_DS, reportListColumnExtPoint(), EcReportManager.instance().enrollmentDirProfReportListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportListColumnExtPoint()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("ico").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("formingDate", EnrollmentDirProfReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(EcReportEnrollmentDirProfPub.class).order())
                .addColumn(textColumn("fromTo", EnrollmentDirProfReportListDSHandler.FROM_TO_PROPERTY))
                .addColumn(textColumn("enrollmentCampaignStep", EnrollmentDirProfReport.enrollmentCampaignStepText()))
                .addColumn(textColumn("qualification", EnrollmentDirProfReport.qualificationText()))
                .addColumn(textColumn("developForm", EnrollmentDirProfReport.developFormText()))
                .addColumn(textColumn("developCondition", EnrollmentDirProfReport.developConditionText()))
                .addColumn(textColumn("compensationType", EnrollmentDirProfReport.compensationTypeText()))
                .addColumn(textColumn("studentCategory", EnrollmentDirProfReport.studentCategoryText()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintReport").permissionKey("printUniecStorableReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("reportListDSHandler.delete.alert", EnrollmentDirProfReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME)).permissionKey("deleteUniecStorableReport"))
                .create();
    }
}
