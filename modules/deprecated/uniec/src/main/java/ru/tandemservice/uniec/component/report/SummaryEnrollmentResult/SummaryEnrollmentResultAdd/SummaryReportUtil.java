/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.*;

/**
 * Утилита, которая получает список строк для отчетов сводок.
 * Умеет хитро группировать направления приема и с помощью ISummaryReportRowFactory наполнять строки отчета данными
 * В итоговых строках не происходит подсчет статистики
 *
 * @author vip_delete
 * @since 23.04.2009
 */
class SummaryReportUtil
{
    /**
     * смысл: все направления приема хитро группируем
     * вначале по ур. обр ОУ, потом разделяем по филиалам-представительствам и другим типам, внутри филиала-представительства по подразделению
     * <p/>
     * Map<уровень образования ОУ, Object[]{Map<подразделение, мн-во выбранных направлений приема>, мн-во выбранных направлений приема} >
     *
     * @param enrollmentDirectionList список направлений приема
     * @param factory                 фабрика создания элемента строки отчета из направления приема
     * @param compensationType        вид возмещения затрат для подсчета плана приема
     * @return структурированные данные отчета: укрупненная группа -> список ее подстрок
     */
    public static Map<EducationLevels, Set<SummaryReportRow>> getReportRowList(List<EnrollmentDirection> enrollmentDirectionList, ISummaryReportRowFactory factory, CompensationType compensationType)
    {
        Map<EducationLevels, Set<SummaryReportRow>> resultMap = new TreeMap<>(new Comparator<EducationLevels>()
        {
            @Override
            public int compare(EducationLevels o1, EducationLevels o2)
            {
                return o1.getDisplayableTitle().compareTo(o2.getDisplayableTitle());
            }
        });

        Map<MultiKey, SummaryReportRow> id2row = new HashMap<>();

        for (EnrollmentDirection direction : enrollmentDirectionList)
        {
            int plan = getPlan(direction, compensationType);
            List<ISummaryRowItem> rowItems = factory.createSummaryReportItem(direction);
            if (rowItems.isEmpty() && plan == 0) continue;

            // определяем укрупненную группу
            EducationLevels parentLevel = direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            while (parentLevel.getParentLevel() != null) parentLevel = parentLevel.getParentLevel();

            // список строк в нее входящих
            Set<SummaryReportRow> resultList = resultMap.get(parentLevel);
            if (resultList == null)
                resultMap.put(parentLevel, resultList = new TreeSet<>(ITitled.TITLED_COMPARATOR));

            // определяем id строки отчета
            EducationLevelsHighSchool highSchool = direction.getEducationOrgUnit().getEducationLevelHighSchool();

            OrgUnit formativeOrgUnit = direction.getEducationOrgUnit().getFormativeOrgUnit();
            String orgUnitType = formativeOrgUnit.getOrgUnitType().getCode();

            MultiKey key;
            boolean branchOrRepresensation = ((OrgUnitTypeCodes.BRANCH.equals(orgUnitType) || OrgUnitTypeCodes.REPRESENTATION.equals(orgUnitType)));
            if (branchOrRepresensation)
                key = new MultiKey(highSchool.getId(), formativeOrgUnit.getId());
            else
                key = new MultiKey(new Object[]{highSchool.getId()});

            SummaryReportRow row = id2row.get(key);
            if (row == null)
            {
                String title = branchOrRepresensation ? highSchool.getPrintTitle() + " (" + formativeOrgUnit.getShortTitle() + ")" : highSchool.getPrintTitle();
                resultList.add(row = new SummaryReportRow(title, title + " (" + highSchool.getOrgUnit().getShortTitle() + ")"));
                id2row.put(key, row);
            }

            row.increasePlan(plan);
            row.appendRowItems(rowItems);
        }

        return resultMap;
    }

    private static int getPlan(EnrollmentDirection enrollmentDirection, CompensationType compensationType)
    {
        Integer mp = enrollmentDirection.getMinisterialPlan();
        Integer tp = enrollmentDirection.getTargetAdmissionPlanBudget();
        Integer cp = enrollmentDirection.getContractPlan();

        int mplan = mp == null ? 0 : mp;
        int tplan = tp == null ? 0 : tp;
        int cplan = cp == null ? 0 : cp;

        if (compensationType == null) return mplan + tplan + cplan;
        if (compensationType.getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET)) return mplan;
        if (compensationType.getCode().equals(UniDefines.COMPENSATION_TYPE_CONTRACT)) return cplan;
        return 0;
    }
}
