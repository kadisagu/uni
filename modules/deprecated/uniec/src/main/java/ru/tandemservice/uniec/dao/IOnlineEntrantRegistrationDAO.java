/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.dao;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniec.ws.EntrantData;
import ru.tandemservice.uniec.ws.UserDirectionData;

import java.util.List;

/**
 * @author agolubenko
 * @since Apr 29, 2010
 */
public interface IOnlineEntrantRegistrationDAO extends IUniBaseDao
{
    /**
     * Создает или обновляет данные онлайн абитуриента из последней приемной кампании
     *
     * @param userId      идентификатор пользователя в онлайн-регистрации
     * @param entrantData данные онлайн абитуриента
     * @return онлайн абитуриент
     */
    OnlineEntrant createOrUpdateOnlineEntrant(long userId, EntrantData entrantData);

    /**
     * Получает онлайн-абитуриента из последней приемной кампании
     *
     * @param userId идентификатор пользователя в онлайн-регистрации
     * @return онлайн абитуриент
     */
    OnlineEntrant getOnlineEntrant(long userId);

    /**
     * Получает данные (DTO) онлайн абитуриента
     *
     * @param userId идентификатор пользователя в онлайн-регистрации
     * @return данные онлайн абитуриента
     */
    EntrantData getEntrantData(long userId);

    /**
     * Получает оценки из сертификата ЕГЭ
     *
     * @param onlineEntrant онлайн абитуриента
     * @return оценки из сертификата ЕГЭ
     */
    List<OnlineEntrantCertificateMark> getCertificateMarks(OnlineEntrant onlineEntrant);

    /**
     * Получает источники информации о вузе
     *
     * @param onlineEntrant онлайн абитуриента
     * @return источники информации о вузе
     */
    List<OnlineEntrantInfoAboutUniversity> getUniversityInfoSources(OnlineEntrant onlineEntrant);

    /**
     * Получает ВНП онлайн абитуриента
     *
     * @param onlineEntrant онлайн абитуриент
     * @return ВНП онлайн абитуриента
     */
    List<OnlineRequestedEnrollmentDirection> getRequestedEnrollmentDirections(OnlineEntrant onlineEntrant);

    /**
     * Получает данные (DTO) о ВНП онлайн абитуриента
     *
     * @param userId идентификатор пользователя в онлайн-регистрации
     * @return данные (DTO) о ВНП онлайн абитуриента
     */
    List<UserDirectionData> getUserDirections(long userId);

    /**
     * Обновляет данные о ВНП онлайн абитуриента
     *
     * @param userId         идентификатор пользователя в онлайн-регистрации
     * @param userDirections данные о ВНП
     */
    void updateUserDirections(long userId, UserDirectionData[] userDirections);

    /**
     * Получает печатную форму заявления онлайн абитуриента
     *
     * @param userId идентификатор пользователя в онлайн-регистрации
     * @return печатная форма заявления онлайн абитуриента (rtf-файл)
     */
    byte[] getPrintOnlineEntrantRequest(long userId);

    /**
     * Сохраняет или заменяет архив с копиями документов
     *
     * @param userId             идентификатор пользователя в онлайн-регистрации
     * @param zipArchive         zip-архив с копиями документов
     * @param documentCopiesName имя zip-архива
     */
    void saveDocumentCopies(long userId, byte[] zipArchive, String documentCopiesName);

    /**
     * Удаляет архив с копиями документов
     *
     * @param userId идентификатор пользователя в онлайн-регистрации
     */
    void deleteDocumentCopies(long userId);
}
