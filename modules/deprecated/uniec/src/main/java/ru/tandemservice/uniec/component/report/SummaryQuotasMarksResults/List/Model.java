/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.List;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.SummaryQuotasMarksResultsReport;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private DynamicListDataSource<SummaryQuotasMarksResultsReport> _dataSource;
    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    public DynamicListDataSource<SummaryQuotasMarksResultsReport> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<SummaryQuotasMarksResultsReport> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }
}
