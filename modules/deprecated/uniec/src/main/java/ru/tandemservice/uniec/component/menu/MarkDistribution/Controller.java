/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.MarkDistribution;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantExamList;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;

/**
 * @author vip_delete
 * @since 31.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            model.getDataSource().refresh();
            return;
        }

        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("ФИО абитуриента", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT, Entrant.L_PERSON, Person.P_FULLFIO}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пол", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.L_SEX, Sex.P_SHORT_TITLE}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ экзам. листа", new String[]{EntrantDisciplineMarkWrapper.P_EXAM_PASS_DISCIPLINE, ExamPassDiscipline.L_ENTRANT_EXAM_LIST, EntrantExamList.P_NUMBER}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.P_BIRTH_DATE}, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", new String[]{EntrantDisciplineMarkWrapper.P_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.P_FULL_NUMBER}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.COLUMN_EXAM_PASS_DISCIPLINE_MARK, "Оценка").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.COLUMN_EXAM_PASS_DISCIPLINE_PAPER_CODE, "Шифр работы").setWidth(1).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn(Model.COLUMN_EXAM_PASS_DISCIPLINE_EXAMINERS, "ФИО экзаменаторов").setWidth(1).setOrderable(false).setClickable(false));
        model.setDataSource(dataSource);
    }

    public void onClickShow(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        model.setDiscipline(null);
        getDao().prepare(model);
        onClickShow(component);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDiscipline() == null) return; // не печатается

        // подготавливаем данные для скрипта печати
        List<EntrantDisciplineMarkWrapper> wrapperList = model.getDataSource().getEntityList();
        List<Map<String, Object>> table = new ArrayList<Map<String, Object>>();
        for (EntrantDisciplineMarkWrapper wrapper : wrapperList)
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("entrantId", wrapper.getEntrant().getId());
            map.put("disciplineId", wrapper.getExamPassDiscipline().getId());
            map.put("paperCode", wrapper.getPaperCode());
            map.put("mark", wrapper.getMark());
            map.put("paperCode", wrapper.getPaperCode());
            map.put("examiners", wrapper.getExaminers());

            Set<Long> directionIds = new HashSet<Long>();
            for (RequestedEnrollmentDirection direction : wrapper.getRequestedEnrollmentDirections())
                directionIds.add(direction.getId());
            map.put("directionIds", directionIds);
            table.add(map);
        }

        // запускаем скрипт
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENROLLMENT_EXAM_LIST),
                "hsId", model.getEducationLevelsHighSchool() == null ? null : model.getEducationLevelsHighSchool().getId(),
                "developFormId", model.getDevelopForm() == null ? null : model.getDevelopForm().getId(),
                "examGroupTitle", model.getGroup() == null ? null : model.getGroup().getTitle(),
                "disciplineTitle", model.getDiscipline().getTitle(),
                "examPassDate", model.getPassDate() == null ? null : model.getPassDate().getWrapped(),
                "table", table
        );
    }

    public List<Map<String, Object>> getEntrantList (Model model) {
        // подготавливаем данные для скрипта печати
        List<EntrantDisciplineMarkWrapper> wrapperList = model.getDataSource().getEntityList();
        List<Map<String, Object>> table = new ArrayList<Map<String, Object>>();
        for (EntrantDisciplineMarkWrapper wrapper : wrapperList)
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("entrantId", wrapper.getEntrant().getId());
            map.put("disciplineId", wrapper.getExamPassDiscipline().getId());
            map.put("mark", wrapper.getMark());

            Set<Long> directionIds = new HashSet<Long>();
            for (RequestedEnrollmentDirection direction : wrapper.getRequestedEnrollmentDirections())
                directionIds.add(direction.getId());
            map.put("directionIds", directionIds);
            table.add(map);
        }
        return table;
    }
//
//    public void onClickMassPrint(IBusinessComponent component)
//    {
//        Integer id = getDao().prepareMassPrintReport(getModel(component));
//        if (id == null) return;
//        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id)));
//    }
}
