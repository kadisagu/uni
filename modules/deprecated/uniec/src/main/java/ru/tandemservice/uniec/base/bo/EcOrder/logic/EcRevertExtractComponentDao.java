/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniec.component.enrollmentextract.ICommonEnrollmentExtractDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 05.08.2013
 */
public class EcRevertExtractComponentDao implements IExtractComponentDao<EnrollmentRevertExtract>
{
    @Override
    public void doCommit(EnrollmentRevertExtract extract, Map parameters)
    {
        ICommonEnrollmentExtractDao.instance.get().doCommitRevertExtract(extract);
    }

    @Override
    public void doRollback(EnrollmentRevertExtract extract, Map parameters)
    {
        throw new ApplicationException("Отменить действие приказа во изменение нельзя.");
    }
}