package ru.tandemservice.uniec.component.report.EnrolledDealList.Add;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.component.reports.YesNoUIObject;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.report.EnrolledDealListReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setDetailList(YesNoUIObject.createYesNoList());
        model.setDetail(model.getDetailList().get(1));

        model.setEntrantEnrollmentOrderTypeListModel(createOrderTypeListModel(model));
        model.setEnrollmentOrderListModel(createOrderListModel(model));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setParallelList(YesNoUIObject.createYesNoList());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EnrolledDealListReport report = model.getReport();
        clearReport(report);

        if (model.isEntrantEnrollmentOrderTypeActive())
            report.setEntrantEnrollmentOrderType(UniStringUtils.join(model.getEntrantEnrollmentOrderTypeList(), EntrantEnrollmentOrderType.P_TITLE, ";"));
        if (model.isEnrollmentOrderActive())
            report.setEnrollmentOrder(getOrdersTitle(model.getEnrollmentOrderList()));
        if (model.isStudentCategoryActive())
            report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        if (model.isQualificationActive())
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
        if (model.isDevelopFormActive())
            report.setDevelopForm(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.P_TITLE, "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.P_TITLE, "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());
        report.setIncludeForeignPerson(model.isIncludeForeignPerson());
        report.setFormingDate(new Date());
        DatabaseFile databaseFile = new EnrolledDealListReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    public static ISelectModel createOrderTypeListModel(final Model model)
    {
        return new FullCheckSelectModel(EntrantEnrollmentOrderType.P_TITLE)
        {
            @Override
            public ListResult<EntrantEnrollmentOrderType> findValues(String filter)
            {
                return new ListResult<>(EcOrderManager.instance().dao().getOrderTypeList(model.getEnrollmentCampaign()));
            }
        };
    }

    public static ISelectModel createOrderListModel(final Model model) {

        return new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentOrder.class, "order");
                c.add(Restrictions.eq("order." + EnrollmentOrder.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                if (model.getEntrantEnrollmentOrderTypeList() != null && model.getEntrantEnrollmentOrderTypeList().size() > 0)
                    c.add(Restrictions.in("order." + EnrollmentOrder.L_TYPE, model.getEntrantEnrollmentOrderTypeList()));
                c.setProjection(Projections.distinct(Projections.property("order." + EnrollmentOrder.P_ID)));

                List<Long> orderIds = c.list();

                if (orderIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(EnrollmentOrder.class);
                listCriteria.add(Restrictions.in(EnrollmentOrder.P_ID, orderIds));
                return new ListResult<>(listCriteria.list());
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EnrollmentOrder order = (EnrollmentOrder) value;
                StringBuilder orderTitle = new StringBuilder();
                orderTitle.append("Приказ о зачислении № ")
                        .append(order.getNumber()).append(" от ")
                        .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
                return orderTitle.toString();
            }
        };
    }

    private String getOrdersTitle(List<EnrollmentOrder> ordersList) {
        StringBuilder ordersTitle = new StringBuilder();
        for (EnrollmentOrder order : ordersList) {
            if (ordersTitle.length() > 0)
                ordersTitle.append("; ");
            ordersTitle.append("Приказ о зачислении № ")
                    .append(order.getNumber()).append(" от ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        }
        return ordersTitle.toString();
    }

    private void clearReport(EnrolledDealListReport report)
    {
        report.setDevelopPeriod(null);
        report.setDevelopCondition(null);
        report.setDevelopForm(null);
        report.setDevelopTech(null);
        report.setEnrollmentOrder(null);
        report.setEntrantEnrollmentOrderType(null);
        report.setEntrantEnrollmentOrderType(null);
        report.setFormativeOrgUnit(null);
        report.setQualificationTitle(null);
        report.setStudentCategoryTitle(null);
        report.setTerritorialOrgUnit(null);
        report.setIncludeForeignPerson(null);
    }
}
