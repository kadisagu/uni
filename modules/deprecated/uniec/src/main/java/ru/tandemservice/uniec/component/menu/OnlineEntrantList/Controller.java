/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.OnlineEntrantList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author agolubenko
 * @since May 11, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    protected final String ENTRANT_REG_DATE_COLUMN = "entrantRegDate";
    protected final String PRINT_REQUEST_COLUMN = "printRequest";
    protected final String DELETE_COLUMN = "delete";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    protected void addPrintColumn(DynamicListDataSource<OnlineEntrant> dataSource, Integer position)
    {
        // код вынесен для возможности кастомизаций в проекте
        IndicatorColumn column = new IndicatorColumn(PRINT_REQUEST_COLUMN, "Печать", null, "onClickPrint", (String) null);
        column.setOrderable(false);
        column.setImageHeader(false);
        column.defaultIndicator(new IndicatorColumn.Item("printer", "Печать"));
        column.setDisableSecondSubmit(false);
        if (position == null)
            dataSource.addColumn(column);
        else
            dataSource.addColumn(column, position);
    }

    public void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);

        DynamicListDataSource<OnlineEntrant> dataSource = UniBaseUtils.createDataSource(component, getDao());

        SimpleColumn personalNumber = new SimpleColumn("Личный код", "personalCode")
        {
            @Override
            public String getContent(IEntity entity)
            {
                return ((OnlineEntrant) ((ViewWrapper) entity).getEntity()).getPersonalCode();
            }
        };
        personalNumber.setClickable(false);
        dataSource.addColumn(personalNumber);

        dataSource.addColumn(new SimpleColumn("Онлайн-абитуриент", OnlineEntrant.P_FULL_FIO));
        dataSource.addColumn(new SimpleColumn("Пол", OnlineEntrant.sex().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", OnlineEntrant.P_FULL_PASSPORT_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", OnlineEntrant.birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата регистрации", OnlineEntrant.registrationDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn(ENTRANT_REG_DATE_COLUMN, "Дата рассмотрения", OnlineEntrant.entrant().registrationDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));

        if (Debug.isDisplay()) {
            // в жизни очень сложно поределить что это за абитуриент, тем более на нашей деперсонализированной базе, когда фамилии не совпадают вообще
            dataSource.addColumn(new PublisherLinkColumn("Личный номер абитуриента", Entrant.personalNumber(), OnlineEntrant.entrant()));
        }

        if (model.getEnrollmentCampaign() != null && model.getEnrollmentCampaign().isAllowUploadDocuments())
        {
            IndicatorColumn column = new IndicatorColumn("printDocument", "Документы", OnlineEntrant.P_DOCUMENT_COPIES_NAME, "onClickPrintDocumentCopies", (String) null);
            column.setOrderable(false);
            column.setImageHeader(false);
            column.defaultIndicator(new IndicatorColumn.Item("printer", "Документы"));
            column.indicator(null, null);
            column.setDisableSecondSubmit(false);
            dataSource.addColumn(column);
        }

        if (model.getEnrollmentCampaign() != null && model.getEnrollmentCampaign().isAllowPrintOnlineRequest())
        {
            addPrintColumn(dataSource, null);
        }

        ActionColumn deleteColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить онлайн-абитуриента «{0}»?", OnlineEntrant.P_FULL_TITLE);
        deleteColumn.setName(DELETE_COLUMN);
        dataSource.addColumn(deleteColumn);

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        prepareDataSource(component);
        getModel(component).getDataSource().refresh();
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        EnrollmentCampaign savedEnrollmentCampaign = model.getEnrollmentCampaign();
        model.getSettings().clear();
        if (savedEnrollmentCampaign != null) // не сбрасывать фильтр кампании
            model.setEnrollmentCampaign(savedEnrollmentCampaign);
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickPrint(IBusinessComponent component) throws Exception
    {
        OnlineEntrant onlineEntrant = DataAccessServices.dao().getNotNull(component.<Long>getListenerParameter());
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ONLINE_ENTRANT_REQUEST),
            IScriptExecutor.OBJECT_VARIABLE, onlineEntrant.getId(),
            IScriptExecutor.PRINT_OBJECT_VARIABLE, onlineEntrant,
            IScriptExecutor.PRINT_FILE_NAME, "Заявление онлайн-абитуриента.rtf"
        );
    }

    public void onClickPrintDocumentCopies(IBusinessComponent component) throws Exception
    {
        OnlineEntrant onlineEntrant = DataAccessServices.dao().getNotNull((Long) component.getListenerParameter());
        if (null == onlineEntrant) { return; }

        final DatabaseFile documentCopies = onlineEntrant.getDocumentCopies();
        if (null == documentCopies) { return; }

        final String filename = StringUtils.trimToNull(onlineEntrant.getDocumentCopiesName());
        if (null == filename) { return; }

        final byte[] content = documentCopies.getContent();
        if (null == content) { return; }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(null).fileName(filename).document(content), true);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
    }
}
