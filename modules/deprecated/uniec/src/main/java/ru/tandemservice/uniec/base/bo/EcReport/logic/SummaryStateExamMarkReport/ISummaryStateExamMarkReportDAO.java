/**
 *$Id:$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.SummaryStateExamMarkReport;

import jxl.write.WriteException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.report.SummaryStateExamMarkReport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 30.07.12
 */
public interface ISummaryStateExamMarkReportDAO
{
    /**
     * Подготавливает модель формы добавления отчета.<p/>
     * Заполняет модели селектов и значения по умолчанию.
     * @param model модель
     * @return подготовленную модель
     */
    <M extends SummaryStateExamMarkReportModel> M prepareModel(M model);

    /**
     * Листенер на изменеие поля Приемная компания.
     * Сетит даты заявления.
     * @param model модель
     * @return модель
     */
    <M extends SummaryStateExamMarkReportModel> M onChangeEnrollmentCampaign(M model);

    /**
     * Создает отчет и заполняет его поля.
     * @param model      модель
     * @param reportFile файл отчета
     * @return Отчет "Сводка о средних баллах абитуриентов, поступающих по ЕГЭ"
     */
    <M extends SummaryStateExamMarkReportModel> SummaryStateExamMarkReport createReport(M model, DatabaseFile reportFile);

    /**
     * Создает печатную форму отчета "Сводка о средних баллах абитуриентов, поступающих по ЕГЭ".
     * @param model модель
     * @return Файл отчета хранимый в базе данных
     */
    <M extends SummaryStateExamMarkReportModel> DatabaseFile createPrintReportFile(M model) throws IOException, WriteException;
}
