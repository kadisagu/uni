package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вступительное испытание для направления для приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntranceDisciplineGen extends VersionedEntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntranceDiscipline";
    public static final String ENTITY_NAME = "entranceDiscipline";
    public static final int VERSION_HASH = -850487070;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String L_DISCIPLINE = "discipline";
    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String L_KIND = "kind";
    public static final String L_TYPE = "type";
    public static final String P_BUDGET = "budget";
    public static final String P_CONTRACT = "contract";
    public static final String P_PASS_MARK = "passMark";
    public static final String P_PASS_MARK_CONTRACT = "passMarkContract";
    public static final String P_PRIORITY = "priority";

    private int _version; 
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private SetDiscipline _discipline;     // Элемент набора вступительных испытаний
    private StudentCategory _studentCategory;     // Категория обучаемого
    private EntranceDisciplineKind _kind;     // Виды вступительных испытаний
    private EntranceDisciplineType _type;     // Тип вступительного испытания
    private boolean _budget;     // Бюджет
    private boolean _contract;     // Контракт
    private Double _passMark;     // Зачетный балл
    private Double _passMarkContract;     // Зачетный балл (для небюджетников)
    private Integer _priority;     // Приоритет ВИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public SetDiscipline getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Элемент набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(SetDiscipline discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Виды вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public EntranceDisciplineKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Виды вступительных испытаний. Свойство не может быть null.
     */
    public void setKind(EntranceDisciplineKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return Тип вступительного испытания. Свойство не может быть null.
     */
    @NotNull
    public EntranceDisciplineType getType()
    {
        return _type;
    }

    /**
     * @param type Тип вступительного испытания. Свойство не может быть null.
     */
    public void setType(EntranceDisciplineType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Бюджет. Свойство не может быть null.
     */
    @NotNull
    public boolean isBudget()
    {
        return _budget;
    }

    /**
     * @param budget Бюджет. Свойство не может быть null.
     */
    public void setBudget(boolean budget)
    {
        dirty(_budget, budget);
        _budget = budget;
    }

    /**
     * @return Контракт. Свойство не может быть null.
     */
    @NotNull
    public boolean isContract()
    {
        return _contract;
    }

    /**
     * @param contract Контракт. Свойство не может быть null.
     */
    public void setContract(boolean contract)
    {
        dirty(_contract, contract);
        _contract = contract;
    }

    /**
     * @return Зачетный балл.
     */
    public Double getPassMark()
    {
        return _passMark;
    }

    /**
     * @param passMark Зачетный балл.
     */
    public void setPassMark(Double passMark)
    {
        dirty(_passMark, passMark);
        _passMark = passMark;
    }

    /**
     * @return Зачетный балл (для небюджетников).
     */
    public Double getPassMarkContract()
    {
        return _passMarkContract;
    }

    /**
     * @param passMarkContract Зачетный балл (для небюджетников).
     */
    public void setPassMarkContract(Double passMarkContract)
    {
        dirty(_passMarkContract, passMarkContract);
        _passMarkContract = passMarkContract;
    }

    /**
     * @return Приоритет ВИ.
     */
    public Integer getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет ВИ.
     */
    public void setPriority(Integer priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntranceDisciplineGen)
        {
            setVersion(((EntranceDiscipline)another).getVersion());
            setEnrollmentDirection(((EntranceDiscipline)another).getEnrollmentDirection());
            setDiscipline(((EntranceDiscipline)another).getDiscipline());
            setStudentCategory(((EntranceDiscipline)another).getStudentCategory());
            setKind(((EntranceDiscipline)another).getKind());
            setType(((EntranceDiscipline)another).getType());
            setBudget(((EntranceDiscipline)another).isBudget());
            setContract(((EntranceDiscipline)another).isContract());
            setPassMark(((EntranceDiscipline)another).getPassMark());
            setPassMarkContract(((EntranceDiscipline)another).getPassMarkContract());
            setPriority(((EntranceDiscipline)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntranceDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntranceDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EntranceDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "discipline":
                    return obj.getDiscipline();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "kind":
                    return obj.getKind();
                case "type":
                    return obj.getType();
                case "budget":
                    return obj.isBudget();
                case "contract":
                    return obj.isContract();
                case "passMark":
                    return obj.getPassMark();
                case "passMarkContract":
                    return obj.getPassMarkContract();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "discipline":
                    obj.setDiscipline((SetDiscipline) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "kind":
                    obj.setKind((EntranceDisciplineKind) value);
                    return;
                case "type":
                    obj.setType((EntranceDisciplineType) value);
                    return;
                case "budget":
                    obj.setBudget((Boolean) value);
                    return;
                case "contract":
                    obj.setContract((Boolean) value);
                    return;
                case "passMark":
                    obj.setPassMark((Double) value);
                    return;
                case "passMarkContract":
                    obj.setPassMarkContract((Double) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "discipline":
                        return true;
                case "studentCategory":
                        return true;
                case "kind":
                        return true;
                case "type":
                        return true;
                case "budget":
                        return true;
                case "contract":
                        return true;
                case "passMark":
                        return true;
                case "passMarkContract":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "discipline":
                    return true;
                case "studentCategory":
                    return true;
                case "kind":
                    return true;
                case "type":
                    return true;
                case "budget":
                    return true;
                case "contract":
                    return true;
                case "passMark":
                    return true;
                case "passMarkContract":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "discipline":
                    return SetDiscipline.class;
                case "studentCategory":
                    return StudentCategory.class;
                case "kind":
                    return EntranceDisciplineKind.class;
                case "type":
                    return EntranceDisciplineType.class;
                case "budget":
                    return Boolean.class;
                case "contract":
                    return Boolean.class;
                case "passMark":
                    return Double.class;
                case "passMarkContract":
                    return Double.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntranceDiscipline> _dslPath = new Path<EntranceDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntranceDiscipline");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getDiscipline()
     */
    public static SetDiscipline.Path<SetDiscipline> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Виды вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getKind()
     */
    public static EntranceDisciplineKind.Path<EntranceDisciplineKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return Тип вступительного испытания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getType()
     */
    public static EntranceDisciplineType.Path<EntranceDisciplineType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Бюджет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#isBudget()
     */
    public static PropertyPath<Boolean> budget()
    {
        return _dslPath.budget();
    }

    /**
     * @return Контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#isContract()
     */
    public static PropertyPath<Boolean> contract()
    {
        return _dslPath.contract();
    }

    /**
     * @return Зачетный балл.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getPassMark()
     */
    public static PropertyPath<Double> passMark()
    {
        return _dslPath.passMark();
    }

    /**
     * @return Зачетный балл (для небюджетников).
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getPassMarkContract()
     */
    public static PropertyPath<Double> passMarkContract()
    {
        return _dslPath.passMarkContract();
    }

    /**
     * @return Приоритет ВИ.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends EntranceDiscipline> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private SetDiscipline.Path<SetDiscipline> _discipline;
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private EntranceDisciplineKind.Path<EntranceDisciplineKind> _kind;
        private EntranceDisciplineType.Path<EntranceDisciplineType> _type;
        private PropertyPath<Boolean> _budget;
        private PropertyPath<Boolean> _contract;
        private PropertyPath<Double> _passMark;
        private PropertyPath<Double> _passMarkContract;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EntranceDisciplineGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Элемент набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getDiscipline()
     */
        public SetDiscipline.Path<SetDiscipline> discipline()
        {
            if(_discipline == null )
                _discipline = new SetDiscipline.Path<SetDiscipline>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Виды вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getKind()
     */
        public EntranceDisciplineKind.Path<EntranceDisciplineKind> kind()
        {
            if(_kind == null )
                _kind = new EntranceDisciplineKind.Path<EntranceDisciplineKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return Тип вступительного испытания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getType()
     */
        public EntranceDisciplineType.Path<EntranceDisciplineType> type()
        {
            if(_type == null )
                _type = new EntranceDisciplineType.Path<EntranceDisciplineType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Бюджет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#isBudget()
     */
        public PropertyPath<Boolean> budget()
        {
            if(_budget == null )
                _budget = new PropertyPath<Boolean>(EntranceDisciplineGen.P_BUDGET, this);
            return _budget;
        }

    /**
     * @return Контракт. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#isContract()
     */
        public PropertyPath<Boolean> contract()
        {
            if(_contract == null )
                _contract = new PropertyPath<Boolean>(EntranceDisciplineGen.P_CONTRACT, this);
            return _contract;
        }

    /**
     * @return Зачетный балл.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getPassMark()
     */
        public PropertyPath<Double> passMark()
        {
            if(_passMark == null )
                _passMark = new PropertyPath<Double>(EntranceDisciplineGen.P_PASS_MARK, this);
            return _passMark;
        }

    /**
     * @return Зачетный балл (для небюджетников).
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getPassMarkContract()
     */
        public PropertyPath<Double> passMarkContract()
        {
            if(_passMarkContract == null )
                _passMarkContract = new PropertyPath<Double>(EntranceDisciplineGen.P_PASS_MARK_CONTRACT, this);
            return _passMarkContract;
        }

    /**
     * @return Приоритет ВИ.
     * @see ru.tandemservice.uniec.entity.entrant.EntranceDiscipline#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EntranceDisciplineGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return EntranceDiscipline.class;
        }

        public String getEntityName()
        {
            return "entranceDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
