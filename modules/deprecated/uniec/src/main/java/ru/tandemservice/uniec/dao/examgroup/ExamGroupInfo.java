/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

/**
 * Информация о возможной экзаменационной группе
 *
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class ExamGroupInfo implements IExamGroupInfo
{
    private String _key;

    private String _title;

    private String _shortTitle;

    public ExamGroupInfo(String key, String title, String shortTitle)
    {
        _key = key;
        _title = title;
        _shortTitle = shortTitle;
    }

    @Override
    public String getKey()
    {
        return _key;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    @Override
    public String getShortTitle()
    {
        return _shortTitle;
    }
}
