// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaignPeriod;

/**
 * @author oleyba
 * @since 14.05.2010
 */
public class Model
{
    private String title;
    private EducationYear eduYear;
    private EnrollmentCampaignPeriod _period = new EnrollmentCampaignPeriod();

    private ISelectModel _educationYearList;
    private List<EnrollmentCampaign> campaignList;
    private EnrollmentCampaign campaignToCopy;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public EducationYear getEduYear()
    {
        return eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        this.eduYear = eduYear;
    }

    public EnrollmentCampaignPeriod getPeriod()
    {
        return _period;
    }

    public void setPeriod(EnrollmentCampaignPeriod period)
    {
        _period = period;
    }

    public ISelectModel getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(ISelectModel educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public List<EnrollmentCampaign> getCampaignList()
    {
        return campaignList;
    }

    public void setCampaignList(List<EnrollmentCampaign> campaignList)
    {
        this.campaignList = campaignList;
    }

    public EnrollmentCampaign getCampaignToCopy()
    {
        return campaignToCopy;
    }

    public void setCampaignToCopy(EnrollmentCampaign campaignToCopy)
    {
        this.campaignToCopy = campaignToCopy;
    }
}
