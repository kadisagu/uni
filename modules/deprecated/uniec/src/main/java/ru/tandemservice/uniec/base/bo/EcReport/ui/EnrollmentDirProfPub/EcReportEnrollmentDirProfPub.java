/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDirProfPub;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Alexander Shaburov
 * @since 04.07.12
 */
@Configuration
public class EcReportEnrollmentDirProfPub extends BusinessComponentManager
{
}
