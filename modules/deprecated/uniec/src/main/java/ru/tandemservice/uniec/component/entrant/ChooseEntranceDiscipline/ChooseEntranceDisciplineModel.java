/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ChooseEntranceDiscipline;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

/**
 * @author vip_delete
 * @since 17.03.2009
 */
@State({
        @Bind(key = ChooseEntranceDisciplineModel.BINDING_DIRECTION_ID, binding = "directionId"),
        @Bind(key = ChooseEntranceDisciplineModel.BINDING_ENTRANT_REQUEST_ID, binding = "entrantRequestId"),
        @Bind(key = ChooseEntranceDisciplineModel.BINDING_ENTRANT_ID, binding = "entrantId")
})
public class ChooseEntranceDisciplineModel
{
    public static final String BINDING_DIRECTION_ID = "directionId";             // выбираем для направления
    public static final String BINDING_ENTRANT_REQUEST_ID = "entrantRequestId";  // выбираем много для заявления
    public static final String BINDING_ENTRANT_ID = "entrantId";                 // выбираем много для абитуриента

    public static final String COLUMN_ENROLLMENT_DIRECTION_TITLE = "enrollmentDirectionTitle";
    public static final String COLUMN_EXAM_SET = "examSet";

    // binding data
    private Long _directionId;
    private Long _entrantRequestId;
    private Long _entrantId;
    private Entrant _entrant;
    private EntrantRequest _entrantRequest;

    // page data
    private DynamicListDataSource<RequestedEnrollmentDirectionWrapper> _dataSource;
    private boolean _disciplineFormRestriction;
    private EnrollmentCampaign _enrollmentCampaign;

    // Getters & Setters

    public Long getDirectionId()
    {
        return _directionId;
    }

    public void setDirectionId(Long directionId)
    {
        _directionId = directionId;
    }

    public Long getEntrantRequestId()
    {
        return _entrantRequestId;
    }

    public void setEntrantRequestId(Long entrantRequestId)
    {
        _entrantRequestId = entrantRequestId;
    }

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        _entrantRequest = entrantRequest;
    }

    public DynamicListDataSource<RequestedEnrollmentDirectionWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<RequestedEnrollmentDirectionWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public boolean isDisciplineFormRestriction()
    {
        return _disciplineFormRestriction;
    }

    public void setDisciplineFormRestriction(boolean disciplineFormRestriction)
    {
        _disciplineFormRestriction = disciplineFormRestriction;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}
