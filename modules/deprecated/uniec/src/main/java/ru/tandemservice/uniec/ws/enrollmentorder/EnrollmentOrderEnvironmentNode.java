// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.uniec.ws.enrollmentorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Информация о приказах о зачислении для вебсервиса EnrollmentOrderService
 *
 * @author Vasily Zhukov
 * @since 25.02.2011
 */
@XmlRootElement
public class EnrollmentOrderEnvironmentNode
{
    /**
     * Название приемной кампании
     */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /**
     * Название года обучения приемной кампании
     */
    @XmlAttribute(required = true)
    public String educationYearTitle;

    /**
     * Справочник "Вид возмещения затрат"
     */
    public CompensationTypeNode compensationType = new CompensationTypeNode();

    /**
     * Справочник "Типы приказов о зачислении абитуриентов"
     */
    public EnrollmentOrderTypeNode enrollmentOrderType = new EnrollmentOrderTypeNode();

    /**
     * Справочник "Формы освоения"
     */
    public DevelopFormNode developForm = new DevelopFormNode();

    /**
     * Справочник "Условия освоения"
     */
    public DevelopConditionNode developCondition = new DevelopConditionNode();

    /**
     * Справочник "Технологии освоения"
     */
    public DevelopTechNode developTech = new DevelopTechNode();

    /**
     * Справочник "Сроки освоения"
     */
    public DevelopPeriodNode developPeriod = new DevelopPeriodNode();

    /**
     * Справочник "Квалификации (уровни профессионального образования)"
     */
    public QualificationNode qualification = new QualificationNode();

    /**
     * Справочник "Структура направлений подготовки (специальностей) Минобрнауки РФ"
     */
    public StructureEducationLevelNode structureEducationLevel = new StructureEducationLevelNode();

    /**
     * Все типы всех используемых формирующих и территориальных подразделений из всех полученных выписок о зачислении
     */
    public OrgUnitTypeNode orgUnitType = new OrgUnitTypeNode();

    /**
     * Все используемые формирующие и территориальные подразделения из всех полученных выписок о зачислении
     */
    public OrgUnitNode orgUnit = new OrgUnitNode();

    /**
     * Все используемые направления подготовки (специальности) вуза из всех полученных выписок о зачислении
     */
    public EducationLevelHighSchoolNode educationLevelHighSchool = new EducationLevelHighSchoolNode();

    /**
     * Все приказы о зачислении в указанной приемной кампании
     */
    public EnrollmentOrderNode enrollmentOrder = new EnrollmentOrderNode();

    /**
     * Простая строка таблицы
     */
    public static class Row implements Comparable<Row>
    {
        public Row()
        {
        }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /**
     * Справочник "Вид возмещения затрат"
     */
    public static class CompensationTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Типы приказов о зачислении абитуриентов"
     */
    public static class EnrollmentOrderTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Формы освоения"
     */
    public static class DevelopFormNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Условия освоения"
     */
    public static class DevelopConditionNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Технологии освоения"
     */
    public static class DevelopTechNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Сроки освоения"
     */
    public static class DevelopPeriodNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Квалификации (уровни профессионального образования)"
     */
    public static class QualificationNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Структура направлений подготовки (специальностей) Минобрнауки РФ"
     */
    public static class StructureEducationLevelNode
    {
        public static class StructureEducationLevelRow extends Row
        {
            public StructureEducationLevelRow()
            {
            }

            public StructureEducationLevelRow(String title, String id, String parent)
            {
                super(title, id);
                this.parent = parent;
            }

            /**
             * Родительский элемент
             *
             * @see StructureEducationLevelNode
             */
            @XmlAttribute
            public String parent;
        }

        public List<StructureEducationLevelRow> row = new ArrayList<StructureEducationLevelRow>();
    }

    /**
     * Все типы всех используемых формирующих и территориальных подразделений из всех полученных выписок о зачислении
     */
    public static class OrgUnitTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Все используемые формирующие и территориальные подразделения из всех полученных выписок о зачислении
     */
    public static class OrgUnitNode
    {
        /**
         * Подразделение
         */
        public static class OrgUnitRow extends Row
        {
            public OrgUnitRow()
            {
            }

            public OrgUnitRow(String shortTitle, String title, String id, String orgUnitType, String nominativeCaseTitle, String settlementTitle, String territorialTitle, String territorialShortTitle)
            {
                super(shortTitle, title, id);
                this.orgUnitType = orgUnitType;
                this.nominativeCaseTitle = nominativeCaseTitle;
                this.settlementTitle = settlementTitle;
                this.territorialTitle = territorialTitle;
                this.territorialShortTitle = territorialShortTitle;
            }

            /**
             * Тип подразделения
             *
             * @see OrgUnitTypeNode
             */
            @XmlAttribute(required = true)
            public String orgUnitType;

            /**
             * Печатное название подразделения в именительном падеже
             */
            @XmlAttribute
            public String nominativeCaseTitle;

            /**
             * Название населенного пункта из фактического адреса подразделения
             */
            @XmlAttribute
            public String settlementTitle;

            /**
             * Название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialTitle;

            /**
             * Сокращенное название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialShortTitle;
        }

        public List<OrgUnitRow> row = new ArrayList<OrgUnitRow>();
    }

    /**
     * Все используемые направления подготовки (специальности) вуза из всех полученных выписок о зачислении
     */
    public static class EducationLevelHighSchoolNode
    {
        /**
         * Направление подготовки (специальность) вуза
         */
        public static class EducationLevelHighSchoolRow extends Row
        {
            public EducationLevelHighSchoolRow()
            {
            }

            public EducationLevelHighSchoolRow(String shortTitle, String title, String id, String code, String qualification)
            {
                super(shortTitle, title, id);
                this.code = code;
                this.qualification = qualification;
            }

            /**
             * Код
             */
            @XmlAttribute(required = true)
            public String code;

            /**
             * Квалификация (уровень профессионального образования)
             *
             * @see QualificationNode
             */
            @XmlAttribute(required = true)
            public String qualification;
        }

        public List<EducationLevelHighSchoolRow> row = new ArrayList<EducationLevelHighSchoolRow>();
    }

    /**
     * Все выписки о зачислении
     */
    public static class EnrollmentExtractNode
    {
        /**
         * Выписка о зачислении
         */
        public static class EnrollmentExtractRow
        {
            public EnrollmentExtractRow()
            {
            }

            public EnrollmentExtractRow(String middleName, String firstName, String lastName, String entrantId)
            {
                this.middleName = middleName;
                this.firstName = firstName;
                this.lastName = lastName;
                this.entrantId = entrantId;
            }

            /**
             * Отчество
             */
            @XmlAttribute(required = true)
            public String middleName;

            /**
             * Имя
             */
            @XmlAttribute(required = true)
            public String firstName;

            /**
             * Фамилия
             */
            @XmlAttribute(required = true)
            public String lastName;

            /**
             * Идентификатор абитуриента
             */
            @XmlAttribute(required = true)
            public String entrantId;
        }

        public List<EnrollmentExtractRow> row = new ArrayList<EnrollmentExtractRow>();
    }

    /**
     * Все параграфы о зачислении
     */
    public static class EnrollmentParagraphNode
    {
        /**
         * Параграф о зачислении
         */
        public static class EnrollmentParagraphRow
        {
            /**
             * Идентификатор студента предзачисления, который будет студентом-старостой группы после проведения приказа
             */
            @XmlAttribute
            public String groupManager;

            /**
             * Срок освоения
             *
             * @see DevelopPeriodNode
             */
            @XmlAttribute(required = true)
            public String developPeriod;

            /**
             * Технология освоения
             *
             * @see DevelopTechNode
             */
            @XmlAttribute(required = true)
            public String developTech;

            /**
             * Условие освоения
             *
             * @see DevelopConditionNode
             */
            @XmlAttribute(required = true)
            public String developCondition;

            /**
             * Форма освоения
             *
             * @see DevelopFormNode
             */
            @XmlAttribute(required = true)
            public String developForm;

            /**
             * Элемент структуры направлений подготовки (специальностей) Минобрнауки РФ
             *
             * @see StructureEducationLevelNode
             */
            @XmlAttribute(required = true)
            public String structureEducationLevel;

            /**
             * Направление подготовки (специальности) вуза
             *
             * @see EducationLevelHighSchoolNode
             */
            @XmlAttribute(required = true)
            public String educationLevelHighSchool;

            /**
             * Территориальное подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String territorialOrgUnit;

            /**
             * Формирующее подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String formativeOrgUnit;

            /**
             * Идентификатор направления подготовки подразделения.
             * Может использоваться для связи данных из других веб-сервисов
             */
            @XmlAttribute(required = true)
            public String educationOrgUnit;

            /**
             * Все выписки о зачислении
             */
            public EnrollmentExtractNode extract = new EnrollmentExtractNode();
        }

        public List<EnrollmentParagraphRow> row = new ArrayList<EnrollmentParagraphRow>();
    }

    /**
     * Все приказы о зачислении
     */
    public static class EnrollmentOrderNode
    {
        /**
         * Приказ о зачислении
         */
        public static class EnrollmentOrderRow
        {
            /**
             * Тип приказа о зачислении абитуриентов
             *
             * @see EnrollmentOrderTypeNode
             */
            @XmlAttribute(required = true)
            public String enrollmentOrderType;

            /**
             * Дата приказа
             */
            @XmlAttribute(required = true)
            public Date date;

            /**
             * Номер приказа
             */
            @XmlAttribute(required = true)
            public String number;

            /**
             * Печатная форма приказа
             */
            @XmlAttribute
            public byte[] text;

            /**
             * Идентификатор приказа
             */
            @XmlAttribute
            public long id;

            /**
             * Все параграфы о зачислении
             */
            public EnrollmentParagraphNode paragraph = new EnrollmentParagraphNode();
        }

        public List<EnrollmentOrderRow> row = new ArrayList<EnrollmentOrderRow>();
    }
}
