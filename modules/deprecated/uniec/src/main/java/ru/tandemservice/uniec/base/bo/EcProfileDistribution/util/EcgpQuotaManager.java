/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.util;

import org.apache.commons.lang.mutable.MutableInt;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Алгоритм выбора профилей направлений из строки рейтинга в соответствии с планами
 *
 * @author Vasily Zhukov
 * @since 14.05.2012
 */
public class EcgpQuotaManager implements IEcgQuotaManager
{
    private int _freeTotal;
    private Map<Long, MutableInt> _freeMap = new HashMap<>();

    public EcgpQuotaManager(IEcgpQuotaFreeDTO quotaFreeDTO)
    {
        for (Map.Entry<Long, Integer> entry : quotaFreeDTO.getFreeMap().entrySet())
            _freeMap.put(entry.getKey(), new MutableInt((int) (entry.getValue() == null ? Integer.MAX_VALUE : entry.getValue())));
        _freeTotal = quotaFreeDTO.getTotalFree();
    }

    @Override
    public IEcgPlanChoiceResult getPossibleDirectionList(IEcgEntrantRateRowDTO rateRowDTO, Long preferableDirectionId)
    {
        List<IEcgEntrantRateDirectionDTO> list = rateRowDTO.getDirectionList();

        IEcgEntrantRateDirectionDTO chosenDirection = null;
        List<IEcgEntrantRateDirectionDTO> possibleDirectionList = new ArrayList<>();

        MutableInt preferableMutableQuota = null;

        for (IEcgEntrantRateDirectionDTO direction : list)
        {
            MutableInt mutableQuota = _freeMap.get(direction.getDirectionId());

            if (mutableQuota.intValue() > 0)
            {
                possibleDirectionList.add(direction);

                if (direction.getId().equals(preferableDirectionId))
                {
                    chosenDirection = direction;
                    preferableMutableQuota = mutableQuota;
                }
            }
        }

        if (chosenDirection == null)
        {
            if (!possibleDirectionList.isEmpty())
            {
                chosenDirection = possibleDirectionList.get(0);
                _freeMap.get(chosenDirection.getDirectionId()).decrement();
            }
        } else
        {
            preferableMutableQuota.decrement();
        }

        return new EcgPlanChoiceResult(chosenDirection, possibleDirectionList);
    }

    @Override
    public int getFreeTotal()
    {
        return _freeTotal;
    }
}
