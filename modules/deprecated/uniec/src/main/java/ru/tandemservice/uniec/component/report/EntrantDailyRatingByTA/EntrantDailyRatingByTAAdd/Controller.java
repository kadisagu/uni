/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.hibsupport.DataAccessServices;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.ws.entrantrating.EntrantRatingTAEnvironmentNode;
import ru.tandemservice.uniec.ws.entrantrating.IEntrantRatingServiceDao;

/**
 * @author oleyba
 * @since 12.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        getModel(component).setPrincipalContext(component.getUserContext().getPrincipalContext());
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.isXmlFormat())
        {
            try
            {
                ByteArrayOutputStream result = new ByteArrayOutputStream();
                JAXBContext context = JAXBContext.newInstance(EntrantRatingTAEnvironmentNode.class);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                if (model.isByAllEnrollmentDirections())
                {
                    marshaller.marshal(IEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(
                            model.getEnrollmentCampaign(),
                            model.getReport().getDateFrom(),
                            model.getReport().getDateTo(),
                            model.getReport().getCompensationType(),
                            model.getStudentCategoryList(),
                            model.isHideEmptyDirections(),
                            model.getQualificationList(),
                            model.getDevelopFormList(),
                            model.getDevelopConditionList()
                    ), result);
                } else
                {
                    marshaller.marshal(IEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(
                            model.getEnrollmentCampaign(),
                            model.getReport().getDateFrom(),
                            model.getReport().getDateTo(),
                            model.getReport().getCompensationType(),
                            model.getStudentCategoryList(),
                            EnrollmentDirectionUtil.getEnrollmentDirection(model, DataAccessServices.dao().getComponentSession())
                    ), result);
                }

                String fileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-" + model.getReport().getCompensationType().getCode() + "-entrantRatingTAEnvironment.soap.xml";

                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName).document(result), false);
            } catch (JAXBException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        } else
        {
            getDao().update(model);

            deactivate(component);

            activateInRoot(component, new PublisherActivator(model.getReport()));
        }
    }

    public void onChangeByAllEnrollmentDirections(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.isByAllEnrollmentDirections())
            model.setHideEmptyDirections(false);
    }
}
