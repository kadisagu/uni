/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultMinzdrav2009.Add;

import java.util.List;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EnrollmentResultMinzdrav2009;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

/**
 * @author vip_delete
 * @since 18.08.2009
 */
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    private EnrollmentResultMinzdrav2009 _report = new EnrollmentResultMinzdrav2009();
    private IPrincipalContext _principalContext;
    private MultiEnrollmentDirectionUtil.Parameters _parameters;

    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private boolean _studentCategoryActive;
    private ISelectModel _studentCategoryListModel;
    private List<StudentCategory> _studentCategoryList;

    private boolean _qualificationActive;
    private ISelectModel _qualificationListModel;
    private List<Qualifications> _qualificationList;

    private boolean _formativeOrgUnitActive;
    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;

    private boolean _territorialOrgUnitActive;
    private ISelectModel _territorialOrgUnitListModel;
    private List<OrgUnit> _territorialOrgUnitList;

    private ISelectModel _developFormListModel;
    private DevelopForm _developForm;

    private boolean _developConditionActive;
    private ISelectModel _developConditionListModel;
    private List<DevelopCondition> _developConditionList;

    private boolean _developTechActive;
    private ISelectModel _developTechListModel;
    private List<DevelopTech> _developTechList;

    private boolean _developPeriodActive;
    private ISelectModel _developPeriodListModel;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _parallelActive;
    private List<CommonYesNoUIObject> _parallelList;
    private CommonYesNoUIObject _parallel;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(_report.getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return _formativeOrgUnitActive ? _formativeOrgUnitList : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return _territorialOrgUnitActive ? _territorialOrgUnitList : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(_developForm);
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return _developConditionActive ? _developConditionList : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return _developTechActive ? _developTechList : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return _developPeriodActive ? _developPeriodList : null;
    }

    // Getters & Setters

    public EnrollmentResultMinzdrav2009 getReport()
    {
        return _report;
    }

    public void setReport(EnrollmentResultMinzdrav2009 report)
    {
        _report = report;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<CommonYesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<CommonYesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public CommonYesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(CommonYesNoUIObject parallel)
    {
        _parallel = parallel;
    }
}
