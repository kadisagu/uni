/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;

import java.util.Date;
import java.util.List;

/**
 * @author vip_delete
 * @since 07.04.2009
 */
@Input({@Bind(key = "orderId", binding = "order.id")})
public class Model implements IEnrollmentCampaignSelectModel
{
    private EnrollmentOrder _order = new EnrollmentOrder();
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private ISelectModel _orderTypeListModel;
    private boolean _enrollmentCampaignDisabled;
    private boolean _orderTypeDisabled;
    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;
    private boolean _addForm;
    private Date _createDate;

    private ISelectModel _reasonModel;
    private ISelectModel _basicModel;
    private EnrollmentOrderBasic _basic;
    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _order.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _order.setEnrollmentCampaign(enrollmentCampaign);
    }

    public boolean isOrderTypeUseEnrollmentDate()
    {
        if (getOrder() == null || getOrder().getType() == null) return false;
        EnrollmentOrderType orderType = new DQLSelectBuilder().fromEntity(EnrollmentOrderType.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderType.entrantEnrollmentOrderType().fromAlias("e")), DQLExpressions.value(getOrder().getType())))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderType.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(getEnrollmentCampaign())))
                .createStatement(DataAccessServices.dao().getComponentSession()).uniqueResult();
        return orderType != null && orderType.isUseEnrollmentDate();
    }

    // Getters & Setters

    public EnrollmentOrderBasic getBasic()
    {
        return _basic;
    }

    public void setBasic(EnrollmentOrderBasic basic)
    {
        _basic = basic;
    }

    public ISelectModel getBasicModel()
    {
        return _basicModel;
    }

    public void setBasicModel(ISelectModel basicModel)
    {
        _basicModel = basicModel;
    }

    public ISelectModel getReasonModel()
    {
        return _reasonModel;
    }

    public void setReasonModel(ISelectModel reasonModel)
    {
        _reasonModel = reasonModel;
    }

    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrollmentOrder order)
    {
        _order = order;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getOrderTypeListModel()
    {
        return _orderTypeListModel;
    }

    public void setOrderTypeListModel(ISelectModel orderTypeListModel)
    {
        _orderTypeListModel = orderTypeListModel;
    }

    public boolean isEnrollmentCampaignDisabled()
    {
        return _enrollmentCampaignDisabled;
    }

    public void setEnrollmentCampaignDisabled(boolean enrollmentCampaignDisabled)
    {
        _enrollmentCampaignDisabled = enrollmentCampaignDisabled;
    }

    public boolean isOrderTypeDisabled()
    {
        return _orderTypeDisabled;
    }

    public void setOrderTypeDisabled(boolean orderTypeDisabled)
    {
        _orderTypeDisabled = orderTypeDisabled;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public Date getCreateDate()
    {
        return _createDate;
    }

    public void setCreateDate(Date createDate)
    {
        _createDate = createDate;
    }
}
