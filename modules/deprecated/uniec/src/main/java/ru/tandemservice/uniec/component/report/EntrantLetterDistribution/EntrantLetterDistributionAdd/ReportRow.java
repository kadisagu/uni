/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantLetterDistribution.EntrantLetterDistributionAdd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author oleyba
 * @since 06.07.2009
 */
class ReportRow implements ITitled
{
    private String _title;
    private String _fullTitle;
    private boolean _useFullTitle;
    private Set<Long> _usedIds = new HashSet<Long>();
    protected ReportRowGroup _group;
    protected Map<Character, Integer> _content = new HashMap<Character, Integer>();

    public ReportRow(ReportRowGroup group, String title, String fullTitle)
    {
        _fullTitle = fullTitle;
        _group = group;
        _title = title;
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    public String getPrintTitle()
    {
        return _useFullTitle ? _fullTitle : _title;
    }

    public void add(Set<Character> usedLetters, Entrant entrant)
    {
        char letter = entrant.getPerson().getIdentityCard().getLastName().charAt(0);
        usedLetters.add(letter);
        add(letter, entrant.getId(), true);
    }

    protected void add(Character key, Long id, boolean checkId)
    {
        if (checkId && _usedIds.contains(id)) return;
        if (checkId) _usedIds.add(id);
        Integer sum = _content.get(key);
        if (null == sum)
        {
            sum = 0;
            _content.put(key, sum);
        }
        _content.put(key, ++sum);
        if (null != _group) _group.add(key, null, false);
    }

    public Integer get(Character key)
    {
        return _content.get(key);
    }

    public Integer getSum()
    {
        Integer sum = 0;
        for (Integer entry : _content.values()) sum += entry;
        return sum;
    }

    public boolean isUseFullTitle()
    {
        return _useFullTitle;
    }

    public void useFullTitle()
    {
        _useFullTitle = true;
    }

    public void setGroup(ReportRowGroup group)
    {
        _group = group;
    }

    public ReportRowGroup getGroup()
    {
        return _group;
    }
}
