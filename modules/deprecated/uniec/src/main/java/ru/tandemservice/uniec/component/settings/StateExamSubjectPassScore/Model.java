/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.StateExamSubjectPassScore;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author oleyba
 * @since 07.07.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private DynamicListDataSource<SubjectWrapper> _dataSource;

    private IDataSettings _settings;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    public DynamicListDataSource<SubjectWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<SubjectWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public static class SubjectWrapper extends IdentifiableWrapper<StateExamSubject>
    {
        private static final long serialVersionUID = 8922939483996932403L;
        public static final String VIEW_P_SUBJECT = "subject";
        private StateExamSubject subject;
        private Integer mark;

        public SubjectWrapper(StateExamSubject i, Integer mark)
                throws ClassCastException
        {
            super(i);
            this.subject = i;
            this.mark = mark;
        }

        public Integer getMark()
        {
            return mark;
        }

        public void setMark(Integer mark)
        {
            this.mark = mark;
        }

        public StateExamSubject getSubject()
        {
            return subject;
        }
    }
}
