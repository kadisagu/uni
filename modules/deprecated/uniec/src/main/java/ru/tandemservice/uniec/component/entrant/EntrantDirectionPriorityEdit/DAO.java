/* $Id:$ */
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityEdit;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author oleyba
 * @since 5/25/12
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        if (model.getCompType().getId() != null)
            model.setCompType(getNotNull(CompensationType.class, model.getCompType().getId()));
        else
            model.setCompType(getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET));

        StaticListDataSource<RequestedDirectionPriorityDTO> dataSource = new StaticListDataSource<RequestedDirectionPriorityDTO>();
        model.setDataSource(dataSource);
        
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fullTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("priority", "Приоритет").setOrderable(false));
        
        List<RequestedEnrollmentDirection> directions = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("e")), DQLExpressions.value(model.getEntrant())))
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().fromAlias("e")), DQLExpressions.value(model.getCompType())))
                .order(DQLExpressions.property(RequestedEnrollmentDirection.priorityPerEntrant().fromAlias("e")))
                .createStatement(getSession()).list();
        
        int priority = 1;

        ArrayList<RequestedDirectionPriorityDTO> wrapperrs = new ArrayList<RequestedDirectionPriorityDTO>();
        for (RequestedEnrollmentDirection direction : directions)
            wrapperrs.add(new RequestedDirectionPriorityDTO(direction, priority++));
        model.getDataSource().setupRows(wrapperrs);
    }

    @Override
    public void update(Model model)
    {
        EntrantUtil.lockEntrant(getSession(), model.getEntrant());

        List<RequestedDirectionPriorityDTO> wrappers = model.getDataSource().getEntityList();

        Set<Integer> priorities = new HashSet<Integer>();
        for (RequestedDirectionPriorityDTO wrapper : wrappers)
        {
            if (wrapper.getOverallPriority() == null)
                throw new ApplicationException("Поле «Приоритет» должно быть заполнено.");
            if (wrapper.getOverallPriority() < 1 || wrapper.getOverallPriority() > wrappers.size())
                throw new ApplicationException("Приоритеты указаны неверно, необходимо вводить цифры от 1 до " + wrappers.size() + ".");
            priorities.add(wrapper.getOverallPriority());
        }
        if (wrappers.size() != priorities.size())
            throw new ApplicationException("Приоритеты не могут совпадать.");

        final Session session = getSession();

        int fakePriority = -1;
        for (RequestedDirectionPriorityDTO wrapper : wrappers)
        {
            wrapper.getDirection().setPriorityPerEntrant(fakePriority--);
            session.update(wrapper.getDirection());
        }
        session.flush();

        for (RequestedDirectionPriorityDTO wrapper : wrappers)
        {
            RequestedEnrollmentDirection requestedEnrollmentDirection = (RequestedEnrollmentDirection) get(wrapper.getDirection().getId());
            requestedEnrollmentDirection.setPriorityPerEntrant(wrapper.getOverallPriority());
            session.update(requestedEnrollmentDirection);
        }
        session.flush();

        EcEntrantManager.instance().dao().doNormalizePriorityPerEntrant(model.getEntrant());
    }
}
