/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
@Configuration
public class EcDistributionManager extends BusinessObjectManager
{
    public static EcDistributionManager instance()
    {
        return instance(EcDistributionManager.class);
    }

    @Bean
    public IEcDistributionDao dao()
    {
        return new EcDistributionDao();
    }

    public static final DataWrapper DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS = new DataWrapper(0L, "Студент/Слушатель");
    public static final DataWrapper DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION = new DataWrapper(1L, "Второе высшее");
}
