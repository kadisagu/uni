/**
 *$Id$
 */
package ru.tandemservice.uniec.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.event.IEventServiceLock;
import ru.tandemservice.uniec.base.bo.EcSystemAction.EcSystemActionManager;
import ru.tandemservice.uniec.dao.UniecDAOFacade;

/**
 * @author Alexander Shaburov
 * @since 13.02.13
 */
public class EcSystemActionPubAddon extends UIAddon
{
    public EcSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
    
    public void onClickCheckEntrantLogicConsistency() throws Exception
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            UniecDAOFacade.getEntrantDAO().doCheckEntrantLogicConsistency();
        } finally
        {
            eventLock.release();
        }
    }

    public void onClickUpdateEntrantData()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {

            EcSystemActionManager.instance().dao().updateEntrantData();
        } finally
        {
            eventLock.release();
        }
    }

    public void onClickSetRequestedEnrollmentDirectionProfileDisciplines()
    {
        EcSystemActionManager.instance().dao().setRequestedEnrollmentDirectionProfileDisciplines();
    }

    public void onClickCorrectExamGroupNamesMechanism1()
    {
        EcSystemActionManager.instance().dao().doCorrectExamGroupNamesMechanism1();
    }

    public void onClickGetEnrollmentEnvironmentXml()
    {
        getActivationBuilder().asCurrent("ru.tandemservice.uniec.component.settings.EnrollmentEnvironmentAdd")
                .activate();
    }
}
