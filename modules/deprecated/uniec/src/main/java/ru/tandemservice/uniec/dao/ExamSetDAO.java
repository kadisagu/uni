/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.examset.ExamSetItemComparator;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author agolubenko
 * @since 22.01.2009
 */
public class ExamSetDAO extends UniBaseDao implements IExamSetDAO
{
    /**
     * Системный метод вычисления всех наборов в указанном году
     *
     * @param enrollmentCampaign год приема
     * @return ID набора -> набор
     */
    @Override
    public Map<String, ExamSet> getExamSetMap(EnrollmentCampaign enrollmentCampaign)
    {
        List<EntranceDiscipline> entranceDisciplines = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "e")
                .addJoinFetch("e", EntranceDiscipline.L_ENROLLMENT_DIRECTION, "direction")
                .addJoinFetch("e", EntranceDiscipline.L_DISCIPLINE, "discipline")
                .add(MQExpression.eq("e", EntranceDiscipline.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                .getResultList(getSession());

        List<EntranceDiscipline2SetDisciplineRelation> relationList = new MQBuilder(EntranceDiscipline2SetDisciplineRelation.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", EntranceDiscipline2SetDisciplineRelation.L_ENTRANCE_DISCIPLINE + "." + EntranceDiscipline.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                .getResultList(getSession());

        Map<EntranceDiscipline, List<SetDiscipline>> entranceDisciplineListMap = new HashMap<>();
        for (EntranceDiscipline2SetDisciplineRelation rel : relationList)
        {
            List<SetDiscipline> list = entranceDisciplineListMap.get(rel.getEntranceDiscipline());
            if (list == null)
                entranceDisciplineListMap.put(rel.getEntranceDiscipline(), list = new ArrayList<>());
            list.add(rel.getSetDiscipline());
        }

        // EnrollmentDirection -> StudentCategory -> List<ExamSetItem>
        Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> map = new HashMap<>();
        for (EntranceDiscipline entranceDiscipline : entranceDisciplines)
        {
            Map<StudentCategory, List<ExamSetItem>> value = map.get(entranceDiscipline.getEnrollmentDirection());
            if (value == null)
                map.put(entranceDiscipline.getEnrollmentDirection(), value = new HashMap<>());

            boolean budget = entranceDiscipline.isBudget();
            boolean contract = entranceDiscipline.isContract();
            EntranceDisciplineKind kind = entranceDiscipline.getKind();
            EntranceDisciplineType type = entranceDiscipline.getType();
            StudentCategory studentCategory = entranceDiscipline.getStudentCategory();
            SetDiscipline subject = entranceDiscipline.getDiscipline();
            List<SetDiscipline> choice = entranceDisciplineListMap.get(entranceDiscipline);
            if (choice == null)
                choice = Collections.emptyList();

            List<ExamSetItem> list = value.get(studentCategory);
            if (list == null)
                value.put(studentCategory, list = new ArrayList<>());

            ExamSetItem examSetItem = new ExamSetItem((long) list.size(), budget, contract, kind, type, subject, choice);
            examSetItem.getList().add(entranceDiscipline);
            list.add(examSetItem);
        }

        // сейчас все EnrollmentDirection и StudentCategory, у которых один и тот же List<ExamSetItem>, объявляем в одной группе

        // examSetId -> ExamSet
        Map<String, ExamSet> result = new HashMap<>();
        long id = 0L;
        ExamSetItemComparator comparator = new ExamSetItemComparator();
        for (Map.Entry<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> directionMapEntry : map.entrySet())
        {
            final EnrollmentDirection direction = directionMapEntry.getKey();

            for (Map.Entry<StudentCategory, List<ExamSetItem>> entry : directionMapEntry.getValue().entrySet())
            {
                final StudentCategory studentCategory = entry.getKey();
                final List<ExamSetItem> list = entry.getValue();

                Collections.sort(list, comparator);

                ExamSet newExamSet = new ExamSet(id, enrollmentCampaign, studentCategory, list);

                ExamSet savedExamSet = result.get(newExamSet.getExamSetId());

                if (savedExamSet == null)
                {
                    id++;
                    newExamSet.getList().add(direction);
                    result.put(newExamSet.getExamSetId(), newExamSet);
                } else
                {
                    savedExamSet.getList().add(direction);
                    for (int i = 0; i < list.size(); i++)
                        savedExamSet.getSetItemList().get(i).getList().add(list.get(i).getList().get(0));
                }
            }
        }


        // создаем пустые наборы для каждой категории поступающего

        // если в приемной кампании нет разницы наборов по категориям, то пустой набор надо создать только на категорию студента
        List<StudentCategory> studentCategoryList = enrollmentCampaign.isExamSetDiff() ? getCatalogItemList(StudentCategory.class) : Arrays.asList(getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT));

        // для этого достаточно найти направления приема в указанной приемной кампании у которых нет вступительных испытаний по указанной категории
        for (StudentCategory studentCategory : studentCategoryList)
        {
            List<EnrollmentDirection> list = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "e")
                    .add(MQExpression.eq("e", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                    .add(MQExpression.notIn("e", EnrollmentDirection.P_ID, new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "e", new String[]{EntranceDiscipline.L_ENROLLMENT_DIRECTION + ".id"})
                            .add(MQExpression.eq("e", EntranceDiscipline.L_STUDENT_CATEGORY, studentCategory))
                            .add(MQExpression.eq("e", EntranceDiscipline.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))))
                    .getResultList(getSession());

            if (list.size() > 0)
            {
                ExamSet examSet = new ExamSet(id, enrollmentCampaign, studentCategory, new ArrayList<ExamSetItem>());
                examSet.getList().addAll(list);
                result.put(examSet.getExamSetId(), examSet);
            }
        }

        return result;
    }

    @Override
    public Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> getExamSetItemByEnrDirAndCategory(EnrollmentCampaign enrollmentCampaign)
    {
        List<EntranceDiscipline> entranceDisciplines = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "e")
                .addJoinFetch("e", EntranceDiscipline.L_ENROLLMENT_DIRECTION, "direction")
                .addJoinFetch("e", EntranceDiscipline.L_DISCIPLINE, "discipline")
                .add(MQExpression.eq("e", EntranceDiscipline.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                .getResultList(getSession());

        List<EntranceDiscipline2SetDisciplineRelation> relationList = new MQBuilder(EntranceDiscipline2SetDisciplineRelation.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", EntranceDiscipline2SetDisciplineRelation.L_ENTRANCE_DISCIPLINE + "." + EntranceDiscipline.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign))
                .getResultList(getSession());

        Map<EntranceDiscipline, List<SetDiscipline>> entranceDisciplineListMap = new HashMap<>();
        for (EntranceDiscipline2SetDisciplineRelation rel : relationList)
        {
            List<SetDiscipline> list = entranceDisciplineListMap.get(rel.getEntranceDiscipline());
            if (list == null)
                entranceDisciplineListMap.put(rel.getEntranceDiscipline(), list = new ArrayList<>());
            list.add(rel.getSetDiscipline());
        }

        // EnrollmentDirection -> StudentCategory -> List<ExamSetItem>
        Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> map = new HashMap<>();
        for (EntranceDiscipline entranceDiscipline : entranceDisciplines)
        {
            if (entranceDiscipline.getEnrollmentDirection().getId().equals(1338045874728604854L))
                System.out.print("1");

            Map<StudentCategory, List<ExamSetItem>> value = map.get(entranceDiscipline.getEnrollmentDirection());
            if (value == null)
                map.put(entranceDiscipline.getEnrollmentDirection(), value = new HashMap<>());

            boolean budget = entranceDiscipline.isBudget();
            boolean contract = entranceDiscipline.isContract();
            EntranceDisciplineKind kind = entranceDiscipline.getKind();
            EntranceDisciplineType type = entranceDiscipline.getType();
            StudentCategory studentCategory = entranceDiscipline.getStudentCategory();
            SetDiscipline subject = entranceDiscipline.getDiscipline();
            List<SetDiscipline> choice = entranceDisciplineListMap.get(entranceDiscipline);
            if (choice == null)
                choice = Collections.emptyList();

            List<ExamSetItem> list = value.get(studentCategory);
            if (list == null)
                value.put(studentCategory, list = new ArrayList<>());

            ExamSetItem examSetItem = new ExamSetItem((long) list.size(), budget, contract, kind, type, subject, choice);
            examSetItem.getList().add(entranceDiscipline);
            list.add(examSetItem);
        }

        return map;
    }


    @Override
    public List<SetDiscipline> getChoicableDisciplines(EntranceDiscipline entranceDiscipline)
    {
        return getChoicableDisciplines(Collections.singletonList(entranceDiscipline)).get(entranceDiscipline);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<EntranceDiscipline, List<SetDiscipline>> getChoicableDisciplines(Collection<EntranceDiscipline> entranceDisciplines)
    {
        if (entranceDisciplines.isEmpty())
        {
            return Collections.emptyMap();
        }

        Criteria criteria = getSession().createCriteria(EntranceDiscipline2SetDisciplineRelation.class);
        criteria.createAlias(EntranceDiscipline2SetDisciplineRelation.setDiscipline().s(), "setDiscipline");
        criteria.add(Restrictions.in(EntranceDiscipline2SetDisciplineRelation.entranceDiscipline().s(), entranceDisciplines));

        // инициализируем
        Map<EntranceDiscipline, List<SetDiscipline>> result = new HashMap<>();
        for (EntranceDiscipline entranceDiscipline : entranceDisciplines)
        {
            result.put(entranceDiscipline, new ArrayList<SetDiscipline>());
        }

        // группируем
        for (EntranceDiscipline2SetDisciplineRelation relation : (List<EntranceDiscipline2SetDisciplineRelation>) criteria.list())
        {
            result.get(relation.getEntranceDiscipline()).add(relation.getSetDiscipline());
        }

        // сортируем
        for (List<SetDiscipline> setDisciplines : result.values())
        {
            Collections.sort(setDisciplines, ITitled.TITLED_COMPARATOR);
        }

        return result;
    }

    @Override
    public Set<ExamSet> getCoveredExamSet(Entrant entrant)
    {
        Set<Discipline2RealizationWayRelation> disciplineList = getCoveredDisciplineList(entrant);

        Set<ExamSet> result = new HashSet<>();
        for (ExamSet examSet : getExamSetMap(entrant.getEnrollmentCampaign()).values())
        {
            boolean examSetCovered = true;
            int i = 0;
            while (i < examSet.getSetItemList().size() && examSetCovered)
            {
                ExamSetItem examSetItem = examSet.getSetItemList().get(i);
                List<Discipline2RealizationWayRelation> itemList = new ArrayList<>();
                itemList.addAll(examSetItem.getSubject().getDisciplines());
                for (SetDiscipline discipline : examSetItem.getChoice())
                    itemList.addAll(discipline.getDisciplines());

                // строка набора покрывается, если хотя бы одна дисциплина из нее покрывается
                int j = 0;
                while (j < itemList.size() && !disciplineList.contains(itemList.get(j))) j++;
                examSetCovered = j < itemList.size();
                i++;
            }

            if (examSetCovered)
                result.add(examSet);
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<EnrollmentDirection> getCoveredEnrollmentDirection(Entrant entrant)
    {
        Set<Discipline2RealizationWayRelation> disciplineList = getCoveredDisciplineList(entrant);

        Set<EnrollmentDirection> result = new HashSet<>();
        Map<EnrollmentDirection, List<EntranceDiscipline>> direction2Disciplines = SafeMap.get(ArrayList.class);
        List<EntranceDiscipline> entranceDisciplines = new ArrayList<>();

        Criteria criteria = getSession().createCriteria(EntranceDiscipline.class);
        criteria.createAlias(EntranceDiscipline.enrollmentDirection().s(), "enrollmentDirection");
        criteria.createAlias(EntranceDiscipline.discipline().s(), "discipline");
        criteria.add(Restrictions.eq(EnrollmentDirection.enrollmentCampaign().fromAlias("enrollmentDirection").s(), entrant.getEnrollmentCampaign()));
        for (EntranceDiscipline entranceDiscipline : (List<EntranceDiscipline>) criteria.list())
        {
            direction2Disciplines.get(entranceDiscipline.getEnrollmentDirection()).add(entranceDiscipline);
            entranceDisciplines.add(entranceDiscipline);
        }

        Map<EntranceDiscipline, List<SetDiscipline>> choicableDisciplines = getChoicableDisciplines(entranceDisciplines);

        for (Entry<EnrollmentDirection, List<EntranceDiscipline>> entry : direction2Disciplines.entrySet())
        {
            int i = 0;
            boolean directionCovered = true;
            List<EntranceDiscipline> disciplines = entry.getValue();

            while (i < disciplines.size() && directionCovered)
            {
                EntranceDiscipline entranceDiscipline = disciplines.get(i);
                List<Discipline2RealizationWayRelation> itemList = new ArrayList<>();
                itemList.addAll(entranceDiscipline.getDiscipline().getDisciplines());
                for (SetDiscipline discipline : choicableDisciplines.get(entranceDiscipline))
                {
                    itemList.addAll(discipline.getDisciplines());
                }

                // строка набора покрывается, если хотя бы одна дисциплина из нее покрывается
                int j = 0;
                while (j < itemList.size() && !disciplineList.contains(itemList.get(j)))
                {
                    j++;
                }
                directionCovered = j < itemList.size();
                i++;
            }

            if (directionCovered)
            {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    @Override
    public Set<Discipline2RealizationWayRelation> getCoveredDisciplineList(Entrant entrant)
    {
        MQBuilder builder = new MQBuilder(ConversionScale.ENTITY_CLASS, "c", new String[]{ConversionScale.L_DISCIPLINE});
        builder.add(MQExpression.eq("c", ConversionScale.discipline().enrollmentCampaign().s(), entrant.getEnrollmentCampaign()));
        builder.addDomain("m", StateExamSubjectMark.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("c", ConversionScale.L_SUBJECT, "m", StateExamSubjectMark.L_SUBJECT));
        builder.addDomain("f", Discipline2RealizationFormRelation.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("f", Discipline2RealizationFormRelation.L_DISCIPLINE, "c", ConversionScale.L_DISCIPLINE));
        builder.add(MQExpression.eq("m", StateExamSubjectMark.certificate().entrant().s(), entrant));
        builder.add(MQExpression.eq("f", Discipline2RealizationFormRelation.subjectPassForm().code().s(), UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM));
        builder.setNeedDistinct(true);
        return new HashSet<>(builder.<Discipline2RealizationWayRelation>getResultList(getSession()));
    }
}
