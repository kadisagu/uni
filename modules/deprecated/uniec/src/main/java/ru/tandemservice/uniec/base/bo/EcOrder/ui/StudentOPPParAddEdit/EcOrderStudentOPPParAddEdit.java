/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.StudentOPPParAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
@Configuration
public class EcOrderStudentOPPParAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EcOrderManager.PRE_STUDENT_DS, EcOrderManager.instance().preStudentDS(), EcOrderManager.instance().preStudentDSHandler()))
                .create();
    }
}
