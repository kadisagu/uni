/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.ui.RevertAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.OrderBasicsDSHandler;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;

/**
 * @author Nikolay Fedorovskih
 * @since 06.08.2013
 */
@Configuration
public class EcOrderRevertAddEdit extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";
    public static final String ORDER_REASON_DS = "orderReasonDS";
    public static final String ORDER_BASICS_DS = "orderBasicsDS";

    public static final String ORDER_REASON_PARAM = OrderBasicsDSHandler.ORDER_REASON_PARAM;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, employeePostDSHandler()).addColumn(EmployeePost.P_FULL_TITLE))
                .addDataSource(selectDS(ORDER_REASON_DS, orderReasonDSHandler()))
                .addDataSource(selectDS(ORDER_BASICS_DS, orderBasicsDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeePostDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeePost.class)
                .order(EmployeePost.person().identityCard().fullFio())
                .filter(EmployeePost.person().identityCard().fullFio())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderReasonDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentOrderReason.class)
                .order(EnrollmentOrderReason.title())
                .filter(EnrollmentOrderReason.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderBasicsDSHandler()
    {
        return new OrderBasicsDSHandler(getName());
    }
}