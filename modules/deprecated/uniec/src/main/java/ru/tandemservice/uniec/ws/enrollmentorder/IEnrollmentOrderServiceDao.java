package ru.tandemservice.uniec.ws.enrollmentorder;

import java.util.Date;
import java.util.List;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

/**
 * @author Vasily Zhukov
 * @since 25.02.2011
 */
public interface IEnrollmentOrderServiceDao
{
    public static final SpringBeanCache<IEnrollmentOrderServiceDao> INSTANCE = new SpringBeanCache<IEnrollmentOrderServiceDao>(IEnrollmentOrderServiceDao.class.getName());

    public EnrollmentOrderEnvironmentNode getEnrollmentOrderEnvironmentData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeCode, List<String> orderTypeCodes, List<String> formativeOrgUnitIds, List<String> developFormCodes, Boolean withPrintData);

    public EnrollmentOrderEnvironmentNode getEnrollmentOrderEnvironmentData(EnrollmentCampaign enrollmentCampaign, List<EnrollmentOrder> enrollmentOrderList);
}
