package ru.tandemservice.uniec.ws.entrantrating;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.entity.NotUsedBenefit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 29.03.2011
 */
public class EntrantRatingServiceDao extends CommonDAO implements IEntrantRatingServiceDao
{
    public static final String ORIGINAL = "1";
    public static final String COPY = "2";
    public static final String ENROLLED = "3";

    @Override
    public EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeId, List<String> studentCategoryIds, boolean hideEmptyDirections, List<String> qualificationIds, List<String> developFormIds, List<String> developConditionIds)
    {
        // загружаем приемную кампанию
        EnrollmentCampaign enrollmentCampaign = get(EnrollmentCampaign.class, EnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);
        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        IUniBaseDao dao = UniDaoFacade.getCoreDao();

        // загружаем вид возмещения затрат
        CompensationType compensationType = dao.getByNaturalId(new CompensationType.NaturalId(compensationTypeId));
        if (compensationType == null)
            throw new RuntimeException("CompensationType with code '" + compensationTypeId + "' not found!");

        // загружаем категории обучаемых
        List<StudentCategory> studentCategoryList = new ArrayList<>();
        if (studentCategoryIds != null)
            for (String code : studentCategoryIds)
            {
                StudentCategory studentCategory = dao.getByNaturalId(new StudentCategory.NaturalId(code));
                if (studentCategory == null)
                    throw new RuntimeException("StudentCategory with code '" + code + "' not found!");
                studentCategoryList.add(studentCategory);
            }

        // загружаем квалификации
        List<Qualifications> qualificationList = new ArrayList<>();
        if (qualificationIds != null)
            for (String code : qualificationIds)
            {
                Qualifications qualification = dao.getByNaturalId(new Qualifications.NaturalId(code));
                if (qualification == null)
                    throw new RuntimeException("Qualification with code '" + code + "' not found!");
                qualificationList.add(qualification);
            }

        // загружаем формы освоения
        List<DevelopForm> developFormList = new ArrayList<>();
        if (developFormIds != null)
            for (String code : developFormIds)
            {
                DevelopForm developForm = dao.getByNaturalId(new DevelopForm.NaturalId(code));
                if (developForm == null)
                    throw new RuntimeException("DevelopForm with code '" + code + "' not found!");
                developFormList.add(developForm);
            }

        // загружаем условия освоения
        List<DevelopCondition> developConditionList = new ArrayList<>();
        if (developConditionIds != null)
            for (String code : developConditionIds)
            {
                DevelopCondition developCondition = dao.getByNaturalId(new DevelopCondition.NaturalId(code));
                if (developCondition == null)
                    throw new RuntimeException("DevelopCondition with code '" + code + "' not found!");
                developConditionList.add(developCondition);
            }

        return getEntrantRatingTAEnvironmentData(enrollmentCampaign, dateFrom, dateTo, compensationType, studentCategoryList, hideEmptyDirections, qualificationList, developFormList, developConditionList);
    }

    @Override
    public EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeId, List<String> studentCategoryIds, String enrollmentDirectionId)
    {
        // загружаем приемную кампания
        EnrollmentCampaign enrollmentCampaign = get(EnrollmentCampaign.class, EnrollmentCampaign.P_TITLE, enrollmentCampaignTitle);
        if (enrollmentCampaign == null)
            throw new RuntimeException("EnrollmentCampaign with title '" + enrollmentCampaignTitle + "' not found!");

        IUniBaseDao dao = UniDaoFacade.getCoreDao();

        // загружаем вид возмещения затрат
        CompensationType compensationType = dao.getByNaturalId(new CompensationType.NaturalId(compensationTypeId));
        if (compensationType == null)
            throw new RuntimeException("CompensationType with code '" + compensationTypeId + "' not found!");

        // загружаем направление приема
        EnrollmentDirection enrollmentDirection = getNotNull(Long.parseLong(enrollmentDirectionId));
        if (enrollmentDirection == null)
            throw new RuntimeException("EnrollmentDirection with id '" + enrollmentDirectionId + "' not found!");

        // загружаем категории обучаемых
        List<StudentCategory> studentCategoryList = new ArrayList<>();
        if (studentCategoryIds != null)
            for (String code : studentCategoryIds)
            {
                StudentCategory studentCategory = dao.getByNaturalId(new StudentCategory.NaturalId(code));
                if (studentCategory == null)
                    throw new RuntimeException("StudentCategory with code '" + code + "' not found!");
                studentCategoryList.add(studentCategory);
            }

        return getEntrantRatingTAEnvironmentData(enrollmentCampaign, dateFrom, dateTo, compensationType, studentCategoryList, enrollmentDirection);
    }

    @Override
    public EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(EnrollmentCampaign enrollmentCampaign, Date dateFrom, Date dateTo, CompensationType compensationType, List<StudentCategory> studentCategoryList, boolean hideEmptyDirections, List<Qualifications> qualificationList, List<DevelopForm> developFormList, List<DevelopCondition> developConditionList)
    {
        MQBuilder builder = getBaseEnrollmentDirectionBuilder(enrollmentCampaign, dateFrom, dateTo, compensationType, studentCategoryList);
        if (qualificationList != null && !qualificationList.isEmpty())
            builder.add(MQExpression.in("d", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), qualificationList));
        if (developFormList != null && !developFormList.isEmpty())
            builder.add(MQExpression.in("d", EnrollmentDirection.educationOrgUnit().developForm().s(), developFormList));
        if (developConditionList != null && !developConditionList.isEmpty())
            builder.add(MQExpression.in("d", EnrollmentDirection.educationOrgUnit().developCondition().s(), developConditionList));

        EntrantRatingTAEnvironmentNode env = new EntrantRatingTAEnvironmentNode();

        env.enrollmentCampaignTitle = enrollmentCampaign.getTitle();

        env.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        env.dateFrom = dateFrom;

        env.dateTo = dateTo;

        env.compensationTypeId = compensationType.getCode();

        if (studentCategoryList != null)
            env.studentCategoryIds = CommonBaseUtil.getPropertiesList(studentCategoryList, StudentCategory.P_CODE);

        env.hideEmptyDirections = hideEmptyDirections;

        if (qualificationList != null)
            env.qualificationIds = CommonBaseUtil.getPropertiesList(qualificationList, Qualifications.P_CODE);

        if (developFormList != null)
            env.developFormIds = CommonBaseUtil.getPropertiesList(developFormList, DevelopForm.P_CODE);

        if (developConditionList != null)
            env.developConditionIds = CommonBaseUtil.getPropertiesList(developConditionList, DevelopCondition.P_CODE);

        List<EnrollmentDirection> list = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
                .fetchPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("e"), "ou")
                .fetchPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("ou"), "hs")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .order(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle().fromAlias("e")))
                .createStatement(new DQLExecutionContext(getSession())).list();

        fillEntrantRatingTAEnvironmentNode(getSession(), enrollmentCampaign, studentCategoryList, compensationType, builder, env, list, hideEmptyDirections);

        return env;
    }

    @Override
    public EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(EnrollmentCampaign enrollmentCampaign, Date dateFrom, Date dateTo, CompensationType compensationType, List<StudentCategory> studentCategoryList, EnrollmentDirection enrollmentDirection)
    {
        MQBuilder builder = getBaseEnrollmentDirectionBuilder(enrollmentCampaign, dateFrom, dateTo, compensationType, studentCategoryList);
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirection));

        EntrantRatingTAEnvironmentNode env = new EntrantRatingTAEnvironmentNode();

        env.enrollmentCampaignTitle = enrollmentCampaign.getTitle();

        env.educationYearTitle = enrollmentCampaign.getEducationYear().getTitle();

        env.dateFrom = dateFrom;

        env.dateTo = dateTo;

        env.compensationTypeId = compensationType.getCode();

        if (studentCategoryList != null)
            env.studentCategoryIds = CommonBaseUtil.getPropertiesList(studentCategoryList, StudentCategory.P_CODE);

        env.enrollmentDirectionId = enrollmentDirection.getId().toString();

        fillEntrantRatingTAEnvironmentNode(getSession(), enrollmentCampaign, studentCategoryList, compensationType, builder, env, Arrays.asList(enrollmentDirection), false);

        return env;
    }

    /**
     * Создает билдер для EntrantDataUtil на основе обязательных параметром
     *
     * @param enrollmentCampaign  приемная кампания
     * @param dateFrom            Дата приема с
     * @param dateTo              Дата приема по
     * @param compensationType    Вид возмещения затрат
     * @param studentCategoryList Категорий обучаемых
     * @return билдер для EntrantDataUtil
     */
    private static MQBuilder getBaseEnrollmentDirectionBuilder(EnrollmentCampaign enrollmentCampaign, Date dateFrom, Date dateTo, CompensationType compensationType, List<StudentCategory> studentCategoryList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.addJoin("r", RequestedEnrollmentDirection.enrollmentDirection().s(), "d");
        builder.addJoinFetch("r", RequestedEnrollmentDirection.entrantRequest().s(), "request");
        builder.addJoinFetch("request", EntrantRequest.entrant().s(), "entrant");
        builder.addJoinFetch("entrant", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "identityCard");
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().s(), enrollmentCampaign));
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.entrantRequest().regDate().s(), dateFrom, dateTo));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.compensationType().s(), compensationType));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.entrantRequest().entrant().archival().s(), Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (studentCategoryList != null && !studentCategoryList.isEmpty())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.studentCategory().s(), studentCategoryList));
        return builder;
    }

    private static void fillEntrantRatingMap(Session session, EnrollmentCampaign enrollmentCampaign, MQBuilder builder, Map<EnrollmentDirection, List<EntrantRatingRow>> generalMap, Map<EnrollmentDirection, List<EntrantRatingRow>> targetMap)
    {
        // подготавливаем данные
        EntrantDataUtil dataUtil = new EntrantDataUtil(session, enrollmentCampaign, builder);
        Map<Person, Set<Benefit>> benefitMap = EntrantDataUtil.getBenefitMap(session, builder);

        // загружаем студентов предзачисления
        Map<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> preliminaryMap = new HashMap<>();
        for (PreliminaryEnrollmentStudent item : new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "preliminary").add(MQExpression.in("preliminary", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, builder)).<PreliminaryEnrollmentStudent>getResultList(session))
            preliminaryMap.put(item.getRequestedEnrollmentDirection(), item);

        // поехали
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            // опрделяем основной или целевой прием
            Map<EnrollmentDirection, List<EntrantRatingRow>> map = direction.isTargetAdmission() ? targetMap : generalMap;

            // получаем детализацию
            EnrollmentDirection enrollmentDirection = direction.getEnrollmentDirection();
            Person person = direction.getEntrantRequest().getEntrant().getPerson();
            PersonEduInstitution personEduInstitution = person.getPersonEduInstitution();
            Double averageMark = personEduInstitution != null ? personEduInstitution.getAverageMark() : null;

            // добавляем в список строк
            List<EntrantRatingRow> rows = map.get(enrollmentDirection);
            if (rows == null)
                map.put(enrollmentDirection, rows = new ArrayList<>());

            // добавляем строку отчета
            rows.add(new EntrantRatingRow(direction,
                    preliminaryMap.get(direction),
                    person.getIdentityCard().getFullFio(),
                    dataUtil.getMarkMap(direction),
                    dataUtil.getFinalMark(direction),
                    benefitMap.get(person),
                    averageMark)
            );
        }

        Comparator<IReportRow> comparator = new RequestedEnrollmentDirectionComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(enrollmentCampaign));

        for (List<EntrantRatingRow> list : generalMap.values())
            Collections.sort(list, comparator);

        for (List<EntrantRatingRow> list : targetMap.values())
            Collections.sort(list, comparator);
    }

    private static void fillEntrantRatingTAEnvironmentNode(Session session, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, CompensationType compensationType, MQBuilder builder, EntrantRatingTAEnvironmentNode env, List<EnrollmentDirection> directionList, boolean hideEmptyDirections)
    {
        // статистика по основному приему
        Map<EnrollmentDirection, List<EntrantRatingRow>> generalMap = new HashMap<>();

        // статистика по целевому приему
        Map<EnrollmentDirection, List<EntrantRatingRow>> targetMap = new HashMap<>();

        // считаем статистику
        fillEntrantRatingMap(session, enrollmentCampaign, builder, generalMap, targetMap);

        // получаем все не используемые виды целевого приема
        Set<Long> notUsedTargetAdmissionKindIds = new HashSet<>(DataAccessServices.dao().<Long>getList(
                new DQLSelectBuilder().fromEntity(NotUsedTargetAdmissionKind.class, "e")
                        .column(DQLExpressions.property(NotUsedTargetAdmissionKind.targetAdmissionKind().id().fromAlias("e")))
                        .where(DQLExpressions.eq(DQLExpressions.property(NotUsedTargetAdmissionKind.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
        ));

        // сохраняем все виды целевого приема
        for (TargetAdmissionKind row : DataAccessServices.dao().getList(TargetAdmissionKind.class, TargetAdmissionKind.P_CODE))
            env.targetAdmissionKind.row.add(new EntrantRatingTAEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), row.getParent() == null ? null : row.getParent().getCode(), row.getPriority(), !notUsedTargetAdmissionKindIds.contains(row.getId())));

        // получаем все не используемые виды конкурса
        Set<Long> notUsedCompetitionKind = new HashSet<>(DataAccessServices.dao().<Long>getList(
                new DQLSelectBuilder().fromEntity(EnrollmentCompetitionKind.class, "e")
                        .column(DQLExpressions.property(EnrollmentCompetitionKind.competitionKind().id().fromAlias("e")))
                        .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                        .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.used().fromAlias("e")), DQLExpressions.value(false)))
        ));

        // сохраняем все виды конкурса
        for (CompetitionKind row : DataAccessServices.dao().getList(CompetitionKind.class, CompetitionKind.P_CODE))
            env.competitionKind.row.add(new EntrantRatingTAEnvironmentNode.CompetitionKindNode.CompetitionKindRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedCompetitionKind.contains(row.getId())));

        // получаем все не используемые льготы
        Set<Long> notUsedBenefitIds = new HashSet<>(DataAccessServices.dao().<Long>getList(
                new DQLSelectBuilder().fromEntity(NotUsedBenefit.class, "e")
                .column(DQLExpressions.property(NotUsedBenefit.benefit().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(NotUsedBenefit.personRoleName().fromAlias("e")), DQLExpressions.value(Entrant.class.getSimpleName())))
        ));

        // сохраняем все льготы
        for (Benefit row : DataAccessServices.dao().getList(Benefit.class, Benefit.P_CODE))
            env.benefit.row.add(new EntrantRatingTAEnvironmentNode.BenefitNode.BenefitRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedBenefitIds.contains(row.getId())));

        // получаем все не используемые типы приказов
        Set<Long> notUsedOrderTypeIds = new HashSet<>(new DQLSelectBuilder().fromEntity(EnrollmentOrderType.class, "e")
                .column(DQLExpressions.property(EnrollmentOrderType.entrantEnrollmentOrderType().id().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderType.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentOrderType.used().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
                .createStatement(session).<Long>list());

        // сохраняем типы приказов о зачислении
        for (EntrantEnrollmentOrderType row : DataAccessServices.dao().getList(EntrantEnrollmentOrderType.class, EntrantEnrollmentOrderType.P_CODE))
            env.orderType.row.add(new EntrantRatingTAEnvironmentNode.EnrollmentOrderTypeNode.EnrollmentOrderTypeRow(row.getShortTitle(), row.getTitle(), row.getCode(), !notUsedOrderTypeIds.contains(row.getId())));

        // сохраняем все состояния документов
        env.documentState.row.add(new EntrantRatingTAEnvironmentNode.Row("Оригиналы", ORIGINAL));
        env.documentState.row.add(new EntrantRatingTAEnvironmentNode.Row("Копии", COPY));

        // сохраняем все состояния зачисления
        env.enrollmentState.row.add(new EntrantRatingTAEnvironmentNode.Row("Зачислен", ENROLLED));

        Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> disciplineMap = ExamSetUtil.getDirection2DisciplineMap(session, enrollmentCampaign, studentCategoryList, compensationType);

        // заполняем строки
        for (EnrollmentDirection direction : directionList)
        {
            // сохраняем дисциплины из набора вступительных испытаний
            List<Discipline2RealizationWayRelation> disciplineList = disciplineMap.get(direction);

            EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow directionRow = new EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow();
            directionRow.id = direction.getId().toString();

            // записываем дисциплины набора вступительных испытаний
            if (disciplineList != null)
                for (Discipline2RealizationWayRelation discipline : disciplineList)
                    directionRow.discipline.row.add(new EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.DisciplineNode.DisciplineRow(discipline.getTitle(), discipline.getShortTitle(), discipline.getId().toString()));

            // записываем строки с выбранными направлениями приема
            fillEntrantRatingREDByTA(session, directionRow.generalAdmission.row, generalMap.get(direction), disciplineList);
            fillEntrantRatingREDByTA(session, directionRow.targetAdmission.row, targetMap.get(direction), disciplineList);

            // если не скрываем пустые или есть что показывать, то показываем статистику для направления приема
            if (!hideEmptyDirections || !directionRow.generalAdmission.row.isEmpty() || !directionRow.targetAdmission.row.isEmpty())
            {
                env.enrollmentDirection.row.add(directionRow);
            }
        }
    }

    private static void fillEntrantRatingREDByTA(Session session, List<EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow> rows, List<EntrantRatingRow> ratingRowList, List<Discipline2RealizationWayRelation> disciplineList)
    {
        if (ratingRowList == null) return;

        Map<PreliminaryEnrollmentStudent, EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow> rowMap = new HashMap<>();

        for (EntrantRatingRow row : ratingRowList)
        {
            EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow item = new EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow();

            item.regNumber = row._direction.getNumber();

            item.fio = row._fio;

            if (row._direction.getTargetAdmissionKind() != null)
                item.targetAdmissionKind = row._direction.getTargetAdmissionKind().getCode();

            item.competitionKind = row._direction.getCompetitionKind().getCode();

            if (row._benefitSet != null)
            {
                item.benefitList = new ArrayList<>();
                for (Benefit benefit : row._benefitSet)
                    item.benefitList.add(benefit.getCode());
                Collections.sort(item.benefitList);
            }

            item.documentState = row._direction.isOriginalDocumentHandedIn() ? ORIGINAL : COPY;

            if (row._preStudent != null)
            {
                item.enrollmentState = ENROLLED;
                rowMap.put(row._preStudent, item);
            }

            if (row._preStudent != null)
                item.enrollmentCompensationType = row._preStudent.getCompensationType().getCode();

            if (row._averageMark != null)
                item.averageEduInstitutionMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row._averageMark);

            int experience = row._direction.getTotalProfileWorkExperienceMonths();
            if (experience > 0)
                item.profileWorkExperienceMonths = Integer.toString(experience);

            item.finalMark = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row._finalMark);

            for (Discipline2RealizationWayRelation discipline : disciplineList)
            {
                Double mark = row._markMap.get(discipline);
                if (mark == null)
                    item.marks.add("x");
                else if (mark == -1.0)
                    item.marks.add("-");
                else
                    item.marks.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
            }

            item.entrantId = row._direction.getEntrantRequest().getEntrant().getId().toString();

            rows.add(item);
        }

        for (EnrollmentExtract extract : new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e").column("e")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().fromAlias("e")), rowMap.keySet()))
                .createStatement(session).<EnrollmentExtract>list())
        {
            EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow row = rowMap.get(extract.getEntity());
            
            row.orderId = extract.getOrder().getId().toString();
            row.orderNumber = extract.getOrder().getNumber();
            row.orderType = extract.getOrder().getType().getCode();
        }
    }

    /**
     * Строка рейтинга абитуриента с доп. атрибутами, которые нужны в отчетах
     */
    private static class EntrantRatingRow implements IReportRow
    {
        private RequestedEnrollmentDirection _direction;
        private PreliminaryEnrollmentStudent _preStudent;
        private String _fio;
        private Map<Discipline2RealizationWayRelation, Double> _markMap;
        private double _finalMark;
        private Double _profileMark;
        private Set<Benefit> _benefitSet;
        private Double _averageMark;

        private EntrantRatingRow(RequestedEnrollmentDirection direction, PreliminaryEnrollmentStudent preStudent, String fio, Map<Discipline2RealizationWayRelation, Double> markMap, double finalMark, Set<Benefit> benefitSet, Double averageMark)
        {
            _direction = direction;
            _preStudent = preStudent;
            _fio = fio;
            _markMap = markMap;
            _finalMark = finalMark;
            _profileMark = direction.getProfileChosenEntranceDiscipline() == null ? null : direction.getProfileChosenEntranceDiscipline().getFinalMark();
            _benefitSet = benefitSet;
            _averageMark = averageMark;
        }

        // Implements

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _direction;
        }

        @Override
        public Double getFinalMark()
        {
            return _finalMark;
        }

        @Override
        public Double getProfileMark()
        {
            return _profileMark;
        }

        @Override
        public String getFio()
        {
            return _fio;
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE; // по ним не сравниваем
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return _averageMark;
        }
    }
}
