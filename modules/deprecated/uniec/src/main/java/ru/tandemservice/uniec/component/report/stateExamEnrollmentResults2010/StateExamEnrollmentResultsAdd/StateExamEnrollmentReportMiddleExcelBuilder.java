/* $Id: StateExamEnrollmentReportExcelBuilder.java 10599 2009-11-17 08:33:06Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * логика создания excel файла для СПО
 *
 * @author vip_delete
 * @since 24.04.2009
 */
class StateExamEnrollmentReportMiddleExcelBuilder implements IStateExamEnrollmentExcelBuilder
{
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] buildExcelContent(String highSchoolTitle,
                                    String enrollmentCampStage,
                                    String periodTitle,
                                    Model model,
                                    List<ReportRow> reportRowList) throws Exception
                                    {
        String paramsTitle = getParamsTitle(model);
        Long reportAlgorithmId = model.getReportAlgorithm().getId();
        boolean showEduGroups = model.isShowEduGroups();
        String developFormTitle = StringUtils.uncapitalize(model.getReport().getDevelopForm().getTitle());
        String compensationTypeTitle = StringUtils.uncapitalize(model.getReport().getCompensationType().getShortTitle());

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // create fonts
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial9 = new WritableFont(WritableFont.ARIAL, 9);
        WritableFont arial9bold = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
        WritableFont arial9boldGray = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
        arial9boldGray.setColour(Colour.GRAY_50);
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat rowTitleFormat = new WritableCellFormat(arial9bold);
        rowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        rowTitleFormat.setWrap(true);

        WritableCellFormat subRowTitleFormat = new WritableCellFormat(arial9);
        subRowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        subRowTitleFormat.setWrap(true);
        subRowTitleFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat subRowTitleFormatGray = new WritableCellFormat(arial9boldGray);
        subRowTitleFormatGray.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        subRowTitleFormatGray.setWrap(true);
        subRowTitleFormatGray.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat subSubRowTitleFormat = new WritableCellFormat(arial8);
        subSubRowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        subSubRowTitleFormat.setWrap(true);
        subSubRowTitleFormat.setVerticalAlignment(VerticalAlignment.TOP);

        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        headerFormat.setAlignment(Alignment.CENTRE);
        headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        // create list
        WritableSheet sheet = workbook.createSheet("Отчет", 0);

        // впихиваем отчет на лист A4 альбомного формата
        sheet.setColumnView(0, 25);
        sheet.setColumnView(1, 10);
        sheet.setColumnView(2, 9);
        sheet.setColumnView(3, 9);
        sheet.setColumnView(4, 16);
        for (int i = 0; i < 21; i++)
            sheet.setColumnView(i + 5, 6);
        sheet.setPageSetup(PageOrientation.LANDSCAPE);
        sheet.getSettings().setLeftMargin(0.19);
        sheet.getSettings().setRightMargin(0.19);
        sheet.getSettings().setTopMargin(0.38);
        sheet.getSettings().setBottomMargin(0.38);
        sheet.getSettings().setScaleFactor(75);

        // row 1
        sheet.addCell(new Label(1, 0, highSchoolTitle, new WritableCellFormat(arial10bold)));

        // row 2
        sheet.addCell(new Label(1, 1, "Сведения по результатам " + (Model.REPORT_ALGORITHM_EXAM == reportAlgorithmId ? "экзаменов" : "ЕГЭ") + " " + enrollmentCampStage + " (для программ СПО) (по заявлениям " + periodTitle + ")", new WritableCellFormat(arial10bold)));

        // row 4
        sheet.addCell(new Label(0, 3, paramsTitle, new WritableCellFormat(arial8)));

        // write table by columns at this coordinates
        final int TABLE_START_ROW_INDEX = 4;
        final int TABLE_START_COLUMN_INDEX = 0;

        // usefull indexes
        int row;
        int column;

        // write table header
        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX;

            sheet.addCell(new Label(column, row, "Специальность по ОКСО", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            column++;

            sheet.addCell(new Label(column, row, "Код", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            column++;

            sheet.addCell(new Label(column, row, "Форма обучения", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            column++;

            sheet.addCell(new Label(column, row, "Форма финансирования", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            column++;

            sheet.addCell(new Label(column, row, "Предмет ЕГЭ", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            column++;

            sheet.addCell(new Label(column, row, "Всего   чел.", headerFormat));
            sheet.mergeCells(column, row, column, row + 1);
            column++;

            sheet.addCell(new Label(column, row, "Количество абитуриентов с результатами " + (reportAlgorithmId == Model.REPORT_ALGORITHM_EXAM ? "экзаменов" : "единого государственного экзамена") + ", распределенное по интервалам тестовых баллов (чел)", headerFormat));
            sheet.mergeCells(column, row, column + 19, row);
            for (int i = 0; i < 20; i++)
                sheet.addCell(new Label(column + i, row + 1, i == 0 ? "0-5" : (i * 5 + 1) + "-" + ((i + 1) * 5), headerFormat));
        }

        // write table data
        {
            row = TABLE_START_ROW_INDEX + 2;
            column = TABLE_START_COLUMN_INDEX;

            for (ReportRow reportRow : reportRowList)
            {
                if (model.isGroupByBranchAndRepresentation())
                {
                    // рисуем row
                    sheet.addCell(new Label(column, row, reportRow.getTitle(), rowTitleFormat));
                    sheet.mergeCells(column, row, column + 4, row);
                    for (int i = 0; i < 22; i++)
                        sheet.addCell(new Label(column + i + 4, row, "", rowTitleFormat));
                    row++;
                }

                for (Map.Entry<EducationLevels, Set<ReportSubRow>> entry : reportRow.getSubRowMap().entrySet())
                {
                    if (showEduGroups)
                    {
                        // рисуем укрупненную группу
                        String code = entry.getKey().getTitleCodePrefix();
                        String title = entry.getKey().getTitle();

                        sheet.addCell(new Label(column, row, title, subRowTitleFormatGray));
                        sheet.addCell(new Label(column + 1, row, code, subRowTitleFormatGray));
                        sheet.addCell(new Label(column + 2, row, developFormTitle, subRowTitleFormatGray));
                        sheet.addCell(new Label(column + 3, row, compensationTypeTitle, subRowTitleFormatGray));

                        int eduGroupRowIndex = row;
                        for (int i = 0; i < 22; i++)
                            sheet.addCell(new Label(column + i + 4, row, "", rowTitleFormat));
                        row++;

                        // определяем предметы
                        Map<Long, ReportSubSubRow> eduGroupMap = new HashMap<>();
                        for (ReportSubRow subRow : entry.getValue())
                        {
                            for (ReportSubSubRow key : subRow.getSubSubRowList())
                            {
                                ReportSubSubRow eduGroupRow = eduGroupMap.get(key.getId());
                                if (eduGroupRow == null)
                                    eduGroupMap.put(key.getId(), eduGroupRow = new ReportSubSubRow(key.getId(), key.getTitle(), new HashMap<Entrant, Integer>()));
                                if (key.getMarks() != null)
                                    eduGroupRow.getMarks().putAll(key.getMarks());
                            }
                        }
                        List<ReportSubSubRow> eduGroupRows = new ArrayList<>();
                        eduGroupRows.addAll(eduGroupMap.values());
                        Collections.sort(eduGroupRows, ITitled.TITLED_COMPARATOR);

                        int rowSize = eduGroupMap.size();
                        sheet.mergeCells(column, eduGroupRowIndex, column, eduGroupRowIndex + rowSize);
                        sheet.mergeCells(column + 1, eduGroupRowIndex, column + 1, eduGroupRowIndex + rowSize);
                        sheet.mergeCells(column + 2, eduGroupRowIndex, column + 2, eduGroupRowIndex + rowSize);
                        sheet.mergeCells(column + 3, eduGroupRowIndex, column + 3, eduGroupRowIndex + rowSize);

                        // рисуем предметы укрупненной группы
                        Set<Long> eduGroupEntrantSet = new HashSet<>();
                        for (ReportSubSubRow eduGroupSubject : eduGroupRows)
                        {
                            sheet.addCell(new Label(column + 4, row, eduGroupSubject.getTitle(), subRowTitleFormatGray));

                            int[] data = new int[20]; //0-5, 6-10, и тд
                            int sum = 0;
                            for (Map.Entry<Entrant, Integer> mark : eduGroupSubject.getMarks().entrySet())
                            {
                                eduGroupEntrantSet.add(mark.getKey().getId());
                                int m = mark.getValue();
                                sum++;
                                int index = m == 0 ? 0 : (m - 1) / 5;
                                if (index < data.length)
                                    data[index]++;
                            }
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + 5, row, sum, subRowTitleFormatGray));
                            for (int i = 0; i < 20; i++)
                                sheet.addCell(JExcelUtil.getNumberNullableCell(column + i + 6, row, data[i], subRowTitleFormatGray));
                            row++;
                        }
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 5, eduGroupRowIndex, eduGroupEntrantSet.size(), subRowTitleFormatGray));
                    }

                    // рисуем все subRow
                    for (ReportSubRow subRow : entry.getValue())
                    {
                        sheet.addCell(new Label(column, row, subRow.getTitleWithoutCode(), subRowTitleFormat));
                        sheet.addCell(new Label(column + 1, row, subRow.getCode(), subRowTitleFormat));
                        sheet.addCell(new Label(column + 2, row, developFormTitle, subRowTitleFormat));
                        sheet.addCell(new Label(column + 3, row, compensationTypeTitle, subRowTitleFormat));

                        // fill cells
                        int subRowIndex = row;
                        for (int i = 0; i < 22; i++)
                            sheet.addCell(new Label(column + i + 4, row, "", rowTitleFormat));
                        row++;

                        int rowSize = subRow.getSubSubRowList().size();
                        sheet.mergeCells(column, subRowIndex, column, subRowIndex + rowSize);
                        sheet.mergeCells(column + 1, subRowIndex, column + 1, subRowIndex + rowSize);
                        sheet.mergeCells(column + 2, subRowIndex, column + 2, subRowIndex + rowSize);
                        sheet.mergeCells(column + 3, subRowIndex, column + 3, subRowIndex + rowSize);

                        // рисуем все предметы из набора экзаменов
                        Set<Entrant> entrantSet = new HashSet<>();
                        for (ReportSubSubRow subSubRow : subRow.getSubSubRowList())
                        {
                            sheet.addCell(new Label(column + 4, row, subSubRow.getTitle(), subSubRowTitleFormat));
                            int[] data = new int[20]; //0-5, 6-10, и тд
                            int sum = 0;
                            if (subSubRow.getMarks() != null)
                                for (Map.Entry<Entrant, Integer> mark : subSubRow.getMarks().entrySet())
                                {
                                    entrantSet.add(mark.getKey());
                                    int m = mark.getValue();
                                    sum++;
                                    int index = m == 0 ? 0 : (m - 1) / 5;
                                    if (index < data.length)
                                        data[index]++;
                                }
                            sheet.addCell(JExcelUtil.getNumberNullableCell(column + 5, row, sum, subSubRowTitleFormat));
                            for (int i = 0; i < 20; i++)
                                sheet.addCell(JExcelUtil.getNumberNullableCell(column + i + 6, row, data[i], subSubRowTitleFormat));
                            row++;
                        }
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 5, subRowIndex, entrantSet.size(), subSubRowTitleFormat));
                    }
                }
            }
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
                                    }

    private String getParamsTitle(Model model)
    {
        // получаем строку параметров отчета
        List<String> paramsTitleList = new ArrayList<>();
        if (model.isDevelopConditionActive())
            paramsTitleList.add("Условие обучения: " + UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));
        if (model.isQualificationActive())
            paramsTitleList.add("Квалификация: " + UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, ", "));
        if (model.isStudentCategoryActive())
            paramsTitleList.add("Категория поступающего: " + UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, ", "));
        if (model.isFormativeOrgUnitActive())
            paramsTitleList.add("Формирующее подр.: " + UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_TYPE_TITLE, ", "));
        if (model.isTerritorialOrgUnitActive())
            paramsTitleList.add("Территориальное подр.: " + UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TYPE_TITLE, ", "));
        return StringUtils.join(paramsTitleList, "  ");
    }
}
