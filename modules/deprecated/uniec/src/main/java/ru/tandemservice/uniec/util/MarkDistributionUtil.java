/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author vip_delete
 * @since 01.07.2009
 */
public class MarkDistributionUtil
{
    /**
     * Находит самое подходящее распределение оценок по вступительным испытаниям
     *
     * @param chosenDisciplines выбранные вступительные испытания, которыми надо покрыть набор вступительных испытаний
     * @param structureItemList представление набора вступительных испытаний.
     *                          Набор экзамена разбивается на строки - вступительные испытания
     *                          Каждое вступительное испытание разбирается на список дисциплин приемной кампании (раскрываются все группы дисциплин и дисциплины по выбору)
     *                          entranceDisciplineRow - это массив таких списков. количество элементов = количество вступительных испытаний
     * @return выбранные вступительные испытания том порядке, в котором они покрывают набор вступительных испытаний
     *         размер возвращенного массива всегда равен structureItemList.site()
     *         на месте вступительного испытания, которое нельзя покрыть выбранным вступительным испытанием стоит null
     */
    public static ChosenEntranceDiscipline[] getChosenDistribution(Set<ChosenEntranceDiscipline> chosenDisciplines, List<ExamSetStructureItem> structureItemList)
    {
        if (null == structureItemList)
            structureItemList = Collections.emptyList();
        int size = structureItemList.size();
        ChosenEntranceDiscipline[] result = new ChosenEntranceDiscipline[size];
        Object[][] permutations = getAllPermutations(chosenDisciplines.toArray(), size);
        int maxCoverCount = -1;
        int maxCoverSum = -1;
        for (Object[] perm : permutations)
        {
            int coverCount = 0;
            int coverSum = 0;
            ChosenEntranceDiscipline[] coverPermut = new ChosenEntranceDiscipline[size];

            for (int j = 0; j < size; j++)
            {
                ChosenEntranceDiscipline chosen = (ChosenEntranceDiscipline) perm[j];
                Set<Discipline2RealizationWayRelation> list = structureItemList.get(j).getDisciplineSet();
                if (chosen != null && list.contains(chosen.getEnrollmentCampaignDiscipline()))
                {
                    coverCount++;
                    coverPermut[j] = chosen;
                    if (chosen.getFinalMark() != null && chosen.getFinalMark() > 0) coverSum += chosen.getFinalMark();
                }
            }

            if (coverCount > maxCoverCount || (coverCount == maxCoverCount && coverSum > maxCoverSum))
            {
                result = coverPermut;
                maxCoverCount = coverCount;
                maxCoverSum = coverSum;
            }
        }

        return result;
    }

    /**
     * Находит самое подходящее распределение оценок по вступительным испытаниям
     *
     * @param chosenDisciplines     выбранные вступительные испытания, которыми надо покрыть набор вступительных испытаний
     * @param entranceDisciplineRow - представление набора вступительных испытаний.
     *                              Набор экзамена разбивается на строки - вступительные испытания
     *                              Каждое вступительное испытание разбирается на список дисциплин приемной кампании (раскрываются все группы дисциплин и дисциплины по выбору)
     *                              entranceDisciplineRow - это массив таких списков. количество элементов = количество вступительных испытаний
     * @return выбранные вступительные испытания том порядке, в котором они покрывают набор вступительных испытаний
     *         размер возвращенного массива всегда равен entranceDisciplineRow.length
     *         на месте вступительного испытания, которое нельзя покрыть выбранным вступительным испытанием стоит null
     * @deprecated use getChosenDistribution
     */
    @Deprecated
    public static ChosenEntranceDiscipline[] getNearestDistribution(Set<ChosenEntranceDiscipline> chosenDisciplines, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> entranceDisciplineRow)
    {
        if (null == entranceDisciplineRow)
            entranceDisciplineRow = Collections.emptyList();
        int size = entranceDisciplineRow.size();
        ChosenEntranceDiscipline[] result = new ChosenEntranceDiscipline[size];
        Object[][] permutations = getAllPermutations(chosenDisciplines.toArray(), size);
        int maxCoverCount = -1;
        int maxCoverSum = -1;
        for (Object[] perm : permutations)
        {
            int coverCount = 0;
            int coverSum = 0;
            ChosenEntranceDiscipline[] coverPermut = new ChosenEntranceDiscipline[size];

            for (int j = 0; j < size; j++)
            {
                ChosenEntranceDiscipline chosen = (ChosenEntranceDiscipline) perm[j];
                Set<Discipline2RealizationWayRelation> list = entranceDisciplineRow.get(j).getSecond();
                if (chosen != null && list.contains(chosen.getEnrollmentCampaignDiscipline()))
                {
                    coverCount++;
                    coverPermut[j] = chosen;
                    if (chosen.getFinalMark() != null && chosen.getFinalMark() > 0) coverSum += chosen.getFinalMark();
                }
            }

            if (coverCount > maxCoverCount || (coverCount == maxCoverCount && coverSum > maxCoverSum))
            {
                result = coverPermut;
                maxCoverCount = coverCount;
                maxCoverSum = coverSum;
            }
        }

        return result;
    }

    /**
     * Генерирует все перестановки для массива объектов values длины size
     * размер values может быть как больше (тогда будут null-значения на пустых местах),
     * так и меньше size (тогда в перестановку, естественно, входят не все values)
     * получившиеся перестановки всегда уникальны и всегда длины size
     *
     * @param values массив объектов
     * @param size   размер результирующей перестановки
     * @return массив всех перестановок размером size
     */
    private static Object[][] getAllPermutations(Object[] values, int size)
    {
        int length = Math.max(values.length, size);
        int[][] indexPermutation = getAllIndexPermutation(length);

        Object[][] data = new Object[indexPermutation.length][size];
        for (int i = 0; i < indexPermutation.length; i++)
            for (int j = 0; j < size; j++)
            {
                int k = indexPermutation[i][j];
                if (k < values.length)
                    data[i][j] = values[k];
                else
                    data[i][j] = null;
            }

        // если размеры равны, то были сгенерированы все n! различных перестановок
        if (values.length == size) return data;

        // исключим повторяющиеся перестановки
        List<Object[]> result = new ArrayList<>();
        Set<MultiKey> used = new HashSet<>();
        for (Object[] row : data)
        {
            MultiKey key = new MultiKey(row);
            if (used.add(key))
                result.add(row);
        }
        return result.toArray(new Object[result.size()][]);
    }

    private static final Map<Integer, int[][]> index2perms = new ConcurrentHashMap<>();

    /**
     * Генерирует все перестановки без повторений длины n для вектора (0,1,...,n)
     *
     * @param n длина вектора
     * @return массив всех перестановок
     */
    private static int[][] getAllIndexPermutation(int n)
    {
        int[][] perms = index2perms.get(n);
        if (perms == null)
        {
            List<int[]> values = new ArrayList<>();

            int[] p = new int[n];
            for (int i = 0; i < n; i++) p[i] = i;
            int i, j, k, t;
            while (true)
            {
                values.add(p.clone());

                i = n - 2;
                while (i >= 0 && p[i] > p[i + 1]) i--;
                if (i < 0) break;
                t = p[i];
                j = n - 1;
                while (t > p[j]) j--;

                p[i] = p[j];
                p[j] = t;
                j = n - 1;
                k = i + 1;
                while (j > k)
                {
                    t = p[j];
                    p[j] = p[k];
                    p[k] = t;
                    j--;
                    k++;
                }
            }

            index2perms.put(n, perms = values.toArray(new int[values.size()][]));
        }
        return perms;
    }

    public static void main(String[] args)
    {
//        int[][] a;
//
//        System.out.println("Permitation N = 2");
//        a = getAllIndexPermutation(2);
//        for (int i = 0; i < a.length; i++) { for (int j = 0; j < a[i].length; j++) System.out.print(a[i][j]); System.out.println(); }
//        System.out.println("Permitation N = 3");
//        a = getAllIndexPermutation(3);
//        for (int i = 0; i < a.length; i++) { for (int j = 0; j < a[i].length; j++) System.out.print(a[i][j]); System.out.println(); }
//        System.out.println("Permitation N = 4");
//        a = getAllIndexPermutation(6);
//        for (int i = 0; i < a.length; i++) { for (int j = 0; j < a[i].length; j++) System.out.print(a[i][j]); System.out.println(); }

        System.out.println("Permitation N = 3");
        Object[][] a = getAllPermutations(new Object[]{'a', 4, '*'}, 4);
        for (Object[] anA : a)
        {
            for (Object anAnA : anA) System.out.print(anAnA);
            System.out.println();
        }
    }
}
