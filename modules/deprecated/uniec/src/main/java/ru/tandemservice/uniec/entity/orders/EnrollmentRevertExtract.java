package ru.tandemservice.uniec.entity.orders;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.utils.ICommentableExtract;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.dao.IGeneratedCommentForEntrantOrders;
import ru.tandemservice.uniec.dao.IPrintableEnrollmentExtract;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.gen.EnrollmentRevertExtractGen;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Выписка из приказа об изменении приказа о зачислении
 */
public class EnrollmentRevertExtract extends EnrollmentRevertExtractGen implements IPrintableEnrollmentExtract<RequestedEnrollmentDirection, EnrollmentRevertParagraph>, ICommentableExtract
{
    @Override
    public String getTitle()
    {
        return "Выписка «" + getType().getTitle() + "» №" + getNumber() + "/" + getParagraph().getNumber() + " из приказа №" + getOrder().getNumber() + " | " + getEntity().getEntrantRequest().getEntrant().getPerson().getFullFio();
    }

    @Override
    public EntrantEnrollmentOrderType getType()
    {
        return getOrder().getType();
    }

    @Override
    public void setType(ICatalogItem type)
    {
        throw new RuntimeException("You cannot set extractType");
    }

    @Override
    public void setState(ICatalogItem state)
    {
        super.setState((ExtractStates) state);
    }

    @Override
    public boolean canBePrinted()
    {
        return true;
    }

    @Override
    public void doExtractPrint(boolean printPdf)
    {
        if (!canBePrinted())
            throw new ApplicationException("Выписка не может быть распечатана.");
        EcOrderManager.instance().dao().getDownloadExtractPrintForm(getId(), printPdf);
    }

    @Override
    public void doOrderPrint(boolean printPdf)
    {
        if (!canBePrinted())
            throw new ApplicationException("Приказ не может быть распечатан.");
        EcOrderManager.instance().dao().getDownloadPrintForm(getOrder().getId(), printPdf);
    }

    private void updateExtInfo(EnrollmentExtract fromExtract)
    {
        // Синхронизаируем данные
        final PreliminaryEnrollmentStudent preStudent = fromExtract.getEntity();
        setEntity(preStudent.getRequestedEnrollmentDirection());
        setCompensationType(preStudent.getCompensationType());
        setEducationOrgUnit(preStudent.getEducationOrgUnit());
        setStudentCategory(preStudent.getStudentCategory());
        setCourse(fromExtract.getCourse());
    }

    @Override
    public void setCanceledExtract(EnrollmentExtract canceledExtract)
    {
        if (canceledExtract != null && !canceledExtract.equals(getCanceledExtract()))
            updateExtInfo(canceledExtract);
        else if (canceledExtract == null && getCanceledExtract() != null)
            updateExtInfo(getCanceledExtract());

        super.setCanceledExtract(canceledExtract);
    }

    @Override
    public String getPrintScriptCatalogCode()
    {
        return UniecDefines.SCRIPT_ENROLLMENT_REVERT_EXTRACT;
    }

    @Override
    public EnrollmentOrder getOrder()
    {
        return (EnrollmentOrder) getParagraph().getOrder();
    }

    // Генерируемое примечание приказа
    public String getGeneratedComment(boolean isPrintOrderData)
    {
        final String methodName = "getComment";
        try
        {
            Method method = IGeneratedCommentForEntrantOrders.instance.get().getClass().getMethod(methodName, new Class[]{this.getClass(), boolean.class});
            IGeneratedCommentForEntrantOrders obj = IGeneratedCommentForEntrantOrders.instance.get();
            return (String) method.invoke(obj, this, isPrintOrderData);
        }
        catch (NoSuchMethodException e1)
        {
            String orderStr = getType().getTitle();
            if (isPrintOrderData && null != this.getParagraph() && null != this.getParagraph().getOrder())
            {
                AbstractEntrantOrder order = this.getParagraph().getOrder();
                if (null != order.getNumber())
                    orderStr += ", приказ № " + order.getNumber() + (order.getCommitDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) + " года." : "");
                else if (order.getCommitDate() != null)
                    orderStr += ", приказ от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate());
            }
            return orderStr;
        }
        catch (InvocationTargetException | IllegalAccessException e2)
        {
            throw new RuntimeException(e2);
        }
    }
}