package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ParentDistribution;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO;
import ru.tandemservice.uniec.dao.ecg.IEnrollmentCompetitionGroupDAO.IEntrantRateRow;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribRelationGen;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantRequestGen;
import ru.tandemservice.uniec.entity.entrant.gen.PreliminaryEnrollmentStudentGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

import java.util.*;


/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.DAO implements IDAO {

    @Override
    public void prepare(final ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.Model model) {
        super.prepare(model);
        if (null == model.getDistrib().getParent()) {
            throw new ApplicationException("Данная операция допустима только для уточняющих распределений.");
        }
        model.setFiltered(true);
    }

    @Override
    protected List<IEntrantRateRow> getEntrants4Add(final ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.Model model) {
        if (null == model.getDistrib().getParent()) {
            throw new ApplicationException("Данная операция допустима только для уточняющих распределений.");
        }
        final MQBuilder builder = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "rel", new String[] { EcgDistribRelationGen.L_ENTRANT_DIRECTION+"."+RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST+"."+EntrantRequestGen.L_ENTRANT+".id" });
        builder.add(MQExpression.eq("rel", EcgDistribRelationGen.L_QUOTA+"."+EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
        final Set<Long> usedEntrantIds = new HashSet<>(builder.<Long>getResultList(getSession()));

        final List<IEntrantRateRow> entrantRateRows = IEnrollmentCompetitionGroupDAO.INSTANCE.get().getEntrantRateRows(model.getDistrib().getParent());
        CollectionUtils.filter(entrantRateRows, object -> {
            /* только тех, кто пришес оригиналы документов (ну и уже не включен) */
            final IEntrantRateRow entrantRateRow = (IEntrantRateRow)object;
            return entrantRateRow.getOriginalDocumentHandedInPriority() != Integer.MAX_VALUE && !usedEntrantIds.contains(entrantRateRow.getEntrant().getId());
        });

        return entrantRateRows;
    }

    @Override
    protected void postProcessRows(final EcgDistribObject distrib, final List<ModelBase.Row> entrantRateRows, final Map<Long, Integer> dir2quotaMap) {
        final Map<Long, ModelBase.Row> entrant2row = new HashMap<>();
        for (final ModelBase.Row row: entrantRateRows) {
            if (null != entrant2row.put(row.getRow().getEntrant().getId(), row)) {
                throw new IllegalArgumentException("У абитуриента «"+row.getRow().getEntrant().getPerson().getFullFio()+"» указано несколько направлений приема");
            }
            row.setChecked(true);
        }

        BatchUtils.execute(entrant2row.keySet(), DQL.MAX_VALUES_ROW_NUMBER, entrantIds -> {
            final MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudentGen.ENTITY_CLASS, "s");
            builder.add(MQExpression.eq("s", PreliminaryEnrollmentStudentGen.L_STUDENT_CATEGORY+".code", UniDefines.STUDENT_CATEGORY_STUDENT));
            builder.addJoinFetch("s", PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION, "dir");
            builder.addJoinFetch("dir", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST, "req");
            builder.addJoinFetch("req", EntrantRequestGen.L_ENTRANT, "e");
            builder.add(MQExpression.in("e", "id", entrantIds));

            for (final PreliminaryEnrollmentStudent s: builder.<PreliminaryEnrollmentStudent>getResultList(getSession())) {
                final RequestedEnrollmentDirection reqDirection = s.getRequestedEnrollmentDirection();
                final ModelBase.Row row = entrant2row.get(reqDirection.getEntrantRequest().getEntrant().getId());
                if (null != row /*абитуриент должен быть*/) {
                    if (distrib.getCompetitionGroup().equals(reqDirection.getEnrollmentDirection().getCompetitionGroup()) /*направление должно быть из этой группы*/) {

                        RequestedEnrollmentDirection enrolledReqDirection;
                        if (!s.getEducationOrgUnit().equals(reqDirection.getEnrollmentDirection().getEducationOrgUnit())) {
                            enrolledReqDirection = (RequestedEnrollmentDirection) CollectionUtils.find(
                                    row.getDirections(),
                                    object -> ((RequestedEnrollmentDirection)object).getEnrollmentDirection().getEducationOrgUnit().equals(s.getEducationOrgUnit())
                            );
                            if (null == enrolledReqDirection) {
                                throw new ApplicationException(
                                        "Вы не можете добавить абитуриентов в уточняющее распределение, поскольку абитуриент «"+reqDirection.getEntrantRequest().getEntrant().getPerson().getFullFio()+
                                        "» был предварительно зачислен со сменой направления приема с «"+reqDirection.getEnrollmentDirection().getTitle()+"» на «"+s.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle()+"» (которого у него нет в распределении)"
                                );
                            }
                        } else {
                            enrolledReqDirection = reqDirection;
                        }

                        row.setDirection(enrolledReqDirection);
                        row.setPreEnrolled(true);
                    }
                }
            }
        });


        Collections.sort(entrantRateRows, (o1, o2) ->- Boolean.compare(o1.isPreEnrolled(), o2.isPreEnrolled()));



        //
        //		final Map<Long, ModelBase.Row> entrant2row = new HashMap<Long, ModelBase.Row>();
        //		for (final ModelBase.Row row: entrantRateRows) {
        //			if (null != entrant2row.put(row.getRow().getEntrant().getId(), row)) {
        //				throw new IllegalArgumentException("У абитуриента «"+row.getRow().getEntrant().getPerson().getFullFio()+"» указано несколько направлений приема");
        //			}
        //			row.setChecked(true);
        //		}
        //
        //		final StringBuilder entrants = new StringBuilder();
        //		BatchUtils.execute(entrant2row.keySet(), 400, new BatchUtils.Action<Long>() {
        //			@Override public void execute(final Collection<Long> entrantIds) {
        //				final MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudentGen.ENTITY_CLASS, "s");
        //				builder.add(MQExpression.eq("s", PreliminaryEnrollmentStudentGen.L_STUDENT_CATEGORY+".code", UniDefines.STUDENT_CATEGORY_STUDENT));
        //				builder.addJoinFetch("s", PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION, "dir");
        //				builder.addJoinFetch("dir", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST, "req");
        //				builder.addJoinFetch("req", EntrantRequestGen.L_ENTRANT, "e");
        //				builder.add(MQExpression.in("e", "id", entrantIds));
        //
        //				for (final PreliminaryEnrollmentStudent s: builder.<PreliminaryEnrollmentStudent>getResultList(getSession())) {
        //					if (entrants.length() > 0) {
        //						entrants.append(", ");
        //					}
        //					entrants.append(s.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio());
        //					entrants.append(" (").append(s.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle()).append(")");
        //				}
        //			}
        //		});
        //
        //		if (entrants.length() > 0) {
        //			throw new ApplicationException("Вы не можете добавлять студентов в уточняющее распределение. В распределении есть предзачисленные абитуриенты ("+entrants.toString()+")");
        //		}
    }

    @Override
    protected void save(final ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.Model model, final Map<Entrant, RequestedEnrollmentDirection> directions) {
        final Session session = this.getSession();

        /* для начала перезаполняем квоту (по факту) */ {
            final Map<EnrollmentDirection, MutableInt> countMap = SafeMap.get(key -> new MutableInt());

            final MQBuilder builder = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "rel", new String[] { EcgDistribRelationGen.L_ENTRANT_DIRECTION });
            builder.add(MQExpression.eq("rel", EcgDistribRelationGen.L_QUOTA+"."+EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));

            for (final RequestedEnrollmentDirection dir: builder.<RequestedEnrollmentDirection>getResultList(session)) {
                countMap.get(dir.getEnrollmentDirection()).increment();

            }
            for (final RequestedEnrollmentDirection dir: directions.values()) {
                countMap.get(dir.getEnrollmentDirection()).increment();
            }

            final Map<EnrollmentDirection, EcgDistribQuota> quotaMap = SafeMap.get(key -> {
                final EcgDistribQuota q = new EcgDistribQuota();
                q.setDirection(key);
                q.setDistribution(model.getDistrib());
                return q;
            });


            final MQBuilder qb = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q");
            qb.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, model.getDistrib()));
            final List<EcgDistribQuota> quotas = qb.getResultList(session);
            for (final EcgDistribQuota quota: quotas) {
                quotaMap.put(quota.getDirection(), quota);
            }

            for (final Map.Entry<EnrollmentDirection, EcgDistribQuota> e: quotaMap.entrySet()) {
                final EcgDistribQuota quota = e.getValue();
                final MutableInt removed = countMap.remove(e.getKey());
                quota.setQuota(null == removed ? 0 : removed.intValue());
                getSession().saveOrUpdate(quota);
            }
        }

        super.save(model, directions);
    }

}
