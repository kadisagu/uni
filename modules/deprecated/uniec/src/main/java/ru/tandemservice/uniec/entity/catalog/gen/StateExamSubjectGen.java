package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Предмет ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamSubjectGen extends EntityBase
 implements INaturalIdentifiable<StateExamSubjectGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.StateExamSubject";
    public static final String ENTITY_NAME = "stateExamSubject";
    public static final int VERSION_HASH = -531966787;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SUBJECT_C_T_M_O_CODE = "subjectCTMOCode";
    public static final String P_SUBJECT_F_I_S_CODE = "subjectFISCode";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _subjectCTMOCode;     // Код предмета по ЦТМО
    private String _subjectFISCode;     // Код предмета по ФИС
    private String _shortTitle;     // Сокращенное название
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Код предмета по ЦТМО. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getSubjectCTMOCode()
    {
        return _subjectCTMOCode;
    }

    /**
     * @param subjectCTMOCode Код предмета по ЦТМО. Свойство не может быть null и должно быть уникальным.
     */
    public void setSubjectCTMOCode(String subjectCTMOCode)
    {
        dirty(_subjectCTMOCode, subjectCTMOCode);
        _subjectCTMOCode = subjectCTMOCode;
    }

    /**
     * @return Код предмета по ФИС. Свойство должно быть уникальным.
     */
    @Length(max=255)
    public String getSubjectFISCode()
    {
        return _subjectFISCode;
    }

    /**
     * @param subjectFISCode Код предмета по ФИС. Свойство должно быть уникальным.
     */
    public void setSubjectFISCode(String subjectFISCode)
    {
        dirty(_subjectFISCode, subjectFISCode);
        _subjectFISCode = subjectFISCode;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamSubjectGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StateExamSubject)another).getCode());
            }
            setSubjectCTMOCode(((StateExamSubject)another).getSubjectCTMOCode());
            setSubjectFISCode(((StateExamSubject)another).getSubjectFISCode());
            setShortTitle(((StateExamSubject)another).getShortTitle());
            setTitle(((StateExamSubject)another).getTitle());
        }
    }

    public INaturalId<StateExamSubjectGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StateExamSubjectGen>
    {
        private static final String PROXY_NAME = "StateExamSubjectNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StateExamSubjectGen.NaturalId) ) return false;

            StateExamSubjectGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamSubjectGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamSubject.class;
        }

        public T newInstance()
        {
            return (T) new StateExamSubject();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "subjectCTMOCode":
                    return obj.getSubjectCTMOCode();
                case "subjectFISCode":
                    return obj.getSubjectFISCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "subjectCTMOCode":
                    obj.setSubjectCTMOCode((String) value);
                    return;
                case "subjectFISCode":
                    obj.setSubjectFISCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "subjectCTMOCode":
                        return true;
                case "subjectFISCode":
                        return true;
                case "shortTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "subjectCTMOCode":
                    return true;
                case "subjectFISCode":
                    return true;
                case "shortTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "subjectCTMOCode":
                    return String.class;
                case "subjectFISCode":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamSubject> _dslPath = new Path<StateExamSubject>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamSubject");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Код предмета по ЦТМО. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getSubjectCTMOCode()
     */
    public static PropertyPath<String> subjectCTMOCode()
    {
        return _dslPath.subjectCTMOCode();
    }

    /**
     * @return Код предмета по ФИС. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getSubjectFISCode()
     */
    public static PropertyPath<String> subjectFISCode()
    {
        return _dslPath.subjectFISCode();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StateExamSubject> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _subjectCTMOCode;
        private PropertyPath<String> _subjectFISCode;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StateExamSubjectGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Код предмета по ЦТМО. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getSubjectCTMOCode()
     */
        public PropertyPath<String> subjectCTMOCode()
        {
            if(_subjectCTMOCode == null )
                _subjectCTMOCode = new PropertyPath<String>(StateExamSubjectGen.P_SUBJECT_C_T_M_O_CODE, this);
            return _subjectCTMOCode;
        }

    /**
     * @return Код предмета по ФИС. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getSubjectFISCode()
     */
        public PropertyPath<String> subjectFISCode()
        {
            if(_subjectFISCode == null )
                _subjectFISCode = new PropertyPath<String>(StateExamSubjectGen.P_SUBJECT_F_I_S_CODE, this);
            return _subjectFISCode;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(StateExamSubjectGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.StateExamSubject#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StateExamSubjectGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StateExamSubject.class;
        }

        public String getEntityName()
        {
            return "stateExamSubject";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
