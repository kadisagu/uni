/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.util.IEcgpRecommendedDTO;
import ru.tandemservice.uniec.base.entity.ecgp.EcgpEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

/**
 * @author Vasily Zhukov
 * @since 10.05.2012
 */
@Configuration
public class EcProfileDistributionPub extends BusinessComponentManager
{
    public static final String TAB_PANEL = "tabPanel";
    public static final String TAB_PANEL_PUB = "tabPanelPub";
    public static final String TAB_PANEL_RECOMMENDATION = "tabPanelRecommendation";

    public static final String DIRECTION_DS = "directionDS";
    public static final String RECOMMENDED_DS = "recommendedDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DIRECTION_DS, directionDS(), directionDSHandler()))
                .addDataSource(searchListDS(RECOMMENDED_DS, recommendedDS(), recommendedDSHandler()))
                .create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(htmlTab(TAB_PANEL_PUB, "Pub").create())
                .addTab(htmlTab(TAB_PANEL_RECOMMENDATION, "Recommendation").create())
                .create();
    }

    @Bean
    public ColumnListExtPoint directionDS()
    {
        return columnListExtPointBuilder(DIRECTION_DS)
                .addColumn(textColumn("code", ProfileEducationOrgUnit.educationOrgUnit().educationLevelHighSchool().shortTitle()))
                .addColumn(textColumn("title", ProfileEducationOrgUnit.educationOrgUnit().title()))
                .addColumn(textColumn("formativeOrgUnit", ProfileEducationOrgUnit.educationOrgUnit().formativeOrgUnit().title()))
                .addColumn(textColumn("territorialOrgUnit", ProfileEducationOrgUnit.educationOrgUnit().territorialOrgUnit().territorialShortTitle()))
                .addColumn(textColumn("developForm", ProfileEducationOrgUnit.educationOrgUnit().developForm().title()))
                .addColumn(textColumn("developCondition", ProfileEducationOrgUnit.educationOrgUnit().developCondition().title()))
                .addColumn(textColumn("developTech", ProfileEducationOrgUnit.educationOrgUnit().developTech().title()))
                .addColumn(textColumn("developPeriod", ProfileEducationOrgUnit.educationOrgUnit().developPeriod().title()))
                .addColumn(blockColumn("quota", "quotaBlockColumn").width("10").create())
                .addColumn(blockColumn("used", "usedBlockColumn").width("10").create())
                .create();
    }

    @Bean
    public ColumnListExtPoint recommendedDS()
    {
        return columnListExtPointBuilder(RECOMMENDED_DS)
                .addColumn(textColumn("competitionKind", IEcgpRecommendedDTO.COMPETITION_KIND_TITLE).create())
                .addColumn(textColumn("targetAdmissionKind", IEcgpRecommendedDTO.TARGET_ADMISSION_KIND_TITLE).create())
                .addColumn(textColumn("finalMark", IEcgpRecommendedDTO.FINAL_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
                .addColumn(textColumn("profileMark", IEcgpRecommendedDTO.PROFILE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS).create())
                .addColumn(textColumn("graduatedProfileEduInstitution", IEcgpRecommendedDTO.GRADUATED_PROFILE_EDU_INSTITUTION_TITLE).create())
                .addColumn(textColumn("certificateAverageMark", IEcgpRecommendedDTO.CERTIFICATE_AVERAGE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).create())
                .addColumn(textColumn("fio", IEcgpRecommendedDTO.FIO))
                .addColumn(textColumn("direction", IEcgpRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgpEntrantRecommended.profile().educationOrgUnit().title().s()))
                .addColumn(textColumn("developForm", IEcgpRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgpEntrantRecommended.profile().educationOrgUnit().developForm().title().s()).create())
                .addColumn(textColumn("developCondition", IEcgpRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgpEntrantRecommended.profile().educationOrgUnit().developCondition().title().s()).create())
                .addColumn(textColumn("developTech", IEcgpRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgpEntrantRecommended.profile().educationOrgUnit().developTech().title().s()).create())
                .addColumn(textColumn("developPeriod", IEcgpRecommendedDTO.ENTRANT_RECOMMENDED + "." + EcgpEntrantRecommended.profile().educationOrgUnit().developPeriod().title().s()).create())
                .addColumn(textColumn("priorities", IEcgpRecommendedDTO.PRIORITIES).create())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("recommendedDS.delete.alert", IEcgpRecommendedDTO.FIO)).permissionKey("profileDistributionSectEdit").create())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> directionDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> recommendedDSHandler()
    {
        return new EcSimpleDSHandler(getName());
    }
}
