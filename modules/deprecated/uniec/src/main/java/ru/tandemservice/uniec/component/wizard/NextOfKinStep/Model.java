/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.NextOfKinStep;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.bo.Person.ui.NextOfKinAddEdit.PersonNextOfKinAddEditUI;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author vip_delete
 * @since 15.06.2009
 */
@Input({
        @Bind(key = "entrantId", binding = "entrant.id"),
        @Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey"),
        @Bind(key = "onlineEntrantId", binding = "onlineEntrant.id")
})
public class Model
{
    private Entrant _entrant = new Entrant();
    private OnlineEntrant _onlineEntrant = new OnlineEntrant();
    private List<PersonNextOfKin> _personNextOfKinList = new ArrayList<PersonNextOfKin>();
    private DynamicListDataSource<PersonNextOfKin> _dataSource;

    private NextOfKinModel _kinModel = new NextOfKinModel();
    private String _entrantMasterPermKey;

    public String getEntrantMasterPermKey()
    {
        if(StringUtils.isEmpty(_entrantMasterPermKey)) return "addEntrantMaster";
        return _entrantMasterPermKey;
    }

    public void setEntrantMasterPermKey(String entrantMasterPermKey)
    {
        _entrantMasterPermKey = entrantMasterPermKey;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public void setOnlineEntrant(OnlineEntrant onlineEntrant)
    {
        _onlineEntrant = onlineEntrant;
    }

    public List<PersonNextOfKin> getPersonNextOfKinList()
    {
        return _personNextOfKinList;
    }

    public void setPersonNextOfKinList(List<PersonNextOfKin> personNextOfKinList)
    {
        _personNextOfKinList = personNextOfKinList;
    }

    public DynamicListDataSource<PersonNextOfKin> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<PersonNextOfKin> dataSource)
    {
        _dataSource = dataSource;
    }

    public NextOfKinModel getKinModel()
    {
        return _kinModel;
    }

    public void setKinModel(NextOfKinModel kinModel)
    {
        _kinModel = kinModel;
    }
}
