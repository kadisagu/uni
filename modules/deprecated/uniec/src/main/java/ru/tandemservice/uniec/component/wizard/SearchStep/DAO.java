/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.SearchStep;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.person.base.bo.Person.util.CongenialStringFinder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.uni.UniDefines;
import org.tandemframework.shared.person.base.bo.Person.util.CongenialStringFinder.SearchElement;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.component.wizard.SearchStep.Model.SearchMethod;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author agolubenko
 * @since May 12, 2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected void setOnlineEntrantListCondition(MQBuilder builder, String filter, Model model, String alias)
    {
        builder.add(MQExpression.eq(alias, OnlineEntrant.enrollmentCampaign().s(), model.getEnrollmentCampaign()));
        if (StringUtils.isNotEmpty(filter))
        {
            if (StringUtils.isNumeric(filter))
                builder.add(MQExpression.eq(alias, OnlineEntrant.personalNumber().s(), Integer.parseInt(filter)));
            else if(Pattern.compile("\\d{4}-\\d+").matcher(filter).matches())
            {
                int year = Integer.parseInt(filter.substring(0, 4));
                builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.registrationYear().s(), year));
                int number = Integer.parseInt(filter.substring(5));
                builder.add(MQExpression.eq("onlineEntrant", OnlineEntrant.personalNumber().s(), number));
            }
            else
                builder.add(MQExpression.like(alias, OnlineEntrant.lastName().s(), CoreStringUtils.escapeLike(filter)));
        }
    }

    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());
        model.setEnrollmentCampaign(null);
        model.setIdentityCardTypeList(getCatalogItemListOrderByCode(IdentityCardType.class));
        model.getIdentityCard().setCardType(getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT));
        model.setOnlineEntrantModel(new FullCheckSelectModel(OnlineEntrant.P_FULL_TITLE)
        {
            @Override
            public ListResult<OnlineEntrant> findValues(String filter)
            {
                String alias = "onlineEntrant";
                MQBuilder builder = new MQBuilder(OnlineEntrant.ENTITY_CLASS, alias);
                setOnlineEntrantListCondition(builder, filter, model, alias);
                builder.addOrder(alias, OnlineEntrant.lastName().s());
                builder.addOrder(alias, OnlineEntrant.firstName().s());
                builder.addOrder(alias, OnlineEntrant.middleName().s());
                return new ListResult<>(builder.<OnlineEntrant> getResultList(getSession()));
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public Entrant findEntrant(Model model)
    {
        if (model.getSearchMethod() == SearchMethod.ENTRANTS)
        {
            Criteria criteria = getSession().createCriteria(Entrant.class);
            criteria.createAlias(Entrant.person().s(), "person");
            criteria.add(Restrictions.eq(Entrant.enrollmentCampaign().s(), model.getEnrollmentCampaign()));
            criteria.createCriteria(Person.identityCard().fromAlias("person").s()).add(Example.create(model.getIdentityCard()));
            List<Entrant> result = criteria.list();
            if (result.size() > 1)
            {
                throw new ApplicationException("Найдено более одного абитуриента с данными параметрами.");
            }
            return (!result.isEmpty()) ? result.get(0) : null;
        }
        else
        {
            return model.getOnlineEntrant().getEntrant();
        }
    }

    @Override
    public IdentityCard findDuplicate(IdentityCard identityCard)
    {
        String[] properties = new String[] { IdentityCard.P_LAST_NAME, IdentityCard.P_FIRST_NAME, IdentityCard.P_SERIA, IdentityCard.P_NUMBER };
        List<SearchElement> identityCards = CongenialStringFinder.getCongenialIdentityCards(identityCard, properties, 0, getSession());
        return (!identityCards.isEmpty()) ? get(IdentityCard.class, identityCards.get(0).getCardId()) : null;
    }
}
