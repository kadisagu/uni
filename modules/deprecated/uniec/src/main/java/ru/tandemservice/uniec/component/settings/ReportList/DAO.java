/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ReportList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.menu.ReportList.IReportDefinition;
import ru.tandemservice.uniec.component.menu.ReportList.ReportDefinition;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        IDataSettings settings = DataSettingsFacade.getSettings(UniecDefines.EC_MODULE_OWNER, UniecDefines.IS_REPORT_USED);
        List<ReportDefinitionWrapper> wrappers = new ArrayList<ReportDefinitionWrapper>();
        Map<Long, ReportDefinitionWrapper> wrapperById = new HashMap<Long, ReportDefinitionWrapper>();
        Long id = 0L;
        for (ReportDefinition repDef : (List<ReportDefinition>) ApplicationRuntime.getBean(IReportDefinition.UNIEC_REPORT_DEFINITION_LIST_BEAN_NAME))
        {
            Boolean used = (Boolean)settings.getEntry(repDef.getId()).getValue();
            ReportDefinitionWrapper wrapper = new ReportDefinitionWrapper(id, repDef.getId(), repDef.getTitle(), used == null ? true : used);
            wrappers.add(wrapper);
            wrapperById.put(id++, wrapper);
        }
        model.setWrapperById(wrapperById);
        Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

        model.getDataSource().setTotalSize(wrappers.size());
        model.getDataSource().setCountRow(wrappers.size());
        model.getDataSource().createPage(wrappers);
    }

    class ReportDefinitionWrapper extends IdentifiableWrapper
    {
        private static final long serialVersionUID = -149420829158818911L;
        private boolean _used = true;
        private String _reportDefinitionId;

        ReportDefinitionWrapper(Long id, String reportDefinitionId, String title, boolean used)
        {
            super(id, title);
            _reportDefinitionId = reportDefinitionId;
            _used = used;
        }

        public boolean isUsed()
        {
            return _used;
        }

        public void setUsed(boolean used)
        {
            _used = used;
        }

        public String getReportDefinitionId()
        {
            return _reportDefinitionId;
        }

        public void setReportDefinitionId(String reportDefinitionId)
        {
            _reportDefinitionId = reportDefinitionId;
        }
    }
}
