/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcPreEnroll.util;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.report.IReportRow;

/**
 * @author Vasily Zhukov
 * @since 26.05.2011
 */
public class PreEntrantDTO extends DataWrapper implements IReportRow
{
    private static final long serialVersionUID = 1L;
    public static final String REG_NUMBER = "regNumber";
    public static final String COMPETITION_KIND_TITLE = "competitionKindTitle";
    public static final String FINAL_MARK_STR = "finalMarkStr";
    public static final String AVERAGE_MARK = "averageMark";
    public static final String ENROLLMENT_CONDITIONS = "enrollmentConditions";
    public static final String ORDER_NUMBER = "orderNumber";
    public static final String CHANGE_ENROLLMENT_CONDITION_DISABLED = "changeEnrollmentConditionDisabled";

    private Entrant _entrant;
    private PreliminaryEnrollmentStudent _preStudent;

    private String _regNumber;
    private String _competitionKindTitle;
    private Double _finalMark;
    private String _finalMarkStr;
    private Double _averageMark;
    private String _enrollmentConditions;
    private String _orderNumber;
    private boolean _changeOrderDisabled;
    private boolean _changeEnrollmentConditionDisabled;
    private int _preStudentCount;

    /**
     * @param requestedEnrollmentDirection выбранное направление приема, на которое абитуриент уже предзачислен (preStudent != null), или по которому успешно сдал все вступительные испытания
     * @param preStudent                   предзачисленный студент, либо null, если еще не предзачислен
     * @param finalMarkStr                 текстовое название суммы баллов, если предзачисление идет по собеседование, то слова "прошел" или "не прошел"
     * @param finalMark                    финальная оценка по выбранному направлению приема, либо null, если предзачисление идет по собеседованию
     * @param averageMark                  средний балл основного документа образования
     * @param orderNumber                  номер приказа, в который включен предзачисленный абитуриент (preStudent != null), либо null, если в приказ еще не включен
     * @param preStudentCount              кол-во предзачисленных
     */
    public PreEntrantDTO(RequestedEnrollmentDirection requestedEnrollmentDirection, PreliminaryEnrollmentStudent preStudent, String finalMarkStr, Double finalMark, Double averageMark, String orderNumber, int preStudentCount)
    {
        super(requestedEnrollmentDirection.getId(), "", requestedEnrollmentDirection);

        _entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
        _preStudent = preStudent;

        _regNumber = UniecDAOFacade.getRequestedEnrollmentDirectionNumberFormatter().format(requestedEnrollmentDirection);

        CompetitionKind kind = requestedEnrollmentDirection.getCompetitionKind();
        if (!requestedEnrollmentDirection.isTargetAdmission())
        {
            _competitionKindTitle = kind.getTitle();
        } else
        {
            String result = EnrollmentCompetitionKind.TARGET_ADMISSION;
            if (!UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(kind.getCode()))
                result += ", " + kind.getShortTitle();
            _competitionKindTitle = result;
        }

        _finalMark = finalMark;
        _finalMarkStr = finalMarkStr;
        _averageMark = averageMark;
        _enrollmentConditions = _preStudent == null ? null : preStudent.getEnrollmentConditions();
        _orderNumber = orderNumber;

        // если уже есть приказ, то нельзя менять тип приказа, по которому будет предзачисление
        _changeOrderDisabled = orderNumber != null;

        // если нет предазачисления, то нельзя менять условия презачисления
        // если есть приказ, то можно менять условия только для предзачисляемых слушателей
        _changeEnrollmentConditionDisabled = preStudent == null || (_changeOrderDisabled && !UniDefines.STUDENT_CATEGORY_LISTENER.equals(preStudent.getStudentCategory().getCode()));

        _preStudentCount = preStudentCount;
    }

    // IReportRow

    @Override
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return (RequestedEnrollmentDirection) getWrapped();
    }

    @Override
    public Double getFinalMark()
    {
        return _finalMark;
    }

    @Override
    public Double getProfileMark()
    {
        ChosenEntranceDiscipline chosen = getRequestedEnrollmentDirection().getProfileChosenEntranceDiscipline();
        return chosen == null ? null : chosen.getFinalMark();
    }

    @Override
    public String getFio()
    {
        return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution()
    {
        return getRequestedEnrollmentDirection().isGraduatedProfileEduInstitution();
    }

    @Override
    public Double getCertificateAverageMark()
    {
        return _averageMark;
    }

    // Getters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public PreliminaryEnrollmentStudent getPreStudent()
    {
        return _preStudent;
    }

    public String getRegNumber()
    {
        return _regNumber;
    }

    public String getCompetitionKindTitle()
    {
        return _competitionKindTitle;
    }

    public String getFinalMarkStr()
    {
        return _finalMarkStr;
    }

    public Double getAverageMark()
    {
        return _averageMark;
    }

    public String getEnrollmentConditions()
    {
        return _enrollmentConditions;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public boolean isChangeOrderDisabled()
    {
        return _changeOrderDisabled;
    }

    public boolean isChangeEnrollmentConditionDisabled()
    {
        return _changeEnrollmentConditionDisabled;
    }

    public int getPreStudentCount()
    {
        return _preStudentCount;
    }
}
