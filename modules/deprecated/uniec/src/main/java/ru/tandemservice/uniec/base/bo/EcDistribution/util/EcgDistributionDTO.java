/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Date;

import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;
import ru.tandemservice.uniec.base.entity.ecg.codes.EcgDistributionStateCodes;

/**
 * @author Vasily Zhukov
 * @since 06.07.2011
 */
public class EcgDistributionDTO extends IdentifiableWrapper
{
    private static final long serialVersionUID = 1L;
    public static final String STATUS_DISTRIBUTION_FORMING = EcgDistributionStateCodes.FORMATIVE;                   // основное распределение (формируется)
    public static final String STATUS_DISTRIBUTION_LOCKED = EcgDistributionStateCodes.LOCKED;                       // основное распределение (зафиксировано)
    public static final String STATUS_DISTRIBUTION_APPROVED = EcgDistributionStateCodes.APPROVED;                   // основное распределение (утверждено)
    public static final String STATUS_DISTRIBUTION_DETAIL_FORMING = "detail" + EcgDistributionStateCodes.FORMATIVE; // уточняющее распределение (формируется)
    public static final String STATUS_DISTRIBUTION_DETAIL_APPROVED = "detail" + EcgDistributionStateCodes.APPROVED; // уточняющее распределение (утверждено)

    public static final String ECG_ITEM = "ecgItem";
    public static final String COMPENSATION_TYPE = "compensationType";
    public static final String STATE_CODE = "stateCode";
    public static final String TITLE = "title";
    public static final String FORMING_DATE = "formingDate";

    public static final String ADD_DISABLED = "addDisabled";
    public static final String ADD_DETAIL_DISABLED = "addDetailDisabled";
    public static final String DOWNLOAD_DISABLED = "downloadDisabled";
    public static final String EDIT_DISABLED = "editDisabled";
    public static final String DELETE_DISABLED = "deleteDisabled";

    private Long _persistentId;
    private int _wave;
    private IPersistentEcgItem _ecgItem;
    private CompensationType _compensationType;
    private boolean _secondHighAdmission;
    private String _stateCode;
    private Date _formingDate;
    private Date _approvalDate;
    private boolean _detailDistribution;

    private boolean _addDisabled;
    private boolean _addDetailDisabled;
    private boolean _downloadDisabled;
    private boolean _editDisabled;
    private boolean _deleteDisabled;

    /**
     * Создает DTO на основное распределение
     *
     * @param id                 идентификатор
     * @param distribution       основное распределение
     * @param distributionDetail уточняющее распределение
     */
    public EcgDistributionDTO(long id, EcgDistribution distribution, EcgDistributionDetail distributionDetail)
    {
        super(id, distribution.getTitle());
        _persistentId = distribution.getId();
        _wave = distribution.getWave();
        _ecgItem = distribution.getConfig().getEcgItem();
        _compensationType = distribution.getConfig().getCompensationType();
        _secondHighAdmission = distribution.getConfig().isSecondHighAdmission();
        _stateCode = distribution.getState().getCode();
        _formingDate = distribution.getFormingDate();
        _approvalDate = distribution.getApprovalDate();
        _detailDistribution = false;

        _addDisabled = true;
        _addDetailDisabled = !distribution.getState().isApproved() || distributionDetail != null;
        _downloadDisabled = false;
        _editDisabled = distribution.getState().isApproved();
        _deleteDisabled = distributionDetail != null;
    }

    /**
     * Создает DTO на уточняющее распределение
     *
     * @param id                 идентификатор
     * @param distributionDetail уточняющее распределение
     */
    public EcgDistributionDTO(long id, EcgDistributionDetail distributionDetail)
    {
        super(id, distributionDetail.getTitle());
        _persistentId = distributionDetail.getId();
        _wave = distributionDetail.getDistribution().getWave();
        _ecgItem = distributionDetail.getDistribution().getConfig().getEcgItem();
        _compensationType = distributionDetail.getDistribution().getConfig().getCompensationType();
        _secondHighAdmission = distributionDetail.getDistribution().getConfig().isSecondHighAdmission();
        _stateCode = "detail" + distributionDetail.getState().getCode();
        _formingDate = distributionDetail.getFormingDate();
        _approvalDate = distributionDetail.getApprovalDate();
        _detailDistribution = true;

        _addDisabled = true;
        _addDetailDisabled = true;
        _downloadDisabled = true;
        _editDisabled = true;
        _deleteDisabled = false;
    }

    /**
     * Создает заглушку под новое основное распределение
     *
     * @param id                  идентификатор
     * @param ecgItem             объект распределения
     * @param compensationType    вид возмещения затрат
     * @param secondHighAdmission true, если распределение на второе высшее
     */
    public EcgDistributionDTO(long id, IPersistentEcgItem ecgItem, CompensationType compensationType, boolean secondHighAdmission)
    {
        super(id, "");
        _persistentId = null;
        _ecgItem = ecgItem;
        _compensationType = compensationType;
        _secondHighAdmission = secondHighAdmission;
        _detailDistribution = false;

        _addDisabled = false;
        _addDetailDisabled = true;
        _downloadDisabled = true;
        _editDisabled = true;
        _deleteDisabled = true;
    }

    // ConfigKey

    public EcgConfigDTO getConfigDTO()
    {
        return new EcgConfigDTO(_ecgItem, _compensationType, _secondHighAdmission);
    }

    // Getters

    public Long getPersistentId()
    {
        return _persistentId;
    }

    public int getWave()
    {
        return _wave;
    }

    public IPersistentEcgItem getEcgItem()
    {
        return _ecgItem;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public boolean isSecondHighAdmission()
    {
        return _secondHighAdmission;
    }

    public String getStateCode()
    {
        return _stateCode;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public Date getApprovalDate()
    {
        return _approvalDate;
    }

    public boolean isDetailDistribution()
    {
        return _detailDistribution;
    }

    public boolean isAddDisabled()
    {
        return _addDisabled;
    }

    public boolean isAddDetailDisabled()
    {
        return _addDetailDisabled;
    }

    public boolean isDownloadDisabled()
    {
        return _downloadDisabled;
    }

    public boolean isEditDisabled()
    {
        return _editDisabled;
    }

    public boolean isDeleteDisabled()
    {
        return _deleteDisabled;
    }
}
