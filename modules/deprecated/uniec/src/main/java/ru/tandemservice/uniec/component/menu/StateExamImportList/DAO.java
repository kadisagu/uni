/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.StateExamImportList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.StateExamImportedFile;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author Julia Oschepkova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StateExamImportedFile.ENTITY_CLASS, "importedFiles");
        builder.add(MQExpression.eq("importedFiles", StateExamImportedFile.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        new OrderDescriptionRegistry("importedFiles").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public byte[] getPrintImportedFileRequest(long id)
    {
        return get(StateExamImportedFile.class, id).getImportedFile().getContent();
    }

    @Override
    public byte[] getPrintResultFileRequest(long id)
    {
        return get(StateExamImportedFile.class, id).getResultFile().getContent();
    }
}
