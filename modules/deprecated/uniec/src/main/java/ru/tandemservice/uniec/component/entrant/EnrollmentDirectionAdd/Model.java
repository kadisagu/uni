/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionAdd;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author vip_delete
 * @since 16.02.2009
 */
public class Model implements IEducationLevelModel, IEnrollmentCampaignSelectModel, IEnrollmentCampaignModel
{
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String CHECKBOX_SELECTED_COLUMN = "checkboxSelected";

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private EducationLevelsHighSchool _educationLevelsHighSchool;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private EnrollmentCampaign _enrollmentCampaign;
    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;

    private DynamicListDataSource<EducationOrgUnit> _educationOrgUnitDataSource;
    private DynamicListDataSource<EducationOrgUnit> _educationOrgUnitSelectedDataSource;

    private Set<EducationOrgUnit> _selectedEducationOrgUnitSet = new HashSet<EducationOrgUnit>();

    // IEducationLevelModel Implementation

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return null;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return null;
    }

    // Getters & Setters

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public DynamicListDataSource<EducationOrgUnit> getEducationOrgUnitDataSource()
    {
        return _educationOrgUnitDataSource;
    }

    public void setEducationOrgUnitDataSource(DynamicListDataSource<EducationOrgUnit> educationOrgUnitDataSource)
    {
        _educationOrgUnitDataSource = educationOrgUnitDataSource;
    }

    public DynamicListDataSource<EducationOrgUnit> getEducationOrgUnitSelectedDataSource()
    {
        return _educationOrgUnitSelectedDataSource;
    }

    public void setEducationOrgUnitSelectedDataSource(DynamicListDataSource<EducationOrgUnit> educationOrgUnitSelectedDataSource)
    {
        _educationOrgUnitSelectedDataSource = educationOrgUnitSelectedDataSource;
    }

    public Set<EducationOrgUnit> getSelectedEducationOrgUnitSet()
    {
        return _selectedEducationOrgUnitSet;
    }

    public void setSelectedEducationOrgUnitSet(Set<EducationOrgUnit> selectedEducationOrgUnitSet)
    {
        _selectedEducationOrgUnitSet = selectedEducationOrgUnitSet;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }
}
