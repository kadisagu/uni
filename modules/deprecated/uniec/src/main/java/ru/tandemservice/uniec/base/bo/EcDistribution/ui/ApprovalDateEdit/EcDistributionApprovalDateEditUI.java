/* $Id: EcDistributionAddEditUI.java 22919 2012-05-17 13:02:47Z vzhukov $ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.ApprovalDateEdit;

import java.util.Date;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;

/**
 * Форма добавления/редактирования основного распределения
 *
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
@Input({
    @Bind(key = EcDistributionApprovalDateEditUI.DISTRIBUTION_DTO, binding = EcDistributionApprovalDateEditUI.DISTRIBUTION_DTO, required = true)
})
public class EcDistributionApprovalDateEditUI extends UIPresenter
{
    public static final String DISTRIBUTION_DTO = "distributionDTO";

    private EcgDistributionDTO _distributionDTO;
    private Date approvalDate;


    @Override
    public void onComponentRefresh()
    {
        setApprovalDate(getDistributionDTO().getApprovalDate());
    }


    // Getters & Setters

    public EcgDistributionDTO getDistributionDTO() {
        return _distributionDTO;
    }

    public void setDistributionDTO(EcgDistributionDTO distributionDTO) {
        _distributionDTO = distributionDTO;
    }

    public Date getApprovalDate() {
        return this.approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    // Listeners

    public void onClickApply()
    {
        EcDistributionManager.instance().dao().updateApprovalDate(getDistributionDTO().getPersistentId(), getApprovalDate());
        deactivate();
    }
}
