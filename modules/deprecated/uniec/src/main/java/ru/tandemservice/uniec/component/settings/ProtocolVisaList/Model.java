/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ProtocolVisaList;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author vip_delete
 * @since 21.07.2009
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _orgUnitListModel;
    private IDataSettings _settings;

    private String _commissionChairman;
    private boolean _commissionChairmanAll;
    private String _executiveSecretary;
    private boolean _executiveSecretaryAll;
    private String _selectionSecretary;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _settings.get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _settings.set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public String getCommissionChairman()
    {
        return _commissionChairman;
    }

    public void setCommissionChairman(String commissionChairman)
    {
        _commissionChairman = commissionChairman;
    }

    public boolean isCommissionChairmanAll()
    {
        return _commissionChairmanAll;
    }

    public void setCommissionChairmanAll(boolean commissionChairmanAll)
    {
        _commissionChairmanAll = commissionChairmanAll;
    }

    public String getExecutiveSecretary()
    {
        return _executiveSecretary;
    }

    public void setExecutiveSecretary(String executiveSecretary)
    {
        _executiveSecretary = executiveSecretary;
    }

    public boolean isExecutiveSecretaryAll()
    {
        return _executiveSecretaryAll;
    }

    public void setExecutiveSecretaryAll(boolean executiveSecretaryAll)
    {
        _executiveSecretaryAll = executiveSecretaryAll;
    }

    public String getSelectionSecretary()
    {
        return _selectionSecretary;
    }

    public void setSelectionSecretary(String selectionSecretary)
    {
        _selectionSecretary = selectionSecretary;
    }
}
