package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.PreEnroll;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author vdanilov
 */
@State({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="distrib.id")
})
public class Model {

	private EcgDistribObject distrib = new EcgDistribObject();
	public EcgDistribObject getDistrib() { return this.distrib; }
	public void setDistrib(final EcgDistribObject distrib) { this.distrib = distrib; }

	private List<EntrantEnrollmentOrderType> orderTypes = Collections.emptyList();
	public List<EntrantEnrollmentOrderType> getOrderTypes() { return this.orderTypes; }
	public void setOrderTypes(final List<EntrantEnrollmentOrderType> orderTypes) { this.orderTypes = orderTypes; }

	@SuppressWarnings("serial")
	public static abstract class Row extends IdentifiableWrapper<PreliminaryEnrollmentStudent> {
		public Row(final Long id, final String title) { super(id, title); }
		public abstract RequestedEnrollmentDirection getDirection();
		public abstract PreliminaryEnrollmentStudent getStudent();
	}

	private List<Row> rows = Collections.emptyList();
	public List<Row> getRows() { return this.rows; }
	public void setRows(final List<Row> rows) { this.rows = rows; }

	private AbstractListDataSource<Row> dataSource = new AbstractListDataSource<Row>() {
		@Override public void onChangeOrder() {}
		@Override public void onRefresh() {}
		@Override public long getTotalSize() { return getEntityList().size(); }
		@Override public long getStartRow() { return 0; }
		@Override public long getCountRow() { return this.getTotalSize(); }
		@Override public List<Row> getEntityList() { return rows; }
        @Override public AbstractListDataSource<Row> getCopy() { return this; }
    };
	public AbstractListDataSource<Row> getStudentDataSource() { return this.dataSource; }

}
