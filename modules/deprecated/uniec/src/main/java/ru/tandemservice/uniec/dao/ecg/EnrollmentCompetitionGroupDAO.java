package ru.tandemservice.uniec.dao.ecg;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.gen.PersonGen;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;
import ru.tandemservice.uniec.entity.ecg.EcgDistribRelation;
import ru.tandemservice.uniec.entity.ecg.EcgReserveRelation;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribObjectGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribQuotaGen;
import ru.tandemservice.uniec.entity.ecg.gen.EcgDistribRelationGen;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.*;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author vdanilov
 */
public class EnrollmentCompetitionGroupDAO extends UniBaseDao implements IEnrollmentCompetitionGroupDAO
{

    /**
     * реализация строки абитуриента
     */
    protected static abstract class EntrantRateRow implements IEntrantRateRow
    {
        private final List<RequestedEnrollmentDirection> directions = new ArrayList<>();

        public void filterDirections(final Predicate predicate)
        {
            CollectionUtils.filter(this.directions, predicate);
        }

        public void add(final RequestedEnrollmentDirection direction)
        {
            this.directions.add(direction);
            this.graduatedProfileEduInstitutionPriority = null;
            this.originalDocumentHandedInPriority = null;
        }

        private final List<RequestedEnrollmentDirection> directions_exp = Collections.unmodifiableList(this.directions);

        @Override
        public List<RequestedEnrollmentDirection> getDirections()
        {
            return this.directions_exp;
        }

        private Integer graduatedProfileEduInstitutionPriority;

        @Override
        public int getGraduatedProfileEduInstitutionPriority()
        {
            if (null != this.graduatedProfileEduInstitutionPriority)
            {
                return this.graduatedProfileEduInstitutionPriority;
            }

            int result = Integer.MAX_VALUE;
            for (final RequestedEnrollmentDirection dir : this.directions)
            {
                if (dir.isGraduatedProfileEduInstitution())
                {
                    if (result > dir.getPriority())
                    {
                        result = dir.getPriority();
                    }
                }
            }
            return (this.graduatedProfileEduInstitutionPriority = result);
        }

        private Integer originalDocumentHandedInPriority;

        @Override
        public int getOriginalDocumentHandedInPriority()
        {
            if (null != this.originalDocumentHandedInPriority)
            {
                return this.originalDocumentHandedInPriority;
            }
            int result = Integer.MAX_VALUE;
            for (final RequestedEnrollmentDirection dir : this.directions)
            {
                if (dir.isOriginalDocumentHandedIn())
                {
                    if (result > dir.getPriority())
                    {
                        result = dir.getPriority();
                    }
                }
            }
            return (this.originalDocumentHandedInPriority = result);
        }

        private Double _totalMark = null;

        @Override
        public Double getTotalMark()
        {
            return this._totalMark;
        }

        private Double _profileMark = null;

        @Override
        public Double getProfileMark()
        {
            return this._profileMark;
        }

        protected void updateTotalMarks(final Double totalMark, final Double profileMark)
        {
            this._totalMark = EntrantRateRow.max(this._totalMark, totalMark);
            this._profileMark = EntrantRateRow.max(this._profileMark, profileMark);
        }

        private static Double max(final Double a, final Double b)
        {
            if ((null == a) && (null == b))
            {
                return null;
            }
            if (null == a)
            {
                return b;
            }
            if (null == b)
            {
                return a;
            }
            return Math.max(a, b);
        }

    }

    /**
     * сортировка строк абитуриентов (внутри групп)
     */
    private static final Comparator<IEntrantRateRow> IN_GROUP_ROWS_COMPARATOR = new Comparator<IEntrantRateRow>()
    {
        @Override
        public int compare(final IEntrantRateRow o1, final IEntrantRateRow o2)
        {
            // по баллу (по убыванию)
            int result = -UniBaseUtils.compare(o1.getTotalMark(), o2.getTotalMark(), true);

            // по профильному баллу (по убыванию)
            if (result == 0)
            {
                result = -UniBaseUtils.compare(o1.getProfileMark(), o2.getProfileMark(), true);
            }

            // СПО (по убыванию)
            if (result == 0)
            {
                result = -Long.compare((long) o1.getGraduatedProfileEduInstitutionPriority(), (long) o2.getGraduatedProfileEduInstitutionPriority());
            }

            // аттестат (по убыванию)
            if (result == 0)
            {
                result = -UniBaseUtils.compare(o1.getCertificateAverageMark(), o2.getCertificateAverageMark(), true);
            }

            // по ФИО (по возрастанию)
            if (result == 0)
            {
                result = this.getFullFio(o1).compareTo(this.getFullFio(o2));
            }

            return result;
        }

        private String getFullFio(final IEntrantRateRow row)
        {
            return row.getEntrant().getPerson().getFullFio();
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    public List<IEntrantRateRow> getEntrantRateRows(final EcgDistribObject distribution)
    {
        Debug.begin("getEntrantRateRows(" + distribution.getId() + ")");
        try
        {
            final MQBuilder directionBuilder = this.directionBuilder(distribution, false);

            /* включены в данное распределение */
            {
                final MQBuilder b = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "eSelf", new String[]{EcgDistribRelationGen.L_ENTRANT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id"});
                b.setNeedDistinct(true);
                b.add(MQExpression.eq("eSelf", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, distribution));
                directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id", b));
            }

            return (List) this.processDirectionQuery(distribution, directionBuilder, null);
        } finally
        {
            Debug.end();
        }
    }

    protected abstract static class EntrantRateRowsSelector implements IEntrantRateRowsSelector
    {
        public abstract void patchDirectionBuilder(MQBuilder directionBuilder);
    }

    /* целевой прием */
    public static final IEntrantRateRowsSelector TARGET_ADMISSION_SELECTOR = new EntrantRateRowsSelector()
    {
        @Override
        public void patchDirectionBuilder(final MQBuilder directionBuilder)
        {
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirectionGen.P_TARGET_ADMISSION, Boolean.TRUE));
        }
    };

    /* вне конкурса */
    public static final IEntrantRateRowsSelector NONCOMPETITIVE_ADMISSION_SELECTOR = new EntrantRateRowsSelector()
    {
        @Override
        public void patchDirectionBuilder(final MQBuilder directionBuilder)
        {
            final CompetitionKind competitiveAdmission = UniDaoFacade.getCoreDao().getByNaturalId(new CompetitionKind.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION));
            directionBuilder.add(MQExpression.and(MQExpression.notEq("r", RequestedEnrollmentDirectionGen.P_TARGET_ADMISSION, Boolean.TRUE), MQExpression.notEq("r", RequestedEnrollmentDirectionGen.L_COMPETITION_KIND, competitiveAdmission)));
        }
    };

    /* обычный прием */
    public static final IEntrantRateRowsSelector COMPETITIVE_ADMISSION_SELECTOR = new EntrantRateRowsSelector()
    {
        @Override
        public void patchDirectionBuilder(final MQBuilder directionBuilder)
        {
            final CompetitionKind competitiveAdmission = UniDaoFacade.getCoreDao().getByNaturalId(new CompetitionKind.NaturalId(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION));
            directionBuilder.add(MQExpression.and(MQExpression.notEq("r", RequestedEnrollmentDirectionGen.P_TARGET_ADMISSION, Boolean.TRUE), MQExpression.eq("r", RequestedEnrollmentDirectionGen.L_COMPETITION_KIND, competitiveAdmission)));
        }
    };

    private static final ThreadLocal<Map<IEntrantRateRowsSelector, List<IEntrantRateRow>>> entrantRateRows4AddCache = new ThreadLocal<>();

    protected interface InEntrantRateRows4AddCacheAction
    {
        void execute();
    }

    protected static void executeInEntrantRateRows4AddCache(final InEntrantRateRows4AddCacheAction action)
    {
        final Map<IEntrantRateRowsSelector, List<IEntrantRateRow>> map = EnrollmentCompetitionGroupDAO.entrantRateRows4AddCache.get();
        if (null != map)
        {
            action.execute();
        } else
        {
            try
            {
                EnrollmentCompetitionGroupDAO.entrantRateRows4AddCache.set(new HashMap<>());
                action.execute();
            } finally
            {
                EnrollmentCompetitionGroupDAO.entrantRateRows4AddCache.remove();
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IEntrantRateRow> getEntrantRateRows4Add(final EcgDistribObject distribution, final IEntrantRateRowsSelector selector)
    {
        final Map<IEntrantRateRowsSelector, List<IEntrantRateRow>> resultCacheMap = EnrollmentCompetitionGroupDAO.entrantRateRows4AddCache.get();
        if (null != resultCacheMap)
        {
            // check it in cache
            final List<IEntrantRateRow> r = resultCacheMap.get(selector);
            if (null != r)
            {
                return r;
            }
        }

        Debug.begin("getEntrantRateRows4Add(" + distribution.getId() + ")");
        try
        {

            final MQBuilder directionBuilder = this.directionBuilder(distribution, true);
            if (null != selector)
            {
                ((EntrantRateRowsSelector) selector).patchDirectionBuilder(directionBuilder);
            }

            /* не в данном распределении (прямо абитуриенты) */
            {
                final MQBuilder b = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "eSelf", new String[]{EcgDistribRelationGen.L_ENTRANT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id"});
                b.setNeedDistinct(true);
                b.add(MQExpression.eq("eSelf", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, distribution));
                directionBuilder.add(MQExpression.notIn("r", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id", b));
            }

            final Set<Long> distributedEntrantIdsList = this.getAlredyDistributedEntrantIds(distribution);
            final List<EntrantRateRow> rows = this.processDirectionQuery(distribution, directionBuilder, (entrant, direction) -> !distributedEntrantIdsList.contains(entrant.getId()));

            Debug.begin("getEntrantRateRows4Add(" + distribution.getId() + "): filter");
            try
            {

                final Map<Long, EntrantRateRow> entrant2row = new HashMap<>();
                for (final IEntrantRateRow row : rows)
                {
                    if (null != entrant2row.put(row.getEntrant().getId(), (EntrantRateRow) row))
                    {
                        this.logger.warn("duplicate entrant «" + row.getEntrant().getId() + "» (" + row.getEntrant().getPerson().getFullFio() + ")");
                    }
                }

                // нужно исключить те направления приема, по которым абитуриент уже пред зачислен
                BatchUtils.execute(entrant2row.keySet(), DQL.MAX_VALUES_ROW_NUMBER, entrantIds -> {
                    final MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudentGen.ENTITY_CLASS, "s");
                    builder.add(MQExpression.eq("s", PreliminaryEnrollmentStudentGen.L_STUDENT_CATEGORY + ".code", UniDefines.STUDENT_CATEGORY_STUDENT));
                    builder.addJoinFetch("s", PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION, "dir");
                    builder.addJoinFetch("dir", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST, "req");
                    builder.addJoinFetch("req", EntrantRequestGen.L_ENTRANT, "e");
                    builder.add(MQExpression.in("e", "id", entrantIds));

                    for (final PreliminaryEnrollmentStudent s : builder.<PreliminaryEnrollmentStudent>getResultList(EnrollmentCompetitionGroupDAO.this.getSession()))
                    {
                        final RequestedEnrollmentDirection reqDirection = s.getRequestedEnrollmentDirection();
                        final EntrantRateRow row = entrant2row.get(reqDirection.getEntrantRequest().getEntrant().getId());
                        if (null != row)
                        {
                            if (s.getStudentCategory().isStudent())
                            {
                                // если он предзачислен как студент, то мы убиваем все студенческие направления

                                // должны остаться только нестуденческие категории
                                row.filterDirections(object -> !((RequestedEnrollmentDirection) object).getStudentCategory().isStudent());
                            }

                            // удаляем то направление, с которого его перевели
                            row.filterDirections(object -> !s.getRequestedEnrollmentDirection().equals(object));
                        }
                    }
                });

                // убираем пустые строки (после фильтрации)
                CollectionUtils.filter(rows, object -> !((EntrantRateRow) object).getDirections().isEmpty());

            } finally
            {
                Debug.end();
            }

            if (null != resultCacheMap)
            {
                // cache it
                resultCacheMap.put(selector, (List) rows);
            }

            return (List) rows;
        } finally
        {
            Debug.end();
        }
    }

    // уже распределенные абитуриенты
    private Set<Long> getAlredyDistributedEntrantIds(final EcgDistribObject distribution)
    {
        if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(distribution.getCompensationType().getCode()))
        {
            // ТОЛЯ: если абитуриент пролетает в бюджетной итерации, то он пролитает пожизни по всем бюджетным итерациям

            final MQBuilder dExB = new MQBuilder(EcgDistribObjectGen.ENTITY_CLASS, "dEx", new String[]{"id"});
            dExB.setNeedDistinct(true);
            dExB.add(MQExpression.eq("dEx", EcgDistribObjectGen.P_APPROVED, Boolean.TRUE));
            dExB.add(MQExpression.eq("dEx", EcgDistribObjectGen.L_COMPENSATION_TYPE, distribution.getCompensationType()));
            dExB.add(MQExpression.eq("dEx", EcgDistribObjectGen.L_COMPETITION_GROUP, distribution.getCompetitionGroup()));
            dExB.add(MQExpression.eq("dEx", EcgDistribObjectGen.P_SECOND_HIGH_ADMISSION, distribution.isSecondHighAdmission()));

            /* однако, абитуриент может быть в основном распределении */
            {
                final Set<Long> excludedDiscributionIds = new HashSet<>();
                EcgDistribObject p = distribution.getParent();
                while (null != p)
                {
                    excludedDiscributionIds.add(p.getId());
                    p = p.getParent();
                }
                if (!excludedDiscributionIds.isEmpty())
                {
                    dExB.add(MQExpression.notIn("dEx", "id", excludedDiscributionIds));
                }
            }

            final MQBuilder b = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "eEx", new String[]{EcgDistribRelationGen.L_ENTRANT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id"});
            b.setNeedDistinct(true);
            b.add(MQExpression.in("eEx", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION + ".id", dExB.getResultList(this.getSession()) /* так быстрее, потому что сам запрос будет еще несколько раз подставляться в in - что очень страшно (а объектов здесь гарантированно мало - 4-5 штук максимум) */
            ));

            // к сожалению MSSQL очень дико тормозит, если это условие положить в in (как подзапрос) - 30 минут против одной минуты при загрузке вообще всех данных
            final List<Long> resultList = b.<Long>getResultList(this.getSession());
            if (!resultList.isEmpty())
            {
                return new HashSet<>(resultList);
            }
        }
        return Collections.emptySet();
    }

    private MQBuilder directionBuilder(final EcgDistribObject distribution, final boolean checkStatus)
    {
        // предварительно подготавливаем связи (упрощаем запросы/подзапросы предварительной выборкой)

        final MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirectionGen.ENTITY_CLASS, "r");
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirectionGen.L_COMPENSATION_TYPE, distribution.getCompensationType()));

        final List<EnrollmentDirection> directions = this.getList(EnrollmentDirection.class, EnrollmentDirectionGen.L_COMPETITION_GROUP, distribution.getCompetitionGroup());
        directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION, directions));

        final StudentCategory secondHiCategoryCategory = this.getCatalogItem(StudentCategory.class, StudentCategory.STUDENT_CATEGORY_SECOND_HIGH);
        if (distribution.isSecondHighAdmission())
        {
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirectionGen.L_STUDENT_CATEGORY, secondHiCategoryCategory));
        } else
        {
            directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirectionGen.L_STUDENT_CATEGORY, secondHiCategoryCategory));
        }

        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST, "request");
        // directionBuilder.addLeftJoinFetch("r", RequestedEnrollmentDirectionGen.L_PROFILE_CHOSEN_ENTRANCE_DISCIPLINE, "profileDiscipline");
        directionBuilder.addJoinFetch("request", EntrantRequestGen.L_ENTRANT, "entrant");
        directionBuilder.addJoinFetch("entrant", PersonRoleGen.L_PERSON, "person");
        directionBuilder.addJoinFetch("person", PersonGen.L_IDENTITY_CARD, "identityCard");

        if (checkStatus)
        {
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirectionGen.L_STATE, this.getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE)));
            directionBuilder.add(MQExpression.eq("entrant", EntrantGen.P_ARCHIVAL, Boolean.FALSE));

            // final EntrantState[] toBeEnrolledStates = new EntrantState[] {
            // getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE),
            // // getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_IN_ORDER),
            // // getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE),
            // // getCatalogItem(EntrantState.class, UniecDefines.ENTRANT_STATE_ENROLED_CODE),
            // };
            //
            // directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirectionGen.L_STATE, Arrays.asList(toBeEnrolledStates)));
            // directionBuilder.add(MQExpression.eq("entrant", EntrantGen.P_ARCHIVAL, Boolean.FALSE));
            //
            // // /* исключаем те направления (именно направления), по которым абитуриент уже предзачислен */ {
            // // final MQBuilder preenrolledEntrantsDirectionsBuilder = new MQBuilder(PreliminaryEnrollmentStudentGen.ENTITY_CLASS, "student", new String[] {
            // // PreliminaryEnrollmentStudentGen.L_REQUESTED_ENROLLMENT_DIRECTION+".id"
            // // });
            // // directionBuilder.add(MQExpression.notIn("r", "id", preenrolledEntrantsDirectionsBuilder));
            // // }
        }

        return directionBuilder;
    }

    protected interface EntrantRateRowPredicate
    {
        boolean evaluate(Entrant entrant, RequestedEnrollmentDirection direction);
    }

    protected List<EntrantRateRow> processDirectionQuery(final EcgDistribObject distribution, final MQBuilder directionBuilder, final EntrantRateRowPredicate predicate)
    {
        Debug.begin("processDirectionQuery");
        try
        {

            final EnrollmentCampaign enrollmentCampaign = distribution.getCompetitionGroup().getEnrollmentCampaign();

            class EntrantGroupSortKey
            {
                private final TargetAdmissionKind targetAdmissionKind;
                private final CompetitionKind competitionKind;

                public EntrantGroupSortKey(final TargetAdmissionKind targetAdmissionKind, final CompetitionKind competitionKind)
                {
                    this.targetAdmissionKind = targetAdmissionKind;
                    this.competitionKind = competitionKind;
                }

                @Override
                public int hashCode()
                {
                    return (UniBaseUtils.hashCode(this.targetAdmissionKind) * 3 + UniBaseUtils.hashCode(this.competitionKind) * 2);
                }

                @Override
                public boolean equals(final Object obj)
                {
                    final EntrantGroupSortKey o = (EntrantGroupSortKey) obj;
                    return (UniBaseUtils.equals(this.targetAdmissionKind, o.targetAdmissionKind) && UniBaseUtils.equals(this.competitionKind, o.competitionKind));
                }
            }

            final Session session = this.getSession();

            final Map<EntrantGroupSortKey, Map<Entrant, EntrantRateRow>> dataMap = new HashMap<>();
            final Set<Long> usedEntrantIds = new HashSet<>();

            Debug.begin("processDirectionQuery.fetch");
            try
            {
                final EntrantDataUtil dataUtil = new EntrantDataUtil(session, enrollmentCampaign, directionBuilder);

                for (final RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                {
                    final TargetAdmissionKind targetAdmissionKind = (direction.isTargetAdmission() ? direction.getTargetAdmissionKind() : null);
                    final CompetitionKind competitionKind = direction.getCompetitionKind();
                    final EntrantGroupSortKey key = new EntrantGroupSortKey(targetAdmissionKind, competitionKind);

                    Map<Entrant, EntrantRateRow> competitionMap = dataMap.get(key);
                    if (null == competitionMap)
                    {
                        dataMap.put(key, competitionMap = new HashMap<>());
                    }

                    final Entrant entrant = direction.getEntrantRequest().getEntrant();
                    if ((null == predicate) || predicate.evaluate(entrant, direction))
                    {
                        EntrantRateRow row = competitionMap.get(entrant);

                        if (null == row)
                        {
                            final PersonEduInstitution lastPersonEduInstitution = entrant.getPerson().getPersonEduInstitution();
                            final Double certificateAverageMark = (lastPersonEduInstitution != null) ? lastPersonEduInstitution.getAverageMark() : null;

                            competitionMap.put(entrant, row = new EntrantRateRow()
                            {
                                @Override
                                public Entrant getEntrant()
                                {
                                    return entrant;
                                }

                                @Override
                                public TargetAdmissionKind getTargetAdmissionKind()
                                {
                                    return targetAdmissionKind;
                                }

                                @Override
                                public CompetitionKind getCompetitionKind()
                                {
                                    return competitionKind;
                                }

                                @Override
                                public Double getCertificateAverageMark()
                                {
                                    return certificateAverageMark;
                                }
                            });
                        }

                        // добавляем направление
                        row.add(direction);

                        // считаем баллы
                        // ТОЛЯ: по конкурсной группе наборы испытаний одинаковые - поэтому берем первое направление и считаем по нему (по другим будет так же)
                        // если же будет не так - то берутся максимальные баллы (если отличаются - то их проблемы (с) ТОЛЯ)
                        final Double totalMark = dataUtil.getFinalMark(direction);
                        final Double profileMark = (Double) FastBeanUtils.getValue(direction, RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
                        row.updateTotalMarks(totalMark, profileMark);

                    }
                }
            } finally
            {
                Debug.end();
            }

            Debug.begin("processDirectionQuery.sort");
            try
            {
                final Map<CompetitionKind, Integer> competitionKindPriorities = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(enrollmentCampaign);
                final List<EntrantRateRow> result = new ArrayList<>();

                // сначала сортируем по группам (по вижу конкурса, потом по виду ЦП, если есть)
                final List<Map.Entry<EntrantGroupSortKey, Map<Entrant, EntrantRateRow>>> multiKeyEntries = new ArrayList<>(dataMap.entrySet());
                Collections.sort(multiKeyEntries, (o1, o2) -> {
                    final TargetAdmissionKind t1 = o1.getKey().targetAdmissionKind;
                    final TargetAdmissionKind t2 = o2.getKey().targetAdmissionKind;
                    if ((null != t1) && (null != t2))
                    {
                        // если оба ЦП (т.е. в рамках одной группы)
                        final int result1 = TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR.compare(t1, t2);
                        if (0 != result1)
                        {
                            return result1;
                        }

                        // сортируем по виду конкурса внутри ЦП
                        final CompetitionKind c1 = o1.getKey().competitionKind;
                        final CompetitionKind c2 = o2.getKey().competitionKind;
                        return (competitionKindPriorities.get(c1) - competitionKindPriorities.get(c2));

                    } else
                    {

                        // сортируем по видам конкурса (с учетом того, что все ЦП - в одной группе)
                        final CompetitionKind c1 = (null != t1 ? null : o1.getKey().competitionKind);
                        final CompetitionKind c2 = (null != t2 ? null : o2.getKey().competitionKind);
                        return (competitionKindPriorities.get(c1) - competitionKindPriorities.get(c2));
                    }
                });

                // затем в рамках каждой группы (сортируем по рейтингу)
                for (final Map.Entry<EntrantGroupSortKey, Map<Entrant, EntrantRateRow>> multiKeyEntry : multiKeyEntries)
                {
                    final List<EntrantRateRow> entrantEntries = new ArrayList<>(multiKeyEntry.getValue().values());
                    Collections.sort(entrantEntries, EnrollmentCompetitionGroupDAO.IN_GROUP_ROWS_COMPARATOR);

                    for (final EntrantRateRow e : entrantEntries)
                    {
                        if (usedEntrantIds.add(e.getEntrant().getId()))
                        {
                            result.add(e);
                        } else
                        {
                            this.logger.error("entrant «" + e.getEntrant().getId() + "» (" + e.getEntrant().getPerson().getFullFio() + ") is ignored (stage 0)");
                        }
                    }
                }

                for (final EntrantRateRow row : result)
                {
                    // сортируем направления внутри абитуриентов (по приоритетам)
                    Collections.sort(row.directions, (o1, o2) -> Long.compare((long) o1.getPriority(), (long) o2.getPriority()));
                }

                return result;
            } finally
            {
                Debug.end();
            }
        } finally
        {
            Debug.end();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, Integer> getDir2QuotaMap(final EcgDistribObject distrib)
    {
        final Map dir2quota = new HashMap();

        final MQBuilder builder = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q", new String[]{EcgDistribQuotaGen.L_DIRECTION + ".id", EcgDistribQuotaGen.P_QUOTA});
        builder.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, distrib));
        for (final Object[] row : builder.<Object[]>getResultList(this.getSession()))
        {
            dir2quota.put(row[0], new MutableInt((Integer) row[1]));
        }

        final MQBuilder b = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "r", new String[]{EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DIRECTION + ".id"});
        b.add(MQExpression.eq("r", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, distrib));
        for (final Long id : b.<Long>getResultList(this.getSession()))
        {
            ((MutableInt) dir2quota.get(id)).decrement();
        }

        final Set<Map.Entry> entrySet = dir2quota.entrySet();
        for (final Map.Entry e : entrySet)
        {
            e.setValue(((MutableInt) e.getValue()).toInteger());
        }
        return dir2quota;

    }

    @Override
    public void doAppendEntrantDirections(final EcgDistribObject distrib, final Collection<RequestedEnrollmentDirection> values)
    {
        this.checkEditable(distrib);

        if (distrib.isApproved())
        {
            throw new ApplicationException("Нельзя изменять утвержденное распределение.");
        }

        synchronized (String.valueOf(distrib.getId()).intern())
        {
            final MQBuilder eb = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "eb", new String[]{EcgDistribRelationGen.L_ENTRANT_DIRECTION + "." + RequestedEnrollmentDirectionGen.L_ENTRANT_REQUEST + "." + EntrantRequestGen.L_ENTRANT + ".id"});
            eb.add(MQExpression.eq("eb", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, distrib));
            eb.setNeedDistinct(true);
            final Set<Long> entrantIds = new HashSet<>(eb.<Long>getResultList(this.getSession()));

            final Map<Long, Integer> quotaMap = this.getDir2QuotaMap(distrib);
            final Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> dir2entrants = new HashMap<>();
            for (final RequestedEnrollmentDirection e : values)
            {
                if (entrantIds.add(e.getEntrantRequest().getEntrant().getId()))
                {
                    List<RequestedEnrollmentDirection> list = dir2entrants.get(e.getEnrollmentDirection());
                    if (null == list)
                    {
                        dir2entrants.put(e.getEnrollmentDirection(), list = new ArrayList<>());
                    }
                    list.add(e);
                } else
                {
                    this.logger.warn("entrant «" + e.getEntrantRequest().getEntrant().getId() + "» (" + e.getEntrantRequest().getEntrant().getPerson().getFullFio() + ", " + e.getEnrollmentDirection().getTitle() + ") is ignored (stage 1)");
                }
            }

            for (final Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> e : dir2entrants.entrySet())
            {
                final Integer quota = quotaMap.get(e.getKey().getId());
                if ((null == quota) || (e.getValue().size() > quota))
                {
                    throw new IllegalStateException();
                }
            }

            final MQBuilder builder = new MQBuilder(EcgDistribQuotaGen.ENTITY_CLASS, "q");
            builder.add(MQExpression.eq("q", EcgDistribQuotaGen.L_DISTRIBUTION, distrib));
            for (final EcgDistribQuota quota : builder.<EcgDistribQuota>getResultList(this.getSession()))
            {
                final List<RequestedEnrollmentDirection> list = dir2entrants.remove(quota.getDirection());
                if (null != list)
                {
                    for (final RequestedEnrollmentDirection dir : list)
                    {
                        final EcgDistribRelation rel = new EcgDistribRelation();
                        rel.setQuota(quota);
                        rel.setEntrantDirection(dir);
                        this.getSession().save(rel);
                    }
                }
            }

            if (!dir2entrants.isEmpty())
            {
                for (final List<RequestedEnrollmentDirection> dirList : dir2entrants.values())
                {
                    for (final RequestedEnrollmentDirection e : dirList)
                    {
                        this.logger.warn("entrant «" + e.getEntrantRequest().getEntrant().getId() + "» (" + e.getEntrantRequest().getEntrant().getPerson().getFullFio() + ", " + e.getEnrollmentDirection().getTitle() + ") is ignored (stage 2)");
                    }
                }
                throw new IllegalStateException("Не все абитуриенты распределены");
            }

            this.getSession().flush();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void doApproveDistribution(final EcgDistribObject distrib)
    {
        this.checkEditable(distrib);

        synchronized (String.valueOf(distrib.getCompetitionGroup().getId()).intern())
        {
            final Session session = this.getSession();

            Debug.begin("doApproveDistribution(" + distrib.getId() + "): main-actions");
            try
            {

                final MQBuilder b = new MQBuilder(EcgDistribRelationGen.ENTITY_CLASS, "r", new String[]{"id"});
                b.add(MQExpression.eq("r", EcgDistribRelationGen.L_QUOTA + "." + EcgDistribQuotaGen.L_DISTRIBUTION, distrib));
                if (b.getResultCount(session) <= 0)
                {
                    throw new ApplicationException("В распределение должен быть включен хотя бы один абитуриент.");
                }

                final Map<Long, Integer> quotaMap = this.getDir2QuotaMap(distrib);
                for (final Integer i : quotaMap.values())
                {
                    if (i < 0)
                    {
                        throw new ApplicationException("Количество абитуриентов в распределении превышает количество мест.");
                    }
                    // if (i.intValue() > 0) { throw new ApplicationException("В распределении еще есть не занятые места."); }
                }

                // удаляем все другие конкурсные группы
                final List<EcgDistribObject> distributions = (session.createCriteria(EcgDistribObject.class).add(null == distrib.getParent() ? Restrictions.isNull(EcgDistribObjectGen.L_PARENT) : Restrictions.eq(EcgDistribObjectGen.L_PARENT, distrib.getParent())).add(Restrictions.not(Restrictions.eq("id", distrib.getId()))).add(Restrictions.not(Restrictions.eq(EcgDistribObjectGen.P_APPROVED, Boolean.TRUE))).add(Restrictions.eq(EcgDistribObjectGen.L_COMPETITION_GROUP, distrib.getCompetitionGroup())).add(Restrictions.eq(EcgDistribObjectGen.L_COMPENSATION_TYPE, distrib.getCompensationType())).add(Restrictions.eq(EcgDistribObjectGen.P_SECOND_HIGH_ADMISSION, distrib.isSecondHighAdmission())).list());

                for (final EcgDistribObject d : distributions)
                {
                    session.delete(d);
                }

                distrib.setApproved(true);
                session.saveOrUpdate(distrib);
                session.flush();
            } finally
            {
                Debug.end();
            }

            EnrollmentCompetitionGroupDAO.executeInEntrantRateRows4AddCache(() -> {
                Debug.begin("doApproveDistribution(" + distrib.getId() + "): reports");
                try
                {
                    // сохраняем печатные формы результатов и резерва
                    {
                        final IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("competitionGroupDistribResultPrint");
                        final IScriptItem templateDocument = EnrollmentCompetitionGroupDAO.this.getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANTS_RATING_CG);
                        final byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(templateDocument.getCurrentTemplate(), distrib.getId()));
                        final DatabaseFile file = new DatabaseFile();
                        file.setContent(content);
                        session.save(file);
                        distrib.setResultPrintForm(file);
                        session.flush();
                    }
                    {
                        final IPrintFormCreator<Long> formCreator = (IPrintFormCreator<Long>) ApplicationRuntime.getBean("competitionGroupDistribReservePrint");
                        final IScriptItem templateDocument = EnrollmentCompetitionGroupDAO.this.getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANTS_RATING_CG_RES);
                        final byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(templateDocument.getCurrentTemplate(), distrib.getId()));
                        final DatabaseFile file = new DatabaseFile();
                        file.setContent(content);
                        session.save(file);
                        distrib.setReservePrintForm(file);
                        session.flush();
                    }

                    session.saveOrUpdate(distrib);
                    session.flush();
                } finally
                {
                    Debug.end();
                }

                Debug.begin("doApproveDistribution(" + distrib.getId() + "): save-state");
                try
                {
                    // сохраняем резерв
                    final Map<Long, Entrant> reserveEntrants = new HashMap<>();
                    for (final IEntrantRateRow entrantRow : EnrollmentCompetitionGroupDAO.this.getEntrantRateRows4Add(distrib, null))
                    {
                        reserveEntrants.put(entrantRow.getEntrant().getId(), entrantRow.getEntrant());
                    }

                    for (final Entrant entrant : reserveEntrants.values())
                    {
                        final EcgReserveRelation rel = new EcgReserveRelation();
                        rel.setEntrant(entrant);
                        rel.setDistribution(distrib);
                        session.save(rel);
                    }

                    session.flush();
                } finally
                {
                    Debug.end();
                }

            });
        }
    }

    @Override
    public void checkEditable(final EcgDistribObject distrib) throws ApplicationException
    {
        if (distrib.isApproved())
        {
            throw new ApplicationException("Нельзя изменять утвержденное распределение.");
        }

        /* нельзя создавать новые распределения, если у утвержденный распределений (того же уровня) есть уточняющие, которые не распределены */
        {
            final MQBuilder builder = new MQBuilder(EcgDistribObjectGen.ENTITY_CLASS, "c");
            builder.add(MQExpression.eq("e", EcgDistribObjectGen.L_COMPETITION_GROUP, distrib.getCompetitionGroup()));
            builder.add(MQExpression.eq("e", EcgDistribObjectGen.L_COMPENSATION_TYPE, distrib.getCompensationType()));
            builder.add(MQExpression.eq("e", EcgDistribObjectGen.P_SECOND_HIGH_ADMISSION, distrib.isSecondHighAdmission()));
            builder.add(MQExpression.eq("c", EcgDistribObjectGen.P_APPROVED, Boolean.FALSE));

            builder.addJoin("c", EcgDistribObjectGen.L_PARENT, "e");
            builder.add(MQExpression.eq("e", EcgDistribObjectGen.P_APPROVED, Boolean.TRUE));
            if (null == distrib.getParent())
            {
                builder.add(MQExpression.isNull("e", EcgDistribObjectGen.L_PARENT));
            } else
            {
                builder.add(MQExpression.eq("e", EcgDistribObjectGen.L_PARENT, distrib.getParent()));
            }

            final List<EcgDistribObject> l = builder.getResultList(this.getSession());
            if (l.size() > 0)
            {
                final EcgDistribObject c = l.get(0);
                throw new ApplicationException("Вы не можете " + (null == distrib.getId() ? "создать" : "изменить") + " распределение, поскольку в утвержденном распределении «" + c.getParent().getTitle() + "» есть неутвержденное уточняющее распределение «" + c.getTitle() + "»");
            }
        }

        if (null != distrib.getParent())
        {
            /* нельзя создавать / изменять уточняющие распределения, если уже есть утвержденные */

            final MQBuilder b = new MQBuilder(EcgDistribObjectGen.ENTITY_CLASS, "d");
            b.add(MQExpression.eq("d", EcgDistribObjectGen.L_PARENT, distrib.getParent()));
            b.add(MQExpression.eq("d", EcgDistribObjectGen.P_APPROVED, Boolean.TRUE));

            final List<EcgDistribObject> l = b.getResultList(this.getSession());
            if (l.size() > 0)
            {
                final EcgDistribObject c = l.get(0);
                if (!c.getId().equals(distrib.getId()))
                {
                    throw new ApplicationException("Вы не можете " + (null == distrib.getId() ? "создать" : "изменить") + " уточняющее распределение, поскольку в распределении «" + c.getParent().getTitle() + "» уже есть утвержденное уточняющее распределение «" + c.getTitle() + "».");
                }
            }
        }
    }

    @Override
    public void doDelete(final EcgDistribObject distrib)
    {
        if (!distrib.isApproved())
        {
            // если не утвержден, то все прекрасно
            this.delete(distrib);

        } else
        {
            // если утвержден - то удаляем (просим удалить) все, что было содзано после удаляемого распределения
            if (true)
            {
                final MQBuilder builder = new MQBuilder(EcgDistribObjectGen.ENTITY_CLASS, "e");
                builder.add(MQExpression.eq("e", EcgDistribObjectGen.L_COMPETITION_GROUP, distrib.getCompetitionGroup()));
                builder.add(MQExpression.eq("e", EcgDistribObjectGen.L_COMPENSATION_TYPE, distrib.getCompensationType()));
                builder.add(MQExpression.eq("e", EcgDistribObjectGen.P_SECOND_HIGH_ADMISSION, distrib.isSecondHighAdmission()));
                builder.add(MQExpression.great("e", "id", distrib.getId()));
                final List<EcgDistribObject> result = builder.getResultList(this.getSession());
                if (!result.isEmpty())
                {
                    throw new ApplicationException("По группе «" + distrib.getCompetitionGroup().getTitle() + "» есть уточняющие распределения («" + result.get(0).getTitle() + "»). Прежде, чем удалять основное распределение, необходимо удалить уточняющие.");
                }
                // for (final EcgDistribObject o: result) {
                // delete(o);
                // }
            }
            this.delete(distrib);
        }
    }

    @Override
    public Map<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> getPreliminaryEnrollmentStudentFor(final Collection<RequestedEnrollmentDirection> directions)
    {
        final Map<RequestedEnrollmentDirection, PreliminaryEnrollmentStudent> result = new LinkedHashMap<>();
        final Map<Long, RequestedEnrollmentDirection> entrant2direction = new HashMap<>();
        for (final RequestedEnrollmentDirection dir : directions)
        {
            if (null != entrant2direction.put(dir.getEntrantRequest().getEntrant().getId(), dir))
            {
                throw new IllegalArgumentException("У абитуриента «" + dir.getEntrantRequest().getEntrant().getPerson().getFullFio() + "» указано несколько направлений приема.");
            }
        }

        BatchUtils.execute(entrant2direction.keySet(), DQL.MAX_VALUES_ROW_NUMBER, entrantIds -> {
            final MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "s");
            builder.addJoinFetch("s", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "dir");
            builder.addJoinFetch("dir", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "req");
            builder.addJoinFetch("req", EntrantRequestGen.L_ENTRANT, "e");
            builder.add(MQExpression.in("e", "id", entrantIds));

            for (final PreliminaryEnrollmentStudent s : builder.<PreliminaryEnrollmentStudent>getResultList(EnrollmentCompetitionGroupDAO.this.getSession()))
            {
                final RequestedEnrollmentDirection dir = entrant2direction.get(s.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId());
                if (null == dir)
                {
                    continue;
                }
                if (!s.getStudentCategory().getId().equals(dir.getStudentCategory().getId()))
                {
                    continue;
                }

                if (null != result.put(dir, s))
                {
                    // в данном случае - не сильно фатально (все равно для чтения извлекаем)
                    EnrollmentCompetitionGroupDAO.this.logger.error("У абитуриента «" + dir.getEntrantRequest().getEntrant().getPerson().getFullFio() + "» создано несколько студентов предзачисления с одной категорией обучающегося");

                }
            }
        });

        final Map<String, EntrantEnrollmentOrderType> code2orderType = new HashMap<>();
        for (final EntrantEnrollmentOrderType tp : this.getCatalogItemList(EntrantEnrollmentOrderType.class))
        {
            code2orderType.put(tp.getCode(), tp);
        }

        for (final RequestedEnrollmentDirection dir : directions)
        {
            if (null == result.get(dir))
            {
                final String orderTypeCode = this.getEntrantEnrollmentOrderTypeCode(dir);
                final EntrantEnrollmentOrderType orderType = (null == orderTypeCode ? null : code2orderType.get(orderTypeCode));

                final PreliminaryEnrollmentStudent s = new PreliminaryEnrollmentStudent();
                s.setRequestedEnrollmentDirection(dir);
                s.setEducationOrgUnit(dir.getEnrollmentDirection().getEducationOrgUnit());
                s.setTargetAdmission(dir.isTargetAdmission());
                s.setStudentCategory(dir.getStudentCategory());

                if (null != orderType)
                {
                    s.setEntrantEnrollmentOrderType(orderType);
                    s.setCompensationType(orderType.getCompensationType());
                }

                result.put(s.getRequestedEnrollmentDirection(), s);
            }
        }

        return result;
    }

    /**
     * определяет по направлению подготовки код типа приказа по умолчанию
     */
    protected String getEntrantEnrollmentOrderTypeCode(final RequestedEnrollmentDirection dir)
    {
        return null;
    }

}
