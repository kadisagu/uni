/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic2.MarkDistribution;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution.MarkDistributionDAO;
import ru.tandemservice.uniec.dao.examgroup.logic2.ExamGroupLogicSample2;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public class DAO extends MarkDistributionDAO<Model> implements IDAO
{
    @Override
    protected void prepareModel(Model model)
    {
        // получаем список значений фильтров
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(CompensationType.class)));
        model.setFormativeOrgUnitListModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelsHighSchoolListModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));
    }

    @Override
    protected void prepareGroupList(MQBuilder builder, String alias, Model model)
    {
        // получаем фильтры
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");
        EnrollmentDirection enrollmentDirection = EnrollmentDirectionUtil.getEnrollmentDirection(model, getSession());

        if (compensationType == null || enrollmentDirection == null)
        {
            // не все фильтры выбраны, возвращаем пустой список
            builder.add(MQExpression.isNull(alias, ExamGroup.P_KEY));
        } else
        {
            // создаем ключ
            String key = ExamGroupLogicSample2.createKey(enrollmentDirection, compensationType);

            // применяем фильтр
            builder.add(MQExpression.eq(alias, ExamGroup.P_KEY, key));
        }
    }
}
