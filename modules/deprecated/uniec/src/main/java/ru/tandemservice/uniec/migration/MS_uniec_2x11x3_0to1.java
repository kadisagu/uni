package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x11x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantDailyRatingByED2Report

        // создано обязательное свойство headerWithoutParam
        {
            // создать колонку
            tool.createColumn("enr_rating_by_ed2_report_t", new DBColumn("headerwithoutparam_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update enr_rating_by_ed2_report_t set headerwithoutparam_p=? where headerwithoutparam_p is null", Boolean.TRUE);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr_rating_by_ed2_report_t", "headerwithoutparam_p", false);
        }
    }
}