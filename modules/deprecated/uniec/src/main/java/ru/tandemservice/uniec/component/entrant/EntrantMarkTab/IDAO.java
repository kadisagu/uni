/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author vip_delete
 * @since 23.03.2009
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareRequestedEnrollmentDirectionDataSource(Model model);

    void prepareRequestedEnrollmentDirectionInterviewDataSource(Model model);

    void prepareMarkResultDataSource(Model model);

    void updateEntrantRequestData(Model model, Long entrantRequestId);
}
