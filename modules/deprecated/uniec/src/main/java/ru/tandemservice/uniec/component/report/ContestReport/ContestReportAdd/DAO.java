/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.ContestReport.ContestReportAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.report.ContestReport;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil.DetailLevel;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 12.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayFirstTimeMoment(new Date()));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        // заполняем другие селекты
        model.setCompensationTypeModel(new LazySimpleSelectModel<>(CompensationType.class, CompensationType.shortTitle().s()).setSortProperty(CompensationType.P_CODE));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(StudentCategory.class));

        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setEducationLevelHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(EnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(EnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(EnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(EnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setGroupModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<GroupWrapper> findValues(final String filter)
            {
                List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections(model);
                if (enrollmentDirections.isEmpty())
                {
                    return ListResult.getEmpty();
                }

                EntrantDataUtil dataUtil = getDataUtil(model, enrollmentDirections, null, getSession());

                Set<String> groups = new HashSet<>();
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                {
                    for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                    {
                        for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
                        {
                            // только внутренние формы сдачи
                            if (!examPass.getSubjectPassForm().isInternal() || examPass.getSubjectPassForm().getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                            {
                                continue;
                            }

                            // не учитываем пустые группы
                            if (!StringUtils.isEmpty(direction.getGroup()))
                            {
                                groups.add(direction.getGroup());
                            }
                        }
                    }
                }

                List<GroupWrapper> result = new ArrayList<>();
                for (String group : groups)
                {
                    result.add(new GroupWrapper("id_" + group, group));
                }
                Collections.sort(result, ITitled.TITLED_COMPARATOR);

                return new ListResult<>(result);
            }
        });
        model.setDisciplineModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<DisciplineWrapper> findValues(final String filter)
            {
                List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections(model);
                if (enrollmentDirections.isEmpty())
                {
                    return ListResult.getEmpty();
                }

                EntrantDataUtil dataUtil = getDataUtil(model, enrollmentDirections, getGroup(model), getSession());

                Set<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>> pairs = new HashSet<>();
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                {
                    for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                    {
                        for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen))
                        {
                            // только внутренние формы сдачи
                            if (!examPass.getSubjectPassForm().isInternal() || examPass.getSubjectPassForm().getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                            {
                                continue;
                            }

                            pairs.add(PairKey.create(examPass.getEnrollmentCampaignDiscipline(), examPass.getSubjectPassForm()));
                        }
                    }
                }

                List<DisciplineWrapper> result = new ArrayList<>();
                for (PairKey<Discipline2RealizationWayRelation, SubjectPassForm> pair : pairs)
                {
                    result.add(new DisciplineWrapper(pair.getFirst(), pair.getSecond()));
                }
                Collections.sort(result);

                return new ListResult<>(result);
            }
        });
        reloadModelEntities(model);
    }

    private List<ChosenEntranceDiscipline> getChosenDisciplines(Model model, boolean filterBySelectedDiscipline)
    {
        Session session = getSession();
        EntrantDataUtil util = getDataUtil(model, getEnrollmentDirections(model), getGroup(model), session);
        DisciplineWrapper wrapper = model.getDiscipline();

        List<ChosenEntranceDiscipline> result = new ArrayList<>();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : util.getDirectionSet())
        {
            for (ChosenEntranceDiscipline chosenEntranceDiscipline : util.getChosenEntranceDisciplineSet(requestedEnrollmentDirection))
            {
                if (filterBySelectedDiscipline && !chosenEntranceDiscipline.getEnrollmentCampaignDiscipline().equals(wrapper.getDiscipline()))
                {
                    continue;
                }

                for (ExamPassDiscipline examPassDiscipline : util.getExamPassDisciplineSet(chosenEntranceDiscipline))
                {
                    SubjectPassForm subjectPassForm = examPassDiscipline.getSubjectPassForm();

                    if (filterBySelectedDiscipline && !subjectPassForm.equals(wrapper.getSubjectPassForm()))
                    {
                        continue;
                    }

                    if (subjectPassForm.isInternal() && !subjectPassForm.getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                    {
                        result.add(chosenEntranceDiscipline);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        ContestReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setEnrollmentDirection(UniecDAOFacade.getEntranceEduLevelDAO().getEnrollmentDirection(model));
        report.setStudentCategoryTitle(StringUtils.trimToNull(StringUtils.lowerCase(CommonBaseStringUtil.join(model.getStudentCategoryList(), "title", "; "))));
        report.setCompensationType(model.getCompensationType());
        report.setDisciplineTitle(model.getDiscipline().getTitle());

        if (model.isByAllEnrollmentDirections())
        {
            report.setGroupTitle("все");
            preparePrintReportByAllDirections(model);
        } else
        {
            report.setGroupTitle(model.getGroup().getTitle());
            preparePrintReportForDirection(model);
        }

        session.save(report);
    }

    private void preparePrintReportForDirection(Model model)
    {
        Session session = getSession();
        RtfDocument document = new RtfReader().read(getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_CONTEST_REPORT).getCurrentTemplate());

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        TopOrgUnit academy = TopOrgUnit.getInstance();

        injectModifier.put("highSchoolTitle", getOuTitle(academy));
        injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("discipline", model.getDiscipline().getTitle());
        injectModifier.put("eduLevelType", StringUtils.capitalize(model.getEducationLevelsHighSchool().getEducationLevel().getLevelType().getNominativeCaseSimpleShortTitle()));
        injectModifier.put("eduLevel", model.getEducationLevelsHighSchool().getPrintTitle());
        injectModifier.put("developForm", model.getDevelopForm().getTitle() + " форма");
        injectModifier.put("group", model.getGroup().getTitle());
        injectModifier.put("formativeOrgUnit", getOuTitle(model.getFormativeOrgUnit()));
        injectModifier.put("territorialOrgUnit", getOuTitle(model.getTerritorialOrgUnit()));
        injectModifier.modify(document);

        Set<RequestedEnrollmentDirection> directionsSet = UniBaseUtils.getPropertiesSet(getChosenDisciplines(model, true), ChosenEntranceDiscipline.chosenEnrollmentDirection().s());
        List<RequestedEnrollmentDirection> directionList = new ArrayList<>(directionsSet);
        directionList.sort(CommonCollator.comparing(red -> red.getEntrantRequest().getEntrant().getPerson().getFullFio(), true));
        List<String[]> data = new ArrayList<>();
        int counter = 1;
        for (RequestedEnrollmentDirection direction : directionList)
        {
            addReportRow(data, direction, counter++);
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", data.toArray(new String[data.size()][]));
        tableModifier.modify(document);

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        session.save(content);
        model.getReport().setContent(content);
    }

    private void preparePrintReportByAllDirections(Model model)
    {
        Session session = getSession();

        // сформируем мап по направлениям приема и названиям групп
        Map<EnrollmentDirection, Map<String, List<RequestedEnrollmentDirection>>> directions = new HashMap<>();
        for (ChosenEntranceDiscipline chosenEntranceDiscipline : getChosenDisciplines(model, true))
        {
            EnrollmentDirection direction = chosenEntranceDiscipline.getChosenEnrollmentDirection().getEnrollmentDirection();
            Map<String, List<RequestedEnrollmentDirection>> byDirection = directions.get(direction);
            if (byDirection == null)
            {
                directions.put(direction, byDirection = new HashMap<>());
            }

            String group = chosenEntranceDiscipline.getChosenEnrollmentDirection().getGroup();
            List<RequestedEnrollmentDirection> directionList = byDirection.get(group);
            if (directionList == null)
            {
                byDirection.put(group, directionList = new ArrayList<>());
            }
            directionList.add(chosenEntranceDiscipline.getChosenEnrollmentDirection());
        }

        RtfDocument template = new RtfReader().read(getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_CONTEST_REPORT).getCurrentTemplate());
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setSettings(template.getSettings());
        result.setHeader(template.getHeader());

        for (EnrollmentDirection direction : directions.keySet())
        {
            for (String group : directions.get(direction).keySet())
            {
                RtfDocument page = template.getClone();

                RtfInjectModifier injectModifier = new RtfInjectModifier();
                TopOrgUnit academy = TopOrgUnit.getInstance();

                injectModifier.put("highSchoolTitle", getOuTitle(academy));
                injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
                injectModifier.put("discipline", model.getDiscipline().getTitle());
                EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
                injectModifier.put("eduLevelType", StringUtils.capitalize(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType().getNominativeCaseSimpleShortTitle()));
                injectModifier.put("eduLevel", educationOrgUnit.getEducationLevelHighSchool().getPrintTitle());
                injectModifier.put("developForm", educationOrgUnit.getDevelopForm().getTitle() + " форма");
                injectModifier.put("group", group);
                injectModifier.put("formativeOrgUnit", getOuTitle(educationOrgUnit.getFormativeOrgUnit()));
                injectModifier.put("territorialOrgUnit", getOuTitle(educationOrgUnit.getTerritorialOrgUnit()));
                injectModifier.modify(page);

                List<RequestedEnrollmentDirection> directionList = directions.get(direction).get(group);
                Collections.sort(directionList, new Comparator<RequestedEnrollmentDirection>()
                {
                    @Override
                    public int compare(RequestedEnrollmentDirection o1, RequestedEnrollmentDirection o2)
                    {
                        return getFullFio(o1).compareTo(getFullFio(o2));
                    }

                    private String getFullFio(RequestedEnrollmentDirection requestedEnrollmentDirection)
                    {
                        return requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPerson().getFullFio();
                    }
                });
                List<String[]> data = new ArrayList<>();
                int counter = 1;
                for (RequestedEnrollmentDirection reqDirection : directionList)
                {
                    addReportRow(data, reqDirection, counter++);
                }

                RtfTableModifier tableModifier = new RtfTableModifier();
                tableModifier.put("T", data.toArray(new String[data.size()][]));
                tableModifier.modify(page);

                result.getElementList().addAll(page.getElementList());
            }
        }

        if (result.getElementList().isEmpty())
        {
            throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(result));
        session.save(content);
        model.getReport().setContent(content);
    }

    private String getOuTitle(OrgUnit ou)
    {
        return (ou != null) ? StringUtils.defaultIfEmpty(ou.getNominativeCaseTitle(), ou.getTitle()) : null;
    }

    private static EntrantDataUtil getDataUtil(Model model, List<EnrollmentDirection> enrollmentDirections, String groupTitle, Session session)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.entrantRequest().regDate().s(), model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().s(), model.getEnrollmentCampaign()));
        builder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirections));
        builder.add(MQExpression.isNotNull("d", RequestedEnrollmentDirection.group().s()));
        builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().archival().s(), Boolean.FALSE));
        builder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (model.getCompensationType() != null)
        {
            builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.compensationType().s(), model.getCompensationType()));
        }
        if (model.getStudentCategoryList() != null && !model.getStudentCategoryList().isEmpty())
        {
            builder.add(MQExpression.in("d", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList()));
        }
        if (groupTitle != null)
        {
            builder.add(MQExpression.eq("d", RequestedEnrollmentDirection.P_GROUP, groupTitle));
        }
        return new EntrantDataUtil(session, model.getEnrollmentCampaign(), builder, DetailLevel.EXAM_PASS);
    }

    protected static DateFormatter yearFormatter = new DateFormatter(DateFormatter.PATTERN_JUST_YEAR);

    protected void addReportRow(List<String[]> data, RequestedEnrollmentDirection direction, int number)
    {
        data.add(new String[]{String.valueOf(number), direction.getEntrantRequest().getEntrant().getPerson().getFullFio(), direction.getEntrantRequest().getStringNumber(), yearFormatter.format(direction.getEntrantRequest().getEntrant().getPerson().getIdentityCard().getBirthDate())});
    }

    private List<EnrollmentDirection> getEnrollmentDirections(Model model)
    {
        return (!model.isByAllEnrollmentDirections()) ? MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession()) : getList(EnrollmentDirection.class, EnrollmentDirection.enrollmentCampaign().s(), model.getEnrollmentCampaign());
    }

    private String getGroup(Model model)
    {
        return (model.getGroup() != null) ? model.getGroup().getTitle() : null;
    }
}
