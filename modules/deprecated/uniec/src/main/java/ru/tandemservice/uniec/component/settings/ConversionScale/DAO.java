/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.ConversionScale;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author agolubenko
 * @since 25.06.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        super.prepare(model);
    }

    @Override
    public void prepareMatrices(Model model)
    {
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        if (enrollmentCampaign == null)
        {
            return;
        }

        List<Matrix> matrices = prepareMatrices(enrollmentCampaign);

        model.setMatrices(matrices);
        model.setBudgetMatrix2Data(prepareData(matrices, true)); // в любом случае будут заполнять данные по бюджету
        if (model.isDifferenceExist()) // если есть отличие между бюджетом и контрактом в данной приемной компании
        {
            model.setContractMatrix2Data(prepareData(matrices, false)); // то для контракта отдельные данные
        }
    }

    /**
     * Подготавливает список матриц шкал перевода данной приемной компании для отображения на форме
     * 
     * @param enrollmentCampaign приемная компания
     * @return список шкал перевода сгруппированных в матрицы по следующему принципу:
     * в одну матрицу попадают шкалы перевода для дисциплин, у которых одинаковые минимальный, максимальный балл и шаг  
     */
    private List<Matrix> prepareMatrices(EnrollmentCampaign enrollmentCampaign)
    {
        Map<MultiKey, Matrix> key2Matrix = new HashMap<MultiKey, Matrix>(); // параметры матрицы -> матрица

        for (ConversionScale conversionScale : getConversionScales(enrollmentCampaign)) // для каждой шкалы перевода приемной компании
        {
            Discipline2RealizationWayRelation discipline = conversionScale.getDiscipline(); // из ее дисциплины
            int minMark = discipline.getMinMark();
            int maxMark = discipline.getMaxMark();
            double step = discipline.getStep();

            MultiKey key = new MultiKey(minMark, maxMark, step); // получить параметры
            Matrix matrix = key2Matrix.get(key); // и по ним матрицу

            if (matrix == null) // если матрица с такими параметрами не существует
            {
                matrix = new Matrix(minMark, maxMark, step); // создать ее
                key2Matrix.put(key, matrix);
            }

            matrix.addConversionScale(conversionScale); // и добавить эту шкалу перевода в соответствующую матрицу 
        }

        ArrayList<Matrix> result = new ArrayList<Matrix>(key2Matrix.values());
        Collections.sort(result, new Comparator<Matrix>() // сортировать матрицы по возрастанию количества колонок
        {
            @Override
            public int compare(Matrix o1, Matrix o2)
            {
                int i1 = o1.getHorizontalObjects().size();
                int i2 = o2.getHorizontalObjects().size();
                return (i1 < i2 ? -1 : (i1 == i2 ? 0 : 1));
            }
        });
        return result;
    }

    /**
     * @param enrollmentCampaign приемная компания
     * @return шкалы перевода заданные для данной приемной компании
     */
    @SuppressWarnings("unchecked")
    private List<ConversionScale> getConversionScales(EnrollmentCampaign enrollmentCampaign)
    {
        Criteria criteria = getSession().createCriteria(ConversionScale.class);
        criteria.createAlias(ConversionScale.L_DISCIPLINE, "discipline");
        criteria.add(Restrictions.eq("discipline." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        criteria.setFetchMode("discipline." + Discipline2RealizationWayRelation.L_EDUCATION_SUBJECT, FetchMode.JOIN);
        criteria.setFetchMode("disciplien." + Discipline2RealizationWayRelation.L_SUBJECT_PASS_WAY, FetchMode.JOIN);
        List<ConversionScale> result = (List<ConversionScale>) criteria.list();

        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        return result;
    }

    /**
     * Подготавливает данные матриц для отображения на форме
     * 
     * @param matrices список матриц
     * @param budget true - бюджет, false - контракт
     * @return [Матрица] -> [Ранее сохраненные данные этой матрицы]
     */
    private Map<Matrix, Map<CellCoordinates, Integer>> prepareData(List<Matrix> matrices, boolean budget)
    {
        Map<Matrix, Map<CellCoordinates, Integer>> result = new HashMap();
        for (Matrix matrix : matrices)
        {
            result.put(matrix, prepareData(matrix, budget));
        }
        return result;
    }

    /**
     * Подготавливает данные матрицы для отображения на форме
     * 
     * @param matrix матрица
     * @param budget true - бюджет, false - контракт
     * @return ранее сохраненные данные этой матрицы
     */
    private Map<CellCoordinates, Integer> prepareData(Matrix matrix, boolean budget)
    {
        Map<CellCoordinates, Integer> result = new HashMap();
        for (ConversionScale conversionScale : matrix.getVerticalObjects()) // для каждой шкалы перевода (строки) из матрицы
        {
            Discipline2RealizationWayRelation discipline = conversionScale.getDiscipline();
            byte[] scale = conversionScale.getScale(budget);
            for (int i = 0; i < scale.length; i++)
            {
                result.put(new CellCoordinates(conversionScale.getId(), ScaleColumn.getId((discipline.getMinMark() + discipline.getStep() * i))), (int) scale[i]);
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateBudget(Model model)
    {
        updateConversionScales(model.getBudgetMatrix2Data(), true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateContract(Model model)
    {
        updateConversionScales(model.getContractMatrix2Data(), false);
    }

    /**
     * Обновляет данные шкал перевода
     * 
     * @param matrix2DataMap [Матрица] -> [Введенные данные]
     * @param budget true бюджет, false контракт
     */
    private void updateConversionScales(Map<Matrix, Map<CellCoordinates, Integer>> matrix2DataMap, boolean budget)
    {
        for (Entry<Matrix, Map<CellCoordinates, Integer>> matrix2Data : matrix2DataMap.entrySet())
        {
            Matrix matrix = matrix2Data.getKey();

            Map<ConversionScale, Byte[]> data = transformData(matrix, matrix2Data.getValue());
            for (Entry<ConversionScale, Byte[]> entry : data.entrySet())
            {
                ConversionScale conversionScale = entry.getKey();
                Byte[] row = entry.getValue();

                if (row.length != 0 && row[1] != null) // валидация была, так что проверяем, что строка полностью заполнена
                {
                    byte[] marks = new byte[row.length];
                    for (int i = 0; i < row.length; i++)
                    {
                        marks[i] = row[i]; // Byte -> byte
                    }
                    conversionScale.setScale(marks, budget);
                }
                else
                {
                    conversionScale.setScale(new byte[0], budget);
                }
            }

            /*for (ConversionScale conversionScale : matrix.getVerticalObjects())
            {
                conversionScale.setScale(new byte[matrix.getHorizontalObjects().size()], budget);
            }

            for (Entry<CellCoordinates, Long> entry : matrix2Data.getValue().entrySet())
            {
                Long value = entry.getValue();
                if (value != null)
                {
                    Long conversionScaleId = entry.getKey().getX();
                    double internalMark = ScaleColumn.getMark(entry.getKey().getY());
                    byte externalMark = value.byteValue();

                    matrix.getConversionScale(conversionScaleId).setTransformation(internalMark, externalMark, budget);
                }
            }*/
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBudget(Model model, ErrorCollector errorCollector)
    {
        validate(model.getBudgetMatrix2Data(), errorCollector);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateContract(Model model, ErrorCollector errorCollector)
    {
        validate(model.getContractMatrix2Data(), errorCollector);
    }

    /**
     * Валидирует данные введенные в матрицы формы
     * 
     * @param matrix2Data [Матрица] -> [Ее данные]
     */
    private void validate(Map<Matrix, Map<CellCoordinates, Integer>> matrix2Data, ErrorCollector errorCollector)
    {
        for (Entry<Matrix, Map<CellCoordinates, Integer>> entry : matrix2Data.entrySet())
        {
            validate(entry.getKey(), entry.getValue(), errorCollector);
        }
    }

    /**
     * Валидирует введенные в матрицу данные
     * 
     * @param matrix матрица
     * @param data данные
     */
    private void validate(Matrix matrix, Map<CellCoordinates, Integer> data, ErrorCollector errorCollector)
    {
        String matrixId = matrix.getId();
        List<ConversionScale> conversionScales = matrix.getVerticalObjects();
        Map<ConversionScale, Byte[]> conversionScale2Row = transformData(matrix, data);

        for (int conversionScaleIndex = 0; conversionScaleIndex < conversionScales.size(); conversionScaleIndex++) // проверяем каждую строку отдельно
        {
            Byte[] row = conversionScale2Row.get(conversionScales.get(conversionScaleIndex));
            validate(row, matrixId, conversionScaleIndex, errorCollector);
        }
    }

    /**
     * Валидирует введенные в строку матрицы данные 
     * 
     * @param row строка баллов в матрице
     * @param matrixId идентификатор матрицы
     * @param conversionScaleIndex номер шкалы перевода в матрице
     */
    private void validate(Byte[] row, String matrixId, int conversionScaleIndex, ErrorCollector errorCollector)
    {
        if (validateNotFilledFields(row, matrixId, conversionScaleIndex, errorCollector)) // если нет ошибок с пустыми полями
        {
            validateInversedFields(row, matrixId, conversionScaleIndex, errorCollector); /// проверить, что каждое последующее значение не меньше предыдущего
        }
    }

    /**
     * Валидирует введенные в матрицу данные и добавляет пользовательские ошибки в том случае, 
     * если пользователь ввел в строку значение, но не во все поля этой строки 
     * 
     * @param row строка баллов в матрице
     * @param matrixId идентификатор матрицы
     * @param conversionScaleIndex номер шкалы перевода в матрице
     * @return true если нужна дальнейшая проверка, false в противном случае
     */
    private boolean validateNotFilledFields(Byte[] row, String matrixId, int conversionScaleIndex, ErrorCollector errorCollector)
    {
        List<Integer> nullValueIndexes = new ArrayList<Integer>();

        for (int index = 0; index < row.length; index++)
        {
            if (row[index] == null) // ищем поля в которые ничего не введено
            {
                nullValueIndexes.add(index);
            }
        }

        if (!(nullValueIndexes.isEmpty() || nullValueIndexes.size() == row.length)) // строка матрицы должна быть либо пустой, либо целиком заполненной
        {
            List<String> fieldIds = new ArrayList<String>();
            for (Integer nullValueIndex : nullValueIndexes)
            {
                fieldIds.add(matrixId + "_" + conversionScaleIndex + "_" + nullValueIndex);
            }
            errorCollector.add("Необходимо заполнить значениями все поля для данной дисциплины", fieldIds.toArray(new String[0]));
        }
        return (nullValueIndexes.isEmpty());
    }

    /**
     * Валидирует введенные в матрицу данные и добавляет пользовательские ошибки в том случае, 
     * если пользователь ввел в два соседних поля значения так, что последующее значение меньше предыдущего
     * 
     * @param row строка баллов в матрице
     * @param matrixId идентификатор матрицы
     * @param conversionScaleIndex номер шкалы перевода в матрице
     */
    private void validateInversedFields(Byte[] row, String matrixId, int conversionScaleIndex, ErrorCollector errorCollector)
    {
        List<Integer> inverseValueIndexes = new ArrayList<Integer>();
        for (int index = 0; index < row.length; index++)
        {
            if (index > 0 && row[index] < row[index - 1]) // ищем соседние поля, где последующее значение меньше предыдущего
            {
                inverseValueIndexes.add(index);
            }
        }

        if (!inverseValueIndexes.isEmpty())
        {
            List<String> fieldIds = new ArrayList<String>(inverseValueIndexes.size());
            for (Integer inverseValueIndex : inverseValueIndexes)
            {
                fieldIds.add(matrixId + "_" + conversionScaleIndex + "_" + inverseValueIndex);
            }
            errorCollector.add("Большему числу баллов должно соответствовать большее значение в шкале перевода", fieldIds.toArray(new String[0]));
        }
    }

    /**
     * Преобразовывает данные ячеек матрицы в более удобный формат
     * 
     * @param matrix матрица
     * @param data данные ячеек матрицы
     * @return [Шкала перевода] -> [Массив баллов]
     */
    private Map<ConversionScale, Byte[]> transformData(Matrix matrix, Map<CellCoordinates, Integer> data)
    {
        Map<ConversionScale, Byte[]> result = new HashMap<ConversionScale, Byte[]>();

        for (ConversionScale conversionScale : matrix.getVerticalObjects()) // для каждой шкалы
        {
            result.put(conversionScale, new Byte[matrix.getHorizontalObjects().size()]); // подготавливаем пустой массив
        }

        for (Entry<CellCoordinates, Integer> entry : data.entrySet()) // для каждой ячейки матрицы
        {
            CellCoordinates cellCoordinates = entry.getKey();
            Integer inputValue = (Integer) entry.getValue();

            ConversionScale conversionScale = matrix.getConversionScale(cellCoordinates.getRowId());
            double internalMark = ScaleColumn.getMark(cellCoordinates.getColumnId());
            Byte externalMark = (inputValue != null) ? inputValue.byteValue() : null;

            int index = conversionScale.getIndexNumber(internalMark);
            Byte[] byteArray = result.get(conversionScale);
            if (index >= 0 && index < byteArray.length)
                byteArray[index] = externalMark; // запомнить введенное значение по нужному индексу
        }

        return result;
    }
}
