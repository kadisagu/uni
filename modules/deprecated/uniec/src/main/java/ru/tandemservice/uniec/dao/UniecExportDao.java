/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IEntityExportDAO.EntityExporter;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;

import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since May 7, 2010
 */
public class UniecExportDao extends UniBaseDao implements IUniecExportDao
{
    @Override
    public EntityExporter<EnrollmentCampaign> createEnrollmentCampaignExporter()
    {
        return () -> getOpenedEnrollmentCampaigns().iterator();
    }

    @Override
    public EntityExporter<EnrollmentDirection> createEnrollmentDirectionExporter()
    {
        return () -> (Iterator) new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
                .where(in(property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")),
                          getOpenedEnrollmentCampaigns()))
                .where(or(
                        eq(property(EnrollmentDirection.budget().fromAlias("e")), value(Boolean.TRUE)),
                        eq(property(EnrollmentDirection.contract().fromAlias("e")), value(Boolean.TRUE))
                )).createStatement(getSession()).list().iterator();
    }

    @Override
    public EntityExporter<CompetitionGroup> createСompetitionGroupExporter()
    {
       return () -> getList(CompetitionGroup.class, CompetitionGroup.enrollmentCampaign(), getOpenedEnrollmentCampaigns()).iterator();
    }

    @Override
    public EntityExporter<Qualification2CompetitionKindRelation> createQualification2CompetitionKindRelationExporter()
    {
        return () -> getList(Qualification2CompetitionKindRelation.class,
                             Qualification2CompetitionKindRelation.enrollmentCampaign(),
                             getOpenedEnrollmentCampaigns()).iterator();
    }

    @Override
    public EntityExporter<EnrollmentCompetitionKind> createEnrollmentCompetitionKindExporter()
    {
        String alias = "ecd";
        return () -> new DQLSelectBuilder()
                   .fromEntity(EnrollmentCompetitionKind.class, alias)
                   .column(property(alias))
                   .where(in(property(alias, EnrollmentCompetitionKind.enrollmentCampaign()), getOpenedEnrollmentCampaigns()))
                   .where(isNotNull(property(alias, EnrollmentCompetitionKind.competitionKind())))
                   .where(eq(property(alias, EnrollmentCompetitionKind.used()), value(true)))
                   .createStatement(getSession()).<EnrollmentCompetitionKind>list()
                   .iterator();
    }

    protected EnrollmentCampaign getLastEnrollmentCampaign()
    {
        Criteria criteria = getSession().createCriteria(EnrollmentCampaign.class);
        criteria.addOrder(Order.desc(EnrollmentCampaign.id().s()));
        criteria.setMaxResults(1);
        return (EnrollmentCampaign) criteria.uniqueResult();
    }

    protected List<EnrollmentCampaign> getOpenedEnrollmentCampaigns()
    {
        return getList(EnrollmentCampaign.class, EnrollmentCampaign.closed(), false);
    }
}
