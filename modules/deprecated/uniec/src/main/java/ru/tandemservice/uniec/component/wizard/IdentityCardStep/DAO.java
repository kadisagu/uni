/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.IdentityCardStep;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.dao.IComponentBaseDAO;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.fias.base.entity.gen.ICitizenshipGen;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.util.CongenialStringFinder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

import java.util.Calendar;
import java.util.Date;

/**
 * @author vip_delete
 * @since 14.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepare(Model model)
    {
        ru.tandemservice.uniec.component.entrant.EntrantAdd.Model entrantModel = model.getEntrantModel();
        org.tandemframework.shared.person.base.bo.Person.ui.AddressEdit.Model addressModel = model.getAddressModel();

        ((IComponentBaseDAO) BusinessComponentUtils.getDao(IUniecComponents.ENTRANT_ADD)).prepare(entrantModel);

        // если передали удостоверение личности в компонент
        IdentityCard templateIdentityCard = model.getIdentityCard();
        IdentityCard identityCard = entrantModel.getIdentityCard();
        if (templateIdentityCard != null)
            identityCard.update(templateIdentityCard);

        OnlineEntrant onlineEntrant = model.getEntrantModel().getOnlineEntrant();
        if (onlineEntrant.getId() != null)
        {
            addressModel.setAddressEquals(model.getEntrantModel().getOnlineEntrant().isLiveAtRegistration());
            if (onlineEntrant.getPassportCitizenship() != null)
                identityCard.setCitizenship(onlineEntrant.getPassportCitizenship());
        }

        if (identityCard.getCitizenship() == null)
            identityCard.setCitizenship(identityCard.getCardType().getCitizenshipDefault());
    }

    // IDAO

    @Override
    public void createOrFindSimilar(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        final Session session = getSession();
        final Entrant entrant = model.getEntrantModel().getEntrant();
        final Person person = model.getEntrantModel().getPerson();
        final IdentityCard identityCard = person.getIdentityCard();

        // FIX FIO
        if (StringUtils.isNotEmpty(identityCard.getFirstName()))
            identityCard.setFirstName(StringUtils.capitalize(identityCard.getFirstName().trim()));
        if (StringUtils.isNotEmpty(identityCard.getLastName()))
            identityCard.setLastName(StringUtils.capitalize(identityCard.getLastName().trim()));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            identityCard.setMiddleName(StringUtils.capitalize(identityCard.getMiddleName().trim()));

        // VALIDATE

        int amount = -13;
        if (identityCard.getBirthDate() != null && identityCard.getBirthDate().after(CoreDateUtils.add(CoreDateUtils.getDayFirstTimeMoment(new Date()), Calendar.YEAR, amount)))
            errors.add("Абитуриент должен быть старше 13 лет.", "birthDate");

        PersonManager.instance().dao().checkIdentityCard(identityCard, errors);
        if (entrant.getRegistrationDate().after(new Date()))
            errors.add("Дата добавления должна быть не позже сегодняшней.", "registrationDate");

        if (errors.hasErrors())
            return;

        // FIND SIMILAR PERSONS

        String[] properties = new String[]{IdentityCard.P_LAST_NAME, IdentityCard.P_FIRST_NAME, IdentityCard.P_SERIA, IdentityCard.P_NUMBER};
        model.getEntrantModel().setSimilarPersons(CongenialStringFinder.getCongenialIdentityCards(identityCard, properties, 3, session));

        if (model.getEntrantModel().getSimilarPersons().isEmpty())
            createEntrant(model);
    }

    @Override
    public void createEntrant(Model model)
    {
        final Session session = getSession();
        final Entrant entrant = model.getEntrantModel().getEntrant();
        final Person person = model.getEntrantModel().getPerson();
        final IdentityCard identityCard = person.getIdentityCard();
        final AddressBase address = model.getAddressModel().getAddress();

        PersonManager.instance().dao().checkPersonUnique(person);

        // сохраняем адрес регистрации (если указан город)
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(identityCard, AddressBaseUtils.getSameAddress(address), IdentityCard.L_ADDRESS, true);

        // сохраняем адрес фактический (если он совпадает с адресом регистрации)
        if (model.getAddressModel().isAddressEquals())
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(person, AddressBaseUtils.getSameAddress(address), Person.L_ADDRESS, true);
        else if (person.getAddress() != null)
            session.saveOrUpdate(person.getAddress());

        identityCard.setPhoto(new DatabaseFile());
        session.save(person.getContactData());
        session.save(identityCard.getPhoto()); //сохраняем фотку
        session.save(identityCard); //сохраняем удостоверение личности
        session.save(person); //cохраняем персону

        identityCard.setPerson(person);
        session.update(identityCard); //сохраняем персону в удостоверении личности

        // CREATE ENTRANT

        entrant.setPersonalNumber(EcCampaignManager.instance().dao().getUniqueEntrantNumber(entrant.getEnrollmentCampaign()));
        entrant.setPerson(person);
        session.save(entrant);

        // сохраняем ссылку в онлайн-абитуриенте
        OnlineEntrant onlineEntrant = model.getEntrantModel().getOnlineEntrant();
        if (onlineEntrant.getId() != null)
        {
            onlineEntrant.setEntrant(entrant);
            session.update(onlineEntrant);
        }
    }

    @Override
    public void createBySimilar(Entrant entrant, Long basisPersonId, OnlineEntrant onlineEntrant)
    {
        Person person = getNotNull(Person.class, basisPersonId);

        if (existsEntity(Entrant.class, Entrant.L_PERSON, person, Entrant.L_ENROLLMENT_CAMPAIGN, entrant.getEnrollmentCampaign()))
            throw new ApplicationException("Данная персона уже является абитуриентом в выбранной приемной кампании.");

        Session session = getSession();
        EnrollmentCampaign enrollmentCampaign = get(EnrollmentCampaign.class, entrant.getEnrollmentCampaign().getId());

        entrant.setPersonalNumber(EcCampaignManager.instance().dao().getUniqueEntrantNumber(enrollmentCampaign));
        entrant.setPerson(person);
        session.save(entrant);

        if (onlineEntrant != null && onlineEntrant.getId() != null)
        {
            onlineEntrant.setEntrant(entrant);
            session.update(onlineEntrant);
        }
    }
}
