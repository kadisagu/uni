/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.bo.EcProfileDistribution;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.logic.EcProfileDistributionDao;
import ru.tandemservice.uniec.base.bo.EcProfileDistribution.logic.IEcProfileDistributionDao;

/**
 * @author Vasily Zhukov
 * @since 05.05.2012
 */
@Configuration
public class EcProfileDistributionManager extends BusinessObjectManager
{
    public static EcProfileDistributionManager instance()
    {
        return instance(EcProfileDistributionManager.class);
    }

    @Bean
    public IEcProfileDistributionDao dao()
    {
        return new EcProfileDistributionDao();
    }

    public static final DataWrapper DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS = new DataWrapper(0L, "Студент/Слушатель");
    public static final DataWrapper DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION = new DataWrapper(1L, "Второе высшее");
}
