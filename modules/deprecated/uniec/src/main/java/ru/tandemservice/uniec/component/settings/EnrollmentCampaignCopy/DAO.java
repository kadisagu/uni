// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.EnrollmentCampaignCopy;

import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.*;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 14.05.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEducationYearList(new EducationYearModel());

        final List<EnrollmentCampaign> enrollmentCampaigns = new MQBuilder(EnrollmentCampaign.ENTITY_CLASS, "e").addDescOrder("e", EnrollmentCampaign.P_ID).getResultList(getSession());
        if ((model.getCampaignToCopy() == null) && !enrollmentCampaigns.isEmpty())
            model.setCampaignToCopy(enrollmentCampaigns.get(0));
        model.setCampaignList(enrollmentCampaigns);

        EducationYear educationYear = get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE);
        EducationYear nextYear = get(EducationYear.class, EducationYear.P_INT_VALUE, educationYear.getIntValue() + 1);
        model.setEduYear(nextYear);
        if (nextYear != null)
        {
            model.setTitle(nextYear.getTitle());
        }
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (!model.getPeriod().getDateFrom().before(model.getPeriod().getDateTo()))
        {
            errors.add("Дата начала должна быть раньше даты окончания", "dateFrom", "dateTo");
            return;
        }

        EnrollmentCampaign enrollmentCampaign = new EnrollmentCampaign();

        // Заполняем все флаги приемной кампании
        EnrollmentCampaign campaignToCopy = model.getCampaignToCopy();
        enrollmentCampaign.update(campaignToCopy);
        enrollmentCampaign.setTitle(model.getTitle());
        enrollmentCampaign.setEducationYear(model.getEduYear());
//        enrollmentCampaign.setOlympiadDiplomaForDirection(true);

        Session session = getSession();

        session.save(enrollmentCampaign);
        model.getPeriod().setEnrollmentCampaign(enrollmentCampaign);
        session.save(model.getPeriod());

        // ограничения
        List<EnrollmentDirectionRestriction> existingDirectionsRestrictions = getList(EnrollmentDirectionRestriction.class, EnrollmentDirectionRestriction.enrollmentCampaign(), campaignToCopy);
        for (EnrollmentDirectionRestriction item : existingDirectionsRestrictions)
        {
            EnrollmentDirectionRestriction newRestriction = new EnrollmentDirectionRestriction();
            newRestriction.update(item);
            newRestriction.setEnrollmentCampaign(enrollmentCampaign);
            session.save(newRestriction);
        }
        List<CompetitionGroupRestriction> existingCompetitionGroupRestrictions = getList(CompetitionGroupRestriction.class, CompetitionGroupRestriction.enrollmentCampaign(), campaignToCopy);
        for (CompetitionGroupRestriction item : existingCompetitionGroupRestrictions)
        {
            CompetitionGroupRestriction newRestriction = new CompetitionGroupRestriction();
            newRestriction.update(item);
            newRestriction.setEnrollmentCampaign(enrollmentCampaign);
            session.save(newRestriction);
        }

        // Используемые виды конкурса
        for (EnrollmentCompetitionKind priority : getList(EnrollmentCompetitionKind.class, EnrollmentCompetitionKind.enrollmentCampaign().s(), campaignToCopy))
        {
            EnrollmentCompetitionKind newPriority = new EnrollmentCompetitionKind();
            newPriority.setCompetitionKind(priority.getCompetitionKind());
            newPriority.setEnrollmentCampaign(enrollmentCampaign);
            newPriority.setPriority(priority.getPriority());
            newPriority.setUsed(priority.isUsed());
            session.save(newPriority);
        }

        // Виды конкурса для категорий поступающих и квалификаций
        for (Qualification2CompetitionKindRelation relation : getList(Qualification2CompetitionKindRelation.class, Qualification2CompetitionKindRelation.enrollmentCampaign().s(), campaignToCopy))
        {
            Qualification2CompetitionKindRelation newRelation = new Qualification2CompetitionKindRelation();
            newRelation.setCompetitionKind(relation.getCompetitionKind());
            newRelation.setQualification(relation.getQualification());
            newRelation.setStudentCategory(relation.getStudentCategory());
            newRelation.setEnrollmentCampaign(enrollmentCampaign);
            session.save(newRelation);
        }

        // Используемые типы приказов о зачислении
        for (EnrollmentOrderType type : getList(EnrollmentOrderType.class, EnrollmentOrderType.enrollmentCampaign().s(), campaignToCopy))
        {
            EnrollmentOrderType newType = new EnrollmentOrderType();
            newType.setEntrantEnrollmentOrderType(type.getEntrantEnrollmentOrderType());
            newType.setEnrollmentCampaign(enrollmentCampaign);
            newType.setPriority(type.getPriority());
            newType.setUsed(type.isUsed());
            session.save(newType);
        }

        // Зачетные баллы по предметам ЕГЭ
        for (StateExamSubjectPassScore passScore : getList(StateExamSubjectPassScore.class, StateExamSubjectPassScore.enrollmentCampaign().s(), campaignToCopy))
        {
            StateExamSubjectPassScore newPassScore = new StateExamSubjectPassScore();
            newPassScore.setEnrollmentCampaign(enrollmentCampaign);
            newPassScore.setMark(passScore.getMark());
            newPassScore.setStateExamSubject(passScore.getStateExamSubject());
            session.save(newPassScore);
        }

        // Вступительные испытания (включая все ее 5 штук под-настроек)
        // Discipline2RealizationWayRelation - дисциплины
        Map<SetDiscipline, SetDiscipline> disciplines = new HashMap<SetDiscipline, SetDiscipline>();
        for (Discipline2RealizationWayRelation discipline : getList(Discipline2RealizationWayRelation.class, Discipline2RealizationWayRelation.enrollmentCampaign().s(), campaignToCopy))
        {
            Discipline2RealizationWayRelation newDiscipline = new Discipline2RealizationWayRelation();
            newDiscipline.update(discipline);
            newDiscipline.setEnrollmentCampaign(enrollmentCampaign);
            session.save(newDiscipline);
            disciplines.put(discipline, newDiscipline);
        }
        // Discipline2RealizationFormRelation - формы сдачи
        for (Discipline2RealizationFormRelation rel : getList(Discipline2RealizationFormRelation.class, Discipline2RealizationFormRelation.discipline().enrollmentCampaign().s(), campaignToCopy))
        {
            Discipline2RealizationFormRelation newRel = new Discipline2RealizationFormRelation();
            newRel.update(rel);
            newRel.setDiscipline((Discipline2RealizationWayRelation) disciplines.get(rel.getDiscipline()));
            session.save(newRel);
        }
        // DisciplinesGroup, Group2DisciplineRelation - группы дисциплин
        for (DisciplinesGroup group : getList(DisciplinesGroup.class, DisciplinesGroup.enrollmentCampaign().s(), campaignToCopy))
        {
            DisciplinesGroup newGroup = new DisciplinesGroup();
            newGroup.update(group);
            newGroup.setEnrollmentCampaign(enrollmentCampaign);
            disciplines.put(group, newGroup);
            session.save(newGroup);
        }
        for (Group2DisciplineRelation rel : getList(Group2DisciplineRelation.class, Group2DisciplineRelation.discipline().enrollmentCampaign().s(), campaignToCopy))
        {
            Group2DisciplineRelation newRel = new Group2DisciplineRelation();
            newRel.setDiscipline((Discipline2RealizationWayRelation) disciplines.get(rel.getDiscipline()));
            newRel.setGroup((DisciplinesGroup) disciplines.get(rel.getGroup()));
            session.save(newRel);
        }
        // ConversionScale - шкала перевода
        for (ConversionScale scale : getList(ConversionScale.class, ConversionScale.discipline().enrollmentCampaign().s(), campaignToCopy))
        {
            ConversionScale newScale = new ConversionScale();
            newScale.update(scale);
            newScale.setDiscipline((Discipline2RealizationWayRelation) disciplines.get(scale.getDiscipline()));
            session.save(newScale);
        }
        // Используемые документы для подачи в ОУ
        for (UsedEnrollmentDocument settings : getList(UsedEnrollmentDocument.class, UsedEnrollmentDocument.enrollmentCampaign().s(), campaignToCopy))
        {
            UsedEnrollmentDocument newSettings = new UsedEnrollmentDocument();
            newSettings.update(settings);
            newSettings.setEnrollmentCampaign(enrollmentCampaign);
            session.save(newSettings);
        }

        // те же самые направления приема с такими же наборами вступительных испытаний для них,
        // только без заданных планов приема и со сброшенным признаком собеседования
        // EnrollmentDirection  entranceDiscipline  entranceDiscipline2SetDisciplineRelation
        Map<EnrollmentDirection, EnrollmentDirection> directionMap = new HashMap<EnrollmentDirection, EnrollmentDirection>();
        for (EnrollmentDirection direction : getList(EnrollmentDirection.class, EnrollmentDirection.enrollmentCampaign().s(), campaignToCopy))
        {
            EnrollmentDirection newDirection = new EnrollmentDirection();
            newDirection.setEnrollmentCampaign(enrollmentCampaign);
            newDirection.setEducationOrgUnit(direction.getEducationOrgUnit());
            directionMap.put(direction, newDirection);
            session.save(newDirection);
        }
        Map<EntranceDiscipline, EntranceDiscipline> entranceDisciplineMap = new HashMap<EntranceDiscipline, EntranceDiscipline>();
        for (EntranceDiscipline discipline : getList(EntranceDiscipline.class, EntranceDiscipline.enrollmentDirection().enrollmentCampaign().s(), campaignToCopy))
        {
            EntranceDiscipline newDiscipline = new EntranceDiscipline();
            newDiscipline.update(discipline);
            newDiscipline.setEnrollmentDirection(directionMap.get(discipline.getEnrollmentDirection()));
            newDiscipline.setDiscipline(disciplines.get(discipline.getDiscipline()));
            entranceDisciplineMap.put(discipline, newDiscipline);
            session.save(newDiscipline);
        }
        for (EntranceDiscipline2SetDisciplineRelation rel : getList(EntranceDiscipline2SetDisciplineRelation.class, EntranceDiscipline2SetDisciplineRelation.setDiscipline().enrollmentCampaign().s(), campaignToCopy))
        {
            EntranceDiscipline2SetDisciplineRelation newRel = new EntranceDiscipline2SetDisciplineRelation();
            newRel.setEntranceDiscipline(entranceDisciplineMap.get(rel.getEntranceDiscipline()));
            newRel.setSetDiscipline(disciplines.get(rel.getSetDiscipline()));
            session.save(newRel);
        }

        //Используемые индивидуальные достижения
        for(EnrCampaignEntrantIndividualProgress rel : getList(EnrCampaignEntrantIndividualProgress.class, EnrCampaignEntrantIndividualProgress.enrollmentCampaign().s(), campaignToCopy))
        {
            EnrCampaignEntrantIndividualProgress newRel = new EnrCampaignEntrantIndividualProgress();
            newRel.update(rel);
            newRel.setEnrollmentCampaign(enrollmentCampaign);
            session.save(newRel);
        }
    }
}
