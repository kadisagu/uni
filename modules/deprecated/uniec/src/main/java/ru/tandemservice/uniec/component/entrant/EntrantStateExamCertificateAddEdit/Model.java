/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

import java.util.List;
import java.util.Map;

import static ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateAddEdit.Controller.*;

/**
 * @author agolubenko
 * @since 02.03.2009
 */
@Input({
        @Bind(key = "entrantId", binding = "certificate.entrant.id"),
        @Bind(key = "certificateId", binding = "certificate.id")
})
public class Model
{
    private EntrantStateExamCertificate _certificate = new EntrantStateExamCertificate();

    {
        _certificate.setEntrant(new Entrant());
    }

    // полный номер свидетельства состоит из трех частей
    private String _territoryCode;
    private String _number;
    private String _year;

    private List<StateExamSubject> _stateExamSubjects;
    private List<StateExamType> _stateExamTypeList;
    private DynamicListDataSource<StateExamSubject> _dataSource;

    private Map<StateExamSubject, StateExamSubjectMark> _markBySubjectMap;

    public String getMarkId()
    {
        return getCurrentSearchListFieldId(MARK_COLUMN_NAME);
    }

    public String getNumberId()
    {
        return getCurrentSearchListFieldId(NUMBER_COLUMN_NAME);
    }

    public String getYearId()
    {
        return getCurrentSearchListFieldId(YEAR_COLUMN_NAME);
    }

    public String getCurrentSearchListFieldId(String field)
    {
        return getSearchListFieldId(field, _stateExamSubjects.indexOf(_dataSource.getCurrentEntity()));
    }

    public static String getSearchListFieldId(String field, int index)
    {
        return SEARCHLIST_FIELD_ID_PREFIX + field + "_" + String.valueOf(index) + "_0";
    }

    public boolean isOldStyle()
    {
        return false;
    }

    // Getters & Setters
    public DynamicListDataSource<StateExamSubject> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StateExamSubject> dataSource)
    {
        _dataSource = dataSource;
    }

    public EntrantStateExamCertificate getCertificate()
    {
        return _certificate;
    }

    public void setCertificate(EntrantStateExamCertificate certificate)
    {
        _certificate = certificate;
    }

    public String getTerritoryCode()
    {
        return _territoryCode;
    }

    public void setTerritoryCode(String territoryCode)
    {
        _territoryCode = territoryCode;
    }

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }

    public String getYear()
    {
        return _year;
    }

    public void setYear(String year)
    {
        _year = year;
    }

    public List<StateExamSubject> getStateExamSubjects()
    {
        return _stateExamSubjects;
    }

    public void setStateExamSubjects(List<StateExamSubject> stateExamSubjects)
    {
        _stateExamSubjects = stateExamSubjects;
    }

    public List<StateExamType> getStateExamTypeList()
    {
        return _stateExamTypeList;
    }

    public void setStateExamTypeList(List<StateExamType> stateExamTypeList)
    {
        _stateExamTypeList = stateExamTypeList;
    }

    public Map<StateExamSubject, StateExamSubjectMark> getMarkBySubjectMap()
    {
        return _markBySubjectMap;
    }

    public void setMarkBySubjectMap(Map<StateExamSubject, StateExamSubjectMark> markBySubjectMap)
    {
        _markBySubjectMap = markBySubjectMap;
    }
}
