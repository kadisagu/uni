/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e2.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitContractBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class DAO extends AbstractListParagraphPubDAO<SplitContractBachelorEntrantsStuListExtract, Model> implements IDAO
{
}