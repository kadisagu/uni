/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionList;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author agolubenko
 * @since 03.07.2008
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private DynamicListDataSource<EnrollmentDirection> _dataSource;

    private IDataSettings _settings;
    private ISelectModel _developFormList;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<HSelectOption> _levelTypeList;
    private ISelectModel _formativeOrgUnitsModel;
    private ISelectModel _territorialOrgUnitsModel;

    public static final int BUDGET_IDX = 0;
    public static final int CONTRACT_IDX = 1;
    private Map<Long, int[]> _directionId2profileSum;
    private boolean _showPossibleProfileColumn;
    private Set<Long> _idsWithPossibleProfileSet;

    public boolean isHasPossibleProfile()
    {
        return _idsWithPossibleProfileSet.contains(_dataSource.getCurrentEntity().getId());
    }
    
    // Getters & Setters

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<EnrollmentDirection> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EnrollmentDirection> dataSource)
    {
        _dataSource = dataSource;
    }

    public ISelectModel getDevelopFormList() { return _developFormList; }
    public void setDevelopFormList(ISelectModel developFormList) { _developFormList = developFormList; }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<HSelectOption> getLevelTypeList()
    {
        return _levelTypeList;
    }

    public void setLevelTypeList(List<HSelectOption> levelTypeList)
    {
        _levelTypeList = levelTypeList;
    }

    public ISelectModel getFormativeOrgUnitsModel()
    {
        return _formativeOrgUnitsModel;
    }

    public void setFormativeOrgUnitsModel(ISelectModel formativeOrgUnitsModel)
    {
        _formativeOrgUnitsModel = formativeOrgUnitsModel;
    }

    public ISelectModel getTerritorialOrgUnitsModel()
    {
        return _territorialOrgUnitsModel;
    }

    public void setTerritorialOrgUnitsModel(ISelectModel territorialOrgUnitsModel)
    {
        _territorialOrgUnitsModel = territorialOrgUnitsModel;
    }

    public Map<Long, int[]> getDirectionId2profileSum()
    {
        return _directionId2profileSum;
    }

    public void setDirectionId2profileSum(Map<Long, int[]> directionId2profileSum)
    {
        _directionId2profileSum = directionId2profileSum;
    }

    public boolean isShowPossibleProfileColumn()
    {
        return _showPossibleProfileColumn;
    }

    public void setShowPossibleProfileColumn(boolean showPossibleProfileColumn)
    {
        _showPossibleProfileColumn = showPossibleProfileColumn;
    }

    public Set<Long> getIdsWithPossibleProfileSet()
    {
        return _idsWithPossibleProfileSet;
    }

    public void setIdsWithPossibleProfileSet(Set<Long> idsWithPossibleProfileSet)
    {
        _idsWithPossibleProfileSet = idsWithPossibleProfileSet;
    }
}
