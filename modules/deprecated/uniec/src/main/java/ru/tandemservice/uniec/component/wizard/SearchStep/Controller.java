/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.wizard.SearchStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;
import ru.tandemservice.uniec.component.wizard.SearchStep.Model.SearchMethod;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.HashMap;
import java.util.Map;

/**
 * @author agolubenko
 * @since May 12, 2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onChangeBlock(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setBlockName(model.getSearchMethod().getName());
    }

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);

        // деактивируем компонент
        deactivate(component);

        // ищем абитуриента
        Entrant entrant = getDao().findEntrant(model);
        if (entrant != null)
        {
            // если нашли, то переходим на его карточку
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("selectedTab", "entrantTab");
            params.put("selectedDataTab", null);
            activateInRoot(component, new PublisherActivator(entrant, params));
            return;
        }

        // ищем персону с полным сходством
        IdentityCard identityCard = model.getIdentityCard();
        IdentityCard duplicate = getDao().findDuplicate(identityCard);

        // иначе открываем мастер или форму предварительного поиска
        Map<String, Object> params = new HashMap<String, Object>();
        if (model.getSearchMethod() == SearchMethod.ENTRANTS)
        {
            params.put("identityCard", (duplicate != null) ? duplicate : identityCard);
        }
        else
        {
            params.put("identityCard", duplicate);
            params.put("onlineEntrantId", model.getOnlineEntrant().getId());
        }
        params.put("enrollmentCampaignId", model.getEnrollmentCampaign().getId());
        params.put("entrantMasterPermKey", model.getEntrantMasterPermKey());

        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.IDENTITY_CARD_STEP, params));
    }
}
