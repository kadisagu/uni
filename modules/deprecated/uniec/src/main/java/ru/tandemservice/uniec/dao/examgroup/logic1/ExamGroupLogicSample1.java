/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup.logic1;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.examgroup.AbstractExamGroupLogic;
import ru.tandemservice.uniec.dao.examgroup.ExamGroupInfo;
import ru.tandemservice.uniec.dao.examgroup.IExamGroupInfo;
import ru.tandemservice.uniec.entity.catalog.ExamGroupType;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Логика работы экзаменационной группы, пример 1 (используется в СИНХ УрГЭУ)
 * Уникальный ключ: examGroupType, compensationType, developForm
 *
 * @author Vasily Zhukov
 * @since 27.05.2010
 */
public class ExamGroupLogicSample1 extends AbstractExamGroupLogic
{
    @Override
    public List<IExamGroupInfo> getExamGroupInfo(EntrantRequest entrantRequest)
    {
        Session session = getSession();

        // получаем первое направление приема из заявления
        List<RequestedEnrollmentDirection> list = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest))
                .addOrder("r", RequestedEnrollmentDirection.P_PRIORITY)
                .getResultList(session, 0, 1);

        if (list.size() == 0) return Collections.emptyList(); // пустое заявление. ключ нельзя создать

        RequestedEnrollmentDirection first = list.get(0);
        DevelopForm developForm = first.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();
        ExamGroupType examGroupType = getExamGroupType(session, first.getEnrollmentDirection().getEducationOrgUnit());

        String key = createKey(examGroupType, first.getCompensationType(), developForm);
        StringBuilder buf = new StringBuilder();
        buf.append("${index}");

        if (examGroupType.getCode().compareTo(UniecDefines.CATALOG_EXAM_GROUP_TYPE_SPEC) == 0)
            buf.append("-Бак/Cп");
        else if (examGroupType.getCode().compareTo(UniecDefines.CATALOG_EXAM_GROUP_TYPE_MASTER) == 0)
            buf.append("-М");
        else if (examGroupType.getCode().compareTo(UniecDefines.CATALOG_EXAM_GROUP_TYPE_SHORT) == 0)
            buf.append("-С");

        buf.append(", ").append(developForm.getTitle().toLowerCase()).append(" форма, ").append(first.getCompensationType().getShortTitle());

        String shortTitle = buf.toString();
        String title = "Группа №" + shortTitle;

        List<IExamGroupInfo> resultList = new ArrayList<>();
        resultList.add(new ExamGroupInfo(key, title, shortTitle));
        return resultList;
    }

    @Override
    public String getExamGroupListBusinessComponent()
    {
        return "ru.tandemservice.uniec.component.menu.examGroup.logic1.ExamGroupList";
    }

    @Override
    public String getMarkDistributionBusinessComponent()
    {
        return "ru.tandemservice.uniec.component.menu.examGroup.logic1.MarkDistribution";
    }

    private static ExamGroupType getExamGroupType(Session session, EducationOrgUnit educationOrgUnit)
    {
        MQBuilder builder = new MQBuilder(ExamGroupType.ENTITY_CLASS, "t");
        if (!UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(educationOrgUnit.getDevelopCondition().getCode()))
        {
            // если направление приема не по полной программе, то ставим ему тип "По сокр. программе"
            builder.add(MQExpression.eq("t", ExamGroupType.P_CODE, UniecDefines.CATALOG_EXAM_GROUP_TYPE_SHORT));
        } else
        {
            educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode();
            // если направление создано уровень образования "направление магистров" или " магистерский профиль (программа)", то ставим ему тип "Магистры"
            if (educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType().isMaster())
                builder.add(MQExpression.eq("t", ExamGroupType.P_CODE, UniecDefines.CATALOG_EXAM_GROUP_TYPE_MASTER));
            else
                builder.add(MQExpression.eq("t", ExamGroupType.P_CODE, UniecDefines.CATALOG_EXAM_GROUP_TYPE_SPEC));
        }
        return (ExamGroupType) builder.uniqueResult(session);
    }

    public static String createKey(ExamGroupType examGroupType, CompensationType compensationType, DevelopForm developForm)
    {
        return examGroupType.getId() + ";" + compensationType.getId() + ";" + developForm.getId();
    }

    public static ExamGroupKey parseKey(Session session, String key)
    {
        String[] part = StringUtils.split(key, ';');
        ExamGroupType examGroupType = (ExamGroupType) session.get(ExamGroupType.class, Long.parseLong(part[0]));
        CompensationType compensationType = (CompensationType) session.get(CompensationType.class, Long.parseLong(part[1]));
        DevelopForm developForm = (DevelopForm) session.get(DevelopForm.class, Long.parseLong(part[2]));
        return new ExamGroupKey(examGroupType, compensationType, developForm);
    }

    public static final class ExamGroupKey
    {
        private ExamGroupType _examGroupType;
        private CompensationType _compensationType;
        private DevelopForm _developForm;

        public ExamGroupKey(ExamGroupType examGroupType, CompensationType compensationType, DevelopForm developForm)
        {
            _examGroupType = examGroupType;
            _compensationType = compensationType;
            _developForm = developForm;
        }

        public ExamGroupType getExamGroupType()
        {
            return _examGroupType;
        }

        public CompensationType getCompensationType()
        {
            return _compensationType;
        }

        public DevelopForm getDevelopForm()
        {
            return _developForm;
        }
    }
}
