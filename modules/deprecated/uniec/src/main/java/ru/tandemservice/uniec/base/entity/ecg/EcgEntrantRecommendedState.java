package ru.tandemservice.uniec.base.entity.ecg;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgHasBringOriginal;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgEntrantRecommendedStateGen;

/**
 * Состояние предзачисления рекомендованного для распределения
 */
public class EcgEntrantRecommendedState extends EcgEntrantRecommendedStateGen implements IEcgHasBringOriginal
{
}