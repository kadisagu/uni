/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractMarkDistribution;

import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniec.component.entrant.EntrantMarkTab.EntrantMarkValidator;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public abstract class MarkDistributionModel implements IEnrollmentCampaignSelectModel
{
    public static final String COLUMN_EXAM_PASS_DISCIPLINE_MARK = "examPassDisciplineMark";
    public static final String COLUMN_EXAM_PASS_DISCIPLINE_PAPER_CODE = "examPassDisciplinePaperCode";
    public static final String COLUMN_EXAM_PASS_DISCIPLINE_EXAMINERS = "examPassDisciplineExaminers";

    // сеттинги для хранения приемной кампании, так как это общий селект для всех компонентов
    private IDataSettings _common;

    // сеттинги для хранения всех остальных фильтров
    private IDataSettings _settings;

    // список приемных кампании
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    // список наборов экзаменов
    private ISelectModel _examGroupSetModel;

    // список дисциплин для сдачи
    private DynamicListDataSource<EntrantDisciplineMarkWrapper> _dataSource;

    // бизнес компонент который надо загрузить
    private String _redirectComponentName;

    // селект экзаменационных групп
    private ISelectModel _groupModel;

    // селект дисциплин по форме сдачи
    private ISelectModel _disciplineModel;

    // выбранная дисциплина по форме сдачи, ее в сеттингах лучше не сохранять
    private DisciplineWrapper _discipline;

    private byte[] _content;
    private String _contentType;
    private String _fileName;

    public Validator getValidator()
    {
        Discipline2RealizationWayRelation discipline = getDataSource().getCurrentEntity().getExamPassDiscipline().getEnrollmentCampaignDiscipline();
        return new EntrantMarkValidator(false, discipline.getMinMark(), discipline.getMaxMark());
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _common.get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _common.set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    public IDataSettings getCommon()
    {
        return _common;
    }

    public void setCommon(IDataSettings common)
    {
        _common = common;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getExamGroupSetModel()
    {
        return _examGroupSetModel;
    }

    public void setExamGroupSetModel(ISelectModel examGroupSetModel)
    {
        _examGroupSetModel = examGroupSetModel;
    }

    public DynamicListDataSource<EntrantDisciplineMarkWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public String getRedirectComponentName()
    {
        return _redirectComponentName;
    }

    public void setRedirectComponentName(String redirectComponentName)
    {
        _redirectComponentName = redirectComponentName;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public ISelectModel getDisciplineModel()
    {
        return _disciplineModel;
    }

    public void setDisciplineModel(ISelectModel disciplineModel)
    {
        _disciplineModel = disciplineModel;
    }

    public DisciplineWrapper getDiscipline()
    {
        return _discipline;
    }

    public void setDiscipline(DisciplineWrapper discipline)
    {
        _discipline = discipline;
    }

    public byte[] getContent()
    {
        return _content;
    }

    public void setContent(byte[] content)
    {
        _content = content;
    }

    public String getContentType()
    {
        return _contentType;
    }

    public void setContentType(String contentType)
    {
        _contentType = contentType;
    }

    public String getFileName()
    {
        return _fileName;
    }

    public void setFileName(String fileName)
    {
        _fileName = fileName;
    }
}
