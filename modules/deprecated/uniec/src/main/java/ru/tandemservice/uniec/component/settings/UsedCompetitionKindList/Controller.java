/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.UsedCompetitionKindList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;

/**
 * @author vip_delete
 * @since 12.02.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());
        
        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EnrollmentCompetitionKind> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", new String[]{EnrollmentCompetitionKind.P_TITLE}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));

        dataSource.addColumn(new ToggleColumn("Использовать", EnrollmentOrderType.P_USED).setListener("onClickUsed"));

        model.setDataSource(dataSource);
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().updatePriority(getModel(component), (Long)component.getListenerParameter(), true);
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().updatePriority(getModel(component), (Long)component.getListenerParameter(), false);
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }

    public void onClickUsed(IBusinessComponent component)
    {
        getDao().updateToggleUsed((Long) component.getListenerParameter());
    }
}
