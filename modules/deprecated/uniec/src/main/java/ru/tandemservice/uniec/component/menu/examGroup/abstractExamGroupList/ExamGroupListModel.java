/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public abstract class ExamGroupListModel implements IEnrollmentCampaignSelectModel
{
    public static final String ENTRANT_COUNT = "entrantCount";

    // сеттинги для хранения приемной кампании, так как это общий селект для всех компонентов
    private IDataSettings _common;

    // сеттинги для хранения всех остальных фильтров
    private IDataSettings _settings;

    // список приемных кампании
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    // список наборов экзаменов
    private ISelectModel _examGroupSetModel;

    // список экзаменационных групп
    private DynamicListDataSource _dataSource;

    // бизнес компонент который надо загрузить
    private String _redirectComponentName;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _common.get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _common.set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    public IDataSettings getCommon()
    {
        return _common;
    }

    public void setCommon(IDataSettings common)
    {
        _common = common;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getExamGroupSetModel()
    {
        return _examGroupSetModel;
    }

    public void setExamGroupSetModel(ISelectModel examGroupSetModel)
    {
        _examGroupSetModel = examGroupSetModel;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public String getRedirectComponentName()
    {
        return _redirectComponentName;
    }

    public void setRedirectComponentName(String redirectComponentName)
    {
        _redirectComponentName = redirectComponentName;
    }
}
