/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryForm1.EntrantRegistryForm1Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.report.EntrantRegistryForm1Report;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 30.06.2009
 */
class EntrantRegistryForm1ReportContentGenerator
{
    private EntrantRegistryForm1Report report;
    private List<StudentCategory> studentCategoryList;
    private Session session;

    public EntrantRegistryForm1ReportContentGenerator(EntrantRegistryForm1Report report, List<StudentCategory> studentCategoryList, Session session)
    {
        this.report = report;
        this.session = session;
        this.studentCategoryList = studentCategoryList;
    }

    public byte[] generateReportContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_REGISTRY_FORM_1);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();

        List<String[]> tableRows = new ArrayList<String[]>();

        for (EntrantRequest request : getEntrantRequestList())
        {
            PersonEduInstitution personEduInstitution = request.getEntrant().getPerson().getPersonEduInstitution();
            String documentString = "";
            if (personEduInstitution != null)
            {
                List<String> titleParams = new ArrayList<String>();
                if (personEduInstitution.getDocumentType() != null)
                    titleParams.add(personEduInstitution.getDocumentType().getTitle());
                if (personEduInstitution.getSeria() != null || personEduInstitution.getNumber() != null)
                    titleParams.add((personEduInstitution.getSeria() == null ? "" : personEduInstitution.getSeria() + " ") + (personEduInstitution.getNumber() == null ? "" : personEduInstitution.getNumber()));
                titleParams.add(Integer.toString(personEduInstitution.getYearEnd()) + " г.");
                documentString = StringUtils.join(titleParams, ", ");
            }
            String addressString = "";
            if (request.getEntrant().getPerson().getIdentityCard().getAddress() != null)
                addressString = request.getEntrant().getPerson().getIdentityCard().getAddress().getTitleWithFlat();
            tableRows.add(new String[]
                    {
                            request.getStringNumber(),
                            DateFormatter.DEFAULT_DATE_FORMATTER.format(request.getRegDate()),
                            request.getEntrant().getPerson().getFullFio(),
                            documentString,
                            addressString
                    });
        }


        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableRows.toArray(new String[tableRows.size()][]));
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private List<EntrantRequest> getEntrantRequestList()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        filterByReportParams(builder, "red");
        builder.addOrder("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_NUMBER);
        builder.addOrder("red", RequestedEnrollmentDirection.L_COMPENSATION_TYPE + ".code");
        return CommonBaseUtil.getPropertiesList(builder.<RequestedEnrollmentDirection>getResultList(session), RequestedEnrollmentDirection.L_ENTRANT_REQUEST);
    }

    /**
     * Накладывает фильтр по параметрам отчета - условия накладываются от альяса выбранного напр. приема
     *
     * @param builder - билдер
     * @param alias   - альяс выбранного напр. приема
     */
    private void filterByReportParams(MQBuilder builder, String alias)
    {
        builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, report.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate(alias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.P_REG_DATE, report.getDateFrom(), report.getDateTo()));

        if (studentCategoryList != null && !studentCategoryList.isEmpty())
            builder.add(MQExpression.in(alias, RequestedEnrollmentDirection.L_STUDENT_CATEGORY + ".id", CommonBaseUtil.getPropertiesList(studentCategoryList, "id")));
        if (report.getCompensationType() != null)
            builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_COMPENSATION_TYPE, report.getCompensationType()));

        builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, report.getEnrollmentDirection()));
        builder.add(MQExpression.eq(alias, RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
    }
}
