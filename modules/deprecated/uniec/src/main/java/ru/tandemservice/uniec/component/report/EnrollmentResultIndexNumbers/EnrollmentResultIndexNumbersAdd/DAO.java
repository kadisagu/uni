// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultIndexNumbers.EnrollmentResultIndexNumbersAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.report.EnrollmentResultIndexNumbersReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author oleyba
 * @since 26.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());

        model.setStudentCategoryList(new ArrayList<>());
        model.setQualificationList(new ArrayList<>());
        model.setFormativeOrgUnitList(new ArrayList<>());
        model.setTerritorialOrgUnitList(new ArrayList<>());
        model.setDevelopConditionList(new ArrayList<>());
        model.setDevelopTechList(new ArrayList<>());
        model.setDevelopPeriodList(new ArrayList<>());

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EnrollmentResultIndexNumbersReport report = model.getReport();
        report.setFormingDate(new Date());

        DatabaseFile databaseFile = new EnrollmentResultIndexNumbersReportBuilder(model, session).getContent();
        session.save(databaseFile);
        report.setContent(databaseFile);

        report.setCompensationType(model.getCompensationType().getShortTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        if (model.isQualificationActive())
            report.setQualification(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(CommonBaseStringUtil.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchool(UniStringUtils.join(model.getEducationLevelHighSchoolList(), "displayableTitle", "; "));
        report.setDevelopForm(model.getDevelopForm().getTitle());
        if (model.isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), "title", "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), "title", "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        session.save(report);
    }
}
