/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.IIdentifiable;

import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.List;

/**
 * @author agolubenko
 * @since 19.06.2008
 */
public abstract class DAO<T extends Model<Vertical, Horizontal, FieldType>, Vertical extends IIdentifiable, Horizontal extends IIdentifiable, FieldType, ObjectType> extends org.tandemframework.shared.commonbase.base.util.AbstractMatrixComponent.DAO<T, Vertical, Horizontal, FieldType, ObjectType>
{
    @Override
    public void prepare(T model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        super.prepare(model);
    }
}
