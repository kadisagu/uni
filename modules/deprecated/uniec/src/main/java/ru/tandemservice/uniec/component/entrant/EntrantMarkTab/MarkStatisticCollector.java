/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.EntrantMarkTab;

import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Сборщик всех статистик оценок в разрезе дисциплины по форме сдачи
 * Для обеспечения непротиворечивости данных поставлены проверки на входящие статистики
 * <p/>
 * TODO: задокументировать, если класс будет полезным в некоторых отчетах, сейчас пока на странице оценок используется
 *
 * @author Vasily Zhukov
 * @since 28.07.2010
 */
public class MarkStatisticCollector
{
    public enum CollectScope
    {
        REQUESTED_ENROLLMENT_DIRECTION,  // разрешается совмещать статистики только из одного выбранного направления
        ENTRANT_REQUEST,                 // ... из одного заявления
        ENTRANT                          // ... из одного абитуриента
    }

    private CollectScope _collectScope;
    private boolean _compensationTypeDiff;
    private Long _scopeObjectId;
    private Map<Discipline2RealizationWayRelation, MarkStatisticCollectorItem> _discipline2statisticMap;

    public MarkStatisticCollector(CollectScope collectScope, boolean compensationTypeDiff, EntrantDataUtil dataUtil)
    {
        if (collectScope == null)
            throw new RuntimeException("Can't create MarkStatisticCollector: collectScope is null!");

        _collectScope = collectScope;
        _compensationTypeDiff = compensationTypeDiff;
        _discipline2statisticMap = new HashMap<>();

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                collect(dataUtil.getMarkStatistic(chosen));
    }

    private void collect(MarkStatistic statistic)
    {
        long scopeObjectId = getScopeObjectId(statistic);

        if (_scopeObjectId == null)
            _scopeObjectId = scopeObjectId;
        else if (_scopeObjectId != scopeObjectId)
            throw new RuntimeException("Can't collect MarkStatistic: scope fail!");

        Discipline2RealizationWayRelation discipline = statistic.getChosenDiscipline().getEnrollmentCampaignDiscipline();

        MarkStatisticCollectorItem item = _discipline2statisticMap.get(discipline);
        if (item == null)
            _discipline2statisticMap.put(discipline, new MarkStatisticCollectorItem(_compensationTypeDiff, statistic));
        else
            item.collect(_compensationTypeDiff, statistic);
    }

    private long getScopeObjectId(MarkStatistic statistic)
    {
        RequestedEnrollmentDirection direction = statistic.getChosenDiscipline().getChosenEnrollmentDirection();

        switch (_collectScope)
        {
            case REQUESTED_ENROLLMENT_DIRECTION:
                return direction.getId();
            case ENTRANT_REQUEST:
                return direction.getEntrantRequest().getId();
            case ENTRANT:
                return direction.getEntrantRequest().getEntrant().getId();
            default:
                throw new RuntimeException("Unknown collectScope: " + _collectScope);
        }
    }

    public Set<Discipline2RealizationWayRelation> getDisciplineSet()
    {
        return _discipline2statisticMap.keySet();
    }

    public StateExamSubjectMark getStateExamMark(Discipline2RealizationWayRelation discipline)
    {
        return _discipline2statisticMap.get(discipline).stateExamMark;
    }

    public Integer getOlympiad(Discipline2RealizationWayRelation discipline)
    {
        return _discipline2statisticMap.get(discipline).olympiad;
    }

    public Double getScaledStateExamMarkBudget(Discipline2RealizationWayRelation discipline)
    {
        if (!_compensationTypeDiff)
            throw new RuntimeException("MarkStatisticCollector is created with compensationTypeDiff=false");
        return _discipline2statisticMap.get(discipline).scaledStateExamMarkBudget;
    }

    public Double getScaledStateExamMarkContract(Discipline2RealizationWayRelation discipline)
    {
        if (!_compensationTypeDiff)
            throw new RuntimeException("MarkStatisticCollector is created with compensationTypeDiff=false");
        return _discipline2statisticMap.get(discipline).scaledStateExamMarkContract;
    }

    public Double getScaledStateExamMark(Discipline2RealizationWayRelation discipline)
    {
        if (_compensationTypeDiff)
            throw new RuntimeException("MarkStatisticCollector is created with compensationTypeDiff=true");
        return _discipline2statisticMap.get(discipline).scaledStateExamMark;
    }

    public Map<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> getInternalMarkMap(Discipline2RealizationWayRelation discipline)
    {
        return Collections.unmodifiableMap(_discipline2statisticMap.get(discipline).internalMarkMap);
    }

    //

    private static final class MarkStatisticCollectorItem
    {
        StateExamSubjectMark stateExamMark;
        Integer olympiad;
        Double scaledStateExamMarkBudget;
        Double scaledStateExamMarkContract;
        Double scaledStateExamMark;
        Map<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> internalMarkMap;

        MarkStatisticCollectorItem(boolean compensationTypeDiff, MarkStatistic statistic)
        {
            stateExamMark = statistic.getStateExamMark();

            olympiad = statistic.getOlympiad();

            if (compensationTypeDiff)
            {
                if (statistic.getChosenDiscipline().getChosenEnrollmentDirection().getCompensationType().isBudget())
                {
                    scaledStateExamMarkBudget = statistic.getScaledStateExamMark();
                } else
                {
                    scaledStateExamMarkContract = statistic.getScaledStateExamMark();
                }
            } else
            {
                scaledStateExamMark = statistic.getScaledStateExamMark();
            }

            internalMarkMap = new HashMap<>(statistic.getInternalMarkMap());
        }

        void collect(boolean compensationTypeDiff, MarkStatistic statistic)
        {
            // если stateExamMark и statistic.getStateExamMark() не равны null, то они должны быть равны
            if (stateExamMark == null)
                stateExamMark = statistic.getStateExamMark();
            else if (statistic.getStateExamMark() != null)
                if (!stateExamMark.equals(statistic.getStateExamMark()))
                    throw new RuntimeException("Can't collect MarkStatistic: stateExamMarks is not equals!");

            if (olympiad == null)
                olympiad = statistic.getOlympiad();
            else if (statistic.getOlympiad() != null && statistic.getOlympiad() > olympiad)
                olympiad = statistic.getOlympiad();

            if (compensationTypeDiff)
            {
                if (statistic.getChosenDiscipline().getChosenEnrollmentDirection().getCompensationType().isBudget())
                {
                    // если scaledStateExamMarkBudget и statistic.getScaledStateExamMark() не равны null, то они должны быть равны
                    if (scaledStateExamMarkBudget == null)
                        scaledStateExamMarkBudget = statistic.getScaledStateExamMark();
                    else if (statistic.getScaledStateExamMark() != null)
                        if (!scaledStateExamMarkBudget.equals(statistic.getScaledStateExamMark()))
                            throw new RuntimeException("Can't collect MarkStatistic: scaledStateExamMarkBudget is not equals!");
                } else
                {
                    // если scaledStateExamMarkContract и statistic.getScaledStateExamMark() не равны null, то они должны быть равны
                    if (scaledStateExamMarkContract == null)
                        scaledStateExamMarkContract = statistic.getScaledStateExamMark();
                    else if (statistic.getScaledStateExamMark() != null)
                        if (!scaledStateExamMarkContract.equals(statistic.getScaledStateExamMark()))
                            throw new RuntimeException("Can't collect MarkStatistic: scaledStateExamMarkContract is not equals!");
                }
            } else
            {
                // если scaledStateExamMark и statistic.getScaledStateExamMark() не равны null, то они должны быть равны
                if (scaledStateExamMark == null)
                    scaledStateExamMark = statistic.getScaledStateExamMark();
                else if (statistic.getScaledStateExamMark() != null)
                    if (!scaledStateExamMark.equals(statistic.getScaledStateExamMark()))
                        throw new RuntimeException("Can't collect MarkStatistic: scaledStateExamMark is not equals!");
            }

            for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> entry : statistic.getInternalMarkMap().entrySet())
            {
                SubjectPassForm passForm = entry.getKey();
                PairKey<ExamPassMark, ExamPassMarkAppeal> pairKey = entry.getValue();

                if (!internalMarkMap.containsKey(passForm))
                {
                    // такой формы сдачи еще не было. добавляем ее
                    internalMarkMap.put(passForm, pairKey);
                } else
                {
                    // форма сдачи есть и результаты сдачи по ней должны совпадать с результами сдачи из statistic
                    PairKey<ExamPassMark, ExamPassMarkAppeal> existPairKey = internalMarkMap.get(passForm);
                    if (!itemEquals(pairKey, existPairKey))
                        throw new RuntimeException("Can't collect MarkStatistic: marks is not equals!");
                }
            }
        }

        private static boolean itemEquals(Object a, Object b)
        {
            return a == null ? b == null : a.equals(b);
        }
    }
}
