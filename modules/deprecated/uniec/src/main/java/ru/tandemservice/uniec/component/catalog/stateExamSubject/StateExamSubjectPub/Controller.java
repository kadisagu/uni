/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.catalog.stateExamSubject.StateExamSubjectPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

/**
 * @author agolubenko
 * @since 27.02.2009
 */
public class Controller extends DefaultCatalogPubController<StateExamSubject, DefaultCatalogPubModel<StateExamSubject>, IDefaultCatalogPubDAO<StateExamSubject, DefaultCatalogPubModel<StateExamSubject>>>
{
    @Override
    protected DynamicListDataSource<StateExamSubject> createListDataSource(IBusinessComponent component)
    {
        DefaultCatalogPubModel<StateExamSubject> model = getModel(component);
        DynamicListDataSource<StateExamSubject> dataSource = new DynamicListDataSource<StateExamSubject>(component, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", StateExamSubject.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", StateExamSubject.P_SHORT_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Код предмета", StateExamSubject.P_CODE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Код предмета по ЦТМО", StateExamSubject.P_SUBJECT_C_T_M_O_CODE).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        return dataSource;
    }
}
