package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniec_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrollmentCampaign

		// создано обязательное свойство useIndividualProgressInAmountMark
		{
			// создать колонку
			tool.createColumn("enrollmentcampaign_t", new DBColumn("sindvdlprgrssinamntmrk_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultUseIndividualProgressInAmountMark = false;
			tool.executeUpdate("update enrollmentcampaign_t set sindvdlprgrssinamntmrk_p=? where sindvdlprgrssinamntmrk_p is null", defaultUseIndividualProgressInAmountMark);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enrollmentcampaign_t", "sindvdlprgrssinamntmrk_p", false);

		}


        ////////////////////////////////////////////////////////////////////////////////
        // сущность individualProgress

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("individualprogress_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_individualprogress"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("maxmark_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("individualProgress");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrCampaignEntrantIndividualProgress

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("nrcmpgnentrntindvdlprgrss_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ca5fc48d"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("individualprogress_id", DBType.LONG).setNullable(false),
                                      new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                      new DBColumn("cnsdrdinenrllmntcmpgn_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrCampaignEntrantIndividualProgress");

        }


    }
}