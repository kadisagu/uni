/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionStateAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 03.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setRequestedEnrollmentDirection(getNotNull(RequestedEnrollmentDirection.class, model.getRequestedEnrollmentDirection().getId()));
        model.setEntrantStateList(getList(EntrantState.class, EntrantState.P_SELECTABLE, Boolean.TRUE, EntrantState.P_CODE));
    }

    @Override
    public void update(Model model)
    {
        model.getRequestedEnrollmentDirection().setState(model.getEntrantState());

        // do not uncomment #2307
        // а то будет перевычислять состояние абитуриента
        //getSession().update(model.getRequestedEnrollmentDirection());
    }
}
