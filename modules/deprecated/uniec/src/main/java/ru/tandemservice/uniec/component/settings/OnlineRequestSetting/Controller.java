/* $Id$ */
package ru.tandemservice.uniec.component.settings.OnlineRequestSetting;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Vasily Zhukov
 * @since 03.06.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        
        model.setSettings(component.getSettings());

        getDao().prepare(model);
    }
    
    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
        
        getDao().prepare(getModel(component));
    }
    
    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
}
