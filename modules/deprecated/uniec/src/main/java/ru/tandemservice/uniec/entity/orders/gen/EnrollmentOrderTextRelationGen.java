package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь приказа и текста приказа (приказы по зачислению)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderTextRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation";
    public static final String ENTITY_NAME = "enrollmentOrderTextRelation";
    public static final int VERSION_HASH = 388570674;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_TEXT = "text";
    public static final String P_FILE_NAME = "fileName";

    private EnrollmentOrder _order;     // Приказ о зачислении абитуриентов
    private byte[] _text;     // Сохраненная печатная форма
    private String _fileName;     // Имя файла печатной формы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(EnrollmentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Имя файла печатной формы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileName()
    {
        return _fileName;
    }

    /**
     * @param fileName Имя файла печатной формы. Свойство не может быть null.
     */
    public void setFileName(String fileName)
    {
        dirty(_fileName, fileName);
        _fileName = fileName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderTextRelationGen)
        {
            setOrder(((EnrollmentOrderTextRelation)another).getOrder());
            setText(((EnrollmentOrderTextRelation)another).getText());
            setFileName(((EnrollmentOrderTextRelation)another).getFileName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderTextRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderTextRelation.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderTextRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "text":
                    return obj.getText();
                case "fileName":
                    return obj.getFileName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrder) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
                case "fileName":
                    obj.setFileName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "text":
                        return true;
                case "fileName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "text":
                    return true;
                case "fileName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrder.class;
                case "text":
                    return byte[].class;
                case "fileName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderTextRelation> _dslPath = new Path<EnrollmentOrderTextRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderTextRelation");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation#getOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Имя файла печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation#getFileName()
     */
    public static PropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    public static class Path<E extends EnrollmentOrderTextRelation> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _order;
        private PropertyPath<byte[]> _text;
        private PropertyPath<String> _fileName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation#getOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> order()
        {
            if(_order == null )
                _order = new EnrollmentOrder.Path<EnrollmentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(EnrollmentOrderTextRelationGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Имя файла печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderTextRelation#getFileName()
     */
        public PropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new PropertyPath<String>(EnrollmentOrderTextRelationGen.P_FILE_NAME, this);
            return _fileName;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderTextRelation.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderTextRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
