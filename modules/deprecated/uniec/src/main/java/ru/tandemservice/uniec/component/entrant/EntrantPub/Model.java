/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import ru.tandemservice.uni.component.person.util.ISecureRoleContextOwner;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.ui.EntrantCustomStateCollectionFormatter;

import java.util.List;
import java.util.Map;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id"),
        @Bind(key = "selectedTab"),
        @Bind(key = "selectedDataTab")
})
//TODO: try to remove it
@Output({
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = ISecureRoleContextOwner.SECURE_ROLE_CONTEXT),
        @Bind(key = "entrantId", binding = "entrant.id")
})
public final class Model implements ISecureRoleContextOwner
{
    public static final String SCOPE_NAME = "entrantTabPanel";

    private Entrant _entrant = new Entrant();
    private String _selectedTab;
    private String _selectedDataTab;

    private DynamicListDataSource<OlympiadDiplomaWrapper> _olympiadDiplomaDataSource;
    private DynamicListDataSource<EntrantAccessCourse> _accessCoursesDataSource;
    private DynamicListDataSource<EntrantStateExamCertificate> _entrantStateExamCertificatesDataSource;
    private DynamicListDataSource<PreliminaryEnrollmentStudent> _preliminaryEnrollmentDataSource;
    private DynamicListDataSource<EntrantIndividualProgress> _individualProgressDataSource;

    private Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> _certificate2SubjectsMarks;
    private StateExamSubjectMark _currentStateExamSubjectMark;
    private String _sourceInfoTitle;
    private String _enrollmentRecomendationsTitle;
    private String _accessCoursesTitle;
    private String _accessDepartmentsTitle;
    private List<EntrantCustomState> _entrantCustomStates;

    private IEntityHandler _defaultEntityHandler = entity -> !isAccessible();

    @Override
    public ISecureRoleContext getSecureRoleContext()
    {
        return SecureRoleContext.instance(_entrant).hasBirthDateCheck(true).accessible(!_entrant.isArchival());
    }

    public boolean isAccessible()
    {
        return !_entrant.isArchival();
    }

    // Getters & Setters
    public Map<String, Object> getEntrantParameters()
    {
        return ParametersMap.createWith("entrantId", _entrant.getId());
    }

    public String getEntrantCustomStates()
    {
        return new EntrantCustomStateCollectionFormatter().format(_entrantCustomStates);
    }

    public void setEntrantCustomStates(List<EntrantCustomState> entrantCustomStates)
    {
        _entrantCustomStates = entrantCustomStates;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return _selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        _selectedDataTab = selectedDataTab;
    }

    public DynamicListDataSource<OlympiadDiplomaWrapper> getOlympiadDiplomaDataSource()
    {
        return _olympiadDiplomaDataSource;
    }

    public void setOlympiadDiplomaDataSource(DynamicListDataSource<OlympiadDiplomaWrapper> olympiadDiplomaDataSource)
    {
        _olympiadDiplomaDataSource = olympiadDiplomaDataSource;
    }

    public DynamicListDataSource<EntrantAccessCourse> getAccessCoursesDataSource()
    {
        return _accessCoursesDataSource;
    }

    public void setAccessCoursesDataSource(DynamicListDataSource<EntrantAccessCourse> accessCoursesDataSource)
    {
        _accessCoursesDataSource = accessCoursesDataSource;
    }

    public DynamicListDataSource<EntrantStateExamCertificate> getEntrantStateExamCertificatesDataSource()
    {
        return _entrantStateExamCertificatesDataSource;
    }

    public void setEntrantStateExamCertificatesDataSource(DynamicListDataSource<EntrantStateExamCertificate> entrantStateExamCertificatesDataSource)
    {
        _entrantStateExamCertificatesDataSource = entrantStateExamCertificatesDataSource;
    }

    public Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> getCertificate2SubjectsMarks()
    {
        return _certificate2SubjectsMarks;
    }

    public void setCertificate2SubjectsMarks(Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> certificate2SubjectsMarks)
    {
        _certificate2SubjectsMarks = certificate2SubjectsMarks;
    }

    public StateExamSubjectMark getCurrentStateExamSubjectMark()
    {
        return _currentStateExamSubjectMark;
    }

    public void setCurrentStateExamSubjectMark(StateExamSubjectMark currentStateExamSubjectMark)
    {
        _currentStateExamSubjectMark = currentStateExamSubjectMark;
    }

    public String getSourceInfoTitle()
    {
        return _sourceInfoTitle;
    }

    public void setSourceInfoTitle(String sourceInfoTitle)
    {
        _sourceInfoTitle = sourceInfoTitle;
    }

    public String getEnrollmentRecomendationsTitle()
    {
        return _enrollmentRecomendationsTitle;
    }

    public void setEnrollmentRecomendationsTitle(String enrollmentRecomendationsTitle)
    {
        _enrollmentRecomendationsTitle = enrollmentRecomendationsTitle;
    }

    public String getAccessCoursesTitle()
    {
        return _accessCoursesTitle;
    }

    public void setAccessCoursesTitle(String accessCoursesTitle)
    {
        _accessCoursesTitle = accessCoursesTitle;
    }

    public String getAccessDepartmentsTitle()
    {
        return _accessDepartmentsTitle;
    }

    public void setAccessDepartmentsTitle(String accessDepartmentsTitle)
    {
        _accessDepartmentsTitle = accessDepartmentsTitle;
    }

    static class OlympiadDiplomaWrapper extends IdentifiableWrapper<OlympiadDiploma>
    {
        private static final long serialVersionUID = 1L;
        public static final String P_DIPLOMA = "diploma";
        private OlympiadDiploma _diploma;
        private String _enrollmentDirectionTitle;
        private String _disciplineListTitle;

        public static final String ENROLLMENT_DIRECTION_TITLE = "enrollmentDirectionTitle";
        public static final String DISCIPLINE_LIST_TITLE = "disciplineListTitle";

        public OlympiadDiplomaWrapper(OlympiadDiploma diploma, String enrollmentDirectionTitle, String disciplineListTitle) throws ClassCastException
        {
            super(diploma.getId(), "");
            _diploma = diploma;
            _enrollmentDirectionTitle = enrollmentDirectionTitle;
            _disciplineListTitle = disciplineListTitle;
        }

        public OlympiadDiploma getDiploma()
        {
            return _diploma;
        }

        public String getEnrollmentDirectionTitle()
        {
            return _enrollmentDirectionTitle;
        }

        public String getDisciplineListTitle()
        {
            return _disciplineListTitle;
        }
    }

    public DynamicListDataSource<PreliminaryEnrollmentStudent> getPreliminaryEnrollmentDataSource()
    {
        return _preliminaryEnrollmentDataSource;
    }

    public void setPreliminaryEnrollmentDataSource(DynamicListDataSource<PreliminaryEnrollmentStudent> preliminaryEnrollmentDataSource)
    {
        _preliminaryEnrollmentDataSource = preliminaryEnrollmentDataSource;
    }

    public IEntityHandler getDefaultEntityHandler()
    {
        return _defaultEntityHandler;
    }

    public DynamicListDataSource<EntrantIndividualProgress> getIndividualProgressDataSource()
    {
        return _individualProgressDataSource;
    }

    public void setIndividualProgressDataSource(DynamicListDataSource<EntrantIndividualProgress> individualProgressDataSource)
    {
        _individualProgressDataSource = individualProgressDataSource;
    }
}