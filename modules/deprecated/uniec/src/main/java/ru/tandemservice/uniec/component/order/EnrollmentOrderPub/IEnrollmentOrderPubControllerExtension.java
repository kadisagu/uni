/* $Id$ */
package ru.tandemservice.uniec.component.order.EnrollmentOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

/**
 * @author Dmitry Seleznev
 * @since 05.08.2013
 */
public interface IEnrollmentOrderPubControllerExtension
{
    String ENROLLMENT_ORDER_PUB_CONTROLLER_EXTENSIONS_BEAN_NAME = "enrollmentOrderPubControllerExtensions";

    void doSendToCoordinationAdditionalAction(IBusinessComponent component, EnrollmentOrder order);

    void doExecuteAdditionalAction(IBusinessComponent component, EnrollmentOrder order);
}
