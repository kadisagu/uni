/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EnrollmentResultByOrgUnits.EnrollmentResultByOrgUnitsAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.io.RtfWriter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.*;

/**
 * @author ekachanova
 */
public class EnrollmentResultByOrgUnitsReportBuilder
{
    private Model _model;
    private Session _session;

    EnrollmentResultByOrgUnitsReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_BY_ORGUNITS);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        AddressDetailed academyAddress = academy.getAddress();
        AddressItem academyCity = academyAddress == null ? null : academyAddress.getSettlement();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getTitle());
        injectModifier.put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle().split("/")[0]);
        injectModifier.put("developForm", _model.getReport().getDevelopForm().getTitle().toLowerCase() + " форма обучения");
        injectModifier.modify(document);

        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);
        if(_model.isQualificationActive())
            enrollmentDirectionList = filterByQualification(enrollmentDirectionList);

        Map<OrgUnit, Set<EnrollmentDirection>> formativeOrgUnit2EnrollmentDirectionSet = new HashMap<>();
        Map<Long, ReportRow> formativeOrgUnitId2ReportRow = new HashMap<>();
        for(EnrollmentDirection direction : enrollmentDirectionList)
        {
            OrgUnit foramtiveOrgUnit = direction.getEducationOrgUnit().getFormativeOrgUnit();
            Set<EnrollmentDirection> set = formativeOrgUnit2EnrollmentDirectionSet.get(foramtiveOrgUnit);
            if(set == null)
                formativeOrgUnit2EnrollmentDirectionSet.put(foramtiveOrgUnit, set = new HashSet<>());
            set.add(direction);

            if(!formativeOrgUnitId2ReportRow.containsKey(foramtiveOrgUnit.getId()))
                formativeOrgUnitId2ReportRow.put(foramtiveOrgUnit.getId(), new ReportRow(StringUtils.isNotEmpty(foramtiveOrgUnit.getNominativeCaseTitle()) ? foramtiveOrgUnit.getNominativeCaseTitle() : foramtiveOrgUnit.getTitle()));
        }

        List<Long> EGEEnrollmentDirectionIds = getEGEEnrollmentDirectionIds();
        for(Map.Entry<OrgUnit, Set<EnrollmentDirection>> entry : formativeOrgUnit2EnrollmentDirectionSet.entrySet())
        {
            ReportRow row = formativeOrgUnitId2ReportRow.get(entry.getKey().getId());
            for(EnrollmentDirection direction : entry.getValue())
            {
                row.eduLevelsHighSchoolIds.add(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId());
                if(EGEEnrollmentDirectionIds.contains(direction.getId()))
                    row.eduLevelsHighSchoolEGEIds.add(direction.getEducationOrgUnit().getEducationLevelHighSchool().getId());
                if(direction.getMinisterialPlan() != null)
                    row.budgetPlan += direction.getMinisterialPlan();
            }
        }

        MQBuilder directionBuilder = getBaseDirectionBuilder(enrollmentDirectionList);
        Map<Long, Integer> directionId2NotEmptyEGEChosenDisciplineCount = getDirectionId2NotEmptyEGEChosenDisciplineCount(directionBuilder);
        Map<Long, Integer> directionId2ChosenDisciplineCount = getDirectionId2ChosenDisciplineCount(directionBuilder);

        directionBuilder.addSelect("r", new String[] {
                "id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST+".id",
                RequestedEnrollmentDirection.L_COMPENSATION_TYPE+"."+ CompensationType.P_CODE,
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirection.L_EDUCATION_ORG_UNIT+"."+ EducationOrgUnit.L_FORMATIVE_ORG_UNIT+".id",
                RequestedEnrollmentDirection.L_ENTRANT_REQUEST+"."+EntrantRequest.L_ENTRANT+".id"
        });

        Set<Long> humanRequested = new HashSet<>();
        Set<Long> humanRequestedEGE = new HashSet<>();
        for(Object[] obj : directionBuilder.<Object[]>getResultList(_session))
        {
            Long directionId = (Long)obj[0];
            Long requestId = (Long)obj[1];
            String compensationTypeCode = (String)obj[2];
            Long formativeOrgUnitId = (Long)obj[3];
            Long entrantId = (Long)obj[4];

            ReportRow row = formativeOrgUnitId2ReportRow.get(formativeOrgUnitId);
            row.requested.add(new MultiKey(requestId, compensationTypeCode));
            humanRequested.add(entrantId);
            if(directionId2NotEmptyEGEChosenDisciplineCount.containsKey(directionId))
            {
                row.requestedEGE.add(new MultiKey(requestId, compensationTypeCode));
                humanRequestedEGE.add(entrantId);
            }
        }

        MQBuilder preliminaryBuilder = getBasePreliminaryBuilder(enrollmentDirectionList);
        for(PreliminaryEnrollmentStudent preliminary : preliminaryBuilder.<PreliminaryEnrollmentStudent>getResultList(_session))
        {
            ReportRow row = formativeOrgUnitId2ReportRow.get(preliminary.getEducationOrgUnit().getFormativeOrgUnit().getId());
            AddressBase address = preliminary.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getAddress();
            AddressItem city = address != null && address instanceof AddressDetailed ? ((AddressDetailed)address).getSettlement() : null;
            boolean ege = directionId2NotEmptyEGEChosenDisciplineCount.containsKey(preliminary.getRequestedEnrollmentDirection().getId());

            if(preliminary.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET))
            {
                row.preliminaryBudgetIds.add(preliminary.getId());
                if(city == null || !city.equals(academyCity))
                {
                    row.preliminaryBudgetOtherCityIds.add(preliminary.getId());
                    if(ege)
                        row.preliminaryBudgetOtherCityEGEIds.add(preliminary.getId());
                }
                if(city != null && city.getAddressType().isCountryside())
                {
                    row.preliminaryBudgetCountrysideIds.add(preliminary.getId());
                    if(ege)
                        row.preliminaryBudgetCountrysideEGEIds.add(preliminary.getId());
                }
                if(ege)
                {
                    row.preliminaryBudgetEGEIds.add(preliminary.getId());
                    Integer notEmptyEGEChosenDisciplineCount = directionId2NotEmptyEGEChosenDisciplineCount.get(preliminary.getRequestedEnrollmentDirection().getId());
                    Integer chosenDisciplineCount = directionId2ChosenDisciplineCount.get(preliminary.getRequestedEnrollmentDirection().getId());
                    if(notEmptyEGEChosenDisciplineCount == 1)
                        row.preliminaryBudgetEGEOneIds.add(preliminary.getId());
                    if(notEmptyEGEChosenDisciplineCount > 1 && notEmptyEGEChosenDisciplineCount < chosenDisciplineCount)
                        row.preliminaryBudgetEGEPartIds.add(preliminary.getId());
                    if(notEmptyEGEChosenDisciplineCount.equals(chosenDisciplineCount))
                        row.preliminaryBudgetEGEAllIds.add(preliminary.getId());
                }
            }
            else
            {
                row.preliminaryContractIds.add(preliminary.getId());
                if(city == null || !city.equals(academyCity))
                {
                    row.preliminaryContractOtherCityIds.add(preliminary.getId());
                    if(ege)
                        row.preliminaryContractOtherCityEGEIds.add(preliminary.getId());
                }
                if(city != null && city.getAddressType().isCountryside())
                {
                    row.preliminaryContractCountrysideIds.add(preliminary.getId());
                    if(ege)
                        row.preliminaryContractCountrysideEGEIds.add(preliminary.getId());
                }
                if(ege)
                {
                    Integer notEmptyEGEChosenDisciplineCount = directionId2NotEmptyEGEChosenDisciplineCount.get(preliminary.getRequestedEnrollmentDirection().getId());
                    Integer chosenDisciplineCount = directionId2ChosenDisciplineCount.get(preliminary.getRequestedEnrollmentDirection().getId());
                    row.preliminaryContractEGEIds.add(preliminary.getId());
                    if(notEmptyEGEChosenDisciplineCount == 1)
                        row.preliminaryContractEGEOneIds.add(preliminary.getId());
                    if(notEmptyEGEChosenDisciplineCount > 1 && notEmptyEGEChosenDisciplineCount < chosenDisciplineCount)
                        row.preliminaryContractEGEPartIds.add(preliminary.getId());
                    if(notEmptyEGEChosenDisciplineCount.equals(chosenDisciplineCount))
                        row.preliminaryContractEGEAllIds.add(preliminary.getId());
                }
            }
        }

        String[][] data = new String[formativeOrgUnit2EnrollmentDirectionSet.size() * 2][24];
        List<OrgUnit> formativeOrgUnitList = new ArrayList<>(formativeOrgUnit2EnrollmentDirectionSet.keySet());
        Collections.sort(formativeOrgUnitList, ITitled.TITLED_COMPARATOR);
        int i = 0;
        int[] total = new int[23];
        for(OrgUnit formativeOrgUnit : formativeOrgUnitList)
        {
            ReportRow row = formativeOrgUnitId2ReportRow.get(formativeOrgUnit.getId());
            data[i++] = new String[]{row.title};
            data[i++] = new String[]{"", convert(row.eduLevelsHighSchoolIds),convert(row.eduLevelsHighSchoolEGEIds),
                    convert(row.requested),convert(row.requestedEGE),String.valueOf(row.budgetPlan),
                    convert(row.preliminaryContractIds),convert(row.preliminaryContractOtherCityIds),convert(row.preliminaryContractCountrysideIds),
                    convert(row.preliminaryContractEGEIds),convert(row.preliminaryContractOtherCityEGEIds),convert(row.preliminaryContractCountrysideEGEIds),
                    convert(row.preliminaryContractEGEOneIds),convert(row.preliminaryContractEGEPartIds),convert(row.preliminaryContractEGEAllIds),
                    convert(row.preliminaryBudgetIds),convert(row.preliminaryBudgetOtherCityIds),convert(row.preliminaryBudgetCountrysideIds),
                    convert(row.preliminaryBudgetEGEIds),convert(row.preliminaryBudgetOtherCityEGEIds),convert(row.preliminaryBudgetCountrysideEGEIds),
                    convert(row.preliminaryBudgetEGEOneIds),convert(row.preliminaryBudgetEGEPartIds),convert(row.preliminaryBudgetEGEAllIds)};

            total[0] += row.eduLevelsHighSchoolIds.size();
            total[1] += row.eduLevelsHighSchoolEGEIds.size();
            total[2] += row.requested.size();
            total[3] += row.requestedEGE.size();
            total[4] += row.budgetPlan;
            total[5] += row.preliminaryContractIds.size();
            total[6] += row.preliminaryContractOtherCityIds.size();
            total[7] += row.preliminaryContractCountrysideIds.size();
            total[8] += row.preliminaryContractEGEIds.size();
            total[9] += row.preliminaryContractOtherCityEGEIds.size();
            total[10] += row.preliminaryContractCountrysideEGEIds.size();
            total[11] += row.preliminaryContractEGEOneIds.size();
            total[12] += row.preliminaryContractEGEPartIds.size();
            total[13] += row.preliminaryContractEGEAllIds.size();
            total[14] += row.preliminaryBudgetIds.size();
            total[15] += row.preliminaryBudgetOtherCityIds.size();
            total[16] += row.preliminaryBudgetCountrysideIds.size();
            total[17] += row.preliminaryBudgetEGEIds.size();
            total[18] += row.preliminaryBudgetOtherCityEGEIds.size();
            total[19] += row.preliminaryBudgetCountrysideEGEIds.size();
            total[20] += row.preliminaryBudgetEGEOneIds.size();
            total[21] += row.preliminaryBudgetEGEPartIds.size();
            total[22] += row.preliminaryBudgetEGEAllIds.size();
        }

        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", data);
        tableModifier.modify(document);

        String[][] totalData = new String[4][24];
        for(int j = 0; j < 4; j++)
            totalData[j] = new String[24];
        for(int j = 0; j < 23; j++)
        {
            totalData[1][j + 1] = String.valueOf(total[j]);
            totalData[3][j + 1] = String.valueOf(total[j]);
        }
        totalData[0][0] = "ИТОГО (по заявлениям):";
        totalData[2][0] = "ИТОГО (человек):";
        totalData[3][3] = String.valueOf(humanRequested.size());
        totalData[3][4] = String.valueOf(humanRequestedEGE.size());

        tableModifier = new RtfTableModifier();
        tableModifier.put("T1", totalData);
        tableModifier.modify(document);

        for(int j = 6; j < table.getRowList().size(); j += 2)
        {
            table.getRowList().get(j).getCellList().get(0).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);

            RtfBorder oldBottom = table.getRowList().get(j).getCellList().get(0).getCellBorder().getBottom();
            RtfBorder newBottom = RtfBorder.getInstance(oldBottom.getWidth(), 17, oldBottom.getStyle());
            table.getRowList().get(j).getCellList().get(0).getCellBorder().setBottom(newBottom);
            for(int k = 1; k < table.getRowList().get(j).getCellList().size(); k++)
                table.getRowList().get(j).getCellList().get(k).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        RtfWriter.write(document, out);
        return out.toByteArray();
    }

    private static String convert(Set set)
    {
        return set == null ? null : String.valueOf(set.size());
    }

    private List<EnrollmentDirection> filterByQualification(List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EnrollmentDirection> result = new ArrayList<>();
        for(EnrollmentDirection direction : enrollmentDirectionList)
            if(_model.getQualificationList().contains(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
                result.add(direction);
        return result;
    }

    private MQBuilder getBaseDirectionBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if(_model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        return builder;
    }

    private MQBuilder getBasePreliminaryBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if(_model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if(_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        return builder;
    }

    private List<Long> getEGEEnrollmentDirectionIds()
    {
        MQBuilder builder = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "entranceDiscipline", new String[] {
                EntranceDiscipline.L_ENROLLMENT_DIRECTION+".id"});
        builder.add(MQExpression.in("entranceDiscipline", EntranceDiscipline.L_ENROLLMENT_DIRECTION+"."+EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        if(_model.isStudentCategoryActive())
            builder.add(MQExpression.in("entranceDiscipline", EntranceDiscipline.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        builder.addDomain("discipline2RealizationFormRelation", Discipline2RealizationFormRelation.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("entranceDiscipline", EntranceDiscipline.L_DISCIPLINE, "discipline2RealizationFormRelation", Discipline2RealizationFormRelation.L_DISCIPLINE));
        builder.add(MQExpression.in("discipline2RealizationFormRelation", Discipline2RealizationFormRelation.L_SUBJECT_PASS_FORM+"."+SubjectPassForm.P_CODE,
                                    UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM, UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL));
        builder.setNeedDistinct(true);
        return builder.<Long>getResultList(_session);
    }

    private Map<Long, Integer> getDirectionId2NotEmptyEGEChosenDisciplineCount(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[] {
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION+".id", "id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE, UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1,
                                    UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2,
                                    UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL,
                                    UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL));
        builder.setNeedDistinct(true);
        Map<Long, Integer> directionId2ChosenDisciplineCount = new HashMap<>();
        for(Object[] obj : builder.<Object[]>getResultList(_session))
        {
            Long directionId = (Long) obj[0];
            Integer count = directionId2ChosenDisciplineCount.get(directionId);
            directionId2ChosenDisciplineCount.put(directionId, count == null ? 1 : count + 1);
        }
        return directionId2ChosenDisciplineCount;
    }

    private Map<Long, Integer> getDirectionId2ChosenDisciplineCount(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[] {
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION+".id", "id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.setNeedDistinct(true);
        Map<Long, Integer> directionId2ChosenDisciplineCount = new HashMap<>();
        for(Object[] obj : builder.<Object[]>getResultList(_session))
        {
            Long directionId = (Long) obj[0];
            Integer count = directionId2ChosenDisciplineCount.get(directionId);
            directionId2ChosenDisciplineCount.put(directionId, count == null ? 1 : count + 1);
        }
        return directionId2ChosenDisciplineCount;
    }

    @SuppressWarnings("unchecked")
    private class ReportRow implements Comparable
    {
        private String title;
        private Set<Long> eduLevelsHighSchoolIds = new HashSet<>();
        private Set<Long> eduLevelsHighSchoolEGEIds = new HashSet<>();
        private Set<MultiKey> requested = new HashSet<>(); //ключ = (заявление id, вид затрат id)
        private Set<MultiKey> requestedEGE = new HashSet<>();
        private int budgetPlan = 0;

        private Set<Long> preliminaryBudgetIds = new HashSet<>();
        private Set<Long> preliminaryBudgetOtherCityIds = new HashSet<>();
        private Set<Long> preliminaryBudgetCountrysideIds = new HashSet<>();
        private Set<Long> preliminaryBudgetEGEIds = new HashSet<>();
        private Set<Long> preliminaryBudgetOtherCityEGEIds = new HashSet<>();
        private Set<Long> preliminaryBudgetCountrysideEGEIds = new HashSet<>();
        private Set<Long> preliminaryBudgetEGEOneIds = new HashSet<>();
        private Set<Long> preliminaryBudgetEGEPartIds = new HashSet<>();
        private Set<Long> preliminaryBudgetEGEAllIds = new HashSet<>();

        private Set<Long> preliminaryContractIds = new HashSet<>();
        private Set<Long> preliminaryContractOtherCityIds = new HashSet<>();
        private Set<Long> preliminaryContractCountrysideIds = new HashSet<>();
        private Set<Long> preliminaryContractEGEIds = new HashSet<>();
        private Set<Long> preliminaryContractOtherCityEGEIds = new HashSet<>();
        private Set<Long> preliminaryContractCountrysideEGEIds = new HashSet<>();
        private Set<Long> preliminaryContractEGEOneIds = new HashSet<>();
        private Set<Long> preliminaryContractEGEPartIds = new HashSet<>();
        private Set<Long> preliminaryContractEGEAllIds = new HashSet<>();

        private ReportRow(String title)
        {
            this.title = title;
        }

        @Override
        public int compareTo(Object o)
        {
            return title.compareTo(((ReportRow)o).title);
        }
    }
}
