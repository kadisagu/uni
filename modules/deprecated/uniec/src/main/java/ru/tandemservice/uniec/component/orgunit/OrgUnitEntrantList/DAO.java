/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList.Model.ArchivalSelectOption;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author agolubenko
 * @since 24.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _entrantOrderSettings = new OrderDescriptionRegistry("entrant");
    static
    {
        _entrantOrderSettings.setOrders(Entrant.P_FULLFIO, new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(Entrant.P_PERSONAL_NUMBER, new OrderDescription("entrant", Entrant.P_PERSONAL_NUMBER), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(PersonRole.person().identityCard().sex().shortTitle().s(), new OrderDescription("identityCard", IdentityCard.sex().shortTitle().s()), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(PersonRole.person().identityCard().birthDate().s(), new OrderDescription("identityCard", IdentityCard.birthDate().s()), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(Entrant.P_REGISTRATION_DATE, new OrderDescription("entrant", Entrant.P_REGISTRATION_DATE), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(PersonRole.person().identityCard().citizenship().title().s(), new OrderDescription("identityCard", IdentityCard.citizenship().title().s()), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrgUnit(getNotNull(OrgUnit.class, model.getOrgUnitId()));

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setQualificationModel(new QualificationModel(getSession()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setCitizenShipModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if(null != primaryKey)
                {
                    if(primaryKey.equals(0L))
                    {
                        AddressCountryVO countryVO = new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION);
                        if(findValues("").getObjects().contains(countryVO)) return countryVO;
                        else return null;

                    }
                    else
                    {
                        AddressCountryVO countryVO = new AddressCountryVO(get((Long) primaryKey));
                        if(findValues("").getObjects().contains(countryVO)) return countryVO;
                        else return null;
                    }
                }
                return null;
            }

            @Override
            public ListResult findValues(final String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressCountry.class, "o");

                builder.order(property("o", AddressCountry.title()));
                List<AddressCountry> addressCountries = builder.createStatement(getSession()).list();

                List<AddressCountryVO> addressCountryVOs = Lists.newArrayList();

                addressCountryVOs.add(new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION));

                for(AddressCountry addressCountry : addressCountries)
                {
                    addressCountryVOs.add(new AddressCountryVO(addressCountry));
                }

                CollectionUtils.filter(addressCountryVOs, object -> {
                    if (!StringUtils.isEmpty(filter))
                    {
                        AddressCountryVO c = ((AddressCountryVO) object);
                        return null == c || StringUtils.containsIgnoreCase(c.getTitle(), filter.trim().replace(" ", "*"));
                    }
                    return true;
                });

                return new ListResult<>(addressCountryVOs);
            }
        });
        Long citizenship = model.getSettings().get("citizenship");
        if(null != citizenship)
        {
            if(citizenship.equals(0L))
            {
                model.setCitizenship(new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION));
            }
            else
            {
                model.setCitizenship(this.<AddressCountryVO>get(citizenship));
            }

        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<Entrant> dataSource = model.getDataSource();

        if (model.getEnrollmentCampaign() == null)
        {
            UniBaseUtils.createPage(dataSource, Collections.<Entrant> emptyList());
            return;
        }
        
        MQBuilder builder = getEntrantListBuilder(model);
        UniBaseUtils.createPage(dataSource, builder, getSession());

        Map<Entrant, List<String>> map = new HashMap<>();
        for (EntrantRequest request : new MQBuilder(EntrantRequest.ENTITY_CLASS, "r").add(MQExpression.in("r", EntrantRequest.L_ENTRANT, dataSource.getEntityList())).addOrder("r", EntrantRequest.P_REG_DATE).<EntrantRequest> getResultList(getSession()))
        {
            List<String> numberList = map.get(request.getEntrant());
            if (numberList == null)
            {
                map.put(request.getEntrant(), numberList = new ArrayList<>());
            }
            numberList.add(request.getStringNumber());
        }

        for (ViewWrapper<Entrant> view : ViewWrapper.<Entrant> getPatchedList(dataSource))
        {
            List<String> numberList = map.get(view.getEntity());
            view.setViewProperty(Model.REQUEST_NUMBER, numberList == null ? "" : StringUtils.join(numberList, ", "));
        }
    }

    private MQBuilder getEntrantListBuilder(Model model)
    {
        MQBuilder builder = new MQBuilder(Entrant.ENTITY_CLASS, "entrant");
        builder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        builder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");

        IDataSettings settings = model.getSettings();
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        String lastName = settings.get("lastNameFilter");
        String firstName = settings.get("firstNameFilter");
        String middleName = settings.get("middleNameFilter");
        String personalNumber = settings.get("personalNumberFilter");
        Number requestNumber = settings.get("requestNumberFilter");
        Date dateFrom = settings.get("registeredFromFilter");
        Date dateTo = settings.get("registeredToFilter");
        CompensationType compensationType = settings.get("compensationTypeFilter");
        List<Qualifications> qualificationsList = settings.get("qualificationFilter");
        DevelopForm developForm = settings.get("developFormFilter");
        List<DevelopCondition> developConditionList = settings.get("developConditionFilter");
        String cardSeria = settings.get("identityCardSeriaFilter");
        String cardNumber = settings.get("identityCardNumberFilter");
        ArchivalSelectOption archivalSelectOption = settings.get("archival");

        boolean developConditionActive = (developConditionList != null && !developConditionList.isEmpty());
        boolean qualificationActive = (qualificationsList != null && !qualificationsList.isEmpty());

        builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (StringUtils.isNotEmpty(lastName))
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(lastName)));
        }
        if (StringUtils.isNotEmpty(firstName))
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_FIRST_NAME, CoreStringUtils.escapeLike(firstName)));
        }
        if (StringUtils.isNotEmpty(middleName))
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_MIDDLE_NAME, CoreStringUtils.escapeLike(middleName)));
        }
        if (StringUtils.isNotEmpty(personalNumber))
        {
            builder.add(MQExpression.like("entrant", Entrant.P_PERSONAL_NUMBER, "%" + personalNumber));
        }

        MQBuilder requestBuilder = new MQBuilder(EntrantRequest.ENTITY_CLASS, "request", new String[] { EntrantRequest.L_ENTRANT + ".id" });
        requestBuilder.setNeedDistinct(true);

        requestBuilder.addDomain("requestedEnrollmentDirection", RequestedEnrollmentDirection.ENTITY_CLASS);
        requestBuilder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        requestBuilder.add(MQExpression.or(MQExpression.eq("educationOrgUnit", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit()), MQExpression.eq("educationOrgUnit", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit())));
        requestBuilder.add(MQExpression.eqProperty("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id", "request", "id"));

        if (requestNumber != null)
        {
            requestBuilder.add(MQExpression.eq("request", EntrantRequest.P_REG_NUMBER, requestNumber.intValue()));
        }
        if (dateFrom != null)
        {
            requestBuilder.add(UniMQExpression.greatOrEq("request", EntrantRequest.P_REG_DATE, dateFrom));
        }
        if (dateTo != null)
        {
            requestBuilder.add(UniMQExpression.lessOrEq("request", EntrantRequest.P_REG_DATE, dateTo));
        }
        if (compensationType != null)
        {
            requestBuilder.add(MQExpression.eq("requestedEnrollmentDirection", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, compensationType));
        }
        if (developForm != null)
        {
            requestBuilder.add(MQExpression.eq("educationOrgUnit", EducationOrgUnit.L_DEVELOP_FORM, developForm));
        }
        if (developConditionActive)
        {
            requestBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.L_DEVELOP_CONDITION + ".id", CommonBaseEntityUtil.getIdList(developConditionList)));
        }
        if (qualificationActive)
        {
            requestBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION + ".id", CommonBaseEntityUtil.getIdList(qualificationsList)));
        }
        builder.add(MQExpression.in("entrant", Entrant.P_ID, requestBuilder));

        if (StringUtils.isNotEmpty(cardSeria))
        {
            builder.add(MQExpression.eq("identityCard", IdentityCard.P_SERIA, cardSeria));
        }
        if (StringUtils.isNotEmpty(cardNumber))
        {
            builder.add(MQExpression.eq("identityCard", IdentityCard.P_NUMBER, cardNumber));
        }
        if (archivalSelectOption != null)
        {
            builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, archivalSelectOption.isArchival()));
        }

        if(null != model.getCitizenship())
        {
            if(model.getCitizenship().getId().equals(0L))
            {
                builder.add(MQExpression.notEq("entrant", Entrant.person().identityCard().citizenship().code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
            }
            else
            {
                builder.add(MQExpression.eq("entrant", Entrant.person().identityCard().citizenship().id(), model.getCitizenship().getId()));
            }
        }

        _entrantOrderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());

        return builder;
    }

    @Override
    public List<Long> getEntrantIds(Model model)
    {
        MQBuilder builder = getEntrantListBuilder(model);
        builder.getSelectAliasList().clear();
        builder.addSelect("entrant", new String[]{Entrant.P_ID});
        return builder.getResultList(getSession());
    }

    @Override
    public String[][] getHeaderTable(Model model)
    {
        List<String[]> headerTableData = new ArrayList<>();

        IDataSettings settings = model.getSettings();
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        headerTableData.add(new String[]{"Приемная кампания:", enrollmentCampaign.getTitle()});
        String lastName = settings.get("lastNameFilter");
        if (lastName != null)
            headerTableData.add(new String[]{"Фамилия:", lastName});
        String firstName = settings.get("firstNameFilter");
        if (firstName != null)
            headerTableData.add(new String[]{"Имя:", firstName});
        String middleName = settings.get("middleNameFilter");
        if (middleName != null)
            headerTableData.add(new String[]{"Отчество:", middleName});
        String personalNumber =  settings.get("personalNumberFilter");
        if (personalNumber != null)
            headerTableData.add(new String[]{"Личный номер:", personalNumber});
        Number requestNumber = settings.get("requestNumberFilter");
        if (requestNumber != null)
            headerTableData.add(new String[]{"Номер заявления:", String.valueOf(requestNumber)});
        Date dateFrom = settings.get("registeredFromFilter");
        if (dateFrom != null)
            headerTableData.add(new String[]{"Заявления с:", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom)});
        Date dateTo = settings.get("registeredToFilter");
        if (dateTo != null)
            headerTableData.add(new String[]{"Заявления по:", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo)});
        CompensationType compensationType = settings.get("compensationTypeFilter");
        if (compensationType != null)
            headerTableData.add(new String[]{"Вид возмещения затрат:", compensationType.getShortTitle()});
        List<StudentCategory> studentCategoryList = settings.get("studentCategoryFilter");
        if (studentCategoryList != null && !studentCategoryList.isEmpty())
            headerTableData.add(new String[]{"Категория поступающего:", CommonBaseStringUtil.join(studentCategoryList, "title", ", ")});
        List<Qualifications> qualificationsList = settings.get("qualificationFilter");
        if (qualificationsList != null && !qualificationsList.isEmpty())
            headerTableData.add(new String[]{"Квалификация:", CommonBaseStringUtil.join(qualificationsList, "title", ", ")});
        OrgUnit formativeOrgUnit = settings.get("formativeOrgUnitFilter");
        if (formativeOrgUnit != null)
            headerTableData.add(new String[]{"Формирующее подр.:", formativeOrgUnit.getTitle()});
        DevelopForm developForm = settings.get("developFormFilter");
        if (developForm != null)
            headerTableData.add(new String[]{"Форма освоения:", developForm.getTitle()});
        List<DevelopCondition> developConditionList = settings.get("developConditionFilter");
        if (developConditionList != null && !developConditionList.isEmpty())
            headerTableData.add(new String[]{"Условие освоения:", CommonBaseStringUtil.join(developConditionList, "title", ", ")});
        String cardSeria = settings.get("identityCardSeriaFilter");
        if (cardSeria != null)
            headerTableData.add(new String[]{"Серия паспорта:", cardSeria});
        String cardNumber = settings.get("identityCardNumberFilter");
        if (cardNumber != null)
            headerTableData.add(new String[]{"Номер паспорта:", cardNumber});
        IdentifiableWrapper archivalSelectOption = settings.get("archivalFilter");
        if (archivalSelectOption != null)
            headerTableData.add(new String[]{"Статус абитуриента:", archivalSelectOption.getTitle()});
        return headerTableData.toArray(new String[headerTableData.size()][]);
    }
}
