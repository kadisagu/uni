/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public class EcgCortegeDTO implements IEcgCortegeDTO
{
    private IEntity _entity;

    private boolean _targetAdmission;

    private TargetAdmissionKind _targetAdmissionKind;

    private CompetitionKind _competitionKind;

    private Double _finalMark;

    private Double _profileMark;

    private Boolean _graduatedProfileEduInstitution;

    private Double _certificateAverageMark;

    private String _fio;

    public EcgCortegeDTO(IEntity entity, boolean targetAdmission, TargetAdmissionKind targetAdmissionKind, CompetitionKind competitionKind, Double finalMark, Double profileMark, boolean graduatedProfileEduInstitution, Double certificateAverageMark, String fio)
    {
        _entity = entity;
        _targetAdmission = targetAdmission;
        _targetAdmissionKind = targetAdmissionKind;
        _competitionKind = competitionKind;
        _finalMark = finalMark;
        _profileMark = profileMark;
        _graduatedProfileEduInstitution = graduatedProfileEduInstitution;
        _certificateAverageMark = certificateAverageMark;
        _fio = fio;
    }

    // Getters

    @Override
    public IEntity getEntity()
    {
        return _entity;
    }

    @Override
    public boolean isTargetAdmission()
    {
        return _targetAdmission;
    }

    @Override
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    @Override
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    @Override
    public Double getFinalMark()
    {
        return _finalMark;
    }

    @Override
    public Double getProfileMark()
    {
        return _profileMark;
    }

    @Override
    public Boolean isGraduatedProfileEduInstitution()
    {
        return _graduatedProfileEduInstitution;
    }

    @Override
    public Double getCertificateAverageMark()
    {
        return _certificateAverageMark;
    }

    @Override
    public String getFio()
    {
        return _fio;
    }
}
