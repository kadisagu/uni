/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.util;

import org.tandemframework.core.meta.component.IComponentMeta;
import org.tandemframework.core.runtime.BusinessComponentRuntime;

import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;

/**
 * @author Vasily Zhukov
 * @since 24.05.2011
 */
public class EcOrderUtil
{
    public static String getParagraphAddEditComponent(EntrantEnrollmentOrderType orderType)
    {
        String name = orderType.getComponentPrefix() + "ParAddEdit";

        IComponentMeta meta = BusinessComponentRuntime.getInstance().getComponentMeta(name);

        if (meta == null)
            throw new RuntimeException("No paragraph add/edit component found for order type '" + orderType.getTitle() + "', expected '" + name + "'");

        return meta.getName();
    }

    public static String getParagraphPubComponent(EntrantEnrollmentOrderType orderType)
    {
        String name = orderType.getComponentPrefix() + "ParPub";

        IComponentMeta meta = BusinessComponentRuntime.getInstance().getComponentMeta(name);

        if (meta == null)
            throw new RuntimeException("No paragraph pub component found for order type '" + orderType.getTitle() + "', expected '" + name + "'");

        return meta.getName();
    }
}
