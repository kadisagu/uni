/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent1.e1.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.uniec.entity.orders.SplitBudgetBachelorEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 13.07.2012
 */
public class DAO extends AbstractListExtractPubDAO<SplitBudgetBachelorEntrantsStuListExtract, Model> implements IDAO
{
}