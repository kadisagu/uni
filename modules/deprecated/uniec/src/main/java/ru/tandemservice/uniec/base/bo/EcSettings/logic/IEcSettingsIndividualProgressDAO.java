/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import javax.validation.constraints.NotNull;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
public interface IEcSettingsIndividualProgressDAO
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void toggleIndividualProgressUse(@NotNull Long individualProgressId, @NotNull EnrollmentCampaign enrollmentCampaign);
}