/* $Id:$ */
package ru.tandemservice.uniec.fis;

import org.tandemframework.core.CoreExceptionUtils;
import ru.tandemservice.uniec.fis.gen.del.DeletePackageInfo;
import ru.tandemservice.uniec.fis.gen.dic.Dictionaries;
import ru.tandemservice.uniec.fis.gen.dic.Root;
import ru.tandemservice.uniec.fis.gen.dic_det.DictionaryData;
import ru.tandemservice.uniec.fis.gen.imp.ImportPackageInfo;
import ru.tandemservice.uniec.fis.gen.imp.TEntranceTestSubject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
* @author oleyba
* @since 4/27/12
*/
public class TestConnect
{
    public static final String LOGIN = "alukshin@tandemservice.ru";
    public static final String PASS = "Spi9rbc";

    public static void main(String[] args)
    {
        try {
            //printDicContent();
            //checkApplication();
            //importAdmissionInfoTest();

            //importApplicationsTest();
            importResult("3726");

            //deleteAppTest();
            //deleteResult("1708");
        } catch (Throwable e) {
            System.out.println("!");
        }
    }

    /*

/checkapplication 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/checkapplication
/delete 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/delete
/delete/result 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/delete/result
/dictionary 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/dictionary
/dictionarydetails 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/dictionarydetails
/import 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/import
/import/result 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/import/result
/institutioninfo 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/institutioninfo
/test/checkapplication 	GET 	Service at http://priem.edu.ru:8000/import/ImportService.svc/test/checkapplication
/test/delete 	GET 	Service at http://priem.edu.ru:8000/import/ImportService.svc/test/delete
/test/dictionary 	GET 	Service at http://priem.edu.ru:8000/import/ImportService.svc/test/dictionary
/test/dictionarydetails 	GET 	Service at http://priem.edu.ru:8000/import/ImportService.svc/test/dictionarydetails
/test/import 	GET 	Service at http://priem.edu.ru:8000/import/ImportService.svc/test/import
/validate 	POST 	Service at http://priem.edu.ru:8000/import/ImportService.svc/validate

     */

    private static void deleteAppTest() throws IOException, JAXBException
    {
        HttpURLConnection conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/delete");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

        ru.tandemservice.uniec.fis.gen.del.Root root = new ru.tandemservice.uniec.fis.gen.del.Root();
        root.setAuthData(new ru.tandemservice.uniec.fis.gen.del.Root.AuthData());
        root.getAuthData().setLogin(LOGIN);
        root.getAuthData().setPass(PASS);

        root.setDataForDelete(new ru.tandemservice.uniec.fis.gen.del.Root.DataForDelete());
        root.getDataForDelete().setApplications(new ru.tandemservice.uniec.fis.gen.del.Root.DataForDelete.Applications());
        ru.tandemservice.uniec.fis.gen.del.Root.DataForDelete.Applications.Application app = new ru.tandemservice.uniec.fis.gen.del.Root.DataForDelete.Applications.Application();
        root.getDataForDelete().getApplications().getApplication().add(app);

        app.setApplicationNumber("7");

        XMLGregorianCalendar xmlDate = null;
        try {
            DatatypeFactory dataTypeFactory = DatatypeFactory.newInstance();
            Date date = new Date();
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            xmlDate = dataTypeFactory.newXMLGregorianCalendar(gc);

        } catch (Exception e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        app.setRegistrationDate(xmlDate);

        write("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/tmp.xml", root);

        write(wr, root);
        wr.flush();

        StringBuilder result = new StringBuilder();
        if (true) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
                System.out.println(line);
            }
            rd.close();
        }

        try {
            DeletePackageInfo read = (DeletePackageInfo) read(new ByteArrayInputStream(result.toString().getBytes()), DeletePackageInfo.class);
            write("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/" + read.getPackageID().toString() + ".xml", root);
        }
        catch (Throwable e) {
            System.out.print(".");
        }
    }

    private static void importApplicationsTest() throws IOException, JAXBException
    {
        String app_UID = "test_app3";
        String app_number = "7";
        String entrant_uid = "test_entr1";

        for (int i = 8; i <= 1008; i++)
            importApplication(app_UID, String.valueOf(i), entrant_uid, false);
    }

    private static void importApplicationTest() throws IOException, JAXBException
    {
        String app_UID = "test_app3";
        String app_number = "7";
        String entrant_uid = "test_entr1";

        importApplication(app_UID, app_number, entrant_uid, true);
    }

    private static void importApplication(String app_UID, String app_number, String entrant_uid, boolean saveFile) throws IOException, JAXBException
    {HttpURLConnection conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/import");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

        final ru.tandemservice.uniec.fis.gen.imp.Root root = new ru.tandemservice.uniec.fis.gen.imp.Root();
        root.setAuthData(new ru.tandemservice.uniec.fis.gen.imp.Root.AuthData());
        root.getAuthData().setLogin(LOGIN);
        root.getAuthData().setPass(PASS);

        root.setPackageData(new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData());
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData packageData = root.getPackageData();

        packageData.setApplications(new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications());
        List<ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application> appList = packageData.getApplications().getApplication();

        //addApplication(appList, "test_app1", "5", "test_entr1");
        addApplication(appList, app_UID, app_number, entrant_uid);

        write("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/tmp.xml", root);

        write(wr, root);
        wr.flush();

        StringBuilder result = new StringBuilder();
        if (true) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
                System.out.println(line);
            }
            rd.close();
        }

        if (saveFile)
            try {
                ImportPackageInfo read = (ImportPackageInfo) read(new ByteArrayInputStream(result.toString().getBytes()), ImportPackageInfo.class);
                write("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/" + read.getPackageID().toString() + ".xml", root);
            }
            catch (Throwable e) {
                System.out.print(".");
            }
    }

    private static void checkApplication() throws IOException
    {
        URL url = new URL("http://priem.edu.ru:8000/import/importservice.svc/test/checkapplication");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setDoOutput(true);

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
        }
        rd.close();

    }

    private static void deleteResult(String code) throws IOException, JAXBException
    {
        HttpURLConnection conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/delete/result");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

        ru.tandemservice.uniec.fis.gen.del_result.Root root = new ru.tandemservice.uniec.fis.gen.del_result.Root();
        root.setAuthData(new ru.tandemservice.uniec.fis.gen.del_result.Root.AuthData());
        root.getAuthData().setLogin(LOGIN);
        root.getAuthData().setPass(PASS);
        root.setGetResultDeleteApplication(new ru.tandemservice.uniec.fis.gen.del_result.Root.GetResultDeleteApplication());
        root.getGetResultDeleteApplication().setPackageID(new BigInteger(code));

        write(wr, root);
        wr.flush();

        StringBuilder result = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
            result.append(line);
        }
        rd.close();
        wr.close();
        conn.disconnect();

        File file = new File("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/" + code + "_result.xml");
        file.createNewFile();
        final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(result.toString());
        writer.flush();
    }


    private static void importResult(String code) throws IOException, JAXBException
    {
        HttpURLConnection conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/import/result");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

        ru.tandemservice.uniec.fis.gen.imp_result.Root root = new ru.tandemservice.uniec.fis.gen.imp_result.Root();
        root.setAuthData(new ru.tandemservice.uniec.fis.gen.imp_result.Root.AuthData());
        root.getAuthData().setLogin(LOGIN);
        root.getAuthData().setPass(PASS);
        root.setGetResultImportApplication(new ru.tandemservice.uniec.fis.gen.imp_result.Root.GetResultImportApplication());
        root.getGetResultImportApplication().setPackageID(new BigInteger(code));

        write(wr, root);
        wr.flush();

        StringBuilder result = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
            result.append(line);
        }
        rd.close();
        wr.close();
        conn.disconnect();

        File file = new File("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/" + code + "_result.xml");
        file.createNewFile();
        final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(result.toString());
        writer.flush();
        writer.close();
    }

    private static void addApplication(List<ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application> appList, String app_uid, String app_number, String entrant_uid)
    {
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application application = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application();
        appList.add(application);

        application.setUID(app_uid);
        application.setApplicationNumber(app_number);

        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.Entrant entrant = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.Entrant();
        application.setEntrant(entrant);
        entrant.setUID(entrant_uid);
        entrant.setFirstName("Иван");
        entrant.setLastName("Иванов");
        entrant.setGenderID(1L);

        XMLGregorianCalendar xmlDate = null;
        try {
            DatatypeFactory dataTypeFactory = DatatypeFactory.newInstance();
            Date date = new Date();
            final GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            xmlDate = dataTypeFactory.newXMLGregorianCalendar(gc);

        } catch (Exception e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        application.setRegistrationDate(xmlDate);
        application.setNeedHostel(0L);
        application.setStatusID(4L);
        application.setCompetitiveGroupID(658658L);

        application.setFinSourceAndEduForms(new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.FinSourceAndEduForms());
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.FinSourceAndEduForms.FinSourceEduForm finSourceEduForm = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.FinSourceAndEduForms.FinSourceEduForm();
        application.getFinSourceAndEduForms().getFinSourceEduForm().add(finSourceEduForm);
        finSourceEduForm.setEducationFormID(11L);
        finSourceEduForm.setFinanceSourceID(14L);

        application.setLastEducation(new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.LastEducation());
        application.getLastEducation().setKindLastEducationID(1L);
        application.getLastEducation().setOriginalDocumentsReceived(false);

        application.setApplicationDocuments(new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.ApplicationDocuments());
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.ApplicationDocuments.IdentityDocument identityDocument = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.Applications.Application.ApplicationDocuments.IdentityDocument();
        application.getApplicationDocuments().setIdentityDocument(identityDocument);

        identityDocument.setDocumentSeries(0);
        identityDocument.setDocumentNumber(0);
        identityDocument.setDocumentDate(xmlDate);
        identityDocument.setIdentityDocumentTypeID(1L);
        identityDocument.setNationalityTypeID(3L);
        identityDocument.setBirthDate(xmlDate);
        identityDocument.setUID("id_"+entrant_uid);
    }

    private static void importAdmissionInfoTest() throws IOException, JAXBException
    {
        HttpURLConnection conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/import");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

        final ru.tandemservice.uniec.fis.gen.imp.Root root = new ru.tandemservice.uniec.fis.gen.imp.Root();
        root.setAuthData(new ru.tandemservice.uniec.fis.gen.imp.Root.AuthData());
        root.getAuthData().setLogin(LOGIN);
        root.getAuthData().setPass(PASS);

        root.setPackageData(new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData());
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData packageData = root.getPackageData();
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo admissionInfo = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo();
        packageData.setAdmissionInfo(admissionInfo);
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.AdmissionVolume admissionVolume = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.AdmissionVolume();
        admissionInfo.setAdmissionVolume(admissionVolume);
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups competitiveGroups = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups();
        admissionInfo.setCompetitiveGroups(competitiveGroups);

        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.AdmissionVolume.Item admVolumeItem = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.AdmissionVolume.Item();
        admissionVolume.getItem().add(admVolumeItem);

        admVolumeItem.setDirectionID(2946L);
        admVolumeItem.setEducationLevelID(2L);
        admVolumeItem.setNumberBudgetC(1L);
        admVolumeItem.setNumberBudgetO(1L);
        admVolumeItem.setNumberBudgetOZ(1L);
        admVolumeItem.setNumberBudgetZ(1L);
        admVolumeItem.setNumberPaidO(1L);
        admVolumeItem.setNumberPaidOZ(1L);
        admVolumeItem.setNumberPaidZ(1L);
        admVolumeItem.setUID("test1");

        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup compGroup = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup();
        competitiveGroups.getCompetitiveGroup().add(compGroup);

        compGroup.setUID("658658");
        compGroup.setEducationLevelID(2L);
        compGroup.setCourse(1);
        compGroup.setName("тест");

        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.Items cgItems = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.Items();
        compGroup.setItems(cgItems);
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.Items.CompetitiveGroupItem cgItem = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.Items.CompetitiveGroupItem();
        cgItems.getCompetitiveGroupItem().add(cgItem);
        cgItem.setUID("test3");
        cgItem.setDirectionID(2946L);
        cgItem.setNumberBudgetO(1L);
        cgItem.setNumberBudgetZ(1L);
        cgItem.setNumberBudgetOZ(1L);
        cgItem.setNumberBudgetZ(1L);
        cgItem.setNumberPaidO(1L);
        cgItem.setNumberPaidOZ(1L);
        cgItem.setNumberPaidZ(1L);

        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.EntranceTestItems etItems = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.EntranceTestItems();
        compGroup.setEntranceTestItems(etItems);

        etItems.getEntranceTestItem().add(createEntranceTestItem("test4", 1, "экзамен", 1L));
        etItems.getEntranceTestItem().add(createEntranceTestItem("test5", 1, "тест", 2L));
        //etItems.getEntranceTestItem().add(createEntranceTestItem("test6", 3, "тест", 3L));

        write("/opt/uni/trunk/tandem.uni/uniec/src/main/java/ru/tandemservice/uniec/fis/tmp.xml", root);

        write(wr, root);
        wr.flush();

        StringBuilder result = new StringBuilder();
        if (true) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
                System.out.println(line);
            }
            rd.close();
        }

//        try {
//            ImportPackageInfo read = (ImportPackageInfo) read(new ByteArrayInputStream(result.toString().getBytes()), ImportPackageInfo.class);
//            importResult(read.getPackageID().toString());
//        }
//        catch (Throwable e) {
//            System.out.print(".");
//        }

        wr.close();
        conn.disconnect();
    }

    private static ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.EntranceTestItems.EntranceTestItem createEntranceTestItem(String test4, int value, String ex, long value1)
    {
        ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.EntranceTestItems.EntranceTestItem etItem = new ru.tandemservice.uniec.fis.gen.imp.Root.PackageData.AdmissionInfo.CompetitiveGroups.CompetitiveGroup.EntranceTestItems.EntranceTestItem();
        etItem.setUID(test4);
        etItem.setEntranceTestTypeID(value);
        etItem.setForm(ex);
        TEntranceTestSubject entranceTestSubject = new TEntranceTestSubject();
        etItem.setEntranceTestSubject(entranceTestSubject);
        entranceTestSubject.setSubjectID(value1);
        return etItem;
    }


    private static void printDicContent() throws IOException, JAXBException
    {
        HttpURLConnection conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/dictionary");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");

        final Root root = new Root();
        root.setAuthData(new Root.AuthData());
        root.getAuthData().setLogin(LOGIN);
        root.getAuthData().setPass(PASS);

        write(wr, root);
        wr.flush();

        Dictionaries read = (Dictionaries) read(conn.getInputStream(), Dictionaries.class);

        wr.close();
        conn.disconnect();

        for (Dictionaries.Dictionary dicKey : read.getDictionary()) {
            conn = getConnection("http://priem.edu.ru:8000/import/importservice.svc/dictionarydetails");

            wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            System.out.println("--------------------------");
            System.out.println("h1. Справочник: " + dicKey.getCode() + " " + dicKey.getName());

            final ru.tandemservice.uniec.fis.gen.dic_det.Root dicRoot = new ru.tandemservice.uniec.fis.gen.dic_det.Root();
            dicRoot.setGetDictionaryContent(new ru.tandemservice.uniec.fis.gen.dic_det.Root.GetDictionaryContent());
            dicRoot.getGetDictionaryContent().setDictionaryCode(dicKey.getCode());
            dicRoot.setAuthData(new ru.tandemservice.uniec.fis.gen.dic_det.Root.AuthData());
            dicRoot.getAuthData().setLogin(LOGIN);
            dicRoot.getAuthData().setPass(PASS);

            write(wr, dicRoot);
            wr.flush();

            if (false) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    System.out.println(line);
                }
                rd.close();
                wr.close();
                conn.disconnect();

                continue;
            }

            DictionaryData dictionaryData = (DictionaryData) read(conn.getInputStream(), DictionaryData.class);
            for (DictionaryData.DictionaryItems.DictionaryItem item : dictionaryData.getDictionaryItems().getDictionaryItem()) {
                System.out.println(item.getID() + " " + item.getName());
            }

            wr.close();
            conn.disconnect();
        }

        wr.close();
    }

    private static void write(String filename, Object root) throws JAXBException
    {
        final JAXBContext jc = JAXBContext.newInstance(root.getClass());
        final Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        m.marshal(root, new File(filename));
    }


    private static void write(OutputStreamWriter wr, Object root) throws JAXBException
    {
        final JAXBContext jc = JAXBContext.newInstance(root.getClass());
        final Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        m.marshal(root, wr);
    }

    private static Object read(InputStream is, Class clazz) throws JAXBException
    {
        JAXBContext jc = JAXBContext.newInstance(clazz);
        Unmarshaller u = jc.createUnmarshaller();
        return u.unmarshal(is);
    }

    private static HttpURLConnection getConnection(String url_string) throws IOException
    {
        URL url = new URL(url_string);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "text/xml");
        return conn;
    }
}
