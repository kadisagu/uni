/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.entity.examset;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.ISecured;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Набор экзаменов
 *
 * @author vip_delete
 * @since 03.02.2009
 */
public class ExamSet extends IdentifiableWrapper implements ISecured
{
    private static final long serialVersionUID = -4599029213389966037L;
    public static final String P_TITLE = "title"; // название набора
    public static final String P_COUNT = "count"; // кол-во направлений приема в наборе

    private String _examSetId;                      // идентификатор набора экзаменов
    private EnrollmentCampaign _enrollmentCampaign; // приемная кампания
    private StudentCategory _studentCategory;       // категория поступающего
    private List<ExamSetItem> _setItemList;         // сортированный список элементов набора
    private List<EnrollmentDirection> _list;        // список направлений приема в наборе

    public ExamSet(Long id, EnrollmentCampaign enrollmentCampaign, StudentCategory studentCategory, List<ExamSetItem> setItemList)
    {
        super(id, prepareExamSetTitle(setItemList, enrollmentCampaign.isEnrollmentPerCompTypeDiff()));
        if (!checkItemsProfileUniqueness())
        {
            throw new IllegalArgumentException("В наборе может быть только одно профильное испытание");
        }
        
        _enrollmentCampaign = enrollmentCampaign;
        _studentCategory = studentCategory;
        _setItemList = setItemList;
        _list = new ArrayList<>();
        _examSetId = prepareExamSetId();
    }

    private static String prepareExamSetTitle(List<ExamSetItem> setItemList, boolean differenceExist)
    {
        if (setItemList.isEmpty())
            return "Пустой набор";

        StringBuilder result = new StringBuilder();
        for (Iterator<ExamSetItem> iter = setItemList.iterator(); iter.hasNext();)
        {
            ExamSetItem item = iter.next();

            result.append(StringUtils.capitalize(item.getSubject().getTitle()));

            if (!item.getChoice().isEmpty())
            {
                List<String> titleList = new ArrayList<>();
                for (SetDiscipline discipline : item.getChoice())
                    titleList.add(discipline.getTitle());
                result.append(" (").append(StringUtils.join(titleList.iterator(), ", ")).append(')');
            }

            if (item.isProfile() || (differenceExist && item.isContract()))
            {
                result.append(" [");
                if (item.isProfile())
                    result.append("П");
                if (differenceExist && item.isContract())
                    result.append("К");
                result.append("]");
            }
            if (iter.hasNext())
                result.append('\n');
        }
        return result.toString();
    }

    public List<Discipline2RealizationWayRelation> getProfileDisciplineList()
    {
        List<Discipline2RealizationWayRelation> result = new ArrayList<>();
        for (ExamSetItem item : getSetItemList())
            if (item.isProfile())
                result.addAll(item.getSubject().getDisciplines());
        return result;
    }

    private String prepareExamSetId()
    {
        List<String> ids = new ArrayList<>();
        for (ExamSetItem item : _setItemList)
            ids.add(item.getExamSetItemId());
        Collections.sort(ids);
        return _enrollmentCampaign.getId() + "$" + _studentCategory.getId() + "$" + StringUtils.join(ids.iterator(), '-');
    }

    public int getCount()
    {
        return _list.size();
    }
    
    public ExamSetItem getProfileExamSetItem()
    {
        for (ExamSetItem examSetItem : getSetItemList())
        {
            if (examSetItem.isProfile())
            {
                return examSetItem;
            }
        }
        return null;
    }
    
    private boolean checkItemsProfileUniqueness()
    {
        return (CollectionUtils.countMatches(getSetItemList(), object -> ((ExamSetItem) object).isProfile()) < 1);
    }

    // Getters

    public String getExamSetId()
    {
        return _examSetId;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public List<ExamSetItem> getSetItemList()
    {
        return _setItemList;
    }

    public List<EnrollmentDirection> getList()
    {
        return _list;
    }
}
