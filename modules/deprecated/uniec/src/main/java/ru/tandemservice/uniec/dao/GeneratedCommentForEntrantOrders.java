/* $Id$ */
package ru.tandemservice.uniec.dao;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniec.entity.orders.*;

import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 14.11.2013
 */
public class GeneratedCommentForEntrantOrders implements IGeneratedCommentForEntrantOrders
{
    @Override
    public String getComment(EntrStudentBugetExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrStudentContractExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrMasterBugetExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrMasterContractExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrListenerContractExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrTargetBudgetExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrTargetContractExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrStudentShBugetExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrStudentShContractExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrHighShortExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrListenerParExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Зачислен" : "Зачислена") + " на " + extract.getCourse().getTitle() + " курс " + group(extract) + "(параллельное освоение: «" + extract.getParallel().getEducationLevel().getProgramSubjectTitleWithCode() + "») " + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrStudentOPPExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrStudentExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    @Override
    public String getComment(EntrTargetExtract extract, boolean isPrintOrder)
    {
        return getDefaultComment(extract, isPrintOrder);
    }

    public String getDefaultComment(EnrollmentExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Зачислен" : "Зачислена") + " на " + extract.getCourse().getTitle() + " курс " + group(extract) + order(extract, isPrintOrder);
    }

    // Базовый метод для вывода даты и номера приказа
    public String getDefaultOrder(AbstractEntrantExtract extract, boolean isPrintOrder, boolean isComma)
    {
        if (isPrintOrder)
        {
            String comma = "";
            if (isComma) comma = ", ";

            if (null != extract.getParagraph() || null != extract.getParagraph().getOrder())
            {
                AbstractEntrantOrder order = extract.getParagraph().getOrder();
                if (null != order.getNumber())
                    return comma + "приказ № " + order.getNumber() + (order.getCommitDate() != null ? " от " + date(order.getCommitDate()) + " года." : "");
                else if (order.getCommitDate() != null)
                    return comma + "приказ от " + date(order.getCommitDate()) + " года.";
            }
        }
        return "";
    }

    // Вывод даты и номера приказа с запятой
    public String order(AbstractEntrantExtract extract, boolean isPrintOrder)
    {
        return getDefaultOrder(extract, isPrintOrder, true);
    }

    public String date(Date date)
    {
        return null != date ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date) : "";
    }

    public String group(EnrollmentExtract extract)
    {
        return extract.getGroupTitle() == null ? "" : "в группу " + extract.getGroupTitle();
    }

    public boolean isMale(EnrollmentExtract extract)
    {
        return extract.getStudentNew().getPerson().getIdentityCard().getSex().isMale();
    }
}
