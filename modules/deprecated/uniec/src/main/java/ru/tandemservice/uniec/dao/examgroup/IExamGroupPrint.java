/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * @author Vasily Zhukov
 * @since 31.05.2010
 */
public interface IExamGroupPrint
{
    // имя класса для печати экзаменационной группы
    String EXAM_GROUP_PRINT_FORM = "ExamGroupPrintForm";

    // имя класса для печати ведомости на экзаменационную группу
    String EXAM_GROUP_PRINT_SHEET = "ExamGroupPrintSheetForm";

    // имя класса для печати оценкой в экзаменационной группе
    String EXAM_GROUP_PRINT_MARKS = "ExamGroupPrintMarksForm";

    /**
     * печатные алгоритмы не используют механизм дао, так как это не синглтоны (есть локальные переменные)
     * поэтому им нужна сессия
     *
     * @param session сессия
     */
    void init(Session session);

    /**
     * печать
     *
     * @param ids идентификаторы экзаменационных групп для печати
     * @return результирующий документ
     */
    RtfDocument createPrintForm(List<Long> ids);
}
