package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Возможный вид конкурса по квалификации для студента и слушателя на год
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Qualification2CompetitionKindRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation";
    public static final String ENTITY_NAME = "qualification2CompetitionKindRelation";
    public static final int VERSION_HASH = -665634443;
    private static IEntityMeta ENTITY_META;

    public static final String L_QUALIFICATION = "qualification";
    public static final String L_COMPETITION_KIND = "competitionKind";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_STUDENT_CATEGORY = "studentCategory";

    private Qualifications _qualification;     // Квалификация
    private CompetitionKind _competitionKind;     // Вид конкурса
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private StudentCategory _studentCategory;     // Категория обучаемого

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     */
    @NotNull
    public Qualifications getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация. Свойство не может быть null.
     */
    public void setQualification(Qualifications qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     */
    @NotNull
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    /**
     * @param competitionKind Вид конкурса. Свойство не может быть null.
     */
    public void setCompetitionKind(CompetitionKind competitionKind)
    {
        dirty(_competitionKind, competitionKind);
        _competitionKind = competitionKind;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория обучаемого. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Qualification2CompetitionKindRelationGen)
        {
            setQualification(((Qualification2CompetitionKindRelation)another).getQualification());
            setCompetitionKind(((Qualification2CompetitionKindRelation)another).getCompetitionKind());
            setEnrollmentCampaign(((Qualification2CompetitionKindRelation)another).getEnrollmentCampaign());
            setStudentCategory(((Qualification2CompetitionKindRelation)another).getStudentCategory());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Qualification2CompetitionKindRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Qualification2CompetitionKindRelation.class;
        }

        public T newInstance()
        {
            return (T) new Qualification2CompetitionKindRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "qualification":
                    return obj.getQualification();
                case "competitionKind":
                    return obj.getCompetitionKind();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "studentCategory":
                    return obj.getStudentCategory();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "qualification":
                    obj.setQualification((Qualifications) value);
                    return;
                case "competitionKind":
                    obj.setCompetitionKind((CompetitionKind) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "qualification":
                        return true;
                case "competitionKind":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "studentCategory":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "qualification":
                    return true;
                case "competitionKind":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "studentCategory":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "qualification":
                    return Qualifications.class;
                case "competitionKind":
                    return CompetitionKind.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "studentCategory":
                    return StudentCategory.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Qualification2CompetitionKindRelation> _dslPath = new Path<Qualification2CompetitionKindRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Qualification2CompetitionKindRelation");
    }
            

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getQualification()
     */
    public static Qualifications.Path<Qualifications> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getCompetitionKind()
     */
    public static CompetitionKind.Path<CompetitionKind> competitionKind()
    {
        return _dslPath.competitionKind();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    public static class Path<E extends Qualification2CompetitionKindRelation> extends EntityPath<E>
    {
        private Qualifications.Path<Qualifications> _qualification;
        private CompetitionKind.Path<CompetitionKind> _competitionKind;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private StudentCategory.Path<StudentCategory> _studentCategory;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getQualification()
     */
        public Qualifications.Path<Qualifications> qualification()
        {
            if(_qualification == null )
                _qualification = new Qualifications.Path<Qualifications>(L_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Вид конкурса. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getCompetitionKind()
     */
        public CompetitionKind.Path<CompetitionKind> competitionKind()
        {
            if(_competitionKind == null )
                _competitionKind = new CompetitionKind.Path<CompetitionKind>(L_COMPETITION_KIND, this);
            return _competitionKind;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Категория обучаемого. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.Qualification2CompetitionKindRelation#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

        public Class getEntityClass()
        {
            return Qualification2CompetitionKindRelation.class;
        }

        public String getEntityName()
        {
            return "qualification2CompetitionKindRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
