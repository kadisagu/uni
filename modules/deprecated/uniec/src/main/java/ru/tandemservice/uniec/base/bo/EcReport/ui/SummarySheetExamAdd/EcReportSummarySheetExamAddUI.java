/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.SummarySheetExamAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.SummarySheetExamReport.SummarySheetExamReportModel;
import ru.tandemservice.uniec.base.bo.EcReport.ui.SummarySheetExamPub.EcReportSummarySheetExamPub;
import ru.tandemservice.uniec.entity.report.SummarySheetExamReport;

/**
 * @author Alexander Shaburov
 * @since 18.07.12
 */
public class EcReportSummarySheetExamAddUI extends UIPresenter
{
    // fields

    private Object _model;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _model = new SummarySheetExamReportModel();

        EcReportManager.instance().summarySheetExamReportDAO().prepareModel((SummarySheetExamReportModel) _model);
    }

    public void onChangeEnrollmentCampaign()
    {
        EcReportManager.instance().summarySheetExamReportDAO().onChangeEnrollmentCampaign((SummarySheetExamReportModel) _model);
    }

    // Listeners

    public void onClickApply()
    {
        ErrorCollector errorCollector = EcReportManager.instance().summarySheetExamReportDAO().validate((SummarySheetExamReportModel) _model);

        if (errorCollector.hasErrors())
            return;

        DatabaseFile reportFile = EcReportManager.instance().summarySheetExamReportDAO().createPrintReportFile((SummarySheetExamReportModel) _model);


        SummarySheetExamReport report = EcReportManager.instance().summarySheetExamReportDAO().createReport((SummarySheetExamReportModel) _model, reportFile);

        DataAccessServices.dao().save(reportFile);
        DataAccessServices.dao().save(report);

        deactivate();

        getActivationBuilder().asDesktopRoot(EcReportSummarySheetExamPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    // Getters & Setters

    public Object getModel()
    {
        return _model;
    }

    public void setModel(Object model)
    {
        _model = model;
    }
}
