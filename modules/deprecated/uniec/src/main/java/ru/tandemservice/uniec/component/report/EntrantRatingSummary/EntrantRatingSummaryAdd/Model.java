/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd;

import java.util.Collections;
import java.util.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantRatingSummaryReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

/**
 * @author oleyba
 * @since 21.07.2009
 */
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    private EntrantRatingSummaryReport _report = new EntrantRatingSummaryReport();
    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private IPrincipalContext _principalContext;

    private boolean _qualificationActive;
    private boolean _studentCategoryActive;
    private boolean _formativeOrgUnitActive;
    private boolean _territorialOrgUnitActive;
    private boolean _developConditionActive;

    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<IdentifiableWrapper> _enrollmentCampaignStageList;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _qualificationListModel;
    private ISelectModel _studentCategoryListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;

    private IdentifiableWrapper _enrollmentCampaignStage;
    private List<Qualifications> _qualificationList;
    private List<StudentCategory> _studentCategoryList;
    private List<OrgUnit> _formativeOrgUnitList;
    private List<OrgUnit> _territorialOrgUnitList;
    private List<DevelopCondition> _developConditionList;

    private ISelectModel _customStateRulesModel;
    private DataWrapper _customStateRule;
    private ISelectModel _entrantCustomStateListModel;
    private List<EntrantCustomStateCI> _entrantCustomStateList;


    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    //  MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return Collections.singletonList(getReport().getDevelopForm());
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return null;
    }

    // Getters & Setters

    public EntrantRatingSummaryReport getReport()
    {
        return _report;
    }

    public void setReport(EntrantRatingSummaryReport report)
    {
        _report = report;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<IdentifiableWrapper> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<IdentifiableWrapper> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public IdentifiableWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(IdentifiableWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public DataWrapper getCustomStateRule()
    {
        return _customStateRule;
    }

    public Boolean getCustomStateRuleValue()
    {
        return TwinComboDataSourceHandler.getSelectedValue(_customStateRule);
    }

    public void setCustomStateRule(DataWrapper customStateRule)
    {
        _customStateRule = customStateRule;
    }

    public ISelectModel getCustomStateRulesModel()
    {
        return _customStateRulesModel;
    }

    public void setCustomStateRulesModel(ISelectModel customStateRulesModel)
    {
        _customStateRulesModel = customStateRulesModel;
    }

    public List<EntrantCustomStateCI> getEntrantCustomStateList()
    {
        return _entrantCustomStateList;
    }

    public void setEntrantCustomStateList(List<EntrantCustomStateCI> entrantCustomStateList)
    {
        _entrantCustomStateList = entrantCustomStateList;
    }

    public ISelectModel getEntrantCustomStateListModel()
    {
        return _entrantCustomStateListModel;
    }

    public void setEntrantCustomStateListModel(ISelectModel entrantCustomStateListModel)
    {
        _entrantCustomStateListModel = entrantCustomStateListModel;
    }

    public boolean getEntrantCustomStateListDisabled()
    {
        return _customStateRule == null;
    }
}
