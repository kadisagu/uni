/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IOrgUnitPermissionCustomizer;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.IOrgstructDAO;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel;
import ru.tandemservice.uniec.entity.settings.*;

import java.util.*;

/**
 * @author agolubenko
 * @since 20.06.2008
 */
public class SettingsDAO extends HibernateDaoSupport implements ISettingsDAO, IOrgUnitPermissionCustomizer
{
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Discipline2RealizationWayRelation> getDisciplines(DisciplinesGroup group)
    {
        Criteria criteria = getSession().createCriteria(Group2DisciplineRelation.class);
        criteria.add(Restrictions.eq(Group2DisciplineRelation.L_GROUP, group));
        addDisciplinesOrder(criteria);
        criteria.setProjection(Projections.property(Group2DisciplineRelation.L_DISCIPLINE));
        return criteria.list();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map<DisciplinesGroup, List<Discipline2RealizationWayRelation>> getGroups2Disciplines(Collection<DisciplinesGroup> groups)
    {
        if (groups.isEmpty())
        {
            return Collections.emptyMap();
        }

        Map<DisciplinesGroup, List<Discipline2RealizationWayRelation>> result = new HashMap<DisciplinesGroup, List<Discipline2RealizationWayRelation>>();

        Criteria criteria = getSession().createCriteria(Group2DisciplineRelation.class);
        criteria.add(Restrictions.in(Group2DisciplineRelation.L_GROUP, groups));
        addDisciplinesOrder(criteria);

        for (Group2DisciplineRelation relation : (List<Group2DisciplineRelation>) criteria.list())
        {
            DisciplinesGroup group = relation.getGroup();
            List<Discipline2RealizationWayRelation> disciplines = result.get(group);
            if (disciplines == null)
            {
                disciplines = new ArrayList<Discipline2RealizationWayRelation>();
                result.put(group, disciplines);
            }
            disciplines.add(relation.getDiscipline());
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Discipline2RealizationWayRelation> getProfileDisciplines(EnrollmentDirection enrollmentDirection)
    {
        MQBuilder builder = new MQBuilder(EntranceDiscipline.ENTITY_CLASS, "d", new String[]{EntranceDiscipline.L_DISCIPLINE});
        builder.add(MQExpression.eq("d", EntranceDiscipline.L_ENROLLMENT_DIRECTION, enrollmentDirection));
        builder.add(MQExpression.eq("d", EntranceDiscipline.L_KIND + "." + EntranceDisciplineKind.P_CODE, UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE));

        Set<Discipline2RealizationWayRelation> set = new HashSet<Discipline2RealizationWayRelation>();
        for (SetDiscipline discipline : builder.<SetDiscipline>getResultList(getSession()))
            set.addAll(discipline.getDisciplines());
        return set;
    }

    private void addDisciplinesOrder(Criteria criteria)
    {
        criteria.createAlias(Group2DisciplineRelation.L_DISCIPLINE, "discipline");
        criteria.createAlias("discipline." + Discipline2RealizationWayRelation.L_EDUCATION_SUBJECT, "subject");
        criteria.addOrder(Order.asc("subject." + EducationSubject.P_TITLE));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<CompetitionKind, Integer> getCompetitionKindPriorities(EnrollmentCampaign enrollmentCampaign)
    {
        List<EnrollmentCompetitionKind> list = new DQLSelectBuilder().fromEntity(EnrollmentCompetitionKind.class, "e").column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentCompetitionKind.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(enrollmentCampaign)))
                .fetchPath(DQLJoinType.left, EnrollmentCompetitionKind.competitionKind().fromAlias("e"), "c")
                .createStatement(getSession()).list();

        Map<CompetitionKind, Integer> result = new HashMap<CompetitionKind, Integer>();
        for (EnrollmentCompetitionKind enrollmentCompetitionKind : list)
            result.put(enrollmentCompetitionKind.getCompetitionKind(), enrollmentCompetitionKind.getPriority());
        return result;
    }

    @Override
    public List<CompetitionKind> getCompetitionKindPrioritizedList(EnrollmentCampaign enrollmentCampaign)
    {
        MQBuilder builder = new MQBuilder(EnrollmentCompetitionKind.ENTITY_CLASS, "rel", new String[]{EnrollmentCompetitionKind.competitionKind().s()});
        builder.add(MQExpression.eq("rel", EnrollmentCompetitionKind.enrollmentCampaign().s(), enrollmentCampaign));
        builder.add(MQExpression.isNotNull("rel", EnrollmentCompetitionKind.competitionKind().s()));
        builder.addOrder("rel", EnrollmentCompetitionKind.priority().s());
        return builder.getResultList(getSession());
    }

    @Override
    public boolean isEntrantListTabVisible(OrgUnit orgUnit)
    {
        return IOrgstructDAO.instance.get().isOrgUnitFormingOrTerritorial(orgUnit);
    }

    @Override
    public void customize(OrgUnitType orgUnitType, Map<String, PermissionGroupMeta> permissionGroupMap)
    {
        boolean hidden = IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
        permissionGroupMap.get(orgUnitType.getCode() + "EntrantsTabPermissionGroup").setHidden(hidden);
    }

    @Override
    public List<EnrollmentOrderBasic> getReasonToBasicsList(EnrollmentOrderReason enrollmentOrderReason)
    {
        MQBuilder builder = new MQBuilder(EnrollmentOrderReasonToBasicsRel.ENTITY_CLASS, "rel", new String[]{EnrollmentOrderReasonToBasicsRel.L_BASIC});
        builder.add(MQExpression.eq("rel", EnrollmentOrderReasonToBasicsRel.L_REASON, enrollmentOrderReason));
        builder.addOrder("rel", EnrollmentOrderReasonToBasicsRel.L_BASIC + "." + EnrollmentOrderBasic.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public EnrollmentOrderBasic getBasic(EnrollmentOrder order)
    {
        MQBuilder builder = new MQBuilder(EnrollmentOrderToBasicRel.ENTITY_CLASS, "rel", new String[]{EnrollmentOrderToBasicRel.L_BASIC});
        builder.add(MQExpression.eq("rel", EnrollmentOrderToBasicRel.L_ORDER, order));
        return (EnrollmentOrderBasic) builder.uniqueResult(getSession());
    }

}
