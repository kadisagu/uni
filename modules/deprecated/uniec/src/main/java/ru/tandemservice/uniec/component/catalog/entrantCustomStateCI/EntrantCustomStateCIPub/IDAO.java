/* $Id$ */
package ru.tandemservice.uniec.component.catalog.entrantCustomStateCI.EntrantCustomStateCIPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
public interface IDAO extends IDefaultCatalogPubDAO<EntrantCustomStateCI, Model>
{
}
