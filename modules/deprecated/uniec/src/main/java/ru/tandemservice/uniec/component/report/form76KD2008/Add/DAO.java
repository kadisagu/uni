/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.form76KD2008.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uniec.entity.report.Form76KD2008Report;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.Date;

/**
 * @author agolubenko
 * @since 27.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        final Session session = getSession();
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, session);
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(EducationCatalogsManager.getDevelopConditionSelectModel());
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setParallelList(CommonYesNoUIObject.createYesNoList());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        Form76KD2008Report report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isDevelopConditionActive())
            report.setDevelopCondition(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getDevelopConditionList(), DevelopForm.P_TITLE), "; "));
        if (model.isQualificationActive())
            report.setQualification(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getQualificationList(), Qualifications.P_TITLE), "; "));
        if (model.isStudentCategoryActive())
            report.setStudentCategory(StringUtils.join(CommonBaseUtil.getPropertiesList(model.getStudentCategoryList(), StudentCategory.P_TITLE), "; "));
        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());

        DatabaseFile databaseFile = new Form76KDReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}
