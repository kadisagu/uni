package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.SourceInfoAboutUniversity;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранный источник информации об ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantInfoAboutUniversityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity";
    public static final String ENTITY_NAME = "entrantInfoAboutUniversity";
    public static final int VERSION_HASH = 1758321742;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_SOURCE_INFO = "sourceInfo";

    private Entrant _entrant;     // (Старый) Абитуриент
    private SourceInfoAboutUniversity _sourceInfo;     // Источники информации об ОУ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Источники информации об ОУ. Свойство не может быть null.
     */
    @NotNull
    public SourceInfoAboutUniversity getSourceInfo()
    {
        return _sourceInfo;
    }

    /**
     * @param sourceInfo Источники информации об ОУ. Свойство не может быть null.
     */
    public void setSourceInfo(SourceInfoAboutUniversity sourceInfo)
    {
        dirty(_sourceInfo, sourceInfo);
        _sourceInfo = sourceInfo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantInfoAboutUniversityGen)
        {
            setEntrant(((EntrantInfoAboutUniversity)another).getEntrant());
            setSourceInfo(((EntrantInfoAboutUniversity)another).getSourceInfo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantInfoAboutUniversityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantInfoAboutUniversity.class;
        }

        public T newInstance()
        {
            return (T) new EntrantInfoAboutUniversity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "sourceInfo":
                    return obj.getSourceInfo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "sourceInfo":
                    obj.setSourceInfo((SourceInfoAboutUniversity) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "sourceInfo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "sourceInfo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "sourceInfo":
                    return SourceInfoAboutUniversity.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantInfoAboutUniversity> _dslPath = new Path<EntrantInfoAboutUniversity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantInfoAboutUniversity");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Источники информации об ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity#getSourceInfo()
     */
    public static SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity> sourceInfo()
    {
        return _dslPath.sourceInfo();
    }

    public static class Path<E extends EntrantInfoAboutUniversity> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity> _sourceInfo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Источники информации об ОУ. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity#getSourceInfo()
     */
        public SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity> sourceInfo()
        {
            if(_sourceInfo == null )
                _sourceInfo = new SourceInfoAboutUniversity.Path<SourceInfoAboutUniversity>(L_SOURCE_INFO, this);
            return _sourceInfo;
        }

        public Class getEntityClass()
        {
            return EntrantInfoAboutUniversity.class;
        }

        public String getEntityName()
        {
            return "entrantInfoAboutUniversity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
