/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.settings.ProtocolVisaList.IProtocolEntity;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.report.OrgUnitCommissionChairman;
import ru.tandemservice.uniec.entity.report.OrgUnitExecutiveSecretary;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
class EntranceExamMeetingForm3RtfBuilder implements EntranceExamMeetingRtfBuilder
{
    private static Map<String, String> _developFormByCode = new HashMap<>();

    static
    {
        _developFormByCode.put(DevelopFormCodes.FULL_TIME_FORM, "очную форму обучения");
        _developFormByCode.put(DevelopFormCodes.CORESP_FORM, "заочную форму обучения");
        _developFormByCode.put(DevelopFormCodes.PART_TIME_FORM, "очно-заочную форму обучения");
        _developFormByCode.put(DevelopFormCodes.EXTERNAL_FORM, "экстернат");
        _developFormByCode.put(DevelopFormCodes.APPLICANT_FORM, "самостоятельное обучение и итоговую аттестацию");
    }

    private Session _session;
    private Model _model;
    private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> _examSetDataMap;
    private RtfDocument _template;
    private EntrantDataUtil _dataUtil;
    private Map<Integer, CompetitionKind> _priority2CompetitionKind;

    @Override
    public RtfDocument getTemplate()
    {
        return _template;
    }

    @Override
    public void init(Session session, Model model, Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examSetDataMap, EntrantDataUtil dataUtil, MQBuilder directionBuilder, Map<Integer, CompetitionKind> priority2CompetitionKind)
    {
        _session = session;
        _model = model;
        _examSetDataMap = examSetDataMap;

        // загружаем шаблон
        MQBuilder builder = new MQBuilder(UniecScriptItem.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", UniecScriptItem.P_CODE, UniecDefines.TEMPLATE_ENTRANCE_EXAM_MEETING_BY_CK3));
        IScriptItem templateDocument = (UniecScriptItem) builder.uniqueResult(session);
        _template = new RtfReader().read(templateDocument.getCurrentTemplate());
        _dataUtil = dataUtil;
        _priority2CompetitionKind = priority2CompetitionKind;
    }

    @Override
    public List<IRtfElement> buildReportPart(EnrollmentDirectionGroup group, Map<CompetitionKind, List<ReportRow>> competitionKind2RowList, final boolean structuresEqualInWholeGroup,
                                             final List<String> typeTitles, final List<String[]> titles,final boolean useIndProgress)
    {
        // кол-во предметов
        final int size = typeTitles.size();

        // data for tables
        Map<CompetitionKind, List<String[]>> competitionKind2Data = SafeMap.get(ArrayList.class);
        int num = 1;
        for (Map.Entry<Integer, CompetitionKind> entry : _priority2CompetitionKind.entrySet())
        {
            CompetitionKind entryCompetitionKind = entry.getValue(); // может быть null для целевого приема
            List<String[]> data = new ArrayList<>();
            competitionKind2Data.put(entryCompetitionKind, data);
            List<ReportRow> rowList = competitionKind2RowList.get(entryCompetitionKind);
            if (rowList == null) continue;

            for (ReportRow row : rowList)
            {
                RequestedEnrollmentDirection requestedEnrollmentDirection = row.getRequestedEnrollmentDirection();
                EnrollmentDirection direction = requestedEnrollmentDirection.getEnrollmentDirection();
                Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
                Person person = entrant.getPerson();

                String[] cells = new String[5 + size + (useIndProgress ? 1:0)];

                cells[0] = Integer.toString(num++);
                cells[1] = person.getFullFio();
                cells[2] = requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "оригинал" : "копия";

                int j = 3;
                if (size == 0)
                {
                    j++;
                } else
                {
                    Set<ChosenEntranceDiscipline> chosenSet = _dataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection);
                    ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getChosenDistribution((chosenSet != null) ? chosenSet : Collections.<ChosenEntranceDiscipline>emptySet(), _examSetDataMap.get(direction).getExamSetStructure().getItemList());

                    for (int k = 0; k < size; k++)
                    {
                        ChosenEntranceDiscipline chosen = distribution[k];
                        Double mark = chosen == null ? null : row.getMarkMap().get(chosen.getEnrollmentCampaignDiscipline());

                        cells[j] = mark == null ? "x" : (mark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
                        j++;
                    }
                }
                if (useIndProgress)
                    cells[j++] = String.valueOf(row.getIndividProgressMark());
                cells[j] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getSumMark());

                data.add(cells);
            }
        }

        // direction header modifier
        String commissionChairman = (String) new MQBuilder(OrgUnitCommissionChairman.ENTITY_CLASS, "r", new String[]{OrgUnitCommissionChairman.P_FIO})
                .add(MQExpression.eq("r", IProtocolEntity.L_ORG_UNIT, group.getFormativeOrgUnit()))
                .add(MQExpression.eq("r", IProtocolEntity.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()))
                .add(MQExpression.eq("r", IProtocolEntity.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()))
                .uniqueResult(_session);

        String executiveSecretary = (String) new MQBuilder(OrgUnitExecutiveSecretary.ENTITY_CLASS, "r", new String[]{OrgUnitExecutiveSecretary.P_FIO})
                .add(MQExpression.eq("r", IProtocolEntity.L_ORG_UNIT, group.getFormativeOrgUnit()))
                .add(MQExpression.eq("r", IProtocolEntity.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()))
                .add(MQExpression.eq("r", IProtocolEntity.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()))
                .uniqueResult(_session);

        RtfDocument document = _template.getClone();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("HS", TopOrgUnit.getInstance().getShortTitle());
        injectModifier.put("CT", _model.getReport().getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "за счет средств федерального бюджета" : "с оплатой стоимости обучения");
        injectModifier.put("DF", _developFormByCode.get(group.getDevelopForm().getCode())); //форма освоения
        injectModifier.put("FD", StringUtils.isNotEmpty(group.getFormativeOrgUnit().getNominativeCaseTitle()) ? group.getFormativeOrgUnit().getNominativeCaseTitle() : group.getFormativeOrgUnit().getTitle()); //формирующее подр.
        injectModifier.put("ELT", group.getEducationLevels().getLevelType().getDativeCaseShortTitle()); //по спец, по направл
        injectModifier.put("commissionChairman", StringUtils.isEmpty(commissionChairman) ? "               " : commissionChairman);
        injectModifier.put("executiveSecretary", StringUtils.isEmpty(executiveSecretary) ? "               " : executiveSecretary);

        String educationLevelText = group.getTitle() + " (" + StringUtils.uncapitalize(group.getDevelopCondition().getTitle()) + ", " + group.getDevelopPeriod().getTitle() + ")";
        injectModifier.put("EL", educationLevelText); //направление приема
        injectModifier.put("EL_Alt", group.getTitle()); //направление приема
        String eduProgramKind = "";
        EduProgramSubject eduProgramSubject = group.getEducationLevels().getEduProgramSubject();
        if(eduProgramSubject != null)
        {
            eduProgramKind = DeclinableManager.instance().dao()
                    .getPropertyValue(eduProgramSubject.getEduProgramKind(), EduProgramKind.P_TITLE, IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_DATIVE));
        }
        injectModifier.put("eduProgramKind", eduProgramKind);
        injectModifier.modify(document);

        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "H1");
        RtfRow head = table.getRowList().get(1);

        //Инд.достижения не учитываются - удаляем колонку из шаблона
        if (!useIndProgress)
        {
            UniRtfUtil.deleteCell(table.getRowList().get(0), 4, 3);
            UniRtfUtil.deleteCell(head, 4, 3);
        }

        if (size == 0)
        {
            UniRtfUtil.fillCell(head.getCellList().get(3), "");
        } else
        {
            int[] parts = new int[size];
            Arrays.fill(parts, 1);
            RtfUtil.splitRow(head, 3, (newCell, index) -> {
                if (structuresEqualInWholeGroup)
                {
                    String[] title = titles.get(index);
                    RtfString string = new RtfString();
                    for (int i = 0; i < title.length - 1; i++)
                        string.append(title[i]).append("/").append(IRtfData.LINE);
                    if (title.length > 0)
                        string.append(title[title.length - 1]);
                    newCell.setElementList(string.toList());
                } else
                {
                    newCell.setElementList(new RtfString().append(typeTitles.get(index)).toList());
                }
            }, parts);
        }

        final List<Integer> headerIndexes = new ArrayList<>();
        List<String[]> tableRows = new ArrayList<>();

        for (Map.Entry<Integer, CompetitionKind> entry : _priority2CompetitionKind.entrySet())
        {
            CompetitionKind competitionKind = entry.getValue();
            List<String[]> data = competitionKind2Data.get(competitionKind);

            if (!data.isEmpty())
            {
                headerIndexes.add(tableRows.size());
                tableRows.add(new String[]{ReportRow.getHeader(competitionKind)});
                tableRows.addAll(data);
            }
        }

        RtfTableModifier modifier = new RtfTableModifier();
        modifier.put("T", tableRows.toArray(new String[tableRows.size()][]));

        modifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                //Инд.достижения не учитываются - удаляем колонку
                if (!useIndProgress)
                    UniRtfUtil.deleteCell(table.getRowList().get(currentRowIndex), 4, 3);

                if (size != 0)
                {
                    // разбиваем колонку по количеству вступительных испытаний в наборе
                    int[] parts = new int[size];
                    Arrays.fill(parts, 1);

                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 3, null, parts);
                }
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем ячейки в строках с заголовками и выравниваем по центру
                for (Integer headerIndex : headerIndexes)
                {
                    RtfRow row = newRowList.get(startIndex + headerIndex);
                    RtfUtil.unitAllCell(row, 0);
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QR, IRtfData.QC);
                }
            }
        });
        modifier.modify(document);

        return document.getElementList();
    }
}
