package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Неиспользуемый вид целевого приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NotUsedTargetAdmissionKindGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind";
    public static final String ENTITY_NAME = "notUsedTargetAdmissionKind";
    public static final int VERSION_HASH = 24698373;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    private TargetAdmissionKind _targetAdmissionKind;     // Вид целевого приема
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     */
    @NotNull
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид целевого приема. Свойство не может быть null.
     */
    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NotUsedTargetAdmissionKindGen)
        {
            setTargetAdmissionKind(((NotUsedTargetAdmissionKind)another).getTargetAdmissionKind());
            setEnrollmentCampaign(((NotUsedTargetAdmissionKind)another).getEnrollmentCampaign());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NotUsedTargetAdmissionKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NotUsedTargetAdmissionKind.class;
        }

        public T newInstance()
        {
            return (T) new NotUsedTargetAdmissionKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((TargetAdmissionKind) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "targetAdmissionKind":
                        return true;
                case "enrollmentCampaign":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "targetAdmissionKind":
                    return true;
                case "enrollmentCampaign":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "targetAdmissionKind":
                    return TargetAdmissionKind.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NotUsedTargetAdmissionKind> _dslPath = new Path<NotUsedTargetAdmissionKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NotUsedTargetAdmissionKind");
    }
            

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind#getTargetAdmissionKind()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    public static class Path<E extends NotUsedTargetAdmissionKind> extends EntityPath<E>
    {
        private TargetAdmissionKind.Path<TargetAdmissionKind> _targetAdmissionKind;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид целевого приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind#getTargetAdmissionKind()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

        public Class getEntityClass()
        {
            return NotUsedTargetAdmissionKind.class;
        }

        public String getEntityName()
        {
            return "notUsedTargetAdmissionKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
