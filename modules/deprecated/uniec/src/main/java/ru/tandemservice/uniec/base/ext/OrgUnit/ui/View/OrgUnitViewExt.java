/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        /**
         <component-tab 
         name="entrantList" 
         label="Абитуриенты" 
         component-name="ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList" 
         secured-object="fast:model.orgUnit" 
         permission-key="ognl:'orgUnit_viewEntrantTab_' + model.orgUnit.orgUnitType.code" 
         visible="ognl:@ru.tandemservice.uniec.dao.UniecDAOFacade@getSettingsDAO().isEntrantListTabVisible(model.orgUnit)" 
         before="abstractOrgUnit_OrgUnitEmployeeList"
         />         */
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("entrantList", "ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList").permissionKey("ui:secModel.orgUnit_viewEntrantTab").visible("ognl:@ru.tandemservice.uniec.dao.UniecDAOFacade@getSettingsDAO().isEntrantListTabVisible(presenter.orgUnit)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
