/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.StateExamImport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author ekachanova
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private Map<Long, ITitled> _importTypes = new LinkedHashMap<Long, ITitled>();
    private List<IdentifiableWrapper> _importTypeList = new ArrayList<IdentifiableWrapper>();

    {
        long i = 0;
        List<ITitled> imports = Arrays.asList(
                ImportByCertificateWithNoFio.instance.get(),
                ImportByIdentityCardNewWithNoFio.instance.get(),
                ImportByCertificateWithFio.instance.get(),
                ImportByIdentityCardNewWithFio.instance.get(),
                ImportByCertificateWithIcFio2016.instance.get()
        );
        for (ITitled imp: imports) {
            IdentifiableWrapper w = new IdentifiableWrapper(i++, imp.getTitle());
            _importTypeList.add(w);
            _importTypes.put(w.getId(), imp);
        }
    }

    public ITitled getImport()
    {
        IdentifiableWrapper itp = getImportType();
        if (null == itp) { return null; }

        Long id = itp.getId();
        if (null == id) { return null; }

        return _importTypes.get(id);
    }

    public boolean isImportByIdentityCard() {
        return (getImport() instanceof IImportByIdentityCard);
    }


    private EnrollmentCampaign _enrollmentCampaign;
    private IdentifiableWrapper importType;
    private boolean deleteNotFound;
    private IUploadFile _uploadFile;


    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    // accessors

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<IdentifiableWrapper> getImportTypeList()
    {
        return _importTypeList;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public IdentifiableWrapper getImportType()
    {
        return importType;
    }

    public void setImportType(IdentifiableWrapper importType)
    {
        this.importType = importType;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public boolean isDeleteNotFound()
    {
        return deleteNotFound;
    }

    public void setDeleteNotFound(boolean deleteNotFound)
    {
        this.deleteNotFound = deleteNotFound;
    }


}
