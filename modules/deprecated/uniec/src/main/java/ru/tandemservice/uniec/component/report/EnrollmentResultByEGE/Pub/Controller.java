/* $Id$ */
package ru.tandemservice.uniec.component.report.EnrollmentResultByEGE.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }

    public void onClickPrintReport(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new ParametersMap()
                .add("reportId", getModel(component).getReport().getId())
                .add("extension", "rtf")
        ));
    }
}
