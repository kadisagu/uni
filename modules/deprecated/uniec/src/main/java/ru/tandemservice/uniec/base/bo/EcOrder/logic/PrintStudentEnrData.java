/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.component.student.StudentPersonCard.IPrintStudentEnrData;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

/**
 * @author Nikolay Fedorovskih
 * @since 10.09.2014
 */
public class PrintStudentEnrData implements IPrintStudentEnrData
{
    @Override
    public RtfInjectModifier getEnrPrintDataModifier(Long studentId)
    {
        final EnrollmentExtract extract = DataAccessServices.dao().get(EnrollmentExtract.class, EnrollmentExtract.L_STUDENT_NEW, studentId);
        if (extract == null || extract.getOrder() == null)
            return null;

        final EnrollmentOrder order = extract.getOrder();
        final Double sumMark = IEntrantDAO.instance.get().getSumFinalMarks(extract.getEntity().getRequestedEnrollmentDirection());

        return new RtfInjectModifier()
                .put("orderNumber", order.getNumber())
                .put("orderDate", order.getCommitDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) : "")
                .put("course", extract.getCourse().getTitle())
                .put("ball", DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark));
    }
}