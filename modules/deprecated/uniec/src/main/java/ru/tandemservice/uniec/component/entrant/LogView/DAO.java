/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.LogView;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.shared.fias.base.entity.Address;

import ru.tandemservice.uni.component.log.EntityLogViewBase.Model;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.base.entity.PersonSportAchievement;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.Discipline2OlympiadDiplomaRelation;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantAccessCourse;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantExamList;
import ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.ExamPassMark;
import ru.tandemservice.uniec.entity.entrant.ExamPassMarkAppeal;
import ru.tandemservice.uniec.entity.entrant.InterviewResult;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

/**
 * @author ekachanova
 */
public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
            Entrant.ENTITY_CLASS,
            Person.ENTITY_CLASS,
            IdentityCard.ENTITY_CLASS,
            PersonEduInstitution.ENTITY_CLASS,
            PersonBenefit.ENTITY_CLASS,
            PersonNextOfKin.ENTITY_CLASS,
            PersonSportAchievement.ENTITY_CLASS,
            PersonForeignLanguage.ENTITY_CLASS,
            Address.ENTITY_CLASS,
            OlympiadDiploma.ENTITY_CLASS,
            EntrantStateExamCertificate.ENTITY_CLASS,
            StateExamSubjectMark.ENTITY_CLASS,
            PreliminaryEnrollmentStudent.ENTITY_CLASS,
            EntrantRequest.ENTITY_CLASS,
            RequestedEnrollmentDirection.ENTITY_CLASS,
            EntrantEnrollmentDocument.ENTITY_CLASS,
            ChosenEntranceDiscipline.ENTITY_CLASS,
            ExamPassDiscipline.ENTITY_CLASS,
            ExamPassMark.ENTITY_CLASS,
            ExamPassMarkAppeal.ENTITY_CLASS,
            InterviewResult.ENTITY_CLASS,
            RequestedProfileKnowledge.ENTITY_CLASS,
            ProfileExaminationMark.ENTITY_CLASS,
            Discipline2OlympiadDiplomaRelation.ENTITY_CLASS,
            EntrantInfoAboutUniversity.ENTITY_CLASS,
            EntrantExamList.ENTITY_CLASS,
            EntrantAccessCourse.ENTITY_CLASS //Подготовительный курс
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }

    @Override
    protected Collection<Long> getEntityIds(Model model)
    {
        Entrant entrant = get(Entrant.class, model.getEntityId());
        Set<Long> entityIds = new HashSet<Long>();
        entityIds.add(entrant.getId());
        entityIds.add(entrant.getPerson().getId());
        entityIds.add(entrant.getPerson().getIdentityCard().getId());
        return entityIds;
    }
}
