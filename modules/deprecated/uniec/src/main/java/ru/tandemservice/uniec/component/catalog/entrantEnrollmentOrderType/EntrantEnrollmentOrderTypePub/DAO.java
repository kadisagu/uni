/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.catalog.entrantEnrollmentOrderType.EntrantEnrollmentOrderTypePub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.core.entity.ViewWrapper;

import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup;

/**
 * @author AutoGenerator
 *         Created on 09.11.2009
 */
public class DAO extends DefaultCatalogPubDAO<EntrantEnrollmentOrderType, Model> implements IDAO
{
    @Override
    public void prepareListItemDataSource(Model model)
    {
        List<EntrantEnrollmentOrderType> result = CatalogManager.instance().dao().getCatalogItemListOrderByCode(EntrantEnrollmentOrderType.class);
        UniBaseUtils.createPage(model.getDataSource(), result);

        Map<EntrantEnrollmentOrderType, String> groupMap = new HashMap<EntrantEnrollmentOrderType, String>();
        for (EntrantEnrOrderTypeToGroup rel : getList(EntrantEnrOrderTypeToGroup.class))
        {
            String group = groupMap.get(rel.getType());
            if (null == group)
                group = rel.getGroup().getTitle();
            else
                group = group + ", " + rel.getGroup().getTitle();
            groupMap.put(rel.getType(), group);
        }

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            final String groupTitle = groupMap.get(wrapper.getEntity());
            wrapper.setViewProperty("group", groupTitle == null ? "" : groupTitle);
        }
    }
}
