/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantStateExamCertificateList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.dao.IStateExamCertificateDAO;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author agolubenko
 * @since 06.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    private IStateExamCertificateDAO _certificateDAO;

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());

        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() == null)
        {
            DynamicListDataSource<EntrantStateExamCertificate> dataSource = new DynamicListDataSource<EntrantStateExamCertificate>(component, this);

            dataSource.addColumn(new PublisherColumnBuilder("Абитуриент", Entrant.P_FULLFIO, null).entity(EntrantStateExamCertificate.entrant().s()).subTab(null).build());
            dataSource.addColumn(new SimpleColumn("Пол", EntrantStateExamCertificate.entrant().person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Паспорт", new String[]{EntrantStateExamCertificate.L_ENTRANT, Entrant.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.P_FULL_NUMBER}).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Дата рождения", EntrantStateExamCertificate.entrant().person().identityCard().birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            dataSource.addColumn(new BlockColumn("subjectsMarks", "Баллы по предметам"));
            dataSource.addColumn(new ToggleColumn("Зачтено", EntrantStateExamCertificate.P_ACCEPTED).setListener("onChangeCertificateChecked").setPermissionKey("editCheckedEntrantStateExamCertificate"));
            dataSource.addColumn(new ToggleColumn("Проверено", EntrantStateExamCertificate.P_SENT).setListener("onChangeCertificateSent").setPermissionKey("editSentEntrantStateExamCertificate"));
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditCertificate").setDisabledProperty(EntrantStateExamCertificate.P_SENT).setPermissionKey("editEntrantStateExamCertificate"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteCertificate", "Удалить свидетельство ЕГЭ № «{0}»", EntrantStateExamCertificate.P_TITLE).setDisabledProperty(EntrantStateExamCertificate.P_ACCEPTED).setPermissionKey("deleteEntrantStateExamCertificate"));

            dataSource.setOrder(1, OrderDirection.asc);
            model.setDataSource(dataSource);
        }
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickEditCertificate(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_STATE_EXAM_CERTIFICATE_ADD_EDIT, new ParametersMap().add("certificateId", component.getListenerParameter())));
    }

    public void onClickDeleteCertificate(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
        getModel(component).getDataSource().refresh();
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        EntrantFilterUtil.resetEnrollmentCampaignFilter(getModel(component));
        onClickSearch(component);
    }

    public void onChangeCertificateChecked(IBusinessComponent component)
    {
        try
        {
            _certificateDAO.changeCertificateChecked((Long) component.getListenerParameter());
        } finally
        {
            // при следующем рендере почистить сессию
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void onChangeCertificateSent(IBusinessComponent component)
    {
        try
        {
            _certificateDAO.changeCertificateSent((Long) component.getListenerParameter());
        } finally
        {
            // при следующем рендере почистить сессию
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
        }
    }

    public void setCertificateDAO(IStateExamCertificateDAO certificateDAO)
    {
        _certificateDAO = certificateDAO;
    }
}
