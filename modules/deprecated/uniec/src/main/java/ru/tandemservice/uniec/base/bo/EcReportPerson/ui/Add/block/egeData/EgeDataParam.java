package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.egeData;

import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.StateExamType;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EgeDataParam implements IReportDQLModifier
{
    private IReportParam<IIdentifiableWrapper> _stateExamInternal = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<IIdentifiableWrapper> _hasStateExamCertificate = new ReportParam<IIdentifiableWrapper>();
    private IReportParam<StateExamType> _stateExamType = new ReportParam<StateExamType>();
    private IReportParam<List<IIdentifiableWrapper>> _certificateIssuanceYear = new ReportParam<List<IIdentifiableWrapper>>();
    private IReportParam<List<StateExamSubject>> _stateExamSubject = new ReportParam<List<StateExamSubject>>();

    private String entrantAlias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(Entrant.class, Entrant.person());

        // добавляем сортировку
        dql.order(entrantAlias, Entrant.enrollmentCampaign().id());

        return entrantAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_stateExamInternal.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamInternal", _stateExamInternal.getData().getTitle());

            String nextAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(ExamPassDiscipline.class, nextAlias)
                    .column(DQLExpressions.property(ExamPassDiscipline.id().fromAlias(nextAlias)))
                    .where(DQLExpressions.eq(DQLExpressions.property(ExamPassDiscipline.entrantExamList().entrant().entrantId().fromAlias(nextAlias)), DQLExpressions.property(Entrant.id().fromAlias(entrantAlias(dql)))))
                    .where(DQLExpressions.eq(DQLExpressions.property(ExamPassDiscipline.subjectPassForm().code().fromAlias(nextAlias)), DQLExpressions.value(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL)))
                    .buildQuery();

            dql.builder.where(_stateExamInternal.getData().getId().equals(EgeDataBlock.STATE_EXAM_INTERNAL_YES) ? DQLExpressions.exists(query) : DQLExpressions.notExists(query));
        }

        if (_hasStateExamCertificate.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "egeData.hasStateExamCertificate", _hasStateExamCertificate.getData().getTitle());

            String nextAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EntrantStateExamCertificate.class, nextAlias)
                    .column(DQLExpressions.property(EntrantStateExamCertificate.id().fromAlias(nextAlias)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.entrant().id().fromAlias(nextAlias)), DQLExpressions.property(Entrant.id().fromAlias(entrantAlias(dql)))))
                    .buildQuery();

            dql.builder.where(_hasStateExamCertificate.getData().getId().equals(EgeDataBlock.HAS_STATE_EXAM_CERTIFICATE_YES) ? DQLExpressions.exists(query) : DQLExpressions.notExists(query));
        }

        if (_stateExamType.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamType", _stateExamType.getData().getTitle());

            String nextAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EntrantStateExamCertificate.class, nextAlias)
                    .column(DQLExpressions.property(EntrantStateExamCertificate.id().fromAlias(nextAlias)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.entrant().id().fromAlias(nextAlias)), DQLExpressions.property(Entrant.id().fromAlias(entrantAlias(dql)))))
                    .where(DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.stateExamType().fromAlias(nextAlias)), DQLExpressions.value(_stateExamType.getData())))
                    .buildQuery();

            dql.builder.where(DQLExpressions.exists(query));
        }

        if (_certificateIssuanceYear.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "egeData.certificateIssuanceYear", CommonBaseStringUtil.join(_certificateIssuanceYear.getData(), "title", ", "));

            List<Integer> yearList = new ArrayList<Integer>();
            for (IIdentifiableWrapper wrapper : _certificateIssuanceYear.getData())
                yearList.add(wrapper.getId().intValue());

            String nextAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(EntrantStateExamCertificate.class, nextAlias)
                    .column(DQLExpressions.property(EntrantStateExamCertificate.id().fromAlias(nextAlias)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EntrantStateExamCertificate.entrant().id().fromAlias(nextAlias)), DQLExpressions.property(Entrant.id().fromAlias(entrantAlias(dql)))))
                    .where(DQLExpressions.in(DQLFunctions.cast(DQLFunctions.substring(DQLExpressions.property(EntrantStateExamCertificate.number().fromAlias(nextAlias)), DQLExpressions.value(12), DQLExpressions.value(14)), PropertyType.INTEGER), yearList))
                        .buildQuery();

            dql.builder.where(DQLExpressions.exists(query));
        }

        if (_stateExamSubject.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "egeData.stateExamSubject", CommonBaseStringUtil.join(_stateExamSubject.getData(), StateExamSubject.P_TITLE, ", "));

            String nextAlias = dql.nextAlias();

            IDQLSelectableQuery query = new DQLSelectBuilder().fromEntity(StateExamSubjectMark.class, nextAlias)
                    .column(DQLExpressions.property(StateExamSubjectMark.id().fromAlias(nextAlias)))
                    .where(DQLExpressions.eq(DQLExpressions.property(StateExamSubjectMark.certificate().entrant().id().fromAlias(nextAlias)), DQLExpressions.property(Entrant.id().fromAlias(entrantAlias(dql)))))
                    .where(DQLExpressions.in(DQLExpressions.property(StateExamSubjectMark.subject().fromAlias(nextAlias)), _stateExamSubject.getData()))
                    .buildQuery();

            dql.builder.where(DQLExpressions.exists(query));
        }
    }

    // Getters

    public IReportParam<IIdentifiableWrapper> getStateExamInternal()
    {
        return _stateExamInternal;
    }

    public IReportParam<IIdentifiableWrapper> getHasStateExamCertificate()
    {
        return _hasStateExamCertificate;
    }

    public IReportParam<StateExamType> getStateExamType()
    {
        return _stateExamType;
    }

    public IReportParam<List<IIdentifiableWrapper>> getCertificateIssuanceYear()
    {
        return _certificateIssuanceYear;
    }

    public IReportParam<List<StateExamSubject>> getStateExamSubject()
    {
        return _stateExamSubject;
    }
}
