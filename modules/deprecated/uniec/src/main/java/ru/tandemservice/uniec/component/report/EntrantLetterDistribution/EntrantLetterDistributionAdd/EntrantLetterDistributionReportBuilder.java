/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantLetterDistribution.EntrantLetterDistributionAdd;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author oleyba
 * @since 06.07.2009
 */
class EntrantLetterDistributionReportBuilder
{
    private Model _model;
    private Session _session;
    private List<ReportRowGroup> _groupList = new ArrayList<ReportRowGroup>();
    // MultiKey - getKeyOrgUnit(enrollmentDirection), educationLevelsHighSchool
    private Map<MultiKey, ReportRow> _rowMap = new HashMap<MultiKey, ReportRow>();
    private Set<Character> _usedLetters = new HashSet<Character>();

    public EntrantLetterDistributionReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        prepareReportRowList();

        if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT.equals(_model.getEnrollmentCampaignStage().getId()))
            fillRowListByPreliminary();
        else
            fillRowListByDirection();

        String highSchoolTitle = TopOrgUnit.getInstance().getTitle();
        String paramsTitle = getParamsTitle();

        try
        {
            return EntrantLetterDistributionExcelBuilder.buildExcelContent(highSchoolTitle, paramsTitle, _groupList, _usedLetters);
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    void fillRowListByDirection()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        if (Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS.equals((_model.getEnrollmentCampaignStage().getId())))
            builder.add(MQExpression.notEq("red", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

        if (Model.ENROLLMENT_CAMP_STAGE_EXAMS.equals(_model.getEnrollmentCampaignStage().getId()))
            // состояние выбранной дисциплины приемной кампании должно быть любое состояние, кроме «Забрал документы», «Выбыл из конкурса», «Активный»
            builder.add(MQExpression.notIn("red", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE,
                    UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                    UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                    UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
        // параметры отчета
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        if (_model.isCompensationTypeActive())
            builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getCompensationType()));
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)
                    :
                    MQExpression.in("red", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList())
            );
        // не архивные
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : builder.<RequestedEnrollmentDirection>getResultList(_session))
            addDirection(requestedEnrollmentDirection);
    }

    void fillRowListByPreliminary()
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        // параметры отчета
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        if (_model.isCompensationTypeActive())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getCompensationType()));
        if (_model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)
                    :
                    MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList())
            );
        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.TRUE));
        // не архивные
        builder.add(MQExpression.notEq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        for (PreliminaryEnrollmentStudent preliminary : builder.<PreliminaryEnrollmentStudent>getResultList(_session))
            addDirection(preliminary.getRequestedEnrollmentDirection());
    }

    private void addDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        OrgUnit orgUnit = getKeyOrgUnit(requestedEnrollmentDirection.getEnrollmentDirection());
        EducationLevelsHighSchool highSchool = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();
        MultiKey key = new MultiKey(orgUnit, highSchool);
        _rowMap.get(key).add(_usedLetters, requestedEnrollmentDirection.getEntrantRequest().getEntrant());
    }

    void prepareReportRowList()
    {
        AddressDetailed address = TopOrgUnit.getInstance().getAddress();
        AddressItem city = address == null ? null : address.getSettlement();
        String highSchoolCityTitle = city == null ? "город ОУ" : city.getTitleWithType();

        ReportRowGroup firstGroup = null;
        ReportRowGroup totalGroup = new ReportRowGroup("");
        Map<OrgUnit, Set<EducationLevelsHighSchool>> orgUnit2highSchoolList = getOrgUnit2HighSchoolList();
        for (Map.Entry<OrgUnit, Set<EducationLevelsHighSchool>> entry : orgUnit2highSchoolList.entrySet())
        {
            OrgUnit orgUnit = entry.getKey();
            String title = orgUnit == null ? highSchoolCityTitle : (orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());
            ReportRowGroup group = new ReportRowGroup(title);
            group.setGroup(totalGroup);
            if (orgUnit == null)
                firstGroup = group;
            else
                _groupList.add(group);
            for (EducationLevelsHighSchool highSchool : entry.getValue())
            {
                ReportRow row = new ReportRow(group, highSchool.getPrintTitle(), highSchool.getPrintTitle() + " (" + highSchool.getOrgUnit().getShortTitle() + ")");
                group.getRowList().add(row);
                _rowMap.put(new MultiKey(orgUnit, highSchool), row);
            }
            Collections.sort(group.getRowList(), ITitled.TITLED_COMPARATOR);
            // если получаются строки с одинаковым названием,
            // то используем название с приписанным в скобках выпускающим подразделением
            ReportRow prevRow = null;
            for (ReportRow row : group.getRowList())
            {
                if (prevRow != null && row.getTitle().equals(prevRow.getTitle()))
                {
                    prevRow.useFullTitle();
                    row.useFullTitle();
                }
                prevRow = row;
            }
        }
        Collections.sort(_groupList, ITitled.TITLED_COMPARATOR);
        if (null != firstGroup) _groupList.add(0, firstGroup);
    }

    private Map<OrgUnit, Set<EducationLevelsHighSchool>> getOrgUnit2HighSchoolList()
    {
        Set<EnrollmentDirection> enrollmentDirectionSet = getEnrollmentDirectionSet();
        Map<OrgUnit, Set<EducationLevelsHighSchool>> result = new HashMap<OrgUnit, Set<EducationLevelsHighSchool>>();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirectionSet)
        {
            OrgUnit key = getKeyOrgUnit(enrollmentDirection);
            Set<EducationLevelsHighSchool> set = result.get(key);
            if (set == null)
                result.put(key, set = new HashSet<EducationLevelsHighSchool>());
            set.add(enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool());
        }
        return result;
    }

    private OrgUnit getKeyOrgUnit(EnrollmentDirection enrollmentDirection)
    {
        OrgUnit key = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit();
        if (!key.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) && !key.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.REPRESENTATION))
            return null;
        return key;
    }

    // список направлений приема удовлетв. фильтрам. ( по ним будут строится строки отчета)
    private Set<EnrollmentDirection> getEnrollmentDirectionSet()
    {
        // получаем список строк отчета (в него попадают все выбранные направления приема сгруппированные по территориальному расположению)
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));
        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)
                    :
                    MQExpression.in("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList())
            );
        return new HashSet<EnrollmentDirection>(builder.<EnrollmentDirection>getResultList(_session));
    }

    private String getParamsTitle()
    {
        // получаем строку параметров отчета
        List<String> paramsTitleList = new ArrayList<String>();
        paramsTitleList.add("Приемная кампания: " + _model.getReport().getEnrollmentCampaign().getTitle());
        paramsTitleList.add("Стадия приемной кампании: " + _model.getReport().getEnrollmentCampStage());
        paramsTitleList.add("Заявления с: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getDateFrom()));
        paramsTitleList.add("Заявления по: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getDateTo()));
        if (_model.isCompensationTypeActive())
            paramsTitleList.add("Финансирование: " + _model.getCompensationType().getShortTitle());
        if (_model.isQualificationActive())
            paramsTitleList.add("Квалификация: " + UniStringUtils.join(_model.getQualificationList(), Qualifications.P_TITLE, ", "));
        if (_model.isStudentCategoryActive())
            paramsTitleList.add("Категория поступающего: " + UniStringUtils.join(_model.getStudentCategoryList(), StudentCategory.P_TITLE, ", "));
        if (_model.isFormativeOrgUnitActive())
            paramsTitleList.add("Формирующее подр.: " + UniStringUtils.join(_model.getFormativeOrgUnitList(), OrgUnit.P_TYPE_TITLE, ", "));
        if (_model.isTerritorialOrgUnitActive())
            paramsTitleList.add("Территориальное подр.: " + UniStringUtils.join(_model.getTerritorialOrgUnitList(), OrgUnit.P_TYPE_TITLE, ", "));
        if (_model.isDevelopFormActive())
            paramsTitleList.add("Форма освоения: " + UniStringUtils.join(_model.getDevelopFormList(), DevelopCondition.P_TITLE, ", "));
        if (_model.isDevelopConditionActive())
            paramsTitleList.add("Условие освоения: " + UniStringUtils.join(_model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));
        if (_model.isDevelopTechActive())
            paramsTitleList.add("Технология освоения: " + UniStringUtils.join(_model.getDevelopTechList(), DevelopCondition.P_TITLE, ", "));
        if (_model.isDevelopPeriodActive())
            paramsTitleList.add("Срок освоения: " + UniStringUtils.join(_model.getDevelopPeriodList(), DevelopCondition.P_TITLE, ", "));
        if (_model.isParallelActive())
            paramsTitleList.add("Исключая поступивших на параллельное освоение: " + YesNoFormatter.INSTANCE.format(_model.getParallel().isTrue()));
        return org.apache.commons.lang.StringUtils.join(paramsTitleList.iterator(), "; ");
    }
}
