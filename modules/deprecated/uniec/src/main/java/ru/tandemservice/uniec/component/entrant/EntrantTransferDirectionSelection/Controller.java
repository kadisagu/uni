/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantTransferDirectionSelection;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;

/**
 * @author agolubenko
 * @since 03.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    private void prepareDataSource(final IBusinessComponent component)
    {
        final DynamicListDataSource<RequestedEnrollmentDirection> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new RadioButtonColumn().setDisableHandler(entity -> !((RequestedEnrollmentDirection) entity).getState().getCode().equals(UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE)));
        dataSource.addColumn(new SimpleColumn("Рег. №", "", UniecDAOFacade.getRequestedEnrollmentDirectionNumberFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION, EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT, EducationOrgUnitGen.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{RequestedEnrollmentDirectionGen.L_ENROLLMENT_DIRECTION, EnrollmentDirectionGen.L_EDUCATION_ORG_UNIT, EducationOrgUnitGen.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()));
        dataSource.addColumn(new SimpleColumn("Форма освоения", RequestedEnrollmentDirectionGen.enrollmentDirection().educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", RequestedEnrollmentDirectionGen.enrollmentDirection().educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", RequestedEnrollmentDirection.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ЦП", RequestedEnrollmentDirectionGen.P_TARGET_ADMISSION, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Категория поступающего", RequestedEnrollmentDirectionGen.L_STUDENT_CATEGORY + "." + ICatalogItem.CATALOG_ITEM_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", RequestedEnrollmentDirectionGen.L_COMPETITION_KIND + "." + ICatalogItem.CATALOG_ITEM_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", RequestedEnrollmentDirection.state().title().s()).setClickable(false).setOrderable(false).setWidth(7));
        dataSource.addColumn(new SimpleColumn("Оригиналы документов", RequestedEnrollmentDirection.originalDocumentHandedIn().s(), YesNoFormatter.IGNORE_NUMBERS).setClickable(false).setOrderable(false));
        getModel(component).setDataSource(dataSource);
    }

    public void onClickTransfer(final IBusinessComponent component)
    {
        final Model model = getModel(component);

        final RequestedEnrollmentDirection selectedDirection = (RequestedEnrollmentDirection) ((RadioButtonColumn) model.getDataSource().getColumn(0)).getSelectedEntity();
        if (selectedDirection == null)
        {
            throw new ApplicationException("Необходимо выбрать направление, с которого будет производиться перевод.");
        }

        model.setRequestedEnrollmentDirection(selectedDirection);
        try
        {
            getDao().update(model);
        }
        catch (final ApplicationException e)
        {
            final ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

            // возвращаемся на предыдущий шаг визарда
            deactivate(component);
            errorCollector.add(e.getMessage());
            return;
        }

        deactivate(component, 2);
    }

    public void onClickCancel(final IBusinessComponent component)
    {
        deactivate(component, 2);
    }
}
