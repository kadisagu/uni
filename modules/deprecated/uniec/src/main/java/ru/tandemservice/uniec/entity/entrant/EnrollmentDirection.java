package ru.tandemservice.uniec.entity.entrant;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CourseCodes;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.gen.EnrollmentDirectionGen;

public class EnrollmentDirection extends EnrollmentDirectionGen implements ITitled
{
    public static final String[] FORMATIVE_ORG_UNIT_SHORT_TITLE_KEY = new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_SHORT_TITLE};
    public static final String[] TERRITORIAL_ORG_UNIT_SHORT_TITLE_KEY = new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_SHORT_TITLE};

    @EntityDSLSupport
    @Override
    public String getShortTitle()
    {
        final EducationLevelsHighSchool levelsHighSchool = getEducationOrgUnit().getEducationLevelHighSchool();
        EducationLevels educationLevel = levelsHighSchool.getEducationLevel();
        return educationLevel.getTitleCodePrefix() + " " + levelsHighSchool.getShortTitle() + " (" + educationLevel.getLevelType().getTitle() + ")";
    }

    @EntityDSLSupport
    @Override
    public String getPrintTitle()
    {
        return getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle() + " (" + getEducationOrgUnit().getDevelopCombinationTitle().toLowerCase() + ")";
    }


    private Course _course_cache;

    /**
     * @return курс, на который ведется прием
     */
    public Course getEntranceCourse() {
        if (null == _course_cache) {
            _course_cache = DataAccessServices.dao().getByNaturalId(new CourseGen.NaturalId(CourseCodes.COURSE_1));
        }
        return _course_cache;
    }
}