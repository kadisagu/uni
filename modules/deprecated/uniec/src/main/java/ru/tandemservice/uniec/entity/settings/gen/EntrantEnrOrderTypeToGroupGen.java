package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.StudentExtractGroup;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь типа приказа с группой (приказы о зачислении)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantEnrOrderTypeToGroupGen extends EntityBase
 implements INaturalIdentifiable<EntrantEnrOrderTypeToGroupGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup";
    public static final String ENTITY_NAME = "entrantEnrOrderTypeToGroup";
    public static final int VERSION_HASH = -1963274884;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_GROUP = "group";

    private EntrantEnrollmentOrderType _type;     // Тип приказа на зачисление абитуриента в личный состав студентов
    private StudentExtractGroup _group;     // Группы приказов по движению студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     */
    @NotNull
    public EntrantEnrollmentOrderType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     */
    public void setType(EntrantEnrollmentOrderType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Группы приказов по движению студентов. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группы приказов по движению студентов. Свойство не может быть null.
     */
    public void setGroup(StudentExtractGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantEnrOrderTypeToGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setType(((EntrantEnrOrderTypeToGroup)another).getType());
                setGroup(((EntrantEnrOrderTypeToGroup)another).getGroup());
            }
        }
    }

    public INaturalId<EntrantEnrOrderTypeToGroupGen> getNaturalId()
    {
        return new NaturalId(getType(), getGroup());
    }

    public static class NaturalId extends NaturalIdBase<EntrantEnrOrderTypeToGroupGen>
    {
        private static final String PROXY_NAME = "EntrantEnrOrderTypeToGroupNaturalProxy";

        private Long _type;
        private Long _group;

        public NaturalId()
        {}

        public NaturalId(EntrantEnrollmentOrderType type, StudentExtractGroup group)
        {
            _type = ((IEntity) type).getId();
            _group = ((IEntity) group).getId();
        }

        public Long getType()
        {
            return _type;
        }

        public void setType(Long type)
        {
            _type = type;
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EntrantEnrOrderTypeToGroupGen.NaturalId) ) return false;

            EntrantEnrOrderTypeToGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getType(), that.getType()) ) return false;
            if( !equals(getGroup(), that.getGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getType());
            result = hashCode(result, getGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getType());
            sb.append("/");
            sb.append(getGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantEnrOrderTypeToGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantEnrOrderTypeToGroup.class;
        }

        public T newInstance()
        {
            return (T) new EntrantEnrOrderTypeToGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((EntrantEnrollmentOrderType) value);
                    return;
                case "group":
                    obj.setGroup((StudentExtractGroup) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return EntrantEnrollmentOrderType.class;
                case "group":
                    return StudentExtractGroup.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantEnrOrderTypeToGroup> _dslPath = new Path<EntrantEnrOrderTypeToGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantEnrOrderTypeToGroup");
    }
            

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup#getType()
     */
    public static EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Группы приказов по движению студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup#getGroup()
     */
    public static StudentExtractGroup.Path<StudentExtractGroup> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends EntrantEnrOrderTypeToGroup> extends EntityPath<E>
    {
        private EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> _type;
        private StudentExtractGroup.Path<StudentExtractGroup> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа на зачисление абитуриента в личный состав студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup#getType()
     */
        public EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> type()
        {
            if(_type == null )
                _type = new EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Группы приказов по движению студентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EntrantEnrOrderTypeToGroup#getGroup()
     */
        public StudentExtractGroup.Path<StudentExtractGroup> group()
        {
            if(_group == null )
                _group = new StudentExtractGroup.Path<StudentExtractGroup>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return EntrantEnrOrderTypeToGroup.class;
        }

        public String getEntityName()
        {
            return "entrantEnrOrderTypeToGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
