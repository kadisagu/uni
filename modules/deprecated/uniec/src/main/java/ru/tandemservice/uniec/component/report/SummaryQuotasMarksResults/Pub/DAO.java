/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Pub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.report.SummaryQuotasMarksResultsReport;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(SummaryQuotasMarksResultsReport.class, model.getReport().getId()));
    }
}
