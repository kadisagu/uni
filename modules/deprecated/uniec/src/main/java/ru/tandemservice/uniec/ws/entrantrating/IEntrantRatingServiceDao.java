package ru.tandemservice.uniec.ws.entrantrating;

import java.util.Date;
import java.util.List;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 29.03.2011
 */
public interface IEntrantRatingServiceDao
{
    public static final SpringBeanCache<IEntrantRatingServiceDao> INSTANCE = new SpringBeanCache<IEntrantRatingServiceDao>(IEntrantRatingServiceDao.class.getName());

    EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeId, List<String> studentCategoryIds, boolean hideEmptyDirections, List<String> qualificationIds, List<String> developFormIds, List<String> developConditionIds);

    EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(String enrollmentCampaignTitle, Date dateFrom, Date dateTo, String compensationTypeId, List<String> studentCategoryIds, String enrollmentDirectionId);

    EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(EnrollmentCampaign enrollmentCampaign, Date dateFrom, Date dateTo, CompensationType compensationType, List<StudentCategory> studentCategoryList, boolean hideEmptyDirections, List<Qualifications> qualificationList, List<DevelopForm> developFormList, List<DevelopCondition> developConditionList);

    EntrantRatingTAEnvironmentNode getEntrantRatingTAEnvironmentData(EnrollmentCampaign enrollmentCampaign, Date dateFrom, Date dateTo, CompensationType compensationType, List<StudentCategory> studentCategoryList, EnrollmentDirection enrollmentDirection);
}
