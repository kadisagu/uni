/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.order.EnrollmentVisaGroupAdd;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.settings.EnrollmentOrderVisaItem;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EnrollmentOrder order = getNotNull(EnrollmentOrder.class, model.getDocumentId());

        MQBuilder builder = new MQBuilder(EnrollmentOrderVisaItem.ENTITY_CLASS, "e", new String[]{EnrollmentOrderVisaItem.P_TITLE});
        builder.add(MQExpression.eq("e", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN, order.getEnrollmentCampaign()));
        builder.addOrder("e", EnrollmentOrderVisaItem.P_TITLE);

        Set<String> titleSet = new HashSet<>(builder.<String>getResultList(getSession()));
        List<IdentifiableWrapper> list = new ArrayList<>();
        long i = 0;
        for (String title : titleSet)
            list.add(new IdentifiableWrapper(i++, title));

        model.setVisaGroupListModel(new LazySimpleSelectModel<>(list));
    }

    @Override
    public void update(Model model)
    {
        EnrollmentOrder order = getNotNull(EnrollmentOrder.class, model.getDocumentId());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentOrderVisaItem.class, "i");
        builder.column("i");
        builder.where(DQLExpressions.eq(DQLExpressions.property("i", EnrollmentOrderVisaItem.L_ENROLLMENT_CAMPAIGN), DQLExpressions.value(order.getEnrollmentCampaign())));
        builder.where(DQLExpressions.eq(DQLExpressions.property("i", EnrollmentOrderVisaItem.P_TITLE), DQLExpressions.value(model.getVisaGroup().getTitle())));
        builder.order(DQLExpressions.property("i", EnrollmentOrderVisaItem.P_PRIORITY));
        List<EnrollmentOrderVisaItem> itemList = builder.createStatement(new DQLExecutionContext(getSession())).list();

        IAbstractDocument document = get(model.getDocumentId());
        for (EnrollmentOrderVisaItem visaItem : itemList)
        {
//            if (UnimvDaoFacade.getVisaDao().getVisa(document, visaItem.getPossibleVisa()) == null)
//            {
                Visa visa = new Visa();
                visa.setMandatory(false);
                visa.setPossibleVisa(visaItem.getPossibleVisa());
                visa.setDocument(document);
                visa.setGroupMemberVising(visaItem.getGroupsMemberVising());
                visa.setIndex(getCount(Visa.class, Visa.L_DOCUMENT, document));
                save(visa);
//            }
        }
    }
}
