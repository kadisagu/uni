/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.ReportList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.sec.runtime.SecurityRuntime;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;

/**
 * @author ekachanova
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model, IPrincipalContext principalContext)
    {
        IDataSettings settings = DataSettingsFacade.getSettings(UniecDefines.EC_MODULE_OWNER, UniecDefines.IS_REPORT_USED);
        ISecured securedObject = (ISecured) SecurityRuntime.getInstance().getCommonSecurityObject();

        List<ReportDefinitionWrapper> wrappers = new ArrayList<ReportDefinitionWrapper>();
        Long id = 0L;
        for (ReportDefinition repDef : (List<ReportDefinition>) ApplicationRuntime.getBean(IReportDefinition.UNIEC_REPORT_DEFINITION_LIST_BEAN_NAME))
        {
            Boolean used = (Boolean)settings.getEntry(repDef.getId()).getValue();
            if(used != null && !used) { continue; }
            if(!CoreServices.securityService().check(securedObject, principalContext, repDef.getPermissionKey())) { continue; }
            wrappers.add(new ReportDefinitionWrapper(id++, repDef));
        }
        Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

        model.getDataSource().setTotalSize(wrappers.size());
        model.getDataSource().setCountRow(wrappers.size());
        model.getDataSource().createPage(wrappers);
    }

    class ReportDefinitionWrapper extends IdentifiableWrapper
    {
        private static final long serialVersionUID = 4199871956331006485L;
        private ReportDefinition _reportDefinition;

        public ReportDefinitionWrapper(Long id, ReportDefinition reportDefinition)
        {
            super(id, reportDefinition.getTitle());
            _reportDefinition = reportDefinition;
        }

        public ReportDefinition getReportDefinition()
        {
            return _reportDefinition;
        }
    }
}
