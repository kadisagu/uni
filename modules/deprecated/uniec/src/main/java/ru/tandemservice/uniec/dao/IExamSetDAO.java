/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author agolubenko
 * @since 22.01.2009
 */
public interface IExamSetDAO
{
    /**
     * Создает все наборы экзаменов для указанной приемной кампании
     *
     * @param enrollmentCampaign Приемная кампания
     * @return ID набора экзамена -> набор экзаменов
     */
    Map<String, ExamSet> getExamSetMap(EnrollmentCampaign enrollmentCampaign);

    /**
     *
     * @param enrollmentCampaign
     * @return
     */
    Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> getExamSetItemByEnrDirAndCategory(EnrollmentCampaign enrollmentCampaign);

    /**
     * Возвращает список испытаний по выбору для вступительного испытания
     *
     * @param entranceDiscipline вступительное испытание
     * @return список испытаний по выбору
     */
    List<SetDiscipline> getChoicableDisciplines(EntranceDiscipline entranceDiscipline);
    
    /**
     * Возвращает списки испытаний по выбору для вступительных испытаний
     * 
     * @param entranceDisciplines вступительные испытания
     * @return вступительное испытание - список испытаний по выбору
     */
    Map<EntranceDiscipline, List<SetDiscipline>> getChoicableDisciplines(Collection<EntranceDiscipline> entranceDisciplines);

    /**
     * Возвращает список наборов экзаменов, которые покрываются сертификатами ЕГЭ
     *
     * @param entrant абитуриент
     * @return список наборов экзаменов
     */
    Set<ExamSet> getCoveredExamSet(Entrant entrant);

    /**
     * Возвращает список направлений приема, все вступительные испытания которых покрываются сертификатами ЕГЭ
     *
     * @param entrant абитуриент
     * @return список направлений приема
     */
    Set<EnrollmentDirection> getCoveredEnrollmentDirection(Entrant entrant);

    /**
     * Возвращает список дисциплин приемной кампании, которые покрываются сертификатами ЕГЭ и по ним есть форма сдачи ЕГЭ
     *
     * @param entrant абитуриента
     * @return мн-во покрываемых дисциплин
     */
    Set<Discipline2RealizationWayRelation> getCoveredDisciplineList(Entrant entrant);
}
