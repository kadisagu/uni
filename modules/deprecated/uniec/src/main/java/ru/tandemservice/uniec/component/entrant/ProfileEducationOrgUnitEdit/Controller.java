/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ProfileEducationOrgUnitEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;

/**
 * @author Vasily Zhukov
 * @since 25.04.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        StaticListDataSource<ProfileEducationOrgUnit> dataSource = new StaticListDataSource<ProfileEducationOrgUnit>(model.getDtoList());
        BlockColumn<Boolean> checkboxColumn = new BlockColumn<Boolean>("checkbox", "");
        checkboxColumn.setValueMap(model.getValueMap());
        dataSource.addColumn(checkboxColumn);
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", ProfileEducationOrgUnit.educationOrgUnit().educationLevelHighSchool().fullTitle()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", ProfileEducationOrgUnit.educationOrgUnit().formativeOrgUnit().shortTitle()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", ProfileEducationOrgUnit.educationOrgUnit().territorialOrgUnit().territorialShortTitle()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", ProfileEducationOrgUnit.educationOrgUnit().developForm().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", ProfileEducationOrgUnit.educationOrgUnit().developCondition().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", ProfileEducationOrgUnit.educationOrgUnit().developTech().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок", ProfileEducationOrgUnit.educationOrgUnit().developPeriod().title()).setOrderable(false));
        dataSource.addColumn(new BlockColumn("budgetPlan", "Б").setOrderable(false));
        dataSource.addColumn(new BlockColumn("contractPlan", "К").setOrderable(false));

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        deactivate(component);
    }
}
