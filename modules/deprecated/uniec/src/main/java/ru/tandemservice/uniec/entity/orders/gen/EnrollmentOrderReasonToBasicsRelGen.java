package ru.tandemservice.uniec.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причин для зачисления с основаниями
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderReasonToBasicsRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel";
    public static final String ENTITY_NAME = "enrollmentOrderReasonToBasicsRel";
    public static final int VERSION_HASH = -1252912637;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String L_BASIC = "basic";

    private EnrollmentOrderReason _reason;     // Причина приказа о зачислении абитуриентов
    private EnrollmentOrderBasic _basic;     // Основание приказа о зачислении абитуриентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentOrderReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    public void setReason(EnrollmentOrderReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Основание приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentOrderBasic getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    public void setBasic(EnrollmentOrderBasic basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderReasonToBasicsRelGen)
        {
            setReason(((EnrollmentOrderReasonToBasicsRel)another).getReason());
            setBasic(((EnrollmentOrderReasonToBasicsRel)another).getBasic());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderReasonToBasicsRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderReasonToBasicsRel.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderReasonToBasicsRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "reason":
                    return obj.getReason();
                case "basic":
                    return obj.getBasic();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "reason":
                    obj.setReason((EnrollmentOrderReason) value);
                    return;
                case "basic":
                    obj.setBasic((EnrollmentOrderBasic) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "reason":
                        return true;
                case "basic":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "reason":
                    return true;
                case "basic":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "reason":
                    return EnrollmentOrderReason.class;
                case "basic":
                    return EnrollmentOrderBasic.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderReasonToBasicsRel> _dslPath = new Path<EnrollmentOrderReasonToBasicsRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderReasonToBasicsRel");
    }
            

    /**
     * @return Причина приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel#getReason()
     */
    public static EnrollmentOrderReason.Path<EnrollmentOrderReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Основание приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel#getBasic()
     */
    public static EnrollmentOrderBasic.Path<EnrollmentOrderBasic> basic()
    {
        return _dslPath.basic();
    }

    public static class Path<E extends EnrollmentOrderReasonToBasicsRel> extends EntityPath<E>
    {
        private EnrollmentOrderReason.Path<EnrollmentOrderReason> _reason;
        private EnrollmentOrderBasic.Path<EnrollmentOrderBasic> _basic;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel#getReason()
     */
        public EnrollmentOrderReason.Path<EnrollmentOrderReason> reason()
        {
            if(_reason == null )
                _reason = new EnrollmentOrderReason.Path<EnrollmentOrderReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Основание приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.orders.EnrollmentOrderReasonToBasicsRel#getBasic()
     */
        public EnrollmentOrderBasic.Path<EnrollmentOrderBasic> basic()
        {
            if(_basic == null )
                _basic = new EnrollmentOrderBasic.Path<EnrollmentOrderBasic>(L_BASIC, this);
            return _basic;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderReasonToBasicsRel.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderReasonToBasicsRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
