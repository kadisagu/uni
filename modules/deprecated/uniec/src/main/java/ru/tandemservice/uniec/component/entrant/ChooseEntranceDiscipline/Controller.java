/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.entrant.ChooseEntranceDiscipline;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author vip_delete
 * @since 17.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, ChooseEntranceDisciplineModel>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ChooseEntranceDisciplineModel model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        final ChooseEntranceDisciplineModel model = getModel(component);

        DynamicListDataSource<RequestedEnrollmentDirectionWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        AbstractColumn enrollmentDirectionTitleColumn = new SimpleColumn(ChooseEntranceDisciplineModel.COLUMN_ENROLLMENT_DIRECTION_TITLE, "Выбранное направление подготовки (специальность)", "", new BaseRawFormatter<RequestedEnrollmentDirectionWrapper>()
                {
            @Override
            public String format(RequestedEnrollmentDirectionWrapper wrapper)
            {
                RequestedEnrollmentDirection source = wrapper.getDirection();
                EducationOrgUnit educationOrgUnit = source.getEnrollmentDirection().getEducationOrgUnit();
                List<String> lines = new ArrayList<String>();

                lines.add(educationOrgUnit.getEducationLevelHighSchool().getDisplayableTitle());
                lines.add("<b>" + educationOrgUnit.getFormativeOrgUnit().getOrgUnitType().getTitle() + ": </b>" + educationOrgUnit.getFormativeOrgUnit().getTitle());
                if (educationOrgUnit.getTerritorialOrgUnit() != null)
                    lines.add("<b>Территориальное подр.: </b>" + educationOrgUnit.getTerritorialOrgUnit().getTerritorialTitle());
                lines.add("<b>Вид затрат: </b>" + source.getCompensationType().getShortTitle() + "&nbsp;&nbsp;&nbsp;<b>Вид конкурса: </b>" + source.getCompetitionKind().getTitle());
                lines.add("<b>Состояние абитуриента: </b>" + source.getState().getTitle());
                return StringUtils.join(lines.iterator(), "<br/>");
            }
                }).setClickable(false).setOrderable(false);

        enrollmentDirectionTitleColumn.setStyle("width:50%;vertical-align:top;");
        dataSource.addColumn(enrollmentDirectionTitleColumn);

        AbstractColumn enrollmentDisciplinesTitleColumn = new BlockColumn(ChooseEntranceDisciplineModel.COLUMN_EXAM_SET, "Вступительные испытания");
        enrollmentDisciplinesTitleColumn.setClickable(false).setOrderable(false);
        enrollmentDisciplinesTitleColumn.setStyle("width:50%;vertical-align:middle;");
        dataSource.addColumn(enrollmentDisciplinesTitleColumn);

        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));

        deactivate(component);

        component.getUserContext().getRootRegion().getActiveComponent().refresh();
    }
}
