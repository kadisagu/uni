package ru.tandemservice.uniec.component.report.EnrolledDealList.Add;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.reports.YesNoUIObject;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.report.EnrolledDealListReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.List;

@SuppressWarnings("deprecation")
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    private EnrolledDealListReport _report = new EnrolledDealListReport();
    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private IPrincipalContext _principalContext;

    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private List<YesNoUIObject> _detailList;
    private YesNoUIObject _detail;

    private boolean _entrantEnrollmentOrderTypeActive;
    private List<EntrantEnrollmentOrderType> _entrantEnrollmentOrderTypeList;
    private ISelectModel _entrantEnrollmentOrderTypeListModel;

    private boolean _enrollmentOrderActive;
    private List<EnrollmentOrder> _enrollmentOrderList;
    private ISelectModel _enrollmentOrderListModel;

    private boolean _compensationTypeActive;
    private List<CompensationType> _compensationTypeList;

    private boolean _studentCategoryActive;
    private ISelectModel _studentCategoryListModel;
    private List<StudentCategory> _studentCategoryList;

    private boolean _qualificationActive;
    private List<Qualifications> _qualificationList;
    private ISelectModel _qualificationListModel;

    private boolean _formativeOrgUnitActive;
    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;

    private boolean _territorialOrgUnitActive;
    private ISelectModel _territorialOrgUnitListModel;
    private List<OrgUnit> _territorialOrgUnitList;

    private boolean _developFormActive;
    private ISelectModel _developFormListModel;
    private List<DevelopForm> _developFormList;

    private boolean _developConditionActive;
    private ISelectModel _developConditionListModel;
    private List<DevelopCondition> _developConditionList;

    private boolean _developTechActive;
    private ISelectModel _developTechListModel;
    private List<DevelopTech> _developTechList;

    private boolean _developPeriodActive;
    private ISelectModel _developPeriodListModel;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _parallelActive;
    private List<YesNoUIObject> _parallelList;
    private YesNoUIObject _parallel;

    private boolean _includeForeignPerson = true;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    public boolean isEntrantEnrollmentOrderTypeActive() {
        return _entrantEnrollmentOrderTypeActive;
    }

    public void setEntrantEnrollmentOrderTypeActive(boolean entrantEnrollmentOrderTypeActive) {
        _entrantEnrollmentOrderTypeActive = entrantEnrollmentOrderTypeActive;
    }

    public List<EntrantEnrollmentOrderType> getEntrantEnrollmentOrderTypeList() {
        return _entrantEnrollmentOrderTypeList;
    }

    public void setEntrantEnrollmentOrderTypeList(List<EntrantEnrollmentOrderType> entrantEnrollmentOrderTypeList) {
        _entrantEnrollmentOrderTypeList = entrantEnrollmentOrderTypeList;
    }

    public ISelectModel getEntrantEnrollmentOrderTypeListModel() {
        return _entrantEnrollmentOrderTypeListModel;
    }

    public void setEntrantEnrollmentOrderTypeListModel(ISelectModel entrantEnrollmentOrderTypeListModel) {
        _entrantEnrollmentOrderTypeListModel = entrantEnrollmentOrderTypeListModel;
    }

    public boolean isEnrollmentOrderActive() {
        return _enrollmentOrderActive;
    }

    public void setEnrollmentOrderActive(boolean enrollmentOrderActive) {
        _enrollmentOrderActive = enrollmentOrderActive;
    }

    public List<EnrollmentOrder> getEnrollmentOrderList() {
        return _enrollmentOrderList;
    }

    public void setEnrollmentOrderList(List<EnrollmentOrder> enrollmentOrderList) {
        _enrollmentOrderList = enrollmentOrderList;
    }

    public ISelectModel getEnrollmentOrderListModel() {
        return _enrollmentOrderListModel;
    }

    public void setEnrollmentOrderListModel(ISelectModel enrollmentOrderListModel) {
        _enrollmentOrderListModel = enrollmentOrderListModel;
    }

    public List<EntrantEnrollmentOrderType> getSelectedEnrollmentOrderTypeList()
    {
        return isEntrantEnrollmentOrderTypeActive() ? getEntrantEnrollmentOrderTypeList() : null;
    }

    public List<EnrollmentOrder> getSelectedEnrollmentOrderList()
    {
        return isEnrollmentOrderActive() ? getEnrollmentOrderList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    // Getters & Setters

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public EnrolledDealListReport getReport()
    {
        return _report;
    }

    public void setReport(EnrolledDealListReport report)
    {
        _report = report;
    }

    public List<YesNoUIObject> getDetailList()
    {
        return _detailList;
    }

    public void setDetailList(List<YesNoUIObject> detailList)
    {
        _detailList = detailList;
    }

    public YesNoUIObject getDetail()
    {
        return _detail;
    }

    public void setDetail(YesNoUIObject detail)
    {
        _detail = detail;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<YesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<YesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public YesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(YesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public boolean isIncludeForeignPerson() {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(boolean includeForeignPerson) {
        _includeForeignPerson = includeForeignPerson;
    }

    public boolean isCompensationTypeActive() {
        return _compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive) {
        _compensationTypeActive = compensationTypeActive;
    }
}
