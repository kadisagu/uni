/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ReportList;

import java.util.Map;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author ekachanova
 */
public class Model
{
    private DynamicListDataSource<DAO.ReportDefinitionWrapper> _dataSource;
    private Map<Long, DAO.ReportDefinitionWrapper> _wrapperById;

    public DynamicListDataSource<DAO.ReportDefinitionWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<DAO.ReportDefinitionWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<Long, DAO.ReportDefinitionWrapper> getWrapperById()
    {
        return _wrapperById;
    }

    public void setWrapperById(Map<Long, DAO.ReportDefinitionWrapper> wrapperById)
    {
        _wrapperById = wrapperById;
    }
}
