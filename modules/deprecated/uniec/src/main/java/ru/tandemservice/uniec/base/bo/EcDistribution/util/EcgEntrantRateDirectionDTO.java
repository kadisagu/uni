/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.tandemframework.core.entity.IdentifiableWrapper;

/**
 * @author Vasily Zhukov
 * @since 23.07.2011
 */
public class EcgEntrantRateDirectionDTO extends IdentifiableWrapper implements IEcgEntrantRateDirectionDTO
{
    private static final long serialVersionUID = 1L;
    private Long _enrollmentDirectionId;

    public EcgEntrantRateDirectionDTO(Long id, Long directionId, String shortTitle)
    {
        super(id, shortTitle);

        _enrollmentDirectionId = directionId;
    }

    @Override
    public Long getDirectionId()
    {
        return _enrollmentDirectionId;
    }
}
