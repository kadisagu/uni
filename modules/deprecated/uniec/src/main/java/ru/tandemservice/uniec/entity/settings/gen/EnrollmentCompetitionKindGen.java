package ru.tandemservice.uniec.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вида конкурса в рамках приемной кампании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentCompetitionKindGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind";
    public static final String ENTITY_NAME = "enrollmentCompetitionKind";
    public static final int VERSION_HASH = 275934144;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_COMPETITION_KIND = "competitionKind";
    public static final String P_PRIORITY = "priority";
    public static final String P_USED = "used";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private CompetitionKind _competitionKind;     // Вид конкурса
    private int _priority;     // Приоритет
    private boolean _used;     // Используется

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид конкурса.
     */
    public CompetitionKind getCompetitionKind()
    {
        return _competitionKind;
    }

    /**
     * @param competitionKind Вид конкурса.
     */
    public void setCompetitionKind(CompetitionKind competitionKind)
    {
        dirty(_competitionKind, competitionKind);
        _competitionKind = competitionKind;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsed()
    {
        return _used;
    }

    /**
     * @param used Используется. Свойство не может быть null.
     */
    public void setUsed(boolean used)
    {
        dirty(_used, used);
        _used = used;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentCompetitionKindGen)
        {
            setEnrollmentCampaign(((EnrollmentCompetitionKind)another).getEnrollmentCampaign());
            setCompetitionKind(((EnrollmentCompetitionKind)another).getCompetitionKind());
            setPriority(((EnrollmentCompetitionKind)another).getPriority());
            setUsed(((EnrollmentCompetitionKind)another).isUsed());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentCompetitionKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentCompetitionKind.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentCompetitionKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "competitionKind":
                    return obj.getCompetitionKind();
                case "priority":
                    return obj.getPriority();
                case "used":
                    return obj.isUsed();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "competitionKind":
                    obj.setCompetitionKind((CompetitionKind) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "used":
                    obj.setUsed((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "competitionKind":
                        return true;
                case "priority":
                        return true;
                case "used":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "competitionKind":
                    return true;
                case "priority":
                    return true;
                case "used":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "competitionKind":
                    return CompetitionKind.class;
                case "priority":
                    return Integer.class;
                case "used":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentCompetitionKind> _dslPath = new Path<EnrollmentCompetitionKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentCompetitionKind");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид конкурса.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#getCompetitionKind()
     */
    public static CompetitionKind.Path<CompetitionKind> competitionKind()
    {
        return _dslPath.competitionKind();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#isUsed()
     */
    public static PropertyPath<Boolean> used()
    {
        return _dslPath.used();
    }

    public static class Path<E extends EnrollmentCompetitionKind> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private CompetitionKind.Path<CompetitionKind> _competitionKind;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _used;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид конкурса.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#getCompetitionKind()
     */
        public CompetitionKind.Path<CompetitionKind> competitionKind()
        {
            if(_competitionKind == null )
                _competitionKind = new CompetitionKind.Path<CompetitionKind>(L_COMPETITION_KIND, this);
            return _competitionKind;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EnrollmentCompetitionKindGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind#isUsed()
     */
        public PropertyPath<Boolean> used()
        {
            if(_used == null )
                _used = new PropertyPath<Boolean>(EnrollmentCompetitionKindGen.P_USED, this);
            return _used;
        }

        public Class getEntityClass()
        {
            return EnrollmentCompetitionKind.class;
        }

        public String getEntityName()
        {
            return "enrollmentCompetitionKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
