/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDataCollect;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;
import java.util.List;
import java.util.TreeMap;

/**
 * @author Alexander Shaburov
 * @since 15.08.13
 */
public class EnrollmentDataCollectReportModel
{
    // поля активности фильтров отчета
    private boolean _compensTypeActive;
    private boolean _studentCategoryActive;
    private boolean _excludeParallelActive;

    // поля выбранных элементов в фильтрах отчета
    private EnrollmentCampaign _enrollmentCampaign;
    private Date _requestFrom;
    private Date _requestTo;
    private CompensationType _compensationType;
    private List<StudentCategory> _studentCategoryList;
    private DataWrapper _excludeParallel;
    private UniEduProgramEducationOrgUnitAddon _eduFiltersUtil;

    // поля для построения отчета
    private TreeMap<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> _wrapeprListMap;


    // Accessors

    public boolean isCompensTypeActive()
    {
        return _compensTypeActive;
    }

    public void setCompensTypeActive(boolean compensTypeActive)
    {
        _compensTypeActive = compensTypeActive;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isExcludeParallelActive()
    {
        return _excludeParallelActive;
    }

    public void setExcludeParallelActive(boolean excludeParallelActive)
    {
        _excludeParallelActive = excludeParallelActive;
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getRequestFrom()
    {
        return _requestFrom;
    }

    public void setRequestFrom(Date requestFrom)
    {
        _requestFrom = requestFrom;
    }

    public Date getRequestTo()
    {
        return _requestTo;
    }

    public void setRequestTo(Date requestTo)
    {
        _requestTo = requestTo;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public DataWrapper getExcludeParallel()
    {
        return _excludeParallel;
    }

    public void setExcludeParallel(DataWrapper excludeParallel)
    {
        _excludeParallel = excludeParallel;
    }

    public UniEduProgramEducationOrgUnitAddon getEduFiltersUtil()
    {
        return _eduFiltersUtil;
    }

    public void setEduFiltersUtil(UniEduProgramEducationOrgUnitAddon eduFiltersUtil)
    {
        _eduFiltersUtil = eduFiltersUtil;
    }

    public TreeMap<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> getWrapeprListMap()
    {
        return _wrapeprListMap;
    }

    public void setWrapeprListMap(TreeMap<TripletKey<EducationLevels, CompensationType, DevelopForm>, EnrollmentDataCollectReportSumData> wrapeprListMap)
    {
        _wrapeprListMap = wrapeprListMap;
    }
}
