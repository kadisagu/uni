/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import org.apache.commons.lang.mutable.MutableInt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Алгоритм выбора направлений из строки рейтинга в соответствии с планами
 *
 * @author Vasily Zhukov
 * @since 14.07.2011
 */
public class EcgQuotaManager implements IEcgQuotaManager
{
    private int _freeTotal;
    private Map<Long, MutableInt> _freeMap = new HashMap<>();
    private Map<Long, Map<Long, MutableInt>> _taFreeMap = new HashMap<>();

    public EcgQuotaManager(IEcgQuotaFreeDTO quotaFreeDTO)
    {
        for (Map.Entry<Long, Integer> entry : quotaFreeDTO.getFreeMap().entrySet())
        {
            int free = entry.getValue();
            _freeTotal += free;
            _freeMap.put(entry.getKey(), new MutableInt(free));
        }

        for (Map.Entry<Long, Map<Long, Integer>> mapEntry : quotaFreeDTO.getTaFreeMap().entrySet())
        {
            Map<Long, MutableInt> taMap = new HashMap<>();
            for (Map.Entry<Long, Integer> entry : mapEntry.getValue().entrySet())
            {
                int free = entry.getValue();
                taMap.put(entry.getKey(), new MutableInt(free));
            }
            _taFreeMap.put(mapEntry.getKey(), taMap);
        }

    }

    @Override
    public IEcgPlanChoiceResult getPossibleDirectionList(IEcgEntrantRateRowDTO rateRowDTO, Long preferableDirectionId)
    {
        List<IEcgEntrantRateDirectionDTO> list = rateRowDTO.getDirectionList();

        IEcgEntrantRateDirectionDTO chosenDirection = null;
        List<IEcgEntrantRateDirectionDTO> possibleDirectionList = new ArrayList<>();

        if (rateRowDTO.isTargetAdmission())
        {
            Long taKindId = rateRowDTO.getTargetAdmissionKind().getId();

            MutableInt preferableMutableQuota = null;
            MutableInt preferableMutableTaQuota = null;

            for (IEcgEntrantRateDirectionDTO direction : list)
            {
                MutableInt mutableQuota = _freeMap.get(direction.getDirectionId());
                MutableInt mutableTaQuota = _taFreeMap.get(direction.getDirectionId()).get(taKindId);

                //                if (null == mutableTaQuota) {
                //                    throw new ApplicationException(
                //                        "Нельзя добавить абитуриента «"+rateRowDTO.getFio()+"» в распределение, т.к. по виду целевого приема «"+rateRowDTO.getTargetAdmissionKind().getTitle()+"» нет свободных мест."
                //                    );
                //                }

                if (mutableQuota.intValue() > 0 && (null != mutableTaQuota && mutableTaQuota.intValue() > 0))
                {
                    possibleDirectionList.add(direction);

                    if (direction.getId().equals(preferableDirectionId))
                    {
                        chosenDirection = direction;
                        preferableMutableQuota = mutableQuota;
                        preferableMutableTaQuota = mutableTaQuota;
                    }
                }
            }

            if (chosenDirection == null)
            {
                if (!possibleDirectionList.isEmpty())
                {
                    chosenDirection = possibleDirectionList.get(0);
                    _freeMap.get(chosenDirection.getDirectionId()).decrement();
                    _taFreeMap.get(chosenDirection.getDirectionId()).get(taKindId).decrement();
                }
            }
            else
            {
                preferableMutableQuota.decrement();
                preferableMutableTaQuota.decrement();
            }
        }

        else

        {
            MutableInt preferableMutableQuota = null;

            for (IEcgEntrantRateDirectionDTO direction : list)
            {
                MutableInt mutableQuota = _freeMap.get(direction.getDirectionId());

                if (mutableQuota.intValue() > 0)
                {
                    possibleDirectionList.add(direction);

                    if (direction.getId().equals(preferableDirectionId))
                    {
                        chosenDirection = direction;
                        preferableMutableQuota = mutableQuota;
                    }
                }
            }

            if (chosenDirection == null)
            {
                if (!possibleDirectionList.isEmpty())
                {
                    chosenDirection = possibleDirectionList.get(0);
                    _freeMap.get(chosenDirection.getDirectionId()).decrement();
                }
            }
            else
            {
                preferableMutableQuota.decrement();
            }
        }

        return new EcgPlanChoiceResult(chosenDirection, possibleDirectionList);
    }

    @Override
    public int getFreeTotal()
    {
        return _freeTotal;
    }
}
