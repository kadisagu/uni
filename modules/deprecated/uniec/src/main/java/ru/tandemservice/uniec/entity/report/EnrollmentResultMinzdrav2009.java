package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.EnrollmentResultMinzdrav2009Gen;

/**
 * Результаты приема (по приказу №287 Минздравсоцразвития, по форме 2009 года)
 */
public class EnrollmentResultMinzdrav2009 extends EnrollmentResultMinzdrav2009Gen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}