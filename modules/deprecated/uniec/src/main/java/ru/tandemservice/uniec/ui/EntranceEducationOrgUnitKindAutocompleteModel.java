/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.ui;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Collection;
import java.util.Collections;

/**
 * @author agolubenko
 * @since 13.10.2008
 *
 * Используйте EnrollmentDirectionUtil
 */
public abstract class EntranceEducationOrgUnitKindAutocompleteModel extends OrgUnitKindAutocompleteModel
{
    private EnrollmentCampaign _enrollmentCampaign;
    private String _eduOrgUnitPropertyName;
    private String _educationOrgUnitFilterPropertyName;

    @Deprecated
    public EntranceEducationOrgUnitKindAutocompleteModel(String kindCode, String eduOrgUnitPropertyName, IPrincipalContext principalContext, EnrollmentCampaign enrollmentCampaign)
    {
        super(kindCode, principalContext);
        _enrollmentCampaign = enrollmentCampaign;
        _eduOrgUnitPropertyName = eduOrgUnitPropertyName;
    }

    public EntranceEducationOrgUnitKindAutocompleteModel setEducationOrgUnitFilterPropertyName(String educationOrgUnitFilterPropertyName)
    {
        _educationOrgUnitFilterPropertyName = educationOrgUnitFilterPropertyName;
        return this;
    }

    @Override
    public ListResult<OrgUnit> findValues(String filter)
    {
        if (_educationOrgUnitFilterPropertyName == null && getFilterOrgUnit() != null)
        {
            throw new RuntimeException("eduOrgUnitFilterPropertyName isn't specified, but filterOrgUnit object is not empty");
        }

        if (_educationOrgUnitFilterPropertyName != null && getFilterOrgUnit() == null)
        {
            return ListResult.getEmpty();
        }

        return UniecDAOFacade.getEntranceEduLevelDAO().getKindOrgUnitsList(filter, getKindCode(), _eduOrgUnitPropertyName, _educationOrgUnitFilterPropertyName, getFilterOrgUnit(), getFilterEntranceEducationOrgUnitIds(), getPrincipalContext(), _enrollmentCampaign, 100);
    }

    public abstract OrgUnit getFilterOrgUnit();

    public Collection<Long> getFilterEntranceEducationOrgUnitIds()
    {
        return Collections.emptyList();
    }
}
