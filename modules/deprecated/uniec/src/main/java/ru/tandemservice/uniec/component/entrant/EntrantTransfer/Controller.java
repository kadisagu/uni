/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantTransfer;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author agolubenko
 * @since 02.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<Entrant> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new RadioButtonColumn("select", "Выбрать абитуриента для перевода", true));
        dataSource.addColumn(new PublisherColumnBuilder("ФИО абитуриента", Entrant.P_FULLFIO, "entrantTab").subTab("entrantRequestTab").build());
        dataSource.addColumn(new SimpleColumn("Личный номер", Entrant.P_PERSONAL_NUMBER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", new String[] { Entrant.L_PERSON, Person.P_FULL_IDCARD_NUMBER }).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", Entrant.person().identityCard().birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        model.setDataSource(dataSource);
    }

    public void onClickProceed(IBusinessComponent component)
    {
        Model model = getModel(component);
        
        Entrant selectedEntrant = (Entrant) ((RadioButtonColumn) model.getDataSource().getColumn(0)).getSelectedEntity();
        if (selectedEntrant == null)
        {
            throw new ApplicationException("Необходимо выбрать абитуриента для перевода.");
        }

        model.setEntrant(selectedEntrant);
        activate(component, new ComponentActivator(IUniecComponents.ENTRANT_TRANSFER_DIRECTION_SELECTION));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DynamicListDataSource<Entrant> dataSource = getModel(component).getDataSource();
        dataSource.showFirstPage();
        dataSource.refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        
        model.setLastNameFilter(null);
        model.setFirstNameFilter(null);
        model.setMiddleNameFilter(null);
        
        onClickSearch(component);
    }
}
