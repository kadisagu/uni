package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.catalog.EnrollmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранное преимущество при равенстве конкурсных баллов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantEnrolmentRecommendationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation";
    public static final String ENTITY_NAME = "entrantEnrolmentRecommendation";
    public static final int VERSION_HASH = -539125468;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String L_RECOMMENDATION = "recommendation";

    private Entrant _entrant;     // (Старый) Абитуриент
    private EnrollmentRecommendation _recommendation;     // Преимущество при равенстве конкурсных баллов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Преимущество при равенстве конкурсных баллов. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentRecommendation getRecommendation()
    {
        return _recommendation;
    }

    /**
     * @param recommendation Преимущество при равенстве конкурсных баллов. Свойство не может быть null.
     */
    public void setRecommendation(EnrollmentRecommendation recommendation)
    {
        dirty(_recommendation, recommendation);
        _recommendation = recommendation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantEnrolmentRecommendationGen)
        {
            setEntrant(((EntrantEnrolmentRecommendation)another).getEntrant());
            setRecommendation(((EntrantEnrolmentRecommendation)another).getRecommendation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantEnrolmentRecommendationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantEnrolmentRecommendation.class;
        }

        public T newInstance()
        {
            return (T) new EntrantEnrolmentRecommendation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "recommendation":
                    return obj.getRecommendation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "recommendation":
                    obj.setRecommendation((EnrollmentRecommendation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "recommendation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "recommendation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "recommendation":
                    return EnrollmentRecommendation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantEnrolmentRecommendation> _dslPath = new Path<EntrantEnrolmentRecommendation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantEnrolmentRecommendation");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Преимущество при равенстве конкурсных баллов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation#getRecommendation()
     */
    public static EnrollmentRecommendation.Path<EnrollmentRecommendation> recommendation()
    {
        return _dslPath.recommendation();
    }

    public static class Path<E extends EntrantEnrolmentRecommendation> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private EnrollmentRecommendation.Path<EnrollmentRecommendation> _recommendation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Преимущество при равенстве конкурсных баллов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation#getRecommendation()
     */
        public EnrollmentRecommendation.Path<EnrollmentRecommendation> recommendation()
        {
            if(_recommendation == null )
                _recommendation = new EnrollmentRecommendation.Path<EnrollmentRecommendation>(L_RECOMMENDATION, this);
            return _recommendation;
        }

        public Class getEntityClass()
        {
            return EntrantEnrolmentRecommendation.class;
        }

        public String getEntityName()
        {
            return "entrantEnrolmentRecommendation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
