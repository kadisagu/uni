/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcOrder.logic;

import java.util.Map;

import ru.tandemservice.uniec.component.enrollmentextract.ICommonEnrollmentExtractDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author Vasily Zhukov
 * @since 25.05.2011
 */
public class EcExtractComponentDao implements IExtractComponentDao<EnrollmentExtract>
{
    @Override
    public void doCommit(EnrollmentExtract extract, Map parameters)
    {
        ICommonEnrollmentExtractDao.instance.get().doCommit(extract);
    }

    @Override
    public void doRollback(EnrollmentExtract extract, Map parameters)
    {
        ICommonEnrollmentExtractDao.instance.get().doRollback(extract);
    }
}
