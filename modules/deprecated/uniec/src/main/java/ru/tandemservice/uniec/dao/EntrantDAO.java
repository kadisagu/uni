/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.*;
import org.hibernate.action.BeforeTransactionCompletionProcess;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.ActionQueue;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.impl.SessionImpl;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.meta.entity.impl.FieldPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniec.util.MarkStatistic;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.io.*;
import java.sql.Blob;
import java.sql.Clob;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since 08.10.2008
 */
@SuppressWarnings("deprecation")
public class EntrantDAO extends UniBaseDao implements IEntrantDAO
{
    @Override
    public List<ExamPassMark> getExamPassMarkList(Entrant entrant)
    {
        boolean formingForEntrant = entrant.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);
        MQBuilder builder = new MQBuilder(ExamPassMark.ENTITY_CLASS, "m");
        MQBuilder innerBuilder = new MQBuilder(EntrantExamList.ENTITY_CLASS, "e", new String[]{EntrantExamList.P_ID});
        innerBuilder.add(formingForEntrant ? MQExpression.eq("e", EntrantExamList.L_ENTRANT + ".id", entrant.getId()) : MQExpression.in("e", EntrantExamList.L_ENTRANT + ".id", new MQBuilder(EntrantRequest.ENTITY_CLASS, "r", new String[]{EntrantRequest.P_ID}).add(MQExpression.eq("r", EntrantRequest.L_ENTRANT, entrant))));
        builder.add(MQExpression.in("m", ExamPassMark.L_EXAM_PASS_DISCIPLINE + "." + ExamPassDiscipline.L_ENTRANT_EXAM_LIST + ".id", innerBuilder));
        return builder.getResultList(getSession());
    }

    @Override
    public List<ChosenEntranceDiscipline> getChosenEntranceDisciplineList(Entrant entrant)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrant));
        return builder.getResultList(getSession());
    }

    @Override
    public Map<EntrantExamList, Map<Discipline2RealizationWayRelation, Set<ExamPassMark>>> getExamPassMarkMap(List<ExamPassMark> examPassMarkList)
    {
        Map<EntrantExamList, Map<Discipline2RealizationWayRelation, Set<ExamPassMark>>> result = new HashMap<>();
        for (ExamPassMark mark : examPassMarkList)
        {
            EntrantExamList entrantExamList = mark.getExamPassDiscipline().getEntrantExamList();
            Discipline2RealizationWayRelation discipline = mark.getExamPassDiscipline().getEnrollmentCampaignDiscipline();

            Map<Discipline2RealizationWayRelation, Set<ExamPassMark>> discipline2marks = result.get(entrantExamList);
            if (discipline2marks == null)
                result.put(entrantExamList, discipline2marks = new HashMap<>());

            Set<ExamPassMark> marks = discipline2marks.get(discipline);
            if (marks == null)
                discipline2marks.put(discipline, marks = new HashSet<>());
            marks.add(mark);
        }
        return result;
    }

    @Override
    public Map<EntrantRequest, Set<ChosenEntranceDiscipline>> getChosenEntranceDisciplineMap(Entrant entrant)
    {
        Map<EntrantRequest, Set<ChosenEntranceDiscipline>> result = new HashMap<>();

        for (ChosenEntranceDiscipline chosenEntranceDiscipline : getChosenEntranceDisciplineList(entrant))
        {
            EntrantRequest entrantRequest = chosenEntranceDiscipline.getChosenEnrollmentDirection().getEntrantRequest();
            Set<ChosenEntranceDiscipline> set = result.get(entrantRequest);
            if (set == null)
                result.put(entrantRequest, set = new HashSet<>());
            set.add(chosenEntranceDiscipline);
        }
        return result;
    }

    @Override
    public Map<EntrantRequest, EntrantExamList> getEntrantExamListMap(Entrant entrant)
    {
        Map<EntrantRequest, EntrantExamList> result = new HashMap<>();
        boolean formingForEntrant = entrant.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);
        if (formingForEntrant)
            return result;

        MQBuilder builder = new MQBuilder(EntrantExamList.ENTITY_CLASS, "e");
        builder.add(MQExpression.in("e", EntrantExamList.L_ENTRANT + ".id", new MQBuilder(EntrantRequest.ENTITY_CLASS, "r", new String[]{EntrantRequest.P_ID}).add(MQExpression.eq("r", EntrantRequest.L_ENTRANT, entrant))));
        List<EntrantExamList> entrantExamListList = builder.getResultList(getSession());
        for (EntrantExamList entrantExamList : entrantExamListList)
            result.put((EntrantRequest) entrantExamList.getEntrant(), entrantExamList);
        return result;
    }

    @Override
    public Map<ChosenEntranceDiscipline, EntrantExamList> getEntrantExamLists(Entrant entrant)
    {
        Map<ChosenEntranceDiscipline, EntrantExamList> result = new HashMap<>();
        boolean formingForEntrant = entrant.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);

        if (formingForEntrant)
        {
            EntrantExamList entrantExamList = get(EntrantExamList.class, EntrantExamList.L_ENTRANT, entrant);
            if (entrantExamList != null)
            {
                // все выбранные дисциплины относятся к одному экзам. листу
                for (ChosenEntranceDiscipline chosenEntranceDiscipline : getChosenEntranceDisciplineList(entrant))
                    result.put(chosenEntranceDiscipline, entrantExamList);
            }
        } else
        {
            // для каждого заявления получаем список выбранных дисциплин приемной кампании
            Map<EntrantRequest, Set<ChosenEntranceDiscipline>> chosenEntranceDisciplineMap = getChosenEntranceDisciplineMap(entrant);

            // для каждого заявления получаем экзаменационный лист
            Map<EntrantRequest, EntrantExamList> entrantExamListMap = getEntrantExamListMap(entrant);

            for (Map.Entry<EntrantRequest, EntrantExamList> entry : entrantExamListMap.entrySet())
            {
                Set<ChosenEntranceDiscipline> chosenEntranceDisciplineSet = chosenEntranceDisciplineMap.get(entry.getKey());
                if (chosenEntranceDisciplineSet != null)
                    for (ChosenEntranceDiscipline chosenEntranceDiscipline : chosenEntranceDisciplineSet)
                        result.put(chosenEntranceDiscipline, entry.getValue());
            }
        }
        return result;
    }

    @Override
    public Map<StateExamSubject, Set<StateExamSubjectMark>> getStateExamMarks(Entrant entrant)
    {
        Map<StateExamSubject, Set<StateExamSubjectMark>> result = new HashMap<>();
        MQBuilder builder = new MQBuilder(StateExamSubjectMark.ENTITY_CLASS, "m");
        builder.add(MQExpression.eq("m", StateExamSubjectMark.L_CERTIFICATE + "." + EntrantStateExamCertificate.L_ENTRANT, entrant));
        List<StateExamSubjectMark> markList = builder.getResultList(getSession());
        for (StateExamSubjectMark mark : markList)
        {
            Set<StateExamSubjectMark> markSet = result.get(mark.getSubject());
            if (markSet == null)
                result.put(mark.getSubject(), markSet = new HashSet<>());
            markSet.add(mark);
        }
        return result;
    }

    @Override
    public Map<Discipline2RealizationWayRelation, ConversionScale> getConversionScales(EnrollmentCampaign enrollmentCampaign)
    {
        Map<Discipline2RealizationWayRelation, ConversionScale> result = new HashMap<>();
        MQBuilder scaleBuilder = new MQBuilder(ConversionScale.ENTITY_CLASS, "c");
        scaleBuilder.add(MQExpression.eq("c", ConversionScale.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        List<ConversionScale> conversionScaleList = scaleBuilder.getResultList(getSession());
        for (ConversionScale conversionScale : conversionScaleList)
            result.put(conversionScale.getDiscipline(), conversionScale);
        return result;
    }

    @Override
    public Map<ExamPassMark, ExamPassMarkAppeal> getExamPassMarkAppeals(List<ExamPassMark> examPassMarkList)
    {
        Map<ExamPassMark, ExamPassMarkAppeal> result = new HashMap<>();
        if (!examPassMarkList.isEmpty())
        {
            MQBuilder builder = new MQBuilder(ExamPassMarkAppeal.ENTITY_CLASS, "a");
            builder.add(MQExpression.in("a", ExamPassMarkAppeal.L_EXAM_PASS_MARK, examPassMarkList));
            List<ExamPassMarkAppeal> examPassMarkAppealList = builder.getResultList(getSession());
            for (ExamPassMarkAppeal appeal : examPassMarkAppealList)
                result.put(appeal.getExamPassMark(), appeal);
        }
        return result;
    }

    @Override
    public Set<MultiKey> getRealizationFormSet(EnrollmentCampaign enrollmentCampaign)
    {
        boolean differenceExist = enrollmentCampaign.isEnrollmentPerCompTypeDiff();

        MQBuilder builder = new MQBuilder(Discipline2RealizationFormRelation.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        if (!differenceExist)
            builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_TYPE + "." + CompensationType.P_CODE, UniDefines.COMPENSATION_TYPE_BUDGET));
        List<Discipline2RealizationFormRelation> list = builder.getResultList(getSession());

        Set<MultiKey> result = new HashSet<>();
        if (differenceExist)
        {
            for (Discipline2RealizationFormRelation item : list)
                result.add(new MultiKey(item.getDiscipline(), item.getSubjectPassForm(), item.getType()));
        } else
        {
            for (Discipline2RealizationFormRelation item : list)
                result.add(new MultiKey(item.getDiscipline(), item.getSubjectPassForm()));
        }
        return result;
    }

    @Override
    public Double getSumFinalMarks(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        Criteria criteria = getSession().createCriteria(ChosenEntranceDiscipline.class);
        criteria.add(Restrictions.eq(ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, requestedEnrollmentDirection));
        criteria.setProjection(Projections.sum(ChosenEntranceDiscipline.P_FINAL_MARK));
        Number number = (Number) criteria.uniqueResult();
        return number == null ? null : number.doubleValue();
    }

    @Override
    public List<PreliminaryEnrollmentStudent> getPreliminaryEnrollmentStudentList(Entrant entrant)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrant));
        builder.addOrder("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY + ".code");
        return builder.getResultList(getSession());
    }

    @Override
    public synchronized org.tandemframework.core.CoreCollectionUtils.Pair<String, PreliminaryEnrollmentStudent> saveOrUpdatePreliminaryStudent(PreliminaryEnrollmentStudent student)
    {
        // нам нужно проверять других студентов предзачисления абитуриента
        // заблокировать на уровне базы мы не можем, потому что всю таблицу не умеем,
        // а блокировки набора записей недостаточно -
        // могут добавить нового студента предзачисления в число студентов,
        // после проверки и до внесения изменений
        // поэтому сохранение и изменение студентов предзачисления надо делать только через этот метод,
        // потому что он синхронизирован

        Session session = getSession();

        // в выбранном направлении могут что-нибудь поменять, поэтому блокируем и обновляем его
        RequestedEnrollmentDirection direction = student.getRequestedEnrollmentDirection();
        session.lock(direction, LockMode.UPGRADE);
        session.refresh(direction);

        Entrant entrant = direction.getEntrantRequest().getEntrant();
        String fio = entrant.getPerson().getFullFio();

        if (entrant.getEnrollmentCampaign().isNeedOriginDocForPreliminary())
        {
            if (student.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_STUDENT) && !direction.isOriginalDocumentHandedIn())
                return new org.tandemframework.core.CoreCollectionUtils.Pair<>("Абитуриент «" + fio + "» не сдал оригиналы документов, поэтому нельзя провести предварительное зачисление абитуриента в число студентов.", null);

            Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class);
            criteria.createAlias(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
            criteria.add(Restrictions.eq("entrantRequest." + EntrantRequest.L_ENTRANT, entrant));
            criteria.add(Restrictions.eq(RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
            criteria.setLockMode(LockMode.UPGRADE);

            if (criteria.list().isEmpty())
                return new org.tandemframework.core.CoreCollectionUtils.Pair<>("Абитуриент «" + fio + "» не сдал оригиналы документов, поэтому нельзя провести предварительное зачисление абитуриента в число студентов.", null);
        }

        // проверка, что еще нет предзачисления, как студент
        if (student.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_STUDENT))
        {
            MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrant));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY + ".code", UniDefines.STUDENT_CATEGORY_STUDENT));
            if (student.getId() != null)
                builder.add(MQExpression.notEq("p", "id", student.getId()));
            PreliminaryEnrollmentStudent otherStudent = (PreliminaryEnrollmentStudent) builder.uniqueResult(session);
            if (otherStudent != null)
            {
                String directionTitle = otherStudent.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle();
                return new org.tandemframework.core.CoreCollectionUtils.Pair<>("Абитуриент «" + fio + "» уже предварительно зачислен в число студентов по направлению «" + directionTitle + "».", null);
            }
        }

        // проверка, что еще нет предзачисления на бюджет
        if (student.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET))
        {
            MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrant));
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + ".code", UniDefines.COMPENSATION_TYPE_BUDGET));
            if (student.getId() != null)
                builder.add(MQExpression.notEq("p", "id", student.getId()));
            PreliminaryEnrollmentStudent otherStudent = (PreliminaryEnrollmentStudent) builder.uniqueResult(session);
            if (otherStudent != null)
            {
                String directionTitle = otherStudent.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle();
                return new org.tandemframework.core.CoreCollectionUtils.Pair<>("Абитуриент «" + fio + "» уже предварительно зачислен на бюджет по направлению «" + directionTitle + "».", null);
            }
        }

        // на направление подготовки подразделения для этого абитуриента - только один раз
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrant));
        builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, student.getEducationOrgUnit()));
        if (student.getId() != null)
            builder.add(MQExpression.notEq("p", "id", student.getId()));
        if (builder.getResultCount(session) != 0)
            return new org.tandemframework.core.CoreCollectionUtils.Pair<>("Абитуриент «" + fio + "» уже предварительно зачислен в число студентов по данному направлению.", null);

        if(entrant.getEnrollmentCampaign().isRequiredAgreement4PreEnrollment() && !direction.isAgree4Enrollment())
            return new org.tandemframework.core.CoreCollectionUtils.Pair<>("Абитуриент «" + fio + "»  не предоставил согласие на зачисление по данному направлению приема, поэтому нельзя провести предварительное зачисление абитуриента в число студентов.", null);

        PreliminaryEnrollmentStudent newStudent = new PreliminaryEnrollmentStudent();
        newStudent.update(student);
        if (student.getId() != null)
        {
            session.delete(student);
            session.flush(); // как обычно, хотим удалить старого до создания нового
        }
        session.saveOrUpdate(newStudent);

        return new org.tandemframework.core.CoreCollectionUtils.Pair<>(null, newStudent);
    }

    @Override
    public void updateEntrantData(Collection<Long> entrantIds)
    {
        Map<EnrollmentCampaign, Set<Entrant>> campaign2entrantSet = new HashMap<>();
        for (Long entrantId : entrantIds)
        {
            Entrant entrant = (Entrant) getSession().get(Entrant.class, entrantId);
            if (entrant != null)
            {
                Set<Entrant> set = campaign2entrantSet.get(entrant.getEnrollmentCampaign());
                if (set == null)
                    campaign2entrantSet.put(entrant.getEnrollmentCampaign(), set = new HashSet<>());
                set.add(entrant);
            }
        }

        for (Map.Entry<EnrollmentCampaign, Set<Entrant>> entry : campaign2entrantSet.entrySet())
        {
            final EnrollmentCampaign enrollmentCampaign = entry.getKey();
            BatchUtils.execute(entry.getValue(), 1000, new BatchUtils.Action<Entrant>()
            {
                @Override
                public void execute(Collection<Entrant> elements)
                {
                    updateEntrantDataFast(enrollmentCampaign, elements);
                }
            });
        }
    }

    @Override
    public Map<Long, Double> getDirectionFinalMarks(final EnrollmentCampaign enrollmentCampaign, Set<Long> directionIds)
    {
        final Map<Long, Double> result = new HashMap<>();
        BatchUtils.execute(directionIds, 2000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
                builder.add(MQExpression.in("d", RequestedEnrollmentDirection.P_ID, elements));

                EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), enrollmentCampaign, builder);
                for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
                    result.put(direction.getId(), dataUtil.getFinalMark(direction));
            }
        });
        return result;
    }

    @Override
    public EnrollmentCampaign getLastEnrollmentCampaign()
    {
        Criteria criteria = getSession().createCriteria(EnrollmentCampaign.class);
        criteria.addOrder(Order.desc(EnrollmentCampaign.id().s()));
        criteria.setMaxResults(1);
        return (EnrollmentCampaign) criteria.uniqueResult();
    }

    @Override
    public void deleteExamPassDiscipline(ExamPassDiscipline examPassDiscipline)
    {
        IEntrant ientrant = examPassDiscipline.getEntrantExamList().getEntrant();
        Entrant entrant = ientrant instanceof Entrant ? (Entrant) ientrant : ((EntrantRequest) ientrant).getEntrant();
        deleteExamPassDisciplineSet(entrant, Collections.singletonList(examPassDiscipline));
    }

    @Override
    public void deleteEntrant(Entrant entrant)
    {
        Session session = getSession();

        // если у абитуриента есть заявления, то удалять нельзя
        List<EntrantRequest> requestList = getList(EntrantRequest.class, EntrantRequest.entrant().s(), entrant);
        if (!requestList.isEmpty())
            throw new ApplicationException("Нельзя удалять абитуриента с заявлениями.");

        // нет заявлений -> удаляем все дисциплины для сдачи
        // если экзам лист формируется на заявление, то дисциплин для сдачи нет, т.к. нет заявлений и удалять нечего
        if (entrant.getEnrollmentCampaign().isExamListsFormingForEntrant())
        {
            MQBuilder builder = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d");
            builder.add(MQExpression.eq("d", ExamPassDiscipline.entrantExamList().entrant().s(), entrant));
            deleteExamPassDisciplineSet(entrant, builder.<ExamPassDiscipline>getResultList(session));
        }

        // обнуляем ссылку в онлайн-абитуриенте, если необходимо
        // когда появится cascade="orphan" заюзать его
        OnlineEntrant onlineEntrant = get(OnlineEntrant.class, OnlineEntrant.entrant().s(), entrant);
        if (onlineEntrant != null)
        {
            onlineEntrant.setEntrant(null);
            session.update(onlineEntrant);
        }

        // удаляем абитуриента через апи функцию удаления категории персоны
        PersonManager.instance().dao().deletePersonRole(entrant.getId());
    }

    // удаляет дисциплины для сдачи у заданного абитуриента

    private void deleteExamPassDisciplineSet(Entrant entrant, Collection<ExamPassDiscipline> examPassDisciplineSet)
    {
        if (examPassDisciplineSet.isEmpty()) return;

        Session session = getSession();
        EnrollmentCampaign enrollmentCampaign = entrant.getEnrollmentCampaign();

        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.entrantRequest().entrant().s(), entrant));
        EntrantDataUtil util = new EntrantDataUtil(session, enrollmentCampaign, builder);

        Set<ExamPassDiscipline> actualExamPassSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
            for (ChosenEntranceDiscipline chosen : util.getChosenEntranceDisciplineSet(direction))
                actualExamPassSet.addAll(util.getExamPassDisciplineSet(chosen));

        Set<ExamPassDiscipline> passDisciplinesWithMarks = new HashSet<>();
        for (ExamPassMark examPassMark : getExamPassMarkList(entrant))
            passDisciplinesWithMarks.add(examPassMark.getExamPassDiscipline());

        for (ExamPassDiscipline examPassDiscipline : examPassDisciplineSet)
        {
            if (actualExamPassSet.contains(examPassDiscipline))
            {
                // дисциплина для сдачи актуальна, можно ее удалять если нет оценок
                if (passDisciplinesWithMarks.contains(examPassDiscipline))
                    throw new ApplicationException("Нельзя удалять актуальную дисциплину для сдачи с оценкой.");
                session.delete(examPassDiscipline);
            } else
            {
                // неактуальную дисциплину для сдачи надо удалять со всеми оценками
                List<ExamPassMark> markList = getList(ExamPassMark.class, ExamPassMark.examPassDiscipline().s(), examPassDiscipline);
                List<ExamPassMarkAppeal> appelList = getList(ExamPassMarkAppeal.class, ExamPassMarkAppeal.examPassMark().s(), markList);
                for (ExamPassMarkAppeal item : appelList) session.delete(item);
                for (ExamPassMark item : markList) session.delete(item);
                session.delete(examPassDiscipline);
            }
        }
    }

    /**
     * Получение зачетного балла по выбранной дисциплине и направлению приема.
     * Метод вынесен отдельно для переопределения в проектном слое ДВФУ (DEV-2894), для учета повышенных зачетных баллов.
     *
     * @param chosen Выбранное вступительное испытание
     * @param direction Выбранное направление приема
     * @return зачетный балл по дисциплине, который необходимо набрать
     */
    protected double getPassMarkForDiscipline(ChosenEntranceDiscipline chosen, RequestedEnrollmentDirection direction)
    {
        Double passMarkFromEntranceDiscipline = getPassMarkFromEntranceDiscipline(chosen, direction);
        if (null != passMarkFromEntranceDiscipline) return passMarkFromEntranceDiscipline;
        return chosen.getEnrollmentCampaignDiscipline().getPassMark();
    }

    protected Double getPassMarkFromEntranceDiscipline(ChosenEntranceDiscipline chosen, RequestedEnrollmentDirection direction)
    {
        List<Group2DisciplineRelation> groups = new DQLSelectBuilder().fromEntity(Group2DisciplineRelation.class, "dg")
                .where(eq(property(Group2DisciplineRelation.discipline().id().fromAlias("dg")), value(chosen.getEnrollmentCampaignDiscipline().getId())))
                .column(property(Group2DisciplineRelation.group().fromAlias("dg")))
                .createStatement(getSession()).list();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "e").column("e")
                .where(eq(property(EntranceDiscipline.enrollmentDirection().fromAlias("e")), value(direction.getEnrollmentDirection())))
                .where(eq(property(EntranceDiscipline.studentCategory().fromAlias("e")), value(chosen.getChosenEnrollmentDirection().getStudentCategory())));

        if (groups.isEmpty())
        {
            builder.where(eq(property(EntranceDiscipline.discipline().fromAlias("e")), value(chosen.getEnrollmentCampaignDiscipline())));
        } else
        {
            builder.where(eq(property(EntranceDiscipline.discipline().fromAlias("e")), value(groups.get(0))));
        }

        if (direction.getEnrollmentDirection().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
        {
            if (chosen.getChosenEnrollmentDirection().getCompensationType().isBudget())
                builder.where(eq(property(EntranceDiscipline.budget().fromAlias("e")), value(Boolean.TRUE)));
            else
                builder.where(eq(property(EntranceDiscipline.contract().fromAlias("e")), value(Boolean.TRUE)));
        }

        List<EntranceDiscipline> entranceDisciplineList = builder.createStatement(getSession()).list();

        if (!entranceDisciplineList.isEmpty())
        {
            if (direction.getEnrollmentDirection().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
            {
                if (chosen.getChosenEnrollmentDirection().getCompensationType().isBudget() && null != entranceDisciplineList.get(0).getPassMark())
                    return entranceDisciplineList.get(0).getPassMark();

                if (!chosen.getChosenEnrollmentDirection().getCompensationType().isBudget() && null != entranceDisciplineList.get(0).getPassMarkContract())
                    return entranceDisciplineList.get(0).getPassMarkContract();
            } else
            {
                if (null != entranceDisciplineList.get(0).getPassMark())
                    return entranceDisciplineList.get(0).getPassMark();
            }
        }

        return null;
    }

    // мегометод, который все пересчитывает

    private void updateEntrantDataFast(EnrollmentCampaign enrollmentCampaign, Collection<Entrant> entrants)
    {
        final Session session = getSession();

        // П Е Р Е С Ч И Т Ы В А Е М   В С Е   Ф И Н А Л Ь Н Ы Е   О Ц Е Н К И

        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "direction");
        builder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, entrants));

        // fetch data
        EntrantDataUtil dataUtil = new EntrantDataUtil(session, enrollmentCampaign, builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);
        final int DISCIPLINE_SOURCE_INDEX = 0;
        final int APPEAL_SOURCE_INDEX = 1;
        Map<String, int[]> form2markSource = new HashMap<>();
        form2markSource.put(UniecDefines.SUBJECT_PASS_FORM_EXAM, new int[]{UniecDefines.FINAL_MARK_DISCIPLINE_EXAM, UniecDefines.FINAL_MARK_APPEAL_EXAM});
        form2markSource.put(UniecDefines.SUBJECT_PASS_FORM_TEST, new int[]{UniecDefines.FINAL_MARK_DISCIPLINE_TEST, UniecDefines.FINAL_MARK_APPEAL_TEST});
        form2markSource.put(UniecDefines.SUBJECT_PASS_FORM_INTERVIEW, new int[]{UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW, UniecDefines.FINAL_MARK_APPEAL_INTERVIEW});
        form2markSource.put(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL, new int[]{UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL, UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL});

        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> direction2examSetDataMap = ExamSetUtil.getDirectionExamSetDataMap(getSession(), enrollmentCampaign, null, null);
        final double NO_FINAL_MARK = -1;
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
            {
                double finalMark = NO_FINAL_MARK;
                Integer finalMarkSource = null;

                MarkStatistic markStatistic = dataUtil.getMarkStatistic(chosen);

                // в финальной оценке учитываем результаты ЕГЭ
                if (markStatistic.getScaledStateExamMark() != null && markStatistic.getScaledStateExamMark() > finalMark)
                {
                    finalMark = markStatistic.getScaledStateExamMark();
                    String waveCode = markStatistic.getStateExamMark().getCertificate().getStateExamType().getCode();
                    finalMarkSource = UniecDefines.STATE_EXAM_TYPE_FIRST.equals(waveCode) ? UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 : UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2;
                }

                // в финальной оценке учитываем дипломы участника олимпиад
                if (markStatistic.getOlympiad() != null && markStatistic.getOlympiad() > finalMark)
                {
                    finalMark = markStatistic.getOlympiad();
                    finalMarkSource = UniecDefines.FINAL_MARK_OLYMPIAD;
                }

                // учитываем оценки по дисциплинам
                for (Map.Entry<SubjectPassForm, PairKey<ExamPassMark, ExamPassMarkAppeal>> entry : markStatistic.getInternalMarkMap().entrySet())
                {
                    PairKey<ExamPassMark, ExamPassMarkAppeal> pairMark = entry.getValue();

                    if (pairMark != null)
                    {
                        if (pairMark.getSecond() != null)
                        {
                            if (pairMark.getSecond().getMark() > finalMark)
                            {
                                finalMark = pairMark.getSecond().getMark();
                                finalMarkSource = form2markSource.get(entry.getKey().getCode())[APPEAL_SOURCE_INDEX];
                            }
                        } else
                        {
                            if (pairMark.getFirst().getMark() > finalMark)
                            {
                                finalMark = pairMark.getFirst().getMark();
                                finalMarkSource = form2markSource.get(entry.getKey().getCode())[DISCIPLINE_SOURCE_INDEX];
                            }
                        }
                    }
                }

                // финальный балл, наконец-то, посчитан
                chosen.setFinalMark(finalMark == NO_FINAL_MARK ? null : finalMark);
                chosen.setFinalMarkSource(finalMarkSource);
                session.update(chosen);
            }

            // обновляем ссылку на профильное испытание

            // получаем информацию по наборам экзаменов для направления приема
            EnrollmentDirectionExamSetData examSetData = direction2examSetDataMap.get(direction.getEnrollmentDirection());

            // получаем структуру набора экзамена для текущего выбранного направления приема
            ExamSetStructure examSetStructure = examSetData.getCategoryExamSetStructure(direction.getStudentCategory());

            // получаем список строк структуры набора экзамена
            List<ExamSetStructureItem> itemList = examSetStructure.getItemList();

            int i = 0;
            while (i < itemList.size() && !itemList.get(i).isProfile()) i++;
            if (i < itemList.size())
            {
                // находим наилучшее распределение выбранных вступительных испытаний по строкам структуры набора
                ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getChosenDistribution(dataUtil.getChosenEntranceDisciplineSet(direction), itemList);
                direction.setProfileChosenEntranceDiscipline(distribution[i]);
                update(direction);
            }
        }

        // П Е Р Е С Ч И Т Ы В А Е М   О Р И Г И Н А Л Ы   Д О К У М Е Н Т О В

        MQBuilder originalBuilder = new MQBuilder(EntrantOriginalDocumentRelation.ENTITY_CLASS, "o");
        originalBuilder.add(MQExpression.in("o", EntrantOriginalDocumentRelation.L_REQUESTED_ENROLLMENT_DIRECTION, builder));

        // абитуриент сдал оригиналы по этим направлениям
        Map<Entrant, RequestedEnrollmentDirection> originalMap = new HashMap<>();
        for (EntrantOriginalDocumentRelation item : originalBuilder.<EntrantOriginalDocumentRelation>getResultList(session))
            originalMap.put(item.getEntrant(), item.getRequestedEnrollmentDirection());

        // абитуриент забрал документы по этим заявлениям
        Set<EntrantRequest> awayEntrantRequestSet = new HashSet<>();
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
            if (direction.getEntrantRequest().isTakeAwayDocument())
                awayEntrantRequestSet.add(direction.getEntrantRequest());

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            EntrantRequest entrantRequest = direction.getEntrantRequest();
            Entrant entrant = entrantRequest.getEntrant();

            if (awayEntrantRequestSet.contains(entrantRequest))
            {
                direction.setOriginalDocumentHandedIn(false);
            } else
            {
                RequestedEnrollmentDirection original = originalMap.get(entrant);
                if (original == null)
                {
                    direction.setOriginalDocumentHandedIn(false);
                } else
                {
                    if (enrollmentCampaign.isOriginalDocumentPerDirection())
                    {
                        CompetitionGroup competitionGroup = original.getEnrollmentDirection().getCompetitionGroup();
                        if (competitionGroup == null)
                        {
                            direction.setOriginalDocumentHandedIn(direction.equals(original));
                        } else
                        {
                            direction.setOriginalDocumentHandedIn(competitionGroup.equals(direction.getEnrollmentDirection().getCompetitionGroup()) && original.getCompensationType().equals(direction.getCompensationType()));
                        }
                    } else
                    {
                        direction.setOriginalDocumentHandedIn(original.getEntrantRequest().equals(direction.getEntrantRequest()));
                    }
                }
            }
        }

        // П Е Р Е С Ч И Т Ы В А Е М   С О С Т О Я Н И Я   В Ы Б Р А Н Н Ы Х   Н А П Р А В Л Е Н И Й

        // fetch data
        Map<String, EntrantState> code2state = new HashMap<>();
        for (EntrantState state : getCatalogItemList(EntrantState.class)) code2state.put(state.getCode(), state);

        Set<RequestedEnrollmentDirection> preEnrolledSet = new HashSet<>();
        Map<RequestedEnrollmentDirection, String> direction2extractState = new HashMap<>();
        Map<RequestedEnrollmentDirection, InterviewResult> direction2interview = new HashMap<>();

        MQBuilder preEnrolledBuilder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION});
        preEnrolledBuilder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, builder));
        preEnrolledSet.addAll(preEnrolledBuilder.<RequestedEnrollmentDirection>getResultList(session));

        MQBuilder extractBuilder = new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, IAbstractExtract.L_STATE + "." + ExtractStates.P_CODE});
        extractBuilder.add(MQExpression.in("e", IAbstractExtract.L_ENTITY + "." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, builder));
        for (Object[] row : extractBuilder.<Object[]>getResultList(session))
            direction2extractState.put((RequestedEnrollmentDirection) row[0], (String) row[1]);

        MQBuilder interviewBuilder = new MQBuilder(InterviewResult.ENTITY_CLASS, "i");
        interviewBuilder.add(MQExpression.in("i", InterviewResult.L_REQUESTED_ENROLLMENT_DIRECTION, builder));
        for (InterviewResult interview : interviewBuilder.<InterviewResult>getResultList(session))
            direction2interview.put(interview.getRequestedEnrollmentDirection(), interview);

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            if (direction.getEntrantRequest().isTakeAwayDocument())
            {
                direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            } else
            {
                String stateCode = direction2extractState.get(direction);
                if (stateCode != null)
                {
                    boolean finished = UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(stateCode);
                    direction.setState(code2state.get(finished ? UniecDefines.ENTRANT_STATE_ENROLED_CODE : UniecDefines.ENTRANT_STATE_IN_ORDER));
                } else if (preEnrolledSet.contains(direction))
                {
                    direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE));
                } else if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(direction.getCompetitionKind().getCode()))
                {
                    direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE));
                } else if (UniecDefines.COMPETITION_KIND_INTERVIEW.equals(direction.getCompetitionKind().getCode()))
                {
                    InterviewResult interview = direction2interview.get(direction);
                    if (interview == null)
                        direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
                    else
                        direction.setState(code2state.get(interview.isPassed() ? UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE : UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
                } else
                {
                    Set<ChosenEntranceDiscipline> chosenSet = dataUtil.getChosenEntranceDisciplineSet(direction);
                    if (chosenSet.isEmpty())
                    {
                        direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
                    } else
                    {
                        boolean hasBadMark = false; // true, если хотя бы одна оценка меньше проходной по выбранному вступ. исп.
                        boolean hasNoMark = false;  // true, есть вступ. исп. без оценки.

                        for (ChosenEntranceDiscipline chosen : chosenSet)
                        {
                            Double finalMark = chosen.getFinalMark();
                            if (finalMark == null)
                            {
                                hasNoMark = true;
                            } else
                            {
                                if (finalMark < getPassMarkForDiscipline(chosen, direction))
                                    hasBadMark = true;
                            }
                        }

                        if (hasBadMark)
                        {
                            // есть оценка ниже минимально допустимой, состояние "выбыл из конкурса"
                            direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
                        } else if (hasNoMark)
                        {
                            // есть вступ. исп без оценки, состояние "активный"
                            direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_ACTIVE_CODE));
                        } else
                        {
                            // по всем вступ. исп. есть оценки не ниже минимальной, состояние "к зачислению"
                            direction.setState(code2state.get(UniecDefines.ENTRANT_STATE_TOBE_ENROLED_CODE));
                        }
                    }
                }
            }
            session.update(direction);
        }

        // П Е Р Е С Ч И Т Ы В А Е М   С О С Т О Я Н И Я   А Б И Т У Р И Е Н Т О В

        Map<Entrant, EntrantState> entrant2state = new HashMap<>();
        for (Entrant entrant : entrants)
            entrant2state.put(entrant, null);

        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            EntrantState entrantState = entrant2state.get(entrant);
            EntrantState directionState = direction.getState();

            if (entrantState == null || directionState.getPriority() < entrantState.getPriority())
                entrant2state.put(entrant, directionState);
        }

        for (Map.Entry<Entrant, EntrantState> entry : entrant2state.entrySet())
        {
            entry.getKey().setState(entry.getValue());
            session.update(entry.getKey());
        }

        // П Е Р Е С Ч И Т Ы В А Е М   П Р И З Н А К   П А Р А Л Л Е Л Ь Н О Е   О С В О Е Н И Е

        DQLSelectBuilder preStudentBuilder = new DQLSelectBuilder();
        preStudentBuilder.fromEntity(PreliminaryEnrollmentStudent.class, "preStudent");
        preStudentBuilder.joinPath(DQLJoinType.inner, "preStudent." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "direction");
        preStudentBuilder.joinPath(DQLJoinType.inner, "direction." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        preStudentBuilder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        preStudentBuilder.joinEntity("entrant", DQLJoinType.left, Student.class, "student", "entrant.person=student.person");
        preStudentBuilder.where(in(property("request." + EntrantRequest.L_ENTRANT), entrants));
        preStudentBuilder.column(property("entrant.id"), "entrantId");
        preStudentBuilder.column(property("student"), "student");
        preStudentBuilder.column(property("preStudent"), "preStudent");

        Set<Long> entrantParallelIds = new HashSet<>();
        Map<Long, List<PreliminaryEnrollmentStudent>> entrantId2preStudentList = new HashMap<>();
        List<Object[]> dataList = preStudentBuilder.createStatement(new DQLExecutionContext(session)).list();

        for (Object[] row : dataList)
        {
            Long entrantId = ((Number) row[0]).longValue();
            Student student = (Student) row[1];
            PreliminaryEnrollmentStudent preStudent = (PreliminaryEnrollmentStudent) row[2];

            String studentCategoryCode = student == null ? null : student.getStudentCategory().getCode();
            String preStudentCategoryCode = preStudent.getStudentCategory().getCode();

            if (UniDefines.STUDENT_CATEGORY_STUDENT.equals(studentCategoryCode) || UniDefines.STUDENT_CATEGORY_STUDENT.equals(preStudentCategoryCode))
                entrantParallelIds.add(entrantId);

            List<PreliminaryEnrollmentStudent> preStudentList = entrantId2preStudentList.get(entrantId);
            if (preStudentList == null)
                entrantId2preStudentList.put(entrantId, preStudentList = new ArrayList<>());
            preStudentList.add(preStudent);
        }

        for (Map.Entry<Long, List<PreliminaryEnrollmentStudent>> entry : entrantId2preStudentList.entrySet())
        {
            Long entrantId = entry.getKey();
            boolean parallel = entrantParallelIds.contains(entrantId);
            for (PreliminaryEnrollmentStudent preStudent : entry.getValue())
            {
                if (UniDefines.STUDENT_CATEGORY_LISTENER.equals(preStudent.getStudentCategory().getCode()))
                {
                    preStudent.setParallel(parallel || preStudent.isParallelLocked());
                } else
                {
                    preStudent.setParallel(false);
                    preStudent.setParallelLocked(false);
                }
                session.update(preStudent);
            }
        }
    }

    @Override
    public void doCheckEntrantLogicConsistency() throws Exception
    {
        final long start = System.currentTimeMillis();
        final File tmpDir = new File(System.getProperty("java.io.tmpdir") + "/uni");

        if (tmpDir.exists()) FileUtils.deleteDirectory(tmpDir);

        tmpDir.mkdir();

        Session session = getSession();

        System.out.println("dump state 'before'...");

        final List<String> tableNameList = new ArrayList<>();
        final Map<String, String> table2select = new HashMap<>();
        for (IEntityMeta meta : EntityRuntime.getInstance().getEntities())
            for (String moduleName : new String[]{"uniec", "uniecc"})
                if (meta.hasTable() && meta.getClassName().startsWith("ru.tandemservice." + moduleName + "."))
                {
                    List<String> list = new ArrayList<>();
                    list.add("id");
                    for (IPropertyMeta propertyMeta : meta.getDeclaredProperties())
                        list.add(((FieldPropertyMeta) propertyMeta).getColumnName());
                    list.remove("version");
                    tableNameList.add(meta.getTableName());
                    table2select.put(meta.getTableName(), StringUtils.join(list, ","));
                }

        for (String tableName : tableNameList)
        {
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(tmpDir, tableName))));

            ScrollableResults results = ((SessionImpl) session).createSQLQuery("select " + table2select.get(tableName) + " from " + tableName + " order by id").scroll(ScrollMode.FORWARD_ONLY);
            while (results.next())
                for (Object data : results.get())
                    if (!(data instanceof Clob || data instanceof Blob))
                        out.writeUTF(data == null ? "-" : data.toString());
            out.close();
        }

        System.out.println("update all entrant...");

        ScrollableResults results = new DQLSelectBuilder().fromEntity(Entrant.class, "e")
                .column("e")
                .createStatement(new DQLExecutionContext(session))
                .scroll(ScrollMode.FORWARD_ONLY);

        while (results.next())
            session.update(results.get()[0]);

        ActionQueue queue = ((SessionImpl) session).getActionQueue();

        queue.registerProcess(new BeforeTransactionCompletionProcess()
        {
            @Override
            public void doBeforeTransactionCompletion(SessionImplementor session)
            {
                System.out.println("doBeforeTransactionCompletion...");
                // уже прошли все синхронизации и flush сессии
                // можем легко освободить память
                getSession().clear();
                try
                {
                    List<String> list = new ArrayList<>();

                    System.out.println("dump state 'after'...");

                    for (String tableName : tableNameList)
                    {
                        ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
                        DataOutputStream out = new DataOutputStream(byteArrayOut);
                        ScrollableResults results = ((SessionImpl) session).createSQLQuery("select " + table2select.get(tableName) + " from " + tableName + " order by id").scroll(ScrollMode.FORWARD_ONLY);
                        while (results.next())
                            for (Object data : results.get())
                                if (!(data instanceof Clob || data instanceof Blob))
                                    out.writeUTF(data == null ? "-" : data.toString());
                        out.close();

                        byte[] afterArray = byteArrayOut.toByteArray();
                        byte[] beforeArray = FileUtils.readFileToByteArray(new File(tmpDir, tableName));

                        if (!Arrays.equals(beforeArray, afterArray))
                            list.add(tableName);
                    }

                    System.out.println("time: " + (System.currentTimeMillis() - start));
                    System.out.println("queries: " + Debug.getRootSection().getQueryCount());

                    if (!list.isEmpty())
                        System.out.println("Logic fail for tables:\n " + StringUtils.join(list, "\n "));
                    else
                        System.out.println("entrant logic is consistence!");

                    FileUtils.deleteDirectory(tmpDir);
                } catch (Throwable t)
                {
                    t.printStackTrace();
                    // hide exception
                }
            }
        });

        System.out.println("run syncronization...");
    }
}
