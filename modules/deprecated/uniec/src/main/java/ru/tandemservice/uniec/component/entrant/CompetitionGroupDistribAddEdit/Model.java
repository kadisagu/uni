package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribAddEdit;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.Return;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;
import ru.tandemservice.uniec.entity.ecg.EcgDistribQuota;

/**
 * @author vdanilov
 */
@Input({
	@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="distrib.id"),
	@Bind(key="parentId", binding="parentId"),
	@Bind(key="compensationTypeId", binding="compensationTypeId"),
    @Bind(key="category", binding="secondHighAdmission")
})
@Return({
	@Bind(key="competitionGroupDistribId", binding="resultId")
})
public class Model {
	private Long resultId;
	public Long getResultId() { return this.resultId; }
	public void setResultId(final Long resultId) { this.resultId = resultId; }

	private Long parentId;
	public Long getParentId() { return this.parentId; }
	public void setParentId(final Long parentId) { this.parentId = parentId; }

	private Long compensationTypeId;
	public Long getCompensationTypeId() { return this.compensationTypeId; }
	public void setCompensationTypeId(final Long compensationTypeId) { this.compensationTypeId = compensationTypeId; }

	private EcgDistribObject distrib = new EcgDistribObject();
	public EcgDistribObject getDistrib() { return this.distrib; }
	public void setDistrib(final EcgDistribObject distrib) { this.distrib = distrib; }

	private List<CompensationType> compensationTypeList = Collections.emptyList();
	public List<CompensationType> getCompensationTypeList() { return this.compensationTypeList; }
	public void setCompensationTypeList(final List<CompensationType> compensationTypeList) { this.compensationTypeList = compensationTypeList; }

	private List<EcgDistribQuota> quotaList = Collections.emptyList();
	protected List<EcgDistribQuota> getQuotaList() { return this.quotaList; }
	protected void setQuotaList(final List<EcgDistribQuota> quotaList) { this.quotaList = quotaList; }

    private List<IdentifiableWrapper> categoryList;
    public List<IdentifiableWrapper> getCategoryList() { return categoryList; }
    public void setCategoryList(List<IdentifiableWrapper> categoryList) { this.categoryList = categoryList; }

    private IdentifiableWrapper category;
    public IdentifiableWrapper getCategory() { return category; }
    public void setCategory(IdentifiableWrapper category) { this.category = category; }

    boolean secondHighAdmission;
    public boolean isSecondHighAdmission() { return secondHighAdmission; }
    public void setSecondHighAdmission(boolean secondHighAdmission) { this.secondHighAdmission = secondHighAdmission; }

    private final AbstractListDataSource<EcgDistribQuota> quotaDataSource = new AbstractListDataSource<EcgDistribQuota>() {
		@Override public List<EcgDistribQuota> getEntityList() { return Model.this.quotaList; }
		@Override public long getTotalSize() { return this.getEntityList().size(); }
		@Override public long getCountRow() { return this.getTotalSize(); }
		@Override public long getStartRow() { return 0; }
		@Override public void onChangeOrder() {}
		@Override public void onRefresh() {}
        @Override public AbstractListDataSource<EcgDistribQuota> getCopy() { return this; }
	};
	public AbstractListDataSource<EcgDistribQuota> getQuotaDataSource() { return this.quotaDataSource; }

	protected EcgDistribQuota current() { return this.getQuotaDataSource().getCurrentEntity(); }
	public Integer getCurrentQuota() {
		final int q = this.current().getQuota();
		if (q < 0) { return null; }
		return q;
	}
	public void setCurrentQuota(final Integer value) {
		this.current().setQuota(null == value ? -1 : value.intValue());
	}

}
