package ru.tandemservice.uniec.component.report.EnrollmentResultByStage.EnrollmentResultByStageAdd;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;

/**
 * @author oleyba
 * @since 16.10.2009
 */
public class EnrollmentResultByStageReportBuilder
{
    private Model model;
    private Session session;

    public EnrollmentResultByStageReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    @SuppressWarnings("unchecked")
    public DatabaseFile getContent()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENROLLMENT_RESULT_BY_STAGES);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        TopOrgUnit academy = TopOrgUnit.getInstance();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        injectModifier.put("year", Integer.toString(model.getReport().getEnrollmentCampaign().getEducationYear().getIntValue()));
        injectModifier.put("developForm", getDevelopFormTitle());
        injectModifier.modify(document);

        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, session);
        if (model.isQualificationActive())
            enrollmentDirectionList = filterByQualification(enrollmentDirectionList);

        Map<Long, ReportRow> tableRows = new HashMap<>();

        for (RequestedEnrollmentDirection direction : getDirections(enrollmentDirectionList))
        {
            EducationLevels level = getGroupLevel(direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
            ReportRow row = tableRows.get(level.getId());
            if (null == row)
                tableRows.put(level.getId(), row = new ReportRow(level.getDisplayableTitle()));
            row.requestId.add(direction.getEntrantRequest().getId());
        }

        EntrantDataUtil util = new EntrantDataUtil(session, model.getReport().getEnrollmentCampaign(), getPreliminaryDirectionBuilder(enrollmentDirectionList));
        List<Long> notEmptyOlympiadDirectionIds = getNotEmptyOlympiadDirectionIds(getPreliminaryDirectionBuilder(enrollmentDirectionList));
        Map<PreliminaryEnrollmentStudent, Date> studentMap = getPreliminaryStudents(enrollmentDirectionList);
        for (PreliminaryEnrollmentStudent student : studentMap.keySet())
        {
            EducationLevels level = getGroupLevel(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());
            ReportRow row = tableRows.get(level.getId());
            if (null == row)
                tableRows.put(level.getId(), row = new ReportRow(level.getDisplayableTitle()));
            Date date = studentMap.get(student);
            RowBlock block = null;
            for (int key = 1; key <=4; key++)
                if (!date.after(model.getStageDateTo().get(key)) && !date.before(model.getStageDateFrom().get(key)))
                    block = row.blockMap.get(key);
            if (null == block) continue;
            RequestedEnrollmentDirection direction = student.getRequestedEnrollmentDirection();
            String compKind = direction.getCompetitionKind().getCode();
            if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(compKind))
                block.enteredBeyondCompetiton++;
            if (notEmptyOlympiadDirectionIds.contains(direction.getId()))
                block.enteredOlympiad++;
            if (student.isTargetAdmission())
                block.enteredTarget++;
            if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(compKind))
            {
                boolean hasStateExamMark = false;
                Set<ChosenEntranceDiscipline> entranceDisciplines = util.getChosenEntranceDisciplineSet(direction);
                for (ChosenEntranceDiscipline discipline : entranceDisciplines)
                    if (discipline.getFinalMark() != null && (discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1
                            || discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2
                            || discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL
                            || discipline.getFinalMarkSource() == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL))
                        hasStateExamMark = true;
                if (hasStateExamMark)
                    block.enteredStateExam++;
            }
        }

        List<ReportRow> rowList = new ArrayList<>(tableRows.values());
        Collections.sort(rowList);
        List<String[]> tableData = new ArrayList<>();
        for (ReportRow row : rowList)
            tableData.add(row.format());

        ReportRow totalRow = new ReportRow("ВСЕГО");
        long requestsTotal = totalRow.sum(rowList);
        String[] total = totalRow.format();
        total[1] = String.valueOf(requestsTotal);
        tableData.add(total);

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));
        tableModifier.modify(document);

        DatabaseFile file = new DatabaseFile();
        file.setContent(RtfUtil.toByteArray(document));
        return file;
    }

    private EducationLevels getGroupLevel(EducationLevels educationLevel)
    {
        while (educationLevel.getParentLevel() != null)
            educationLevel = educationLevel.getParentLevel();
        return educationLevel;
    }

    private List<EnrollmentDirection> filterByQualification(List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EnrollmentDirection> result = new ArrayList<>();
        for (EnrollmentDirection direction : enrollmentDirectionList)
            if (model.getQualificationList().contains(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification()))
                result.add(direction);
        return result;
    }

    private List<RequestedEnrollmentDirection> getDirections(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirectionList));
        return builder.getResultList(session);
    }

    private Map<PreliminaryEnrollmentStudent, Date> getPreliminaryStudents(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        if (model.isParallelActive() && model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.addDomain("extr", EnrollmentExtract.ENTITY_CLASS);
        builder.addSelect("extr", new Object[]{EnrollmentExtract.paragraph().order().commitDate()});
        builder.add(MQExpression.eqProperty("extr", IAbstractExtract.L_ENTITY + ".id", "p", "id"));

        Map<PreliminaryEnrollmentStudent, Date> result = new HashMap<>();
        for (Object[] row : builder.<Object[]>getResultList(session))
            if (null != row[1])
                result.put((PreliminaryEnrollmentStudent) row[0], (Date) row[1]);
        return result;
    }

    private MQBuilder getPreliminaryDirectionBuilder(List<EnrollmentDirection> enrollmentDirectionList)
    {
        MQBuilder builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p");
        if (model.isStudentCategoryActive())
            builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, model.getStudentCategoryList()));
        if (model.isParallelActive() && model.getParallel().isTrue())
            builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, false));

        builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, CommonBaseEntityUtil.getPropertiesSet(enrollmentDirectionList, EnrollmentDirection.L_EDUCATION_ORG_UNIT)));
        builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getReport().getEnrollmentCampaign()));
        builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.getSelectAliasList().clear();
        builder.addSelect("r");
        return builder;
    }

    private List<Long> getNotEmptyOlympiadDirectionIds(MQBuilder directionBuilder)
    {
        MQBuilder builder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "chosen", new String[]{
                ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + ".id"});
        builder.add(MQExpression.in("chosen", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, directionBuilder));
        builder.add(MQExpression.isNotNull("chosen", ChosenEntranceDiscipline.P_FINAL_MARK));
        builder.add(MQExpression.eq("chosen", ChosenEntranceDiscipline.P_FINAL_MARK_SOURCE, UniecDefines.FINAL_MARK_OLYMPIAD));
        builder.setNeedDistinct(true);
        return builder.getResultList(session);
    }

    private String getDevelopFormTitle()
    {
        List<String> titles = new ArrayList<>();
        for (DevelopForm form : model.getDevelopFormList())
        {
            String title = getDevelopFormTitle(form);
            if (!titles.contains(title))
                titles.add(title);
        }
        return StringUtils.join(titles, ", ").trim();
    }

    private static String getDevelopFormTitle(DevelopForm developForm)
    {
        String code = developForm.getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            return "очной";
        if (DevelopFormCodes.CORESP_FORM.equals(code))
            return "заочной";
        return "очно-заочной";
    }

    @SuppressWarnings("unchecked")
    private static class ReportRow implements Comparable
    {
        private String title;

        private Set<Long> requestId = new HashSet<>();

        private Map<Integer, RowBlock> blockMap = ImmutableMap.of(
                1, new RowBlock(), 2, new RowBlock(),
                3, new RowBlock(), 4, new RowBlock());

        private ReportRow(String title)
        {
            this.title = title;
        }

        @Override
        public int compareTo(Object o)
        {
            return title.compareTo(((ReportRow) o).title);
        }

        public long sum(List<ReportRow> rowList)
        {
            long result = 0;
            for (ReportRow row : rowList)
            {
                result = result + row.requestId.size();
                for (int key = 1; key <=4; key++)
                    blockMap.get(key).sum(row.blockMap.get(key));
            }
            return result;
        }

        public String[] format()
        {
            List<String> result = new ArrayList<>();
            result.add(title);
            result.add(String.valueOf(requestId.size()));
            for (int key = 1; key <=4; key++)
                result.addAll(blockMap.get(key).format());
            return result.toArray(new String[result.size()]);
        }
    }

    private static class RowBlock
    {
        private long enteredStateExam;
        private long enteredOlympiad;
        private long enteredBeyondCompetiton;
        private long enteredTarget;

        public void sum(RowBlock block)
        {
            enteredStateExam = enteredStateExam + block.enteredStateExam;
            enteredOlympiad = enteredOlympiad + block.enteredOlympiad;
            enteredBeyondCompetiton = enteredBeyondCompetiton + block.enteredBeyondCompetiton;
            enteredTarget = enteredTarget + block.enteredTarget;
        }

        public Collection<? extends String> format()
        {
            return Arrays.asList(String.valueOf(enteredStateExam),
                    String.valueOf(enteredOlympiad),
                    String.valueOf(enteredBeyondCompetiton),
                    String.valueOf(enteredTarget));
        }
    }
}
