/* $Id:$ */
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author oleyba
 * @since 5/25/12
 */
public class RequestedDirectionPriorityDTO extends DataWrapper
{
    private RequestedEnrollmentDirection _direction;
    private Integer _overallPriority;

    public RequestedDirectionPriorityDTO(RequestedEnrollmentDirection direction, Integer priority)
    {
        super(direction);
        _direction = direction;
        _overallPriority = priority;
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return getDirection().getEnrollmentDirection();
    }

    public RequestedEnrollmentDirection getDirection()
    {
        return _direction;
    }

    public void setDirection(RequestedEnrollmentDirection direction)
    {
        _direction = direction;
    }

    public Integer getOverallPriority()
    {
        return _overallPriority;
    }

    public void setOverallPriority(Integer overallPriority)
    {
        _overallPriority = overallPriority;
    }
}
