/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.SummarySheetExamPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.report.SummarySheetExamReport;

/**
 * @author Alexander Shaburov
 * @since 18.07.12
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class EcReportSummarySheetExamPubUI extends UIPresenter
{
    // fields

    private Long _reportId;
    private SummarySheetExamReport _report;
    private String _requestFromToPeriod;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(SummarySheetExamReport.class, _reportId);

        _requestFromToPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getRequestDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_report.getRequestDateTo());
    }

    // Listeners

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "rtf")
                .activate();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public SummarySheetExamReport getReport()
    {
        return _report;
    }

    public void setReport(SummarySheetExamReport report)
    {
        _report = report;
    }

    public String getRequestFromToPeriod()
    {
        return _requestFromToPeriod;
    }

    public void setRequestFromToPeriod(String requestFromToPeriod)
    {
        _requestFromToPeriod = requestFromToPeriod;
    }
}
