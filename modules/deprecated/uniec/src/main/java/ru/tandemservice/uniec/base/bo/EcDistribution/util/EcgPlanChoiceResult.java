/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 14.07.2011
 */
public class EcgPlanChoiceResult implements IEcgPlanChoiceResult
{
    private IEcgEntrantRateDirectionDTO _chosenDirection;
    private List<IEcgEntrantRateDirectionDTO> _possibleDirectionList;

    public EcgPlanChoiceResult(IEcgEntrantRateDirectionDTO chosenDirection, List<IEcgEntrantRateDirectionDTO> possibleDirectionList)
    {
        _chosenDirection = chosenDirection;
        _possibleDirectionList = possibleDirectionList;
    }

    @Override
    public IEcgEntrantRateDirectionDTO getChosenDirection()
    {
        return _chosenDirection;
    }

    @Override
    public List<IEcgEntrantRateDirectionDTO> getPossibleDirectionList()
    {
        return _possibleDirectionList;
    }
}
