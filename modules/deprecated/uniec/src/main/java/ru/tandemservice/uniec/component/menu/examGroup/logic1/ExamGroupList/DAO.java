/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.logic1.ExamGroupList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList.ExamGroupListDAO;
import ru.tandemservice.uniec.dao.examgroup.logic1.ExamGroupLogicSample1;
import ru.tandemservice.uniec.entity.catalog.ExamGroupType;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public class DAO extends ExamGroupListDAO<Model> implements IDAO
{
    @Override
    protected void prepareModel(Model model)
    {
        // получаем список значений фильтров
        model.setExamGroupTypeListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(ExamGroupType.class)));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(CompensationType.class), CompensationType.P_SHORT_TITLE));
    }

    @Override
    protected void prepareGroupList(MQBuilder builder, String alias, Model model)
    {
        // получаем фильтры
        ExamGroupType examGroupType = (ExamGroupType) model.getSettings().get("examGroupType");
        DevelopForm developForm = (DevelopForm) model.getSettings().get("developForm");
        CompensationType compensationType = (CompensationType) model.getSettings().get("compensationType");

        if (examGroupType == null || developForm == null || compensationType == null)
        {
            // не все фильтры выбраны, возвращаем пустой список
            builder.add(MQExpression.isNull(alias, ExamGroup.P_KEY));
        } else
        {
            // создаем ключ
            String key = ExamGroupLogicSample1.createKey(examGroupType, compensationType, developForm);

            // применяем фильтр
            builder.add(MQExpression.eq(alias, ExamGroup.P_KEY, key));
        }
    }
}
