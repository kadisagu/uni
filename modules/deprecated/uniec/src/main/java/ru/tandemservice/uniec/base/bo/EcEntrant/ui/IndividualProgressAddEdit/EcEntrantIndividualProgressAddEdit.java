/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.IndividualProgressAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.catalog.IndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EnrCampaignEntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantIndividualProgress;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 31.03.2015
 */
@Configuration
public class EcEntrantIndividualProgressAddEdit extends BusinessComponentManager
{
    public static final String INDIVIDUAL_PROGRESS_DS = "individualProgressTypeDS";
    public static final String ENTRANT_REQUEST_DS = "entrantRequestDS";
    public static final String ENTRANT = "entrant";
    public static final String PROGRESS_ID = "progressId";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(INDIVIDUAL_PROGRESS_DS, individualProgressDSHandler()))
                .addDataSource(selectDS(ENTRANT_REQUEST_DS, entrantRequestDSHandler()))
                .create();
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> individualProgressDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            Entrant entrant = context.get(ENTRANT);
            Long progressId = context.get(PROGRESS_ID);

            dql.where(exists(EnrCampaignEntrantIndividualProgress.class,
                             EnrCampaignEntrantIndividualProgress.enrollmentCampaign().s(), entrant.getEnrollmentCampaign(),
                             EnrCampaignEntrantIndividualProgress.individualProgress().s(), property(alias)));

            dql.where(or(
                    eq(property(alias, IndividualProgress.id()), value(progressId)),
                    not(exists(EntrantIndividualProgress.class,
                               EntrantIndividualProgress.individualProgressType().s(), property(alias),
                               EntrantIndividualProgress.entrant().s(), entrant))
            ));

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), IndividualProgress.class)
                .customize(customizer)
                .order(IndividualProgress.title())
                .filter(IndividualProgress.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entrantRequestDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            FilterUtils.applySelectFilter(dql, alias, EntrantRequest.entrant().s(), context.get(ENTRANT));
            return dql;
        };

     return EntrantRequest.defaultSelectDSHandler(getName()).customize(customizer);
    }
}