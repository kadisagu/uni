package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состояние предзачисления рекомендованного для распределения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgEntrantRecommendedStateGen extends EntityBase
 implements INaturalIdentifiable<EcgEntrantRecommendedStateGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState";
    public static final String ENTITY_NAME = "ecgEntrantRecommendedState";
    public static final int VERSION_HASH = 37419243;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_RECOMMENDED = "recommended";
    public static final String P_BRING_ORIGINAL = "bringOriginal";
    public static final String P_HAS_PRE_STUDENT = "hasPreStudent";

    private EcgDistribution _distribution;     // Основное распределение абитуриентов
    private EcgEntrantRecommended _recommended;     // Рекомендованный абитуриент основного распределения
    private Boolean _bringOriginal;     // Принес оригиналы
    private Boolean _hasPreStudent;     // Есть предзачисление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EcgDistribution getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Основное распределение абитуриентов. Свойство не может быть null.
     */
    public void setDistribution(EcgDistribution distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Рекомендованный абитуриент основного распределения. Свойство не может быть null.
     */
    @NotNull
    public EcgEntrantRecommended getRecommended()
    {
        return _recommended;
    }

    /**
     * @param recommended Рекомендованный абитуриент основного распределения. Свойство не может быть null.
     */
    public void setRecommended(EcgEntrantRecommended recommended)
    {
        dirty(_recommended, recommended);
        _recommended = recommended;
    }

    /**
     * @return Принес оригиналы.
     */
    public Boolean getBringOriginal()
    {
        return _bringOriginal;
    }

    /**
     * @param bringOriginal Принес оригиналы.
     */
    public void setBringOriginal(Boolean bringOriginal)
    {
        dirty(_bringOriginal, bringOriginal);
        _bringOriginal = bringOriginal;
    }

    /**
     * @return Есть предзачисление.
     */
    public Boolean getHasPreStudent()
    {
        return _hasPreStudent;
    }

    /**
     * @param hasPreStudent Есть предзачисление.
     */
    public void setHasPreStudent(Boolean hasPreStudent)
    {
        dirty(_hasPreStudent, hasPreStudent);
        _hasPreStudent = hasPreStudent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgEntrantRecommendedStateGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgEntrantRecommendedState)another).getDistribution());
                setRecommended(((EcgEntrantRecommendedState)another).getRecommended());
            }
            setBringOriginal(((EcgEntrantRecommendedState)another).getBringOriginal());
            setHasPreStudent(((EcgEntrantRecommendedState)another).getHasPreStudent());
        }
    }

    public INaturalId<EcgEntrantRecommendedStateGen> getNaturalId()
    {
        return new NaturalId(getDistribution(), getRecommended());
    }

    public static class NaturalId extends NaturalIdBase<EcgEntrantRecommendedStateGen>
    {
        private static final String PROXY_NAME = "EcgEntrantRecommendedStateNaturalProxy";

        private Long _distribution;
        private Long _recommended;

        public NaturalId()
        {}

        public NaturalId(EcgDistribution distribution, EcgEntrantRecommended recommended)
        {
            _distribution = ((IEntity) distribution).getId();
            _recommended = ((IEntity) recommended).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public Long getRecommended()
        {
            return _recommended;
        }

        public void setRecommended(Long recommended)
        {
            _recommended = recommended;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgEntrantRecommendedStateGen.NaturalId) ) return false;

            EcgEntrantRecommendedStateGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            if( !equals(getRecommended(), that.getRecommended()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            result = hashCode(result, getRecommended());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            sb.append("/");
            sb.append(getRecommended());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgEntrantRecommendedStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgEntrantRecommendedState.class;
        }

        public T newInstance()
        {
            return (T) new EcgEntrantRecommendedState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "recommended":
                    return obj.getRecommended();
                case "bringOriginal":
                    return obj.getBringOriginal();
                case "hasPreStudent":
                    return obj.getHasPreStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistribution) value);
                    return;
                case "recommended":
                    obj.setRecommended((EcgEntrantRecommended) value);
                    return;
                case "bringOriginal":
                    obj.setBringOriginal((Boolean) value);
                    return;
                case "hasPreStudent":
                    obj.setHasPreStudent((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "recommended":
                        return true;
                case "bringOriginal":
                        return true;
                case "hasPreStudent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "recommended":
                    return true;
                case "bringOriginal":
                    return true;
                case "hasPreStudent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistribution.class;
                case "recommended":
                    return EcgEntrantRecommended.class;
                case "bringOriginal":
                    return Boolean.class;
                case "hasPreStudent":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgEntrantRecommendedState> _dslPath = new Path<EcgEntrantRecommendedState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgEntrantRecommendedState");
    }
            

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getDistribution()
     */
    public static EcgDistribution.Path<EcgDistribution> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Рекомендованный абитуриент основного распределения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getRecommended()
     */
    public static EcgEntrantRecommended.Path<EcgEntrantRecommended> recommended()
    {
        return _dslPath.recommended();
    }

    /**
     * @return Принес оригиналы.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getBringOriginal()
     */
    public static PropertyPath<Boolean> bringOriginal()
    {
        return _dslPath.bringOriginal();
    }

    /**
     * @return Есть предзачисление.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getHasPreStudent()
     */
    public static PropertyPath<Boolean> hasPreStudent()
    {
        return _dslPath.hasPreStudent();
    }

    public static class Path<E extends EcgEntrantRecommendedState> extends EntityPath<E>
    {
        private EcgDistribution.Path<EcgDistribution> _distribution;
        private EcgEntrantRecommended.Path<EcgEntrantRecommended> _recommended;
        private PropertyPath<Boolean> _bringOriginal;
        private PropertyPath<Boolean> _hasPreStudent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Основное распределение абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getDistribution()
     */
        public EcgDistribution.Path<EcgDistribution> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistribution.Path<EcgDistribution>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Рекомендованный абитуриент основного распределения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getRecommended()
     */
        public EcgEntrantRecommended.Path<EcgEntrantRecommended> recommended()
        {
            if(_recommended == null )
                _recommended = new EcgEntrantRecommended.Path<EcgEntrantRecommended>(L_RECOMMENDED, this);
            return _recommended;
        }

    /**
     * @return Принес оригиналы.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getBringOriginal()
     */
        public PropertyPath<Boolean> bringOriginal()
        {
            if(_bringOriginal == null )
                _bringOriginal = new PropertyPath<Boolean>(EcgEntrantRecommendedStateGen.P_BRING_ORIGINAL, this);
            return _bringOriginal;
        }

    /**
     * @return Есть предзачисление.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedState#getHasPreStudent()
     */
        public PropertyPath<Boolean> hasPreStudent()
        {
            if(_hasPreStudent == null )
                _hasPreStudent = new PropertyPath<Boolean>(EcgEntrantRecommendedStateGen.P_HAS_PRE_STUDENT, this);
            return _hasPreStudent;
        }

        public Class getEntityClass()
        {
            return EcgEntrantRecommendedState.class;
        }

        public String getEntityName()
        {
            return "ecgEntrantRecommendedState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
