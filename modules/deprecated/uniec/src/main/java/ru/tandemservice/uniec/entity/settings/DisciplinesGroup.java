package ru.tandemservice.uniec.entity.settings;

import java.util.List;

import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.settings.gen.DisciplinesGroupGen;

public class DisciplinesGroup extends DisciplinesGroupGen
{
    public static final String DISCIPLINES = "disciplines";

    @Override
    public List<Discipline2RealizationWayRelation> getDisciplines() {
        return UniecDAOFacade.getSettingsDAO().getDisciplines(this);
    }

    @Override
    public String getFullTitle() {
        List<Discipline2RealizationWayRelation> disciplines = getDisciplines();
        if (disciplines.isEmpty()) { return getTitle() + " (пустая группа)"; }

        StringBuilder sb = new StringBuilder();
        for (Discipline2RealizationWayRelation dsc: disciplines) {
            if (sb.length() > 0) { sb.append(", "); }
            sb.append(dsc.getTitle());
        }

        return getTitle() + " (" +sb.toString()+")";
    }


}