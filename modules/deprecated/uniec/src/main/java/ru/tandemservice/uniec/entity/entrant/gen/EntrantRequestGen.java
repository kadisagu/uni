package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.catalog.MethodDeliveryNReturnDocs;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.IEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявление абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantRequestGen extends VersionedEntityBase
 implements IEntrant{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.EntrantRequest";
    public static final String ENTITY_NAME = "entrantRequest";
    public static final int VERSION_HASH = -1617823794;
    private static IEntityMeta ENTITY_META;

    public static final String P_VERSION = "version";
    public static final String P_ENTRANT_ID = "entrantId";
    public static final String L_ENTRANT = "entrant";
    public static final String P_REG_DATE = "regDate";
    public static final String P_REG_NUMBER = "regNumber";
    public static final String P_GROUP = "group";
    public static final String P_TAKE_AWAY_DOCUMENT = "takeAwayDocument";
    public static final String P_TECHNIC_COMMISSION = "technicCommission";
    public static final String P_COMMENT = "comment";
    public static final String L_METHOD_DELIVERY_DOCS = "methodDeliveryDocs";
    public static final String L_METHOD_RETURN_DOCS = "methodReturnDocs";

    private int _version; 
    private long _entrantId; 
    private Entrant _entrant;     // (Старый) Абитуриент
    private Date _regDate;     // Дата добавления заявления
    private int _regNumber;     // Номер заявления
    private String _group;     // Группа
    private boolean _takeAwayDocument;     // Забрал документы
    private String _technicCommission;     // Техническая комиссия
    private String _comment;     // Комментарий
    private MethodDeliveryNReturnDocs _methodDeliveryDocs;     // Способ подачи оригиналов документов
    private MethodDeliveryNReturnDocs _methodReturnDocs;     // Способ возврата оригиналов документов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getVersion()
    {
        return _version;
    }

    /**
     * @param version  Свойство не может быть null.
     */
    public void setVersion(int version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entrant.id".
     */
    // @NotNull
    public long getEntrantId()
    {
        return _entrantId;
    }

    /**
     * @param entrantId  Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setEntrantId(long entrantId)
    {
        dirty(_entrantId, entrantId);
        _entrantId = entrantId;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Дата добавления заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата добавления заявления. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Номер заявления. Свойство не может быть null.
     */
    @NotNull
    public int getRegNumber()
    {
        return _regNumber;
    }

    /**
     * @param regNumber Номер заявления. Свойство не может быть null.
     */
    public void setRegNumber(int regNumber)
    {
        dirty(_regNumber, regNumber);
        _regNumber = regNumber;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Забрал документы. Свойство не может быть null.
     */
    @NotNull
    public boolean isTakeAwayDocument()
    {
        return _takeAwayDocument;
    }

    /**
     * @param takeAwayDocument Забрал документы. Свойство не может быть null.
     */
    public void setTakeAwayDocument(boolean takeAwayDocument)
    {
        dirty(_takeAwayDocument, takeAwayDocument);
        _takeAwayDocument = takeAwayDocument;
    }

    /**
     * @return Техническая комиссия.
     */
    @Length(max=255)
    public String getTechnicCommission()
    {
        return _technicCommission;
    }

    /**
     * @param technicCommission Техническая комиссия.
     */
    public void setTechnicCommission(String technicCommission)
    {
        dirty(_technicCommission, technicCommission);
        _technicCommission = technicCommission;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=2000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Способ подачи оригиналов документов.
     */
    public MethodDeliveryNReturnDocs getMethodDeliveryDocs()
    {
        return _methodDeliveryDocs;
    }

    /**
     * @param methodDeliveryDocs Способ подачи оригиналов документов.
     */
    public void setMethodDeliveryDocs(MethodDeliveryNReturnDocs methodDeliveryDocs)
    {
        dirty(_methodDeliveryDocs, methodDeliveryDocs);
        _methodDeliveryDocs = methodDeliveryDocs;
    }

    /**
     * @return Способ возврата оригиналов документов.
     */
    public MethodDeliveryNReturnDocs getMethodReturnDocs()
    {
        return _methodReturnDocs;
    }

    /**
     * @param methodReturnDocs Способ возврата оригиналов документов.
     */
    public void setMethodReturnDocs(MethodDeliveryNReturnDocs methodReturnDocs)
    {
        dirty(_methodReturnDocs, methodReturnDocs);
        _methodReturnDocs = methodReturnDocs;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantRequestGen)
        {
            setVersion(((EntrantRequest)another).getVersion());
            setEntrantId(((EntrantRequest)another).getEntrantId());
            setEntrant(((EntrantRequest)another).getEntrant());
            setRegDate(((EntrantRequest)another).getRegDate());
            setRegNumber(((EntrantRequest)another).getRegNumber());
            setGroup(((EntrantRequest)another).getGroup());
            setTakeAwayDocument(((EntrantRequest)another).isTakeAwayDocument());
            setTechnicCommission(((EntrantRequest)another).getTechnicCommission());
            setComment(((EntrantRequest)another).getComment());
            setMethodDeliveryDocs(((EntrantRequest)another).getMethodDeliveryDocs());
            setMethodReturnDocs(((EntrantRequest)another).getMethodReturnDocs());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantRequest.class;
        }

        public T newInstance()
        {
            return (T) new EntrantRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "entrantId":
                    return obj.getEntrantId();
                case "entrant":
                    return obj.getEntrant();
                case "regDate":
                    return obj.getRegDate();
                case "regNumber":
                    return obj.getRegNumber();
                case "group":
                    return obj.getGroup();
                case "takeAwayDocument":
                    return obj.isTakeAwayDocument();
                case "technicCommission":
                    return obj.getTechnicCommission();
                case "comment":
                    return obj.getComment();
                case "methodDeliveryDocs":
                    return obj.getMethodDeliveryDocs();
                case "methodReturnDocs":
                    return obj.getMethodReturnDocs();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((Integer) value);
                    return;
                case "entrantId":
                    obj.setEntrantId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "regNumber":
                    obj.setRegNumber((Integer) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "takeAwayDocument":
                    obj.setTakeAwayDocument((Boolean) value);
                    return;
                case "technicCommission":
                    obj.setTechnicCommission((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "methodDeliveryDocs":
                    obj.setMethodDeliveryDocs((MethodDeliveryNReturnDocs) value);
                    return;
                case "methodReturnDocs":
                    obj.setMethodReturnDocs((MethodDeliveryNReturnDocs) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "entrantId":
                        return true;
                case "entrant":
                        return true;
                case "regDate":
                        return true;
                case "regNumber":
                        return true;
                case "group":
                        return true;
                case "takeAwayDocument":
                        return true;
                case "technicCommission":
                        return true;
                case "comment":
                        return true;
                case "methodDeliveryDocs":
                        return true;
                case "methodReturnDocs":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "entrantId":
                    return true;
                case "entrant":
                    return true;
                case "regDate":
                    return true;
                case "regNumber":
                    return true;
                case "group":
                    return true;
                case "takeAwayDocument":
                    return true;
                case "technicCommission":
                    return true;
                case "comment":
                    return true;
                case "methodDeliveryDocs":
                    return true;
                case "methodReturnDocs":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return Integer.class;
                case "entrantId":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "regDate":
                    return Date.class;
                case "regNumber":
                    return Integer.class;
                case "group":
                    return String.class;
                case "takeAwayDocument":
                    return Boolean.class;
                case "technicCommission":
                    return String.class;
                case "comment":
                    return String.class;
                case "methodDeliveryDocs":
                    return MethodDeliveryNReturnDocs.class;
                case "methodReturnDocs":
                    return MethodDeliveryNReturnDocs.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantRequest> _dslPath = new Path<EntrantRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantRequest");
    }
            

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getVersion()
     */
    public static PropertyPath<Integer> version()
    {
        return _dslPath.version();
    }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entrant.id".
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getEntrantId()
     */
    public static PropertyPath<Long> entrantId()
    {
        return _dslPath.entrantId();
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Дата добавления заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Номер заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getRegNumber()
     */
    public static PropertyPath<Integer> regNumber()
    {
        return _dslPath.regNumber();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Забрал документы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#isTakeAwayDocument()
     */
    public static PropertyPath<Boolean> takeAwayDocument()
    {
        return _dslPath.takeAwayDocument();
    }

    /**
     * @return Техническая комиссия.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getTechnicCommission()
     */
    public static PropertyPath<String> technicCommission()
    {
        return _dslPath.technicCommission();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Способ подачи оригиналов документов.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getMethodDeliveryDocs()
     */
    public static MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> methodDeliveryDocs()
    {
        return _dslPath.methodDeliveryDocs();
    }

    /**
     * @return Способ возврата оригиналов документов.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getMethodReturnDocs()
     */
    public static MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> methodReturnDocs()
    {
        return _dslPath.methodReturnDocs();
    }

    public static class Path<E extends EntrantRequest> extends EntityPath<E>
    {
        private PropertyPath<Integer> _version;
        private PropertyPath<Long> _entrantId;
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Integer> _regNumber;
        private PropertyPath<String> _group;
        private PropertyPath<Boolean> _takeAwayDocument;
        private PropertyPath<String> _technicCommission;
        private PropertyPath<String> _comment;
        private MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> _methodDeliveryDocs;
        private MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> _methodReturnDocs;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getVersion()
     */
        public PropertyPath<Integer> version()
        {
            if(_version == null )
                _version = new PropertyPath<Integer>(EntrantRequestGen.P_VERSION, this);
            return _version;
        }

    /**
     * @return  Свойство не может быть null.
     *
     * Это формула "entrant.id".
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getEntrantId()
     */
        public PropertyPath<Long> entrantId()
        {
            if(_entrantId == null )
                _entrantId = new PropertyPath<Long>(EntrantRequestGen.P_ENTRANT_ID, this);
            return _entrantId;
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Дата добавления заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(EntrantRequestGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Номер заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getRegNumber()
     */
        public PropertyPath<Integer> regNumber()
        {
            if(_regNumber == null )
                _regNumber = new PropertyPath<Integer>(EntrantRequestGen.P_REG_NUMBER, this);
            return _regNumber;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(EntrantRequestGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Забрал документы. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#isTakeAwayDocument()
     */
        public PropertyPath<Boolean> takeAwayDocument()
        {
            if(_takeAwayDocument == null )
                _takeAwayDocument = new PropertyPath<Boolean>(EntrantRequestGen.P_TAKE_AWAY_DOCUMENT, this);
            return _takeAwayDocument;
        }

    /**
     * @return Техническая комиссия.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getTechnicCommission()
     */
        public PropertyPath<String> technicCommission()
        {
            if(_technicCommission == null )
                _technicCommission = new PropertyPath<String>(EntrantRequestGen.P_TECHNIC_COMMISSION, this);
            return _technicCommission;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EntrantRequestGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Способ подачи оригиналов документов.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getMethodDeliveryDocs()
     */
        public MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> methodDeliveryDocs()
        {
            if(_methodDeliveryDocs == null )
                _methodDeliveryDocs = new MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs>(L_METHOD_DELIVERY_DOCS, this);
            return _methodDeliveryDocs;
        }

    /**
     * @return Способ возврата оригиналов документов.
     * @see ru.tandemservice.uniec.entity.entrant.EntrantRequest#getMethodReturnDocs()
     */
        public MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs> methodReturnDocs()
        {
            if(_methodReturnDocs == null )
                _methodReturnDocs = new MethodDeliveryNReturnDocs.Path<MethodDeliveryNReturnDocs>(L_METHOD_RETURN_DOCS, this);
            return _methodReturnDocs;
        }

        public Class getEntityClass()
        {
            return EntrantRequest.class;
        }

        public String getEntityName()
        {
            return "entrantRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
