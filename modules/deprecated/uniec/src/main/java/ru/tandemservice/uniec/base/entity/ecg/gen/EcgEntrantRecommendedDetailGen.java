package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionDetail;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рекомендованный абитуриент уточняющего распределения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgEntrantRecommendedDetailGen extends EntityBase
 implements INaturalIdentifiable<EcgEntrantRecommendedDetailGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail";
    public static final String ENTITY_NAME = "ecgEntrantRecommendedDetail";
    public static final int VERSION_HASH = 1992606979;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISTRIBUTION = "distribution";
    public static final String L_DIRECTION = "direction";
    public static final String L_TARGET_ADMISSION_KIND = "targetAdmissionKind";
    public static final String P_BRING_ORIGINAL = "bringOriginal";
    public static final String P_HAS_PRE_STUDENT = "hasPreStudent";

    private EcgDistributionDetail _distribution;     // Уточняющее распределение
    private RequestedEnrollmentDirection _direction;     // Выбранное направление приема
    private TargetAdmissionKind _targetAdmissionKind;     // Вид целевого приема
    private Boolean _bringOriginal;     // Принес оригиналы
    private Boolean _hasPreStudent;     // Есть предзачисление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Уточняющее распределение. Свойство не может быть null.
     */
    @NotNull
    public EcgDistributionDetail getDistribution()
    {
        return _distribution;
    }

    /**
     * @param distribution Уточняющее распределение. Свойство не может быть null.
     */
    public void setDistribution(EcgDistributionDetail distribution)
    {
        dirty(_distribution, distribution);
        _distribution = distribution;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getDirection()
    {
        return _direction;
    }

    /**
     * @param direction Выбранное направление приема. Свойство не может быть null.
     */
    public void setDirection(RequestedEnrollmentDirection direction)
    {
        dirty(_direction, direction);
        _direction = direction;
    }

    /**
     * @return Вид целевого приема.
     */
    public TargetAdmissionKind getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    /**
     * @param targetAdmissionKind Вид целевого приема.
     */
    public void setTargetAdmissionKind(TargetAdmissionKind targetAdmissionKind)
    {
        dirty(_targetAdmissionKind, targetAdmissionKind);
        _targetAdmissionKind = targetAdmissionKind;
    }

    /**
     * @return Принес оригиналы.
     */
    public Boolean getBringOriginal()
    {
        return _bringOriginal;
    }

    /**
     * @param bringOriginal Принес оригиналы.
     */
    public void setBringOriginal(Boolean bringOriginal)
    {
        dirty(_bringOriginal, bringOriginal);
        _bringOriginal = bringOriginal;
    }

    /**
     * @return Есть предзачисление.
     */
    public Boolean getHasPreStudent()
    {
        return _hasPreStudent;
    }

    /**
     * @param hasPreStudent Есть предзачисление.
     */
    public void setHasPreStudent(Boolean hasPreStudent)
    {
        dirty(_hasPreStudent, hasPreStudent);
        _hasPreStudent = hasPreStudent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgEntrantRecommendedDetailGen)
        {
            if (withNaturalIdProperties)
            {
                setDistribution(((EcgEntrantRecommendedDetail)another).getDistribution());
                setDirection(((EcgEntrantRecommendedDetail)another).getDirection());
            }
            setTargetAdmissionKind(((EcgEntrantRecommendedDetail)another).getTargetAdmissionKind());
            setBringOriginal(((EcgEntrantRecommendedDetail)another).getBringOriginal());
            setHasPreStudent(((EcgEntrantRecommendedDetail)another).getHasPreStudent());
        }
    }

    public INaturalId<EcgEntrantRecommendedDetailGen> getNaturalId()
    {
        return new NaturalId(getDistribution(), getDirection());
    }

    public static class NaturalId extends NaturalIdBase<EcgEntrantRecommendedDetailGen>
    {
        private static final String PROXY_NAME = "EcgEntrantRecommendedDetailNaturalProxy";

        private Long _distribution;
        private Long _direction;

        public NaturalId()
        {}

        public NaturalId(EcgDistributionDetail distribution, RequestedEnrollmentDirection direction)
        {
            _distribution = ((IEntity) distribution).getId();
            _direction = ((IEntity) direction).getId();
        }

        public Long getDistribution()
        {
            return _distribution;
        }

        public void setDistribution(Long distribution)
        {
            _distribution = distribution;
        }

        public Long getDirection()
        {
            return _direction;
        }

        public void setDirection(Long direction)
        {
            _direction = direction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgEntrantRecommendedDetailGen.NaturalId) ) return false;

            EcgEntrantRecommendedDetailGen.NaturalId that = (NaturalId) o;

            if( !equals(getDistribution(), that.getDistribution()) ) return false;
            if( !equals(getDirection(), that.getDirection()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDistribution());
            result = hashCode(result, getDirection());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDistribution());
            sb.append("/");
            sb.append(getDirection());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgEntrantRecommendedDetailGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgEntrantRecommendedDetail.class;
        }

        public T newInstance()
        {
            return (T) new EcgEntrantRecommendedDetail();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "distribution":
                    return obj.getDistribution();
                case "direction":
                    return obj.getDirection();
                case "targetAdmissionKind":
                    return obj.getTargetAdmissionKind();
                case "bringOriginal":
                    return obj.getBringOriginal();
                case "hasPreStudent":
                    return obj.getHasPreStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "distribution":
                    obj.setDistribution((EcgDistributionDetail) value);
                    return;
                case "direction":
                    obj.setDirection((RequestedEnrollmentDirection) value);
                    return;
                case "targetAdmissionKind":
                    obj.setTargetAdmissionKind((TargetAdmissionKind) value);
                    return;
                case "bringOriginal":
                    obj.setBringOriginal((Boolean) value);
                    return;
                case "hasPreStudent":
                    obj.setHasPreStudent((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "distribution":
                        return true;
                case "direction":
                        return true;
                case "targetAdmissionKind":
                        return true;
                case "bringOriginal":
                        return true;
                case "hasPreStudent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "distribution":
                    return true;
                case "direction":
                    return true;
                case "targetAdmissionKind":
                    return true;
                case "bringOriginal":
                    return true;
                case "hasPreStudent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "distribution":
                    return EcgDistributionDetail.class;
                case "direction":
                    return RequestedEnrollmentDirection.class;
                case "targetAdmissionKind":
                    return TargetAdmissionKind.class;
                case "bringOriginal":
                    return Boolean.class;
                case "hasPreStudent":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgEntrantRecommendedDetail> _dslPath = new Path<EcgEntrantRecommendedDetail>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgEntrantRecommendedDetail");
    }
            

    /**
     * @return Уточняющее распределение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getDistribution()
     */
    public static EcgDistributionDetail.Path<EcgDistributionDetail> distribution()
    {
        return _dslPath.distribution();
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> direction()
    {
        return _dslPath.direction();
    }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getTargetAdmissionKind()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
    {
        return _dslPath.targetAdmissionKind();
    }

    /**
     * @return Принес оригиналы.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getBringOriginal()
     */
    public static PropertyPath<Boolean> bringOriginal()
    {
        return _dslPath.bringOriginal();
    }

    /**
     * @return Есть предзачисление.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getHasPreStudent()
     */
    public static PropertyPath<Boolean> hasPreStudent()
    {
        return _dslPath.hasPreStudent();
    }

    public static class Path<E extends EcgEntrantRecommendedDetail> extends EntityPath<E>
    {
        private EcgDistributionDetail.Path<EcgDistributionDetail> _distribution;
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _direction;
        private TargetAdmissionKind.Path<TargetAdmissionKind> _targetAdmissionKind;
        private PropertyPath<Boolean> _bringOriginal;
        private PropertyPath<Boolean> _hasPreStudent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Уточняющее распределение. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getDistribution()
     */
        public EcgDistributionDetail.Path<EcgDistributionDetail> distribution()
        {
            if(_distribution == null )
                _distribution = new EcgDistributionDetail.Path<EcgDistributionDetail>(L_DISTRIBUTION, this);
            return _distribution;
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> direction()
        {
            if(_direction == null )
                _direction = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_DIRECTION, this);
            return _direction;
        }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getTargetAdmissionKind()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> targetAdmissionKind()
        {
            if(_targetAdmissionKind == null )
                _targetAdmissionKind = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_TARGET_ADMISSION_KIND, this);
            return _targetAdmissionKind;
        }

    /**
     * @return Принес оригиналы.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getBringOriginal()
     */
        public PropertyPath<Boolean> bringOriginal()
        {
            if(_bringOriginal == null )
                _bringOriginal = new PropertyPath<Boolean>(EcgEntrantRecommendedDetailGen.P_BRING_ORIGINAL, this);
            return _bringOriginal;
        }

    /**
     * @return Есть предзачисление.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommendedDetail#getHasPreStudent()
     */
        public PropertyPath<Boolean> hasPreStudent()
        {
            if(_hasPreStudent == null )
                _hasPreStudent = new PropertyPath<Boolean>(EcgEntrantRecommendedDetailGen.P_HAS_PRE_STUDENT, this);
            return _hasPreStudent;
        }

        public Class getEntityClass()
        {
            return EcgEntrantRecommendedDetail.class;
        }

        public String getEntityName()
        {
            return "ecgEntrantRecommendedDetail";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
