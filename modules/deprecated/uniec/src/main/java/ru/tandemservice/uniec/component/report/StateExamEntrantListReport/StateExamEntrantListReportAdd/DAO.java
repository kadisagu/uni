// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.StateExamEntrantListReport.StateExamEntrantListReportAdd;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonYesNoUIObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.report.StateExamEntrantListReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

/**
 * @author oleyba
 * @since 06.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(new Date());
        model.setEnrollmentCampaignStageList(Arrays.asList(
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS, "по ходу приема документов"),
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_EXAMS, "по результатам сдачи вступительных испытаний"),
                new IdentifiableWrapper((long) Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT, "по результатам зачисления")
        ));

        refreshCompetitionGroupModel(model);
        refreshTechnicCommissionModel(model);

        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setParallelList(CommonYesNoUIObject.createYesNoList());
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        StateExamEntrantListReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaignStage(model.getEnrollmentCampaignStage().getTitle());

        try
        {
            DatabaseFile databaseFile = new StateExamEntrantListReportBuilder(model, session).getContentFormat();
            session.save(databaseFile);
            report.setContent(databaseFile);
        }
        catch (ApplicationException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

        if (model.isTechnicCommissionActive())
            report.setTechnicCommission(model.getTechnicCommission().getTitle());
        if (model.isCompetitionGroupActive())
            report.setCompetitionGroup(model.getCompetitionGroup().getTitle());
        if (model.isCompensationTypeActive())
            report.setCompensationType(model.getCompensationType().getShortTitle());
        if (model.isStudentCategoryActive())
            report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        if (model.isQualificationActive())
            report.setQualification(UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchool(UniStringUtils.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_TITLE, "; "));
        if (model.isDevelopFormActive())
            report.setDevelopForm(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.P_TITLE, "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), DevelopTech.P_TITLE, "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, "; "));

        if (model.isParallelActive())
            report.setExcludeParallel(model.getParallel().isTrue());
        
        session.save(report);
    }

    private void refreshTechnicCommissionModel(Model model)
    {
        Criteria criteria = getSession().createCriteria(EntrantRequest.class, "entrantRequest");
        criteria.createAlias(EntrantRequest.L_ENTRANT, "entrant");
        criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        criteria.add(Restrictions.isNotNull("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION));
        criteria.setProjection(Projections.distinct(Projections.alias(Projections.property("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION), "technicCommission")));
        criteria.addOrder(Order.asc("technicCommission"));

        long id = 0;
        List<IdentifiableWrapper<?>> result = new ArrayList<>();

        for (Object technicCommission : criteria.list())
            result.add(new IdentifiableWrapper(id++, (String) technicCommission));

        model.setTechnicCommissionModel(new LazySimpleSelectModel<>(result));
    }

    private void refreshCompetitionGroupModel(Model model)
    {
        List<CompetitionGroup> competitionGroupList = getList(CompetitionGroup.class, CompetitionGroup.enrollmentCampaign().s(), model.getEnrollmentCampaign());
        Collections.sort(competitionGroupList, CompetitionGroup.TITLED_COMPARATOR);
        model.setCompetitionGroupList(competitionGroupList);
    }

    @Override
    public void refresh(Model model)
    {
        refreshTechnicCommissionModel(model);
        refreshCompetitionGroupModel(model);
    }
}
