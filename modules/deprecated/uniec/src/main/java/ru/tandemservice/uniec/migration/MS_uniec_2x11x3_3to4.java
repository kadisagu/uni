package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniec_2x11x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность entranceExamPhase
        DBTable dbt = new DBTable("entranceexamphase_t",
                                  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_entranceexamphase"),
                                  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                  new DBColumn("setting_id", DBType.LONG).setNullable(false),
                                  new DBColumn("passdate_p", DBType.TIMESTAMP).setNullable(false)
        );
        tool.createTable(dbt);

        short phaseCode = tool.entityCodes().ensure("entranceExamPhase");


        SQLSelectQuery selectQuery = new SQLSelectQuery()
                .from(SQLFrom.table("disciplinedatesetting_t", "st"))
                .column("st.id", "setting")
                .column("st.passdate_p", "date")
                .where("st.passdate_p is not null");

        Statement selectStatement = tool.getConnection().createStatement();
        selectStatement.execute(tool.getDialect().getSQLTranslator().toSql(selectQuery));

        ResultSet selectResult = selectStatement.getResultSet();
        HashMap<Long, Date> dateBySettingMap = new HashMap<>();
        while (selectResult.next())
            dateBySettingMap.put(selectResult.getLong("setting"), selectResult.getDate("date"));

        PreparedStatement insert = tool.prepareStatement("insert into entranceexamphase_t " +
                                                                 "(id,discriminator,setting_id,passdate_p) " +
                                                                 "values (?,?,?,?)");

        for (Map.Entry<Long, Date> entry : dateBySettingMap.entrySet())
        {
            Long phaseId = EntityIDGenerator.generateNewId(phaseCode);
            Long setting = entry.getKey();
            Date date = entry.getValue();
            insert.clearParameters();
            insert.setLong(1, phaseId);
            insert.setLong(2, phaseCode);
            insert.setLong(3, setting);
            insert.setDate(4, date);
            insert.executeUpdate();
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность disciplineDateSetting

        // удалено свойство passDate
        tool.dropColumn("disciplinedatesetting_t", "passdate_p");

        // создано обязательное свойство compensationType
        tool.createColumn("disciplinedatesetting_t", new DBColumn("compensationtype_id", DBType.LONG));
        long budget = (Long)tool.getUniqueResult("select ct.id from compensationtype_t ct where code_p = ?", "1");
        tool.executeUpdate("update disciplinedatesetting_t set compensationtype_id=? where compensationtype_id is null", budget);
        tool.setColumnNullable("disciplinedatesetting_t", "compensationtype_id", false);

        // создано обязательное свойство developForm
        tool.createColumn("disciplinedatesetting_t", new DBColumn("developform_id", DBType.LONG));
        long fullTime = (Long)tool.getUniqueResult("select ct.id from developform_t ct where code_p = ?", "1");
        tool.executeUpdate("update disciplinedatesetting_t set developform_id=? where developform_id is null", fullTime);
        tool.setColumnNullable("disciplinedatesetting_t", "developform_id", false);
    }
}