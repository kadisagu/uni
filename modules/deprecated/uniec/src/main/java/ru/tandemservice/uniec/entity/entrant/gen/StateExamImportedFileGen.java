package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.StateExamImportedFile;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Импортированный файл данных по ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StateExamImportedFileGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.StateExamImportedFile";
    public static final String ENTITY_NAME = "stateExamImportedFile";
    public static final int VERSION_HASH = -2132139187;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_IMPORT_DATE = "importDate";
    public static final String P_IMPORT_TYPE = "importType";
    public static final String L_IMPORTED_FILE = "importedFile";
    public static final String L_RESULT_FILE = "resultFile";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _importDate;     // Дата импорта
    private String _importType;     // Тип импорта
    private DatabaseFile _importedFile;     // Импортированный файл
    private DatabaseFile _resultFile;     // Результирующий файл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     */
    @NotNull
    public Date getImportDate()
    {
        return _importDate;
    }

    /**
     * @param importDate Дата импорта. Свойство не может быть null.
     */
    public void setImportDate(Date importDate)
    {
        dirty(_importDate, importDate);
        _importDate = importDate;
    }

    /**
     * @return Тип импорта. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getImportType()
    {
        return _importType;
    }

    /**
     * @param importType Тип импорта. Свойство не может быть null.
     */
    public void setImportType(String importType)
    {
        dirty(_importType, importType);
        _importType = importType;
    }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getImportedFile()
    {
        return _importedFile;
    }

    /**
     * @param importedFile Импортированный файл. Свойство не может быть null.
     */
    public void setImportedFile(DatabaseFile importedFile)
    {
        dirty(_importedFile, importedFile);
        _importedFile = importedFile;
    }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getResultFile()
    {
        return _resultFile;
    }

    /**
     * @param resultFile Результирующий файл. Свойство не может быть null.
     */
    public void setResultFile(DatabaseFile resultFile)
    {
        dirty(_resultFile, resultFile);
        _resultFile = resultFile;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StateExamImportedFileGen)
        {
            setEnrollmentCampaign(((StateExamImportedFile)another).getEnrollmentCampaign());
            setImportDate(((StateExamImportedFile)another).getImportDate());
            setImportType(((StateExamImportedFile)another).getImportType());
            setImportedFile(((StateExamImportedFile)another).getImportedFile());
            setResultFile(((StateExamImportedFile)another).getResultFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StateExamImportedFileGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StateExamImportedFile.class;
        }

        public T newInstance()
        {
            return (T) new StateExamImportedFile();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "importDate":
                    return obj.getImportDate();
                case "importType":
                    return obj.getImportType();
                case "importedFile":
                    return obj.getImportedFile();
                case "resultFile":
                    return obj.getResultFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "importDate":
                    obj.setImportDate((Date) value);
                    return;
                case "importType":
                    obj.setImportType((String) value);
                    return;
                case "importedFile":
                    obj.setImportedFile((DatabaseFile) value);
                    return;
                case "resultFile":
                    obj.setResultFile((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "importDate":
                        return true;
                case "importType":
                        return true;
                case "importedFile":
                        return true;
                case "resultFile":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "importDate":
                    return true;
                case "importType":
                    return true;
                case "importedFile":
                    return true;
                case "resultFile":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "importDate":
                    return Date.class;
                case "importType":
                    return String.class;
                case "importedFile":
                    return DatabaseFile.class;
                case "resultFile":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StateExamImportedFile> _dslPath = new Path<StateExamImportedFile>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StateExamImportedFile");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getImportDate()
     */
    public static PropertyPath<Date> importDate()
    {
        return _dslPath.importDate();
    }

    /**
     * @return Тип импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getImportType()
     */
    public static PropertyPath<String> importType()
    {
        return _dslPath.importType();
    }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getImportedFile()
     */
    public static DatabaseFile.Path<DatabaseFile> importedFile()
    {
        return _dslPath.importedFile();
    }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getResultFile()
     */
    public static DatabaseFile.Path<DatabaseFile> resultFile()
    {
        return _dslPath.resultFile();
    }

    public static class Path<E extends StateExamImportedFile> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _importDate;
        private PropertyPath<String> _importType;
        private DatabaseFile.Path<DatabaseFile> _importedFile;
        private DatabaseFile.Path<DatabaseFile> _resultFile;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getImportDate()
     */
        public PropertyPath<Date> importDate()
        {
            if(_importDate == null )
                _importDate = new PropertyPath<Date>(StateExamImportedFileGen.P_IMPORT_DATE, this);
            return _importDate;
        }

    /**
     * @return Тип импорта. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getImportType()
     */
        public PropertyPath<String> importType()
        {
            if(_importType == null )
                _importType = new PropertyPath<String>(StateExamImportedFileGen.P_IMPORT_TYPE, this);
            return _importType;
        }

    /**
     * @return Импортированный файл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getImportedFile()
     */
        public DatabaseFile.Path<DatabaseFile> importedFile()
        {
            if(_importedFile == null )
                _importedFile = new DatabaseFile.Path<DatabaseFile>(L_IMPORTED_FILE, this);
            return _importedFile;
        }

    /**
     * @return Результирующий файл. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.StateExamImportedFile#getResultFile()
     */
        public DatabaseFile.Path<DatabaseFile> resultFile()
        {
            if(_resultFile == null )
                _resultFile = new DatabaseFile.Path<DatabaseFile>(L_RESULT_FILE, this);
            return _resultFile;
        }

        public Class getEntityClass()
        {
            return StateExamImportedFile.class;
        }

        public String getEntityName()
        {
            return "stateExamImportedFile";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
