/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcSimpleDSHandler;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaDTO;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.codes.TargetAdmissionKindCodes;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Форма добавления/редактирования основного распределения
 *
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
@Input({
        @Bind(key = EcDistributionAddEditUI.DISTRIBUTION_DTO, binding = EcDistributionAddEditUI.DISTRIBUTION_DTO, required = true)
})
public class EcDistributionAddEditUI extends UIPresenter
{
    public static final String DISTRIBUTION_DTO = "distributionDTO";

    private EcgDistributionDTO _distributionDTO;

    private boolean _addForm;                          // форма добавления
    private int _wave;                                 // номер волны добавляемого или редактируемого распределения 
    private String _ecgItemDisplayName;                // конкурсная группа или направление подготовки (специальность)
    private String _distributionCategoryTitle;         // студент/слушатель или второе высшее
    private boolean _editDisabled;                     // true, если нельзя менять планы приема в распределении
    private List<TargetAdmissionKind> _taKindList;     // виды цп по которым следуем задать планы
    private TargetAdmissionKind _currentTaKind;
    private boolean _defaultTaKind;                    // true, если только один системный вид цп 
    private Map<Long, Integer> _quotaMap;              // id направления приема распределения -> план
    private Map<Long, Map<Long, Integer>> _taQuotaMap; // id направления приема распределения -> вид цп -> план
    private List<EnrollmentDirection> _directionList;  // список направлений приема входящих в распределение

    @Override
    public void onComponentRefresh()
    {
        _addForm = _distributionDTO.getPersistentId() == null;

        _ecgItemDisplayName = getConfig().getProperty(_distributionDTO.getEcgItem().getEnrollmentCampaign().isUseCompetitionGroup() ? "ui.ecgItem.competitionGroup" : "ui.ecgItem.educationLevelHighSchool");

        _distributionCategoryTitle = _distributionDTO.isSecondHighAdmission() ? EcDistributionManager.DISTRIBUTION_CATEGORY_SECOND_HIGH_ADMISSION.getTitle() : EcDistributionManager.DISTRIBUTION_CATEGORY_STUDENTS_AND_LISTENERS.getTitle();

        IEcgQuotaDTO quotaDTO;

        if (_addForm)
        {
            // получаем номер волны
            EcgDistribution maxDistribution = EcDistributionManager.instance().dao().getDistributionWithMaxWave(_distributionDTO.getConfigDTO());
            _wave = maxDistribution == null ? IEcDistributionDao.DISTRIBUTION_FIRST_WAVE : maxDistribution.getWave() + 1;

            // планы приема можно менять
            _editDisabled = false;

            // получаем предполагаемые планы приема
            quotaDTO = EcDistributionManager.instance().dao().getNextQuotaDTO(_distributionDTO.getConfigDTO());

            _directionList = EcDistributionManager.instance().dao().getDistributionDirectionList(_distributionDTO.getConfigDTO());
        } else
        {
            EcgDistribution distribution = DataAccessServices.dao().get(_distributionDTO.getPersistentId());

            // получаем номер волны
            _wave = distribution.getWave();

            // планы приема менять нельзя
            _editDisabled = distribution.getState().isLockedOrApproved();

            // получаем сохраненные планы приема
            quotaDTO = EcDistributionManager.instance().dao().getCurrentQuotaDTO(distribution);

            _directionList = EcDistributionManager.instance().dao().getDistributionDirectionList(distribution);
        }

        // виды цп
        _taKindList = quotaDTO.getTaKindList();

        // планы приема по направлениям
        _quotaMap = quotaDTO.getQuotaMap();

        // планы приема по видам цп 
        _taQuotaMap = quotaDTO.getTaQuotaMap();

        // true, если используется единственный вид цп и он по умолчанию
        _defaultTaKind = _taKindList.size() == 1 && _taKindList.get(0).getCode().equals(TargetAdmissionKindCodes.TA_DEFAULT);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EcDistributionAddEdit.DIRECTION_DS.equals(dataSource.getName()))
            dataSource.put(EcSimpleDSHandler.LIST, _directionList);
    }

    // SearchList binding

    public int getCurrentQuota()
    {
        Long id = ((EnrollmentDirection) getConfig().getDataSource(EcDistributionAddEdit.DIRECTION_DS).getCurrent()).getId();
        Integer quota = _quotaMap.get(id);
        return quota == null ? 0 : quota;
    }

    public void setCurrentQuota(int quota)
    {
        Long id = ((EnrollmentDirection) getConfig().getDataSource(EcDistributionAddEdit.DIRECTION_DS).getCurrent()).getId();
        _quotaMap.put(id, quota);
    }

    public int getCurrentQuotaTA()
    {
        Long id = ((EnrollmentDirection) getConfig().getDataSource(EcDistributionAddEdit.DIRECTION_DS).getCurrent()).getId();
        Map<Long, Integer> map = _taQuotaMap.get(id);
        if (map == null) return 0;
        Integer quota = map.get(_currentTaKind.getId());
        return quota == null ? 0 : quota;
    }

    public void setCurrentQuotaTA(int quota)
    {
        Long id = ((EnrollmentDirection) getConfig().getDataSource(EcDistributionAddEdit.DIRECTION_DS).getCurrent()).getId();
        Map<Long, Integer> map = _taQuotaMap.get(id);
        if (map == null)
            _taQuotaMap.put(id, map = new HashMap<>());
        map.put(_currentTaKind.getId(), quota);
    }

    // Getters & Setters

    public EcgDistributionDTO getDistributionDTO()
    {
        return _distributionDTO;
    }

    public void setDistributionDTO(EcgDistributionDTO distributionDTO)
    {
        _distributionDTO = distributionDTO;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public int getWave()
    {
        return _wave;
    }

    public void setWave(int wave)
    {
        _wave = wave;
    }

    public String getEcgItemDisplayName()
    {
        return _ecgItemDisplayName;
    }

    public void setEcgItemDisplayName(String ecgItemDisplayName)
    {
        _ecgItemDisplayName = ecgItemDisplayName;
    }

    public String getDistributionCategoryTitle()
    {
        return _distributionCategoryTitle;
    }

    public void setDistributionCategoryTitle(String distributionCategoryTitle)
    {
        _distributionCategoryTitle = distributionCategoryTitle;
    }

    public boolean isEditDisabled()
    {
        return _editDisabled;
    }

    public void setEditDisabled(boolean editDisabled)
    {
        _editDisabled = editDisabled;
    }

    public List<TargetAdmissionKind> getTaKindList()
    {
        return _taKindList;
    }

    public void setTaKindList(List<TargetAdmissionKind> taKindList)
    {
        _taKindList = taKindList;
    }

    public TargetAdmissionKind getCurrentTaKind()
    {
        return _currentTaKind;
    }

    public void setCurrentTaKind(TargetAdmissionKind currentTaKind)
    {
        _currentTaKind = currentTaKind;
    }

    public boolean isDefaultTaKind()
    {
        return _defaultTaKind;
    }

    public void setDefaultTaKind(boolean defaultTaKind)
    {
        _defaultTaKind = defaultTaKind;
    }

    public Map<Long, Integer> getQuotaMap()
    {
        return _quotaMap;
    }

    public void setQuotaMap(Map<Long, Integer> quotaMap)
    {
        _quotaMap = quotaMap;
    }

    public Map<Long, Map<Long, Integer>> getTaQuotaMap()
    {
        return _taQuotaMap;
    }

    public void setTaQuotaMap(Map<Long, Map<Long, Integer>> taQuotaMap)
    {
        _taQuotaMap = taQuotaMap;
    }

    public List<EnrollmentDirection> getDirectionList()
    {
        return _directionList;
    }

    public void setDirectionList(List<EnrollmentDirection> directionList)
    {
        _directionList = directionList;
    }

    // Listeners

    public void onClickApply()
    {
        if (_addForm)
            // создаем распределение на указанную конфигурацию указанной волны с указанными планами приема
            EcDistributionManager.instance().dao().saveDistribution(_distributionDTO.getConfigDTO(), _wave, _quotaMap, _taQuotaMap);
        else
            // обновляем название и планы приема указанного распределения
            EcDistributionManager.instance().dao().updateDistribution(_distributionDTO.getPersistentId(), _quotaMap, _taQuotaMap);

        deactivate();
    }
}
