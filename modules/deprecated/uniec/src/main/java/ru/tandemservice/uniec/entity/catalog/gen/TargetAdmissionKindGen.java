package ru.tandemservice.uniec.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид целевого приема
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TargetAdmissionKindGen extends EntityBase
 implements INaturalIdentifiable<TargetAdmissionKindGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind";
    public static final String ENTITY_NAME = "targetAdmissionKind";
    public static final int VERSION_HASH = -1115092808;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_PRIORITY = "priority";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String L_PARENT = "parent";
    public static final String P_TITLE = "title";
    public static final String P_FULL_HIERARHY_TITLE = "fullHierarhyTitle";

    private String _code;     // Системный код
    private int _priority;     // Приоритет
    private String _shortTitle;     // Сокращенное название
    private TargetAdmissionKind _parent;     // Вид целевого приема
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Вид целевого приема.
     */
    public TargetAdmissionKind getParent()
    {
        return _parent;
    }

    /**
     * @param parent Вид целевого приема.
     */
    public void setParent(TargetAdmissionKind parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TargetAdmissionKindGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((TargetAdmissionKind)another).getCode());
            }
            setPriority(((TargetAdmissionKind)another).getPriority());
            setShortTitle(((TargetAdmissionKind)another).getShortTitle());
            setParent(((TargetAdmissionKind)another).getParent());
            setTitle(((TargetAdmissionKind)another).getTitle());
        }
    }

    public INaturalId<TargetAdmissionKindGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<TargetAdmissionKindGen>
    {
        private static final String PROXY_NAME = "TargetAdmissionKindNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TargetAdmissionKindGen.NaturalId) ) return false;

            TargetAdmissionKindGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TargetAdmissionKindGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TargetAdmissionKind.class;
        }

        public T newInstance()
        {
            return (T) new TargetAdmissionKind();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "priority":
                    return obj.getPriority();
                case "shortTitle":
                    return obj.getShortTitle();
                case "parent":
                    return obj.getParent();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "parent":
                    obj.setParent((TargetAdmissionKind) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "priority":
                        return true;
                case "shortTitle":
                        return true;
                case "parent":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "priority":
                    return true;
                case "shortTitle":
                    return true;
                case "parent":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "shortTitle":
                    return String.class;
                case "parent":
                    return TargetAdmissionKind.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TargetAdmissionKind> _dslPath = new Path<TargetAdmissionKind>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TargetAdmissionKind");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getParent()
     */
    public static TargetAdmissionKind.Path<TargetAdmissionKind> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getFullHierarhyTitle()
     */
    public static SupportedPropertyPath<String> fullHierarhyTitle()
    {
        return _dslPath.fullHierarhyTitle();
    }

    public static class Path<E extends TargetAdmissionKind> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _shortTitle;
        private TargetAdmissionKind.Path<TargetAdmissionKind> _parent;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _fullHierarhyTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(TargetAdmissionKindGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(TargetAdmissionKindGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(TargetAdmissionKindGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Вид целевого приема.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getParent()
     */
        public TargetAdmissionKind.Path<TargetAdmissionKind> parent()
        {
            if(_parent == null )
                _parent = new TargetAdmissionKind.Path<TargetAdmissionKind>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(TargetAdmissionKindGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind#getFullHierarhyTitle()
     */
        public SupportedPropertyPath<String> fullHierarhyTitle()
        {
            if(_fullHierarhyTitle == null )
                _fullHierarhyTitle = new SupportedPropertyPath<String>(TargetAdmissionKindGen.P_FULL_HIERARHY_TITLE, this);
            return _fullHierarhyTitle;
        }

        public Class getEntityClass()
        {
            return TargetAdmissionKind.class;
        }

        public String getEntityName()
        {
            return "targetAdmissionKind";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullHierarhyTitle();
}
