package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.Included;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add.ModelBase;
import ru.tandemservice.uniec.entity.ecg.EcgDistribObject;

/**
 * @author vdanilov
 */
@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="distrib.id")
})
public class Model extends ModelBase {
    private EcgDistribObject distrib = new EcgDistribObject();
    public EcgDistribObject getDistrib() { return this.distrib; }
    public void setDistrib(final EcgDistribObject distrib) { this.distrib = distrib; }


}
