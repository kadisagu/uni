/* $Id: Controller.java 13244 2010-06-16 17:06:00Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.report.StateExamEnrollmentReport2010;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * @author vip_delete
 * @since 20.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StateExamEnrollmentReport2010> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Сводка")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", StateExamEnrollmentReport2010.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Отчетный период", StateExamEnrollmentReport2010.P_PERIOD_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Печатная форма", StateExamEnrollmentReport2010.P_PRINT_FORM).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Стадия приемной кампании", StateExamEnrollmentReport2010.P_ENROLLMENT_CAMP_STAGE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", StateExamEnrollmentReport2010.P_QUALIFICATION_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", StateExamEnrollmentReport2010.developForm().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", StateExamEnrollmentReport2010.P_DEVELOP_CONDITION_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Вид возмещения затрат", StateExamEnrollmentReport2010.compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Категория поступающего", StateExamEnrollmentReport2010.studentCategoryTitle().s()).setOrderable(false).setClickable(false));

        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printUniecStorableReport").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить сводку от «{0}»?", StateExamEnrollmentReport2010.P_FORMING_DATE).setPermissionKey("deleteUniecStorableReport"));
        dataSource.setOrder(StateExamEnrollmentReport2010.P_FORMING_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniec.component.report.stateExamEnrollmentResults2010.StateExamEnrollmentResultsAdd"));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.saveSettings();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        EntrantFilterUtil.resetEnrollmentCampaignFilter(getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new ParametersMap()
                .add("reportId", component.getListenerParameter())
                .add("extension", "xls")
        ));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
