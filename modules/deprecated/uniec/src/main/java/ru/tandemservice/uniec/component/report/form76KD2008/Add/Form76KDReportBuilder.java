/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.form76KD2008.Add;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.fias.base.entity.codes.AddressCountryTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.Form76KD2008Report;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 07.08.2009
 */
class Form76KDReportBuilder
{
    private Session _session;
    private Model _model;

    Form76KDReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        DQLSelectBuilder rowBuilder = new DQLSelectBuilder()
                .column(new DQLSelectBuilder().fromDataSource(getT1().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT2().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT3().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT4().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT5().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT6().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT21().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT22().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT23().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT24().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT25().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT26().buildQuery(), "t").buildCountQuery())
                .column(new DQLSelectBuilder().fromDataSource(getT7().buildQuery(), "t").buildCountQuery())
                .column(getT8().buildQuery());

        Object[] rowData = (Object[]) rowBuilder.createStatement(new DQLExecutionContext(_session)).uniqueResult();

        Map<String, String> code2Title = new HashMap<String, String>();
        code2Title.put("1", "очную");
        code2Title.put("2", "заочную");
        code2Title.put("3", "очно-заочную");
        code2Title.put("4", "экстернат");
        code2Title.put("5", "самостоятельное обучение и итоговая аттестация");

        RtfInjectModifier injectModifier = new RtfInjectModifier()
                .put("vuz", TopOrgUnit.getInstance().getTitle())
                .put("year", _model.getReport().getEnrollmentCampaign().getEducationYear().getTitle())
                .put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()))
                .put("developForm", code2Title.get(_model.getReport().getDevelopForm().getCode()))
                .put("T1", rowData[0].toString())
                .put("T2", rowData[1].toString())
                .put("T3", rowData[2].toString())
                .put("T4", rowData[3].toString())
                .put("T5", rowData[4].toString())
                .put("T6", rowData[5].toString())
                .put("T21", rowData[6].toString())
                .put("T22", rowData[7].toString())
                .put("T23", rowData[8].toString())
                .put("T24", rowData[9].toString())
                .put("T25", rowData[10].toString())
                .put("T26", rowData[11].toString())
                .put("T7", rowData[12].toString())
                .put("T8", rowData[13] != null ? rowData[13].toString() : "0");

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_FORM_76KD_2008);

        return RtfUtil.toByteArray(templateDocument.getCurrentTemplate(), injectModifier, null);
    }

    private DQLSelectBuilder getT1()
    {
        return getRequestedBuilder()
                .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST))
                .group(property("d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE));
    }

    private DQLSelectBuilder getT2()
    {
        return getRequestedBuilder()
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct")
                .where(or(
                        eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)),
                        eq(property("d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE))
                ))
                .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST));
    }

    private DQLSelectBuilder getT3()
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder();
        subBuilder.fromEntity(RequestedEnrollmentDirection.class, "sub_d");
        subBuilder.column(property("sub_d.id"));
        subBuilder.joinPath(DQLJoinType.inner, "sub_d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "sub_ed");
        subBuilder.joinPath(DQLJoinType.inner, "sub_ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "sub_ou");
        subBuilder.joinPath(DQLJoinType.inner, "sub_ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "sub_hs");
        subBuilder.joinPath(DQLJoinType.inner, "sub_hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "sub_el");
        subBuilder.joinPath(DQLJoinType.inner, "sub_el." + EducationLevels.L_QUALIFICATION, "sub_q");
        subBuilder.where(eq(property("sub_d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST), property("request.id")));
        subBuilder.where(eq(property("sub_d." + RequestedEnrollmentDirection.P_TARGET_ADMISSION), value(Boolean.TRUE)));

        return getRequestedBuilder()
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct")
                .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
                .where(not(exists(subBuilder.buildQuery())))
                .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST));
    }

    private DQLSelectBuilder getT4()
    {
        return getRequestedBuilder()
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
                .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                        eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
                .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)))
                .group(property("d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST))
                .group(property("d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE));
    }

    private DQLSelectBuilder getT5()
    {
        return getT4()
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct")
                .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)));
    }

    private DQLSelectBuilder getT6()
    {
        return getT1()
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
                .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)));
    }

    private DQLSelectBuilder getT21()
    {
        return getPreliminaryBuilder();
    }

    private DQLSelectBuilder getT22()
    {
        return getPreliminaryBuilder()
                .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
                .where(or(
                        eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)),
                        eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.TRUE))
                ));
    }

    private DQLSelectBuilder getT23()
    {
        return getPreliminaryBuilder()
                .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
                .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
                .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.FALSE)));
    }

    private DQLSelectBuilder getT24()
    {
        return getPreliminaryBuilder()
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.left, "card." + IdentityCard.L_ADDRESS, "baseAddress")
                .joinEntity("baseAddress", DQLJoinType.inner, AddressDetailed.class, "detailedAddress",
                        eq(AddressBase.id().fromAlias("baseAddress"), AddressDetailed.id().fromAlias("detailedAddress")))
                .where(eq(property("detailedAddress", AddressDetailed.country().countryType().code()), value(AddressCountryTypeCodes.NEAR)));
    }

    private DQLSelectBuilder getT25()
    {
        return getT24()
                .joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, "ct")
                .where(eq(property("ct." + CompensationType.P_CODE), value(UniDefines.COMPENSATION_TYPE_CONTRACT)))
                .where(eq(property("p." + PreliminaryEnrollmentStudent.P_TARGET_ADMISSION), value(Boolean.FALSE)));
    }

    private DQLSelectBuilder getT26()
    {
        return getPreliminaryBuilder()
                .joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person")
                .joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card")
                .joinPath(DQLJoinType.inner, "card." + IdentityCard.L_SEX, "sex")
                .where(eq(property("sex." + Sex.P_CODE), value(SexCodes.MALE)));
    }

    private DQLSelectBuilder getT7()
    {
        return getT1()
                .joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state")
                .where(notIn(property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY,
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )));
    }

    private DQLSelectBuilder getT8()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(DQLFunctions.sum(property("ed." + EnrollmentDirection.P_MINISTERIAL_PLAN)));
        builder.fromEntity(EnrollmentDirection.class, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        builder.where(eq(property("ed." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));

        patchEduOrgUnit(builder);

        return builder;
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        Form76KD2008Report report = _model.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");

        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(ne(property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(report.getEnrollmentCampaign())));

        if (_model.isStudentCategoryActive())
            builder.where(in(property("d." + RequestedEnrollmentDirection.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));

        patchEduOrgUnit(builder);

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        Form76KD2008Report report = _model.getReport();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.column(value("0"), "x");
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");

        builder.where(betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(report.getEnrollmentCampaign())));

        if (_model.isStudentCategoryActive())
            builder.where(in(property("p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));

        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.where(eq(property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), value(Boolean.FALSE)));

        patchEduOrgUnit(builder);

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder)
    {
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(eq(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), value(_model.getReport().getDevelopForm())));
        if (_model.isDevelopConditionActive())
            builder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isQualificationActive())
            builder.where(in(property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
    }
}
