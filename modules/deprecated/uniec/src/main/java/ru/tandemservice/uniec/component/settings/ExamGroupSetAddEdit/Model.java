/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupSetAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;

/**
 * @author vip_delete
 * @since 10.06.2009
 */
@Input({
        @Bind(key = "examGroupSetId", binding = "examGroupSet.id")
})
public class Model implements IEnrollmentCampaignSelectModel
{
    private ExamGroupSet _examGroupSet = new ExamGroupSet();
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _examGroupSet.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _examGroupSet.setEnrollmentCampaign(enrollmentCampaign);
    }

    // Getters & Setters

    public ExamGroupSet getExamGroupSet()
    {
        return _examGroupSet;
    }

    public void setExamGroupSet(ExamGroupSet examGroupSet)
    {
        _examGroupSet = examGroupSet;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }
}
