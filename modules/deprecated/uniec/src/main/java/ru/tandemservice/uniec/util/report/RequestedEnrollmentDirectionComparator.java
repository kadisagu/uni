/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.util.report;

import java.util.Comparator;
import java.util.Map;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author agolubenko
 * @since 16.07.2009
 */
public class RequestedEnrollmentDirectionComparator implements Comparator<IReportRow>
{
    private Map<CompetitionKind, Integer> _competitionKindPriorities;

    /**
     * @param competitionKindPriorities приоритеты видов конкурса (по ключу null должен лежать "Целевой прием")
     */
    public RequestedEnrollmentDirectionComparator(Map<CompetitionKind, Integer> competitionKindPriorities)
    {
        _competitionKindPriorities = competitionKindPriorities;
    }

    @Override
    public int compare(IReportRow o1, IReportRow o2)
    {
        // получаем выбранные направления
        RequestedEnrollmentDirection r1 = o1.getRequestedEnrollmentDirection();
        RequestedEnrollmentDirection r2 = o2.getRequestedEnrollmentDirection();

        int result;

        // если оба по целевому приему
        if (r1.isTargetAdmission() && r2.isTargetAdmission())
        {
            // то сравнить по приоритетам целевого приема
            result = TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR.compare(r1.getTargetAdmissionKind(), r2.getTargetAdmissionKind());

            // если одинаковы
            if (result == 0)
            {
                // то сравнить по приоритетам видов конкурса внутри вида целевого приема
                result = _competitionKindPriorities.get(r1.getCompetitionKind()) - _competitionKindPriorities.get(r2.getCompetitionKind());
            }
        }
        // иначе
        else
        {
            // получаем виды конкурса, для целевого приема — null
            CompetitionKind c1 = getCompetitionKind(r1);
            CompetitionKind c2 = getCompetitionKind(r2);

            // сравнить по приоритетам видов конкурса
            result = _competitionKindPriorities.get(c1) - _competitionKindPriorities.get(c2);
        }

        // сначала сравниваем по сумме баллов
        if (result == 0)
            result = - UniBaseUtils.compare(o1.getFinalMark(), o2.getFinalMark(), true);

        // теперь по баллам за профильное вступительное
        if (result == 0)
            result = - UniBaseUtils.compare(o1.getProfileMark(), o2.getProfileMark(), true);

        // теперь по призаку "закончил профильное обр. учреждение"
        if (result == 0)
            result = - o1.isGraduatedProfileEduInstitution().compareTo(o2.isGraduatedProfileEduInstitution());

        // теперь по среднему баллу аттестата
        if (result == 0)
            result = - UniBaseUtils.compare(o1.getCertificateAverageMark(), o2.getCertificateAverageMark(), true);

        // все осмысленное кончилось, теперь по фио
        if (result == 0)
            result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());

        return result;
    }

    private CompetitionKind getCompetitionKind(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return (!requestedEnrollmentDirection.isTargetAdmission()) ? requestedEnrollmentDirection.getCompetitionKind() : null;
    }
}
