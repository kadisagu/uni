/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.ExamGroupSetList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.ExamGroupSet;

/**
 * @author vip_delete
 * @since 10.06.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ExamGroupSet> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", ExamGroupSet.P_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Размер группы", ExamGroupSet.P_SIZE).setOrderable(false).setClickable(false));

        ToggleColumn openColumn = new ToggleColumn("", Model.P_OPEN, "Набор открыт", "Набор закрыт")
        .toggleOffListener("onClickClose", "Закрыть набор «{0}»?", new String[]{ExamGroupSet.P_TITLE})
        .toggleOnListener("onClickOpen", "Открыть набор «{0}»?", new String[]{ExamGroupSet.P_TITLE});

        dataSource.addColumn(openColumn);
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить набор «{0}»?", ExamGroupSet.P_TITLE));

        model.setDataSource(dataSource);
    }

    public void onClickClose(IBusinessComponent component)
    {
        getDao().updateExamGroupSetStatus((Long) component.getListenerParameter(), false);
    }

    public void onClickOpen(IBusinessComponent component)
    {
        getDao().updateExamGroupSetStatus((Long) component.getListenerParameter(), true);
    }

    public void onClickAdd(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniec.component.settings.ExamGroupSetAddEdit", new ParametersMap()
                .add("examGroupSetId", null)
        ));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniec.component.settings.ExamGroupSetAddEdit", new ParametersMap()
                .add("examGroupSetId", component.getListenerParameter())
        ));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
