/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantTransfer;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author agolubenko
 * @since 02.04.2009
 */
@Input(@Bind(key = "enrollmentDirectionIds"))
@Output( { @Bind(key = "entrantId", binding = "entrant.id"), @Bind(key = "enrollmentDirectionId", binding = "enrollmentDirection.id"), @Bind(key = "orderTypeId", binding = "orderType.id") })
public class Model
{
    private Entrant _entrant;

    private List<Long> _enrollmentDirectionIds = new ArrayList<Long>();
    private List<EnrollmentDirection> _enrollmentDirectionList;
    private EnrollmentDirection _enrollmentDirection;

    private List<EntrantEnrollmentOrderType> _orderTypeList;
    private EntrantEnrollmentOrderType _orderType;

    private String _lastNameFilter;
    private String _firstNameFilter;
    private String _middleNameFilter;

    private DynamicListDataSource<Entrant> _dataSource;

    public List<Long> getEnrollmentDirectionIds()
    {
        return _enrollmentDirectionIds;
    }

    public void setEnrollmentDirectionIds(List<Long> enrollmentDirectionIds)
    {
        _enrollmentDirectionIds = enrollmentDirectionIds;
    }

    public List<EnrollmentDirection> getEnrollmentDirectionList()
    {
        return _enrollmentDirectionList;
    }

    public void setEnrollmentDirectionList(List<EnrollmentDirection> enrollmentDirectionList)
    {
        _enrollmentDirectionList = enrollmentDirectionList;
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public DynamicListDataSource<Entrant> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Entrant> dataSource)
    {
        _dataSource = dataSource;
    }

    public List<EntrantEnrollmentOrderType> getOrderTypeList()
    {
        return _orderTypeList;
    }

    public void setOrderTypeList(List<EntrantEnrollmentOrderType> orderTypeList)
    {
        _orderTypeList = orderTypeList;
    }

    public EntrantEnrollmentOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(EntrantEnrollmentOrderType orderType)
    {
        _orderType = orderType;
    }

    public String getLastNameFilter()
    {
        return _lastNameFilter;
    }

    public void setLastNameFilter(String lastNameFilter)
    {
        _lastNameFilter = lastNameFilter;
    }

    public String getFirstNameFilter()
    {
        return _firstNameFilter;
    }

    public void setFirstNameFilter(String firstNameFilter)
    {
        _firstNameFilter = firstNameFilter;
    }

    public String getMiddleNameFilter()
    {
        return _middleNameFilter;
    }

    public void setMiddleNameFilter(String middleNameFilter)
    {
        _middleNameFilter = middleNameFilter;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }
}