// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.settings.UsedTargetAdmissionKindList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author oleyba
 * @since 07.07.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareListDataSource(component);
    }

    @SuppressWarnings({"unchecked"})
    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", TargetAdmissionKind.P_TITLE).setTreeColumn(true).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Использовать", Model.P_IN_USE).setListener("onClicToggleUse"));
        model.setDataSource(dataSource);
    }

    public void onClicToggleUse(IBusinessComponent component)
    {
        getDao().update(getModel(component), (Long) component.getListenerParameter());
    }

    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        component.saveSettings();
        prepareListDataSource(component);
        getDao().prepareExternalOrgUnitOptionGroup(getModel(component));
    }
    
    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
    }
}