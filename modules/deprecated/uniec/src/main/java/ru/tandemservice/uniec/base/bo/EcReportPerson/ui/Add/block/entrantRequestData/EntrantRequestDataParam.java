package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrolmentRecommendation;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
public class EntrantRequestDataParam implements IReportDQLModifier
{
    private IReportParam<Date> _reqDateFrom = new ReportParam<Date>();
    private IReportParam<Date> _reqDateTo = new ReportParam<Date>();
    private IReportParam<List<Qualifications>> _qualification = new ReportParam<List<Qualifications>>();
    private IReportParam<List<OrgUnit>> _formativeOrgUnit = new ReportParam<List<OrgUnit>>();
    private IReportParam<List<OrgUnit>> _territorialOrgUnit = new ReportParam<List<OrgUnit>>();
    private IReportParam<List<EducationLevelsHighSchool>> _educationOrgUnit = new ReportParam<List<EducationLevelsHighSchool>>();
    private IReportParam<List<DevelopForm>> _developForm = new ReportParam<List<DevelopForm>>();
    private IReportParam<List<DevelopCondition>> _developCondition = new ReportParam<List<DevelopCondition>>();
    private IReportParam<List<DevelopTech>> _developTech = new ReportParam<List<DevelopTech>>();
    private IReportParam<List<DevelopPeriod>> _developPeriod = new ReportParam<List<DevelopPeriod>>();
    private IReportParam<List<CompetitionKind>> _competitionKind = new ReportParam<List<CompetitionKind>>();
    private IReportParam<CompensationType> _compensationType = new ReportParam<CompensationType>();
    private IReportParam<List<StudentCategory>> _studentCategory = new ReportParam<List<StudentCategory>>();
    private IReportParam<IdentifiableWrapper> _targetAdmission = new ReportParam<IdentifiableWrapper>();
    private IReportParam<List<TargetAdmissionKind>> _targetAdmissionKind = new ReportParam<List<TargetAdmissionKind>>();
    private IReportParam<List<EntrantState>> _entrantState = new ReportParam<List<EntrantState>>();
    private IReportParam<IdentifiableWrapper> _originalDocuments = new ReportParam<IdentifiableWrapper>();
    private IReportParam<String> _technicCommission = new ReportParam<String>();
    private IReportParam<String> _entrReqComment = new ReportParam<String>();
    private IReportParam<String> _reqEnrDirectionComment = new ReportParam<String>();
    private IReportParam<DataWrapper> _agree4Enrollment = new ReportParam<DataWrapper>();
    private IReportParam<DataWrapper> _recommendations = new ReportParam<DataWrapper>();

    private String alias(ReportDQL dql)
    {
        // соединяем абитуриента
        String entrantAlias = dql.innerJoinEntity(Entrant.class, Entrant.person());

        // соединяем заявление
        String requestAlias = dql.innerJoinEntity(entrantAlias, EntrantRequest.class, EntrantRequest.entrant());

        // соединяем выбранное направление приема
        String directionAlias = dql.innerJoinEntity(requestAlias, RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest());

        // добавляем сортировки
        dql.order(entrantAlias, Entrant.enrollmentCampaign().id());
        dql.order(requestAlias, EntrantRequest.regDate());
        dql.order(requestAlias, EntrantRequest.id());
        dql.order(directionAlias, RequestedEnrollmentDirection.priority());

        return directionAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_reqDateFrom.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.reqDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_reqDateFrom.getData()));

            dql.builder.where(ge(property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias(alias(dql))), valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(_reqDateFrom.getData()))));
        }

        if (_reqDateTo.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.reqDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_reqDateTo.getData()));

            Date regDateTo = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(_reqDateTo.getData(), Calendar.DAY_OF_YEAR, 1));
            dql.builder.where(lt(property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias(alias(dql))), valueTimestamp(regDateTo)));
        }

        if (_qualification.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.qualification", CommonBaseStringUtil.join(_qualification.getData(), Qualifications.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias(alias(dql))), _qualification.getData()));
        }

        if (_formativeOrgUnit.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.formativeOrgUnit", CommonBaseStringUtil.join(_formativeOrgUnit.getData(), OrgUnit.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias(alias(dql))), _formativeOrgUnit.getData()));
        }

        if (_territorialOrgUnit.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.territorialOrgUnit", CommonBaseStringUtil.join(_territorialOrgUnit.getData(), OrgUnit.P_TERRITORIAL_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().fromAlias(alias(dql))), _territorialOrgUnit.getData()));
        }

        if (_educationOrgUnit.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.educationOrgUnit", CommonBaseStringUtil.join(_educationOrgUnit.getData(), EducationLevelsHighSchool.P_PRINT_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias(alias(dql))), _educationOrgUnit.getData()));
        }

        if (_developForm.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.developForm", CommonBaseStringUtil.join(_developForm.getData(), DevelopForm.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias(alias(dql))), _developForm.getData()));
        }

        if (_developCondition.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.developCondition", CommonBaseStringUtil.join(_developCondition.getData(), DevelopCondition.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().fromAlias(alias(dql))), _developCondition.getData()));
        }

        if (_developTech.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.developTech", CommonBaseStringUtil.join(_developTech.getData(), DevelopTech.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech().fromAlias(alias(dql))), _developTech.getData()));
        }

        if (_developPeriod.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.developPeriod", CommonBaseStringUtil.join(_developPeriod.getData(), DevelopPeriod.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().fromAlias(alias(dql))), _developPeriod.getData()));
        }

        if (_competitionKind.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.competitionKind", CommonBaseStringUtil.join(_competitionKind.getData(), CompetitionKind.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.competitionKind().fromAlias(alias(dql))), _competitionKind.getData()));
        }

        if (_compensationType.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.compensationType", _compensationType.getData().getShortTitle());

            dql.builder.where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias(alias(dql))), value(_compensationType.getData())));
        }

        if (_studentCategory.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.studentCategory", CommonBaseStringUtil.join(_studentCategory.getData(), StudentCategory.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.studentCategory().fromAlias(alias(dql))), _studentCategory.getData()));
        }

        if (_targetAdmission.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.targetAdmission", _targetAdmission.getData().getTitle());

            dql.builder.where(eq(property(RequestedEnrollmentDirection.targetAdmission().fromAlias(alias(dql))), value(EntrantRequestDataBlock.TARGET_ADMISSION_YES.equals(_targetAdmission.getData().getId()))));
        }

        if (_targetAdmissionKind.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.targetAdmissionKind", CommonBaseStringUtil.join(_targetAdmissionKind.getData(), TargetAdmissionKind.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.targetAdmissionKind().fromAlias(alias(dql))), _targetAdmissionKind.getData()));
        }

        if (_entrantState.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.entrantState", CommonBaseStringUtil.join(_entrantState.getData(), EntrantState.P_TITLE, ", "));

            dql.builder.where(in(property(RequestedEnrollmentDirection.state().fromAlias(alias(dql))), _entrantState.getData()));
        }

        if (_originalDocuments.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.originalDocuments", _originalDocuments.getData().getTitle());

            dql.builder.where(eq(property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias(alias(dql))), value(EntrantRequestDataBlock.ORIGINAL_DOCUMENT_YES.equals(_originalDocuments.getData().getId()))));
        }

        if (_technicCommission.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.technicCommission", _technicCommission.getData());

            dql.builder.where(likeUpper(property(RequestedEnrollmentDirection.entrantRequest().technicCommission().fromAlias(alias(dql))), value(CoreStringUtils.escapeLike(_technicCommission.getData()))));
        }

        if (_entrReqComment.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.entrReqComment", _entrReqComment.getData());
            
            dql.builder.where(likeUpper(property(RequestedEnrollmentDirection.entrantRequest().comment().fromAlias(alias(dql))), value(CoreStringUtils.escapeLike(_entrReqComment.getData()))));
        }

        if (_reqEnrDirectionComment.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.reqEnrDirectionComment", _reqEnrDirectionComment.getData());
            
            dql.builder.where(likeUpper(property(RequestedEnrollmentDirection.comment().fromAlias(alias(dql))), value(CoreStringUtils.escapeLike(_reqEnrDirectionComment.getData()))));
        }

        if (_agree4Enrollment.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.agree4Enrollment", _agree4Enrollment.getData().getTitle());

            dql.builder.where(likeUpper(property(RequestedEnrollmentDirection.agree4Enrollment().fromAlias(alias(dql))),
                                        value(TwinComboDataSourceHandler.getSelectedValue(_agree4Enrollment.getData()))));
        }

        if (_recommendations.isActive())
        {
            printInfo.addPrintParam(EcReportPersonAddUI.ENTRANT_SCHEET, "entrantRequestData.recommendations", _recommendations.getData().getTitle());
            Boolean recommendations = TwinComboDataSourceHandler.getSelectedValue(_recommendations.getData());
            IDQLExpression expression = exists(EntrantEnrolmentRecommendation.class,
                                               EntrantEnrolmentRecommendation.entrant().s(),
                                               property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias(alias(dql))));
            if (recommendations)
                dql.builder.where(expression);
            else
                dql.builder.where(not(expression));
        }
    }

    // Getters

    public IReportParam<Date> getReqDateFrom()
    {
        return _reqDateFrom;
    }

    public IReportParam<Date> getReqDateTo()
    {
        return _reqDateTo;
    }

    public IReportParam<List<Qualifications>> getQualification()
    {
        return _qualification;
    }

    public IReportParam<List<OrgUnit>> getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public IReportParam<List<OrgUnit>> getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public IReportParam<List<EducationLevelsHighSchool>> getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public IReportParam<List<DevelopForm>> getDevelopForm()
    {
        return _developForm;
    }

    public IReportParam<List<DevelopCondition>> getDevelopCondition()
    {
        return _developCondition;
    }

    public IReportParam<List<DevelopTech>> getDevelopTech()
    {
        return _developTech;
    }

    public IReportParam<List<DevelopPeriod>> getDevelopPeriod()
    {
        return _developPeriod;
    }

    public IReportParam<List<CompetitionKind>> getCompetitionKind()
    {
        return _competitionKind;
    }

    public IReportParam<CompensationType> getCompensationType()
    {
        return _compensationType;
    }

    public IReportParam<List<StudentCategory>> getStudentCategory()
    {
        return _studentCategory;
    }

    public IReportParam<IdentifiableWrapper> getTargetAdmission()
    {
        return _targetAdmission;
    }

    public IReportParam<List<TargetAdmissionKind>> getTargetAdmissionKind()
    {
        return _targetAdmissionKind;
    }

    public IReportParam<List<EntrantState>> getEntrantState()
    {
        return _entrantState;
    }

    public IReportParam<IdentifiableWrapper> getOriginalDocuments()
    {
        return _originalDocuments;
    }

    public IReportParam<String> getTechnicCommission()
    {
        return _technicCommission;
    }

    public IReportParam<String> getEntrReqComment()
    {
        return _entrReqComment;
    }

    public IReportParam<String> getReqEnrDirectionComment()
    {
        return _reqEnrDirectionComment;
    }

    public IReportParam<DataWrapper> getAgree4Enrollment()
    {
        return _agree4Enrollment;
    }

    public IReportParam<DataWrapper> getRecommendations()
    {
        return _recommendations;
    }
}
