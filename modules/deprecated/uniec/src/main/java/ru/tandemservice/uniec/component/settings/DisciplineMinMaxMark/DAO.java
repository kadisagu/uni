/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

/**
 * @author agolubenko
 * @since 13.02.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setStateExamSubjectList(getCatalogItemList(StateExamSubject.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List<Discipline2RealizationWayRelation> disciplines = model.getDisciplines();
        DynamicListDataSource<Discipline2RealizationWayRelation> dataSource = model.getDataSource();
        dataSource.setCountRow(disciplines.size());
        UniBaseUtils.createPage(dataSource, disciplines);
    }

    /**
     * Подготавливает данные для отображения в списке
     *
     * @param model модель
     */
    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        List<Discipline2RealizationWayRelation> disciplines = getDisciplines(model.getEnrollmentCampaign());

        Collections.sort(disciplines, new EntityComparator<>(new EntityOrder(Discipline2RealizationWayRelation.P_PRIORITY)));
        model.setDisciplines(disciplines);

        ((BlockColumn<StateExamSubject>) model.getDataSource().getColumn("stateExamSubject")).setValueMap(getDisciplineId2ExamSubjectMap(disciplines));
    }

    /**
     * Возвращает для данных дисциплин связанные с ними предметы ЕГЭ
     *
     * @param disciplines список дисциплин
     * @return [идентификатор дисциплины] -> [связанный с нею предмет ЕГЭ]
     */
    @SuppressWarnings("unchecked")
    private Map<Long, StateExamSubject> getDisciplineId2ExamSubjectMap(List<Discipline2RealizationWayRelation> disciplines)
    {
        List<Object[]> list = Collections.emptyList();
        if (!disciplines.isEmpty())
        {
            Criteria criteria = getSession().createCriteria(ConversionScale.class);
            criteria.add(Restrictions.in(ConversionScale.L_DISCIPLINE, disciplines));

            ProjectionList projections = Projections.projectionList();
            projections.add(Projections.property(ConversionScale.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.P_ID));
            projections.add(Projections.property(ConversionScale.L_SUBJECT));
            criteria.setProjection(projections);
            list = criteria.list();
        }
        Map<Long, StateExamSubject> result = new HashMap<>();
        for (Object[] row : list)
        {
            result.put((Long) row[0], (StateExamSubject) row[1]);
        }
        return result;
    }

    /**
     * Возвращает список дисциплин для данной приемной кампании
     *
     * @param enrollmentCampaign приемная кампания
     * @return список дисциплин
     */
    @SuppressWarnings("unchecked")
    private List<Discipline2RealizationWayRelation> getDisciplines(EnrollmentCampaign enrollmentCampaign)
    {
        Criteria criteria = getSession().createCriteria(Discipline2RealizationWayRelation.class);
        criteria.add(Restrictions.eq(Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        criteria.setFetchMode(Discipline2RealizationWayRelation.L_EDUCATION_SUBJECT, FetchMode.JOIN);
        criteria.setFetchMode(Discipline2RealizationWayRelation.L_SUBJECT_PASS_WAY, FetchMode.JOIN);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        DynamicListDataSource<Discipline2RealizationWayRelation> dataSource = model.getDataSource();
        Map<Long, StateExamSubject> disciplineId2StateExamSubject = ((BlockColumn<StateExamSubject>) dataSource.getColumn("stateExamSubject")).getValueMap();

        List<Discipline2RealizationWayRelation> disciplines;
        try
        {
            disciplines = dataSource.getEntityList();
        } catch (Throwable e)
        {
            throw new RuntimeException(e);
        }

        Session session = getSession();
        boolean differenceExist = model.getEnrollmentCampaign().isEnrollmentPerCompTypeDiff();
        for (Discipline2RealizationWayRelation discipline : disciplines) // для каждой дисциплины
        {
            session.update(discipline); // вообще она и так обновится (но вдруг кто загрузит объекты в read-only mode)

            StateExamSubject subject = disciplineId2StateExamSubject.get(discipline.getId());
            ConversionScale conversionScale = get(ConversionScale.class, ConversionScale.L_DISCIPLINE, discipline);

            if (subject != null) //  если выбрали непустой предмет
            {
                if (conversionScale != null) // и уже существует шкала перевода 
                {
                    conversionScale.setSubject(subject); // просто сменить в ней предмет
                } else
                // иначе создать новую шкалу
                {
                    conversionScale = new ConversionScale();
                    conversionScale.setDiscipline(discipline);
                    conversionScale.setBudgetScale(new byte[]{});
                    conversionScale.setContractScale(new byte[]{});
                    conversionScale.setSubject(subject);

                    if (discipline.getMinMark() == 0 && discipline.getMaxMark() == 100 && discipline.getStep() == 1.0) // шкала ЕГЭ
                    {
                        byte[] marks = new byte[101];
                        for (byte i = 0; i <= 100; i++)
                        {
                            marks[i] = i;
                        }
                        conversionScale.setBudgetScale(marks);
                        if (differenceExist)
                        {
                            conversionScale.setContractScale(marks);
                        }
                    }
                }
                session.saveOrUpdate(conversionScale);
            } else
            // иначе если предмет пуст
            {
                if (conversionScale != null) // и шкала перевода существует
                {
                    session.delete(conversionScale); // удалить ее
                }
            }
        }
    }

    /**
     * Проверяет правильность введенных данных для каждой дисциплины
     */
    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        List<String> fieldsWithMaxMinErrorIds = new ArrayList<>();
        List<String> fieldsWithStepMultiplicityErrorIds = new ArrayList<>();
        List<String> fieldsWithStepErrorIds = new ArrayList<>();
        List<String> fieldsWithPassMarkErrorIds = new ArrayList<>();

        for (Discipline2RealizationWayRelation discipline : model.getDisciplines()) // для каждой дисциплины ошибка, если:
        {
            Long id = discipline.getId();

            int maxMark = discipline.getMaxMark();
            int minMark = discipline.getMinMark();
            double step = discipline.getStep();
            double passMark = discipline.getPassMark();

            if (minMark >= maxMark) // максимальный не больше минимального
                fieldsWithMaxMinErrorIds.add("maxMark" + id);

            if (step != 0)
            {
                if ((maxMark - minMark) / step % 1 != 0) // разница максимального и минимального не делится нацело на ненулевой шаг
                    fieldsWithStepErrorIds.add("step" + id);

                if (step % 0.5 != 0) // шаг должен быть кратен 0.5
                    fieldsWithStepMultiplicityErrorIds.add("step" + id);
            }

            if (passMark < minMark || passMark > maxMark) // зачетный не в диапазоне от минимального до максимального
                fieldsWithPassMarkErrorIds.add("passMark" + id);
        }

        if (!fieldsWithMaxMinErrorIds.isEmpty())
            errors.add("Максимальный балл должен быть больше минимального.", fieldsWithMaxMinErrorIds.toArray(new String[fieldsWithMaxMinErrorIds.size()]));

        if (!fieldsWithStepErrorIds.isEmpty())
            errors.add("Заданный шаг должен укладываться в диапазон баллов по дисциплине.", fieldsWithStepErrorIds.toArray(new String[fieldsWithStepErrorIds.size()]));

        if (!fieldsWithPassMarkErrorIds.isEmpty())
            errors.add("Зачетный балл должен быть в диапазоне баллов от минимального до максимального.", fieldsWithPassMarkErrorIds.toArray(new String[fieldsWithPassMarkErrorIds.size()]));

        if (!fieldsWithStepMultiplicityErrorIds.isEmpty())
            errors.add("Заданный шаг должен быть кратен 0.5.", fieldsWithStepMultiplicityErrorIds.toArray(new String[fieldsWithStepMultiplicityErrorIds.size()]));
    }
}