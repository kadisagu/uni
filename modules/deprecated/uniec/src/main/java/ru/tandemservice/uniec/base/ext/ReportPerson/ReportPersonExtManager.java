package ru.tandemservice.uniec.base.ext.ReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Vasily Zhukov
 * @since 08.02.2011
 */
@Configuration
public class ReportPersonExtManager extends BusinessObjectExtensionManager
{
}
