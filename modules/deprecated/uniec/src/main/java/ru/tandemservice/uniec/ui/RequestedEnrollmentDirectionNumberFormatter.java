/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.ui;

import org.tandemframework.core.view.formatter.IFormatter;

import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author agolubenko
 * @since 12.07.2009
 */
public class RequestedEnrollmentDirectionNumberFormatter implements IFormatter<RequestedEnrollmentDirection>
{
    @Override
    public String format(RequestedEnrollmentDirection source)
    {
        if (source == null) return null;

        return source.getEntrantRequest().getStringNumber();
    }
}
