package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приоритет профиля направления подготовки по ВНП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PriorityProfileEduOuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu";
    public static final String ENTITY_NAME = "priorityProfileEduOu";
    public static final int VERSION_HASH = -345008909;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";
    public static final String L_PROFILE_EDUCATION_ORG_UNIT = "profileEducationOrgUnit";
    public static final String P_PRIORITY = "priority";

    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление приема
    private ProfileEducationOrgUnit _profileEducationOrgUnit;     // Профиль направления подготовки подразделения
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление приема. Свойство не может быть null.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     */
    @NotNull
    public ProfileEducationOrgUnit getProfileEducationOrgUnit()
    {
        return _profileEducationOrgUnit;
    }

    /**
     * @param profileEducationOrgUnit Профиль направления подготовки подразделения. Свойство не может быть null.
     */
    public void setProfileEducationOrgUnit(ProfileEducationOrgUnit profileEducationOrgUnit)
    {
        dirty(_profileEducationOrgUnit, profileEducationOrgUnit);
        _profileEducationOrgUnit = profileEducationOrgUnit;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PriorityProfileEduOuGen)
        {
            setRequestedEnrollmentDirection(((PriorityProfileEduOu)another).getRequestedEnrollmentDirection());
            setProfileEducationOrgUnit(((PriorityProfileEduOu)another).getProfileEducationOrgUnit());
            setPriority(((PriorityProfileEduOu)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PriorityProfileEduOuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PriorityProfileEduOu.class;
        }

        public T newInstance()
        {
            return (T) new PriorityProfileEduOu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
                case "profileEducationOrgUnit":
                    return obj.getProfileEducationOrgUnit();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "profileEducationOrgUnit":
                    obj.setProfileEducationOrgUnit((ProfileEducationOrgUnit) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
                case "profileEducationOrgUnit":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
                case "profileEducationOrgUnit":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "profileEducationOrgUnit":
                    return ProfileEducationOrgUnit.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PriorityProfileEduOu> _dslPath = new Path<PriorityProfileEduOu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PriorityProfileEduOu");
    }
            

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu#getProfileEducationOrgUnit()
     */
    public static ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> profileEducationOrgUnit()
    {
        return _dslPath.profileEducationOrgUnit();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends PriorityProfileEduOu> extends EntityPath<E>
    {
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;
        private ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> _profileEducationOrgUnit;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное направление приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

    /**
     * @return Профиль направления подготовки подразделения. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu#getProfileEducationOrgUnit()
     */
        public ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit> profileEducationOrgUnit()
        {
            if(_profileEducationOrgUnit == null )
                _profileEducationOrgUnit = new ProfileEducationOrgUnit.Path<ProfileEducationOrgUnit>(L_PROFILE_EDUCATION_ORG_UNIT, this);
            return _profileEducationOrgUnit;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(PriorityProfileEduOuGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return PriorityProfileEduOu.class;
        }

        public String getEntityName()
        {
            return "priorityProfileEduOu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
