/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressStreet;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantCertificateMark;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniec.ws.EntrantData;
import ru.tandemservice.uniec.ws.UserDirectionData;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since Apr 29, 2010
 */
public class OnlineEntrantRegistrationDAO extends UniBaseDao implements IOnlineEntrantRegistrationDAO
{
    @Override
    public OnlineEntrant createOrUpdateOnlineEntrant(long userId, EntrantData entrantData)
    {
        boolean create = false;
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        // создаем нового, если необходимо
        if (onlineEntrant == null)
        {
            create = true;
            onlineEntrant = new OnlineEntrant();
            onlineEntrant.setActualAddress(new Address());
            onlineEntrant.setRegistrationAddress(new Address());
        }
        // иначе проверяем, что можно редактировать
        else if (!isOnlineEntrantEditable(onlineEntrant))
        {
            throw new RuntimeException("Данные онлайн-абитуриента нельзя отредактировать, так как уже создан абитуриент.");
        }

        // Идентификатор пользователя в econline
        onlineEntrant.setUserId(userId);

        // Данные абитуриента
        if (create)
        {
            EnrollmentCampaign enrollmentCampaign = new DQLSelectBuilder()
                    .fromEntity(EnrollmentCampaign.class, "e").top(1)
                    .column(property("e"))
                    .where(eq(property("e", EnrollmentCampaign.closed()), value(Boolean.FALSE)))
                    .order(property(EnrollmentCampaign.id().fromAlias("e")), OrderDirection.desc)
                    .createStatement(getSession()).uniqueResult();
            onlineEntrant.setEnrollmentCampaign(enrollmentCampaign);
            Calendar now = Calendar.getInstance();
            onlineEntrant.setRegistrationDate(now.getTime());
            int year = now.get(Calendar.YEAR);
            onlineEntrant.setRegistrationYear(year);
            onlineEntrant.setPersonalNumber(getMaxEntrantPersonalNumber(year) + 1);
        }

        // ФИО
        onlineEntrant.setLastName(entrantData.lastName);
        onlineEntrant.setFirstName(entrantData.firstName);
        onlineEntrant.setMiddleName(entrantData.middleName);

        // Удостоверение личности
        onlineEntrant.setPassportType(safeGet(IdentityCardType.class, IdentityCardType.id().s(), entrantData.passportTypeId));
        onlineEntrant.setPassportSeria(entrantData.passportSeria);
        onlineEntrant.setPassportNumber(entrantData.passportNumber);
        onlineEntrant.setPassportCitizenship(safeGet(AddressCountry.class, AddressCountry.id().s(), entrantData.passportCitizenshipId));
        onlineEntrant.setPassportIssuancePlace(entrantData.passportIssuancePlace);
        onlineEntrant.setPassportIssuanceCode(entrantData.passportIssuanceCode);
        onlineEntrant.setPassportIssuanceDate(entrantData.passportIssuanceDate);
        onlineEntrant.setSex(safeGet(Sex.class, Sex.id().s(), entrantData.sexId));
        onlineEntrant.setBirthDate(entrantData.birthDate);
        onlineEntrant.setBirthPlace(entrantData.birthPlace);

        // Адрес регистрации
        Address registrationAddress = onlineEntrant.getRegistrationAddress();
        registrationAddress.setCountry(safeGet(AddressCountry.class, AddressCountry.id().s(), entrantData.registrationAddressCountryId));
        registrationAddress.setSettlement(safeGet(AddressItem.class, AddressItem.id().s(), entrantData.registrationAddressSettlementId));
        registrationAddress.setStreet(safeGet(AddressStreet.class, AddressStreet.id().s(), entrantData.registrationAddressStreetId));
        registrationAddress.setHouseNumber(entrantData.registrationAddressHouseNumber);
        registrationAddress.setHouseUnitNumber(entrantData.registrationAddressHouseUnitNumber);
        registrationAddress.setFlatNumber(entrantData.registrationAddressFlatNumber);
        getSession().saveOrUpdate(registrationAddress);

        // Фактический адрес
        onlineEntrant.setLiveAtRegistration(entrantData.liveAtRegistration);
        Address actualAddress = onlineEntrant.getActualAddress();
        actualAddress.setCountry(safeGet(AddressCountry.class, AddressCountry.id().s(), entrantData.actualAddressCountryId));
        actualAddress.setSettlement(safeGet(AddressItem.class, AddressItem.id().s(), entrantData.actualAddressSettlementId));
        actualAddress.setStreet(safeGet(AddressStreet.class, AddressStreet.id().s(), entrantData.actualAddressStreetId));
        actualAddress.setHouseNumber(entrantData.actualAddressHouseNumber);
        actualAddress.setHouseUnitNumber(entrantData.actualAddressHouseUnitNumber);
        actualAddress.setFlatNumber(entrantData.actualAddressFlatNumber);
        getSession().saveOrUpdate(actualAddress);

        // Контакты
        onlineEntrant.setPhoneMobile(entrantData.mobilePhoneNumber);
        onlineEntrant.setPhoneFact(entrantData.homePhoneNumber);
        onlineEntrant.setEmail(entrantData.contactEmail);
        onlineEntrant.setPhoneRelatives(entrantData.relativePhoneNumber);

        // Свидетельство ЕГЭ
        onlineEntrant.setCertificateNumber(entrantData.certificateNumber);
        onlineEntrant.setCertificatePlace(safeGet(StateExamType.class, StateExamType.code().s(), entrantData.certificatePlaceCode));
        onlineEntrant.setCertificateDate(entrantData.certificateDate);

        //Диплом участника олимпиады
        onlineEntrant.setOlympiadDiplomaNumber(entrantData.olympiadDiplomaNumber);
        onlineEntrant.setOlympiadDiplomaIssuanceDate(entrantData.olympiadDiplomaIssuanceDate);
        onlineEntrant.setOlympiadDiplomaType(safeGet(OlympiadDiplomaType.class, OlympiadDiplomaType.id().s(), entrantData.olympiadDiplomaTypeId));
        onlineEntrant.setOlympiadDiplomaDegree(safeGet(OlympiadDiplomaDegree.class, OlympiadDiplomaDegree.id().s(), entrantData.olympiadDiplomaDegreeId));
        onlineEntrant.setOlympiadDiplomaTitle(entrantData.olympiadDiplomaTitle);
        onlineEntrant.setOlympiadDiplomaSubject(entrantData.olympiadDiplomaSubject);
        onlineEntrant.setOlympiadAddressCountry(safeGet(AddressCountry.class, AddressCountry.id().s(), entrantData.olympiadAddressCountryId));
        onlineEntrant.setOlympiadAddressSettlement(safeGet(AddressItem.class, AddressItem.id().s(), entrantData.olympiadAddressSettlementId));

        // Законченное образовательное учреждение
        onlineEntrant.setEduInstitutionCountry(safeGet(AddressCountry.class, AddressCountry.id().s(), entrantData.eduInstitutionCountryId));
        onlineEntrant.setEduInstitutionSettlement(safeGet(AddressItem.class, AddressItem.id().s(), entrantData.eduInstitutionSettlementId));
        onlineEntrant.setEduInstitution(safeGet(EduInstitution.class, EduInstitution.id().s(), entrantData.eduInstitutionId));
        onlineEntrant.setEduInstitutionStr(entrantData.eduInstitutionStr);
        onlineEntrant.setEduDocumentType(safeGet(EducationDocumentType.class, EducationDocumentType.id().s(), entrantData.eduDocumentTypeId));
        onlineEntrant.setEduDocumentLevel(safeGet(EducationLevelStage.class, EducationLevelStage.id().s(), entrantData.eduDocumentLevelId));
        onlineEntrant.setEduDocumentSeria(entrantData.eduDocumentSeria);
        onlineEntrant.setEduDocumentNumber(entrantData.eduDocumentNumber);
        onlineEntrant.setEduDocumentYearEnd(entrantData.eduDocumentYearEnd);
        onlineEntrant.setEduDocumentDate(entrantData.eduDocumentDate);
        onlineEntrant.setEduDocumentGraduationHonour(safeGet(GraduationHonour.class, GraduationHonour.id().s(), entrantData.eduDocumentGraduationHonourId));
        onlineEntrant.setMark5(entrantData.mark5);
        onlineEntrant.setMark4(entrantData.mark4);
        onlineEntrant.setMark3(entrantData.mark3);

        // Ближайший родственник
        onlineEntrant.setNextOfKinRelationDegree(safeGet(RelationDegree.class, RelationDegree.id().s(), entrantData.nextOfKinRelationDegreeId));
        onlineEntrant.setNextOfKinLastName(entrantData.nextOfKinLastName);
        onlineEntrant.setNextOfKinFirstName(entrantData.nextOfKinFirstName);
        onlineEntrant.setNextOfKinMiddleName(entrantData.nextOfKinMiddleName);
        onlineEntrant.setNextOfKinWorkPlace(entrantData.nextOfKinWorkPlace);
        onlineEntrant.setNextOfKinWorkPost(entrantData.nextOfKinWorkPost);
        onlineEntrant.setNextOfKinPhone(entrantData.nextOfKinPhone);
//        onlineEntrant.setPhoneRelatives(entrantData.nextOfKinPhone);

        // Иностранный язык
        onlineEntrant.setForeignLanguage(safeGet(ForeignLanguage.class, ForeignLanguage.id().s(), entrantData.foreignLanguageId));

        // Спортивные достижения
        onlineEntrant.setSportType(safeGet(SportType.class, SportType.id().s(), entrantData.sportTypeId));
        onlineEntrant.setSportRank(safeGet(SportRank.class, SportRank.id().s(), entrantData.sportRankId));

        // Льгота при поступлении
        onlineEntrant.setBenefit(safeGet(Benefit.class, Benefit.id().s(), entrantData.benefitId));
        onlineEntrant.setBenefitDate(entrantData.benefitDate);
        onlineEntrant.setBenefitDocument(entrantData.benefitDocument);
        onlineEntrant.setBenefitBasic(entrantData.benefitBasic);

        // Дополнительные сведения
        onlineEntrant.setChildCount(entrantData.childCount);
        onlineEntrant.setFamilyStatus(safeGet(FamilyStatus.class, FamilyStatus.id().s(), entrantData.familyStatusId));
        onlineEntrant.setWorkPlace(entrantData.workPlace);
        onlineEntrant.setWorkPost(entrantData.workPost);
        onlineEntrant.setNeedHotel(entrantData.needHotel);
        onlineEntrant.setBeginArmy(entrantData.beginArmy);
        onlineEntrant.setEndArmy(entrantData.endArmy);
        onlineEntrant.setEndArmyYear(entrantData.endArmyYear);
        onlineEntrant.setParticipateInSecondWaveUSE(entrantData.participateInSecondWaveUSE);
        onlineEntrant.setParticipateInEntranceTests(entrantData.participateInEntranceTests);
        onlineEntrant.setMethodDeliveryNReturnDocs(safeGet(MethodDeliveryNReturnDocs.class,
                                                           MethodDeliveryNReturnDocs.id().s(),
                                                           entrantData.methodDeliveryNReturnDocsId));

        // Сохраняем онлайн-абитуриента
        getSession().saveOrUpdate(onlineEntrant);

        // Оценки сертификата
        synchronizeCertificateMarks(onlineEntrant, entrantData);

        // Источники информации о вузе
        synchronizeUniversityInfoSources(onlineEntrant, entrantData);

        return onlineEntrant;
    }

    @Override
    public OnlineEntrant getOnlineEntrant(long userId)
    {
        return get(OnlineEntrant.class, OnlineEntrant.userId().s(), userId);
    }

    protected void fillEntrantData(EntrantData data, OnlineEntrant onlineEntrant)
    {
        // ФИО
        data.lastName = onlineEntrant.getLastName();
        data.firstName = onlineEntrant.getFirstName();
        data.middleName = onlineEntrant.getMiddleName();

        // Удостоверение личности
        data.passportTypeId = (Long) onlineEntrant.getProperty(OnlineEntrant.passportType().id().s());
        data.passportSeria = onlineEntrant.getPassportSeria();
        data.passportNumber = onlineEntrant.getPassportNumber();
        data.passportCitizenshipId = (Long) onlineEntrant.getProperty(OnlineEntrant.passportCitizenship().id().s());
        data.passportIssuancePlace = onlineEntrant.getPassportIssuancePlace();
        data.passportIssuanceCode = onlineEntrant.getPassportIssuanceCode();
        data.passportIssuanceDate = onlineEntrant.getPassportIssuanceDate();
        data.sexId = (Long) onlineEntrant.getProperty(OnlineEntrant.sex().id().s());
        data.birthDate = onlineEntrant.getBirthDate();
        data.birthPlace = onlineEntrant.getBirthPlace();

        // Адрес регистрации
        Address registrationAddress = onlineEntrant.getRegistrationAddress();
        if (registrationAddress != null)
        {
            data.registrationAddressCountryId = (Long) registrationAddress.getProperty(Address.country().id().s());
            data.registrationAddressSettlementId = (Long) registrationAddress.getProperty(Address.settlement().id().s());
            data.registrationAddressStreetId = (Long) registrationAddress.getProperty(Address.street().id().s());
            data.registrationAddressHouseNumber = registrationAddress.getHouseNumber();
            data.registrationAddressHouseUnitNumber = registrationAddress.getHouseUnitNumber();
            data.registrationAddressFlatNumber = registrationAddress.getFlatNumber();
        }

        // Фактический адрес
        data.liveAtRegistration = onlineEntrant.isLiveAtRegistration();
        Address actualAddress = onlineEntrant.getActualAddress();
        if (actualAddress != null)
        {
            data.actualAddressCountryId = (Long) actualAddress.getProperty(Address.country().id().s());
            data.actualAddressSettlementId = (Long) actualAddress.getProperty(Address.settlement().id().s());
            data.actualAddressStreetId = (Long) actualAddress.getProperty(Address.street().id().s());
            data.actualAddressHouseNumber = actualAddress.getHouseNumber();
            data.actualAddressHouseUnitNumber = actualAddress.getHouseUnitNumber();
            data.actualAddressFlatNumber = actualAddress.getFlatNumber();
        }

        // Контакты
        data.mobilePhoneNumber = onlineEntrant.getPhoneMobile();
        data.homePhoneNumber = onlineEntrant.getPhoneFact();
        data.contactEmail = onlineEntrant.getEmail();
        data.relativePhoneNumber = onlineEntrant.getPhoneRelatives();

        // Свидетельство ЕГЭ
        data.certificateNumber = onlineEntrant.getCertificateNumber();
        data.certificatePlaceCode = (String) onlineEntrant.getProperty(OnlineEntrant.certificatePlace().code().s());
        data.certificateDate = onlineEntrant.getCertificateDate();

        //Диплом участника олимпиады
        data.olympiadDiplomaNumber = onlineEntrant.getOlympiadDiplomaNumber();
        data.olympiadDiplomaIssuanceDate = onlineEntrant.getOlympiadDiplomaIssuanceDate();
        data.olympiadDiplomaTypeId = (Long) onlineEntrant.getProperty(OnlineEntrant.olympiadDiplomaType().id().s());
        data.olympiadDiplomaDegreeId = (Long) onlineEntrant.getProperty(OnlineEntrant.olympiadDiplomaDegree().id().s());
        data.olympiadDiplomaTitle = onlineEntrant.getOlympiadDiplomaTitle();
        data.olympiadDiplomaSubject = onlineEntrant.getOlympiadDiplomaSubject();
        data.olympiadAddressCountryId = (Long) onlineEntrant.getProperty(OnlineEntrant.olympiadAddressCountry().id().s());
        data.olympiadAddressSettlementId = (Long) onlineEntrant.getProperty(OnlineEntrant.olympiadAddressSettlement().id().s());

        // Законченное образовательное учреждение
        data.eduInstitutionCountryId = (Long) onlineEntrant.getProperty(OnlineEntrant.eduInstitutionCountry().id().s());
        data.eduInstitutionSettlementId = (Long) onlineEntrant.getProperty(OnlineEntrant.eduInstitutionSettlement().id().s());
        data.eduInstitutionId = (Long) onlineEntrant.getProperty(OnlineEntrant.eduInstitution().id().s());
        data.eduInstitutionStr = onlineEntrant.getEduInstitutionStr();
        data.eduDocumentTypeId = (Long) onlineEntrant.getProperty(OnlineEntrant.eduDocumentType().id().s());
        data.eduDocumentLevelId = (Long) onlineEntrant.getProperty(OnlineEntrant.eduDocumentLevel().id().s());
        data.eduDocumentSeria = onlineEntrant.getEduDocumentSeria();
        data.eduDocumentNumber = onlineEntrant.getEduDocumentNumber();
        data.eduDocumentYearEnd = onlineEntrant.getEduDocumentYearEnd();
        data.eduDocumentDate = onlineEntrant.getEduDocumentDate();
        data.eduDocumentGraduationHonourId = (Long) onlineEntrant.getProperty(OnlineEntrant.eduDocumentGraduationHonour().id().s());
        data.mark5 = onlineEntrant.getMark5();
        data.mark4 = onlineEntrant.getMark4();
        data.mark3 = onlineEntrant.getMark3();

        // Ближайший родственник
        data.nextOfKinRelationDegreeId = (Long) onlineEntrant.getProperty(OnlineEntrant.nextOfKinRelationDegree().id().s());
        data.nextOfKinLastName = onlineEntrant.getNextOfKinLastName();
        data.nextOfKinFirstName = onlineEntrant.getNextOfKinFirstName();
        data.nextOfKinMiddleName = onlineEntrant.getNextOfKinMiddleName();
        data.nextOfKinWorkPlace = onlineEntrant.getNextOfKinWorkPlace();
        data.nextOfKinWorkPost = onlineEntrant.getNextOfKinWorkPost();
        data.nextOfKinPhone = onlineEntrant.getNextOfKinPhone();

        // Иностранный язык
        data.foreignLanguageId = (Long) onlineEntrant.getProperty(OnlineEntrant.foreignLanguage().id().s());

        // Спортивные достижения
        data.sportTypeId = (Long) onlineEntrant.getProperty(OnlineEntrant.sportType().id().s());
        data.sportRankId = (Long) onlineEntrant.getProperty(OnlineEntrant.sportRank().id().s());

        // Льгота при поступлении
        data.benefitId = (Long) onlineEntrant.getProperty(OnlineEntrant.benefit().id().s());
        data.benefitDocument = onlineEntrant.getBenefitDocument();
        data.benefitDate = onlineEntrant.getBenefitDate();
        data.benefitBasic = onlineEntrant.getBenefitBasic();

        // Дополнительные сведения
        data.childCount = onlineEntrant.getChildCount();
        data.familyStatusId = (Long) onlineEntrant.getProperty(OnlineEntrant.familyStatus().id().s());
        data.workPlace = onlineEntrant.getWorkPlace();
        data.workPost = onlineEntrant.getWorkPost();
        data.needHotel = onlineEntrant.isNeedHotel();
        data.beginArmy = onlineEntrant.getBeginArmy();
        data.endArmy = onlineEntrant.getEndArmy();
        data.endArmyYear = onlineEntrant.getEndArmyYear();
        data.participateInSecondWaveUSE = onlineEntrant.getParticipateInSecondWaveUSE() != null && onlineEntrant.getParticipateInSecondWaveUSE();
        data.participateInEntranceTests = onlineEntrant.getParticipateInEntranceTests() != null && onlineEntrant.getParticipateInEntranceTests();
        data.methodDeliveryNReturnDocsId = (Long) onlineEntrant.getProperty(OnlineEntrant.methodDeliveryNReturnDocs().id().s());

        // Оценки сертификата
        for (OnlineEntrantCertificateMark certificateMark : UniecDAOFacade.getOnlineEntrantRegistrationDAO().getCertificateMarks(onlineEntrant))
        {
            data.subjectId2Mark.put(certificateMark.getSubject().getId(), new Integer[]{certificateMark.getMark(), certificateMark.getYear()});
        }

        // Источники информации о вузе
        for (OnlineEntrantInfoAboutUniversity infoAboutUniversity : UniecDAOFacade.getOnlineEntrantRegistrationDAO().getUniversityInfoSources(onlineEntrant))
        {
            data.universityInfoSourceIds.add(infoAboutUniversity.getSourceInfo().getId());
        }
    }

    @Override
    public EntrantData getEntrantData(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        if (onlineEntrant == null) return null;
        EntrantData result = new EntrantData();
        fillEntrantData(result, onlineEntrant);
        return result;
    }

    @Override
    public List<OnlineEntrantCertificateMark> getCertificateMarks(OnlineEntrant onlineEntrant)
    {
        return getList(OnlineEntrantCertificateMark.class, OnlineEntrantCertificateMark.entrant().s(), onlineEntrant, OnlineEntrantCertificateMark.subject().title().s());
    }

    @Override
    public List<OnlineEntrantInfoAboutUniversity> getUniversityInfoSources(OnlineEntrant onlineEntrant)
    {
        return getList(OnlineEntrantInfoAboutUniversity.class, OnlineEntrantInfoAboutUniversity.entrant().s(), onlineEntrant);
    }

    @Override
    public List<OnlineRequestedEnrollmentDirection> getRequestedEnrollmentDirections(OnlineEntrant onlineEntrant)
    {
        return new DQLSelectBuilder().fromEntity(OnlineRequestedEnrollmentDirection.class, "e")
                .column("e")
                .where(eq(property(OnlineRequestedEnrollmentDirection.entrant().fromAlias("e")), value(onlineEntrant)))
                .where(or(
                        eq(property(OnlineRequestedEnrollmentDirection.enrollmentDirection().budget().fromAlias("e")), value(Boolean.TRUE)),
                        eq(property(OnlineRequestedEnrollmentDirection.enrollmentDirection().contract().fromAlias("e")), value(Boolean.TRUE))
                )).createStatement(getSession()).list();
    }

    @Override
    public List<UserDirectionData> getUserDirections(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        if (onlineEntrant == null) return Collections.emptyList();

        List<UserDirectionData> result = new ArrayList<>();
        for (OnlineRequestedEnrollmentDirection direction : getRequestedEnrollmentDirections(onlineEntrant))
        {
            UserDirectionData userDirectionData = new UserDirectionData();
            userDirectionData.enrollmentDirectionId = direction.getEnrollmentDirection().getId();
            userDirectionData.compensationTypeId = direction.getCompensationType().getId();
            userDirectionData.studentCategoryId = direction.getStudentCategory().getId();
            userDirectionData.competitionKindId = direction.getCompetitionKind().getId();
            userDirectionData.targetAdmission = direction.isTargetAdmission();
            userDirectionData.profileWorkExperienceYears = direction.getProfileWorkExperienceYears();
            userDirectionData.profileWorkExperienceMonths = direction.getProfileWorkExperienceMonths();
            userDirectionData.profileWorkExperienceDays = direction.getProfileWorkExperienceDays();
            result.add(userDirectionData);
        }
        return result;
    }

    @Override
    public void updateUserDirections(long userId, UserDirectionData[] userDirections)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        // удаляем все старые
        getList(OnlineRequestedEnrollmentDirection.class, OnlineRequestedEnrollmentDirection.entrant().s(), onlineEntrant)
                .forEach(this::delete);

        // синхронизируем сессию с базой
        getSession().flush();

        updateEnrollmentCampaign(onlineEntrant, userDirections);

        // сохраняем новые
        for (UserDirectionData userDirectionData : userDirections)
        {
            OnlineRequestedEnrollmentDirection direction = new OnlineRequestedEnrollmentDirection();
            direction.setEntrant(onlineEntrant);
            direction.setEnrollmentDirection(get(EnrollmentDirection.class, userDirectionData.enrollmentDirectionId));
            direction.setCompensationType(get(CompensationType.class, userDirectionData.compensationTypeId));
            direction.setStudentCategory(get(StudentCategory.class, userDirectionData.studentCategoryId));
            direction.setCompetitionKind(get(CompetitionKind.class, userDirectionData.competitionKindId));
            direction.setTargetAdmission(userDirectionData.targetAdmission);
            direction.setProfileWorkExperienceYears(userDirectionData.profileWorkExperienceYears);
            direction.setProfileWorkExperienceMonths(userDirectionData.profileWorkExperienceMonths);
            direction.setProfileWorkExperienceDays(userDirectionData.profileWorkExperienceDays);
            save(direction);
        }
    }

    protected void updateEnrollmentCampaign(OnlineEntrant onlineEntrant, UserDirectionData[] userDirections)
    {
        if (userDirections.length > 0)
        {
            EnrollmentCampaign campaign = get(EnrollmentDirection.class, userDirections[0].enrollmentDirectionId).getEnrollmentCampaign();
            if (!onlineEntrant.getEnrollmentCampaign().getId().equals(campaign.getId()))
            {
                onlineEntrant.setEnrollmentCampaign(campaign);
                saveOrUpdate(onlineEntrant);
            }
        }
    }

    private boolean isOnlineEntrantEditable(OnlineEntrant onlineEntrant)
    {
        return (onlineEntrant.getEntrant() == null);
    }

    private void synchronizeCertificateMarks(OnlineEntrant onlineEntrant, EntrantData entrantData)
    {
        Map<Long, OnlineEntrantCertificateMark> markMap = UniecDAOFacade.getOnlineEntrantRegistrationDAO()
                .getCertificateMarks(onlineEntrant).stream()
                .collect(Collectors.toMap(mark->mark.getSubject().getId(), mark -> mark));

        entrantData.subjectId2Mark.entrySet()
                .forEach(entry -> {
                    Long subjectId = entry.getKey();
                    OnlineEntrantCertificateMark mark = markMap.get(subjectId);
                    Integer[] data = entry.getValue();

                    if (data == null || data[0] == null)
                    {
                        if (mark != null) delete(mark);
                    }
                    else
                    {
                        if (mark == null)
                            mark = new OnlineEntrantCertificateMark(onlineEntrant, get(StateExamSubject.class, subjectId));

                        mark.setMark(data[0]);
                        mark.setYear(data[1] == 0 ? null : data[1]);
                        saveOrUpdate(mark);
                    }
                });
    }

    private void synchronizeUniversityInfoSources(OnlineEntrant onlineEntrant, EntrantData entrantData)
    {
        Set<Long> universityInfoSourceIds = new HashSet<>(entrantData.universityInfoSourceIds);

        // для каждого существующего источника информации о вузе
        for (OnlineEntrantInfoAboutUniversity infoAboutUniversity : UniecDAOFacade.getOnlineEntrantRegistrationDAO().getUniversityInfoSources(onlineEntrant))
        {
            // удалить из списка для обработки, а если не было там, то удалить вообще
            if (!universityInfoSourceIds.remove(infoAboutUniversity.getSourceInfo().getId()))
            {
                delete(infoAboutUniversity);
            }
        }

        // для остальных создать новые источники
        for (Long universityInfoSourceId : universityInfoSourceIds)
        {
            OnlineEntrantInfoAboutUniversity infoAboutUniversity = new OnlineEntrantInfoAboutUniversity();
            infoAboutUniversity.setSourceInfo(get(SourceInfoAboutUniversity.class, universityInfoSourceId));
            infoAboutUniversity.setEntrant(onlineEntrant);
            save(infoAboutUniversity);
        }
    }

    private int getMaxEntrantPersonalNumber(int year)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "OnlineEntrantPersonalNumberSync");

        Integer number = new DQLSelectBuilder()
                .fromEntity(OnlineEntrant.class, "oe")
                .column(DQLFunctions.max(property("oe", OnlineEntrant.personalNumber())))
                .where(eq(property("oe", OnlineEntrant.registrationYear()), value(year)))
                .createStatement(getSession()).<Integer>uniqueResult();

        return number == null ? 0: number;
    }

    protected <T extends EntityBase> T safeGet(Class<T> clazz, String property, Object value)
    {
        return (value != null) ? get(clazz, property, value) : null;
    }

    @Override
    public byte[] getPrintOnlineEntrantRequest(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        if (onlineEntrant == null) return null;

        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(getByCode(ScriptItem.class, UniecDefines.SCRIPT_ONLINE_ENTRANT_REQUEST),
                IScriptExecutor.OBJECT_VARIABLE, onlineEntrant.getId(),
                IScriptExecutor.PRINT_OBJECT_VARIABLE, onlineEntrant
        );

        return (byte[]) result.get(IScriptExecutor.DOCUMENT);
    }

    @Override
    public void saveDocumentCopies(long userId, byte[] zipArchive, String documentCopiesName)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        if (onlineEntrant == null || zipArchive == null || StringUtils.isEmpty(documentCopiesName)) return;

        // если файл уже загружен, то удаляем его
        deleteDocumentCopies(userId);

        // сохраняем файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(zipArchive);
        save(databaseFile);

        onlineEntrant.setDocumentCopies(databaseFile);
        onlineEntrant.setDocumentCopiesName(documentCopiesName);
        update(onlineEntrant);
    }

    @Override
    public void deleteDocumentCopies(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        if (onlineEntrant == null) return;

        DatabaseFile databaseFile = onlineEntrant.getDocumentCopies();
        onlineEntrant.setDocumentCopies(null);
        onlineEntrant.setDocumentCopiesName(null);
        update(onlineEntrant);

        if (databaseFile != null)
            delete(databaseFile);
    }
}
