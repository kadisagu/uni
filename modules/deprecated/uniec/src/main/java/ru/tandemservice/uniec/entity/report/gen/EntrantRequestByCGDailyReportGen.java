package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневная сводка по заявлениям (по конкурсным группам) с нарастающим итогом
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantRequestByCGDailyReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport";
    public static final String ENTITY_NAME = "entrantRequestByCGDailyReport";
    public static final int VERSION_HASH = 1599964405;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String P_COMPETITION_GROUP_TITLE = "competitionGroupTitle";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Дата с
    private Date _dateTo;     // Дата по
    private DevelopForm _developForm;     // Форма освоения
    private String _competitionGroupTitle;     // Конкурсная группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Дата с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Дата по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Дата по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Конкурсная группа.
     */
    @Length(max=2000)
    public String getCompetitionGroupTitle()
    {
        return _competitionGroupTitle;
    }

    /**
     * @param competitionGroupTitle Конкурсная группа.
     */
    public void setCompetitionGroupTitle(String competitionGroupTitle)
    {
        dirty(_competitionGroupTitle, competitionGroupTitle);
        _competitionGroupTitle = competitionGroupTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntrantRequestByCGDailyReportGen)
        {
            setEnrollmentCampaign(((EntrantRequestByCGDailyReport)another).getEnrollmentCampaign());
            setDateFrom(((EntrantRequestByCGDailyReport)another).getDateFrom());
            setDateTo(((EntrantRequestByCGDailyReport)another).getDateTo());
            setDevelopForm(((EntrantRequestByCGDailyReport)another).getDevelopForm());
            setCompetitionGroupTitle(((EntrantRequestByCGDailyReport)another).getCompetitionGroupTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantRequestByCGDailyReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantRequestByCGDailyReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantRequestByCGDailyReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "developForm":
                    return obj.getDevelopForm();
                case "competitionGroupTitle":
                    return obj.getCompetitionGroupTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "competitionGroupTitle":
                    obj.setCompetitionGroupTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "developForm":
                        return true;
                case "competitionGroupTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "developForm":
                    return true;
                case "competitionGroupTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "developForm":
                    return DevelopForm.class;
                case "competitionGroupTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantRequestByCGDailyReport> _dslPath = new Path<EntrantRequestByCGDailyReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantRequestByCGDailyReport");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Дата по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Конкурсная группа.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getCompetitionGroupTitle()
     */
    public static PropertyPath<String> competitionGroupTitle()
    {
        return _dslPath.competitionGroupTitle();
    }

    public static class Path<E extends EntrantRequestByCGDailyReport> extends StorableReport.Path<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private DevelopForm.Path<DevelopForm> _developForm;
        private PropertyPath<String> _competitionGroupTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantRequestByCGDailyReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Дата по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantRequestByCGDailyReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Конкурсная группа.
     * @see ru.tandemservice.uniec.entity.report.EntrantRequestByCGDailyReport#getCompetitionGroupTitle()
     */
        public PropertyPath<String> competitionGroupTitle()
        {
            if(_competitionGroupTitle == null )
                _competitionGroupTitle = new PropertyPath<String>(EntrantRequestByCGDailyReportGen.P_COMPETITION_GROUP_TITLE, this);
            return _competitionGroupTitle;
        }

        public Class getEntityClass()
        {
            return EntrantRequestByCGDailyReport.class;
        }

        public String getEntityName()
        {
            return "entrantRequestByCGDailyReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
