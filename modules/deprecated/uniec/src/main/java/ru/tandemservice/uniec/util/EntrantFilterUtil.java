/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.util;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.CurrentExamGroupLogic;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 30.04.2009
 */
public class EntrantFilterUtil
{
    private enum SelectType
    {
        ALL,
        ALL_WITH_SECURITY,
        WITHOUT_AUTO_FORMING,
        WITH_AUTO_FORMING,
        WITH_AUTO_FORMING_AND_ALGORITHM
    }

    /**
     * создает селект со всеми приемными кампаниями
     *
     * @param model   модель
     * @param session сессия
     */
    public static void prepareEnrollmentCampaignFilter(final IEnrollmentCampaignSelectModel model, final Session session)
    {
        prepareEnrollmentCampaignFilter(model, session, SelectType.ALL);
    }

    /**
     * создает селект со всеми приемными кампаниями, используется проверка глобального права workWithAllEnrollmentCampaigns
     * если право есть, то все работает как в prepareEnrollmentCampaignFilter
     * если права нет, то видна только последняя приемная кампания
     *
     * @param model   модель
     * @param session сессия
     */
    public static void prepareEnrollmentCampaignFilterWithSecurity(final IEnrollmentCampaignSelectModel model, final Session session)
    {
        prepareEnrollmentCampaignFilter(model, session, SelectType.ALL_WITH_SECURITY);
    }

    /**
     * создает селект с приемными кампаниями, в которых не выбран алгоритм формирования экаменационных групп
     *
     * @param model   модель
     * @param session сессия
     */
    public static void prepareEnrollmentCampaignFilterWithoutExamGroupAutoForming(final IEnrollmentCampaignSelectModel model, final Session session)
    {
        prepareEnrollmentCampaignFilter(model, session, SelectType.WITHOUT_AUTO_FORMING);
    }

    /**
     * создает селект с приемными кампаниями, в которых используется автоматическое формирование экаменационных групп
     *
     * @param model   модель
     * @param session сессия
     */
    public static void prepareEnrollmentCampaignFilterWithExamGroupAutoForming(final IEnrollmentCampaignSelectModel model, final Session session)
    {
        prepareEnrollmentCampaignFilter(model, session, SelectType.WITH_AUTO_FORMING);
    }

    /**
     * создает селект с приемными кампаниями, в которых используется автоматическое формирование экаменационных групп и выбран алгоритм
     *
     * @param model   модель
     * @param session сессия
     */
    public static void prepareEnrollmentCampaignFilterWithExamGroupAutoFormingAndAlgorithm(final IEnrollmentCampaignSelectModel model, final Session session)
    {
        prepareEnrollmentCampaignFilter(model, session, SelectType.WITH_AUTO_FORMING_AND_ALGORITHM);
    }

    private static void prepareEnrollmentCampaignFilter(final IEnrollmentCampaignSelectModel model, final Session session, final SelectType selectType)
    {
        EnrollmentCampaign savedEnrollmentCampaign = model.getEnrollmentCampaign();
        if (selectType.equals(EntrantFilterUtil.SelectType.ALL_WITH_SECURITY))
        {
            boolean check = CoreServices.securityService().check(new EnrollmentCampaign(), ContextLocal.getUserContext().getPrincipalContext(), "workWithAllEnrollmentCampaigns");
            if (!check)
            {
                // права нет, значит нужно показывать только последнюю приемную кампанию
                List<EnrollmentCampaign>  enrollmentCampaigns = new DQLSelectBuilder()
                        .fromEntity(EnrollmentCampaign.class, "e")
                        .column(property("e"))
                        .where(eq(property("e",  EnrollmentCampaign.closed()), value(false)))
                        .order(property("e", EnrollmentCampaign.title()))
                        .createStatement(session).list();

                if (CollectionUtils.isEmpty(enrollmentCampaigns))
                {
                    model.setEnrollmentCampaignList(Collections.<EnrollmentCampaign>emptyList());
                    model.setEnrollmentCampaign(null);
                }
                else
                {
                    model.setEnrollmentCampaignList(enrollmentCampaigns);
                    if (savedEnrollmentCampaign == null || !enrollmentCampaigns.contains(savedEnrollmentCampaign))
                        model.setEnrollmentCampaign(enrollmentCampaigns.get(0));
                }
                return;
            }
        }

        final MQBuilder builder = new MQBuilder(EnrollmentCampaign.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", EnrollmentCampaign.L_EDUCATION_YEAR, "y");
        builder.addJoinFetch("e", EnrollmentCampaign.L_ENTRANT_EXAM_LISTS_FORMING_FEATURE, "f");

        switch (selectType)
        {
            case ALL:
            case ALL_WITH_SECURITY:
                break;
            case WITHOUT_AUTO_FORMING:
                builder.add(MQExpression.eq("e", EnrollmentCampaign.P_FORMING_EXAM_GROUP_AUTO, Boolean.FALSE));
                break;
            case WITH_AUTO_FORMING:
                builder.add(MQExpression.eq("e", EnrollmentCampaign.P_FORMING_EXAM_GROUP_AUTO, Boolean.TRUE));
                break;
            case WITH_AUTO_FORMING_AND_ALGORITHM:
                builder.add(MQExpression.eq("e", EnrollmentCampaign.P_FORMING_EXAM_GROUP_AUTO, Boolean.TRUE));
                builder.addDomain("c", CurrentExamGroupLogic.ENTITY_CLASS);
                builder.add(MQExpression.eqProperty("e", EnrollmentCampaign.P_ID, "c", CurrentExamGroupLogic.enrollmentCampaign().id().s()));
                break;
            default:
                throw new RuntimeException("Unknown selectType: " + selectType);
        }

        builder.addDescOrder("e", EnrollmentCampaign.P_ID);

        final List<EnrollmentCampaign> enrollmentCampaigns = builder.getResultList(session);

        if ((savedEnrollmentCampaign == null) && !enrollmentCampaigns.isEmpty())
        {
            model.setEnrollmentCampaign(enrollmentCampaigns.get(0));
        }
        model.setEnrollmentCampaignList(enrollmentCampaigns);
    }

    /**
     * сбрасывает селект приемной кампании
     *
     * @param model модель
     */
    public static void resetEnrollmentCampaignFilter(final IEnrollmentCampaignSelectModel model)
    {
        model.getSettings().clear();
        final List<EnrollmentCampaign> enrollmentCampaigns = model.getEnrollmentCampaignList();
        if (!enrollmentCampaigns.isEmpty())
        {
            model.getSettings().set(IEnrollmentCampaignSelectModel.ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaigns.get(0));
        }
    }
}
