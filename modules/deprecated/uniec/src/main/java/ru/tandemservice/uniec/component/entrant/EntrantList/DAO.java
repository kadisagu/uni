/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantList;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.base.bo.UniStudent.vo.AddressCountryVO;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since 15.05.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _entrantOrderSettings = new OrderDescriptionRegistry("entrant");
    public static final String TARGET_ADMISSION_PARAM = "targetAdmission";
    public static final String TARGET_ADMISSION_KIND_LIST_PARAM = "targetAdmissionKindList";
    public static final String BENEFIT_LIST_PARAM = "benefitList";
    public static final String COMPETITION_KIND_PARAM = "competitionKind";


    static
    {
        _entrantOrderSettings.setOrders(Entrant.P_FULLFIO, new OrderDescription("identityCard", IdentityCard.P_LAST_NAME), new OrderDescription("identityCard", IdentityCard.P_FIRST_NAME), new OrderDescription("identityCard", IdentityCard.P_MIDDLE_NAME));
        _entrantOrderSettings.setOrders(Entrant.P_PERSONAL_NUMBER, new OrderDescription("entrant", Entrant.P_PERSONAL_NUMBER), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(PersonRole.person().identityCard().sex().shortTitle().s(), new OrderDescription("entrant", Entrant.person().identityCard().sex().shortTitle().s()), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(PersonRole.person().identityCard().birthDate().s(), new OrderDescription("entrant", Entrant.person().identityCard().birthDate().s()), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(Entrant.P_REGISTRATION_DATE, new OrderDescription("entrant", Entrant.P_REGISTRATION_DATE), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
        _entrantOrderSettings.setOrders(PersonRole.person().identityCard().citizenship().title().s(), new OrderDescription("identityCard", IdentityCard.citizenship().title().s()), new OrderDescription("identityCard", IdentityCard.P_FULL_FIO));
    }

    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());
        model.setCompensationTypeModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(CompensationType.class), CompensationType.P_SHORT_TITLE));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationModel(new QualificationModel(getSession()));

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        model.getParameters().setRequired(false, MultiEnrollmentDirectionUtil.Parameters.Filter.values());
        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        model.setEntrantStateModel(new LazySimpleSelectModel<>(getCatalogItemList(EntrantState.class)));
        model.setArchivalModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(Model.SHOW_ARCHIVAL_CODE, "Показать архивных"),
                new IdentifiableWrapper(Model.SHOW_NON_ARCHIVAL_CODE, "Показать не архивных")
        )));

        model.setEntrantCustomStateCIModel(new CommonMultiSelectModel(EntrantCustomStateCI.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EntrantCustomStateCI.class, "st")
                        .column("st")
                        .order(property("st", EntrantCustomStateCI.title()));

                FilterUtils.applySimpleLikeFilter(builder, "st", EntrantCustomStateCI.P_TITLE, filter);

                if (set != null)
                    builder.where(in("st", set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
        model.setCitizenShipModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                if (null != primaryKey)
                {
                    if (primaryKey.equals(0L))
                    {
                        AddressCountryVO countryVO = new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION);
                        if (findValues("").getObjects().contains(countryVO)) return countryVO;
                        else return null;

                    }
                    else
                    {
                        AddressCountryVO countryVO = new AddressCountryVO(get((Long) primaryKey));
                        if (findValues("").getObjects().contains(countryVO)) return countryVO;
                        else return null;
                    }
                }
                return null;
            }

            @Override
            public ListResult findValues(final String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AddressCountry.class, "o");

                builder.order(property("o", AddressCountry.title()));
                List<AddressCountry> addressCountries = builder.createStatement(getSession()).list();

                List<AddressCountryVO> addressCountryVOs = Lists.newArrayList();

                addressCountryVOs.add(new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION));

                for (AddressCountry addressCountry : addressCountries)
                {
                    addressCountryVOs.add(new AddressCountryVO(addressCountry));
                }

                CollectionUtils.filter(addressCountryVOs, object -> {
                    if (!StringUtils.isEmpty(filter))
                    {
                        AddressCountryVO c = ((AddressCountryVO) object);
                        if (null != c)
                        {
                            String flt = filter.trim().toUpperCase().replace(" ", "*");
                            return c.getTitle().toUpperCase().contains(flt);
                        }
                        return true;
                    }
                    return true;
                });

                return new ListResult<>(addressCountryVOs);
            }
        });
        Long citizenship = model.getSettings().get("citizenship");
        if (null != citizenship)
        {
            if (citizenship.equals(0L))
                model.setCitizenship(new AddressCountryVO(0L, AddressCountryVO.NOT_RUSSIAN_FEDERATION));
            else
                model.setCitizenship(new AddressCountryVO(this.<AddressCountry>get(citizenship)));
        }

        model.setSpecialCondition4ExamModel(TwinComboDataSourceHandler.getYesNoDefaultSelectModel());
        model.setBase4ExamByDifferentSourcesModel(new StaticSelectModel("id", "title", getCatalogItemList(Base4ExamByDifferentSources.class)));

        CommonMultiSelectModel targetAdmissionKindModel = new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                String alias = "tak";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(TargetAdmissionKind.class, alias)
                        .column(property(alias))
                        .where(notExistsByExpr(NotUsedTargetAdmissionKind.class, "nu", and(
                                eq(property("nu", NotUsedTargetAdmissionKind.enrollmentCampaign()), value(model.getEnrollmentCampaign())),
                                eq(property("nu", NotUsedTargetAdmissionKind.targetAdmissionKind()), property(alias))
                        )))
                        .order(property(alias, TargetAdmissionKind.priority()));

                FilterUtils.applySimpleLikeFilter(builder, alias, TargetAdmissionKind.title(), filter);
                FilterUtils.applySelectFilter(builder, TargetAdmissionKind.id().fromAlias(alias), set);

                return new DQLListResultBuilder(builder);
            }
        };
        targetAdmissionKindModel.setHierarchical(true);
        model.setTargetAdmissionKindModel(targetAdmissionKindModel);

        model.setBenefitModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(Benefit.class), Benefit.title().s()));

        model.setCompetitionKindModel(new LazySimpleSelectModel<>(CompetitionKind.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = getEntrantListBuilder(model);

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());

        List<Long> entrantIds = CommonBaseEntityUtil.getIdList(model.getDataSource().getEntityList());

        Set<Long> entrantWithOriginalList = new HashSet<>(
                new DQLSelectBuilder()
                        .fromEntity(RequestedEnrollmentDirection.class, "r")
                        .column(property("r", RequestedEnrollmentDirection.entrantRequest().entrant().id()))
                        .where(in(property("r", RequestedEnrollmentDirection.entrantRequest().entrant()), entrantIds))
                        .where(eq(property("r", RequestedEnrollmentDirection.originalDocumentHandedIn()), value(Boolean.TRUE)))
                        .createStatement(getSession()).<Long>list()
        );

        final List<EntrantRequest> list = getList(EntrantRequest.class, EntrantRequest.L_ENTRANT, entrantIds, EntrantRequest.P_REG_DATE);
        final Multimap<Long, String> map = ArrayListMultimap.create(entrantIds.size(), 3);
        for (EntrantRequest request : list)
        {
            map.put(request.getEntrant().getId(), request.getStringNumber());
        }

        //Текущие активные доп. статусы
        final DQLSelectBuilder csDQL = new DQLSelectBuilder().fromEntity(EntrantCustomState.class, "st")
                .column("st")
                .where(in(property("st", EntrantCustomState.entrant()), entrantIds));

        FilterUtils.applyInPeriodFilterNullSafe(csDQL, "st", EntrantCustomState.P_BEGIN_DATE, EntrantCustomState.P_END_DATE, new Date());

        final List<EntrantCustomState> csList = csDQL.createStatement(getSession()).list();
        final Multimap<Long, EntrantCustomState> csMap = ArrayListMultimap.create(csList.size(), 2);
        for (EntrantCustomState entrantCustomState : csList)
        {
            csMap.put(entrantCustomState.getEntrant().getId(), entrantCustomState);
        }

        for (ViewWrapper<Entrant> view : ViewWrapper.<Entrant>getPatchedList(model.getDataSource()))
        {
            Collection<String> numberList = map.get(view.getEntity().getId());
            view.setViewProperty(Model.REQUEST_NUMBER_COLUMN, numberList == null ? "" : StringUtils.join(numberList, ", "));
            view.setViewProperty(Model.ORIGINAL_DOCUMENT_HANDLE_IN_COLUMN, entrantWithOriginalList.contains(view.getEntity().getId()));
            view.setViewProperty(Model.P_ENTRANT_ACTIVE_CUSTOME_STATES, csMap.get(view.getEntity().getId()));
        }
    }

    protected void filterByEntrantRequest(Model model, MQBuilder builder, String baseAlias)
    {
        IDataSettings settings = model.getSettings();
        Number requestNumber = settings.get("requestNumberFilter");
        Date dateFrom = settings.get("registeredFromFilter");
        Date dateTo = settings.get("registeredToFilter");
        CompensationType compensationType = settings.get("compensationTypeFilter");
        List<StudentCategory> studentCategoryList = settings.get("studentCategoryFilter");
        List<Qualifications> qualificationsList = settings.get("qualificationFilter");

        boolean addRequestConditions = requestNumber != null || dateFrom != null || dateTo != null;
        boolean addDirectionConditions = compensationType != null || CollectionUtils.isNotEmpty(studentCategoryList);
        boolean addEnrollmentDirectionConditions = (qualificationsList != null && !qualificationsList.isEmpty()) || model.getSelectedFormativeOrgUnitList() != null || model.getSelectedTerritorialOrgUnitList() != null || model.getSelectedEducationLevelHighSchoolList() != null || model.getSelectedDevelopFormList() != null || model.getSelectedDevelopConditionList() != null || model.getSelectedDevelopTechList() != null || model.getSelectedDevelopPeriodList() != null;

        if (addRequestConditions || addDirectionConditions || addEnrollmentDirectionConditions)
        {
            MQBuilder b = new MQBuilder(EntrantRequest.ENTITY_CLASS, "request", new String[]{EntrantRequest.L_ENTRANT + ".id"});
            if (requestNumber != null)
                b.add(MQExpression.eq("request", EntrantRequest.P_REG_NUMBER, requestNumber.intValue()));
            if (dateFrom != null)
                b.add(UniMQExpression.greatOrEq("request", EntrantRequest.P_REG_DATE, dateFrom));
            if (dateTo != null)
                b.add(UniMQExpression.lessOrEq("request", EntrantRequest.P_REG_DATE, dateTo));
            if (addDirectionConditions || addEnrollmentDirectionConditions)
            {
                b.addDomain("dir", RequestedEnrollmentDirection.ENTITY_CLASS);
                b.add(MQExpression.eqProperty("dir", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id", "request", "id"));
                if (compensationType != null)
                    b.add(MQExpression.eq("dir", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, compensationType));
                if (CollectionUtils.isNotEmpty(studentCategoryList))
                    b.add(MQExpression.in("dir", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, studentCategoryList));
                if (CollectionUtils.isNotEmpty(qualificationsList))
                    b.add(MQExpression.in("dir", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), qualificationsList));
                if (addEnrollmentDirectionConditions)
                {
                    List<EnrollmentDirection> directionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession());
                    b.add(MQExpression.in("dir", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, directionList));
                }
            }
            builder.add(MQExpression.in(baseAlias, Entrant.P_ID, b));
        }

        List<Benefit> benefitList = settings.get(BENEFIT_LIST_PARAM);
        if (CollectionUtils.isNotEmpty(benefitList))
        {
            builder.addDomain("benefit", PersonBenefit.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("entrant", Entrant.L_PERSON, "benefit", PersonBenefit.L_PERSON));
            builder.add(MQExpression.in("benefit", PersonBenefit.L_BENEFIT, benefitList));
        }

        Boolean targetAdmission = TwinComboDataSourceHandler.getSelectedValue(settings.get(TARGET_ADMISSION_PARAM));
        if (targetAdmission != null)
        {
            MQBuilder subBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
            subBuilder.getSelectAliasList().clear();
            subBuilder.addSelect(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("r").s());
            if (targetAdmission)
            {
                subBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.targetAdmission(), Boolean.TRUE));

                List<TargetAdmissionKind> targetAdmissionKindList = settings.get(TARGET_ADMISSION_KIND_LIST_PARAM);
                if (CollectionUtils.isNotEmpty(targetAdmissionKindList))
                    subBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.targetAdmissionKind(), targetAdmissionKindList));
            }
            else
                subBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.targetAdmission(), Boolean.FALSE));

            builder.add(MQExpression.in("entrant", Entrant.P_ID, subBuilder));
        }


        CompetitionKind competitionKind = settings.get(COMPETITION_KIND_PARAM);
        if (competitionKind != null)
        {
            MQBuilder mqBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r", new String[]{RequestedEnrollmentDirection.entrantRequest().entrant().id().s()});
            mqBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.competitionKind(), competitionKind));

            builder.add(MQExpression.in("entrant", "id", mqBuilder));
        }
    }

    protected MQBuilder getEntrantListBuilder(Model model)
    {
        MQBuilder builder = new MQBuilder(Entrant.ENTITY_CLASS, "entrant");
        builder.addJoin("entrant", Entrant.L_PERSON, "person");
        builder.addJoin("person", Person.L_IDENTITY_CARD, "identityCard");

        IDataSettings settings = model.getSettings();
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();

        String lastName = settings.get("lastNameFilter");
        String firstName = settings.get("firstNameFilter");
        String middleName = settings.get("middleNameFilter");
        String personalNumber = settings.get("personalNumberFilter");
        String cardSeria = settings.get("identityCardSeriaFilter");
        String cardNumber = settings.get("identityCardNumberFilter");
        List<EntrantState> entrantStates = settings.get("entrantStateFilter");
        IdentifiableWrapper archivalSelectOption = settings.get("archivalFilter");

        if (enrollmentCampaign != null)
        {
            builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        }
        if (StringUtils.isNotEmpty(lastName))
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(lastName)));
        }
        if (StringUtils.isNotEmpty(firstName))
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_FIRST_NAME, CoreStringUtils.escapeLike(firstName)));
        }
        if (StringUtils.isNotEmpty(middleName))
        {
            builder.add(MQExpression.like("identityCard", IdentityCard.P_MIDDLE_NAME, CoreStringUtils.escapeLike(middleName)));
        }
        if (StringUtils.isNotEmpty(personalNumber))
        {
            builder.add(MQExpression.like("entrant", Entrant.P_PERSONAL_NUMBER, "%" + personalNumber));
        }

        // добавляем условия на заявление и выбранное направление, если они есть
        filterByEntrantRequest(model, builder, "entrant");

        if (StringUtils.isNotEmpty(cardSeria))
        {
            builder.add(MQExpression.eq("identityCard", IdentityCard.P_SERIA, cardSeria));
        }
        if (StringUtils.isNotEmpty(cardNumber))
        {
            builder.add(MQExpression.eq("identityCard", IdentityCard.P_NUMBER, cardNumber));
        }
        if (entrantStates != null && !entrantStates.isEmpty())
        {
            builder.add(MQExpression.in("entrant", Entrant.state().s(), entrantStates));
        }
        if (archivalSelectOption != null)
        {
            builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, archivalSelectOption.getId().equals(Model.SHOW_ARCHIVAL_CODE)));
        }

        List<EntrantCustomStateCI> entrantCustomStateCIList = model.getSettings().get("entrantCustomStateCIs");

        if (null != entrantCustomStateCIList && !entrantCustomStateCIList.isEmpty())
        {
                MQBuilder csBuilder = new MQBuilder(EntrantCustomState.ENTITY_CLASS, "st", new String[]{EntrantCustomState.entrant().id().s()});
                csBuilder.add(MQExpression.in("st", EntrantCustomState.customState().id(), EntityUtils.getIdsFromEntityList(entrantCustomStateCIList)));
                FilterUtils.applyInPeriodFilterNullSafe(csBuilder, "st", EntrantCustomState.P_BEGIN_DATE, EntrantCustomState.P_END_DATE, new Date());

                builder.add(MQExpression.in("entrant", Entrant.id(), csBuilder));
        }

        if (null != model.getCitizenship())
        {
            if (model.getCitizenship().getId().equals(0L))
                builder.add(MQExpression.notEq("entrant", Entrant.person().identityCard().citizenship().code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
            else
                builder.add(MQExpression.eq("entrant", Entrant.person().identityCard().citizenship().id(), model.getCitizenship().getAddressCountry().getId()));
        }

        if (model.getSpecialCondition4Exam() != null)
            builder.add(MQExpression.eq("entrant", Entrant.specialCondition4Exam().s(), TwinComboDataSourceHandler.getSelectedValue(model.getSpecialCondition4Exam())));

        List<Base4ExamByDifferentSources> base4ExamByDifferentSources = settings.get("base4ExamByDifferentSources");
        if(CollectionUtils.isNotEmpty(base4ExamByDifferentSources))
            builder.add(MQExpression.in("entrant", Entrant.base4ExamByDifferentSources().id().s(), EntityUtils.getIdsFromEntityList(base4ExamByDifferentSources)));

        _entrantOrderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());

        return builder;
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        Entrant entrant = getNotNull(Entrant.class, context.getListenerParameter());
        UniecDAOFacade.getEntrantDAO().deleteEntrant(entrant);
    }

    @Override
    public void changeArchival(Long entrantId)
    {
        Session session = getSession();

        Entrant entrant = get(Entrant.class, entrantId);
        if (entrant.isArchival())
        {
            entrant.setArchival(false);
        }
        else
        {
            Criteria criteria = session.createCriteria(RequestedEnrollmentDirection.class);
            criteria.createAlias(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
            criteria.createAlias(RequestedEnrollmentDirection.L_STATE, "state");
            criteria.add(Restrictions.eq("entrantRequest." + EntrantRequest.L_ENTRANT, entrant));
            criteria.add(Restrictions.in("state." + EntrantState.P_CODE, new String[]{UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE, UniecDefines.ENTRANT_STATE_IN_ORDER, UniecDefines.ENTRANT_STATE_ENROLED_CODE}));
            criteria.setProjection(Projections.rowCount());
            if (((Number) criteria.uniqueResult()).intValue() > 0)
            {
                throw new ApplicationException("Невозможно списать абитуриента «" + entrant.getPerson().getFullFio() + "» в архив, поскольку существуют выбранные направления приема с состоянием «предзачислен», «в приказе» или «зачислен».");
            }
            entrant.setArchival(true);
        }
        session.update(entrant);
    }

    @Override
    public List<Long> getEntrantIds(Model model)
    {
        MQBuilder builder = getEntrantListBuilder(model);
        builder.getSelectAliasList().clear();
        builder.addSelect("entrant", new String[]{Entrant.P_ID});
        return builder.getResultList(getSession());
    }

    @Override
    public String[][] getHeaderTable(Model model)
    {
        List<String[]> headerTableData = new ArrayList<>();

        IDataSettings settings = model.getSettings();
        EnrollmentCampaign enrollmentCampaign = model.getEnrollmentCampaign();
        headerTableData.add(new String[]{"Приемная кампания:", enrollmentCampaign.getTitle()});
        String lastName = settings.get("lastNameFilter");
        if (lastName != null)
            headerTableData.add(new String[]{"Фамилия:", lastName});
        String firstName = settings.get("firstNameFilter");
        if (firstName != null)
            headerTableData.add(new String[]{"Имя:", firstName});
        String middleName = settings.get("middleNameFilter");
        if (middleName != null)
            headerTableData.add(new String[]{"Отчество:", middleName});
        String personalNumber = settings.get("personalNumberFilter");
        if (personalNumber != null)
            headerTableData.add(new String[]{"Личный номер:", personalNumber});
        Number requestNumber = settings.get("requestNumberFilter");
        if (requestNumber != null)
            headerTableData.add(new String[]{"Номер заявления:", String.valueOf(requestNumber)});
        Date dateFrom = settings.get("registeredFromFilter");
        if (dateFrom != null)
            headerTableData.add(new String[]{"Заявления с:", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateFrom)});
        Date dateTo = settings.get("registeredToFilter");
        if (dateTo != null)
            headerTableData.add(new String[]{"Заявления по:", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateTo)});
        CompensationType compensationType = settings.get("compensationTypeFilter");
        if (compensationType != null)
            headerTableData.add(new String[]{"Вид возмещения затрат:", compensationType.getShortTitle()});
        List<StudentCategory> studentCategoryList = settings.get("studentCategoryFilter");
        if (studentCategoryList != null && !studentCategoryList.isEmpty())
            headerTableData.add(new String[]{"Категория поступающего:", CommonBaseStringUtil.join(studentCategoryList, "title", ", ")});
        List<Qualifications> qualificationsList = settings.get("qualificationFilter");
        if (qualificationsList != null && !qualificationsList.isEmpty())
            headerTableData.add(new String[]{"Квалификация:", CommonBaseStringUtil.join(qualificationsList, "title", ", ")});
        OrgUnit formativeOrgUnit = settings.get("formativeOrgUnitFilter");
        if (formativeOrgUnit != null)
            headerTableData.add(new String[]{"Формирующее подр.:", formativeOrgUnit.getTitle()});
        DevelopForm developForm = settings.get("developFormFilter");
        if (developForm != null)
            headerTableData.add(new String[]{"Форма освоения:", developForm.getTitle()});
        List<DevelopCondition> developConditionList = settings.get("developConditionFilter");
        if (developConditionList != null && !developConditionList.isEmpty())
            headerTableData.add(new String[]{"Условие освоения:", CommonBaseStringUtil.join(developConditionList, "title", ", ")});
        String cardSeria = settings.get("identityCardSeriaFilter");
        if (cardSeria != null)
            headerTableData.add(new String[]{"Серия паспорта:", cardSeria});
        String cardNumber = settings.get("identityCardNumberFilter");
        if (cardNumber != null)
            headerTableData.add(new String[]{"Номер паспорта:", cardNumber});
        IdentifiableWrapper archivalSelectOption = settings.get("archivalFilter");
        if (archivalSelectOption != null)
            headerTableData.add(new String[]{"Статус абитуриента:", archivalSelectOption.getId().equals(Model.SHOW_ARCHIVAL_CODE) ? "Показать архивных" : "Показать не архивных"});
        return headerTableData.toArray(new String[headerTableData.size()][]);
    }
}
