package ru.tandemservice.uniec.entity.orders;

import java.util.List;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.orders.gen.AbstractEntrantParagraphGen;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * Абстрактный параграф на абитуриентов
 */
public abstract class AbstractEntrantParagraph extends AbstractEntrantParagraphGen implements IAbstractParagraph<AbstractEntrantOrder>
{
    @Override
    public List<? extends IAbstractExtract> getExtractList()
    {
        return UniDaoFacade.getCoreDao().getList(AbstractEntrantExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }

    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(AbstractEntrantExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }
}