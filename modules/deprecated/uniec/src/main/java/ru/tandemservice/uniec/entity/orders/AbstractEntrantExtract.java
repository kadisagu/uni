package ru.tandemservice.uniec.entity.orders;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.uniec.dao.IPrintableEnrollmentExtract;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.gen.AbstractEntrantExtractGen;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * Абстрактная выписка на абитуриента
 */
public abstract class AbstractEntrantExtract extends AbstractEntrantExtractGen implements IPrintableEnrollmentExtract<PreliminaryEnrollmentStudent, AbstractEntrantParagraph>
{
    @Override
    public boolean canBePrinted()
    {
        return true;
    }

    @Override
    public String getTitle()
    {
        return "Выписка «" + getType().getTitle() + "» №" + getNumber() + "/" + getParagraph().getNumber() + " из приказа №" + getOrder().getNumber() + " | " + getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
    }

    @Override
    public void setState(ICatalogItem state)
    {
        setState((ExtractStates) state);
    }

    @Override
    public EntrantEnrollmentOrderType getType()
    {
        return getEntity().getEntrantEnrollmentOrderType();
    }

    @Override
    public void setType(ICatalogItem type)
    {
        throw new RuntimeException("You cannot set extractType");
    }

    @Override
    public EnrollmentOrder getOrder()
    {
        return (EnrollmentOrder) getParagraph().getOrder();
    }
}