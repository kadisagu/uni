/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EnrollmentDirectionEdit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.NotUsedTargetAdmissionKind;
import ru.tandemservice.uniec.entity.settings.TargetAdmissionPlanRelation;

/**
 * @author agolubenko
 * @since 04.07.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(Model model)
    {
        model.setEnrollmentDirection(getNotNull(EnrollmentDirection.class, model.getEnrollmentDirection().getId()));

        List<TargetAdmissionKind> targetAdmissionKindList = getTargetAdmissionKinds(model);

        model.setSimpleTargetAdmissionKind(targetAdmissionKindList.size() <= 1);
        model.setTargetAdmissionKindList(targetAdmissionKindList);

        if (!model.isSimpleTargetAdmissionKind())
           for (TargetAdmissionPlanRelation rel : getList(TargetAdmissionPlanRelation.class, TargetAdmissionPlanRelation.L_ENTRANCE_EDUCATION_ORG_UNIT, model.getEnrollmentDirection()))
            {
                if (rel.isBudget())
                    model.getValueMapBudget().put(rel.getTargetAdmissionKind(), rel.getPlanValue());
                else
                    model.getValueMapContract().put(rel.getTargetAdmissionKind(), rel.getPlanValue());
            }
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void update(Model model)
    {
        EnrollmentDirection direction = model.getEnrollmentDirection();

        if (!model.isSimpleTargetAdmissionKind())
        {
            // распеределение для бюджета
            updateTargetAdm(model, true);

            // распеределение для контракта
            updateTargetAdm(model, false);
        }

        //Крым
        if (direction.getCrimeaQuota() != null && direction.getCrimeaQuota() > 0 && !direction.isCrimea())
            direction.setCrimea(true);

        getSession().update(direction);
    }

    private void updateTargetAdm(Model model, boolean budget)
    {
        List<TargetAdmissionPlanRelation> relations = new MQBuilder(TargetAdmissionPlanRelation.ENTITY_CLASS, "r")
                .add(MQExpression.eq("r", TargetAdmissionPlanRelation.L_ENTRANCE_EDUCATION_ORG_UNIT, model.getEnrollmentDirection()))
                .add(MQExpression.eq("r", TargetAdmissionPlanRelation.P_BUDGET, budget))
                .getResultList(getSession());

        List<TargetAdmissionPlanRelation> oldRelations = new ArrayList<TargetAdmissionPlanRelation>(relations);

        Map<Long, TargetAdmissionPlanRelation> id2rel = new HashMap<Long, TargetAdmissionPlanRelation>();
        for (TargetAdmissionPlanRelation rel : relations)
            id2rel.put(rel.getTargetAdmissionKind().getId(), rel);

        Map<TargetAdmissionKind, Integer> valueMap = budget ? model.getValueMapBudget() : model.getValueMapContract();
        int totalPlanValue = 0;
        for (Map.Entry<TargetAdmissionKind, Integer> entry : valueMap.entrySet())
        {
            TargetAdmissionKind targetAdmissionKind = entry.getKey();
            if (null == targetAdmissionKind) continue;
            TargetAdmissionPlanRelation targetAdmPlanRel = id2rel.get(targetAdmissionKind.getId());
            Integer planValue = entry.getValue();
            if (null == planValue) continue;

            if (targetAdmPlanRel == null)
            {
                targetAdmPlanRel = new TargetAdmissionPlanRelation();
                targetAdmPlanRel.setEntranceEducationOrgUnit(model.getEnrollmentDirection());
                targetAdmPlanRel.setTargetAdmissionKind(targetAdmissionKind);
                targetAdmPlanRel.setBudget(budget);
            }

            totalPlanValue += planValue;
            targetAdmPlanRel.setPlanValue(planValue);
            oldRelations.remove(targetAdmPlanRel);
            getSession().saveOrUpdate(targetAdmPlanRel);
        }

        for (TargetAdmissionPlanRelation rel : oldRelations) getSession().delete(rel);

        if (budget)
            model.getEnrollmentDirection().setTargetAdmissionPlanBudget(totalPlanValue);
        else
            model.getEnrollmentDirection().setTargetAdmissionPlanContract(totalPlanValue);
    }

    @Override
    public List<TargetAdmissionKind> getTargetAdmissionKinds(Model model)
    {
        model.setEnrollmentDirection(getNotNull(EnrollmentDirection.class, model.getEnrollmentDirection().getId()));
        MQBuilder builder = new MQBuilder(TargetAdmissionKind.ENTITY_CLASS, "ta");
        MQBuilder notUsed = new MQBuilder(NotUsedTargetAdmissionKind.ENTITY_CLASS, "nu");
        notUsed.add(MQExpression.eq("nu", NotUsedTargetAdmissionKind.enrollmentCampaign().s(), model.getEnrollmentDirection().getEnrollmentCampaign()));
        notUsed.add(MQExpression.eqProperty("nu", NotUsedTargetAdmissionKind.targetAdmissionKind().id().s(), "ta", "id"));
        builder.add(MQExpression.notExists(notUsed));
        List<TargetAdmissionKind> list = builder.getResultList(getSession());
        Collections.sort(list, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);
        return list;
    }
}
