/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 3/25/13
 */
public class EntrantCustomStateSearchListDSHandler extends DefaultSearchDataSourceHandler
{
    public EntrantCustomStateSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long entrantCustomStateCI = context.get("entrantCustomStateCI");
        Long entrantId = context.get("entrantId");
        Date activeDateFrom = context.get("activeDateFrom");
        Date activeDateTo = context.get("activeDateTo");

        if (activeDateFrom != null && activeDateTo != null && activeDateFrom.after(activeDateTo))
            return new DSOutput(input);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantCustomState.class, "st")
                .column("st")
                .fetchPath(DQLJoinType.inner, EntrantCustomState.customState().fromAlias("st"), "customState")
                .where(eq(property("st", EntrantCustomState.entrant()), value(entrantId)));

        if (null != entrantCustomStateCI)
            builder.where(eq(property("", EntrantCustomState.customState()), value(entrantCustomStateCI)));

        FilterUtils.applyIntersectPeriodFilter(
                builder, "st", EntrantCustomState.P_BEGIN_DATE, EntrantCustomState.P_END_DATE, activeDateFrom, activeDateTo);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
