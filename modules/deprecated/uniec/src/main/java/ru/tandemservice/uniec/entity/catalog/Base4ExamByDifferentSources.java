package ru.tandemservice.uniec.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.catalog.gen.*;

/** @see ru.tandemservice.uniec.entity.catalog.gen.Base4ExamByDifferentSourcesGen */
public class Base4ExamByDifferentSources extends Base4ExamByDifferentSourcesGen
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, Base4ExamByDifferentSources.class)
                .order(Base4ExamByDifferentSources.title())
                .filter(Base4ExamByDifferentSources.title());
    }
}