/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.dao;

import java.util.List;
import java.util.Map;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

/**
 * @author agolubenko
 * @since 11.03.2009
 */
public interface IStateExamCertificateDAO extends IUniBaseDao
{
    /**
     * Возвращает оценки по предметам данных свидетельств ЕГЭ
     * 
     * @param certificates список свидетельств ЕГЭ
     * @return [Свидетельство ЕГЭ] -> [Оценки по предметам]
     */
    Map<EntrantStateExamCertificate, List<StateExamSubjectMark>> prepareCertificatesSubjectsMarks(List<EntrantStateExamCertificate> certificates);

    /**
     * Меняет свойство "Оригинал" свидетельства ЕГЭ на противоположное значение
     * @param id идентификатор свидетельства
     */
    void changeCertificateOriginal(Long id);

    /**
     * Меняет свойство "Зачтено" свидетельства ЕГЭ на противоположное значение
     * @param id идентификатор свидетельства
     */
    void changeCertificateChecked(Long id);

    /**
     * Меняет свойство "Отправлялось на проверку в ФБС" свидетельства ЕГЭ на противоположное значение
     * @param id идентификатор свидетельства
     */
    void changeCertificateSent(Long id);
}
