/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.report.EntranceExamMeetingByCK2010.EntranceExamMeetingByCKAdd;

import java.util.List;

import org.tandemframework.core.common.ITitled;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

/**
 * @author Vasily Zhukov
 * @since 06.08.2010
 */
class EnrollmentDirectionGroup implements ITitled
{
    private String _title;                                      // название группы
    private EducationLevels _educationLevels;                   // группирующий ур.обр. ВПО
    private List<EnrollmentDirection> _enrollmentDirectionList; // направления, входящие в группу
    private OrgUnit _formativeOrgUnit;
    private DevelopForm _developForm;
    private DevelopCondition _developCondition;
    private DevelopTech _developTech;
    private DevelopPeriod _developPeriod;

    EnrollmentDirectionGroup(String title, EducationLevels educationLevels, List<EnrollmentDirection> enrollmentDirectionList, OrgUnit formativeOrgUnit, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod)
    {
        _title = title;
        _educationLevels = educationLevels;
        _enrollmentDirectionList = enrollmentDirectionList;
        _formativeOrgUnit = formativeOrgUnit;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
    }

    // Getters

    @Override
    public String getTitle()
    {
        return _title;
    }

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public List<EnrollmentDirection> getEnrollmentDirectionList()
    {
        return _enrollmentDirectionList;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }
}
