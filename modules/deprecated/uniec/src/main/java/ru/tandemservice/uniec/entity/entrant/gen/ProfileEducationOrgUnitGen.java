package ru.tandemservice.uniec.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Профиль направления подготовки подразделения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProfileEducationOrgUnitGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit";
    public static final String ENTITY_NAME = "profileEducationOrgUnit";
    public static final int VERSION_HASH = 203840348;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_BUDGET_PLAN = "budgetPlan";
    public static final String P_CONTRACT_PLAN = "contractPlan";

    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private EducationOrgUnit _educationOrgUnit;     // Параметры обучения студентов по направлению подготовки (НПП)
    private Integer _budgetPlan;     // План приема на бюджет
    private Integer _contractPlan;     // План приема по договору

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема. Свойство не может быть null.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return План приема на бюджет.
     */
    public Integer getBudgetPlan()
    {
        return _budgetPlan;
    }

    /**
     * @param budgetPlan План приема на бюджет.
     */
    public void setBudgetPlan(Integer budgetPlan)
    {
        dirty(_budgetPlan, budgetPlan);
        _budgetPlan = budgetPlan;
    }

    /**
     * @return План приема по договору.
     */
    public Integer getContractPlan()
    {
        return _contractPlan;
    }

    /**
     * @param contractPlan План приема по договору.
     */
    public void setContractPlan(Integer contractPlan)
    {
        dirty(_contractPlan, contractPlan);
        _contractPlan = contractPlan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ProfileEducationOrgUnitGen)
        {
            setEnrollmentDirection(((ProfileEducationOrgUnit)another).getEnrollmentDirection());
            setEducationOrgUnit(((ProfileEducationOrgUnit)another).getEducationOrgUnit());
            setBudgetPlan(((ProfileEducationOrgUnit)another).getBudgetPlan());
            setContractPlan(((ProfileEducationOrgUnit)another).getContractPlan());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProfileEducationOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProfileEducationOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new ProfileEducationOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "budgetPlan":
                    return obj.getBudgetPlan();
                case "contractPlan":
                    return obj.getContractPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "budgetPlan":
                    obj.setBudgetPlan((Integer) value);
                    return;
                case "contractPlan":
                    obj.setContractPlan((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "budgetPlan":
                        return true;
                case "contractPlan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "budgetPlan":
                    return true;
                case "contractPlan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "budgetPlan":
                    return Integer.class;
                case "contractPlan":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProfileEducationOrgUnit> _dslPath = new Path<ProfileEducationOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProfileEducationOrgUnit");
    }
            

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return План приема на бюджет.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getBudgetPlan()
     */
    public static PropertyPath<Integer> budgetPlan()
    {
        return _dslPath.budgetPlan();
    }

    /**
     * @return План приема по договору.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getContractPlan()
     */
    public static PropertyPath<Integer> contractPlan()
    {
        return _dslPath.contractPlan();
    }

    public static class Path<E extends ProfileEducationOrgUnit> extends EntityPath<E>
    {
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<Integer> _budgetPlan;
        private PropertyPath<Integer> _contractPlan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return План приема на бюджет.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getBudgetPlan()
     */
        public PropertyPath<Integer> budgetPlan()
        {
            if(_budgetPlan == null )
                _budgetPlan = new PropertyPath<Integer>(ProfileEducationOrgUnitGen.P_BUDGET_PLAN, this);
            return _budgetPlan;
        }

    /**
     * @return План приема по договору.
     * @see ru.tandemservice.uniec.entity.entrant.ProfileEducationOrgUnit#getContractPlan()
     */
        public PropertyPath<Integer> contractPlan()
        {
            if(_contractPlan == null )
                _contractPlan = new PropertyPath<Integer>(ProfileEducationOrgUnitGen.P_CONTRACT_PLAN, this);
            return _contractPlan;
        }

        public Class getEntityClass()
        {
            return ProfileEducationOrgUnit.class;
        }

        public String getEntityName()
        {
            return "profileEducationOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
