/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.menu.examGroup.abstractExamGroupList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 28.05.2010
 */
public interface IExamGroupListDAO<Model extends ExamGroupListModel> extends IUniDao<Model>
{
}
