/**
 *$Id$
 */
package ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDirProfAdd;

import jxl.write.WriteException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.base.bo.EcReport.EcReportManager;
import ru.tandemservice.uniec.base.bo.EcReport.logic.EnrollmentDirProfReport.EnrollmentDirProfReportModel;
import ru.tandemservice.uniec.base.bo.EcReport.ui.EnrollmentDirProfPub.EcReportEnrollmentDirProfPub;
import ru.tandemservice.uniec.entity.report.EnrollmentDirProfReport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 03.07.12
 */
public class EcReportEnrollmentDirProfAddUI extends UIPresenter
{
    // fields

    private EnrollmentDirProfReportModel _model;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _model = new EnrollmentDirProfReportModel();

        EcReportManager.instance().enrollmentDirProfReportDAO().prepareModel(_model);
    }


    // Listeners

    public void onClickApply()
    {
        DatabaseFile reportFile;
        try
        {
            reportFile = EcReportManager.instance().enrollmentDirProfReportDAO().createPrintReportFile(_model);
        }
        catch (IOException | WriteException e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }

        EnrollmentDirProfReport report = EcReportManager.instance().enrollmentDirProfReportDAO().createReport(_model, reportFile);

        DataAccessServices.dao().save(reportFile);
        DataAccessServices.dao().save(report);

        deactivate();

        getActivationBuilder().asDesktopRoot(EcReportEnrollmentDirProfPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    // Getters & Setters

    public EnrollmentDirProfReportModel getModel()
    {
        return _model;
    }

    public void setModel(EnrollmentDirProfReportModel model)
    {
        _model = model;
    }
}
