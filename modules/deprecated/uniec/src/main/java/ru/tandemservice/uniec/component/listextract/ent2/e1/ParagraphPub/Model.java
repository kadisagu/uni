/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e1.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.uniec.entity.orders.SplitBudgetMasterEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class Model extends AbstractListParagraphPubModel<SplitBudgetMasterEntrantsStuListExtract>
{
}