/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.print.preenroll;

import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintMapColumn;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAddUI;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.util.EcReportPersonUtil;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 05.09.2011
 */
public class PreEnrollPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _formativeOrgUnitPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _territorialOrgUnitPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _educationOrgUnitPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _developFormPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _compensationTypePre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _studentCategoryPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _targetAdmissionPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentOrderPre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentOrderDatePre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentOrderTypePre = new ReportPrintCheckbox();
    private IReportPrintCheckbox _enrollmentGroupPre = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 10; i++)
            ids.add("chPreEnroll" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {
        // соединяем абитуриента через left join, потому что мог быть выбран фильтр "не является абитуриентом"
        // если абитуриент был ранее соединен по inner join, то останется inner join
        String entrantAlias = dql.leftJoinEntity(Entrant.class, Entrant.person());

        // соединяем заявление абитуриента
        String requestAlias = dql.leftJoinEntity(entrantAlias, EntrantRequest.class, EntrantRequest.entrant());

        // соединяем выбранное направление абитуриента
        String directionAlias = dql.leftJoinEntity(requestAlias, RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest());

        // добавляем колонку entrant, агрегируем в список
        final int entrantIndex = dql.addListAggregateColumn(entrantAlias);

        // добавляем колонку request, агрегируем в мап
        final int requestIndex = dql.addMapAggregateColumn(requestAlias, entrantIndex);

        // добавляем колонку preStudent, агрегируем в мап
        final int directionIndex = dql.addMapAggregateColumn(directionAlias, entrantIndex, requestIndex);

        if (_formativeOrgUnitPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "formativeOrgUnitPre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.educationOrgUnit().formativeOrgUnit().title(), null));

        if (_territorialOrgUnitPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "territorialOrgUnitPre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.educationOrgUnit().territorialOrgUnit().territorialTitle(), null));

        if (_educationOrgUnitPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "educationOrgUnitPre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.educationOrgUnit().educationLevelHighSchool().printTitle(), null));

        if (_developFormPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "developFormPre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.educationOrgUnit().developForm().title(), null));

        if (_compensationTypePre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "compensationTypePre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.compensationType().shortTitle(), null));

        if (_studentCategoryPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "studentCategoryPre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.studentCategory().title(), null));

        if (_targetAdmissionPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new PreEnrollUniReportPrintMapColumn(printInfo, "targetAdmissionPre", entrantIndex, directionIndex, PreliminaryEnrollmentStudent.targetAdmission(), YesNoFormatter.INSTANCE));

        if (_enrollmentOrderPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new EnrollmentExtractUniReportPrintMapColumn(printInfo, "enrollmentOrderPre", entrantIndex, directionIndex, EnrollmentExtract.paragraph().order().number(), null));

        if (_enrollmentOrderDatePre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new EnrollmentExtractUniReportPrintMapColumn(printInfo, "enrollmentOrderDatePre", entrantIndex, directionIndex, EnrollmentExtract.paragraph().order().commitDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        if (_enrollmentOrderTypePre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new EnrollmentExtractUniReportPrintMapColumn(printInfo, "enrollmentOrderTypePre", entrantIndex, directionIndex, new PropertyPath(EnrollmentExtract.paragraph().order().s() + ".type.shortTitle"), null));

        if (_enrollmentGroupPre.isActive())
            printInfo.addPrintColumn(EcReportPersonAddUI.ENTRANT_SCHEET, new EnrollmentExtractUniReportPrintMapColumn(printInfo, "enrollmentGroupPre", entrantIndex, directionIndex, EnrollmentExtract.groupTitle(), null));
    }

    // Getters

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getFormativeOrgUnitPre()
    {
        return _formativeOrgUnitPre;
    }

    public IReportPrintCheckbox getTerritorialOrgUnitPre()
    {
        return _territorialOrgUnitPre;
    }

    public IReportPrintCheckbox getEducationOrgUnitPre()
    {
        return _educationOrgUnitPre;
    }

    public IReportPrintCheckbox getDevelopFormPre()
    {
        return _developFormPre;
    }

    public IReportPrintCheckbox getCompensationTypePre()
    {
        return _compensationTypePre;
    }

    public IReportPrintCheckbox getStudentCategoryPre()
    {
        return _studentCategoryPre;
    }

    public IReportPrintCheckbox getTargetAdmissionPre()
    {
        return _targetAdmissionPre;
    }

    public IReportPrintCheckbox getEnrollmentOrderPre()
    {
        return _enrollmentOrderPre;
    }

    public IReportPrintCheckbox getEnrollmentOrderTypePre()
    {
        return _enrollmentOrderTypePre;
    }

    public IReportPrintCheckbox getEnrollmentGroupPre()
    {
        return _enrollmentGroupPre;
    }

    // class

    private static class PreEnrollUniReportPrintMapColumn extends ReportPrintMapColumn
    {
        private IReportPrintInfo _printInfo;
        private int _entrantIndex;

        public PreEnrollUniReportPrintMapColumn(final IReportPrintInfo printInfo, String name, int entrantIndex, int directionIndex, final IPropertyPath preStudentPath, final IFormatter formatter)
        {
            super(name, directionIndex, null, new IFormatter<RequestedEnrollmentDirection>()
                {
                @SuppressWarnings("unchecked")
                @Override
                public String format(RequestedEnrollmentDirection source)
                {
                    if (source == null) return null;
                    PreliminaryEnrollmentStudent preStudent = EcReportPersonUtil.getPreStudent(printInfo, source);
                    if (preStudent == null) return null;
                    Object obj = preStudent.getProperty(preStudentPath);
                    if (obj == null) return null;
                    return formatter == null ? obj.toString() : formatter.format(obj);
                }
                });

            _printInfo = printInfo;
            _entrantIndex = entrantIndex;
        }

        @Override
        public void prefetch(List<Object[]> rows)
        {
            EcReportPersonUtil.prefetchPreStudentMap(_printInfo, rows, _entrantIndex);
        }
    }

    private static class EnrollmentExtractUniReportPrintMapColumn extends ReportPrintMapColumn
    {
        private IReportPrintInfo _printInfo;
        private int _entrantIndex;

        @SuppressWarnings("unchecked")
        public EnrollmentExtractUniReportPrintMapColumn(final IReportPrintInfo printInfo, String name, int entrantIndex, int directionIndex, final IPropertyPath extractPath, final IFormatter formatter)
        {
            super(name, directionIndex, null, source -> {
                if (source == null) return null;
                EnrollmentExtract extract = EcReportPersonUtil.getEnrollmentExtract(printInfo, (RequestedEnrollmentDirection) source);
                if (extract == null) return null;
                Object obj = extract.getProperty(extractPath);
                if (obj == null) return null;
                return formatter == null ? obj.toString() : formatter.format(obj);
            });

            _printInfo = printInfo;
            _entrantIndex = entrantIndex;
        }

        @Override
        public void prefetch(List<Object[]> rows)
        {
            EcReportPersonUtil.prefetchEnrollmentExtractMap(_printInfo, rows, _entrantIndex);
        }
    }

    public IReportPrintCheckbox getEnrollmentOrderDatePre()
    {
        return _enrollmentOrderDatePre;
    }
}
