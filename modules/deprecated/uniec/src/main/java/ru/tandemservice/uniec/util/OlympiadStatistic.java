package ru.tandemservice.uniec.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Объект помогающий определять зачитывается ли выбранная дисциплина за диплом олимпиады
 *
 * @author Vasily Zhukov
 * @since 05.04.2011
 */
public class OlympiadStatistic
{
    private Map<Key, Set<OlympiadDiploma>> _key2olympiadMap = new HashMap<>();

    public OlympiadStatistic(Map<Key, Set<OlympiadDiploma>> key2olympiadMap)
    {
        _key2olympiadMap = key2olympiadMap;
    }

    public Set<OlympiadDiploma> getDiplomaSet(ChosenEntranceDiscipline chosenDiscipline)
    {
        final RequestedEnrollmentDirection requestedDirection = chosenDiscipline.getChosenEnrollmentDirection();
        final EnrollmentDirection direction = requestedDirection.getEnrollmentDirection();
        final CompensationType compensationType = requestedDirection.getCompensationType();
        final Discipline2RealizationWayRelation discipline = chosenDiscipline.getEnrollmentCampaignDiscipline();

        Set<OlympiadDiploma> diplomaSet = new HashSet<>();

        Set<OlympiadDiploma> set;

        // сейчас надо проверить 4 способа зачтения

        // за дисциплину (за все направления приема и виды затрат)
        set = _key2olympiadMap.get(new Key(null, null, discipline));
        if (set != null) diplomaSet.addAll(set);

        // за дисциплина и вид затрат (и за все направления приема)
        set = _key2olympiadMap.get(new Key(null, compensationType, discipline));
        if (set != null) diplomaSet.addAll(set);

        // за направление приема и дисциплину (и за все виды затрат)
        set = _key2olympiadMap.get(new Key(direction, null, discipline));
        if (set != null) diplomaSet.addAll(set);

        // за направление приема, вид затрат и дисциплину
        set = _key2olympiadMap.get(new Key(direction, compensationType, discipline));
        if (set != null) diplomaSet.addAll(set);

        return diplomaSet.isEmpty() ? null : diplomaSet;
    }

    public static class Key extends MultiKey
    {
        private static final long serialVersionUID = 1L;

        public Key(EnrollmentDirection direction, CompensationType compensationType, Discipline2RealizationWayRelation discipline)
        {
            super(direction == null ? null : (direction.getCompetitionGroup() == null ? direction : direction.getCompetitionGroup()), compensationType, discipline);
        }
    }
}
