/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantAgeDistrib.EntrantAgeDistribAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.*;

/**
 * @author ekachanova
 */
class EntrantAgeDistribReportBuilder
{
    private static int ROWS = 6;
    private static int COLS = 9;

    private Model _model;
    private Session _session;

    EntrantAgeDistribReportBuilder(Model model, Session session)
    {
        _session = session;
        _model = model;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        Object[] orgUnits = getOrgUnits();
        List<EducationOrgUnit> educationOrgUnits = (List) orgUnits[0];
        List<OrgUnit> formOrgUnits = (List) orgUnits[1];

        Map<OrgUnit, Map<Integer, Object[][]>> formOrgUnitsData = new HashMap<>();
        //init structure
        for (OrgUnit formOrgUnit : formOrgUnits)
            formOrgUnitsData.put(formOrgUnit, initDataItem());
        Map<Integer, Object[][]> totalData = initDataItem();

        //builders
        List<MQBuilder> entrantBuilders = new ArrayList<>();
        {   //по ходу приема документов, индекс = 0
            entrantBuilders.add(getRequestedBuilderBase(educationOrgUnits));
        }
        {   //по результатам сдачи вступительных испытаний, индекс = 1
            final MQBuilder requestedBuilder = getRequestedBuilderBase(educationOrgUnits);
            requestedBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
            requestedBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            requestedBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_ACTIVE_CODE));

            entrantBuilders.add(requestedBuilder);
        }
        {   //по результатам сдачи вступительных испытаний, индекс = 2
            MQBuilder preliminaryBuilder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{
                    PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT,
                    PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE, "id"});
            if (_model.getReport().getCompensationType() != null)
                preliminaryBuilder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
            if (!_model.getStudentCategoryList().isEmpty())
                preliminaryBuilder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            if (_model.isParallelActive() && _model.getParallel().isTrue())
                preliminaryBuilder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.P_PARALLEL, Boolean.FALSE));
            preliminaryBuilder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, educationOrgUnits));

            preliminaryBuilder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
            preliminaryBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
            preliminaryBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
            preliminaryBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
            preliminaryBuilder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

            preliminaryBuilder.addJoin("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
            preliminaryBuilder.addSelect("idCard", new String[]{IdentityCard.P_BIRTH_DATE, IdentityCard.L_SEX + "." + Sex.P_CODE});

            entrantBuilders.add(preliminaryBuilder);
        }

        //fill structure
        Date today = CoreDateUtils.getDayFirstTimeMoment(new Date());
        for (int i = 0; i < entrantBuilders.size(); i++)
        {
            // i - это номер стадии приемной кампании
            MQBuilder builder = entrantBuilders.get(i);
            for (Object[] row : builder.<Object[]>getResultList(_session))
            {
                OrgUnit formOrgUnit = (OrgUnit) row[0];
                String compensTypeCode = (String) row[1];
                Long id = (Long) row[2];    //IdentityCard (для стадий По ходу приема... и По результатам сдачи...) или PreliminaryEnrollmentStudentId (для По результатам зачисления)
                Date birthDate = row[3] == null ? today : (Date) row[3];
                String sexCode = (String) row[4];

                int columnIndex = getColumnIndex(birthDate);

                Object[][] set = formOrgUnitsData.get(formOrgUnit).get(i);
                fillData(set, id, compensTypeCode, columnIndex, 0);
                if (sexCode.equals(SexCodes.FEMALE))
                    fillData(set, id, compensTypeCode, columnIndex, 3);

                Object[][] totalSet = totalData.get(i);
                fillData(totalSet, id, compensTypeCode, columnIndex, 0);
                if (sexCode.equals(SexCodes.FEMALE))
                    fillData(totalSet, id, compensTypeCode, columnIndex, 3);
            }
        }

        for (OrgUnit formOrgUnit : formOrgUnitsData.keySet())
        {
            for (int i = 0; i < 3; i++)
            {
                Object[][] set = formOrgUnitsData.get(formOrgUnit).get(i);
                Object[][] totalSet = totalData.get(i);
                for (int k = 0; k < COLS; k++)
                {
                    fillTotalCell(set, 0, 1, 2, k);
                    fillTotalCell(set, 3, 4, 5, k);
                    fillTotalCell(totalSet, 0, 1, 2, k);
                    fillTotalCell(totalSet, 3, 4, 5, k);
                }
            }
        }

        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_AGE_DISTRIB);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        final IRtfControl b = RtfBean.getElementFactory().createRtfControl(IRtfData.B);
        final IRtfControl b0 = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);
//        b0.setValue(0);
        final IRtfControl page = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        final IRtfElement par = RtfBean.getElementFactory().createRtfControl(IRtfData.PAR);
        final IRtfElement pard = RtfBean.getElementFactory().createRtfControl(IRtfData.PARD);
        final IRtfControl cf0 = RtfBean.getElementFactory().createRtfControl(IRtfData.CF, 0);
//        cf0.setValue(0);

        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        UniRtfUtil.fillCell(table.getRowList().get(0).getCellList().get(0), "");
        document.getElementList().remove(table);

        RtfInjectModifier iModifier = new RtfInjectModifier();
        iModifier.put("HS", TopOrgUnit.getInstance().getTitle())
                .put("DATE", DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getReport().getFormingDate()));
        iModifier.modify(document);

        for (OrgUnit formOrgUnit : formOrgUnits)
        {
            IRtfText formOrgUnitTitle = RtfBean.getElementFactory().createRtfText(StringUtils.isNotEmpty(formOrgUnit.getNominativeCaseTitle())
                    ? formOrgUnit.getNominativeCaseTitle() : formOrgUnit.getTitle());
            document.getElementList().add(pard);
            document.getElementList().add(cf0);
            document.getElementList().add(b);
            document.getElementList().add(formOrgUnitTitle);
            document.getElementList().add(b0);
            document.getElementList().add(par);

            RtfTable part = table.getClone();
            fillRtfTable(part, formOrgUnitsData.get(formOrgUnit));
            document.getElementList().add(part);
            document.getElementList().add(page);
        }

        IRtfText totalTitle = RtfBean.getElementFactory().createRtfText("Итого по " + TopOrgUnit.getInstance().getShortTitle());
        document.getElementList().add(pard);
        document.getElementList().add(cf0);
        document.getElementList().add(b);
        document.getElementList().add(totalTitle);
        document.getElementList().add(b0);
        document.getElementList().add(par);

        RtfTable part = table.getClone();
        fillRtfTable(part, totalData);
        document.getElementList().add(part);

        return RtfUtil.toByteArray(document);
    }

    @SuppressWarnings("unchecked")
    private static void fillTotalCell(Object[][] set, int budgetRow, int contractRow, int totalRow, int column)
    {
        set[totalRow][column] = (set[budgetRow][column] == null ? 0 : ((Collection) set[budgetRow][column]).size()) + (set[contractRow][column] == null ? 0 : ((Collection) set[contractRow][column]).size());
    }

    private static void fillRtfTable(RtfTable table, Map<Integer, Object[][]> tableData)
    {
        int rowShift = 4;
        int colShift = 2;
        for (int i = 0; i < 3; i++)
        {
            String[][] data = convert(tableData.get(i));
            List<RtfRow> rows = table.getRowList().subList(rowShift, rowShift + ROWS);
            for (int k = 0; k < rows.size(); k++)
            {
                RtfRow row = rows.get(k);
                List<RtfCell> cells = row.getCellList().subList(colShift, colShift + COLS);
                for (int n = 0; n < cells.size(); n++)
                {
                    RtfCell cell = cells.get(n);
                    UniRtfUtil.fillCell(cell, data[k][n]);
                }
            }
            rowShift += 7;
        }
    }

    @SuppressWarnings("unchecked")
    private static String[][] convert(Object[][] set)
    {
        String[][] result = new String[ROWS][COLS];
        for (int i = 0; i < ROWS; i++)
            for (int j = 0; j < COLS; j++)
                result[i][j] = set[i][j] == null ? "0" : set[i][j] instanceof Collection ? String.valueOf(((Collection) set[i][j]).size()) : set[i][j].toString();
        return result;
    }

    private static int getColumnIndex(Date birthDate)
    {
        int age = CoreDateUtils.getFullYearsAge(birthDate);
        if (age <= 17)
            return 1;
        else if (age >= 24)
            return 8;
        else
            return age - 16;
    }

    private static Map<Integer, Object[][]> initDataItem()
    {
        Map<Integer, Object[][]> dataItem = new HashMap<>();
        for (int i = 0; i < 3; i++)
        {
            dataItem.put(i, new Object[ROWS][COLS]);
        }
        return dataItem;
    }

    private static void fillData(Object[][] set, Long idCardId, String compensTypeCode, int columnIndex, int inc)
    {
        if (compensTypeCode.equals(UniDefines.COMPENSATION_TYPE_BUDGET))
        {
            getSet(set, inc, columnIndex).add(idCardId);
            getSet(set, inc, 0).add(idCardId);
        } else if (compensTypeCode.equals(UniDefines.COMPENSATION_TYPE_CONTRACT))
        {
            getSet(set, 1 + inc, columnIndex).add(idCardId);
            getSet(set, 1 + inc, 0).add(idCardId);
        }
    }

    @SuppressWarnings("unchecked")
    private static Collection<Long> getSet(Object[][] set, int row, int col)
    {
        Collection<Long> idCardIds = (Collection<Long>) set[row][col];
        if (idCardIds == null)
            set[row][col] = (idCardIds = new HashSet<>());
        return idCardIds;
    }

    private MQBuilder getRequestedBuilderBase(List<EducationOrgUnit> educationOrgUnits)
    {
        final MQBuilder requestedBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d", new String[]{
                RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT,
                RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE});
        requestedBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        requestedBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));
        requestedBuilder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, educationOrgUnits));
        requestedBuilder.add(UniMQExpression.betweenDate("d", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        requestedBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        if (_model.getReport().getCompensationType() != null)
            requestedBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        if (!_model.getStudentCategoryList().isEmpty())
            requestedBuilder.add(MQExpression.in("d", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));

        requestedBuilder.addJoin("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        requestedBuilder.addSelect("idCard", new String[]{"id", IdentityCard.P_BIRTH_DATE, IdentityCard.L_SEX + "." + Sex.P_CODE});

        return requestedBuilder;
    }

    private Object[] getOrgUnits()
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.ENTITY_CLASS, "d", new String[]{EnrollmentDirection.L_EDUCATION_ORG_UNIT});
        builder.add(MQExpression.eq("d", EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getReport().getEnrollmentCampaign()));

        builder.addJoin("d", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.L_EDUCATION_LEVEL + "." + EducationLevels.L_QUALIFICATION, _model.getQualificationList()));

        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));

        if (_model.isTerritorialOrgUnitActive())
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ?
                    MQExpression.isNull("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) :
                    MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));

        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));

        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));

        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));

        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));

        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        List<OrgUnit> educationOrgUnitList = builder.getResultList(_session);
        List<OrgUnit> formativeOrgUnitList = CommonBaseUtil.getPropertiesList(educationOrgUnitList, EducationOrgUnit.L_FORMATIVE_ORG_UNIT);
        List<OrgUnit> difFormativeOrgUnitList = new ArrayList<>(new HashSet<>(formativeOrgUnitList));
        Collections.sort(difFormativeOrgUnitList, (o1, o2) -> {
            int result = Integer.compare(o1.getOrgUnitType().getPriority(), o2.getOrgUnitType().getPriority());
            if (result == 0)
                result = o1.getTitle().compareTo(o2.getTitle());
            return result;
        });
        return new Object[]{educationOrgUnitList, difFormativeOrgUnitList};
    }
}
