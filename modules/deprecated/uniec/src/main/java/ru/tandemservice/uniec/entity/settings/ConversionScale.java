package ru.tandemservice.uniec.entity.settings;

import org.tandemframework.core.common.ITitled;

import ru.tandemservice.uniec.entity.settings.gen.ConversionScaleGen;

public class ConversionScale extends ConversionScaleGen implements ITitled
{
    @Override
    public String getTitle()
    {
        return getDiscipline().getTitle();
    }

    public void setTransformation(double internalMark, byte externalMark, boolean budget)
    {
        getScale(budget)[getIndexNumber(internalMark)] = externalMark;
    }

    public int getIndexNumber(double internalMark)
    {
        double result = ((internalMark - getDiscipline().getMinMark()) / getDiscipline().getStep());
        if (result % 1 != 0.0)
        {
            throw new IllegalArgumentException();
        }
        return (int) result;
    }

    public void setScale(byte[] scale, boolean budget)
    {
        if (budget) {
            setBudgetScale(scale);
        } else {
            setContractScale(scale);
        }
    }

    public byte[] getScale(boolean budget)
    {
        return (budget) ? getBudgetScale() : getContractScale();
    }

    public double getInternalMark(int externalMark, boolean budget)
    {
        byte[] scale = getScale(budget);
        // если нет различий в приеме на бюджет\контракт - берем шкалу бюджета
        if (!getDiscipline().getEnrollmentCampaign().isEnrollmentPerCompTypeDiff())
            scale = getScale(true);
        int i = 0;
        while (i < (scale.length - 1) && !(scale[i] <= externalMark && externalMark < scale[i + 1])) i++;
        if (i < scale.length) return getDiscipline().getMinMark() + i * getDiscipline().getStep();
        return getDiscipline().getMaxMark();
    }
}