/* $Id$ */
package ru.tandemservice.uniec.component.report.AdmittedEntrantsDataExport.Add;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 24.07.12
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }
}
