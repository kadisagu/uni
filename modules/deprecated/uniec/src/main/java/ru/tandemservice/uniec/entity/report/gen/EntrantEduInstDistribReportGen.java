package ru.tandemservice.uniec.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение абитуриентов по законченным образовательным учреждениям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantEduInstDistribReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport";
    public static final String ENTITY_NAME = "entrantEduInstDistribReport";
    public static final int VERSION_HASH = 1693752640;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_STAGE_TITLE = "stageTitle";
    public static final String P_GROUP = "group";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY = "studentCategory";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";

    private DatabaseFile _content;     // Печатная форма
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _formingDate;     // Дата формирования
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _stageTitle;     // Базовое (полученное) образование
    private String _group;     // Группировать по формирующим подразделениям и направлениям подготовки
    private String _compensationType;     // Вид возмещения затрат
    private String _studentCategory;     // Категория поступающего
    private String _qualification;     // Квалификация
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _educationLevelHighSchool;     // Направления подготовки (специальности)
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Базовое (полученное) образование.
     */
    @Length(max=255)
    public String getStageTitle()
    {
        return _stageTitle;
    }

    /**
     * @param stageTitle Базовое (полученное) образование.
     */
    public void setStageTitle(String stageTitle)
    {
        dirty(_stageTitle, stageTitle);
        _stageTitle = stageTitle;
    }

    /**
     * @return Группировать по формирующим подразделениям и направлениям подготовки.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группировать по формирующим подразделениям и направлениям подготовки.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория поступающего.
     */
    public void setStudentCategory(String studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Направления подготовки (специальности).
     */
    public String getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направления подготовки (специальности).
     */
    public void setEducationLevelHighSchool(String educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantEduInstDistribReportGen)
        {
            setContent(((EntrantEduInstDistribReport)another).getContent());
            setEnrollmentCampaign(((EntrantEduInstDistribReport)another).getEnrollmentCampaign());
            setFormingDate(((EntrantEduInstDistribReport)another).getFormingDate());
            setDateFrom(((EntrantEduInstDistribReport)another).getDateFrom());
            setDateTo(((EntrantEduInstDistribReport)another).getDateTo());
            setStageTitle(((EntrantEduInstDistribReport)another).getStageTitle());
            setGroup(((EntrantEduInstDistribReport)another).getGroup());
            setCompensationType(((EntrantEduInstDistribReport)another).getCompensationType());
            setStudentCategory(((EntrantEduInstDistribReport)another).getStudentCategory());
            setQualification(((EntrantEduInstDistribReport)another).getQualification());
            setFormativeOrgUnit(((EntrantEduInstDistribReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((EntrantEduInstDistribReport)another).getTerritorialOrgUnit());
            setEducationLevelHighSchool(((EntrantEduInstDistribReport)another).getEducationLevelHighSchool());
            setDevelopForm(((EntrantEduInstDistribReport)another).getDevelopForm());
            setDevelopCondition(((EntrantEduInstDistribReport)another).getDevelopCondition());
            setDevelopTech(((EntrantEduInstDistribReport)another).getDevelopTech());
            setDevelopPeriod(((EntrantEduInstDistribReport)another).getDevelopPeriod());
            setExcludeParallel(((EntrantEduInstDistribReport)another).getExcludeParallel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantEduInstDistribReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantEduInstDistribReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantEduInstDistribReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "formingDate":
                    return obj.getFormingDate();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "stageTitle":
                    return obj.getStageTitle();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategory":
                    return obj.getStudentCategory();
                case "qualification":
                    return obj.getQualification();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "excludeParallel":
                    return obj.getExcludeParallel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "stageTitle":
                    obj.setStageTitle((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "studentCategory":
                    obj.setStudentCategory((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "formingDate":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "stageTitle":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategory":
                        return true;
                case "qualification":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "excludeParallel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "formingDate":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "stageTitle":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategory":
                    return true;
                case "qualification":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "excludeParallel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "formingDate":
                    return Date.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "stageTitle":
                    return String.class;
                case "group":
                    return String.class;
                case "compensationType":
                    return String.class;
                case "studentCategory":
                    return String.class;
                case "qualification":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "educationLevelHighSchool":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "excludeParallel":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantEduInstDistribReport> _dslPath = new Path<EntrantEduInstDistribReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantEduInstDistribReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Базовое (полученное) образование.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getStageTitle()
     */
    public static PropertyPath<String> stageTitle()
    {
        return _dslPath.stageTitle();
    }

    /**
     * @return Группировать по формирующим подразделениям и направлениям подготовки.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getStudentCategory()
     */
    public static PropertyPath<String> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getEducationLevelHighSchool()
     */
    public static PropertyPath<String> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    public static class Path<E extends EntrantEduInstDistribReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _stageTitle;
        private PropertyPath<String> _group;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _studentCategory;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _educationLevelHighSchool;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<Boolean> _excludeParallel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantEduInstDistribReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantEduInstDistribReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantEduInstDistribReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Базовое (полученное) образование.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getStageTitle()
     */
        public PropertyPath<String> stageTitle()
        {
            if(_stageTitle == null )
                _stageTitle = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_STAGE_TITLE, this);
            return _stageTitle;
        }

    /**
     * @return Группировать по формирующим подразделениям и направлениям подготовки.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getStudentCategory()
     */
        public PropertyPath<String> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getEducationLevelHighSchool()
     */
        public PropertyPath<String> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(EntrantEduInstDistribReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.uniec.entity.report.EntrantEduInstDistribReport#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(EntrantEduInstDistribReportGen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

        public Class getEntityClass()
        {
            return EntrantEduInstDistribReport.class;
        }

        public String getEntityName()
        {
            return "entrantEduInstDistribReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
