package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniec.entity.report.gen.EnrollmentResults3NKReportGen;

/**
 * 3НК (результаты приема)
 */
public class EnrollmentResults3NKReport extends EnrollmentResults3NKReportGen
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}