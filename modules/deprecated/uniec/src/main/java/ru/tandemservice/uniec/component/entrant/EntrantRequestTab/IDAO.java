/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.EntrantRequestTab;

import java.util.List;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ExamGroup;

/**
 * @author agolubenko
 * @since 16.05.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void prepareEntrantRequestList(Model model);

    void prepareRequestedEnrollmentDirectionDataSource(Model model, EntrantRequest request);

    void updateDocumentsTakeAway(Long requestId);

    void updateDocumentsReturn(Long requestId);

    void updateOriginalsTakeAway(Model model, Long directionId);

    void updateOriginalsReturn(Model model, Long directionId);

    void executeIncludeIntoExamGroup(Long entrantRequestId);

    boolean isAllowIncludeIntoExamGroup(EntrantRequest entrantRequest);

    boolean isExamGroupInvalid(EntrantRequest entrantRequest);

    List<ExamGroup> getEntrantRequestGroup(EntrantRequest entrantRequest);

    /**
     * @param requestedDirectionId ID Выбранного направление приема
     * @param value Согласие на зачисление
     */
    void setAgree4Enrollment(Long requestedDirectionId, boolean value);
}