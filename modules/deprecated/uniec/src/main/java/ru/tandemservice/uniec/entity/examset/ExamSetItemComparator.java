/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.entity.examset;

import java.util.Comparator;

/**
 * @author vip_delete
 * @since 10.02.2009
 */
public class ExamSetItemComparator implements Comparator<IExamSetItem>
{
    @Override
    public int compare(IExamSetItem o1, IExamSetItem o2)
    {
        // вначале идет сортировка по коду типа вступ. испытания
        int type = o1.getKind().getCode().compareTo(o2.getKind().getCode());
        if (type != 0) return type;

        // внутри них сортируем по названию дисциплины
        int title = o1.getTitle().compareTo(o2.getTitle());
        if (title != 0) return title;

        // внутри них по названию дисциплин по выбору
        return o1.getChoiceTitle().compareTo(o2.getChoiceTitle());
    }
}
