/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntranceExaminationMeeting.EntranceExaminationMeetingReportAdd;

import java.util.Collections;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.Qualifications;

/**
 * @author agolubenko
 * @since 10.10.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
    }

    public void onChangeByAllEducationLevels(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setPickOutUnlicensedEducationLevels(false);
        model.setTerritorialOrgUnitActive(false);
        model.setTerritorialOrgUnitList(Collections.<OrgUnit> emptyList());
        model.getReport().setEducationLevelsHighSchool(null);
        model.setDevelopTechActive(false);
        model.setDevelopTechList(Collections.<DevelopTech> emptyList());
        model.setDevelopPeriodActive(false);
        model.setDevelopPeriodList(Collections.<DevelopPeriod> emptyList());
        model.setQualificationActive(false);
        model.setQualificationList(Collections.<Qualifications> emptyList());
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }
}
