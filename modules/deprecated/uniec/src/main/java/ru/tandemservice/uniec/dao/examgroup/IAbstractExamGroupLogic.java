/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.dao.examgroup;

import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.List;

/**
 * Логика работы экзаменационных групп
 *
 * @author Vasily Zhukov
 * @since 27.05.2010
 */
public interface IAbstractExamGroupLogic
{
    /**
     * Получает список возможных групп (информацию о них) куда может быть включено указанное заявление
     *
     * @param entrantRequest заявление абитуриента
     * @return информация об экзаменационных группа. key - ключ группы, title - ее название, shortTitle - сокращенное название
     *         допускается использование метки ${index} в title и shortTitle в качестве индекса группы
     */
    List<IExamGroupInfo> getExamGroupInfo(EntrantRequest entrantRequest);

    /**
     * получает имя бизнес компонента для отображения списка экзаменационных групп
     *
     * @return имя бизнесс компонента
     */
    String getExamGroupListBusinessComponent();

    /**
     * получает имя бизнес компонента для ввода оценок по экзаменационным группам
     *
     * @return имя бизнесс компонента
     */
    String getMarkDistributionBusinessComponent();
}
