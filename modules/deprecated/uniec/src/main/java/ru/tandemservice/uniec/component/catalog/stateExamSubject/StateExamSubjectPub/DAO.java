/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.catalog.stateExamSubject.StateExamSubjectPub;

import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;

import java.util.Collections;
import java.util.List;

/**
 * @author agolubenko
 * @since 27.02.2009
 */
public class DAO extends DefaultCatalogPubDAO<StateExamSubject, Model> implements IDAO
{
    @Override
    protected void prepareListItemDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StateExamSubject.ENTITY_CLASS, "ci");
        applyFilters(model, builder);

        EntityOrder entityOrder = model.getDataSource().getEntityOrder();
        if (entityOrder.getKey().equals(StateExamSubject.P_TITLE))
        {
            getOrderDescriptionRegistry().applyOrder(builder, entityOrder);
            UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
        }
        else
        {
            List<StateExamSubject> result = builder.getResultList(getSession());
            Collections.sort(result, CommonCatalogUtil.createCodeComparator((String) entityOrder.getKey()));
            if (entityOrder.getDirection() == OrderDirection.desc)
            {
                Collections.reverse(result);
            }
            UniBaseUtils.createPage(model.getDataSource(), result);
        }
    }
}
