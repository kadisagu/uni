package ru.tandemservice.uniec.entity.orders;

import org.tandemframework.core.view.formatter.DateFormatter;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.orders.gen.EnrollmentOrderGen;

/**
 * Приказ о зачислении абитуриентов
 */
public class EnrollmentOrder extends EnrollmentOrderGen
{
    @Override
    public String getTitle()
    {
        return "Приказ о зачислении " + (getNumber() == null ? "" : " №" + getNumber()) + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }

    public EnrollmentOrderBasic getBasic()
    {
        return UniecDAOFacade.getSettingsDAO().getBasic(this);
    }

    public boolean isRevert()
    {
        return getType().getCode().equals(EntrantEnrollmentOrderType.REVERT_ORDER_CODE);
    }

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, EnrollmentOrder.class)
                .filter(EnrollmentOrder.number())
                .order(EnrollmentOrder.number());
    }
}