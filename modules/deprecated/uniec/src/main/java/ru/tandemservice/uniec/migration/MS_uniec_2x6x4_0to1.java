package ru.tandemservice.uniec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniec_2x6x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrollmentCampaign

		// создано обязательное свойство prioritiesAdmissionTest
        if (!tool.columnExists("enrollmentcampaign_t", "prioritiesadmissiontest_p"))
		{
			// создать колонку
			tool.createColumn("enrollmentcampaign_t", new DBColumn("prioritiesadmissiontest_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultPrioritiesAdmissionTest = false;
			tool.executeUpdate("update enrollmentcampaign_t set prioritiesadmissiontest_p=? where prioritiesadmissiontest_p is null", defaultPrioritiesAdmissionTest);

			// сделать колонку NOT NULL
			tool.setColumnNullable("enrollmentcampaign_t", "prioritiesadmissiontest_p", false);

		}


        // сущность entranceDiscipline

        // создано свойство priority
        if (!tool.columnExists("entrancediscipline_t", "priority_p"))
        {
            // создать колонку
            tool.createColumn("entrancediscipline_t", new DBColumn("priority_p", DBType.INTEGER));

        }


    }
}