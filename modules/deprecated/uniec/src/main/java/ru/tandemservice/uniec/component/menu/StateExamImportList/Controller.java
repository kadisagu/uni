/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.menu.StateExamImportList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.entrant.StateExamImportedFile;

/**
 * @author Julia Oschepkova
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);

        DynamicListDataSource<StateExamImportedFile> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Дата импорта", StateExamImportedFile.P_IMPORT_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Тип импорта", StateExamImportedFile.P_IMPORT_TYPE));
        dataSource.addColumn(new IndicatorColumn("Импортированный файл", null, "onClickPrintImportedFile").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printUniecImportedStateExamFile").setOrderable(false));
        dataSource.addColumn(new IndicatorColumn("Результирующий файл", null, "onClickPrintResultFile").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("printUniecResultStateExamFile").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteFiles", "Удалить импортированный и результирующий файлы от «{0}»?", StateExamImportedFile.P_IMPORT_DATE).setPermissionKey("deleteUniecImportedResultFile"));
        dataSource.setOrder(StateExamImportedFile.P_IMPORT_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickImport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.uniec.component.menu.StateExamImport"));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.clearSettings();
        getDao().prepare(getModel(component));
        onClickSearch(component);
    }

    public void onClickPrintImportedFile(IBusinessComponent component)
    {
        byte[] content = getDao().getPrintImportedFileRequest(component.getListenerParameter());
        if (content == null)
            throw new ApplicationException("Файл данных по ЕГЭ пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Импортированный файл данных по ЕГЭ.csv");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("extension", "csv")));
    }

    public void onClickPrintResultFile(IBusinessComponent component)
    {
        byte[] content = getDao().getPrintResultFileRequest(component.getListenerParameter());
        if (content == null)
            throw new ApplicationException("Файл импорта данных по ЕГЭ пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Результирующий файл импорта данных по ЕГЭ.xls");
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("extension", "xls")));
    }

    public void onClickDeleteFiles(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}
