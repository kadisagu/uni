/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantLetterDistribution.EntrantLetterDistributionAdd;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;

/**
 * @author oleyba
 * @since 06.07.2009
 */
class EntrantLetterDistributionExcelBuilder
{
    /**
     * создает Excel-файл отчета
     *
     * @param highSchoolTitle название ОУ
     * @param paramsTitle     строка параметров
     * @param reportRowList   список строк отчета
     * @param usedLetters     используемые буквы
     * @return бинарное представление excel-файла отчета
     * @throws Exception ошибка
     */
    public static final byte[] buildExcelContent(String highSchoolTitle,
                                                 String paramsTitle,
                                                 List<ReportRowGroup> reportRowList, Set<Character> usedLetters) throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // шрифты
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10gray = new WritableFont(WritableFont.ARIAL, 10);
        arial10gray.setColour(Colour.GRAY_50);
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);

        // форматы ячеек
        WritableCellFormat tableHeaderFormat = new WritableCellFormat(arial10bold);
        tableHeaderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        tableHeaderFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat groupTitleFormat = new WritableCellFormat(arial10bold);
        groupTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        groupTitleFormat.setAlignment(Alignment.LEFT);

        WritableCellFormat rowTitleFormat = new WritableCellFormat(arial10);
        rowTitleFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowTitleFormat.setAlignment(Alignment.LEFT);

        WritableCellFormat grayFormat = new WritableCellFormat(arial10gray);
        grayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        grayFormat.setAlignment(Alignment.LEFT);

        WritableCellFormat numberBoldFormat = new WritableCellFormat(arial10, NumberFormats.INTEGER);
        numberBoldFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        numberBoldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat numberFormat = new WritableCellFormat(arial10, NumberFormats.INTEGER);
        numberFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        numberFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat numberGrayFormat = new WritableCellFormat(arial10gray, NumberFormats.INTEGER);
        numberGrayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        numberGrayFormat.setAlignment(Alignment.CENTRE);

        // create list
        WritableSheet sheet = workbook.createSheet("Отчет", 0);
        sheet.setColumnView(0, 70);// ~ 490 пикселей

        // row 1
        sheet.addCell(new Label(1, 0, highSchoolTitle, new WritableCellFormat(arial10bold)));

        // row 2, 3
        sheet.addCell(new Label(1, 1, "Информация приемной комиссии", new WritableCellFormat(arial10bold)));
        sheet.addCell(new Label(1, 2, "Распределение абитуриентов по буквам алфавита", new WritableCellFormat(arial10bold)));

        // row 5
        sheet.addCell(new Label(0, 4, paramsTitle, new WritableCellFormat(arial8)));

        // координаты начала таблицы
        final int TABLE_START_ROW_INDEX = 6;
        final int TABLE_START_COLUMN_INDEX = 0;

        // индексы
        int row;
        int column;

        // заголовки колонок с буквами
        List<Character> letters = new ArrayList<Character>(usedLetters);
        Collections.sort(letters);

        // рисуем заголовок
        {
            row = TABLE_START_ROW_INDEX;
            column = TABLE_START_COLUMN_INDEX;

            sheet.addCell(new Label(column, row, "Направление подготовки (специальность)", tableHeaderFormat));
            sheet.mergeCells(column, row, column, row);

            sheet.addCell(new Label(column + 1, row, "Всего", tableHeaderFormat));
            sheet.mergeCells(column + 1, row, column + 1, row);

            for (int i = 0; i < letters.size(); i++)
                sheet.addCell(new Label(column + 2 + i, row, letters.get(i).toString().toUpperCase(), tableHeaderFormat));
        }

        // заполняем таблицу
        {
            row = TABLE_START_ROW_INDEX + 1;
            column = TABLE_START_COLUMN_INDEX;

            ReportRowGroup total = null;
            for (ReportRowGroup group : reportRowList)
            {
                if (total == null) total = group.getGroup();
                // рисуем группу
                sheet.addCell(new Label(column, row, group.getTitle(), groupTitleFormat));
                sheet.addCell(new Label(column + 1, row, "", groupTitleFormat));
                // добавляем пустые ячейки до конца таблицы
                for (int i = 0; i < letters.size(); i++)
                    sheet.addCell(new Label(column + i + 2, row, "", groupTitleFormat));
                row++;

                // рисуем строки группы
                for (ReportRow reportRow : group.getRowList())
                {
                    sheet.addCell(new Label(column, row, reportRow.getPrintTitle(), rowTitleFormat));
                    sheet.addCell(new jxl.write.Number(column + 1, row, reportRow.getSum(), numberFormat));
                    // суммы
                    for (int i = 0; i < letters.size(); i++)
                        sheet.addCell(JExcelUtil.getNumberNullableCell(column + 2 + i, row, reportRow.get(letters.get(i)), numberFormat));
                    row++;
                }

                // рисуем строку итого для группы
                sheet.addCell(new Label(column, row, "Итого:", grayFormat));
                sheet.addCell(new jxl.write.Number(column + 1, row, group.getSum(), numberGrayFormat));
                // суммы
                for (int i = 0; i < letters.size(); i++)
                {
                    Integer value = group.get(letters.get(i));
                    if (value == null) value = 0;
                    sheet.addCell(new jxl.write.Number(column + 2 + i, row, value, numberGrayFormat));
                }
                row++;
            }
            if (total != null)
            {
                // рисуем строку итого для всей таблицы
                sheet.addCell(new Label(column, row, "Итого по ОУ:", groupTitleFormat));
                sheet.addCell(new jxl.write.Number(column + 1, row, total.getSum(), numberBoldFormat));
                // суммы
                for (int i = 0; i < letters.size(); i++)
                {
                    Integer value = total.get(letters.get(i));
                    if (value == null) value = 0;
                    sheet.addCell(new jxl.write.Number(column + 2 + i, row, value, numberBoldFormat));
                }
            }
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }
}
