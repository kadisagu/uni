/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcDistribution.util;

import java.util.Date;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionState;

/**
 * @author Vasily Zhukov
 * @since 07.07.2011
 */
public interface IEcgDistribution extends IEntity, ITitled
{
    EcgDistributionConfig getConfig();

    EcgDistribution getDistribution();

    EcgDistributionState getState();

    String getStateTitle();

    Date getApprovalDate();
}
