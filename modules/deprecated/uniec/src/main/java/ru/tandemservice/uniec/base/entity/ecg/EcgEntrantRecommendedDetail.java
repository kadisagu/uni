package ru.tandemservice.uniec.base.entity.ecg;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgEntrantRecommended;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgHasBringOriginal;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgEntrantRecommendedDetailGen;

/**
 * Рекомендованный абитуриент уточняющего распределения
 */
public class EcgEntrantRecommendedDetail extends EcgEntrantRecommendedDetailGen implements IEcgEntrantRecommended, IEcgHasBringOriginal
{
}