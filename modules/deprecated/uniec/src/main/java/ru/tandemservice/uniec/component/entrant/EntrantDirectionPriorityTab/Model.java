/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.EntrantDirectionPriorityTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit.PriorityProfileEduOuDTO;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 26.04.2012
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id")})
public class Model
{
    private Entrant _entrant = new Entrant();
    private List<CompensationType> _compensationTypeList;
    private IDataSettings _settings;
    
    private List<RequestedEnrollmentDirection> directionList;
    private Map<RequestedEnrollmentDirection, List<PriorityProfileEduOuDTO>> profileMap;

    private RequestedEnrollmentDirection direction;
    private PriorityProfileEduOuDTO profile;
    
    public String getDirectionTA()
    {
        return getDirection().isTargetAdmission() ? "да" : "нет";
    }
    
    public String getProfilePriorityEditId()
    {
        return "profileEdit_" + getDirection().getId().hashCode();
    }
    
    public List<PriorityProfileEduOuDTO> getProfileList()
    {
        return getProfileMap().get(getDirection());
    }

    public boolean isShowProfileList()
    {
        return !CollectionUtils.isEmpty(getProfileList());
    }
    
    // Getters & Setters

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public List<RequestedEnrollmentDirection> getDirectionList()
    {
        return directionList;
    }

    public void setDirectionList(List<RequestedEnrollmentDirection> directionList)
    {
        this.directionList = directionList;
    }

    public Map<RequestedEnrollmentDirection, List<PriorityProfileEduOuDTO>> getProfileMap()
    {
        return profileMap;
    }

    public void setProfileMap(Map<RequestedEnrollmentDirection, List<PriorityProfileEduOuDTO>> profileMap)
    {
        this.profileMap = profileMap;
    }

    public RequestedEnrollmentDirection getDirection()
    {
        return direction;
    }

    public void setDirection(RequestedEnrollmentDirection direction)
    {
        this.direction = direction;
    }

    public PriorityProfileEduOuDTO getProfile()
    {
        return profile;
    }

    public void setProfile(PriorityProfileEduOuDTO profile)
    {
        this.profile = profile;
    }
}
