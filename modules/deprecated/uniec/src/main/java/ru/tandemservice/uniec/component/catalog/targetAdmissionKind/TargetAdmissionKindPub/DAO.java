/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.catalog.targetAdmissionKind.TargetAdmissionKindPub;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.IBusinessComponent;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;

/**
 * @author agolubenko
 * @since 28.06.2008
 */
public class DAO extends DefaultCatalogPubDAO<TargetAdmissionKind, Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void deleteRow(IBusinessComponent context)
    {
        TargetAdmissionKind targetAdmissionKind = get(TargetAdmissionKind.class, (Long) context.getListenerParameter());
        TargetAdmissionKind parent = (TargetAdmissionKind) targetAdmissionKind.getParent();
        
        Session session = getSession();
        Criteria criteria = session.createCriteria(TargetAdmissionKind.class);
        if (parent == null)
        {
            criteria.add(Restrictions.isNull(TargetAdmissionKind.L_PARENT));
        }
        else
        {
            criteria.add(Restrictions.eq(TargetAdmissionKind.L_PARENT, parent));
        }
        criteria.add(Restrictions.gt(TargetAdmissionKind.P_PRIORITY, targetAdmissionKind.getPriority()));
        for (TargetAdmissionKind lower : (List<TargetAdmissionKind>) criteria.list())
        {
            lower.setPriority(lower.getPriority() - 1);
            session.update(lower);
        }
        
        super.deleteRow(context);
    }
}
