/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.report.EntrantsRating.EntrantsRatingAdd;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.report.EntrantsRatingReport;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author agolubenko
 * @since 16.06.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setCompetitionGroupList(getCompetitionGroups(model.getEnrollmentCampaign()));
        model.setTechnicCommissionModel(new StaticSelectModel(IdentifiableWrapper.P_ID, new String[]{"title"}, getTechnicCommissions(model.getEnrollmentCampaign())));
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(new Date());
    }

    @Override
    public List<CompetitionGroup> getCompetitionGroups(EnrollmentCampaign enrollmentCampaign)
    {
        List<CompetitionGroup> competitionGroupList = getList(CompetitionGroup.class, CompetitionGroup.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign);
        Collections.sort(competitionGroupList, CompetitionGroup.TITLED_COMPARATOR);
        return competitionGroupList;
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        EntrantsRatingReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setCompetitionGroupTitle(model.getCompetitionGroup().getTitle());

        if (model.isTechnicCommissionActive())
            report.setTechnicCommission(UniStringUtils.join(model.getTechnicCommissionList(), "title", ", "));

        DatabaseFile databaseFile = getReportContent(model, session);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IdentifiableWrapper<?>> getTechnicCommissions(EnrollmentCampaign enrollmentCampaign)
    {
        Criteria criteria = getSession().createCriteria(EntrantRequest.class, "entrantRequest");
        criteria.createAlias(EntrantRequest.L_ENTRANT, "entrant");
        criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
        criteria.add(Restrictions.isNotNull("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION));
        criteria.setProjection(Projections.distinct(Projections.alias(Projections.property("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION), "technicCommission")));
        criteria.addOrder(Order.asc("technicCommission"));

        long id = 0;
        List<IdentifiableWrapper<?>> result = new ArrayList<IdentifiableWrapper<?>>();

        for (String technicCommission : (List<String>) criteria.list())
        {
            result.add(new IdentifiableWrapper(id++, technicCommission));
        }
        return result;
    }

    protected DatabaseFile getReportContent(Model model, Session session) {
        return new EntrantsRatingReportBuilder(model, session).getContent();
    }
}
