/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EntrantCustomState;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 4/3/13
 */
public class EntrantCustomStateDAO extends UniBaseDao implements IEntrantCustomStateDAO
{
    @Override
    public void saveOrUpdate(EntrantCustomState entrantCustomState)
    {
        Date beginDate = entrantCustomState.getBeginDate();
        Date endDate = entrantCustomState.getEndDate();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantCustomState.class, "st");
        builder.where(eq(property("st", EntrantCustomState.entrant().id()), value(entrantCustomState.getEntrant().getId())));
        builder.where(eq(property("st", EntrantCustomState.customState().id()), value(entrantCustomState.getCustomState().getId())));
        if (null != entrantCustomState.getId())
            builder.where(ne(property("st", EntrantCustomState.id()), value(entrantCustomState.getId())));

        FilterUtils.applyIntersectPeriodFilterNullSafe(builder, "st", EntrantCustomState.P_BEGIN_DATE, EntrantCustomState.P_END_DATE, beginDate, endDate);

        if (existsEntity(builder.buildQuery()))
            throw new ApplicationException("Данный дополнительный статус «" + entrantCustomState.getCustomState().getTitle() + "» пересекается по срокам действия с уже имеющимся аналогичным статусом абитуриента.");

        getSession().saveOrUpdate(entrantCustomState);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EntrantCustomState> getActiveStatesList(Long entrantId, Date date)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantCustomState.class, "st");
        builder.where(eq(property("st", EntrantCustomState.entrant().id()), value(entrantId)));
        FilterUtils.applyInPeriodFilterNullSafe(builder, "st", EntrantCustomState.P_BEGIN_DATE, EntrantCustomState.P_END_DATE, date);
        builder.order(property("st", EntrantCustomState.customState().title()));

        return  createStatement(builder).list();
    }
}
