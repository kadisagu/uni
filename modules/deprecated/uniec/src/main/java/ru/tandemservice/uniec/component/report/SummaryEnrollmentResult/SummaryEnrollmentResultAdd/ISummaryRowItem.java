/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.SummaryEnrollmentResult.SummaryEnrollmentResultAdd;

import java.util.Set;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * Наименьший элемент строки отчета сводки
 * Создается ISummaryReportRowFactory
 * на базе выбранного направления приема (для сводки по приему документов и прогресса сдачи экзаменов) или
 * на базе студента предзачисления (для сводки по результатам зачисления)
 *
 * @author vip_delete
 * @since 23.04.2009
 */
interface ISummaryRowItem
{
    EnrollmentDirection getEnrollmentDirection();

    RequestedEnrollmentDirection getDirection();
    
    EntrantRequest getEntrantRequest();

    Entrant getEntrant();

    PersonEduInstitution getLastPersonEduInstitution();

    Set<ChosenEntranceDiscipline> getChosenEntranceDisciplineSet();

    Set<String> getBenefitSet();
    
    boolean isHasOlympiadDiplomas();
    
    boolean isHasStudents();
    
    boolean isHasRecommendations();

    boolean isHasProfileKnowledges();

    boolean isTargetAdmission();

    String getTargetAdmissionKind();

    String getCompetitionKind();

    String getStudentCategory();

    boolean isHasWorkExperience();
}
