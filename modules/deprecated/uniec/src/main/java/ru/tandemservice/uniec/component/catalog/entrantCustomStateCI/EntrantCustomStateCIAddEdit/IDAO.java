/* $Id$ */
package ru.tandemservice.uniec.component.catalog.entrantCustomStateCI.EntrantCustomStateCIAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<EntrantCustomStateCI, Model>
{
}
