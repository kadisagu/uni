/* $Id$ */
package ru.tandemservice.uniec.component.report.SummaryQuotasMarksResults.Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 18.08.2011
 */
class SummaryQuotasMarksResultsReportBuilder
{
    private Session _session;
    private Model _model;

    SummaryQuotasMarksResultsReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        MQBuilder builder = getRequestedEnrollmentDirectionBuilder();

        Map<Discipline2RealizationWayRelation, ConversionScale> discipline2scaleMap = UniecDAOFacade.getEntrantDAO().getConversionScales(_model.getEnrollmentCampaign());

        EntrantDataUtil dataUtil = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        Set<StateExamSubject> stateExamSubjectSet = new HashSet<>();
        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
        Set<EnrollmentDirection> directionSet = new HashSet<>();

        getStateExamSubjectAndDisciplineSet(dataUtil, discipline2scaleMap, stateExamSubjectSet, disciplineSet, directionSet);

        List<StateExamSubject> stateExamSubjectList = new ArrayList<>(stateExamSubjectSet);
        List<Discipline2RealizationWayRelation> disciplineList = new ArrayList<>(disciplineSet);
        Collections.sort(stateExamSubjectList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        Collections.sort(disciplineList, ITitled.TITLED_COMPARATOR);

        List<ISumLev> list = getDataRowList(dataUtil, discipline2scaleMap, directionSet);

        // получаем бинарное представление excel-файла
        try
        {
            return SummaryQuotasMarksResultsReportExcelBuilder.buildExcelContent(
                    getParamsTitle(),
                    stateExamSubjectList,
                    disciplineList,
                    Arrays.asList(DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getEnroll3007to()),
                            DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getEnroll0508to()),
                            DateFormatter.DEFAULT_DATE_FORMATTER.format(_model.getEnroll3108to())
                    ),
                    list);
        } catch (Exception e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private String getParamsTitle()
    {
        List<String> paramsTitleList = new ArrayList<>();
        paramsTitleList.add("Стадия приемной кампании: " + _model.getEnrollmentCampaignStage().getTitle());

        if (_model.isStudentCategoryActive())
            paramsTitleList.add("Категория поступающего: " + UniStringUtils.join(_model.getStudentCategoryList(), StudentCategory.P_TITLE, ", "));

        if (_model.isQualificationActive())
            paramsTitleList.add("Квалификация: " + UniStringUtils.join(_model.getQualificationList(), Qualifications.P_TITLE, ", "));

        if (_model.isCompensationTypeActive())
            paramsTitleList.add("Вид возмещения затрат: " + _model.getCompensationType().getShortTitle());

        if (_model.isFormativeOrgUnitActive())
            paramsTitleList.add("Формирующее подр.: " + UniStringUtils.join(_model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, ", "));

        if (_model.isTerritorialOrgUnitActive())
            paramsTitleList.add("Территориальное подр.: " + UniStringUtils.join(_model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, ", "));

        if (_model.isEducationLevelHighSchoolActive())
            paramsTitleList.add("Направление подготовки (специальность): " + UniStringUtils.join(_model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_PRINT_TITLE, ", "));

        if (_model.isDevelopFormActive())
            paramsTitleList.add("Форма освоения: " + UniStringUtils.join(_model.getDevelopFormList(), DevelopForm.P_TITLE, ", "));

        if (_model.isDevelopConditionActive())
            paramsTitleList.add("Условие освоения: " + UniStringUtils.join(_model.getDevelopConditionList(), DevelopCondition.P_TITLE, ", "));

        if (_model.isDevelopTechActive())
            paramsTitleList.add("Технология освоения: " + UniStringUtils.join(_model.getDevelopTechList(), DevelopTech.P_TITLE, ", "));

        if (_model.isDevelopPeriodActive())
            paramsTitleList.add("Срок освоения: " + UniStringUtils.join(_model.getDevelopPeriodList(), DevelopPeriod.P_TITLE, ", "));

        return StringUtils.join(paramsTitleList.iterator(), "; ");
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder()
    {
        int stateId = _model.getEnrollmentCampaignStage().getId().intValue();

        MQBuilder builder;
        if (Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS == stateId || Model.ENROLLMENT_CAMP_STAGE_EXAMS == stateId)
        {
            builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
            builder.addJoin("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit(), "ou");
            builder.addJoin("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
            builder.addJoin("request", EntrantRequest.L_ENTRANT, "entrant");
            builder.addJoin("entrant", Entrant.L_PERSON, "person");
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            if (_model.isCompensationTypeActive())
                builder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getCompensationType()));
            if (Model.ENROLLMENT_CAMP_STAGE_EXAMS == stateId)
                builder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.state().code(), UniecDefines.ENTRANT_STATE_ACTIVE_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        } else if (Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT == stateId)
        {
            builder = new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p", new String[]{PreliminaryEnrollmentStudent.requestedEnrollmentDirection().s()});
            builder.addJoin("p", PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
            builder.addJoin("p", PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "r");
            builder.addJoin("r", RequestedEnrollmentDirection.entrantRequest().s(), "request");
            builder.addJoin("request", EntrantRequest.entrant().s(), "entrant");
            builder.addJoin("entrant", Entrant.person().s(), "person");
            if (_model.isStudentCategoryActive())
                builder.add(MQExpression.in("p", PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
            if (_model.isCompensationTypeActive())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE, _model.getCompensationType()));
            if (_model.isParallelActive() && _model.getParallel().isTrue())
                builder.add(MQExpression.eq("p", PreliminaryEnrollmentStudent.parallel().s(), Boolean.FALSE));
        } else
            throw new RuntimeException("Unknown EnrollmentCampaignStage: " + _model.getEnrollmentCampaignStage().getTitle());

        builder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.state().code(), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("entrant", Entrant.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("entrant", Entrant.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        if (_model.isQualificationActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, _model.getTerritorialOrgUnitList()));
        if (_model.isEducationLevelHighSchoolActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, _model.getEducationLevelHighSchoolList()));
        if (_model.isDevelopFormActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_FORM, _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_CONDITION, _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_TECH, _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.add(MQExpression.in("ou", EducationOrgUnit.L_DEVELOP_PERIOD, _model.getDevelopPeriodList()));

        return builder;
    }

    private void getStateExamSubjectAndDisciplineSet(EntrantDataUtil dataUtil, Map<Discipline2RealizationWayRelation, ConversionScale> discipline2scaleMap, Set<StateExamSubject> stateExamSubjectSet, Set<Discipline2RealizationWayRelation> disciplineSet, Set<EnrollmentDirection> directionSet)
    {
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            directionSet.add(direction.getEnrollmentDirection());

            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
            {
                if (chosen.getFinalMark() != null && chosen.getFinalMarkSource() != null)
                {
                    int source = chosen.getFinalMarkSource();

                    if (source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 || source == UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2)
                    {
                        StateExamSubjectMark stateExamSubjectMark = dataUtil.getMarkStatistic(chosen).getStateExamMark();
                        if (stateExamSubjectMark != null)
                            stateExamSubjectSet.add(stateExamSubjectMark.getSubject());
                    } else if (source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL || source == UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL)
                    {
                        ConversionScale scale = discipline2scaleMap.get(chosen.getEnrollmentCampaignDiscipline());
                        if (scale != null && scale.getSubject() != null)
                            stateExamSubjectSet.add(scale.getSubject());
                    } else
                    {
                        disciplineSet.add(chosen.getEnrollmentCampaignDiscipline());
                    }
                }
            }
        }
    }

    private List<ISumLev> getDataRowList(EntrantDataUtil dataUtil, Map<Discipline2RealizationWayRelation, ConversionScale> discipline2scaleMap, Set<EnrollmentDirection> directionSet)
    {
        // направление приема -> [укрупненная группа, группировочная группа]
        Map<EnrollmentDirection, EnrollmentDirectionDescription> directionDataMap = new HashMap<>();

        Map<EducationLevels, SumLev1> mapLev1 = new HashMap<>();
        Map<EducationLevels, SumLev2> mapLev2 = new HashMap<>();
        Map<EnrollmentDirection, SumLev3> mapLev3 = new HashMap<>();

        // подготавливаем данные
        for (EnrollmentDirection enrollmentDirection : directionSet)
        {
            // направление для группировки
            EducationLevels educationLevel = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
            while (!educationLevel.getLevelType().isGroupingLevel())
                educationLevel = educationLevel.getParentLevel();

            // укрупненная группа
            EducationLevels educationLevelGroup = educationLevel;
            while (educationLevelGroup.getParentLevel() != null)
                educationLevelGroup = educationLevelGroup.getParentLevel();

            EnrollmentDirectionDescription data = new EnrollmentDirectionDescription();
            data.educationLevel = educationLevel;
            data.educationLevelGroup = educationLevelGroup;
            directionDataMap.put(enrollmentDirection, data);

            if (!mapLev1.containsKey(educationLevelGroup))
                mapLev1.put(educationLevelGroup, new SumLev1(dataUtil, educationLevelGroup));

            if (!mapLev2.containsKey(educationLevel))
                mapLev2.put(educationLevel, new SumLev2(dataUtil, educationLevel));

            if (!mapLev3.containsKey(enrollmentDirection))
                mapLev3.put(enrollmentDirection, new SumLev3(dataUtil, _model.getCompensationType(), educationLevel, enrollmentDirection));
        }

        // наполняем данные
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
        {
            EnrollmentDirection enrollmentDirection = direction.getEnrollmentDirection();
            EnrollmentDirectionDescription data = directionDataMap.get(enrollmentDirection);

            if (data == null) continue;

            mapLev1.get(data.educationLevelGroup).add(direction, discipline2scaleMap);
            mapLev2.get(data.educationLevel).add(direction, discipline2scaleMap);
            mapLev3.get(enrollmentDirection).add(direction, discipline2scaleMap);
        }

        // группируем данные
        Map<SumLev1, Map<SumLev2, Set<SumLev3>>> resultMap = new TreeMap<>();

        for (Map.Entry<EnrollmentDirection, SumLev3> entry : mapLev3.entrySet())
        {
            // направление приема
            EnrollmentDirection enrollmentDirection = entry.getKey();
            EnrollmentDirectionDescription data = directionDataMap.get(enrollmentDirection);

            SumLev1 sumLev1 = mapLev1.get(data.educationLevelGroup);
            SumLev2 sumLev2 = mapLev2.get(data.educationLevel);
            SumLev3 sumLev3 = entry.getValue();

            Map<SumLev2, Set<SumLev3>> map = resultMap.get(sumLev1);
            if (map == null)
                if (null != resultMap.put(sumLev1, map = new TreeMap<>()))
                    throw new RuntimeException("Fatal override");

            Set<SumLev3> list = map.get(sumLev2);
            if (list == null)
                if (null != map.put(sumLev2, list = new TreeSet<>()))
                    throw new RuntimeException("Fatal override");

            if (!list.add(sumLev3))
                throw new RuntimeException("Fatal override");
        }

        // направление приема -> число зачисленных по волнам
        Map<Long, int[]> enrollMap = getEnrollCount();
        final int len = 3;

        // создаем список строк
        List<ISumLev> list = new ArrayList<>();

        for (Map.Entry<SumLev1, Map<SumLev2, Set<SumLev3>>> entry : resultMap.entrySet())
        {
            // укрупненная группа
            SumLev1 sumLev1 = entry.getKey();
            list.add(sumLev1);

            int[] enrollCountLev1 = new int[len];
            int budgetPlanSum = 0;
            int targetPlanSum = 0;
            int contractPlanSum = 0;

            for (Map.Entry<SumLev2, Set<SumLev3>> subEntry : entry.getValue().entrySet())
            {
                SumLev2 sumLev2 = subEntry.getKey();
                list.add(sumLev2);

                int[] enrollCountLev2 = new int[len];
                int budgetPlan = 0;
                int targetPlan = 0;
                int contractPlan = 0;

                for (SumLev3 sumLev3 : subEntry.getValue())
                {
                    int[] enrollCountLev3 = enrollMap.get(sumLev3.getId());
                    if (enrollCountLev3 == null)
                        enrollCountLev3 = new int[len];

                    sumLev3.setEnrollCount(enrollCountLev3);
                    for (int i = 0; i < len; i++)
                        enrollCountLev2[i] += enrollCountLev3[i];

                    budgetPlan += sumLev3.getBudgetPlan();
                    targetPlan += sumLev3.getTargetPlan();
                    contractPlan += sumLev3.getContractPlan();
                    list.add(sumLev3);
                }

                sumLev2.setEnrollCount(enrollCountLev2);
                for (int i = 0; i < len; i++)
                    enrollCountLev1[i] += enrollCountLev2[i];

                sumLev2.setBudgetPlan(budgetPlan);
                sumLev2.setTargetPlan(targetPlan);
                sumLev2.setContractPlan(contractPlan);

                budgetPlanSum += budgetPlan;
                targetPlanSum += targetPlan;
                contractPlanSum += contractPlan;
            }

            sumLev1.setEnrollCount(enrollCountLev1);

            sumLev1.setBudgetPlan(budgetPlanSum);
            sumLev1.setTargetPlan(targetPlanSum);
            sumLev1.setContractPlan(contractPlanSum);
        }

        return list;
    }

    @SuppressWarnings("deprecation")
    private Map<Long, int[]> getEnrollCount()
    {
        MQBuilder builder = new MQBuilder(EnrollmentExtract.ENTITY_CLASS, "e", new String[]{EnrollmentExtract.entity().requestedEnrollmentDirection().enrollmentDirection().id().s(), EnrollmentExtract.paragraph().order().commitDate().s()});

        final int len = 3;

        Date[][] dates = new Date[len][];
        dates[0] = new Date[]{CoreDateUtils.getDayFirstTimeMoment(_model.getEnroll3007from()), CoreDateUtils.getDayLastTimeMoment(_model.getEnroll3007to())};
        dates[1] = new Date[]{CoreDateUtils.getDayFirstTimeMoment(_model.getEnroll0508from()), CoreDateUtils.getDayLastTimeMoment(_model.getEnroll0508to())};
        dates[2] = new Date[]{CoreDateUtils.getDayFirstTimeMoment(_model.getEnroll3108from()), CoreDateUtils.getDayLastTimeMoment(_model.getEnroll3108to())};

        builder.add(MQExpression.or(
                UniMQExpression.betweenDate("e", EnrollmentExtract.paragraph().order().commitDate().s(), _model.getEnroll3007from(), _model.getEnroll3007to()),
                UniMQExpression.betweenDate("e", EnrollmentExtract.paragraph().order().commitDate().s(), _model.getEnroll0508from(), _model.getEnroll0508to()),
                UniMQExpression.betweenDate("e", EnrollmentExtract.paragraph().order().commitDate().s(), _model.getEnroll3108from(), _model.getEnroll3108to())
        ));

        builder.add(MQExpression.in("e", EnrollmentExtract.entity().requestedEnrollmentDirection(), getRequestedEnrollmentDirectionBuilder()));

        Map<Long, int[]> resultMap = new HashMap<>();

        for (Object[] row : builder.<Object[]>getResultList(_session))
        {
            Long id = (Long) row[0];
            Date date = (Date) row[1];

            long time = date.getTime();

            for (int k = 0; k < len; k++)
            {
                if (dates[k][0].getTime() <= time && time <= dates[k][1].getTime())
                {
                    int[] data = resultMap.get(id);
                    if (data == null)
                        resultMap.put(id, data = new int[len]);
                    data[k]++;
                    break;
                }
            }
        }

        return resultMap;
    }

    private static class EnrollmentDirectionDescription
    {
        private EducationLevels educationLevel;
        private EducationLevels educationLevelGroup;
    }
}
