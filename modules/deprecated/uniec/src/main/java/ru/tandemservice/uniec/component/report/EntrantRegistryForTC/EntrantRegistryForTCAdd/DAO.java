// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.report.EntrantRegistryForTC.EntrantRegistryForTCAdd;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.report.EntrantRegistryForTCReport;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 18.06.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        getSession();

        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(new Date());

        refreshTechnicCommissionModel(model);
        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeModel(new LazySimpleSelectModel<>(CompensationType.class).setSortProperty(CompensationType.P_CODE));
    }

    @Override
    public void update(Model model, String cgTitle)
    {
        final Session session = getSession();

        EntrantRegistryForTCReport report = model.getReport();
        report.setFormingDate(new Date());

        report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        report.setQualification(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.getCompensationType() != null)
            report.setCompensationType(model.getCompensationType().getTitle());
        report.setTechnicCommission(model.getTechnicCommission().getTitle());

        DatabaseFile databaseFile = new EntrantRegistryForTCReportBuilder(model, cgTitle, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    @Override
    public void refreshTechnicCommissionModel(Model model)
    {
        Criteria criteria = getSession().createCriteria(EntrantRequest.class, "entrantRequest");
        criteria.createAlias(EntrantRequest.L_ENTRANT, "entrant");
        criteria.add(Restrictions.eq("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        criteria.add(Restrictions.isNotNull("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION));
        criteria.setProjection(Projections.distinct(Projections.alias(Projections.property("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION), "technicCommission")));
        criteria.addOrder(Order.asc("technicCommission"));

        long id = 0;
        List<IdentifiableWrapper<?>> result = new ArrayList<>();

        for (Object technicCommission : criteria.list())
        {
            result.add(new IdentifiableWrapper(id++, (String) technicCommission));
        }
        
        model.setTechnicCommissionModel(new LazySimpleSelectModel<>(result));
    }
}
