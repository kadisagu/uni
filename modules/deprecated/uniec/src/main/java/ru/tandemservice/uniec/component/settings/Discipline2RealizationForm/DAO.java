/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.settings.Discipline2RealizationForm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author agolubenko
 * @since 09.06.2008
 */
public class DAO extends ru.tandemservice.uniec.component.settings.AbstractMatrixSettingsComponent.DAO<Model, Discipline2RealizationWayRelation, SubjectPassFormWrapper, Boolean, Discipline2RealizationFormRelation> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setBudget(getByCode(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET));
        model.setContract(getByCode(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT));

        super.prepare(model);

        model.setCompensationType(model.getBudget());
        model.getCompensationType2MatrixData().put(model.getBudget(), getMatrixData(model));

        model.setCompensationType(model.getContract());
        model.getCompensationType2MatrixData().put(model.getContract(), getMatrixData(model));
    }

    @Override
    public void update(Model model)
    {
        // budget
        model.setCompensationType(model.getBudget());
        synchronize(fillMatrixData(model), model.getCompensationType2MatrixData().get(model.getCompensationType()), model.getCompensationType());

        // contract
        model.setCompensationType(model.getContract());
        synchronize(fillMatrixData(model), model.getCompensationType2MatrixData().get(model.getCompensationType()), model.getCompensationType());
    }

    private void synchronize(Map<CellCoordinates, Discipline2RealizationFormRelation> ids, Map<CellCoordinates, Boolean> matrixData, CompensationType compensationType)
    {
        for (CellCoordinates cellCoordinates : matrixData.keySet())
        {
            if (matrixData.get(cellCoordinates))
            {
                if (!ids.containsKey(cellCoordinates))
                {
                    Discipline2RealizationFormRelation relation = new Discipline2RealizationFormRelation();
                    relation.setDiscipline(get(Discipline2RealizationWayRelation.class, cellCoordinates.getRowId()));
                    relation.setSubjectPassForm(get(SubjectPassForm.class, cellCoordinates.getColumnId()));
                    relation.setType(compensationType);
                    getSession().save(relation);
                }
            } else
            {
                if (ids.containsKey(cellCoordinates))
                {
                    getSession().delete(ids.get(cellCoordinates));
                }
            }
        }
    }

    @Override
    protected List<SubjectPassFormWrapper> getHorizontalObjects(Model model)
    {
        List<SubjectPassForm> queryResult = getList(SubjectPassForm.class);
        if (queryResult.isEmpty())
            return Collections.emptyList();

        List<SubjectPassFormWrapper> result = new ArrayList<SubjectPassFormWrapper>();
        for (Object obj : queryResult)
            result.add(new SubjectPassFormWrapper((SubjectPassForm) obj));
        return result;
    }

    @Override
    protected Long getLeftId(Discipline2RealizationFormRelation object)
    {
        return object.getDiscipline().getId();
    }

    @Override
    protected List<Discipline2RealizationFormRelation> getObjects(Model model)
    {
        if (model.getCompensationType() == null)
            return Collections.emptyList();
        return getList(Discipline2RealizationFormRelation.class, Discipline2RealizationFormRelation.L_TYPE, model.getCompensationType());
    }

    @Override
    protected Long getRightId(Discipline2RealizationFormRelation object)
    {
        return object.getSubjectPassForm().getId();
    }

    @Override
    protected Boolean getValue(Discipline2RealizationFormRelation object)
    {
        return true;
    }

    @Override
    protected List<Discipline2RealizationWayRelation> getVerticalObjects(Model model)
    {
        List<Discipline2RealizationWayRelation> list = getList(Discipline2RealizationWayRelation.class, Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign());
        Collections.sort(list, ITitled.TITLED_COMPARATOR);
        return list;
    }
}
