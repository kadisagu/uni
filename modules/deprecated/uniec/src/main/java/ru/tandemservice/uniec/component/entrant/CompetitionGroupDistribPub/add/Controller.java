package ru.tandemservice.uniec.component.entrant.CompetitionGroupDistribPub.add;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    @SuppressWarnings("unchecked")
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        final boolean budget = UniDefines.COMPENSATION_TYPE_BUDGET.equals(model.getDistrib().getCompensationType().getCode());
        EnrollmentCampaign campaign = model.getDistrib().getCompetitionGroup().getEnrollmentCampaign();

        final AbstractListDataSource<ModelBase.Row> ds = model.getEntrantRateRowsDataSource();
        ds.getColumns().clear();
        if (!model.isFiltered()) {
            ds.addColumn(new BlockColumn("checker", "").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("ФИО", "title").setOrderable(false).setClickable(false));
            if (budget) {
                ds.addColumn(new SimpleColumn("Вид конкурса", "competitionKind").setOrderable(false).setClickable(false));
            } else {
                ds.addColumn(new SimpleColumn("Льготы", "benefitsExistence").setOrderable(false).setClickable(false));
            }
            addAdditionalColumns(ds, null, campaign);
            ds.addColumn(new SimpleColumn("Сумма баллов", "totalMark").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("*", "graduatedProfileEduInstitution", ModelBase.BooleanFormatter.INSTANCE).setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("Ср. балл аттестата", "certificateAverageMark").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("Приоритеты", "directionsPriorities").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("Оригиналы документов", "originalDocumentHandedIn", ModelBase.CopiesFormatter.INSTANCE).setOrderable(false).setClickable(false));

        } else {
            final IMergeRowIdResolver competitionKindRowIdResolver = entity -> {
                final ModelBase.Row row = (ModelBase.Row)entity;
                return (row.isTargetAdmission() ? "0" : String.valueOf(row.getRow().getCompetitionKind().getId()));
            };
            final IMergeRowIdResolver totalMarkRowIdResolver = entity -> competitionKindRowIdResolver.getMergeRowId(entity)+"-"+((ModelBase.Row)entity).getTotalMark();

            if (budget) {
                ds.addColumn(new SimpleColumn("Вид конкурса", "competitionKind").setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(competitionKindRowIdResolver));
            } else {
                ds.addColumn(new SimpleColumn("Льготы", "benefitsExistence").setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(competitionKindRowIdResolver));
            }
            addAdditionalColumns(ds, competitionKindRowIdResolver, campaign);
            ds.addColumn(new SimpleColumn("Сумма баллов", "totalMark").setOrderable(false).setClickable(false).setMergeRows(true).setMergeRowIdResolver(totalMarkRowIdResolver));
            ds.addColumn(new SimpleColumn("*", "graduatedProfileEduInstitution", ModelBase.BooleanFormatter.INSTANCE).setOrderable(false).setClickable(false)/*.setMergeRows(true).setMergeRowIdResolver(profileEduInstitutionRowIdResolver)*/);
            ds.addColumn(new SimpleColumn("Ср. балл аттестата", "certificateAverageMark").setOrderable(false).setClickable(false)/*.setMergeRows(true).setMergeRowIdResolver(certificateAverageMarkRowIdResolver)*/);
            ds.addColumn(new SimpleColumn("ФИО", "title").setOrderable(false).setClickable(false));
            ds.addColumn(new BlockColumn("selector", "Направление подготовки (специальность)").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("Приоритеты", "directionsPriorities").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("Оригиналы документов", "originalDocumentHandedIn", ModelBase.CopiesFormatter.INSTANCE).setOrderable(false).setClickable(false));
        }

        if (model.isFiltered() && (!model.getEntrantRateRows().isEmpty())) {
            this.refreshSelectors(model, model.getEntrantRateRows().iterator().next().getId());
        }
    }

    protected void addAdditionalColumns(AbstractListDataSource<ModelBase.Row> ds, IMergeRowIdResolver resolver, EnrollmentCampaign campaign)
    {
    }

    protected void refreshSelectors(final Model model, final Long id) {
        if (model.isFiltered()) {
            final Map<Long, Integer> dir2QuotaMap = model.getDir2quotaMap();
            final Map<Long, MutableInt> dir2Quota = new HashMap<>(dir2QuotaMap.size());
            for (final Map.Entry<Long, Integer> e: dir2QuotaMap.entrySet()) {
                dir2Quota.put(e.getKey(), new MutableInt(e.getValue()));
            }

            boolean found = false;
            for (final ModelBase.Row row: model.getEntrantRateRows()) {
                found |= row.getId().equals(id);
                if (row.isPreEnrolled()) {

                    if (null != row.getDirection()) {
                        final MutableInt i = dir2Quota.get(row.getDirection().getEnrollmentDirection().getId());
                        if ((null != i) && (i.intValue() > 0)) { i.decrement(); }
                        /* else { ну вот так вот у них руки из жопы растут значит (зачислили больше, чем можно)  }*/
                    } else {
                        throw new IllegalStateException();
                    }

                } else {
                    if (row.isChecked()) {
                        row.filter(dir2Quota);


                        if ((!row.getDirections().contains(row.getDirection())) || (found && !row.getId().equals(id))) {
                            row.setDirection(null);
                        }

                        if ((null == row.getDirection()) && (row.getDirections().size() > 0)) {
                            row.setDirection(row.getDirections().iterator().next());
                        }

                        if (null != row.getDirection()) {
                            final MutableInt i = dir2Quota.get(row.getDirection().getEnrollmentDirection().getId());
                            if ((null != i) && (i.intValue() > 0)) { i.decrement(); }
                            else { throw new IllegalStateException(); /* значит бага в filter */ }
                        }


                    } else {
                        row.filter(Collections.<Long, MutableInt>emptyMap());
                        row.setDirection(null);

                    }

                    row.setChecked(row.getDirections().size() > 0);
                }
            }
        }
    }


    public void onClickFilter(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        CollectionUtils.filter(model.getEntrantRateRows(), object -> ((ModelBase.Row)object).isChecked());
        model.setFiltered(true);
        this.onRefreshComponent(component);
    }


    public void onChangeDirection(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        if (model.isFiltered()) {
            this.refreshSelectors(model, (Long) component.getListenerParameter());
        }
    }


    public void onClickApply(final IBusinessComponent component) {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            this.getDao().save(this.getModel(component));
        } finally {
            eventLock.release();
        }
        this.deactivate(component);
    }


}
