/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.component.menu.MarkDistribution;

import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.component.entrant.EntrantMarkTab.EntrantMarkValidator;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.Collections;
import java.util.List;

/**
 * @author vip_delete
 * @since 17.11.2009
 */
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignModel, IEnrollmentCampaignSelectModel
{
    public static final String COLUMN_EXAM_PASS_DISCIPLINE_MARK = "examPassDisciplineMark";
    public static final String COLUMN_EXAM_PASS_DISCIPLINE_EXAMINERS = "examPassDisciplineExaminers";
    public static final String COLUMN_EXAM_PASS_DISCIPLINE_PAPER_CODE = "examPassDisciplinePaperCode";

    private IDataSettings _settings;
    private DynamicListDataSource<EntrantDisciplineMarkWrapper> _dataSource;
    private MultiEnrollmentDirectionUtil.Parameters _parameters = new MultiEnrollmentDirectionUtil.Parameters();

    // models
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _studentCategoryModel;
    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _groupModel;
    private ISelectModel _disciplineModel;
    private ISelectModel _passDateModel;

    private GroupWrapper _group;
    private DisciplineWrapper _discipline;

    // validator

    public Validator getValidator()
    {
        Discipline2RealizationWayRelation discipline = getDataSource().getCurrentEntity().getExamPassDiscipline().getEnrollmentCampaignDiscipline();
        return new EntrantMarkValidator(false, discipline.getMinMark(), discipline.getMaxMark());
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return getEnrollmentCampaign() == null ? null : Collections.singletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return getFormativeOrgUnit() == null ? null : Collections.singletonList(getFormativeOrgUnit());
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return getTerritorialOrgUnit() == null ? null : Collections.singletonList(getTerritorialOrgUnit());
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return getEducationLevelsHighSchool() == null ? null : Collections.singletonList(getEducationLevelsHighSchool());
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return getDevelopForm() == null ? null : Collections.singletonList(getDevelopForm());
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return getDevelopCondition() == null ? null : Collections.singletonList(getDevelopCondition());
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return getDevelopTech() == null ? null : Collections.singletonList(getDevelopTech());
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return getDevelopPeriod() == null ? null : Collections.singletonList(getDevelopPeriod());
    }

    public ISelectModel getPassDateModel()
    {
        return _passDateModel;
    }

    public void setPassDateModel(ISelectModel passDateModel)
    {
        _passDateModel = passDateModel;
    }

    // Filters

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _settings.get("enrollmentCampaign");
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _settings.set("enrollmentCampaign", enrollmentCampaign);
    }

    public CompensationType getCompensationType()
    {
        return (CompensationType) _settings.get("compensationType");
    }

    @SuppressWarnings("unchecked")
    public List<StudentCategory> getStudentCategoryList()
    {
        return (List<StudentCategory>) _settings.get("studentCategoryList");
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return (OrgUnit) _settings.get("formativeOrgUnit");
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return (OrgUnit) _settings.get("territorialOrgUnit");
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return (EducationLevelsHighSchool) _settings.get("educationLevelsHighSchool");
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return (DevelopForm) _settings.get("developForm");
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return (DevelopCondition) _settings.get("developCondition");
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return (DevelopTech) _settings.get("developTech");
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return (DevelopPeriod) _settings.get("developPeriod");
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<EntrantDisciplineMarkWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getStudentCategoryModel()
    {
        return _studentCategoryModel;
    }

    public void setStudentCategoryModel(ISelectModel studentCategoryModel)
    {
        _studentCategoryModel = studentCategoryModel;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelHighSchoolModel()
    {
        return _educationLevelHighSchoolModel;
    }

    public void setEducationLevelHighSchoolModel(ISelectModel educationLevelHighSchoolModel)
    {
        _educationLevelHighSchoolModel = educationLevelHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public ISelectModel getDisciplineModel()
    {
        return _disciplineModel;
    }

    public void setDisciplineModel(ISelectModel disciplineModel)
    {
        _disciplineModel = disciplineModel;
    }

    public GroupWrapper getGroup()
    {
        return _group;
    }

    public void setGroup(GroupWrapper group)
    {
        _group = group;
    }

    public DisciplineWrapper getDiscipline()
    {
        return _discipline;
    }

    public void setDiscipline(DisciplineWrapper discipline)
    {
        _discipline = discipline;
    }

    public String getDisciplineTitle() {
        return getDiscipline().getTitle();
    }

    public DataWrapper getPassDate()
    {
        return _settings.get("passDate");
    }

    public void setPassDate(DataWrapper passDate)
    {
        _settings.set("passDate", passDate);
    }
}
