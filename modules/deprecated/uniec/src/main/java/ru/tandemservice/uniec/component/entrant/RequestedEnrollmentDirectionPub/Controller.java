/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniec.component.entrant.RequestedEnrollmentDirectionPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.entrant.PriorityProfileEduOu;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author agolubenko
 * @since 16.03.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        createListDataSource(component);
        prepareListDataSource(component);
    }

    private void createListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<ProfileExaminationMark> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("Название предмета", new String[]{ProfileExaminationMark.L_DISCIPLINE, Discipline2RealizationWayRelation.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Оценка", ProfileExaminationMark.P_MARK).setClickable(false).setOrderable(false));
        getModel(component).setMarksDataSource(dataSource);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<PriorityProfileEduOuDTO> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().preparePriorityProfileEduOu(getModel(component1));
        });

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().educationLevelHighSchool().fullTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().formativeOrgUnit().shortTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().territorialOrgUnit().territorialShortTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developForm().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developCondition().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developTech().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок", PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().developPeriod().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Приоритет", PriorityProfileEduOu.priority().s()).setOrderable(false));

        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.REQUESTED_ENROLLMENT_DIRECTION_ADD_EDIT, new ParametersMap()
                .add("requestedEnrollmentDirectionId", getModel(component).getRequestedEnrollmentDirection().getId())
        ));
    }

    public void onClickEditState(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.REQUESTED_ENROLLMENT_DIRECTION_STATE_ADD_EDIT, new ParametersMap()
                .add("requestedEnrollmentDirectionId", getModel(component).getRequestedEnrollmentDirection().getId())
        ));
    }

    public void onClickEditPriorityProfileEduOu(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uniec.component.entrant.PriorityProfileEduOuEdit.Controller.class.getPackage().getName(), new ParametersMap()
                .add("requestedEnrollmentDirectionId", getModel(component).getRequestedEnrollmentDirection().getId())
        ));
    }
}
