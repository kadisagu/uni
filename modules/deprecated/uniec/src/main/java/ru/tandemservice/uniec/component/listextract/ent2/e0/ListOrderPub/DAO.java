/* $Id$ */
package ru.tandemservice.uniec.component.listextract.ent2.e0.ListOrderPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubDAO;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.AbstractListOrderPubModel;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.orders.SplitEntrantsStuListExtract;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2012
 */
public class DAO extends AbstractListOrderPubDAO<StudentListOrder, Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        for (DataWrapper wrapper : model.getDataSource().getEntityList())
        {
            SplitEntrantsStuListExtract extract = wrapper.get(AbstractListOrderPubModel.FIRST_EXTRACT);
            EducationOrgUnit eduOrgUnit = extract.getEcgpEntrantRecommended().getDistribution().getConfig().getEcgItem().getEducationOrgUnit();

            StringBuilder eduOrgUnitStrLines = new StringBuilder();
            eduOrgUnitStrLines.append(eduOrgUnit.getEducationLevelHighSchool().getDisplayableTitle());
            eduOrgUnitStrLines.append("\n").append(eduOrgUnit.getDevelopCombinationFullTitle()).append("\n");
            eduOrgUnitStrLines
                    .append("ФП: ").append(eduOrgUnit.getFormativeOrgUnit().getShortTitle())
                    .append(", ТП: ").append(eduOrgUnit.getTerritorialOrgUnit().getTerritorialShortTitle());

            wrapper.setProperty(Model.EDU_ORGUNIT_EXTENDED, eduOrgUnitStrLines.toString());
        }

    }
}