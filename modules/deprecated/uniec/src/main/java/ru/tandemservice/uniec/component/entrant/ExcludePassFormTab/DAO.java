/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.component.entrant.ExcludePassFormTab;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 26.07.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());

        // создаем данные для матрицы
        prepareListDataSource(model);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DevelopForm developForm = model.getSettings().get("developForm");

        // создаем группы

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.entrantRequest().entrant().s(), model.getEntrant()));
        if (developForm != null)
            directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().s(), developForm));

        EntrantDataUtil util = new EntrantDataUtil(getSession(), model.getEntrant().getEnrollmentCampaign(), directionBuilder);

        Map<EntrantRequest, Set<RequestedEnrollmentDirection>> map = new HashMap<>();
        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
        {
            Set<RequestedEnrollmentDirection> set = map.get(direction.getEntrantRequest());
            if (set == null)
                map.put(direction.getEntrantRequest(), set = new HashSet<>());
            set.add(direction);
        }

        List<EntrantRequest> entrantRequests = new ArrayList<>(map.keySet());
        Collections.sort(entrantRequests, new EntityComparator<EntrantRequest>(new EntityOrder(EntrantRequest.P_REG_DATE)));

        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
        List<EntrantRequestGroup> entrantRequestGroups = new ArrayList<>();
        for (EntrantRequest entrantRequest : entrantRequests)
        {
            List<RequestedEnrollmentDirection> directions = new ArrayList<>(map.get(entrantRequest));
            Collections.sort(directions, new EntityComparator<RequestedEnrollmentDirection>(new EntityOrder(RequestedEnrollmentDirection.P_PRIORITY)));

            List<EnrollmentDirectionGroup> enrollmentDirectionGroups = new ArrayList<>();

            for (RequestedEnrollmentDirection direction : directions)
            {
                List<ChosenEntranceDiscipline> chosenList = new ArrayList<>(util.getChosenEntranceDisciplineSet(direction));
                Collections.sort(chosenList, ITitled.TITLED_COMPARATOR);
                enrollmentDirectionGroups.add(new EnrollmentDirectionGroup(direction, chosenList));

                for (ChosenEntranceDiscipline chosen : chosenList)
                    disciplineSet.add(chosen.getEnrollmentCampaignDiscipline());
            }

            entrantRequestGroups.add(new EntrantRequestGroup(entrantRequest, enrollmentDirectionGroups));
        }

        model.setEntrantRequestGroups(entrantRequestGroups);

        // создаем список колонок

        MQBuilder formBuilder = new MQBuilder(Discipline2RealizationFormRelation.ENTITY_CLASS, "d", new String[]{Discipline2RealizationFormRelation.subjectPassForm().s()});
        formBuilder.add(MQExpression.eq("d", Discipline2RealizationFormRelation.discipline().enrollmentCampaign().s(), model.getEntrant().getEnrollmentCampaign()));
        formBuilder.add(MQExpression.in("d", Discipline2RealizationFormRelation.discipline().s(), disciplineSet));
        formBuilder.setNeedDistinct(true);

        List<SubjectPassForm> subjectPassFormList = formBuilder.getResultList(getSession());
        Collections.sort(subjectPassFormList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        model.setSubjectPassForms(subjectPassFormList);

        // создаем матрицу значений
        MQBuilder builder = new MQBuilder(ExcludeSubjectPassForm.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ExcludeSubjectPassForm.chosenEntranceDiscipline().enrollmentCampaignDiscipline().enrollmentCampaign().s(), model.getEntrant().getEnrollmentCampaign()));
        List<ExcludeSubjectPassForm> excludeList = builder.getResultList(getSession());

        Map<CellCoordinates, Boolean> matrixData = new HashMap<>();
        for (ExcludeSubjectPassForm item : excludeList)
            matrixData.put(new CellCoordinates(item.getChosenEntranceDiscipline().getId(), item.getSubjectPassForm().getId()), true);

        model.setMatrixData(matrixData);
    }

    @Override
    public void update(Model model)
    {
        MQBuilder builder = new MQBuilder(ExcludeSubjectPassForm.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ExcludeSubjectPassForm.chosenEntranceDiscipline().enrollmentCampaignDiscipline().enrollmentCampaign().s(), model.getEntrant().getEnrollmentCampaign()));
        List<ExcludeSubjectPassForm> excludeList = builder.getResultList(getSession());

        Map<CellCoordinates, ExcludeSubjectPassForm> matrixData = new HashMap<>();
        for (ExcludeSubjectPassForm item : excludeList)
            matrixData.put(new CellCoordinates(item.getChosenEntranceDiscipline().getId(), item.getSubjectPassForm().getId()), item);

        for (Map.Entry<CellCoordinates, Boolean> entry : model.getMatrixData().entrySet())
        {
            CellCoordinates cell = entry.getKey();
            if (entry.getValue())
            {
                if (!matrixData.containsKey(entry.getKey()))
                {
                    ChosenEntranceDiscipline chosen = get(ChosenEntranceDiscipline.class, cell.getRowId());
                    SubjectPassForm passForm = get(SubjectPassForm.class, cell.getColumnId());
                    if (chosen != null && passForm != null)
                    {
                        ExcludeSubjectPassForm item = new ExcludeSubjectPassForm();
                        item.setChosenEntranceDiscipline(chosen);
                        item.setSubjectPassForm(passForm);
                        save(item);
                    }
                }
            } else
            {
                ExcludeSubjectPassForm item = matrixData.get(entry.getKey());
                if (item != null)
                    delete(item);
            }
        }
    }
}
