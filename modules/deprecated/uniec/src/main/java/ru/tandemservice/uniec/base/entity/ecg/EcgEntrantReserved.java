package ru.tandemservice.uniec.base.entity.ecg;

import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgHasBringOriginal;
import ru.tandemservice.uniec.base.entity.ecg.gen.EcgEntrantReservedGen;

/**
 * Резервный абитуриент распределения
 */
public class EcgEntrantReserved extends EcgEntrantReservedGen implements IEcgHasBringOriginal
{
}