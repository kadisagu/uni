package ru.tandemservice.uniec.base.entity.ecg.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Конфигурация распределения абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EcgDistributionConfigGen extends EntityBase
 implements INaturalIdentifiable<EcgDistributionConfigGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig";
    public static final String ENTITY_NAME = "ecgDistributionConfig";
    public static final int VERSION_HASH = -298081792;
    private static IEntityMeta ENTITY_META;

    public static final String L_ECG_ITEM = "ecgItem";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_SECOND_HIGH_ADMISSION = "secondHighAdmission";

    private IPersistentEcgItem _ecgItem;     // Целевой объект распределения абитуриентов
    private CompensationType _compensationType;     // Вид возмещения затрат
    private boolean _secondHighAdmission;     // Распределение на второе высшее

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Целевой объект распределения абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public IPersistentEcgItem getEcgItem()
    {
        return _ecgItem;
    }

    /**
     * @param ecgItem Целевой объект распределения абитуриентов. Свойство не может быть null.
     */
    public void setEcgItem(IPersistentEcgItem ecgItem)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && ecgItem!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPersistentEcgItem.class);
            IEntityMeta actual =  ecgItem instanceof IEntity ? EntityRuntime.getMeta((IEntity) ecgItem) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_ecgItem, ecgItem);
        _ecgItem = ecgItem;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Распределение на второе высшее. Свойство не может быть null.
     */
    @NotNull
    public boolean isSecondHighAdmission()
    {
        return _secondHighAdmission;
    }

    /**
     * @param secondHighAdmission Распределение на второе высшее. Свойство не может быть null.
     */
    public void setSecondHighAdmission(boolean secondHighAdmission)
    {
        dirty(_secondHighAdmission, secondHighAdmission);
        _secondHighAdmission = secondHighAdmission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EcgDistributionConfigGen)
        {
            if (withNaturalIdProperties)
            {
                setEcgItem(((EcgDistributionConfig)another).getEcgItem());
                setCompensationType(((EcgDistributionConfig)another).getCompensationType());
                setSecondHighAdmission(((EcgDistributionConfig)another).isSecondHighAdmission());
            }
        }
    }

    public INaturalId<EcgDistributionConfigGen> getNaturalId()
    {
        return new NaturalId(getEcgItem(), getCompensationType(), isSecondHighAdmission());
    }

    public static class NaturalId extends NaturalIdBase<EcgDistributionConfigGen>
    {
        private static final String PROXY_NAME = "EcgDistributionConfigNaturalProxy";

        private Long _ecgItem;
        private Long _compensationType;
        private boolean _secondHighAdmission;

        public NaturalId()
        {}

        public NaturalId(IPersistentEcgItem ecgItem, CompensationType compensationType, boolean secondHighAdmission)
        {
            _ecgItem = ((IEntity) ecgItem).getId();
            _compensationType = ((IEntity) compensationType).getId();
            _secondHighAdmission = secondHighAdmission;
        }

        public Long getEcgItem()
        {
            return _ecgItem;
        }

        public void setEcgItem(Long ecgItem)
        {
            _ecgItem = ecgItem;
        }

        public Long getCompensationType()
        {
            return _compensationType;
        }

        public void setCompensationType(Long compensationType)
        {
            _compensationType = compensationType;
        }

        public boolean isSecondHighAdmission()
        {
            return _secondHighAdmission;
        }

        public void setSecondHighAdmission(boolean secondHighAdmission)
        {
            _secondHighAdmission = secondHighAdmission;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EcgDistributionConfigGen.NaturalId) ) return false;

            EcgDistributionConfigGen.NaturalId that = (NaturalId) o;

            if( !equals(getEcgItem(), that.getEcgItem()) ) return false;
            if( !equals(getCompensationType(), that.getCompensationType()) ) return false;
            if( !equals(isSecondHighAdmission(), that.isSecondHighAdmission()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEcgItem());
            result = hashCode(result, getCompensationType());
            result = hashCode(result, isSecondHighAdmission());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEcgItem());
            sb.append("/");
            sb.append(getCompensationType());
            sb.append("/");
            sb.append(isSecondHighAdmission());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EcgDistributionConfigGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EcgDistributionConfig.class;
        }

        public T newInstance()
        {
            return (T) new EcgDistributionConfig();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ecgItem":
                    return obj.getEcgItem();
                case "compensationType":
                    return obj.getCompensationType();
                case "secondHighAdmission":
                    return obj.isSecondHighAdmission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ecgItem":
                    obj.setEcgItem((IPersistentEcgItem) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "secondHighAdmission":
                    obj.setSecondHighAdmission((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ecgItem":
                        return true;
                case "compensationType":
                        return true;
                case "secondHighAdmission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ecgItem":
                    return true;
                case "compensationType":
                    return true;
                case "secondHighAdmission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ecgItem":
                    return IPersistentEcgItem.class;
                case "compensationType":
                    return CompensationType.class;
                case "secondHighAdmission":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EcgDistributionConfig> _dslPath = new Path<EcgDistributionConfig>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EcgDistributionConfig");
    }
            

    /**
     * @return Целевой объект распределения абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig#getEcgItem()
     */
    public static IPersistentEcgItemGen.Path<IPersistentEcgItem> ecgItem()
    {
        return _dslPath.ecgItem();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Распределение на второе высшее. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig#isSecondHighAdmission()
     */
    public static PropertyPath<Boolean> secondHighAdmission()
    {
        return _dslPath.secondHighAdmission();
    }

    public static class Path<E extends EcgDistributionConfig> extends EntityPath<E>
    {
        private IPersistentEcgItemGen.Path<IPersistentEcgItem> _ecgItem;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Boolean> _secondHighAdmission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Целевой объект распределения абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig#getEcgItem()
     */
        public IPersistentEcgItemGen.Path<IPersistentEcgItem> ecgItem()
        {
            if(_ecgItem == null )
                _ecgItem = new IPersistentEcgItemGen.Path<IPersistentEcgItem>(L_ECG_ITEM, this);
            return _ecgItem;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Распределение на второе высшее. Свойство не может быть null.
     * @see ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig#isSecondHighAdmission()
     */
        public PropertyPath<Boolean> secondHighAdmission()
        {
            if(_secondHighAdmission == null )
                _secondHighAdmission = new PropertyPath<Boolean>(EcgDistributionConfigGen.P_SECOND_HIGH_ADMISSION, this);
            return _secondHighAdmission;
        }

        public Class getEntityClass()
        {
            return EcgDistributionConfig.class;
        }

        public String getEntityName()
        {
            return "ecgDistributionConfig";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
