/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util.system;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.util.system.UniSystemUtils;
import ru.tandemservice.uniec.entity.entrant.EntrantInfoAboutUniversity;
import ru.tandemservice.uniec.entity.entrant.StateExamSubjectMark;

import java.io.*;
import java.util.*;

/**
 * use ReportPerson
 *
 * @author vip_delete
 * @since 28.01.2009
 */
@Deprecated
public class SampleEntrantReportGenerator
{
    private static final String _tandemUniPath = UniSystemUtils.getModulesDir().getAbsolutePath();
    private static final String _reportPath = SampleEntrantReportGenerator._tandemUniPath + "/uniec/src/main/java/ru/tandemservice/uniec/component/report/EntrantSamplesReport";
    private static final String _reportMetaFile = SampleEntrantReportGenerator._reportPath + "/sample.txt";
    private static final String _reportTooltipsFile = SampleEntrantReportGenerator._reportPath + "/sample_tooltips.txt";
    private static final String _columnVisibilityFile = SampleEntrantReportGenerator._reportPath + "/sample_columns.txt";
    private static final String _reportValidatorsFile = SampleEntrantReportGenerator._reportPath + "/sample_validators.txt";
    private static final String _reportTranslatorsFile = SampleEntrantReportGenerator._reportPath + "/sample_translators.txt";
    private static final Map<String, String[]> SET_IN_AND_QUERY_PARAMETERS_MAP = new HashMap<String, String[]>();

    static
    {
        /**
         * Map фильтров для случаев использования фильтров типа "и". <p/>
         * Перед генерацией компонента отчетов необходимо внести сюда данные для всех фильтров,
         * которым можно указать тип фильтрации "и".<p/>
         * Key - префикс конкретного фильтра.<br/>
         * Value - набор значений: <br/>
         * <название родительского префикса> - префикса с которым нужно сравнивать выборку, например, персона,<br/>
         * <название поля id в таблице поиска> - id с которым будет сравниваться id из общей выборки,<br/>
         * <название таблицы поиска> - таблица, в которой нужно искать данные фильтра<br/>
         * <название поля поиска> - поле, по которому нужно искать данные фильтра. <br/>
         */
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personEduInstitution." + PersonEduInstitution.L_EDUCATION_LEVEL_STAGE + "_id", new String[]{"_person.id", "person_id", "personeduinstitution_t", "educationlevelstage_id"});
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personEduInstitution." + PersonEduInstitution.L_DOCUMENT_TYPE + "_id", new String[]{"_person.id", "person_id", "personeduinstitution_t", "documenttype_id"});
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personEduInstitution." + PersonEduInstitution.L_GRADUATION_HONOUR + "_id", new String[]{"_person.id", "person_id", "personeduinstitution_t", "graduationhonour_id"});

        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personBenefit." + PersonBenefit.L_BENEFIT + "_id", new String[]{"_person.id", "person_id", "personbenefit_t", "benefit_id"});
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personSportAchievement." + PersonSportAchievement.L_SPORT_KIND + "_id", new String[]{"_person.id", "person_id", "personsportachievement_t", "sportkind_id"});
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personForeignLanguage." + PersonForeignLanguage.L_LANGUAGE + "_id", new String[]{"_person.id", "person_id", "personforeignlanguage_t", "language_id"});
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_personDormitoryBenefit." + PersonDormitoryBenefit.L_BENEFIT + "_id", new String[]{"_person.id", "person_id", "persondormitorybenefit_t", "benefit_id"});

        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_stateExamSubjectMark." + StateExamSubjectMark.L_SUBJECT + "_id", new String[]{"_entrantStateExamCertificate.id", "certificate_id", "stateexamsubjectmark_t", "subject_id"});
        SET_IN_AND_QUERY_PARAMETERS_MAP.put("_entrantInfoAboutUniversity." + EntrantInfoAboutUniversity.L_SOURCE_INFO + "_id", new String[]{"_entrant.id", "entrant_id", "entrantInfoAboutUniversity_t", "sourceInfo_id"});
    }

    public static void main(final String[] args) throws IOException
    {
        Map<String, String> tooltipsMap = new HashMap<String, String>();
        Map<String, String> validatorsMap = new HashMap<String, String>();
        Map<String, String> translatorsMap = new HashMap<String, String>();

        final BufferedReader toolTipReader = new BufferedReader(new InputStreamReader(new FileInputStream(SampleEntrantReportGenerator._reportTooltipsFile), "UTF-8"));

        String fileLine;
        while ((fileLine = toolTipReader.readLine()) != null)
        {
            fileLine = fileLine.trim();
            if ((fileLine.length() == 0) || (fileLine.charAt(0) == '#'))
            {
                continue;
            }

            int firstSpace = fileLine.indexOf(" ");
            if (firstSpace > 0)
                tooltipsMap.put(fileLine.substring(0, firstSpace), fileLine.substring(firstSpace + 1).trim());
        }

        final BufferedReader validatorsReader = new BufferedReader(new InputStreamReader(new FileInputStream(SampleEntrantReportGenerator._reportValidatorsFile), "UTF-8"));

        while ((fileLine = validatorsReader.readLine()) != null)
        {
            fileLine = fileLine.trim();
            if ((fileLine.length() == 0) || (fileLine.charAt(0) == '#'))
            {
                continue;
            }

            int firstSpace = fileLine.indexOf(" ");
            if (firstSpace > 0)
                validatorsMap.put(fileLine.substring(0, firstSpace), fileLine.substring(firstSpace + 1).trim());
            else
                validatorsMap.put(fileLine, null);
        }

        final BufferedReader translatorsReader = new BufferedReader(new InputStreamReader(new FileInputStream(SampleEntrantReportGenerator._reportTranslatorsFile), "UTF-8"));

        while ((fileLine = translatorsReader.readLine()) != null)
        {
            fileLine = fileLine.trim();
            if ((fileLine.length() == 0) || (fileLine.charAt(0) == '#'))
            {
                continue;
            }

            int firstSpace = fileLine.indexOf(" ");
            if (firstSpace > 0)
                translatorsMap.put(fileLine.substring(0, firstSpace), fileLine.substring(firstSpace + 1).trim());
            else
                validatorsMap.put(fileLine, null);
        }

        final OutputStreamWriter html = new OutputStreamWriter(new FileOutputStream(SampleEntrantReportGenerator._reportPath + "/EntrantSamplesReport.html"), "UTF-8");
        final OutputStreamWriter model = new OutputStreamWriter(new FileOutputStream(SampleEntrantReportGenerator._reportPath + "/EntrantSamplesAbstractReportModel.java"), "UTF-8");
        final OutputStreamWriter printBuilder = new OutputStreamWriter(new FileOutputStream(SampleEntrantReportGenerator._reportPath + "/EntrantSamplesFiltersPrintingBuilder.java"), "UTF-8");

        html.write(
                "<div jwcid=\"@tandem:mBlock\" securedObject=\"ognl:@org.tandemframework.sec.runtime.SecurityRuntime@getInstance().getCommonSecurityObject()\" permissionKey=\"viewEntrantSamplesReport\">\n" +
                        "<div jwcid=\"@tandem:mSticker\"><div jwcid=\"@Insert\" value=\"fast:businessComponent.title\"/></div>\n" +
                        "\n" +
                        "    <div jwcid=\"@tandem:mTabPanel\" id=\"jsTabForm\" selectedTabId=\"fast:model.selectedTabId\" regionName=\"jsTabForm\" active=\"true\">\n");

        model.write("/* $I" + "d$ */\n" +
                "\n" +
                "// Copyright 2006-2008 TANDEMFRAMEWORK\n" +
                "//\n" +
                "// Licensed under the TANDEMFRAMEWORK Public License;\n" +
                "// you may not use this file except in compliance with the License.\n" +
                "// You may obtain a copy of the License at\n" +
                "//\n" +
                "//     http://www.opensource.org/licenses/tandemframework.php\n" +
                "//\n" +
                "// Unless required by applicable law or agreed to in writing, software\n" +
                "// distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "// See the License for the specific language governing permissions and\n" +
                "// limitations under the License.\n" +
                "\n" +
                "package ru.tandemservice.uniec.component.report.EntrantSamplesReport;\n" +
                "\n" +
                "import org.tandemframework.core.entity.IdentifiableWrapper;\n" +
                "import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;\n" +
                "import org.tandemframework.shared.kladr.address.entity.AddressCountry;\n" +
                "import org.tandemframework.shared.kladr.address.entity.AddressItem;\n" +
                "import org.tandemframework.shared.kladr.address.entity.AddressStreet;\n" +
                "import ru.tandemservice.uni.component.reports.AndOrNotUIObject;\n" +
                "import ru.tandemservice.uni.component.reports.IReportModel;\n" +
                "import ru.tandemservice.uni.component.reports.YesNoUIObject;\n" +
                "import ru.tandemservice.uni.entity.catalog.*;\n" +
                "import ru.tandemservice.uni.entity.orgstruct.OrgUnit;\n" +
                "import ru.tandemservice.uniec.entity.catalog.*;\n" +
                "import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;\n" +
                "\n" +
                "import java.util.Date;\n" +
                "import java.util.List;\n" +
                "import java.util.Map;\n" +
                "import java.util.Arrays;\n" +
                "import ru.tandemservice.uni.util.UniMap;\n" +
                "\n" +
                "/**\n" +
                " * @author SampleEntrantReportGenerator\n" +
                " * @since 28.04.2009\n" +
                " */\n" +
                "public abstract class EntrantSamplesAbstractReportModel implements IReportModel\n" +
                "{\n" +
                "    private List<AndOrNotUIObject> _andOrNotList = AndOrNotUIObject.createAndOrNotList();\n" +
                "    private List<AndOrNotUIObject> _orNotList = AndOrNotUIObject.createOrNotList();\n" +
                "    private List<YesNoUIObject> _yesNoList = YesNoUIObject.createYesNoList();\n" +
                "\n" +
                "    public List<AndOrNotUIObject> getAndOrNotList()\n" +
                "    {\n" +
                "        return _andOrNotList;\n" +
                "    }\n" +
                "\n" +
                "    public List<AndOrNotUIObject> getOrNotList()\n" +
                "    {\n" +
                "        return _orNotList;\n" +
                "    }\n" +
                "\n" +
                "    public List<YesNoUIObject> getYesNoList()\n" +
                "    {\n" +
                "        return _yesNoList;\n" +
                "    }\n" +
                "\n" +
                "    // Ф И Л Ь Т Р Ы\n" +
                "");

        printBuilder.write("/* $Id$ */\n" +
                "\n" +
                "// Copyright 2006-2008 TANDEMFRAMEWORK\n" +
                "//\n" +
                "// Licensed under the TANDEMFRAMEWORK Public License;\n" +
                "// you may not use this file except in compliance with the License.\n" +
                "// You may obtain a copy of the License at\n" +
                "//\n" +
                "//     http://www.opensource.org/licenses/tandemframework.php\n" +
                "//\n" +
                "// Unless required by applicable law or agreed to in writing, software\n" +
                "// distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "// See the License for the specific language governing permissions and\n" +
                "// limitations under the License.\n" +
                "\n" +
                "package ru.tandemservice.uniec.component.report.EntrantSamplesReport;\n" +
                "\n" +
                "import jxl.format.CellFormat;\n" +
                "import jxl.write.WritableCell;\n" +
                "import org.tandemframework.core.view.formatter.DateFormatter;\n" +
                "import org.tandemframework.core.view.formatter.DoubleFormatter;\n" +
                "\n" +
                "import java.util.*;\n" +
                "\n" +
                "/**\n" +
                " * @author SampleEntrantReportGenerator\n" +
                " * @since 20.03.2009\n" +
                " */\n" +
                "public class EntrantSamplesFiltersPrintingBuilder\n" +
                "{\n" +
                "    private static Map<Integer, IFilterParameterExcelRenderer> FILTER_CAPTION_TITLES_MAP = new TreeMap<Integer, IFilterParameterExcelRenderer>();\n" +
                "\n" +
                "    static\n" +
                "    {\n");

        final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(SampleEntrantReportGenerator._reportMetaFile), "UTF-8"));

        final List<String[]> getsetList = new ArrayList<String[]>();
        final List<ReportParam> paramList = new ArrayList<ReportParam>();

        String line;
        boolean firstTab = true;
        String validator;
        String notNullValidator = " validators=\"validators:required\"";
        while ((line = reader.readLine()) != null)
        {
            line = line.trim();
            if ((line.length() == 0) || (line.charAt(0) == '#'))
            {
                continue;
            }

            final String[] data = StringUtils.split(line);

            if ("t".equals(data[0]))
            {
                final String title = data[2].trim().replace('_', ' ');

                if (firstTab)
                {
                    firstTab = false;
                } else
                {
                    html.write("            </table>\n" +
                            "        </div>\n" +
                            "        </div>\n");
                }
                html.write("        <div jwcid=\"@tandem:mHtmlTab\" id=\"" + data[1].trim() + "\" title=\"" + title + "\">\n" +
                        "            <div class=\"wrapper\">\n" +
                        "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");

                model.write("\n    // Таб «" + title + "»\n");

            } else if ("s".equals(data[0]))
            {
                final String title = data[1].trim().replace('_', ' ');

                html.write("                <tr>\n" +
                        "                    <td><div class=\"section\">" + title + "</div></td>\n" +
                        "                </tr>\n");

                model.write("\n    // Блок «" + title + "»\n");

            } else if ("f".equals(data[0]))
            {
                final ReportParam param = new ReportParam();
                param.logicFlag = Integer.parseInt(data[1].trim());
                param.name = data[2].trim();
                param.htmlFieldCode = data[3].trim();
                param.title = data[4].trim().replace('_', ' ');
                param.javaClass = data[5].trim();
                param.dependencies = data[6].trim();
                param.queryParams = data.length == 7 ? "" : data[7].trim();
                param.titleProperty = ("m".equals(data[3]) || "s".equals(data[3])) ? (data.length <= 8 ? "title" : data[8].trim()) : "";
                param.tooltip = tooltipsMap.get(param.name);

                paramList.add(param);

                if ("targetAdmissionKind".equals(param.name))
                    html.write("                <div jwcid=\"@tandem:mIf\" condition=\"fast:model.showTargetAdmissionKindFilter\">\n");

                String tooltip = null != param.tooltip ? " <div jwcid=\"@tandem:mTooltip\" tooltipMessage=\"" + param.tooltip + "\"/>" : "";
                html.write("                <tr><td><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                        "                    <tr><td><div>" + param.title + "&nbsp;" + tooltip + "</div></td></tr>\n" +
                        "                    <tr><td><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\n");

                // рисуем чекбокс активации фильтра

                model.write("    private boolean _" + param.name + "Active;\n");
                getsetList.add(new String[]{"boolean", param.name + "Active"});

                switch (param.logicFlag)
                {
                    case 0:
                        html.write("                    <td><div jwcid=\"" + param.name + "Active@tandem:mCheckbox\" value=\"fast:model." + param.name + "Active\" submitOnChange=\"ajax\" updateOnChange=\"" + param.name + "\"/></td>\n");
                        break;
                    case 1:
                        html.write("                    <td><div jwcid=\"" + param.name + "Active@tandem:mCheckbox\" value=\"fast:model." + param.name + "Active\" submitOnChange=\"ajax\" updateOnChange=\"" + param.name + "OrNot, " + param.name + "\"/></td>\n");
                        html.write("                    <td><div jwcid=\"" + param.name + "OrNot@tandem:mAutoCompleteSelect\" disabled=\"ognl:!model." + param.name + "Active\" style=\"width:45px;\" source=\"fast:model.orNotList\" value=\"fast:model." + param.name + "OrNot\" propertyId=\"id\" propertyView=\"title\" validators=\"validators:required\" displayName=\"Тип фильтрации для фильтра „" + param.title + "“\" hideLabel=\"true\"/></td>\n");
                        model.write("    private AndOrNotUIObject _" + param.name + "OrNot = _orNotList.get(0);\n");
                        getsetList.add(new String[]{"AndOrNotUIObject", param.name + "OrNot"});
                        break;
                    case 2:
                        html.write("                    <td><div jwcid=\"" + param.name + "Active@tandem:mCheckbox\" value=\"fast:model." + param.name + "Active\" submitOnChange=\"ajax\" updateOnChange=\"" + param.name + "AndOrNot, " + param.name + "\"/></td>\n");
                        html.write("                    <td><div jwcid=\"" + param.name + "AndOrNot@tandem:mAutoCompleteSelect\" disabled=\"ognl:!model." + param.name + "Active\" style=\"width:45px;\" source=\"fast:model.andOrNotList\" value=\"fast:model." + param.name + "AndOrNot\" propertyId=\"id\" propertyView=\"title\" validators=\"validators:required\" displayName=\"Тип фильтрации для фильтра „" + param.title + "“\" hideLabel=\"true\"/></td>\n");
                        model.write("    private AndOrNotUIObject _" + param.name + "AndOrNot = _andOrNotList.get(AndOrNotUIObject.INTERSECTION);\n");
                        getsetList.add(new String[]{"AndOrNotUIObject", param.name + "AndOrNot"});
                        break;
                    case 9:
                        html.write("                    <td><div jwcid=\"" + param.name + "Active@tandem:mCheckbox\" value=\"fast:model." + param.name + "Active\" submitOnChange=\"ajax\" updateOnChange=\"" + param.name + "\" disabled=\"true\"/></td>\n");
                        break;
                }

                if ("m".equals(param.htmlFieldCode))
                {
                    validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                    if ("-".equals(param.dependencies))
                    {
                        html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mMultiSelect\" disabled=\"ognl:!model." + param.name + "Active\" multidelete=\"false\" searchable=\"true\" values=\"fast:model." + param.name + "List\" model=\"fast:model." + param.name + "ListModel\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                    } else
                    {
                        html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mMultiSelect\" disabled=\"ognl:!model." + param.name + "Active\" multidelete=\"false\" searchable=\"true\" values=\"fast:model." + param.name + "List\" model=\"fast:model." + param.name + "ListModel\" submitOnChange=\"ajax\" updateOnChange=\"" + param.dependencies + "\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                    }
                    model.write("    private List<" + param.javaClass + "> _" + param.name + "List;\n");
                    model.write("    private IMultiSelectModel _" + param.name + "ListModel;\n");
                    getsetList.add(new String[]{"List<" + param.javaClass + ">", param.name + "List"});
                    getsetList.add(new String[]{"IMultiSelectModel", param.name + "ListModel"});
                } else if ("t".equals(param.htmlFieldCode))
                {
                    validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                    html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mTextField\" disabled=\"ognl:!model." + param.name + "Active\" value=\"fast:model." + param.name + "\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                    model.write("    private String _" + param.name + ";\n");
                    getsetList.add(new String[]{"String", param.name});
                } else if ("d".equals(param.htmlFieldCode))
                {
                    validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                    html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mDatePicker\" disabled=\"ognl:!model." + param.name + "Active\" value=\"fast:model." + param.name + "\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                    model.write("    private Date _" + param.name + ";\n");
                    getsetList.add(new String[]{"Date", param.name});
                } else if ("u".equals(param.htmlFieldCode))
                {
                    validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                    html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mTextField\" disabled=\"ognl:!model." + param.name + "Active\" value=\"fast:model." + param.name + "\" translator=\"translator:strong-number,pattern=#.##\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                    model.write("    private Double _" + param.name + ";\n");
                    getsetList.add(new String[]{"Double", param.name});
                } else if ("s".equals(param.htmlFieldCode))
                {
                    if (param.javaClass.equals("YesNoUIObject"))
                    {
                        validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                        html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mSelect\" disabled=\"ognl:!model." + param.name + "Active\" source=\"fast:model.yesNoList\" value=\"fast:model." + param.name + "\" propertyId=\"id\" propertyView=\"" + param.titleProperty + "\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                        model.write("    private YesNoUIObject _" + param.name + ";\n");
                        getsetList.add(new String[]{"YesNoUIObject", param.name});
                    } else
                    {
                        validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                        if ("-".equals(param.dependencies))
                        {
                            html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mSelect\" disabled=\"ognl:!model." + param.name + "Active\" source=\"fast:model." + param.name + "List\" value=\"fast:model." + param.name + "\" propertyId=\"id\" propertyView=\"" + param.titleProperty + "\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                        } else
                        {
                            html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mSelect\" disabled=\"ognl:!model." + param.name + "Active\" source=\"fast:model." + param.name + "List\" value=\"fast:model." + param.name + "\" propertyId=\"id\" propertyView=\"" + param.titleProperty + "\" submitOnChange=\"ajax\" updateOnChange=\"" + param.dependencies + "\" displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                        }
                        model.write("    private " + param.javaClass + " _" + param.name + ";\n");
                        model.write("    private List<" + param.javaClass + "> _" + param.name + "List;\n");
                        getsetList.add(new String[]{param.javaClass, param.name});
                        getsetList.add(new String[]{"List<" + param.javaClass + ">", param.name + "List"});
                    }
                } else if ("n".equals(param.htmlFieldCode))
                {
                    String translator = !translatorsMap.containsKey(param.name) ? " translator=\"translator:strong-number\"" : (null == translatorsMap.get(param.name) ? "" : " translator=\"translator:" + translatorsMap.get(param.name) + "\"");
                    validator = !validatorsMap.containsKey(param.name) ? notNullValidator : (null == validatorsMap.get(param.name) ? "" : (" validators=\"validators:" + validatorsMap.get(param.name) + "\""));
                    html.write("                    <td><div jwcid=\"" + param.name + "@tandem:mTextField\" disabled=\"ognl:!model." + param.name + "Active\" value=\"fast:model." + param.name + "\"" + translator + " displayName=\"" + param.title + "\" hideLabel=\"true\"" + validator + "/></td>\n");
                    model.write("    private Integer _" + param.name + ";\n");
                    getsetList.add(new String[]{"Integer", param.name});
                } else
                {
                    throw new RuntimeException("Unknown field type: " + param.htmlFieldCode);
                }

                html.write("                    </tr></table></td></tr>\n" +
                        "                </table></td></tr>\n");

                if ("targetAdmissionKind".equals(param.name))
                    html.write("                </div>\n");
            }
        }

        html.write("            </table>\n" +
                "           </div>\n" +
                "        </div>\n");

        // таб видимости колонок
        html.write("        <div jwcid=\"@tandem:mHtmlTab\" id=\"columns\" title=\"Настройки печати\">\n" +
                "            <div class=\"wrapper\">\n" +
                "            <div jwcid=\"printsettings@Any\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                <tr><td><div jwcid=\"@tandem:mCheckbox\" value=\"fast:model.checkAllColumns\" displayName=\"Выбрать все\" submitOnChange=\"true\" listener=\"checkAllColumns\" updateOnChange=\"printsettings\" /><br/></td></tr>\n" +
                "                <span jwcid=\"@tandem:mFor\" source=\"fast:model.columnSectionList\" value=\"fast:model.columnSection\">\n" +
                "                    <tr><td><div class=\"section\"><div jwcid=\"@Insert\" value=\"fast:model.columnSection\"/></div></td></tr>\n" +
                "                        <span jwcid=\"@tandem:mFor\" source=\"fast:model.columnList\" value=\"fast:model.column\">\n" +
                "                            <tr><td><div jwcid=\"@tandem:mCheckbox\" value=\"fast:model.columnVisible\" disabled=\"fast:model.columnVisibleDisabled\" displayName=\"fast:model.column\"/></td></tr>\n" +
                "                        </span>\n" +
                "                    <tr><td><br/></td></tr>\n" +
                "                </span>\n" +
                "            </table>\n" +
                "            </div>\n" +
                "            </div>\n" +
                "        </div>\n");
        html.write("    </div>\n" +
                "\n" +
                "<div class=\"wrapper\">\n" +
                "    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\n" +
                "        <td><div jwcid=\"@tandem:mSubmit\" name=\"save\" listener=\"onClickApply\" validate=\"true\" type=\"action\" id=\"submit_desktop\" disableSecondSubmit=\"false\">Сохранить</div></td>\n" +
                "        <td><div jwcid=\"@tandem:mSubmit\" name=\"cancel\" listener=\"deactivate\" type=\"cancel\">Отменить</div></td>\n" +
                "    </tr></table>\n" +
                "</div>\n" +
                "</div>");

        model.write("\n" +
                "    // Parameter Indexes\n");
        int counter = 0;
        for (final ReportParam param : paramList)
        {
            model.write("    public static final int " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX = " + (counter++) + ";\n");
        }

        //  isActive

        model.write("\n    @Override\n" +
                "    public boolean isActive(int parameterIndex)\n" +
                "    {\n" +
                "        switch (parameterIndex)\n" +
                "        {\n");

        for (final ReportParam param : paramList)
        {
            model.write("            case " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX: return is" + StringUtils.capitalize(param.name) + "Active();\n");
        }

        model.write("        }\n" +
                "        throw new RuntimeException(\"Unknown parameterIndex: \" + parameterIndex);\n" +
                "    }\n");

        // getValue

        model.write("\n    @Override\n" +
                "    public Object getValue(int parameterIndex)\n" +
                "    {\n" +
                "        switch (parameterIndex)\n" +
                "        {\n");

        for (final ReportParam param : paramList)
        {
            model.write("            case " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX: return ");
            if ("m".equals(param.htmlFieldCode))
            {
                model.write("get" + StringUtils.capitalize(param.name) + "List();\n");
            } else if (param.javaClass.equals("YesNoUIObject"))
            {
                model.write("get" + StringUtils.capitalize(param.name) + "().isTrue();\n");
            } else
            {
                model.write("get" + StringUtils.capitalize(param.name) + "();\n");
            }
        }

        model.write("        }\n" +
                "        throw new RuntimeException(\"Unknown parameterIndex: \" + parameterIndex);\n" +
                "    }\n");

        // getLogic

        model.write("\n    @Override\n" +
                "    public int getLogic(int parameterIndex)\n" +
                "    {\n" +
                "        switch (parameterIndex)\n" +
                "        {\n");

        for (final ReportParam param : paramList)
        {
            switch (param.logicFlag)
            {
                case 0:
                    if ("m".equals(param.htmlFieldCode))
                    {
                        model.write("            case " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX: return AndOrNotUIObject.INTERSECTION;\n");
                    } else
                    {
                        model.write("            case " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX: return -1;\n");
                    }
                    break;
                case 1:
                    model.write("            case " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX: return get" + StringUtils.capitalize(param.name) + "OrNot().getId();\n");
                    break;
                case 2:
                    model.write("            case " + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX: return get" + StringUtils.capitalize(param.name) + "AndOrNot().getId();\n");
                    break;
            }
        }

        model.write("        }\n" +
                "        throw new RuntimeException(\"Unknown parameterIndex: \" + parameterIndex);\n" +
                "    }\n");

        for (final ReportParam param : paramList)
        {
            printBuilder.write("        FILTER_CAPTION_TITLES_MAP.put(EntrantSamplesAbstractReportModel." + CoreStringUtils.getUpperStyleString(param.name) + "_PARAMETER_INDEX, new ");
            if ("m".equals(param.htmlFieldCode))
                printBuilder.write("FilterParametersExcelRenderer(\"" + param.title + "\", \"" + param.titleProperty + "\"));\n");
            else if ("s".equals(param.htmlFieldCode))
                printBuilder.write("FilterParametersExcelRenderer(\"" + param.title + "\", \"" + ("YesNoUIObject".equals(param.javaClass) ? "" : param.titleProperty) + "\"));\n");
            else if ("d".equals(param.htmlFieldCode))
                printBuilder.write("FilterParametersExcelRenderer(\"" + param.title + "\", \"\", DateFormatter.DEFAULT_DATE_FORMATTER));\n");
            else if ("u".equals(param.htmlFieldCode))
                printBuilder.write("FilterParametersExcelRenderer(\"" + param.title + "\", \"\",  DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS));\n");
            else
                printBuilder.write("FilterParametersExcelRenderer(\"" + param.title + "\", \"\"));\n");
        }

        // column visibility
        Map<String, List<String>> columnMap = new LinkedHashMap<String, List<String>>();
        {
            final BufferedReader columnReader = new BufferedReader(new InputStreamReader(new FileInputStream(SampleEntrantReportGenerator._columnVisibilityFile), "UTF-8"));
            List<String> currentList = new ArrayList<String>();
            String columnFileLine;
            while ((columnFileLine = columnReader.readLine()) != null)
            {
                columnFileLine = columnFileLine.trim();
                if ((columnFileLine.length() == 0) || (columnFileLine.charAt(0) == '#'))
                {
                    continue;
                }

                if ((columnFileLine.charAt(0) == 's'))
                {
                    currentList = new ArrayList<String>();
                    columnMap.put(columnFileLine.substring(2), currentList);
                }
                if ((columnFileLine.charAt(0) == 'c'))
                {
                    currentList.add(columnFileLine.substring(2));
                }
            }
        }
        model.write("\n   protected Map<String, List<String>> columnSectionsMap = new UniMap.Parametrized<String, List<String>>()\n");
        for (String section : columnMap.keySet())
        {
            model.write("           .add(\"" + section + "\", Arrays.asList(\"" + StringUtils.join(columnMap.get(section), "\", \"") + "\"))\n");
        }
        model.write("       ;\n\n");

        // generate getters & setters
        for (final String[] row : getsetList)
        {
            final String clazz = row[0];
            final String code = row[1];
            model.write("\n    public " + clazz + " " + (clazz.equals("boolean") ? "is" : "get") + "" + StringUtils.capitalize(code) + "()\n" +
                    "    {\n" +
                    "        return _" + code + ";\n" +
                    "    }\n\n" +
                    "    public void set" + StringUtils.capitalize(code) + "(" + clazz + " " + code + ")\n" +
                    "    {\n" +
                    "        _" + code + " = " + code + ";\n" +
                    "    }\n");

        }
        model.write("}\n");

        printBuilder.write("}\n" +
                "\n" +
                "    public static List<WritableCell[]> getFiltersTable(int column, int row, Model model, CellFormat captionCellFormat, CellFormat titlePropertyCellFormat)\n" +
                "    {\n" +
                "        List<WritableCell[]> filtersList = new ArrayList<WritableCell[]>();\n" +
                "\n" +
                "        filtersList.add(new FilterParametersExcelRenderer(\"Дата формирования отчета\", \"\", DateFormatter.DATE_FORMATTER_WITH_TIME).renderFilterParameter(column, row, new Date(), captionCellFormat, titlePropertyCellFormat));\n" +
                "        row++;\n" +
                "\n" +
                "        for (Map.Entry<Integer, IFilterParameterExcelRenderer> item : FILTER_CAPTION_TITLES_MAP.entrySet())\n" +
                "        {\n" +
                "            int parameterIndex = item.getKey();\n" +
                "            if (model.isActive(parameterIndex))\n" +
                "            {\n" +
                "                Object value = model.getValue(parameterIndex);\n" +
                "                filtersList.add(item.getValue().renderFilterParameter(column, row, value, captionCellFormat, titlePropertyCellFormat));\n" +
                "                row++;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        return filtersList;\n" +
                "    }\n" +
                "}\n");

        html.close();
        model.close();
        printBuilder.close();

        final OutputStreamWriter filterModel = new OutputStreamWriter(new FileOutputStream(SampleEntrantReportGenerator._reportPath + "/EntrantSamplesAbstractFilterModel.java"), "UTF-8");
        filterModel.write("/* $I" + "d" + "$ */\n" +
                "\n" +
                "// Copyright 2006-2008 TANDEMFRAMEWORK\n" +
                "//\n" +
                "// Licensed under the TANDEMFRAMEWORK Public License;\n" +
                "// you may not use this file except in compliance with the License.\n" +
                "// You may obtain a copy of the License at\n" +
                "//\n" +
                "//     http://www.opensource.org/licenses/tandemframework.php\n" +
                "//\n" +
                "// Unless required by applicable law or agreed to in writing, software\n" +
                "// distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "// See the License for the specific language governing permissions and\n" +
                "// limitations under the License.\n" +
                "\n" +
                "package ru.tandemservice.uniec.component.report.EntrantSamplesReport;\n" +
                "\n" +
                "import org.tandemframework.shared.kladr.address.entity.Address;\n" +
                "import org.tandemframework.shared.kladr.address.entity.AddressItem;\n" +
                "import ru.tandemservice.uni.component.reports.IReportFilter;\n" +
                "import ru.tandemservice.uni.component.reports.IReportModel;\n" +
                "import ru.tandemservice.uni.component.reports.SQLBuilder;\n" +
                "import ru.tandemservice.uni.entity.catalog.EducationLevels;\n" +
                "import ru.tandemservice.uni.entity.catalog.EducationalInstitutionTypeKind;\n" +
                "import ru.tandemservice.uni.entity.education.EducationOrgUnit;\n" +
                "import ru.tandemservice.uni.entity.employee.*;\n" +
                "import ru.tandemservice.uni.entity.sec.IdentityCard;\n" +
                "import ru.tandemservice.uni.entity.sec.Person;\n" +
                "import ru.tandemservice.uniec.entity.entrant.*;\n" +
                "\n" +
                "/**\n" +
                " * @author SampleEntrantReportGenerator\n" +
                " * @since 02.02.2009\n" +
                " */\n" +
                "abstract class EntrantSamplesAbstractFilterModel\n" +
                "{\n");

        for (final ReportParam param : paramList)
        {
            String paramUpperTitle = CoreStringUtils.getUpperStyleString(param.name);
            filterModel.write("    private static final IReportFilter " + CoreStringUtils.getUpperStyleString(param.name) + "_FILTER = new IReportFilter()\n" +
                    "    {\n" +
                    "        @Override\n" +
                    "        public void patchBuilder(SQLBuilder builder, IReportModel model, Object context)\n" +
                    "        {\n" +
                    "            if (model.isActive(EntrantSamplesAbstractReportModel." + paramUpperTitle + "_PARAMETER_INDEX))\n" +
                    "                ((EntrantSamplesAbstractFilterModel) context).patch" + StringUtils.capitalize(param.name) + "(builder, model, EntrantSamplesAbstractReportModel." + paramUpperTitle + "_PARAMETER_INDEX);\n" +
                    "        }\n" +
                    "    };\n" +
                    "\n");
        }

        filterModel.write("    public static final IReportFilter[] ALL_FILTERS = new IReportFilter[]{\n");
        for (final ReportParam param : paramList)
        {
            filterModel.write("            " + CoreStringUtils.getUpperStyleString(param.name) + "_FILTER,\n");
        }
        filterModel.write("    };\n\n");

        for (final ReportParam param : paramList)
        {
            if ("abstract".equals(param.queryParams))
            {
                filterModel.write("    public abstract void patch" + StringUtils.capitalize(param.name) + "(SQLBuilder builder, IReportModel model, int index);\n");
            } else
            {
                filterModel.write("    public void patch" + StringUtils.capitalize(param.name) + "(SQLBuilder builder, IReportModel model, int index)\n" +
                        "    {\n");

                final String[] q = StringUtils.split(param.queryParams, '$');
                if (q.length > 0)
                {
                    final String restriction = q[0].trim();
                    final String clazz = q[1].trim();
                    String alias = q[2].trim();
                    if (alias.isEmpty())
                    {
                        alias = StringUtils.uncapitalize(clazz);
                    }

                    final String property = q[3].trim();
                    final String postfix = q[4].trim();

                    if (param.logicFlag != 2)
                    {
                        filterModel.write("        builder." + restriction + "(\"_" + alias + ".\" + " + clazz + "." + ("id".equals(postfix) ? "L_" : "P_") + CoreStringUtils.getUpperStyleString(property) + " + \"_" + postfix + "\", index);\n");
                    } else
                    {
                        String[] inQueryParams = SET_IN_AND_QUERY_PARAMETERS_MAP.get("_" + alias + "." + property + "_" + postfix);
                        if (null == inQueryParams)
                            throw new RuntimeException("There is one or more filters haven't 'and' case parameters specified");
                        filterModel.write("        builder." + restriction + "(\"_" + alias + ".\" + " + clazz + "." + ("id".equals(postfix) ? "L_" : "P_") + CoreStringUtils.getUpperStyleString(property) + " + \"_" + postfix + "\", index, \"" + inQueryParams[0] + "\", \"" + inQueryParams[2] + "\", \"" + inQueryParams[1] + "\", \"" + inQueryParams[3] + "\");\n");
                    }
                }
                filterModel.write("    }\n");
            }
        }

        filterModel.write("}");
        filterModel.close();
    }

    public static class ReportParam
    {
        int logicFlag;
        String name;
        String htmlFieldCode;
        String title;
        String javaClass;
        String dependencies;
        String queryParams;
        String titleProperty;
        String tooltip; //tooltipMessage
    }
}