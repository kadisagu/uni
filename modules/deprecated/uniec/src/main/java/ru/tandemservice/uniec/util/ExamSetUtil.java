/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniec.util;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.ExamPassDisciplineGen;
import ru.tandemservice.uniec.entity.examset.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vip_delete
 * @since 10.02.2009
 */
public class ExamSetUtil
{
    public static String getExamSetItemId(ExamSet examSet, Long id)
    {
        int index = getExamSetItemIndex(examSet, id);
        return index == -1 ? null : examSet.getSetItemList().get(index).getExamSetItemId();
    }

    public static int getExamSetItemIndex(ExamSet examSet, Long id)
    {
        List<ExamSetItem> list = examSet.getSetItemList();
        int i = 0;
        while (i < list.size() && !(list.get(i).getId().equals(id))) i++;
        return i < list.size() ? i : -1;
    }

    public static List<EntranceDiscipline> getEntranceDiscipline(ExamSet examSet, String examSetItemId)
    {
        List<ExamSetItem> list = examSet.getSetItemList();
        int i = 0;
        while (i < list.size() && !(list.get(i).getExamSetItemId().equals(examSetItemId))) i++;
        return i < list.size() ? list.get(i).getList() : null;
    }

    public static ExamSet getExamSet(EnrollmentDirection enrollmentDirection, StudentCategory studentCategory)
    {
        for (ExamSet examSet : UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentDirection.getEnrollmentCampaign()).values())
            if (studentCategory.equals(examSet.getStudentCategory()) && examSet.getList().contains(enrollmentDirection))
                return examSet;
        return null;
    }

    public static ExamSet getExamSet(String examSetId)
    {
        Long enrollmentCampaignId = Long.parseLong(examSetId.substring(0, examSetId.indexOf('$')));
        EnrollmentCampaign enrollmentCampaign = UniDaoFacade.getCoreDao().get(EnrollmentCampaign.class, enrollmentCampaignId);
        return UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).get(examSetId);
    }

    public static Set<Discipline2RealizationWayRelation> getDisciplines(ExamSetItem examSetItem, Map<Long, Set<Discipline2RealizationWayRelation>> group2discipline)
    {
        // из всех SetDiscipline получаем дисциплины приемной кампании
        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
        Set<SetDiscipline> set = new HashSet<>();
        set.add(examSetItem.getSubject());
        set.addAll(examSetItem.getChoice());

        for (SetDiscipline setDiscipline : set)
        {
            if (setDiscipline instanceof DisciplinesGroup)
                disciplineSet.addAll(group2discipline.get(setDiscipline.getId()));
            else
                disciplineSet.add((Discipline2RealizationWayRelation) setDiscipline);
        }

        return disciplineSet;
    }

    /**
     * Для каждого направления приема вычисляет дисциплины приемной кампании входящих в наборы
     *
     * @param session             сессия
     * @param enrollmentCampaign  приемная кампания
     * @param studentCategoryList список категорий поступающих наборы по которыи надо учитывать
     *                            если нет в разницы в наборах по категориям или список категорий null или пуст, то берутся все наборы
     * @param compensationType    вид возмещения затрат. фильтрует вступительные испытания
     * @return мап приемная кампания -> список дисциплин приемной кампании из наборов
     */
    public static Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> getDirection2DisciplineMap(Session session, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, CompensationType compensationType)
    {
        boolean examSetDiff = enrollmentCampaign.isExamSetDiff();

        // groupId -> дисциплины приемной кампании
        Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(session, enrollmentCampaign);

        // направление приема -> список дисциплин приемной кампании  (наш мегамап, который надо бы уже вынести в утиль)
        Map<EnrollmentDirection, Set<Discipline2RealizationWayRelation>> direction2set = new HashMap<>();

        // получаем все наборы текущей приемной кампании
        Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();
        for (ExamSet examSet : examSetList)
        {
            // если есть различия в наборах и категория текущего набора не та, что выбрали на форме, то игнорируем такой набор
            if (examSetDiff && studentCategoryList != null && !studentCategoryList.isEmpty() && !studentCategoryList.contains(examSet.getStudentCategory()))
                continue;

            Set<Discipline2RealizationWayRelation> set = new HashSet<>();
            for (ExamSetItem examSetItem : examSet.getSetItemList())
            {
                // если выбрали бюджет на форме, а вступительное испытание не поддерживаем бюджет, то игнорируем его
                if (compensationType.isBudget() && !examSetItem.isBudget())
                    continue;

                // если выбрали контракт на форме, а вступительное испытание не поддерживаем контракт, то игнорируем его
                if (!compensationType.isBudget() && !examSetItem.isContract())
                    continue;

                set.addAll(getDisciplines(examSetItem, group2directions));
            }

            for (EnrollmentDirection direction : examSet.getList())
            {
                Set<Discipline2RealizationWayRelation> valueSet = direction2set.get(direction);
                if (valueSet == null)
                    direction2set.put(direction, valueSet = new HashSet<>());
                valueSet.addAll(set);
            }
        }

        Map<EnrollmentDirection, List<Discipline2RealizationWayRelation>> result = new HashMap<>();
        for (Map.Entry<EnrollmentDirection, Set<Discipline2RealizationWayRelation>> entry : direction2set.entrySet())
        {
            List<Discipline2RealizationWayRelation> disciplineList = new ArrayList<>(entry.getValue());
            Collections.sort(disciplineList, ITitled.TITLED_COMPARATOR);
            result.put(entry.getKey(), disciplineList);
        }
        return result;
    }

    /**
     * Получает представление набора экзамена в удобной для обработки форме для распределения оценок по вступительным испытаниям
     * все наборы по указанным категориям должны быть одинаковые, т.е.
     * а). количество вступительных испытаний в наборе A и B равно и их типы идут в одинаковом порядке
     * б). мн-во дисциплин приемной кампании, входящих строку набора А и B должны быть равны (для каждой строки)
     *
     * @param session             hibernate session
     * @param enrollmentCampaign  приемная кампания
     * @param studentCategoryList список категорий студента
     * @param compensationType    вид возмещения затрат. фильтрует вступительные испытания
     * @return мап направление приема -> структура набора экзамена
     *         TODO: сделать нормальный класс для структуры набора, а то PairKey не расширяемо...
     *         TODO: когда будет такой класс, тогда в следующем методе не будет нужды
     * @deprecated use getDirectionExamSetDataMap
     */
    @Deprecated
    public static Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> getDirection2disciplinesMap(Session session, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, CompensationType compensationType)
    {
        // groupId -> дисциплины приемной кампании
        Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(session, enrollmentCampaign);

        Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> direction2examSetDetailMap = getDirection2examSetDetailMap(enrollmentCampaign, studentCategoryList, compensationType);

        // направление приема -> структура набора экзамена по категориям
        Map<EnrollmentDirection, Map<StudentCategory, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>>> direction2structureMap = new HashMap<>();

        // переводим один мап в другой
        for (Map.Entry<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> directionEntry : direction2examSetDetailMap.entrySet())
        {
            // добавляем первый уровень
            Map<StudentCategory, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> directionMap = direction2structureMap.get(directionEntry.getKey());
            if (directionMap == null)
                direction2structureMap.put(directionEntry.getKey(), directionMap = new HashMap<>());

            for (Map.Entry<StudentCategory, List<ExamSetItem>> categoryEntry : directionEntry.getValue().entrySet())
            {
                // добавляем второй уровень
                List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> categoryMap = directionMap.get(categoryEntry.getKey());
                if (categoryMap == null)
                    directionMap.put(categoryEntry.getKey(), categoryMap = new ArrayList<>());

                for (ExamSetItem examSetItem : categoryEntry.getValue())
                {
                    categoryMap.add(PairKey.create(examSetItem.getKind(), getDisciplines(examSetItem, group2directions)));
                }
            }
        }

        // сейчас бизнес логика требует, чтобы в direction2structureMap для каждого направления приема структура набора экзамена была одинаковая
        // если это не так, то это направление приема не войдет в результирующий мап
        Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> result = new HashMap<>();

        for (Map.Entry<EnrollmentDirection, Map<StudentCategory, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>>> entry : direction2structureMap.entrySet())
        {
            Collection<List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> types2Disciplines = entry.getValue().values();
            int differenceCount = new HashSet<Object>(types2Disciplines).size();
            if (differenceCount == 1)
            {
                // все структуры наборов одинаковы
                result.put(entry.getKey(), types2Disciplines.iterator().next());
            }
        }
        return result;
    }

    /**
     * Получает представление набора экзамена в удобной для обработки форме
     *
     * @param enrollmentCampaign  приемная кампания
     * @param studentCategoryList список категорий студента
     * @param compensationType    вид возмещения затрат. фильтрует вступительные испытания
     * @return мап направление приема -> структура набора экзамена
     * @deprecated use getDirectionExamSetDataMap
     */
    @Deprecated
    public static Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> getDirection2examSetDetailMap(EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, CompensationType compensationType)
    {
        boolean examSetDiff = enrollmentCampaign.isExamSetDiff();

        // направление приема -> структура набора экзамена по категориям
        Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> direction2structureMap = new HashMap<>();

        // получаем все наборы текущей приемной кампании
        Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();

        // создаем распределение структур наборов экзаменов по направления приема и категориям поступающих
        for (ExamSet examSet : examSetList)
        {
            // если есть различия в наборах и категория текущего набора не та, что выбрали на форме, то игнорируем такой набор
            if (examSetDiff && studentCategoryList != null && !studentCategoryList.isEmpty() && !studentCategoryList.contains(examSet.getStudentCategory()))
            {
                continue;
            }

            List<ExamSetItem> structureList = new ArrayList<>();
            for (ExamSetItem item : examSet.getSetItemList())
            {
                // если выбрали бюджет на форме, а вступительное испытание не поддерживаем бюджет, то игнорируем его
                if (compensationType != null && compensationType.isBudget() && !item.isBudget())
                {
                    continue;
                }

                // если выбрали контракт на форме, а вступительное испытание не поддерживаем контракт, то игнорируем его
                if (compensationType != null && !compensationType.isBudget() && !item.isContract())
                {
                    continue;
                }

                structureList.add(item);
            }

            for (EnrollmentDirection direction : examSet.getList())
            {
                Map<StudentCategory, List<ExamSetItem>> map = direction2structureMap.get(direction);
                if (map == null)
                    direction2structureMap.put(direction, map = new HashMap<>());
                map.put(examSet.getStudentCategory(), structureList);
            }
        }

        return direction2structureMap;
    }

    public static Map<EnrollmentDirection, EnrollmentDirectionExamSetData> getDirectionExamSetDataMap(Session session, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, CompensationType compensationType)
    {
        // если есть хотя бы одна категория
        boolean hasStudentCategories = studentCategoryList != null && !studentCategoryList.isEmpty();

        // есть разница в наборах экзаменов по категориям
        boolean examSetDiff = enrollmentCampaign.isExamSetDiff();

        // направление приема -> строки набора экзамена по категориям
        Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> direction2categoryExamSetMap = new HashMap<>();

        // получаем все наборы экзаменов текущей приемной кампании
        Collection<ExamSet> examSetList = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign).values();

        // создаем распределение структур наборов экзаменов по направления приема и категориям поступающих
        for (ExamSet examSet : examSetList)
        {
            StudentCategory studentCategory = examSet.getStudentCategory();

            // если есть различия в наборах и категория текущего набора не та, что выбрали на форме, то игнорируем такой набор
            if (examSetDiff && hasStudentCategories && !studentCategoryList.contains(studentCategory)) continue;

            List<ExamSetItem> examSetItemList = new ArrayList<>();

            for (ExamSetItem item : examSet.getSetItemList())
            {
                // если выбрали бюджет на форме, а вступительное испытание не поддерживаем бюджет, то игнорируем его
                if (compensationType != null && compensationType.isBudget() && !item.isBudget()) continue;

                // если выбрали контракт на форме, а вступительное испытание не поддерживаем контракт, то игнорируем его
                if (compensationType != null && !compensationType.isBudget() && !item.isContract()) continue;

                // сохраняем строку экзамена
                examSetItemList.add(item);
            }

            for (EnrollmentDirection direction : examSet.getList())
            {
                Map<StudentCategory, List<ExamSetItem>> itemMap = direction2categoryExamSetMap.get(direction);

                if (itemMap == null)
                    direction2categoryExamSetMap.put(direction, itemMap = new HashMap<>());

                if (itemMap.put(studentCategory, examSetItemList) != null)
                    throw new RuntimeException("Duplicate ExamSet per Direction per StudentCategory!");
            }
        }

        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> result = new HashMap<>();

        // groupId -> дисциплины приемной кампании
        Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(session, enrollmentCampaign);

        for (EnrollmentDirection enrollmentDirection : UniDaoFacade.getCoreDao().getList(EnrollmentDirection.class))
        {
            Map<StudentCategory, List<ExamSetItem>> itemMap = direction2categoryExamSetMap.get(enrollmentDirection);

            if (itemMap == null)
            {
                result.put(enrollmentDirection, new EnrollmentDirectionExamSetData(enrollmentDirection, Collections.<StudentCategory, List<ExamSetItem>>emptyMap(), Collections.<StudentCategory, ExamSetStructure>emptyMap()));
            } else
            {
                Map<StudentCategory, ExamSetStructure> categoryExamSetStructureMap = new HashMap<>();

                for (Map.Entry<StudentCategory, List<ExamSetItem>> entry : itemMap.entrySet())
                {
                    List<ExamSetStructureItem> itemList = new ArrayList<>();
                    for (ExamSetItem item : entry.getValue())
                        itemList.add(new ExamSetStructureItem(item.getKind(), item.getType(), item.isBudget(), item.isContract(), getDisciplines(item, group2directions)));
                    categoryExamSetStructureMap.put(entry.getKey(), new ExamSetStructure(itemList));
                }

                result.put(enrollmentDirection, new EnrollmentDirectionExamSetData(enrollmentDirection, itemMap, categoryExamSetStructureMap));
            }
        }

        return result;
    }

    public static Map<EnrollmentDirection, EnrollmentDirectionExamSetData> getDirectionExamSetDataByTypeAndCategoryMap(Session session, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, CompensationType compensationType)
    {
        // если есть хотя бы одна категория
        boolean hasStudentCategories = studentCategoryList != null && !studentCategoryList.isEmpty();

        // есть разница в наборах экзаменов по категориям
        boolean examSetDiff = enrollmentCampaign.isExamSetDiff();

        // направление приема -> строки набора экзамена по категориям
        Map<EnrollmentDirection, Map<StudentCategory, List<ExamSetItem>>> direction2categoryExamSetMap = UniecDAOFacade.getExamSetDAO().getExamSetItemByEnrDirAndCategory(enrollmentCampaign);

        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> result = new HashMap<>();

        // groupId -> дисциплины приемной кампании
        Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(session, enrollmentCampaign);

        for (EnrollmentDirection enrollmentDirection : UniDaoFacade.getCoreDao().getList(EnrollmentDirection.class))
        {
            Map<StudentCategory, List<ExamSetItem>> itemMap = direction2categoryExamSetMap.get(enrollmentDirection);

            if (itemMap == null)
            {
                result.put(enrollmentDirection, new EnrollmentDirectionExamSetData(enrollmentDirection, Collections.<StudentCategory, List<ExamSetItem>>emptyMap(), Collections.<StudentCategory, ExamSetStructure>emptyMap()));
            } else
            {
                Map<StudentCategory, ExamSetStructure> categoryExamSetStructureMap = new HashMap<>();

                for (Map.Entry<StudentCategory, List<ExamSetItem>> entry : itemMap.entrySet())
                {
                    List<ExamSetStructureItem> itemList = new ArrayList<>();
                    for (ExamSetItem item : entry.getValue())
                        itemList.add(new ExamSetStructureItem(item.getKind(), item.getType(), item.isBudget(), item.isContract(), getDisciplines(item, group2directions)));
                    categoryExamSetStructureMap.put(entry.getKey(), new ExamSetStructure(itemList));
                }

                result.put(enrollmentDirection, new EnrollmentDirectionExamSetData(enrollmentDirection, itemMap, categoryExamSetStructureMap));
            }
        }

        return result;
    }


    /**
     * @return Формы сдачи дисциплин для выбранных направлений приема сгруппированные по абитуриентам и дисциплинам ВИ
     */
    public static Map<Entrant, Map<Discipline2RealizationWayRelation, List<SubjectPassForm>>> getExamPassDisciplineByEntrantByDiscipline(
            Session session, List<RequestedEnrollmentDirection> requestedDirections)
    {
        List<ExamPassDiscipline> examPassDisciplineList = new DQLSelectBuilder()
                .fromEntity(ExamPassDiscipline.class, "epd")
                .column(property("epd"))
                .where(existsByExpr(ChosenEntranceDiscipline.class, "ced",
                                    and(eq(property("epd", ExamPassDiscipline.enrollmentCampaignDiscipline()),
                                           property("ced", ChosenEntranceDiscipline.enrollmentCampaignDiscipline())),
                                        in(property("ced", ChosenEntranceDiscipline.chosenEnrollmentDirection()), requestedDirections))
                ))

                .createStatement(session).<ExamPassDiscipline>list();

        return examPassDisciplineList.stream()
                .filter(row -> row.getEntrantExamList().getEntrant() instanceof Entrant ||
                        row.getEntrantExamList().getEntrant() instanceof EntrantRequest)
                .collect(Collectors.groupingBy(
                        row -> {
                            IEntrant iEntrant = row.getEntrantExamList().getEntrant();
                            if (iEntrant instanceof Entrant)
                                return ((Entrant) iEntrant);
                            else if (iEntrant instanceof EntrantRequest)
                                return ((EntrantRequest) iEntrant).getEntrant();
                            return null;
                        },
                        Collectors.groupingBy(ExamPassDisciplineGen::getEnrollmentCampaignDiscipline,
                                              Collectors.mapping(ExamPassDisciplineGen::getSubjectPassForm, Collectors.toList()))
                ));
    }
}
