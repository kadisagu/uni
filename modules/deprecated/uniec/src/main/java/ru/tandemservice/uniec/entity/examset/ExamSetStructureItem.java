/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniec.entity.examset;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * Элемент структуры набора экзамена
 *
 * @author Vasily Zhukov
 * @since 04.08.2010
 */
public class ExamSetStructureItem
{
    private EntranceDisciplineKind _kind;
    private EntranceDisciplineType _type;
    private boolean _budget;
    private boolean _contract;
    private Set<Discipline2RealizationWayRelation> _disciplineSet;

    private int _hash;

    public ExamSetStructureItem(EntranceDisciplineKind kind, EntranceDisciplineType type, boolean budget, boolean contract, Set<Discipline2RealizationWayRelation> disciplineSet)
    {
        if (kind == null)
            throw new RuntimeException("EntranceDisciplineKind cannot be null!");
        if (disciplineSet == null)
            throw new RuntimeException("Set<Discipline2RealizationWayRelation> cannot be null!");
        _kind = kind;
        _type = type;
        _budget = budget;
        _contract = contract;
        _disciplineSet = Collections.unmodifiableSet(new HashSet<Discipline2RealizationWayRelation>(disciplineSet));

        {
            int hash = 0;
            hash = (hash*2 + _kind.getId().hashCode());
            hash = (hash*2 +  _disciplineSet.hashCode());
            _hash = hash;
        }
    }

    // Getters

    public EntranceDisciplineKind getKind()
    {
        return _kind;
    }

    public EntranceDisciplineType getType()
    {
        return _type;
    }

    public boolean isBudget()
    {
        return _budget;
    }

    public boolean isContract()
    {
        return _contract;
    }

    public Set<Discipline2RealizationWayRelation> getDisciplineSet()
    {
        return _disciplineSet;
    }

    public boolean isProfile()
    {
        return UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE.equals(getKind().getCode());
    }

    @Override
    public int hashCode()
    {
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) { return true; }
        if (!(obj instanceof ExamSetStructureItem)) { return false; }

        ExamSetStructureItem other = (ExamSetStructureItem) obj;
        if (!other._kind.equals(_kind)) { return false; }
        return other._disciplineSet.equals(_disciplineSet);
    }
}
