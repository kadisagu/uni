package ru.tandemservice.uniec.component.report.EntrantStudentForm;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;


/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/1/12
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniecDefines.SCRIPT_ENTRANT_STUDENT_FORM),
            "enrollmentCampaign", model.getEnrollmentCampaign(),
            "dateFrom", model.getDateFrom(),
            "dateTo", model.getDateTo(),
            "compensationType", model.isCompensTypeActive() ? model.getCompensationType() : null,
            "studentCategory", model.isStudentCategoryActive() ? model.getStudentCategoryList() : null,
            "qualification", model.isQualificationActive() ? model.getQualificationList() : null,
            "formativeOrgUnit", model.isFormativeOrgUnitActive() ? model.getFormativeOrgUnitList() : null,
            "territorialOrgUnit", model.isTerritorialOrgUnitActive() ? model.getTerritorialOrgUnitList() : null,
            "educationLevelHighSchool", model.isEducationLevelHighSchoolActive() ? model.getEducationLevelHighSchoolList() : null,
            "developForm", model.isDevelopFormActive() ? model.getDevelopFormList() : null,
            "developCondition", model.isDevelopConditionActive() ? model.getDevelopConditionList() : null,
            "developTech", model.isDevelopTechActive() ? model.getDevelopTechList() : null,
            "developPeriod", model.isDevelopPeriodActive() ? model.getDevelopPeriodList() : null,
            "parallel", model.isParallelActive() && model.getParallel().isTrue() ? Boolean.TRUE : Boolean.FALSE
        );
    }
}