/* $Id$ */
package ru.tandemservice.uniec.base.bo.EcEntrant.ui.CustomStateAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;

/**
 * @author nvankov
 * @since 4/3/13
 */
@Configuration
public class EcEntrantCustomStateAddEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS("entrantCustomStateCIDS", entrantCustomStateCIComboDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantCustomStateCIComboDS()
    {
        return new DefaultComboDataSourceHandler(getName(), EntrantCustomStateCI.class);
    }
}
