package ru.tandemservice.uniec.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.report.gen.EnrollmentResultByEGEReportGen;

/**
 * Результаты приема (средние баллы по ЕГЭ и число зачисленных по видам конкурса)
 */
public class EnrollmentResultByEGEReport extends EnrollmentResultByEGEReportGen implements IStorableReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}