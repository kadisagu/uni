// Copyright 2006-2012 Tandem Service Software
// Proprietary Licence for Tandem University

package ru.tandemservice.uniec.ws.enrollment;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Информация о приемной кампании для вебсервиса EnrollmentService
 *
 * @author Vasily Zhukov
 * @since 15.02.2011
 */
@XmlRootElement
public class EnrollmentEnvironmentNode
{
    /**
     * Название приемной кампании
     */
    @XmlAttribute(required = true)
    public String enrollmentCampaignTitle;

    /**
     * Название года обучения приемной кампании
     */
    @XmlAttribute(required = true)
    public String educationYearTitle;

    /**
     * Справочник "Вид возмещения затрат"
     */
    public CompensationTypeNode compensationType = new CompensationTypeNode();

    /**
     * Справочник "Формы сдачи дисциплин"
     */
    public SubjectPassFormNode subjectPassForm = new SubjectPassFormNode();

    /**
     * Справочник "Категории обучаемых"
     */
    public StudentCategoryNode studentCategory = new StudentCategoryNode();

    /**
     * Справочник "Виды вступительных испытаний"
     */
    public EntranceDisciplineKindNode entranceDisciplineKind = new EntranceDisciplineKindNode();

    /**
     * Справочник "Формы освоения"
     */
    public DevelopFormNode developForm = new DevelopFormNode();

    /**
     * Справочник "Условия освоения"
     */
    public DevelopConditionNode developCondition = new DevelopConditionNode();

    /**
     * Справочник "Технологии освоения"
     */
    public DevelopTechNode developTech = new DevelopTechNode();

    /**
     * Справочник "Сроки освоения"
     */
    public DevelopPeriodNode developPeriod = new DevelopPeriodNode();

    /**
     * Справочник "Квалификации (уровни профессионального образования)"
     */
    public QualificationNode qualification = new QualificationNode();

    /**
     * Справочник "Структура направлений подготовки (специальностей) Минобрнауки РФ"
     */
    public StructureEducationLevelNode structureEducationLevel = new StructureEducationLevelNode();

    /**
     * Используемые виды целевого приема в указанной приемной кампании
     */
    public TargetAdmissionKindNode targetAdmissionKind = new TargetAdmissionKindNode();

    /**
     * Дисциплины вступительных испытаний в указанной приемной кампании
     */
    public DisciplinePassWayNode disciplinePassWay = new DisciplinePassWayNode();

    /**
     * Группы дисциплин вступительных испытаний в указанной приемной кампании
     */
    public DisciplinePassWayGroupNode disciplinePassWayGroup = new DisciplinePassWayGroupNode();

    /**
     * Формы сдачи дисциплин вступительных испытаний в указанной приемной кампании
     */
    public DisciplinePassFormNode disciplinePassForm = new DisciplinePassFormNode();

    /**
     * Все типы всех используемых формирующих и территориальных подразделений из всех направлений приема в указанной приемной кампании
     */
    public OrgUnitTypeNode orgUnitType = new OrgUnitTypeNode();

    /**
     * Все используемые формирующие и территориальные подразделения из всех направлений приема в указанной приемной кампании
     */
    public OrgUnitNode orgUnit = new OrgUnitNode();

    /**
     * Все используемые направления подготовки (специальности) вуза из всех направлений приема в указанной приемной кампании
     */
    public EducationLevelHighSchoolNode educationLevelHighSchool = new EducationLevelHighSchoolNode();

    /**
     * Все направления приема в указанной приемной кампании
     */
    public EnrollmentDirectionNode enrollmentDirection = new EnrollmentDirectionNode();

    /**
     * Простая строка таблицы
     */
    public static class Row implements Comparable<Row>
    {
        public Row()
        {
        }

        public Row(String title, String id)
        {
            this.title = title;
            this.id = id;
        }

        public Row(String shortTitle, String title, String id)
        {
            this.shortTitle = shortTitle;
            this.title = title;
            this.id = id;
        }

        @Override
        public int compareTo(Row o)
        {
            return o.id.compareTo(o.toString());
        }

        /**
         * Сокращенное название
         */
        @XmlAttribute
        public String shortTitle;

        /**
         * Название
         */
        @XmlAttribute(required = true)
        public String title;

        /**
         * Идентификатор строки
         */
        @XmlAttribute(required = true)
        public String id;
    }

    /**
     * Справочник "Вид возмещения затрат"
     */
    public static class CompensationTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Формы сдачи дисциплин"
     */
    public static class SubjectPassFormNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Категории обучаемых"
     */
    public static class StudentCategoryNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Виды вступительных испытаний"
     */
    public static class EntranceDisciplineKindNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Формы освоения"
     */
    public static class DevelopFormNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Условия освоения"
     */
    public static class DevelopConditionNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Технологии освоения"
     */
    public static class DevelopTechNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Сроки освоения"
     */
    public static class DevelopPeriodNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Квалификации (уровни профессионального образования)"
     */
    public static class QualificationNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Справочник "Структура направлений подготовки (специальностей) Минобрнауки РФ"
     */
    public static class StructureEducationLevelNode
    {
        public static class StructureEducationLevelRow extends Row
        {
            public StructureEducationLevelRow()
            {
            }

            public StructureEducationLevelRow(String title, String id, String parent)
            {
                super(title, id);
                this.parent = parent;
            }

            /**
             * Родительский элемент
             *
             * @see StructureEducationLevelNode
             */
            @XmlAttribute
            public String parent;
        }

        public List<StructureEducationLevelRow> row = new ArrayList<StructureEducationLevelRow>();
    }

    /**
     * Используемые виды целевого приема в указанной приемной кампании
     */
    public static class TargetAdmissionKindNode
    {
        public static class TargetAdmissionKindRow extends Row
        {
            public TargetAdmissionKindRow()
            {
            }

            public TargetAdmissionKindRow(String shortTitle, String title, String id, String parent, int priority)
            {
                super(shortTitle, title, id);
                this.parent = parent;
                this.priority = priority;
            }

            /**
             * Родительский вид целевого приема
             *
             * @see TargetAdmissionKindNode
             */
            @XmlAttribute
            public String parent;

            /**
             * Приоритет
             */
            @XmlAttribute(required = true)
            public int priority;
        }

        public List<TargetAdmissionKindRow> row = new ArrayList<TargetAdmissionKindRow>();
    }

    /**
     * Дисциплины вступительных испытаний в указанной приемной кампании
     */
    public static class DisciplinePassWayNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Группы дисциплин вступительных испытаний в указанной приемной кампании
     */
    public static class DisciplinePassWayGroupNode
    {
        public static class GroupRow extends Row
        {
            public GroupRow()
            {
            }

            public GroupRow(String shortTitle, String title, String id, List<String> disciplines)
            {
                super(shortTitle, title, id);
                this.disciplines = disciplines;
            }

            /**
             * Список дисциплин вступительных испытаний в указанной приемной кампании
             * В xml отображается как список идентификаторов через пробел
             *
             * @see DisciplinePassWayNode
             */
            @XmlList
            @XmlAttribute(required = true)
            public List<String> disciplines;
        }

        public List<GroupRow> row = new ArrayList<GroupRow>();
    }

    /**
     * Формы сдачи дисциплин вступительных испытаний в указанной приемной кампании
     */
    public static class DisciplinePassFormNode
    {
        public static class FormRow
        {
            public FormRow()
            {
            }

            public FormRow(String disciplinePassWay, String subjectPassForm, String compensationType)
            {
                this.disciplinePassWay = disciplinePassWay;
                this.subjectPassForm = subjectPassForm;
                this.compensationType = compensationType;
            }

            /**
             * Дисциплина вступительного испытания
             *
             * @see DisciplinePassWayNode
             */
            @XmlAttribute(required = true)
            public String disciplinePassWay;

            /**
             * Формы сдачи дисциплины
             *
             * @see SubjectPassFormNode
             */
            @XmlAttribute(required = true)
            public String subjectPassForm;

            /**
             * Вид возмещения затрат
             *
             * @see CompensationTypeNode
             */
            @XmlAttribute(required = true)
            public String compensationType;
        }

        public List<FormRow> row = new ArrayList<FormRow>();
    }

    /**
     * Все типы всех используемых формирующих и территориальных подразделений из всех направлений приема в указанной приемной кампании
     */
    public static class OrgUnitTypeNode
    {
        public List<Row> row = new ArrayList<Row>();
    }

    /**
     * Все используемые формирующие и территориальные подразделения из всех направлений приема в указанной приемной кампании
     */
    public static class OrgUnitNode
    {
        /**
         * Подразделение
         */
        public static class OrgUnitRow extends Row
        {
            public OrgUnitRow()
            {
            }

            public OrgUnitRow(String shortTitle, String title, String id, String orgUnitType, String nominativeCaseTitle, String settlementTitle, String territorialTitle, String territorialShortTitle)
            {
                super(shortTitle, title, id);
                this.orgUnitType = orgUnitType;
                this.nominativeCaseTitle = nominativeCaseTitle;
                this.settlementTitle = settlementTitle;
                this.territorialTitle = territorialTitle;
                this.territorialShortTitle = territorialShortTitle;
            }

            /**
             * Тип подразделения
             *
             * @see OrgUnitTypeNode
             */
            @XmlAttribute(required = true)
            public String orgUnitType;

            /**
             * Печатное название подразделения в именительном падеже
             */
            @XmlAttribute
            public String nominativeCaseTitle;

            /**
             * Название населенного пункта из фактического адреса подразделения
             */
            @XmlAttribute
            public String settlementTitle;

            /**
             * Название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialTitle;

            /**
             * Сокращенное название подразделения в контексте территориального подр.
             */
            @XmlAttribute(required = true)
            public String territorialShortTitle;
        }

        public List<OrgUnitRow> row = new ArrayList<OrgUnitRow>();
    }

    /**
     * Все используемые направления подготовки (специальности) вуза из всех направлений приема в указанной приемной кампании
     */
    public static class EducationLevelHighSchoolNode
    {
        /**
         * Направление подготовки (специальность) вуза
         */
        public static class EducationLevelHighSchoolRow extends Row
        {
            public EducationLevelHighSchoolRow()
            {
            }

            public EducationLevelHighSchoolRow(String shortTitle, String title, String id, String code, String qualification)
            {
                super(shortTitle, title, id);
                this.code = code;
                this.qualification = qualification;
            }

            /**
             * Код
             */
            @XmlAttribute(required = true)
            public String code;

            /**
             * Квалификация (уровень профессионального образования)
             *
             * @see QualificationNode
             */
            @XmlAttribute(required = true)
            public String qualification;
        }

        public List<EducationLevelHighSchoolRow> row = new ArrayList<EducationLevelHighSchoolRow>();
    }

    /**
     * Все направления приема в указанной приемной кампании
     */
    public static class EnrollmentDirectionNode
    {
        /**
         * Вступительное испытание направления приема
         */
        public static class EntranceDisciplineRow
        {
            /**
             * Испытания по выбору: дисциплины вступительных испытаний и группы дисциплин вступительных испытаний
             * В xml отображается как список идентификаторов через пробел
             *
             * @see DisciplinePassWayNode, DisciplinePassWayGroupNode
             */
            @XmlList
            @XmlAttribute
            public List<String> choiceDisciplines;

            /**
             * Категория обучаемого
             *
             * @see StudentCategoryNode
             */
            @XmlAttribute(required = true)
            public String studentCategory;

            /**
             * Вид вступительного испытания
             */
            @XmlAttribute(required = true)
            public String entranceDisciplineKind;

            /**
             * true, если это вступительное испытание можно выбирать в заявлениях на бюджет
             * false, если нельзя
             */
            @XmlAttribute(required = true)
            public boolean budget;

            /**
             * true, если это вступительное испытание можно выбирать в заявлениях на контракт
             * false, если нельзя
             */
            @XmlAttribute(required = true)
            public boolean contract;

            /**
             * Дисциплина вступительного испытания или группа дисциплин вступительных испытаний
             *
             * @see DisciplinePassWayNode, DisciplinePassWayGroupNode
             */
            @XmlAttribute(required = true)
            public String discipline;
        }

        /**
         * Все вступительные испытания направления приема в указанной приемной кампании
         */
        public static class EntranceDisciplineNode
        {
            public List<EntranceDisciplineRow> row = new ArrayList<EntranceDisciplineRow>();
        }

        /**
         * План приема по виду целевого набора в направлении приема в указанной приемной кампании
         */
        public static class TargetAdmissionPlanRow
        {
            /**
             * Значение плана приема
             */
            @XmlAttribute(required = true)
            public int plan;

            /**
             * true, если план приема на бюджет
             * false, если план приема на контракт
             */
            @XmlAttribute(required = true)
            public boolean budget;

            /**
             * Вид целевого приема
             *
             * @see TargetAdmissionKindNode
             */
            @XmlAttribute(required = true)
            public String targetAdmissionKind;
        }

        /**
         * Все планы приема по видам целевых наборов в направлении приема в указанной приемной кампании
         */
        public static class TargetAdmissionPlanNode
        {
            public List<TargetAdmissionPlanRow> row = new ArrayList<TargetAdmissionPlanRow>();
        }

        /**
         * Направление приема в указанной приемной кампании
         */
        public static class DirectionRow
        {
            /**
             * План приема для слушателей
             */
            @XmlAttribute
            public Integer listenerPlan;

            /**
             * План приема на второе высшее
             */
            @XmlAttribute
            public Integer secondHighEducationPlan;

            /**
             * План приема по целевому набору на контракт
             */
            @XmlAttribute
            public Integer targetAdmissionPlanContract;

            /**
             * План приема по целевому набору на бюджет
             */
            @XmlAttribute
            public Integer targetAdmissionPlanBudget;

            /**
             * План приема на контракт
             */
            @XmlAttribute
            public Integer contractPlan;

            /**
             * План приема на бюджет
             */
            @XmlAttribute
            public Integer ministerialPlan;

            /**
             * true, если по этому направлению приема проводится собеседование
             */
            @XmlAttribute(required = true)
            public boolean interview;

            /**
             * true, если по этому направлению приема есть прием на контракт
             * false, если нету
             */
            @XmlAttribute(required = true)
            public boolean contract;

            /**
             * true, если по этому направлению приема есть прием на бюджет
             * false, если нету
             */
            @XmlAttribute(required = true)
            public boolean budget;

            /**
             * Срок освоения
             *
             * @see DevelopPeriodNode
             */
            @XmlAttribute(required = true)
            public String developPeriod;

            /**
             * Технология освоения
             *
             * @see DevelopTechNode
             */
            @XmlAttribute(required = true)
            public String developTech;

            /**
             * Условие освоения
             *
             * @see DevelopConditionNode
             */
            @XmlAttribute(required = true)
            public String developCondition;

            /**
             * Форма освоения
             *
             * @see DevelopFormNode
             */
            @XmlAttribute(required = true)
            public String developForm;

            /**
             * Элемент структуры направлений подготовки (специальностей) Минобрнауки РФ
             *
             * @see StructureEducationLevelNode
             */
            @XmlAttribute(required = true)
            public String structureEducationLevel;

            /**
             * Направление подготовки (специальности) вуза
             *
             * @see EducationLevelHighSchoolNode
             */
            @XmlAttribute(required = true)
            public String educationLevelHighSchool;

            /**
             * Территориальное подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String territorialOrgUnit;

            /**
             * Формирующее подразделение
             *
             * @see OrgUnitNode
             */
            @XmlAttribute(required = true)
            public String formativeOrgUnit;

            /**
             * Идентификатор направления подготовки подразделения.
             * Может использоваться для связи данных из других веб-сервисов
             */
            @XmlAttribute(required = true)
            public String educationOrgUnit;

            /**
             * Идентификатор этого направления приема
             */
            @XmlAttribute(required = true)
            public String id;

            /**
             * Все планы приема по видам целевых наборов в направлении приема в указанной приемной кампании
             */
            public TargetAdmissionPlanNode targetAdmissionPlan = new TargetAdmissionPlanNode();

            /**
             * Все вступительные испытания направления приема в указанной приемной кампании
             */
            public EntranceDisciplineNode entranceDiscipline = new EntranceDisciplineNode();
        }

        public List<DirectionRow> row = new ArrayList<DirectionRow>();
    }
}
